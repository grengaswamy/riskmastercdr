﻿

using System;
using System.Text;
using System.IO;
using System.Xml;
using System.Data.OracleClient;
using System.Data.SqlClient; 
using System.Data;
using System.Runtime.InteropServices;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System.Configuration;
using Amazon.S3.IO;


namespace Riskmaster.Application.FileStorage 
{
	///************************************************************** 
	///* $File				: FileStorageManager.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 16-Nov-2004
	///* $Author			: Neelima Dabral
	///***************************************************************	
	
	/// <summary>	
	///	This class contains methods to handle the files that are being kept at the Document Storage.
	///	Document Storage can either be File System or Database. 
	/// </summary>
    [ComVisible(true), GuidAttribute("815C5BA6-E35B-11DF-9778-05DDDED72085")]
    [ProgId("RMXFileStorage.RMXFileStorageManager")]
    [ClassInterface(ClassInterfaceType.None)]
    public class FileStorageManager
	{		

		#region Member Variables
		
		/// <summary>
		/// File Storage Type (File System or Database) 
		/// </summary>
		private StorageType   m_enmStorageType;

		/// <summary>
		/// Destination Storage Path
		/// </summary>
		private string m_sDestinationStoragePath;	
        private int m_iStorageId ;
        private int m_iClientId;
        private string sRegion = ConfigurationManager.AppSettings["Region"];
		#endregion	

		#region Properties

		/// <summary>
		/// Storage Type. If set, it overrides the value set 
		/// for the application from the configuration file.
		/// </summary>
		public StorageType FileStorageType
		{
			get
			{
				return m_enmStorageType; 
			}
			set
			{
				m_enmStorageType = value;				 
			}
		}
		
		/// <summary>
		/// Storage Path. If set, it overrides the value set 
		/// for the application from the configuration file.
		/// </summary>
		public string DestinationStoragePath
		{
			get
			{
				return m_sDestinationStoragePath; 
			}
			set
			{
				if(value!=null)	
				{
					m_sDestinationStoragePath = value; 
				}
				else
				{
					m_sDestinationStoragePath = string.Empty;
				}
			}
		}
		/// <summary>
		/// This will track updating the stored document in database server. Tanuj
		/// </summary>
		public int StorageId
		{
			get
			{
				return m_iStorageId; 
			}
			set
			{
				m_iStorageId= value ;
			}
		}
		#endregion 

		#region Constructors

		/// Name			: FileStorageManager
		/// Author			: Aditya Babbar
		/// Date Created	: 28-Jan-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
        public FileStorageManager(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}

		/// Name			: FileStorageManager
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_iStorageType">
		/// Storage Type (File System or Database)
		/// </param>
		/// <param name="p_sDestinationStoragePath">
		/// Destination storage path.
		/// If the storage type is File System then File System path
		/// else if the storage type is database then database connection string.
		/// </param>
		
		public FileStorageManager(StorageType p_iStorageType,string p_sDestinationStoragePath,int p_iClientId)
		{
			m_enmStorageType=p_iStorageType;
			m_sDestinationStoragePath = p_sDestinationStoragePath;
            m_iClientId = p_iClientId;
		}
		#endregion

        public int UpdateDocumentFile(byte[] p_objFileContents, string p_sDestinationFileName)
        {
            int iReturnValue = 0;
            string sPathOnly = "";
            string sFNOnly = "";
            int iFileId = 0;
            StringBuilder sbSql = null;
            DbWriter objWriter = null;
            DbReader objReader = null;
            FileStream objFileStream = null;
            BinaryWriter objBinaryWriter = null;
            bool bUpdate = false;

            try
            {
                if ((p_sDestinationFileName == null) || (p_sDestinationFileName.Trim() == ""))
                    throw new InvalidValueException
                        (Globalization.GetString("FileStorageManager.StoreFile.DestinationFileNotSpecified",m_iClientId));

                if (p_objFileContents == null)
                    throw new InvalidValueException
                        (Globalization.GetString("FileStorageManager.StoreFile.NullContents",m_iClientId));

                this.ParseFilePath(p_sDestinationFileName, ref sPathOnly, ref sFNOnly);
                if (m_enmStorageType == StorageType.S3BucketStorage)
                {
                    IAmazonS3 client;
                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
                    {

                        S3Bucket objS3bk = new S3Bucket();
                        objS3bk.BucketName = m_sDestinationStoragePath;
                        ListBucketsResponse responsebucket = client.ListBuckets();
                        if (responsebucket.Buckets.Contains(objS3bk))
                        {
                            PutBucketRequest request = new PutBucketRequest();
                            request.BucketName = m_sDestinationStoragePath;
                            client.PutBucket(request);
                        }

                        //  WritingAnObject();
                        try
                        {
                            // simple object put
                            PutObjectRequest request = new PutObjectRequest()
                            {
                                BucketName = m_sDestinationStoragePath,
                                Key = "Attachments/" + p_sDestinationFileName
                            };
                            request.InputStream = new MemoryStream(p_objFileContents);
                            PutObjectResponse response = client.PutObject(request);
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                        }
                    }
                }
                //Store file depending on document store type
                else if (m_enmStorageType == StorageType.FileSystemStorage)
                {
                    try
                    {
                        if (File.Exists(this.MaterializeFilename(sPathOnly, sFNOnly)))
                        {
                            File.Delete(this.MaterializeFilename(sPathOnly, sFNOnly));
                        }
                        objFileStream = new FileStream(this.MaterializeFilename(sPathOnly, sFNOnly),
                            FileMode.Create);
                    }
                    // If the specified Destination path does not exist in the File Store
                    // Raise error
                    catch (System.IO.DirectoryNotFoundException p_objException)
                    {
                        throw new FileInputOutputException
                            (Globalization.GetString("FileStorageManager.StoreFile.DestinationPathNotExist",m_iClientId), p_objException);
                    }
                    objBinaryWriter = new BinaryWriter(objFileStream);
                    objBinaryWriter.Write(p_objFileContents);                 
                    objBinaryWriter.Close();
                    objFileStream.Close();
                }
                else if (m_enmStorageType == StorageType.DatabaseStorage)
                {
                    if ((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
                        throw new InvalidValueException
                            (Globalization.GetString("FileStorageManager.StoreFile.DestPathNotSpecified",m_iClientId));

                    //Target is Database, So store that file in the DOCUMENT_STORAGE table
                    iFileId = this.GetNextUID("DOCUMENT_STORAGE");

                    sbSql = new StringBuilder();
                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("SELECT STORAGE_ID FROM DOCUMENT_STORAGE WHERE");
                    sbSql.Append(" UPPER(FILENAME) = UPPER('" + sFNOnly.Replace("'", "''") + "')  AND ");
                    sbSql.Append(" DIR_PATH = '" + sPathOnly.Replace("'", "''") + "'");
                    objReader = DbFactory.GetDbReader(m_sDestinationStoragePath, sbSql.ToString());
                    bUpdate = false;
                    if (objReader.Read())
                        bUpdate = true;
                    objReader.Close();
                    objReader.Dispose();
                    objReader = null;

                    objWriter = DbFactory.GetDbWriter(m_sDestinationStoragePath);
                    objWriter.Tables.Add("DOCUMENT_STORAGE");
                    objWriter.Fields.Add("STORAGE_ID", iFileId);
                    objWriter.Fields.Add("FILENAME", sFNOnly);
                    objWriter.Fields.Add("DIR_PATH", sPathOnly);
                    objWriter.Fields.Add("STORE_TYPE", 0);
                    objWriter.Fields.Add("DOC_BLOB", p_objFileContents);

                    if (bUpdate)
                        // Update existing record				
                        objWriter.Where.Add(" FILENAME='" + sFNOnly.Replace("'", "''") +
                                                "' AND DIR_PATH = '" + sPathOnly.Replace("'", "''") + "'");

                    objWriter.Execute();
                    objWriter.Reset(true);
                }
                iReturnValue = 1;
            }
            catch (FileInputOutputException p_objException)
            {
                throw p_objException;
            }
            catch (InvalidValueException p_objException)
            {
                throw p_objException;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException
                    (Globalization.GetString("FileStorageManager.StoreFile.Error",m_iClientId), p_objException);
            }
            finally
            {
                sbSql = null;
                objWriter = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objFileStream != null)
                {
                    objFileStream.Close();
                    objFileStream = null;
                }
                if (objBinaryWriter != null)
                {
                    objBinaryWriter.Close();
                    objBinaryWriter = null;
                }
            }
            return iReturnValue;
        }

        
		#region Public Methods
		/// Name			: StoreFile
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will store specified file in the document storage.
        ///		It will overwrite any existing file with same name and path in the destination store.
		/// </summary>
		/// <param name="p_sSourceFileName">
		///		Fully qualified DOS or UNC path to the file to store
		/// </param>
		/// <param name="p_sDestinationFileName">
		///		A document storage relative path + filename
		///		Examples are \README.DOC, \SMNET\README.DOC, \RDMS\LOSSENGINE\README.DOC, etc.
		/// </param>
		/// <returns>1 - Success; 0 - Failure</returns>
		public int StoreFile(string p_sSourceFileName,string p_sDestinationFileName)
		{
			int				iReturnValue = 0;
			string			sPathOnly = "";
			string			sFNOnly = "";
			int				iFileId=0;
			StringBuilder	sbSql=null;
			DbWriter		objWriter = null;
			DbReader		objReader = null;
			bool            bUpdate = false;

			try
			{
				if((p_sSourceFileName == null) || (p_sSourceFileName.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.StoreFile.SourceFileNotSpecified",m_iClientId));

				if((p_sDestinationFileName == null) || (p_sDestinationFileName.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.StoreFile.DestinationFileNotSpecified",m_iClientId));
				
				// If the file to be copied in the File Store does not exist
				// Raise error
				if(!File.Exists(p_sSourceFileName))
					throw new FileInputOutputException
						(Globalization.GetString("FileStorageManager.StoreFile.SourceFileNotExist",m_iClientId));

				this.ParseFilePath(p_sDestinationFileName, ref sPathOnly, ref sFNOnly);

                if (m_enmStorageType == StorageType.S3BucketStorage)
                {
                    IAmazonS3 client;
                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
                    {
                        PutObjectResponse objresponse = null;
                        try
                        {
                            //Check S3 Bucket Exists or not
                            S3Bucket objS3bk = new S3Bucket();
                            objS3bk.BucketName = m_sDestinationStoragePath;
                            ListBucketsResponse response = client.ListBuckets();
                            if (response.Buckets.Contains(objS3bk))
                            {
                                PutBucketRequest request = new PutBucketRequest();
                                request.BucketName = m_sDestinationStoragePath;
                                client.PutBucket(request);
                            }
                            // simple object put
                            PutObjectRequest objrequest = new PutObjectRequest()
                            {
                                BucketName = m_sDestinationStoragePath,
                                Key = "Attachments/" + sFNOnly
                            };
                            MemoryStream ms=new MemoryStream();
                            using (FileStream file = new FileStream(p_sSourceFileName, FileMode.Open, FileAccess.Read))
                            {
                                byte[] bytes = new byte[file.Length];
                                file.Read(bytes, 0, (int)file.Length);
                                ms.Write(bytes, 0, (int)file.Length);
                            }
                            objrequest.InputStream = ms;
                            //objrequest.Headers.ContentLength = long.Parse(p_sFileSize);
                            objresponse = client.PutObject(objrequest);
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                        }
                    }
                }
				//Store file depending on File store type
				else if (m_enmStorageType == StorageType.FileSystemStorage)
				{
					try
					{
						//File Server , so Copy the file in Destination Folder
						File.Copy(p_sSourceFileName,this.MaterializeFilename(sPathOnly, sFNOnly),true);
					}
					// Raise error if the specified path does not exist in the File Store.
					catch(System.IO.DirectoryNotFoundException  p_objException)
					{
						throw new FileInputOutputException
							(Globalization.GetString("FileStorageManager.StoreFile.DestinationPathNotExist",m_iClientId),p_objException);
					}
				}
				else if (m_enmStorageType == StorageType.DatabaseStorage)
				{
					if((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
						throw new InvalidValueException
							(Globalization.GetString("FileStorageManager.StoreFile.DestPathNotSpecified",m_iClientId));

					//Target is Database, So store that file in the DOCUMENT_STORAGE table
					iFileId = this.GetNextUID("DOCUMENT_STORAGE");
					sbSql = new StringBuilder();
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT STORAGE_ID FROM DOCUMENT_STORAGE WHERE ");
					sbSql.Append(" UPPER(FILENAME) = UPPER('" + sFNOnly.Replace("'","''") + "') ");					
					sbSql.Append(" AND DIR_PATH = '" + sPathOnly.Replace("'","''") + "'");
					objReader = DbFactory.GetDbReader(m_sDestinationStoragePath,sbSql.ToString());
					bUpdate = false;
					if (objReader.Read())
						bUpdate = true;
					objReader.Close();
					objReader.Dispose();
					objReader = null;

					objWriter = DbFactory.GetDbWriter(m_sDestinationStoragePath);
					objWriter.Tables.Add("DOCUMENT_STORAGE");
					objWriter.Fields.Add("STORAGE_ID",iFileId);
					objWriter.Fields.Add("FILENAME",sFNOnly);
					objWriter.Fields.Add("DIR_PATH",sPathOnly);
					objWriter.Fields.Add("STORE_TYPE",0);
					objWriter.Fields.Add("DOC_BLOB",this.FileAsByteArray(p_sSourceFileName));

					if(bUpdate)
						// Update existing record				
						objWriter.Where.Add(" FILENAME='" + sFNOnly.Replace("'","''") 
							+ "' AND DIR_PATH = '" + sPathOnly.Replace("'","''") + "'" );
					
					objWriter.Execute();
					objWriter.Reset(true);
                    //Deb:MITS 22213 Set StorageId property for future use
                    this.StorageId = iFileId;
                    //Deb:MITS 22213
				}
	
				iReturnValue = 1;
			}
			catch(FileInputOutputException p_objException)
			{
				throw p_objException;				
			}	
			catch(InvalidValueException p_objException)
			{
				throw p_objException;				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.StoreFile.Error",m_iClientId),p_objException);
			}
			finally
			{
				sbSql = null;
				objWriter = null;		
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return iReturnValue;
		}

		/// Name			: StoreFile
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will store the binary stream in the document storage.
		///		It will overwrite any existing file with same name and path in the destination store.
		/// </summary>
		/// <param name="p_objFileContents">
		///		Binary stream to be stored in the document storage
		/// </param>
		/// <param name="p_sDestinationFileName">
		///		A document storage relative path + filename
		///		Examples are \README.DOC, \SMNET\README.DOC, \RDMS\LOSSENGINE\README.DOC, etc.
		/// </param>
		/// <returns>1 - Success; 0 - Failure</returns>
		public int StoreFile(MemoryStream p_objFileContents ,string p_sDestinationFileName)
		{
			int				iReturnValue = 0;
			string			sPathOnly = "";
			string			sFNOnly = "";
			int				iFileId=0;
			StringBuilder	sbSql=null;
			DbWriter		objWriter = null;
			DbReader		objReader = null;
			FileStream		objFileStream = null;
			BinaryWriter	objBinaryWriter=null;
			bool            bUpdate = false;

			try
			{				
				if((p_sDestinationFileName == null) || (p_sDestinationFileName.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.StoreFile.DestinationFileNotSpecified",m_iClientId));

				if(p_objFileContents == null)
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.StoreFile.NullContents",m_iClientId));

				this.ParseFilePath(p_sDestinationFileName, ref sPathOnly, ref sFNOnly);
                if (m_enmStorageType == StorageType.S3BucketStorage)
                {
                    IAmazonS3 client;
                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
                    {

                        S3Bucket objS3bk = new S3Bucket();
                        objS3bk.BucketName = m_sDestinationStoragePath;
                        ListBucketsResponse responsebucket = client.ListBuckets();
                        if (responsebucket.Buckets.Contains(objS3bk))
                        {
                            PutBucketRequest request = new PutBucketRequest();
                            request.BucketName = m_sDestinationStoragePath;
                            client.PutBucket(request);
                        }

                        //  WritingAnObject();
                        try
                        {
                            // simple object put
                            PutObjectRequest request = new PutObjectRequest()
                            {
                                BucketName = m_sDestinationStoragePath,
                                Key = "Attachments/" + p_sDestinationFileName
                            };
                            request.InputStream = p_objFileContents;
                            PutObjectResponse response = client.PutObject(request);
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                        }
                    }
                }
                //Store file depending on document store type
                else if (m_enmStorageType == StorageType.FileSystemStorage)
				{
					try
					{
						objFileStream = new FileStream(this.MaterializeFilename(sPathOnly, sFNOnly),
							FileMode.Create);										
					}
					// If the specified Destination path does not exist in the File Store
					// Raise error
					catch(System.IO.DirectoryNotFoundException p_objException)
					{
						throw new FileInputOutputException
							(Globalization.GetString("FileStorageManager.StoreFile.DestinationPathNotExist",m_iClientId),p_objException);
					}
					objBinaryWriter = new BinaryWriter(objFileStream);	
					objBinaryWriter.Write(p_objFileContents.ToArray());								
					p_objFileContents.Close();
					objBinaryWriter.Close();
					objFileStream.Close();
				}
				else if(m_enmStorageType == StorageType.DatabaseStorage)
				{
					if((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
						throw new InvalidValueException
							(Globalization.GetString("FileStorageManager.StoreFile.DestPathNotSpecified",m_iClientId));

					//Target is Database, So store that file in the DOCUMENT_STORAGE table
					iFileId = this.GetNextUID("DOCUMENT_STORAGE");
					
					sbSql = new StringBuilder();
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT STORAGE_ID FROM DOCUMENT_STORAGE WHERE");
					sbSql.Append(" UPPER(FILENAME) = UPPER('" + sFNOnly.Replace("'","''") + "') AND ");
					sbSql.Append(" DIR_PATH = '" + sPathOnly.Replace("'","''") + "'");
					objReader = DbFactory.GetDbReader(m_sDestinationStoragePath,sbSql.ToString());
					bUpdate = false;
					if(objReader.Read())
						bUpdate = true;
					objReader.Close();
					objReader.Dispose();
					objReader = null;

					objWriter = DbFactory.GetDbWriter(m_sDestinationStoragePath);
					objWriter.Tables.Add("DOCUMENT_STORAGE");
					objWriter.Fields.Add("STORAGE_ID",iFileId);
					objWriter.Fields.Add("FILENAME",sFNOnly);
					objWriter.Fields.Add("DIR_PATH",sPathOnly);
					objWriter.Fields.Add("STORE_TYPE",0);
					objWriter.Fields.Add("DOC_BLOB",p_objFileContents.ToArray());

					if(bUpdate)
						// Update existing record				
						objWriter.Where.Add(" FILENAME='" + sFNOnly.Replace("'","''") + 
												"' AND DIR_PATH = '" + sPathOnly.Replace("'","''") + "'" );
					
					objWriter.Execute();
					objWriter.Reset(true);					
				}	
				iReturnValue = 1;
			}
			catch(FileInputOutputException p_objException)
			{
				throw p_objException;				
			}
			catch(InvalidValueException p_objException)
			{
				throw p_objException;				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.StoreFile.Error",m_iClientId),p_objException);
			}
			finally
			{
				sbSql=null;
				objWriter = null;		
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}				
				if(objFileStream != null)
				{
					objFileStream.Close();
					objFileStream = null;
				}
				if(objBinaryWriter != null)
				{
					objBinaryWriter.Close();
					objBinaryWriter = null;
				}			
			}
			return iReturnValue;
		}
		
		/// Name			: UpdateFile
		/// Author			: Tanuj Narula
		/// Date Created	: 29-May-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method updates the BLOB stored in database.
		/// </summary>
		/// <param name="p_sSourceFile">file to be dumped.</param>
		
        public int StoreLargeFile(Stream p_objFileContents,string p_sDestinationFileName,string p_sFileSize)
        {
            int				iReturnValue = 0;
			string			sPathOnly = "";
			string			sFNOnly = "";
			int				iFileId=0;
			StringBuilder	sbSql=null;
			DbWriter		objWriter = null;
			DbReader		objReader = null;
			FileStream		objFileStream = null;
			BinaryWriter	objBinaryWriter=null;
			bool            bUpdate = false;
            bool            bResult = false;
            string attachSizeWithoutChunk = string.Empty;

			try
			{				
				if((p_sDestinationFileName == null) || (p_sDestinationFileName.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.StoreFile.DestinationFileNotSpecified",m_iClientId));

				if(p_objFileContents == null)
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.StoreFile.NullContents",m_iClientId));

				this.ParseFilePath(p_sDestinationFileName, ref sPathOnly, ref sFNOnly);
                if (m_enmStorageType == StorageType.S3BucketStorage)
                {
                    IAmazonS3 client;
                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
                    {
                        PutObjectResponse objresponse=null;
                        try
                        {
                            //Check S3 Bucket Exists or not
                            S3Bucket objS3bk = new S3Bucket();
                            objS3bk.BucketName = m_sDestinationStoragePath;
                            ListBucketsResponse response = client.ListBuckets();
                            if (response.Buckets.Contains(objS3bk))
                            {
                                PutBucketRequest request = new PutBucketRequest();
                                request.BucketName = m_sDestinationStoragePath;
                                client.PutBucket(request);
                            }
                            // simple object put
                            PutObjectRequest objrequest = new PutObjectRequest()
                            {
                                BucketName = m_sDestinationStoragePath,
                                Key = "Attachments/" + p_sDestinationFileName
                            };
                            objrequest.InputStream = p_objFileContents;
                            objrequest.Headers.ContentLength = long.Parse(p_sFileSize);
                            objresponse = client.PutObject(objrequest);
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                        }
                        bResult = objresponse != null ? true : false;
                    }
                }
                //Store file depending on document store type
				else if (m_enmStorageType == StorageType.FileSystemStorage)
				{
                   bResult =  StoreLargeStream_FileSystem(p_objFileContents, sPathOnly, sFNOnly); 
				}
				else if(m_enmStorageType == StorageType.DatabaseStorage)
				{
					if((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
						throw new InvalidValueException
							(Globalization.GetString("FileStorageManager.StoreFile.DestPathNotSpecified",m_iClientId));

					//Target is Database, So store that file in the DOCUMENT_STORAGE table
					iFileId = this.GetNextUID("DOCUMENT_STORAGE");
					
					sbSql = new StringBuilder();
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT STORAGE_ID FROM DOCUMENT_STORAGE WHERE");
					sbSql.Append(" UPPER(FILENAME) = UPPER('" + sFNOnly.Replace("'","''") + "') AND ");
					sbSql.Append(" DIR_PATH = '" + sPathOnly.Replace("'","''") + "'");
					objReader = DbFactory.GetDbReader(m_sDestinationStoragePath,sbSql.ToString());
					bUpdate = false;
					if(objReader.Read())
						bUpdate = true;
					objReader.Close();
					objReader.Dispose();
					objReader = null;

					objWriter = DbFactory.GetDbWriter(m_sDestinationStoragePath);
					objWriter.Tables.Add("DOCUMENT_STORAGE");
					objWriter.Fields.Add("STORAGE_ID",iFileId);
					objWriter.Fields.Add("FILENAME",sFNOnly);
					objWriter.Fields.Add("DIR_PATH",sPathOnly);
					objWriter.Fields.Add("STORE_TYPE",0);
                    
					if(bUpdate)
						// Update existing record				
						objWriter.Where.Add(" FILENAME='" + sFNOnly.Replace("'","''") + 
												"' AND DIR_PATH = '" + sPathOnly.Replace("'","''") + "'" );
					
					objWriter.Execute();
					objWriter.Reset(true);	
				
                    if(DbFactory.IsOracleDatabase(m_sDestinationStoragePath))
                    {
                        bResult = StoreLargeStream_Oracle(p_objFileContents,iFileId);
                    }
                    else
                    {
                        attachSizeWithoutChunk = RMConfigurationManager.GetNameValueSectionSettings("AtachmentSize",m_sDestinationStoragePath,m_iClientId)["AttachmentSizeWithoutChunk"];

                        if (Convert.ToInt32(p_sFileSize) < Convert.ToInt32(attachSizeWithoutChunk)) //if file size is lesser than value mentioned in web.config
                        {
                            bResult = StoreStreamWithChunking_SqlServer(p_objFileContents, iFileId, Convert.ToInt32(p_sFileSize));  
                        }
                        else
                        {
                            bResult = StoreLargeStream_SqlServer(p_objFileContents, iFileId);  
                        }
                    }
				}
                iReturnValue = 1;
			}
			catch(FileInputOutputException p_objException)
			{
                throw p_objException;				
			}
			catch(InvalidValueException p_objException)
			{
                throw p_objException;				
			}
			catch(RMAppException p_objException)
			{
                throw p_objException;				
			}
			catch(Exception p_objException)
			{
                throw p_objException;
                //throw new RMAppException
                //    (Globalization.GetString("FileStorageManager.StoreFile.Error",m_iClientId),p_objException);
			}
			finally
			{
				sbSql=null;
				objWriter = null;		
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}				
				if(objFileStream != null)
				{
					objFileStream.Close();
					objFileStream = null;
				}
				if(objBinaryWriter != null)
				{
					objBinaryWriter.Close();
					objBinaryWriter = null;
				}			
			}
			return iReturnValue;
        }

        public long GetFileSize(string p_sDestinationFileName)
        {
            string sPathOnly = "";
            string sFNOnly = "";
            long lFileSize = 0;
            FileStream fStream = null;

            try
            {
                if ((p_sDestinationFileName == null) || (p_sDestinationFileName.Trim() == ""))
                    throw new InvalidValueException
                        (Globalization.GetString("FileStorageManager.StoreFile.DestinationFileNotSpecified",m_iClientId));

                
                this.ParseFilePath(p_sDestinationFileName, ref sPathOnly, ref sFNOnly);
                if (m_enmStorageType == StorageType.S3BucketStorage)
                {
                    IAmazonS3 client;
                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
                    {

                        try
                        {
                            GetObjectRequest request = new GetObjectRequest()
                            {
                                BucketName = m_sDestinationStoragePath,
                                Key = "Attachments/" + p_sDestinationFileName
                            };

                            using (GetObjectResponse response = client.GetObject(request))
                            {
                                lFileSize = response.Headers.ContentLength;
                            }
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                        }
                    }
                }
                //Store file depending on document store type
                else if (m_enmStorageType == StorageType.FileSystemStorage)
                {
                    //FileStorage
                    fStream =  File.Open(this.MaterializeFilename(sPathOnly, sFNOnly), FileMode.Open);
                    lFileSize =  fStream.Length;

                }
                else if (m_enmStorageType == StorageType.DatabaseStorage)
                {
                    if ((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
                        throw new InvalidValueException
                            (Globalization.GetString("FileStorageManager.StoreFile.DestPathNotSpecified",m_iClientId));

                    //Target is Database, So store that file in the DOCUMENT_STORAGE table
                    
                    if (DbFactory.IsOracleDatabase(m_sDestinationStoragePath))
                    {
                        //Oracle Storage
                        lFileSize = RetrieveFileSize_Oracle(sFNOnly, sPathOnly);
                    }
                    else
                    {
                        //Sqlserver Storage
                        lFileSize = RetrieveFileSize_SqlServer(sFNOnly, sPathOnly);
                    }
                }
            }
            catch (FileInputOutputException p_objException)
            {
                throw p_objException;
            }
            catch (InvalidValueException p_objException)
            {
                throw p_objException;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw p_objException;
                //throw new RMAppException
                //    (Globalization.GetString("FileStorageManager.StoreFile.Error",m_iClientId),p_objException);
            }
            finally
            {
                if (fStream != null)
                {
                    fStream.Close();
                    fStream = null;
                }
            }
            return lFileSize;
        }

        private bool StoreLargeStream_Oracle(Stream sourceStream,int pi_StorageId)
        {   
            OracleConnection orclCon = null;
            OracleCommand orclCmd = null;
            OracleLob myLob = null;
            OracleTransaction orclTrans = null;
            String strSQL = string.Empty;
            int offset = 0;
            byte[] b_Buffer = null;
            int BUFFER_LENGTH = 32768; // Chunk size.
            BinaryReader b_Reader = null;
            bool bResult = false;
            string connectionString = string.Empty;
            string[] arrConnectionString;
            try
            {
                b_Reader = new System.IO.BinaryReader(sourceStream);
                
                //since driver and DBQ doesnot supported by ADODBFactory , so remvoing them from connection string
                arrConnectionString = m_sDestinationStoragePath.Split(';');
                for (int item = 0; item < arrConnectionString.GetUpperBound(0); item++)
                {
                    if (arrConnectionString[item] != string.Empty)
                    {
                        if ((arrConnectionString[item].IndexOf("Driver=") == -1) && (arrConnectionString[item].IndexOf("DBQ=") == -1))
                        {
                            connectionString += arrConnectionString[item] + ";";
                        }
                    }
                }

                orclCon = (OracleConnection)ADODbFactory.CreateDatabase(connectionString, "System.Data.OracleClient").CreateConnection(); 
                
                using (orclCon)
                {
                    orclCon.Open();
                    orclTrans = orclCon.BeginTransaction();

                    orclCmd = orclCon.CreateCommand();
                    orclCmd.Transaction = orclTrans;

                    strSQL = "DECLARE xx BLOB; BEGIN DBMS_LOB.CREATETEMPORARY(xx, false, 0); :TempBLOB := xx; END;";

                    OracleParameter parmData = new OracleParameter();
                    parmData.Direction = ParameterDirection.Output;
                    parmData.OracleType = OracleType.Blob;
                    parmData.ParameterName = "TempBLOB";
                    orclCmd.Parameters.Add(parmData);

                    orclCmd.CommandText = strSQL;
                    orclCmd.ExecuteNonQuery();

                    myLob = (OracleLob)orclCmd.Parameters[0].Value;
                    b_Buffer = b_Reader.ReadBytes(BUFFER_LENGTH);
                    //buffer = br.ReadBytes((int)fs.Length);

                    //while (buffer.Length > 0)
                    while (b_Buffer.Length > 0)
                    {
                        myLob.Write(b_Buffer, offset, b_Buffer.Length);
                        //offset += buffer.Length;
                        b_Buffer = b_Reader.ReadBytes(BUFFER_LENGTH);
                    }
                    myLob.Write(b_Buffer, offset, b_Buffer.Length);

                    strSQL = "UPDATE DOCUMENT_STORAGE SET DOC_BLOB=:TEXT_Lob WHERE STORAGE_ID=" + pi_StorageId;

                    orclCmd = orclCon.CreateCommand();
                    orclCmd.Transaction = orclTrans;
                    parmData = new OracleParameter();
                    parmData.Direction = ParameterDirection.Input;
                    parmData.OracleType = OracleType.Blob;
                    parmData.ParameterName = "TEXT_Lob";
                    parmData.Value = myLob;
                    orclCmd.Parameters.Add(parmData);

                    orclCmd.CommandText = strSQL;
                    orclCmd.ExecuteNonQuery();

                    orclTrans.Commit();

                    bResult = true;
                    return bResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                b_Reader.Close();
                sourceStream.Close();
                orclCmd.Dispose();
                orclCon.Close();
            }
        }

        private bool StoreLargeStream_SqlServer(Stream sourceStream,int pi_StorageId)
        {
            SqlConnection sqlCon = null;
            SqlCommand sqlCmd = null;
            String strSQL = string.Empty;
            int BUFFER_LENGTH = 12768; // Chunk size.
            BinaryReader br = null;
            Byte[] Buffer = null;
            SqlCommand cmdUploadBinary = null;
            SqlParameter OffsetParam = null;
            SqlParameter BytesParam = null;
            bool bResult = false;
            int Offset = 0;
            string connectionString = string.Empty;
            try
            {
                //removing driver from destination path
                connectionString = m_sDestinationStoragePath.Substring(m_sDestinationStoragePath.IndexOf(';') + 1);
                
                sqlCon = (SqlConnection)ADODbFactory.CreateDatabase(connectionString, "System.Data.SqlClient").CreateConnection(); 
                
                sqlCmd = new SqlCommand("SET NOCOUNT ON;UPDATE DOCUMENT_STORAGE SET DOC_BLOB = 0x0 WHERE STORAGE_ID =" + pi_StorageId, sqlCon);
                sqlCon.Open();
                sqlCmd.ExecuteNonQuery();

                br = new System.IO.BinaryReader(sourceStream);
                Offset = 0;

                Buffer = br.ReadBytes(BUFFER_LENGTH);

                cmdUploadBinary = new SqlCommand();
                cmdUploadBinary.Connection = sqlCon;
                cmdUploadBinary.CommandTimeout = 6000;
                cmdUploadBinary.CommandText = "UPDATE DOCUMENT_STORAGE SET DOC_BLOB.write(@Bytes,@Offset," + Buffer.Length + ") WHERE STORAGE_ID =" + pi_StorageId;

                OffsetParam = cmdUploadBinary.Parameters.Add("@Offset", SqlDbType.Int);
                OffsetParam.Value = Offset;
                BytesParam = cmdUploadBinary.Parameters.Add("@Bytes", SqlDbType.Binary, BUFFER_LENGTH);
                BytesParam.Value = Buffer;

                while (Buffer.Length > 0)
                {
                    cmdUploadBinary.ExecuteNonQuery();
                    Offset += Buffer.Length;
                    OffsetParam.Value = Offset;
                    Buffer = br.ReadBytes(BUFFER_LENGTH);
                    BytesParam.Value = Buffer;
                }
                bResult = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                br.Close();
                sourceStream.Close();
                sqlCmd.Dispose();
                sqlCon.Close();
            }
            return bResult;
        }

        private bool StoreStreamWithChunking_SqlServer(Stream sourceStream, int pi_StorageId,int p_sFileSize)
        {
            SqlConnection sqlCon = null;
            SqlCommand sqlCmd = null;
            String strSQL = string.Empty;
            int BUFFER_LENGTH = 12768; // Chunk size.
            BinaryReader br = null;
            Byte[] Buffer = null;
            int iMaxSizeLimit = 0;
            Byte[] content = null;
            SqlParameter BytesParam = null;
            bool bResult = false;
            int Offset = 0;
            string connectionString = string.Empty;
            string attachSizeWithoutChunk = string.Empty;
            try
            {
                //removing driver from destination path
                connectionString = m_sDestinationStoragePath.Substring(m_sDestinationStoragePath.IndexOf(';') + 1);

                attachSizeWithoutChunk = RMConfigurationManager.GetNameValueSectionSettings("AtachmentSize", connectionString, m_iClientId)["AttachmentSizeWithoutChunk"];
                iMaxSizeLimit = Convert.ToInt32(attachSizeWithoutChunk);

                content = new Byte[p_sFileSize];                

                sqlCon = (SqlConnection)ADODbFactory.CreateDatabase(connectionString, "System.Data.SqlClient").CreateConnection();

                br = new System.IO.BinaryReader(sourceStream);
                Offset = 0;

                Buffer = br.ReadBytes(BUFFER_LENGTH);
                while (Buffer.Length > 0)
                {
                    Buffer.CopyTo(content, Offset);
                    Offset += Buffer.Length;
                    Buffer = br.ReadBytes(BUFFER_LENGTH);
                    if (Offset > iMaxSizeLimit)
                        throw new Exception("File size exceeds limit Configuration file,so need chunking.");
                }

                sqlCmd = new SqlCommand("UPDATE DOCUMENT_STORAGE SET DOC_BLOB = @Bytes WHERE STORAGE_ID =" + pi_StorageId, sqlCon);
                sqlCmd.CommandTimeout = 6000;
                BytesParam = sqlCmd.Parameters.Add("@Bytes", SqlDbType.Binary, Offset);
                BytesParam.Value = content;

                sqlCon.Open();
                sqlCmd.ExecuteNonQuery();
                bResult = true;
            }
            catch (Exception ex)
            {
                throw new RMAppException("Error occurred while storing large file to SQL Server document database.", ex);
            }
            finally
            {
                br.Close();
                sourceStream.Close();
                sqlCmd.Dispose();
                sqlCon.Close();
            }
            return bResult;
        }

        
        private bool StoreLargeStream_FileSystem(Stream sourceStream,string p_sPath,string p_sFileName)
        {
            FileStream targetStream = null;
            BinaryWriter objBinaryWriter = null;
            string tempFilename = string.Empty;
            const int BufferLen = 4096;
            byte[] buffer = new byte[BufferLen];
            int count = 0;
            bool bResult = false;
            try
            {
                try
                {
                    targetStream = new FileStream(this.MaterializeFilename(p_sPath, p_sFileName), FileMode.Create);
                }
                catch(System.IO.DirectoryNotFoundException p_objException)
				{
					throw new FileInputOutputException
						(Globalization.GetString("FileStorageManager.StoreFile.DestinationPathNotExist",m_iClientId),p_objException);
				}
                
                using (targetStream)
                {
                    objBinaryWriter = new BinaryWriter(targetStream);
                    buffer = new byte[BufferLen];
                    count = 0;
                    while ((count = sourceStream.Read(buffer, 0, BufferLen)) > 0)
                    {
                        objBinaryWriter.Write(buffer, 0, count);
                        objBinaryWriter.Flush();
                    }
                }

                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objBinaryWriter.Close();
                targetStream.Close();
                sourceStream.Close();
            }
        }
		public void UpdateFile(MemoryStream p_sSourceFile)
		{
			
            DbWriter objWriter = null;
			try
			{				
				if(p_sSourceFile == null)
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.StoreFile.NullContents",m_iClientId));


				if(m_enmStorageType == StorageType.DatabaseStorage)
				{

					//Target is Database, So store that file in the DOCUMENT_STORAGE table
					objWriter = DbFactory.GetDbWriter(m_sDestinationStoragePath);
					objWriter.Tables.Add("DOCUMENT_STORAGE");

					objWriter.Fields.Add("DOC_BLOB",p_sSourceFile.ToArray());

					objWriter.Where.Add("STORAGE_ID = "+m_iStorageId.ToString()) ;
					
					objWriter.Execute();
					objWriter.Reset(true);					
				}	
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.UpdateFile.Error",m_iClientId),p_objException);
			}
			finally
			{
				
				objWriter = null;		
					
			}
		}
		
		public int RetrieveFile(string p_sFileName,out MemoryStream p_objFileContents)
		{
			int				iReturnValue = 0;
			string			sPathOnly="";
			string			sFNOnly="";
			MemoryStream	objFileStream=null;
			DbReader		objReader =null;			 
			StringBuilder	sbSQL =null;	
			byte[]			arrDocBlob = null;
			string			sFileName="";
			try
			{
				if((p_sFileName == null) || (p_sFileName.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.RetrieveFile.FileNotSpecified",m_iClientId));

				// Strip off the path from the filename passed in
				this.ParseFilePath(p_sFileName, ref sPathOnly, ref sFNOnly);
                if (m_enmStorageType == StorageType.S3BucketStorage)
                {
                    IAmazonS3 client;
                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
                    {
                        try
                        {
                            GetObjectRequest request = new GetObjectRequest()
                            {
                                BucketName = m_sDestinationStoragePath,
                                Key = "Attachments/" + p_sFileName
                            };

                            using (GetObjectResponse response = client.GetObject(request))
                            {
                                p_sFileName = RMConfigurator.TempPath + "\\" + this.MaterializeFilename(sPathOnly, sFNOnly);
                                if (File.Exists(p_sFileName))
                                {
                                    File.Delete(p_sFileName);
                                }
                                response.WriteResponseStreamToFile(p_sFileName);
                                FileStream oFileStream = (new System.IO.FileStream(p_sFileName, System.IO.FileMode.Open, System.IO.FileAccess.Read));
                                objFileStream = StreamToMemory(oFileStream);
                            }
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                            throw new FileInputOutputException(Globalization.GetString("FileStorageManager.RetrieveFile.SourceFileNotExist",m_iClientId));
                        }
                    }
                }
				else if (m_enmStorageType == StorageType.FileSystemStorage)
				{
					sFileName=this.MaterializeFilename(sPathOnly,sFNOnly);
					if(!File.Exists(sFileName))
						throw new FileInputOutputException
							(Globalization.GetString("FileStorageManager.RetrieveFile.SourceFileNotExist",m_iClientId));

					objFileStream = new MemoryStream(this.FileAsByteArray(sFileName));
				}
				else if (m_enmStorageType == StorageType.DatabaseStorage)
				{
					if((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
						throw new InvalidValueException
							(Globalization.GetString("FileStorageManager.RetrieveFile.DestPathNotSpecified",m_iClientId));

					sbSQL = new StringBuilder();
					sbSQL.Remove(0,sbSQL.Length);
					sbSQL.Append("SELECT DOC_BLOB FROM DOCUMENT_STORAGE WHERE");
                    sbSQL.Append(" UPPER(FILENAME) = UPPER('" + sFNOnly.Replace("'", "''") + "') ");//Parijat 19460 --attachements with an apostrophe
                    sbSQL.Append(" AND DIR_PATH = '" + sPathOnly.Replace("'", "''") + "'");//Parijat 19460 --  attachements with an apostrophe
					objReader = DbFactory.GetDbReader(m_sDestinationStoragePath,sbSQL.ToString());
					if(objReader.Read())
					{
						arrDocBlob = objReader.GetAllBytes("DOC_BLOB");
						if (arrDocBlob != null)
							objFileStream = new MemoryStream(arrDocBlob);						
					}
					else
					{
						throw new FileInputOutputException
							(Globalization.GetString("FileStorageManager.RetrieveFile.SourceFileNotExist",m_iClientId));
					}

				}
				p_objFileContents = objFileStream;	
				iReturnValue = 1;
			}			

			catch(InvalidValueException p_objException)
			{
				throw p_objException;
			}
			catch(FileInputOutputException p_objException)
			{
				throw p_objException;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.RetrieveFile.Error",m_iClientId),p_objException);
			}
			finally
			{
				sbSQL=null;
				if(objFileStream != null)
				{
					// BSB 03.24.2006 How can we expect calling code to use the MemoryStream reference
					// if we close it before they get it ?????? 
					// It's their object - let them use and close it...
                    // objFileStream.Close();
					
					objFileStream = null;
				}
				if(objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				
			}
			return iReturnValue;
		}
        private MemoryStream StreamToMemory(FileStream fileStream)
        {
            MemoryStream memoryStream = new MemoryStream();
            memoryStream.SetLength(fileStream.Length);
            fileStream.Read(memoryStream.GetBuffer(), 0, (int)fileStream.Length);
            memoryStream.Flush();
            fileStream.Close();
            memoryStream.Close();
            return memoryStream;
        }
        public int RetrieveLargeFile(string p_sFileName, out Stream p_objFileContents,ref string p_sStreamFilePath)
        {
            int iReturnValue = 0;
            string sPathOnly = "";
            string sFNOnly = "";
            FileStream objFileStream = null;
            DbReader objReader = null;
            string sFileName = "";
            string tempFileDestinationPath = string.Empty;
            string completedFileDestinationPath = string.Empty;
            try
            {
                if ((p_sFileName == null) || (p_sFileName.Trim() == ""))
                    throw new InvalidValueException
                        (Globalization.GetString("FileStorageManager.RetrieveFile.FileNotSpecified",m_iClientId));

                // Strip off the path from the filename passed in
                this.ParseFilePath(p_sFileName, ref sPathOnly, ref sFNOnly);
                if (m_enmStorageType == StorageType.S3BucketStorage)
                {
                    IAmazonS3 client;
                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
                    {

                        try
                        {
                            GetObjectRequest request = new GetObjectRequest()
                            {
                                BucketName = m_sDestinationStoragePath,
                                Key = "Attachments/" + p_sFileName
                            };

                            using (GetObjectResponse response = client.GetObject(request))
                            {
                                sFileName = RMConfigurator.TempPath + "\\" + this.MaterializeFilename(sPathOnly, sFNOnly);
                                if (File.Exists(sFileName))
                                {
                                    File.Delete(sFileName);
                                }
                                response.WriteResponseStreamToFile(sFileName);
                                objFileStream = new System.IO.FileStream(sFileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                                p_sStreamFilePath = sFileName;
                            }
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                            throw new FileInputOutputException(Globalization.GetString("FileStorageManager.RetrieveFile.SourceFileNotExist",m_iClientId));
                        }
                    }
                }
                else if (m_enmStorageType == StorageType.FileSystemStorage)
                {
                    sFileName = this.MaterializeFilename(sPathOnly, sFNOnly);
                    if (!File.Exists(sFileName))
                        throw new FileInputOutputException
                            (Globalization.GetString("FileStorageManager.RetrieveFile.SourceFileNotExist",m_iClientId));

                    objFileStream  = new System.IO.FileStream(sFileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    p_sStreamFilePath = sFileName;
                }
                else if (m_enmStorageType == StorageType.DatabaseStorage)
                {
                    if ((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
                        throw new InvalidValueException
                            (Globalization.GetString("FileStorageManager.RetrieveFile.DestPathNotSpecified",m_iClientId));

                    tempFileDestinationPath = RMConfigurationManager.GetAppSetting("BasePath") + "\\temp\\" + System.Guid.NewGuid().ToString() + "_Temp";

                    if (DbFactory.IsOracleDatabase(m_sDestinationStoragePath))
                    {
                        RetrieveLargeStream_Oracle(sFNOnly, sPathOnly, tempFileDestinationPath);
                    }
                    else
                    {
                        RetrieveLargeStream_SQLServer(sFNOnly, sPathOnly, tempFileDestinationPath);
                    }

                    if (!File.Exists(tempFileDestinationPath))
                        throw new FileInputOutputException("Temp File not generated properly");
                    
                        completedFileDestinationPath = tempFileDestinationPath.Replace("_Temp", "_Completed");
                        File.Move(tempFileDestinationPath,completedFileDestinationPath);  
                        objFileStream  = new System.IO.FileStream(completedFileDestinationPath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                        
                    p_sStreamFilePath = completedFileDestinationPath;
                    
                }
                p_objFileContents = objFileStream;
                iReturnValue = 1;
            }

            catch (InvalidValueException p_objException)
            {
                throw p_objException;
            }
            catch (FileInputOutputException p_objException)
            {
                throw p_objException;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException
                    (Globalization.GetString("FileStorageManager.RetrieveFile.Error",m_iClientId), p_objException);
            }
            finally
            {
                if (objFileStream != null)
                {
                    // BSB 03.24.2006 How can we expect calling code to use the MemoryStream reference
                    // if we close it before they get it ?????? 
                    // It's their object - let them use and close it...
                    // objFileStream.Close();

                    objFileStream.Close();
                    objFileStream = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }

            }
            return iReturnValue;
        }


        private bool RetrieveLargeStream_Oracle(string ps_FNOnly, string ps_PathOnly, string ps_DestinationPath)
        {
            string connectionString = string.Empty;
            string[] arrConnectionString;
            OracleConnection orclCon = null;
            OracleCommand orclCmd = null;
            OracleDataReader orclReader = null;
            OracleLob myLob = null;
            String strSQL = string.Empty;
            byte[] buffer = null;
            int BUFFER_LENGTH = 32768; // Chunk size.
            FileStream objStream = null;
            bool bResult = false;
            StringBuilder sbSQL = null;
            int actualLength = 0;

            try
            {
                //since driver and DBQ doesnot supported by ADODBFactory , so remvoing them from connection string
                arrConnectionString = m_sDestinationStoragePath.Split(';');
                for (int item = 0; item < arrConnectionString.GetUpperBound(0); item++)
                {
                    if (arrConnectionString[item] != string.Empty)
                    {
                        if ((arrConnectionString[item].IndexOf("Driver=") == -1) && (arrConnectionString[item].IndexOf("DBQ=") == -1))
                        {
                            connectionString += arrConnectionString[item] + ";";
                        }
                    }
                }
                
                orclCon = (OracleConnection)ADODbFactory.CreateDatabase(connectionString, "System.Data.OracleClient").CreateConnection(); 
                orclCon.Open();
                orclCmd = orclCon.CreateCommand();

                sbSQL = new StringBuilder();

                sbSQL.Append("SELECT DOC_BLOB FROM DOCUMENT_STORAGE WHERE");
                //Start Changes: bsharma33 MITS 21663
                sbSQL.Append(" UPPER(FILENAME) = UPPER('" + ps_FNOnly.Replace("'", "''") + "') ");
                //End Changes : bsharma33 MITS 21663
                sbSQL.Append(" AND DIR_PATH = '" + ps_PathOnly + "'");

                orclCmd.CommandText = sbSQL.ToString();
                orclReader = orclCmd.ExecuteReader();

                objStream = new System.IO.FileStream(ps_DestinationPath, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                using (orclReader)
                {
                    orclReader.Read();
                    myLob = orclReader.GetOracleLob(0);
                     
                    buffer = new byte[BUFFER_LENGTH];
                    while ((actualLength = myLob.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        objStream.Write(buffer, 0, actualLength);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                orclReader.Close();
                objStream.Close();
                orclCmd.Dispose();
                orclCon.Close();
            }
            return bResult;
        }

        private bool RetrieveLargeStream_SQLServer(string ps_FNOnly,string ps_PathOnly, string ps_DestinationPath)
        {
            FileStream objStream = null;
            Byte[] objBuffer = null;
            SqlConnection objCon = null;
            int PictureCol = 0;  // position of Picture column in DataReader
            int BUFFER_LENGTH = 32768; // chunk size
            string connectionString = string.Empty;
            SqlCommand cmdGetPointer = null;
            SqlParameter LengthOutParam = null;
            SqlParameter StorageIdOutParam = null;
            SqlCommand cmdReadBinary = null;
            SqlParameter SizeParam = null;
            SqlParameter OffsetParam = null;
            SqlDataReader dr = null;
            int Offset = 0; 
            bool bResult = false;
            StringBuilder sbSQL = null;
            try
            {
                connectionString = m_sDestinationStoragePath.Substring(m_sDestinationStoragePath.IndexOf(';') + 1);
                objCon = (SqlConnection)ADODbFactory.CreateDatabase(connectionString, "System.Data.SqlClient").CreateConnection(); 

                sbSQL = new StringBuilder();
                
                sbSQL.Append("SELECT @StoreId=STORAGE_ID,@Length=DataLength(DOC_BLOB) FROM DOCUMENT_STORAGE WHERE");
                //Start Changes: bsharma33 MITS 21663
                sbSQL.Append(" UPPER(FILENAME) = UPPER('" + ps_FNOnly.Replace("'", "''") + "') ");
                //End Changes: bsharma33 MITS 21663
                sbSQL.Append(" AND DIR_PATH = '" + ps_PathOnly + "'");
                
                cmdGetPointer = new SqlCommand(sbSQL.ToString(), objCon);
                LengthOutParam = cmdGetPointer.Parameters.Add("@Length", SqlDbType.Int);
                LengthOutParam.Direction = ParameterDirection.Output;
                StorageIdOutParam = cmdGetPointer.Parameters.Add("@StoreId", SqlDbType.Int);
                StorageIdOutParam.Direction = ParameterDirection.Output;
                objCon.Open();
                cmdGetPointer.ExecuteNonQuery();
                cmdGetPointer = null;
                sbSQL.Remove(0, sbSQL.Length);

                // Set up READTEXT command, parameters, and open BinaryReader.
                objBuffer = new Byte[BUFFER_LENGTH];

                cmdReadBinary = new SqlCommand("SELECT SUBSTRING(DOCUMENT_STORAGE.DOC_BLOB,@Offset,@Size) as Buff FROM DOCUMENT_STORAGE WHERE STORAGE_ID=" + StorageIdOutParam.Value, objCon);
                SizeParam = cmdReadBinary.Parameters.Add("@Size", SqlDbType.Int);
                OffsetParam = cmdReadBinary.Parameters.Add("@Offset", SqlDbType.Int);
                
                objStream = new System.IO.FileStream(ps_DestinationPath, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                OffsetParam.Value = Offset+1;

                // Read buffer full of data and write to the file stream.
                do
                {
                    // Calculate buffer size - may be less than BUFFER_LENGTH for last block.
                    if ((Offset + BUFFER_LENGTH) >= System.Convert.ToInt32(LengthOutParam.Value))
                        SizeParam.Value = System.Convert.ToInt32(LengthOutParam.Value) - Offset;
                    else SizeParam.Value = BUFFER_LENGTH;

                    dr = cmdReadBinary.ExecuteReader(CommandBehavior.SingleResult);
                    dr.Read();
                    dr.GetBytes(PictureCol, 0, objBuffer, 0, System.Convert.ToInt32(SizeParam.Value));
                    dr.Close();
                    objStream.Write(objBuffer, 0, System.Convert.ToInt32(SizeParam.Value));
                    Offset += BUFFER_LENGTH;
                    OffsetParam.Value = Offset + 1;
                } while (Offset < System.Convert.ToInt32(LengthOutParam.Value));
                bResult = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objStream.Close();
                cmdReadBinary = null;
                dr = null;
                objCon.Close();
            }
            return bResult;
        }

        private long RetrieveFileSize_Oracle(string ps_FNOnly, string ps_PathOnly)
        {
            long lFileSize = 0;
            string connectionString = string.Empty;
            string[] arrConnectionString;
            OracleConnection orclCon = null;
            OracleCommand orclCmd = null;
            OracleDataReader orclReader = null;
            StringBuilder sbSQL = null;

            try
            {
                //since driver and DBQ doesnot supported by ADODBFactory , so remvoing them from connection string
                arrConnectionString = m_sDestinationStoragePath.Split(';');
                for (int item = 0; item < arrConnectionString.GetUpperBound(0); item++)
                {
                    if (arrConnectionString[item] != string.Empty)
                    {
                        if ((arrConnectionString[item].IndexOf("Driver=") == -1) && (arrConnectionString[item].IndexOf("DBQ=") == -1))
                        {
                            connectionString += arrConnectionString[item] + ";";
                        }
                    }
                }
                orclCon = (OracleConnection)ADODbFactory.CreateDatabase(connectionString, "System.Data.OracleClient").CreateConnection();
                orclCon.Open();
                orclCmd = orclCon.CreateCommand();

                sbSQL = new StringBuilder();

                //sbSQL.Append("SELECT DOC_BLOB FROM DOCUMENT_STORAGE WHERE");
                //sbSQL.Append(" UPPER(FILENAME) = UPPER('" + ps_FNOnly + "') ");
                //sbSQL.Append(" AND DIR_PATH = '" + ps_PathOnly + "'");

                //orclCmd.CommandText = sbSQL.ToString();
                //orclReader = orclCmd.ExecuteReader();

                //using (orclReader)
                //{
                //    orclReader.Read();
                //    lFileSize = orclReader.GetOracleLob(0).Length;
                //}

                //rsolanki2 : using SQL function to grab the filesize
                sbSQL.Append("SELECT DBMS_LOB.GETLENGTH(DOC_BLOB) FROM DOCUMENT_STORAGE WHERE");
                //Start Changes: bsharma33 MITS 21663
                sbSQL.Append(" UPPER(FILENAME) = UPPER('" + ps_FNOnly.Replace("'", "''") + "') ");
                //End Changes: bsharma33 MITS 21663
                sbSQL.Append(" AND DIR_PATH = '" + ps_PathOnly + "'");

                orclCmd.CommandText = sbSQL.ToString();
                orclReader = orclCmd.ExecuteReader();

                using (orclReader)
                {
                    orclReader.Read();
                    //lFileSize = (long)orclReader.GetValue(0);
                    //Deb,MITS 25373-Commented the above line as orclReader.GetValue(0) cant be casted to long
                    //Its may be due to OracleDataReader as above line works fine for SqlDataReader
                    //C# long is alias to System.Int64 
                    lFileSize = orclReader.GetInt64(0);
                    //Deb,MITS 25373
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                orclReader.Close();
                orclCmd.Dispose();
                orclCon.Close();
            }
            return lFileSize;
        }

        private long RetrieveFileSize_SqlServer(string ps_FNOnly, string ps_PathOnly)
        {
            SqlConnection objCon = null;
            string connectionString = string.Empty;
            SqlCommand cmdGetPointer = null;
            SqlDataReader docReader = null;
            StringBuilder sbSQL = null;
            long lFileSize = 0;

            try
            {
                connectionString = m_sDestinationStoragePath.Substring(m_sDestinationStoragePath.IndexOf(';') + 1);
                objCon = (SqlConnection)ADODbFactory.CreateDatabase(connectionString, "System.Data.SqlClient").CreateConnection();

                sbSQL = new StringBuilder();

                sbSQL.Append("SELECT DataLength(DOC_BLOB) length FROM DOCUMENT_STORAGE WHERE");
                //Start Changes: bsharma33 MITS 21663
                sbSQL.Append(" UPPER(FILENAME) = UPPER('" + ps_FNOnly.Replace("'", "''") + "') ");
                //End Changes: bsharma33 MITS 21663
                sbSQL.Append(" AND DIR_PATH = '" + ps_PathOnly + "'");

                cmdGetPointer = new SqlCommand(sbSQL.ToString(), objCon);
                objCon.Open();
                docReader = cmdGetPointer.ExecuteReader();

                using (docReader)
                {
                    docReader.Read();
                    lFileSize =   (long)docReader.GetValue(0);
                }
                docReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                docReader = null;
                objCon.Close();
            }
            return lFileSize;
        }


		/// Name			: RetrieveFile
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method retrieves a file from the document store 
		///		and stores it in a temporary OS file system file or at the path specified to the method.
		///		Temporary file will be created in temporary directory and 
		///		will have the name specified in the document store - including its original extension.
		/// </summary>
		/// <param name="p_sFileName">
		///		Name of the file to be retrieved from document store (A document storage relative path 
		///		+ filename) 		
		///		Examples are \README.DOC, \SMNET\README.DOC, \RDMS\LOSSENGINE\README.DOC, etc.
		/// </param>
		/// <param name="p_sOutputFileName">
		///		A UNC or DOS path + filename to write the file to
		///	</param>
		/// <param name="p_sRetrievedFile">
		///		Complete UNC or DOS file path (path + filename) of the file requested
		/// </param>
		/// <returns>1 - Success; 0 - Failure</returns>
        public int RetrieveFile(string p_sFileName, string p_sOutputFileName, string p_sUserName, out string p_sRetrievedFile)
		{
			int				iReturnValue = 0;
			string			sFNOnly ="";
			string			sPathOnly ="";
			FileStream		objFileStream = null;
			BinaryWriter	objBinaryWriter = null;
			DbReader		objReader = null;
			StringBuilder	sbSQL=null;
			byte[]			arrDocBlob=null;
			string			sRetFileName="";
			bool			bError = false;
			string			sTemp = "";
			int				iPosition=0;
			int				iRetry=0;
			const	int		NO_OF_RETRY = 25;
            //Add  by kuladeep for MITS:30929 Start
            string sSQL = string.Empty;
            Dictionary<string, string> dictParams = null;
            //Add by kuladeep for MITS:30929 End

			try
			{
				if((p_sFileName == null) || (p_sFileName.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.RetrieveFile.FileNotSpecified",m_iClientId));

				// Strip off the path from the filename passed in
				this.ParseFilePath(p_sFileName, ref sPathOnly, ref sFNOnly);
                if (m_enmStorageType == StorageType.S3BucketStorage)
                {
                    IAmazonS3 client;
                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
                    {

                        try
                        {
                            GetObjectRequest request = new GetObjectRequest()
                            {
                                BucketName = m_sDestinationStoragePath,
                                Key = "Attachments/" + p_sFileName
                            };

                            using (GetObjectResponse response = client.GetObject(request))
                            {
                                if ((p_sOutputFileName == null) || (p_sOutputFileName.Length == 0))
                                    p_sOutputFileName = RMConfigurator.TempPath + "\\" + this.MaterializeFilename(sPathOnly, sFNOnly);
                                if (File.Exists(p_sOutputFileName))
                                {
                                    File.Delete(p_sOutputFileName);
                                }
                                response.WriteResponseStreamToFile(p_sOutputFileName);
                                sRetFileName = p_sOutputFileName;
                            }
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                            throw new FileInputOutputException(Globalization.GetString("FileStorageManager.RetrieveFile.SourceFileNotExist",m_iClientId));
                        }
                    }
                }
				else if (m_enmStorageType == StorageType.FileSystemStorage)
				{
					sRetFileName = this.MaterializeFilename(sPathOnly,sFNOnly);
					if(!File.Exists(sRetFileName))
						throw new FileInputOutputException
							(Globalization.GetString("FileStorageManager.RetrieveFile.SourceFileNotExist",m_iClientId));
				}
				else if (m_enmStorageType == StorageType.DatabaseStorage)
				{
					if((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
						throw new InvalidValueException
							(Globalization.GetString("FileStorageManager.RetrieveFile.DestPathNotSpecified",m_iClientId));

					// Get a temp filename for output if it is not specified
					if ((p_sOutputFileName == null) || (p_sOutputFileName.Length == 0))
						p_sOutputFileName = Path.GetTempPath() + sFNOnly;
                    //Change by kuladeep for MITS:30929 Start--Apostrophe in document filename causes error e-mailing file
                    //sbSQL = new StringBuilder();
                    //sbSQL.Remove(0,sbSQL.Length);
                    //sbSQL.Append("SELECT DOC_BLOB FROM DOCUMENT_STORAGE WHERE");
                    //sbSQL.Append(" UPPER(FILENAME) = UPPER('" + sFNOnly + "') ");
                    //sbSQL.Append(" AND DIR_PATH = '" + sPathOnly + "'");
                    //objReader = DbFactory.GetDbReader(m_sDestinationStoragePath,sbSQL.ToString());
                                        
                    dictParams = new Dictionary<string, string>();
                    sSQL = string.Format("SELECT DOC_BLOB FROM DOCUMENT_STORAGE WHERE UPPER(FILENAME) = {0} AND DIR_PATH = {1}", "~FNAME~", "~DPATH~");//nkaranam2 - JIRA ID: RMA-1096
                    dictParams.Add("FNAME", sFNOnly.ToUpper());
                    dictParams.Add("DPATH", sPathOnly);
                    objReader = DbFactory.ExecuteReader(m_sDestinationStoragePath, sSQL, dictParams);
                    //Change by kuladeep for MITS:30929 End--Apostrophe in document filename causes error e-mailing file
				    if(objReader.Read())
					{
						try
						{						
							objFileStream = new FileStream(p_sOutputFileName,FileMode.Create);
							bError = false;
						}
                        // If the file path,where the data fetched from database 
						// to be copied, does not exist then raise error
						catch(System.IO.DirectoryNotFoundException  p_objException)
						{
							throw new FileInputOutputException
								(Globalization.GetString("FileStorageManager.RetrieveFile.DestinationPathNotExist",m_iClientId),p_objException);
						}	
						// If there are two requests to write the data fetched from the database
						// on the same file at the same time (i.e. collision)
						// then try writing data to another file
						// else write the data to this file
						catch(System.IO.IOException)
						{
							bError = true;							
						}
						// If there are no collisions in the request to write to the file
						if (! bError)
						{
							objBinaryWriter = new BinaryWriter(objFileStream);			

							arrDocBlob = objReader.GetAllBytes("DOC_BLOB");
							if (arrDocBlob != null)						
								objBinaryWriter.Write(arrDocBlob);					
							sRetFileName= p_sOutputFileName;							
							objBinaryWriter.Close();
							objFileStream.Close();
						}
						// if there are collisions while writing data to the same file
						else
						{
							// Try to write the data fetched from database onto a new file.							
							// This process is repeated NO_OF_RETRY times.
							while((iRetry < NO_OF_RETRY) && (bError))
							{
								++iRetry;
								sTemp = p_sOutputFileName;
								iPosition = p_sOutputFileName.LastIndexOf(".");
								if(iPosition > -1) 									
									sTemp = p_sOutputFileName.Substring(0,iPosition)  
										+ "Retry" + p_sOutputFileName.Substring(iPosition);
								try
								{
									bError = false;
									objFileStream = new FileStream(sTemp,FileMode.Create);									
								}
								catch(System.IO.IOException)
								{
									bError = true;							
								}
							}
							// Collision occurs even after defined number of re-tries (NO_OF_RETRY)
							// then raise error.
							if (bError)
							{
								throw new FileInputOutputException
									(Globalization.GetString("FileStorageManager.RetrieveFile.CouldNotSave",m_iClientId));
							}
							// A file has been sought successfully.
							// Writing data to the file.
							else
							{
								objBinaryWriter = new BinaryWriter(objFileStream);			

								arrDocBlob = objReader.GetAllBytes("DOC_BLOB");
								if (arrDocBlob != null)						
									objBinaryWriter.Write(arrDocBlob);					
								sRetFileName= sTemp;							
								objBinaryWriter.Close();
								objFileStream.Close();
							}
						}
					}
					// Requested file does not exist in the Document Storage
					// then raise error.
					else
						throw new FileInputOutputException
							(Globalization.GetString("FileStorageManager.RetrieveFile.SourceFileNotExist",m_iClientId));

					objReader.Close();					
				}				
				p_sRetrievedFile = sRetFileName;
				iReturnValue = 1;
			}
			catch(FileInputOutputException p_objException)
			{
				throw p_objException;				
			}
			catch(InvalidValueException p_objException)
			{
				throw p_objException;				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.RetrieveFile.Error",m_iClientId),p_objException);
			}
			finally
			{
				sbSQL =null;
				if(objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objFileStream != null)
				{
					objFileStream.Close();
					objFileStream = null;
				}
				if(objBinaryWriter != null)
				{
					objBinaryWriter.Close();
					objBinaryWriter = null;
				}				
			}
			return iReturnValue;
		}
		/// Name			: CopyFile
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method copies a file from one place in the document storage to another.
		///		It will overwrite any existing file with same name and path in the destination store.
		/// </summary>
		/// <param name="p_sSourceFileName">
		///		File in document storage that which needs to be copied 
		///		(document storage relative path + filename).
		/// </param>
		/// <param name="p_sDestinationFileName">
		///		Name of the file to which contents of the source file are copied
		/// </param>
		/// <returns>1 - Success; 0 - Failure</returns>
		public int CopyFile(string p_sSourceFileName,string p_sDestinationFileName)
		{
			string			sPathOnly="";
			string			sFNOnly ="";
			int				iFileId=0;
			int				iReturnValue = 0;
			DbReader		objReader=null;
			string			sSourceFile="";
			string			sDestFile="";
			StringBuilder	sbSql=null;
			DbWriter		objWriter=null;
			try
			{
				if((p_sSourceFileName == null) || (p_sSourceFileName.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.CopyFile.SourceFileNotSpecified",m_iClientId));

				if((p_sDestinationFileName == null) || (p_sDestinationFileName.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.CopyFile.DestinationFileNotSpecified",m_iClientId));


                this.ParseFilePath(p_sSourceFileName, ref sPathOnly, ref sFNOnly);

                if (m_enmStorageType == StorageType.S3BucketStorage)
                {
                    IAmazonS3 client;
                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
                    {

                        try
                        {
                            S3FileInfo objFileInfo = new S3FileInfo(client, m_sDestinationStoragePath, "Attachments/"+p_sSourceFileName);
                              objFileInfo.CopyTo(m_sDestinationStoragePath, "Attachments/"+p_sDestinationFileName, true);
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                            throw new FileInputOutputException(Globalization.GetString("FileStorageManager.CopyFile.SourceFileNotFound", m_iClientId));
                        }
                    }
                }
				//Store file depending on document store type
				else if (m_enmStorageType == StorageType.FileSystemStorage)
				{
					sSourceFile = this.MaterializeFilename(sPathOnly, sFNOnly);
					// Check if file exists or not
					if (! File.Exists(sSourceFile))
					{
						throw new FileInputOutputException
							(Globalization.GetString("FileStorageManager.CopyFile.SourceFileNotFound",m_iClientId));
					}
					sPathOnly ="";
					sFNOnly	="";
					this.ParseFilePath(p_sDestinationFileName, ref sPathOnly, ref sFNOnly);
					sDestFile=this.MaterializeFilename(sPathOnly,sFNOnly );
					try
					{
						//File Server , so Copy the file in Destination Folder					
						File.Copy(sSourceFile,sDestFile ,true);
					}
					catch(System.IO.DirectoryNotFoundException  p_objException)
					{
						throw new FileInputOutputException
							(Globalization.GetString("FileStorageManager.CopyFile.DestinationPathNotExist",m_iClientId),p_objException);
					}
				}
				else if (m_enmStorageType == StorageType.DatabaseStorage)
				{
					if((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
						throw new InvalidValueException
							(Globalization.GetString("FileStorageManager.CopyFile.DestPathNotSpecified",m_iClientId));

					iFileId = this.GetNextUID("DOCUMENT_STORAGE");
					sbSql = new StringBuilder();
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT STORAGE_ID, FILENAME, DIR_PATH, STORE_TYPE, DOC_BLOB ");
					sbSql.Append(" FROM DOCUMENT_STORAGE WHERE UPPER(FILENAME) = UPPER('" 
									+ sFNOnly.Replace("'","''") + "')") ;
					sbSql.Append(" AND DIR_PATH = '" + sPathOnly.Replace("'","''") + "'");

					objReader = DbFactory.GetDbReader(m_sDestinationStoragePath,sbSql.ToString());
					
					if(objReader.Read())
					{
						objWriter = DbFactory.GetDbWriter(m_sDestinationStoragePath);
						objWriter.Tables.Add("DOCUMENT_STORAGE");
						objWriter.Fields.Add("STORAGE_ID",iFileId);

                        //MGaba2:MITS 23998: Tranfer Functionality is not working in case of DB storage
						//objWriter.Fields.Add("FILENAME",p_sDestinationFileName);
                        this.ParseFilePath(p_sDestinationFileName, ref sPathOnly, ref sFNOnly);
                        objWriter.Fields.Add("FILENAME", sFNOnly);

						objWriter.Fields.Add("DIR_PATH",objReader.GetString("DIR_PATH"));
						objWriter.Fields.Add("STORE_TYPE",Conversion.ConvertObjToInt(objReader.GetValue("STORE_TYPE"), m_iClientId));						
						if(objReader.GetAllBytes("DOC_BLOB") != null)
							objWriter.Fields.Add("DOC_BLOB",objReader.GetAllBytes("DOC_BLOB"));

						objWriter.Execute();
						objWriter.Reset(true);	
					}
					else
					{
						throw new FileInputOutputException
							(Globalization.GetString("FileStorageManager.CopyFile.SourceFileNotFound",m_iClientId));
					}
					objReader.Close();
				}
				iReturnValue = 1;
			}	
			catch(InvalidValueException p_objException)
			{
				throw p_objException;				
			}
			catch(FileInputOutputException p_objException)
			{
				throw p_objException;				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.CopyFile.Error",m_iClientId),p_objException);
			}
			finally
			{
				sbSql = null;
				if(objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objWriter = null;
			}
			return iReturnValue;
		}

		/// Name			: DeleteFile
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method deletes a file from the document storage.
		/// </summary>
		/// <param name="p_sFileName">
		///		Name of the file to be deleted (A document storage relative path + filename) 		
		///		Examples are \README.DOC, \SMNET\README.DOC, \RDMS\LOSSENGINE\README.DOC, etc.
		/// </param>
		/// <returns>1 - Success; 0 - Failure</returns>
		public int DeleteFile(string p_sFileName)
		{
			int				iReturnValue=0;
			string			sPathOnly="";
			string			sFNOnly="";
			string			sDeleteFileName="";
			StringBuilder	sbSql =null;
			DbConnection	objConn=null;
			int				iRowsAffected=0;
			try
			{
				if((p_sFileName == null) || (p_sFileName.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.DeleteFile.FileNotSpecified",m_iClientId));

				this.ParseFilePath(p_sFileName, ref sPathOnly, ref sFNOnly);
                if (m_enmStorageType == StorageType.S3BucketStorage)
                {
                    IAmazonS3 client;
                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
                    {
                        try
                        {
                            DeleteObjectRequest request = new DeleteObjectRequest()
                            {
                                BucketName = m_sDestinationStoragePath,
                                Key = "Attachments/"+p_sFileName
                            };

                            DeleteObjectResponse response = client.DeleteObject(request);
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                        }
                    }
                }
                else if (m_enmStorageType == StorageType.FileSystemStorage)
                {
                    sDeleteFileName = this.MaterializeFilename(sPathOnly, sFNOnly);
                    if (!File.Exists(sDeleteFileName))
                    {
                        //dont throw exception , come out silently MITS 15298
                        //throw new FileInputOutputException
                        //(Globalization.GetString("FileStorageManager.DeleteFile.FileNotFound",m_iClientId));
                        iReturnValue = 1;
                        return iReturnValue;
                    }

                    File.SetAttributes(sDeleteFileName, FileAttributes.Normal);
                    File.Delete(sDeleteFileName);
                }
                else if (m_enmStorageType == StorageType.DatabaseStorage)
                {
                    if ((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
                        throw new InvalidValueException
                            (Globalization.GetString("FileStorageManager.DeleteFile.DestPathNotSpecified",m_iClientId));

                    sbSql = new StringBuilder();
                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("DELETE FROM DOCUMENT_STORAGE WHERE");
                    sbSql.Append(" UPPER(FILENAME) = UPPER('" + sFNOnly.Replace("'", "''") + "')");
                    sbSql.Append(" AND DIR_PATH = '" + sPathOnly.Replace("'", "''") + "'");

                    objConn = DbFactory.GetDbConnection(m_sDestinationStoragePath);
                    objConn.Open();
                    iRowsAffected = objConn.ExecuteNonQuery(sbSql.ToString());
                    if (iRowsAffected == 0)
                        throw new FileInputOutputException
                            (Globalization.GetString("FileStorageManager.DeleteFile.FileNotFound",m_iClientId));
                    objConn.Close();
                }
				iReturnValue = 1;
			}
			catch(FileInputOutputException p_objException)
			{
				throw p_objException;				
			}
			catch(InvalidValueException p_objException)
			{
				throw p_objException;				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.DeleteFile.Error",m_iClientId),p_objException);
			}
			finally
			{				
				sbSql = null;
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}

			}
			return iReturnValue;
		}
		/// Name			: FileExists
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method checks if specified file exists in document storage.
		/// </summary>
		/// <param name="p_sFileName">
		///		Name of the file whose existence needs to be checked (A document storage relative path 
		///		+ filename) 		
		///		Examples are \README.DOC, \SMNET\README.DOC, \RDMS\LOSSENGINE\README.DOC, etc.
		/// </param>
		/// <param name="p_bFileExist">
		///		True - If file exists; False - If file does not exist
		/// </param>
		/// <returns>1 - Success; 0 - Failure</returns>
		public int FileExists(string p_sFileName,out bool p_bFileExist)
		{
			int				iReturnValue=0;
			bool			bFileExist=false;
			string			sPathOnly="";
			string			sFNOnly="";
			StringBuilder	sbSql =null;
			DbReader		objReader=null;
			try
			{
				if((p_sFileName == null) || (p_sFileName.Trim() == ""))
				{
					bFileExist= false;					
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.FileExists.FileNotSpecified",m_iClientId));
				}
				else
				{
					this.ParseFilePath(p_sFileName, ref sPathOnly, ref sFNOnly);
					if(sFNOnly.Trim() == "")
						throw new InvalidValueException
							(Globalization.GetString("FileStorageManager.FileExists.FileNotSpecified",m_iClientId));

					if (m_enmStorageType == StorageType.FileSystemStorage)
					{
						
						if (File.Exists(this.MaterializeFilename(sPathOnly,sFNOnly)))						
							bFileExist= true;
						else
							bFileExist= false; 
					}
					else if (m_enmStorageType == StorageType.DatabaseStorage)
					{
						if((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
							throw new InvalidValueException
								(Globalization.GetString("FileStorageManager.FileExists.DestPathNotSpecified",m_iClientId));

						sbSql = new StringBuilder();
						sbSql.Remove(0,sbSql.Length);
						sbSql.Append("SELECT FILENAME FROM DOCUMENT_STORAGE WHERE ");
						sbSql.Append(" UPPER(FILENAME) = UPPER('" + sFNOnly.Replace("'","''") + "') ");
						sbSql.Append(" AND DIR_PATH = '" + sPathOnly.Replace("'","''") + "'");   

						objReader = DbFactory.GetDbReader(m_sDestinationStoragePath, sbSql.ToString());
						if (objReader.Read())
							bFileExist= true;
						else
							bFileExist= false; 
						objReader.Close();
					}
                    else if (StorageType.S3BucketStorage == m_enmStorageType)
                    {
                        IAmazonS3 client;
                        using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
                        {

                            try
                            {
                                Amazon.S3.IO.S3FileInfo objFileInfo = new Amazon.S3.IO.S3FileInfo(client, m_sDestinationStoragePath, "Attachments/"+p_sFileName);
                                if (objFileInfo.Exists)
                                {
                                    bFileExist = true;
                                }

                            }
                            catch (AmazonS3Exception amazonS3Exception)
                            {
                            }
                        }
                    }
				}				
				p_bFileExist = bFileExist;
				iReturnValue = 1;
			}
				
			catch(InvalidValueException p_objException)
			{
				throw p_objException;				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.FileExists.Error",m_iClientId),p_objException);
			}
			finally
			{
				sbSql =null;				
				if(objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return iReturnValue;
		}
		/// Name			: GetSafeFileName
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method returns a unique file name based on template file name passed as parameter.
		/// </summary>
		/// <param name="p_sFileName">
		///		Template file name (document storage relative path + filename)
		///		Examples are \README.DOC, \SMNET\README.DOC, \RDMS\LOSSENGINE\README.DOC, etc.
		/// </param>
		/// <param name="p_sSafeFileName">
		///		Unique file name (document storage relative path + filename)
		///		it will either be original filename if no conflict or the original filename 
		///		with a sequence number (starting at 1) appended to it before the extension. 
		///		Examples are \SMNET\README1.DOC, \SMNET\README2.DOC, etc.
		/// </param>
		/// <returns>1 - Success; 0 - Failure</returns>

		public int GetSafeFileName (string p_sFileName, out string p_sSafeFileName)
		{
			int			  iReturnValue = 0;
			string		  sSafeFileName ="";
			string		  sPathOnly="";
			string		  sFNOnly="";
			int		      iPos=0;
			string		  sExtension="";			
			const string  DEF_FILENAME ="RMDoc";
			long		  lCounter=0;
			string		  sNewFileName="";
			bool		  bUniqueFile=true;			
			try
			{
				if((p_sFileName == null) || (p_sFileName.Trim() == ""))
				{
					sSafeFileName = "";
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.GetSafeFileName.FileNotSpecified",m_iClientId));					
				}
				else
				{
                    if (StorageType.S3BucketStorage == m_enmStorageType)
                    {
                        IAmazonS3 client;
                        using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
                        {

                            try
                            {
                                Amazon.S3.IO.S3FileInfo objFileInfo = new Amazon.S3.IO.S3FileInfo(client, m_sDestinationStoragePath, "Attachments/"+p_sFileName);
                                if (!objFileInfo.Exists)
                                {
                                    bUniqueFile = false;
                                    // image exists
                                }
                            }
                            catch (AmazonS3Exception amazonS3Exception)
                            {
                            }
                        }
                        if (!bUniqueFile)
                            sSafeFileName = p_sFileName;
                        else
                        {
                            this.ParseFilePath(p_sFileName, ref sPathOnly, ref sFNOnly);
                            iPos = sFNOnly.IndexOf(".");
                            if (iPos > -1)
                            {
                                sExtension = sFNOnly.Substring(iPos + 1);
                                sFNOnly = sFNOnly.Substring(0, iPos);
                            }
                            if (sFNOnly == "")
                                sFNOnly = DEF_FILENAME;
                            sNewFileName = sFNOnly + Conversion.ToDbDateTime(DateTime.Now) + ((sExtension == "") ? "" : "." + sExtension);
                            if (sPathOnly.EndsWith(@"\"))
                                sSafeFileName = sPathOnly + sNewFileName;
                            else
                                sSafeFileName = sPathOnly + @"\" + sNewFileName;
                        }
                    }
                    else
                    {
                        this.FileExists(p_sFileName, out bUniqueFile);
                        if (!bUniqueFile)
                            sSafeFileName = p_sFileName;
                        else
                        {
                            this.ParseFilePath(p_sFileName, ref sPathOnly, ref sFNOnly);
                            iPos = sFNOnly.IndexOf(".");
                            if (iPos > -1)
                            {
                                sExtension = sFNOnly.Substring(iPos + 1);
                                sFNOnly = sFNOnly.Substring(0, iPos);
                            }
                            if (sFNOnly == "")
                                sFNOnly = DEF_FILENAME;
                            //Mjain8 MITS 20018 changed the logic to remain out of big loop where same kind of name always used like ackletters funtionality
                            sNewFileName = sFNOnly + Conversion.ToDbDateTime(DateTime.Now) + ((sExtension == "") ? "" : "." + sExtension);
                            //do
                            //{
                            //    ++lCounter;
                            //    sNewFileName = sFNOnly + lCounter.ToString() + 
                            //        ((sExtension == "") ? "" : "." + sExtension); 
                            //    if (sPathOnly.EndsWith(@"\"))
                            //        this.FileExists(sPathOnly + sNewFileName,out bUniqueFile);	
                            //    else
                            //        this.FileExists(sPathOnly + @"\" + sNewFileName,out bUniqueFile);	
                            //}
                            //while (bUniqueFile);
                            if (sPathOnly.EndsWith(@"\"))
                                sSafeFileName = sPathOnly + sNewFileName;
                            else
                                sSafeFileName = sPathOnly + @"\" + sNewFileName;
                        }
                    }	
				}
				p_sSafeFileName = sSafeFileName;
				iReturnValue = 1;
			}
			catch(InvalidValueException p_objException)
			{
				throw p_objException;				
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.GetSafeFileName.Error",m_iClientId),p_objException);
			}			
			return iReturnValue;

		}
		/// Name			: GetUniqueFileName
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method generates a statistically unique file name.
		/// </summary>
		/// <param name="p_sFilePath">
		///		A document storage relative path, 
		///		If blank then root path (\) will be assumed.		
		///		Examples are \, \SMNET, \RDMS\LOSSENGINE, etc.
		/// </param>
		/// <param name="p_sFileExtension">
		///		File extension for which unique file name has to be generated.
		/// </param>
		/// <param name="p_sUniqueFileName">
		///		Unique file name generated with the specified extension.
		/// </param>
		/// <returns>1 - Success; 0 - Failure</returns>
		public int GetUniqueFileName(string p_sFilePath,string p_sFileExtension,
															out string p_sUniqueFileName)
		{
			int		iReturnValue = 0;
			string	sFileName="";
			bool	bUniqueFile=false;
			int		iIteration=0;
			int		iCounter=0;
			string	sTempFileName="";
			Random	objRandom = null;
			int		iRandomNumber=0;
			bool	bFileExist=false;
			long	lTempRandom=0;
			const	int	NO_CHARS_IN_FILENAME = 12;	
			const	int	NO_OF_ITERATION = 50000;	

			try
			{
				if((p_sFileExtension == null) || (p_sFileExtension.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.GetUniqueFileName.FileExtNotSpecified",m_iClientId));					

				if((p_sFilePath == null) || (p_sFilePath.Trim() == ""))
					p_sFilePath = @"\";
				
				objRandom = new Random( );				
				if(!p_sFilePath.EndsWith(@"\"))
					p_sFilePath += @"\";				
				do
				{
					sTempFileName ="";
					for(iCounter = 0;iCounter < NO_CHARS_IN_FILENAME ;++iCounter)
					{
						iRandomNumber = objRandom.Next();							
						lTempRandom = (long) iRandomNumber * 10;
						sTempFileName += lTempRandom.ToString().Substring(0,1) ;						
					} 
					sFileName =p_sFilePath + sTempFileName.Trim() + "." + p_sFileExtension;
					//Test if the file name is unique
					this.FileExists(sFileName,out bFileExist);
					if(! bFileExist)
						bUniqueFile = true;
					++iIteration;
				}
				while((!bUniqueFile) && (iIteration < NO_OF_ITERATION));
				// If unique name is found
				if(bUniqueFile)
					p_sUniqueFileName=sFileName;				
				else
					p_sUniqueFileName="";
					iReturnValue = 1;
			}
			catch(InvalidValueException p_objException)
			{
				throw p_objException;				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.GetUniqueFileName.Error",m_iClientId),p_objException);
			}	
			finally
			{
				objRandom=null;
			}
			return iReturnValue;
		}
		
		#endregion
        
		#region Private Methods

		/// Name			: ParseFilePath
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will parse a file path (filename with complete path information) into 
		///		filename and path. Returned filename will consist of the filename + extension. 		
		/// </summary>		
		/// <param name="p_sFilePath">
		///		A document storage relative path + filename, 
		///		Ex- \README.DOC, \SMNET\README.DOC, \RDMS\LOSSENGINE\README.DOC, etc.
		///	</param>
		/// <param name="p_sPath">
		///		A document storage relative path, it will always include a leading \.
		///		It will never include a trailing \ unless it is the root path (\)
		///		Root path is represented by \.
		///		Root path will be returned if no path is specified.
		/// </param>
		/// <param name="p_sFileName">
		///		The raw file name with no path information attached.
		/// </param>		
		
		private void ParseFilePath(string p_sFilePath,ref string p_sPath,ref string p_sFileName)
		{
			int iPosition=0;
			try
			{
				iPosition = p_sFilePath.LastIndexOf(@"\");
				if(iPosition == -1)
				{
					p_sFileName = p_sFilePath;
					p_sPath=@"\";
				}
				else
				{
					p_sFileName=p_sFilePath.Substring(iPosition + 1);
					p_sPath=p_sFilePath.Substring(0,iPosition);
					
					// Make sure path has \ in front of it. No relative pathing allowed.
					if (! p_sPath.StartsWith(@"\"))
						p_sPath = @"\" + p_sPath;
					// Strip off trailing \
					if(iPosition != 0)
						if(p_sPath.EndsWith(@"\"))						
							p_sPath = p_sPath.Substring(0,p_sPath.Length -1);
				}
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.ParseFilePath.Error",m_iClientId),p_objException);
			}
		}
		/// Name			: MaterializeFilename
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method takes in path and filename. It constructs the actual UNC filename 
		///		based on the document path. The path passed in is always relative to the document 
		///		path set in Security Management System. This function is used only in case 
		///		the data storage is File System.
		/// </summary>
		/// <param name="p_sPath">
		///		This is a fully qualified path within the document system.
		///		This is not a UNC or DOS path. It is implicitly relative to the document system root.
		///		Examples are \, \SMNET, \RMDOCS, etc.
		///		This do not include a trailing \ on the path but do include a leading \.
		///		Use "\" for document to be stored in the root folder 
		///		(this is how all RISKMASTER doc sys documents are stored currently)
		/// </param>
		/// <param name="p_sFileName">
		///		Filename + extension
		/// </param>
		/// <returns>
		///		Returns a full UNC or DOS path for accessing the file using file system functions.
		///		All paths start with the RISKMASTER document directory and 
		///		then contain the relative document system path and then the filename.
		/// </returns>
		private string MaterializeFilename(string p_sPath,string p_sFileName)
		{
			string sRetFileName ="";
			string sDestPath="";
			try
			{
				if((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.MaterializeFilename.DestPathNotSpecified",m_iClientId));

				//First, get document path
				sDestPath = m_sDestinationStoragePath;
				if (sDestPath.EndsWith(@"\"))					
					sDestPath = sDestPath.Substring(0,sDestPath.Length -1);

				if(! p_sPath.StartsWith(@"\"))
					p_sPath = @"\" + p_sPath;

				sDestPath = sDestPath + p_sPath;
				if (!sDestPath.EndsWith(@"\"))
					sDestPath = sDestPath + @"\";

				sDestPath = sDestPath + p_sFileName;
				
				sRetFileName = sDestPath;

				return sRetFileName;				
			}
			catch(InvalidValueException p_objException )
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.MaterializeFilename.Error",m_iClientId),p_objException);
			}
			
		}

		/// Name			: GetNextUID
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method retrieves and increments the current unique ids counter for 
		///		Document Storage table. 		
		/// </summary>
		/// <param name="p_sTableName">
		///		Document Storage table name
		/// </param>
		/// <returns>Unique Storage Id</returns>
		private int GetNextUID(string p_sTableName)
		{
			StringBuilder	sbSql=null;
			int				iNextUID=0;
			int				iOrigUID=0;
			int				iRows=0;
			int				iCollisionRetryCount=0;
			int				iErrRetryCount=0;
			DbConnection	objConn = null;
			DbReader		objReader = null;
			const	int		COLLISION_RETRY_COUNT=1000;
			const	int		ERROR_RETRY_COUNT=5;
			try
			{
				if((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("FileStorageManager.GetNextUID.DestPathNotSpecified",m_iClientId));

				objConn = DbFactory.GetDbConnection(m_sDestinationStoragePath);				
				sbSql = new StringBuilder();
				do
				{
					objConn.Open();						
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT NEXT_UNIQUE_ID FROM DOCSTORE_IDS WHERE ");
					sbSql.Append(" SYSTEM_TABLE_NAME = '" + p_sTableName + "'");
					//Get Current Id.
					objReader = objConn.ExecuteReader(sbSql.ToString());							
					if(!objReader.Read())
					{	
						objReader.Close();
						objReader = null;
						objConn.Close();
						throw new RMAppException
							(Globalization.GetString("FileStorageManager.GetNextUID.NoSuchTable",m_iClientId));						
					}

                    iNextUID = Conversion.ConvertObjToInt(objReader.GetValue("NEXT_UNIQUE_ID"), m_iClientId);
					objReader.Close();
					objReader = null;
	            
					//Compute next id
					iOrigUID = iNextUID;
					if(iOrigUID !=0)
						iNextUID++;
					else 
						iNextUID = 2;
	        
					//try to reserve id (searched update)
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("UPDATE DOCSTORE_IDS SET NEXT_UNIQUE_ID = " + iNextUID);
					sbSql.Append(" WHERE SYSTEM_TABLE_NAME = '" + p_sTableName + "'");
						  
					// only add searched clause if no chance of a null originally in row 
					//(only if no records ever saved against table)   
					if(iOrigUID !=0)
						sbSql.Append(" AND NEXT_UNIQUE_ID = " + iOrigUID);
	                
					// Try update
					try{iRows = objConn.ExecuteNonQuery(sbSql.ToString());}
					catch(Exception p_objException)
					{
						iErrRetryCount++;
						if (iErrRetryCount >= ERROR_RETRY_COUNT)
						{
							objConn.Close();
							throw new RMAppException
								(Globalization.GetString("FileStorageManager.GetNextUID.ErrorModifyingDB",m_iClientId),p_objException);
						}
					}
					// if success, return
					if( iRows == 1)
					{
						objConn.Close();
						return iNextUID - 1;
					}
					else
						// collided with another user - try again (up to 1000 times)
						iCollisionRetryCount++;   
				
				}while ((iErrRetryCount < ERROR_RETRY_COUNT) && (iCollisionRetryCount < COLLISION_RETRY_COUNT));
	     
				if(iCollisionRetryCount >= COLLISION_RETRY_COUNT)
				{
					objConn.Close();
					throw new RMAppException
						(Globalization.GetString("FileStorageManager.GetNextUID.CollisionTimeout",m_iClientId));
				}
				objConn.Close();
				//shouldn't get here under normal conditions.
				return 0;  
			}
			catch(InvalidValueException p_objException)
			{
				throw p_objException;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.GetNextUID.Error",m_iClientId),p_objException);
			}
			finally
			{
				sbSql=null;
				if(objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State== System.Data.ConnectionState.Open)
					{
						objConn.Close();
						objConn.Dispose();
					}
				}
			}
		}
		/// Name			: FileAsByteArray
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method would convert the contents of the file to the binary stream. 
		/// </summary>
		/// <param name="p_sFileName">
		///		File Name
		/// </param>
		/// <returns>Binary stream containing file contents</returns>
		private byte[] FileAsByteArray(string p_sFileName)
		{
			byte[]			arrRet  = null;
			FileStream		objFStream=null;
			BinaryReader	objBReader=null;
			try
			{
				objFStream = new FileStream(p_sFileName,FileMode.Open);
				objBReader = new BinaryReader(objFStream);
				arrRet = objBReader.ReadBytes((int)objFStream.Length);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("FileStorageManager.FileAsByteArray.Error",m_iClientId),p_objException);						
			}
			finally
			{
				if( objFStream != null) 
				{
					objFStream.Close();
					objFStream = null;
				}
				if( objBReader != null) 
				{
					objBReader.Close();
					objBReader = null;					
				}
			}			
			return arrRet;
		}	

		#endregion
	}

	#region Enumeration

	/// <summary>
	/// File Storage Type
	/// </summary>
	public enum StorageType:int
	{
		FileSystemStorage = 0,
		DatabaseStorage = 1,
        S3BucketStorage = 2
	} 
	#endregion
}
