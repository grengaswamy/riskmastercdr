﻿
using System;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;


namespace Riskmaster.Application.LeaveLibrary
{
    ///************************************************************** 
    ///* $File				: LeaveEmp.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 01-31-2007
    ///* $Author			: Aashish Bhateja
    ///***************************************************************	
    /// <summary>	
    ///	Employee object definition.
    /// </summary>
    public class LeaveEmp
	{
		#region Constants
        /// <summary>
        /// Contains the table name.
        /// </summary>
		private const string m_sTableName = "EMPLOYEE";
        /// <summary>
        /// Constant for plan revoked.
        /// </summary>
		private const string SC_PLAN_REVOKED = "R";
        /// <summary>
        /// Constant for plan in-effect.
        /// </summary>
		private const string SC_PLAN_IN_EFFECT = "I";
        /// <summary>
        /// Constant for expired plan. 
        /// </summary>
		private const string SC_PLAN_EXPIRED = "E";
        /// <summary>
        /// Constant for cancelled plan.
        /// </summary>
		private const string SC_PLAN_CANCELED = "C";
		#endregion
		#region Variables Declaration
        /// <summary>
        /// Holds the connection string.
        /// </summary>
		private string m_sConnectionString="";
		/// <summary>
		/// Date employee is hired.
		/// </summary>
		private string m_sDateHired=""; 
		/// <summary>
		/// Eligibility Date.
		/// </summary>
		private string m_sEligDate=""; 
		/// <summary>
        /// EmployeeEid.
		/// </summary>
		private long m_lEmployeeEid=0; 
		/// <summary>
		/// Department assigned to the employee.
		/// </summary>
		private long m_lDeptAssignedEid=0; 
		/// <summary>
		/// Weekly hours for the employee.
		/// </summary>
		private double m_dWeeklyHoursFloat=0.0;
        /// <summary>
        /// Collection of leave plans.
        /// </summary>
		private LeavePlans m_EmpXPlans = null;

        private int m_iClientId = 0;
		#endregion

		#region Properties Declaration
		/// <summary>
        /// Date employee is hired.
		/// </summary>
		public string DateHired
		{
			get
			{
				return m_sDateHired;
			}
			set
			{
				m_sDateHired=value;
			}
		}
		/// <summary>
		/// Eligibility Date
		/// </summary>
		public string EligDate
		{
			get
			{
				return m_sEligDate;
			}
			set
			{
				m_sEligDate=value;
			}
		}
		/// <summary>
        /// EmployeeEid
		/// </summary>
		public long EmployeeEid
		{
			get
			{
				return m_lEmployeeEid;
			}
			set
			{
				m_lEmployeeEid=value;
			}
		}
		/// <summary>
        /// Department assigned to the employee.
		/// </summary>
		public long DeptAssignedEid
		{
			get
			{
				return m_lDeptAssignedEid;
			}
			set
			{
				m_lDeptAssignedEid=value;
			}
		}
        /// <summary>
        /// Weekly hours for the employee.
        /// </summary>
		public double WeeklyHoursFloat
		{
			get
			{
				return m_dWeeklyHoursFloat;
			}
			set
			{
				m_dWeeklyHoursFloat=value;
			}
		}
        /// <summary>
        /// Leave plans collection.
        /// </summary>
		public LeavePlans EmpXPlans
		{
			get
			{
				return m_EmpXPlans;
			}
		}

		#endregion

        /// <summary>
        /// Creates collection of plans.
        /// </summary>
        /// <param name="p_lEmpID"></param>
        /// <param name="p_sDate"></param>
        /// <returns></returns>
		public bool LoadData(long p_lEmpID,string p_sDate)
		{    
			const string PLAN_STATUS = "PLAN_STATUS";
			int iCN_PLAN_REVOKED = 0;
			int iCN_PLAN_IN_EFFECT = 0;
			int iCN_PLAN_EXPIRED = 0;
			int iCN_PLAN_CANCELLED = 0;

			bool bRetVal=false;
			string sSQL = string.Empty;
			DbConnection objConn = null; 
			DbReader objReader = null;
		    LeavePlan objLeavePlan = new LeavePlan(m_sConnectionString, m_iClientId);
			LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);

			try
			{
				sSQL = "";
                //Shruti
                sSQL = "SELECT EMPLOYEE_EID, DATE_HIRED,DEPT_ASSIGNED_EID,WEEKLY_HOURS FROM " + m_sTableName;
				sSQL = sSQL + " WHERE EMPLOYEE_EID=" + p_lEmpID;
		    
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				using (objReader=objConn.ExecuteReader(sSQL))
                {    				
				    if(objReader.Read())
				    {
                        //Shruti
                        m_lEmployeeEid = Conversion.ConvertObjToInt64(objReader.GetValue("EMPLOYEE_EID"), m_iClientId);
					    m_sDateHired = objReader.GetString("DATE_HIRED");
                        //Shruti
                        m_lDeptAssignedEid = Conversion.ConvertObjToInt64(objReader.GetValue("DEPT_ASSIGNED_EID"), m_iClientId);
					    m_dWeeklyHoursFloat = objReader.GetDouble("WEEKLY_HOURS");
				    }
                }
                
				iCN_PLAN_REVOKED = objLocalCache.GetCodeId(SC_PLAN_REVOKED,PLAN_STATUS);
				iCN_PLAN_IN_EFFECT = objLocalCache.GetCodeId(SC_PLAN_IN_EFFECT,PLAN_STATUS);
				iCN_PLAN_EXPIRED = objLocalCache.GetCodeId(SC_PLAN_EXPIRED,PLAN_STATUS);
				iCN_PLAN_CANCELLED = objLocalCache.GetCodeId(SC_PLAN_CANCELED,PLAN_STATUS);
				
				sSQL = "";
				sSQL = sSQL + "SELECT ";
				sSQL = sSQL + "LEAVE_PLAN.LP_ROW_ID,LEAVE_PLAN_NAME,LEAVE_PLAN_CODE,LEAVE_PLAN_TYPE";
				sSQL = sSQL + ",DEFAULT_FLAG,ALLOWED_NUMBER,ALLOWED_PRD_TYPE,PER_NUMBER";
				sSQL = sSQL + ",PER_PRD_TYPE,HOURS_PER_WEEK,ELIG_BENE_NUMBER,BENE_PRD_TYPE";
				sSQL = sSQL + ",BENE_FIRST_FLAG,ELIG_HOURS";
				sSQL = sSQL + ",ENTITY_EID,LP_STATUS_CODE,EFFECTIVE_DATE,EXPIRATION_DATE,USE_LEAVES_FLAG";
				sSQL = sSQL + " FROM LEAVE_PLAN LEFT JOIN LP_X_ENTITY ON LEAVE_PLAN.LP_ROW_ID = LP_X_ENTITY.LP_ROW_ID";
				sSQL = sSQL + " WHERE ((LP_X_ENTITY.ENTITY_EID IN (" + m_lDeptAssignedEid + ")";
				sSQL = sSQL + " OR DEFAULT_FLAG <> 0))";
				sSQL = sSQL + " AND ((LEAVE_PLAN.LP_STATUS_CODE IN (" + iCN_PLAN_IN_EFFECT + "," + iCN_PLAN_EXPIRED + "))";
				sSQL = sSQL + "     OR (LEAVE_PLAN.LP_STATUS_CODE = " + iCN_PLAN_CANCELLED + " ";
				sSQL = sSQL + "       AND LEAVE_PLAN.CANCEL_DATE > '" + p_sDate + "'))";
				sSQL = sSQL + " AND ((LEAVE_PLAN.EFFECTIVE_DATE <= '" + p_sDate + "'";
				sSQL = sSQL + "       AND LEAVE_PLAN.EXPIRATION_DATE >= '" + p_sDate + "'))";
				sSQL = sSQL + " ORDER BY LEAVE_PLAN_CODE";
				
				using (objReader=objConn.ExecuteReader(sSQL))
                {
				    while(objReader.Read())
				    {
					    objLeavePlan.LPRowId = objReader.GetInt32("LP_ROW_ID");
					    objLeavePlan.LPStatusCode = objReader.GetInt32("LP_STATUS_CODE");
					    objLeavePlan.LPTypeCode = objReader.GetInt32("LEAVE_PLAN_TYPE");
					    objLeavePlan.LeavePlanName = objReader.GetString("LEAVE_PLAN_NAME");
					    objLeavePlan.LeavePlanCode = objReader.GetString("LEAVE_PLAN_CODE");
                        //Shruti
                        objLeavePlan.DefaultFlag = Conversion.ConvertObjToInt(objReader.GetValue("DEFAULT_FLAG"), m_iClientId);
                        objLeavePlan.AllowedNumber = Conversion.ConvertObjToInt(objReader.GetValue("ALLOWED_NUMBER"), m_iClientId);
                        //--
					    objLeavePlan.AllowedPeriodType = objReader.GetInt32("ALLOWED_PRD_TYPE");
                        //Shruti
                        objLeavePlan.PerNumber = Conversion.ConvertObjToInt(objReader.GetValue("PER_NUMBER"), m_iClientId);
					    objLeavePlan.PerPeriodType = objReader.GetInt32("PER_PRD_TYPE");
					    objLeavePlan.HoursPerWeek = objReader.GetDouble("HOURS_PER_WEEK");
                        //Shruti
                        objLeavePlan.EligBeneNumber = Conversion.ConvertObjToInt(objReader.GetValue("ELIG_BENE_NUMBER"), m_iClientId);
					    objLeavePlan.BenePeriodType = objReader.GetInt32("BENE_PRD_TYPE");
                        //Shruti
					    objLeavePlan.BeneFirstFlag = Conversion.ConvertObjToInt(objReader.GetValue("BENE_FIRST_FLAG"), m_iClientId);
					    objLeavePlan.UseLeavesFlag = Conversion.ConvertObjToInt(objReader.GetValue("USE_LEAVES_FLAG"), m_iClientId);
					    objLeavePlan.EligHours = objReader.GetDouble("ELIG_HOURS");
					    objLeavePlan.EffectiveDate = objReader.GetString("EFFECTIVE_DATE");
					    objLeavePlan.ExpirationDate = objReader.GetString("EXPIRATION_DATE");

					    m_EmpXPlans.Add(objLeavePlan.LPRowId, objLeavePlan.LeavePlanName,objLeavePlan.LPTypeCode,objLeavePlan.LeavePlanCode,objLeavePlan.DefaultFlag,objLeavePlan.AllowedNumber,objLeavePlan.AllowedPeriodType,objLeavePlan.PerNumber,objLeavePlan.PerPeriodType,objLeavePlan.HoursPerWeek,objLeavePlan.EligBeneNumber,objLeavePlan.BenePeriodType,objLeavePlan.BeneFirstFlag,objLeavePlan.UseLeavesFlag,objLeavePlan.EligHours,objLeavePlan.EffectiveDate,objLeavePlan.ExpirationDate,objLeavePlan.LPRowId.ToString());
				    }
                }
				
				bRetVal=true;
			}				
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("LeaveEmp.LoadData.Err", m_iClientId), p_objEx);
			}
			finally
			{	
				if(objConn!=null)
                {
                    objConn.Close();
					objConn.Dispose();
                }

                if (objLocalCache != null)
                    objLocalCache.Dispose();
			}
			return bRetVal;

		}

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="p_sConnectionString"></param>
		public LeaveEmp(string p_sConnectionString, int p_iClientId)
		{
			m_sConnectionString=p_sConnectionString;
            m_iClientId = p_iClientId;
            m_EmpXPlans = new LeavePlans(m_sConnectionString, m_iClientId);
		}
	}
}
