using System;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.LeaveLibrary
{
    ///************************************************************** 
    ///* $File				: UpdateLeaveRecords.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 01-31-2007
    ///* $Author			: Aashish Bhateja
    ///***************************************************************	
    /// <summary>	
    ///	Called by user to update the Leave Records. This can be activated through
    /// Utilities (all employees, all plans).
    /// </summary>
    public class UpdateLeaveRecords
	{
        /// <summary>
        /// Holds the connection string.
        /// </summary>
		private string m_sConnectionString="";
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="p_sConnectionString"></param>
        /// 
        private int m_iClientId = 0; //rkaur27
        public UpdateLeaveRecords(string p_sConnectionString, int p_iClientId)//rkaur27
		{
			m_sConnectionString=p_sConnectionString;
            m_iClientId = p_iClientId;//rkaur27
		}
        /// <summary>
        /// Used to update leave records from utilities.
        /// </summary>
        /// <param name="p_iLeavePlanId"></param>
        /// <returns></returns>
		public bool Update(int p_iLeavePlanId)
		{
			bool bRetVal = false;
			string sEligDate = "";
			try
			{
				sEligDate = Conversion.GetDate(DateTime.Now.ToString()); 

				if(!(p_iLeavePlanId > 0))
					p_iLeavePlanId = -1;
			        
				if(bUpdateLeaveRecords(sEligDate,p_iLeavePlanId))
					bRetVal = true;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UpdateLeaveRecords.Update.Err", m_iClientId),p_objEx);
			}
			finally
			{}
			return bRetVal;
		}

        /// <summary>
        /// Updates leave records,called from Update function.
        /// </summary>
        /// <param name="p_sEligDate"></param>
        /// <param name="p_iLeavePlanId"></param>
        /// <returns></returns>
		private bool bUpdateLeaveRecords(string p_sEligDate,int p_iLeavePlanId)
		{
			string sSQL = string.Empty;
			string sSQL_Plans = string.Empty;
			string sSQL_Emp = string.Empty;
			string sSQL_ClaimStatus = string.Empty;
			string sSQL_LeaveStatus = string.Empty;
			int iLVTableID = 0;
			int iClaimStatus = 0;
			int iLeaveStatus = 0;
			bool bLeaveElig = false;
			bool bRetVal = false;
			int iLeavePlan = 0;
			int iStatusCode = 0;
			string sStartDate = string.Empty;
			string sEndDate = string.Empty;
			string sPerPeriod = string.Empty;
			string sPerPeriodDesc = string.Empty;
			int iEmpID = 0;
            int iPiRowId = 0;
			LeaveElig objLeaveElig = new LeaveElig(m_sConnectionString, m_iClientId);
			LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);
			DbConnection objConn = null; 
			DbReader objRdrPlans=null;
			DbReader objRdrStatus=null;
			DbReader objReader=null;

			try
			{
				//-- Pull the table id for the Leave record table
				iLVTableID = objLocalCache.GetTableId("CLAIM_LEAVE");

				//-- Pull the status code type ids for Claim status and Leave status
				iClaimStatus = objLocalCache.GetTableId("CLAIM_STATUS");
				iLeaveStatus = objLocalCache.GetTableId("LEAVE_STATUS");
			
				//-- Pull a list of the Leave Plan Ids
				sSQL_Plans = "SELECT LP_ROW_ID, PER_NUMBER, PER_PRD_TYPE ";
				sSQL_Plans = sSQL_Plans + " FROM LEAVE_PLAN";
				if(p_iLeavePlanId > 0)
					sSQL_Plans = sSQL_Plans + " WHERE LP_ROW_ID = " + p_iLeavePlanId;

//				objConn = DbFactory.GetDbConnection(m_sConnectionString);
//				objConn.Open();
//				objRdrPlans = objConn.ExecuteReader(sSQL_Plans);
				objRdrPlans = DbFactory.GetDbReader(m_sConnectionString,sSQL_Plans);
				while(objRdrPlans.Read())
				{
					iLeavePlan = objRdrPlans.GetInt("LP_ROW_ID");

					//-- Pull the Leave/Claim status codes from the Leave Plan
					sSQL = "SELECT LP_X_UPD_CODES.STATUS_CODE_TYPE, LP_X_UPD_CODES.STATUS_CODE, ";
					sSQL = sSQL + "LEAVE_PLAN.LP_ROW_ID ";
					sSQL = sSQL + "FROM LP_X_UPD_CODES RIGHT JOIN LEAVE_PLAN ON LP_X_UPD_CODES.LP_ROW_ID = LEAVE_PLAN.LP_ROW_ID ";
					sSQL = sSQL + "WHERE ((LP_X_UPD_CODES.UPD_TABLE_ID)= " + iLVTableID + ")";
					sSQL = sSQL + " AND ((LEAVE_PLAN.LP_ROW_ID)= " + iLeavePlan + ")";

//					if(objConn!=null)
//						objConn.Dispose();
//
//					objConn = DbFactory.GetDbConnection(m_sConnectionString);
//					objConn.Open();
//					objRdrStatus = objConn.ExecuteReader(sSQL);
					objRdrStatus = DbFactory.GetDbReader(m_sConnectionString,sSQL);
					
					//-- Create the Leave/Claim status sql string
					while(objRdrStatus.Read())
					{
						if(objRdrStatus.GetInt32("STATUS_CODE_TYPE") == iClaimStatus)
						{
							if(sSQL_ClaimStatus == "")
							{
								iStatusCode = objRdrStatus.GetInt32("STATUS_CODE");
								sSQL_ClaimStatus = " AND (CLAIM.CLAIM_STATUS_CODE) IN(" + iStatusCode;
							}
							else
							{
								iStatusCode = objRdrStatus.GetInt32("STATUS_CODE");
								sSQL_ClaimStatus = sSQL_ClaimStatus + ", " + iStatusCode;
							}
						}
						else if(objRdrStatus.GetInt32("STATUS_CODE_TYPE") == iLeaveStatus)
						{
							if(sSQL_LeaveStatus == "")
							{
								iStatusCode = objRdrStatus.GetInt32("STATUS_CODE");
								sSQL_LeaveStatus = " AND (CLAIM_LEAVE.LEAVE_STATUS_CODE) IN(" + iStatusCode;
							}
							else
							{
								iStatusCode = objRdrStatus.GetInt32("STATUS_CODE");
								sSQL_LeaveStatus = sSQL_LeaveStatus + ", " + iStatusCode;
							}
						}
					}

					//-- Set SQL to pull claims/leave records
                    //sSQL = "SELECT LEAVE_ROW_ID, CLAIMANT_EID ";
                    //sSQL = sSQL + "FROM (CLAIM INNER JOIN CLAIM_LEAVE ON CLAIM.CLAIM_ID = CLAIM_LEAVE.CLAIM_ID) ";
                    //sSQL = sSQL + "INNER JOIN CLAIMANT ON CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID ";
                    //sSQL = sSQL + "WHERE (CLAIM_LEAVE.LP_ROW_ID = " + iLeavePlan + ")";

                    //MITS 22294
                    //sSQL = "SELECT LEAVE_ROW_ID, PI_EID,PI_ROW_ID , ";
                    sSQL = "SELECT LEAVE_ROW_ID, PI_EID,PI_ROW_ID  ";//Changed by Amitosh for MITS 24226  (03/04/2011)
                    sSQL = sSQL + "FROM (CLAIM INNER JOIN CLAIM_LEAVE ON CLAIM.CLAIM_ID = CLAIM_LEAVE.CLAIM_ID) ";
                    sSQL = sSQL + "INNER JOIN PERSON_INVOLVED ON CLAIM.EVENT_ID = PERSON_INVOLVED.EVENT_ID ";
                    sSQL = sSQL + "WHERE (CLAIM_LEAVE.LP_ROW_ID = " + iLeavePlan + ")";

					if(sSQL_ClaimStatus != "")
						sSQL = sSQL + sSQL_ClaimStatus + ") ";

					if(sSQL_LeaveStatus != "")
						sSQL = sSQL + sSQL_LeaveStatus + ") ";

					
//					if(objConn!=null)
//						objConn.Dispose();
//
//					objConn = DbFactory.GetDbConnection(m_sConnectionString);
//					objConn.Open();
//					objReader = objConn.ExecuteReader(sSQL);
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
                    bool bRecords = false;
                    //-- Create the Leave/Claim status sql string
                    //if(objReader.Read())
                    //{
						while(objReader.Read())
						{
                            bRecords = true;
                            //MITS 22294
                            iEmpID = objReader.GetInt32("PI_EID");
                            iPiRowId = objReader.GetInt32("PI_ROW_ID");

							objLeaveElig.LeaveRowId = objReader.GetInt32("LEAVE_ROW_ID");
							
							if(objLeaveElig.LoadData(iEmpID,iLeavePlan,p_sEligDate,iPiRowId))
								bLeaveElig = objLeaveElig.SaveData();
						}
                    //}
				}
				bRetVal = true;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UpdateLeaveRecords.Update.Err", m_iClientId),p_objEx);
			}
			finally
			{
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
                if (objRdrPlans != null)
                {
                    objRdrPlans.Close();
                    objRdrPlans.Dispose();
                }
                if (objRdrStatus != null)
                {
                    objRdrStatus.Close();
                    objRdrStatus.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
			}
			return bRetVal;
		}

	}
}
