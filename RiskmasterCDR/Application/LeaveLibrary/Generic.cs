﻿
using System;
using Riskmaster.Db ;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.LeaveLibrary
{
	///************************************************************** 
	///* $File				: Generic.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 01-31-2007
	///* $Author			: Aashish Bhateja
	///***************************************************************	
	/// <summary>	
	///	This class contains the Generic functions related to Leave Library.
	/// </summary>
	public class Generic
	{
        /// <summary>
        /// Holds the Connection String.
        /// </summary>
		private string m_sConnectionString="";

        private int m_iClientId = 0;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="p_sConnectionString">Connection String</param>
        public Generic(string p_sConnectionString, int p_iClientId)
		{
			m_sConnectionString = p_sConnectionString;
            m_iClientId = p_iClientId;
		}

        /// Name		    :   GetPiEmpID
        /// Author		    :   Geeta Sharma
        /// Date Created	:   2 Jan 2007
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author              
        /// ************************************************************
        /// <summary>
        /// Creats a new XmlElement with the Node Name passed as parameter.         
        /// </summary>
        /// <param name="p_iEmpRowId">Employee Row Id</param>  
        /// <returns>Employee ID</returns>
		public int GetPiEmpID(int p_iEmpRowId)
		{
			string sSQL = string.Empty;
			int iEmpId = 0;
			DbReader objDbReader = null;
			sSQL = "SELECT PI_EID FROM PERSON_INVOLVED WHERE PI_ROW_ID =" + p_iEmpRowId;
			try
			{
				objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
				while (objDbReader.Read())
				{
					iEmpId = Conversion.ConvertObjToInt(objDbReader["PI_EID"], m_iClientId);
				}

                objDbReader.Close();
			}

            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Generic.GetPiEmpID.Err", m_iClientId), p_objEx);
            }

			finally
			{
                if (objDbReader != null)
                    objDbReader.Dispose();
			}
			return iEmpId; 
		}

	
	}
}
