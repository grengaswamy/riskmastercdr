using System;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.LeaveLibrary
{
    ///************************************************************** 
    ///* $File				: UpdateLeaveHistory.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 01-31-2007
    ///* $Author			: Aashish Bhateja
    ///***************************************************************	
    /// <summary>	
    ///	Called by user to update the Leave History table. This can be activated
    /// through the Leave form (per Employee) or through Utilities (all employees, 
    /// all plans).
    /// </summary>
    public class UpdateLeaveHistory
	{
        /// <summary>
        /// Constant for days.
        /// </summary>
		private const string LV_DAYS = "d";
        /// <summary>
        /// Constant for months.
        /// </summary>
		private const string LV_MONTHS = "m";
        /// <summary>
        /// Constant for weeks.
        /// </summary>
		private const string LV_WEEKS = "ww";
        /// <summary>
        /// Holds the connection string.
        /// </summary>
		private string m_sConnectionString="";
        private int m_iClientId = 0; //rkaur27
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="p_sConnectionString"></param>
		public UpdateLeaveHistory(string p_sConnectionString, int p_iClientId)//rkaur27
		{
			m_sConnectionString=p_sConnectionString;
            m_iClientId = p_iClientId;//rkaur27
		}

        /// <summary>
        /// Updates the leave history on the click of update button for an emp.
        /// of from utilities.
        /// </summary>
        /// <param name="p_sEligDate"></param>
        /// <param name="p_iLeavePlanId"></param>
        /// <param name="p_iEmpEid"></param>
        /// <param name="p_dHoursWorked"></param>
        /// <returns></returns>
		public bool UpdateLeaveHist(string p_sEligDate,int p_iLeavePlanId,int p_iEmpEid,double p_dHoursWorked)
		{
			bool bRetVal = false;
			bool bResult = false;
			DbConnection objConn = null; 
			DbReader objReader=null;
			string sSQL = string.Empty;

//			string sEligDate = "";
			int iLPRowId = 0;
			int iUseLeaves = 0;
			try
			{
				//-- If no employee number is sent, dHoursWorked cannot be greater than 0
				//-- otherwise, all employees could get updated with the hours worked.
				if(!(p_iEmpEid > 0))
					p_dHoursWorked = -1;
			        
				if(!(p_dHoursWorked > 0)) 
					p_dHoursWorked = -1;

//				if(!(p_iLeavePlanId > 0)) 
//					p_iLeavePlanId = -1;

				if(!(p_iEmpEid > 0)) 
					p_iEmpEid = -1;
			

//				sEligDate = Conversion.GetDate(DateTime.Now.ToString()); 
				sSQL = "SELECT LP_ROW_ID, USE_LEAVES_FLAG FROM LEAVE_PLAN";
				if(p_iLeavePlanId != 0)
					sSQL = sSQL + " WHERE LP_ROW_ID = " + p_iLeavePlanId;

				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();

                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {
                        iLPRowId = objReader.GetInt("LP_ROW_ID");
                        iUseLeaves = objReader.GetInt16("USE_LEAVES_FLAG");
                        if (iUseLeaves == -1)
                        {
                            bResult = bUpdateLeaveHistory(p_sEligDate, p_iEmpEid, iLPRowId, p_dHoursWorked);
                        }
                    }
                }
				bRetVal = true;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UpdateLeaveHistory.UpdateLeaveHist.Err", m_iClientId),p_objEx);
			}
			finally
			{
                if (objConn != null)
                {                    
                    objConn.Close();
                    objConn.Dispose();
                }
			}			
			return bRetVal;
		}

        /// <summary>
        /// Used to update the leave history, called from UpdateLeaveHist().
        /// </summary>
        /// <param name="p_sEligDate"></param>
        /// <param name="p_iEmpEid"></param>
        /// <param name="p_iLPRowId"></param>
        /// <param name="p_dHoursWorked"></param>
        /// <returns></returns>
		private bool bUpdateLeaveHistory(string p_sEligDate,int p_iEmpEid,int p_iLPRowId,double p_dHoursWorked)
		{
			string sSQL = string.Empty;
			string sSQL_Plans = string.Empty;
			string sSQL_Emp = string.Empty;
			string sSQL_ClaimStatus = string.Empty;
			string sSQL_LeaveStatus = string.Empty;
			int iLVHistTableID = 0;
			int iClaimStatus = 0;
			int iLeaveStatus = 0;
			bool bHistory = false;
			bool bRetVal = false;
			int iLeavePlan = 0;
			int iStatusCode = 0;
			string sStartDate = string.Empty;
			string sEndDate = string.Empty;
			int iPerNum = 0;
			string sPerPeriod = string.Empty;
			string sPerPeriodDesc = string.Empty;
			int iEmpID = 0;
			double dUsedHours = 0.0;
			LeaveHistory objLeaveHist = new LeaveHistory(m_sConnectionString, m_iClientId);//psharma206
			LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);
			DbConnection objConn = null; 
			DbReader objRdrPlans=null;
			DbReader objRdrStatus=null;
			DbReader objReader=null;

			try
			{
				//-- Pull the table id for the Leave History table
				iLVHistTableID = objLocalCache.GetTableId("CLAIM_LV_X_HIST");

				//-- Pull the status code type ids for Claim status and Leave status
				iClaimStatus = objLocalCache.GetTableId("CLAIM_STATUS");
				iLeaveStatus = objLocalCache.GetTableId("LEAVE_STATUS");
			
			    //-- Pull a list of the Leave Plan Ids
				sSQL_Plans = "SELECT LP_ROW_ID, PER_NUMBER, PER_PRD_TYPE ";
				sSQL_Plans = sSQL_Plans + " FROM LEAVE_PLAN";
				if(p_iLPRowId > 0)
					sSQL_Plans = sSQL_Plans + " WHERE LP_ROW_ID = " + p_iLPRowId;

//				objConn = DbFactory.GetDbConnection(m_sConnectionString);
//				objConn.Open();
//				objReader = objConn.ExecuteReader(sSQL_Plans);
				objRdrPlans = DbFactory.GetDbReader(m_sConnectionString,sSQL_Plans);
				while(objRdrPlans.Read())
				{
					iLeavePlan = objRdrPlans.GetInt("LP_ROW_ID");
					sEndDate = Conversion.GetDBDateFormat(p_sEligDate,"d");
					objLocalCache.GetCodeInfo(objRdrPlans.GetInt("PER_PRD_TYPE"),ref sPerPeriod,ref sPerPeriodDesc);
					iPerNum = objRdrPlans.GetInt32("PER_NUMBER");
				
				    if(sPerPeriod.ToLower() == "w") 
						sPerPeriod = "ww";

					switch(sPerPeriod.ToLower())
					{
						case LV_DAYS:
							sStartDate = Convert.ToDateTime(sEndDate).AddDays((0 - iPerNum)).ToString();
							break;
						case LV_WEEKS:
							sStartDate = Convert.ToDateTime(sEndDate).AddDays((0 - iPerNum)*7).ToString();
							break;
						case LV_MONTHS:
							sStartDate = Convert.ToDateTime(sEndDate).AddMonths((0 - iPerNum)).ToString();
							break;
						default:
                            sStartDate = Convert.ToDateTime(sEndDate).AddDays((0 - iPerNum)).ToString();
							break;
					}

					sEndDate = Conversion.GetDate(sEndDate);
					sStartDate = Conversion.GetDate(sStartDate);

					//-- Pull the Leave/Claim status codes from the Leave Plan
					sSQL = "SELECT LP_X_UPD_CODES.STATUS_CODE_TYPE, LP_X_UPD_CODES.STATUS_CODE, ";
					sSQL = sSQL + "LEAVE_PLAN.LP_ROW_ID ";
					sSQL = sSQL + "FROM LP_X_UPD_CODES RIGHT JOIN LEAVE_PLAN ON LP_X_UPD_CODES.LP_ROW_ID = LEAVE_PLAN.LP_ROW_ID ";
					sSQL = sSQL + "WHERE ((LP_X_UPD_CODES.UPD_TABLE_ID)= " + iLVHistTableID + ")";
					sSQL = sSQL + " AND ((LEAVE_PLAN.LP_ROW_ID)= " + iLeavePlan + ")";

//					if(objConn!=null)
//						objConn.Dispose();
//
//					objConn = DbFactory.GetDbConnection(m_sConnectionString);
//					objConn.Open();
//					objReader = objConn.ExecuteReader(sSQL);
					objRdrStatus = DbFactory.GetDbReader(m_sConnectionString,sSQL);
					//-- Create the Leave/Claim status sql string
					while(objRdrStatus.Read())
					{
						if(objRdrStatus.GetInt32("STATUS_CODE_TYPE") == iClaimStatus)
						{
							if(sSQL_ClaimStatus == "")
							{
								iStatusCode = objRdrStatus.GetInt32("STATUS_CODE");
								sSQL_ClaimStatus = " AND (CLAIM.CLAIM_STATUS_CODE) IN(" + iStatusCode;
							}
							else
							{
								iStatusCode = objRdrStatus.GetInt32("STATUS_CODE");
								sSQL_ClaimStatus = sSQL_ClaimStatus + ", " + iStatusCode;
							}
						}
						else if(objRdrStatus.GetInt32("STATUS_CODE_TYPE") == iLeaveStatus)
						{
							if(sSQL_LeaveStatus == "")
							{
								iStatusCode = objRdrStatus.GetInt32("STATUS_CODE");
								sSQL_LeaveStatus = " AND (CLAIM_LEAVE.LEAVE_STATUS_CODE) IN(" + iStatusCode;
							}
							else
							{
								iStatusCode = objRdrStatus.GetInt32("STATUS_CODE");
								sSQL_LeaveStatus = sSQL_LeaveStatus + ", " + iStatusCode;
							}
						}
					}

					sSQL = "SELECT Sum(CLAIM_LV_X_DETAIL.HOURS_LEAVE) AS SumOfHOURS_LEAVE, ";
					sSQL = sSQL + "CLAIMANT.CLAIMANT_EID ";
					sSQL = sSQL + "FROM ((CLAIM INNER JOIN CLAIM_LEAVE ON CLAIM.CLAIM_ID = CLAIM_LEAVE.CLAIM_ID) ";
					sSQL = sSQL + "LEFT JOIN CLAIM_LV_X_DETAIL ON CLAIM_LEAVE.LEAVE_ROW_ID = CLAIM_LV_X_DETAIL.LEAVE_ROW_ID) ";
					sSQL = sSQL + "INNER JOIN CLAIMANT ON CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID ";
					sSQL = sSQL + "WHERE ((CLAIM_LV_X_DETAIL.DATE_LEAVE_START)>= " + sStartDate + ") AND ";
					sSQL = sSQL + "((CLAIM_LV_X_DETAIL.DATE_LEAVE_END)<= " + sEndDate + ") ";
					sSQL = sSQL + "AND (CLAIM_LEAVE.LP_ROW_ID = " + iLeavePlan + ") ";

					if(sSQL_ClaimStatus != "")
						sSQL = sSQL + sSQL_ClaimStatus + ") ";

					if(sSQL_LeaveStatus != "")
						sSQL = sSQL + sSQL_LeaveStatus + ") ";

					if(p_iEmpEid > 0) 
						sSQL = sSQL + " AND (CLAIMANT.CLAIMANT_EID = " + p_iEmpEid + ") ";

					sSQL = sSQL + "GROUP BY CLAIMANT.CLAIMANT_EID ";
				
//					if(objConn!=null)
//						objConn.Dispose();
//
//					objConn = DbFactory.GetDbConnection(m_sConnectionString);
//					objConn.Open();
//					objReader = objConn.ExecuteReader(sSQL);
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
					bool bRecords = false;
					//-- Create the Leave/Claim status sql string
//					if(objReader.Read() > 0)
//					{
						while(objReader.Read())
						{
							bRecords = true;
							iEmpID = objReader.GetInt32("CLAIMANT_EID");
							dUsedHours = objReader.GetDouble("SumOfHOURS_LEAVE");
							
							objLeaveHist.DateElig = p_sEligDate;
							objLeaveHist.DateTFEnd= sEndDate;
							objLeaveHist.DateTFStart = sStartDate;
							objLeaveHist.EmployeeEid = iEmpID;
							objLeaveHist.HoursUsed = dUsedHours;
							//-- If dHoursWorked is sent as a parameter, enter it here.
							//-- If not, dHoursWorked will be 0.
							if(p_dHoursWorked > 0)
								objLeaveHist.HoursWorked = p_dHoursWorked;
							
							objLeaveHist.LPRowId = iLeavePlan;

							bHistory = objLeaveHist.SaveData();
						}
//					}
//					else //if no leave detail records found
					if(!bRecords)
					{
						if(p_iEmpEid > 0)//And the update is for one employee
						{
							objLeaveHist.DateElig = p_sEligDate;
							objLeaveHist.DateTFEnd = sEndDate;
							objLeaveHist.DateTFStart = sStartDate;
							objLeaveHist.EmployeeEid = p_iEmpEid;
							objLeaveHist.HoursUsed = 0;
							//-- If dHoursWorked is sent as a parameter, enter it here.
							//-- If not, dHoursWorked will be 0.
							if(p_dHoursWorked > 0)
								objLeaveHist.HoursWorked = p_dHoursWorked;
							objLeaveHist.LPRowId = iLeavePlan;
			                
							bHistory = objLeaveHist.SaveData();
						}
					}
				}
				bRetVal = true;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UpdateLeaveHistory.UpdateLeaveHist.Err", m_iClientId),p_objEx);
			}
			finally
			{
                if (objLocalCache != null)
                     objLocalCache.Dispose();
                 if (objRdrPlans != null)
                 {
                     if (!objRdrPlans.IsClosed)
                     {
                         objRdrPlans.Close();
                     }
                     objRdrPlans.Dispose();
                 }
                 if (objRdrStatus != null)
                 {
                     if (!objRdrStatus.IsClosed)
                     {
                         objRdrStatus.Close();
                     }
                     objRdrStatus.Dispose();
                 }
                 if (objReader != null)
                 {
                     if (!objReader.IsClosed)
                     {
                         objReader.Close();
                     }
                     objReader.Dispose();
                 }
                 if (objConn != null)
                 {                     
                     objConn.Close();
                     objConn.Dispose();
                 }
			}
			return bRetVal;
		}
	}
}
