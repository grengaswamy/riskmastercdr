using System;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;


namespace Riskmaster.Application.LeaveLibrary
{
    ///************************************************************** 
    ///* $File				: LeaveElig.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 01-31-2007
    ///* $Author			: Aashish Bhateja
    ///***************************************************************	
    /// <summary>	
    ///	Leave Elig object definition.It is to be used to return the amount of 
    /// Leave time available, used; eligibility; and timeframe dates.
    /// </summary>
    public class LeaveElig
	{
        /// <summary>
        /// Constant for Days.
        /// </summary>
		private const string LV_DAYS = "d";
        /// <summary>
        /// Constant for Months.
        /// </summary>
		private const string LV_MONTHS = "m";
        /// <summary>
        /// Constant for Weeks.
        /// </summary>
		private const string LV_WEEKS = "ww";

		#region Variables Declaration

        /// <summary>
        /// Holds the Connection String.
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
		/// Leave Row Id
		/// </summary>
		private int m_iLeaveRowId=0; 
		/// <summary>
		/// Eligibility Date
		/// </summary>
		private string m_sDateElig=""; 
		/// <summary>
		/// Time frame start date
		/// </summary>
		private string m_sDateTFStart=""; 
		/// <summary>
		/// Time frame end date
		/// </summary>
		private string m_sDateTFEnd=""; 
		/// <summary>
		/// Date the employee is eligible to take leave
		/// </summary>
		private string m_sDateEligForLeave="";
		/// <summary>
		/// Eligibility status code.
		/// </summary>
		private int m_iEligStatusCode=0;
		/// <summary>
		/// MaxDurHours
		/// </summary>
		private double m_dMaxDurHours=0.0;
		/// <summary>
		/// Number of hours employee has worked
		/// </summary>
		private double m_dHoursWorked=0.0;
		/// <summary>
		/// Number of hours used by the employee
		/// </summary>
		private double m_dHoursUsed=0.0;
		/// <summary>
		/// Number of hours available
		/// </summary>
		private double m_dHoursAvail=0.0;
		/// <summary>
		/// Hours Worked Flag
		/// </summary>
		private int m_iHoursWorkedFlag=0; 
		/// <summary>
		/// Elig Date Flag
		/// </summary>
		private int m_iEligDateFlag=0;

        private int m_iClientId = 0;

		#endregion

		#region Properties Declaration
		/// <summary>
		/// Leave Row Id
		/// </summary>
		public int LeaveRowId
		{
			get
			{
				return m_iLeaveRowId;
			}
			set
			{
				m_iLeaveRowId=value;
			}
		}
		/// <summary>
		/// Eligibility Date
		/// </summary>
		public string DateElig
		{
			get
			{
				return m_sDateElig;
			}
			set
			{
				m_sDateElig=value;
			}
		}
		/// <summary>
		/// Time frame start date
		/// </summary>
		public string DateTFStart
		{
			get
			{
				return m_sDateTFStart;
			}
			set
			{
				m_sDateTFStart=value;
			}
		}
		/// <summary>
		/// Time frame end date
		/// </summary>
		public string DateTFEnd
		{
			get
			{
				return m_sDateTFEnd;
			}
			set
			{
				m_sDateTFEnd=value;
			}
		}
		/// <summary>
		/// Date the employee is eligible to take leave
		/// </summary>
		public string DateEligForLeave
		{
			get
			{
				return m_sDateEligForLeave;
			}
			set
			{
				m_sDateEligForLeave=value;
			}
		}
		/// <summary>
		/// Eligibility status code.
		/// </summary>
		public int EligStatusCode
		{
			get
			{
				return m_iEligStatusCode;
			}
			set
			{
				m_iEligStatusCode=value;
			}
		}
		/// <summary>
		/// MaxDurHours
		/// </summary>
		public double MaxDurHours
		{
			get
			{
				return m_dMaxDurHours;
			}
			set
			{
				m_dMaxDurHours=value;
			}
		}
		/// <summary>
		/// Number of hours employee has worked
		/// </summary>
		public double HoursWorked
		{
			get
			{
				return m_dHoursWorked;
			}
			set
			{
				m_dHoursWorked=value;
			}
		}
		/// <summary>
		/// Number of hours used by the employee
		/// </summary>
		public double HoursUsed
		{
			get
			{
				return m_dHoursUsed;
			}
			set
			{
				m_dHoursUsed=value;
			}
		}
		/// <summary>
		/// Number of hours available
		/// </summary>
		public double HoursAvail
		{
			get
			{
				return m_dHoursAvail;
			}
			set
			{
				m_dHoursAvail=value;
			}
		}
		/// <summary>
		/// Hours Worked Flag
		/// </summary>
		public int HoursWorkedFlag
		{
			get
			{
				return m_iHoursWorkedFlag;
			}
			set
			{
				m_iHoursWorkedFlag=value;
			}
		}
		/// <summary>
		/// Elig Date Flag
		/// </summary>
		public int EligDateFlag
		{
			get
			{
				return m_iEligDateFlag;
			}
			set
			{
				m_iEligDateFlag=value;
			}
		}

		#endregion

        /// <summary>
        /// Determines the eligibility status of the employee.
        /// </summary>
        /// <param name="sDateHired"></param>
        /// <param name="dHoursWorked"></param>
        /// <param name="iEligNumber"></param>
        /// <param name="sEligPrdType"></param>
        /// <param name="iFirstMonthFlag"></param>
        /// <param name="sEligDate"></param>
        /// <param name="dEligHours"></param>
        /// <returns>Eligibility Status i.e. I or E.</returns>
		private string GetEligStatus(string sDateHired,double dHoursWorked,int iEligNumber, string sEligPrdType, int iFirstMonthFlag, string sEligDate,double dEligHours)
        {   
			int iMonth = 0;
			int iYear = 0;
			string sEligForLeaveDate = "";

			try
			{
				sDateHired = Conversion.GetDBDateFormat(sDateHired,"MM/dd/yyyy"); 
				sEligForLeaveDate = sDateHired;
				//--Get the date the employee is eligible for Leave benefits
				if (sEligPrdType == "w") 
					sEligPrdType = "ww";
				if (iFirstMonthFlag == 0) //# & period from Date Hired
				{	
					switch(sEligPrdType.ToLower())
					{
						case LV_DAYS:
							sEligForLeaveDate = Convert.ToDateTime(sDateHired).AddDays(iEligNumber).ToString();
							break;
						case LV_WEEKS:
							sEligForLeaveDate = Convert.ToDateTime(sDateHired).AddDays(iEligNumber*7).ToString();
							break;
						case LV_MONTHS:
							sEligForLeaveDate = Convert.ToDateTime(sDateHired).AddMonths(iEligNumber).ToString();
							break;
						default:
							break;
					}
				}
				else //# & period from 1st of month after date hired
				{
					if (Convert.ToDateTime(sDateHired).Day != 1)
					{
						iMonth = Convert.ToDateTime(sDateHired).Month;
						iYear = Convert.ToDateTime(sDateHired).Year;
						if (iMonth == 12) 
						{    
							iMonth = 1;
							iYear = iYear + 1;
						}
						else
						{
							iMonth = iMonth + 1;
						}
						sEligForLeaveDate = iMonth + "/01/" + iYear;
					}
					switch(sEligPrdType.ToLower())
                    {//Parijat: Mits 11705----replacing'sDateHired' with 'sEligForLeaveDate' so that error is thrown when claim date is less than the first of next month the 
						case LV_DAYS:
                            sEligForLeaveDate = Convert.ToDateTime(sEligForLeaveDate).AddDays(iEligNumber).ToString();
							break;
						case LV_WEEKS:
                            sEligForLeaveDate = Convert.ToDateTime(sEligForLeaveDate).AddDays(iEligNumber * 7).ToString();
							break;
						case LV_MONTHS:
                            sEligForLeaveDate = Convert.ToDateTime(sEligForLeaveDate).AddMonths(iEligNumber).ToString();
							break;
						default:
							break;
					}
				}
    
    
				m_sDateEligForLeave = Conversion.GetDate(sEligForLeaveDate);
		        
				if (Conversion.ToDate(m_sDateEligForLeave) > Conversion.ToDate(sEligDate)) 
					m_iEligDateFlag = 0;
				else
					m_iEligDateFlag = -1;
		        
				//-- check are the hours worked in the timeframe by the employee less
				//-- than the hours required for leave?
				if (dEligHours > dHoursWorked) 
					m_iHoursWorkedFlag = 0;
				else
					m_iHoursWorkedFlag = -1;
			}

			catch{return "";}

			if ((m_iEligDateFlag == 0) || (m_iHoursWorkedFlag == 0))
				return "I";
			else
				return "E";
	    
		}

        /// <summary>
        /// Calculate the maximum number of leave hours for the employee in the 
        /// timeframe.
        /// </summary>
        /// <param name="iWeeklyHours"></param>
        /// <param name="iAllowedNumber"></param>
        /// <param name="sAllowedPrdType"></param>
        /// <returns></returns>
        private double GetMaxDurHours(double p_dWeeklyHours, int iAllowedNumber, string sAllowedPrdType)
        {                        
			double dAllowedNumber = 0.0;
                               
			//-- convert period type to weeks if it is "d" or "m"
			if (sAllowedPrdType == LV_DAYS)
				dAllowedNumber = iAllowedNumber / 7.0; //no days in a week
			else if (sAllowedPrdType == LV_MONTHS)
				dAllowedNumber = iAllowedNumber * 4.34812141; //no weeks in a month
			else //already weeks
				dAllowedNumber = iAllowedNumber;
        
			//--Return the maximum number of leave hours permitted
            //Geeta 03/21/07 : Modified for Mits number 9009
            return (dAllowedNumber * p_dWeeklyHours);
		}

        /// <summary>
        /// Creates a SQL Statement.
        /// </summary>
        /// <returns></returns>
		private string GetSQLFieldList()
		{
			string sSQL = ""; 

			sSQL = "";
			sSQL = sSQL + "SELECT";
			sSQL = sSQL + " LEAVE_ROW_ID, DATE_ELIGIBILITY";
			sSQL = sSQL + ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER";
			sSQL = sSQL + ", ELIG_STATUS_CODE, MAX_DUR_HOURS";
			sSQL = sSQL + ", HOURS_WORKED, HOURS_AVAIL";
			sSQL = sSQL + ", HOURS_USED";
			sSQL = sSQL + ", DATE_TF_START, DATE_TF_END";
			return sSQL;
		}

        /// <summary>
        /// Loads the eligibility data.
        /// </summary>
        /// <param name="p_iEmpID"></param>
        /// <param name="p_iLeavePlanID"></param>
        /// <param name="p_sEligDate"></param>
        /// <returns>a boolean value, whether the leave eligibility object created 
        /// successfully or not.
        /// </returns>
        public bool LoadData(int p_iEmpID, int p_iLeavePlanID, string p_sEligDate, int p_iPiRowId)
		{    
			//Const sFunctionName As String = "LoadData"
			bool bRetVal=false;
			bool bResult=false;
			string sSQL = string.Empty;

			string sDateHired = string.Empty;

			string sBenePrdType = string.Empty, sBenePrdTypeDesc = string.Empty;
			string sAllowedPrdType = string.Empty, sAllowedPrdTypeDesc = string.Empty;
			string sEligStatusCode = string.Empty;

			int iIneligibleStatus = 0;
			int iEligibleStatus = 0;

            //Geeta 03/21/07 : Modified for Mits number 9009
            double dWeeklyHours = 0;

			DbConnection objConn = null; 
			DbReader objReader = null;

			LeaveHistory objLeaveHist = new LeaveHistory(m_sConnectionString, m_iClientId);
            LeavePlan objLeavePlan = new LeavePlan(m_sConnectionString, m_iClientId);
			LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);
		    
			try
			{
				//-- pull leave plan data
				bResult = objLeavePlan.LoadData(p_iLeavePlanID);
				
				if(bResult)
                {
                    sSQL = "";
                    //Geeta 03/21/07 : Modified for Mits number 9009
                    //MITS 22294 : Date Hired and weekly hours should be fetched from PERSON_INVOLVED as same employee have different 
                    //Date Hired and weekly hours in differnt claim.
                    sSQL = "SELECT DATE_HIRED,WEEKLY_HOURS FROM PERSON_INVOLVED";
                    sSQL = sSQL + " WHERE PI_ROW_ID=" + p_iPiRowId;

                    objConn = DbFactory.GetDbConnection(m_sConnectionString);
                    objConn.Open();

                    using (objReader = objConn.ExecuteReader(sSQL))
                    {
                        if (objReader.Read())
                        {
                            sDateHired = objReader.GetString("DATE_HIRED");
                            dWeeklyHours = objReader.GetDouble("WEEKLY_HOURS"); //Geeta 03/21/07 : Modified for Mits number 9009
                        }                        
                    }

                    objLocalCache.GetCodeInfo(objLeavePlan.BenePeriodType, ref sBenePrdType, ref sBenePrdTypeDesc);
                    objLocalCache.GetCodeInfo(objLeavePlan.AllowedPeriodType, ref sAllowedPrdType, ref sAllowedPrdTypeDesc);
                    iIneligibleStatus = objLocalCache.GetCodeId("I", "LEAVE_ELIG_STATUS");
                    iEligibleStatus = objLocalCache.GetCodeId("E", "LEAVE_ELIG_STATUS");

                    //-- Check that weeklyHours is greater than 0
                    if (dWeeklyHours == 0)
                        dWeeklyHours = Convert.ToDouble(objLeavePlan.HoursPerWeek);

                    //-- load leave history data
                    bResult = objLeaveHist.LoadData(p_iEmpID, objLeavePlan.LPRowId);

                    if (bResult)
                    {
                        m_sDateElig = objLeaveHist.DateElig;
                        m_sDateTFStart = objLeaveHist.DateTFStart;
                        m_sDateTFEnd = objLeaveHist.DateTFEnd;
                        m_dHoursWorked = objLeaveHist.HoursWorked;
                        m_dHoursUsed = objLeaveHist.HoursUsed;



                        //-- get the eligibility status code
                        sEligStatusCode = GetEligStatus(sDateHired, m_dHoursWorked, objLeavePlan.EligBeneNumber, sBenePrdType, objLeavePlan.BeneFirstFlag, m_sDateElig, objLeavePlan.EligHours);

                        if (sEligStatusCode == "E")
                            m_iEligStatusCode = iEligibleStatus;
                        else
                            m_iEligStatusCode = iIneligibleStatus;

                        m_dMaxDurHours = GetMaxDurHours(dWeeklyHours, objLeavePlan.AllowedNumber, sAllowedPrdType.ToLower()); //Geeta 03/21/07 : Modified for Mits number 9009

                        //-- Get Maximum hours - check for zero or negative
                        if (m_dMaxDurHours > 0)
                        {
                            m_dHoursAvail = m_dMaxDurHours - m_dHoursUsed;
                            //-- check for a negative number hours avail
                            if (m_dHoursAvail < 0)
                                m_dHoursAvail = 0;
                        }
                        else
                            m_dHoursAvail = 0;



                        bRetVal = true;
                    }
                    else
                        bRetVal = false;
                }
				else
					bRetVal=false;
			}				
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("LeaveElig.LoadData.Err", m_iClientId), p_objEx);
			}
			finally
			{	
				if(objConn!=null)
				{
					objConn.Close();
					objConn.Dispose();
                }				
                if(objLocalCache !=null)
                    objLocalCache.Dispose();
			}
			return bRetVal;

		}

        /// <summary>
        /// Saves the data to CLAIM_LEAVE table.Called from UpdateLeaveRecords.cs
        /// </summary>
        /// <returns></returns>
		public bool SaveData()
		{    
			//Const sFunctionName As String = "LoadData"
			bool bRetVal=false;
			bool bUpdate=false;
			string sSQL = string.Empty;
			DbConnection objConn = null; 
			DbReader objReader = null;
			DbWriter objDbWriter = null;
		    
			try
			{
				//Pull Leave History records
				sSQL = GetSQLFieldList();
				sSQL = sSQL + " FROM CLAIM_LEAVE";
				sSQL = sSQL + " WHERE LEAVE_ROW_ID = " + m_iLeaveRowId;
		    
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                        bUpdate = true;
                }
				if(objConn!=null)
					objConn.Dispose();

				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();

				objDbWriter = DbFactory.GetDbWriter(objConn);

				objDbWriter.Tables.Add("CLAIM_LEAVE");
				//-- Need to uncomment the below lines.
				objDbWriter.Fields.Add("DTTM_RCD_LAST_UPD",Conversion.GetDate(DateTime.Now.ToString()));
//				objDbWriter.Fields.Add("UPDATED_BY_USER",this.);
				objDbWriter.Fields.Add("LEAVE_ROW_ID",this.LeaveRowId);
				objDbWriter.Fields.Add("DATE_ELIGIBILITY",this.DateElig);					
				objDbWriter.Fields.Add("DATE_TF_START",this.DateTFStart);
				objDbWriter.Fields.Add("DATE_TF_END",this.DateTFEnd);
				objDbWriter.Fields.Add("HOURS_USED",this.HoursUsed);
				objDbWriter.Fields.Add("HOURS_WORKED",this.HoursWorked);
				objDbWriter.Fields.Add("HOURS_AVAIL",this.HoursAvail);
				objDbWriter.Fields.Add("ELIG_STATUS_CODE",this.EligStatusCode);
				objDbWriter.Fields.Add("MAX_DUR_HOURS",this.MaxDurHours);

				if(bUpdate)
				{
					// Update existing record				
					objDbWriter.Where.Add(" LEAVE_ROW_ID = " + m_iLeaveRowId);    
				}
				else
				{
					// Insert new record
                    m_iLeaveRowId = Utilities.GetNextUID(m_sConnectionString, "CLAIM_LEAVE", m_iClientId);
					objDbWriter.Fields.Add("LEAVE_ROW_ID",m_iLeaveRowId);
				//-- Need to uncomment the below lines.
//					objDbWriter.Fields.Add("ADDED_BY_USER",m_sAddedByUser);
//					objDbWriter.Fields.Add("DTTM_RCD_ADDED",m_sDttmRcdAdded);
				}
				objDbWriter.Execute(); 
				bRetVal=true;
			}				
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("LeaveElig.SaveData.Err", m_iClientId), p_objEx);
			}
			finally
			{
				if(objConn!=null)
				{
					objConn.Close();
					objConn.Dispose();
				}			
				objDbWriter = null;
			}
			return bRetVal;

		}

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="p_sConnectionString"></param>
        public LeaveElig(string p_sConnectionString, int p_iClientId)
		{
			m_sConnectionString=p_sConnectionString;
            m_iClientId = p_iClientId;
		}
	}
}
