using System;
using System.Collections;
using Riskmaster.Settings; 
using DataDynamics.ActiveReports.Document;
using Riskmaster.Application.ReportInterfaces;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using DataDynamics.ActiveReports;

namespace Riskmaster.Application.PrintEOB
{
	/// <summary>	
	/// Author  :  Anurag Agarwal
	/// Date    :  31 Dec 2004	
	/// </summary>
	
	/// <summary>
	/// This is the ultimate base class for all reports; this basically is the result of optimization of code for all reports.
	/// Also, this class can absorb future report changes if any.This class inherits the ActiveReport class in namespace DataDynamics.ActiveReports. 
	/// </summary>
	public class CustomActiveReport:DataDynamics.ActiveReports.ActiveReport3
	{ 
		#region Member Variables
		/// <summary>
		/// Collection of reports.
		/// </summary>
		internal CEOBRecord m_structCEOBRecord; 
		/// <summary>
		/// Notification object.
		/// </summary>
		protected  IWorkerNotifications m_objNotify=null;
		#endregion

		#region Properties
		/// <summary>
		/// Property to access reports collection.
		/// </summary>
		internal virtual CEOBRecord EOBReportData
		{
			set 
			{
				m_structCEOBRecord=value;
			}
		}
		/// <summary>
		/// Property to access notification object.
		/// </summary>
		public virtual  IWorkerNotifications Notify
		{
			set 
			{
				m_objNotify=value;
			}
		}		
		#endregion

		#region Constructor
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		internal CustomActiveReport()
		{
		}
		#endregion
	}

	/// <summary>
	/// This is the base class for all EOB reports.
	/// It has been framed taking into consideration the future changes to the reports(EOB).
	/// </summary>
	public class CommonEOB : CustomActiveReport 
	{
		#region Member Variables
		/// <summary>
		/// This will hold the year of the report.
		/// </summary>
		protected string m_sConnectionString;

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = string.Empty;

		/// <summary>
		/// This will hold the value of total restricted days.
		/// </summary>
		int m_EOBFetchCount=0;

        private int m_iClientId = 0;
	
		#endregion

		#region Constructor
		/// <summary>
		/// This is the constructor with year of report as parameter.
		/// </summary>
		/// <param name="p_iYearOfReport">Year Of Report</param>
		internal CommonEOB(string p_sConnectionString, string p_sUserName, int p_iClientId)
		{
			m_sConnectionString=p_sConnectionString;
			m_sUserName = p_sUserName;
            m_iClientId = p_iClientId;
		}
		/// <summary>
		/// It is the default constructor.
		/// </summary>
		internal CommonEOB()
		{
			
		}
		#endregion

		#region Methods
		/// <summary>
		/// This method will initialize the fields of the EOB report to be printed.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		protected void DataInitializeBase(ref object p_objsender, ref System.EventArgs p_objeArgs)
		{       
			try
			{
				m_EOBFetchCount = 0;

				Fields.Add("SYS_CLIENT_NAME");
				Fields.Add("SYS_CLIENT_ADDR1");
				Fields.Add("SYS_CLIENT_ADDR2");
                Fields.Add("SYS_CLIENT_ADDR3"); // JIRA 6420 syadav55
                Fields.Add("SYS_CLIENT_ADDR4"); // JIRA 6420 syadav55
				Fields.Add("SYS_CLIENT_CITY");
				Fields.Add("SYS_CLIENT_STATE");
				Fields.Add("SYS_CLIENT_ZIPCODE");
				Fields.Add("SYS_CLIENT_PHONE");
				Fields.Add("SYS_CLIENT_FAX");
				Fields.Add("TOTAL_AMOUNT_PAID");
				Fields.Add("TOTAL_AMOUNT_BILLED");
				Fields.Add("TOTAL_AMOUNT_SAVED");
				Fields.Add("TOTAL_AMOUNT_REDUCED");
				Fields.Add("TOTAL_SCHEDULED_AMOUNT");
				Fields.Add("TOTAL_BASE_AMOUNT");
				Fields.Add("TOTAL_AMOUNT_ALLOWED");
				Fields.Add("TOTAL_DISCOUNT_AMOUNT");
				Fields.Add("TOTAL_PER_DIEM_AMT");
				Fields.Add("TOTAL_STOP_LOSS_AMT");
				Fields.Add("TOTAL_FEE_TABLE_AMT");
				Fields.Add("PAYEE_LAST_NAME");
				Fields.Add("PAYEE_FIRST_NAME");
				Fields.Add("PAYEE_FULL_NAME");
				Fields.Add("PAYEE_FULL_NAME_ADDR");
				Fields.Add("PAYEE_ADDR1");
				Fields.Add("PAYEE_ADDR2");
                Fields.Add("PAYEE_ADDR3"); // JIRA 6420 syadav55
                Fields.Add("PAYEE_ADDR4"); // JIRA 6420 syadav55
				Fields.Add("PAYEE_CITY");
				Fields.Add("PAYEE_STATE");
				Fields.Add("PAYEE_POSTAL_CODE");
				Fields.Add("CLAIM_NUMBER");
				Fields.Add("CLAIMANT_LAST_NAME");
				Fields.Add("CLAIMANT_FIRST_NAME");
				Fields.Add("CLAIMANT_FULL_NAME");
				Fields.Add("CLAIMANT_ADDR1");
				Fields.Add("CLAIMANT_ADDR2");
                Fields.Add("CLAIMANT_ADDR3"); // JIRA 6420 syadav55
                Fields.Add("CLAIMANT_ADDR4"); // JIRA 6420 syadav55
				Fields.Add("CLAIMANT_CITY");
				Fields.Add("CLAIMANT_STATE");
				Fields.Add("CLAIMANT_POSTAL_CODE");
				Fields.Add("SYS_USER");
				Fields.Add("SYS_DATE");
				Fields.Add("CLAIMANT_SSN");
				Fields.Add("EVENT_DATE");
				Fields.Add("DATE_REPORTED");
				Fields.Add("RPTD_FULL_NAME");
				Fields.Add("TRANS_DATE");
				Fields.Add("PAYEE_TAXID");
				Fields.Add("EMP_DEPT_CODE");
				Fields.Add("EMP_DEPARTMENT");
				Fields.Add("EMP_FACILITY");
				Fields.Add("EMP_LOCATION");
				Fields.Add("EMP_DIVISION");
				Fields.Add("EMP_REGION");
				Fields.Add("EMP_OPERATION");
				Fields.Add("EMP_COMPANY");
				Fields.Add("EMP_CLIENT");
				Fields.Add("EMP_DEPARTMENT_NAICS");
				Fields.Add("EMP_FACILITY_NAICS");
				Fields.Add("EMP_LOCATION_NAICS");
				Fields.Add("EMP_DIVISION_NAICS");
				Fields.Add("EMP_REGION_NAICS");
				Fields.Add("EMP_OPERATION_NAICS");
				Fields.Add("EMP_COMPANY_NAICS");
				Fields.Add("EMP_CLIENT_NAICS");
				Fields.Add("ADJUSTER_ABBREV");
				Fields.Add("ADJUSTER_FULL_NAME");
				Fields.Add("ADJUSTER_LAST_NAME");
				Fields.Add("INVOICE_NUMBER");
				Fields.Add("INVOICE_DATE");
				Fields.Add("PROCEDURE_DATE");
				Fields.Add("PROC_TO_DATE");
				Fields.Add("PROCEDURE_CODE");
				Fields.Add("PROCEDURE");
				Fields.Add("FEE_SCHEDULE");
				Fields.Add("PERCENTILE");
				Fields.Add("PLACE_OF_SERVICE");
				Fields.Add("PLACE_OF_SERVICE_CODE");
				Fields.Add("TYPE_OF_SERVICE");
				Fields.Add("TYPE_OF_SERVICE_CODE");
				Fields.Add("TRANS_TYPE");
				Fields.Add("TRANS_TYPE_CODE");
				Fields.Add("EXPLANATION");
				Fields.Add("DIAGNOSIS");
				Fields.Add("MODIFIER");
				Fields.Add("UNITS_BILLED");
				Fields.Add("AMOUNT_BILLED");
				Fields.Add("AMOUNT_PAID");
				Fields.Add("SCHEDULED_AMOUNT");
				Fields.Add("AMOUNT_REDUCED");
				Fields.Add("AMOUNT_SAVED");
				Fields.Add("CONTRACT_AMOUNT");
				Fields.Add("PROVIDER_DISCOUNT");
				Fields.Add("BILLING_POSTAL_CODE");
				Fields.Add("BASE_AMOUNT");
				Fields.Add("AMOUNT_ALLOWED");
				Fields.Add("DISCOUNT_AMOUNT");
				Fields.Add("PER_DIEM_AMOUNT");
				Fields.Add("STOP_LOSS_AMOUNT");
				Fields.Add("FEE_TABLE_AMOUNT");
				Fields.Add("TWCC_NUMBER");
				Fields.Add("HEALTH_CARE_PROVIDER_TAX_ID");
				Fields.Add("EMPLOYERS_NAME_ADDRESS");
				Fields.Add("EMPLOYEE_ADDR");
				Fields.Add("NAME_ADDR_AUDIT");
				Fields.Add("DATE_OF_AUDIT");
				Fields.Add("AUDIT_NAME");
				Fields.Add("SYS_CLIENT_FULL_ADDRESS");
				Fields.Add("SYS_CLIENT_NAME_ADDRESS");
				Fields.Add("PEC");
				Fields.Add("PAYEE_CITY_STATE_ZIP");
				Fields.Add("EMP_FACILITY_FULL_ADDRESS");
				Fields.Add("BATCH_NUMBER");
				Fields.Add("CHECK_NUMBER");
				Fields.Add("VENDOR_NUMBER");
				Fields.Add("LOSS_DATE");
				Fields.Add("PAYMENT_CODE");
				Fields.Add("PERIOD_COVERED");
				Fields.Add("CODE");
				Fields.Add("DCN");
				Fields.Add("DIVISION_NAME");
				Fields.Add("DIVISION_TAXID");
				Fields.Add("SYS_CLIENT_NUMBER");
				Fields.Add("DIVISION_NAME_ADDR");
				Fields.Add("PATIENT_ACCOUNT_NUMBER");
				Fields.Add("OTHER_REF_1");
				Fields.Add("OTHER_REF_2");
				Fields.Add("FEE_OR_CUSTOMARY");
				Fields.Add("RECOMMENDED_ALLOWANCE");
				Fields.Add("RECEIPT_DATE");
				Fields.Add("REASON_CODE");
				Fields.Add("REASON_MESSAGES");
				Fields.Add("CTL_NUMBER");
				Fields.Add("ICN_CTL_NUMBER");

				Fields.Add("CURRENTDATE");
                //Changed by Gagan for MITS 11220 : Start
                Fields.Add("TOTAL_CHARGES");
                Fields.Add("TOTAL_PAYMENT");
                Fields.Add("PAYEE_NPIFEIN");
                Fields.Add("PROCEDURE_CODE_MODIFIER");
                //Changed by Gagan for MITS 11220 : End
                
			}			
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("CommonEOB.DataInitializeBase.Error", m_iClientId), p_objException);
			}	
			
		}
		/// <summary>
		/// This method will fetch the data for populating the fields of the EOB report to be printed.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		protected void FetchDataBase(ref object p_objsender,ref FetchEventArgs p_objeArgs)		
		{
			p_objeArgs.EOF=true;
			string sStrTmp = string.Empty;
			SysSettings objSysSettings = null;
			CEOBDetailRecord objCEOBDetailRecord;
            //Changed by Gagan for MITS 11220 : Start
            double dblTotalAmountBilled = 0;
            double dblTotalAmountPaid = 0;
            //Changed by Gagan for MITS 11220 : End

			try
			{
    
				//Position to latest detail record. If we're all done, signal we're done (eof).
				m_EOBFetchCount = m_EOBFetchCount + 1;
				if(m_EOBFetchCount > m_structCEOBRecord.DetailRecordsList.Count)
				{
					p_objeArgs.EOF = true;
					return;
				}
				else
                    p_objeArgs.EOF = false;
    
				// ... Invoice
				Fields["TOTAL_AMOUNT_PAID"].Value = m_structCEOBRecord.TotalAmountPaid;
				Fields["TOTAL_AMOUNT_BILLED"].Value = m_structCEOBRecord.TotalAmountBilled;
				Fields["TOTAL_AMOUNT_SAVED"].Value = m_structCEOBRecord.TotalAmountSaved;
				Fields["TOTAL_AMOUNT_REDUCED"].Value = m_structCEOBRecord.TotalAmountReduced;
				Fields["TOTAL_SCHEDULED_AMOUNT"].Value = m_structCEOBRecord.TotalScheduledAmount;
				Fields["TOTAL_BASE_AMOUNT"].Value = m_structCEOBRecord.TotalBaseAmount;
				Fields["TOTAL_AMOUNT_ALLOWED"].Value = m_structCEOBRecord.TotalAmountAllowed;
				Fields["TOTAL_DISCOUNT_AMOUNT"].Value = m_structCEOBRecord.TotalDiscountAmount;
				Fields["TOTAL_PER_DIEM_AMT"].Value = m_structCEOBRecord.TotalPerDiemAmt;
				Fields["TOTAL_STOP_LOSS_AMT"].Value = m_structCEOBRecord.TotalStopLossAmt;
				Fields["TOTAL_FEE_TABLE_AMT"].Value = m_structCEOBRecord.TotalFeeTableAmt;
				Fields["PAYEE_LAST_NAME"].Value = m_structCEOBRecord.PayeeLastName;
				Fields["PAYEE_FIRST_NAME"].Value = m_structCEOBRecord.PayeeFirstName;
				Fields["PAYEE_FULL_NAME"].Value = m_structCEOBRecord.PayeeFullName; 
				Fields["PAYEE_FULL_NAME_ADDR"].Value = m_structCEOBRecord.PayeeFullNameAddr;
				Fields["PAYEE_ADDR1"].Value = m_structCEOBRecord.PayeeAddr1;
				Fields["PAYEE_ADDR2"].Value = m_structCEOBRecord.PayeeAddr2;
                Fields["PAYEE_ADDR3"].Value = m_structCEOBRecord.PayeeAddr3; // JIRA 6420 syadav55
                Fields["PAYEE_ADDR4"].Value = m_structCEOBRecord.PayeeAddr4; // JIRA 6420 syadav55
				Fields["PAYEE_CITY"].Value = m_structCEOBRecord.PayeeCity;
				Fields["PAYEE_CITY_STATE_ZIP"].Value = m_structCEOBRecord.PayeeCity + ", " + m_structCEOBRecord.PayeeState + " " + m_structCEOBRecord.PayeePostalCode;
                //changed by gagan for MITS 11520 : start
                if (m_structCEOBRecord.PayeeState!=null && m_structCEOBRecord.PayeeState.Length >= 2)
				    Fields["PAYEE_STATE"].Value = m_structCEOBRecord.PayeeState.Substring(0,2);
                //changed by gagan for MITS 11520 : end
				Fields["PAYEE_POSTAL_CODE"].Value = m_structCEOBRecord.PayeePostalCode;
				Fields["CLAIM_NUMBER"].Value = m_structCEOBRecord.ClaimNumber;
				Fields["CLAIMANT_LAST_NAME"].Value = m_structCEOBRecord.ClaimantLastName;
				Fields["CLAIMANT_FIRST_NAME"].Value = m_structCEOBRecord.ClaimantFirstName;
				Fields["CLAIMANT_FULL_NAME"].Value = m_structCEOBRecord.ClaimantFullName;
				Fields["CLAIMANT_ADDR1"].Value = m_structCEOBRecord.ClaimantAddr1;
				Fields["CLAIMANT_ADDR2"].Value = m_structCEOBRecord.ClaimantAddr2;
                Fields["CLAIMANT_ADDR3"].Value = m_structCEOBRecord.ClaimantAddr3; // JIRA 6420 syadav55
                Fields["CLAIMANT_ADDR4"].Value = m_structCEOBRecord.ClaimantAddr4; // JIRA 6420 syadav55
				Fields["CLAIMANT_CITY"].Value = m_structCEOBRecord.ClaimantCity;

                //changed by gagan for MITS 11520 : start
                if (m_structCEOBRecord.ClaimantState!= null && m_structCEOBRecord.ClaimantState.Length >= 2)
                    Fields["CLAIMANT_STATE"].Value = m_structCEOBRecord.ClaimantState.Substring(0, 2);
                //changed by gagan for MITS 11520 : end	

				Fields["CLAIMANT_POSTAL_CODE"].Value = m_structCEOBRecord.ClaimantPostalCode;
				Fields["SYS_USER"].Value = m_sUserName;
                // akaushik5 Changed for MITS 35846 Starts
                //Fields["SYS_DATE"].Value = string.Format("{0:MMMM dd, YYYY}",DateTime.Now);
                Fields["SYS_DATE"].Value = string.Format("{0:MMMM dd, yyyy}", DateTime.Now);
                // akaushik5 Changed for MITS 35846 Ends
				Fields["CLAIMANT_SSN"].Value = m_structCEOBRecord.ClaimantSSN;
                //Changed by Gagan for MITS 11520 : Start
                if (m_structCEOBRecord.EventDate != "" && m_structCEOBRecord.EventDate!= null)
				    Fields["EVENT_DATE"].Value = Conversion.ToDate(m_structCEOBRecord.EventDate).ToShortDateString() ;
                if (m_structCEOBRecord.DateReported != "" && m_structCEOBRecord.DateReported != null)
				    Fields["DATE_REPORTED"].Value = Conversion.ToDate(m_structCEOBRecord.DateReported).ToShortDateString();
				Fields["RPTD_FULL_NAME"].Value = m_structCEOBRecord.RptdFullName;                
                if (m_structCEOBRecord.TransDate != "" && m_structCEOBRecord.TransDate != null)
				    Fields["TRANS_DATE"].Value = Conversion.ToDate(m_structCEOBRecord.TransDate).ToShortDateString();
                //Changed by Gagan for MITS 11520 : End
				Fields["PAYEE_TAXID"].Value = m_structCEOBRecord.PayeeTaxID;
				Fields["EMP_DEPT_CODE"].Value = m_structCEOBRecord.EmpDeptCode;
				Fields["EMP_DEPARTMENT"].Value = m_structCEOBRecord.EmpDepartment; 
				Fields["EMP_FACILITY"].Value = m_structCEOBRecord.EmpFacility; 
				Fields["EMP_LOCATION"].Value = m_structCEOBRecord.EmpLocation; 
				Fields["EMP_DIVISION"].Value = m_structCEOBRecord.EmpDivision; 
				Fields["EMP_REGION"].Value = m_structCEOBRecord.EmpRegion; 
				Fields["EMP_OPERATION"].Value = m_structCEOBRecord.EmpOperation; 
				Fields["EMP_COMPANY"].Value = m_structCEOBRecord.EmpCompany; 
				Fields["EMP_CLIENT"].Value = m_structCEOBRecord.EmpClient; 
			    
				sStrTmp = m_structCEOBRecord.PayeeFullName; 
				if(m_structCEOBRecord.PayeeAddr1 != string.Empty)
					sStrTmp = sStrTmp + "\n\r" + m_structCEOBRecord.PayeeAddr1;
				
				if(m_structCEOBRecord.PayeeAddr2 != string.Empty)
					sStrTmp = sStrTmp + "\n\r" + m_structCEOBRecord.PayeeAddr2;

                if (m_structCEOBRecord.PayeeAddr3 != string.Empty)
                    sStrTmp = sStrTmp + "\n\r" + m_structCEOBRecord.PayeeAddr3; // JIRA 6420 syadav55

                if (m_structCEOBRecord.PayeeAddr4 != string.Empty)
                    sStrTmp = sStrTmp + "\n\r" + m_structCEOBRecord.PayeeAddr4; // JIRA 6420 syadav55
				
				if(m_structCEOBRecord.PayeeCity != string.Empty)
					sStrTmp = sStrTmp + "\n\r" + m_structCEOBRecord.PayeeCity + ", " + m_structCEOBRecord.PayeeState + " " + m_structCEOBRecord.PayeePostalCode;

				Fields["PAYEE_FULL_NAME_ADDR"].Value = sStrTmp;
				Fields["EMP_FACILITY_FULL_ADDRESS"].Value = sStrTmp; 
				Fields["HEALTH_CARE_PROVIDER_TAX_ID"].Value = m_structCEOBRecord.PayeeTaxID;

			    
				Fields["EMP_DEPARTMENT_NAICS"].Value = m_structCEOBRecord.EmpDepartmentNAICS;
				Fields["EMP_FACILITY_NAICS"].Value = m_structCEOBRecord.EmpFacilityNAICS;
				Fields["EMP_LOCATION_NAICS"].Value = m_structCEOBRecord.EmpLocationNAICS;
				Fields["EMP_DIVISION_NAICS"].Value = m_structCEOBRecord.EmpDivisionNAICS;
				Fields["EMP_REGION_NAICS"].Value = m_structCEOBRecord.EmpRegionNAICS;
				Fields["EMP_OPERATION_NAICS"].Value = m_structCEOBRecord.EmpOperationNAICS;
				Fields["EMP_COMPANY_NAICS"].Value = m_structCEOBRecord.EmpCompanyNAICS;
				Fields["EMP_CLIENT_NAICS"].Value = m_structCEOBRecord.EmpClientNAICS;
			    
				Fields["ADJUSTER_ABBREV"].Value = m_structCEOBRecord.AdjusterAbbrev;
				Fields["ADJUSTER_FULL_NAME"].Value = m_structCEOBRecord.AdjusterFullName;
				Fields["ADJUSTER_LAST_NAME"].Value = m_structCEOBRecord.AdjusterLastName;
                //Arnab: MITS-10722 - Getting Adjuster name and phone no
                Fields["AUDIT_NAME"].Value = m_structCEOBRecord.AdjusterFullName + "\n\r" + m_structCEOBRecord.AdjusterPhone1;
                //MITS-10722 End
			    
				objSysSettings = new SysSettings(m_sConnectionString,m_iClientId);
  
				Fields["SYS_CLIENT_NAME"].Value = objSysSettings.ClientName;
				Fields["SYS_CLIENT_ADDR1"].Value = objSysSettings.Addr1;
				Fields["SYS_CLIENT_ADDR2"].Value = objSysSettings.Addr2;
                //Fields["SYS_CLIENT_ADDR3"].Value = objSysSettings.Addr3; // JIRA 6420 syadav55
                //Fields["SYS_CLIENT_ADDR4"].Value = objSysSettings.Addr4; // JIRA 6420 syadav55
				Fields["SYS_CLIENT_CITY"].Value = objSysSettings.City;
				Fields["SYS_CLIENT_STATE"].Value = objSysSettings.State;
				Fields["SYS_CLIENT_ZIPCODE"].Value = objSysSettings.ZipCode;
				Fields["SYS_CLIENT_PHONE"].Value = objSysSettings.PhoneNumber;
				Fields["SYS_CLIENT_FAX"].Value = objSysSettings.FaxNumber;
			    
				Fields["SYS_CLIENT_NUMBER"].Value = string.Empty;
 
				sStrTmp = string.Empty;
 
				if(objSysSettings.Addr1 != string.Empty)
					sStrTmp = objSysSettings.Addr1;

				if(objSysSettings.Addr2 != string.Empty)
					sStrTmp = sStrTmp + "\n\r" + objSysSettings.Addr2;

                //if (objSysSettings.Addr3 != string.Empty)
                //    sStrTmp = sStrTmp + "\n\r" + objSysSettings.Addr3; // JIRA 6420 syadav55

                //if (objSysSettings.Addr4 != string.Empty)
                //    sStrTmp = sStrTmp + "\n\r" + objSysSettings.Addr4; // JIRA 6420 syadav55

				if(objSysSettings.City != string.Empty)
					sStrTmp = sStrTmp + "\n\r" + objSysSettings.City + ", " + objSysSettings.State + " " + objSysSettings.ZipCode;

				Fields["SYS_CLIENT_NAME_ADDRESS"].Value = objSysSettings.ClientName + "\n\r" + sStrTmp;
				Fields["NAME_ADDR_AUDIT"].Value = string.Empty;
			    
				Fields["DIVISION_NAME"].Value = m_structCEOBRecord.DivisionName; 
				Fields["DIVISION_TAXID"].Value = m_structCEOBRecord.DivisionTaxID;
				Fields["DIVISION_NAME_ADDR"].Value = m_structCEOBRecord.DivisionNameAddr;
			    
				Fields["DATE_OF_AUDIT"].Value = string.Empty; 
                //Arnab: MITS-10722 - Getting Adjuster name and phone no
                //Fields["AUDIT_NAME"].Value = string.Empty; 
                //Arnab: MITS-10722 - End
				Fields["TWCC_NUMBER"].Value = m_structCEOBRecord.FileNumber;
				Fields["CLAIM_NUMBER"].Value = m_structCEOBRecord.ClaimNumber;
				Fields["EMPLOYEE_ADDR"].Value = m_structCEOBRecord.ClaimantFullAddress;
				Fields["EMPLOYERS_NAME_ADDRESS"].Value = m_structCEOBRecord.EmpFacility + "\n\r" + m_structCEOBRecord.EmpFacilityAddress;

				Fields["BATCH_NUMBER"].Value = m_structCEOBRecord.BatchNumber;
				Fields["CHECK_NUMBER"].Value = m_structCEOBRecord.CheckNumber;
				Fields["VENDOR_NUMBER"].Value = m_structCEOBRecord.VendorNumber;
                // akaushik5 Changed for MITS 35846 Starts
                //Fields["LOSS_DATE"].Value = m_structCEOBRecord.EventDate;
                Fields["LOSS_DATE"].Value = Conversion.ToDate(m_structCEOBRecord.EventDate).ToShortDateString();
                // akaushik5 Changed for MITS 35846 Ends
				Fields["PAYMENT_CODE"].Value = m_structCEOBRecord.PaymentCode;
				Fields["PERIOD_COVERED"].Value = m_structCEOBRecord.PeriodCovered;
				Fields["CODE"].Value = m_structCEOBRecord.Code; 
				Fields["DCN"].Value = m_structCEOBRecord.DCN;

				Fields["PATIENT_ACCOUNT_NUMBER"].Value = m_structCEOBRecord.PatientAccountNumber;
				Fields["OTHER_REF_1"].Value = m_structCEOBRecord.OtherRef1;
				Fields["OTHER_REF_2"].Value = m_structCEOBRecord.OtherRef2;
				Fields["FEE_OR_CUSTOMARY"].Value = m_structCEOBRecord.FeeOrCustomary;
				Fields["RECOMMENDED_ALLOWANCE"].Value = m_structCEOBRecord.RecommendedAllowance;
				Fields["RECEIPT_DATE"].Value = m_structCEOBRecord.ReceiptDate;
				Fields["REASON_CODE"].Value = m_structCEOBRecord.ReasonCodes;
				Fields["REASON_MESSAGES"].Value = m_structCEOBRecord.CodeMessages;
				Fields["CTL_NUMBER"].Value = m_structCEOBRecord.ControlNumber;
				Fields["ICN_CTL_NUMBER"].Value = m_structCEOBRecord.ICNControlNumber;

                
				// ... Detail
				objCEOBDetailRecord = (CEOBDetailRecord)m_structCEOBRecord.DetailRecordsList[m_EOBFetchCount];

					Fields["INVOICE_NUMBER"].Value = objCEOBDetailRecord.InvoiceNumber;

                    //Changed by Gagan for MITS 11520 : Start
					//if(objCEOBDetailRecord.InvoiceDate != string.Empty)                 
                    
                    if (objCEOBDetailRecord.InvoiceDate != string.Empty && objCEOBDetailRecord.InvoiceDate != null)
                        Fields["INVOICE_DATE"].Value = Conversion.ToDate(objCEOBDetailRecord.InvoiceDate).ToShortDateString();       //Added support since BRS now collects.
                    else if (m_structCEOBRecord.TransDate != string.Empty && m_structCEOBRecord.TransDate != null)
                        Fields["INVOICE_DATE"].Value = Conversion.ToDate(m_structCEOBRecord.TransDate).ToShortDateString(); //Fallback for now if invoice date is empty.             

                    
                    Fields["PROCEDURE_DATE"].Value = Conversion.ToDate(objCEOBDetailRecord.ProcedureDate).ToShortDateString();
                    //Changed by Gagan for MITS 11520 : End
					Fields["PROC_TO_DATE"].Value = Conversion.ToDate(objCEOBDetailRecord.ProcToDate).ToShortDateString();//Deb Underwrtiters
					Fields["PROCEDURE_CODE"].Value = objCEOBDetailRecord.ProcedureCode;
					Fields["PROCEDURE"].Value = objCEOBDetailRecord.Procedure; 
					Fields["FEE_SCHEDULE"].Value = objCEOBDetailRecord.FeeSchedule;
					Fields["PERCENTILE"].Value = objCEOBDetailRecord.Percentile;
					Fields["PLACE_OF_SERVICE"].Value = objCEOBDetailRecord.PlaceOfService; 
					Fields["PLACE_OF_SERVICE_CODE"].Value = objCEOBDetailRecord.PlaceofServiceCode;
					Fields["TYPE_OF_SERVICE"].Value = objCEOBDetailRecord.TypeOfService; 
					Fields["TYPE_OF_SERVICE_CODE"].Value = objCEOBDetailRecord.TypeOfServiceCode;
					Fields["TRANS_TYPE"].Value = objCEOBDetailRecord.TransType; 
					Fields["TRANS_TYPE_CODE"].Value = objCEOBDetailRecord.TransTypeCode;
					Fields["UNITS_BILLED"].Value = objCEOBDetailRecord.UnitsBilled;
					Fields["AMOUNT_BILLED"].Value = objCEOBDetailRecord.AmountBilled;
					Fields["AMOUNT_PAID"].Value = objCEOBDetailRecord.AmountPaid;
					Fields["SCHEDULED_AMOUNT"].Value = objCEOBDetailRecord.ScheduledAmount;
					Fields["AMOUNT_REDUCED"].Value = objCEOBDetailRecord.AmountReduced;
					Fields["AMOUNT_SAVED"].Value = objCEOBDetailRecord.AmountSaved;
					Fields["CONTRACT_AMOUNT"].Value = objCEOBDetailRecord.ContractAmount;
					Fields["PROVIDER_DISCOUNT"].Value = objCEOBDetailRecord.Discount; 
					Fields["BILLING_POSTAL_CODE"].Value = objCEOBDetailRecord.ProviderZipCode;
					Fields["BASE_AMOUNT"].Value = objCEOBDetailRecord.BaseAmount;
					Fields["AMOUNT_ALLOWED"].Value = objCEOBDetailRecord.AmountAllowed;
					Fields["DISCOUNT_AMOUNT"].Value = objCEOBDetailRecord.DiscountAmount;
					Fields["PER_DIEM_AMOUNT"].Value = objCEOBDetailRecord.PerDiemAmount;
					Fields["STOP_LOSS_AMOUNT"].Value = objCEOBDetailRecord.StopLossAmount;
					Fields["FEE_TABLE_AMOUNT"].Value = objCEOBDetailRecord.FeeTableAmount;
			        
					// Comma separate if multiple eobs, diagnosis, or modifiers
					sStrTmp = string.Empty; 
                // 12/12/2007 Geeta MITS 10744 
					foreach(DictionaryEntry sStrEOB in objCEOBDetailRecord.EOBList)
					{
						if(sStrTmp.Length > 0)
							sStrTmp = sStrTmp + ", ";
                        sStrTmp = sStrTmp + Conversion.ConvertObjToStr(sStrEOB.Value); 
					}
		
					Fields["EXPLANATION"].Value = sStrTmp;
			        
					sStrTmp = string.Empty;

                    foreach (DictionaryEntry sStrDiag in objCEOBDetailRecord.DiagnosisList)
					{
						if(sStrTmp.Length > 0)
							sStrTmp = sStrTmp + ", ";
						sStrTmp = sStrTmp + Conversion.ConvertObjToStr(sStrDiag.Value); 
					}
                    
					Fields["DIAGNOSIS"].Value = sStrTmp;
			        
					sStrTmp = String.Empty;

                    foreach (DictionaryEntry sStrMod in objCEOBDetailRecord.ModifierList)
					{
						if(sStrTmp.Length > 0)
							sStrTmp = sStrTmp + ", ";
						sStrTmp = sStrTmp + Conversion.ConvertObjToStr(sStrMod.Value); 
					}
                    // 12/12/2007 Geeta MITS 10744 End
					Fields["MODIFIER"].Value = sStrTmp; 

					Fields["CURRENTDATE"].Value = DateTime.Now.ToShortDateString();

                    //Changed by Gagan for MITS 11220 : Start              
                    if(objCEOBDetailRecord.ProcedureCode!= "")
                    {
                        if (sStrTmp != "")
                            Fields["PROCEDURE_CODE_MODIFIER"].Value = objCEOBDetailRecord.ProcedureCode + " - " + sStrTmp;
                        else
                            Fields["PROCEDURE_CODE_MODIFIER"].Value = objCEOBDetailRecord.ProcedureCode;
                    }
                    else
                    {
                            Fields["PROCEDURE_CODE_MODIFIER"].Value = sStrTmp;
                    }

                    if (m_structCEOBRecord.PayeeTaxID != "")
                    {
                        if (m_structCEOBRecord.PayeeNPI != "")
                            Fields["PAYEE_NPIFEIN"].Value = m_structCEOBRecord.PayeeNPI + "/" + m_structCEOBRecord.PayeeTaxID;                      
                      else
                            Fields["PAYEE_NPIFEIN"].Value = m_structCEOBRecord.PayeeTaxID;                      
                     }        
                     else
                     {
                         Fields["PAYEE_NPIFEIN"].Value = m_structCEOBRecord.PayeeNPI;
                     }

                    foreach (int iKey in m_structCEOBRecord.DetailRecordsList.Keys )
                    {
                        objCEOBDetailRecord = (CEOBDetailRecord)m_structCEOBRecord.DetailRecordsList[iKey];
                        dblTotalAmountBilled += objCEOBDetailRecord.AmountBilled;
                        dblTotalAmountPaid += objCEOBDetailRecord.AmountPaid;
                    }
                // akaushik5 Changed for MITS 35846 Starts
                    // Fields["TOTAL_CHARGES"].Value = dblTotalAmountBilled.ToString();
                    // Fields["TOTAL_PAYMENT"].Value = dblTotalAmountPaid.ToString();
                    Fields["TOTAL_CHARGES"].Value = dblTotalAmountBilled;
                    Fields["TOTAL_PAYMENT"].Value = dblTotalAmountPaid;
                // akaushik5 Changed for MITS 35846 Ends
                    //Changed by Gagan for MITS 11220 : End               
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("CommonEOB.FetchDataBase.Error", m_iClientId), p_objException);
			}	
			finally
			{
				if(objSysSettings != null)
					objSysSettings = null;
			}
		}


		/// <summary>
		/// This method will do the page setting for the EOB Report to be printed.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event</param>
		protected void ReportStartBase(ref object p_objsender, ref System.EventArgs p_objeArgs)
		{
			try
			{
				this.PageSettings.Orientation=DataDynamics.ActiveReports.Document.PageOrientation.Portrait;
               
                //Deb : Underwriters 
                this.PageSettings.Margins.Left = 0.5F;
                this.PageSettings.Margins.Top = 2.0F;
                this.PageSettings.Margins.Bottom = 2.0F;
                //Deb : Underwriters 
               
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("CommonEOB.ReportStartBase.Error", m_iClientId), p_objException);
			}
		}
	
		#endregion
		
	}
}
