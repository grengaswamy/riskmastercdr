﻿using System;
using System.Collections;
using System.Data;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using C1.C1Pdf;
using ceTe.DynamicPDF.Printing;
using DataDynamics.ActiveReports.Export.Pdf;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;

namespace Riskmaster.Application.PrintEOB
{
    /// <summary> 
    ///Author  :   Anurag Agarwal
    ///Dated   :   23th,December 2004
    ///Purpose :   Contains generic Functions to be used by the PrintEOB & PrintEOBBRS classes
    /// </summary>
    // akaushik5 Changed for MITS 35846 Starts
    // internal class Functions
    internal class Functions:IDisposable
    // akaushik5 Changed for MITS 35846 Ends
    {
        //Deb : MITS 32174 obsoleted no longer to be used instead use PrintCheckJobNEW
        //[DllImportAttribute("pdfprintsdk.dll")]//It should be in bin
        //public static extern int VeryPDF_PDFPrint(string strCommandLine);
        //[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        //public static extern int GetShortPathName([MarshalAs(UnmanagedType.LPTStr)]string path, [MarshalAs(UnmanagedType.LPTStr)]StringBuilder shortPath, int shortPathLength);
        //Deb : MITS 32174 obsoleted no longer to be used instead use PrintCheckJobNEW

		/// <summary>
		/// Default Constructor 
		/// </summary>
		internal Functions(string p_sDsnId ,string p_sUserName,string p_ConnectionString,string p_sPassword,string p_sDsnName, int p_iClientId)
		{
			m_sUserName = p_sUserName;
			m_sDsnId = p_sDsnId;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
			m_ConnectionString = p_ConnectionString;
			m_objColumnsSelected = new Hashtable();
			m_objSpaceWidth = new Hashtable();
            m_iClientId = p_iClientId;//rkaur27
            //abisht
            this.ReadCheckOptions();
            // akaushik5 Added for MITS 35846 Starts
            this.m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId);//sharishkumar Jira 835
            this.m_objLocalCache = new LocalCache(m_ConnectionString,m_iClientId);
            // akaushik5 Added for MITS 35846 Ends
		}

		#region "Variable Declaration"

        /// <summary>
        /// Private variable to store dsn name
        /// </summary>
        private string m_sDsnName = string.Empty;

        // akaushik5 Added for MITS 35846 Starts
        /// <summary>
        /// Gets or sets the data model factory.
        /// </summary>
        /// <value>
        /// The data model factory.
        /// </value>
        private DataModelFactory m_objDataModelFactory = null;

        /// <summary>
        /// The m_obj local cache
        /// </summary>
        private LocalCache m_objLocalCache = null;
        // akaushik5 Added for MITS 35846 Ends

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = string.Empty;

        /// <summary>
        /// Stores the funds object
        /// </summary>
        private Funds m_objFunds = null;

        /// <summary>
        /// Private variable to store EOB record info
        /// </summary>
        private EOBRecord m_structEOBRecord;

        /// <summary>
        /// Private variable to store EOB detail record info
        /// </summary>
        private EOBDetailRecord m_structEOBDetailRecord;

        /// <summary>
        /// Private variable to store user password
        /// </summary>
        private string m_sPassword = string.Empty;

		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_ConnectionString = string.Empty;

		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sDsnId = string.Empty;

        /// <summary>
        /// Private variable for Printer Name
        /// </summary>
        private string m_Printername = "";

        /// <summary>
        /// Private variable for EOB bin value
        /// </summary>
        private string m_EOBBin = "";
        /// <summary>
        /// Wrapper class object for PrintDocument component to generate PDF
        /// </summary>
        private PrintWrapper m_objPrintWrapper = null; 
        ///abisht
        /// <summary>
        /// Private variable for Print Claimant EOB Flag
        /// </summary>
        public bool m_bPrintClaimantEOB = false;
        /// <summary>
        /// Private variable for Print Payee EOB Flag
        /// </summary>
        public bool m_bPrintPayeeEOB = false;
        /// <summary>
        /// Private variable for Direct to Printer Flag
        /// </summary>
        public bool m_bDirectToPrinter = false;
        /// <summary>
        /// Private variable for EOB Description
        /// </summary>
        public bool m_bEOBDescNextLine = false;
		/// <summary>
		/// Collection to hold the columns extracted from text file
		/// </summary>
		public Hashtable m_objColumnsSelected;

		/// <summary>
		/// Collection to hold the Width of SPACE tags
		/// </summary>
		public Hashtable m_objSpaceWidth;

		#endregion

		#region "Constants declaration"

		/// <summary>
		/// Constant char for '<'
		/// </summary>
		public const char SIGN_GREATER = '<';

		/// <summary>
		/// Constant char for '>'
		/// </summary>
		public const char SIGN_LOWER = '>';

		/// <summary>
		/// Constant char for '>'
		/// </summary>
		public const string TEXT_SPACE = "SPACE";

		/// <summary>
		/// Constant string for Alignment LEFT
		/// </summary>
		public const string ALIGN_LEFT = "LEFT";

		/// <summary>
		/// Constant string for Alignment RIGHT
		/// </summary>
		public const string ALIGN_RIGHT = "RIGHT";

		/// <summary>
		/// Constant string for Alignment CENTER
		/// </summary>
		public const string ALIGN_CENTER = "CENTER";

		/// <summary>
		/// Constant string for total_amount_paid
		/// </summary>
		public const string TOTAL_AMOUNT_PAID = "TOTAL_AMOUNT_PAID";

		/// <summary>
		/// Constant string for total_amount_billed
		/// </summary>
		public const string TOTAL_AMOUNT_BILLED = "TOTAL_AMOUNT_BILLED";

		/// <summary>
		/// Constant string for total_amount_saved
		/// </summary>
		public const string TOTAL_AMOUNT_SAVED = "TOTAL_AMOUNT_SAVED";

		/// <summary>
		/// Constant string for total_amount_reduced
		/// </summary>
		public const string TOTAL_AMOUNT_REDUCED = "TOTAL_AMOUNT_REDUCED";

		/// <summary>
		/// Constant string for total_scheduled_amount
		/// </summary>
		public const string TOTAL_SCHEDULED_AMOUNT = "TOTAL_SCHEDULED_AMOUNT";

		/// <summary>
		/// Constant string for total_base_amount
		/// </summary>
		public const string TOTAL_BASE_AMOUNT = "TOTAL_BASE_AMOUNT";

		/// <summary>
		/// Constant string for total_amount_allowed
		/// </summary>
		public const string TOTAL_AMOUNT_ALLOWED = "TOTAL_AMOUNT_ALLOWED";

		/// <summary>
		/// Constant string for total_discount_amount
		/// </summary>
		public const string TOTAL_DISCOUNT_AMOUNT = "TOTAL_DISCOUNT_AMOUNT";

		/// <summary>
		/// Constant string for total_per_diem_amt
		/// </summary>
		public const string TOTAL_PER_DIEM_AMT = "TOTAL_PER_DIEM_AMT";

		/// <summary>
		/// Constant string for total_stop_loss_amt
		/// </summary>
		public const string TOTAL_STOP_LOSS_AMT = "TOTAL_STOP_LOSS_AMT";

		/// <summary>
		/// Constant string for total_fee_table_amt
		/// </summary>
		public const string TOTAL_FEE_TABLE_AMT = "TOTAL_FEE_TABLE_AMT";

		/// <summary>
		/// Constant string for payee_last_name
		/// </summary>
		public const string PAYEE_LAST_NAME = "PAYEE_LAST_NAME";

		/// <summary>
		/// Constant string for payee_first_name
		/// </summary>
		public const string PAYEE_FIRST_NAME = "PAYEE_FIRST_NAME";

		/// <summary>
		/// Constant string for payee_full_name
		/// </summary>
		public const string PAYEE_FULL_NAME = "PAYEE_FULL_NAME";

		/// <summary>
		/// Constant string for payee_addr1
		/// </summary>
		public const string PAYEE_ADDR1 = "PAYEE_ADDR1";

		/// <summary>
		/// Constant string for payee_addr2
		/// </summary>
		public const string PAYEE_ADDR2 = "PAYEE_ADDR2";

        /// <summary>
        /// Constant string for payee_addr3
        /// </summary>
        public const string PAYEE_ADDR3 = "PAYEE_ADDR3";

        /// <summary>
        /// Constant string for payee_addr4
        /// </summary>
        public const string PAYEE_ADDR4 = "PAYEE_ADDR4";

		/// <summary>
		/// Constant string for payee_city
		/// </summary>
		public const string PAYEE_CITY = "PAYEE_CITY";

		/// <summary>
		/// Constant string for payee_state
		/// </summary>
		public const string PAYEE_STATE = "PAYEE_STATE";

        /// <summary>
        /// Constant string for payee_state_code
        /// </summary>
        public const string PAYEE_STATE_CODE = "PAYEE_STATE_CODE";

        /// <summary>
        /// Constant string for payee_state_desc
        /// </summary>
        public const string PAYEE_STATE_DESC = "PAYEE_STATE_DESC";
		/// <summary>
		/// Constant string for payee_postal_code
		/// </summary>
		public const string PAYEE_POSTAL_CODE = "PAYEE_POSTAL_CODE";

		/// <summary>
		/// Constant string for claim_number
		/// </summary>
		public const string CLAIM_NUMBER = "CLAIM_NUMBER";

		/// <summary>
		/// Constant string for claimant_last_name
		/// </summary>
		public const string CLAIMANT_LAST_NAME = "CLAIMANT_LAST_NAME";

		/// <summary>
		/// Constant string for claimant_first_name
		/// </summary>
		public const string CLAIMANT_FIRST_NAME = "CLAIMANT_FIRST_NAME";

		/// <summary>
		/// Constant string for claimant_full_name
		/// </summary>
		public const string CLAIMANT_FULL_NAME = "CLAIMANT_FULL_NAME";

		/// <summary>
		/// Constant string for claimant_addr1
		/// </summary>
		public const string CLAIMANT_ADDR1 = "CLAIMANT_ADDR1";

		/// <summary>
		/// Constant string for claimant_addr2
		/// </summary>
		public const string CLAIMANT_ADDR2 = "CLAIMANT_ADDR2";

        /// <summary>
        /// Constant string for claimant_addr3
        /// </summary>
        public const string CLAIMANT_ADDR3 = "CLAIMANT_ADDR3";

        /// <summary>
        /// Constant string for claimant_addr4
        /// </summary>
        public const string CLAIMANT_ADDR4 = "CLAIMANT_ADDR4";

		/// <summary>
		/// Constant string for claimant_city
		/// </summary>
		public const string CLAIMANT_CITY = "CLAIMANT_CITY";

		/// <summary>
		/// Constant string for claimant_state
		/// </summary>
		public const string CLAIMANT_STATE = "CLAIMANT_STATE";

		/// <summary>
		/// Constant string for claimant_postal_code
		/// </summary>
		public const string CLAIMANT_POSTAL_CODE = "CLAIMANT_POSTAL_CODE";

		/// <summary>
		/// Constant string for sys_user
		/// </summary>
		public const string SYS_USER = "SYS_USER";

        //nadim for 14513
        /// <summary>
        /// Constant string for sys_user_last_name
        /// </summary>
        public const string SYS_USER_LAST_NAME = "SYS_USER_LAST_NAME";
        /// <summary>
        /// Constant string for sys_user_first_name
        /// </summary>
        public const string SYS_USER_FIRST_NAME = "SYS_USER_FIRST_NAME";

        //nadim for 14513     
		/// <summary>
		/// Constant string for sys_date
		/// </summary>
		public const string SYS_DATE = "SYS_DATE";

		/// <summary>
		/// Constant string for claimant_ssn
		/// </summary>
		public const string CLAIMANT_SSN = "CLAIMANT_SSN";

		/// <summary>
		/// Constant string for event_date
		/// </summary>
		public const string EVENT_DATE = "EVENT_DATE";

		/// <summary>
		/// Constant string for date_reported
		/// </summary>
		public const string DATE_REPORTED = "DATE_REPORTED";

		/// <summary>
		/// Constant string for rptd_full_name
		/// </summary>
		public const string RPTD_FULL_NAME = "RPTD_FULL_NAME";

		/// <summary>
		/// Constant string for trans_date
		/// </summary>
		public const string TRANS_DATE = "TRANS_DATE";

		/// <summary>
		/// Constant string for payee_taxid
		/// </summary>
		public const string PAYEE_TAXID = "PAYEE_TAXID";

		/// <summary>
		/// Constant string for emp_dept_code
		/// </summary>
		public const string EMP_DEPT_CODE = "EMP_DEPT_CODE";

		/// <summary>
		/// Constant string for emp_department
		/// </summary>
		public const string EMP_DEPARTMENT = "EMP_DEPARTMENT";

		/// <summary>
		/// Constant string for emp_facility
		/// </summary>
		public const string EMP_FACILITY = "EMP_FACILITY";

		/// <summary>
		/// Constant string for emp_location
		/// </summary>
		public const string EMP_LOCATION = "EMP_LOCATION";

		/// <summary>
		/// Constant string for emp_division
		/// </summary>
		public const string EMP_DIVISION = "EMP_DIVISION";

		/// <summary>
		/// Constant string for emp_region
		/// </summary>
		public const string EMP_REGION = "EMP_REGION";

		/// <summary>
		/// Constant string for emp_operation
		/// </summary>
		public const string EMP_OPERATION = "EMP_OPERATION";

		/// <summary>
		/// Constant string for emp_company
		/// </summary>
		public const string EMP_COMPANY = "EMP_COMPANY";
        //skhare7 MITS 23373
        public const string EMP_ADDRESS1 = "EMP_ADDRESS1";

        //Ashish Ahuja Mits 33681 : Start
        public const string EMP_DIV_CITY = "EMP_DIV_CITY";
        public const string EMP_DIV_STATE = "EMP_DIV_STATE";
        public const string EMP_DIV_POSTAL_CODE = "EMP_DIV_POSTAL_CODE";
        //Ashish Ahuja Mits 33681 : End
        //skhare7 MITS 23373
        /// <summary>
        /// Stores the EmpDiv Add
        /// </summary>
        public const string EMP_DIV_ADDR1="EMP_DIV_ADDR1";

        public const string EMP_DIV_ADDR2="EMP_DIV_ADDR2";
        public const string EMP_DIV_ADDR3 = "EMP_DIV_ADDR3";
        public const string EMP_DIV_ADDR4 = "EMP_DIV_ADDR4";
        /// <summary>
        /// Stores the EmpDEpt Add
        /// </summary>
        public const string EMP_DEPT_ADDR1="EMP_DEPT_ADDR1";

        public const string EMP_DEPT_ADDR2="EMP_DEPT_ADDR2";
        public const string EMP_DEPT_ADDR3 = "EMP_DEPT_ADDR3";
        public const string EMP_DEPT_ADDR4 = "EMP_DEPT_ADDR4";
        /// <summary>
        /// Stores the EmpFac Add
        /// </summary>
        public const string EMP_FAC_ADDR1="EMP_FAC_ADDR1";

        public const string EMP_FAC_ADDR2="EMP_FAC_ADDR2";
        public const string EMP_FAC_ADDR3 = "EMP_FAC_ADDR3";
        public const string EMP_FAC_ADDR4 = "EMP_FAC_ADDR4";
        // <summary>
        /// Stores the EmpLocation Add
        /// </summary>
        public const string EMP_LOC_ADDR1="EMP_LOC_ADDR1";

        public const string EMP_LOC_ADDR2="EMP_LOC_ADDR2";
        public const string EMP_LOC_ADDR3 = "EMP_LOC_ADDR3";
        public const string EMP_LOC_ADDR4 = "EMP_LOC_ADDR4";
        // <summary>
        /// Stores the EmpRegion Add
        /// </summary>
        public const string EMP_REG_ADDR1="EMP_REG_ADDR1";

        public const string EMP_REG_ADDR2="EMP_REG_ADDR2";
        public const string EMP_REG_ADDR3 = "EMP_REG_ADDR3";
        public const string EMP_REG_ADDR4 = "EMP_REG_ADDR4";
        // <summary>
        /// Stores the EmpClient Add
        /// </summary>
        public const string EMP_CLIENT_ADDR1="EMP_CLIENT_ADDR1";

        public const string EMP_CLIENT_ADDR2="EMP_CLIENT_ADDR2";
        public const string EMP_CLIENT_ADDR3 = "EMP_CLIENT_ADDR3";
        public const string EMP_CLIENT_ADDR4 = "EMP_CLIENT_ADDR4";

        // <summary>
        /// Stores the EmpComp Add
        /// </summary>
        public const string EMP_COMP_ADDR1="EMP_COMP_ADDR1";

        public  const string EMP_COMP_ADDR2="EMP_COMP_ADDR2";
        public const string EMP_COMP_ADDR3 = "EMP_COMP_ADDR3";
        public const string EMP_COMP_ADDR4 = "EMP_COMP_ADDR4";
        // <summary>
        /// Stores the EmpCertificate Add
        /// </summary>
        public const string EMP_CERT_NUM="EMP_CERT_NUM";

        // <summary>
        /// Stores the EmpOP Add
        /// </summary>
        public const string EMP_OP_ADDR1="EMP_OP_ADDR1";

        public const string EMP_OP_ADDR2="EMP_OP_ADDR2";
        public const string EMP_OP_ADDR3 = "EMP_OP_ADDR3";
        public const string EMP_OP_ADDR4 = "EMP_OP_ADDR4";

		/// <summary>
		/// Constant string for emp_client
		/// </summary>
		public const string EMP_CLIENT = "EMP_CLIENT";

		/// <summary>
		/// Constant string for adjuster_abbrev
		/// </summary>
		public const string ADJUSTER_ABBREV = "ADJUSTER_ABBREV";

		/// <summary>
		/// Constant string for adjuster_full_name
		/// </summary>
		public const string ADJUSTER_FULL_NAME = "ADJUSTER_FULL_NAME";

		/// <summary>
		/// Constant string for adjuster_last_name
		/// </summary>
		public const string ADJUSTER_LAST_NAME = "ADJUSTER_LAST_NAME";

		/// <summary>
		/// Constant string for check_number
		/// </summary>
		public const string CHECK_NUMBER = "CHECK_NUMBER";

		/// <summary>
		/// Constant string for check_memo
		/// </summary>
		public const string CHECK_MEMO = "CHECK_MEMO";

		/// <summary>
		/// Constant string for check_date
		/// </summary>
		public const string CHECK_DATE = "CHECK_DATE";

		/// <summary>
		/// Constant string for ctl_number
		/// </summary>
		public const string CTL_NUMBER = "CTL_NUMBER";

		/// <summary>
		/// Constant string for funds_supp.
		/// </summary>
		public const string FUNDS_SUPP = "FUNDS_SUPP";

		/// <summary>
		/// Constant string for orig_invoice_date
		/// </summary>
		public const string ORIG_INVOICE_DATE = "ORIG_INVOICE_DATE";

		/// <summary>
		/// Constant string for orig_invoice_user
		/// </summary>
		public const string ORIG_INVOICE_USER = "ORIG_INVOICE_USER";

		/// <summary>
		/// Constant string for invoice_date
		/// </summary>
		public const string INVOICE_DATE = "INVOICE_DATE";

		/// <summary>
		/// Constant string for invoiced_by
		/// </summary>
		public const string INVOICED_BY = "INVOICED_BY";

		/// <summary>
		/// Constant string for disc_on_fee_schd
		/// </summary>
		public const string DISC_ON_FEE_SCHD = "DISC_ON_FEE_SCHD";

		/// <summary>
		/// Constant string for prescrip_no
		/// </summary>
		public const string PRESCRIP_NO = "PRESCRIP_NO";

		/// <summary>
		/// Constant string for drug_name
		/// </summary>
		public const string DRUG_NAME = "DRUG_NAME";

		/// <summary>
		/// Constant string for prescrip_date
		/// </summary>
		public const string PRESCRIP_DATE = "PRESCRIP_DATE";

		/// <summary>
		/// Constant string for invoice_number
		/// </summary>
		public const string INVOICE_NUMBER = "INVOICE_NUMBER";

		/// <summary>
		/// Constant string for procedure_date
		/// </summary>
		public const string PROCEDURE_DATE = "PROCEDURE_DATE";

		/// <summary>
		/// Constant string for proc_to_date
		/// </summary>
		public const string PROC_TO_DATE = "PROC_TO_DATE";

		/// <summary>
		/// Constant string for procedure_code
		/// </summary>
		public const string PROCEDURE_CODE = "PROCEDURE_CODE";

		/// <summary>
		/// Constant string for procedure
		/// </summary>
		public const string PROCEDURE = "PROCEDURE";

		/// <summary>
		/// Constant string for fee_schedule
		/// </summary>
		public const string FEE_SCHEDULE = "FEE_SCHEDULE";

		/// <summary>
		/// Constant string for percentile
		/// </summary>
		public const string PERCENTILE = "PERCENTILE";

		/// <summary>
		/// Constant string for place_of_service
		/// </summary>
		public const string PLACE_OF_SERVICE = "PLACE_OF_SERVICE";

		/// <summary>
		/// Constant string for place_of_service_code
		/// </summary>
		public const string PLACE_OF_SERVICE_CODE = "PLACE_OF_SERVICE_CODE";

		/// <summary>
		/// Constant string for type_of_service
		/// </summary>
		public const string TYPE_OF_SERVICE = "TYPE_OF_SERVICE";

		/// <summary>
		/// Constant string for type_of_service_code
		/// </summary>
		public const string TYPE_OF_SERVICE_CODE = "TYPE_OF_SERVICE_CODE";

        //BRS FL Merge
        public const string FL_LICENSE = "FL_LICENSE";
        public const string REV_CODE = "REV_CODE";

        public const string LICENSE_NUMBER_FIRST_RECORD = "LICENSE_NUMBER_FIRST_RECORD";
        public const string INVOICE_NUMBER_FIRST_RECORD = "INVOICE_NUMBER_FIRST_RECORD";
        public const string INVOICE_DATE_FIRST_RECORD = "INVOICE_DATE_FIRST_RECORD";//Debabrata Biswas MITS Issue 17183
        public const string DATE_OF_SERVICE = "DATE_OF_SERVICE";//Debabrata Biswas MITS Issue 17183
        public const string BILLING_POSTAL_CODE_FIRST_RECORD = "BILLING_POSTAL_CODE_FR";
        //End
		/// <summary>
		/// Constant string for modifier
		/// </summary>
		public const string MODIFIER = "MODIFIER";

		/// <summary>
		/// Constant string for modifier_code
		/// </summary>
		public const string MODIFIER_CODE = "MODIFIER_CODE";

		/// <summary>
		/// Constant string for quantity
		/// </summary>
		public const string QUANTITY = "QUANTITY";

		/// <summary>
		/// Constant string for quantity_code
		/// </summary>
		public const string QUANTITY_CODE = "QUANTITY_CODE";

		/// <summary>
		/// Constant string for store
		/// </summary>
		public const string STORE = "STORE";

		/// <summary>
		/// Constant string for store_code
		/// </summary>
		public const string STORE_CODE = "STORE_CODE";

		/// <summary>
		/// Constant string for explanation
		/// </summary>
		public const string EXPLANATION = "EXPLANATION";


        /// <summary>
		/// Constant string for explanation details
		/// </summary>
        public const string EXPLANATION_DETAIL = "EXPLANATION_DETAIL";

		/// <summary>
		/// Constant string for diagnosis
		/// </summary>
		public const string DIAGNOSIS = "DIAGNOSIS";

        /// <summary>
        /// Constant string for diagnosis codes
        /// </summary>
        public const string DIAGNOSIS_CODE = "DIAGNOSIS_CODE";//Debabrata Biswas MITS Issue 17183

        /// <summary>
        /// Constant string for diagnosis details
        /// </summary>
        public const string DIAGNOSIS_DETAILS = "DIAGNOSIS_DETAILS";//Debabrata Biswas MITS Issue 17183

		/// <summary>
		/// Constant string for units_billed
		/// </summary>
		public const string UNITS_BILLED = "UNITS_BILLED";

		/// <summary>
		/// Constant string for amount_billed
		/// </summary>
		public const string AMOUNT_BILLED = "AMOUNT_BILLED";

		/// <summary>
		/// Constant string for amount_paid
		/// </summary>
		public const string AMOUNT_PAID = "AMOUNT_PAID";

		/// <summary>
		/// Constant string for scheduled_amount
		/// </summary>
		public const string SCHEDULED_AMOUNT = "SCHEDULED_AMOUNT";

		/// <summary>
		/// Constant string for amount_reduced
		/// </summary>
		public const string AMOUNT_REDUCED = "AMOUNT_REDUCED";

		/// <summary>
		/// Constant string for amount_saved
		/// </summary>
		public const string AMOUNT_SAVED = "AMOUNT_SAVED";

		/// <summary>
		/// Constant string for contract_amount
		/// </summary>
		public const string CONTRACT_AMOUNT = "CONTRACT_AMOUNT";

		/// <summary>
		/// Constant string for provider_discount
		/// </summary>
		public const string PROVIDER_DISCOUNT = "PROVIDER_DISCOUNT";

		/// <summary>
		/// Constant string for billing_postal_code
		/// </summary>
		public const string BILLING_POSTAL_CODE = "BILLING_POSTAL_CODE";

		/// <summary>
		/// Constant string for base_amount
		/// </summary>
		public const string BASE_AMOUNT = "BASE_AMOUNT";

		/// <summary>
		/// Constant string for amount_allowed
		/// </summary>
		public const string AMOUNT_ALLOWED = "AMOUNT_ALLOWED";

		/// <summary>
		/// Constant string for discount_amount
		/// </summary>
		public const string DISCOUNT_AMOUNT = "DISCOUNT_AMOUNT";

		/// <summary>
		/// Constant string for per_diem_amount
		/// </summary>
		public const string PER_DIEM_AMOUNT = "PER_DIEM_AMOUNT";

		/// <summary>
		/// Constant string for stop_loss_amount
		/// </summary>
		public const string STOP_LOSS_AMOUNT = "STOP_LOSS_AMOUNT";

		/// <summary>
		/// Constant string for fee_table_amount
		/// </summary>
		public const string FEE_TABLE_AMOUNT = "FEE_TABLE_AMOUNT";

		/// <summary>
		/// Constant string for space
		/// </summary>
		public const string SPACE = "SPACE";

		/// <summary>
		/// Constant string for eob
		/// </summary>
		public const string TAG_EOB = "EOB";

		/// <summary>
		/// Constant string for logo
		/// </summary>
		public const string TAG_LOGO = "LOGO";

		/// <summary>
		/// Constant string for title
		/// </summary>
		public const string TAG_TITLE = "TITLE";

		/// <summary>
		/// Constant string for subtitle
		/// </summary>
		public const string TAG_SUBTITLE = "SUBTITLE";

		/// <summary>
		/// Constant string for br
		/// </summary>
		public const string TAG_BR = "BR";

		/// <summary>
		/// Constant string for b
		/// </summary>
		public const string TAG_B = "B";

		/// <summary>
		/// Constant string for /b
		/// </summary>
		public const string ENDTAG_B = "/B";

		/// <summary>
		/// Constant string for font
		/// </summary>
		public const string TAG_FONT = "FONT";

		/// <summary>
		/// Constant string for align
		/// </summary>
		public const string TAG_ALIGN = "ALIGN";

		/// <summary>
		/// Constant string for leftmargin
		/// </summary>
		public const string TAG_LEFTMARGIN = "LEFTMARGIN";

		/// <summary>
		/// Constant string for topmargin
		/// </summary>
		public const string TAG_TOPMARGIN = "TOPMARGIN";

		/// <summary>
		/// Constant string for rightmargin
		/// </summary>
		public const string TAG_RIGHTMARGIN = "RIGHTMARGIN";

		/// <summary>
		/// Constant string for bottommargin
		/// </summary>
		public const string TAG_BOTTOMMARGIN = "BOTTOMMARGIN";

		/// <summary>
		/// Constant string for line_items
		/// </summary>
		public const string TAG_LINE_ITEMS = "LINE_ITEMS";

		/// <summary>
		/// Constant string for End Tag of LINE ITEMS
		/// </summary>
		public const string ENDTAG_LINEITEMS = "/LINE_ITEMS";

		/// <summary>
		/// Constant string for /eob
		/// </summary>
		public const string ENDTAG_EOB = "/EOB";

		/// <summary>
		/// Constant for format file path
		/// </summary>
		public const string TAG_FORMATFILEPATH = "FormatFilePath";

		/// <summary>
		/// Constant for claim format file path for EOB
		/// </summary>
		public const string TAG_EOBCLAIMFILE = "EOBClaimFormatFile";

		/// <summary>
		/// Constant for payee format file path for EOB
		/// </summary>
		public const string TAG_EOBPAYEEFILE = "EOBPayeeFormatFile";

        /// <summary>
        /// Constant for claim HTML format file path for EOB * Debabrata Biswas MITS Issue 17183
        /// </summary>
        public const string TAG_EOBCLAIMHTMLFILE = "EOBClaimHTMLFormatFile";

        /// <summary>
        /// Constant for payee HTML format file path for EOB * Debabrata Biswas MITS Issue 17183
        /// </summary>
        public const string TAG_EOBPAYEEHTMLFILE = "EOBPayeeHTMLFormatFile";

        /// <summary>
        /// Constant for EOBRCDES file path for EOB 
        /// </summary>

        public const string TAG_EOBDESCRIPTIONFILE = "EOBDescriptionFile";
		
        //Added for MITS 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
        /// <summary>
        /// Constant for using EOB Code Description  File 
        /// </summary>
        public const string TAG_USEEOBDESCRIPTIONFILE = "UseEOBDescriptionFile";
        //Added for MITS 19415:Format of Explaination of Benefit form is different in RMX from RMWorld

		/// <summary>
		/// Constant for next line char
		/// </summary>
		public const string CRLF = "\n\r";

        /// <summary>
        /// Constant for Horizontal Line
        /// </summary>
        public const string TAG_HORIZONTAL_LINE = "HR";//Debabrata Biswas MITS Issue 17183

        public const string TAG_TABLE = "TABLE";//Debabrata Biswas MITS Issue 17183
        public const string ENDTAG_TABLE = "/TABLE";//Debabrata Biswas MITS Issue 17183

        public const string TAG_TR = "TR";//Debabrata Biswas MITS Issue 17183
        public const string ENDTAG_TR = "/TR";//Debabrata Biswas MITS Issue 17183

        public const string TAG_TH = "TH";//Debabrata Biswas MITS Issue 17183
        public const string ENDTAG_TH = "/TH";//Debabrata Biswas MITS Issue 17183

        public const string TAG_TD = "TD";//Debabrata Biswas MITS Issue 17183
        public const string ENDTAG_TD = "/TD";//Debabrata Biswas MITS Issue 17183

        private const string MI = "25";

        private const string TX = "52";

        private int m_iClientId = 0;//rkaur27
		#endregion

		#region "SplitDetailTag(string p_sToken,ref string s_pFieldName,ref int p_iWidth,ref int p_iDataWidth,ref string p_sAlign)"
		/// Name		: SplitDetailTag
		/// Author		: Anurag Agarwal
		/// Date Created	: 23 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Splits the EOB Tags and return the values of FieldName, width, Datawidth and alignment
		/// </summary>
		/// <param name="p_sToken">EOB Tag from the format file</param>
		/// <param name="s_pFieldName">Field Name as output parameter</param>
		/// <param name="p_iWidth">Width as output parameter</param>
		/// <param name="p_iDataWidth">Datawidth as output parameter</param>
		/// <param name="p_sAlign">Alignment as output parameter</param>
		internal void SplitDetailTag(string p_sToken,ref string p_sFieldName,ref int p_iWidth,ref int p_iDataWidth,ref string p_sAlign)
		{
			string sTemp = string.Empty;
			int iPosition = 0;

			try
			{
				if(FillSpace(p_sToken.Length) == p_sToken)
					p_sToken = TEXT_SPACE + "," + p_sToken.Length;
				else if(p_sToken.Length > 4)
					sTemp = p_sToken.Substring(1,p_sToken.Length - 2);  
				else
				{
					p_sFieldName = string.Empty;
					return;
				}
				
				iPosition = sTemp.IndexOf(",");
				if(iPosition == -1)
				{
					p_sFieldName = string.Empty;
					return;
				}

				p_sFieldName = sTemp.Substring(0,iPosition);
  				sTemp = sTemp.Substring(iPosition + 1);
				
				if(p_sFieldName == TEXT_SPACE)
				{
					p_iDataWidth = Conversion.ConvertStrToInteger(sTemp);
					p_iWidth = p_iDataWidth;
					p_sAlign = ALIGN_LEFT;
					return;
				}

				iPosition = sTemp.IndexOf(",");  
				p_iWidth = Conversion.ConvertStrToInteger(sTemp.Substring(0,iPosition));  
				sTemp = sTemp.Substring(iPosition +1);
				iPosition = sTemp.IndexOf(",");  
				p_iDataWidth = Conversion.ConvertStrToInteger(sTemp.Substring(0,iPosition));  
				p_sAlign = sTemp.Substring(iPosition +1);
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.SplitDetailTag.Error", m_iClientId), p_objEx);		//sharishkumar Jira 835		
			}
		}
		#endregion
		
		#region "AlignDouble(double p_dToken,int p_iWidth,int p_iDataWidth,string p_sAlign)"
		/// Name		: AlignDouble
		/// Author		: Anurag Agarwal
		/// Date Created	: 23 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Align the floating point numbers for EOB
		/// </summary>
		/// <param name="p_dToken">EOB Tag</param>
		/// <param name="p_iWidth">Width of the field</param>
		/// <param name="p_iDataWidth">Datawidth of the field</param>
		/// <param name="p_sAlign">Alignment</param>
		/// <returns>Align value of the token passed</returns>
		internal string AlignDouble(double p_dToken,int p_iWidth,int p_iDataWidth,string p_sAlign)
		{
			try
			{
				if(p_sAlign == null || p_sAlign.Length == 0)
					p_sAlign = ALIGN_RIGHT;
 
				if(p_iDataWidth == 0)
					p_iDataWidth = 10;

				if(p_iWidth == 0)
					p_iWidth = 12;
				return AlignString(string.Format("{0:#,###,##0.00}",p_dToken),p_iWidth,p_iDataWidth,p_sAlign);
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.FuncAlign.Error", m_iClientId), p_objEx);	//sharishkumar Jira 835			
			}
		}
		#endregion

		#region "AlignString(string p_sToken,int p_iWidth,int p_iDataWidth,string p_sAlign)"
		/// Name		: AlignString
		/// Author		: Anurag Agarwal
		/// Date Created	: 23 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Align the string for EOB
		/// </summary>
		/// <param name="p_sToken">EOB Tag</param>
		/// <param name="p_iWidth">Width of the field</param>
		/// <param name="p_iDataWidth">Datawidth of the field</param>
		/// <param name="p_sAlign">Alignment</param>
		/// <returns>Align value of the token passed</returns>
		internal string AlignString(string p_sToken,int p_iWidth,int p_iDataWidth,string p_sAlign)
		{
			
			int iNum = 0;
			string sAlignString = string.Empty; 

			try
			{
				//Mukul 3/2/2007 Corrected this function to align data
				if(p_sToken.Length > p_iDataWidth)
				{
					p_sToken = p_sToken.Substring(0,p_iDataWidth);

				}
			 
				switch (p_sAlign)
					{
						case ALIGN_CENTER:
							iNum = p_iWidth - p_sToken.Length;
							sAlignString = FillSpace(iNum/2);
							sAlignString += p_sToken;
							sAlignString += FillSpace(iNum/2);
							if (sAlignString.Length<p_iWidth)
								sAlignString+=FillSpace(p_iWidth - sAlignString.Length);
							return sAlignString;
						case ALIGN_RIGHT:
						case "R":
							sAlignString = FillSpace(p_iWidth - p_sToken.Length);
							sAlignString += p_sToken;
							return sAlignString;
						default: //Left
							sAlignString = p_sToken;
							sAlignString += FillSpace(p_iWidth - sAlignString.Length);
							return sAlignString;
					}
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.FuncAlign.Error", m_iClientId), p_objEx);			//sharishkumar Jira 835	
			}

		}
		#endregion

		#region "ProcessToken(string p_sToken,EOBRecord p_structEOBRecord,Funds p_objFunds)"
		/// Name		: ProcessToken
		/// Author		: Anurag Agarwal
		/// Date Created	: 27 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// To retrieve Custom EOB header token
		/// </summary>
		/// <param name="p_sToken">EOB Tag</param>
		/// <param name="p_structEOBRecord">EOB record structure</param>
		/// <param name="p_objFunds">Funds object</param>
		/// <returns>Token</returns>
		internal string ProcessToken(string p_sToken,EOBRecord p_structEOBRecord,Funds p_objFunds)
		{
			string sTokenUpper = string.Empty; 
			string sFieldName = string.Empty;
			int iWidth = 0;
			int iDataWidth = 0;
			string sAlign = string.Empty;
 
			try
			{
				if(p_sToken.IndexOf("{") == -1 || p_sToken == string.Empty)
					return p_sToken;
 
				sTokenUpper = p_sToken.ToUpper();
				
				//handle amount fields
				if(sTokenUpper.IndexOf(",") != -1)
				{
					SplitDetailTag(sTokenUpper,ref sFieldName,ref iWidth,ref iDataWidth,ref sAlign);
					switch(sFieldName)
					{
						case TOTAL_AMOUNT_PAID:
							return AlignDouble(p_structEOBRecord.TotalAmountPaid,iWidth,iDataWidth,sAlign);
						case TOTAL_AMOUNT_BILLED:
							return AlignDouble(p_structEOBRecord.TotalAmountBilled,iWidth,iDataWidth,sAlign);
						case TOTAL_AMOUNT_SAVED:
							return AlignDouble(p_structEOBRecord.TotalAmountSaved,iWidth,iDataWidth,sAlign);
						case TOTAL_AMOUNT_REDUCED:
							return AlignDouble(p_structEOBRecord.TotalAmountReduced,iWidth,iDataWidth,sAlign);
						case TOTAL_SCHEDULED_AMOUNT:
							return AlignDouble(p_structEOBRecord.TotalScheduledAmount,iWidth,iDataWidth,sAlign);
						case TOTAL_BASE_AMOUNT:
							return AlignDouble(p_structEOBRecord.TotalBaseAmount,iWidth,iDataWidth,sAlign);
						case TOTAL_AMOUNT_ALLOWED:
							return AlignDouble(p_structEOBRecord.TotalAmountAllowed,iWidth,iDataWidth,sAlign);
						case TOTAL_DISCOUNT_AMOUNT:
							return AlignDouble(p_structEOBRecord.TotalDiscountAmount,iWidth,iDataWidth,sAlign);
						case TOTAL_PER_DIEM_AMT:
							return AlignDouble(p_structEOBRecord.TotalPerDiemAmt,iWidth,iDataWidth,sAlign);
						case TOTAL_STOP_LOSS_AMT:
							return AlignDouble(p_structEOBRecord.TotalStopLossAmt,iWidth,iDataWidth,sAlign);
						case TOTAL_FEE_TABLE_AMT:
							return AlignDouble(p_structEOBRecord.TotalFeeTableAmt,iWidth,iDataWidth,sAlign);
						default:
							return p_sToken;
					}
				}
				else
				{
					sTokenUpper = sTokenUpper.Substring(1,sTokenUpper.Length - 2); //remove { & } from token
					
					switch (sTokenUpper)
					{
						case PAYEE_LAST_NAME:
							return p_structEOBRecord.PayeeLastName;
						case PAYEE_FIRST_NAME:
							return p_structEOBRecord.PayeeFirstName;  
						case PAYEE_FULL_NAME:
							return p_structEOBRecord.PayeeFullName;
						case PAYEE_ADDR1:
							return p_structEOBRecord.PayeeAddr1;
						case PAYEE_ADDR2:
							return p_structEOBRecord.PayeeAddr2;
                        case PAYEE_ADDR3:
                            return p_structEOBRecord.PayeeAddr3;
                        case PAYEE_ADDR4:
                            return p_structEOBRecord.PayeeAddr4;
						case PAYEE_CITY:
							return p_structEOBRecord.PayeeCity;
						case PAYEE_STATE:
							return p_structEOBRecord.PayeeState;
                        case PAYEE_STATE_CODE:
                            if(!string.IsNullOrEmpty(p_structEOBRecord.PayeeState))
                                return p_structEOBRecord.PayeeState.Substring(0,p_structEOBRecord.PayeeState.IndexOf(" "));
                            else
                                return string.Empty;
                        case PAYEE_STATE_DESC:
                            if (!string.IsNullOrEmpty(p_structEOBRecord.PayeeState))
                                return p_structEOBRecord.PayeeState.Substring(p_structEOBRecord.PayeeState.IndexOf(" ")+1);
                            else
                                return string.Empty;
						case PAYEE_POSTAL_CODE:
							return p_structEOBRecord.PayeePostalCode;
						case CLAIM_NUMBER:
							return p_structEOBRecord.ClaimNumber;
						case CLAIMANT_LAST_NAME:
							return p_structEOBRecord.ClaimantLastName;
						case CLAIMANT_FIRST_NAME:
							return p_structEOBRecord.ClaimantFirstName;
						case CLAIMANT_FULL_NAME:
							return p_structEOBRecord.ClaimantFullName;
						case CLAIMANT_ADDR1:
							return p_structEOBRecord.ClaimantAddr1;
						case CLAIMANT_ADDR2:
							return p_structEOBRecord.ClaimantAddr2;
                        case CLAIMANT_ADDR3:
                            return p_structEOBRecord.ClaimantAddr3;
                        case CLAIMANT_ADDR4:
                            return p_structEOBRecord.ClaimantAddr4;
						case CLAIMANT_CITY:
							return p_structEOBRecord.ClaimantCity;
						case CLAIMANT_STATE:
							return p_structEOBRecord.ClaimantState;
						case CLAIMANT_POSTAL_CODE:
							return p_structEOBRecord.ClaimantPostalCode;
						case SYS_USER:
							return m_sUserName;
                       //nadim for 14513
                        case SYS_USER_LAST_NAME:
                            return GetUserLastNameFromLoginName(m_sUserName, m_sDsnId);
                        case SYS_USER_FIRST_NAME:
                            return GetUserFirstNameFromLoginName(m_sUserName, m_sDsnId);
                            //nadim for 14513
						case SYS_DATE:
							//Mukul Changed 4/10/07 MITS 9160
							return string.Format("{0:MMMM dd, yyyy}",DateTime.Now);
						case CLAIMANT_SSN:
							return sPrivacyMask(p_structEOBRecord.ClaimantSSN); 
						case EVENT_DATE:
                            return Conversion.GetDBDateFormat(p_structEOBRecord.EventDate, "MM/dd/yyyy");
						case DATE_REPORTED:
                            return Conversion.GetDBDateFormat(p_structEOBRecord.DateReported, "MM/dd/yyyy");
                            //return p_structEOBRecord.DateReported;
						case RPTD_FULL_NAME:
							return p_structEOBRecord.RptdFullName;
						case TRANS_DATE:
							return p_structEOBRecord.TransDate;
						case PAYEE_TAXID:
							return sPrivacyMask(p_structEOBRecord.PayeeTaxID);
						case EMP_DEPT_CODE:
							return p_structEOBRecord.EmpDeptCode;
						case EMP_DEPARTMENT:
							return p_structEOBRecord.EmpDepartment;
						case EMP_FACILITY:
							return p_structEOBRecord.EmpFacility;
						case EMP_LOCATION:
							return p_structEOBRecord.EmpLocation;
						case EMP_DIVISION:
							return p_structEOBRecord.EmpDivision;
						case EMP_REGION:
							return p_structEOBRecord.EmpRegion;
						case EMP_OPERATION:
							return p_structEOBRecord.EmpOperation;
						case EMP_COMPANY:
							return p_structEOBRecord.EmpCompany;
                            //skhare7
                        //skhare7
                        case EMP_ADDRESS1:
                            return p_structEOBRecord.EMP_ADDRESS1;
                        case EMP_CLIENT_ADDR1:
                            return p_structEOBRecord.EMP_CLIENT_ADDR1;
                        case EMP_CLIENT_ADDR2:
                            return p_structEOBRecord.EMP_CLIENT_ADDR2;
                        case EMP_CLIENT_ADDR3:
                            return p_structEOBRecord.EMP_CLIENT_ADDR3;
                        case EMP_CLIENT_ADDR4:
                            return p_structEOBRecord.EMP_CLIENT_ADDR4;
                        case EMP_COMP_ADDR1:
                            return p_structEOBRecord.EMP_COMP_ADDR1;
                        case EMP_COMP_ADDR2:
                            return p_structEOBRecord.EMP_COMP_ADDR2;
                        case EMP_COMP_ADDR3:
                            return p_structEOBRecord.EMP_COMP_ADDR3;
                        case EMP_COMP_ADDR4:
                            return p_structEOBRecord.EMP_COMP_ADDR4;
                        case EMP_DEPT_ADDR1:
                            return p_structEOBRecord.EMP_DEPT_ADDR1;
                        case EMP_DEPT_ADDR2:
                            return p_structEOBRecord.EMP_DEPT_ADDR2;
                        case EMP_DEPT_ADDR3:
                            return p_structEOBRecord.EMP_DEPT_ADDR3;
                        case EMP_DEPT_ADDR4:
                            return p_structEOBRecord.EMP_DEPT_ADDR4;
                        case EMP_DIV_ADDR1:
                            return p_structEOBRecord.EMP_DIV_ADDR1;
                        case EMP_DIV_ADDR2:
                            return p_structEOBRecord.EMP_DIV_ADDR2;
                        case EMP_DIV_ADDR3:
                            return p_structEOBRecord.EMP_DIV_ADDR3;
                        case EMP_DIV_ADDR4:
                            return p_structEOBRecord.EMP_DIV_ADDR4;
                        //Ashish Ahuja Mits 33681 : Start
                        case EMP_DIV_CITY:
                            return p_structEOBRecord.EMP_DIV_CITY;
                        case EMP_DIV_STATE:
                            return p_structEOBRecord.EMP_DIV_STATE;
                        case EMP_DIV_POSTAL_CODE:
                            return p_structEOBRecord.EMP_DIV_POSTAL_CODE;
                        //Ashish Ahuja Mits 33681 : End
                        case EMP_FAC_ADDR1:
                            return p_structEOBRecord.EMP_FAC_ADDR1;
                        case EMP_FAC_ADDR2:
                            return p_structEOBRecord.EMP_FAC_ADDR2;
                        case EMP_FAC_ADDR3:
                            return p_structEOBRecord.EMP_FAC_ADDR3;
                        case EMP_FAC_ADDR4:
                            return p_structEOBRecord.EMP_FAC_ADDR4;
                        case EMP_LOC_ADDR1:
                            return p_structEOBRecord.EMP_LOC_ADDR1;
                        case EMP_LOC_ADDR2:
                            return p_structEOBRecord.EMP_LOC_ADDR2;
                        case EMP_LOC_ADDR3:
                            return p_structEOBRecord.EMP_LOC_ADDR3;
                        case EMP_LOC_ADDR4:
                            return p_structEOBRecord.EMP_LOC_ADDR4; 
                        case EMP_OP_ADDR1:
                            return p_structEOBRecord.EMP_OP_ADDR1;
                        case EMP_OP_ADDR2:
                            return p_structEOBRecord.EMP_OP_ADDR2;
                        case EMP_OP_ADDR3:
                            return p_structEOBRecord.EMP_OP_ADDR3;
                        case EMP_OP_ADDR4:
                            return p_structEOBRecord.EMP_OP_ADDR4;
                        case EMP_REG_ADDR1:
                            return p_structEOBRecord.EMP_REG_ADDR1;
                        case EMP_REG_ADDR2:
                            return p_structEOBRecord.EMP_REG_ADDR2;
                        case EMP_REG_ADDR3:
                            return p_structEOBRecord.EMP_REG_ADDR3;
                        case EMP_REG_ADDR4:
                            return p_structEOBRecord.EMP_REG_ADDR4;
                        case EMP_CERT_NUM:
                            return p_structEOBRecord.EMP_CERT_NUM;
                        //skhare7
						case EMP_CLIENT:
							return p_structEOBRecord.EmpClient;
						case ADJUSTER_ABBREV:
							return p_structEOBRecord.AdjusterAbbrev;
						case ADJUSTER_FULL_NAME:
							return p_structEOBRecord.AdjusterFullName;
						case ADJUSTER_LAST_NAME:
							return p_structEOBRecord.AdjusterLastName;
						case CHECK_NUMBER:
							//return p_objFunds.TransNumber.ToString();
                            return p_structEOBRecord.CheckNumber;
						case CHECK_MEMO:
							return p_objFunds.CheckMemo;
						case CHECK_DATE:
                            //zmohammad JIRA 6804/MITs 37623 : EOB wrong date issue.
                            if (p_objFunds.StatusCode == 1054)
                            {
                            return Conversion.GetDBDateFormat(p_objFunds.DateOfCheck, "MM/dd/yyyy");
                            }
                            else
                            {
                                return Conversion.GetDBDateFormat(Conversion.GetDate(System.DateTime.Today.Date.ToString("d")), "MM/dd/yyyy");
                            }
							//return p_objFunds.DateOfCheck;
						case CTL_NUMBER:
							return p_objFunds.CtlNumber;
						case FUNDS_SUPP:
							return p_objFunds.Supplementals.ToString();
						case ORIG_INVOICE_DATE:
							return string.Format("{0:MMMM dd, YYYY}",p_structEOBRecord.OrigInvDate);
						case ORIG_INVOICE_USER:
                            //Ashish Ahuja Mits 33681
							//return GetUserNameFromLoginName(m_sUserName, m_sDsnId);
                            return p_structEOBRecord.OrigInvUser;
                            
                        case LICENSE_NUMBER_FIRST_RECORD:
                            return p_structEOBRecord.FirstLicense;
                        case INVOICE_NUMBER_FIRST_RECORD:
                            return p_structEOBRecord.FirstInvoice;
                        case INVOICE_DATE_FIRST_RECORD:           //Debabrata Biswas MITS Issue 17183
                            return string.Format("{0:MMMM dd, YYYY}", p_structEOBRecord.FirstInvoiceDate);
						case BILLING_POSTAL_CODE_FIRST_RECORD:
							return p_structEOBRecord.FirstBillingPostalCode;
                        default:
                            if (sTokenUpper.StartsWith("FUNDS_SUPP."))
                            {
                                string sSuppFieldName = sTokenUpper.Substring(11);
                                SupplementalField oSuppField = p_objFunds.Supplementals[sSuppFieldName];
                                if (oSuppField != null)
                                {
                                    string sFieldValue = string.Empty;
                                    string sMultiFieldValues = string.Empty;//Debabrata Biswas MITS Issue 17183
                                    if (oSuppField.Value != null)
                                    {
                                        sFieldValue = oSuppField.Value.ToString();
                                    }
                                    //Debabrata Biswas MITS Issue 17183
                                    //this handles multi codes
                                    else if (oSuppField.Values != null)
                                    {
                                        sMultiFieldValues = oSuppField.Values.ToString();
                                    }
                                    else
                                    {
                                        return sFieldValue;
                                    }
                                    switch (oSuppField.FieldType)
                                    {
                                        case SupplementalFieldTypes.SuppTypeCode:
                                            string sCodeDesc = string.Empty;
                                            using (LocalCache oCache = new LocalCache(m_ConnectionString,m_iClientId))
                                            {
                                                int iCodeId = 0;
                                                bool bParse = int.TryParse(sFieldValue, out iCodeId);
                                                if (bParse)
                                                {
                                                    sCodeDesc = oCache.GetCodeDesc(iCodeId);
                                                }
                                            }

                                            return sCodeDesc;
                                        case SupplementalFieldTypes.SuppTypeDate:
                                            //Ashish Ahuja : Mits 33681
                                            //return string.Format("{0:MMMM dd, YYYY}", sFieldValue);
                                            return Conversion.GetDBDateFormat(sFieldValue, "MM/dd/yyyy");
                                        case SupplementalFieldTypes.SuppTypeMultiCode://Debabrata Biswas MITS Issue 17183
                                            string sMultiCodeDesc = string.Empty;
                                            using (LocalCache oMultiCodeCache = new LocalCache(m_ConnectionString,m_iClientId))
                                            {
                                                string [] sMultiCodeIds = sMultiFieldValues.Split(' ');
                                                int iCodeId = 0;
                                                for (int cntr = 0; cntr < sMultiCodeIds.Length;cntr++)
                                                {
                                                    bool bParse = int.TryParse(sMultiCodeIds[cntr].ToString(), out iCodeId);
                                                    if (bParse)
                                                    {
                                                        if(sMultiCodeDesc=="")
                                                            sMultiCodeDesc = oMultiCodeCache.GetCodeDesc(iCodeId);
                                                        else
                                                            sMultiCodeDesc = sMultiCodeDesc + "\r\n" + oMultiCodeCache.GetCodeDesc(iCodeId);
                                                    }
                                                }
                                            }

                                            return sMultiCodeDesc;

                                        case SupplementalFieldTypes.SuppTypeGrid:
                                            string sGridValues = string.Empty;
                                            XmlDocument xGridXML;
                                            XmlNodeList xGridROWS;
                                            XmlNodeList xGridCELLS;
                                            if (oSuppField.gridXML != null)
                                                xGridXML = oSuppField.gridXML;
                                            else
                                                return sGridValues;

                                            xGridROWS = xGridXML.SelectNodes("//row");
                                            if (xGridROWS != null)
                                            {
                                                foreach (XmlElement xmlRow in xGridROWS)
                                                {
                                                    xGridCELLS=xmlRow.SelectNodes("cell");
                                                    if (xGridCELLS !=null)
                                                    {
                                                        foreach (XmlElement xmlCell in xGridCELLS)
                                                        {
                                                            if (sGridValues=="")
                                                                sGridValues = xmlCell.InnerText;
                                                            else
                                                                sGridValues = sGridValues + "|" + xmlCell.InnerText;
                                                        }
                                                        
                                                        //string sp= p_structEOBRecord.CurrentX1Margin.ToString();
                                                        string s = FillSpace(17);
                                                        sGridValues += "\r\n" + s;
                                                    }
                                                }
                                            }


                                            return sGridValues;
                                        default:
                                            return sFieldValue;
                                    }
                                }
                                //Ashish Ahuja Mits 33618
                                else
                                {
                                    return "";
                                }
                            }
							return p_sToken; 
					}
				}
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.ProcessToken.Error", m_iClientId), p_objEx);//sharishkumar Jira 835				
			}
		}
		#endregion

        #region "ProcessTokenHTML(string p_sToken,EOBRecord p_structEOBRecord,Funds p_objFunds)"
        /// Name		: ProcessHTMLToken
        /// Author		: Debabrata Biswas MITS Issue 17183
        /// Date Created	: 21 Jul 2009		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// To retrieve EOB HTML token
        /// </summary>
        /// <param name="p_sToken">EOB Tag</param>
        /// <param name="p_structEOBRecord">EOB record structure</param>
        /// <param name="p_objFunds">Funds object</param>
        /// <returns>Token</returns>
        internal string ProcessHTMLToken(string p_sToken, int iDataLength, EOBRecord p_structEOBRecord, EOBDetailRecord p_structEOBDetailRecord, Funds p_objFunds)
        {
            string sTokenUpper = string.Empty;
            string sFieldName = string.Empty;
            string sAlign = string.Empty;

            try
            {
                if (p_sToken.StartsWith("{") && p_sToken.EndsWith("}"))
                {
                    sTokenUpper = p_sToken.ToUpper().Trim();
                    sTokenUpper = sTokenUpper.Substring(1, sTokenUpper.Length - 2); //remove "{" & "}" from token

                    if(sTokenUpper.StartsWith("FUNDS_SUPP."))
                    {
                        string sSuppFieldName = sTokenUpper.Substring(11);
                        SupplementalField objFundsSupp = p_objFunds.Supplementals[sSuppFieldName];

                        if (objFundsSupp != null)
                        {
                            switch (objFundsSupp.FieldType)
                            {
                                case SupplementalFieldTypes.SuppTypeText:
                                    if (iDataLength != 0)
                                        return Conversion.ConvertObjToStr(objFundsSupp.Value).Substring(0, iDataLength);
                                    else
                                        return Conversion.ConvertObjToStr(objFundsSupp.Value);
                                case SupplementalFieldTypes.SuppTypeFreeText:
                                    if (iDataLength != 0)
                                        return Conversion.ConvertObjToStr(objFundsSupp.Value).Substring(0, iDataLength);
                                    else
                                        return Conversion.ConvertObjToStr(objFundsSupp.Value);
                                case SupplementalFieldTypes.SuppTypeMemo:
                                    if (iDataLength != 0)
                                        return Conversion.ConvertObjToStr(objFundsSupp.Value).Substring(0, iDataLength);
                                    else
                                        return Conversion.ConvertObjToStr(objFundsSupp.Value);
                                case SupplementalFieldTypes.SuppTypeNumber:
                                    if (iDataLength != 0)
                                        return Conversion.ConvertObjToStr(objFundsSupp.Value).Substring(0, iDataLength);
                                    else
                                        return Conversion.ConvertObjToStr(objFundsSupp.Value);
                                case SupplementalFieldTypes.SuppTypeCurrency:
                                    if (iDataLength != 0)
                                        return string.Format("{0:#,###,##0.00}", objFundsSupp.Value).Substring(0, iDataLength);
                                        //return Conversion.ConvertObjToStr(objFundsSupp.Value).Substring(0, iDataLength);
                                    else
                                        return string.Format("{0:#,###,##0.00}", objFundsSupp.Value);
                                        //return Conversion.ConvertObjToStr(objFundsSupp.Value);
                                case SupplementalFieldTypes.SuppTypeDate:
                                    if (iDataLength != 0)
                                        return string.Format("{0:MMMM dd, YYYY}", Conversion.ConvertObjToStr(objFundsSupp.Value)).Substring(0, iDataLength);
                                    else
                                        return string.Format("{0:MMMM dd, YYYY}", Conversion.ConvertObjToStr(objFundsSupp.Value));
                                case SupplementalFieldTypes.SuppTypeCode:
                                    using (LocalCache objCache = new LocalCache(m_ConnectionString, m_iClientId))
                                    {
                                        int icodeId;
                                        bool bParse;
                                        bParse = int.TryParse(Conversion.ConvertObjToStr(objFundsSupp.Value), out icodeId);
                                        if (bParse)
                                            if (iDataLength != 0)
                                                return objCache.GetCodeDesc(icodeId).Substring(0, iDataLength);
                                            else
                                                return objCache.GetCodeDesc(icodeId);
                                        else
                                            return "";
                                    }
                                case SupplementalFieldTypes.SuppTypeMultiCode:
                                    string sMultiCodeDesc = string.Empty;
                                    string[] sMultiCodeIds = Conversion.ConvertObjToStr(objFundsSupp.Values).Split(' ');
                                    using (LocalCache objCache = new LocalCache(m_ConnectionString, m_iClientId))
                                    {
                                        foreach (string sCodeId in sMultiCodeIds)
                                        {
                                            int icodeId;
                                            bool bParse;

                                            bParse = int.TryParse(sCodeId, out icodeId);
                                            if (bParse)
                                            {
                                                if (sMultiCodeDesc == string.Empty)
                                                    if (iDataLength != 0)
                                                        sMultiCodeDesc = objCache.GetCodeDesc(icodeId).Substring(0, iDataLength);
                                                    else
                                                        sMultiCodeDesc = objCache.GetCodeDesc(icodeId);
                                                else
                                                    if (iDataLength != 0)
                                                        sMultiCodeDesc = sMultiCodeDesc + "<BR />" + objCache.GetCodeDesc(icodeId).Substring(0, iDataLength);
                                                    else
                                                        sMultiCodeDesc = sMultiCodeDesc + "<BR />" + objCache.GetCodeDesc(icodeId);
                                            }
                                        }
                                    }
                                    return sMultiCodeDesc;
                                case SupplementalFieldTypes.SuppTypeGrid:
                                    if (objFundsSupp.gridXML != null)
                                    {
                                        XmlNodeList xGridROWS;
                                        XmlNodeList xGridCELLS;
                                        string sGridData = string.Empty;
                                        string sGridRow = string.Empty;
                                        string sGridTable = string.Empty;

                                        xGridROWS = objFundsSupp.gridXML.SelectNodes("//row");
                                        if (xGridROWS != null)
                                        {
                                            foreach (XmlNode xRow in xGridROWS)
                                            {
                                                xGridCELLS = xRow.SelectNodes("cell");
                                                if (xGridCELLS != null)
                                                {
                                                    sGridData = string.Empty ;
                                                    foreach (XmlNode xCell in xGridCELLS)
                                                    {
                                                        if (sGridData == string.Empty)
                                                            sGridData = "<TD>" + xCell.InnerText + "</TD>";
                                                        else
                                                            sGridData = sGridData + "<TD>" + xCell.InnerText + "</TD>";
                                                    }
                                                    sGridRow = sGridRow + "<TR>" + sGridData + "</TR>";
                                                }
                                            }
                                            sGridTable = "<TABLE WIDTH=\"100%\">" + sGridRow + "</TABLE>";
                                            return sGridTable;
                                        }
                                        else
                                        {
                                            return "";
                                        }
                                    }
                                    else
                                    {
                                        return "";
                                    }
                                default:
                                    return "";
                            }
                        }
                        else
                        {
                            return sTokenUpper;
                        }


                    }
                    else
                    {
                        switch (sTokenUpper)
                        {
                            case TOTAL_AMOUNT_PAID:
                                return string.Format("{0:#,###,##0.00}", p_structEOBRecord.TotalAmountPaid);
                                //return p_structEOBRecord.TotalAmountPaid.ToString();
                            case TOTAL_AMOUNT_BILLED:
                                return string.Format("{0:#,###,##0.00}", p_structEOBRecord.TotalAmountBilled);
                                //return p_structEOBRecord.TotalAmountBilled.ToString();
                            case TOTAL_AMOUNT_SAVED:
                                return string.Format("{0:#,###,##0.00}", p_structEOBRecord.TotalAmountSaved);
                                //return p_structEOBRecord.TotalAmountSaved.ToString();
                            case TOTAL_AMOUNT_REDUCED:
                                return string.Format("{0:#,###,##0.00}", p_structEOBRecord.TotalAmountReduced);
                                //return p_structEOBRecord.TotalAmountReduced.ToString();
                            case TOTAL_SCHEDULED_AMOUNT:
                                return string.Format("{0:#,###,##0.00}", p_structEOBRecord.TotalScheduledAmount);
                                //return p_structEOBRecord.TotalScheduledAmount.ToString();
                            case TOTAL_BASE_AMOUNT:
                                return string.Format("{0:#,###,##0.00}", p_structEOBRecord.TotalBaseAmount);
                                //return p_structEOBRecord.TotalBaseAmount.ToString();
                            case TOTAL_AMOUNT_ALLOWED:
                                return string.Format("{0:#,###,##0.00}", p_structEOBRecord.TotalAmountAllowed);
                                //return p_structEOBRecord.TotalAmountAllowed.ToString();
                            case TOTAL_DISCOUNT_AMOUNT:
                                return string.Format("{0:#,###,##0.00}", p_structEOBRecord.TotalDiscountAmount);
                                //return p_structEOBRecord.TotalDiscountAmount.ToString();
                            case TOTAL_PER_DIEM_AMT:
                                return string.Format("{0:#,###,##0.00}", p_structEOBRecord.TotalPerDiemAmt);
                                //return p_structEOBRecord.TotalPerDiemAmt.ToString();
                            case TOTAL_STOP_LOSS_AMT:
                                return string.Format("{0:#,###,##0.00}", p_structEOBRecord.TotalStopLossAmt);
                                //return p_structEOBRecord.TotalStopLossAmt.ToString();
                            case TOTAL_FEE_TABLE_AMT:
                                return string.Format("{0:#,###,##0.00}", p_structEOBRecord.TotalFeeTableAmt);
                                //return p_structEOBRecord.TotalFeeTableAmt.ToString();
                            case PAYEE_LAST_NAME:
                                return p_structEOBRecord.PayeeLastName;
                            case PAYEE_FIRST_NAME:
                                return p_structEOBRecord.PayeeFirstName;
                            case PAYEE_FULL_NAME:
                                return p_structEOBRecord.PayeeFullName;
                            case PAYEE_ADDR1:
                                return p_structEOBRecord.PayeeAddr1;
                            case PAYEE_ADDR2:
                                return p_structEOBRecord.PayeeAddr2;
                            case PAYEE_ADDR3:
                                return p_structEOBRecord.PayeeAddr3;
                            case PAYEE_ADDR4:
                                return p_structEOBRecord.PayeeAddr4;
                            case PAYEE_CITY:
                                return p_structEOBRecord.PayeeCity;
                            case PAYEE_STATE:
                                return p_structEOBRecord.PayeeState;
                            case PAYEE_STATE_CODE:
                                return p_structEOBRecord.PayeeState.Substring(0, p_structEOBRecord.PayeeState.IndexOf(" "));
                            case PAYEE_STATE_DESC:
                                return p_structEOBRecord.PayeeState.Substring(p_structEOBRecord.PayeeState.IndexOf(" ") + 1);
                            case PAYEE_POSTAL_CODE:
                                return p_structEOBRecord.PayeePostalCode;
                            case CLAIM_NUMBER:
                                return p_structEOBRecord.ClaimNumber;
                            case CLAIMANT_LAST_NAME:
                                return p_structEOBRecord.ClaimantLastName;
                            case CLAIMANT_FIRST_NAME:
                                return p_structEOBRecord.ClaimantFirstName;
                            case CLAIMANT_FULL_NAME:
                                return p_structEOBRecord.ClaimantFullName;
                            case CLAIMANT_ADDR1:
                                return p_structEOBRecord.ClaimantAddr1;
                            case CLAIMANT_ADDR2:
                                return p_structEOBRecord.ClaimantAddr2;
                            case CLAIMANT_ADDR3:
                                return p_structEOBRecord.ClaimantAddr3;
                            case CLAIMANT_ADDR4:
                                return p_structEOBRecord.ClaimantAddr4;
                            case CLAIMANT_CITY:
                                return p_structEOBRecord.ClaimantCity;
                            case CLAIMANT_STATE:
                                return p_structEOBRecord.ClaimantState;
                            case CLAIMANT_POSTAL_CODE:
                                return p_structEOBRecord.ClaimantPostalCode;
                            case SYS_USER:
                                return m_sUserName;
                            case SYS_USER_LAST_NAME:
                                return GetUserLastNameFromLoginName(m_sUserName, m_sDsnId);
                            case SYS_USER_FIRST_NAME:
                                return GetUserFirstNameFromLoginName(m_sUserName, m_sDsnId);
                            case SYS_DATE:
                                return string.Format("{0:MMMM dd, yyyy}", DateTime.Now);
                            case CLAIMANT_SSN:
                                return sPrivacyMask(p_structEOBRecord.ClaimantSSN);
                            case EVENT_DATE:
                                return Conversion.GetDBDateFormat(p_structEOBRecord.EventDate, "MM/dd/yyyy");
                            case DATE_REPORTED:
                                return Conversion.GetDBDateFormat(p_structEOBRecord.DateReported, "MM/dd/yyyy");
                                //return string.Format("{0:MM/DD/YYYY}", p_structEOBRecord.DateReported);
                            case RPTD_FULL_NAME:
                                return p_structEOBRecord.RptdFullName;
                            case TRANS_DATE:
                                return p_structEOBRecord.TransDate;
                            case PAYEE_TAXID:
                                return sPrivacyMask(p_structEOBRecord.PayeeTaxID);
                            case EMP_DEPT_CODE:
                                return p_structEOBRecord.EmpDeptCode;
                            case EMP_DEPARTMENT:
                                return p_structEOBRecord.EmpDepartment;
                            case EMP_FACILITY:
                                return p_structEOBRecord.EmpFacility;
                            case EMP_LOCATION:
                                return p_structEOBRecord.EmpLocation;
                            case EMP_DIVISION:
                                return p_structEOBRecord.EmpDivision;
                            case EMP_REGION:
                                return p_structEOBRecord.EmpRegion;
                            case EMP_OPERATION:
                                return p_structEOBRecord.EmpOperation;
                            case EMP_COMPANY:
                                return p_structEOBRecord.EmpCompany;
                                //skhare7
                            case EMP_ADDRESS1:
                                return p_structEOBRecord.EMP_ADDRESS1;
                            case EMP_CLIENT_ADDR1:
                                return p_structEOBRecord.EMP_CLIENT_ADDR1;
                            case EMP_CLIENT_ADDR2:
                                return p_structEOBRecord.EMP_CLIENT_ADDR2;
                            case EMP_CLIENT_ADDR3:
                                return p_structEOBRecord.EMP_CLIENT_ADDR3;
                            case EMP_CLIENT_ADDR4:
                                return p_structEOBRecord.EMP_CLIENT_ADDR4;
                            case EMP_COMP_ADDR1:
                                return p_structEOBRecord.EMP_COMP_ADDR1;
                            case EMP_COMP_ADDR2:
                                return p_structEOBRecord.EMP_COMP_ADDR2;
                            case EMP_COMP_ADDR3:
                                return p_structEOBRecord.EMP_COMP_ADDR3;
                            case EMP_COMP_ADDR4:
                                return p_structEOBRecord.EMP_COMP_ADDR4;
                            case EMP_DEPT_ADDR1:
                                return p_structEOBRecord.EMP_DEPT_ADDR1;
                            case EMP_DEPT_ADDR2:
                                return p_structEOBRecord.EMP_DEPT_ADDR2;
                            case EMP_DEPT_ADDR3:
                                return p_structEOBRecord.EMP_DEPT_ADDR3;
                            case EMP_DEPT_ADDR4:
                                return p_structEOBRecord.EMP_DEPT_ADDR4;
                            case EMP_DIV_ADDR1:
                                return p_structEOBRecord.EMP_DIV_ADDR1;
                            case EMP_DIV_ADDR2:
                                return p_structEOBRecord.EMP_DIV_ADDR2;
                            case EMP_DIV_ADDR3:
                                return p_structEOBRecord.EMP_DIV_ADDR3;
                            case EMP_DIV_ADDR4:
                                return p_structEOBRecord.EMP_DIV_ADDR4;
                            case EMP_FAC_ADDR1:
                                return p_structEOBRecord.EMP_FAC_ADDR1;
                            case EMP_FAC_ADDR2:
                                return p_structEOBRecord.EMP_FAC_ADDR2;
                            case EMP_FAC_ADDR3:
                                return p_structEOBRecord.EMP_FAC_ADDR3;
                            case EMP_FAC_ADDR4:
                                return p_structEOBRecord.EMP_FAC_ADDR4;
                            case EMP_LOC_ADDR1:
                                return p_structEOBRecord.EMP_LOC_ADDR1;
                            case EMP_LOC_ADDR2:
                                return p_structEOBRecord.EMP_LOC_ADDR2;
                            case EMP_LOC_ADDR3:
                                return p_structEOBRecord.EMP_LOC_ADDR3;
                            case EMP_LOC_ADDR4:
                                return p_structEOBRecord.EMP_LOC_ADDR4;
                            case EMP_OP_ADDR1:
                                return p_structEOBRecord.EMP_OP_ADDR1;
                            case EMP_OP_ADDR2:
                                return p_structEOBRecord.EMP_OP_ADDR2;
                            case EMP_OP_ADDR3:
                                return p_structEOBRecord.EMP_OP_ADDR3;
                            case EMP_OP_ADDR4:
                                return p_structEOBRecord.EMP_OP_ADDR4;
                            case EMP_REG_ADDR1:
                                return p_structEOBRecord.EMP_REG_ADDR1;
                            case EMP_REG_ADDR2:
                                return p_structEOBRecord.EMP_REG_ADDR2;
                            case EMP_REG_ADDR3:
                                return p_structEOBRecord.EMP_REG_ADDR3;
                            case EMP_REG_ADDR4:
                                return p_structEOBRecord.EMP_REG_ADDR4;
                            case EMP_CERT_NUM:
                                return p_structEOBRecord.EMP_CERT_NUM;
                          //skhare7
                            case EMP_CLIENT:
                                return p_structEOBRecord.EmpClient;
                            case ADJUSTER_ABBREV:
                                return p_structEOBRecord.AdjusterAbbrev;
                            case ADJUSTER_FULL_NAME:
                                return p_structEOBRecord.AdjusterFullName;
                            case ADJUSTER_LAST_NAME:
                                return p_structEOBRecord.AdjusterLastName;
                            case CHECK_NUMBER:
                                return p_structEOBRecord.CheckNumber;
                            case CHECK_MEMO:
                                return p_objFunds.CheckMemo;
                            case CHECK_DATE:
                                //zmohammad JIRA 6804/MITs 37623 : EOB wrong date issue.
                                if (p_objFunds.StatusCode == 1054)
                                {
                                return Conversion.GetDBDateFormat(p_objFunds.DateOfCheck, "MM/dd/yyyy");
                                }
                                else
                                {
                                    return Conversion.GetDBDateFormat(Conversion.GetDate(System.DateTime.Today.Date.ToString("d")),"MM/dd/yyyy");
                                }
                                //return p_objFunds.DateOfCheck;
                            case CTL_NUMBER:
                                return p_objFunds.CtlNumber;
                            case FUNDS_SUPP:
                                return p_objFunds.Supplementals.ToString();
                            case ORIG_INVOICE_DATE:
                                return string.Format("{0:MMMM dd, YYYY}", p_structEOBRecord.OrigInvDate);
                            case ORIG_INVOICE_USER:
                                return GetUserNameFromLoginName(m_sUserName, m_sDsnId);
                            case LICENSE_NUMBER_FIRST_RECORD:
                                return p_structEOBRecord.FirstLicense;
                            case INVOICE_NUMBER_FIRST_RECORD:
                                return p_structEOBRecord.FirstInvoice;
                            case INVOICE_DATE_FIRST_RECORD:             //Debabrata Biswas MITS Issue 17183
                                return string.Format("{0:MMMM dd, YYYY}", p_structEOBRecord.FirstInvoiceDate);
                            case DATE_OF_SERVICE:          //Debabrata Biswas MITS Issue 17183
                                return p_structEOBRecord.DateOfService;
                            case BILLING_POSTAL_CODE_FIRST_RECORD:
                                return p_structEOBRecord.FirstBillingPostalCode;
                            default:
                                return sTokenUpper;
                        }
                    }
                }
                return p_sToken;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Functions.ProcessToken.Error", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
        }
        #endregion

		#region "ExtractLogoInfo(string p_sToken,ref EOBRecord p_structEOBRecord)"
		/// Name		: ExtractLogoInfo
		/// Author		: Anurag Agarwal
		/// Date Created	: 27 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// To retrieve Custom EOB header token
		/// </summary>
		/// <param name="p_sToken">EOB Tag</param>
		/// <param name="p_structEOBRecord">EOB Record Structure</param>
		internal void ExtractLogoInfo(string p_sToken,ref EOBRecord p_structEOBRecord)
		{

			string [] aLogoInfo = null;
 
			try
			{
				aLogoInfo = p_sToken.Split(',');
  
				if(aLogoInfo.Length >= 5)
				{
					p_structEOBRecord.LogoPath = aLogoInfo[0]; 
					p_structEOBRecord.LogoX = Convert.ToSingle(aLogoInfo[1]); 
					p_structEOBRecord.LogoY = Convert.ToSingle(aLogoInfo[2]); 
					p_structEOBRecord.LogoWidth = Convert.ToSingle(aLogoInfo[3]); 
					p_structEOBRecord.LogoHeight = Convert.ToSingle(aLogoInfo[4]); 
				}

			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.ExtractLogoInfo.ErrorGet", m_iClientId), p_objEx);		//sharishkumar Jira 835		
			}
		}
		#endregion

		#region "GetUserNameFromLoginName(string p_sLoginname, string p_sDsnId)"
		/// Name		: GetUserNameFromLoginName
		/// Author		: Anurag Agarwal
		/// Date Created	: 27 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// This function gets the user name from loginname.
		///     Issue: If the system has 2 users with the same user name accessing the same
		///            database, this function picks up the user that has least user id among
		///            the two. This is similar to riskmaster login.
		///
		///            eg: First Name---LastName---User ID---DSN ID---Login name
		///                 tg           d          1         1        dtg
		///                 tg           d123       2         1        dtg
		///            If passwords are left untouched, Riskmaster logs in User Id 1 even when
		///            when user id 2 is logging in. This function does the same
		/// </summary>
		/// <param name="p_sLoginname">Login Name</param>
		/// <param name="p_sDsnId">DSN ID</param>
		/// <returns>User Name</returns>
		internal string GetUserNameFromLoginName(string p_sLoginname, string p_sDsnId)
		{
			
			DbReader objRdr = null;
			string sLastName = string.Empty;
 			string sFirstName = string.Empty;
			string sSQL = string.Empty;
 
			try
			{
				sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME " + 
					" FROM USER_DETAILS_TABLE,USER_TABLE WHERE USER_DETAILS_TABLE.DSNID = " + p_sDsnId + 
					" AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID AND " + 
					"USER_DETAILS_TABLE.LOGIN_NAME = '" + p_sLoginname + "' Order by USER_DETAILS_TABLE.USER_ID";
				objRdr=DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId) ,sSQL);//sharishkumar Jira 835
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						//Get it from USER_DETAILS_TABLE
						sLastName = objRdr["LAST_NAME"].ToString();
						sFirstName = objRdr["FIRST_NAME"].ToString();
					}
					objRdr.Close();
				}
                //nadim for 14513
                //return (sFirstName + " " + sLastName).Trim();
                return (sLastName + " " + sFirstName).Trim();
                //return (sLastName + " " + sFirstName).Trim();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.GetUserNameFromLoginName.ErrorGet", m_iClientId), p_objEx);		//sharishkumar Jira 835		
			}
			finally
			{
				if(objRdr != null)
					objRdr.Dispose();
			}
		}
		#endregion
        //Nadim for 14513.-Start
        #region "GetUserLastNameFromLoginName(string p_sLoginname, string p_sDsnId)"
        /// Name		: GetUserLastNameFromLoginName
        /// Author		: Nadim Zafar
        /// Date Created	: 17 march 2009		
        /// ************************************************************
        /// </summary>
        /// <param name="p_sLoginname">Login Name</param>
        /// <param name="p_sDsnId">DSN ID</param>
        /// <returns>User Last Name</returns>
        internal string GetUserLastNameFromLoginName(string p_sLoginname, string p_sDsnId)
        {

            DbReader objRdr = null;
            string sLastName = string.Empty;          
            string sSQL = string.Empty;

            try
            {
                sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME " +
                    " FROM USER_DETAILS_TABLE,USER_TABLE WHERE USER_DETAILS_TABLE.DSNID = " + p_sDsnId +
                    " AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID AND " +
                    "USER_DETAILS_TABLE.LOGIN_NAME = '" + p_sLoginname + "' Order by USER_DETAILS_TABLE.USER_ID";
                objRdr = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), sSQL);//sharishkumar Jira 835
                if (objRdr != null)
                {
                    if (objRdr.Read())
                    {
                        //Get it from USER_DETAILS_TABLE
                        sLastName = objRdr["LAST_NAME"].ToString();                     
                    }
                    objRdr.Close();
                }
                
               
                return (sLastName+" ").Trim();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Functions.GetUserNameFromLoginName.ErrorGet", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objRdr != null)
                    objRdr.Dispose();
            }
        }
        #endregion
       

        #region "GetUserFirstNameFromLoginName(string p_sLoginname, string p_sDsnId)"
        /// Name		: GetUserFirstNameFromLoginName
        /// Author		: Nadim Zafar
        /// Date Created	: 17 march 2009		
        /// ************************************************************        
        /// </summary>
        /// <param name="p_sLoginname">Login Name</param>
        /// <param name="p_sDsnId">DSN ID</param>
        /// <returns>User First Name</returns>
        internal string GetUserFirstNameFromLoginName(string p_sLoginname, string p_sDsnId)
        {

            DbReader objRdr = null;
           
            string sFirstName = string.Empty;
            string sSQL = string.Empty;

            try
            {
                sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME " +
                    " FROM USER_DETAILS_TABLE,USER_TABLE WHERE USER_DETAILS_TABLE.DSNID = " + p_sDsnId +
                    " AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID AND " +
                    "USER_DETAILS_TABLE.LOGIN_NAME = '" + p_sLoginname + "' Order by USER_DETAILS_TABLE.USER_ID";
                objRdr = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), sSQL);//sharishkumar Jira 835
                if (objRdr != null)
                {
                    if (objRdr.Read())
                    {
                        //Get it from USER_DETAILS_TABLE
                        
                        sFirstName = objRdr["FIRST_NAME"].ToString();
                    }
                    objRdr.Close();
                }               
                return (sFirstName+" ").Trim();               
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Functions.GetUserNameFromLoginName.ErrorGet", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objRdr != null)
                    objRdr.Dispose();
            }
        }
        #endregion
        //nadim for 14513-end

		#region "GetCurrentToken(StreamReader p_objSReader, char p_cStartSep, char p_cEndSep)"
		/// Name		: GetCurrentToken
		/// Author		: Anurag Agarwal
		/// Date Created	: 27 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Return
		/// </summary>
		/// <param name="p_objSReader">Object of IO Stream class containing the format file stream</param>
		/// <param name="p_cStartSep">start seperator character</param>
		/// <param name="p_cEndSep">end seperator character</param>
		/// <returns>Token</returns>
		internal string GetCurrentToken(StreamReader p_objSReader, char p_cStartSep, char p_cEndSep)
		{
			char [] cCurrentChar = {' '};
			string sCurrentToken = string.Empty;
 
			try
			{
				while (true) 
				{ 
					p_objSReader.Read(cCurrentChar,0,1);
					if(cCurrentChar == null)
						break;

					if(cCurrentChar[0] == p_cEndSep)
						break;

					if(cCurrentChar[0] == p_cStartSep)
					{
						if(sCurrentToken == string.Empty)
							p_objSReader.Read(cCurrentChar,0,1);
						else
							break;
					}

					sCurrentToken += cCurrentChar[0];  
				}

				return sCurrentToken; 

			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.GetCurrentToken.ErrorGet", m_iClientId), p_objEx);	//sharishkumar Jira 835			
			}
		}
		#endregion

        #region "GetCurrentHTMLToken(StreamReader p_objSReader, char p_cStartSep, char p_cEndSep)"
        /// Name		: GetCurrentHTMLToken
        /// Author		: Debabrata Biswaas MITS Issue 17183
        /// Date Created	: 03 Aug 2009		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Return
        /// </summary>
        /// <param name="p_objSReader">Object of IO Stream class containing the format file stream</param>
        /// <param name="p_cStartSep">start seperator character</param>
        /// <param name="p_cEndSep">end seperator character</param>
        /// <returns>Token</returns>
        internal string GetCurrentHTMLToken(StreamReader p_objSReader, char p_cStartSep, char p_cEndSep)
        {
            char[] cCurrentChar = { ' ' };
            string sCurrentToken = string.Empty;
            bool bHtmlTag= false;

            try
            {
                while (true)
                {
                    p_objSReader.Read(cCurrentChar, 0, 1);
                    if (cCurrentChar == null)
                        break;

                    if (cCurrentChar[0] == p_cEndSep)
                    {
                        bHtmlTag = true;
                        break;
                    }
                    if (cCurrentChar[0] == p_cStartSep)
                    {
                        if (sCurrentToken == string.Empty)
                            p_objSReader.Read(cCurrentChar, 0, 1);
                        else
                            break;
                    }

                    sCurrentToken += cCurrentChar[0];
                }

                if (bHtmlTag )
                    return "<" + sCurrentToken + ">";
                else
                    return sCurrentToken;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Functions.GetCurrentToken.ErrorGet", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
        }
        #endregion

		#region "GetColumns(StreamReader p_objSReader,ref EOBDetailRecord p_structEOBDetailRecord)"
		/// Name		: GetColumns
		/// Author		: Anurag Agarwal
		/// Date Created	: 28 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Get Invoice Detail and Fund Trans Split columns
		/// </summary>
		/// <param name="p_objSReader">Object of IO Stream class containing the format file stream</param>
		/// <param name="p_structEOBDetailRecord">Structure containing the EOBDetail Record</param>
		internal void GetColumns(StreamReader p_objSReader,ref EOBDetailRecord p_structEOBDetailRecord)
		{
			char [] cCurrentChar = {' '};
			string sCurrentToken = string.Empty;
			int iNextInput = 0;
			int iWidth = 0;
			int iDataWidth = 0;
			string sAlign = string.Empty; 
			string sFieldName = string.Empty;
			int iColumnCount = 0;
			int iSpaceCount = 0;

			try
			{

				m_objColumnsSelected.Clear();
				m_objSpaceWidth.Clear();

				sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
				while ((iNextInput = p_objSReader.Peek()) != -1) 
				{
					if(sCurrentToken.Length != 0)
					{
						sCurrentToken = sCurrentToken.ToUpper();
						
						if(sCurrentToken == ENDTAG_LINEITEMS)
							break;

						SplitDetailTag(sCurrentToken,ref sFieldName,ref iWidth,ref iDataWidth,ref sAlign);
 
						if(sFieldName.Length != 0)
						{
							
							m_objColumnsSelected.Add(++iColumnCount,sFieldName);  		
							
							switch (sFieldName)
							{
								case INVOICE_DATE:
									p_structEOBDetailRecord.InvoiceDate.Alignment = sAlign;
									p_structEOBDetailRecord.InvoiceDate.Width = iWidth;
									p_structEOBDetailRecord.InvoiceDate.Datawidth = iDataWidth;
									break;
								case INVOICED_BY:
									p_structEOBDetailRecord.InvoicedBy.Alignment = sAlign;
									p_structEOBDetailRecord.InvoicedBy.Width = iWidth;
									p_structEOBDetailRecord.InvoicedBy.Datawidth = iDataWidth;
									break;
								case DISC_ON_FEE_SCHD:
									p_structEOBDetailRecord.DiscOnFeeSchd.Alignment = sAlign;
									p_structEOBDetailRecord.DiscOnFeeSchd.Width = iWidth;
									p_structEOBDetailRecord.DiscOnFeeSchd.Datawidth = iDataWidth;
									break;
								case PRESCRIP_NO:
									p_structEOBDetailRecord.PrescripNo.Alignment = sAlign;
									p_structEOBDetailRecord.PrescripNo.Width = iWidth;
									p_structEOBDetailRecord.PrescripNo.Datawidth = iDataWidth;
									break;
								case DRUG_NAME:
									p_structEOBDetailRecord.DrugName.Alignment = sAlign;
									p_structEOBDetailRecord.DrugName.Width = iWidth;
									p_structEOBDetailRecord.DrugName.Datawidth = iDataWidth;
									break;
								case PRESCRIP_DATE:
									p_structEOBDetailRecord.PrescripDate.Alignment = sAlign;
									p_structEOBDetailRecord.PrescripDate.Width = iWidth;
									p_structEOBDetailRecord.PrescripDate.Datawidth = iDataWidth;
									break;
								case INVOICE_NUMBER:
									p_structEOBDetailRecord.InvoiceNumber.Alignment = sAlign;
									p_structEOBDetailRecord.InvoiceNumber.Width = iWidth;
									p_structEOBDetailRecord.InvoiceNumber.Datawidth = iDataWidth;
									break;
								case PROCEDURE_DATE:
									p_structEOBDetailRecord.ProcedureDate.Alignment = sAlign;
									p_structEOBDetailRecord.ProcedureDate.Width = iWidth;
									p_structEOBDetailRecord.ProcedureDate.Datawidth = iDataWidth;
									break;
								case PROC_TO_DATE:
									p_structEOBDetailRecord.ProcToDate.Alignment = sAlign;
									p_structEOBDetailRecord.ProcToDate.Width = iWidth;
									p_structEOBDetailRecord.ProcToDate.Datawidth = iDataWidth;
									break;
								case PROCEDURE_CODE:
									p_structEOBDetailRecord.ProcedureCode.Alignment = sAlign;
									p_structEOBDetailRecord.ProcedureCode.Width = iWidth;
									p_structEOBDetailRecord.ProcedureCode.Datawidth = iDataWidth;
									break;
								case PROCEDURE:
									p_structEOBDetailRecord.Procedure.Alignment = sAlign;
									p_structEOBDetailRecord.Procedure.Width = iWidth;
									p_structEOBDetailRecord.Procedure.Datawidth = iDataWidth;
									break;
								case FEE_SCHEDULE:
									p_structEOBDetailRecord.FeeSchedule.Alignment = sAlign;
									p_structEOBDetailRecord.FeeSchedule.Width = iWidth;
									p_structEOBDetailRecord.FeeSchedule.Datawidth = iDataWidth;
									break;
								case PERCENTILE:
									p_structEOBDetailRecord.Percentile.Alignment = sAlign;
									p_structEOBDetailRecord.Percentile.Width = iWidth;
									p_structEOBDetailRecord.Percentile.Datawidth = iDataWidth;
									break;
								case PLACE_OF_SERVICE:
									p_structEOBDetailRecord.PlaceOfService.Alignment = sAlign;
									p_structEOBDetailRecord.PlaceOfService.Width = iWidth;
									p_structEOBDetailRecord.PlaceOfService.Datawidth = iDataWidth;
									break;
								case PLACE_OF_SERVICE_CODE:
									p_structEOBDetailRecord.PlaceofServiceCode.Alignment = sAlign;
									p_structEOBDetailRecord.PlaceofServiceCode.Width = iWidth;
									p_structEOBDetailRecord.PlaceofServiceCode.Datawidth = iDataWidth;
									break;
                                case TYPE_OF_SERVICE:
                                    p_structEOBDetailRecord.TypeOfService.Alignment = sAlign;
                                    p_structEOBDetailRecord.TypeOfService.Width = iWidth;
                                    p_structEOBDetailRecord.TypeOfService.Datawidth = iDataWidth;
                                    break;
                                case TYPE_OF_SERVICE_CODE:
                                    p_structEOBDetailRecord.TypeOfServiceCode.Alignment = sAlign;
                                    p_structEOBDetailRecord.TypeOfServiceCode.Width = iWidth;
                                    p_structEOBDetailRecord.TypeOfServiceCode.Datawidth = iDataWidth;
                                    break;
								case QUANTITY:
									p_structEOBDetailRecord.Quantity.Alignment = sAlign;
									p_structEOBDetailRecord.Quantity.Width = iWidth;
									p_structEOBDetailRecord.Quantity.Datawidth = iDataWidth;
									break;
								case QUANTITY_CODE:
									p_structEOBDetailRecord.QuantityCode.Alignment = sAlign;
									p_structEOBDetailRecord.QuantityCode.Width = iWidth;
									p_structEOBDetailRecord.QuantityCode.Datawidth = iDataWidth;
									break;
								case STORE:
									p_structEOBDetailRecord.Store.Alignment = sAlign;
									p_structEOBDetailRecord.Store.Width = iWidth;
									p_structEOBDetailRecord.Store.Datawidth = iDataWidth;
									break;
								case STORE_CODE:
									p_structEOBDetailRecord.StoreCode.Alignment = sAlign;
									p_structEOBDetailRecord.StoreCode.Width = iWidth;
									p_structEOBDetailRecord.StoreCode.Datawidth = iDataWidth;
									break;
                                //BRS FL Merge : Umesh
                                case FL_LICENSE:
                                    p_structEOBDetailRecord.PhysicianLicenseNum.Alignment = sAlign;
                                    p_structEOBDetailRecord.PhysicianLicenseNum.Width = iWidth;
                                    p_structEOBDetailRecord.PhysicianLicenseNum.Datawidth = iDataWidth;
                                    break;
                                case REV_CODE:
                                    p_structEOBDetailRecord.RevenueCode.Alignment = sAlign;
                                    p_structEOBDetailRecord.RevenueCode.Width = iWidth;
                                    p_structEOBDetailRecord.RevenueCode.Datawidth = iDataWidth;
                                    break;
                                //End
								case EXPLANATION:
									p_structEOBDetailRecord.EOBAlign = sAlign;
									p_structEOBDetailRecord.EOBWidth = iWidth;
									p_structEOBDetailRecord.EOBDataWidth = iDataWidth;
									break;
								case DIAGNOSIS:
									p_structEOBDetailRecord.DiagnosisAlign = sAlign;
									p_structEOBDetailRecord.DiagnosisWidth = iWidth;
									p_structEOBDetailRecord.DiagnosisDataWidth = iDataWidth;
									break;
								case MODIFIER:
								case MODIFIER_CODE:
									//Mukul Commented 3/2/2007 MITS 8960
									//if(sFieldName == MODIFIER_CODE) 
									//	p_structEOBDetailRecord.ModifierAlign = ALIGN_LEFT;
									//else
									p_structEOBDetailRecord.ModifierAlign = sAlign;
									p_structEOBDetailRecord.ModifierWidth = iWidth;
									p_structEOBDetailRecord.ModifierDataWidth = iDataWidth; 
									break;
								case UNITS_BILLED:
									p_structEOBDetailRecord.UnitsBilled.Alignment = sAlign;
									p_structEOBDetailRecord.UnitsBilled.Width = iWidth;
									p_structEOBDetailRecord.UnitsBilled.Datawidth = iDataWidth;
									break;
								case AMOUNT_BILLED:
									p_structEOBDetailRecord.AmountBilled.Alignment = sAlign;
									p_structEOBDetailRecord.AmountBilled.Width = iWidth;
									p_structEOBDetailRecord.AmountBilled.Datawidth = iDataWidth;
									break;
								case AMOUNT_PAID:
									p_structEOBDetailRecord.AmountPaid.Alignment = sAlign;
									p_structEOBDetailRecord.AmountPaid.Width = iWidth;
									p_structEOBDetailRecord.AmountPaid.Datawidth = iDataWidth;
									break;
								case SCHEDULED_AMOUNT:
									p_structEOBDetailRecord.ScheduledAmount.Alignment = sAlign;
									p_structEOBDetailRecord.ScheduledAmount.Width = iWidth;
									p_structEOBDetailRecord.ScheduledAmount.Datawidth = iDataWidth;
									break;
								case AMOUNT_REDUCED:
									p_structEOBDetailRecord.AmountReduced.Alignment = sAlign;
									p_structEOBDetailRecord.AmountReduced.Width = iWidth;
									p_structEOBDetailRecord.AmountReduced.Datawidth = iDataWidth;
									break;	
								case AMOUNT_SAVED:
									p_structEOBDetailRecord.AmountSaved.Alignment = sAlign;
									p_structEOBDetailRecord.AmountSaved.Width = iWidth;
									p_structEOBDetailRecord.AmountSaved.Datawidth = iDataWidth;
									break;
								case CONTRACT_AMOUNT:
									p_structEOBDetailRecord.ContractAmount.Alignment = sAlign;
									p_structEOBDetailRecord.ContractAmount.Width = iWidth;
									p_structEOBDetailRecord.ContractAmount.Datawidth = iDataWidth;
									break;
								case PROVIDER_DISCOUNT:
									p_structEOBDetailRecord.Discount.Alignment = sAlign;
									p_structEOBDetailRecord.Discount.Width = iWidth;
									p_structEOBDetailRecord.Discount.Datawidth = iDataWidth;
									break;
								case BILLING_POSTAL_CODE:
									p_structEOBDetailRecord.ProviderZipCode.Alignment = sAlign;
									p_structEOBDetailRecord.ProviderZipCode.Width = iWidth;
									p_structEOBDetailRecord.ProviderZipCode.Datawidth = iDataWidth;
									break;
								case BASE_AMOUNT:
									p_structEOBDetailRecord.BaseAmount.Alignment = sAlign;
									p_structEOBDetailRecord.BaseAmount.Width = iWidth;
									p_structEOBDetailRecord.BaseAmount.Datawidth = iDataWidth;
									break;
								case AMOUNT_ALLOWED:
									p_structEOBDetailRecord.AmountAllowed.Alignment = sAlign;
									p_structEOBDetailRecord.AmountAllowed.Width = iWidth;
									p_structEOBDetailRecord.AmountAllowed.Datawidth = iDataWidth;
									break;
								case DISCOUNT_AMOUNT:
									p_structEOBDetailRecord.DiscountAmount.Alignment = sAlign;
									p_structEOBDetailRecord.DiscountAmount.Width = iWidth;
									p_structEOBDetailRecord.DiscountAmount.Datawidth = iDataWidth;
									break;
								case PER_DIEM_AMOUNT:
									p_structEOBDetailRecord.PerDiemAmount.Alignment = sAlign;
									p_structEOBDetailRecord.PerDiemAmount.Width = iWidth;
									p_structEOBDetailRecord.PerDiemAmount.Datawidth = iDataWidth;
									break;
								case STOP_LOSS_AMOUNT:
									p_structEOBDetailRecord.StopLossAmount.Alignment = sAlign;
									p_structEOBDetailRecord.StopLossAmount.Width = iWidth;
									p_structEOBDetailRecord.StopLossAmount.Datawidth = iDataWidth;
									break;
								case FEE_TABLE_AMOUNT:
									p_structEOBDetailRecord.FeeTableAmount.Alignment = sAlign;
									p_structEOBDetailRecord.FeeTableAmount.Width = iWidth;
									p_structEOBDetailRecord.FeeTableAmount.Datawidth = iDataWidth;
									break;
								case SPACE:
									m_objSpaceWidth.Add(++iSpaceCount,iWidth);  
									break;
							}

						}	//End if(sFieldName.Length != 0)
						
					}	//End if(sCurrentToken.Length != 0) 

					sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
				}	//End while ((iNextInput = p_objSReader.Peek()) != -1) 

				p_structEOBDetailRecord.ColumnsSelected = iColumnCount; 
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.GetColumns.ErrorGet", m_iClientId), p_objEx);	//sharishkumar Jira 835			
			}
		}
		#endregion

		#region "GetCustomEOBBody(EOBDetailRecord p_structEOBDetailRecord, ref EOBRecord p_structEOBRecord, ref PrintWrapper p_objPrintWrapper)"
		/// Name		: GetCustomEOBBody
		/// Author		: Anurag Agarwal
		/// Date Created	: 05 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Format Invoice Detail and Fund Trans Split rows to get the body of EOB reports
		/// </summary>
		/// <param name="p_structEOBDetailRecord">EOB Detail Record</param>
		/// <param name="p_structEOBRecord">EOB Record</param>
		/// <param name="p_objPrintWrapper">object of class use to generate the PDF file</param>
		public void GetCustomEOBBody(EOBDetailRecord p_structEOBDetailRecord, ref EOBRecord p_structEOBRecord, ref PrintWrapper p_objPrintWrapper)
		{
			int iColumnCount = 0;
			string sLineItem = string.Empty;
			string sDetailItem = string.Empty;
			bool bIsFirstEOBSeen  = false;
			bool bIsFirstDiagSeen = false;
			bool bIsFirstModSeen = false;
			int iWidthBeforeEOB = 0;	//Integer Width Before for EOB
			int iWidthBeforeDiag = 0;	//Integer Width Before for Diagnosis
			int iWidthBeforeMod = 0;	//Integer Width Before for Modifiers
			int iSpaceWidth1 = 0;		//Integer Space Width 1
		    int iSpaceWidth2 = 0;		//Integer Space Width 2
			int iSpaceCounter = 0;		//Integer Space Counter
 			int iAddItemsCount = 0;

			try
			{
				for(iColumnCount = 1;iColumnCount <= p_structEOBDetailRecord.ColumnsSelected;iColumnCount++)
				{
					switch (m_objColumnsSelected[iColumnCount].ToString())
					{
						case INVOICE_DATE:
							sLineItem = AlignString(p_structEOBDetailRecord.InvoiceDate.Token,p_structEOBDetailRecord.InvoiceDate.Width,p_structEOBDetailRecord.InvoiceDate.Datawidth,p_structEOBDetailRecord.InvoiceDate.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.InvoiceDate.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.InvoiceDate.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.InvoiceDate.Width;
							break;
						case INVOICED_BY:
							sLineItem = AlignString(p_structEOBDetailRecord.InvoicedBy.Token,p_structEOBDetailRecord.InvoicedBy.Width,p_structEOBDetailRecord.InvoicedBy.Datawidth,p_structEOBDetailRecord.InvoicedBy.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.InvoicedBy.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.InvoicedBy.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.InvoicedBy.Width;
							break;
						case DISC_ON_FEE_SCHD:
							sLineItem = AlignDouble(p_structEOBDetailRecord.DiscOnFeeSchd.Token,p_structEOBDetailRecord.DiscOnFeeSchd.Width,p_structEOBDetailRecord.DiscOnFeeSchd.Datawidth,p_structEOBDetailRecord.DiscOnFeeSchd.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.DiscOnFeeSchd.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.DiscOnFeeSchd.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.DiscOnFeeSchd.Width;
							break;
						case PRESCRIP_NO:
							sLineItem = AlignString(p_structEOBDetailRecord.PrescripNo.Token,p_structEOBDetailRecord.PrescripNo.Width,p_structEOBDetailRecord.PrescripNo.Datawidth,p_structEOBDetailRecord.PrescripNo.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.PrescripNo.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.PrescripNo.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.PrescripNo.Width;
							break;
						case DRUG_NAME:
							sLineItem = AlignString(p_structEOBDetailRecord.DrugName.Token,p_structEOBDetailRecord.DrugName.Width,p_structEOBDetailRecord.DrugName.Datawidth,p_structEOBDetailRecord.DrugName.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.DrugName.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.DrugName.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.DrugName.Width;
							break;
						case PRESCRIP_DATE:
							sLineItem = AlignString(p_structEOBDetailRecord.PrescripDate.Token,p_structEOBDetailRecord.PrescripDate.Width,p_structEOBDetailRecord.PrescripDate.Datawidth,p_structEOBDetailRecord.PrescripDate.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.PrescripDate.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.PrescripDate.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.PrescripDate.Width;
							break;
						case INVOICE_NUMBER:
							sLineItem = AlignString(p_structEOBDetailRecord.InvoiceNumber.Token,p_structEOBDetailRecord.InvoiceNumber.Width,p_structEOBDetailRecord.InvoiceNumber.Datawidth,p_structEOBDetailRecord.InvoiceNumber.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.InvoiceNumber.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.InvoiceNumber.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.InvoiceNumber.Width;
							break;

						case PROCEDURE_DATE:
							sLineItem = AlignString(p_structEOBDetailRecord.ProcedureDate.Token,p_structEOBDetailRecord.ProcedureDate.Width,p_structEOBDetailRecord.ProcedureDate.Datawidth,p_structEOBDetailRecord.ProcedureDate.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.ProcedureDate.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.ProcedureDate.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.ProcedureDate.Width;
							break;

						case PROC_TO_DATE:
							sLineItem = AlignString(p_structEOBDetailRecord.ProcToDate.Token,p_structEOBDetailRecord.ProcToDate.Width,p_structEOBDetailRecord.ProcToDate.Datawidth,p_structEOBDetailRecord.ProcToDate.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.ProcToDate.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.ProcToDate.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.ProcToDate.Width;
							break;

						case PROCEDURE_CODE:
							sLineItem = AlignString(p_structEOBDetailRecord.ProcedureCode.Token,p_structEOBDetailRecord.ProcedureCode.Width,p_structEOBDetailRecord.ProcedureCode.Datawidth,p_structEOBDetailRecord.ProcedureCode.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.ProcedureCode.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.ProcedureCode.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.ProcedureCode.Width;
							break;

						case PROCEDURE:
							sLineItem = AlignString(p_structEOBDetailRecord.Procedure.Token,p_structEOBDetailRecord.Procedure.Width,p_structEOBDetailRecord.Procedure.Datawidth,p_structEOBDetailRecord.Procedure.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.Procedure.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.Procedure.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.Procedure.Width;
							break;

						case FEE_SCHEDULE:
							sLineItem = AlignString(p_structEOBDetailRecord.FeeSchedule.Token,p_structEOBDetailRecord.FeeSchedule.Width,p_structEOBDetailRecord.FeeSchedule.Datawidth,p_structEOBDetailRecord.FeeSchedule.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.FeeSchedule.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.FeeSchedule.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.FeeSchedule.Width;
							break;

						case PERCENTILE:
							sLineItem = AlignString(p_structEOBDetailRecord.Percentile.Token,p_structEOBDetailRecord.Percentile.Width,p_structEOBDetailRecord.Percentile.Datawidth,p_structEOBDetailRecord.Percentile.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.Percentile.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.Percentile.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.Percentile.Width;
							break;

						case PLACE_OF_SERVICE:
							sLineItem = AlignString(p_structEOBDetailRecord.PlaceOfService.Token,p_structEOBDetailRecord.PlaceOfService.Width,p_structEOBDetailRecord.PlaceOfService.Datawidth,p_structEOBDetailRecord.PlaceOfService.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.PlaceOfService.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.PlaceOfService.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.PlaceOfService.Width;
							break;

						case PLACE_OF_SERVICE_CODE:
							sLineItem = AlignString(p_structEOBDetailRecord.PlaceofServiceCode.Token,p_structEOBDetailRecord.PlaceofServiceCode.Width,p_structEOBDetailRecord.PlaceofServiceCode.Datawidth,p_structEOBDetailRecord.PlaceofServiceCode.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.PlaceofServiceCode.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.PlaceofServiceCode.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.PlaceofServiceCode.Width;
							break;

						case TYPE_OF_SERVICE:
							sLineItem = AlignString(p_structEOBDetailRecord.TypeOfService.Token,p_structEOBDetailRecord.TypeOfService.Width,p_structEOBDetailRecord.TypeOfService.Datawidth,p_structEOBDetailRecord.TypeOfService.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.TypeOfService.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.TypeOfService.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.TypeOfService.Width;
							break;

						case TYPE_OF_SERVICE_CODE:
							sLineItem = AlignString(p_structEOBDetailRecord.TypeOfServiceCode.Token,p_structEOBDetailRecord.TypeOfServiceCode.Width,p_structEOBDetailRecord.TypeOfServiceCode.Datawidth,p_structEOBDetailRecord.TypeOfServiceCode.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.TypeOfServiceCode.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.TypeOfServiceCode.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.TypeOfServiceCode.Width;
							break;


                            //BRS FL Merge
                        case FL_LICENSE:
                            sLineItem = AlignString(p_structEOBDetailRecord.PhysicianLicenseNum.Token, p_structEOBDetailRecord.PhysicianLicenseNum.Width, p_structEOBDetailRecord.PhysicianLicenseNum.Datawidth, p_structEOBDetailRecord.PhysicianLicenseNum.Alignment);
                            if (!bIsFirstEOBSeen) iWidthBeforeEOB += p_structEOBDetailRecord.PhysicianLicenseNum.Width;
                            if (!bIsFirstDiagSeen) iWidthBeforeDiag += p_structEOBDetailRecord.PhysicianLicenseNum.Width;
                            if (!bIsFirstModSeen) iWidthBeforeMod += p_structEOBDetailRecord.PhysicianLicenseNum.Width;
                            break;

                        case REV_CODE:
                            sLineItem = AlignString(p_structEOBDetailRecord.RevenueCode.Token, p_structEOBDetailRecord.RevenueCode.Width, p_structEOBDetailRecord.RevenueCode.Datawidth, p_structEOBDetailRecord.RevenueCode.Alignment);
                            if (!bIsFirstEOBSeen) iWidthBeforeEOB += p_structEOBDetailRecord.RevenueCode.Width;
                            if (!bIsFirstDiagSeen) iWidthBeforeDiag += p_structEOBDetailRecord.RevenueCode.Width;
                            if (!bIsFirstModSeen) iWidthBeforeMod += p_structEOBDetailRecord.RevenueCode.Width;
                            break;

                            //end
						case EXPLANATION:
							bIsFirstEOBSeen = true;
							if(p_structEOBDetailRecord.EOBList.Count > 0)
								sLineItem = AlignString(p_structEOBDetailRecord.EOBList[1].ToString(),p_structEOBDetailRecord.EOBWidth,p_structEOBDetailRecord.EOBDataWidth,p_structEOBDetailRecord.EOBAlign);
							else
								sLineItem = AlignString(string.Empty,p_structEOBDetailRecord.EOBWidth,p_structEOBDetailRecord.EOBDataWidth,p_structEOBDetailRecord.EOBAlign);
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.EOBWidth;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.EOBWidth;
							break;

						case DIAGNOSIS:
							bIsFirstDiagSeen = true;
							if(p_structEOBDetailRecord.DiagnosisList.Count > 0)
								sLineItem = AlignString(p_structEOBDetailRecord.DiagnosisList[1].ToString(),p_structEOBDetailRecord.DiagnosisWidth,p_structEOBDetailRecord.DiagnosisDataWidth,p_structEOBDetailRecord.DiagnosisAlign);
							else
								sLineItem = AlignString(string.Empty,p_structEOBDetailRecord.DiagnosisWidth,p_structEOBDetailRecord.DiagnosisDataWidth,p_structEOBDetailRecord.DiagnosisAlign);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.DiagnosisWidth;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.DiagnosisWidth;
							break;

						case MODIFIER:
						case MODIFIER_CODE:
							bIsFirstModSeen = true;
							if(p_structEOBDetailRecord.ModifierList.Count > 0)
								sLineItem = AlignString(p_structEOBDetailRecord.ModifierList[1].ToString(),p_structEOBDetailRecord.ModifierWidth,p_structEOBDetailRecord.ModifierDataWidth,p_structEOBDetailRecord.ModifierAlign);
							else
								sLineItem = AlignString(string.Empty,p_structEOBDetailRecord.ModifierWidth,p_structEOBDetailRecord.ModifierDataWidth,p_structEOBDetailRecord.ModifierAlign);
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.ModifierWidth;
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.ModifierWidth;
							break;

						case UNITS_BILLED:
							sLineItem = AlignDouble(p_structEOBDetailRecord.UnitsBilled.Token,p_structEOBDetailRecord.UnitsBilled.Width,p_structEOBDetailRecord.UnitsBilled.Datawidth,p_structEOBDetailRecord.UnitsBilled.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.UnitsBilled.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.UnitsBilled.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.UnitsBilled.Width;
							break;

						case AMOUNT_BILLED:
							sLineItem = AlignDouble(p_structEOBDetailRecord.AmountBilled.Token,p_structEOBDetailRecord.AmountBilled.Width,p_structEOBDetailRecord.AmountBilled.Datawidth,p_structEOBDetailRecord.AmountBilled.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.AmountBilled.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.AmountBilled.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.AmountBilled.Width;
							break;

						case AMOUNT_PAID:
							sLineItem = AlignDouble(p_structEOBDetailRecord.AmountPaid.Token,p_structEOBDetailRecord.AmountPaid.Width,p_structEOBDetailRecord.AmountPaid.Datawidth,p_structEOBDetailRecord.AmountPaid.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.AmountPaid.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.AmountPaid.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.AmountPaid.Width;
							break;

						case SCHEDULED_AMOUNT:
							sLineItem = AlignDouble(p_structEOBDetailRecord.ScheduledAmount.Token,p_structEOBDetailRecord.ScheduledAmount.Width,p_structEOBDetailRecord.ScheduledAmount.Datawidth,p_structEOBDetailRecord.ScheduledAmount.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.ScheduledAmount.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.ScheduledAmount.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.ScheduledAmount.Width;
							break;

						case AMOUNT_REDUCED:
							sLineItem = AlignDouble(p_structEOBDetailRecord.AmountReduced.Token,p_structEOBDetailRecord.AmountReduced.Width,p_structEOBDetailRecord.AmountReduced.Datawidth,p_structEOBDetailRecord.AmountReduced.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.AmountReduced.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.AmountReduced.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.AmountReduced.Width;
							break;

						case AMOUNT_SAVED:
							sLineItem = AlignDouble(p_structEOBDetailRecord.AmountSaved.Token,p_structEOBDetailRecord.AmountSaved.Width,p_structEOBDetailRecord.AmountSaved.Datawidth,p_structEOBDetailRecord.AmountSaved.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.AmountSaved.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.AmountSaved.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.AmountSaved.Width;
							break;

						case CONTRACT_AMOUNT:
							sLineItem = AlignDouble(p_structEOBDetailRecord.ContractAmount.Token,p_structEOBDetailRecord.ContractAmount.Width,p_structEOBDetailRecord.ContractAmount.Datawidth,p_structEOBDetailRecord.ContractAmount.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.ContractAmount.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.ContractAmount.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.ContractAmount.Width;
							break;

						case PROVIDER_DISCOUNT:
							sLineItem = AlignDouble(p_structEOBDetailRecord.Discount.Token,p_structEOBDetailRecord.Discount.Width,p_structEOBDetailRecord.Discount.Datawidth,p_structEOBDetailRecord.Discount.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.Discount.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.Discount.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.Discount.Width;
							break;

						case BILLING_POSTAL_CODE:
							sLineItem = AlignString(p_structEOBDetailRecord.ProviderZipCode.Token,p_structEOBDetailRecord.ProviderZipCode.Width,p_structEOBDetailRecord.ProviderZipCode.Datawidth,p_structEOBDetailRecord.ProviderZipCode.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.ProviderZipCode.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.ProviderZipCode.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.ProviderZipCode.Width;
							break;

						case BASE_AMOUNT:
							sLineItem = AlignDouble(p_structEOBDetailRecord.BaseAmount.Token,p_structEOBDetailRecord.BaseAmount.Width,p_structEOBDetailRecord.BaseAmount.Datawidth,p_structEOBDetailRecord.BaseAmount.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.BaseAmount.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.BaseAmount.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.BaseAmount.Width;
							break;

						case AMOUNT_ALLOWED:
							sLineItem = AlignDouble(p_structEOBDetailRecord.AmountAllowed.Token,p_structEOBDetailRecord.AmountAllowed.Width,p_structEOBDetailRecord.AmountAllowed.Datawidth,p_structEOBDetailRecord.AmountAllowed.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.AmountAllowed.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.AmountAllowed.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.AmountAllowed.Width;
							break;

						case DISCOUNT_AMOUNT:
							sLineItem = AlignDouble(p_structEOBDetailRecord.DiscountAmount.Token,p_structEOBDetailRecord.DiscountAmount.Width,p_structEOBDetailRecord.DiscountAmount.Datawidth,p_structEOBDetailRecord.DiscountAmount.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.DiscountAmount.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.DiscountAmount.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.DiscountAmount.Width;
							break;

						case PER_DIEM_AMOUNT:
							sLineItem = AlignDouble(p_structEOBDetailRecord.PerDiemAmount.Token,p_structEOBDetailRecord.PerDiemAmount.Width,p_structEOBDetailRecord.PerDiemAmount.Datawidth,p_structEOBDetailRecord.PerDiemAmount.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.PerDiemAmount.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.PerDiemAmount.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.PerDiemAmount.Width;
							break;

						case STOP_LOSS_AMOUNT:
							sLineItem = AlignDouble(p_structEOBDetailRecord.StopLossAmount.Token,p_structEOBDetailRecord.StopLossAmount.Width,p_structEOBDetailRecord.StopLossAmount.Datawidth,p_structEOBDetailRecord.StopLossAmount.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.StopLossAmount.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.StopLossAmount.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.StopLossAmount.Width;
							break;

						case FEE_TABLE_AMOUNT:
							sLineItem = AlignDouble(p_structEOBDetailRecord.FeeTableAmount.Token,p_structEOBDetailRecord.FeeTableAmount.Width,p_structEOBDetailRecord.FeeTableAmount.Datawidth,p_structEOBDetailRecord.FeeTableAmount.Alignment);
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += p_structEOBDetailRecord.FeeTableAmount.Width;
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += p_structEOBDetailRecord.FeeTableAmount.Width;
							if(!bIsFirstModSeen)  iWidthBeforeMod += p_structEOBDetailRecord.FeeTableAmount.Width;
							break;

						case SPACE:
							iSpaceCounter++;
							sLineItem = FillSpace(Conversion.ConvertStrToInteger(m_objSpaceWidth[iSpaceCounter].ToString()));
							if(!bIsFirstEOBSeen)  iWidthBeforeEOB += Conversion.ConvertStrToInteger(m_objSpaceWidth[iSpaceCounter].ToString());
							if(!bIsFirstDiagSeen)  iWidthBeforeDiag += Conversion.ConvertStrToInteger(m_objSpaceWidth[iSpaceCounter].ToString());
							if(!bIsFirstModSeen)  iWidthBeforeMod += Conversion.ConvertStrToInteger(m_objSpaceWidth[iSpaceCounter].ToString());
							break;
					}//end select
					sDetailItem += sLineItem; 
				}//end for
				
				GetCustomEOBLine(sDetailItem,ref p_structEOBRecord,ref p_objPrintWrapper);   //1st item goes on normal Split/Detail line
				
				//Etc item(s) go on additional line(s) (multiple EOB, Diagnosis, Modifier).
				iAddItemsCount = 1;
				sLineItem = string.Empty;
				string sTo = string.Empty;
 				string sLineItem2 = string.Empty;
 				string sLineItem3 = string.Empty;

				if((bIsFirstEOBSeen || bIsFirstDiagSeen || bIsFirstModSeen) 
					&& (p_structEOBDetailRecord.EOBList.Count > 1 || p_structEOBDetailRecord.DiagnosisList.Count > 1) 
					|| p_structEOBDetailRecord.ModifierList.Count > 1)
				{
					if(p_structEOBDetailRecord.EOBList.Count > p_structEOBDetailRecord.DiagnosisList.Count)
						if(p_structEOBDetailRecord.EOBList.Count > p_structEOBDetailRecord.ModifierList.Count)
							iAddItemsCount = p_structEOBDetailRecord.EOBList.Count;
						else
							iAddItemsCount = p_structEOBDetailRecord.ModifierList.Count;
					else if(p_structEOBDetailRecord.DiagnosisList.Count > p_structEOBDetailRecord.ModifierList.Count)
						iAddItemsCount = p_structEOBDetailRecord.DiagnosisList.Count;
					else
						iAddItemsCount = p_structEOBDetailRecord.ModifierList.Count;

					for(int iLineItemCount = 2; iLineItemCount <= iAddItemsCount; iLineItemCount++)
					{
						sTo="";
						if(p_structEOBDetailRecord.EOBList.Count >= iLineItemCount)
							sTo = p_structEOBDetailRecord.EOBList[iLineItemCount].ToString();
                        //Ashish Ahuja - Mits 33618
                        if (m_bEOBDescNextLine == true)
                        {
                            sLineItem = "\n" + AlignString(sTo, p_structEOBDetailRecord.EOBWidth, p_structEOBDetailRecord.EOBDataWidth, p_structEOBDetailRecord.EOBAlign);
                        }
                        else
                        {
                            sLineItem = AlignString(sTo, p_structEOBDetailRecord.EOBWidth, p_structEOBDetailRecord.EOBDataWidth, p_structEOBDetailRecord.EOBAlign);
                        }
						sTo="";
						if(p_structEOBDetailRecord.DiagnosisList.Count >= iLineItemCount)
							sTo = p_structEOBDetailRecord.DiagnosisList[iLineItemCount].ToString();
						sLineItem2 = AlignString(sTo, p_structEOBDetailRecord.DiagnosisWidth,p_structEOBDetailRecord.DiagnosisDataWidth,p_structEOBDetailRecord.DiagnosisAlign);
						sTo="";
						if(p_structEOBDetailRecord.ModifierList.Count >= iLineItemCount)
							sTo = p_structEOBDetailRecord.ModifierList[iLineItemCount].ToString();
						sLineItem3 = AlignString(sTo, p_structEOBDetailRecord.ModifierWidth,p_structEOBDetailRecord.ModifierDataWidth,p_structEOBDetailRecord.ModifierAlign);

						if(iWidthBeforeDiag > iWidthBeforeEOB)
						{
							//Eob - Diag - Mod
							if(iWidthBeforeMod > iWidthBeforeDiag)
							{
								sLineItem = FillSpace(iWidthBeforeEOB) + sLineItem;
								iSpaceWidth1 = iWidthBeforeDiag - sLineItem.Length;
								if(iSpaceWidth1 < 0) iSpaceWidth1 = 0;
								
								sLineItem2 = sLineItem + FillSpace(iSpaceWidth1) + sLineItem2;
								iSpaceWidth2 = iWidthBeforeMod - sLineItem2.Length;
								if(iSpaceWidth2 < 0) iSpaceWidth2 = 0;

								sLineItem2 = sLineItem2 + FillSpace(iSpaceWidth2) + sLineItem3;     
							}
							else	
							{
								//Eob - Mod - Diag
								if(iWidthBeforeMod > iWidthBeforeEOB)
								{
									sLineItem = FillSpace(iWidthBeforeEOB) + sLineItem;
									iSpaceWidth1 = iWidthBeforeMod - sLineItem.Length;
									if(iSpaceWidth1 < 0) iSpaceWidth1 = 0;
									
									sLineItem3 = sLineItem + FillSpace(iSpaceWidth1) + sLineItem3;
									iSpaceWidth2 = iWidthBeforeDiag - sLineItem3.Length;
									if(iSpaceWidth2 < 0) iSpaceWidth2 = 0;

									sLineItem2 = sLineItem3 + FillSpace(iSpaceWidth2) + sLineItem2;     
								}
								else	//Mod - Eob - Diag
								{
									sLineItem3 = FillSpace(iWidthBeforeMod) + sLineItem3;
									iSpaceWidth1 = iWidthBeforeEOB - sLineItem3.Length;
									if(iSpaceWidth1 < 0) iSpaceWidth1 = 0;
									
									sLineItem = sLineItem3 + FillSpace(iSpaceWidth1) + sLineItem;
									iSpaceWidth2 = iWidthBeforeDiag - sLineItem.Length;
									if(iSpaceWidth2 < 0) iSpaceWidth2 = 0;

									sLineItem2 = sLineItem + FillSpace(iSpaceWidth2) + sLineItem2;     
								}

							}
						}
						else
						{
							//Diag - Eob - Mod
							if(iWidthBeforeMod > iWidthBeforeEOB)
							{
								sLineItem2 = FillSpace(iWidthBeforeDiag) + sLineItem2;
								iSpaceWidth1 = iWidthBeforeEOB - sLineItem2.Length;
								if(iSpaceWidth1 < 0) iSpaceWidth1 = 0;
								
								sLineItem = sLineItem2 + FillSpace(iSpaceWidth1) + sLineItem;
								iSpaceWidth2 = iWidthBeforeMod - sLineItem.Length;
								if(iSpaceWidth2 < 0) iSpaceWidth2 = 0;

								sLineItem2 = sLineItem + FillSpace(iSpaceWidth2) + sLineItem3;     
							}
							else
							{
								//Diag - Mod - Eob
								if(iWidthBeforeMod > iWidthBeforeDiag)
								{
									sLineItem2 = FillSpace(iWidthBeforeDiag) + sLineItem2;
									iSpaceWidth1 = iWidthBeforeMod - sLineItem2.Length;
									if(iSpaceWidth1 < 0) iSpaceWidth1 = 0;
									
									sLineItem3 = sLineItem2 + FillSpace(iSpaceWidth1) + sLineItem3;
									iSpaceWidth2 = iWidthBeforeEOB - sLineItem3.Length;
									if(iSpaceWidth2 < 0) iSpaceWidth2 = 0;

									sLineItem2 = sLineItem3 + FillSpace(iSpaceWidth2) + sLineItem;     
								}
								else	//Mod - Diag - Eob
								{
									sLineItem3 = FillSpace(iWidthBeforeMod) + sLineItem3;
									iSpaceWidth1 = iWidthBeforeDiag - sLineItem3.Length;
									if(iSpaceWidth1 < 0) iSpaceWidth1 = 0;
									sLineItem2 = sLineItem3 + FillSpace(iSpaceWidth1) + sLineItem2;
									iSpaceWidth2 = iWidthBeforeEOB - sLineItem3.Length;
									if(iSpaceWidth2 < 0) iSpaceWidth2 = 0;

									sLineItem2 = sLineItem2 + FillSpace(iSpaceWidth2) + sLineItem;     
								}//End of If
							}//End of If
						}//End of IF

						//2nd, etc item(s) go on additional line(s) (EOB, Diagnosis, Modifier).
                        //Ashish Ahuja - Mits 33681
                        if (m_bEOBDescNextLine == true)
                        {
                            sLineItem2 = " " + sLineItem2.Trim();
                        }
						GetCustomEOBLine(sLineItem2,ref p_structEOBRecord,ref p_objPrintWrapper);

					}//End For
				}//End IF

			}//End Try
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.GetCustomEOBBody.ErrorGet", m_iClientId), p_objEx);	//sharishkumar Jira 835			
			}
		}
		#endregion

		#region "GetCustomEOBLine(string p_sToken,ref EOBRecord p_structEOBRecord, ref PrintWrapper p_objPrintWrapper)"
		/// Name		: GetCustomEOBLine
		/// Author		: Anurag Agarwal
		/// Date Created	: 27 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Returns the Custom EOB line for report Title or Subtitle
		/// </summary>
		/// <param name="p_sToken">EOB tag</param>
		/// <param name="p_structEOBRecord">EOB Record</param>
		/// <param name="p_objPrintWrapper">object of class use to generate the PDF file</param>
		internal void GetCustomEOBLine(string p_sToken,ref EOBRecord p_structEOBRecord, ref PrintWrapper p_objPrintWrapper)
		{
			string sTokenRemnant = string.Empty;
			string sTokenLine = string.Empty;
			int iCRLoc = 0;
			string sTempAlign = string.Empty;
            string sNewline = string.Empty;//nadim for 14513

			try
			{
				sTokenRemnant = p_sToken;
                //nadim for 14513
                sNewline = p_sToken;
				while(true)
				{
					iCRLoc = sTokenRemnant.IndexOf('\n'); //0x0d
					if(iCRLoc != -1)
					{
						sTokenLine = sTokenRemnant.Substring(0,iCRLoc - 1);
						sTokenRemnant = sTokenRemnant.Substring(iCRLoc + 1); //+1 for Chr(10)/0x0a
					}
					else
						sTokenLine = sTokenRemnant;
                    //nadim for 14513
                    if (sTokenLine.Length != 0 || sNewline.Contains("\r\n"))
                    //if (sTokenLine.Length != 0)
					{
                        sNewline = string.Empty;//nadim for 14513
						if(p_structEOBRecord.FirstPrint)
						{
							p_objPrintWrapper.CurrentY = p_structEOBRecord.CurrentY1Margin;
							p_structEOBRecord.FirstPrint = false; 
						}
						else
						{
							if((p_objPrintWrapper.GetTextHeight(sTokenLine) + p_objPrintWrapper.CurrentY) > (p_objPrintWrapper.PageHeight - p_structEOBRecord.CurrentY2Margin))
							{
								p_objPrintWrapper.NewPage();
								p_objPrintWrapper.PrintImage(p_structEOBRecord);
								p_objPrintWrapper.CurrentY = p_structEOBRecord.CurrentY1Margin;
                                if (p_structEOBRecord.Title != null)
                                {
                                    if (p_structEOBRecord.Title.Length != 0)
                                    {
                                        sTempAlign = p_structEOBRecord.CurrentAlign;
                                        p_objPrintWrapper.SetFont(p_structEOBRecord.TitleBold.ToString(), p_structEOBRecord.TitleFont);
                                        p_structEOBRecord.CurrentAlign = p_structEOBRecord.TitleAlign;
                                        SetCurrentX(p_structEOBRecord.Title, p_structEOBRecord, ref p_objPrintWrapper);
                                        p_objPrintWrapper.PrintText(p_structEOBRecord.Title);
                                        //Geeta 09/24/07 : Mits 10328
                                        p_objPrintWrapper.SetFont("Courier New", 8);
                                        p_structEOBRecord.CurrentAlign = sTempAlign;
                                        p_objPrintWrapper.CurrentY += p_objPrintWrapper.GetTextHeight("W");
                                    }
                                }
                                if (p_structEOBRecord.SubTitle != null)
                                {
                                    if (p_structEOBRecord.SubTitle.Length != 0)
                                    {
                                        sTempAlign = p_structEOBRecord.CurrentAlign;
                                        p_objPrintWrapper.SetFont(p_structEOBRecord.SubTitleBold.ToString(), p_structEOBRecord.SubTitleFont);
                                        p_structEOBRecord.CurrentAlign = p_structEOBRecord.SubTitleAlign;
                                        SetCurrentX(p_structEOBRecord.SubTitle, p_structEOBRecord, ref p_objPrintWrapper);
                                        p_objPrintWrapper.PrintText(p_structEOBRecord.SubTitle);
                                        p_structEOBRecord.CurrentAlign = sTempAlign;
                                    }
                                }

							}//Enf of IF
						}//END of IF
                       
						SetCurrentX(sTokenLine,p_structEOBRecord,ref p_objPrintWrapper);
						p_objPrintWrapper.CurrentY += p_objPrintWrapper.GetTextHeight("W"); //Added  
						p_objPrintWrapper.PrintText(sTokenLine);  
					}//END of IF
					if(iCRLoc == -1)
						break;
				}//END of WHILE
			}//End Try
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.GetCustomEOBLine.ErrorGet", m_iClientId), p_objEx);//sharishkumar Jira 835				
			}
		}
		#endregion

		#region "GetCustomEOBHeader(StreamReader p_objSReader,ref EOBRecord p_structEOBRecord,PrintWrapper p_objPrintWrapper,Funds p_objFunds)"
		/// Name		: GetCustomEOBHeader
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 Jan 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// TO load the EOB Header. The same sub is used for Header and Footer.
		/// The only difference is that Title and SubTitle are not used in the Footer.
		/// </summary>
		/// <param name="p_objSReader">Object of IO Stream class containing the format file stream</param>
		/// <param name="p_stEOBRecord">EOB Record</param>
		/// <param name="p_objPrintWrapper">object of class use to generate the PDF file</param>
		/// <param name="p_objFunds">Funds Object</param>
		internal void GetCustomEOBHeader(StreamReader p_objSReader,ref EOBRecord p_structEOBRecord,PrintWrapper p_objPrintWrapper,Funds p_objFunds)
		{
			char [] cCurrentChar = {' '};
			string sCurrentToken = string.Empty;
			string sProcessedToken = string.Empty;
			int iNextInput = 0;
			double dAvgTextHeight = 0.0;
			double dAvgPrintWidth = 0.0;
			double dCurrY = 0;

			try
			{

				dAvgTextHeight = p_objPrintWrapper.GetTextHeight("W");  
				dAvgPrintWidth = p_objPrintWrapper.PageWidth;
				
				dCurrY = 100;

				sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
				while ((iNextInput = p_objSReader.Peek()) != -1) 
				{
					if(sCurrentToken.Length != 0)
					{
						sCurrentToken = sCurrentToken.ToUpper();
						
						switch (sCurrentToken)
						{
							case TAG_EOB:
								sProcessedToken = string.Empty; 
								break;
							case TAG_LOGO:
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								p_objPrintWrapper.PrintText(string.Empty);
								p_objPrintWrapper.CurrentX = p_structEOBRecord.CurrentX1Margin; 
								p_objPrintWrapper.CurrentY = p_structEOBRecord.CurrentY1Margin;
								ExtractLogoInfo(sCurrentToken,ref p_structEOBRecord);
								p_objPrintWrapper.PrintImage(p_structEOBRecord); 
								p_objPrintWrapper.PrintText(string.Empty);
								p_objPrintWrapper.CurrentX = p_structEOBRecord.CurrentX1Margin; 
								p_objPrintWrapper.CurrentY = p_structEOBRecord.CurrentY1Margin;
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);		
								break;
							case TAG_TITLE:
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								p_structEOBRecord.Title = sCurrentToken;
								p_structEOBRecord.TitleAlign = p_structEOBRecord.CurrentAlign;
								p_structEOBRecord.TitleFont = p_objPrintWrapper.FontSize;
								p_structEOBRecord.TitleBold = 1;
								if(p_structEOBRecord.FirstPrint == true)
								{
									p_objPrintWrapper.CurrentY = p_structEOBRecord.CurrentY1Margin;
									p_structEOBRecord.FirstPrint = false;
								}
								SetCurrentX(p_structEOBRecord.Title,p_structEOBRecord,ref p_objPrintWrapper);
								p_objPrintWrapper.CurrentY = dCurrY + dAvgTextHeight;
  								p_objPrintWrapper.PrintText(p_structEOBRecord.Title);				
								break;
							case TAG_SUBTITLE:
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								p_structEOBRecord.SubTitle  = sCurrentToken;
								p_structEOBRecord.SubTitleAlign = p_structEOBRecord.CurrentAlign;
								p_structEOBRecord.SubTitleFont = p_objPrintWrapper.FontSize;
								p_structEOBRecord.SubTitleBold = 1;
								if(p_structEOBRecord.FirstPrint == true)
								{
									p_objPrintWrapper.CurrentY = p_structEOBRecord.CurrentY1Margin;
									p_structEOBRecord.FirstPrint = false;
								}
								SetCurrentX(p_structEOBRecord.SubTitle,p_structEOBRecord,ref p_objPrintWrapper);
								p_objPrintWrapper.CurrentY = dCurrY + dAvgTextHeight;
								p_objPrintWrapper.PrintText(p_structEOBRecord.SubTitle);
								break;
							case TAG_BR:
								if(sProcessedToken.Length != 0)
								{
									GetCustomEOBLine(sProcessedToken,ref p_structEOBRecord,ref p_objPrintWrapper);
									sProcessedToken = string.Empty; 
								}
								p_objPrintWrapper.PrintText(string.Empty); 
								break;
							case TAG_B:
								if(sProcessedToken.Length != 0)
								{
									GetCustomEOBLine(sProcessedToken,ref p_structEOBRecord,ref p_objPrintWrapper);
									sProcessedToken = string.Empty; 
								}
								p_objPrintWrapper.SetFontBold(true);
								break;
							case ENDTAG_B:
								if(sProcessedToken.Length != 0)
								{
									GetCustomEOBLine(sProcessedToken,ref p_structEOBRecord,ref p_objPrintWrapper);
									sProcessedToken = string.Empty; 
								}
								p_objPrintWrapper.SetFontBold(false);
								break;
							case TAG_FONT:
								if(sProcessedToken.Length != 0)
								{
									GetCustomEOBLine(sProcessedToken,ref p_structEOBRecord,ref p_objPrintWrapper);
									sProcessedToken = string.Empty; 
								}
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								p_objPrintWrapper.SetFont(Conversion.ConvertStrToInteger(sCurrentToken));
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								break;
							case TAG_ALIGN:
								if(sProcessedToken.Length != 0)
								{
									GetCustomEOBLine(sProcessedToken,ref p_structEOBRecord,ref p_objPrintWrapper);
									sProcessedToken = string.Empty; 
								}
								p_structEOBRecord.CurrentAlign = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER).ToUpper();
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								break;
							case TAG_LEFTMARGIN:
								if(sProcessedToken.Length != 0)
								{
									GetCustomEOBLine(sProcessedToken,ref p_structEOBRecord,ref p_objPrintWrapper);
									sProcessedToken = string.Empty; 
								}
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								p_structEOBRecord.CurrentX1Margin = Conversion.ConvertStrToDouble(sCurrentToken) * 1440;
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								break;
							case TAG_TOPMARGIN:
								if(sProcessedToken.Length != 0)
								{
									GetCustomEOBLine(sProcessedToken,ref p_structEOBRecord,ref p_objPrintWrapper);
									sProcessedToken = string.Empty; 
								}
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								p_structEOBRecord.CurrentY1Margin = Conversion.ConvertStrToDouble(sCurrentToken) * 1440;
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								break;
							case TAG_RIGHTMARGIN:
								if(sProcessedToken.Length != 0)
								{
									GetCustomEOBLine(sProcessedToken,ref p_structEOBRecord,ref p_objPrintWrapper);
									sProcessedToken = string.Empty; 
								}
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								p_structEOBRecord.CurrentX2Margin = Conversion.ConvertStrToDouble(sCurrentToken) * 1440;
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								break;
							case TAG_BOTTOMMARGIN:
								if(sProcessedToken.Length != 0)
								{
									GetCustomEOBLine(sProcessedToken,ref p_structEOBRecord,ref p_objPrintWrapper);
									sProcessedToken = string.Empty; 
								}
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								p_structEOBRecord.CurrentY2Margin = Conversion.ConvertStrToDouble(sCurrentToken) * 1440;
								sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
								break;
							case TAG_LINE_ITEMS:
								goto OUTOFWHILE;
							case ENDTAG_EOB:
								if(sProcessedToken.Length != 0)
								{
									GetCustomEOBLine(sProcessedToken,ref p_structEOBRecord,ref p_objPrintWrapper);
									sProcessedToken = string.Empty; 
								}
								break;
                            case TAG_HORIZONTAL_LINE://Debabrata MITS Issue 17183
                                if (sProcessedToken.Length != 0)
                                {
                                    GetCustomEOBLine(sProcessedToken, ref p_structEOBRecord, ref p_objPrintWrapper);
                                    sProcessedToken = string.Empty;
                                }
                                p_objPrintWrapper.PrintLine(p_objPrintWrapper.CurrentX, p_objPrintWrapper.CurrentY, p_objPrintWrapper.PageWidth, p_objPrintWrapper.CurrentY);
                                break;
							default:
								sProcessedToken += ProcessToken(sCurrentToken,p_structEOBRecord,p_objFunds);
								break;
						}
					}	//End if(sCurrentToken.Length != 0) 
					sCurrentToken = GetCurrentToken(p_objSReader,SIGN_GREATER,SIGN_LOWER);
				}	//End while ((iNextInput = p_objSReader.Peek()) != -1) 
			OUTOFWHILE:
				GetCustomEOBLine(sProcessedToken,ref p_structEOBRecord,ref p_objPrintWrapper); 
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.GetCustomEOBHeader.ErrorGet", m_iClientId), p_objEx);	//sharishkumar Jira 835			
			}
		}
		#endregion

        #region "PrintEOBHTMLReport(StreamReader p_objSReader, ref EOBRecord p_structEOBRecord, PrintWrapper p_objPrintWrapper, Funds p_objFunds)"
        /// <summary>
        /// Debabrata Biswas MITS Issue 17183
        /// </summary>
        /// <param name="currentHTMLcell"></param>
        /// <param name="p_structEOBRecord"></param>
        /// <param name="p_structEOBDetailRecord"></param>
        /// <param name="objFundsTransSplit"></param>
        /// <returns></returns>
        internal string ProcessHTMLtableToken(string currentHTMLcell, int iDataLength, ref EOBRecord p_structEOBRecord, ref EOBDetailRecord p_structEOBDetailRecord, FundsTransSplit objFundsTransSplit)
        {
            string sTableColumn = string.Empty;

            if (currentHTMLcell.StartsWith("{") && currentHTMLcell.EndsWith("}"))
            {
                sTableColumn = currentHTMLcell.Substring(1, currentHTMLcell.Length - 2).ToUpper().Trim();
                if (sTableColumn.StartsWith("FUNDS_DETAIL_SUPP."))
                {
                    string sSuppFieldName = sTableColumn.Substring(18);
                    SupplementalField objFundsTransSplitSupp = objFundsTransSplit.Supplementals[sSuppFieldName];

                    if (objFundsTransSplitSupp != null)
                    {
                        switch (objFundsTransSplitSupp.FieldType)
                        {
                            case SupplementalFieldTypes.SuppTypeText:
                                if(iDataLength!=0)
                                    return Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value).Substring(0, iDataLength);
                                else
                                    return Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value);
                            case SupplementalFieldTypes.SuppTypeFreeText:
                                if (iDataLength != 0)
                                    return Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value).Substring(0, iDataLength);
                                else
                                    return Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value);
                            case SupplementalFieldTypes.SuppTypeMemo:
                                if (iDataLength != 0)
                                    return Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value).Substring(0, iDataLength);
                                else
                                    return Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value);
                            case SupplementalFieldTypes.SuppTypeNumber:
                                if (iDataLength != 0)
                                    return Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value).Substring(0, iDataLength);
                                else
                                    return Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value);
                            case SupplementalFieldTypes.SuppTypeCurrency:
                                if (iDataLength != 0)
                                    return string.Format("{0:#,###,##0.00}", objFundsTransSplitSupp.Value).Substring(0, iDataLength);
                                    //return Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value).Substring(0, iDataLength);
                                else
                                    return string.Format("{0:#,###,##0.00}", objFundsTransSplitSupp.Value);
                                    //return Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value);
                            case SupplementalFieldTypes.SuppTypeDate:
                                if(iDataLength!=0)
                                    return string.Format("{0:MMMM dd, YYYY}", Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value)).Substring(0, iDataLength);
                                else
                                    return string.Format("{0:MMMM dd, YYYY}", Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value));
                            case SupplementalFieldTypes.SuppTypeCode:
                                using (LocalCache objCache = new LocalCache(m_ConnectionString, m_iClientId))
                                {
                                    int icodeId;
                                    bool bParse;
                                    bParse = int.TryParse(Conversion.ConvertObjToStr(objFundsTransSplitSupp.Value).ToString(), out icodeId);
                                    if (bParse)
                                        if(iDataLength!=0)
                                            return objCache.GetCodeDesc(icodeId).Substring(0,iDataLength);
                                        else
                                            return objCache.GetCodeDesc(icodeId);
                                    else
                                        return "";
                                }
                            case SupplementalFieldTypes.SuppTypeMultiCode:
                                string sMultiCodeDesc = string.Empty;
                                string[] sMultiCodeIds = Conversion.ConvertObjToStr(objFundsTransSplitSupp.Values).Split(' ');
                                using (LocalCache objCache = new LocalCache(m_ConnectionString, m_iClientId))
                                {
                                    foreach (string sCodeId in sMultiCodeIds)
                                    {
                                        int icodeId;
                                        bool bParse;

                                        bParse = int.TryParse(sCodeId, out icodeId);
                                        if (bParse)
                                        {
                                            if (sMultiCodeDesc == string.Empty)
                                                if(iDataLength!=0)
                                                    sMultiCodeDesc = objCache.GetCodeDesc(icodeId).Substring(0,iDataLength);
                                                else
                                                    sMultiCodeDesc = objCache.GetCodeDesc(icodeId);
                                            else
                                                if(iDataLength!=0)
                                                    sMultiCodeDesc = sMultiCodeDesc + "<BR />" + objCache.GetCodeDesc(icodeId).Substring(0, iDataLength);
                                                else
                                                    sMultiCodeDesc = sMultiCodeDesc + "<BR />" + objCache.GetCodeDesc(icodeId);
                                        }
                                    }
                                }
                                return sMultiCodeDesc;
                            case SupplementalFieldTypes.SuppTypeGrid:
                                if (objFundsTransSplitSupp.gridXML != null)
                                {
                                    XmlNodeList xGridROWS;
                                    XmlNodeList xGridCELLS;
                                    string sGridData = string.Empty;
                                    string sGridRow = string.Empty;
                                    string sGridTable = string.Empty;

                                    xGridROWS = objFundsTransSplitSupp.gridXML.SelectNodes("//row");
                                    if (xGridROWS != null)
                                    {
                                        foreach (XmlNode xRow in xGridROWS)
                                        {
                                            xGridCELLS = xRow.SelectNodes("cell");
                                            if (xGridCELLS != null)
                                            {
                                                sGridData = string.Empty;
                                                foreach (XmlNode xCell in xGridCELLS)
                                                {
                                                    if (sGridData == string.Empty)
                                                        sGridData = "<TD>" + xCell.InnerText + "</TD>";
                                                    else
                                                        sGridData = sGridData + "<TD>" + xCell.InnerText + "</TD>";
                                                }
                                                sGridRow = sGridRow + "<TR>" + sGridData + "</TR>";
                                            }
                                        }
                                        sGridTable = "<TABLE WIDTH=\"100%\">" + sGridRow + "</TABLE>";
                                        return sGridTable;
                                    }
                                    else
                                    {
                                        return "";
                                    }
                                }
                                else
                                {
                                    return "";
                                }
                            default:
                                return "";
                        }
                    }
                    else
                    {
                        return currentHTMLcell;
                    }

                }
                else//logic for normal fields
                {
                    switch (sTableColumn)
                    {
                        case INVOICE_DATE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.InvoiceDate.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.InvoiceDate.Token;
                        case INVOICED_BY:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.InvoicedBy.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.InvoicedBy.Token;
                        case DISC_ON_FEE_SCHD:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.DiscOnFeeSchd.Token.ToString().Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.DiscOnFeeSchd.Token.ToString();
                        case PRESCRIP_NO:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.PrescripNo.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.PrescripNo.Token;
                        case DRUG_NAME:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.DrugName.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.DrugName.Token;
                        case PRESCRIP_DATE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.PrescripDate.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.PrescripDate.Token;
                        case INVOICE_NUMBER:
                            if (iDataLength != 0)
                                return p_structEOBDetailRecord.InvoiceNumber.Token.Substring(0, iDataLength);
                            else
                                return p_structEOBDetailRecord.InvoiceNumber.Token;
                        case PROCEDURE_DATE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.ProcedureDate.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.ProcedureDate.Token;
                        case PROC_TO_DATE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.ProcToDate.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.ProcToDate.Token;
                        case PROCEDURE_CODE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.ProcedureCode.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.ProcedureCode.Token;
                        case PROCEDURE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.Procedure.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.Procedure.Token;
                        case FEE_SCHEDULE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.FeeSchedule.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.FeeSchedule.Token;
                        case PERCENTILE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.Percentile.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.Percentile.Token;
                        case PLACE_OF_SERVICE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.PlaceOfService.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.PlaceOfService.Token;
                        case PLACE_OF_SERVICE_CODE:
                            if (iDataLength != 0)
                                return p_structEOBDetailRecord.PlaceofServiceCode.Token.Substring(0, iDataLength);
                            else
                                return p_structEOBDetailRecord.PlaceofServiceCode.Token;
                        case TYPE_OF_SERVICE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.TypeOfService.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.TypeOfService.Token;
                        case TYPE_OF_SERVICE_CODE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.TypeOfServiceCode.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.TypeOfServiceCode.Token;
                        case FL_LICENSE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.PhysicianLicenseNum.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.PhysicianLicenseNum.Token;
                        case REV_CODE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.RevenueCode.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.RevenueCode.Token;
                        case EXPLANATION:
                            string sEOBitems = String.Empty;
                            foreach (DictionaryEntry sEOB in p_structEOBDetailRecord.EOBList)
                            {
                                if (sEOBitems == string.Empty)
                                    if(iDataLength!=0)
                                        sEOBitems = sEOB.Value.ToString().Substring(0,iDataLength);
                                    else
                                        sEOBitems = sEOB.Value.ToString();
                                else
                                    if(iDataLength!=0)
                                        sEOBitems = sEOBitems + "<BR />" + sEOB.Value.ToString().Substring(0,iDataLength);
                                    else
                                        sEOBitems = sEOBitems + "<BR />" + sEOB.Value.ToString();
                            }
                            return sEOBitems;
                        case EXPLANATION_DETAIL:
                            string sEOBitemsDetail = String.Empty;
                            foreach (DictionaryEntry sEOB in p_structEOBDetailRecord.EOBListDetails)
                            {
                                if (sEOBitemsDetail == string.Empty)
                                    if (iDataLength != 0)
                                        sEOBitemsDetail = sEOB.Value.ToString().Substring(0, iDataLength);
                                    else
                                        sEOBitemsDetail = sEOB.Value.ToString();
                                else
                                    if (iDataLength != 0)
                                        sEOBitemsDetail = sEOBitemsDetail + "<BR />" + sEOB.Value.ToString().Substring(0, iDataLength);
                                    else
                                        sEOBitemsDetail = sEOBitemsDetail + "<BR />" + sEOB.Value.ToString();
                            }
                            return sEOBitemsDetail;
                        case DIAGNOSIS:
                            string sDIAGitems = String.Empty;
                            foreach (DictionaryEntry sDIAG in p_structEOBDetailRecord.DiagnosisList)
                            {
                                if (sDIAGitems == string.Empty)
                                    if(iDataLength!=0)
                                        sDIAGitems = sDIAG.Value.ToString().Substring(0,iDataLength);
                                    else
                                        sDIAGitems = sDIAG.Value.ToString();
                                else
                                    if(iDataLength!=0)
                                        sDIAGitems = sDIAGitems + "<BR />" + sDIAG.Value.ToString().Substring(0,iDataLength);
                                    else
                                        sDIAGitems = sDIAGitems + "<BR />" + sDIAG.Value.ToString();
                            }
                            return sDIAGitems;
                        case DIAGNOSIS_CODE:
                            string sDIAGcodes = String.Empty;
                            foreach (DictionaryEntry sDIAG in p_structEOBDetailRecord.DiagnosisCodeList)
                            {
                                if (sDIAGcodes == string.Empty)
                                    if(iDataLength!=0)
                                        sDIAGcodes = sDIAG.Value.ToString().Substring(0,iDataLength);
                                    else
                                        sDIAGcodes = sDIAG.Value.ToString();
                                else
                                    if(iDataLength!=0)
                                        sDIAGcodes = sDIAGcodes + "<BR />" + sDIAG.Value.ToString().Substring(0,iDataLength);
                                    else
                                        sDIAGcodes = sDIAGcodes + "<BR />" + sDIAG.Value.ToString();
                            }
                            return sDIAGcodes;

                        case DIAGNOSIS_DETAILS:
                            string sDIAGDTL = String.Empty;
                            foreach (DictionaryEntry sDIAG in p_structEOBDetailRecord.DiagnosisDetails)
                            {
                                if (sDIAGDTL == string.Empty)
                                    if(iDataLength!=0)
                                        sDIAGDTL = sDIAG.Value.ToString().Substring(0,iDataLength);
                                    else
                                        sDIAGDTL = sDIAG.Value.ToString();
                                else
                                    if(iDataLength!=0)
                                        sDIAGDTL = sDIAGDTL + "<BR />" + sDIAG.Value.ToString().Substring(0,iDataLength);
                                    else
                                        sDIAGDTL = sDIAGDTL + "<BR />" + sDIAG.Value.ToString();
                            }
                            return sDIAGDTL;

                        case MODIFIER:
                        case MODIFIER_CODE:
                            string sMODitems = String.Empty;
                            foreach (DictionaryEntry sMOD in p_structEOBDetailRecord.ModifierList)
                            {
                                if (sMODitems == string.Empty)
                                    if(iDataLength!=0)
                                        sMODitems = sMOD.Value.ToString().Substring(0,iDataLength);
                                    else
                                        sMODitems = sMOD.Value.ToString();
                                else
                                    if(iDataLength!=0)
                                        sMODitems = sMODitems + "<BR />" + sMOD.Value.ToString().Substring(0,iDataLength);
                                    else
                                        sMODitems = sMODitems + "<BR />" + sMOD.Value.ToString();
                            }
                            return sMODitems;
                        case UNITS_BILLED:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.UnitsBilled.Token.ToString().Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.UnitsBilled.Token.ToString();
                        case AMOUNT_BILLED:
                            if (iDataLength != 0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.AmountBilled.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.AmountBilled.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.AmountBilled.Token);
                                //return p_structEOBDetailRecord.AmountBilled.Token.ToString();
                        case AMOUNT_PAID:
                            if(iDataLength!=0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.AmountPaid.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.AmountPaid.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.AmountPaid.Token);
                                //return p_structEOBDetailRecord.AmountPaid.Token.ToString();
                        case SCHEDULED_AMOUNT:
                            if (iDataLength != 0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.ScheduledAmount.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.ScheduledAmount.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.ScheduledAmount.Token);
                                //return p_structEOBDetailRecord.ScheduledAmount.Token.ToString();
                        case AMOUNT_REDUCED:
                            if (iDataLength != 0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.AmountReduced.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.AmountReduced.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.AmountReduced.Token);
                                //return p_structEOBDetailRecord.AmountReduced.Token.ToString();
                        case AMOUNT_SAVED:
                            if (iDataLength != 0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.AmountSaved.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.AmountSaved.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.AmountSaved.Token);
                                //return p_structEOBDetailRecord.AmountSaved.Token.ToString();
                        case CONTRACT_AMOUNT:
                            if(iDataLength !=0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.ContractAmount.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.ContractAmount.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.ContractAmount.Token);
                                //return p_structEOBDetailRecord.ContractAmount.Token.ToString();
                        case PROVIDER_DISCOUNT:
                            if (iDataLength != 0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.Discount.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.Discount.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.Discount.Token);
                                //return p_structEOBDetailRecord.Discount.Token.ToString();
                        case BILLING_POSTAL_CODE:
                            if(iDataLength!=0)
                                return p_structEOBDetailRecord.ProviderZipCode.Token.Substring(0,iDataLength);
                            else
                                return p_structEOBDetailRecord.ProviderZipCode.Token;
                        case BASE_AMOUNT:
                            if(iDataLength!=0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.BaseAmount.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.BaseAmount.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.BaseAmount.Token);
                                //return p_structEOBDetailRecord.BaseAmount.Token.ToString();
                        case AMOUNT_ALLOWED:
                            if(iDataLength!=0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.AmountAllowed.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.AmountAllowed.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.AmountAllowed.Token);
                                //return p_structEOBDetailRecord.AmountAllowed.Token.ToString();
                        case DISCOUNT_AMOUNT:
                            if(iDataLength!=0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.DiscountAmount.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.DiscountAmount.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.DiscountAmount.Token);    
                                //return p_structEOBDetailRecord.DiscountAmount.Token.ToString();
                        case PER_DIEM_AMOUNT:
                            if (iDataLength != 0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.PerDiemAmount.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.PerDiemAmount.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.PerDiemAmount.Token);
                                //return p_structEOBDetailRecord.PerDiemAmount.Token.ToString();
                        case STOP_LOSS_AMOUNT:
                            if (iDataLength != 0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.StopLossAmount.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.StopLossAmount.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.StopLossAmount.Token);
                                //return p_structEOBDetailRecord.StopLossAmount.Token.ToString();
                        case FEE_TABLE_AMOUNT:
                            if (iDataLength != 0)
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.FeeTableAmount.Token).Substring(0, iDataLength);
                                //return p_structEOBDetailRecord.FeeTableAmount.Token.ToString().Substring(0,iDataLength);
                            else
                                return string.Format("{0:#,###,##0.00}", p_structEOBDetailRecord.FeeTableAmount.Token);
                                //return p_structEOBDetailRecord.FeeTableAmount.Token.ToString();
                        default:
                            return currentHTMLcell;
                    }
                }
            }
            else
            {
                return currentHTMLcell;
            }
        }
        #endregion
		#region "SetCurrentX(string p_sLineItem, EOBRecord p_structEOBRecord, ref PrintWrapper p_objPrintWrapper)"
		/// Name		: SetCurrentX
		/// Author		: Anurag Agarwal
		/// Date Created	: 27 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Setting the current X axis value for writing print text
		/// </summary>
		/// <param name="p_sLineItem">The Print Text</param>
		/// <param name="p_structEOBRecord">EOB Record</param>
		/// <param name="p_objPrintWrapper">object of class use to generate the PDF file</param>
		internal void SetCurrentX(string p_sLineItem, EOBRecord p_structEOBRecord, ref PrintWrapper p_objPrintWrapper)
		{
			double dCurrentX = 0;

			try
			{
				switch(p_structEOBRecord.CurrentAlign.ToUpper()) 	
				{
					case ALIGN_LEFT:
						p_objPrintWrapper.CurrentX = p_structEOBRecord.CurrentX1Margin;
						break;
					case ALIGN_CENTER:
						dCurrentX = (p_objPrintWrapper.PageWidth - p_structEOBRecord.CurrentX1Margin)/2;
						dCurrentX = p_structEOBRecord.CurrentX1Margin + dCurrentX - (p_objPrintWrapper.GetTextWidth(p_sLineItem)/2);  
						p_objPrintWrapper.CurrentX = dCurrentX;
						break;
					case ALIGN_RIGHT:
						dCurrentX = (p_objPrintWrapper.PageWidth  - p_structEOBRecord.CurrentX1Margin);
						dCurrentX = p_structEOBRecord.CurrentX1Margin + dCurrentX - p_objPrintWrapper.GetTextWidth(p_sLineItem);  
						p_objPrintWrapper.CurrentX = dCurrentX;
						break;
				}
			}//End Try
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.SetCurrentX.ErrorSet", m_iClientId), p_objEx);		//sharishkumar Jira 835		
			}
		}
		#endregion

		#region "GetValueFromConfigFile(string p_sTagName)"
		/// Name		: GetConfigKey
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the value of specified Key from config file
		/// </summary>
		/// <param name="p_sTagName">Tag Name whose value to be fetched</param>
		/// <returns>Value from config file</returns>
		public static string GetValueFromConfigFile(string p_sTagName, string p_sConnectionString, int p_iClientId)
		{
            return RMConfigurationManager.GetNameValueSectionSettings("PrintEOB", p_sConnectionString, p_iClientId)[p_sTagName];//rkaur27
		}

        /// <summary>
        /// Gets the FormatFilePath for EOBs
        /// </summary>
        /// <returns>string containing the directory path for the EOB Format File</returns>
        public static string GetFormatFilePath(int p_iClientId)
        {
            if (p_iClientId == 0)
                return RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, "EOB") + @"\";            
			else 
				return "";
        } // method: GetFormatFilePath

        /// <summary>
        /// Gets the SavePath for EOBs
        /// </summary>
        /// <returns>string containing the directory path for saving EOBs</returns>
        public static string GetSavePath()
        {
            return RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "EOB");
        } // method: GetSavePath

        

		#endregion

		#region "CheckEOBPageLength(int p_iPageWidth,PrintWrapper p_objPrintWrapper)"
		/// Name		: CheckEOBPageLength
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Get the Print EOB detail default items and fill into respective structure for printing EOB active report
		/// </summary>
		/// <param name="p_iPageWidth">Page Width</param>
		/// <param name="p_objPrintWrapper">object of class use to generate the PDF file</param>
		internal void CheckEOBPageLength(int p_iPageLength,PrintWrapper p_objPrintWrapper)
		{
			try
			{
                //Geeta 09/24/07 : Modified to fix Mits 10328
				if(p_objPrintWrapper.CurrentY  > p_iPageLength)
				{
					p_objPrintWrapper.NewPage();
					p_objPrintWrapper.CurrentX = 500;	
                    p_objPrintWrapper.CurrentY = 1000;
				}
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("Functions.CheckEOBPageLength.ErrorCheck", m_iClientId), p_objEx);
			}
		}
		#endregion

		#region "FillSpace(int p_iCount)"
		/// Name		: FillSpace
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Get the Print EOB detail default items and fill into respective structure for printing EOB active report
		/// </summary>
		/// <param name="p_iCount">Length of the spaces to be filled</param>
		/// <returns>String containing spaces</returns>
		internal static string FillSpace(int p_iCount)
		{
			return new string(' ',p_iCount);
		}
		#endregion
		
		#region "FormatAmount(double p_dAmount)"
		/// Name		: FormatAmount
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Formats the amount passed in the format #,###,##0.00
		/// </summary>
		/// <param name="p_dAmount">Amount to be formatted</param>
		/// <returns>Formatted amount</returns>
		internal static double FormatAmount(double p_dAmount,int p_iClientId)
		{
			try
			{
				return Conversion.ConvertStrToDouble(string.Format("{0:#,###,##0.00}",p_dAmount));
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("Functions.FormatAmount.ErrorFormat", p_iClientId), p_objEx);
			}
		}
		#endregion

		#region "bIsSSNMaskJurisdiction(string sShortCode)"
		/// Name		: bIsSSNMaskJurisdiction
		/// Author		: Nitesh Deedwania
		/// Date Created	: 19 july 2006		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// </summary>
		/// <param name="sShortCode">Short codes of state</param>
		/// <returns>True/False</returns>
		internal bool bIsSSNMaskJurisdiction(string sShortCode)
		{	DbReader objReader=null;
			try
			{
			   if (sShortCode.Trim()!= string.Empty )
			   {
				   string sSQLParamValues = string.Empty ;
				   string sSSNMaskJurisdictions= string.Empty ;
				   sSQLParamValues="SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='SSN_MASK_JURIS'";
				   objReader = DbFactory.GetDbReader(m_ConnectionString,sSQLParamValues);
				   if(objReader != null)
				   {
					   while(objReader.Read())
					   {
						   sSSNMaskJurisdictions=objReader.GetString("STR_PARM_VALUE");
					   }
				   }

				   if ((sSSNMaskJurisdictions.Trim()).IndexOf((sShortCode.Trim()).Substring(0,2))!=-1)
					   return true;
				   else
					   return false;
			   }
			   return false;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException (Globalization.GetString ("Functions.bIsSSNMaskJurisdiction.Error",m_iClientId),p_objEx);
			}
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }
		}
		#endregion

        #region "sPrivacyMask(string sField ,bool bMask)"
		/// Name		: sPrivacyMask
		/// Author		: Nitesh Deedwania
		/// Date Created	: 19 july 2006		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// This function takes SSN as sField parameter in either ###-##-#### or #########
		/// format and converts it to xxx-xx-#### or xxxxx####.
		/// This could be extended to other fields
		/// </summary>
		/// <param name="sField">SSN to be formated</param>
		/// <param name="bMask">True/False</param>
		/// <returns>Formatted SSN</returns>
		internal string sPrivacyMask(string sField ,bool bMask)
		{
			try
			{ 
				if (bMask)
				{	sField = sField.Trim();
					if( sField.Length == 11)
					{	//SS number is expected to have -s in it
						if (((sField.IndexOf('-')!=-1) && Conversion.IsNumeric(sField.Substring(sField.Length - 4))))  
						{
							return "XXX-XX-" + sField.Substring(sField.Length - 4);	
						}
					
					}
					else if(sField.Length == 9)
						{	//SS number is expected to have no -s in it
							if (((sField.IndexOf('-')==-1) && Conversion.IsNumeric(sField.Substring(sField.Length - 4))))  
							{
								return "XXXXX" + sField.Substring(sField.Length - 4);	
							}
						}
					
					return sField;
				}
				else
					return sField;
				
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException (Globalization.GetString ("Functions.sPrivacyMask.ErrorFormat",m_iClientId),p_objEx);
			}
		}

        #endregion
        #region "sPrivacyMask(string sField)"
        /// Name		: sPrivacyMask (overide to an existing function)
        /// Author		: Alok Singh Bisht 
        /// Date Created	:27 Sep 2010	
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// This function takes SSN as sField parameter in either ###-##-#### or #########
        /// format and converts it to xxx-xx-#### or xxxxx####.
        /// This could be extended to other fields
        /// </summary>
        /// <param name="sField">SSN to be formated</param>
        /// <returns>Formatted SSN</returns>
        internal string sPrivacyMask(string sField)
        {
            try
            {

                sField = sField.Trim();
                if (sField.Length == 11)
                {	//SS number is expected to have -s in it
                    if (((sField.IndexOf('-') != -1) && Conversion.IsNumeric(sField.Substring(sField.Length - 4))))
                    {
                        return "XXX-XX-" + sField.Substring(sField.Length - 4);
                    }

                }
                else if (sField.Length == 9)
                {	//SS number is expected to have no -s in it
                    if (((sField.IndexOf('-') == -1) && Conversion.IsNumeric(sField.Substring(sField.Length - 4))))
                    {
                        return "XXXXX" + sField.Substring(sField.Length - 4);
                    }
                }

                return sField;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Functions.sPrivacyMask.ErrorFormat", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
        }

        #endregion
        #region This function prints the mentioned PDF to the provided printer.

        /// Deb : MITS 32174
        /// <summary>
        /// PrintCheckJobNEW 
        /// </summary>
        /// <param name="filename">filename</param>
        internal void PrintCheckJobNEW(string filename)
        {
            PrintJob printJob = null;
            PrintDocument oPrintDocument = null;

            try
            {
                ceTe.DynamicPDF.Printing.PrintJob.AddLicense("PMG20NXDLGPFIAeGqB9mYQ263av9j9DzMT58X3zE3kFBh8j//PDlC9d+HqvJm5dwEi3LUmebA+afrlOpKzF8X51YS0qKWo1WY8Zg");
                printJob = new PrintJob(m_Printername, filename);
                oPrintDocument = new PrintDocument();
                string sTrayName = string.Empty;
                oPrintDocument.PrinterSettings.PrinterName = m_Printername;

                foreach (System.Drawing.Printing.PaperSource oPaperSource in oPrintDocument.PrinterSettings.PaperSources)
                {
                    if (oPaperSource.RawKind.ToString() == m_EOBBin)
                    {
                        sTrayName = oPaperSource.SourceName;
                        break;
                    }
                }
                if (sTrayName == string.Empty)
                {
                    ceTe.DynamicPDF.Printing.PaperSource paperSource = printJob.Printer.PaperSources.Automatic;
                    sTrayName = paperSource.Name;
                }
                printJob.PrintOptions.SetPaperSourceByName(sTrayName);
                //printJob.DocumentName = new FileInfo(filename).Name;
                printJob.Print();
                printJob.Dispose();
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                if (printJob != null)
                {
                    printJob.Dispose();
                }
                if (oPrintDocument != null)
                {
                    oPrintDocument.Dispose();

                }

            }
        }

        //Deb : MITS 32174 obsoleted no longer to be used instead use PrintCheckJobNEW
        // Return value:
        //     0: Successful
        //   -10: PDFPrint SDK be damaged
        //   -11: Something is wrong in command line options
        //    -4: Can't set default printer correctly
        //    -5: Can't find input PDF file(s)

        //internal string PrintCheckJob(string filename)
        //{
        //    Hashtable hashCheckSettings = RMConfigurationManager.GetDictionarySectionSettings("PrintChecks");
        //    string sprinter = string.Empty, sErrormessage = string.Empty;
        //    StringBuilder sCmd = new StringBuilder();
        //    int iret = 0;
        //    StringBuilder shortPath = new StringBuilder(500);
        //    GetShortPathName(filename, shortPath, shortPath.Capacity);
        //    filename = shortPath.ToString();
        //    sprinter = m_Printername;
        //    sCmd.Append(String.Format("pdfprint.exe -$ {0}", hashCheckSettings["LicenseKey"].ToString()));
        //    sCmd.Append(String.Format(" -printer {0} ", m_Printername));
        //    if (Convert.ToBoolean(hashCheckSettings["UseAdobeReader"].ToString()))
        //    {
        //        sCmd.Append(" -shell ");
        //    }//if
        //    if (Convert.ToBoolean(hashCheckSettings["PrinterMargins"].ToString()))
        //    {
        //        sCmd.Append(" -printermargins ");
        //    }
        //    sCmd.Append(String.Format("-chgbin {0} -restoreprinter {1}", m_EOBBin, filename));
        //    //Deb UnderWriters
        //    try
        //    {
        //        iret = VeryPDF_PDFPrint(sCmd.ToString());
        //    }
        //    catch (Exception ec)
        //    {
        //        Log.Write("Command: " + sCmd.ToString() + "------FileName: " + filename + " Stack Message: " + ec.Message);

        //    }
        //    if (iret != 0)
        //    {
        //        switch (iret)
        //        {
        //            case -10:
        //                sErrormessage = "PDFPrint SDK may be damaged or corrupt";
        //                break;
        //            case -11:
        //                sErrormessage = "Command line options may be set incorrectly";
        //                break;
        //            case -4:
        //                sErrormessage = "Cannot set the default printer correctly";
        //                break;
        //            case -5:
        //                sErrormessage = "Cannot find the input PDF file(s)";
        //                break;

        //        }
        //        throw new RMAppException("Error printing to printer, Error Code:" + iret + " ,Error Reason:" + sErrormessage);
        //    }


        //    return iret.ToString();

        //}
        #endregion

        #region Reading settings from CHECK_OPTIONS Table.
        /// Name		: ReadCheckOptions
        /// Author		: Alok Singh Bisht
        /// Date Created: 04/02/2009		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Initialize Check Options
        /// </summary>
        private void ReadCheckOptions()
        {
            DbReader objReader = null;
            string sSQL = "";
            bool bSetToDefault = true;

            try
            {
                sSQL = "SELECT * FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        m_bPrintClaimantEOB = objReader.GetBoolean("PRT_CLMNT_EOB");
                        m_bPrintPayeeEOB = objReader.GetBoolean("PRT_PAYEE_EOB");
                        m_EOBBin = objReader.GetValue("EOB_PAPER_BIN").ToString();
                        m_Printername = objReader.GetValue("PRINTER_NAME").ToString();
                        m_bDirectToPrinter = objReader.GetBoolean("DIRECT_TO_PRINTER");
                        m_bEOBDescNextLine = objReader.GetBoolean("EOB_DESC_NEXT");
                        bSetToDefault = false;
                    }
                }
                if (bSetToDefault)
                {
                    m_bPrintClaimantEOB = false;
                    m_bPrintPayeeEOB = false;
                    m_bDirectToPrinter = false;
                    m_bEOBDescNextLine = false;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckManager.ReadCheckOptions.InitcheckOptions", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }

        #endregion

        public static bool IsPrinterSelected(string p_sConnectionString, int p_iClientId)
        {

            DbReader objReader = null;
            string sPrinterName = "";
            string sDirectToPrinter = "";
            string sEOBPaperBin = "";
            try
            {
                string sSQL = "SELECT DIRECT_TO_PRINTER , PRINTER_NAME , EOB_PAPER_BIN  FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(p_sConnectionString, sSQL);

                if (objReader.Read())
                {
                    sDirectToPrinter = objReader.GetValue(0).ToString();
                    sPrinterName = objReader.GetValue(1).ToString();
                    sEOBPaperBin = objReader.GetValue(2).ToString();
                }
                objReader.Dispose();
                if (sDirectToPrinter == "-1")
                {
                    if (sPrinterName.Trim() == "" || sEOBPaperBin == "")
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckManager.SetPaymentParameterValues.Error",p_iClientId), p_objEx);
            }
            finally
            {

                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return true;
        }

        #region New function added from PrintEOBAR.cs to this file to support MI and TX eob's

        #region "IsSuppFieldExist(DbReader p_objDBReader,string p_sFieldName)"
        /// Name		: IsSuppFieldExist
        /// Author		: Anurag Agarwal
        /// Date Created	: 10 jan 2005		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Checks for the input field existance in the passed list of columns
        /// </summary>
        /// <param name="p_sFieldName">Field Name to be check</param>
        /// <param name="p_objSupplementals">List of supplimental columns</param>
        /// <returns>Flag saying that whether field exist or not</returns>
        internal bool IsSuppFieldExist(Supplementals p_objSupplementals, string p_sFieldName)
        {
            try
            {
                // akaushik5 Commented for MITS 35846 Starts
                //foreach (SupplementalField objSupplementalField in p_objSupplementals)
                //{
                //    if (objSupplementalField.FieldName == p_sFieldName)
                //        return true;
                //}

                if (!object.ReferenceEquals(p_objSupplementals, null) && p_objSupplementals.Count() > 0)
                {
                    return p_objSupplementals.Cast<SupplementalField>().Select(x => x.FieldName).Contains(p_sFieldName);
                }
                // akaushik5 Commented for MITS 35846 Ends

                return false;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAR.Initialize.ErrorInit", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
        }
        #endregion

        #region "GetEOBDetail(Funds p_objFunds,ref CEOBRecord p_structCEOBRecord, long p_lInvoiceId)"
        /// Name		: GetEOBDetail
        /// Author		: Anurag Agarwal
        /// Date Created	: 23 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Get the Print EOB detail items and fill into respective structure for printing EOB active report
        /// </summary>
        /// <param name="p_objFunds">Funds Object</param>
        /// <param name="p_structCEOBRecord">EOB Record</param>
        /// <param name="p_lInvoiceId">Invoice Id</param>
        private void GetEOBDetail(Funds p_objFunds, ref CEOBRecord p_structCEOBRecord, long p_lInvoiceId)
        {
            string sSQL = string.Empty;
            bool bIsUseRecord = false;
            DbReader objRdr = null;
            DbReader objReader = null;
            CEOBDetailRecord structCEOBDetailRecord;
            FundsTransSplit objFundsTransSplit = null;
            long lTempId = 0;
            string sStrTemp = string.Empty;
            int iNumEOB = 0;
            int iLineItem = 0;
            double dTotAmtReduced = 0.0;
            double dTotSchdAmt = 0.0;
            double dTotAmtSaved = 0.0;
            double dTotalAmountAllowed = 0.0;
            double dTotalBaseAmount = 0.0;
            double dTotalDiscountAmount = 0.0;
            double dTotalPerDiemAmt = 0.0;
            double dTotalStopLossAmt = 0.0;
            double dTotalFeeTableAmt = 0.0;
            double dTotalAmountBilled = 0.0;
            double dTotalAmountPaid = 0.0;
            int intRecordCount = 0;
            // akaushik5 Commented for MITS 35846 Starts
            //DataModelFactory objDataModelFactory = null;
            // akaushik5 Commented for MITS 35846 Ends
            try
            {
                // akaushik5 Commented for MITS 35846 Starts
                //objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword);
                // akaushik5 Commented for MITS 35846 Ends
                sSQL = "SELECT INVOICE_DETAIL_ID,BILLING_CODE_TEXT,AMOUNT_BILLED,AMOUNT_TO_PAY,FUNDS_SPLIT_ROW_ID" +
                    ",TABLE_CODE, PERCENTILE,PLACE_OF_SER_CODE,TYPE_OF_SER_CODE,UNITS_BILLED_NUM" +
                    ",SCHEDULED_AMOUNT,AMOUNT_REDUCED,AMOUNT_SAVED, QUANTITY,STORE,CONTRACT_AMOUNT" +
                    ",DISCOUNT,ZIP_CODE,BASE_AMOUNT,AMOUNT_ALLOWED,PER_DIEM_AMT,STOP_LOSS_AMT," +
                    "FEE_TABLE_AMT,FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE FROM INVOICE_DETAIL," +
                    "FUNDS_TRANS_SPLIT WHERE INVOICE_ID = " + p_lInvoiceId + " AND FUNDS_SPLIT_ROW_ID" +
                    " = SPLIT_ROW_ID ORDER BY FROM_DATE,INVOICE_DETAIL_ID";

                objRdr = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                p_structCEOBRecord.DetailRecordsList = new Hashtable();

                if (objRdr != null)
                {
                    while (objRdr.Read())
                    {
                        structCEOBDetailRecord = new CEOBDetailRecord();
                        structCEOBDetailRecord.EOBList = new Hashtable();
                        structCEOBDetailRecord.DiagnosisList = new Hashtable();
                        structCEOBDetailRecord.ModifierList = new Hashtable();

                        bIsUseRecord = false;

                        //At first check if record is deleted or not
                        lTempId = Conversion.ConvertStrToLong(objRdr["FUNDS_SPLIT_ROW_ID"].ToString());
                        // akaushik5 Changed for MITS 35846 Starts
                        //foreach (FundsTransSplit objFundTSplit in p_objFunds.TransSplitList)
                        //{
                        //    if (objFundTSplit.SplitRowId == lTempId)
                        //    {
                        //        bIsUseRecord = true;
                        //        break;
                        //    }
                        //}
                        bIsUseRecord = p_objFunds.TransSplitList.Cast<FundsTransSplit>().Select(x => x.SplitRowId).Contains((int)lTempId);
                        // akaushik5 Changed for MITS 35846 Ends

                        iNumEOB = 0;
                        if (bIsUseRecord)
                        {
                            lTempId = Conversion.ConvertStrToLong(objRdr["INVOICE_DETAIL_ID"].ToString());

                            //... Explanation description(s)
                            sSQL = "SELECT CODES_TEXT.CODE_DESC FROM INVDETAIL_X_EOB, CODES_TEXT" +
                                " WHERE INVDETAIL_X_EOB.EOB_CODE = CODES_TEXT.CODE_ID AND " +
                                "INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + lTempId + " AND CODES_TEXT.CODE_ID <> 0";
                            objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                            if (objReader != null)
                            {
                                while (objReader.Read())
                                    structCEOBDetailRecord.EOBList.Add(++iNumEOB, objReader[0].ToString().Trim());
                                objReader.Close();
                            }

                            // ... Diagnosis Code(s)
                            sSQL = "SELECT SHORT_CODE FROM INVDETAIL_X_DIAG, CODES WHERE " +
                                "INVDETAIL_X_DIAG.DIAGNOSIS_CODE=CODES.CODE_ID AND " +
                                "INVDETAIL_X_DIAG.INVOICE_DETAIL_ID=" + lTempId + " AND CODES.CODE_ID <> 0";
                            objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                            if (objReader != null)
                            {
                                while (objReader.Read())
                                    structCEOBDetailRecord.DiagnosisList.Add(++iLineItem, objReader[0].ToString().Trim());
                                objReader.Close();
                            }

                            // ... Modifier Code(s)
                            sSQL = "SELECT CODES.SHORT_CODE FROM INVDETAIL_X_MOD, CODES WHERE " +
                                "INVDETAIL_X_MOD.MODIFIER_CODE=CODES.CODE_ID AND " +
                                "INVDETAIL_X_MOD.INVOICE_DETAIL_ID=" + lTempId + " AND CODES.CODE_ID <> 0";
                            objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                            if (objReader != null)
                            {
                                while (objReader.Read())
                                    //Changed by Gagan for MITS 11520 : Start
                                    //structCEOBDetailRecord.DiagnosisList.Add(++iLineItem,objReader[0].ToString().Trim());     
                                    structCEOBDetailRecord.ModifierList.Add(++iLineItem, objReader[0].ToString().Trim());
                                //Changed by Gagan for MITS 11520 : End
                                objReader.Close();
                            }

                            // ... Fee Schedule Name
                            sSQL = "SELECT FEE_TABLES.USER_TABLE_NAME FROM FEE_TABLES WHERE FEE_TABLES.TABLE_ID = " +
                                Conversion.ConvertStrToLong(objRdr["TABLE_CODE"].ToString());
                            objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                            if (objReader != null)
                            {
                                if (objReader.Read())
                                    structCEOBDetailRecord.FeeSchedule = objReader[0].ToString().Trim();
                                objReader.Close();
                            }

                            //... Percentile
                            structCEOBDetailRecord.Percentile = objRdr["PERCENTILE"].ToString().Trim();

                            // ... Place of Service
                            
                            // akaushik5 Changed for MITS 35846 Starts
                            //sSQL = "SELECT SHORT_CODE, CODE_DESC FROM CODES_TEXT WHERE " +
                            //    "CODES_TEXT.CODE_ID = " + Conversion.ConvertStrToLong(objRdr["PLACE_OF_SER_CODE"].ToString());
                            //objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                            //if (objReader != null)
                            //{
                            //    if (objReader.Read())
                            //    {
                            //        structCEOBDetailRecord.PlaceofServiceCode = objReader["SHORT_CODE"].ToString().Trim();
                            //        structCEOBDetailRecord.PlaceOfService = objReader["CODE_DESC"].ToString().Trim();
                            //    }
                            //    objReader.Close();
                            //}
                            
                            // ... Type of Service
                            //sSQL = "SELECT SHORT_CODE, CODE_DESC FROM CODES_TEXT WHERE " +
                            //    "CODES_TEXT.CODE_ID = " + Conversion.ConvertStrToLong(objRdr["TYPE_OF_SER_CODE"].ToString());
                            //objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                            //if (objReader != null)
                            //{
                            //    if (objReader.Read())
                            //    {
                            //        structCEOBDetailRecord.TypeOfServiceCode = objReader["SHORT_CODE"].ToString().Trim();
                            //        structCEOBDetailRecord.TypeOfService = objReader["CODE_DESC"].ToString().Trim();
                            //    }
                            //    objReader.Close();
                            //}

                            // ... Trans Type
                            //sSQL = "SELECT SHORT_CODE, CODE_DESC FROM CODES_TEXT WHERE " +
                            //    "CODES_TEXT.CODE_ID = " + Conversion.ConvertStrToLong(objRdr["TRANS_TYPE_CODE"].ToString());
                            //objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                            //if (objReader != null)
                            //{
                            //    if (objReader.Read())
                            //    {
                            //        structCEOBDetailRecord.TransTypeCode = objReader["SHORT_CODE"].ToString().Trim();
                            //        structCEOBDetailRecord.TransType = objReader["CODE_DESC"].ToString().Trim();
                            //    }
                            //    objReader.Close();
                            //}

                            this.m_objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(objRdr["PLACE_OF_SER_CODE"].ToString())
                                , ref structCEOBDetailRecord.PlaceofServiceCode, ref structCEOBDetailRecord.PlaceOfService);

                            this.m_objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(objRdr["TYPE_OF_SER_CODE"].ToString())
                                , ref structCEOBDetailRecord.TypeOfServiceCode, ref structCEOBDetailRecord.TypeOfService);

                            this.m_objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(objRdr["TRANS_TYPE_CODE"].ToString())
                                , ref structCEOBDetailRecord.TransTypeCode, ref structCEOBDetailRecord.TransType);
                            // akaushik5 Changed for MITS 35846 Ends
                            structCEOBDetailRecord.UnitsBilled = Conversion.ConvertStrToDouble(objRdr["UNITS_BILLED_NUM"].ToString());
                            //round to 2 decimal places so totals match what is paid.
                            structCEOBDetailRecord.ScheduledAmount = Conversion.ConvertStrToDouble(objRdr["SCHEDULED_AMOUNT"].ToString());
                            dTotSchdAmt += Functions.FormatAmount(structCEOBDetailRecord.ScheduledAmount,m_iClientId);

                            structCEOBDetailRecord.AmountReduced = Conversion.ConvertStrToDouble(objRdr["AMOUNT_REDUCED"].ToString());
                            dTotAmtReduced += Functions.FormatAmount(structCEOBDetailRecord.AmountReduced, m_iClientId);

                            structCEOBDetailRecord.AmountSaved = Conversion.ConvertStrToDouble(objRdr["AMOUNT_SAVED"].ToString());
                            dTotAmtSaved += Functions.FormatAmount(structCEOBDetailRecord.AmountSaved, m_iClientId);

                            structCEOBDetailRecord.ContractAmount = Conversion.ConvertStrToDouble(objRdr["CONTRACT_AMOUNT"].ToString());
                            structCEOBDetailRecord.Discount = Conversion.ConvertStrToDouble(objRdr["DISCOUNT"].ToString());
                            structCEOBDetailRecord.ProviderZipCode = objRdr["ZIP_CODE"].ToString();

                            structCEOBDetailRecord.AmountAllowed = Conversion.ConvertStrToDouble(objRdr["AMOUNT_ALLOWED"].ToString());
                            dTotalAmountAllowed += Functions.FormatAmount(structCEOBDetailRecord.AmountAllowed, m_iClientId);

                            structCEOBDetailRecord.BaseAmount = Conversion.ConvertStrToDouble(objRdr["BASE_AMOUNT"].ToString());
                            dTotalBaseAmount += Functions.FormatAmount(structCEOBDetailRecord.BaseAmount, m_iClientId);

                            structCEOBDetailRecord.DiscountAmount = structCEOBDetailRecord.BaseAmount * (structCEOBDetailRecord.Discount / 100);
                            dTotalDiscountAmount += Functions.FormatAmount(structCEOBDetailRecord.DiscountAmount, m_iClientId);

                            structCEOBDetailRecord.PerDiemAmount = Conversion.ConvertStrToDouble(objRdr["PER_DIEM_AMT"].ToString());
                            dTotalPerDiemAmt += Functions.FormatAmount(structCEOBDetailRecord.PerDiemAmount, m_iClientId);

                            structCEOBDetailRecord.StopLossAmount = Conversion.ConvertStrToDouble(objRdr["STOP_LOSS_AMT"].ToString());
                            dTotalStopLossAmt += Functions.FormatAmount(structCEOBDetailRecord.StopLossAmount, m_iClientId);

                            structCEOBDetailRecord.FeeTableAmount = Conversion.ConvertStrToDouble(objRdr["FEE_TABLE_AMT"].ToString());
                            dTotalFeeTableAmt += Functions.FormatAmount(structCEOBDetailRecord.FeeTableAmount, m_iClientId);

                            // ... From/To/Invoice # of Split
                            // akaushik5 Changed for MITS 35846 Starts
                            //objFundsTransSplit = (FundsTransSplit)objDataModelFactory.GetDataModelObject("FundsTransSplit", false);
                            objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit", false);
                            // akaushik5 Changed for MITS 35846 Ends

                            try
                            {
                                objFundsTransSplit.MoveTo(Conversion.ConvertStrToInteger(objRdr["FUNDS_SPLIT_ROW_ID"].ToString()));
                                structCEOBDetailRecord.ProcedureDate = objFundsTransSplit.FromDate;
                                structCEOBDetailRecord.ProcToDate = objFundsTransSplit.ToDate;
                                structCEOBDetailRecord.InvoiceNumber = objFundsTransSplit.InvoiceNumber;
                                structCEOBDetailRecord.InvoiceDate = objFundsTransSplit.InvoiceDate;

                            }
                            catch
                            {
                                structCEOBDetailRecord.ProcedureDate = string.Empty;
                                structCEOBDetailRecord.ProcToDate = string.Empty;
                                structCEOBDetailRecord.InvoiceNumber = "N/A";
                                structCEOBDetailRecord.InvoiceDate = string.Empty;
                            }

                            sStrTemp = objRdr["BILLING_CODE_TEXT"].ToString();
                            //Deb: MITS 32303
                            if (sStrTemp.IndexOf('-') > 0)
                            {
                                string[] sTemp = sStrTemp.Split('-');
                                structCEOBDetailRecord.ProcedureCode = sTemp[0].Trim();
                                structCEOBDetailRecord.Procedure = sTemp[1].Trim();
                            }
                            else if (sStrTemp.IndexOf('-') == 0)
                            {
                                structCEOBDetailRecord.ProcedureCode = "0";
                                structCEOBDetailRecord.Procedure = sStrTemp.Substring(sStrTemp.IndexOf('-') + 1);
                            }
                            else
                            {
                                structCEOBDetailRecord.ProcedureCode = sStrTemp;
                                structCEOBDetailRecord.Procedure = sStrTemp;
                            }
							//Deb: MITS 32303
                            structCEOBDetailRecord.AmountBilled = Conversion.ConvertStrToDouble(objRdr["AMOUNT_BILLED"].ToString());
                            dTotalAmountBilled += structCEOBDetailRecord.AmountBilled;

                            structCEOBDetailRecord.AmountPaid = Conversion.ConvertStrToDouble(objRdr["AMOUNT_TO_PAY"].ToString());
                            dTotalAmountPaid += structCEOBDetailRecord.AmountPaid;

                            // Add detail record to collection
                            p_structCEOBRecord.DetailRecordsList.Add(++intRecordCount, structCEOBDetailRecord);
                        }
                    }
                    objRdr.Close();
                }

                p_structCEOBRecord.TotalAmountBilled = dTotalAmountBilled;
                p_structCEOBRecord.TotalAmountPaid = dTotalAmountPaid;
                p_structCEOBRecord.TotalScheduledAmount = dTotSchdAmt;
                p_structCEOBRecord.TotalAmountReduced = dTotAmtReduced;
                p_structCEOBRecord.TotalAmountSaved = dTotAmtSaved;
                p_structCEOBRecord.TotalBaseAmount = dTotalBaseAmount;
                p_structCEOBRecord.TotalAmountAllowed = dTotalAmountAllowed;
                p_structCEOBRecord.TotalDiscountAmount = dTotalDiscountAmount;
                p_structCEOBRecord.TotalPerDiemAmt = dTotalPerDiemAmt;
                p_structCEOBRecord.TotalStopLossAmt = dTotalStopLossAmt;
                p_structCEOBRecord.TotalFeeTableAmt = dTotalFeeTableAmt;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAR.GetEOBDetail.ErrorGet", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }

                if (objRdr != null)
                {
                    objRdr.Close();
                    objRdr.Dispose();
                }

                if (objFundsTransSplit != null)
                    objFundsTransSplit.Dispose();
                // akaushik5 Commented for MITS 35846 Starts
                //if (objDataModelFactory != null)
                //{
                //    objDataModelFactory.Dispose();
                //    objDataModelFactory = null;
                //}
                // akaushik5 Commented for MITS 35846 Ends
            }   
        }
        #endregion

        #region "GetPrintCheckEOB(Funds p_objFunds, bool p_bIsSilent, out PrintWrapper p_objPrintWrapper)"
        /// Name		: GetPrintCheckEOB
        /// Author		: Anurag Agarwal
        /// Date Created	: 10 Jan 2005		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// THis function is called to print EOB report, It uses format text file for reading report format details
        /// Format File is the Custom files of dml (dorn mark-up lang) tags <b> (like  xml/http).
        /// If a file cannot be found, a Default, internal layout format is used.
        /// </summary>
        /// <param name="p_objFunds">Funds Object</param>
        /// <param name="p_bIsSilent">Flag for enebling/disabling No Data found messages</param>
        /// <param name="p_objPrintWrapper">Output parameter as object of Prinwrapper class. This will contain the PDF doc generated</param>
        private void GetPrintCheckEOB(Funds p_objFunds, bool p_bIsSilent, out PrintWrapper p_objPrintWrapper)
        {
            string sSQL = string.Empty;
            int iTransId = 0;
            int iDeptAssignEId = 0;
            string sTempData = string.Empty;
            double dAvgTextHeight = 0.0;
            double dAvgPrintWidth = 0.0;
            int iInvoiceId = 0;
            string sName = string.Empty;
            DbReader objReader = null;
            BrsInvoice objBrsInvoice = null;
            Claim objClaim = null;
            State objState = null;
            DataSet objDataSet = null;
            Entity objEntity = null;
            Employee objEmployee = null;
            StreamReader objFormatFileStream = null;
            LocalCache objLocalCache = null;
            string strStateDesc = string.Empty;
            string strStateCode = string.Empty;
            string sJurisdiction = string.Empty;
            int iStateId = 0;
            string sClaimantFileName = string.Empty;
            string sPayeeFileName = string.Empty;
            bool bIsFormatFileFound = false;
            int iClaimantEid = 0;
            string sTransDateText = string.Empty;
            string sClaimNumberText = string.Empty;
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            string sClaimantName = string.Empty;
            string sRegClaimantName = string.Empty;
            string sClaimantAddr1 = string.Empty;
            string sClaimantAddr2 = string.Empty;
            string sClaimantAddr3 = string.Empty;
            string sClaimantAddr4 = string.Empty;
            string sClaimantCity = string.Empty;
            string sClaimantTaxId = string.Empty;
            string sAddr1 = string.Empty;
            string sAddr2 = string.Empty;
            string sAddr3 = string.Empty;
            string sAddr4 = string.Empty;
            string sCity = string.Empty;
            //skhare7 MITS 23373
            int iDivisionId = 0;
            int iFacilityId = 0;
            int iLocationId = 0;
            int iRegionId = 0;
            int iOerationId = 0;
            int iCompId = 0;
            int iClientId = 0;
            string sLOBCode = string.Empty;
            int iDeptTableId = 0;
            //skhare7 MITS 23373 End
            DataModelFactory objDataModelFactory = null;
            Common objCommon = null;
            try
            {

                //initialize the out parameter
                p_objPrintWrapper = null;
                objCommon = new Common();
                objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId);//sharishkumar Jira 835
                m_objPrintWrapper = new PrintWrapper(m_iClientId);
                m_objPrintWrapper.StartDoc(false);

                dAvgTextHeight = m_objPrintWrapper.GetTextHeight("W");
                dAvgPrintWidth = m_objPrintWrapper.PageWidth;

                m_objFunds = p_objFunds;
                iTransId = m_objFunds.TransId;
                if (iTransId == 0)
                {
                    if (!p_bIsSilent)
                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.SaveItem", m_iClientId));//sharishkumar Jira 835
                    return;
                }

                objBrsInvoice = (BrsInvoice)objDataModelFactory.GetDataModelObject("BrsInvoice", false);
                try
                {
                    objBrsInvoice.MoveToTransId(iTransId);
                }
                catch
                {
                    if (!p_bIsSilent)
                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems", m_iClientId));//sharishkumar Jira 835
                    return;
                }

                iInvoiceId = objBrsInvoice.InvoiceId;
                sName = objBrsInvoice.FirstNameText + " " + objBrsInvoice.LastNameText;

                sSQL = "SELECT FL_LICENSE,ZIP_CODE FROM INVOICE_DETAIL WHERE INVOICE_ID=" + iInvoiceId;
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        m_structEOBRecord.FirstLicense = objReader["FL_LICENSE"].ToString();
                        m_structEOBRecord.FirstBillingPostalCode = objReader["ZIP_CODE"].ToString();
                    }

                }
                if (objReader != null)
                    objReader.Close();

                sSQL = "SELECT INVOICE_NUMBER FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID=" + iTransId;
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        m_structEOBRecord.FirstInvoice = objReader["INVOICE_NUMBER"].ToString();
                    }

                }
                if (objReader != null)
                    objReader.Close();
                sSQL = null;

                objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);

                // Vaibhav has changed the code for printing EOb reports for those patyments which are not 
                // attached to Claims.
                if (m_objFunds.ClaimId > 0)
                    objClaim.MoveTo(m_objFunds.ClaimId);
                //skhare7 MITS 23373
                sLOBCode = objClaim.LineOfBusCode.ToString();
                //skhare7 MITS 23373 End

                iStateId = objClaim.FilingStateId;
                objClaim.Dispose();

                objState = (State)objDataModelFactory.GetDataModelObject("State", false);
                if (iStateId > 0)
                    objState.MoveTo(iStateId);
                sJurisdiction = objState.StateId;
                objState.Dispose();

                if (m_bPrintClaimantEOB)
                {
                    sClaimantFileName = Functions.GetFormatFilePath(m_iClientId)
                        + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMFILE, m_ConnectionString, m_iClientId);//rkaur27

                    if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true" && m_iClientId == 0)
                    {
                        if ((sJurisdiction == string.Empty) || !File.Exists(sClaimantFileName))
                            sClaimantFileName = Functions.GetFormatFilePath(m_iClientId)
                                 + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMFILE, m_ConnectionString, m_iClientId);//rkaur27
                    }
                }

                if (m_bPrintPayeeEOB)
                {
                    sPayeeFileName = Functions.GetFormatFilePath(m_iClientId)
                        + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEFILE, m_ConnectionString, m_iClientId);//rkaur27

                    if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true" && m_iClientId == 0)
                    {
                        if ((sJurisdiction == string.Empty) || !File.Exists(sPayeeFileName))
                            sPayeeFileName = Functions.GetFormatFilePath(m_iClientId)
                                + Functions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEFILE, m_ConnectionString, m_iClientId);//rkaur27
                    }
                }

                bIsFormatFileFound = true;
                if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true" && m_iClientId ==0)
                {
                    if (!File.Exists(sClaimantFileName) && m_bPrintClaimantEOB)
                        bIsFormatFileFound = false;

                    if (bIsFormatFileFound)
                        if (!File.Exists(sPayeeFileName) && m_bPrintPayeeEOB)
                            bIsFormatFileFound = false;
                }
                else
                {
                    MemoryStream objMemDesc = CommonFunctions.RetreiveTempFileFromDB(sClaimantFileName, "", m_ConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                    if (objMemDesc == null && m_bPrintClaimantEOB)
                    {
                        bIsFormatFileFound = false;
                    }
                    else
                    {
                        bIsFormatFileFound = true;
                    }
                    if (bIsFormatFileFound && m_bPrintPayeeEOB)
                    {
                        objMemDesc = CommonFunctions.RetreiveTempFileFromDB(sPayeeFileName, "", m_ConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                        if (objMemDesc == null)
                        {
                            bIsFormatFileFound = false;
                        }
                    }
                }

                objLocalCache = new LocalCache(m_ConnectionString, m_iClientId);

                if (bIsFormatFileFound)
                {
                    m_structEOBRecord.CurrentX1Margin = 0;
                    m_structEOBRecord.CurrentX2Margin = 0;
                    m_structEOBRecord.CurrentY1Margin = 0;
                    m_structEOBRecord.CurrentY2Margin = 0;
                    m_structEOBRecord.CurrentAlign = Functions.ALIGN_LEFT;
                    m_structEOBRecord.PayeeFirstName = objBrsInvoice.FirstNameText;
                    m_structEOBRecord.PayeeLastName = objBrsInvoice.LastNameText;
                    m_structEOBRecord.PayeeFullName = sName.Trim();
                    m_structEOBRecord.PayeeAddr1 = objBrsInvoice.Addr1Text;
                    m_structEOBRecord.PayeeAddr2 = objBrsInvoice.Addr2Text;
                    m_structEOBRecord.PayeeAddr3 = objBrsInvoice.Addr3Text;
                    m_structEOBRecord.PayeeAddr4 = objBrsInvoice.Addr4Text;
                    objLocalCache.GetStateInfo(objBrsInvoice.StateId, ref strStateCode, ref strStateDesc);
                    //Deb : MITS 32303
                    //m_structEOBRecord.PayeeState = ((string)(strStateCode + " " + strStateDesc)).Trim();
                    m_structEOBRecord.PayeeState = ((string)(strStateCode + " ")).Trim();
                    //Deb : MITS 32303
                    m_structEOBRecord.PayeePostalCode = objBrsInvoice.PostalCodeText;
                    m_structEOBRecord.PayeeCity = objBrsInvoice.CityText;
                    m_structEOBRecord.ClaimNumber = objBrsInvoice.ClaimNumberText;
                    m_structEOBRecord.OrigInvDate = Conversion.GetDate(objBrsInvoice.DttmRcdAdded);
                    m_structEOBRecord.OrigInvUser = objBrsInvoice.AddedByUser;
                }
                iClaimantEid = objBrsInvoice.ClaimantEid;
                sTransDateText = objBrsInvoice.TransDateText;
                sClaimNumberText = objBrsInvoice.ClaimNumberText;
                sAddr1 = objBrsInvoice.Addr1Text;
                sAddr2 = objBrsInvoice.Addr2Text;
                sAddr3 = objBrsInvoice.Addr3Text;
                sAddr4 = objBrsInvoice.Addr4Text;
                sCity = objBrsInvoice.CityText;

                objBrsInvoice.Dispose();

                objEntity = (Entity)objDataModelFactory.GetDataModelObject("Entity", false);
                
                if (bIsFormatFileFound)
                {
                    try
                    {
                        objEntity.MoveTo(m_objFunds.PayeeEid);
                        m_structEOBRecord.PayeeTaxID = objEntity.TaxId;
                    }
                    catch
                    {
                        m_structEOBRecord.PayeeTaxID = string.Empty;
                    }

                    objEmployee = (Employee)objDataModelFactory.GetDataModelObject("Employee", false);
                    try
                    {
                        objEmployee.MoveTo(iClaimantEid);
                        iDeptAssignEId = objEmployee.DeptAssignedEid;
                        objEntity.MoveTo(iDeptAssignEId);
                        //skhare7 MITS 23373
                        iDeptTableId = objEntity.EntityTableId;
                        //skhare7 MITS 23373 End
                        m_structEOBRecord.EmpDeptCode = objEntity.Abbreviation;
                        m_structEOBRecord.EmpDepartment = objEntity.LastName;
                    }
                    catch
                    {
                        m_structEOBRecord.EmpDeptCode = string.Empty;
                        m_structEOBRecord.EmpDepartment = string.Empty;
                    }
                    objEmployee.Dispose();
                    objEntity.Dispose();

                    sSQL = "SELECT DATE_OF_EVENT, DATE_REPORTED, RPTD_BY_EID FROM EVENT,CLAIM WHERE " +
                        "CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + m_objFunds.ClaimId;
                    objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            m_structEOBRecord.EventDate = objReader["DATE_OF_EVENT"].ToString();
                            m_structEOBRecord.DateReported = objReader["DATE_REPORTED"].ToString();
                        }
                        objReader.Close();
                    }
                    //skhare7 MITS 23373
                    sSQL = "SELECT ENTITY_ID,LAST_NAME,FACILITY_EID,LOCATION_EID,DIVISION_EID,REGION_EID,OPERATION_EID" +
                        ",COMPANY_EID,CLIENT_EID FROM ENTITY, ORG_HIERARCHY WHERE DEPARTMENT_EID=" + iDeptAssignEId;
                    objDataSet = DbFactory.GetDataSet(m_ConnectionString, sSQL,m_iClientId);//sharishkumar Jira 835
                    if (objDataSet != null && objDataSet.Tables.Count > 0)
                    {
                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("FACILITY_EID=ENTITY_ID"))
                        {
                            m_structEOBRecord.EmpFacility = objDataRow["LAST_NAME"].ToString();
                            iDivisionId = Conversion.ConvertStrToInteger(objDataRow["DIVISION_EID"].ToString());
                        }

                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("LOCATION_EID=ENTITY_ID"))
                        {
                            m_structEOBRecord.EmpLocation = objDataRow["LAST_NAME"].ToString();
                            iLocationId = Conversion.ConvertStrToInteger(objDataRow["LOCATION_EID"].ToString());
                        }

                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("DIVISION_EID=ENTITY_ID"))
                        {
                            m_structEOBRecord.EmpDivision = objDataRow["LAST_NAME"].ToString();
                            iDivisionId = Conversion.ConvertStrToInteger(objDataRow["DIVISION_EID"].ToString());
                        }
                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("REGION_EID=ENTITY_ID"))
                        {
                            m_structEOBRecord.EmpRegion = objDataRow["LAST_NAME"].ToString();
                            iRegionId = Conversion.ConvertStrToInteger(objDataRow["REGION_EID"].ToString());
                        }

                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("OPERATION_EID=ENTITY_ID"))
                        {
                            m_structEOBRecord.EmpOperation = objDataRow["LAST_NAME"].ToString();
                            iOerationId = Conversion.ConvertStrToInteger(objDataRow["OPERATION_EID"].ToString());
                        }
                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("COMPANY_EID=ENTITY_ID"))
                        {
                            m_structEOBRecord.EmpCompany = objDataRow["LAST_NAME"].ToString();
                            iCompId = Conversion.ConvertStrToInteger(objDataRow["COMPANY_EID"].ToString());
                        }
                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("CLIENT_EID=ENTITY_ID"))
                        {
                            m_structEOBRecord.EmpClient = objDataRow["LAST_NAME"].ToString();
                            iClientId = Conversion.ConvertStrToInteger(objDataRow["CLIENT_EID"].ToString());
                        }
                    }
                    //skhare7 MITS 23373 End
                    if (objDataSet != null)
                        objDataSet.Dispose();

                    sSQL = "SELECT LAST_NAME,FIRST_NAME,ABBREVIATION,CURRENT_ADJ_FLAG,PHONE1 FROM ENTITY," +
                        "CLAIM_ADJUSTER WHERE ADJUSTER_EID=ENTITY_ID AND CLAIM_ID=" + m_objFunds.ClaimId;
                    objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            m_structEOBRecord.AdjusterAbbrev = objReader["ABBREVIATION"].ToString();
                            m_structEOBRecord.AdjusterFullName = ((string)(objReader["FIRST_NAME"].ToString() + " " + objReader["LAST_NAME"].ToString())).Trim();
                            m_structEOBRecord.AdjusterLastName = objReader["LAST_NAME"].ToString();
                            //Arnab: MITS-10722 - Getting Adjuster phone no
                            m_structEOBRecord.AdjusterPhone1 = Conversion.ConvertObjToStr(objReader["PHONE1"]);
                            //MITS-10722 End
                            if (Conversion.ConvertObjToInt(objReader["CURRENT_ADJ_FLAG"], m_iClientId) != 0)
                                break;
                        }
                        objReader.Close();
                    }
                }
                //Make sure we have detail items (exit if none) //Deb UnderWriters
                sSQL = "SELECT COUNT(INVOICE_ID) FROM INVOICE_DETAIL WHERE INVOICE_ID = " + iInvoiceId;
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        if (Conversion.ConvertObjToInt(objReader[0], m_iClientId) == 0)
                        {
                            if (!p_bIsSilent)
                                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems", m_iClientId));//sharishkumar Jira 835
                            return;
                        }
                    }
                    objReader.Close();
                }
                //skhare7 MITS 23373 employer add
                try
                {
                    objEntity.MoveTo(iCompId);
                    m_structEOBRecord.EMP_ADDRESS1 = objEntity.Addr1 + " " + objEntity.Addr2 + " " + objEntity.Addr3 + " " + objEntity.Addr4;



                }
                catch
                {
                    m_structEOBRecord.EMP_ADDRESS1 = "";
                }

                //division add
                try
                {
                    objEntity.MoveTo(iDivisionId);
                    m_structEOBRecord.EMP_DIV_ADDR1 = objEntity.Addr1;
                    m_structEOBRecord.EMP_DIV_ADDR2 = objEntity.Addr2;
                    m_structEOBRecord.EMP_DIV_ADDR3 = objEntity.Addr3;
                    m_structEOBRecord.EMP_DIV_ADDR4 = objEntity.Addr4;
                    //Ashish Ahuja Mits 33681 : Start
                    m_structEOBRecord.EMP_DIV_CITY = objEntity.City;
                    objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                    m_structEOBRecord.EMP_DIV_STATE = ((string)(strStateCode + " " + strStateDesc)).Trim();
                    m_structEOBRecord.EMP_DIV_POSTAL_CODE = objEntity.ZipCode;
                    //Ashish Ahuja Mits 33681 : End

                }
                catch
                {
                    m_structEOBRecord.EMP_DIV_ADDR1 = "";
                    m_structEOBRecord.EMP_DIV_ADDR2 = "";
                    m_structEOBRecord.EMP_DIV_ADDR3 = "";
                    m_structEOBRecord.EMP_DIV_ADDR4 = "";

                }

                //Region add
                try
                {
                    objEntity.MoveTo(iRegionId);
                    m_structEOBRecord.EMP_REG_ADDR1 = objEntity.Addr1;
                    m_structEOBRecord.EMP_REG_ADDR2 = objEntity.Addr2;
                    m_structEOBRecord.EMP_REG_ADDR3 = objEntity.Addr3;
                    m_structEOBRecord.EMP_REG_ADDR4 = objEntity.Addr4;

                }
                catch
                {
                    m_structEOBRecord.EMP_REG_ADDR1 = "";
                    m_structEOBRecord.EMP_REG_ADDR2 = "";
                    m_structEOBRecord.EMP_REG_ADDR3 = "";
                    m_structEOBRecord.EMP_REG_ADDR4 = "";

                }
                //company add
                try
                {
                    objEntity.MoveTo(iCompId);
                    m_structEOBRecord.EMP_COMP_ADDR1 = objEntity.Addr1;
                    m_structEOBRecord.EMP_COMP_ADDR2 = objEntity.Addr2;
                    m_structEOBRecord.EMP_COMP_ADDR3 = objEntity.Addr3;
                    m_structEOBRecord.EMP_COMP_ADDR4 = objEntity.Addr4;

                }
                catch
                {
                    m_structEOBRecord.EMP_COMP_ADDR1 = "";
                    m_structEOBRecord.EMP_COMP_ADDR2 = "";
                    m_structEOBRecord.EMP_COMP_ADDR3 = "";
                    m_structEOBRecord.EMP_COMP_ADDR4 = "";

                }
                //facility add
                try
                {
                    objEntity.MoveTo(iFacilityId);
                    m_structEOBRecord.EMP_FAC_ADDR1 = objEntity.Addr1;
                    m_structEOBRecord.EMP_FAC_ADDR2 = objEntity.Addr2;
                    m_structEOBRecord.EMP_FAC_ADDR3 = objEntity.Addr3;
                    m_structEOBRecord.EMP_FAC_ADDR4 = objEntity.Addr4;

                }
                catch
                {
                    m_structEOBRecord.EMP_FAC_ADDR1 = "";
                    m_structEOBRecord.EMP_FAC_ADDR2 = "";
                    m_structEOBRecord.EMP_FAC_ADDR3 = "";
                    m_structEOBRecord.EMP_FAC_ADDR4 = "";

                }
                //client add
                try
                {
                    objEntity.MoveTo(iClientId);
                    m_structEOBRecord.EMP_CLIENT_ADDR1 = objEntity.Addr1;
                    m_structEOBRecord.EMP_CLIENT_ADDR2 = objEntity.Addr2;
                    m_structEOBRecord.EMP_CLIENT_ADDR3 = objEntity.Addr3;
                    m_structEOBRecord.EMP_CLIENT_ADDR4 = objEntity.Addr4;

                }
                catch
                {
                    m_structEOBRecord.EMP_CLIENT_ADDR1 = "";
                    m_structEOBRecord.EMP_CLIENT_ADDR2 = "";
                    m_structEOBRecord.EMP_CLIENT_ADDR3 = "";
                    m_structEOBRecord.EMP_CLIENT_ADDR4 = "";
                }
                //op add
                try
                {
                    objEntity.MoveTo(iOerationId);
                    m_structEOBRecord.EMP_OP_ADDR1 = objEntity.Addr1;
                    m_structEOBRecord.EMP_OP_ADDR2 = objEntity.Addr2;
                    m_structEOBRecord.EMP_OP_ADDR3 = objEntity.Addr3;
                    m_structEOBRecord.EMP_OP_ADDR4 = objEntity.Addr4;

                }
                catch
                {
                    m_structEOBRecord.EMP_OP_ADDR1 = "";
                    m_structEOBRecord.EMP_OP_ADDR2 = "";
                    m_structEOBRecord.EMP_OP_ADDR3 = "";
                    m_structEOBRecord.EMP_OP_ADDR4 = "";
                }
                //op add
                try
                {
                    objEntity.MoveTo(iLocationId);
                    m_structEOBRecord.EMP_LOC_ADDR1 = objEntity.Addr1;
                    m_structEOBRecord.EMP_LOC_ADDR2 = objEntity.Addr2;
                    m_structEOBRecord.EMP_LOC_ADDR3 = objEntity.Addr3;
                    m_structEOBRecord.EMP_LOC_ADDR4 = objEntity.Addr4;

                }
                catch
                {
                    m_structEOBRecord.EMP_LOC_ADDR1 = "";
                    m_structEOBRecord.EMP_LOC_ADDR2 = "";
                    m_structEOBRecord.EMP_LOC_ADDR3 = "";
                    m_structEOBRecord.EMP_LOC_ADDR4 = "";
                }
                //Dept add
                try
                {
                    objEntity.MoveTo(iDeptAssignEId);
                    m_structEOBRecord.EMP_DEPT_ADDR1 = objEntity.Addr1;
                    m_structEOBRecord.EMP_DEPT_ADDR2 = objEntity.Addr2;
                    m_structEOBRecord.EMP_DEPT_ADDR3 = objEntity.Addr3;
                    m_structEOBRecord.EMP_DEPT_ADDR4 = objEntity.Addr4;

                }
                catch
                {
                    m_structEOBRecord.EMP_DEPT_ADDR1 = "";
                    m_structEOBRecord.EMP_DEPT_ADDR2 = "";
                    m_structEOBRecord.EMP_DEPT_ADDR3 = "";
                    m_structEOBRecord.EMP_DEPT_ADDR4 = "";
                }


                //skhare7 MITS 23373 end
                //Lookup Claimant Name/Address info
                try
                {
                    objEntity.MoveTo(iClaimantEid);

                    sFirstName = objEntity.FirstName;
                    sLastName = objEntity.LastName;
                    sClaimantName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
                    sRegClaimantName = ((string)(sFirstName + " " + sLastName)).Trim();
                    sClaimantAddr1 = objEntity.Addr1;
                    sClaimantAddr2 = objEntity.Addr2;
                    sClaimantAddr3 = objEntity.Addr3;
                    sClaimantAddr4 = objEntity.Addr4;
                    objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                    m_structEOBRecord.ClaimantState = ((string)(strStateCode + " " + strStateDesc)).Trim();
                    m_structEOBRecord.ClaimantCity = objEntity.City;
                    m_structEOBRecord.ClaimantPostalCode = objEntity.ZipCode;
                    sClaimantCity = m_structEOBRecord.ClaimantCity + ", " + m_structEOBRecord.ClaimantState
                        + " " + m_structEOBRecord.ClaimantPostalCode;
                    sClaimantTaxId = objEntity.TaxId;

                    if (bIsFormatFileFound)
                    {
                        m_structEOBRecord.ClaimantFullName = sClaimantName;
                        m_structEOBRecord.ClaimantLastName = sLastName;
                        m_structEOBRecord.ClaimantFirstName = sFirstName;
                        m_structEOBRecord.ClaimantAddr1 = sClaimantAddr1;
                        m_structEOBRecord.ClaimantAddr2 = sClaimantAddr2;
                        m_structEOBRecord.ClaimantAddr3 = sClaimantAddr3;
                        m_structEOBRecord.ClaimantAddr4 = sClaimantAddr4;
                        m_structEOBRecord.TransDate = sTransDateText;
                        m_structEOBRecord.ClaimantSSN = sClaimantTaxId;
                    }
                }
                catch
                {
                    if (bIsFormatFileFound)
                    {
                        m_structEOBRecord.ClaimantFullName = string.Empty;
                        m_structEOBRecord.ClaimantLastName = string.Empty;
                        m_structEOBRecord.ClaimantFirstName = string.Empty;
                        m_structEOBRecord.ClaimantAddr1 = string.Empty;
                        m_structEOBRecord.ClaimantAddr2 = string.Empty;
                        m_structEOBRecord.ClaimantAddr3 = string.Empty;
                        m_structEOBRecord.ClaimantAddr4 = string.Empty;
                        m_structEOBRecord.TransDate = string.Empty;
                        m_structEOBRecord.ClaimantSSN = string.Empty;
                    }
                }
                //skhare7 MITS 23373
                m_structEOBRecord.EMP_CERT_NUM = GetOrganizationLevelInsuredNo(iDeptAssignEId,iTransId, iDeptTableId, iStateId, m_structEOBRecord.EventDate, sLOBCode);
                //skhare7 MITS 23373 End
                objEntity.Dispose();

                //abisht
                if (m_bPrintPayeeEOB || (m_bPrintClaimantEOB && iClaimantEid != 0))
                {
                    sTempData = m_objPrintWrapper.FontName;
                    m_objPrintWrapper.SetFont("Courier New");
                }

                //This prints the default EOB Form
                if (!bIsFormatFileFound)
                {
                    if (m_bPrintPayeeEOB)
                    {
                        this.GetDefaultEOBHeader(sRegClaimantName, sClaimantName, sClaimantAddr1, sClaimantAddr2, sClaimantAddr3, sClaimantAddr4
                            , sClaimantCity, sName, sClaimNumberText, false);
                        this.GetDefaultEOBDetail(iInvoiceId, sName, sClaimantName, false);
                    }
                    //Claimant Copy
                    if (m_bPrintClaimantEOB)
                    {
                        if (m_bPrintPayeeEOB)
                            m_objPrintWrapper.NewPage();
                        this.GetDefaultEOBHeader(sRegClaimantName, sClaimantName, sClaimantAddr1, sClaimantAddr2, sClaimantAddr3, sClaimantAddr4
                            , sClaimantCity, sName, sClaimNumberText, true);
                        this.GetDefaultEOBDetail(iInvoiceId, sName, sClaimantName, true);
                    }
                }
                else
                {
                    if (m_bPrintClaimantEOB)
                    {
                        //Print Custom File
                        m_structEOBRecord.FirstPrint = true;
                        if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true" && m_iClientId != 0)
                        {
                            MemoryStream objMemoryStream = CommonFunctions.RetreiveTempFileFromDB(sClaimantFileName, "", m_ConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                            objFormatFileStream = new StreamReader(objMemoryStream);
                        }
                        else
                        {
                            objFormatFileStream = File.OpenText(sClaimantFileName);
                        }

                        //Custom
                        GetCustomEOBHeader(objFormatFileStream, ref m_structEOBRecord, m_objPrintWrapper, m_objFunds);

                        //Print body of letter
                        GetColumns(objFormatFileStream, ref m_structEOBDetailRecord);
                        this.GetPrintCheckEOBBody(iInvoiceId, sClaimNumberText, sClaimantName, sName, string.Empty, bIsFormatFileFound, 0, 0);

                        //*** Custom footer ***
                        GetCustomEOBHeader(objFormatFileStream, ref m_structEOBRecord, m_objPrintWrapper, m_objFunds);

                        objFormatFileStream.Close();

                    }

                    if (m_bPrintPayeeEOB)
                    {
                        if (m_bPrintClaimantEOB)
                            m_objPrintWrapper.NewPage();
                        m_structEOBRecord.FirstPrint = true;
                        if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true" && m_iClientId != 0)
                        {
                            MemoryStream objMemoryStream = CommonFunctions.RetreiveTempFileFromDB(sPayeeFileName, "", m_ConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                            objFormatFileStream = new StreamReader(objMemoryStream);
                        }
                        else
                        {
                            objFormatFileStream = File.OpenText(sPayeeFileName);
                        }

                        //Custom
                        GetCustomEOBHeader(objFormatFileStream, ref m_structEOBRecord, m_objPrintWrapper, m_objFunds);

                        //Print body of letter
                        GetColumns(objFormatFileStream, ref m_structEOBDetailRecord);
                        this.GetPrintCheckEOBBody(iInvoiceId, sClaimNumberText, sClaimantName, sName, string.Empty, bIsFormatFileFound, 0, 0);

                        //*** Custom footer ***
                        GetCustomEOBHeader(objFormatFileStream, ref m_structEOBRecord, m_objPrintWrapper, m_objFunds);

                        objFormatFileStream.Close();
                    }
                }

                m_objPrintWrapper.EndDoc();
                //abisht
                if (m_bPrintPayeeEOB || m_bPrintClaimantEOB)
                    p_objPrintWrapper = m_objPrintWrapper;

                if (sTempData != string.Empty)
                    m_objPrintWrapper.SetFont(sTempData);

                //Stamp audit info on invoice record
                sSQL = "UPDATE INVOICE SET DTTM_EOB_PRINTED = '" + Conversion.ToDbDateTime(DateTime.Now) + "', " +
                    "EOB_PRINTED_USER = '" + objCommon.m_stLoginInfo.Username + "' WHERE INVOICE_ID = " + iInvoiceId;
                objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);

                m_objColumnsSelected.Clear();
                m_objSpaceWidth.Clear();
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAR.GetPrintCheckEOB.ErrorGet", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objClaim != null)
                    objClaim.Dispose();

                if (objState != null)
                    objState.Dispose();

                if (objDataSet != null)
                    objDataSet.Dispose();

                if (objEmployee != null)
                    objEmployee.Dispose();

                if (objReader != null)
                    objReader.Dispose();

                if (objBrsInvoice != null)
                    objBrsInvoice.Dispose();

                if (objEntity != null)
                    objEntity.Dispose();

                if (objFormatFileStream != null)
                    objFormatFileStream = null;

                if (objLocalCache != null)
                    objLocalCache.Dispose();
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                    objDataModelFactory = null;
                }
            }
        }
        #endregion

        #region "GetDefaultEOBHeader(string p_sRegClaimantName,string p_sClaimantName,string p_sClaimantAddr1,string p_sClaimantAddr2,string p_sClaimantAddr3,string p_sClaimantAddr4,string p_sClaimantCityStateZip,string p_sOtherName,string p_sClaimNumber, bool p_bIsClaimantCopy)"
        /// Name		: GetDefaultEOBHeader
        /// Author		: Anurag Agarwal
        /// Date Created	: 10 jan 2005		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Get the Print EOB Default Format Header
        /// </summary>
        /// <param name="p_sRegClaimantName">Reg Claimant Name</param>
        /// <param name="p_sClaimantName">Claimant Name</param>
        /// <param name="p_sClaimantAddr1">Claimant Address1</param>
        /// <param name="p_sClaimantAddr2">Claimant Address2</param>
        /// <param name="p_sClaimantAddr3">Claimant Address3</param>
        /// <param name="p_sClaimantAddr4">Claimant Address4</param>
        /// <param name="p_sClaimantCityStateZip">Claimant City, State, Zip</param>
        /// <param name="p_sOtherName">Other Name</param>
        /// <param name="p_sClaimNumber">Claim Number</param>
        /// <param name="p_bIsClaimantCopy">Claimant Copy</param>
        private void GetDefaultEOBHeader(string p_sRegClaimantName, string p_sClaimantName, string p_sClaimantAddr1, string p_sClaimantAddr2, string p_sClaimantAddr3, string p_sClaimantAddr4, string p_sClaimantCityStateZip, string p_sOtherName, string p_sClaimNumber, bool p_bIsClaimantCopy)
        {
            double dAvgTextHeight = 0.0;
            double dCurrX = 0;
            double dCurrY = 0;

            dAvgTextHeight = m_objPrintWrapper.GetTextHeight("W");

            try
            {
                if (!p_bIsClaimantCopy)
                {
                    m_objPrintWrapper.SetFont(12);
                    m_objPrintWrapper.SetFontBold(true);
                    m_objPrintWrapper.CurrentY = 1250;
                    m_objPrintWrapper.CurrentX = 3850;
                    //rsharma220 MITS 33704 Start
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBHeader.EOB", m_iClientId)));//sharishkumar Jira 835
                }

                m_objPrintWrapper.SetFont(10);
                m_objPrintWrapper.SetFontBold(false);

                if (p_bIsClaimantCopy)
                {
                    m_objPrintWrapper.CurrentX = dCurrX;
                    dCurrY += 800;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.RE", m_iClientId)));//sharishkumar Jira 835
                    m_objPrintWrapper.CurrentX = dCurrX + 500;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText",m_iClientId)) + Functions.FillSpace(1) + p_sClaimNumber);//sharishkumar Jira 835
                    m_objPrintWrapper.CurrentX = dCurrX + 500;
                    dCurrY += dAvgTextHeight;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName",m_iClientId)) + Functions.FillSpace(1) + p_sClaimantName);//sharishkumar Jira 835
                    m_objPrintWrapper.CurrentX = dCurrX;
                    m_objPrintWrapper.CurrentY = dCurrY + dAvgTextHeight * 2;
                    dCurrY += dAvgTextHeight;
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.Name",m_iClientId)) + Functions.FillSpace(1) + p_sOtherName + ",");//sharishkumar Jira 835
                }
                else
                {
                    dCurrX = 500;
                    dCurrY = 2000;
                    m_objPrintWrapper.CurrentX = dCurrX;
                    m_objPrintWrapper.CurrentY = dCurrY;

                    m_objPrintWrapper.PrintText(DateTime.Now.ToLongDateString());
                    m_objPrintWrapper.CurrentX = dCurrX;
                    dCurrY += dAvgTextHeight * 4;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText(p_sRegClaimantName);
                    m_objPrintWrapper.CurrentX = dCurrX;
                    dCurrY += dAvgTextHeight;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText(p_sClaimantAddr1);
                    if (p_sClaimantAddr2 != string.Empty)
                    {
                        m_objPrintWrapper.CurrentX = dCurrX;
                        dCurrY += dAvgTextHeight;
                        m_objPrintWrapper.CurrentY = dCurrY;
                        m_objPrintWrapper.PrintText(p_sClaimantAddr2);
                    }
                    if (p_sClaimantAddr3 != string.Empty)
                    {
                        m_objPrintWrapper.CurrentX = dCurrX;
                        dCurrY += dAvgTextHeight;
                        m_objPrintWrapper.CurrentY = dCurrY;
                        m_objPrintWrapper.PrintText(p_sClaimantAddr3);
                    }
                    if (p_sClaimantAddr4 != string.Empty)
                    {
                        m_objPrintWrapper.CurrentX = dCurrX;
                        dCurrY += dAvgTextHeight;
                        m_objPrintWrapper.CurrentY = dCurrY;
                        m_objPrintWrapper.PrintText(p_sClaimantAddr4);
                    }
                    m_objPrintWrapper.CurrentX = dCurrX;
                    dCurrY += dAvgTextHeight;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText(p_sClaimantCityStateZip);

                    m_objPrintWrapper.CurrentX = dCurrX;
                    dCurrY += dAvgTextHeight;
                    m_objPrintWrapper.CurrentY = dCurrY;

                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.RE", m_iClientId)));//sharishkumar Jira 835
                    m_objPrintWrapper.CurrentX = dCurrX + 500;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText", m_iClientId)) + Functions.FillSpace(1) + p_sClaimNumber);//sharishkumar Jira 835
                    m_objPrintWrapper.CurrentX = dCurrX + 500;
                    dCurrY += dAvgTextHeight;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName", m_iClientId)) + Functions.FillSpace(1) + p_sClaimantName);//sharishkumar Jira 835
                    m_objPrintWrapper.CurrentX = dCurrX;
                    m_objPrintWrapper.CurrentY = dCurrY + dAvgTextHeight * 2;
                    dCurrY += dAvgTextHeight;
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.Name", m_iClientId)) + Functions.FillSpace(1) + p_sOtherName + ",");//sharishkumar Jira 835
                }

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAR.GetDefaultEOBHeader.ErrorGet", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
        }
        #endregion

        #region "GetDefaultEOBDetail(int p_iInvoiceId, string p_sOtherName, string p_sClaimantName, bool p_bIsClaimantCopy)"
        /// Name		: GetDefaultEOBDetail
        /// Author		: Anurag Agarwal
        /// Date Created	: 10 jan 2005		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Get the Print EOB detail default items and fill into respective structure for printing EOB active report
        /// </summary>
        /// <param name="p_iInvoiceId">Invloice Id</param>
        /// <param name="p_sOtherName">Other Name</param>
        /// <param name="p_sClaimantName">Claimant Name</param>
        /// <param name="p_bIsClaimantCopy">Flag for getting the details of claimant</param>
        private void GetDefaultEOBDetail(int p_iInvoiceId, string p_sOtherName, string p_sClaimantName, bool p_bIsClaimantCopy)
        {
            string sSQL = string.Empty;
            StringBuilder objSQL = null;
            bool bIsPrintTotals = false;
            bool bIsUseRecord = false;
            DbReader objReader = null;
            DbReader objTempReader = null;
            ArrayList arrEOBLine = null;
            FundsTransSplit objFundsTransSplit = null;
            long lTempId = 0;
            string sTempData = string.Empty;
            int iNumEOB = 0;
            double dAvgTextHeight = 0.0;
            double dAvgPrintWidth = 0.0;
            double dTotalAmountBilled = 0.0;
            double dTotalAmountPaid = 0.0;
            double dCurrX = 0;
            double dCurrY = 0;
            double dCurrentEOBX = 0.0;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sInvoiceNumber = string.Empty;
            string sInvoiceDate = string.Empty;
            string sInvoiceBy = string.Empty;
            DataModelFactory objDataModelFactory = null;
            Common objCommon = null;
            try
            {
                objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId);//sharishkumar Jira 835
                objCommon = new Common();
                if (p_bIsClaimantCopy)
                {
                    dCurrY = 2000;
                    dCurrX = 500;
                }
                else
                {
                    dCurrY = 4900;
                    dCurrX = 500;
                }

                dAvgTextHeight = m_objPrintWrapper.GetTextHeight("W");
                dAvgPrintWidth = m_objPrintWrapper.PageWidth;

                m_objPrintWrapper.CurrentX = dCurrX;
                m_objPrintWrapper.CurrentY = dCurrY;

                if (p_sOtherName == string.Empty)
                {
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername1", m_iClientId)));//sharishkumar Jira 835
                    m_objPrintWrapper.CurrentX = dCurrX;
                    dCurrY += dAvgTextHeight;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername2", m_iClientId)));//sharishkumar Jira 835

                }
                else
                {
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername3", m_iClientId)) + Functions.FillSpace(1) + p_sOtherName + Functions.FillSpace(1) + CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername4",m_iClientId)));//sharishkumar Jira 835
                    m_objPrintWrapper.CurrentX = dCurrX;
                    dCurrY += dAvgTextHeight;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername5",m_iClientId)));//sharishkumar Jira 835
                }

                m_objPrintWrapper.CurrentX = dCurrX;
                dCurrY += dAvgTextHeight;
                dCurrY += dAvgTextHeight * 2;
                m_objPrintWrapper.CurrentY = dCurrY;
                m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.ItemsText",m_iClientId)));//sharishkumar Jira 835

                m_objPrintWrapper.CurrentX = dCurrX;
                dCurrY += dAvgTextHeight * 2;
                m_objPrintWrapper.CurrentY = dCurrY;
                m_objPrintWrapper.SetFontBold(true);

                //Tab(6); "INVOICE"; Tab(21); "PROCEDURE"; Tab(32); "PROC."; Tab(39); "EXPLANATION"; Tab(66); "AMOUNT"; Tab(78); "AMOUNT"
                m_objPrintWrapper.PrintText("INVOICE");
                m_objPrintWrapper.CurrentX = dCurrX + 1600;
                m_objPrintWrapper.PrintText("PROCEDURE");
                m_objPrintWrapper.CurrentX = dCurrX + 3200;
                m_objPrintWrapper.PrintText("PROC.");
                m_objPrintWrapper.CurrentX = dCurrX + 4100;
                m_objPrintWrapper.PrintText("EXPLANATION");
                m_objPrintWrapper.CurrentX = dCurrX + 6800;
                m_objPrintWrapper.PrintText("AMOUNT");
                m_objPrintWrapper.CurrentX = dCurrX + 8200;
                m_objPrintWrapper.PrintText("AMOUNT");
                m_objPrintWrapper.CurrentX = dCurrX;
                m_objPrintWrapper.CurrentY = dCurrY + dAvgTextHeight;

                //Tab(6); "NUMBER"; Tab(21); "DATE"; Tab(32); "CODE"; Tab(66); "BILLED"; Tab(78); "PAID"
                m_objPrintWrapper.PrintText("NUMBER");
                m_objPrintWrapper.CurrentX = dCurrX + 1600;
                m_objPrintWrapper.PrintText("DATE");
                m_objPrintWrapper.CurrentX = dCurrX + 3200;
                m_objPrintWrapper.PrintText("CODE");
                m_objPrintWrapper.CurrentX = dCurrX + 6800;
                m_objPrintWrapper.PrintText("BILLED");
                m_objPrintWrapper.CurrentX = dCurrX + 8200;
                m_objPrintWrapper.PrintText("PAID");
                m_objPrintWrapper.CurrentX = dCurrX;
                dCurrY += dAvgTextHeight * 2;
                m_objPrintWrapper.CurrentY = dCurrY;
                m_objPrintWrapper.SetFontBold(false);

                objSQL = new StringBuilder();
                objSQL.Append("SELECT INVOICE_DETAIL_ID,BILLING_CODE_TEXT,AMOUNT_BILLED,AMOUNT_TO_PAY,FUNDS_SPLIT_ROW_ID");
                objSQL.Append(",TABLE_CODE, PERCENTILE,PLACE_OF_SER_CODE,TYPE_OF_SER_CODE,UNITS_BILLED_NUM");
                objSQL.Append(",SCHEDULED_AMOUNT,AMOUNT_REDUCED,AMOUNT_SAVED, QUANTITY,STORE");
                objSQL.Append(",CONTRACT_AMOUNT,DISCOUNT,ZIP_CODE,BASE_AMOUNT,AMOUNT_ALLOWED");
                objSQL.Append(",PER_DIEM_AMT,STOP_LOSS_AMT,FEE_TABLE_AMT");
                objSQL.Append(" FROM INVOICE_DETAIL, FUNDS_TRANS_SPLIT WHERE INVOICE_ID = " + p_iInvoiceId);
                objSQL.Append(" AND FUNDS_SPLIT_ROW_ID = SPLIT_ROW_ID ORDER BY FROM_DATE");

                objReader = DbFactory.GetDbReader(m_ConnectionString, objSQL.ToString());

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        bIsUseRecord = false;

                        //At first check if record is deleted or not
                        lTempId = Conversion.ConvertStrToLong(objReader["FUNDS_SPLIT_ROW_ID"].ToString());

                        foreach (FundsTransSplit objFundsTSplit in m_objFunds.TransSplitList)
                        {
                            if (objFundsTSplit.SplitRowId == lTempId)
                            {
                                bIsUseRecord = true;
                                break;
                            }
                        }

                        iNumEOB = 0;

                        if (bIsUseRecord)
                        {
                            bIsPrintTotals = true;
                            lTempId = Conversion.ConvertStrToLong(objReader["INVOICE_DETAIL_ID"].ToString());

                            sSQL = "SELECT CODES_TEXT.CODE_DESC FROM INVDETAIL_X_EOB,CODES_TEXT WHERE " +
                                "INVDETAIL_X_EOB.EOB_CODE = CODES_TEXT.CODE_ID AND " +
                                "INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + lTempId;
                            objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                            arrEOBLine = new ArrayList();

                            //if(objTempReader != null)
                            //{
                            //    while(objTempReader.Read())
                            //    {
                            //        sTempData = objTempReader["CODE_DESC"].ToString();
                            //        arrEOBLine.Add(sTempData);
                            //        iNumEOB++;
                            //    }
                            //    objTempReader.Close();
                            //}

                            //MITS  :12090  Umesh
                            if (objTempReader != null)
                            {
                                while (objTempReader.Read())
                                {
                                    string sTempEob = "";
                                    sTempData = objTempReader["CODE_DESC"].ToString().Trim();

                                    sTempData = "- " + sTempData; //Starting of New EOB


                                    //iNumEOB++;
                                    if (sTempData.Length > 23)
                                    {

                                        int iStartIndex = 0;
                                        int iLengthFix = 23;
                                        int iLength = 0;
                                        int iLastIndexofSpace = 0;
                                        string sNextChar = "";
                                        while (iStartIndex < sTempData.Length)
                                        {
                                            iNumEOB++;

                                            //code to handle the boundary conditions
                                            if (iStartIndex + iLengthFix + 1 <= sTempData.Length)
                                            {
                                                //find the next character
                                                //if it is space , last word will be in same line

                                                sNextChar = sTempData.Substring(iStartIndex + iLengthFix, 1);
                                                if (sNextChar == " ")
                                                {

                                                    sTempEob = sTempData.Substring(iStartIndex, iLengthFix - 1);
                                                    arrEOBLine.Add(sTempEob);
                                                }
                                                //else last word will come into next line. so remove the last word from current line.
                                                else
                                                {
                                                    sTempEob = sTempData.Substring(iStartIndex, iLengthFix - 1);
                                                    iLastIndexofSpace = sTempEob.LastIndexOf(" ");
                                                    iLength = iLastIndexofSpace;
                                                    sTempEob = sTempData.Substring(iStartIndex, iLength);
                                                    arrEOBLine.Add(sTempEob);


                                                }
                                            }
                                            else
                                            {
                                                sTempEob = sTempData.Substring(iStartIndex, iLengthFix - 1);
                                                arrEOBLine.Add(sTempEob);
                                            }
                                            iStartIndex += iLength;
                                            //get the lenght of remaining string.
                                            if (iStartIndex + 23 >= sTempData.Length)
                                                iLengthFix = sTempData.Length - iStartIndex;

                                        }

                                    }


                                    else
                                    {
                                        iNumEOB++;
                                        arrEOBLine.Add(sTempData.Trim());
                                    }


                                }
                                objTempReader.Close();
                            }

                            //*** Default body ***
                            //Call subCheckEOBPageLength(13500)  ' Checks to see if new page is needed

                            objFundsTransSplit = (FundsTransSplit)objDataModelFactory.GetDataModelObject("FundsTransSplit", false);
                            try
                            {
                                objFundsTransSplit.MoveTo(Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString()));
                                sFromDate = Conversion.ToDate(objFundsTransSplit.FromDate).ToShortDateString();
                                sToDate = Conversion.ToDate(objFundsTransSplit.ToDate).ToShortDateString();
                                sInvoiceNumber = objFundsTransSplit.InvoiceNumber;
                                sInvoiceDate = Conversion.ToDate(objFundsTransSplit.InvoiceDate).ToShortDateString();
                                sInvoiceBy = objFundsTransSplit.InvoicedBy;
                                objFundsTransSplit.Dispose();
                            }
                            catch
                            {
                                sFromDate = string.Empty;
                                sToDate = string.Empty;
                                sInvoiceNumber = "N/A";
                                sInvoiceDate = string.Empty;
                                sInvoiceBy = string.Empty;
                            }

                            m_objPrintWrapper.CurrentX = dCurrX;
                            m_objPrintWrapper.CurrentY = dCurrY;
                            m_objPrintWrapper.PrintText(sInvoiceNumber.Trim());
                            m_objPrintWrapper.CurrentX = dCurrX + 1600;
                            m_objPrintWrapper.CurrentY = dCurrY;
                            m_objPrintWrapper.PrintText(sFromDate);

                            //Print remaining data fields
                            sTempData = objReader["BILLING_CODE_TEXT"].ToString();
                            m_objPrintWrapper.CurrentX = dCurrX + 3200;
                            m_objPrintWrapper.CurrentY = dCurrY;
                            if (sTempData.Length > 6)
                                m_objPrintWrapper.PrintText(sTempData.Substring(0, 6));
                            else
                                m_objPrintWrapper.PrintText(sTempData);

                            if (iNumEOB > 0)
                            {
                                dCurrentEOBX = dCurrX + 4100;
                                m_objPrintWrapper.CurrentX = dCurrX + 4100;
                                m_objPrintWrapper.CurrentY = dCurrY;
                                if (arrEOBLine[0].ToString().Length > 20)
                                    m_objPrintWrapper.PrintText(arrEOBLine[0].ToString().Substring(0, 20));
                                else
                                    m_objPrintWrapper.PrintText(arrEOBLine[0].ToString());
                            }

                            //amount billed
                            double d = Conversion.ConvertStrToDouble(objReader["AMOUNT_BILLED"].ToString());
                            dTotalAmountBilled += d;
                            sTempData = string.Format("{0:#,###,##0.00}", d);
                            m_objPrintWrapper.CurrentX = dCurrX + 6800;
                            m_objPrintWrapper.CurrentY = dCurrY;
                            m_objPrintWrapper.PrintText(sTempData);

                            //amount to pay
                            d = Conversion.ConvertStrToDouble(objReader["AMOUNT_TO_PAY"].ToString());
                            dTotalAmountPaid += d;
                            sTempData = string.Format("{0:#,###,##0.00}", d);
                            m_objPrintWrapper.CurrentX = dCurrX + 8200;
                            m_objPrintWrapper.CurrentY = dCurrY;
                            m_objPrintWrapper.PrintText(sTempData);

                            //Print remaining EOB lines (see above)
                            if (iNumEOB > 1)
                            {
                                //for(int i = 1; i < iNumEOB; i++)
                                //{
                                //    sTempData += arrEOBLine[i].ToString();
                                //    sTempData += ", ";
                                //}
                                //if(sTempData.Substring(sTempData.Length - 1) == ", ")
                                //    sTempData = sTempData.Substring(1,sTempData.Length - 2);
                                //MITS 11435 : Umesh
                                for (int i = 1; i < iNumEOB; i++)
                                {
                                    CheckEOBPageLength(13800, m_objPrintWrapper);
                                    //m_objPrintWrapper.CurrentX = dCurrX + 900;
                                    m_objPrintWrapper.CurrentX = dCurrentEOBX;
                                    dCurrY += dAvgTextHeight;
                                    m_objPrintWrapper.CurrentY = dCurrY;
                                    m_objPrintWrapper.PrintText(arrEOBLine[i].ToString());
                                }
                            }
                        }
                        dCurrY += dAvgTextHeight;
                    }//END of While
                    objReader.Close();
                }//End of IF

                if (bIsPrintTotals)
                {
                    sTempData = Functions.FormatAmount(dTotalAmountBilled, m_iClientId).ToString();
                    m_objPrintWrapper.SetFontBold(true);
                    m_objPrintWrapper.CurrentX = dCurrX + 2900;
                    dCurrY = dCurrY + dAvgTextHeight * 2;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText("TOTALS:");
                    m_objPrintWrapper.CurrentX = dCurrX + 6800;
                    m_objPrintWrapper.PrintText(sTempData);
                    sTempData = Functions.FormatAmount(dTotalAmountPaid, m_iClientId).ToString();
                    m_objPrintWrapper.CurrentX = dCurrX + 8200;
                    m_objPrintWrapper.PrintText(sTempData);
                    m_objPrintWrapper.SetFontBold(false);
                }

                m_objPrintWrapper.CurrentX = dCurrX;
                dCurrY += dAvgTextHeight * 3;
                m_objPrintWrapper.CurrentY = dCurrY;
                CheckEOBPageLength(12300, m_objPrintWrapper); ;
                m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.ContactUs", m_iClientId)));//sharishkumar Jira 835
                m_objPrintWrapper.CurrentX = dCurrX;
                dCurrY += dAvgTextHeight * 2;
                m_objPrintWrapper.CurrentY = dCurrY;
                m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Sincerely", m_iClientId)));//sharishkumar Jira 835
                m_objPrintWrapper.CurrentX = dCurrX;
                dCurrY += dAvgTextHeight * 2;
                m_objPrintWrapper.CurrentY = dCurrY;
                m_objPrintWrapper.PrintText(objCommon.m_stLoginInfo.Username);

                if (!p_bIsClaimantCopy)
                {
                    m_objPrintWrapper.CurrentX = dCurrX + 2500;
                    dCurrY += dAvgTextHeight * 2;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.ClaimantCopy",m_iClientId)));//sharishkumar Jira 835
                    m_objPrintWrapper.CurrentX = dCurrX + 2500;
                    dCurrY += dAvgTextHeight * 2;
                    m_objPrintWrapper.CurrentY = dCurrY;
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Cc", m_iClientId)) + Functions.FillSpace(1) + p_sClaimantName);//sharishkumar Jira 835
                    
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAR.GetDefaultEOBDetail.ErrorGet", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objSQL != null)
                    objSQL = null;

                if (objReader != null)
                    objReader.Dispose();

                if (objTempReader != null)
                    objTempReader.Dispose();

                if (objFundsTransSplit != null)
                    objFundsTransSplit.Dispose();

                if (arrEOBLine != null)
                    arrEOBLine = null;
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                    objDataModelFactory = null;
                }
            }
        }
        #endregion

        #region "GetPrintCheckEOBBody(int p_iInvoiceId, string p_sClaimNumberText, string p_sClaimantName, string p_sName, string p_sOtherName, bool p_bIsFormatFileFound, int p_iCurrentX, int p_iCurrentY)"
        /// Name		: GetPrintCheckEOBBody
        /// Author		: Anurag Agarwal
        /// Date Created	: 06 Jan 2005		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Print EOB; Introduction And Body.Format; File Is the; Custom; Files; of
        /// http-like tags <b>.  If a file cannot be found, a Default format is used.
        /// </summary>
        /// <param name="p_iInvoiceId">Invoice Id</param>
        /// <param name="p_sClaimNumberText">Claim Number Text</param>
        /// <param name="p_sClaimantName">Claimant Name</param>
        /// <param name="p_sName">Name</param>
        /// <param name="p_sOtherName">Other Name</param>
        /// <param name="p_bIsFormatFileFound">Flag for Is format file found</param>
        /// <param name="p_iCurrentX">Current X-axis position</param>
        /// <param name="p_iCurrentY">Current Y-axis position</param>
        private void GetPrintCheckEOBBody(int p_iInvoiceId, string p_sClaimNumberText, string p_sClaimantName, string p_sName, string p_sOtherName, bool p_bIsFormatFileFound, int p_iCurrentX, int p_iCurrentY)
        {

            double dAvgTextHeight = 0.0;
            double dAvgPrintWidth = 0.0;
            bool bIsPrintTotals = false;
            bool bIsUseRecord = false;
            StringBuilder objSQL = null;
            DbReader objReader = null;
            DbReader objTempReader = null;
            FundsTransSplit objFundsTransSplit = null;
            int iTempId = 0;
            int iNumEOB = 0;
            int iLineItem = 0;
            string sSQL = string.Empty;
            string sTempData = string.Empty;
            double dTotAmtReduced = 0.0;
            double dTotSchdAmt = 0.0;
            double dTotAmtSaved = 0.0;
            double dTotalAmountAllowed = 0.0;
            double dTotalBaseAmount = 0.0;
            double dTotalDiscountAmount = 0.0;
            double dTotalPerDiemAmt = 0.0;
            double dTotalStopLossAmt = 0.0;
            double dTotalFeeTableAmt = 0.0;
            double dTotalAmountBilled = 0.0;
            double dTotalAmountPaid = 0.0;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sInvoiceNumber = string.Empty;
            string sInvoiceDate = string.Empty;
            string sInvoiceBy = string.Empty;
            DataModelFactory objDataModelFactory = null;
            Common objCommon = null;
            try
            {
                objCommon = new Common();
                objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId);//sharishkumar Jira 835
                dAvgTextHeight = m_objPrintWrapper.GetTextHeight("W");
                dAvgPrintWidth = m_objPrintWrapper.PageWidth;

                //Print main body & introduction
                if (!p_bIsFormatFileFound)
                {
                    m_objPrintWrapper.CurrentX = p_iCurrentX; //**
                    m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight; //**

                    //*** Default body - Detail record ***
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.RE", m_iClientId)) + Functions.FillSpace(1) + CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText", m_iClientId)) + Functions.FillSpace(1) + p_sClaimNumberText);//sharishkumar Jira 835

                    m_objPrintWrapper.CurrentX = p_iCurrentX;
                    m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName", m_iClientId)) + Functions.FillSpace(1) + p_sClaimantName);//sharishkumar Jira 835
                    m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight; //***
                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.Name", m_iClientId)) + Functions.FillSpace(1) + p_sName);//sharishkumar Jira 835

                    if (p_sOtherName == string.Empty)
                    {
                        m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername1",m_iClientId)));//sharishkumar Jira 835
                        m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight; //***
                        m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername2",m_iClientId)));//sharishkumar Jira 835
                    }
                    else
                    {
                        m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername3", m_iClientId)) + Functions.FillSpace(1) + p_sOtherName + Functions.FillSpace(1) + CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername4",m_iClientId)));//sharishkumar Jira 835
                        m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight; //***
                        m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername5", m_iClientId)));//sharishkumar Jira 835
                    }

                    m_objPrintWrapper.PrintText(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.ItemsText", m_iClientId)));//sharishkumar Jira 835
                    m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight; //***
                    m_objPrintWrapper.PrintText(string.Empty);

                    m_objPrintWrapper.SetFontBold(true);

                    //Tab(6); "INVOICE"; Tab(21); "PROCEDURE"; Tab(32); "PROC."; Tab(39); "EXPLANATION"; Tab(66); "AMOUNT"; Tab(78); "AMOUNT"
                    m_objPrintWrapper.PrintText("INVOICE");
                    m_objPrintWrapper.CurrentX = p_iCurrentX + 100;
                    m_objPrintWrapper.PrintText("PROCEDURE");
                    m_objPrintWrapper.CurrentX = p_iCurrentX + 300;
                    m_objPrintWrapper.PrintText("PROC.");
                    m_objPrintWrapper.CurrentX = p_iCurrentX + 500;
                    m_objPrintWrapper.PrintText("EXPLANATION");
                    m_objPrintWrapper.CurrentX = p_iCurrentX + 700;
                    m_objPrintWrapper.PrintText("AMOUNT");
                    m_objPrintWrapper.CurrentX = p_iCurrentX + 900;
                    m_objPrintWrapper.PrintText("AMOUNT");
                    m_objPrintWrapper.CurrentX = p_iCurrentX;
                    m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;

                    //Tab(6); "NUMBER"; Tab(21); "DATE"; Tab(32); "CODE"; Tab(66); "BILLED"; Tab(78); "PAID"
                    m_objPrintWrapper.PrintText("NUMBER");
                    m_objPrintWrapper.CurrentX = p_iCurrentX + 100;
                    m_objPrintWrapper.PrintText("DATE");
                    m_objPrintWrapper.CurrentX = p_iCurrentX + 300;
                    m_objPrintWrapper.PrintText("CODE");
                    m_objPrintWrapper.CurrentX = p_iCurrentX + 500;
                    m_objPrintWrapper.PrintText("BILLED");
                    m_objPrintWrapper.CurrentX = p_iCurrentX + 700;
                    m_objPrintWrapper.PrintText("PAID");
                    m_objPrintWrapper.SetFontBold(false);
                }

                objSQL = new StringBuilder();
                objSQL.Append("SELECT INVOICE_DETAIL_ID,BILLING_CODE_TEXT,AMOUNT_BILLED,AMOUNT_TO_PAY,FUNDS_SPLIT_ROW_ID");
                objSQL.Append(",TABLE_CODE, PERCENTILE,PLACE_OF_SER_CODE,TYPE_OF_SER_CODE,UNITS_BILLED_NUM");
                objSQL.Append(",SCHEDULED_AMOUNT,AMOUNT_REDUCED,AMOUNT_SAVED, QUANTITY,STORE, MODIFIER_CODE");
                objSQL.Append(",CONTRACT_AMOUNT,DISCOUNT,ZIP_CODE,BASE_AMOUNT,AMOUNT_ALLOWED");
                objSQL.Append(",PER_DIEM_AMT,STOP_LOSS_AMT,FEE_TABLE_AMT, PRESCRIP_NO, DRUG_NAME, PRESCRIP_DATE");
                //BRS FL Merge
                objSQL.Append(",FL_LICENSE, REV_CODE");
                //End
                objSQL.Append(" FROM INVOICE_DETAIL, FUNDS_TRANS_SPLIT WHERE INVOICE_ID = " + p_iInvoiceId);
                objSQL.Append(" AND FUNDS_SPLIT_ROW_ID = SPLIT_ROW_ID  ORDER BY FROM_DATE");

                objReader = DbFactory.GetDbReader(m_ConnectionString, objSQL.ToString());

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        m_structEOBDetailRecord.EOBList = new Hashtable();
                        m_structEOBDetailRecord.DiagnosisList = new Hashtable();
                        m_structEOBDetailRecord.ModifierList = new Hashtable();

                        bIsUseRecord = false;

                        //At first check if record is deleted or not
                        iTempId = Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString());

                        foreach (FundsTransSplit objFundTSplit in m_objFunds.TransSplitList)
                        {
                            if (objFundTSplit.SplitRowId == iTempId)
                            {
                                bIsUseRecord = true;
                                break;
                            }
                        }

                        iNumEOB = 0;
                        int value = (m_bEOBDescNextLine == true) ? 96 : 23; 

                        if (bIsUseRecord)
                        {
                            bIsPrintTotals = true;
                            iTempId = Conversion.ConvertStrToInteger(objReader["INVOICE_DETAIL_ID"].ToString());


                            //BRS FL Merge : Umesh
                            //florida eob support for alternative descriptions
                            //Added for MITS 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
                            bool bUseCodeDescriptionFile = Convert.ToBoolean(Functions.GetValueFromConfigFile(Functions.TAG_USEEOBDESCRIPTIONFILE, m_ConnectionString, m_iClientId));//rkaur27
                            if (bUseCodeDescriptionFile)//Added if block for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
                            {
                                string sDesciptionFileName = Functions.GetFormatFilePath(m_iClientId)
                                                            + Functions.GetValueFromConfigFile(Functions.TAG_EOBDESCRIPTIONFILE, m_ConnectionString, m_iClientId);//rkaur27


                                //string sDesciptionFileName = GetValueFromConfigFile(Functions.TAG_FORMATFILEPATH)
                                //                             + @"\" + GetValueFromConfigFile(Functions.TAG_EOBDESCRIPTIONFILE);
                                if (File.Exists(sDesciptionFileName))
                                {


                                    string sFileText = null;
                                    string sNewText = null;
                                    string sOldText = null;
                                    int iPoint = 0;

                                    StreamReader objSRdr = null;
                                    objSRdr = new StreamReader(sDesciptionFileName);
                                    sFileText = objSRdr.ReadToEnd();


                                    //Ashish Ahuja Mits 33681 : Start
                                    sSQL = "SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM INVDETAIL_X_EOB, CODES, CODES_TEXT";
                                    sSQL = sSQL + " WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND INVDETAIL_X_EOB.EOB_CODE = CODES.CODE_ID";
                                    sSQL = sSQL + " AND INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + iTempId;
                                    //Ashish Ahuja Mits 33681 : End

                                    //Changed for Mits 19415:Format of Explaination of Benifit form is different in RMX from RMWorld
                                    //objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                                    //while (objTempReader.Read())
                                    //{
                                    //    sTempData = "@" + objTempReader.GetString("SHORT_CODE").Trim();
                                    //    iPoint = sFileText.IndexOf(sTempData, 1);
                                    //    if (iPoint >= 0)
                                    //    {
                                    //        sOldText = sFileText.Substring(iPoint + 1);
                                    //        iPoint = sOldText.IndexOf("@");
                                    //        sNewText = sOldText.Substring(0, iPoint - 1);
                                    //        iNumEOB++;
                                    //        m_structEOBDetailRecord.EOBList.Add(iNumEOB, sNewText);


                                    //    }


                                    //}
                                    using (objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL))
                                    {
                                        while (objTempReader.Read())
                                        {
                                            string sTempEob = "";
                                            //Ashish Ahuja Mits 33681 : Start
                                            //sTempData = "@" + objTempReader.GetString("SHORT_CODE").Trim();
                                            sTempData = "@" + objTempReader["SHORT_CODE"].ToString() + " " + objTempReader["CODE_DESC"].ToString();
                                            //Ashish Ahuja Mits 33681 : End
                                            //Ashish Ahuja - Mits 33681
                                            //iPoint = sFileText.IndexOf(sTempData, 0);
                                            iPoint = sFileText.IndexOf(sTempData.Substring(0,sTempData.IndexOf(" ")), 0);
                                            if (iPoint >= 0)
                                            {
                                                sOldText = sFileText.Substring(iPoint + 1);
                                                iPoint = sOldText.IndexOf("@");
                                                sNewText = sOldText.Substring(0, iPoint - 1);
                                                sTempData = sNewText;
                                                //Ashish Ahuja Mits 33681 : Start
                                                if (m_bEOBDescNextLine == true)
                                                {
                                                    if (iNumEOB > 0)
                                                        sTempData = "-\n- " + sTempData; //Starting of New EOB
                                                    else
                                                        sTempData = "-\n-  " + sTempData; //First EOB
                                                }
                                                else
                                                {
                                                    if (iNumEOB > 0)
                                                        sTempData = "- " + sTempData; //Starting of New EOB
                                                    else
                                                        sTempData = " - " + sTempData; //First EOB
                                                }

                                                //Ashish Ahuja Mits 33681 : End
                                                //iNumEOB++;
                                                //Ashish Ahuja - Mits 33681
                                                if (sTempData.Length > value)
                                                {

                                                    int iStartIndex = 0;
                                                    //Ashish Ahuja - Mits 33681
                                                    int iLengthFix = value;
                                                    int iLength = 0;
                                                    int iLastIndexofSpace = 0;
                                                    string sNextChar = "";
                                                    while (iStartIndex < sTempData.Length)
                                                    {
                                                        iNumEOB++;

                                                        //code to handle the boundary conditions
                                                        if (iStartIndex + iLengthFix + 1 <= sTempData.Length)
                                                        {
                                                            //find the next character
                                                            //if it is space , last word will be in same line

                                                            sNextChar = sTempData.Substring(iStartIndex + iLengthFix, 1);
                                                            if (sNextChar == " ")
                                                            {

                                                                sTempEob = sTempData.Substring(iStartIndex, iLengthFix); //Mits 19415:changed iLengthFix-1 to iLengthFix to extract all the chars before the space
                                                                iLength = iLengthFix; //Added for Mits 19415:iLength was not getting incremented in this case.Hence duplicacy of code description on next line.
                                                                m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
                                                            }
                                                            //else last word will come into next line. so remove the last word from current line.
                                                            else
                                                            {
                                                                sTempEob = sTempData.Substring(iStartIndex, iLengthFix - 1);
                                                                iLastIndexofSpace = sTempEob.LastIndexOf(" ");
                                                                iLength = iLastIndexofSpace;
                                                                sTempEob = sTempData.Substring(iStartIndex, iLength);
                                                                m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);


                                                            }
                                                        }
                                                        else
                                                        {
                                                            sTempEob = sTempData.Substring(iStartIndex, iLengthFix - 1);
                                                            // akaushik5 Added for MITS 37068 Starts
                                                            iLength = sTempData.Length - iStartIndex;
                                                            // akaushik5 Added for MITS 37068 Ends
                                                            m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
                                                        }
                                                        iStartIndex += iLength;
                                                        //Ashish Ahuja - Mits 33681
                                                        if (m_bEOBDescNextLine == true)
                                                            iLength = 96;
                                                        //get the lenght of remaining string.

                                                        if (iStartIndex + value >= sTempData.Length)
                                                            iLengthFix = sTempData.Length - iStartIndex;

                                                    }

                                                }


                                                else
                                                {
                                                    iNumEOB++;
                                                    //Ashish Ahuja
                                                    //m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
                                                    m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData);
                                                }


                                            }
                                                //Ashish Ahuja -  Mits 33681
                                            else
                                            {
                                                if (m_bEOBDescNextLine == true)
                                                    sTempData = "-\n- " + sTempData.Substring(1, sTempData.Length - 1);
                                                else
                                                    sTempData = " - " + sTempData.Substring(1, sTempData.Length - 1);
                                                iNumEOB++;
                                                m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData);

                                            }
                                        }
                                    }
                                    //Changed for Mits 19415:Format of Explaination of Benifit form is different in RMX from RMWorld
                                    if (objTempReader != null)
                                    {
                                        objTempReader.Close();
                                    }
                                    if (objSRdr != null)
                                    {
                                        objSRdr.Close();
                                        objSRdr.Dispose();
                                    }


                                    //BRS FL Merge : END


                                }
                                else
                                {
                                    throw new RMAppException(Globalization.GetString("Functions.EobDescriptionFile.Error", m_iClientId));//sharishkumar Jira 835
                                }
                            }//Added if block for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
                            else
                            {
                                //Ashish Ahuja Mits 33681 - added EOB_CODE in query
                                sSQL = "SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM INVDETAIL_X_EOB, CODES,CODES_TEXT";
                                sSQL = sSQL + " WHERE INVDETAIL_X_EOB.EOB_CODE = CODES.CODE_ID AND CODES.CODE_ID = CODES_TEXT.CODE_ID ";
                                sSQL = sSQL + " AND INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + iTempId;
                                objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                                if (objTempReader != null)
                                {
                                    while (objTempReader.Read())
                                    {
                                        string sTempEob = "";
                                        //Ashish Ahuja Mits 33681 : Start
                                        //sTempData = objTempReader["CODE_DESC"].ToString();
                                        sTempData = objTempReader["SHORT_CODE"].ToString() + " " + objTempReader["CODE_DESC"].ToString();
                                        //sTempData = "- " + sTempData; //Starting of New EOB
                                        if (m_bEOBDescNextLine == true)
                                            sTempData = "-\n- " + sTempData; //Starting of New EOB
                                        else
                                            sTempData = "- " + sTempData; //Starting of New EOB

                                        //Ashish Ahuja Mits 33681 : End
                                        //iNumEOB++;
                                        //if(sTempData.Length > 23)
                                        //    m_structEOBDetailRecord.EOBList.Add(iNumEOB,sTempData.Trim().Substring(0,23));      
                                        //else
                                        //    m_structEOBDetailRecord.EOBList.Add(iNumEOB,sTempData);
                                        //Ashish Ahuja - Mits 33681
                                        if (sTempData.Length > value)
                                        {

                                            int iStartIndex = 0;
                                            //Ashish Ahuja - Mits 33681
                                            int iLengthFix = value;
                                            int iLength = 0;
                                            int iLastIndexofSpace = 0;
                                            string sNextChar = "";
                                            while (iStartIndex < sTempData.Length)
                                            {
                                                iNumEOB++;

                                                //code to handle the boundary conditions
                                                if (iStartIndex + iLengthFix + 1 <= sTempData.Length)
                                                {
                                                    //find the next character
                                                    //if it is space , last word will be in same line

                                                    sNextChar = sTempData.Substring(iStartIndex + iLengthFix, 1);
                                                    if (sNextChar == " ")
                                                    {

                                                        sTempEob = sTempData.Substring(iStartIndex, iLengthFix); //Mits 19415:changed iLengthFix-1 to iLengthFix to extract all the chars before the space
                                                        iLength = iLengthFix; //Added for Mits 19415:iLength was not getting incremented in this case.Hence duplicacy of code description on next line.
                                                        m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
                                                    }
                                                    //else last word will come into next line. so remove the last word from current line.
                                                    else
                                                    {
                                                        sTempEob = sTempData.Substring(iStartIndex, iLengthFix - 1);
                                                        iLastIndexofSpace = sTempEob.LastIndexOf(" ");
                                                        iLength = iLastIndexofSpace;
                                                        sTempEob = sTempData.Substring(iStartIndex, iLength);
                                                        m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);

                                                    }
                                                }
                                                else
                                                {
                                                    sTempEob = sTempData.Substring(iStartIndex, iLengthFix); //No Carriage return in this case
                                                    m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
                                                }
                                                iStartIndex += iLength;
                                                //get the lenght of remaining string.
                                                //Ashish Ahuja - Mits 33681
                                                if (iStartIndex + value >= sTempData.Length)
                                                    iLengthFix = sTempData.Length - iStartIndex;

                                            }

                                            //m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData.Trim().Substring(0, 23));
                                        }


                                        else
                                        {
                                            iNumEOB++;
                                            m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData.Trim());
                                        }
                                    }
                                    objTempReader.Close();
                                }
                            }
                            //*** Custom ***
                            if (p_bIsFormatFileFound)
                            {
                                //extract diagnosis code
                                m_structEOBDetailRecord.DiagnosisItems = 0;

                                sSQL = "SELECT CODES_TEXT.CODE_DESC FROM INVDETAIL_X_DIAG, CODES_TEXT" +
                                    " WHERE INVDETAIL_X_DIAG.DIAGNOSIS_CODE=CODES_TEXT.CODE_ID" +
                                    " AND INVDETAIL_X_DIAG.INVOICE_DETAIL_ID= " + iTempId;
                                objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                                if (objTempReader != null)
                                {
                                    while (objTempReader.Read())
                                    {
                                        m_structEOBDetailRecord.DiagnosisItems++;
                                        sTempData = objTempReader["CODE_DESC"].ToString();
                                        iLineItem++;
                                        m_structEOBDetailRecord.DiagnosisList.Add(iLineItem, sTempData.Trim());
                                    }
                                    objTempReader.Close();
                                }
                                iLineItem = 0;
                                //extract modifier code
                                sSQL = "SELECT CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM INVDETAIL_X_MOD, CODES, CODES_TEXT" +
                                    " WHERE INVDETAIL_X_MOD.MODIFIER_CODE=CODES_TEXT.CODE_ID" +
                                    " AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND INVDETAIL_X_MOD.INVOICE_DETAIL_ID=" + iTempId;
                                objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                                if (objTempReader != null)
                                {
                                    while (objTempReader.Read())
                                    {
                                        sTempData = objTempReader["SHORT_CODE"].ToString() + " " + objTempReader["CODE_DESC"].ToString();
                                        iLineItem++;
                                        m_structEOBDetailRecord.ModifierList.Add(iLineItem, sTempData.Trim());
                                    }
                                    objTempReader.Close();
                                }
                                iLineItem = 0;
                                //extract fee schedule name
                                iTempId = Conversion.ConvertStrToInteger(objReader["TABLE_CODE"].ToString());
                                sSQL = "SELECT FEE_TABLES.USER_TABLE_NAME FROM FEE_TABLES WHERE FEE_TABLES.TABLE_ID = " + iTempId;
                                objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                                if (objTempReader != null)
                                {
                                    if (objTempReader.Read())
                                    {
                                        sTempData = objTempReader["USER_TABLE_NAME"].ToString();
                                        m_structEOBDetailRecord.FeeSchedule.Token = sTempData;
                                    }
                                    objTempReader.Close();
                                }

                                m_structEOBDetailRecord.Percentile.Token = objReader["PERCENTILE"].ToString();

                                iTempId = Conversion.ConvertStrToInteger(objReader["PLACE_OF_SER_CODE"].ToString());
                                sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " +
                                    "CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
                                objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                                if (objTempReader != null)
                                {
                                    if (objTempReader.Read())
                                    {
                                        m_structEOBDetailRecord.PlaceofServiceCode.Token = objTempReader["SHORT_CODE"].ToString();
                                        m_structEOBDetailRecord.PlaceOfService.Token = objTempReader["CODE_DESC"].ToString();
                                    }
                                    objTempReader.Close();
                                }

                                iTempId = Conversion.ConvertStrToInteger(objReader["TYPE_OF_SER_CODE"].ToString());
                                sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " +
                                    "CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
                                objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                                if (objTempReader != null)
                                {
                                    if (objTempReader.Read())
                                    {
                                        m_structEOBDetailRecord.TypeOfServiceCode.Token = objTempReader["SHORT_CODE"].ToString();
                                        m_structEOBDetailRecord.TypeOfService.Token = objTempReader["CODE_DESC"].ToString();
                                    }
                                    objTempReader.Close();
                                }

                                m_structEOBDetailRecord.UnitsBilled.Token = Conversion.ConvertStrToLong(objReader["UNITS_BILLED_NUM"].ToString());

                                m_structEOBDetailRecord.ScheduledAmount.Token = Conversion.ConvertStrToDouble(objReader["SCHEDULED_AMOUNT"].ToString());
                                dTotSchdAmt += Functions.FormatAmount(m_structEOBDetailRecord.ScheduledAmount.Token, m_iClientId);

                                m_structEOBDetailRecord.AmountReduced.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_REDUCED"].ToString());
                                dTotAmtReduced += Functions.FormatAmount(m_structEOBDetailRecord.AmountReduced.Token, m_iClientId);

                                m_structEOBDetailRecord.AmountSaved.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_SAVED"].ToString());
                                dTotAmtSaved += Functions.FormatAmount(m_structEOBDetailRecord.AmountSaved.Token, m_iClientId);

                                iTempId = Conversion.ConvertStrToInteger(objReader["MODIFIER_CODE"].ToString());
                                sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " +
                                    "CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
                                objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                                if (objTempReader != null)
                                {
                                    if (objTempReader.Read())
                                    {
                                        m_structEOBDetailRecord.ModifierCode.Token = objTempReader["SHORT_CODE"].ToString();
                                        m_structEOBDetailRecord.Modifier.Token = objTempReader["CODE_DESC"].ToString();
                                    }
                                    objTempReader.Close();
                                }

                                m_structEOBDetailRecord.ContractAmount.Token = Conversion.ConvertStrToDouble(objReader["CONTRACT_AMOUNT"].ToString());
                                m_structEOBDetailRecord.Discount.Token = Conversion.ConvertStrToDouble(objReader["DISCOUNT"].ToString());
                                m_structEOBDetailRecord.ProviderZipCode.Token = objReader["ZIP_CODE"].ToString();

                                m_structEOBDetailRecord.AmountAllowed.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_ALLOWED"].ToString());
                                dTotalAmountAllowed += Functions.FormatAmount(m_structEOBDetailRecord.AmountAllowed.Token, m_iClientId);

                                m_structEOBDetailRecord.BaseAmount.Token = Conversion.ConvertStrToDouble(objReader["BASE_AMOUNT"].ToString());
                                dTotalBaseAmount += Functions.FormatAmount(m_structEOBDetailRecord.BaseAmount.Token, m_iClientId);

                                m_structEOBDetailRecord.DiscountAmount.Token = m_structEOBDetailRecord.BaseAmount.Token * (m_structEOBDetailRecord.Discount.Token / 100);
                                dTotalDiscountAmount += Functions.FormatAmount(m_structEOBDetailRecord.DiscountAmount.Token, m_iClientId);

                                m_structEOBDetailRecord.PerDiemAmount.Token = Conversion.ConvertStrToDouble(objReader["PER_DIEM_AMT"].ToString());
                                dTotalPerDiemAmt += Functions.FormatAmount(m_structEOBDetailRecord.PerDiemAmount.Token, m_iClientId);

                                m_structEOBDetailRecord.StopLossAmount.Token = Conversion.ConvertStrToDouble(objReader["STOP_LOSS_AMT"].ToString());
                                dTotalStopLossAmt += Functions.FormatAmount(m_structEOBDetailRecord.StopLossAmount.Token, m_iClientId);

                                m_structEOBDetailRecord.FeeTableAmount.Token = Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString());
                                dTotalFeeTableAmt += Functions.FormatAmount(m_structEOBDetailRecord.FeeTableAmount.Token, m_iClientId);

                                m_structEOBDetailRecord.PrescripNo.Token = objReader["PRESCRIP_NO"].ToString();
                                m_structEOBDetailRecord.DrugName.Token = objReader["DRUG_NAME"].ToString();
                                m_structEOBDetailRecord.PrescripDate.Token = Conversion.ToDate(objReader["PRESCRIP_DATE"].ToString()).ToShortDateString();
                                //BRS FL Merge  : Umesh

                                m_structEOBDetailRecord.PhysicianLicenseNum.Token = objReader["FL_LICENSE"].ToString();
                                m_structEOBDetailRecord.RevenueCode.Token = objReader["REV_CODE"].ToString();
                                //End

                                m_structEOBDetailRecord.DiscOnFeeSchd.Token = 0;
                                if (Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString()) > 0 && Conversion.ConvertStrToDouble(objReader["DISCOUNT"].ToString()) > 0)
                                    m_structEOBDetailRecord.DiscOnFeeSchd.Token = Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString()) - Conversion.ConvertStrToDouble(objReader["AMOUNT_ALLOWED"].ToString());
                            }
                            else
                            {
                                CheckEOBPageLength(13500, m_objPrintWrapper);  // Checks to see if new page is needed
                            }

                            objFundsTransSplit = (FundsTransSplit)objDataModelFactory.GetDataModelObject("FundsTransSplit", false);
                            try
                            {
                                objFundsTransSplit.MoveTo(Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString()));
                                sFromDate = Conversion.ToDate(objFundsTransSplit.FromDate).ToShortDateString();
                                sToDate = Conversion.ToDate(objFundsTransSplit.ToDate).ToShortDateString();
                                sInvoiceNumber = objFundsTransSplit.InvoiceNumber;
                                sInvoiceDate = Conversion.ToDate(objFundsTransSplit.InvoiceDate).ToShortDateString();
                                sInvoiceBy = objFundsTransSplit.InvoicedBy;
                                objFundsTransSplit.Dispose();
                            }
                            catch
                            {
                                sFromDate = string.Empty;
                                sToDate = string.Empty;
                                sInvoiceNumber = "N/A";
                                sInvoiceDate = string.Empty;
                                sInvoiceBy = string.Empty;
                            }

                            if (p_bIsFormatFileFound)
                            {
                                m_structEOBDetailRecord.ProcedureDate.Token = sFromDate;
                                m_structEOBDetailRecord.ProcToDate.Token = sToDate;
                                m_structEOBDetailRecord.InvoiceNumber.Token = sInvoiceNumber;
                                m_structEOBDetailRecord.InvoiceDate.Token = sInvoiceDate;
                                m_structEOBDetailRecord.InvoicedBy.Token = sInvoiceBy;
                            }
                            else
                            {
                                m_objPrintWrapper.CurrentX = p_iCurrentX;
                                m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
                                m_objPrintWrapper.PrintText(sInvoiceNumber.Trim());
                                m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight; //***
                                m_objPrintWrapper.PrintText(sFromDate);
                            }

                            //Print remaining data fields
                            sTempData = objReader["BILLING_CODE_TEXT"].ToString();

                            //*** Default body ***
                            if (!p_bIsFormatFileFound)
                            {
                                m_objPrintWrapper.CurrentX = p_iCurrentX + 500;
                                m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
                                if (sTempData.Length > 6)
                                    m_objPrintWrapper.PrintText(sTempData.Substring(0, 6));
                                else
                                    m_objPrintWrapper.PrintText(sTempData);
                            }
                            else
                            {
                                m_structEOBDetailRecord.ProcedureCode.Token = sTempData;
                                m_structEOBDetailRecord.Procedure.Token = sTempData.Substring(sTempData.IndexOf('-') + 1);
                            }

                            if (!p_bIsFormatFileFound && iNumEOB > 0)
                                m_objPrintWrapper.PrintText(m_structEOBDetailRecord.EOBList[1].ToString());

                            //amount billed
                            double d = Conversion.ConvertStrToDouble(objReader["AMOUNT_BILLED"].ToString());
                            if (p_bIsFormatFileFound)
                                m_structEOBDetailRecord.AmountBilled.Token = d;
                            dTotalAmountBilled += d;
                            if (!p_bIsFormatFileFound)
                            {
                                sTempData = string.Format("{0:#,###,##0.00}", d);
                                sTempData = Functions.FillSpace(12 - sTempData.Length) + sTempData;
                                m_objPrintWrapper.CurrentX = p_iCurrentX + 900;
                                m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
                                m_objPrintWrapper.PrintText(sTempData);
                            }

                            //amount to pay
                            d = Conversion.ConvertStrToDouble(objReader["AMOUNT_TO_PAY"].ToString());
                            if (p_bIsFormatFileFound)
                                m_structEOBDetailRecord.AmountPaid.Token = d;
                            dTotalAmountPaid += d;
                            if (!p_bIsFormatFileFound)
                            {
                                sTempData = string.Format("{0:#,###,##0.00}", d);
                                sTempData = Functions.FillSpace(12 - sTempData.Length) + sTempData;
                                m_objPrintWrapper.CurrentX = p_iCurrentX + 900;
                                m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
                                m_objPrintWrapper.PrintText(sTempData);
                            }

                            //Print remaining EOB lines (see above)
                            if (!p_bIsFormatFileFound)
                            {
                                if (iNumEOB > 1)
                                {
                                    for (int i = 1; i < iNumEOB - 1; i++)
                                    {
                                        m_objPrintWrapper.CurrentX = p_iCurrentX + 300;
                                        m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
                                        CheckEOBPageLength(13800, m_objPrintWrapper); ;
                                        m_objPrintWrapper.PrintText(Functions.FillSpace(16) + m_structEOBDetailRecord.EOBList[i].ToString());
                                    }
                                }
                            }
                            else
                            {
                                //*** Custom Body - Detail record ***
                                GetCustomEOBBody(m_structEOBDetailRecord, ref m_structEOBRecord, ref m_objPrintWrapper);
                            }
                        }
                        m_structEOBDetailRecord.EOBList.Clear();
                        m_structEOBDetailRecord.DiagnosisList.Clear();
                    }//END of While
                    objReader.Close();
                }//End of IF

                if (bIsPrintTotals)
                {
                    if (p_bIsFormatFileFound)
                    {
                        m_structEOBRecord.TotalAmountBilled = dTotalAmountBilled;
                        m_structEOBRecord.TotalAmountPaid = dTotalAmountPaid;
                        m_structEOBRecord.TotalScheduledAmount = dTotSchdAmt;
                        m_structEOBRecord.TotalAmountReduced = dTotAmtReduced;
                        m_structEOBRecord.TotalAmountSaved = dTotAmtSaved;
                        m_structEOBRecord.TotalBaseAmount = dTotalBaseAmount;
                        m_structEOBRecord.TotalAmountAllowed = dTotalAmountAllowed;
                        m_structEOBRecord.TotalDiscountAmount = dTotalDiscountAmount;
                        m_structEOBRecord.TotalPerDiemAmt = dTotalPerDiemAmt;
                        m_structEOBRecord.TotalStopLossAmt = dTotalStopLossAmt;
                        m_structEOBRecord.TotalFeeTableAmt = dTotalFeeTableAmt;
                    }
                    else
                    {
                        sTempData = string.Format("{0:#,###,##0.00}", dTotalAmountBilled);
                        sTempData = Functions.FillSpace(12 - sTempData.Length) + sTempData;
                        m_objPrintWrapper.SetFontBold(true);
                        m_objPrintWrapper.CurrentX = p_iCurrentX;
                        m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
                        m_objPrintWrapper.PrintText("TOTALS:");
                        m_objPrintWrapper.CurrentX = p_iCurrentX + 300;
                        m_objPrintWrapper.PrintText(sTempData);
                        sTempData = string.Format("{0:#,###,##0.00}", dTotalAmountPaid);
                        sTempData = Functions.FillSpace(12 - sTempData.Length) + sTempData;
                        m_objPrintWrapper.CurrentX = p_iCurrentX + 300;
                        m_objPrintWrapper.PrintText(sTempData);
                        m_objPrintWrapper.SetFontBold(false);
                    }
                }

                if (!p_bIsFormatFileFound)
                {
                    m_objPrintWrapper.CurrentX = p_iCurrentX;
                    m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
                    CheckEOBPageLength(12300, m_objPrintWrapper); ;
                    m_objPrintWrapper.PrintText(Functions.FillSpace(4) + CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.ContactUs", m_iClientId)));//sharishkumar Jira 835
                    m_objPrintWrapper.CurrentX = p_iCurrentX;
                    m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
                    m_objPrintWrapper.PrintText(Functions.FillSpace(4) + "Sincerely,");
                    m_objPrintWrapper.CurrentX = p_iCurrentX;
                    m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
                    m_objPrintWrapper.CurrentX = p_iCurrentX;
                    m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
                    m_objPrintWrapper.PrintText(Functions.FillSpace(4) + objCommon.m_stLoginInfo.Username);
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAR.GetPrintCheckEOBBody.ErrorGet", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objSQL != null)
                    objSQL = null;

                if (objReader != null)
                    objReader.Dispose();

                if (objTempReader != null)
                    objTempReader.Dispose();

                if (objFundsTransSplit != null)
                    objFundsTransSplit.Dispose();
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                    objDataModelFactory = null;
                }
            }
        }
        #endregion

        #region "PrintEOBReport(int p_iTransId, bool p_bIsSilent, string p_sState, bool p_bIsPostCheck, ArrayList p_arrChkPrtList, out PrintWrapper p_objPrintWrapper, out CommonEOB p_objEOBReport)"
        /// Name		: PrintEOBReport
        /// Author		: Anurag Agarwal
        /// Date Created	: 13 Jan 2005		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Prints EOB to an Active Reports EOB
        /// </summary>
        /// <param name="p_iTransId">Funds TransId</param>
        /// <param name="p_bIsSilent">Flag for enabling/disabling the No Data found exceptions</param>
        /// <param name="p_sState">State Id</param>
        /// <param name="p_bIsPostCheck">Flag for Post Checks</param>
        /// <param name="p_arrChkPrtList">Array List containing the list of printed checks</param>
        /// <param name="p_objPrintWrapper">Object of printWrapper class containing the PDF doc. 
        /// This will populated if report is generated through component one</param>
        /// <param name="p_objEOBReport">Object of Common EOB class containing the PDF doc. 
        /// This will populated if report is generated through Active Reports</param>
        internal void PrintEOBReport(int p_iTransId, bool p_bIsSilent, string p_sState, bool p_bIsPostCheck, ArrayList p_arrChkPrtList, out PrintWrapper p_objPrintWrapper, out CommonEOB p_objEOBReport)
        {
            string sSQL = string.Empty;
            string sTempData = string.Empty;
            int iInvoiceId = 0;
            string sName = string.Empty;
            DbReader objReader = null;
            Funds objFunds = null;
            BrsInvoice objBrsInvoice = null;
            Entity objEntity = null;
            CEOBRecord structCEOBRecord;
            Supplementals objSupplementals = null;
            LocalCache objLocalCache = null;
            string strStateDesc = string.Empty;
            string strStateCode = string.Empty;
            string sJurisdiction = string.Empty;
            string sFileNumber = string.Empty;
            string sClaimantFileName = string.Empty;
            string sPayeeFileName = string.Empty;
            int iClaimantEid = 0;
            string sTransDateText = string.Empty;
            string sClaimNumberText = string.Empty;
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            string sClaimantName = string.Empty;
            string sRegClaimantName = string.Empty;
            string sClaimantAddr1 = string.Empty;
            string sClaimantAddr2 = string.Empty;
            string sClaimantAddr3 = string.Empty;
            string sClaimantAddr4 = string.Empty;
            string sClaimantCity = string.Empty;
            string sClaimantTaxId = string.Empty;
            bool bIsICNFieldPresent = false;
            string sFTSRowIDs = string.Empty;
            int iLineCount = 1; //rsharma220 MITS 32303
            string sThisCode = string.Empty;
            string sLastCode = string.Empty;
            string sThisCodeID = string.Empty;
            string sLastCodeID = string.Empty;
            int iDivisionId = 0;
            PdfExport objPDF_AR = null;
            rptEOB objrptEOB = null;
            rptEOB_TX objrptEOB_TX = null;
            rptEOP objrptEOP = null;
            rptEOR objrptEOR = null;

            //Initializing the Output parameters
            p_objEOBReport = null;
            p_objPrintWrapper = null;
            DataModelFactory objDataModelFactory = null;
            Common objCommon = null;
            try
            {
                objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId);//sharishkumar Jira 835
                objCommon = new Common();
                structCEOBRecord = new CEOBRecord();

                objFunds = (Funds)objDataModelFactory.GetDataModelObject("Funds", false);
                try
                {
                    objFunds.MoveTo(p_iTransId);
                }
                catch
                {
                    if (!p_bIsSilent)
                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.SaveItem", m_iClientId));//sharishkumar Jira 835
                    return;
                }

                sSQL = "SELECT STATES.STATE_ID, CLAIM.FILE_NUMBER  FROM CLAIM,STATES WHERE CLAIM_ID = " +
                    objFunds.ClaimId + " AND FILING_STATE_ID = STATES.STATE_ROW_ID";
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sJurisdiction = objReader["STATE_ID"].ToString();
                        sFileNumber = objReader["FILE_NUMBER"].ToString();
                    }
                    objReader.Close();
                }

                if (p_sState.Trim() == string.Empty)
                    p_sState = sJurisdiction;

                if (objFunds.Supplementals.Count() > 0)
                {
                    objSupplementals = objFunds.Supplementals;
                    if (Conversion.ConvertObjToInt(objFunds.Supplementals["TRANS_ID"].Value, m_iClientId) == p_iTransId)
                    {
                        if (IsSuppFieldExist(objSupplementals, "DCN_TEXT"))
                        {
                            structCEOBRecord.DCN = objSupplementals["DCN_TEXT"] != null ? Conversion.ConvertObjToStr(objSupplementals["DCN_TEXT"].Value) : string.Empty;//Deb UnderWriters
                            structCEOBRecord.OtherRef1 = objSupplementals["OTHER_REF_1_TEXT"] != null ? Conversion.ConvertObjToStr(objSupplementals["OTHER_REF_1_TEXT"].Value).Trim() : string.Empty;
                            structCEOBRecord.OtherRef2 = objSupplementals["OTHER_REF_2_TEXT"] != null ? Conversion.ConvertObjToStr(objSupplementals["OTHER_REF_2_TEXT"].Value).Trim() : string.Empty;
                            structCEOBRecord.ICNControlNumber = objSupplementals["ICN_TEXT"] != null ? Conversion.ConvertObjToStr(objSupplementals["ICN_TEXT"].Value).Trim() : string.Empty; 
                        }
                        else
                        {
                            structCEOBRecord.DCN = string.Empty;
                            structCEOBRecord.OtherRef1 = string.Empty;
                            structCEOBRecord.OtherRef2 = string.Empty;
                            structCEOBRecord.ICNControlNumber = string.Empty;
                        }
                    }
                    bIsICNFieldPresent = IsSuppFieldExist(objSupplementals, "ICN_TEXT");
                    objSupplementals.Dispose();
                }

                if (bIsICNFieldPresent)
                {
                    structCEOBRecord.ReasonCodes = string.Empty;
                    foreach (FundsTransSplit objFundsTransSplit in objFunds.TransSplitList)
                    {
                        if (sFTSRowIDs == string.Empty)
                            sFTSRowIDs = objFundsTransSplit.SplitRowId.ToString();
                        else
                            sFTSRowIDs += ", " + objFundsTransSplit.SplitRowId.ToString();
                    }

                    sSQL = "SELECT DISTINCT(INVDETAIL_EOR_CODE.EOR_CODE_ID), INVDETAIL_EOR_CODE.EOR_CODE, MESSAGE " +
                        "FROM INVDETAIL_X_EOR, INVDETAIL_EOR_CODE WHERE INVDETAIL_X_EOR.EOR_CODE_ID = " +
                        "INVDETAIL_EOR_CODE.EOR_CODE_ID AND INVDETAIL_X_EOR.TRANS_ID =" + objFunds.TransId +
                        " AND INVDETAIL_X_EOR.FTS_ROW_ID IN (" + sFTSRowIDs + ")ORDER BY " +
                        "INVDETAIL_EOR_CODE.EOR_CODE, INVDETAIL_EOR_CODE.EOR_CODE_ID";
                    objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            sThisCode = objReader["EOR_CODE"].ToString();
                            if (sThisCode != sLastCode)
                            {
                                if (structCEOBRecord.ReasonCodes == string.Empty)
                                {
                                    structCEOBRecord.ReasonCodes = iLineCount.ToString();
                                    structCEOBRecord.CodeMessages = iLineCount + " - " + objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
                                }
                                else
                                {
                                    structCEOBRecord.ReasonCodes += "," + iLineCount;
                                    structCEOBRecord.CodeMessages += iLineCount + " - " + objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
                                }
                                iLineCount++;
                            }
                            else
                            {
                                if (structCEOBRecord.CodeMessages == string.Empty)
                                    structCEOBRecord.CodeMessages = objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
                                else
                                    structCEOBRecord.CodeMessages += objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
                            }
                            sLastCode = objReader["EOR_CODE"].ToString();
                        }
                        objReader.Close();
                    }
                }//end fo If
                else
                {
                    structCEOBRecord.ReasonCodes = string.Empty;
                    structCEOBRecord.CodeMessages = string.Empty;
                }

                //Use first Trans Code
                foreach (FundsTransSplit objFundsTransSplit in objFunds.TransSplitList)
                {
                    structCEOBRecord.PaymentCode = objFundsTransSplit.TransTypeCode.ToString();
                    structCEOBRecord.Code = objFundsTransSplit.InvoiceNumber;
                    structCEOBRecord.PeriodCovered = Conversion.GetDate(objFundsTransSplit.FromDate) + " - " +
                        Conversion.GetDate(objFundsTransSplit.ToDate);
                }
                //Open up invoice record
                objBrsInvoice = (BrsInvoice)objDataModelFactory.GetDataModelObject("BrsInvoice", false);
                try
                {
                    objBrsInvoice.MoveToTransId(p_iTransId);
                }
                catch
                {
                    if (!p_bIsSilent)
                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems", m_iClientId));//sharishkumar Jira 835
                    return;
                }

                objLocalCache = new LocalCache(m_ConnectionString, m_iClientId);

                iInvoiceId = objBrsInvoice.InvoiceId;
                if (iInvoiceId == 0)
                    return;

                //Make sure we have detail items (exit if none)
                sSQL = "SELECT COUNT(INVOICE_ID) FROM INVOICE_DETAIL WHERE INVOICE_ID = " + iInvoiceId;//Deb UnderWriters
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        if (Conversion.ConvertObjToInt(objReader[0], m_iClientId) == 0)
                        {
                            if (!p_bIsSilent)
                                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems", m_iClientId));//sharishkumar Jira 835
                            return;
                        }
                    }
                    objReader.Close();
                }

                //... Payee Info
                sName = ((string)(objBrsInvoice.FirstNameText + " " + objBrsInvoice.LastNameText)).Trim();
                structCEOBRecord.PayeeFirstName = objBrsInvoice.FirstNameText;
                structCEOBRecord.PayeeLastName = objBrsInvoice.LastNameText;
                structCEOBRecord.PayeeFullName = sName;
                structCEOBRecord.PayeeAddr1 = objBrsInvoice.Addr1Text;
                structCEOBRecord.PayeeAddr2 = objBrsInvoice.Addr2Text;
                structCEOBRecord.PayeeAddr3 = objBrsInvoice.Addr3Text;
                structCEOBRecord.PayeeAddr4 = objBrsInvoice.Addr4Text;
                objLocalCache.GetStateInfo(objBrsInvoice.StateId, ref strStateCode, ref strStateDesc);
                //Deb : MITS 32303
                //structCEOBRecord.PayeeState = ((string)(strStateCode + " " + strStateDesc)).Trim();
                structCEOBRecord.PayeeState = ((string)(strStateCode + " ")).Trim();
                //Deb : MITS 32303
                structCEOBRecord.PayeePostalCode = objBrsInvoice.PostalCodeText;
                structCEOBRecord.PayeeCity = objBrsInvoice.CityText;
                structCEOBRecord.ClaimNumber = objBrsInvoice.ClaimNumberText;
                // akaushik5 Changed for MITS 35846 Starts
                //structCEOBRecord.OrigInvDate = Conversion.GetDate(objBrsInvoice.DttmRcdAdded);
                structCEOBRecord.OrigInvDate = objBrsInvoice.DttmRcdAdded;
                // akaushik5 Changed for MITS 35846 Ends
                structCEOBRecord.OrigInvUser = objBrsInvoice.AddedByUser;
                iClaimantEid = objBrsInvoice.ClaimantEid;
                //Changed by Gagan for MITS 11520 : Start
                if (objBrsInvoice.TransDateText == "" || objBrsInvoice.TransDateText == null)
                    sTransDateText = objFunds.TransDate;
                else
                    sTransDateText = objBrsInvoice.TransDateText;
                structCEOBRecord.TransDate = sTransDateText;
                //sTransDateText = objBrsInvoice.TransDateText; 
                //Changed by Gagan for MITS 11520 : End
                sClaimNumberText = objBrsInvoice.ClaimNumberText;

                //... Payee Tax Id
                objEntity = (Entity)objDataModelFactory.GetDataModelObject("Entity", false);
                try
                {
                    objEntity.MoveTo(objFunds.PayeeEid);
                    structCEOBRecord.PayeeTaxID = objEntity.TaxId;
                    structCEOBRecord.VendorNumber = objEntity.TaxId;
                    //Changed by Gagan for MITS 11520 : Start
                    structCEOBRecord.PayeeNPI = objEntity.NPINumber;
                    //Changed by Gagan for MITS 11520 : End
                }
                catch { }

                try
                {
                    objEntity.MoveTo(iClaimantEid);

                    sFirstName = objEntity.FirstName;
                    sLastName = objEntity.LastName;
                    sClaimantName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
                    sRegClaimantName = ((string)(sFirstName + " " + sLastName)).Trim();
                    sClaimantAddr1 = objEntity.Addr1;
                    sClaimantAddr2 = objEntity.Addr2;
                    sClaimantAddr3 = objEntity.Addr3;
                    sClaimantAddr4 = objEntity.Addr4;
                    objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                    if (strStateCode != string.Empty || strStateDesc != string.Empty)
                        structCEOBRecord.ClaimantState = ((string)(strStateCode + " " + strStateDesc)).Trim().Substring(0, 4);
                    structCEOBRecord.ClaimantCity = objEntity.City;
                    structCEOBRecord.ClaimantPostalCode = objEntity.ZipCode;
                    sClaimantCity = structCEOBRecord.ClaimantCity + ", " + structCEOBRecord.ClaimantState
                        + " " + structCEOBRecord.ClaimantPostalCode;
                    sClaimantTaxId = objEntity.TaxId;

                    structCEOBRecord.ClaimantFullName = sClaimantName;
                    structCEOBRecord.ClaimantLastName = sLastName;
                    structCEOBRecord.ClaimantFirstName = sFirstName;
                    structCEOBRecord.ClaimantAddr1 = sClaimantAddr1;
                    structCEOBRecord.ClaimantAddr2 = sClaimantAddr2;
                    structCEOBRecord.ClaimantAddr3 = sClaimantAddr3;
                    structCEOBRecord.ClaimantAddr4 = sClaimantAddr4;
                    structCEOBRecord.ClaimantSSN = sClaimantTaxId;
                    if (sClaimantAddr1 != string.Empty)
                        sTempData = sClaimantAddr1;
                    if (sClaimantAddr2 != string.Empty)
                        sTempData += Functions.CRLF + sClaimantAddr2;
                    if (sClaimantAddr3 != string.Empty)
                        sTempData += Functions.CRLF + sClaimantAddr3;
                    if (sClaimantAddr4 != string.Empty)
                        sTempData += Functions.CRLF + sClaimantAddr4;
                    if (sClaimantCity != string.Empty)
                        sTempData += Functions.CRLF + sClaimantCity;
                    structCEOBRecord.ClaimantFullAddress = sTempData;
                }
                catch { }

                int iReportedEID = 0;

                //... Event Date Info
                sSQL = "SELECT DATE_OF_EVENT, DATE_REPORTED, RPTD_BY_EID FROM EVENT,CLAIM WHERE " +
                    "CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + objFunds.ClaimId;
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        structCEOBRecord.EventDate = objReader["DATE_OF_EVENT"].ToString();
                        structCEOBRecord.DateReported = objReader["DATE_REPORTED"].ToString();
                        iReportedEID = Conversion.ConvertStrToInteger(objReader["RPTD_BY_EID"].ToString());
                    }
                    objReader.Close();
                }

                //... Reported By Name/Address
                try
                {
                    objEntity.MoveTo(iReportedEID);
                    sFirstName = objEntity.FirstName;
                    sLastName = objEntity.LastName;
                    structCEOBRecord.RptdFullName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
                }
                catch { }

                //... Employee department chain info
                sSQL = "SELECT ORG_HIERARCHY.* FROM EMPLOYEE,ORG_HIERARCHY WHERE EMPLOYEE.EMPLOYEE_EID=" +
                    iClaimantEid + " AND ORG_HIERARCHY.DEPARTMENT_EID = EMPLOYEE.DEPT_ASSIGNED_EID";
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["DEPARTMENT_EID"].ToString()), ref structCEOBRecord.EmpDeptCode, ref structCEOBRecord.EmpDepartment);
                        objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["FACILITY_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpFacility);
                        objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["LOCATION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpLocation);
                        objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["DIVISION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpDivision);
                        objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["REGION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpRegion);
                        objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["COMPANY_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpCompany);
                        objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["CLIENT_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpClient);

                        //Get the address of the facility
                        iDivisionId = Conversion.ConvertStrToInteger(objReader["DIVISION_EID"].ToString());

                        try
                        {
                            objEntity.MoveTo(iDivisionId);
                            structCEOBRecord.DivisionName = objEntity.FirstName + objEntity.LastName;
                            structCEOBRecord.DivisionTaxID = objEntity.TaxId;
                            structCEOBRecord.EmpDivisionNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                            structCEOBRecord.DivisionNameAddr += structCEOBRecord.DivisionName + Functions.CRLF + objEntity.Addr1;

                            if (objEntity.Addr2 != string.Empty)
                                structCEOBRecord.DivisionNameAddr += Functions.CRLF + objEntity.Addr2;
                            if (objEntity.Addr3 != string.Empty)
                                structCEOBRecord.DivisionNameAddr += Functions.CRLF + objEntity.Addr3;
                            if (objEntity.Addr3 != string.Empty)
                                structCEOBRecord.DivisionNameAddr += Functions.CRLF + objEntity.Addr3;

                            if (objEntity.City != string.Empty)
                            {
                                objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                                structCEOBRecord.DivisionNameAddr += Functions.CRLF + objEntity.City + ", " +
                                    //Deb: MITS 32303
                                    //strStateCode + " " + strStateDesc + " " + objEntity.ZipCode;
                                    strStateDesc + " " + objEntity.ZipCode;
                                    //Deb: MITS 32303
                            }
                        }
                        catch (RecordNotFoundException p_objRecordNotFoundException)
                        {
                            p_objRecordNotFoundException = null;
                        }

                        //Get the address of the facility
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["FACILITY_EID"].ToString()));
                            structCEOBRecord.EmpFacilityNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                            if (objEntity.Addr1 != string.Empty)
                                structCEOBRecord.EmpFacilityAddress = objEntity.Addr1;
                            if (objEntity.Addr2 != string.Empty)
                                structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.Addr2;
                            if (objEntity.Addr3 != string.Empty)
                                structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.Addr3;
                            if (objEntity.Addr4 != string.Empty)
                                structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.Addr4;
                            if (objEntity.City != string.Empty || objEntity.StateId != 0 || objEntity.ZipCode != string.Empty)
                            {
                                objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                                structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.City + ", " +
                                    strStateCode + " " + strStateDesc + " " + objEntity.ZipCode;
                            }
                        }
                        catch (RecordNotFoundException p_objRecordNotFoundException)
                        {
                            p_objRecordNotFoundException = null;
                        }

                        //... Get NAICS codes for all of the hierarchy levels
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["DEPARTMENT_EID"].ToString()));
                            structCEOBRecord.EmpDepartmentNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                        }
                        catch { }
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["LOCATION_EID"].ToString()));
                            structCEOBRecord.EmpLocationNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                        }
                        catch { }
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["REGION_EID"].ToString()));
                            structCEOBRecord.EmpRegionNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                        }
                        catch { }
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["OPERATION_EID"].ToString()));
                            structCEOBRecord.EmpOperationNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                        }
                        catch { }
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["COMPANY_EID"].ToString()));
                            structCEOBRecord.EmpCompanyNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                        }
                        catch { }
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["CLIENT_EID"].ToString()));
                            structCEOBRecord.EmpCompanyNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                        }
                        catch { }

                    }
                    objReader.Close();
                }

                sSQL = "SELECT LAST_NAME,FIRST_NAME,ABBREVIATION,CURRENT_ADJ_FLAG,PHONE1 FROM ENTITY," +
                    "CLAIM_ADJUSTER WHERE ADJUSTER_EID=ENTITY_ID AND CLAIM_ID=" + objFunds.ClaimId;
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        structCEOBRecord.AdjusterAbbrev = objReader["ABBREVIATION"].ToString();
                        structCEOBRecord.AdjusterFullName = ((string)(objReader["FIRST_NAME"].ToString() + " " + objReader["LAST_NAME"].ToString())).Trim();
                        structCEOBRecord.AdjusterLastName = objReader["LAST_NAME"].ToString();
                        //Arnab: MITS-10722 - Getting Adjuster phone no
                        structCEOBRecord.AdjusterPhone1 = Conversion.ConvertObjToStr(objReader["PHONE1"]);
                        //MITS-10722 End
                        if (Conversion.ConvertObjToInt(objReader["CURRENT_ADJ_FLAG"], m_iClientId) != 0)
                            break;
                    }
                    objReader.Close();
                }

                structCEOBRecord.BatchNumber = objFunds.BatchNumber.ToString();
                structCEOBRecord.CheckNumber = string.Empty;
                structCEOBRecord.PatientAccountNumber = string.Empty;
                structCEOBRecord.FeeOrCustomary = string.Empty;
                structCEOBRecord.ControlNumber = objFunds.CtlNumber;
                structCEOBRecord.ReceiptDate = string.Empty;
                //Ashish Ahuja Mits 33681 : Start
                structCEOBRecord.CheckNumber = objFunds.TransNumber.ToString();
                //Ashish Ahuja Mits 33681 : End
                if (p_bIsPostCheck)
                    structCEOBRecord.CheckNumber = objFunds.TransNumber.ToString();
                else
                {
                    if (p_arrChkPrtList != null)
                    {
                        foreach (PrintedCheckDetail structPrintedCheckDetail in p_arrChkPrtList)
                        {
                            if (structPrintedCheckDetail.TransId == p_iTransId)
                                structCEOBRecord.CheckNumber = structPrintedCheckDetail.CheckNum.ToString();
                        }
                    }
                }

                m_structEOBRecord.CheckNumber = structCEOBRecord.CheckNumber;
                //Fetching EOB details
                GetEOBDetail(objFunds, ref structCEOBRecord, iInvoiceId);


                //Changed by Gagan for MITS 11520 : Start
                //SSN can have format as ###-##-#### or ##-#######
                if (structCEOBRecord.ClaimantSSN != "")
                {
                    if (structCEOBRecord.ClaimantSSN.Length == 11)
                        structCEOBRecord.ClaimantSSN = "XXX-XX-" + structCEOBRecord.ClaimantSSN.Substring(7, 4);
                    else if (structCEOBRecord.ClaimantSSN.Length == 10)
                        structCEOBRecord.ClaimantSSN = "XX-XXX" + structCEOBRecord.ClaimantSSN.Substring(6, 4);
                }
                //Changed by Gagan for MITS 11520 : End


                switch (p_sState)
                {
                    case MI:
                    case "MI":    
                        objrptEOB = new rptEOB(m_ConnectionString, objCommon.m_stLoginInfo.Username,m_iClientId);
                        objrptEOB.EOBReportData = structCEOBRecord;

                        objrptEOB.Run();
                        p_objEOBReport = objrptEOB;
                        break;

                    case TX:
                    case "TX":
                        objrptEOB_TX = new rptEOB_TX(m_ConnectionString, objCommon.m_stLoginInfo.Username,m_iClientId);
                        objrptEOB_TX.EOBReportData = structCEOBRecord;
                        objrptEOB_TX.Run();
                        p_objEOBReport = objrptEOB_TX;
                        break;

                    case "EOR":
                        objrptEOR = new rptEOR(m_ConnectionString, objCommon.m_stLoginInfo.Username,m_iClientId);
                        objrptEOR.EOBReportData = structCEOBRecord;
                        objrptEOR.Run();
                        p_objEOBReport = objrptEOR;
                        break;

                    case "EOP":
                        objrptEOP = new rptEOP(m_ConnectionString, objCommon.m_stLoginInfo.Username,m_iClientId);
                        objrptEOP.EOBReportData = structCEOBRecord;
                        objrptEOP.Run();
                        p_objEOBReport = objrptEOP;
                        break;

                    default:
                        GetPrintCheckEOB(objFunds, true, out p_objPrintWrapper);
                        break;
                }

                //Stamp audit info on invoice record
                sSQL = "UPDATE INVOICE SET DTTM_EOB_PRINTED = '" + Conversion.ToDbDateTime(DateTime.Now) + "', " +
                    "EOB_PRINTED_USER = '" + objCommon.m_stLoginInfo.Username + "' WHERE INVOICE_ID = " + iInvoiceId;
                objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);

                ////TODO Logging component is in process to be finalized, As that will be finalized this will be done
                //MsgBox "Explanation Of Benefits (EOB) has been printed.", vbInformation + vbOKOnly, "Bill Review System"

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAR.PrintEOBReport.ErrorCreate", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();

                if (objFunds != null)
                    objFunds.Dispose();

                if (objBrsInvoice != null)
                    objBrsInvoice.Dispose();

                if (objEntity != null)
                    objEntity.Dispose();

                if (objSupplementals != null)
                    objSupplementals.Dispose();

                if (objLocalCache != null)
                    objLocalCache.Dispose();

                if (objPDF_AR != null)
                    objPDF_AR.Dispose();

                if (objrptEOB != null)
                    objrptEOB.Dispose();

                if (objrptEOB_TX != null)
                    objrptEOB_TX.Dispose();

                if (objrptEOP != null)
                    objrptEOP.Dispose();

                if (objrptEOR != null)
                    objrptEOR.Dispose();
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                    objDataModelFactory = null;
                }
            }
        }
        #endregion

        #endregion

        #region "New function added from PrintEOBAR.cs to this file to support MI and TX eob's for HTML reports"

        #region "PrintEOBHTMLReport(int p_iTransId, bool p_bIsSilent, string p_sState, bool p_bIsPostCheck, ArrayList p_arrChkPrtList, out string p_sPDFSaveFilePath, out CommonEOB p_objEOBReport)"
        /// <summary>
        /// Prints EOB to an Active Reports EOB
        /// </summary>
        /// <param name="p_iTransId">Funds TransId</param>
        /// <param name="p_bIsSilent">Flag for enabling/disabling the No Data found exceptions</param>
        /// <param name="p_sState">State Id</param>
        /// <param name="p_bIsPostCheck">Flag for Post Checks</param>
        /// <param name="p_arrChkPrtList">Array List containing the list of printed checks</param>
        /// <param name="p_sPDFSaveFilePath">The P_S PDF save file path.</param>
        /// <param name="p_objEOBReport">Object of Common EOB class containing the PDF doc.
        /// This will populated if report is generated through Active Reports</param>
        /// <param name="objFunds">The object funds.</param>
        /// Name		: PrintEOBHTMLReport
        /// Author		: Debabrata Biswas
        /// Date Created	: 07 Oct 2009
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// *               *
        /// *               *
        /// ************************************************************
        /// <exception cref="Riskmaster.ExceptionTypes.RMAppException">
        /// </exception>
        // akaushik5 Changed for MITS 35846 Starts
        //internal void PrintEOBHTMLReport(int p_iTransId, bool p_bIsSilent, string p_sState, bool p_bIsPostCheck, ArrayList p_arrChkPrtList, out string p_sPDFSaveFilePath, out CommonEOB p_objEOBReport)
        internal void PrintEOBHTMLReport(int p_iTransId, bool p_bIsSilent, string p_sState, bool p_bIsPostCheck, ArrayList p_arrChkPrtList, out string p_sPDFSaveFilePath, out CommonEOB p_objEOBReport, Funds objFunds)
        // akaushik5 Changed for MITS 35846 Ends
        {
            string sSQL = string.Empty;
            string sTempData = string.Empty;
            int iInvoiceId = 0;
            string sName = string.Empty;
            DbReader objReader = null;
            // akaushik5 Commented for MITS 35846 Starts
            //Funds objFunds = null;
            // akaushik5 Commented for MITS 35846 Ends
            BrsInvoice objBrsInvoice = null;
            Entity objEntity = null;
            CEOBRecord structCEOBRecord;
            Supplementals objSupplementals = null;
            // akaushik5 Commented for MITS 35846 Starts
            //LocalCache m_objLocalCache = null;
            // akaushik5 Commented for MITS 35846 Ends
            string strStateDesc = string.Empty;
            string strStateCode = string.Empty;
            // akaushik5 Commented for MITS 35846 Starts
            //string sJurisdiction = string.Empty;
            //string sFileNumber = string.Empty;
            // akaushik5 Commented for MITS 35846 Ends
            string sClaimantFileName = string.Empty;
            string sPayeeFileName = string.Empty;
            int iClaimantEid = 0;
            string sTransDateText = string.Empty;
            string sClaimNumberText = string.Empty;
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            string sClaimantName = string.Empty;
            string sRegClaimantName = string.Empty;
            string sClaimantAddr1 = string.Empty;
            string sClaimantAddr2 = string.Empty;
            string sClaimantAddr3 = string.Empty;
            string sClaimantAddr4 = string.Empty;
            string sClaimantCity = string.Empty;
            string sClaimantTaxId = string.Empty;
            bool bIsICNFieldPresent = false;
            string sFTSRowIDs = string.Empty;
            int iLineCount = 1; //rsharma220 MITS 32303
            string sThisCode = string.Empty;
            string sLastCode = string.Empty;
            string sThisCodeID = string.Empty;
            string sLastCodeID = string.Empty;
            int iDivisionId = 0;
            PdfExport objPDF_AR = null;
            rptEOB objrptEOB = null;
            rptEOB_TX objrptEOB_TX = null;
            rptEOP objrptEOP = null;
            rptEOR objrptEOR = null;

            p_sPDFSaveFilePath = "";
            //Initializing the Output parameters
            p_objEOBReport = null;
            // akaushik5 Commented for MITS 35846 Starts
            //DataModelFactory objDataModelFactory = null;
            // akaushik5 Commented for MITS 35846 Ends
            Common objCommon = null;
            try
            {
                // akaushik5 Added for MITS 35846 Starts
                objBrsInvoice = (BrsInvoice)this.m_objDataModelFactory.GetDataModelObject("BrsInvoice", false);
                try
                {
                    objBrsInvoice.MoveToTransId(p_iTransId);
                }
                catch
                {
                    if (!p_bIsSilent)
                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems", m_iClientId));//sharishkumar Jira 835
                    return;
                }

                iInvoiceId = objBrsInvoice.InvoiceId;
                if (iInvoiceId == 0)
                {
                    return;
                }
                // akaushik5 Added for MITS 35846 Ends
                
                structCEOBRecord = new CEOBRecord();
                objCommon = new Common();
                // akaushik5 Changed for MITS 35846 Starts
                //objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword);
                //objFunds = this.m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds;
                //try
                //{
                //    objFunds.MoveTo(p_iTransId);
                //}
                //catch
                //{
                //    if (!p_bIsSilent)
                //        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.SaveItem"));
                //    return;
                //}

                //sSQL = "SELECT STATES.STATE_ID, CLAIM.FILE_NUMBER  FROM CLAIM,STATES WHERE CLAIM_ID = " +
                //    objFunds.ClaimId + " AND FILING_STATE_ID = STATES.STATE_ROW_ID";
                //objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                //if (objReader != null)
                //{
                //    if (objReader.Read())
                //    {
                //        sJurisdiction = objReader["STATE_ID"].ToString();
                //        sFileNumber = objReader["FILE_NUMBER"].ToString();
                //    }
                //    objReader.Close();
                //}

                //if (p_sState.Trim() == string.Empty)
                //    p_sState = sJurisdiction;
                if (object.ReferenceEquals(objFunds, null))
                {
                    objFunds = this.m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds;
                    try
                    {
                        objFunds.MoveTo(p_iTransId);
                    }
                    catch
                    {
                        if (!p_bIsSilent)
                            throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.SaveItem", m_iClientId));//sharishkumar Jira 835
                        return;
                    }
                }
                // akaushik5 Changed for MITS 35846 Ends
                                
                if (objFunds.Supplementals.Count() > 0)
                {
                    objSupplementals = objFunds.Supplementals;
                    if (Conversion.ConvertObjToInt(objFunds.Supplementals["TRANS_ID"].Value, m_iClientId) == p_iTransId)
                    {
                        if (IsSuppFieldExist(objSupplementals, "DCN_TEXT"))
                        {
                            structCEOBRecord.DCN = objSupplementals["DCN_TEXT"] != null ? Conversion.ConvertObjToStr(objSupplementals["DCN_TEXT"].Value) : string.Empty;//Deb UnderWriters
                            structCEOBRecord.OtherRef1 = objSupplementals["OTHER_REF_1_TEXT"] != null ? Conversion.ConvertObjToStr(objSupplementals["OTHER_REF_1_TEXT"].Value).Trim() : string.Empty;
                            structCEOBRecord.OtherRef2 = objSupplementals["OTHER_REF_2_TEXT"] != null ? Conversion.ConvertObjToStr(objSupplementals["OTHER_REF_2_TEXT"].Value).Trim() : string.Empty;
                            structCEOBRecord.ICNControlNumber = objSupplementals["ICN_TEXT"] != null ? Conversion.ConvertObjToStr(objSupplementals["ICN_TEXT"].Value).Trim() : string.Empty; 
                        }
                        else
                        {
                            structCEOBRecord.DCN = string.Empty;
                            structCEOBRecord.OtherRef1 = string.Empty;
                            structCEOBRecord.OtherRef2 = string.Empty;
                            structCEOBRecord.ICNControlNumber = string.Empty;
                        }
                    }
                    bIsICNFieldPresent = IsSuppFieldExist(objSupplementals, "ICN_TEXT");
                    objSupplementals.Dispose();
                }

                if (bIsICNFieldPresent)
                {
                    structCEOBRecord.ReasonCodes = string.Empty;
                    foreach (FundsTransSplit objFundsTransSplit in objFunds.TransSplitList)
                    {
                        if (sFTSRowIDs == string.Empty)
                            sFTSRowIDs = objFundsTransSplit.SplitRowId.ToString();
                        else
                            sFTSRowIDs += ", " + objFundsTransSplit.SplitRowId.ToString();
                    }

                    sSQL = "SELECT DISTINCT(INVDETAIL_EOR_CODE.EOR_CODE_ID), INVDETAIL_EOR_CODE.EOR_CODE, MESSAGE " +
                        "FROM INVDETAIL_X_EOR, INVDETAIL_EOR_CODE WHERE INVDETAIL_X_EOR.EOR_CODE_ID = " +
                        "INVDETAIL_EOR_CODE.EOR_CODE_ID AND INVDETAIL_X_EOR.TRANS_ID =" + objFunds.TransId +
                        " AND INVDETAIL_X_EOR.FTS_ROW_ID IN (" + sFTSRowIDs + ")ORDER BY " +
                        "INVDETAIL_EOR_CODE.EOR_CODE, INVDETAIL_EOR_CODE.EOR_CODE_ID";
                    objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            sThisCode = objReader["EOR_CODE"].ToString();
                            if (sThisCode != sLastCode)
                            {
                                if (structCEOBRecord.ReasonCodes == string.Empty)
                                {
                                    structCEOBRecord.ReasonCodes = iLineCount.ToString();
                                    structCEOBRecord.CodeMessages = iLineCount + " - " + objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
                                }
                                else
                                {
                                    structCEOBRecord.ReasonCodes += "," + iLineCount;
                                    structCEOBRecord.CodeMessages += iLineCount + " - " + objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
                                }
                                iLineCount++;
                            }
                            else
                            {
                                if (structCEOBRecord.CodeMessages == string.Empty)
                                    structCEOBRecord.CodeMessages = objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
                                else
                                    structCEOBRecord.CodeMessages += objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
                            }
                            sLastCode = objReader["EOR_CODE"].ToString();
                        }
                        objReader.Close();
                    }
                }//end fo If
                else
                {
                    structCEOBRecord.ReasonCodes = string.Empty;
                    structCEOBRecord.CodeMessages = string.Empty;
                }

                //Use first Trans Code
                foreach (FundsTransSplit objFundsTransSplit in objFunds.TransSplitList)
                {
                    structCEOBRecord.PaymentCode = objFundsTransSplit.TransTypeCode.ToString();
                    structCEOBRecord.Code = objFundsTransSplit.InvoiceNumber;
                    structCEOBRecord.PeriodCovered = Conversion.GetDate(objFundsTransSplit.FromDate) + " - " +
                        Conversion.GetDate(objFundsTransSplit.ToDate);
                }
                //Open up invoice record
               // akaushik5 Commented For MITS 35846 Starts
                //objBrsInvoice = (BrsInvoice)this.m_objDataModelFactory.GetDataModelObject("BrsInvoice", false);
                //try
                //{
                //    objBrsInvoice.MoveToTransId(p_iTransId);
                //}
                //catch
                //{
                //    if (!p_bIsSilent)
                //        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems"));
                //    return;
                //}

                //m_objLocalCache = new LocalCache(m_ConnectionString);

                //iInvoiceId = objBrsInvoice.InvoiceId;
                //if (iInvoiceId == 0)
                //    return;
               // akaushik5 Commented For MITS 35846 Ends

                //Make sure we have detail items (exit if none)
                sSQL = "SELECT COUNT(INVOICE_ID) FROM INVOICE_DETAIL WHERE INVOICE_ID = " + iInvoiceId;//Deb UnderWriters
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        if (Conversion.ConvertObjToInt(objReader[0], m_iClientId) == 0)
                        {
                            if (!p_bIsSilent)
                                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems", m_iClientId));//sharishkumar Jira 835
                            return;
                        }
                    }
                    objReader.Close();
                }

                //... Payee Info
                sName = ((string)(objBrsInvoice.FirstNameText + " " + objBrsInvoice.LastNameText)).Trim();
                structCEOBRecord.PayeeFirstName = objBrsInvoice.FirstNameText;
                structCEOBRecord.PayeeLastName = objBrsInvoice.LastNameText;
                structCEOBRecord.PayeeFullName = sName;
                structCEOBRecord.PayeeAddr1 = objBrsInvoice.Addr1Text;
                structCEOBRecord.PayeeAddr2 = objBrsInvoice.Addr2Text;
                structCEOBRecord.PayeeAddr3 = objBrsInvoice.Addr3Text;
                structCEOBRecord.PayeeAddr4 = objBrsInvoice.Addr4Text;
                // akaushik5 Changed for MITS 35846 Starts
                //objLocalCache.GetStateInfo(objBrsInvoice.StateId, ref strStateCode, ref strStateDesc);
                m_objLocalCache.GetStateInfo(objBrsInvoice.StateId, ref strStateCode, ref strStateDesc);
                // akaushik5 Changed for MITS 35846 Ends
                //Deb : MITS 32303
                //structCEOBRecord.PayeeState = ((string)(strStateCode + " " + strStateDesc)).Trim();
                structCEOBRecord.PayeeState = ((string)(strStateCode + " ")).Trim();
                //Deb : MITS 32303
                structCEOBRecord.PayeePostalCode = objBrsInvoice.PostalCodeText;
                structCEOBRecord.PayeeCity = objBrsInvoice.CityText;
                structCEOBRecord.ClaimNumber = objBrsInvoice.ClaimNumberText;
                structCEOBRecord.OrigInvDate = Conversion.GetDate(objBrsInvoice.DttmRcdAdded);
                structCEOBRecord.OrigInvUser = objBrsInvoice.AddedByUser;
                iClaimantEid = objBrsInvoice.ClaimantEid;
                //Changed by Gagan for MITS 11520 : Start
                if (objBrsInvoice.TransDateText == "" || objBrsInvoice.TransDateText == null)
                    sTransDateText = objFunds.TransDate;
                else
                    sTransDateText = objBrsInvoice.TransDateText;
                structCEOBRecord.TransDate = sTransDateText;
                //sTransDateText = objBrsInvoice.TransDateText; 
                //Changed by Gagan for MITS 11520 : End
                sClaimNumberText = objBrsInvoice.ClaimNumberText;

                //... Payee Tax Id
                // akaushik5 Changed for MITS 35846 Starts
                //objEntity = (Entity)objDataModelFactory.GetDataModelObject("Entity", false);
                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                // akaushik5 Changed for MITS 35846 Ends
                try
                {
                    objEntity.MoveTo(objFunds.PayeeEid);
                    structCEOBRecord.PayeeTaxID = objEntity.TaxId;
                    structCEOBRecord.VendorNumber = objEntity.TaxId;
                    //Changed by Gagan for MITS 11520 : Start
                    structCEOBRecord.PayeeNPI = objEntity.NPINumber;
                    //Changed by Gagan for MITS 11520 : End
                }
                catch { }

                try
                {
                    objEntity.MoveTo(iClaimantEid);

                    sFirstName = objEntity.FirstName;
                    sLastName = objEntity.LastName;
                    sClaimantName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
                    sRegClaimantName = ((string)(sFirstName + " " + sLastName)).Trim();
                    sClaimantAddr1 = objEntity.Addr1;
                    sClaimantAddr2 = objEntity.Addr2;
                    sClaimantAddr3 = objEntity.Addr3;
                    sClaimantAddr4 = objEntity.Addr4;
                    // akaushik5 Changed for MITS 35846 Starts
                    //objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                    this.m_objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                    // akaushik5 Changed for MITS 35846 Ends
                    if (strStateCode != string.Empty || strStateDesc != string.Empty)
                        structCEOBRecord.ClaimantState = ((string)(strStateCode + " " + strStateDesc)).Trim().Substring(0, 4);
                    structCEOBRecord.ClaimantCity = objEntity.City;
                    structCEOBRecord.ClaimantPostalCode = objEntity.ZipCode;
                    sClaimantCity = structCEOBRecord.ClaimantCity + ", " + structCEOBRecord.ClaimantState
                        + " " + structCEOBRecord.ClaimantPostalCode;
                    sClaimantTaxId = objEntity.TaxId;

                    structCEOBRecord.ClaimantFullName = sClaimantName;
                    structCEOBRecord.ClaimantLastName = sLastName;
                    structCEOBRecord.ClaimantFirstName = sFirstName;
                    structCEOBRecord.ClaimantAddr1 = sClaimantAddr1;
                    structCEOBRecord.ClaimantAddr2 = sClaimantAddr2;
                    structCEOBRecord.ClaimantAddr3 = sClaimantAddr3;
                    structCEOBRecord.ClaimantAddr4 = sClaimantAddr4;
                    structCEOBRecord.ClaimantSSN = sClaimantTaxId;
                    if (sClaimantAddr1 != string.Empty)
                        sTempData = sClaimantAddr1;
                    if (sClaimantAddr2 != string.Empty)
                        sTempData += Functions.CRLF + sClaimantAddr2;
                    if (sClaimantAddr3 != string.Empty)
                        sTempData += Functions.CRLF + sClaimantAddr3;
                    if (sClaimantAddr4 != string.Empty)
                        sTempData += Functions.CRLF + sClaimantAddr4;
                    if (sClaimantCity != string.Empty)
                        sTempData += Functions.CRLF + sClaimantCity;
                    structCEOBRecord.ClaimantFullAddress = sTempData;
                }
                catch { }

                int iReportedEID = 0;

                //... Event Date Info
                sSQL = "SELECT DATE_OF_EVENT, DATE_REPORTED, RPTD_BY_EID FROM EVENT,CLAIM WHERE " +
                    "CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + objFunds.ClaimId;
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        structCEOBRecord.EventDate = objReader["DATE_OF_EVENT"].ToString();
                        structCEOBRecord.DateReported = objReader["DATE_REPORTED"].ToString();
                        iReportedEID = Conversion.ConvertStrToInteger(objReader["RPTD_BY_EID"].ToString());
                    }
                    objReader.Close();
                }

                //... Reported By Name/Address
                try
                {
                    objEntity.MoveTo(iReportedEID);
                    sFirstName = objEntity.FirstName;
                    sLastName = objEntity.LastName;
                    structCEOBRecord.RptdFullName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
                }
                catch { }

                //... Employee department chain info
                sSQL = "SELECT ORG_HIERARCHY.* FROM EMPLOYEE,ORG_HIERARCHY WHERE EMPLOYEE.EMPLOYEE_EID=" +
                    iClaimantEid + " AND ORG_HIERARCHY.DEPARTMENT_EID = EMPLOYEE.DEPT_ASSIGNED_EID";
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        // akaushik5 Changed for MITS 35846 Starts
                        //objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["DEPARTMENT_EID"].ToString()), ref structCEOBRecord.EmpDeptCode, ref structCEOBRecord.EmpDepartment);
                        //objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["FACILITY_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpFacility);
                        //objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["LOCATION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpLocation);
                        //objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["DIVISION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpDivision);
                        //objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["REGION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpRegion);
                        //objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["COMPANY_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpCompany);
                        //objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["CLIENT_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpClient);
                        m_objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["DEPARTMENT_EID"].ToString()), ref structCEOBRecord.EmpDeptCode, ref structCEOBRecord.EmpDepartment);
                        m_objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["FACILITY_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpFacility);
                        m_objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["LOCATION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpLocation);
                        m_objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["DIVISION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpDivision);
                        m_objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["REGION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpRegion);
                        m_objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["COMPANY_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpCompany);
                        m_objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["CLIENT_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpClient);
                        // akaushik5 Changed for MITS 35846 Ends
                        //Get the address of the facility
                        iDivisionId = Conversion.ConvertStrToInteger(objReader["DIVISION_EID"].ToString());

                        try
                        {
                            objEntity.MoveTo(iDivisionId);
                            structCEOBRecord.DivisionName = objEntity.FirstName + objEntity.LastName;
                            structCEOBRecord.DivisionTaxID = objEntity.TaxId;
                            // akaushik5 Changed for MITS 35846 Starts
                            //structCEOBRecord.EmpDivisionNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                            structCEOBRecord.EmpDivisionNAICS = m_objLocalCache.GetShortCode(objEntity.NaicsCode);
                            // akaushik5 Changed for MITS 35846 Ends

                            structCEOBRecord.DivisionNameAddr += structCEOBRecord.DivisionName + Functions.CRLF + objEntity.Addr1;
                            
                            if (objEntity.Addr2 != string.Empty)
                                structCEOBRecord.DivisionNameAddr += Functions.CRLF + objEntity.Addr2;
                            if (objEntity.Addr3 != string.Empty)
                                structCEOBRecord.DivisionNameAddr += Functions.CRLF + objEntity.Addr3;
                            if (objEntity.Addr4 != string.Empty)
                                structCEOBRecord.DivisionNameAddr += Functions.CRLF + objEntity.Addr4;

                            if (objEntity.City != string.Empty)
                            {
                                // akaushik5 Changed for MITS 35846 Starts
                                //objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                                this.m_objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                                // akaushik5 Changed for MITS 35846 Ends
                                structCEOBRecord.DivisionNameAddr += Functions.CRLF + objEntity.City + ", " +
                                    //Deb: MITS 32303
                                    //strStateCode + " " + strStateDesc + " " + objEntity.ZipCode;
                                    strStateDesc + " " + objEntity.ZipCode;
                                    //Deb: MITS 32303
                            }
                        }
                        catch (RecordNotFoundException p_objRecordNotFoundException)
                        {
                            p_objRecordNotFoundException = null;
                        }

                        //Get the address of the facility
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["FACILITY_EID"].ToString()));
                            // akaushik5 Changed for MITS 35846 Starts
                            //structCEOBRecord.EmpFacilityNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                            structCEOBRecord.EmpFacilityNAICS = this.m_objLocalCache.GetShortCode(objEntity.NaicsCode);
                            // akaushik5 Changed for MITS 35846 Ends
                            if (objEntity.Addr1 != string.Empty)
                                structCEOBRecord.EmpFacilityAddress = objEntity.Addr1;
                            if (objEntity.Addr2 != string.Empty)
                                structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.Addr2;
                            if (objEntity.Addr3 != string.Empty)
                                structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.Addr3;
                            if (objEntity.Addr4 != string.Empty)
                                structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.Addr4;
                            if (objEntity.City != string.Empty || objEntity.StateId != 0 || objEntity.ZipCode != string.Empty)
                            {
                                // akaushik5 Changed for MITS 35846 Starts
                                //objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                                this.m_objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                                // akaushik5 Changed for MITS 35846 Ends
                                structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.City + ", " +
                                    strStateCode + " " + strStateDesc + " " + objEntity.ZipCode;
                            }
                        }
                        catch (RecordNotFoundException p_objRecordNotFoundException)
                        {
                            p_objRecordNotFoundException = null;
                        }

                        //... Get NAICS codes for all of the hierarchy levels
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["DEPARTMENT_EID"].ToString()));
                            // akaushik5 Changed for MITS 35846 Starts
                            //structCEOBRecord.EmpDepartmentNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                            structCEOBRecord.EmpDepartmentNAICS = this.m_objLocalCache.GetShortCode(objEntity.NaicsCode);
                            // akaushik5 Changed for MITS 35846 Ends
                        }
                        catch { }
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["LOCATION_EID"].ToString()));
                            // akaushik5 Changed for MITS 35846 Starts
                            //structCEOBRecord.EmpLocationNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                            structCEOBRecord.EmpLocationNAICS = m_objLocalCache.GetShortCode(objEntity.NaicsCode);
                            // akaushik5 Changed for MITS 35846 Ends
                        }
                        catch { }
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["REGION_EID"].ToString()));
                            // akaushik5 Changed for MITS 35846 Starts
                            //structCEOBRecord.EmpRegionNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                            structCEOBRecord.EmpRegionNAICS = m_objLocalCache.GetShortCode(objEntity.NaicsCode);
                            // akaushik5 Changed for MITS 35846 Ends
                        }
                        catch { }
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["OPERATION_EID"].ToString()));
                            // akaushik5 Changed for MITS 35846 Starts
                            //structCEOBRecord.EmpOperationNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                            structCEOBRecord.EmpOperationNAICS = m_objLocalCache.GetShortCode(objEntity.NaicsCode);
                            // akaushik5 Changed for MITS 35846 Ends
                        }
                        catch { }
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["COMPANY_EID"].ToString()));
                            // akaushik5 Changed for MITS 35846 Starts
                            //structCEOBRecord.EmpCompanyNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                            structCEOBRecord.EmpCompanyNAICS = m_objLocalCache.GetShortCode(objEntity.NaicsCode);
                            // akaushik5 Changed for MITS 35846 Ends
                        }
                        catch { }
                        try
                        {
                            objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["CLIENT_EID"].ToString()));
                            // akaushik5 Changed for MITS 35846 Starts
                            //structCEOBRecord.EmpCompanyNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
                            structCEOBRecord.EmpCompanyNAICS = m_objLocalCache.GetShortCode(objEntity.NaicsCode);
                            // akaushik5 Changed for MITS 35846 Ends
                        }
                        catch { }

                    }
                    objReader.Close();
                }

                sSQL = "SELECT LAST_NAME,FIRST_NAME,ABBREVIATION,CURRENT_ADJ_FLAG,PHONE1 FROM ENTITY," +
                    "CLAIM_ADJUSTER WHERE ADJUSTER_EID=ENTITY_ID AND CLAIM_ID=" + objFunds.ClaimId;
                objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        structCEOBRecord.AdjusterAbbrev = objReader["ABBREVIATION"].ToString();
                        structCEOBRecord.AdjusterFullName = ((string)(objReader["FIRST_NAME"].ToString() + " " + objReader["LAST_NAME"].ToString())).Trim();
                        structCEOBRecord.AdjusterLastName = objReader["LAST_NAME"].ToString();
                        //Arnab: MITS-10722 - Getting Adjuster phone no
                        structCEOBRecord.AdjusterPhone1 = Conversion.ConvertObjToStr(objReader["PHONE1"]);
                        //MITS-10722 End
                        if (Conversion.ConvertObjToInt(objReader["CURRENT_ADJ_FLAG"], m_iClientId) != 0)
                            break;
                    }
                    objReader.Close();
                }

                structCEOBRecord.BatchNumber = objFunds.BatchNumber.ToString();
                structCEOBRecord.CheckNumber = string.Empty;
                structCEOBRecord.PatientAccountNumber = string.Empty;
                structCEOBRecord.FeeOrCustomary = string.Empty;
                structCEOBRecord.ControlNumber = objFunds.CtlNumber;
                structCEOBRecord.ReceiptDate = string.Empty;
                //Ashish Ahuja Mits 33681 : Start
                structCEOBRecord.CheckNumber = objFunds.TransNumber.ToString();
                //Ashish Ahuja Mits 33681 : End
                if (p_bIsPostCheck)
                    structCEOBRecord.CheckNumber = objFunds.TransNumber.ToString();
                else
                {
                    if (p_arrChkPrtList != null)
                    {
                        foreach (PrintedCheckDetail structPrintedCheckDetail in p_arrChkPrtList)
                        {
                            if (structPrintedCheckDetail.TransId == p_iTransId)
                                structCEOBRecord.CheckNumber = structPrintedCheckDetail.CheckNum.ToString();
                        }
                    }
                }

                m_structEOBRecord.CheckNumber = structCEOBRecord.CheckNumber;
                //Fetching EOB details
                GetEOBDetail(objFunds, ref structCEOBRecord, iInvoiceId);


                //Changed by Gagan for MITS 11520 : Start
                //SSN can have format as ###-##-#### or ##-#######
                if (structCEOBRecord.ClaimantSSN != "")
                {
                    if (structCEOBRecord.ClaimantSSN.Length == 11)
                        structCEOBRecord.ClaimantSSN = "XXX-XX-" + structCEOBRecord.ClaimantSSN.Substring(7, 4);
                    else if (structCEOBRecord.ClaimantSSN.Length == 10)
                        structCEOBRecord.ClaimantSSN = "XX-XXX" + structCEOBRecord.ClaimantSSN.Substring(6, 4);
                }
                //Changed by Gagan for MITS 11520 : End

                // akaushik5 Added for MITS 35846 Starts
                if (string.IsNullOrEmpty(p_sState.Trim()))
                {
                    sSQL = string.Format("SELECT STATES.STATE_ID FROM CLAIM JOIN STATES ON FILING_STATE_ID = STATES.STATE_ROW_ID WHERE CLAIM_ID = {0}", objFunds.ClaimId);
                    using (objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL))
                    {
                        if (!object.ReferenceEquals(objReader, null) && objReader.Read())
                        {
                            p_sState = objReader["STATE_ID"].ToString();
                        }
                    }
                }
                // akaushik5 Added for MITS 35846 Ends

                switch (p_sState)
                {
                    case "MI":
                    case MI:
                        objrptEOB = new rptEOB(m_ConnectionString, objCommon.m_stLoginInfo.Username,m_iClientId);
                        objrptEOB.EOBReportData = structCEOBRecord;

                        objrptEOB.Run();
                        p_objEOBReport = objrptEOB;
                        break;

                    case "TX":
                    case TX:    
                        objrptEOB_TX = new rptEOB_TX(m_ConnectionString, objCommon.m_stLoginInfo.Username,m_iClientId);
                        objrptEOB_TX.EOBReportData = structCEOBRecord;
                        objrptEOB_TX.Run();
                        p_objEOBReport = objrptEOB_TX;
                        break;

                    case "EOR":
                        objrptEOR = new rptEOR(m_ConnectionString, objCommon.m_stLoginInfo.Username,m_iClientId);
                        objrptEOR.EOBReportData = structCEOBRecord;
                        objrptEOR.Run();
                        p_objEOBReport = objrptEOR;
                        break;

                    case "EOP":
                        objrptEOP = new rptEOP(m_ConnectionString, objCommon.m_stLoginInfo.Username,m_iClientId);
                        objrptEOP.EOBReportData = structCEOBRecord;
                        objrptEOP.Run();
                        p_objEOBReport = objrptEOP;
                        break;

                    default:
                        p_sPDFSaveFilePath = "";
                        GetPrintCheckHTMLEOB(objFunds, true, p_sState, out p_sPDFSaveFilePath);
                        break;
                }

                //Stamp audit info on invoice record
                sSQL = "UPDATE INVOICE SET DTTM_EOB_PRINTED = '" + Conversion.ToDbDateTime(DateTime.Now) + "', " +
                    "EOB_PRINTED_USER = '" + objCommon.m_stLoginInfo.Username + "' WHERE INVOICE_ID = " + iInvoiceId;
                // akaushik5 Changed for MITS 35846 Starts
                //objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);
                this.m_objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);
                // akaushik5 Changed for MITS 35846 Ends

                ////TODO Logging component is in process to be finalized, As that will be finalized this will be done
                //MsgBox "Explanation Of Benefits (EOB) has been printed.", vbInformation + vbOKOnly, "Bill Review System"

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAR.PrintEOBReport.ErrorCreate", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                // akaushik5 Commented for MITS 35846 Starts
                //if (objFunds != null)
                //    objFunds.Dispose();
                // akaushik5 Commented for MITS 35846 Ends

                if (objBrsInvoice != null)
                    objBrsInvoice.Dispose();

                if (objEntity != null)
                    objEntity.Dispose();

                if (objSupplementals != null)
                    objSupplementals.Dispose();

                // akaushik5 Commented for MITS 35846 Starts
                //if (m_objLocalCache != null)
                //    m_objLocalCache.Dispose();
                // akaushik5 Commented for MITS 35846 Ends

                if (objPDF_AR != null)
                    objPDF_AR.Dispose();

                if (objrptEOB != null)
                    objrptEOB.Dispose();

                if (objrptEOB_TX != null)
                    objrptEOB_TX.Dispose();

                if (objrptEOP != null)
                    objrptEOP.Dispose();

                if (objrptEOR != null)
                    objrptEOR.Dispose();

                // akaushik5 Commented for MITS 35846 Starts
                //if (objDataModelFactory != null)
                //{
                //    objDataModelFactory.Dispose();
                //    objDataModelFactory = null;
                //}
                // akaushik5 Commented for MITS 35846 Ends
            }
        }
        #endregion

        #region "GetPrintCheckHTMLEOB(Funds p_objFunds, bool p_bIsSilent, out PrintWrapper p_objPrintWrapper)"
        /// Name		: GetPrintCheckHTMLEOB
        /// Author		: Debabrata Biswas
        /// Date Created	: 06 Oct 2009		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// THis function is called to print EOB report, It uses XHTML format file for reading report format details
        /// Format File is the XHTML Custom files tags <b> (like  xml/http).
        /// If a file cannot be found, a Default, internal layout format is used.
        /// </summary>
        /// <param name="p_objFunds">Funds Object</param>
        /// <param name="p_bIsSilent">Flag for enebling/disabling No Data found messages</param>
        /// <param name="p_objPrintWrapper">Output parameter as object of Prinwrapper class. This will contain the PDF doc generated</param>
        private void GetPrintCheckHTMLEOB(Funds p_objFunds, bool p_bIsSilent, string p_sState, out string p_sPDFSaveFilePath)
        {
            {
                string sSQL = string.Empty;
                int iTransId = 0;
                int iDeptAssignEId = 0;
                string sTempData = string.Empty;
                int iInvoiceId = 0;
                string sName = string.Empty;
                DbReader objReader = null;
                BrsInvoice objBrsInvoice = null;
                Claim objClaim = null;
                State objState = null;
                DataSet objDataSet = null;
                Entity objEntity = null;
                Employee objEmployee = null;
                StreamReader objFormatFileStream = null;
                LocalCache objLocalCache = null;
                string strStateDesc = string.Empty;
                string strStateCode = string.Empty;
                string sJurisdiction = string.Empty;
                int iStateId = 0;
                string sClaimantFileName = string.Empty;
                string sPayeeFileName = string.Empty;
                bool bIsFormatFileFound = false;
                int iClaimantEid = 0;
                string sTransDateText = string.Empty;
                string sClaimNumberText = string.Empty;
                string sFirstName = string.Empty;
                string sLastName = string.Empty;
                string sClaimantName = string.Empty;
                string sRegClaimantName = string.Empty;
                string sClaimantAddr1 = string.Empty;
                string sClaimantAddr2 = string.Empty;
                string sClaimantAddr3 = string.Empty;
                string sClaimantAddr4 = string.Empty;
                string sClaimantCity = string.Empty;
                string sClaimantTaxId = string.Empty;
                string sAddr1 = string.Empty;
                string sAddr2 = string.Empty;
                string sAddr3 = string.Empty;
                string sAddr4 = string.Empty;
                string sCity = string.Empty;
                string sDestFolderPath = "";
                //skhare7
                int iDivisionId=0;
                int iFacilityId=0;
                int iLocationId=0;
                int iRegionId=0;
                int iOerationId=0;
                int iCompId=0;
                int iClientId=0;
                string sLOBCode=string.Empty;
                int iDeptTableId = 0;
                RMConfigurator objConfig = null;
                StringBuilder sPayeeHTMLtext;
                StringBuilder sClaimantHTMLtext;
                C1PdfDocument objPdfDoc;
                p_sPDFSaveFilePath = string.Empty;
                DataModelFactory objDataModelFactory = null;
                try
                {
                    objConfig = new RMConfigurator();
                    objPdfDoc = new C1PdfDocument();
                    objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId);//sharishkumar Jira 835
                    objPdfDoc.Clear();
                    objPdfDoc.CurrentPage = 0;

                    System.Drawing.Font fFont = new System.Drawing.Font("Courier New", 10);
                    System.Drawing.Brush brBrush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(0, 0, 0));
                    System.Drawing.RectangleF rcPage = objPdfDoc.PageRectangle;
                    rcPage.Inflate(-25, -25);

                    m_objFunds = p_objFunds;
                    iTransId = m_objFunds.TransId;
                    if (iTransId == 0)
                    {
                        if (!p_bIsSilent)
                            throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.SaveItem", m_iClientId));//sharishkumar Jira 835
                        return;
                    }

                    objBrsInvoice = (BrsInvoice)objDataModelFactory.GetDataModelObject("BrsInvoice", false);
                    try
                    {
                        objBrsInvoice.MoveToTransId(iTransId);
                    }
                    catch
                    {
                        if (!p_bIsSilent)
                            throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems",m_iClientId));//sharishkumar Jira 835
                        return;
                    }

                    iInvoiceId = objBrsInvoice.InvoiceId;
                    sName = objBrsInvoice.FirstNameText + " " + objBrsInvoice.LastNameText;

                    objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                    if (m_objFunds.ClaimId > 0)
                        objClaim.MoveTo(m_objFunds.ClaimId);
                    //  skhare7 MITS 23373
                        sLOBCode = objClaim.LineOfBusCode.ToString() ;
                        //  skhare7 MITS 23373 End
                    iStateId = objClaim.FilingStateId;
                    objClaim.Dispose();

                    objState = (State)objDataModelFactory.GetDataModelObject("State", false);
                    if (iStateId > 0)
                        objState.MoveTo(iStateId);
                    sJurisdiction = objState.StateId;
                    objState.Dispose();
                    
                    sSQL = "SELECT FL_LICENSE,ZIP_CODE FROM INVOICE_DETAIL WHERE INVOICE_ID=" + iInvoiceId;
                    objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            m_structEOBRecord.FirstLicense = objReader["FL_LICENSE"].ToString();
                            m_structEOBRecord.FirstBillingPostalCode = objReader["ZIP_CODE"].ToString();
                        }

                    }
                    if (objReader != null)
                        objReader.Close();

                    sSQL = "SELECT INVOICE_NUMBER, INVOICE_DATE FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID=" + iTransId;
                    objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            m_structEOBRecord.FirstInvoice = objReader["INVOICE_NUMBER"].ToString();
                            //Ashish Ahuja - Mits 33979
                            m_structEOBRecord.FirstInvoiceDate = Conversion.GetDBDateFormat(objReader["INVOICE_DATE"].ToString(), "MM/dd/yyyy");
                        }

                    }
                    if (objReader != null)
                        objReader.Close();
                    sSQL = null;

                    string sFromDateFirst = string.Empty;
                    string sFromDateLast = string.Empty;

                    sSQL = "SELECT FROM_DATE FROM FUNDS_TRANS_SPLIT WHERE SPLIT_ROW_ID=(SELECT MIN(SPLIT_ROW_ID) FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID=" + iTransId + ")";

                    objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            sFromDateFirst = objReader["FROM_DATE"].ToString();
                            sFromDateFirst = Conversion.ToDate(sFromDateFirst).ToShortDateString();
                        }

                    }
                    if (objReader != null)
                        objReader.Close();
                    sSQL = null;

                    sSQL = "SELECT FROM_DATE FROM FUNDS_TRANS_SPLIT WHERE SPLIT_ROW_ID=(SELECT MAX(SPLIT_ROW_ID) FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID=" + iTransId + ")";

                    objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            sFromDateLast = objReader["FROM_DATE"].ToString();
                            sFromDateLast = Conversion.ToDate(sFromDateLast).ToShortDateString();
                        }

                    }
                    if (objReader != null)
                        objReader.Close();
                    sSQL = null;

                    m_structEOBRecord.DateOfService = sFromDateFirst + " to " + sFromDateLast;

                    if (m_bPrintClaimantEOB)
                    {
                        sClaimantFileName = Functions.GetFormatFilePath(m_iClientId)
                         + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMHTMLFILE, m_ConnectionString, m_iClientId);//rkaur27
                        if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true" && m_iClientId == 0)
                        {
                            if ((sJurisdiction == string.Empty) || !File.Exists(sClaimantFileName))
                                sClaimantFileName = Functions.GetFormatFilePath(m_iClientId)
                                    + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMHTMLFILE, m_ConnectionString, m_iClientId);//rkaur27
                        }
                    }

                    if (m_bPrintPayeeEOB)
                    {
                        sPayeeFileName = Functions.GetFormatFilePath(m_iClientId)
                         + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEHTMLFILE, m_ConnectionString, m_iClientId);//rkaur27
                        if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true" && m_iClientId == 0)
                        {
                            if ((sJurisdiction == string.Empty) || !File.Exists(sPayeeFileName))
                                sPayeeFileName = Functions.GetFormatFilePath(m_iClientId)
                                     + Functions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEHTMLFILE, m_ConnectionString, m_iClientId);//rkaur27
                        }
                    }
                    bIsFormatFileFound = true;
                    if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true")
                    {
                        if (!File.Exists(sClaimantFileName) && m_bPrintClaimantEOB)
                            bIsFormatFileFound = false;

                        if (bIsFormatFileFound)
                            if (!File.Exists(sPayeeFileName) && m_bPrintPayeeEOB)
                                bIsFormatFileFound = false;
                    }
                    else
                    {
                        MemoryStream objMemDesc = CommonFunctions.RetreiveTempFileFromDB(sClaimantFileName, "", m_ConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                        if (objMemDesc == null && m_bPrintClaimantEOB)
                        {
                            bIsFormatFileFound = false;
                        }
                        else
                        {
                            bIsFormatFileFound = true;
                        }
                        if (bIsFormatFileFound && m_bPrintPayeeEOB)
                        {
                            objMemDesc = CommonFunctions.RetreiveTempFileFromDB(sPayeeFileName, "", m_ConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                            if (objMemDesc == null)
                            {
                                bIsFormatFileFound = false;
                            }
                        }
                    }

                    objLocalCache = new LocalCache(m_ConnectionString, m_iClientId);

                    if (bIsFormatFileFound)
                    {
                        m_structEOBRecord.PayeeFirstName = objBrsInvoice.FirstNameText;
                        m_structEOBRecord.PayeeLastName = objBrsInvoice.LastNameText;
                        //Ashish Ahuja - Mits 33979 START
                        //m_structEOBRecord.PayeeFullName = sName.Trim();
                        string tempFullName = sName.Trim() ;
                        string sFullName = "";
                        int startindex = 0;
                        int lastindex = 20;
                        int len = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(tempFullName.Length) / Convert.ToDouble(lastindex)));
                        for (int i = 0; i < len; i++)
                        {
                            if (tempFullName != "")
                            {
                                if (i != len - 1)
                                {
                                    sFullName += tempFullName.Substring(startindex, lastindex) + "<br/>";
                                    tempFullName = tempFullName.Remove(startindex, lastindex);
                                }
                                else
                                {
                                    sFullName += tempFullName.Substring(startindex, tempFullName.Length);
                                }
                            }
                        }
                        m_structEOBRecord.PayeeFullName = sFullName;
                        //Ashish Ahuja - Mits 33979 END

                        //Ashish Ahuja - Mits 33979 start
                        //m_structEOBRecord.PayeeAddr1 = objBrsInvoice.Addr1Text;
                        string tempAddr1 = objBrsInvoice.Addr1Text;
                        string tempAddress = "";
                        startindex = 0;
                        lastindex = 20;
                        len = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(tempAddr1.Length) / Convert.ToDouble(lastindex)));
                        for (int i = 0; i < len; i++)
                        {
                            if (tempAddr1 != "")
                            {
                                if (i != len - 1)
                                {
                                    tempAddress += tempAddr1.Substring(startindex, lastindex) + "<br/>";
                                    tempAddr1 = tempAddr1.Remove(startindex, lastindex);
                                }
                                else
                                {
                                    tempAddress += tempAddr1.Substring(startindex, tempAddr1.Length);
                                }
                            }
                        }
                        m_structEOBRecord.PayeeAddr1 = tempAddress;
                        //Ashish Ahuja - Mits 33979 end

                        //m_structEOBRecord.PayeeAddr2 = objBrsInvoice.Addr2Text;
                        //m_structEOBRecord.PayeeAddr3 = objBrsInvoice.Addr3Text;
                        //m_structEOBRecord.PayeeAddr4 = objBrsInvoice.Addr4Text;
                        string tempAddr2 = objBrsInvoice.Addr2Text;
                        tempAddress = "";
                        len = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(tempAddr2.Length) / Convert.ToDouble(lastindex)));
                        for (int i = 0; i < len; i++)
                        {
                            if (tempAddr2 != "")
                            {
                                if (i != len - 1)
                                {
                                    tempAddress += tempAddr2.Substring(startindex, lastindex) + "<br/>";
                                    tempAddr2 = tempAddr2.Remove(startindex, lastindex);
                                }
                                else
                                {
                                    tempAddress += tempAddr2.Substring(startindex, tempAddr2.Length);
                                }
                            }
                        }
                        m_structEOBRecord.PayeeAddr2 = tempAddress;
                        
                        string tempAddr3 = objBrsInvoice.Addr3Text;
                        tempAddress = "";
                        len = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(tempAddr3.Length) / Convert.ToDouble(lastindex)));
                        for (int i = 0; i < len; i++)
                        {
                            if (tempAddr3 != "")
                            {
                                if (i != len - 1)
                                {
                                    tempAddress += tempAddr3.Substring(startindex, lastindex) + "<br/>";
                                    tempAddr3 = tempAddr3.Remove(startindex, lastindex);
                                }
                                else
                                {
                                    tempAddress += tempAddr3.Substring(startindex, tempAddr3.Length);
                                }
                            }
                        }
                        m_structEOBRecord.PayeeAddr3 = tempAddress;
                        string tempAddr4 = objBrsInvoice.Addr4Text;
                        tempAddress = "";
                        len = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(tempAddr4.Length) / Convert.ToDouble(lastindex)));
                        for (int i = 0; i < len; i++)
                        {
                            if (tempAddr4 != "")
                            {
                                if (i != len - 1)
                                {
                                    tempAddress += tempAddr4.Substring(startindex, lastindex) + "<br/>";
                                    tempAddr4 = tempAddr4.Remove(startindex, lastindex);
                                }
                                else
                                {
                                    tempAddress += tempAddr4.Substring(startindex, tempAddr4.Length);
                                }
                            }
                        }
                        m_structEOBRecord.PayeeAddr4 = tempAddress;

                        objLocalCache.GetStateInfo(objBrsInvoice.StateId, ref strStateCode, ref strStateDesc);
                        //Deb : MITS 32303
                        //m_structEOBRecord.PayeeState = ((string)(strStateCode + " " + strStateDesc)).Trim();
                        m_structEOBRecord.PayeeState = ((string)(strStateCode + " ")).Trim();
                        //Deb : MITS 32303
                        m_structEOBRecord.PayeePostalCode = objBrsInvoice.PostalCodeText;
                        //Ashish Ahuja - Mits 33979 start
                        //m_structEOBRecord.PayeeCity = objBrsInvoice.CityText;
                        string tempCityText = objBrsInvoice.CityText;
                        string tempCity = "";
                        startindex = 0;
                        lastindex = 20;
                        len = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(tempCityText.Length) / Convert.ToDouble(lastindex)));
                        for (int i = 0; i < len; i++)
                        {
                            if (tempCityText != "")
                            {
                                if (i != len - 1)
                                {
                                    tempCity += tempCityText.Substring(startindex, lastindex) + "<br/>";
                                    tempCityText = tempCityText.Remove(startindex, lastindex);
                                }
                                else
                                {
                                    tempCity += tempCityText.Substring(startindex, tempCityText.Length);
                                    if (tempCity.Length >= 12)
                                    {
                                        tempCity = tempCity + "<br/>";
                                    }
                                }
                            }
                        }
                        m_structEOBRecord.PayeeCity = tempCity;
                        //Ashish Ahuja - Mits 33979 start

                        m_structEOBRecord.ClaimNumber = objBrsInvoice.ClaimNumberText;
                        m_structEOBRecord.OrigInvDate = Conversion.GetDate(objBrsInvoice.DttmRcdAdded);
                        m_structEOBRecord.OrigInvUser = objBrsInvoice.AddedByUser;
                    }
                    iClaimantEid = objBrsInvoice.ClaimantEid;
                    sTransDateText = objBrsInvoice.TransDateText;
                    sClaimNumberText = objBrsInvoice.ClaimNumberText;
                    sAddr1 = objBrsInvoice.Addr1Text;
                    sAddr2 = objBrsInvoice.Addr2Text;
                    sAddr3 = objBrsInvoice.Addr3Text;
                    sAddr4 = objBrsInvoice.Addr4Text;
                    sCity = objBrsInvoice.CityText;

                    objBrsInvoice.Dispose();

                    objEntity = (Entity)objDataModelFactory.GetDataModelObject("Entity", false);
                    if (bIsFormatFileFound)
                    {
                        try
                        {
                            objEntity.MoveTo(m_objFunds.PayeeEid);
                            m_structEOBRecord.PayeeTaxID = objEntity.TaxId;
                        }
                        catch
                        {
                            m_structEOBRecord.PayeeTaxID = string.Empty;
                        }

                        objEmployee = (Employee)objDataModelFactory.GetDataModelObject("Employee", false);
                        try
                        {
                            objEmployee.MoveTo(iClaimantEid);
                            iDeptAssignEId = objEmployee.DeptAssignedEid;
                            objEntity.MoveTo(iDeptAssignEId);
                              //  skhare7 MITS 23373 
                            iDeptTableId = objEntity.EntityTableId;
                            //  skhare7 MITS 23373 End
                            m_structEOBRecord.EmpDeptCode = objEntity.Abbreviation;
                            m_structEOBRecord.EmpDepartment = objEntity.LastName;
                        }
                        catch
                        {
                            m_structEOBRecord.EmpDeptCode = string.Empty;
                            m_structEOBRecord.EmpDepartment = string.Empty;
                        }
                        objEmployee.Dispose();
                        objEntity.Dispose();

                        sSQL = "SELECT DATE_OF_EVENT, DATE_REPORTED, RPTD_BY_EID FROM EVENT,CLAIM WHERE " +
                            "CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + m_objFunds.ClaimId;
                        objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                m_structEOBRecord.EventDate = objReader["DATE_OF_EVENT"].ToString();
                                m_structEOBRecord.DateReported = objReader["DATE_REPORTED"].ToString();
                            }
                            objReader.Close();
                        }
                      // skhare7 MITS 23373 
//Deb UnderWriters
                        //sSQL = "SELECT ENTITY_ID,LAST_NAME,FACILITY_EID,LOCATION_EID,DIVISION_EID,REGION_EID,OPERATION_EID" +
                        //    ",COMPANY_EID,CLIENT_EID FROM ENTITY, ORG_HIERARCHY WHERE DEPARTMENT_EID=" + iDeptAssignEId;
                        //objDataSet = DbFactory.GetDataSet(m_ConnectionString, sSQL);
                        //if (objDataSet != null && objDataSet.Tables.Count > 0)
                        //{
                        //    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("FACILITY_EID=ENTITY_ID"))
                        //    {
                        //        m_structEOBRecord.EmpFacility = objDataRow["LAST_NAME"].ToString();
                        //        iFacilityId = Conversion.ConvertStrToInteger(objDataRow["FACILITY_EID"].ToString());
                        //    }

                        //    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("LOCATION_EID=ENTITY_ID"))
                        //    {
                        //        m_structEOBRecord.EmpLocation = objDataRow["LAST_NAME"].ToString();
                        //        iLocationId = Conversion.ConvertStrToInteger(objDataRow["LOCATION_EID"].ToString());
                        //    }

                        //    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("DIVISION_EID=ENTITY_ID"))
                        //    {
                        //        m_structEOBRecord.EmpDivision = objDataRow["LAST_NAME"].ToString();
                        //        iDivisionId = Conversion.ConvertStrToInteger(objDataRow["DIVISION_EID"].ToString());
                        //    }
                        //    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("REGION_EID=ENTITY_ID"))
                        //    {
                        //        m_structEOBRecord.EmpRegion = objDataRow["LAST_NAME"].ToString();
                        //        iRegionId = Conversion.ConvertStrToInteger(objDataRow["REGION_EID"].ToString());
                        //    }

                        //    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("OPERATION_EID=ENTITY_ID"))
                        //    {
                        //        m_structEOBRecord.EmpOperation = objDataRow["LAST_NAME"].ToString();
                        //        iOerationId = Conversion.ConvertStrToInteger(objDataRow["OPERATION_EID"].ToString());
                        //    }
                        //    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("COMPANY_EID=ENTITY_ID"))
                        //    {
                        //        m_structEOBRecord.EmpCompany = objDataRow["LAST_NAME"].ToString();
                        //        iCompId = Conversion.ConvertStrToInteger(objDataRow["COMPANY_EID"].ToString());
                        //    }
                        //    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("CLIENT_EID=ENTITY_ID"))
                        //    {
                        //        m_structEOBRecord.EmpClient = objDataRow["LAST_NAME"].ToString();
                        //        iClientId = Conversion.ConvertStrToInteger(objDataRow["CLIENT_EID"].ToString());
                        //    }
                        //}
                        //if (objDataSet != null)
                        //    objDataSet.Dispose();
                        //  skhare7 MITS 23373 End

                        //Deb
                        sSQL = "SELECT FACILITY_EID,LOCATION_EID,DIVISION_EID,REGION_EID,OPERATION_EID" +
                               ",COMPANY_EID,CLIENT_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=" + iDeptAssignEId;
                        objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                iFacilityId = Conversion.ConvertObjToInt(objReader["FACILITY_EID"], m_iClientId);
                                iLocationId = Conversion.ConvertObjToInt(objReader["LOCATION_EID"], m_iClientId);
                                iDivisionId = Conversion.ConvertObjToInt(objReader["DIVISION_EID"], m_iClientId);
                                iRegionId = Conversion.ConvertObjToInt(objReader["REGION_EID"], m_iClientId);
                                iOerationId = Conversion.ConvertObjToInt(objReader["OPERATION_EID"], m_iClientId);
                                iCompId = Conversion.ConvertObjToInt(objReader["COMPANY_EID"], m_iClientId);
                                iClientId = Conversion.ConvertObjToInt(objReader["CLIENT_EID"], m_iClientId);
                            }
                            objReader.Dispose();
                        }
                        m_structEOBRecord.EmpFacility = Convert.ToString(DbFactory.ExecuteScalar(m_ConnectionString, "SELECT LAST_NAME FROM ENTITY WHERE ENTITY_ID=" + iFacilityId));
                        m_structEOBRecord.EmpLocation = Convert.ToString(DbFactory.ExecuteScalar(m_ConnectionString, "SELECT LAST_NAME FROM ENTITY WHERE ENTITY_ID=" + iLocationId));
                        m_structEOBRecord.EmpDivision = Convert.ToString(DbFactory.ExecuteScalar(m_ConnectionString, "SELECT LAST_NAME FROM ENTITY WHERE ENTITY_ID=" + iDivisionId));
                        m_structEOBRecord.EmpRegion = Convert.ToString(DbFactory.ExecuteScalar(m_ConnectionString, "SELECT LAST_NAME FROM ENTITY WHERE ENTITY_ID=" + iRegionId));
                        m_structEOBRecord.EmpOperation = Convert.ToString(DbFactory.ExecuteScalar(m_ConnectionString, "SELECT LAST_NAME FROM ENTITY WHERE ENTITY_ID=" + iOerationId));
                        m_structEOBRecord.EmpCompany = Convert.ToString(DbFactory.ExecuteScalar(m_ConnectionString, "SELECT LAST_NAME FROM ENTITY WHERE ENTITY_ID=" + iCompId));
                        m_structEOBRecord.EmpClient = Convert.ToString(DbFactory.ExecuteScalar(m_ConnectionString, "SELECT LAST_NAME FROM ENTITY WHERE ENTITY_ID=" + iClientId));
                        //Deb UnderWriters
                        sSQL = "SELECT LAST_NAME,FIRST_NAME,ABBREVIATION,CURRENT_ADJ_FLAG,PHONE1 FROM ENTITY," +
                            "CLAIM_ADJUSTER WHERE ADJUSTER_EID=ENTITY_ID AND CLAIM_ID=" + m_objFunds.ClaimId;
                        objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                        if (objReader != null)
                        {
                            while (objReader.Read())
                            {
                                m_structEOBRecord.AdjusterAbbrev = objReader["ABBREVIATION"].ToString();
                                m_structEOBRecord.AdjusterFullName = ((string)(objReader["FIRST_NAME"].ToString() + " " + objReader["LAST_NAME"].ToString())).Trim();
                                m_structEOBRecord.AdjusterLastName = objReader["LAST_NAME"].ToString();
                                //Arnab: MITS-10722 - Getting Adjuster phone no
                                m_structEOBRecord.AdjusterPhone1 = Conversion.ConvertObjToStr(objReader["PHONE1"]);
                                //MITS-10722 End
                                if (Conversion.ConvertObjToInt(objReader["CURRENT_ADJ_FLAG"], m_iClientId) != 0)
                                    break;
                            }
                            objReader.Close();
                        }
                    }
                    //Make sure we have detail items (exit if none)//Deb UnderWriters
                    sSQL = "SELECT COUNT(INVOICE_ID) FROM INVOICE_DETAIL WHERE INVOICE_ID = " + iInvoiceId;
                    objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            if (Conversion.ConvertObjToInt(objReader[0], m_iClientId) == 0)
                            {
                                if (!p_bIsSilent)
                                    throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems", m_iClientId));//sharishkumar Jira 835
                                return;
                            }
                        }
                        objReader.Close();
                    }
                    //Ashish Ahuja - Mits 33979 start
                    sSQL = "SELECT CLAIMANT_EID FROM CLAIMANT WHERE PRIMARY_CLMNT_FLAG =-1 AND CLAIM_ID = " + objClaim.ClaimId;
                    objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iClaimantEid = Conversion.ConvertObjToInt(objReader[0], m_iClientId);
                        }
                        objReader.Close();
                    }
                    if (iClaimantEid == 0)
                    {    
                        objReader = DbFactory.GetDbReader(m_ConnectionString, "SELECT CLAIMANT_EID FROM CLAIMANT WHERE PRIMARY_CLMNT_FLAG =0 AND CLAIM_ID = " + objClaim.ClaimId);
                        if (objReader.Read())
                        {
                            iClaimantEid = Conversion.ConvertObjToInt(objReader[0], m_iClientId);
                        }
                        objReader.Close();
                    }
                    //Ashish Ahuja - Mits 33979 end
                    //skhare7 MITS 23373 employer add
                    try
                    {
                        objEntity.MoveTo(iCompId);
                        m_structEOBRecord.EMP_ADDRESS1 = objEntity.Addr1 + " " + objEntity.Addr2 + " " + objEntity.Addr3 + " " + objEntity.Addr4;
                    }
                    catch
                    {
                        m_structEOBRecord.EMP_ADDRESS1 = "";
                    }

                    //division add
                    
                    
                    try
                    {
                        objEntity.MoveTo(iDivisionId);
                        m_structEOBRecord.EMP_DIV_ADDR1 = objEntity.Addr1;
                        m_structEOBRecord.EMP_DIV_ADDR2 = objEntity.Addr2;
                        m_structEOBRecord.EMP_DIV_ADDR3 = objEntity.Addr3;
                        m_structEOBRecord.EMP_DIV_ADDR4 = objEntity.Addr4;
                    }
                    catch
                    {
                        m_structEOBRecord.EMP_DIV_ADDR1 = "";
                        m_structEOBRecord.EMP_DIV_ADDR2 = "";
                        m_structEOBRecord.EMP_DIV_ADDR3 = "";
                        m_structEOBRecord.EMP_DIV_ADDR4 = "";
                    }

                    //Region add
                    try
                    {
                        objEntity.MoveTo(iRegionId);
                        m_structEOBRecord.EMP_REG_ADDR1 = objEntity.Addr1;
                        m_structEOBRecord.EMP_REG_ADDR2 = objEntity.Addr2;
                        m_structEOBRecord.EMP_REG_ADDR3 = objEntity.Addr3;
                        m_structEOBRecord.EMP_REG_ADDR4 = objEntity.Addr4;

                    }
                    catch
                    {
                        m_structEOBRecord.EMP_REG_ADDR1 = "";
                        m_structEOBRecord.EMP_REG_ADDR2 = "";
                        m_structEOBRecord.EMP_REG_ADDR3 = "";
                        m_structEOBRecord.EMP_REG_ADDR4 = "";

                    }
                    //company add
                    try
                    {
                        objEntity.MoveTo(iCompId);
                        m_structEOBRecord.EMP_COMP_ADDR1 = objEntity.Addr1;
                        m_structEOBRecord.EMP_COMP_ADDR2 = objEntity.Addr2;
                        m_structEOBRecord.EMP_COMP_ADDR3 = objEntity.Addr3;
                        m_structEOBRecord.EMP_COMP_ADDR4 = objEntity.Addr4;

                    }
                    catch
                    {
                        m_structEOBRecord.EMP_COMP_ADDR1 = "";
                        m_structEOBRecord.EMP_COMP_ADDR2 = "";
                        m_structEOBRecord.EMP_COMP_ADDR3 = "";
                        m_structEOBRecord.EMP_COMP_ADDR4 = "";

                    }
                    //facility add
                    try
                    {
                        objEntity.MoveTo(iFacilityId);
                        m_structEOBRecord.EMP_FAC_ADDR1 = objEntity.Addr1;
                        m_structEOBRecord.EMP_FAC_ADDR2 = objEntity.Addr2;
                        m_structEOBRecord.EMP_FAC_ADDR3 = objEntity.Addr3;
                        m_structEOBRecord.EMP_FAC_ADDR4 = objEntity.Addr4;

                    }
                    catch
                    {
                        m_structEOBRecord.EMP_FAC_ADDR1 = "";
                        m_structEOBRecord.EMP_FAC_ADDR2 = "";
                        m_structEOBRecord.EMP_FAC_ADDR3 = "";
                        m_structEOBRecord.EMP_FAC_ADDR4 = "";

                    }
                    //client add
                    try
                    {
                        objEntity.MoveTo(iClientId);
                        m_structEOBRecord.EMP_CLIENT_ADDR1 = objEntity.Addr1;
                        m_structEOBRecord.EMP_CLIENT_ADDR2 = objEntity.Addr2;
                        m_structEOBRecord.EMP_CLIENT_ADDR3 = objEntity.Addr3;
                        m_structEOBRecord.EMP_CLIENT_ADDR4 = objEntity.Addr4;

                    }
                    catch
                    {
                        m_structEOBRecord.EMP_CLIENT_ADDR1 = "";
                        m_structEOBRecord.EMP_CLIENT_ADDR2 = "";
                        m_structEOBRecord.EMP_CLIENT_ADDR3 = "";
                        m_structEOBRecord.EMP_CLIENT_ADDR4 = "";
                    }
                    //op add
                    try
                    {
                        objEntity.MoveTo(iOerationId);
                        m_structEOBRecord.EMP_OP_ADDR1 = objEntity.Addr1;
                        m_structEOBRecord.EMP_OP_ADDR2 = objEntity.Addr2;
                        m_structEOBRecord.EMP_OP_ADDR3 = objEntity.Addr3;
                        m_structEOBRecord.EMP_OP_ADDR4 = objEntity.Addr4;

                    }
                    catch
                    {
                        m_structEOBRecord.EMP_OP_ADDR1 = "";
                        m_structEOBRecord.EMP_OP_ADDR2 = "";
                        m_structEOBRecord.EMP_OP_ADDR3 = "";
                        m_structEOBRecord.EMP_OP_ADDR4 = "";
                    }
                    //op add
                    try
                    {
                        objEntity.MoveTo(iLocationId);
                        m_structEOBRecord.EMP_LOC_ADDR1 = objEntity.Addr1;
                        m_structEOBRecord.EMP_LOC_ADDR2 = objEntity.Addr2;
                        m_structEOBRecord.EMP_LOC_ADDR3 = objEntity.Addr3;
                        m_structEOBRecord.EMP_LOC_ADDR4 = objEntity.Addr4;

                    }
                    catch
                    {
                        m_structEOBRecord.EMP_LOC_ADDR1 = "";
                        m_structEOBRecord.EMP_LOC_ADDR2 = "";
                        m_structEOBRecord.EMP_LOC_ADDR3 = "";
                        m_structEOBRecord.EMP_LOC_ADDR4 = "";
                    }

                    //Dept add
                    try
                    {
                        objEntity.MoveTo(iDeptAssignEId);
                        m_structEOBRecord.EMP_DEPT_ADDR1 = objEntity.Addr1;
                        m_structEOBRecord.EMP_DEPT_ADDR2 = objEntity.Addr2;
                        m_structEOBRecord.EMP_DEPT_ADDR3 = objEntity.Addr3;
                        m_structEOBRecord.EMP_DEPT_ADDR4 = objEntity.Addr4;

                    }
                    catch
                    {
                        m_structEOBRecord.EMP_DEPT_ADDR1 = "";
                        m_structEOBRecord.EMP_DEPT_ADDR2 = "";
                        m_structEOBRecord.EMP_DEPT_ADDR3 = "";
                        m_structEOBRecord.EMP_DEPT_ADDR4 = "";
                    }
                  //  skhare7 MITS 23373 End

                    //Lookup Claimant Name/Address info
                    try
                    {
                        //Ashish Ahuja - Mits 33979 start
                        objEntity.FirstName = "";
                        objEntity.LastName = "";
                        objEntity.Addr1 = "";
                        objEntity.Addr2 = "";
                        objEntity.Addr3 = "";
                        objEntity.Addr4 = "";
                        objEntity.StateId = 0;
                        objEntity.City = "";
                        objEntity.ZipCode = "";
                        objEntity.TaxId = "";
                        //Ashish Ahuja - Mits 33979 end

                        objEntity.MoveTo(iClaimantEid);

                        sFirstName = objEntity.FirstName;
                        sLastName = objEntity.LastName;
                        //Ashish Ahuja - Mits 33979
                        //sClaimantName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
                        sClaimantName = sFirstName == string.Empty ? sLastName : sFirstName+" "+sLastName;
                        //Ashish Ahuja - Mits 33979
                        string tempClaimantFullName = sClaimantName;
                        string sClaimantFullName = "";
                        int startindex = 0;
                        int lastindex = 20;
                        int len = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(tempClaimantFullName.Length) / Convert.ToDouble(lastindex)));
                        for (int i = 0; i < len; i++)
                        {
                            if (tempClaimantFullName != "")
                            {
                                if (i != len - 1)
                                {
                                    sClaimantFullName += tempClaimantFullName.Substring(startindex, lastindex) + "<br/>";
                                    tempClaimantFullName = tempClaimantFullName.Remove(startindex, lastindex);
                                }
                                else
                                {
                                    sClaimantFullName += tempClaimantFullName.Substring(startindex, tempClaimantFullName.Length);
                                   
                                }
                            }
                        }
                        sClaimantName = sClaimantFullName;
                        //Ashish Ahuja - Mits 33979 END


                        sRegClaimantName = ((string)(sFirstName + " " + sLastName)).Trim();
                        sClaimantAddr1 = objEntity.Addr1;
                        sClaimantAddr2 = objEntity.Addr2;
                        sClaimantAddr3 = objEntity.Addr3;
                        sClaimantAddr4 = objEntity.Addr4;
                        objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                        m_structEOBRecord.ClaimantState = ((string)(strStateCode + " " + strStateDesc)).Trim();
                        m_structEOBRecord.ClaimantCity = objEntity.City;
                        m_structEOBRecord.ClaimantPostalCode = objEntity.ZipCode;
                        sClaimantCity = m_structEOBRecord.ClaimantCity + ", " + m_structEOBRecord.ClaimantState
                            + " " + m_structEOBRecord.ClaimantPostalCode;
                        sClaimantTaxId = objEntity.TaxId;
                        if (bIsFormatFileFound)
                        {
                            m_structEOBRecord.ClaimantFullName = sClaimantName;
                            m_structEOBRecord.ClaimantLastName = sLastName;
                            m_structEOBRecord.ClaimantFirstName = sFirstName;
                            m_structEOBRecord.ClaimantAddr1 = sClaimantAddr1;
                            m_structEOBRecord.ClaimantAddr2 = sClaimantAddr2;
                            m_structEOBRecord.ClaimantAddr3 = sClaimantAddr3;
                            m_structEOBRecord.ClaimantAddr4 = sClaimantAddr4;
                            m_structEOBRecord.TransDate = sTransDateText;
                            m_structEOBRecord.ClaimantSSN = sClaimantTaxId;
                        }
                    }
                    catch
                    {
                        if (bIsFormatFileFound)
                        {
                            m_structEOBRecord.ClaimantFullName = string.Empty;
                            m_structEOBRecord.ClaimantLastName = string.Empty;
                            m_structEOBRecord.ClaimantFirstName = string.Empty;
                            m_structEOBRecord.ClaimantAddr1 = string.Empty;
                            m_structEOBRecord.ClaimantAddr2 = string.Empty;
                            m_structEOBRecord.ClaimantAddr3 = string.Empty;
                            m_structEOBRecord.ClaimantAddr4 = string.Empty;
                            m_structEOBRecord.TransDate = string.Empty;
                            m_structEOBRecord.ClaimantSSN = string.Empty;
                        }
                    }

                    //  skhare7 MITS 23373 
                    m_structEOBRecord.EMP_CERT_NUM = GetOrganizationLevelInsuredNo(iDeptAssignEId,iTransId ,iDeptTableId, iStateId, m_structEOBRecord.EventDate, sLOBCode);
                    //  skhare7 MITS 23373 End
                    objEntity.Dispose();

                    //This prints the default EOB Form
                    if (m_bPrintPayeeEOB)
                    {
                        if (!bIsFormatFileFound)
                        {
                            //Get default HTML template
                            sPayeeHTMLtext = GetDefaultPayeeHTMLTemplate(sName, sAddr1, sAddr2, sAddr3, sAddr4, strStateCode, strStateDesc, sCity,
                                sRegClaimantName, sClaimantAddr1, sClaimantAddr2, sClaimantAddr3, sClaimantAddr4, sClaimantCity, sClaimNumberText, sClaimantName);
                        }
                        else
                        {
                            //Print HTML template Custom File
                            if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true" && m_iClientId != 0)
                            {
                                MemoryStream objMemoryStream = CommonFunctions.RetreiveTempFileFromDB(sPayeeFileName, "", m_ConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                                objFormatFileStream = new StreamReader(objMemoryStream);
                            }
                            else
                            {
                                objFormatFileStream = File.OpenText(sPayeeFileName);
                            }
                            sPayeeHTMLtext = new StringBuilder(objFormatFileStream.ReadToEnd());
                        }
                        XmlDocument xPayeeHTMLdoc = ProcessMultiRowEOBHTMLtables(sPayeeHTMLtext, iInvoiceId);
                        xPayeeHTMLdoc = CreateHTMLdocument(xPayeeHTMLdoc, m_structEOBRecord, m_structEOBDetailRecord, m_objFunds);
                        for (int start = 0; ; )
                        {
                            start = objPdfDoc.DrawStringHtml(xPayeeHTMLdoc.InnerXml, fFont, System.Drawing.Brushes.Black, rcPage, start);
                            if (start >= int.MaxValue)
                                break;
                            objPdfDoc.NewPage();
                        }
                    }

                    //Print copy for claimant
                    if (m_bPrintClaimantEOB)
                    {
                        if (iClaimantEid > 0)
                        {
                        if (m_bPrintPayeeEOB)
                        {
                            objPdfDoc.NewPage();
                        }

                        if (!bIsFormatFileFound)
                        {
                            sClaimantHTMLtext = GetDefaultClaimantHTMLTemplate(sClaimNumberText, sClaimantName, sName);
                        }
                        else
                        {
                            if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true" && m_iClientId != 0)
                            {
                                MemoryStream objMemoryStream = CommonFunctions.RetreiveTempFileFromDB(sClaimantFileName, "", m_ConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                                objFormatFileStream = new StreamReader(objMemoryStream);
                            }
                            else
                            {
                                objFormatFileStream = File.OpenText(sClaimantFileName);
                            }
                            sClaimantHTMLtext = new StringBuilder(objFormatFileStream.ReadToEnd());
                        }
                        XmlDocument xClaimantHTMLdoc = ProcessMultiRowEOBHTMLtables(sClaimantHTMLtext, iInvoiceId);
                        xClaimantHTMLdoc = CreateHTMLdocument(xClaimantHTMLdoc, m_structEOBRecord, m_structEOBDetailRecord, m_objFunds);
                        for (int start = 0; ; )
                        {
                            start = objPdfDoc.DrawStringHtml(xClaimantHTMLdoc.InnerXml, fFont, System.Drawing.Brushes.Black, rcPage, start);
                            if (start >= int.MaxValue)
                                break;
                            objPdfDoc.NewPage();
                        }
                      }
                    }

                    if (objFormatFileStream != null)
                    {
                        objFormatFileStream.Close();
                        objFormatFileStream.Dispose();
                        objFormatFileStream = null;
                    }

                    if (m_bPrintClaimantEOB || m_bPrintPayeeEOB)
                    {
                        objPdfDoc.DocumentInfo.Title = "EOB Report";

                        p_sPDFSaveFilePath = iTransId + "_" + p_sState + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                        string sTempFileName = p_sPDFSaveFilePath;//Add code by kuladeep for Temp Storage-Jira 163     
                        sDestFolderPath = Functions.GetSavePath();

                        objPdfDoc.Save(sDestFolderPath + @"\" + p_sPDFSaveFilePath);
                        p_sPDFSaveFilePath = sDestFolderPath + @"\" + p_sPDFSaveFilePath;

                        //Add code by kuladeep for Temp Storage-Start Jira 163                       
                        CommonFunctions.InsertTempFileIntoDB(sTempFileName, p_sPDFSaveFilePath, m_ConnectionString, m_iClientId);
                        //Add code by kuladeep for Temp Storage-End
                    }
                }
                catch (DataModelException p_objEx)
                {
                    throw p_objEx;
                }
                catch (RMAppException p_objEx)
                {
                    throw p_objEx;
                }
                catch (Exception p_objEx)
                {
                    throw new RMAppException(Globalization.GetString("PrintEOBAR.GetPrintCheckEOB.ErrorGet", m_iClientId), p_objEx);//sharishkumar Jira 835
                }
                finally
                {
                    if (objClaim != null)
                        objClaim.Dispose();

                    if (objState != null)
                        objState.Dispose();

                    if (objDataSet != null)
                        objDataSet.Dispose();

                    if (objEmployee != null)
                        objEmployee.Dispose();

                    if (objReader != null)
                        objReader.Dispose();

                    if (objBrsInvoice != null)
                        objBrsInvoice.Dispose();

                    if (objEntity != null)
                        objEntity.Dispose();

                    if (objFormatFileStream != null)
                        objFormatFileStream = null;

                    if (objLocalCache != null)
                        objLocalCache.Dispose();
                    if (objDataModelFactory != null)
                    {
                        objDataModelFactory.Dispose();
                        objDataModelFactory = null;
                    }
                }
            }
        }
        #endregion
        //skhare7 MITS 23373
        private string GetOrganizationLevelInsuredNo(int p_iDeptAssignEId, int p_iTransId,int p_iDeptTableId, int p_iStateId, string p_sEventDate, string p_sLobCode)
        {
            LocalCache objLocalCache = null;
            DataSet objdataSet=null;
            string sSQL = string.Empty;
          //  StreamWriter sw1 = null;
            string sInsuredNo = string.Empty;
            int iCount = 0;
            try
            {
//Deb UnderWriters
                objLocalCache = new LocalCache(m_ConnectionString,m_iClientId);
                //sSQL = "SELECT SI_ROW_ID, CERT_NUMBER FROM ENTITY_X_SELFINSUR"
                //          + " WHERE ENTITY_ID=" + p_iDeptAssignEId + " AND JURIS_ROW_ID=" + p_iStateId + ""
                //          + " AND EFFECTIVE_DATE<='" + p_sEventDate + "' AND EXPIRATION_DATE>='" + p_sEventDate + "'"
                //          + " AND (LINE_OF_BUS_CODE=" + p_sLobCode + " OR LINE_OF_BUS_CODE=-3) ORDER BY SI_ROW_ID";
                //objdataSet = DbFactory.GetDataSet(m_ConnectionString, sSQL);
                sSQL = "SELECT COUNT(SI_ROW_ID) FROM ENTITY_X_SELFINSUR"
                          + " WHERE ENTITY_ID=" + p_iDeptAssignEId + " AND JURIS_ROW_ID=" + p_iStateId + ""
                          + " AND EFFECTIVE_DATE<='" + p_sEventDate + "' AND EXPIRATION_DATE>='" + p_sEventDate + "'"
                          + " AND (LINE_OF_BUS_CODE=" + p_sLobCode + " OR LINE_OF_BUS_CODE=-3)";
                int iCountTotal = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_ConnectionString, sSQL), m_iClientId);
                if (iCountTotal > 0)
                {
                    sSQL = "SELECT SI_ROW_ID, CERT_NUMBER FROM ENTITY_X_SELFINSUR"
                          + " WHERE ENTITY_ID=" + p_iDeptAssignEId + " AND JURIS_ROW_ID=" + p_iStateId + ""
                          + " AND EFFECTIVE_DATE<='" + p_sEventDate + "' AND EXPIRATION_DATE>='" + p_sEventDate + "'"
                          + " AND (LINE_OF_BUS_CODE=" + p_sLobCode + " OR LINE_OF_BUS_CODE=-3) ORDER BY SI_ROW_ID";
                    objdataSet = DbFactory.GetDataSet(m_ConnectionString, sSQL,m_iClientId);//sharishkumar Jira 835
                    foreach (DataRow objDataRow in objdataSet.Tables[0].Rows)
                    {
                        if (objDataRow["CERT_NUMBER"].ToString() != "")
                        {
                            sInsuredNo = objDataRow["CERT_NUMBER"].ToString();
                            iCount = iCount + 1;
                            if (iCount != 0)
                            {
                                if (!File.Exists(RMConfigurator.AppFilesPath + @"\EOB_Cert_log.Log"))
                                {
                                    FileStream fs = null;
                                    using (fs = File.Create(RMConfigurator.AppFilesPath + @"\EOB_Cert_log.Log"))
                                    {
                                        using (StreamWriter sw1 = new StreamWriter(RMConfigurator.AppFilesPath + @"\EOB_Cert_log.Log"))
                                        {

                                            sw1.WriteLine("TransId:" + p_iTransId);
                                            sw1.WriteLine("CurrentDate:" + DateTime.Now);
                                            sw1.WriteLine("EntityId=" + p_iDeptAssignEId);
                                            sw1.WriteLine("Jurisdiction=" + p_iStateId);
                                            sw1.WriteLine("EventDate=" + p_sEventDate);
                                            sw1.WriteLine("SI_ROW_ID=" + objDataRow["SI_ROW_ID"].ToString());
                                            sw1.WriteLine("Insured No=" + sInsuredNo);
                                            sw1.WriteLine();

                                        }
                                    }
                                }
                                else
                                {
                                    StreamWriter sw1 = File.AppendText(RMConfigurator.AppFilesPath + @"\EOB_Cert_log.Log");
                                    sw1.WriteLine("TransId:" + p_iTransId);
                                    sw1.WriteLine("CurrentDate:" + DateTime.Now);
                                    sw1.WriteLine("EntityId=" + p_iDeptAssignEId);
                                    sw1.WriteLine("Jurisdiction=" + p_iStateId);
                                    sw1.WriteLine("EventDate=" + p_sEventDate);
                                    sw1.WriteLine("SI_ROW_ID=" + objDataRow["SI_ROW_ID"].ToString());
                                    sw1.WriteLine("Insured No=" + sInsuredNo);
                                    sw1.WriteLine();
                                    sw1.Close();
                                }
                            }
                        }
                    }
                    if (sInsuredNo == "")
                    {
                        if (p_iDeptTableId >= (objLocalCache.GetTableId("CLIENT")))
                        {
                            p_iDeptAssignEId = GetOrganization(p_iDeptAssignEId);
                            if (p_iDeptAssignEId == 0) return ""; // asharma326 MITS 35644 
                            p_iDeptTableId--;
                            sInsuredNo = GetOrganizationLevelInsuredNo(p_iDeptAssignEId, p_iTransId, p_iDeptTableId, p_iStateId, p_sEventDate, p_sLobCode);
                        }
                    }
                }
                else
                {
                    if (p_iDeptTableId >= (objLocalCache.GetTableId("CLIENT")))
                    {
                        p_iDeptAssignEId = GetOrganization(p_iDeptAssignEId);
                        if (p_iDeptAssignEId == 0) return ""; // asharma326 MITS 35644 
                        p_iDeptTableId--;
                        sInsuredNo = GetOrganizationLevelInsuredNo(p_iDeptAssignEId, p_iTransId, p_iDeptTableId, p_iStateId, p_sEventDate, p_sLobCode);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.GetDefaultPayeeHTMLTemplate",m_iClientId), ex);//sharishkumar Jira 835
            }
            finally
            {
                if (objdataSet != null)
                    objdataSet.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
              
            
            }
            return sInsuredNo;
        }
      
        private int GetOrganization(int OrgId)
        {
            string sSQLDept = string.Empty;
            bool bIsSuccess = false;
            int iOrgHierId = 0;
            try
            {
                sSQLDept = "SELECT PARENT_EID FROM ENTITY WHERE ENTITY_ID='" + OrgId + "'";
                using (DbConnection objConn = DbFactory.GetDbConnection(m_ConnectionString))
                {
                    objConn.Open();
                    iOrgHierId = Conversion.CastToType<int>(Convert.ToString(objConn.ExecuteScalar(sSQLDept)), out bIsSuccess);
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.GetDefaultPayeeHTMLTemplate", m_iClientId), ex);//sharishkumar Jira 835
            }
            return iOrgHierId;
        }
        //skhare7 MITS 23373 End
        #region "GetDefaultPayeeHTMLTemplate(string sName, string sAddr1, string sAddr2, string sAddr3, string sAddr4, string strStateCode, string strStateDesc,string sCity, string sRegClaimantName, string sClaimantAddr1, string sClaimantAddr2, string sClaimantAddr3, string sClaimantAddr4, string sClaimantCity, string sClaimNumberText,string sClaimantName)"
        /// <summary>
        /// The function returns the default payee HTML template
        /// Author: Debabrata Biswas MITS Issue 17183
        /// Date:   30 july 2009
        /// </summary>
        /// <returns>sDefaultPayeeHTMLtext</returns>
        private StringBuilder GetDefaultPayeeHTMLTemplate(string sName, string sAddr1, string sAddr2, string sAddr3, string sAddr4, string strStateCode, string strStateDesc,
            string sCity, string sRegClaimantName, string sClaimantAddr1, string sClaimantAddr2, string sClaimantAddr3, string sClaimantAddr4, string sClaimantCity, string sClaimNumberText,
            string sClaimantName)
        {
            Common objCommon = null;
            try
            {
                objCommon = new Common();
                StringBuilder sDefaultPayeeHTMLtext = new StringBuilder(null);

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<HTML>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<HEAD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</HEAD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BODY>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<CENTER>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<H1>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBHeader.EOB", m_iClientId)));//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</H1>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</CENTER>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(DateTime.Now.ToLongDateString());
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TABLE>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sName.Trim() + "</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sAddr1 + "</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
                if (sAddr2 != string.Empty)
                {
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sAddr2 + "</TD>");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
                }
                if (sAddr3 != string.Empty)
                {
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sAddr3 + "</TD>");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
                }
                if (sAddr4 != string.Empty)
                {
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sAddr4 + "</TD>");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
                }

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sCity + ", " + ((string)(strStateCode + " " + strStateDesc)).Trim() + " " + m_structEOBRecord.PayeePostalCode + "</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sRegClaimantName + " " + sClaimantAddr1 + " " + " " + sClaimantAddr2 + " " + sClaimantAddr3 + " " + " " + sClaimantAddr4 + " " + sClaimantCity + "</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TABLE>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.RE", m_iClientId)) + " " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText", m_iClientId)) + " " + sClaimNumberText);//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName", m_iClientId)) + " " + sClaimantName);//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.Name", m_iClientId)) + " " + sRegClaimantName);//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");

                if (sName == string.Empty)
                {
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername1", m_iClientId)));//sharishkumar Jira 835
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername2", m_iClientId)));//sharishkumar Jira 835
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                }
                else
                {
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername3",m_iClientId)) + " " + sName + " " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername4",m_iClientId)));//sharishkumar Jira 835
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername5", m_iClientId)));//sharishkumar Jira 835
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                }
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.ItemsText", m_iClientId)));//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");

                //Dynamic Multirow table
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TABLE CELLPADDING=\"1\" BORDER=\"1\" BORDERCOLOR=\"WHITE\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>INVOICE NUMBER</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>PROCEDURE DATE</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>PROC. CODE</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>EXPLANATION</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD ALIGN=\"RIGHT\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>AMOUNT BILLED</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD ALIGN=\"RIGHT\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>AMOUNT PAID</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR TYPE=\"MULTIROW\">");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD VALIGN=\"TOP\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{INVOICE_NUMBER}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD VALIGN=\"TOP\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{PROCEDURE_DATE}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD VALIGN=\"TOP\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{PROCEDURE_CODE}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD VALIGN=\"TOP\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{EXPLANATION}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD ALIGN=\"RIGHT\" VALIGN=\"TOP\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{AMOUNT_BILLED}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD ALIGN=\"RIGHT\" VALIGN=\"TOP\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{AMOUNT_PAID}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>TOTAL</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD STYLE=\"FONT-WEIGHT: BOLD; TEXT-ALIGN: RIGHT;\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{TOTAL_AMOUNT_BILLED}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD STYLE=\"FONT-WEIGHT: BOLD; TEXT-ALIGN: RIGHT;\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{TOTAL_AMOUNT_PAID}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TABLE>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.ContactUs", m_iClientId)));//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("Sincerely,");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(objCommon.m_stLoginInfo.Username);

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.ClaimantCopy",m_iClientId)));//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Cc",m_iClientId)) + " " + sClaimantName);//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</BODY>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</HTML>");

                return sDefaultPayeeHTMLtext;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.GetDefaultPayeeHTMLTemplate", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
        }
        #endregion

        #region "ProcessMultiRowEOBHTMLtables(string sHTML)"
        /// <summary>
        /// Author: Debabrata Biswas MITS Issue 17183
        /// This section handles multirow html table.
        /// </summary>
        /// <param name="sHTML"></param>
        /// <param name="p_iInvoiceId"></param>
        /// <returns></returns>
        private XmlDocument ProcessMultiRowEOBHTMLtables(StringBuilder sHTML, int p_iInvoiceId)
        {
            XmlDocument xHTMLdocDOM;
            XmlNodeList xMultiRowTR;
            XmlNodeList xTD;
            XmlNode xTempNode;

            string sTableColumns = string.Empty;
            string sTableRows = string.Empty;
            string sAttribAndValue = string.Empty;
            string sTempCol = string.Empty;
            string[] sTempArray;
            string[] sTableRowArray;
            string sLength = "0";

            try
            {
                xHTMLdocDOM = new XmlDocument();
                xHTMLdocDOM.LoadXml(sHTML.ToString());
                xMultiRowTR = xHTMLdocDOM.SelectNodes("//TABLE/TR[@TYPE='MULTIROW']");

                foreach (XmlNode xTR in xMultiRowTR)
                {
                    xTD = xTR.SelectNodes("./TD");

                    foreach (XmlNode xEOBField in xTD)
                    {
                        sAttribAndValue = "";
                        foreach (XmlAttribute xAttrb in xEOBField.Attributes)
                        {
                            if (sAttribAndValue == "")
                                sAttribAndValue = xAttrb.Name + "=" + "\"" + xAttrb.Value + "\"";
                            else
                                sAttribAndValue = sAttribAndValue + " " + xAttrb.Name + "=" + "\"" + xAttrb.Value + "\"";
                        }

                        sTempCol = xEOBField.InnerText;
                        sTempCol = sTempCol.Substring(1, sTempCol.Length - 2);
                        sLength = "0";

                        if (sTempCol.IndexOf(',') != -1)
                        {
                            sTempArray = sTempCol.Split(',');
                            sTempCol = sTempArray[0];
                            sLength = sTempArray[1];
                        }

                        sTempCol = "{" + sTempCol + "," + sLength + "," + sAttribAndValue + "}";

                        if (sTableColumns == string.Empty)
                            sTableColumns = sTempCol;
                        else
                            sTableColumns = sTableColumns + "|" + sTempCol;
                    }
                    sTableRows = this.PrintEOBHTMLTABLE(p_iInvoiceId, sTableColumns);
                    sTableRowArray = sTableRows.Split('|');
                    Array.Reverse(sTableRowArray);

                    xTR.InnerXml = "";

                    foreach (string sTemp in sTableRowArray)
                    {
                        xTempNode = xHTMLdocDOM.CreateElement("TR");
                        xTempNode.InnerXml = sTemp.Replace("&", "&amp;");
                        xTR.ParentNode.InsertAfter(xTempNode, xTR);
                    }

                    sTableColumns = string.Empty;
                }
                return xHTMLdocDOM;
            }
            //Added for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            //Added for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.ProcessMultiRowEOBHTMLtables", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                xHTMLdocDOM = null;
                xMultiRowTR = null;
                xTD = null;
                sTempArray = null;
            }
        }
        #endregion

        #region "CreateHTMLdocument(xHTMLdom,m_objFunds)"
        /// <summary>
        /// Author: Debabrata Biswas MITS Issue 17183
        /// This section outputs the complete HTML structure.
        /// </summary>
        /// <param name="xHTMLdom"></param>
        /// <param name="m_structEOBRecord"></param>
        /// <param name="m_structEOBDetailRecord"></param>
        /// <param name="m_objFunds"></param>
        /// <returns></returns>
        private XmlDocument CreateHTMLdocument(XmlDocument xHTMLdom, EOBRecord m_structEOBRecord, EOBDetailRecord m_structEOBDetailRecord, Funds m_objFunds)
        {
            XmlDocument xTempHTMLdom;
            MemoryStream p_MemoryStream;
            StreamReader p_objSReader;
            string sCurrentToken;
            string sProcessedToken;
            string sHTMLdom = string.Empty;
            int iNextInput;

            try
            {
                p_MemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xHTMLdom.InnerXml));
                p_objSReader = new StreamReader(p_MemoryStream);

                while ((iNextInput = p_objSReader.Peek()) != -1)
                {
                    sCurrentToken = GetCurrentHTMLToken(p_objSReader, '<', '>');

                    if (sCurrentToken.StartsWith("{") && sCurrentToken.EndsWith("}"))
                        sProcessedToken = ProcessHTMLToken(sCurrentToken, 0, m_structEOBRecord, m_structEOBDetailRecord, m_objFunds);
                    else
                        sProcessedToken = sCurrentToken;

                    sHTMLdom += sProcessedToken;
                }

                xTempHTMLdom = new XmlDocument();
                xTempHTMLdom.LoadXml(sHTMLdom.ToString().Replace("&amp;", "&").Replace("&", "&amp;"));

                return xTempHTMLdom;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.CreateHTMLdocument", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                xTempHTMLdom = null;
                p_MemoryStream = null;
                p_objSReader = null;
            }
        }
        #endregion

        #region "GetDefaultClaimantHTMLTemplate(string sClaimNumberText, string sClaimantName, string sName)"
        /// <summary>
        /// Author: Debabrata Biswas MITS Issue 17183
        /// Date:   30 july 2009
        /// This function returns the default Claimant HTML template
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetDefaultClaimantHTMLTemplate(string sClaimNumberText, string sClaimantName, string sName)
        {
            Common objCommon = null;
            try
            {
                StringBuilder sDefaultClaimantHTMLtext = new StringBuilder(null);
                objCommon = new Common();
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<HTML>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<HEAD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</HEAD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BODY>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.RE", m_iClientId)) + " " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText", m_iClientId)) + " " + sClaimNumberText);//sharishkumar Jira 835
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName",m_iClientId)) + " " + sClaimantName);//sharishkumar Jira 835
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBReport.Name", m_iClientId)) + " " + sName);//sharishkumar Jira 835
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");

                if (sName == string.Empty)
                {
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername1", m_iClientId)));//sharishkumar Jira 835
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername2", m_iClientId)));//sharishkumar Jira 835
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                }
                else
                {
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername3", m_iClientId)) + " " + sName + " " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername4", m_iClientId)));//sharishkumar Jira 835
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.Othername5", m_iClientId)));//sharishkumar Jira 835
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                }
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.ItemsText", m_iClientId)));//sharishkumar Jira 835
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");

                //Dynamic Multirow table
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TABLE CELLPADDING=\"1\" BORDER=\"1\" BORDERCOLOR=\"WHITE\">");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TR>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>INVOICE NUMBER</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>PROCEDURE DATE</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>PROC. CODE</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>EXPLANATION</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>AMOUNT BILLED</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>AMOUNT PAID</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TR>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TR TYPE=\"MULTIROW\">");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{INVOICE_NUMBER}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{PROCEDURE_DATE}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{PROCEDURE_CODE}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{EXPLANATION}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD ALIGN=\"RIGHT\">");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{AMOUNT_BILLED}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD ALIGN=\"RIGHT\">");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{AMOUNT_PAID}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TR>");


                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TR>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>TOTAL</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD STYLE=\"FONT-WEIGHT: BOLD; TEXT-ALIGN: RIGHT;\">");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{TOTAL_AMOUNT_BILLED}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD STYLE=\"FONT-WEIGHT: BOLD; TEXT-ALIGN: RIGHT;\">");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{TOTAL_AMOUNT_PAID}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TR>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TABLE>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PrintEOBAR.EOBBody.ContactUs", m_iClientId)));//sharishkumar Jira 835
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("Sincerely,");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(objCommon.m_stLoginInfo.Username);

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</BODY>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</HTML>");
                //rsharma220 MITS 33704 End
                return sDefaultClaimantHTMLtext;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.GetDefaultClaimantHTMLTemplate", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
        }
        #endregion

        #region "PrintEOBHTMLTABLE(int p_iInvoiceId, string p_sClaimNumberText, string p_sClaimantName, string p_sName, string p_sOtherName, bool p_bIsFormatFileFound, int p_iCurrentX, int p_iCurrentY)"
        /// Name		: PrintEOBHTMLTABLE
        /// Author		: Debabrata Biswas MITS Issue 17183
        /// Date Created	: 24 Juk 2009		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Print EOB; Introduction And Body.Format; File Is the; Custom; Files; of
        /// http-like tags <b>.  If a file cannot be found, a Default format is used.
        /// </summary>
        /// <param name="p_iInvoiceId">Invoice Id</param>
        /// <param name="p_sClaimNumberText">Claim Number Text</param>
        /// <param name="p_sClaimantName">Claimaint Name</param>
        /// <param name="p_sName">Name</param>
        /// <param name="p_sOtherName">Other Name</param>
        /// <param name="p_bIsFormatFileFound">Flag for Format file</param>
        /// <param name="p_iCurrentX">X-axis value</param>
        /// <param name="p_iCurrentY">Y-axis value</param>
        private string PrintEOBHTMLTABLE(int p_iInvoiceId, string sTableColumns)
        {
            bool bIsPrintTotals = false;
            bool bIsUseRecord = false;
            StringBuilder objSQL = null;
            DbReader objReader = null;
            DbReader objTempReader = null;
            FundsTransSplit objFundsTransSplit = null;
            int iTempId = 0;
            double dTempId = 0.0;
            int iNumEOB = 0;
            int iLineItem = 0;
            string sSQL = string.Empty;
            string sTempData = string.Empty;
            string sTempCode = string.Empty;
            double dTotAmtReduced = 0.0;
            double dTotSchdAmt = 0.0;
            double dTotAmtSaved = 0.0;
            double dTotalAmountAllowed = 0.0;
            double dTotalBaseAmount = 0.0;
            double dTotalDiscountAmount = 0.0;
            double dTotalPerDiemAmt = 0.0;
            double dTotalStopLossAmt = 0.0;
            double dTotalFeeTableAmt = 0.0;
            double dTotalAmountBilled = 0.0;
            double dTotalAmountPaid = 0.0;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sInvoiceNumber = string.Empty;
            string sInvoiceDate = string.Empty;
            string sInvoiceBy = string.Empty;
            string sHTMLcellData = string.Empty;
            string sHTMLrowData = string.Empty;
            string sTDData = string.Empty;
            string[] TDList;
            string[] sTempFormat;
            string sAttributes = string.Empty;
            int iDataLength = 0;
            string sTD = string.Empty;
            DataModelFactory objDataModelFactory = null;
            try
            {
                objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId);//sharishkumar Jira 835
                objSQL = new StringBuilder();
                objSQL.Append("SELECT INVOICE_DETAIL_ID,BILLING_CODE_TEXT,AMOUNT_BILLED,AMOUNT_TO_PAY,FUNDS_SPLIT_ROW_ID");
                objSQL.Append(",TABLE_CODE, PERCENTILE,PLACE_OF_SER_CODE,TYPE_OF_SER_CODE,UNITS_BILLED_NUM");
                objSQL.Append(",SCHEDULED_AMOUNT,AMOUNT_REDUCED,AMOUNT_SAVED, QUANTITY,STORE, MODIFIER_CODE");
                objSQL.Append(",CONTRACT_AMOUNT,DISCOUNT,ZIP_CODE,BASE_AMOUNT,AMOUNT_ALLOWED");
                objSQL.Append(",PER_DIEM_AMT,STOP_LOSS_AMT,FEE_TABLE_AMT, PRESCRIP_NO, DRUG_NAME, PRESCRIP_DATE");
                //BRS FL Merge
                objSQL.Append(",FL_LICENSE, REV_CODE");
                //End
                objSQL.Append(" FROM INVOICE_DETAIL, FUNDS_TRANS_SPLIT WHERE INVOICE_ID = " + p_iInvoiceId);
                objSQL.Append(" AND FUNDS_SPLIT_ROW_ID = SPLIT_ROW_ID  ORDER BY FROM_DATE");

                objReader = DbFactory.GetDbReader(m_ConnectionString, objSQL.ToString());

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        m_structEOBDetailRecord.EOBList = new Hashtable();
                        m_structEOBDetailRecord.EOBListDetails = new Hashtable();
                        m_structEOBDetailRecord.DiagnosisList = new Hashtable();
                        m_structEOBDetailRecord.DiagnosisCodeList = new Hashtable();
                        m_structEOBDetailRecord.DiagnosisDetails = new Hashtable();
                        m_structEOBDetailRecord.ModifierList = new Hashtable();

                        TDList = sTableColumns.Split('|');

                        bIsUseRecord = false;

                        //At first check if record is deleted or not
                        iTempId = Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString());

                        foreach (FundsTransSplit objFundsTSplit in m_objFunds.TransSplitList)
                        {
                            if (objFundsTSplit.SplitRowId == iTempId)
                            {
                                bIsUseRecord = true;
                                break;
                            }
                        }

                        iNumEOB = 0;

                        if (bIsUseRecord)
                        {
                            bIsPrintTotals = true;
                            iTempId = Conversion.ConvertStrToInteger(objReader["INVOICE_DETAIL_ID"].ToString());
                            //Added for MITS 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
                            bool bUseCodeDescriptionFile = Convert.ToBoolean(Functions.GetValueFromConfigFile(Functions.TAG_USEEOBDESCRIPTIONFILE,m_ConnectionString, m_iClientId));
                            if (bUseCodeDescriptionFile)//Added if block for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
                            {
                                string sDesciptionFileName = Functions.GetFormatFilePath(m_iClientId)
                                                              + Functions.GetValueFromConfigFile(Functions.TAG_EOBDESCRIPTIONFILE, m_ConnectionString, m_iClientId);//rkaur27


                                if (File.Exists(sDesciptionFileName))
                                {
                                    string sFileText = null;
                                    string sNewText = null;
                                    string sOldText = null;
                                    int iPoint = 0;

                                    StreamReader objSRdr = null;
                                    objSRdr = new StreamReader(sDesciptionFileName);
                                    sFileText = objSRdr.ReadToEnd();

                                    sSQL = "SELECT CODES.SHORT_CODE FROM INVDETAIL_X_EOB, CODES";
                                    sSQL = sSQL + " WHERE INVDETAIL_X_EOB.EOB_CODE = CODES.CODE_ID ";
                                    sSQL = sSQL + " AND INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + iTempId;
                                    objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                                    if (objTempReader != null)
                                    {
                                        while (objTempReader.Read())
                                        {
                                            sTempData = "@" + objTempReader.GetString("SHORT_CODE").Trim();
                                            iPoint = sFileText.IndexOf(sTempData, 0);
                                            if (iPoint >= 0)
                                            {
                                                sOldText = sFileText.Substring(iPoint + 1);
                                                iPoint = sOldText.IndexOf("@");
                                                sNewText = sOldText.Substring(0, iPoint - 1);

                                                if (iNumEOB > 0)
                                                    sNewText = "- " + sNewText;
                                                else
                                                    sNewText = "  " + sNewText;

                                                iNumEOB++;
                                                m_structEOBDetailRecord.EOBList.Add(iNumEOB, sNewText.Trim());
                                                m_structEOBDetailRecord.EOBListDetails.Add(iNumEOB, objTempReader.GetString("SHORT_CODE").Trim() + " " + sNewText.Trim());
                                            }
                                        }
                                        objTempReader.Close();
                                    }
                                    if (objSRdr != null)
                                    {
                                        objSRdr.Close();
                                        objSRdr.Dispose();
                                    }
                                }
                                else
                                {
                                    throw new RMAppException(Globalization.GetString("Functions.EobDescriptionFile.Error", m_iClientId));//sharishkumar Jira 835
                                }
                            }//Added if block for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
                            else
                            {
                                sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM INVDETAIL_X_EOB,CODES_TEXT WHERE ";
                                sSQL = sSQL + "INVDETAIL_X_EOB.EOB_CODE = CODES_TEXT.CODE_ID AND ";
                                sSQL = sSQL + "INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + iTempId;

                                objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                                if (objTempReader != null)
                                {
                                    while (objTempReader.Read())
                                    {
                                        string sTempEob = "";
                                        //Ashish Ahuja - Mits 33681
                                        sTempData = objTempReader["SHORT_CODE"].ToString().Trim() ;
                                        sTempData = "- " + sTempData;
                                        

                                        iNumEOB++;
                                        m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData.Trim());
                                        m_structEOBDetailRecord.EOBListDetails.Add(iNumEOB, objTempReader["SHORT_CODE"].ToString().Trim() + " " + sTempData.Trim());
                                    }
                                    objTempReader.Close();
                                }
                            }

                            //extract diagnosis list
                            sSQL = "SELECT CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM INVDETAIL_X_DIAG, CODES_TEXT";
                            sSQL = sSQL + " WHERE INVDETAIL_X_DIAG.DIAGNOSIS_CODE=CODES_TEXT.CODE_ID";
                            sSQL = sSQL + " AND INVDETAIL_X_DIAG.INVOICE_DETAIL_ID= " + iTempId;

                            objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                            if (objTempReader != null)
                            {
                                iLineItem = 0;
                                while (objTempReader.Read())
                                {
                                    sTempData = objTempReader["CODE_DESC"].ToString();
                                    sTempCode = objTempReader["SHORT_CODE"].ToString();
                                    iLineItem++;
                                    m_structEOBDetailRecord.DiagnosisList.Add(iLineItem, sTempData.Trim());
                                    m_structEOBDetailRecord.DiagnosisCodeList.Add(iLineItem, sTempCode.Trim());
                                    m_structEOBDetailRecord.DiagnosisDetails.Add(iLineItem, sTempCode.Trim() + "  " + sTempData.Trim());
                                }
                                objTempReader.Close();
                            }

                            //extract modifier code
                            sSQL = "SELECT CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM INVDETAIL_X_MOD, CODES, CODES_TEXT";
                            sSQL = sSQL + " WHERE INVDETAIL_X_MOD.MODIFIER_CODE=CODES_TEXT.CODE_ID";
                            sSQL = sSQL + " AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND INVDETAIL_X_MOD.INVOICE_DETAIL_ID=" + iTempId;

                            objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                            if (objTempReader != null)
                            {
                                iLineItem = 0;
                                while (objTempReader.Read())
                                {
                                    sTempData = objTempReader["SHORT_CODE"].ToString() + " " + objTempReader["CODE_DESC"].ToString();
                                    iLineItem++;
                                    m_structEOBDetailRecord.ModifierList.Add(iLineItem, sTempData.Trim());
                                }
                                objTempReader.Close();
                            }

                            //extract fee schedule name
                            iTempId = Conversion.ConvertStrToInteger(objReader["TABLE_CODE"].ToString());
                            sSQL = "SELECT FEE_TABLES.USER_TABLE_NAME FROM FEE_TABLES WHERE FEE_TABLES.TABLE_ID = " + iTempId;
                            objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                            if (objTempReader != null)
                            {
                                if (objTempReader.Read())
                                {
                                    sTempData = objTempReader["USER_TABLE_NAME"].ToString();
                                    m_structEOBDetailRecord.FeeSchedule.Token = sTempData;
                                }
                                objTempReader.Close();
                            }

                            m_structEOBDetailRecord.Percentile.Token = objReader["PERCENTILE"].ToString();

                            iTempId = Conversion.ConvertStrToInteger(objReader["PLACE_OF_SER_CODE"].ToString());
                            sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " +
                                "CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
                            objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                            if (objTempReader != null)
                            {
                                if (objTempReader.Read())
                                {
                                    m_structEOBDetailRecord.PlaceofServiceCode.Token = objTempReader["SHORT_CODE"].ToString();
                                    m_structEOBDetailRecord.PlaceOfService.Token = objTempReader["CODE_DESC"].ToString();
                                }
                                objTempReader.Close();
                            }

                            iTempId = Conversion.ConvertStrToInteger(objReader["TYPE_OF_SER_CODE"].ToString());
                            sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " +
                                "CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
                            objTempReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                            if (objTempReader != null)
                            {
                                if (objTempReader.Read())
                                {
                                    m_structEOBDetailRecord.TypeOfServiceCode.Token = objTempReader["SHORT_CODE"].ToString();
                                    m_structEOBDetailRecord.TypeOfService.Token = objTempReader["CODE_DESC"].ToString();
                                }
                                objTempReader.Close();
                            }

                            m_structEOBDetailRecord.UnitsBilled.Token = Conversion.ConvertStrToLong(objReader["UNITS_BILLED_NUM"].ToString());

                            m_structEOBDetailRecord.ScheduledAmount.Token = Conversion.ConvertStrToDouble(objReader["SCHEDULED_AMOUNT"].ToString());
                            dTotSchdAmt += Functions.FormatAmount(m_structEOBDetailRecord.ScheduledAmount.Token, m_iClientId);

                            m_structEOBDetailRecord.AmountReduced.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_REDUCED"].ToString());
                            dTotAmtReduced += Functions.FormatAmount(m_structEOBDetailRecord.AmountReduced.Token, m_iClientId);

                            m_structEOBDetailRecord.AmountSaved.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_SAVED"].ToString());
                            dTotAmtSaved += Functions.FormatAmount(m_structEOBDetailRecord.AmountSaved.Token, m_iClientId);

                            m_structEOBDetailRecord.ContractAmount.Token = Conversion.ConvertStrToDouble(objReader["CONTRACT_AMOUNT"].ToString());
                            m_structEOBDetailRecord.Discount.Token = Conversion.ConvertStrToDouble(objReader["DISCOUNT"].ToString());
                            m_structEOBDetailRecord.ProviderZipCode.Token = objReader["ZIP_CODE"].ToString();

                            m_structEOBDetailRecord.AmountAllowed.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_ALLOWED"].ToString());
                            dTotalAmountAllowed += Functions.FormatAmount(m_structEOBDetailRecord.AmountAllowed.Token, m_iClientId);

                            m_structEOBDetailRecord.BaseAmount.Token = Conversion.ConvertStrToDouble(objReader["BASE_AMOUNT"].ToString());
                            dTotalBaseAmount += Functions.FormatAmount(m_structEOBDetailRecord.BaseAmount.Token, m_iClientId);

                            m_structEOBDetailRecord.DiscountAmount.Token = m_structEOBDetailRecord.BaseAmount.Token * (m_structEOBDetailRecord.Discount.Token / 100);
                            dTotalDiscountAmount += Functions.FormatAmount(m_structEOBDetailRecord.DiscountAmount.Token, m_iClientId);

                            m_structEOBDetailRecord.PerDiemAmount.Token = Conversion.ConvertStrToDouble(objReader["PER_DIEM_AMT"].ToString());
                            dTotalPerDiemAmt += Functions.FormatAmount(m_structEOBDetailRecord.PerDiemAmount.Token, m_iClientId);

                            m_structEOBDetailRecord.StopLossAmount.Token = Conversion.ConvertStrToDouble(objReader["STOP_LOSS_AMT"].ToString());
                            dTotalStopLossAmt += Functions.FormatAmount(m_structEOBDetailRecord.StopLossAmount.Token, m_iClientId);

                            m_structEOBDetailRecord.FeeTableAmount.Token = Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString());
                            dTotalFeeTableAmt += Functions.FormatAmount(m_structEOBDetailRecord.FeeTableAmount.Token, m_iClientId);

                            m_structEOBDetailRecord.PrescripNo.Token = objReader["PRESCRIP_NO"].ToString();
                            m_structEOBDetailRecord.DrugName.Token = objReader["DRUG_NAME"].ToString();
                            m_structEOBDetailRecord.PrescripDate.Token = Conversion.ToDate(objReader["PRESCRIP_DATE"].ToString()).ToShortDateString();
                            m_structEOBDetailRecord.PhysicianLicenseNum.Token = objReader["FL_LICENSE"].ToString();
                            m_structEOBDetailRecord.RevenueCode.Token = objReader["REV_CODE"].ToString();

                            m_structEOBDetailRecord.DiscOnFeeSchd.Token = 0;
                            if (Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString()) > 0 && Conversion.ConvertStrToDouble(objReader["DISCOUNT"].ToString()) > 0)
                                m_structEOBDetailRecord.DiscOnFeeSchd.Token = Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString()) - Conversion.ConvertStrToDouble(objReader["AMOUNT_ALLOWED"].ToString());

                            objFundsTransSplit = (FundsTransSplit)objDataModelFactory.GetDataModelObject("FundsTransSplit", false);
                            try
                            {
                                objFundsTransSplit.MoveTo(Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString()));
                                m_structEOBDetailRecord.ProcedureDate.Token = Conversion.ToDate(objFundsTransSplit.FromDate).ToShortDateString();
                                m_structEOBDetailRecord.ProcToDate.Token = Conversion.ToDate(objFundsTransSplit.ToDate).ToShortDateString();
                                m_structEOBDetailRecord.InvoiceNumber.Token = objFundsTransSplit.InvoiceNumber;
                                m_structEOBDetailRecord.InvoiceDate.Token = Conversion.ToDate(objFundsTransSplit.InvoiceDate).ToShortDateString();
                                m_structEOBDetailRecord.InvoicedBy.Token = objFundsTransSplit.InvoicedBy;
                            }
                            catch
                            {
                                m_structEOBDetailRecord.ProcedureDate.Token = string.Empty;
                                m_structEOBDetailRecord.ProcToDate.Token = string.Empty;
                                m_structEOBDetailRecord.InvoiceNumber.Token = "N/A";
                                m_structEOBDetailRecord.InvoiceDate.Token = string.Empty;
                                m_structEOBDetailRecord.InvoicedBy.Token = string.Empty;
                            }

                            //Print remaining data fields
                            sTempData = objReader["BILLING_CODE_TEXT"].ToString();
                            //Ashish Ahuja - Mits 33979
                            // akaushik5 Changed for MITS 38019 Starts
                            //m_structEOBDetailRecord.ProcedureCode.Token = sTempData.Substring(0,sTempData.IndexOf(" "));
                            //m_structEOBDetailRecord.Procedure.Token = sTempData.Substring(sTempData.IndexOf('-') + 1);
                            if (sTempData.IndexOf('-') > -1)
                            {
                                m_structEOBDetailRecord.ProcedureCode.Token = sTempData.Split('-')[0].Trim();
                                m_structEOBDetailRecord.Procedure.Token = string.Join("-", sTempData.Split('-'), 1, sTempData.Split('-').Length - 1).Trim();
                            }
                            else
                            { 
                                m_structEOBDetailRecord.ProcedureCode.Token = string.Empty;
                                m_structEOBDetailRecord.Procedure.Token = sTempData;
                            }
                            // akaushik5 Changed for MITS 38019 Ends
                            
                            dTempId = Conversion.ConvertStrToDouble(objReader["AMOUNT_BILLED"].ToString());
                            m_structEOBDetailRecord.AmountBilled.Token = dTempId;

                            dTotalAmountBilled += dTempId;

                            //amount to pay
                            dTempId = Conversion.ConvertStrToDouble(objReader["AMOUNT_TO_PAY"].ToString());
                            m_structEOBDetailRecord.AmountPaid.Token = dTempId;
                            dTotalAmountPaid += dTempId;

                            foreach (string TD in TDList)
                            {
                                sAttributes = "";
                                sTD = TD.Substring(1, TD.Length - 2);
                                sTempFormat = sTD.Split(',');

                                sTD = "{" + sTempFormat[0] + "}";
                                iDataLength = int.Parse(sTempFormat[1]);
                                sAttributes = " " + sTempFormat[2];

                                sTDData = ProcessHTMLtableToken(sTD, iDataLength, ref m_structEOBRecord, ref m_structEOBDetailRecord, objFundsTransSplit);
                                sHTMLcellData = sHTMLcellData + "<TD" + sAttributes + ">" + sTDData + "</TD>";
                            }
                            if (sHTMLrowData == string.Empty)
                                sHTMLrowData = sHTMLcellData;
                            else
                                sHTMLrowData = sHTMLrowData + "|" + sHTMLcellData;

                            sHTMLcellData = string.Empty;
                        }
                    }//END of While
                    objReader.Close();
                }//End of IF

                //Print Totals
                if (bIsPrintTotals)
                {
                    m_structEOBRecord.TotalAmountBilled = dTotalAmountBilled;
                    m_structEOBRecord.TotalAmountPaid = dTotalAmountPaid;
                    m_structEOBRecord.TotalScheduledAmount = dTotSchdAmt;
                    m_structEOBRecord.TotalAmountReduced = dTotAmtReduced;
                    m_structEOBRecord.TotalAmountSaved = dTotAmtSaved;
                    m_structEOBRecord.TotalBaseAmount = dTotalBaseAmount;
                    m_structEOBRecord.TotalAmountAllowed = dTotalAmountAllowed;
                    m_structEOBRecord.TotalDiscountAmount = dTotalDiscountAmount;
                    m_structEOBRecord.TotalPerDiemAmt = dTotalPerDiemAmt;
                    m_structEOBRecord.TotalStopLossAmt = dTotalStopLossAmt;
                    m_structEOBRecord.TotalFeeTableAmt = dTotalFeeTableAmt;
                }
                return sHTMLrowData;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.PrintEOBHTMLTABLE", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objSQL != null)
                    objSQL = null;

                if (objReader != null)
                    objReader.Dispose();

                if (objTempReader != null)
                    objTempReader.Dispose();

                if (objFundsTransSplit != null)
                    objFundsTransSplit.Dispose();

                m_structEOBDetailRecord.EOBList.Clear();
                m_structEOBDetailRecord.EOBListDetails.Clear();
                m_structEOBDetailRecord.DiagnosisList.Clear();
                m_structEOBDetailRecord.ModifierList.Clear();
                m_structEOBDetailRecord.DiagnosisCodeList.Clear();

                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                    objDataModelFactory = null;
                }
            }
        }
        #endregion

        #endregion
        // akaushik5 Added for MITS 35846 Starts
        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (!object.ReferenceEquals(this.m_objDataModelFactory, null))
            {
                this.m_objDataModelFactory.Dispose();
                this.m_objDataModelFactory = null;
            }

            if (!object.ReferenceEquals(this.m_objLocalCache, null))
            {
                this.m_objLocalCache.Dispose();
                this.m_objLocalCache = null;
            }
        }

        #endregion
        // akaushik5 Added for MITS 35846 Ends
    }
}
