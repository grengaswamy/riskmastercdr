﻿
using System;
using System.Xml;
using System.Globalization;
using System.Resources;
using System.Collections;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using System.Text;
using System.IO; 
using Riskmaster.Application.ReportInterfaces;
using DataDynamics.ActiveReports.Export.Pdf; 
using Riskmaster.Settings ;
using C1.C1Pdf;//Debabrata Biswas MITS Issue 17183


namespace Riskmaster.Application.PrintEOB
{
	/// Name		: PrintedCheckDetail
	/// Author		: Vaibhav Kaushik
	/// Date Created: 01/05/2005		
	///************************************************************
	/// Amendment History
	///************************************************************
	/// Date Amended    *  Amendment   *    Author
	/// 08/03/2005		*  Moved this function from Printcheck to EOB.	* Vaibhav 
	///************************************************************
	/// <summary>
	/// PrintedCheckDetail Structure used For Printed check information. 
	/// </summary>
	public struct PrintedCheckDetail
	{
		/// <summary>
		/// Private variable for TransId.
		/// </summary>
		private int iTransId ;
		/// <summary>
		/// Private variable for CheckNum.
		/// </summary>
        //private int iCheckNum;
        private long lCheckNum;         //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
		/// <summary>
		/// Private variable for RollupId.
		/// </summary>
		private int iRollupId ;
        /// <summary>
        /// Private variable for TransDate.
        /// </summary>
        private string sTransDate;
		/// <summary>
		/// Read/Write property for TransId.
		/// </summary>
		public int TransId 
		{
			get
			{
				return( iTransId ) ;
			}
			set
			{
				iTransId = value ;
			}
		}
		/// <summary>
		/// Read/Write property for CheckNum.
		/// </summary>
		//public int CheckNum       
        public long CheckNum             //pmittal5 Mits 14500 02/24/09 - lCheckNum is of long type
		{
			get
			{
				return( lCheckNum ) ;
			}
			set
			{
				lCheckNum = value ;
			}
		}
		/// <summary>
		/// Read/Write property for RollupId.
		/// </summary>
		public int RollupId 
		{
			get
			{
				return( iRollupId ) ;
			}
			set
			{
				iRollupId = value ;
			}
		}
        /// <summary>
        /// Read/Write property for TransId.
        /// </summary>
        public string TransDate
        {
            get
            {
                return (sTransDate);
            }
            set
            {
                sTransDate = value;
            }
        }
	}


	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   22 December 2004
	///Purpose :   This class fetches the EOB details based on TransId passed and 
	///generate EOB report in the form of PDF doc.
	/// </summary>
	public class PrintEOBAR :IDisposable
	{
		#region "Constructor & Destructor"

		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
        public PrintEOBAR(string p_sUserName, string p_sPassword, string p_sDsnName, string p_sDsnId, int p_iClientId)
		{		
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			m_sDsnId  = p_sDsnId;
            m_iClientId = p_iClientId;//rkaur27
			this.Initialize();
			
		}		
		/// <summary>
		/// Destructor
		/// </summary>
		~PrintEOBAR()
		{			
            Dispose();	
		}
        private bool _isDisposed = false;	
        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
                // akaushik5 Added for MITS 35846 Starts
                if (this.m_objFunds != null)
                {
                    this.m_objFunds.Dispose();
                    this.m_objFunds = null;
                }

                if (this.m_objLocalCache != null)
                {
                    this.m_objLocalCache.Dispose();
                    this.m_objLocalCache = null;
                }

                if (this.m_objFunctions != null)
                {
                    this.m_objFunctions.Dispose();
                    this.m_objFunctions = null;
                }

                // akaushik5 Added for MITS 35846 Ends
                GC.SuppressFinalize(this);
            }
        }
		#endregion

		#region "Member Variables"
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = string.Empty;

		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = string.Empty;

		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = string.Empty;

		/// <summary>
		/// Private variable to store DSN Id
		/// </summary>
		private string m_sDsnId = string.Empty;

		/// <summary>
		/// Private variable to Database type
		/// </summary>
		private string m_sDBType = string.Empty;

		/// <summary>
		/// Private variable to store Security DSN Name
		/// </summary>
		private string m_sSecurityDsnName = string.Empty;

		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;

		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = string.Empty;

		/// <summary>
		/// Wrapper class object for PrintDocument component to generate PDF
		/// </summary>
		private PrintWrapper m_objPrintWrapper = null; 

		/// <summary>
		/// Private variable to store EOB detail record info
		/// </summary>
		private EOBDetailRecord m_structEOBDetailRecord;

		/// <summary>
		/// Private variable to store EOB record info
		/// </summary>
		private EOBRecord m_structEOBRecord;

		/// <summary>
		/// Stores the funds object
		/// </summary>
		private Funds m_objFunds = null;

		/// <summary>
		/// Stores the object of common functions class
		/// </summary>
		private Functions m_objFunctions = null;

		/// <summary>
		/// Instianate the Functions class object
		/// </summary>
		private Common m_objCommon = null;
        /// <summary>
        /// Private variable for Direct to Printer Flag
        /// </summary>
        public bool m_bDirectToPrinter = false;
		/// <summary>
		/// Constant char for '<'
		/// </summary>
		public const string PRINTEOBAR_PDFSAVEPATH = "PrintEOBAR_PdfSavePath";

        // akaushik5 Added for MITS 35846 Starts

        /// <summary>
        /// The m_obj local cache
        /// </summary>
        private LocalCache m_objLocalCache = null;

        /// <summary>
        /// The s dest folder path
        /// </summary>
        private string sDestFolderPath = string.Empty;

        /// <summary>
        /// The is dest folder exist
        /// </summary>
        private bool IsDestFolderExist = false;

        /// <summary>
        /// Gets the object local cache.
        /// </summary>
        /// <value>
        /// The object local cache.
        /// </value>
        private LocalCache ObjLocalCache
        {
            get
            {
                if (object.ReferenceEquals(this.m_objLocalCache, null))
                {
                    this.m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                }
                return this.m_objLocalCache;
            }
        }
        
        /// <summary>
        /// Gets the object funds.
        /// </summary>
        /// <value>
        /// The object funds.
        /// </value>
        private Funds ObjFunds
        {
            get
            {
                if (object.ReferenceEquals(this.m_objFunds, null))
                {
                    this.m_objFunds = this.m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds;
                }
                return m_objFunds;
            }
        }

        /// <summary>
        /// Gets the dest folder path.
        /// </summary>
        /// <value>
        /// The dest folder path.
        /// </value>
        private string DestFolderPath
        {
            get
            {
                if (string.IsNullOrEmpty(this.sDestFolderPath))
                {
                    this.sDestFolderPath = Functions.GetSavePath();
                }
                if (!this.IsDestFolderExist)
                {
                    if (!Directory.Exists(this.sDestFolderPath))
                    {
                        Directory.CreateDirectory(this.sDestFolderPath);
                    }

                    this.IsDestFolderExist = true;
                }
                return this.sDestFolderPath;
            }
        }

        /// <summary>
        /// The m_ is icn number exists
        /// </summary>
        private bool? m_IsICNNumberExists;

        /// <summary>
        /// Gets a value indicating whether [b is icn number exists].
        /// </summary>
        /// <value>
        /// <c>true</c> if [b is icn number exists]; otherwise, <c>false</c>.
        /// </value>
        private bool bIsICNNumberExists
        {
            get
            {
                if (!m_IsICNNumberExists.HasValue)
                {
                    string sSQL = "SELECT * FROM FUNDS_SUPP WHERE 1=0";
                    using (DbReader objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        this.m_IsICNNumberExists = objDbReader != null ? IsFieldExists(objDbReader, "ICN_TEXT") : false;
                    }
                }
                return this.m_IsICNNumberExists.Value;
            }
        }
        // akaushik5 Added for MITS 35846 Ends

        private int m_iClientId = 0;//rkaur27
		#endregion

		#region "Public Functions"

		#region "PostCheckEOB(int p_iAccountId, string p_sOrderByField, int p_iBatchNumber, + string p_sDate, out string p_sPDFSaveFilePaths) +1 overload"
		/// Name		: PostCheckEOB
		/// Author		: Anurag Agarwal
		/// Date Created	: 13 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Overloaded function for PostCheckEOB. Prints EOB to an Active Reports EOB
		/// </summary>
		/// <param name="p_iAccountId">Account ID</param>
		/// <param name="p_sOrderByField">Field on which Order by the records</param>
		/// <param name="p_iBatchNumber">Batch Number</param>
		/// <param name="p_sDate">Date</param>
		/// <param name="p_sPDFSaveFilePaths">Comma seperated saved PDF file names. The naming convention of files is TransId_State</param>
		// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
		public void PostCheckEOB(int p_iAccountId, string p_sOrderByField, int p_iBatchNumber, string p_sDate, int p_iDistributionType, out string p_sPDFSaveFilePaths)
		{
			string sSQL = string.Empty;
			int iTransID = 0;
			Funds objFunds = null;
			FundsTransSplit objFundsTransSplit = null;
			DbReader objDbReader = null;
			ArrayList arrFundsTransIDList = null;
			LocalCache objLocalCache = null;  
			bool bIsRecords = false;
            // akaushik5 Commented for MITS 35846 Starts
            //bool bIsICNNumberExists = false;
            // akaushik5 Commented for MITS 35846 Ends
			string sJurisdiction = string.Empty;
			int iCount = 0;
			string sClaimNumber = "" ;
			bool bIsMultiTransID = false;
			bool bIsMedical = false;
			bool bIsJustCheck = false; //boolean for Single Payment, Not Medical just print the check
			bool bFirstHealthPayment = false;
			string sFileName = string.Empty;
 
			try
			{
				p_sPDFSaveFilePaths = string.Empty;

				// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                sSQL = this.CheckPrintSQLPostCheck(p_iAccountId, p_sOrderByField, p_iBatchNumber, p_iDistributionType);
				
				objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
				arrFundsTransIDList = new ArrayList();
				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						if(iTransID != Conversion.ConvertStrToInteger(objDbReader["FUNDS_TRANS_ID"].ToString()))
							arrFundsTransIDList.Add(Conversion.ConvertStrToInteger(objDbReader["FUNDS_TRANS_ID"].ToString()));
	
						iTransID = Conversion.ConvertStrToInteger(objDbReader["FUNDS_TRANS_ID"].ToString());
						bIsRecords = true;
					}

					if(bIsRecords == false)
					{
						throw new RMAppException(Globalization.GetString("PrintEOBAR.EOBReport.NoChecks",m_iClientId) ); //sharishkumar Jira 835
					}

					objDbReader.Close(); 
				}

                // akaushik5 Commented for MITS 35846 Starts
                //sSQL = "SELECT * FROM FUNDS_SUPP WHERE 1=0";//Deb
                //objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                //if(objDbReader != null)
                //{
                //    if(IsFieldExists(objDbReader, "ICN_TEXT")) 
                //        bIsICNNumberExists = true;
                //    objDbReader.Close();
                //}
                // akaushik5 Commented for MITS 35846 Ends
				
				foreach(object objTransId in arrFundsTransIDList)
				{
					iTransID = Conversion.ConvertStrToInteger(objTransId.ToString());
					
					objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds",false);
					objFunds.MoveTo(iTransID);

					sClaimNumber = objFunds.ClaimNumber ;

					sSQL = "SELECT STATES.STATE_ID FROM CLAIM,STATES WHERE CLAIM_ID = " + objFunds.ClaimId +
						" AND FILING_STATE_ID = STATES.STATE_ROW_ID";
					objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
					if(objDbReader != null)
					{
						if(objDbReader.Read()) 
							sJurisdiction = objDbReader["STATE_ID"].ToString(); 
						objDbReader.Close();
					}

					if(bIsICNNumberExists)
					{
						sSQL = "SELECT COUNT(TRANS_NUMBER) FROM FUNDS WHERE FUNDS.TRANS_ID = " + 
							objFunds.TransId + " AND FUNDS.ACCOUNT_ID = " + p_iAccountId + 
							" AND FUNDS.BATCH_NUMBER = " + p_iBatchNumber;
						objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
						if(objDbReader != null)
						{
							if(objDbReader.Read())
								iCount = Conversion.ConvertStrToInteger(objDbReader[0].ToString()); 
							objDbReader.Close();
						}

						if(iCount > 1)
							bIsMultiTransID = true;
                        //Deb
                        //objFundsTransSplit = objFunds.TransSplitList[0];
                        foreach (FundsTransSplit objFundsTransSplitTemp in objFunds.TransSplitList)
                        {
                            objFundsTransSplit = objFundsTransSplitTemp;
                            break;
                        }
                        objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);
                        //Deb
						if(objLocalCache.GetCodeDesc(objFundsTransSplit.ReserveTypeCode) == "Medical")
						{
							bIsMedical = true;
							bIsJustCheck = false;
						}
						else
						{
							bIsMedical = false;
							if(!bIsMultiTransID && bIsICNNumberExists)
								bIsJustCheck = true;
						}
						
						//Is this a First Health Payment
						if(!bIsJustCheck)
						{
							if(m_objFunctions.IsSuppFieldExist(objFunds.Supplementals, "ICN_TEXT"))
							{
                                if (Conversion.ConvertObjToInt64(objFunds.Supplementals["ICN_TEXT"].Value, m_iClientId) > 0)
									bFirstHealthPayment = true;
								else
									bFirstHealthPayment = false;
							}
						}
						
						//Print appropriate reports
						if(bIsMedical && !bFirstHealthPayment)
                            // akaushik5 Changed for MITS 35846 Starts
							//this.PrintEOBReport(objFunds.TransId, true, "EOP",true, out sFileName);    
							this.PrintEOBReport(objFunds.TransId, true, "EOP",true, out sFileName, objFunds);
                        // akaushik5 Changed for MITS 35846 Ends
                        else if (bFirstHealthPayment)
						{
                            // akaushik5 Changed for MITS 35846 Starts
                            //this.PrintEOBReport(objFunds.TransId, true, "EOR", true, out sFileName);
                            this.PrintEOBReport(objFunds.TransId, true, "EOR", true, out sFileName, objFunds);
                            // akaushik5 Changed for MITS 35846 Ends
                            if (sJurisdiction == "MI" || sJurisdiction == "TX")
                                // akaushik5 Changed for MITS 35846 Starts
                                //this.PrintEOBReport(objFunds.TransId, true, sJurisdiction, true, out sFileName);
                                this.PrintEOBReport(objFunds.TransId, true, sJurisdiction, true, out sFileName, objFunds);
                            // akaushik5 Changed for MITS 35846 Ends
                        }
						else
						{
                            // akaushik5 Changed for MITS 35846 Starts
                            //this.PrintEOBReport(objFunds.TransId, true, sJurisdiction, true, out sFileName);
                            this.PrintEOBReport(objFunds.TransId, true, sJurisdiction, true, out sFileName, objFunds);
                            // akaushik5 Changed for MITS 35846 Ends
                        }
					}//END of if(bIsICNNumberExists)
					else
					{
                        // akaushik5 Changed for MITS 35846 Starts
                        //this.PrintEOBReport(objFunds.TransId, true, sJurisdiction, true, out sFileName);
                        this.PrintEOBReport(objFunds.TransId, true, sJurisdiction, true, out sFileName, objFunds);
                        // akaushik5 Changed for MITS 35846 Ends
                    }
					if(objFunds != null)
						objFunds.Dispose();
				
					if(sFileName != "")
					{
						if( p_sPDFSaveFilePaths != string.Empty)
							p_sPDFSaveFilePaths += "," ;
                        
						p_sPDFSaveFilePaths += sFileName + "|" + sClaimNumber ;						
					}
				}

			}	
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintEOBAR.PostCheckEOB.Error",m_iClientId) , p_objEx );				
			}
			finally
			{
				if(objLocalCache != null)
					objLocalCache.Dispose();

				if(objFundsTransSplit != null)
					objFundsTransSplit.Dispose();

				if(objFunds != null)
					objFunds.Dispose();

				if(arrFundsTransIDList != null)
				{
					arrFundsTransIDList.Clear();
					arrFundsTransIDList = null;
				}

				if(objDbReader != null)
					objDbReader.Dispose();
		
			}
		}
		#endregion

		#region "CheckEOB(int p_iTransId, bool p_bIsMultiTransId,string p_sDestFolderPath, out string p_sPDFSaveFilePath)"
		/// Name		: CheckEOB
		/// Author		: Anurag Agarwal
		/// Date Created	: 13 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Prints EOB to an Active Reports EOB
		/// </summary>
		/// <param name="p_iTransId">Funds Trans Id</param>
		/// <param name="p_bIsMultiTransId">Is the Trans Id's are multiple</param>
		/// <param name="p_sDestFolderPath">Destination folder Path for saving the PDF files</param>
		/// <param name="p_sPDFSaveFilePath">Saved PDF file name as output parameter. The naming convention of file will be TransID_StateId</param>
		public void CheckEOB(int p_iTransId, bool p_bIsMultiTransId,string p_sDestFolderPath, out string p_sPDFSaveFilePath, ArrayList p_arrChkPrtList)
		{
			string sSQL = string.Empty;

            string sClaimNumber = "";
            // akaushik5 Commented for MITS 35846 Starts
            //Funds objFunds = null;
            // akaushik5 Commented for MITS 35846 Ends
            FundsTransSplit objFundsTransSplit = null;
			DbReader objDbReader = null;
            // akaushik5 Commented for MITS 35846 Starts
            //LocalCache objLocalCache = null;  
            //bool bIsICNNumberExists = false;
            // akaushik5 Commented for MITS 35846 Ends
			string sJurisdiction = string.Empty;
			bool bIsMedical = false;
			bool bIsJustCheck = false; //boolean for Single Payment, Not Medical just print the check
			bool bFirstHealthPayment = false;

			p_sPDFSaveFilePath = string.Empty;

			try
			{
				if(p_iTransId == 0)
					return;

                // akaushik5 Changed for MITS 35846 Starts
                //objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                //objFunds.MoveTo(p_iTransId);

                //sSQL = "SELECT STATES.STATE_ID FROM CLAIM,STATES WHERE CLAIM_ID = " + objFunds.ClaimId +
                //    " AND FILING_STATE_ID = STATES.STATE_ROW_ID";
                //objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                //if (objDbReader != null)
                //{
                //    if (objDbReader.Read())
                //        sJurisdiction = objDbReader["STATE_ID"].ToString();
                //    objDbReader.Close();
                //}

                //sSQL = "SELECT * FROM FUNDS_SUPP WHERE 1=0";
                //objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                //if (objDbReader != null)
                //{
                //    if (IsFieldExists(objDbReader, "ICN_TEXT"))
                //        bIsICNNumberExists = true;
                //    objDbReader.Close();
                //}

                //sClaimNumber = objFunds.ClaimNumber;

                ObjFunds.MoveTo(p_iTransId);
                // akaushik5 Changed for MITS 35846 Ends

				if(bIsICNNumberExists)
				{
					//objFundsTransSplit = objFunds.TransSplitList[0]; 
                    foreach (FundsTransSplit objFundsTransSplitTemp in ObjFunds.TransSplitList)
                    {
                        objFundsTransSplit = objFundsTransSplitTemp;
                        break;
                    }
                    // akaushik5 Commented for MITS 35846 Starts
                    //objLocalCache = new LocalCache(m_sConnectionString);
                    // akaushik5 Commented for MITS 35846 Ends
                    if (ObjLocalCache.GetCodeDesc(objFundsTransSplit.ReserveTypeCode) == "Medical")
						bIsMedical = true;
					else
					{
						bIsMedical = false;
						if(!p_bIsMultiTransId && bIsICNNumberExists)
						{
							return;
						}
					}
						
					//Is this a First Health Payment
					if(!bIsJustCheck)
					{
                        if (m_objFunctions.IsSuppFieldExist(ObjFunds.Supplementals, "ICN_TEXT"))
						{
                            if (Conversion.ConvertObjToInt64(ObjFunds.Supplementals["ICN_TEXT"].Value, m_iClientId) > 0)
								bFirstHealthPayment = true;
							else
								bFirstHealthPayment = false;
						}
					}

                    // akaushik5 Added for MITS 35486 Starts
                    sJurisdiction = this.GetJurisdiction();
                    // akaushik5 Added for MITS 35486 Ends

					//Print appropriate reports
					if(bIsMedical && !bFirstHealthPayment)
                        this.PrintEOBReport(ObjFunds.TransId, true, "EOP", false, out p_sPDFSaveFilePath, p_arrChkPrtList, ObjFunds);
					else if(bFirstHealthPayment)
					{
                        this.PrintEOBReport(ObjFunds.TransId, true, "EOR", false, out p_sPDFSaveFilePath, p_arrChkPrtList, ObjFunds);
						if(sJurisdiction == "MI" || sJurisdiction == "TX")
                            this.PrintEOBReport(ObjFunds.TransId, true, sJurisdiction, false, out p_sPDFSaveFilePath, p_arrChkPrtList, ObjFunds);
					}
					else
					{
                        this.PrintEOBReport(ObjFunds.TransId, true, sJurisdiction, false, out p_sPDFSaveFilePath, p_arrChkPrtList, ObjFunds);
					}
				}//END of if(bIsICNNumberExists)
				else
				{
                    // akaushik5 Added for MITS 35486 Starts
                    sJurisdiction = this.GetJurisdiction();
                    // akaushik5 Added for MITS 35486 Ends
                    this.PrintEOBReport(ObjFunds.TransId, true, sJurisdiction, false, out p_sPDFSaveFilePath, p_arrChkPrtList, ObjFunds);
				}

                if (p_sPDFSaveFilePath != "")
                {
                    // akaushik5 Changed for MITS 35846 Starts
                    //p_sPDFSaveFilePath +="|" + sClaimNumber;
                    p_sPDFSaveFilePath += "|" + ObjFunds.ClaimNumber;
                    // akaushik5 Changed for MITS 35846 Ends
                }
			}	
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintEOBAR.CheckEOB.Error",m_iClientId) , p_objEx );				
			}
			finally
			{
                // akaushk5 Commented for MITS 35846 Starts
                //if(ObjLocalCache != null)
                //    ObjLocalCache.Dispose();
                // akaushk5 Commented for MITS 35846 Ends
				
				if(objFundsTransSplit != null)
					objFundsTransSplit.Dispose();

                // akaushk5 Commented for MITS 35846 Starts
                //if(objFunds != null)
                //    objFunds.Dispose();
                // akaushk5 Commented for MITS 35846 Ends

				if(objDbReader != null)
					objDbReader.Dispose();
			}
		}
		#endregion
        //akaushik5 Added for MITS 35846 Starts
        /// <summary>
        /// Gets the jurisdiction.
        /// </summary>
        /// <returns></returns>
        private string GetJurisdiction()
        {
            string sSQL = string.Format( "SELECT STATES.STATE_ID FROM CLAIM,STATES WHERE CLAIM_ID = {0} AND FILING_STATE_ID = STATES.STATE_ROW_ID", ObjFunds.ClaimId);
            string sJurisdiction = string.Empty;
            using (DbReader objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {
                sJurisdiction = !object.ReferenceEquals(objDbReader, null) && objDbReader.Read() ? objDbReader["STATE_ID"].ToString():string.Empty;
            }
            return sJurisdiction;
        }

        /// <summary>
        /// Prints the eob report.
        /// </summary>
        /// <param name="p_iTransId">The p_i trans identifier.</param>
        /// <param name="p_bIsSilent">if set to <c>true</c> [P_B is silent].</param>
        /// <param name="p_sState">State of the P_S.</param>
        /// <param name="p_bIsPostCheck">if set to <c>true</c> [P_B is post check].</param>
        /// <param name="p_sPDFSaveFilePath">The P_S PDF save file path.</param>
        public void PrintEOBReport(int p_iTransId, bool p_bIsSilent, string p_sState, bool p_bIsPostCheck, out string p_sPDFSaveFilePath)
        {
            using (Funds objFunds = this.m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds)
            {
                objFunds.MoveTo(p_iTransId);
                this.PrintEOBReport(p_iTransId, p_bIsSilent, p_sState, p_bIsPostCheck, out  p_sPDFSaveFilePath, objFunds);
            }
        }
        //akaushik5 Added for MITS 35846 Starts

		#region "PrintEOBReport(int p_iTransId, bool p_bIsSilent, string p_sState, bool p_bIsPostCheck, string p_sDestFolderPath, out string p_sPDFSaveFilePath, ArrayList p_arrChkPrtList) +2 Overloads"
		
		/// Name		: PrintEOBReport
		/// Author		: Anurag Agarwal
		/// Date Created	: 13 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Overloaded function for PrintEOBReport. Prints EOB to an Active Reports EOB
		/// </summary>
		/// <param name="p_iTransId">Funds TransId</param>
		/// <param name="p_bIsSilent">Flag for enabling/disabling the No Data found exceptions</param>
		/// <param name="p_sState">State Id</param>
		/// <param name="p_bIsPostCheck">Flag for Post Checks</param>
		/// <param name="p_sFileName">File Name</param>
		/// <param name="p_sDestFolderPath">Destination folder Path for saving the PDF files</param>
		/// <param name="p_sPDFSaveFilePath">Saved PDF file name as output parameter. The naming convention of file will be TransID_StateId</param>
        public void PrintEOBReport(int p_iTransId, bool p_bIsSilent, string p_sState, bool p_bIsPostCheck, out string p_sPDFSaveFilePath, Funds objFunds)
        {
            ArrayList arrTempList = null;
            PrintWrapper objPrintWrapper = null;
            CommonEOB objCommonEOB = null;
            PdfExport objPdfExport = null;
            string sDestFolderPath = "";
            RMConfigurator objConfig = null;

            try
            {
                //Debabrata Biswas
                //Implement Hook to call HTML EOB Templete depending upon the config settings.
                //Debabrata Biswas MITS Issue 17183
                //call HTML report template if Riskmaster.config has the EOBClaimHTMLFormatFile and EOBPayeeHTMLFormatFile nodes

                objConfig = new RMConfigurator();

                if (RMConfigurationManager.GetNameValueSectionSettings("PrintEOB", m_sConnectionString, m_iClientId)["EOBClaimHTMLFormatFile"] != null
                    && RMConfigurationManager.GetNameValueSectionSettings("PrintEOB", m_sConnectionString, m_iClientId)["EOBPayeeHTMLFormatFile"] != null)
                {
                    p_sPDFSaveFilePath = string.Empty;
                    sDestFolderPath = Functions.GetSavePath();

                    if (!Directory.Exists(sDestFolderPath))
                        Directory.CreateDirectory(sDestFolderPath);

                    //PrintEOBHTMLReport(p_iTransId, p_bIsSilent, p_sState, p_bIsPostCheck, arrTempList, out p_sPDFSaveFilePath, out objCommonEOB);
                    m_objFunctions.PrintEOBHTMLReport(p_iTransId, p_bIsSilent, p_sState, p_bIsPostCheck, arrTempList, out p_sPDFSaveFilePath, out objCommonEOB, objFunds);

                    if (objCommonEOB != null)
                    {
                        if (p_sPDFSaveFilePath == string.Empty)
                            p_sPDFSaveFilePath = sDestFolderPath + @"\" + p_iTransId + "_" + p_sState + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                        objPdfExport = new PdfExport();
                        objPdfExport.Export(objCommonEOB.Document, p_sPDFSaveFilePath);
                    }

                    //Deb UnderWriters
                    if (m_objFunctions.m_bDirectToPrinter == true && Functions.IsPrinterSelected(m_sConnectionString,m_iClientId) && !string.IsNullOrEmpty(p_sPDFSaveFilePath))
                    {
                        //m_objFunctions.PrintCheckJob(p_sPDFSaveFilePath);
                        m_objFunctions.PrintCheckJobNEW(p_sPDFSaveFilePath);
                    }
                    //Deb UnderWriters
                }
                else
                {
                    p_sPDFSaveFilePath = p_iTransId + "_" + p_sState + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                    sDestFolderPath = Functions.GetSavePath();

                    if (!Directory.Exists(sDestFolderPath))
                        Directory.CreateDirectory(sDestFolderPath);

                    p_sPDFSaveFilePath = sDestFolderPath + @"\" + p_sPDFSaveFilePath;

                    //PrintEOBReport(p_iTransId, p_bIsSilent, p_sState, p_bIsPostCheck, arrTempList, out objPrintWrapper, out objCommonEOB);
                    m_objFunctions.PrintEOBReport(p_iTransId, p_bIsSilent, p_sState, p_bIsPostCheck, arrTempList, out objPrintWrapper, out objCommonEOB);
                    if (objPrintWrapper != null || objCommonEOB != null)
                    {
                        if (objPrintWrapper != null)
                        {
                            objPrintWrapper.Save(p_sPDFSaveFilePath);
                        }
                        else
                        {
                            objPdfExport = new PdfExport();
                            objPdfExport.Export(objCommonEOB.Document, p_sPDFSaveFilePath);
                        }
                        if (m_objFunctions.m_bDirectToPrinter == true && (objPrintWrapper != null || objCommonEOB != null) && Functions.IsPrinterSelected(m_sConnectionString,m_iClientId))
                            //m_objFunctions.PrintCheckJob(p_sPDFSaveFilePath);
                            m_objFunctions.PrintCheckJobNEW(p_sPDFSaveFilePath);
                    }
                    else
                        p_sPDFSaveFilePath = string.Empty;
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAR.PrintEOBReport.ErrorCreate", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objPrintWrapper != null)
                    objPrintWrapper = null;

                if (objCommonEOB != null)
                    objCommonEOB.Dispose();

                if (objPdfExport != null)
                    objPdfExport.Dispose();

                objConfig = null;
            }
        }


		/// Name		: PrintEOBReport
		/// Author		: Anurag Agarwal
		/// Date Created	: 13 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Overloaded function for PrintEOBReport. Prints EOB to an Active Reports EOB
		/// </summary>
		/// <param name="p_iTransId">Funds TransId</param>
		/// <param name="p_bIsSilent">Flag for enabling/disabling the No Data found exceptions</param>
		/// <param name="p_sState">State Id</param>
		/// <param name="p_bIsPostCheck">Flag for Post Checks</param>
		/// <param name="p_sDestFolderPath">Destination folder Path for saving the PDF files</param>
		/// <param name="p_sPDFSaveFilePath">Saved PDF file name as output parameter. The naming convention of file will be TransID_StateId</param>
		/// <param name="p_arrChkPrtList">Array List containing the list of printed checks</param>
		private void PrintEOBReport(int p_iTransId, bool p_bIsSilent, string p_sState, bool p_bIsPostCheck, out string p_sPDFSaveFilePath, ArrayList p_arrChkPrtList, Funds objFunds)
		{
			PrintWrapper objPrintWrapper = null;
			CommonEOB objCommonEOB = null;
			PdfExport objPdfExport = null;
            // akaushik5 Commented for MITS 35846 Starts
            //string DestFolderPath = "";
            //RMConfigurator objConfig = null;
            // akaushik5 Commented for MITS 35846 Ends

			try
			{
                //Debabrata Biswas
                //Implement Hook to call HTML EOB Templete depending upon the config settings.
                //Debabrata Biswas MITS Issue 17183
                //call HTML report template if Riskmaster.config has the EOBClaimHTMLFormatFile and EOBPayeeHTMLFormatFile nodes

                // akaushik5 Commented for MITS 35846 Starts
                //objConfig = new RMConfigurator();
                // akaushik5 Commented for MITS 35846 Ends

                if (RMConfigurationManager.GetNameValueSectionSettings("PrintEOB", m_sConnectionString, m_iClientId)["EOBClaimHTMLFormatFile"] != null
                    && RMConfigurationManager.GetNameValueSectionSettings("PrintEOB", m_sConnectionString, m_iClientId)["EOBPayeeHTMLFormatFile"] != null)                    
                {
                    p_sPDFSaveFilePath = string.Empty;

                    // akaushik5 Commented for MITS 35846 Starts
                    //DestFolderPath = Functions.GetSavePath();
                    //if (!Directory.Exists(sDestFolderPath))
                    //    Directory.CreateDirectory(sDestFolderPath);
                    // akaushik5 Commented for MITS 35846 Ends

                    //PrintEOBHTMLReport(p_iTransId,p_bIsSilent,p_sState,p_bIsPostCheck,p_arrChkPrtList,out p_sPDFSaveFilePath,out objCommonEOB);
                    m_objFunctions.PrintEOBHTMLReport(p_iTransId, p_bIsSilent, p_sState, p_bIsPostCheck, p_arrChkPrtList, out p_sPDFSaveFilePath, out objCommonEOB, objFunds);
                    if (objCommonEOB != null)
                    {
                        if (p_sPDFSaveFilePath == string.Empty)
                            p_sPDFSaveFilePath = DestFolderPath + @"\" + p_iTransId + "_" + p_sState + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                        objPdfExport = new PdfExport();
                        objPdfExport.Export(objCommonEOB.Document, p_sPDFSaveFilePath);
                    }
                }
                else
                {
				    
				    p_sPDFSaveFilePath = p_iTransId + "_" + p_sState + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                    // akaushik5 Commented for MITS 35846 Starts
                    //DestFolderPath = Functions.GetSavePath(); ;
    				
                    //if( ! Directory.Exists( DestFolderPath ) )
                    //    Directory.CreateDirectory( DestFolderPath );
                    // akaushik5 Commented for MITS 35846 Ends

				    p_sPDFSaveFilePath = DestFolderPath + @"\" + p_sPDFSaveFilePath;                    
                    
                    //PrintEOBReport(p_iTransId,p_bIsSilent,p_sState,p_bIsPostCheck,p_arrChkPrtList,out objPrintWrapper,out objCommonEOB);
                    m_objFunctions.PrintEOBReport(p_iTransId, p_bIsSilent, p_sState, p_bIsPostCheck, p_arrChkPrtList, out objPrintWrapper, out objCommonEOB);
				    if(objPrintWrapper != null || objCommonEOB != null)
				    {
					    if(objPrintWrapper != null)
					    {
						    objPrintWrapper.Save(p_sPDFSaveFilePath);  						
					    }
					    else
					    {
						    objPdfExport = new PdfExport();
						    objPdfExport.Export(objCommonEOB.Document,p_sPDFSaveFilePath);   
					    }
				    }
				    else
					    p_sPDFSaveFilePath = string.Empty;
                    }
			}
            //Added for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            //Added for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintEOBAR.PrintEOBReport.ErrorCreate", m_iClientId), p_objEx);		//sharishkumar Jira 835		
			}
			finally
			{
				if(objPrintWrapper != null)
					objPrintWrapper = null;

				if(objCommonEOB != null)
					objCommonEOB.Dispose();

				if(objPdfExport != null)
					objPdfExport.Dispose();

                // akaushik5 Commented for MITS 35846 Starts
                //objConfig = null;
                // akaushik5 Commented for MITS 35846 Ends
            }
		}

        // akaushik5 Commented for MITS 35846 Starts
		/// Name		: PrintEOBReport
		/// Author		: Anurag Agarwal
		/// Date Created	: 13 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Prints EOB to an Active Reports EOB
		/// </summary>
		/// <param name="p_iTransId">Funds TransId</param>
		/// <param name="p_bIsSilent">Flag for enabling/disabling the No Data found exceptions</param>
		/// <param name="p_sState">State Id</param>
		/// <param name="p_bIsPostCheck">Flag for Post Checks</param>
		/// <param name="p_arrChkPrtList">Array List containing the list of printed checks</param>
		/// <param name="p_objPrintWrapper">Object of printWrapper class containing the PDF doc. 
		/// This will populated if report is generated through component one</param>
		/// <param name="p_objEOBReport">Object of Common EOB class containing the PDF doc. 
		/// This will populated if report is generated through Active Reports</param>
        //private void PrintEOBReport(int p_iTransId, bool p_bIsSilent, string p_sState, bool p_bIsPostCheck, ArrayList p_arrChkPrtList, out PrintWrapper p_objPrintWrapper, out CommonEOB p_objEOBReport)
        //{
        //    string sSQL = string.Empty;
        //    string sTempData = string.Empty;
        //    int iInvoiceId = 0;
        //    string sName = string.Empty;
        //    DbReader objReader = null;
        //    //akaushik5 Commented for MITS 35846 Starts
        //    //Funds objFunds = null;
        //    //akaushik5 Commented for MITS 35846 Ends
        //    BrsInvoice objBrsInvoice = null;
        //    Entity objEntity = null;
        //    CEOBRecord structCEOBRecord;
        //    Supplementals objSupplementals = null;
        //    // akaushik5 Commented for MITS 35846 Starts
        //    //LocalCache ObjLocalCache = null;
        //    // akaushik5 Commented for MITS 35846 Ends
        //    string strStateDesc = string.Empty;
        //    string strStateCode = string.Empty;
        //    string sJurisdiction = string.Empty;
        //    string sFileNumber = string.Empty;
        //    string sClaimantFileName = string.Empty;
        //    string sPayeeFileName = string.Empty;
        //    int iClaimantEid = 0;
        //    string sTransDateText = string.Empty;
        //    string sClaimNumberText = string.Empty;
        //    string sFirstName = string.Empty;
        //    string sLastName = string.Empty;
        //    string sClaimantName = string.Empty;
        //    string sRegClaimantName = string.Empty;
        //    string sClaimantAddr1 = string.Empty;
        //    string sClaimantAddr2 = string.Empty;
        //    string sClaimantCity = string.Empty;
        //    string sClaimantTaxId = string.Empty;
        //    bool bIsICNFieldPresent = false;
        //    string sFTSRowIDs = string.Empty;
        //    int iLineCount = 0;
        //    string sThisCode = string.Empty;
        //    string sLastCode = string.Empty;
        //    string sThisCodeID = string.Empty;
        //    string sLastCodeID = string.Empty;
        //    int iDivisionId = 0;
        //    PdfExport objPDF_AR = null;
        //    rptEOB objrptEOB = null;
        //    rptEOB_TX objrptEOB_TX = null;
        //    rptEOP objrptEOP = null;
        //    rptEOR objrptEOR = null;

        //    //Initializing the Output parameters
        //    p_objEOBReport = null;
        //    p_objPrintWrapper = null;

        //    try
        //    {
        //        structCEOBRecord = new CEOBRecord();
        //        // akaushik5 Commented for MITS 35846 Starts
        //        //ObjFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
        //        // akaushik5 Commented for MITS 35846 Ends
        //        try
        //        {
        //            ObjFunds.MoveTo(p_iTransId);
        //        }
        //        catch
        //        {
        //            if (!p_bIsSilent)
        //                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.SaveItem"));
        //            return;
        //        }

        //        sSQL = "SELECT STATES.STATE_ID, CLAIM.FILE_NUMBER  FROM CLAIM,STATES WHERE CLAIM_ID = " +
        //            ObjFunds.ClaimId + " AND FILING_STATE_ID = STATES.STATE_ROW_ID";
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                sJurisdiction = objReader["STATE_ID"].ToString();
        //                sFileNumber = objReader["FILE_NUMBER"].ToString();
        //            }
        //            objReader.Close();
        //        }

        //        if (p_sState.Trim() == string.Empty)
        //            p_sState = sJurisdiction;

        //        if (ObjFunds.Supplementals.Count() > 0)
        //        {
        //            objSupplementals = ObjFunds.Supplementals;
        //            if (Conversion.ConvertObjToInt(ObjFunds.Supplementals["TRANS_ID"], m_iClientId.Value) == p_iTransId)
        //            {
        //                if (IsSuppFieldExist(objSupplementals, "DCN_TEXT"))
        //                {
        //                    structCEOBRecord.DCN = objSupplementals["DCN_TEXT"].Value.ToString();
        //                    structCEOBRecord.OtherRef1 = objSupplementals["OTHER_REF_1_TEXT"].Value.ToString().Trim();
        //                    structCEOBRecord.OtherRef2 = objSupplementals["OTHER_REF_2_TEXT"].Value.ToString().Trim();
        //                    structCEOBRecord.ICNControlNumber = objSupplementals["ICN_TEXT"].Value.ToString().Trim();
        //                }
        //                else
        //                {
        //                    structCEOBRecord.DCN = string.Empty;
        //                    structCEOBRecord.OtherRef1 = string.Empty;
        //                    structCEOBRecord.OtherRef2 = string.Empty;
        //                    structCEOBRecord.ICNControlNumber = string.Empty;
        //                }
        //            }
        //            bIsICNFieldPresent = IsSuppFieldExist(objSupplementals, "ICN_TEXT");
        //            objSupplementals.Dispose();
        //        }

        //        if (bIsICNFieldPresent)
        //        {
        //            structCEOBRecord.ReasonCodes = string.Empty;
        //            foreach (FundsTransSplit objFundsTransSplit in ObjFunds.TransSplitList)
        //            {
        //                if (sFTSRowIDs == string.Empty)
        //                    sFTSRowIDs = objFundsTransSplit.SplitRowId.ToString();
        //                else
        //                    sFTSRowIDs += ", " + objFundsTransSplit.SplitRowId.ToString();
        //            }

        //            sSQL = "SELECT DISTINCT(INVDETAIL_EOR_CODE.EOR_CODE_ID), INVDETAIL_EOR_CODE.EOR_CODE, MESSAGE " +
        //                "FROM INVDETAIL_X_EOR, INVDETAIL_EOR_CODE WHERE INVDETAIL_X_EOR.EOR_CODE_ID = " +
        //                "INVDETAIL_EOR_CODE.EOR_CODE_ID AND INVDETAIL_X_EOR.TRANS_ID =" + ObjFunds.TransId +
        //                " AND INVDETAIL_X_EOR.FTS_ROW_ID IN (" + sFTSRowIDs + ")ORDER BY " +
        //                "INVDETAIL_EOR_CODE.EOR_CODE, INVDETAIL_EOR_CODE.EOR_CODE_ID";
        //            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

        //            if (objReader != null)
        //            {
        //                while (objReader.Read())
        //                {
        //                    sThisCode = objReader["EOR_CODE"].ToString();
        //                    if (sThisCode != sLastCode)
        //                    {
        //                        if (structCEOBRecord.ReasonCodes == string.Empty)
        //                        {
        //                            structCEOBRecord.ReasonCodes = iLineCount.ToString();
        //                            structCEOBRecord.CodeMessages = iLineCount + " - " + objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
        //                        }
        //                        else
        //                        {
        //                            structCEOBRecord.ReasonCodes += "," + iLineCount;
        //                            structCEOBRecord.CodeMessages += iLineCount + " - " + objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
        //                        }
        //                        iLineCount++;
        //                    }
        //                    else
        //                    {
        //                        if (structCEOBRecord.CodeMessages == string.Empty)
        //                            structCEOBRecord.CodeMessages = objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
        //                        else
        //                            structCEOBRecord.CodeMessages += objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
        //                    }
        //                    sLastCode = objReader["EOR_CODE"].ToString();
        //                }
        //                objReader.Close();
        //            }
        //        }//end fo If
        //        else
        //        {
        //            structCEOBRecord.ReasonCodes = string.Empty;
        //            structCEOBRecord.CodeMessages = string.Empty;
        //        }

        //        //Use first Trans Code
        //        foreach (FundsTransSplit objFundsTransSplit in ObjFunds.TransSplitList)
        //        {
        //            structCEOBRecord.PaymentCode = objFundsTransSplit.TransTypeCode.ToString();
        //            structCEOBRecord.Code = objFundsTransSplit.InvoiceNumber;
        //            structCEOBRecord.PeriodCovered = Conversion.GetDate(objFundsTransSplit.FromDate) + " - " +
        //                Conversion.GetDate(objFundsTransSplit.ToDate);
        //        }
        //        //Open up invoice record
        //        objBrsInvoice = (BrsInvoice)m_objDataModelFactory.GetDataModelObject("BrsInvoice", false);
        //        try
        //        {
        //            objBrsInvoice.MoveToTransId(p_iTransId);
        //        }
        //        catch
        //        {
        //            if (!p_bIsSilent)
        //                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems"));
        //            return;
        //        }

        //        // akaushik5 Commented for MITS 35846 Starts
        //        //ObjLocalCache = new LocalCache(m_sConnectionString);
        //        // akaushik5 Commented for MITS 35846 Ends

        //        iInvoiceId = objBrsInvoice.InvoiceId;
        //        if (iInvoiceId == 0)
        //            return;

        //        //Make sure we have detail items (exit if none)
        //        sSQL = "SELECT COUNT(*) FROM INVOICE_DETAIL WHERE INVOICE_ID = " + iInvoiceId;
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                if (Conversion.ConvertObjToInt(objReader[0]) == 0), m_iClientId
        //                {
        //                    if (!p_bIsSilent)
        //                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems"));
        //                    return;
        //                }
        //            }
        //            objReader.Close();
        //        }

        //        //... Payee Info
        //        sName = ((string)(objBrsInvoice.FirstNameText + " " + objBrsInvoice.LastNameText)).Trim();
        //        structCEOBRecord.PayeeFirstName = objBrsInvoice.FirstNameText;
        //        structCEOBRecord.PayeeLastName = objBrsInvoice.LastNameText;
        //        structCEOBRecord.PayeeFullName = sName;
        //        structCEOBRecord.PayeeAddr1 = objBrsInvoice.Addr1Text;
        //        structCEOBRecord.PayeeAddr2 = objBrsInvoice.Addr2Text;
        //        ObjLocalCache.GetStateInfo(objBrsInvoice.StateId, ref strStateCode, ref strStateDesc);
        //        structCEOBRecord.PayeeState = ((string)(strStateCode + " " + strStateDesc)).Trim();
        //        structCEOBRecord.PayeePostalCode = objBrsInvoice.PostalCodeText;
        //        structCEOBRecord.PayeeCity = objBrsInvoice.CityText;
        //        structCEOBRecord.ClaimNumber = objBrsInvoice.ClaimNumberText;
        //        // akaushik5 Changed for MITS 35846 Starts
        //        //structCEOBRecord.OrigInvDate = Conversion.GetDate(objBrsInvoice.DttmRcdAdded);
        //        structCEOBRecord.OrigInvDate = objBrsInvoice.DttmRcdAdded;
        //        // akaushik5 Changed for MITS 35846 Ends
        //        structCEOBRecord.OrigInvUser = objBrsInvoice.AddedByUser;
        //        iClaimantEid = objBrsInvoice.ClaimantEid;
        //        //Changed by Gagan for MITS 11520 : Start
        //        if (objBrsInvoice.TransDateText == "" || objBrsInvoice.TransDateText == null)
        //            sTransDateText = ObjFunds.TransDate;
        //        else
        //            sTransDateText = objBrsInvoice.TransDateText;
        //        structCEOBRecord.TransDate = sTransDateText;
        //        //sTransDateText = objBrsInvoice.TransDateText; 
        //        //Changed by Gagan for MITS 11520 : End
        //        sClaimNumberText = objBrsInvoice.ClaimNumberText;

        //        //... Payee Tax Id
        //        objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
        //        try
        //        {
        //            objEntity.MoveTo(ObjFunds.PayeeEid);
        //            structCEOBRecord.PayeeTaxID = objEntity.TaxId;
        //            structCEOBRecord.VendorNumber = objEntity.TaxId;
        //            //Changed by Gagan for MITS 11520 : Start
        //            structCEOBRecord.PayeeNPI = objEntity.NPINumber;
        //            //Changed by Gagan for MITS 11520 : End
        //        }
        //        catch { }

        //        try
        //        {
        //            objEntity.MoveTo(iClaimantEid);

        //            sFirstName = objEntity.FirstName;
        //            sLastName = objEntity.LastName;
        //            sClaimantName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
        //            sRegClaimantName = ((string)(sFirstName + " " + sLastName)).Trim();
        //            sClaimantAddr1 = objEntity.Addr1;
        //            sClaimantAddr2 = objEntity.Addr2;
        //            ObjLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
        //            if (strStateCode != string.Empty || strStateDesc != string.Empty)
        //                structCEOBRecord.ClaimantState = ((string)(strStateCode + " " + strStateDesc)).Trim().Substring(0, 4);
        //            structCEOBRecord.ClaimantCity = objEntity.City;
        //            structCEOBRecord.ClaimantPostalCode = objEntity.ZipCode;
        //            sClaimantCity = structCEOBRecord.ClaimantCity + ", " + structCEOBRecord.ClaimantState
        //                + " " + structCEOBRecord.ClaimantPostalCode;
        //            sClaimantTaxId = objEntity.TaxId;

        //            structCEOBRecord.ClaimantFullName = sClaimantName;
        //            structCEOBRecord.ClaimantLastName = sLastName;
        //            structCEOBRecord.ClaimantFirstName = sFirstName;
        //            structCEOBRecord.ClaimantAddr1 = sClaimantAddr1;
        //            structCEOBRecord.ClaimantAddr2 = sClaimantAddr2;
        //            structCEOBRecord.ClaimantSSN = sClaimantTaxId;
        //            if (sClaimantAddr1 != string.Empty)
        //                sTempData = sClaimantAddr1;
        //            if (sClaimantAddr2 != string.Empty)
        //                sTempData += Functions.CRLF + sClaimantAddr2;
        //            if (sClaimantCity != string.Empty)
        //                sTempData += Functions.CRLF + sClaimantCity;
        //            structCEOBRecord.ClaimantFullAddress = sTempData;
        //        }
        //        catch { }

        //        int iReportedEID = 0;

        //        //... Event Date Info
        //        sSQL = "SELECT DATE_OF_EVENT, DATE_REPORTED, RPTD_BY_EID FROM EVENT,CLAIM WHERE " +
        //            "CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + ObjFunds.ClaimId;
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                structCEOBRecord.EventDate = objReader["DATE_OF_EVENT"].ToString();
        //                structCEOBRecord.DateReported = objReader["DATE_REPORTED"].ToString();
        //                iReportedEID = Conversion.ConvertStrToInteger(objReader["RPTD_BY_EID"].ToString());
        //            }
        //            objReader.Close();
        //        }

        //        //... Reported By Name/Address
        //        try
        //        {
        //            objEntity.MoveTo(iReportedEID);
        //            sFirstName = objEntity.FirstName;
        //            sLastName = objEntity.LastName;
        //            structCEOBRecord.RptdFullName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
        //        }
        //        catch { }

        //        //... Employee department chain info
        //        sSQL = "SELECT ORG_HIERARCHY.* FROM EMPLOYEE,ORG_HIERARCHY WHERE EMPLOYEE.EMPLOYEE_EID=" +
        //            iClaimantEid + " AND ORG_HIERARCHY.DEPARTMENT_EID = EMPLOYEE.DEPT_ASSIGNED_EID";
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                ObjLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["DEPARTMENT_EID"].ToString()), ref structCEOBRecord.EmpDeptCode, ref structCEOBRecord.EmpDepartment);
        //                ObjLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["FACILITY_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpFacility);
        //                ObjLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["LOCATION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpLocation);
        //                ObjLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["DIVISION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpDivision);
        //                ObjLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["REGION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpRegion);
        //                ObjLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["COMPANY_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpCompany);
        //                ObjLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["CLIENT_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpClient);

        //                //Get the address of the facility
        //                iDivisionId = Conversion.ConvertStrToInteger(objReader["DIVISION_EID"].ToString());

        //                try
        //                {
        //                    objEntity.MoveTo(iDivisionId);
        //                    structCEOBRecord.DivisionName = objEntity.FirstName + objEntity.LastName;
        //                    structCEOBRecord.DivisionTaxID = objEntity.TaxId;
        //                    structCEOBRecord.EmpDivisionNAICS = ObjLocalCache.GetShortCode(objEntity.NaicsCode);
        //                    if (objEntity.Addr2 != string.Empty)
        //                        structCEOBRecord.DivisionNameAddr += structCEOBRecord.DivisionName + Functions.CRLF +
        //                            objEntity.Addr1 + Functions.CRLF + objEntity.Addr2;
        //                    else
        //                        structCEOBRecord.DivisionNameAddr += structCEOBRecord.DivisionName + Functions.CRLF +
        //                            objEntity.Addr1;

        //                    if (objEntity.City != string.Empty)
        //                    {
        //                        ObjLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
        //                        structCEOBRecord.DivisionNameAddr += Functions.CRLF + objEntity.City + ", " +
        //                            strStateCode + " " + strStateDesc + " " + objEntity.ZipCode;
        //                    }
        //                }
        //                catch (RecordNotFoundException p_objRecordNotFoundException)
        //                {
        //                    p_objRecordNotFoundException = null;
        //                }

        //                //Get the address of the facility
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["FACILITY_EID"].ToString()));
        //                    structCEOBRecord.EmpFacilityNAICS = ObjLocalCache.GetShortCode(objEntity.NaicsCode);
        //                    if (objEntity.Addr1 != string.Empty)
        //                        structCEOBRecord.EmpFacilityAddress = objEntity.Addr1;
        //                    if (objEntity.Addr2 != string.Empty)
        //                        structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.Addr2;
        //                    if (objEntity.City != string.Empty || objEntity.StateId != 0 || objEntity.ZipCode != string.Empty)
        //                    {
        //                        ObjLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
        //                        structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.City + ", " +
        //                            strStateCode + " " + strStateDesc + " " + objEntity.ZipCode;
        //                    }
        //                }
        //                catch (RecordNotFoundException p_objRecordNotFoundException)
        //                {
        //                    p_objRecordNotFoundException = null;
        //                }

        //                //... Get NAICS codes for all of the hierarchy levels
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["DEPARTMENT_EID"].ToString()));
        //                    structCEOBRecord.EmpDepartmentNAICS = ObjLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["LOCATION_EID"].ToString()));
        //                    structCEOBRecord.EmpLocationNAICS = ObjLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["REGION_EID"].ToString()));
        //                    structCEOBRecord.EmpRegionNAICS = ObjLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["OPERATION_EID"].ToString()));
        //                    structCEOBRecord.EmpOperationNAICS = ObjLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["COMPANY_EID"].ToString()));
        //                    structCEOBRecord.EmpCompanyNAICS = ObjLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["CLIENT_EID"].ToString()));
        //                    structCEOBRecord.EmpCompanyNAICS = ObjLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }

        //            }
        //            objReader.Close();
        //        }

        //        sSQL = "SELECT LAST_NAME,FIRST_NAME,ABBREVIATION,CURRENT_ADJ_FLAG,PHONE1 FROM ENTITY," +
        //            "CLAIM_ADJUSTER WHERE ADJUSTER_EID=ENTITY_ID AND CLAIM_ID=" + ObjFunds.ClaimId;
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            while (objReader.Read())
        //            {
        //                structCEOBRecord.AdjusterAbbrev = objReader["ABBREVIATION"].ToString();
        //                structCEOBRecord.AdjusterFullName = ((string)(objReader["FIRST_NAME"].ToString() + " " + objReader["LAST_NAME"].ToString())).Trim();
        //                structCEOBRecord.AdjusterLastName = objReader["LAST_NAME"].ToString();
        //                //Arnab: MITS-10722 - Getting Adjuster phone no
        //                structCEOBRecord.AdjusterPhone1 = Conversion.ConvertObjToStr(objReader["PHONE1"]);
        //                //MITS-10722 End
        //                if (Conversion.ConvertObjToInt(objReader["CURRENT_ADJ_FLAG"], m_iClientId) != 0)
        //                    break;
        //            }
        //            objReader.Close();
        //        }

        //        structCEOBRecord.BatchNumber = ObjFunds.BatchNumber.ToString();
        //        structCEOBRecord.CheckNumber = string.Empty;
        //        structCEOBRecord.PatientAccountNumber = string.Empty;
        //        structCEOBRecord.FeeOrCustomary = string.Empty;
        //        structCEOBRecord.ControlNumber = ObjFunds.CtlNumber;
        //        structCEOBRecord.ReceiptDate = string.Empty;

        //        if (p_bIsPostCheck)
        //            structCEOBRecord.CheckNumber = ObjFunds.TransNumber.ToString();
        //        else
        //        {
        //            if (p_arrChkPrtList != null)
        //            {
        //                foreach (PrintedCheckDetail structPrintedCheckDetail in p_arrChkPrtList)
        //                {
        //                    if (structPrintedCheckDetail.TransId == p_iTransId)
        //                        structCEOBRecord.CheckNumber = structPrintedCheckDetail.CheckNum.ToString();
        //                }
        //            }
        //        }

        //        m_structEOBRecord.CheckNumber = structCEOBRecord.CheckNumber;
        //        //Fetching EOB details
        //        GetEOBDetail(ObjFunds, ref structCEOBRecord, iInvoiceId);


        //        //Changed by Gagan for MITS 11520 : Start
        //        //SSN can have format as ###-##-#### or ##-#######
        //        if (structCEOBRecord.ClaimantSSN != "")
        //        {
        //            if (structCEOBRecord.ClaimantSSN.Length == 11)
        //                structCEOBRecord.ClaimantSSN = "XXX-XX-" + structCEOBRecord.ClaimantSSN.Substring(7, 4);
        //            else if (structCEOBRecord.ClaimantSSN.Length == 10)
        //                structCEOBRecord.ClaimantSSN = "XX-XXX" + structCEOBRecord.ClaimantSSN.Substring(6, 4);
        //        }
        //        //Changed by Gagan for MITS 11520 : End


        //        switch (p_sState)
        //        {
        //            case "MI":
        //                objrptEOB = new rptEOB(m_sConnectionString, m_objCommon.m_stLoginInfo.Username);
        //                objrptEOB.EOBReportData = structCEOBRecord;

        //                objrptEOB.Run();
        //                p_objEOBReport = objrptEOB;
        //                break;

        //            case "TX":
        //                objrptEOB_TX = new rptEOB_TX(m_sConnectionString, m_objCommon.m_stLoginInfo.Username);
        //                objrptEOB_TX.EOBReportData = structCEOBRecord;
        //                objrptEOB_TX.Run();
        //                p_objEOBReport = objrptEOB_TX;
        //                break;

        //            case "EOR":
        //                objrptEOR = new rptEOR(m_sConnectionString, m_objCommon.m_stLoginInfo.Username);
        //                objrptEOR.EOBReportData = structCEOBRecord;
        //                objrptEOR.Run();
        //                p_objEOBReport = objrptEOR;
        //                break;

        //            case "EOP":
        //                objrptEOP = new rptEOP(m_sConnectionString, m_objCommon.m_stLoginInfo.Username);
        //                objrptEOP.EOBReportData = structCEOBRecord;
        //                objrptEOP.Run();
        //                p_objEOBReport = objrptEOP;
        //                break;

        //            default:
        //                GetPrintCheckEOB(ObjFunds, true, out p_objPrintWrapper);
        //                break;
        //        }

        //        //Stamp audit info on invoice record
        //        sSQL = "UPDATE INVOICE SET DTTM_EOB_PRINTED = '" + Conversion.ToDbDateTime(DateTime.Now) + "', " +
        //            "EOB_PRINTED_USER = '" + m_objCommon.m_stLoginInfo.Username + "' WHERE INVOICE_ID = " + iInvoiceId;
        //        m_objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);

        //        ////TODO Logging component is in process to be finalized, As that will be finalized this will be done
        //        //MsgBox "Explanation Of Benefits (EOB) has been printed.", vbInformation + vbOKOnly, "Bill Review System"

        //    }
        //    catch (DataModelException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (RMAppException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBAR.PrintEOBReport.ErrorCreate"), p_objEx);
        //    }
        //    finally
        //    {
        //        if (objReader != null)
        //            objReader.Dispose();

        //        // akaushik5 Commented for MITS 35846 Starts 
        //        //if (ObjFunds != null)
        //        //    ObjFunds.Dispose();
        //        // akaushik5 Commented for MITS 35846 Ends

        //        if (objBrsInvoice != null)
        //            objBrsInvoice.Dispose();

        //        if (objEntity != null)
        //            objEntity.Dispose();

        //        if (objSupplementals != null)
        //            objSupplementals.Dispose();

        //        if (ObjLocalCache != null)
        //            ObjLocalCache.Dispose();

        //        if (objPDF_AR != null)
        //            objPDF_AR.Dispose();

        //        if (objrptEOB != null)
        //            objrptEOB.Dispose();

        //        if (objrptEOB_TX != null)
        //            objrptEOB_TX.Dispose();

        //        if (objrptEOP != null)
        //            objrptEOP.Dispose();

        //        if (objrptEOR != null)
        //            objrptEOR.Dispose();
        //    }
        //}
		/// Name		: PrintEOBHTMLReport
		/// Author		: Debabrata Biswas
		/// Date Created	: 07 Oct 2009		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Prints EOB to an Active Reports EOB
		/// </summary>
		/// <param name="p_iTransId">Funds TransId</param>
		/// <param name="p_bIsSilent">Flag for enabling/disabling the No Data found exceptions</param>
		/// <param name="p_sState">State Id</param>
		/// <param name="p_bIsPostCheck">Flag for Post Checks</param>
		/// <param name="p_arrChkPrtList">Array List containing the list of printed checks</param>
		/// <param name="p_objPrintWrapper">Object of printWrapper class containing the PDF doc. 
		/// This will populated if report is generated through component one</param>
		/// <param name="p_objEOBReport">Object of Common EOB class containing the PDF doc. 
		/// This will populated if report is generated through Active Reports</param>
        //private void PrintEOBHTMLReport(int p_iTransId, bool p_bIsSilent, string p_sState, bool p_bIsPostCheck, ArrayList p_arrChkPrtList, out string p_sPDFSaveFilePath, out CommonEOB p_objEOBReport)
        //{
        //    string sSQL = string.Empty;
        //    string sTempData = string.Empty;
        //    int iInvoiceId = 0;
        //    string sName = string.Empty;
        //    DbReader objReader = null;
        //    Funds objFunds = null;
        //    BrsInvoice objBrsInvoice = null;
        //    Entity objEntity = null;
        //    CEOBRecord structCEOBRecord;
        //    Supplementals objSupplementals = null;
        //    LocalCache objLocalCache = null;
        //    string strStateDesc = string.Empty;
        //    string strStateCode = string.Empty;
        //    string sJurisdiction = string.Empty;
        //    string sFileNumber = string.Empty;
        //    string sClaimantFileName = string.Empty;
        //    string sPayeeFileName = string.Empty;
        //    int iClaimantEid = 0;
        //    string sTransDateText = string.Empty;
        //    string sClaimNumberText = string.Empty;
        //    string sFirstName = string.Empty;
        //    string sLastName = string.Empty;
        //    string sClaimantName = string.Empty;
        //    string sRegClaimantName = string.Empty;
        //    string sClaimantAddr1 = string.Empty;
        //    string sClaimantAddr2 = string.Empty;
        //    string sClaimantCity = string.Empty;
        //    string sClaimantTaxId = string.Empty;
        //    bool bIsICNFieldPresent = false;
        //    string sFTSRowIDs = string.Empty;
        //    int iLineCount = 0;
        //    string sThisCode = string.Empty;
        //    string sLastCode = string.Empty;
        //    string sThisCodeID = string.Empty;
        //    string sLastCodeID = string.Empty;
        //    int iDivisionId = 0;
        //    PdfExport objPDF_AR = null;
        //    rptEOB objrptEOB = null;
        //    rptEOB_TX objrptEOB_TX = null;
        //    rptEOP objrptEOP = null;
        //    rptEOR objrptEOR = null;

        //    p_sPDFSaveFilePath = "";
        //    //Initializing the Output parameters
        //    p_objEOBReport = null;

        //    try
        //    {
        //        structCEOBRecord = new CEOBRecord();

        //        objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
        //        try
        //        {
        //            objFunds.MoveTo(p_iTransId);
        //        }
        //        catch
        //        {
        //            if (!p_bIsSilent)
        //                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.SaveItem"));
        //            return;
        //        }

        //        sSQL = "SELECT STATES.STATE_ID, CLAIM.FILE_NUMBER  FROM CLAIM,STATES WHERE CLAIM_ID = " +
        //            objFunds.ClaimId + " AND FILING_STATE_ID = STATES.STATE_ROW_ID";
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                sJurisdiction = objReader["STATE_ID"].ToString();
        //                sFileNumber = objReader["FILE_NUMBER"].ToString();
        //            }
        //            objReader.Close();
        //        }

        //        if (p_sState.Trim() == string.Empty)
        //            p_sState = sJurisdiction;

        //        if (objFunds.Supplementals.Count() > 0)
        //        {
        //            objSupplementals = objFunds.Supplementals;
        //            if (Conversion.ConvertObjToInt(objFunds.Supplementals["TRANS_ID"], m_iClientId.Value) == p_iTransId)
        //            {
        //                if (m_objFunctions.IsSuppFieldExist(objSupplementals, "DCN_TEXT"))
        //                {
        //                    structCEOBRecord.DCN = objSupplementals["DCN_TEXT"].Value.ToString();
        //                    structCEOBRecord.OtherRef1 = objSupplementals["OTHER_REF_1_TEXT"].Value.ToString().Trim();
        //                    structCEOBRecord.OtherRef2 = objSupplementals["OTHER_REF_2_TEXT"].Value.ToString().Trim();
        //                    structCEOBRecord.ICNControlNumber = objSupplementals["ICN_TEXT"].Value.ToString().Trim();
        //                }
        //                else
        //                {
        //                    structCEOBRecord.DCN = string.Empty;
        //                    structCEOBRecord.OtherRef1 = string.Empty;
        //                    structCEOBRecord.OtherRef2 = string.Empty;
        //                    structCEOBRecord.ICNControlNumber = string.Empty;
        //                }
        //            }
        //            bIsICNFieldPresent = m_objFunctions.IsSuppFieldExist(objSupplementals, "ICN_TEXT");
        //            objSupplementals.Dispose();
        //        }

        //        if (bIsICNFieldPresent)
        //        {
        //            structCEOBRecord.ReasonCodes = string.Empty;
        //            foreach (FundsTransSplit objFundsTransSplit in objFunds.TransSplitList)
        //            {
        //                if (sFTSRowIDs == string.Empty)
        //                    sFTSRowIDs = objFundsTransSplit.SplitRowId.ToString();
        //                else
        //                    sFTSRowIDs += ", " + objFundsTransSplit.SplitRowId.ToString();
        //            }

        //            sSQL = "SELECT DISTINCT(INVDETAIL_EOR_CODE.EOR_CODE_ID), INVDETAIL_EOR_CODE.EOR_CODE, MESSAGE " +
        //                "FROM INVDETAIL_X_EOR, INVDETAIL_EOR_CODE WHERE INVDETAIL_X_EOR.EOR_CODE_ID = " +
        //                "INVDETAIL_EOR_CODE.EOR_CODE_ID AND INVDETAIL_X_EOR.TRANS_ID =" + objFunds.TransId +
        //                " AND INVDETAIL_X_EOR.FTS_ROW_ID IN (" + sFTSRowIDs + ")ORDER BY " +
        //                "INVDETAIL_EOR_CODE.EOR_CODE, INVDETAIL_EOR_CODE.EOR_CODE_ID";
        //            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

        //            if (objReader != null)
        //            {
        //                while (objReader.Read())
        //                {
        //                    sThisCode = objReader["EOR_CODE"].ToString();
        //                    if (sThisCode != sLastCode)
        //                    {
        //                        if (structCEOBRecord.ReasonCodes == string.Empty)
        //                        {
        //                            structCEOBRecord.ReasonCodes = iLineCount.ToString();
        //                            structCEOBRecord.CodeMessages = iLineCount + " - " + objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
        //                        }
        //                        else
        //                        {
        //                            structCEOBRecord.ReasonCodes += "," + iLineCount;
        //                            structCEOBRecord.CodeMessages += iLineCount + " - " + objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
        //                        }
        //                        iLineCount++;
        //                    }
        //                    else
        //                    {
        //                        if (structCEOBRecord.CodeMessages == string.Empty)
        //                            structCEOBRecord.CodeMessages = objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
        //                        else
        //                            structCEOBRecord.CodeMessages += objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
        //                    }
        //                    sLastCode = objReader["EOR_CODE"].ToString();
        //                }
        //                objReader.Close();
        //            }
        //        }//end fo If
        //        else
        //        {
        //            structCEOBRecord.ReasonCodes = string.Empty;
        //            structCEOBRecord.CodeMessages = string.Empty;
        //        }

        //        //Use first Trans Code
        //        foreach (FundsTransSplit objFundsTransSplit in objFunds.TransSplitList)
        //        {
        //            structCEOBRecord.PaymentCode = objFundsTransSplit.TransTypeCode.ToString();
        //            structCEOBRecord.Code = objFundsTransSplit.InvoiceNumber;
        //            structCEOBRecord.PeriodCovered = Conversion.GetDate(objFundsTransSplit.FromDate) + " - " +
        //                Conversion.GetDate(objFundsTransSplit.ToDate);
        //        }
        //        //Open up invoice record
        //        objBrsInvoice = (BrsInvoice)m_objDataModelFactory.GetDataModelObject("BrsInvoice", false);
        //        try
        //        {
        //            objBrsInvoice.MoveToTransId(p_iTransId);
        //        }
        //        catch
        //        {
        //            if (!p_bIsSilent)
        //                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems"));
        //            return;
        //        }

        //        objLocalCache = new LocalCache(m_sConnectionString);

        //        iInvoiceId = objBrsInvoice.InvoiceId;
        //        if (iInvoiceId == 0)
        //            return;

        //        //Make sure we have detail items (exit if none)
        //        sSQL = "SELECT COUNT(*) FROM INVOICE_DETAIL WHERE INVOICE_ID = " + iInvoiceId;
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                if (Conversion.ConvertObjToInt(objReader[0]) == 0), m_iClientId
        //                {
        //                    if (!p_bIsSilent)
        //                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems"));
        //                    return;
        //                }
        //            }
        //            objReader.Close();
        //        }

        //        //... Payee Info
        //        sName = ((string)(objBrsInvoice.FirstNameText + " " + objBrsInvoice.LastNameText)).Trim();
        //        structCEOBRecord.PayeeFirstName = objBrsInvoice.FirstNameText;
        //        structCEOBRecord.PayeeLastName = objBrsInvoice.LastNameText;
        //        structCEOBRecord.PayeeFullName = sName;
        //        structCEOBRecord.PayeeAddr1 = objBrsInvoice.Addr1Text;
        //        structCEOBRecord.PayeeAddr2 = objBrsInvoice.Addr2Text;
        //        objLocalCache.GetStateInfo(objBrsInvoice.StateId, ref strStateCode, ref strStateDesc);
        //        structCEOBRecord.PayeeState = ((string)(strStateCode + " " + strStateDesc)).Trim();
        //        structCEOBRecord.PayeePostalCode = objBrsInvoice.PostalCodeText;
        //        structCEOBRecord.PayeeCity = objBrsInvoice.CityText;
        //        structCEOBRecord.ClaimNumber = objBrsInvoice.ClaimNumberText;
        //        // akaushik5 Changed for MITS 35846 Starts
        //        //structCEOBRecord.OrigInvDate = Conversion.GetDate(objBrsInvoice.DttmRcdAdded);
        //        structCEOBRecord.OrigInvDate = objBrsInvoice.DttmRcdAdded;
        //        // akaushik5 Changed for MITS 35846 Ends
        //        structCEOBRecord.OrigInvUser = objBrsInvoice.AddedByUser;
        //        iClaimantEid = objBrsInvoice.ClaimantEid;
        //        //Changed by Gagan for MITS 11520 : Start
        //        if (objBrsInvoice.TransDateText == "" || objBrsInvoice.TransDateText == null)
        //            sTransDateText = objFunds.TransDate;
        //        else
        //            sTransDateText = objBrsInvoice.TransDateText;
        //        structCEOBRecord.TransDate = sTransDateText;
        //        //sTransDateText = objBrsInvoice.TransDateText; 
        //        //Changed by Gagan for MITS 11520 : End
        //        sClaimNumberText = objBrsInvoice.ClaimNumberText;

        //        //... Payee Tax Id
        //        objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
        //        try
        //        {
        //            objEntity.MoveTo(objFunds.PayeeEid);
        //            structCEOBRecord.PayeeTaxID = objEntity.TaxId;
        //            structCEOBRecord.VendorNumber = objEntity.TaxId;
        //            //Changed by Gagan for MITS 11520 : Start
        //            structCEOBRecord.PayeeNPI = objEntity.NPINumber;
        //            //Changed by Gagan for MITS 11520 : End
        //        }
        //        catch { }

        //        try
        //        {
        //            objEntity.MoveTo(iClaimantEid);

        //            sFirstName = objEntity.FirstName;
        //            sLastName = objEntity.LastName;
        //            sClaimantName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
        //            sRegClaimantName = ((string)(sFirstName + " " + sLastName)).Trim();
        //            sClaimantAddr1 = objEntity.Addr1;
        //            sClaimantAddr2 = objEntity.Addr2;
        //            objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
        //            if (strStateCode != string.Empty || strStateDesc != string.Empty)
        //                structCEOBRecord.ClaimantState = ((string)(strStateCode + " " + strStateDesc)).Trim().Substring(0, 4);
        //            structCEOBRecord.ClaimantCity = objEntity.City;
        //            structCEOBRecord.ClaimantPostalCode = objEntity.ZipCode;
        //            sClaimantCity = structCEOBRecord.ClaimantCity + ", " + structCEOBRecord.ClaimantState
        //                + " " + structCEOBRecord.ClaimantPostalCode;
        //            sClaimantTaxId = objEntity.TaxId;

        //            structCEOBRecord.ClaimantFullName = sClaimantName;
        //            structCEOBRecord.ClaimantLastName = sLastName;
        //            structCEOBRecord.ClaimantFirstName = sFirstName;
        //            structCEOBRecord.ClaimantAddr1 = sClaimantAddr1;
        //            structCEOBRecord.ClaimantAddr2 = sClaimantAddr2;
        //            structCEOBRecord.ClaimantSSN = sClaimantTaxId;
        //            if (sClaimantAddr1 != string.Empty)
        //                sTempData = sClaimantAddr1;
        //            if (sClaimantAddr2 != string.Empty)
        //                sTempData += Functions.CRLF + sClaimantAddr2;
        //            if (sClaimantCity != string.Empty)
        //                sTempData += Functions.CRLF + sClaimantCity;
        //            structCEOBRecord.ClaimantFullAddress = sTempData;
        //        }
        //        catch { }

        //        int iReportedEID = 0;

        //        //... Event Date Info
        //        sSQL = "SELECT DATE_OF_EVENT, DATE_REPORTED, RPTD_BY_EID FROM EVENT,CLAIM WHERE " +
        //            "CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + objFunds.ClaimId;
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                structCEOBRecord.EventDate = objReader["DATE_OF_EVENT"].ToString();
        //                structCEOBRecord.DateReported = objReader["DATE_REPORTED"].ToString();
        //                iReportedEID = Conversion.ConvertStrToInteger(objReader["RPTD_BY_EID"].ToString());
        //            }
        //            objReader.Close();
        //        }

        //        //... Reported By Name/Address
        //        try
        //        {
        //            objEntity.MoveTo(iReportedEID);
        //            sFirstName = objEntity.FirstName;
        //            sLastName = objEntity.LastName;
        //            structCEOBRecord.RptdFullName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
        //        }
        //        catch { }

        //        //... Employee department chain info
        //        sSQL = "SELECT ORG_HIERARCHY.* FROM EMPLOYEE,ORG_HIERARCHY WHERE EMPLOYEE.EMPLOYEE_EID=" +
        //            iClaimantEid + " AND ORG_HIERARCHY.DEPARTMENT_EID = EMPLOYEE.DEPT_ASSIGNED_EID";
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["DEPARTMENT_EID"].ToString()), ref structCEOBRecord.EmpDeptCode, ref structCEOBRecord.EmpDepartment);
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["FACILITY_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpFacility);
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["LOCATION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpLocation);
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["DIVISION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpDivision);
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["REGION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpRegion);
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["COMPANY_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpCompany);
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["CLIENT_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpClient);

        //                //Get the address of the facility
        //                iDivisionId = Conversion.ConvertStrToInteger(objReader["DIVISION_EID"].ToString());

        //                try
        //                {
        //                    objEntity.MoveTo(iDivisionId);
        //                    structCEOBRecord.DivisionName = objEntity.FirstName + objEntity.LastName;
        //                    structCEOBRecord.DivisionTaxID = objEntity.TaxId;
        //                    structCEOBRecord.EmpDivisionNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                    if (objEntity.Addr2 != string.Empty)
        //                        structCEOBRecord.DivisionNameAddr += structCEOBRecord.DivisionName + Functions.CRLF +
        //                            objEntity.Addr1 + Functions.CRLF + objEntity.Addr2;
        //                    else
        //                        structCEOBRecord.DivisionNameAddr += structCEOBRecord.DivisionName + Functions.CRLF +
        //                            objEntity.Addr1;

        //                    if (objEntity.City != string.Empty)
        //                    {
        //                        objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
        //                        structCEOBRecord.DivisionNameAddr += Functions.CRLF + objEntity.City + ", " +
        //                            strStateCode + " " + strStateDesc + " " + objEntity.ZipCode;
        //                    }
        //                }
        //                catch (RecordNotFoundException p_objRecordNotFoundException)
        //                {
        //                    p_objRecordNotFoundException = null;
        //                }

        //                //Get the address of the facility
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["FACILITY_EID"].ToString()));
        //                    structCEOBRecord.EmpFacilityNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                    if (objEntity.Addr1 != string.Empty)
        //                        structCEOBRecord.EmpFacilityAddress = objEntity.Addr1;
        //                    if (objEntity.Addr2 != string.Empty)
        //                        structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.Addr2;
        //                    if (objEntity.City != string.Empty || objEntity.StateId != 0 || objEntity.ZipCode != string.Empty)
        //                    {
        //                        objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
        //                        structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.City + ", " +
        //                            strStateCode + " " + strStateDesc + " " + objEntity.ZipCode;
        //                    }
        //                }
        //                catch (RecordNotFoundException p_objRecordNotFoundException)
        //                {
        //                    p_objRecordNotFoundException = null;
        //                }

        //                //... Get NAICS codes for all of the hierarchy levels
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["DEPARTMENT_EID"].ToString()));
        //                    structCEOBRecord.EmpDepartmentNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["LOCATION_EID"].ToString()));
        //                    structCEOBRecord.EmpLocationNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["REGION_EID"].ToString()));
        //                    structCEOBRecord.EmpRegionNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["OPERATION_EID"].ToString()));
        //                    structCEOBRecord.EmpOperationNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["COMPANY_EID"].ToString()));
        //                    structCEOBRecord.EmpCompanyNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["CLIENT_EID"].ToString()));
        //                    structCEOBRecord.EmpCompanyNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }

        //            }
        //            objReader.Close();
        //        }

        //        sSQL = "SELECT LAST_NAME,FIRST_NAME,ABBREVIATION,CURRENT_ADJ_FLAG,PHONE1 FROM ENTITY," +
        //            "CLAIM_ADJUSTER WHERE ADJUSTER_EID=ENTITY_ID AND CLAIM_ID=" + objFunds.ClaimId;
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            while (objReader.Read())
        //            {
        //                structCEOBRecord.AdjusterAbbrev = objReader["ABBREVIATION"].ToString();
        //                structCEOBRecord.AdjusterFullName = ((string)(objReader["FIRST_NAME"].ToString() + " " + objReader["LAST_NAME"].ToString())).Trim();
        //                structCEOBRecord.AdjusterLastName = objReader["LAST_NAME"].ToString();
        //                //Arnab: MITS-10722 - Getting Adjuster phone no
        //                structCEOBRecord.AdjusterPhone1 = Conversion.ConvertObjToStr(objReader["PHONE1"]);
        //                //MITS-10722 End
        //                if (Conversion.ConvertObjToInt(objReader["CURRENT_ADJ_FLAG"], m_iClientId) != 0)
        //                    break;
        //            }
        //            objReader.Close();
        //        }

        //        structCEOBRecord.BatchNumber = objFunds.BatchNumber.ToString();
        //        structCEOBRecord.CheckNumber = string.Empty;
        //        structCEOBRecord.PatientAccountNumber = string.Empty;
        //        structCEOBRecord.FeeOrCustomary = string.Empty;
        //        structCEOBRecord.ControlNumber = objFunds.CtlNumber;
        //        structCEOBRecord.ReceiptDate = string.Empty;

        //        if (p_bIsPostCheck)
        //            structCEOBRecord.CheckNumber = objFunds.TransNumber.ToString();
        //        else
        //        {
        //            if (p_arrChkPrtList != null)
        //            {
        //                foreach (PrintedCheckDetail structPrintedCheckDetail in p_arrChkPrtList)
        //                {
        //                    if (structPrintedCheckDetail.TransId == p_iTransId)
        //                        structCEOBRecord.CheckNumber = structPrintedCheckDetail.CheckNum.ToString();
        //                }
        //            }
        //        }

        //        m_structEOBRecord.CheckNumber = structCEOBRecord.CheckNumber;
        //        //Fetching EOB details
        //        GetEOBDetail(objFunds, ref structCEOBRecord, iInvoiceId);


        //        //Changed by Gagan for MITS 11520 : Start
        //        //SSN can have format as ###-##-#### or ##-#######
        //        if (structCEOBRecord.ClaimantSSN != "")
        //        {
        //            if (structCEOBRecord.ClaimantSSN.Length == 11)
        //                structCEOBRecord.ClaimantSSN = "XXX-XX-" + structCEOBRecord.ClaimantSSN.Substring(7, 4);
        //            else if (structCEOBRecord.ClaimantSSN.Length == 10)
        //                structCEOBRecord.ClaimantSSN = "XX-XXX" + structCEOBRecord.ClaimantSSN.Substring(6, 4);
        //        }
        //        //Changed by Gagan for MITS 11520 : End


        //        switch (p_sState)
        //        {
        //            case "MI":
        //                objrptEOB = new rptEOB(m_sConnectionString, m_objCommon.m_stLoginInfo.Username);
        //                objrptEOB.EOBReportData = structCEOBRecord;

        //                objrptEOB.Run();
        //                p_objEOBReport = objrptEOB;
        //                break;

        //            case "TX":
        //                objrptEOB_TX = new rptEOB_TX(m_sConnectionString, m_objCommon.m_stLoginInfo.Username);
        //                objrptEOB_TX.EOBReportData = structCEOBRecord;
        //                objrptEOB_TX.Run();
        //                p_objEOBReport = objrptEOB_TX;
        //                break;

        //            case "EOR":
        //                objrptEOR = new rptEOR(m_sConnectionString, m_objCommon.m_stLoginInfo.Username);
        //                objrptEOR.EOBReportData = structCEOBRecord;
        //                objrptEOR.Run();
        //                p_objEOBReport = objrptEOR;
        //                break;

        //            case "EOP":
        //                objrptEOP = new rptEOP(m_sConnectionString, m_objCommon.m_stLoginInfo.Username);
        //                objrptEOP.EOBReportData = structCEOBRecord;
        //                objrptEOP.Run();
        //                p_objEOBReport = objrptEOP;
        //                break;

        //            default:
        //                p_sPDFSaveFilePath = "";
        //                GetPrintCheckHTMLEOB(objFunds, true, p_sState, out p_sPDFSaveFilePath);
        //                break;
        //        }

        //        //Stamp audit info on invoice record
        //        sSQL = "UPDATE INVOICE SET DTTM_EOB_PRINTED = '" + Conversion.ToDbDateTime(DateTime.Now) + "', " +
        //            "EOB_PRINTED_USER = '" + m_objCommon.m_stLoginInfo.Username + "' WHERE INVOICE_ID = " + iInvoiceId;
        //        m_objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);

        //        ////TODO Logging component is in process to be finalized, As that will be finalized this will be done
        //        //MsgBox "Explanation Of Benefits (EOB) has been printed.", vbInformation + vbOKOnly, "Bill Review System"

        //    }
        //    catch (DataModelException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (RMAppException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBAR.PrintEOBReport.ErrorCreate"), p_objEx);
        //    }
        //    finally
        //    {
        //        if (objReader != null)
        //            objReader.Dispose();

        //        if (objFunds != null)
        //            objFunds.Dispose();

        //        if (objBrsInvoice != null)
        //            objBrsInvoice.Dispose();

        //        if (objEntity != null)
        //            objEntity.Dispose();

        //        if (objSupplementals != null)
        //            objSupplementals.Dispose();

        //        if (objLocalCache != null)
        //            objLocalCache.Dispose();

        //        if (objPDF_AR != null)
        //            objPDF_AR.Dispose();

        //        if (objrptEOB != null)
        //            objrptEOB.Dispose();

        //        if (objrptEOB_TX != null)
        //            objrptEOB_TX.Dispose();

        //        if (objrptEOP != null)
        //            objrptEOP.Dispose();

        //        if (objrptEOR != null)
        //            objrptEOR.Dispose();
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends

		#endregion

		#endregion

		#region "Private Functions"

		#region "Initialize"
		/// Name		: Initialize
		/// Author		: Anurag Agarwal
		/// Date Created	: 23 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Initialize the DataModal factory class object and Connection string;
		/// </summary>
		private void Initialize()
		{
			try
			{
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);	//sharishkumar Jira 835
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
				m_sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() ;

				m_objCommon = new Common();

				m_objCommon.m_stLoginInfo.Username = m_sUserName;
				m_objCommon.m_stLoginInfo.DsnName = m_sDsnName;
				m_objCommon.m_stLoginInfo.DsnId = m_sDsnId;

				m_objPrintWrapper = new PrintWrapper(m_iClientId);
				m_structEOBRecord = new EOBRecord();
				m_structEOBDetailRecord = new EOBDetailRecord();
	
				m_objFunctions = new Functions(m_sDsnId, m_sUserName,m_sConnectionString,m_sPassword,m_sDsnName, m_iClientId);//rkaur27
                m_bDirectToPrinter = m_objFunctions.m_bDirectToPrinter;
			}	
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintEOBAR.Initialize.ErrorInit", m_iClientId), p_objEx);//sharishkumar Jira 835				
			}			
		}	
		#endregion

		#region "GetPrintCheckEOB(Funds p_objFunds, bool p_bIsSilent, out PrintWrapper p_objPrintWrapper)"
        // akaushik5 Commented for MITS 35846 Starts
        /// Name		: GetPrintCheckEOB
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// THis function is called to print EOB report, It uses format text file for reading report format details
		/// Format File is the Custom files of dml (dorn mark-up lang) tags <b> (like  xml/http).
		/// If a file cannot be found, a Default, internal layout format is used.
		/// </summary>
		/// <param name="p_objFunds">Funds Object</param>
		/// <param name="p_bIsSilent">Flag for enebling/disabling No Data found messages</param>
		/// <param name="p_objPrintWrapper">Output parameter as object of Prinwrapper class. This will contain the PDF doc generated</param>
        //private void GetPrintCheckEOB(Funds p_objFunds, bool p_bIsSilent, out PrintWrapper p_objPrintWrapper)
        //{
        //    string sSQL = string.Empty;
        //    int iTransId = 0;
        //    int iDeptAssignEId = 0;
        //    string sTempData = string.Empty;
        //    double dAvgTextHeight = 0.0;
        //    double dAvgPrintWidth = 0.0;
        //    int iInvoiceId = 0;
        //    string sName = string.Empty;
        //    DbReader objReader = null;
        //    BrsInvoice objBrsInvoice = null;
        //    Claim objClaim = null;
        //    State objState = null;
        //    DataSet objDataSet = null;
        //    Entity objEntity = null;
        //    Employee objEmployee = null;
        //    StreamReader objFormatFileStream = null;
        //    LocalCache objLocalCache = null;
        //    string strStateDesc = string.Empty;
        //    string strStateCode = string.Empty;
        //    string sJurisdiction = string.Empty;
        //    int iStateId = 0;
        //    string sClaimantFileName = string.Empty;
        //    string sPayeeFileName = string.Empty;
        //    bool bIsFormatFileFound = false;
        //    int iClaimantEid = 0;
        //    string sTransDateText = string.Empty;
        //    string sClaimNumberText = string.Empty;
        //    string sFirstName = string.Empty;
        //    string sLastName = string.Empty;
        //    string sClaimantName = string.Empty;
        //    string sRegClaimantName = string.Empty;
        //    string sClaimantAddr1 = string.Empty;
        //    string sClaimantAddr2 = string.Empty;
        //    string sClaimantCity = string.Empty;
        //    string sClaimantTaxId = string.Empty;
        //    string sAddr1 = string.Empty;
        //    string sAddr2 = string.Empty;
        //    string sCity = string.Empty;

        //    try
        //    {

        //        //initialize the out parameter
        //        p_objPrintWrapper = null;

        //        m_objPrintWrapper.StartDoc(false);

        //        dAvgTextHeight = m_objPrintWrapper.GetTextHeight("W");
        //        dAvgPrintWidth = m_objPrintWrapper.PageWidth;

        //        m_objFunds = p_objFunds;
        //        iTransId = m_objFunds.TransId;
        //        if (iTransId == 0)
        //        {
        //            if (!p_bIsSilent)
        //                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.SaveItem"));
        //            return;
        //        }

        //        objBrsInvoice = (BrsInvoice)m_objDataModelFactory.GetDataModelObject("BrsInvoice", false);
        //        try
        //        {
        //            objBrsInvoice.MoveToTransId(iTransId);
        //        }
        //        catch
        //        {
        //            if (!p_bIsSilent)
        //                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems"));
        //            return;
        //        }

        //        iInvoiceId = objBrsInvoice.InvoiceId;
        //        sName = objBrsInvoice.FirstNameText + " " + objBrsInvoice.LastNameText;

        //        sSQL = "SELECT FL_LICENSE,ZIP_CODE FROM INVOICE_DETAIL WHERE INVOICE_ID=" + iInvoiceId;
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                m_structEOBRecord.FirstLicense = objReader["FL_LICENSE"].ToString();
        //                m_structEOBRecord.FirstBillingPostalCode = objReader["ZIP_CODE"].ToString();
        //            }

        //        }
        //        if (objReader != null)
        //            objReader.Close();

        //        sSQL = "SELECT INVOICE_NUMBER FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID=" + iTransId;
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                m_structEOBRecord.FirstInvoice = objReader["INVOICE_NUMBER"].ToString();
        //            }

        //        }
        //        if (objReader != null)
        //            objReader.Close();
        //        sSQL = null;

        //        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);

        //        // Vaibhav has changed the code for printing EOb reports for those patyments which are not 
        //        // attached to Claims.
        //        if (m_objFunds.ClaimId > 0)
        //            objClaim.MoveTo(m_objFunds.ClaimId);

        //        iStateId = objClaim.FilingStateId;
        //        objClaim.Dispose();

        //        objState = (State)m_objDataModelFactory.GetDataModelObject("State", false);
        //        if (iStateId > 0)
        //            objState.MoveTo(iStateId);
        //        sJurisdiction = objState.StateId;
        //        objState.Dispose();

        //        if (m_objFunctions.m_bPrintClaimantEOB)
        //        {
        //            sClaimantFileName = Functions.GetFormatFilePath()
        //                + @"\" + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMFILE);
        //            if ((sJurisdiction == string.Empty) || !File.Exists(sClaimantFileName))
        //                sClaimantFileName = Functions.GetFormatFilePath()
        //                    + @"\" + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMFILE);
        //        }

        //        if (m_objFunctions.m_bPrintPayeeEOB)
        //        {
        //            sPayeeFileName = Functions.GetFormatFilePath()
        //                + @"\" + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEFILE);
        //            if ((sJurisdiction == string.Empty) || !File.Exists(sPayeeFileName))
        //                sPayeeFileName = Functions.GetFormatFilePath()
        //                    + @"\" + Functions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEFILE);
        //        }

        //        bIsFormatFileFound = true;
        //        if (!File.Exists(sClaimantFileName) && m_objFunctions.m_bPrintClaimantEOB)
        //            bIsFormatFileFound = false;

        //        if (bIsFormatFileFound)
        //            if (!File.Exists(sPayeeFileName) && m_objFunctions.m_bPrintPayeeEOB)
        //                bIsFormatFileFound = false;

        //        objLocalCache = new LocalCache(m_sConnectionString);

        //        if (bIsFormatFileFound)
        //        {
        //            m_structEOBRecord.CurrentX1Margin = 0;
        //            m_structEOBRecord.CurrentX2Margin = 0;
        //            m_structEOBRecord.CurrentY1Margin = 0;
        //            m_structEOBRecord.CurrentY2Margin = 0;
        //            m_structEOBRecord.CurrentAlign = Functions.ALIGN_LEFT;
        //            m_structEOBRecord.PayeeFirstName = objBrsInvoice.FirstNameText;
        //            m_structEOBRecord.PayeeLastName = objBrsInvoice.LastNameText;
        //            m_structEOBRecord.PayeeFullName = sName.Trim();
        //            m_structEOBRecord.PayeeAddr1 = objBrsInvoice.Addr1Text;
        //            m_structEOBRecord.PayeeAddr2 = objBrsInvoice.Addr2Text;
        //            objLocalCache.GetStateInfo(objBrsInvoice.StateId, ref strStateCode, ref strStateDesc);
        //            m_structEOBRecord.PayeeState = ((string)(strStateCode + " " + strStateDesc)).Trim();
        //            m_structEOBRecord.PayeePostalCode = objBrsInvoice.PostalCodeText;
        //            m_structEOBRecord.PayeeCity = objBrsInvoice.CityText;
        //            m_structEOBRecord.ClaimNumber = objBrsInvoice.ClaimNumberText;
        //            m_structEOBRecord.OrigInvDate = Conversion.GetDate(objBrsInvoice.DttmRcdAdded);
        //            m_structEOBRecord.OrigInvUser = objBrsInvoice.AddedByUser;
        //        }
        //        iClaimantEid = objBrsInvoice.ClaimantEid;
        //        sTransDateText = objBrsInvoice.TransDateText;
        //        sClaimNumberText = objBrsInvoice.ClaimNumberText;
        //        sAddr1 = objBrsInvoice.Addr1Text;
        //        sAddr2 = objBrsInvoice.Addr2Text;
        //        sCity = objBrsInvoice.CityText;

        //        objBrsInvoice.Dispose();

        //        objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);

        //        if (bIsFormatFileFound)
        //        {
        //            try
        //            {
        //                objEntity.MoveTo(m_objFunds.PayeeEid);
        //                m_structEOBRecord.PayeeTaxID = objEntity.TaxId;
        //            }
        //            catch
        //            {
        //                m_structEOBRecord.PayeeTaxID = string.Empty;
        //            }

        //            objEmployee = (Employee)m_objDataModelFactory.GetDataModelObject("Employee", false);
        //            try
        //            {
        //                objEmployee.MoveTo(iClaimantEid);
        //                iDeptAssignEId = objEmployee.DeptAssignedEid;
        //                objEntity.MoveTo(iDeptAssignEId);
        //                m_structEOBRecord.EmpDeptCode = objEntity.Abbreviation;
        //                m_structEOBRecord.EmpDepartment = objEntity.LastName;
        //            }
        //            catch
        //            {
        //                m_structEOBRecord.EmpDeptCode = string.Empty;
        //                m_structEOBRecord.EmpDepartment = string.Empty;
        //            }
        //            objEmployee.Dispose();
        //            objEntity.Dispose();

        //            sSQL = "SELECT DATE_OF_EVENT, DATE_REPORTED, RPTD_BY_EID FROM EVENT,CLAIM WHERE " +
        //                "CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + m_objFunds.ClaimId;
        //            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //            if (objReader != null)
        //            {
        //                if (objReader.Read())
        //                {
        //                    m_structEOBRecord.EventDate = objReader["DATE_OF_EVENT"].ToString();
        //                    m_structEOBRecord.DateReported = objReader["DATE_REPORTED"].ToString();
        //                }
        //                objReader.Close();
        //            }

        //            sSQL = "SELECT ENTITY_ID,LAST_NAME,FACILITY_EID,LOCATION_EID,DIVISION_EID,REGION_EID,OPERATION_EID" +
        //                ",COMPANY_EID,CLIENT_EID FROM ENTITY, ORG_HIERARCHY WHERE DEPARTMENT_EID=" + iDeptAssignEId;
        //            objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL);
        //            if (objDataSet != null && objDataSet.Tables.Count > 0)
        //            {
        //                foreach (DataRow objDataRow in objDataSet.Tables[0].Select("FACILITY_EID=ENTITY_ID"))
        //                    m_structEOBRecord.EmpFacility = objDataRow["LAST_NAME"].ToString();

        //                foreach (DataRow objDataRow in objDataSet.Tables[0].Select("LOCATION_EID=ENTITY_ID"))
        //                    m_structEOBRecord.EmpLocation = objDataRow["LAST_NAME"].ToString();

        //                foreach (DataRow objDataRow in objDataSet.Tables[0].Select("DIVISION_EID=ENTITY_ID"))
        //                    m_structEOBRecord.EmpDivision = objDataRow["LAST_NAME"].ToString();

        //                foreach (DataRow objDataRow in objDataSet.Tables[0].Select("REGION_EID=ENTITY_ID"))
        //                    m_structEOBRecord.EmpRegion = objDataRow["LAST_NAME"].ToString();

        //                foreach (DataRow objDataRow in objDataSet.Tables[0].Select("OPERATION_EID=ENTITY_ID"))
        //                    m_structEOBRecord.EmpOperation = objDataRow["LAST_NAME"].ToString();

        //                foreach (DataRow objDataRow in objDataSet.Tables[0].Select("COMPANY_EID=ENTITY_ID"))
        //                    m_structEOBRecord.EmpCompany = objDataRow["LAST_NAME"].ToString();

        //                foreach (DataRow objDataRow in objDataSet.Tables[0].Select("CLIENT_EID=ENTITY_ID"))
        //                    m_structEOBRecord.EmpClient = objDataRow["LAST_NAME"].ToString();
        //            }
        //            if (objDataSet != null)
        //                objDataSet.Dispose();

        //            sSQL = "SELECT LAST_NAME,FIRST_NAME,ABBREVIATION,CURRENT_ADJ_FLAG,PHONE1 FROM ENTITY," +
        //                "CLAIM_ADJUSTER WHERE ADJUSTER_EID=ENTITY_ID AND CLAIM_ID=" + m_objFunds.ClaimId;
        //            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //            if (objReader != null)
        //            {
        //                while (objReader.Read())
        //                {
        //                    m_structEOBRecord.AdjusterAbbrev = objReader["ABBREVIATION"].ToString();
        //                    m_structEOBRecord.AdjusterFullName = ((string)(objReader["FIRST_NAME"].ToString() + " " + objReader["LAST_NAME"].ToString())).Trim();
        //                    m_structEOBRecord.AdjusterLastName = objReader["LAST_NAME"].ToString();
        //                    //Arnab: MITS-10722 - Getting Adjuster phone no
        //                    m_structEOBRecord.AdjusterPhone1 = Conversion.ConvertObjToStr(objReader["PHONE1"]);
        //                    //MITS-10722 End
        //                    if (Conversion.ConvertObjToInt(objReader["CURRENT_ADJ_FLAG"], m_iClientId) != 0)
        //                        break;
        //                }
        //                objReader.Close();
        //            }
        //        }
        //        //Make sure we have detail items (exit if none)
        //        sSQL = "SELECT COUNT(*) FROM INVOICE_DETAIL WHERE INVOICE_ID = " + iInvoiceId;
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                if (Conversion.ConvertObjToInt(objReader[0]) == 0), m_iClientId
        //                {
        //                    if (!p_bIsSilent)
        //                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems"));
        //                    return;
        //                }
        //            }
        //            objReader.Close();
        //        }

        //        //Lookup Claimant Name/Address info
        //        try
        //        {
        //            objEntity.MoveTo(iClaimantEid);

        //            sFirstName = objEntity.FirstName;
        //            sLastName = objEntity.LastName;
        //            sClaimantName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
        //            sRegClaimantName = ((string)(sFirstName + " " + sLastName)).Trim();
        //            sClaimantAddr1 = objEntity.Addr1;
        //            sClaimantAddr2 = objEntity.Addr2;
        //            objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
        //            m_structEOBRecord.ClaimantState = ((string)(strStateCode + " " + strStateDesc)).Trim();
        //            m_structEOBRecord.ClaimantCity = objEntity.City;
        //            m_structEOBRecord.ClaimantPostalCode = objEntity.ZipCode;
        //            sClaimantCity = m_structEOBRecord.ClaimantCity + ", " + m_structEOBRecord.ClaimantState
        //                + " " + m_structEOBRecord.ClaimantPostalCode;
        //            sClaimantTaxId = objEntity.TaxId;

        //            if (bIsFormatFileFound)
        //            {
        //                m_structEOBRecord.ClaimantFullName = sClaimantName;
        //                m_structEOBRecord.ClaimantLastName = sLastName;
        //                m_structEOBRecord.ClaimantFirstName = sFirstName;
        //                m_structEOBRecord.ClaimantAddr1 = sClaimantAddr1;
        //                m_structEOBRecord.ClaimantAddr2 = sClaimantAddr2;
        //                m_structEOBRecord.TransDate = sTransDateText;
        //                m_structEOBRecord.ClaimantSSN = sClaimantTaxId;
        //            }
        //        }
        //        catch
        //        {
        //            if (bIsFormatFileFound)
        //            {
        //                m_structEOBRecord.ClaimantFullName = string.Empty;
        //                m_structEOBRecord.ClaimantLastName = string.Empty;
        //                m_structEOBRecord.ClaimantFirstName = string.Empty;
        //                m_structEOBRecord.ClaimantAddr1 = string.Empty;
        //                m_structEOBRecord.ClaimantAddr2 = string.Empty;
        //                m_structEOBRecord.TransDate = string.Empty;
        //                m_structEOBRecord.ClaimantSSN = string.Empty;
        //            }
        //        }
        //        objEntity.Dispose();

        //        //abisht
        //        if (m_objFunctions.m_bPrintPayeeEOB || (m_objFunctions.m_bPrintClaimantEOB && iClaimantEid != 0))
        //        {
        //            sTempData = m_objPrintWrapper.FontName;
        //            m_objPrintWrapper.SetFont("Courier New");
        //        }

        //        //This prints the default EOB Form
        //        if (!bIsFormatFileFound)
        //        {
        //            if (m_objFunctions.m_bPrintPayeeEOB)
        //            {
        //                this.GetDefaultEOBHeader(sRegClaimantName, sClaimantName, sClaimantAddr1, sClaimantAddr2
        //                    , sClaimantCity, sName, sClaimNumberText, false);
        //                this.GetDefaultEOBDetail(iInvoiceId, sName, sClaimantName, false);
        //            }
        //            //Claimant Copy
        //            if (m_objFunctions.m_bPrintClaimantEOB)
        //            {
        //                if (m_objFunctions.m_bPrintPayeeEOB)
        //                    m_objPrintWrapper.NewPage();
        //                this.GetDefaultEOBHeader(sRegClaimantName, sClaimantName, sClaimantAddr1, sClaimantAddr2
        //                    , sClaimantCity, sName, sClaimNumberText, true);
        //                this.GetDefaultEOBDetail(iInvoiceId, sName, sClaimantName, true);
        //            }
        //        }
        //        else
        //        {
        //            if (m_objFunctions.m_bPrintClaimantEOB)
        //            {
        //                //Print Custom File
        //                m_structEOBRecord.FirstPrint = true;
        //                objFormatFileStream = File.OpenText(sClaimantFileName);

        //                //Custom
        //                m_objFunctions.GetCustomEOBHeader(objFormatFileStream, ref m_structEOBRecord, m_objPrintWrapper, m_objFunds);

        //                //Print body of letter
        //                m_objFunctions.GetColumns(objFormatFileStream, ref m_structEOBDetailRecord);
        //                this.GetPrintCheckEOBBody(iInvoiceId, sClaimNumberText, sClaimantName, sName, string.Empty, bIsFormatFileFound, 0, 0);

        //                //*** Custom footer ***
        //                m_objFunctions.GetCustomEOBHeader(objFormatFileStream, ref m_structEOBRecord, m_objPrintWrapper, m_objFunds);

        //                objFormatFileStream.Close();

        //            }

        //            if (m_objFunctions.m_bPrintPayeeEOB)
        //            {
        //                if (m_objFunctions.m_bPrintClaimantEOB)
        //                    m_objPrintWrapper.NewPage();
        //                m_structEOBRecord.FirstPrint = true;
        //                objFormatFileStream = File.OpenText(sPayeeFileName);

        //                //Custom
        //                m_objFunctions.GetCustomEOBHeader(objFormatFileStream, ref m_structEOBRecord, m_objPrintWrapper, m_objFunds);

        //                //Print body of letter
        //                m_objFunctions.GetColumns(objFormatFileStream, ref m_structEOBDetailRecord);
        //                this.GetPrintCheckEOBBody(iInvoiceId, sClaimNumberText, sClaimantName, sName, string.Empty, bIsFormatFileFound, 0, 0);

        //                //*** Custom footer ***
        //                m_objFunctions.GetCustomEOBHeader(objFormatFileStream, ref m_structEOBRecord, m_objPrintWrapper, m_objFunds);

        //                objFormatFileStream.Close();
        //            }
        //        }

        //        m_objPrintWrapper.EndDoc();
        //        //abisht
        //        if (m_objFunctions.m_bPrintPayeeEOB || m_objFunctions.m_bPrintClaimantEOB)
        //            p_objPrintWrapper = m_objPrintWrapper;

        //        if (sTempData != string.Empty)
        //            m_objPrintWrapper.SetFont(sTempData);

        //        //Stamp audit info on invoice record
        //        sSQL = "UPDATE INVOICE SET DTTM_EOB_PRINTED = '" + Conversion.ToDbDateTime(DateTime.Now) + "', " +
        //            "EOB_PRINTED_USER = '" + m_objCommon.m_stLoginInfo.Username + "' WHERE INVOICE_ID = " + iInvoiceId;
        //        m_objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);

        //        m_objFunctions.m_objColumnsSelected.Clear();
        //        m_objFunctions.m_objSpaceWidth.Clear();
        //    }
        //    catch (DataModelException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (RMAppException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBAR.GetPrintCheckEOB.ErrorGet"), p_objEx);
        //    }
        //    finally
        //    {
        //        if (objClaim != null)
        //            objClaim.Dispose();

        //        if (objState != null)
        //            objState.Dispose();

        //        if (objDataSet != null)
        //            objDataSet.Dispose();

        //        if (objEmployee != null)
        //            objEmployee.Dispose();

        //        if (objReader != null)
        //            objReader.Dispose();

        //        if (objBrsInvoice != null)
        //            objBrsInvoice.Dispose();

        //        if (objEntity != null)
        //            objEntity.Dispose();

        //        if (objFormatFileStream != null)
        //            objFormatFileStream = null;

        //        if (objLocalCache != null)
        //            objLocalCache.Dispose();
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends
        #endregion


        #region "GetPrintCheckHTMLEOB(Funds p_objFunds, bool p_bIsSilent, out PrintWrapper p_objPrintWrapper)"
        // akaushik5 Commented for MITS 35846 Starts
        /// Name		: GetPrintCheckHTMLEOB
        /// Author		: Debabrata Biswas
        /// Date Created	: 06 Oct 2009		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// THis function is called to print EOB report, It uses XHTML format file for reading report format details
        /// Format File is the XHTML Custom files tags <b> (like  xml/http).
        /// If a file cannot be found, a Default, internal layout format is used.
        /// </summary>
        /// <param name="p_objFunds">Funds Object</param>
        /// <param name="p_bIsSilent">Flag for enebling/disabling No Data found messages</param>
        /// <param name="p_objPrintWrapper">Output parameter as object of Prinwrapper class. This will contain the PDF doc generated</param>
        //private void GetPrintCheckHTMLEOB(Funds p_objFunds, bool p_bIsSilent, string p_sState, out string p_sPDFSaveFilePath)
        //{
        //    {
        //        string sSQL = string.Empty;
        //        int iTransId = 0;
        //        int iDeptAssignEId = 0;
        //        string sTempData = string.Empty;
        //        int iInvoiceId = 0;
        //        string sName = string.Empty;
        //        DbReader objReader = null;
        //        BrsInvoice objBrsInvoice = null;
        //        Claim objClaim = null;
        //        State objState = null;
        //        DataSet objDataSet = null;
        //        Entity objEntity = null;
        //        Employee objEmployee = null;
        //        StreamReader objFormatFileStream = null;
        //        LocalCache objLocalCache = null;
        //        string strStateDesc = string.Empty;
        //        string strStateCode = string.Empty;
        //        string sJurisdiction = string.Empty;
        //        int iStateId = 0;
        //        string sClaimantFileName = string.Empty;
        //        string sPayeeFileName = string.Empty;
        //        bool bIsFormatFileFound = false;
        //        int iClaimantEid = 0;
        //        string sTransDateText = string.Empty;
        //        string sClaimNumberText = string.Empty;
        //        string sFirstName = string.Empty;
        //        string sLastName = string.Empty;
        //        string sClaimantName = string.Empty;
        //        string sRegClaimantName = string.Empty;
        //        string sClaimantAddr1 = string.Empty;
        //        string sClaimantAddr2 = string.Empty;
        //        string sClaimantCity = string.Empty;
        //        string sClaimantTaxId = string.Empty;
        //        string sAddr1 = string.Empty;
        //        string sAddr2 = string.Empty;
        //        string sCity = string.Empty;
        //        string sDestFolderPath = "";

        //        RMConfigurator objConfig = null;
        //        StringBuilder sPayeeHTMLtext;
        //        StringBuilder sClaimantHTMLtext;
        //        C1PdfDocument objPdfDoc;
        //        p_sPDFSaveFilePath = string.Empty;
        //        try
        //        {
        //            objConfig = new RMConfigurator();
        //            objPdfDoc = new C1PdfDocument();
        //            objPdfDoc.Clear();
        //            objPdfDoc.CurrentPage = 0;

        //            System.Drawing.Font fFont = new System.Drawing.Font("Courier New", 10);
        //            System.Drawing.Brush brBrush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(0, 0, 0));
        //            System.Drawing.RectangleF rcPage = objPdfDoc.PageRectangle;
        //            rcPage.Inflate(-25, -25);

        //            m_objFunds = p_objFunds;
        //            iTransId = m_objFunds.TransId;
        //            if (iTransId == 0)
        //            {
        //                if (!p_bIsSilent)
        //                    throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.SaveItem"));
        //                return;
        //            }

        //            objBrsInvoice = (BrsInvoice)m_objDataModelFactory.GetDataModelObject("BrsInvoice", false);
        //            try
        //            {
        //                objBrsInvoice.MoveToTransId(iTransId);
        //            }
        //            catch
        //            {
        //                if (!p_bIsSilent)
        //                    throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems"));
        //                return;
        //            }

        //            iInvoiceId = objBrsInvoice.InvoiceId;
        //            sName = objBrsInvoice.FirstNameText + " " + objBrsInvoice.LastNameText;

        //            objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
        //            if (m_objFunds.ClaimId > 0)
        //                objClaim.MoveTo(m_objFunds.ClaimId);

        //            iStateId = objClaim.FilingStateId;
        //            objClaim.Dispose();

        //            objState = (State)m_objDataModelFactory.GetDataModelObject("State", false);
        //            if (iStateId > 0)
        //                objState.MoveTo(iStateId);
        //            sJurisdiction = objState.StateId;
        //            objState.Dispose();

        //            sSQL = "SELECT FL_LICENSE,ZIP_CODE FROM INVOICE_DETAIL WHERE INVOICE_ID=" + iInvoiceId;
        //            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //            if (objReader != null)
        //            {
        //                if (objReader.Read())
        //                {
        //                    m_structEOBRecord.FirstLicense = objReader["FL_LICENSE"].ToString();
        //                    m_structEOBRecord.FirstBillingPostalCode = objReader["ZIP_CODE"].ToString();
        //                }

        //            }
        //            if (objReader != null)
        //                objReader.Close();

        //            sSQL = "SELECT INVOICE_NUMBER, INVOICE_DATE FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID=" + iTransId;
        //            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //            if (objReader != null)
        //            {
        //                if (objReader.Read())
        //                {
        //                    m_structEOBRecord.FirstInvoice = objReader["INVOICE_NUMBER"].ToString();
        //                    m_structEOBRecord.FirstInvoiceDate = objReader["INVOICE_DATE"].ToString();
        //                }

        //            }
        //            if (objReader != null)
        //                objReader.Close();
        //            sSQL = null;

        //            string sFromDateFirst = string.Empty;
        //            string sFromDateLast = string.Empty;

        //            sSQL = "SELECT FROM_DATE FROM FUNDS_TRANS_SPLIT WHERE SPLIT_ROW_ID=(SELECT MIN(SPLIT_ROW_ID) FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID=" + iTransId + ")";

        //            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //            if (objReader != null)
        //            {
        //                if (objReader.Read())
        //                {
        //                    sFromDateFirst = objReader["FROM_DATE"].ToString();
        //                    sFromDateFirst = Conversion.ToDate(sFromDateFirst).ToShortDateString();
        //                }

        //            }
        //            if (objReader != null)
        //                objReader.Close();
        //            sSQL = null;

        //            sSQL = "SELECT FROM_DATE FROM FUNDS_TRANS_SPLIT WHERE SPLIT_ROW_ID=(SELECT MAX(SPLIT_ROW_ID) FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID=" + iTransId + ")";

        //            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //            if (objReader != null)
        //            {
        //                if (objReader.Read())
        //                {
        //                    sFromDateLast = objReader["FROM_DATE"].ToString();
        //                    sFromDateLast = Conversion.ToDate(sFromDateLast).ToShortDateString();
        //                }

        //            }
        //            if (objReader != null)
        //                objReader.Close();
        //            sSQL = null;

        //            m_structEOBRecord.DateOfService = sFromDateFirst + " to " + sFromDateLast;

        //            if (m_objFunctions.m_bPrintClaimantEOB)
        //            {
        //                sClaimantFileName = Functions.GetFormatFilePath()
        //                + @"\" + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMHTMLFILE);

        //                if ((sJurisdiction == string.Empty) || !File.Exists(sClaimantFileName))
        //                    sClaimantFileName = Functions.GetFormatFilePath()
        //                        + @"\" + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMHTMLFILE);
        //            }

        //            if (m_objFunctions.m_bPrintPayeeEOB)
        //            {
        //                sPayeeFileName = Functions.GetFormatFilePath()
        //                + @"\" + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEHTMLFILE);

        //                if ((sJurisdiction == string.Empty) || !File.Exists(sPayeeFileName))
        //                    sPayeeFileName = Functions.GetFormatFilePath()
        //                        + @"\" + Functions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEHTMLFILE);
        //            }

        //            bIsFormatFileFound = true;
        //            if (!File.Exists(sClaimantFileName) && m_objFunctions.m_bPrintClaimantEOB)
        //                bIsFormatFileFound = false;

        //            if (bIsFormatFileFound)
        //                if (!File.Exists(sPayeeFileName) && m_objFunctions.m_bPrintPayeeEOB)
        //                    bIsFormatFileFound = false;

        //            objLocalCache = new LocalCache(m_sConnectionString);

        //            if (bIsFormatFileFound)
        //            {
        //                m_structEOBRecord.PayeeFirstName = objBrsInvoice.FirstNameText;
        //                m_structEOBRecord.PayeeLastName = objBrsInvoice.LastNameText;
        //                m_structEOBRecord.PayeeFullName = sName.Trim();
        //                m_structEOBRecord.PayeeAddr1 = objBrsInvoice.Addr1Text;
        //                m_structEOBRecord.PayeeAddr2 = objBrsInvoice.Addr2Text;
        //                objLocalCache.GetStateInfo(objBrsInvoice.StateId, ref strStateCode, ref strStateDesc);
        //                m_structEOBRecord.PayeeState = ((string)(strStateCode + " " + strStateDesc)).Trim();
        //                m_structEOBRecord.PayeePostalCode = objBrsInvoice.PostalCodeText;
        //                m_structEOBRecord.PayeeCity = objBrsInvoice.CityText;
        //                m_structEOBRecord.ClaimNumber = objBrsInvoice.ClaimNumberText;
        //                m_structEOBRecord.OrigInvDate = Conversion.GetDate(objBrsInvoice.DttmRcdAdded);
        //                m_structEOBRecord.OrigInvUser = objBrsInvoice.AddedByUser;
        //            }
        //            iClaimantEid = objBrsInvoice.ClaimantEid;
        //            sTransDateText = objBrsInvoice.TransDateText;
        //            sClaimNumberText = objBrsInvoice.ClaimNumberText;
        //            sAddr1 = objBrsInvoice.Addr1Text;
        //            sAddr2 = objBrsInvoice.Addr2Text;
        //            sCity = objBrsInvoice.CityText;

        //            objBrsInvoice.Dispose();

        //            objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
        //            if (bIsFormatFileFound)
        //            {
        //                try
        //                {
        //                    objEntity.MoveTo(m_objFunds.PayeeEid);
        //                    m_structEOBRecord.PayeeTaxID = objEntity.TaxId;
        //                }
        //                catch
        //                {
        //                    m_structEOBRecord.PayeeTaxID = string.Empty;
        //                }

        //                objEmployee = (Employee)m_objDataModelFactory.GetDataModelObject("Employee", false);
        //                try
        //                {
        //                    objEmployee.MoveTo(iClaimantEid);
        //                    iDeptAssignEId = objEmployee.DeptAssignedEid;
        //                    objEntity.MoveTo(iDeptAssignEId);
        //                    m_structEOBRecord.EmpDeptCode = objEntity.Abbreviation;
        //                    m_structEOBRecord.EmpDepartment = objEntity.LastName;
        //                }
        //                catch
        //                {
        //                    m_structEOBRecord.EmpDeptCode = string.Empty;
        //                    m_structEOBRecord.EmpDepartment = string.Empty;
        //                }
        //                objEmployee.Dispose();
        //                objEntity.Dispose();

        //                sSQL = "SELECT DATE_OF_EVENT, DATE_REPORTED, RPTD_BY_EID FROM EVENT,CLAIM WHERE " +
        //                    "CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + m_objFunds.ClaimId;
        //                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //                if (objReader != null)
        //                {
        //                    if (objReader.Read())
        //                    {
        //                        m_structEOBRecord.EventDate = objReader["DATE_OF_EVENT"].ToString();
        //                        m_structEOBRecord.DateReported = objReader["DATE_REPORTED"].ToString();
        //                    }
        //                    objReader.Close();
        //                }

        //                sSQL = "SELECT ENTITY_ID,LAST_NAME,FACILITY_EID,LOCATION_EID,DIVISION_EID,REGION_EID,OPERATION_EID" +
        //                    ",COMPANY_EID,CLIENT_EID FROM ENTITY, ORG_HIERARCHY WHERE DEPARTMENT_EID=" + iDeptAssignEId;
        //                objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL);
        //                if (objDataSet != null && objDataSet.Tables.Count > 0)
        //                {
        //                    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("FACILITY_EID=ENTITY_ID"))
        //                        m_structEOBRecord.EmpFacility = objDataRow["LAST_NAME"].ToString();

        //                    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("LOCATION_EID=ENTITY_ID"))
        //                        m_structEOBRecord.EmpLocation = objDataRow["LAST_NAME"].ToString();

        //                    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("DIVISION_EID=ENTITY_ID"))
        //                        m_structEOBRecord.EmpDivision = objDataRow["LAST_NAME"].ToString();

        //                    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("REGION_EID=ENTITY_ID"))
        //                        m_structEOBRecord.EmpRegion = objDataRow["LAST_NAME"].ToString();

        //                    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("OPERATION_EID=ENTITY_ID"))
        //                        m_structEOBRecord.EmpOperation = objDataRow["LAST_NAME"].ToString();

        //                    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("COMPANY_EID=ENTITY_ID"))
        //                        m_structEOBRecord.EmpCompany = objDataRow["LAST_NAME"].ToString();

        //                    foreach (DataRow objDataRow in objDataSet.Tables[0].Select("CLIENT_EID=ENTITY_ID"))
        //                        m_structEOBRecord.EmpClient = objDataRow["LAST_NAME"].ToString();
        //                }
        //                if (objDataSet != null)
        //                    objDataSet.Dispose();

        //                sSQL = "SELECT LAST_NAME,FIRST_NAME,ABBREVIATION,CURRENT_ADJ_FLAG,PHONE1 FROM ENTITY," +
        //                    "CLAIM_ADJUSTER WHERE ADJUSTER_EID=ENTITY_ID AND CLAIM_ID=" + m_objFunds.ClaimId;
        //                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //                if (objReader != null)
        //                {
        //                    while (objReader.Read())
        //                    {
        //                        m_structEOBRecord.AdjusterAbbrev = objReader["ABBREVIATION"].ToString();
        //                        m_structEOBRecord.AdjusterFullName = ((string)(objReader["FIRST_NAME"].ToString() + " " + objReader["LAST_NAME"].ToString())).Trim();
        //                        m_structEOBRecord.AdjusterLastName = objReader["LAST_NAME"].ToString();
        //                        //Arnab: MITS-10722 - Getting Adjuster phone no
        //                        m_structEOBRecord.AdjusterPhone1 = Conversion.ConvertObjToStr(objReader["PHONE1"]);
        //                        //MITS-10722 End
        //                        if (Conversion.ConvertObjToInt(objReader["CURRENT_ADJ_FLAG"], m_iClientId) != 0)
        //                            break;
        //                    }
        //                    objReader.Close();
        //                }
        //            }
        //            //Make sure we have detail items (exit if none)
        //            sSQL = "SELECT COUNT(*) FROM INVOICE_DETAIL WHERE INVOICE_ID = " + iInvoiceId;
        //            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //            if (objReader != null)
        //            {
        //                if (objReader.Read())
        //                {
        //                    if (Conversion.ConvertObjToInt(objReader[0]) == 0), m_iClientId
        //                    {
        //                        if (!p_bIsSilent)
        //                            throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems"));
        //                        return;
        //                    }
        //                }
        //                objReader.Close();
        //            }

        //            //Lookup Claimant Name/Address info
        //            try
        //            {
        //                objEntity.MoveTo(iClaimantEid);

        //                sFirstName = objEntity.FirstName;
        //                sLastName = objEntity.LastName;
        //                sClaimantName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
        //                sRegClaimantName = ((string)(sFirstName + " " + sLastName)).Trim();
        //                sClaimantAddr1 = objEntity.Addr1;
        //                sClaimantAddr2 = objEntity.Addr2;
        //                objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
        //                m_structEOBRecord.ClaimantState = ((string)(strStateCode + " " + strStateDesc)).Trim();
        //                m_structEOBRecord.ClaimantCity = objEntity.City;
        //                m_structEOBRecord.ClaimantPostalCode = objEntity.ZipCode;
        //                sClaimantCity = m_structEOBRecord.ClaimantCity + ", " + m_structEOBRecord.ClaimantState
        //                    + " " + m_structEOBRecord.ClaimantPostalCode;
        //                sClaimantTaxId = objEntity.TaxId;
        //                if (bIsFormatFileFound)
        //                {
        //                    m_structEOBRecord.ClaimantFullName = sClaimantName;
        //                    m_structEOBRecord.ClaimantLastName = sLastName;
        //                    m_structEOBRecord.ClaimantFirstName = sFirstName;
        //                    m_structEOBRecord.ClaimantAddr1 = sClaimantAddr1;
        //                    m_structEOBRecord.ClaimantAddr2 = sClaimantAddr2;
        //                    m_structEOBRecord.TransDate = sTransDateText;
        //                    m_structEOBRecord.ClaimantSSN = sClaimantTaxId;
        //                }
        //            }
        //            catch
        //            {
        //                if (bIsFormatFileFound)
        //                {
        //                    m_structEOBRecord.ClaimantFullName = string.Empty;
        //                    m_structEOBRecord.ClaimantLastName = string.Empty;
        //                    m_structEOBRecord.ClaimantFirstName = string.Empty;
        //                    m_structEOBRecord.ClaimantAddr1 = string.Empty;
        //                    m_structEOBRecord.ClaimantAddr2 = string.Empty;
        //                    m_structEOBRecord.TransDate = string.Empty;
        //                    m_structEOBRecord.ClaimantSSN = string.Empty;
        //                }
        //            }
        //            objEntity.Dispose();

        //            //This prints the default EOB Form
        //            if (m_objFunctions.m_bPrintPayeeEOB)
        //            {
        //                if (!bIsFormatFileFound)
        //                {
        //                    //Get default HTML template
        //                    sPayeeHTMLtext = GetDefaultPayeeHTMLTemplate(sName, sAddr1, sAddr2, strStateCode, strStateDesc, sCity,
        //                        sRegClaimantName, sClaimantAddr1, sClaimantAddr2, sClaimantCity, sClaimNumberText, sClaimantName);
        //                }
        //                else
        //                {
        //                    //Print HTML template Custom File
        //                    objFormatFileStream = File.OpenText(sPayeeFileName);
        //                    sPayeeHTMLtext = new StringBuilder(objFormatFileStream.ReadToEnd());
        //                }
        //                XmlDocument xPayeeHTMLdoc = ProcessMultiRowEOBHTMLtables(sPayeeHTMLtext, iInvoiceId);
        //                xPayeeHTMLdoc = CreateHTMLdocument(xPayeeHTMLdoc, m_structEOBRecord, m_structEOBDetailRecord, m_objFunds);
        //                for (int start = 0; ; )
        //                {
        //                    start = objPdfDoc.DrawStringHtml(xPayeeHTMLdoc.InnerXml, fFont, System.Drawing.Brushes.Black, rcPage, start);
        //                    if (start >= int.MaxValue)
        //                        break;
        //                    objPdfDoc.NewPage();
        //                }
        //            }

        //            //Print copy for claimant
        //            if (m_objFunctions.m_bPrintClaimantEOB)
        //            {
        //                if (iClaimantEid > 0)
        //                {
        //                    if (m_objFunctions.m_bPrintPayeeEOB)
        //                    {
        //                        objPdfDoc.NewPage();
        //                    }

        //                    if (!bIsFormatFileFound)
        //                    {
        //                        sClaimantHTMLtext = GetDefaultClaimantHTMLTemplate(sClaimNumberText, sClaimantName, sName);
        //                    }
        //                    else
        //                    {
        //                        objFormatFileStream = File.OpenText(sClaimantFileName);
        //                        sClaimantHTMLtext = new StringBuilder(objFormatFileStream.ReadToEnd());
        //                    }
        //                    XmlDocument xClaimantHTMLdoc = ProcessMultiRowEOBHTMLtables(sClaimantHTMLtext, iInvoiceId);
        //                    xClaimantHTMLdoc = CreateHTMLdocument(xClaimantHTMLdoc, m_structEOBRecord, m_structEOBDetailRecord, m_objFunds);
        //                    for (int start = 0; ; )
        //                    {
        //                        start = objPdfDoc.DrawStringHtml(xClaimantHTMLdoc.InnerXml, fFont, System.Drawing.Brushes.Black, rcPage, start);
        //                        if (start >= int.MaxValue)
        //                            break;
        //                        objPdfDoc.NewPage();
        //                    }
        //                }
        //            }

        //            if (objFormatFileStream != null)
        //            {
        //                objFormatFileStream.Close();
        //                objFormatFileStream.Dispose();
        //                objFormatFileStream = null;
        //            }

        //            if (m_objFunctions.m_bPrintClaimantEOB || m_objFunctions.m_bPrintPayeeEOB)
        //            {
        //                objPdfDoc.DocumentInfo.Title = "EOB Report";

        //                p_sPDFSaveFilePath = iTransId + "_" + p_sState + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
        //                sDestFolderPath = Functions.GetSavePath();

        //                objPdfDoc.Save(sDestFolderPath + @"\" + p_sPDFSaveFilePath);
        //                p_sPDFSaveFilePath = sDestFolderPath + @"\" + p_sPDFSaveFilePath;
        //            }
        //        }
        //        catch (DataModelException p_objEx)
        //        {
        //            throw p_objEx;
        //        }
        //        catch (RMAppException p_objEx)
        //        {
        //            throw p_objEx;
        //        }
        //        catch (Exception p_objEx)
        //        {
        //            throw new RMAppException(Globalization.GetString("PrintEOBAR.GetPrintCheckEOB.ErrorGet"), p_objEx);
        //        }
        //        finally
        //        {
        //            if (objClaim != null)
        //                objClaim.Dispose();

        //            if (objState != null)
        //                objState.Dispose();

        //            if (objDataSet != null)
        //                objDataSet.Dispose();

        //            if (objEmployee != null)
        //                objEmployee.Dispose();

        //            if (objReader != null)
        //                objReader.Dispose();

        //            if (objBrsInvoice != null)
        //                objBrsInvoice.Dispose();

        //            if (objEntity != null)
        //                objEntity.Dispose();

        //            if (objFormatFileStream != null)
        //                objFormatFileStream = null;

        //            if (objLocalCache != null)
        //                objLocalCache.Dispose();
        //        }
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends
        #endregion        



        #region "GetPrintCheckEOBBody(int p_iInvoiceId, string p_sClaimNumberText, string p_sClaimantName, string p_sName, string p_sOtherName, bool p_bIsFormatFileFound, int p_iCurrentX, int p_iCurrentY)"
        // akaushik5 Commented for MITS 35846 Starts
        /// Name		: GetPrintCheckEOBBody
		/// Author		: Anurag Agarwal
		/// Date Created	: 06 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Print EOB; Introduction And Body.Format; File Is the; Custom; Files; of
		/// http-like tags <b>.  If a file cannot be found, a Default format is used.
		/// </summary>
		/// <param name="p_iInvoiceId">Invoice Id</param>
		/// <param name="p_sClaimNumberText">Claim Number Text</param>
		/// <param name="p_sClaimantName">Claimant Name</param>
		/// <param name="p_sName">Name</param>
		/// <param name="p_sOtherName">Other Name</param>
		/// <param name="p_bIsFormatFileFound">Flag for Is format file found</param>
		/// <param name="p_iCurrentX">Current X-axis position</param>
		/// <param name="p_iCurrentY">Current Y-axis position</param>
        //private void GetPrintCheckEOBBody(int p_iInvoiceId, string p_sClaimNumberText, string p_sClaimantName, string p_sName, string p_sOtherName, bool p_bIsFormatFileFound, int p_iCurrentX, int p_iCurrentY)
        //{

        //    double dAvgTextHeight = 0.0;
        //    double dAvgPrintWidth = 0.0;
        //    bool bIsPrintTotals = false;
        //    bool bIsUseRecord = false;
        //    StringBuilder objSQL = null;
        //    DbReader objReader = null;
        //    DbReader objTempReader = null;
        //    FundsTransSplit objFundsTransSplit = null;
        //    int iTempId = 0;
        //    int iNumEOB = 0;
        //    int iLineItem = 0;
        //    string sSQL = string.Empty;
        //    string sTempData = string.Empty;
        //    double dTotAmtReduced = 0.0;
        //    double dTotSchdAmt = 0.0;
        //    double dTotAmtSaved = 0.0;
        //    double dTotalAmountAllowed = 0.0;
        //    double dTotalBaseAmount = 0.0;
        //    double dTotalDiscountAmount = 0.0;
        //    double dTotalPerDiemAmt = 0.0;
        //    double dTotalStopLossAmt = 0.0;
        //    double dTotalFeeTableAmt = 0.0;
        //    double dTotalAmountBilled = 0.0;
        //    double dTotalAmountPaid = 0.0;
        //    string sFromDate = string.Empty;
        //    string sToDate = string.Empty;
        //    string sInvoiceNumber = string.Empty;
        //    string sInvoiceDate = string.Empty;
        //    string sInvoiceBy = string.Empty;


        //    try
        //    {

        //        dAvgTextHeight = m_objPrintWrapper.GetTextHeight("W");
        //        dAvgPrintWidth = m_objPrintWrapper.PageWidth;

        //        //Print main body & introduction
        //        if (!p_bIsFormatFileFound)
        //        {
        //            m_objPrintWrapper.CurrentX = p_iCurrentX; //**
        //            m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight; //**

        //            //*** Default body - Detail record ***
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.RE") + Functions.FillSpace(1) + Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText") + Functions.FillSpace(1) + p_sClaimNumberText);

        //            m_objPrintWrapper.CurrentX = p_iCurrentX;
        //            m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName") + Functions.FillSpace(1) + p_sClaimantName);
        //            m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight; //***
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.Name") + Functions.FillSpace(1) + p_sName);

        //            if (p_sOtherName == string.Empty)
        //            {
        //                m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Othername1"));
        //                m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight; //***
        //                m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Othername2"));
        //            }
        //            else
        //            {
        //                m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Othername3") + Functions.FillSpace(1) + p_sOtherName + Functions.FillSpace(1) + Globalization.GetString("PrintEOBAR.EOBBody.Othername4"));
        //                m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight; //***
        //                m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Othername5"));
        //            }

        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.ItemsText"));
        //            m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight; //***
        //            m_objPrintWrapper.PrintText(string.Empty);

        //            m_objPrintWrapper.SetFontBold(true);

        //            //Tab(6); "INVOICE"; Tab(21); "PROCEDURE"; Tab(32); "PROC."; Tab(39); "EXPLANATION"; Tab(66); "AMOUNT"; Tab(78); "AMOUNT"
        //            m_objPrintWrapper.PrintText("INVOICE");
        //            m_objPrintWrapper.CurrentX = p_iCurrentX + 100;
        //            m_objPrintWrapper.PrintText("PROCEDURE");
        //            m_objPrintWrapper.CurrentX = p_iCurrentX + 300;
        //            m_objPrintWrapper.PrintText("PROC.");
        //            m_objPrintWrapper.CurrentX = p_iCurrentX + 500;
        //            m_objPrintWrapper.PrintText("EXPLANATION");
        //            m_objPrintWrapper.CurrentX = p_iCurrentX + 700;
        //            m_objPrintWrapper.PrintText("AMOUNT");
        //            m_objPrintWrapper.CurrentX = p_iCurrentX + 900;
        //            m_objPrintWrapper.PrintText("AMOUNT");
        //            m_objPrintWrapper.CurrentX = p_iCurrentX;
        //            m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;

        //            //Tab(6); "NUMBER"; Tab(21); "DATE"; Tab(32); "CODE"; Tab(66); "BILLED"; Tab(78); "PAID"
        //            m_objPrintWrapper.PrintText("NUMBER");
        //            m_objPrintWrapper.CurrentX = p_iCurrentX + 100;
        //            m_objPrintWrapper.PrintText("DATE");
        //            m_objPrintWrapper.CurrentX = p_iCurrentX + 300;
        //            m_objPrintWrapper.PrintText("CODE");
        //            m_objPrintWrapper.CurrentX = p_iCurrentX + 500;
        //            m_objPrintWrapper.PrintText("BILLED");
        //            m_objPrintWrapper.CurrentX = p_iCurrentX + 700;
        //            m_objPrintWrapper.PrintText("PAID");
        //            m_objPrintWrapper.SetFontBold(false);
        //        }

        //        objSQL = new StringBuilder();
        //        objSQL.Append("SELECT INVOICE_DETAIL_ID,BILLING_CODE_TEXT,AMOUNT_BILLED,AMOUNT_TO_PAY,FUNDS_SPLIT_ROW_ID");
        //        objSQL.Append(",TABLE_CODE, PERCENTILE,PLACE_OF_SER_CODE,TYPE_OF_SER_CODE,UNITS_BILLED_NUM");
        //        objSQL.Append(",SCHEDULED_AMOUNT,AMOUNT_REDUCED,AMOUNT_SAVED, QUANTITY,STORE, MODIFIER_CODE");
        //        objSQL.Append(",CONTRACT_AMOUNT,DISCOUNT,ZIP_CODE,BASE_AMOUNT,AMOUNT_ALLOWED");
        //        objSQL.Append(",PER_DIEM_AMT,STOP_LOSS_AMT,FEE_TABLE_AMT, PRESCRIP_NO, DRUG_NAME, PRESCRIP_DATE");
        //        //BRS FL Merge
        //        objSQL.Append(",FL_LICENSE, REV_CODE");
        //        //End
        //        objSQL.Append(" FROM INVOICE_DETAIL, FUNDS_TRANS_SPLIT WHERE INVOICE_ID = " + p_iInvoiceId);
        //        objSQL.Append(" AND FUNDS_SPLIT_ROW_ID = SPLIT_ROW_ID  ORDER BY FROM_DATE");

        //        objReader = DbFactory.GetDbReader(m_sConnectionString, objSQL.ToString());

        //        if (objReader != null)
        //        {
        //            while (objReader.Read())
        //            {
        //                m_structEOBDetailRecord.EOBList = new Hashtable();
        //                m_structEOBDetailRecord.DiagnosisList = new Hashtable();
        //                m_structEOBDetailRecord.ModifierList = new Hashtable();

        //                bIsUseRecord = false;

        //                //At first check if record is deleted or not
        //                iTempId = Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString());

        //                foreach (FundsTransSplit objFundTSplit in m_objFunds.TransSplitList)
        //                {
        //                    if (objFundTSplit.SplitRowId == iTempId)
        //                    {
        //                        bIsUseRecord = true;
        //                        break;
        //                    }
        //                }

        //                iNumEOB = 0;

        //                if (bIsUseRecord)
        //                {
        //                    bIsPrintTotals = true;
        //                    iTempId = Conversion.ConvertStrToInteger(objReader["INVOICE_DETAIL_ID"].ToString());


        //                    //BRS FL Merge : Umesh
        //                    //florida eob support for alternative descriptions
        //                    //Added for MITS 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
        //                    bool bUseCodeDescriptionFile = Convert.ToBoolean(Functions.GetValueFromConfigFile(Functions.TAG_USEEOBDESCRIPTIONFILE));
        //                    if (bUseCodeDescriptionFile)//Added if block for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
        //                    {
        //                        string sDesciptionFileName = Functions.GetFormatFilePath()
        //                                                    + @"\" + Functions.GetValueFromConfigFile(Functions.TAG_EOBDESCRIPTIONFILE);


        //                        //string sDesciptionFileName = m_objFunctions.GetValueFromConfigFile(Functions.TAG_FORMATFILEPATH)
        //                        //                             + @"\" + m_objFunctions.GetValueFromConfigFile(Functions.TAG_EOBDESCRIPTIONFILE);
        //                        if (File.Exists(sDesciptionFileName))
        //                        {


        //                            string sFileText = null;
        //                            string sNewText = null;
        //                            string sOldText = null;
        //                            int iPoint = 0;

        //                            StreamReader objSRdr = null;
        //                            objSRdr = new StreamReader(sDesciptionFileName);
        //                            sFileText = objSRdr.ReadToEnd();


        //                            sSQL = "SELECT CODES.SHORT_CODE FROM INVDETAIL_X_EOB, CODES";
        //                            sSQL = sSQL + " WHERE INVDETAIL_X_EOB.EOB_CODE = CODES.CODE_ID ";
        //                            sSQL = sSQL + " AND INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + iTempId;

        //                            //Changed for Mits 19415:Format of Explaination of Benifit form is different in RMX from RMWorld
        //                            //objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //                            //while (objTempReader.Read())
        //                            //{
        //                            //    sTempData = "@" + objTempReader.GetString("SHORT_CODE").Trim();
        //                            //    iPoint = sFileText.IndexOf(sTempData, 1);
        //                            //    if (iPoint >= 0)
        //                            //    {
        //                            //        sOldText = sFileText.Substring(iPoint + 1);
        //                            //        iPoint = sOldText.IndexOf("@");
        //                            //        sNewText = sOldText.Substring(0, iPoint - 1);
        //                            //        iNumEOB++;
        //                            //        m_structEOBDetailRecord.EOBList.Add(iNumEOB, sNewText);


        //                            //    }


        //                            //}
        //                            using (objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
        //                            {
        //                                while (objTempReader.Read())
        //                                {
        //                                    string sTempEob = "";
        //                                    sTempData = "@" + objTempReader.GetString("SHORT_CODE").Trim();
        //                                    iPoint = sFileText.IndexOf(sTempData, 0);
        //                                    if (iPoint >= 0)
        //                                    {
        //                                        sOldText = sFileText.Substring(iPoint + 1);
        //                                        iPoint = sOldText.IndexOf("@");
        //                                        sNewText = sOldText.Substring(0, iPoint - 1);
        //                                        sTempData = sNewText;
        //                                        if (iNumEOB > 0)
        //                                            sTempData = "- " + sTempData; //Starting of New EOB
        //                                        else
        //                                            sTempData = "  " + sTempData; //First EOB
        //                                        //iNumEOB++;
        //                                        if (sTempData.Length > 23)
        //                                        {

        //                                            int iStartIndex = 0;
        //                                            int iLengthFix = 23;
        //                                            int iLength = 0;
        //                                            int iLastIndexofSpace = 0;
        //                                            string sNextChar = "";
        //                                            while (iStartIndex < sTempData.Length)
        //                                            {
        //                                                iNumEOB++;

        //                                                //code to handle the boundary conditions
        //                                                if (iStartIndex + iLengthFix + 1 <= sTempData.Length)
        //                                                {
        //                                                    //find the next character
        //                                                    //if it is space , last word will be in same line

        //                                                    sNextChar = sTempData.Substring(iStartIndex + iLengthFix, 1);
        //                                                    if (sNextChar == " ")
        //                                                    {

        //                                                        sTempEob = sTempData.Substring(iStartIndex, iLengthFix); //Mits 19415:changed iLengthFix-1 to iLengthFix to extract all the chars before the space
        //                                                        iLength = iLengthFix; //Added for Mits 19415:iLength was not getting incremented in this case.Hence duplicacy of code description on next line.
        //                                                        m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
        //                                                    }
        //                                                    //else last word will come into next line. so remove the last word from current line.
        //                                                    else
        //                                                    {
        //                                                        sTempEob = sTempData.Substring(iStartIndex, iLengthFix - 1);
        //                                                        iLastIndexofSpace = sTempEob.LastIndexOf(" ");
        //                                                        iLength = iLastIndexofSpace;
        //                                                        sTempEob = sTempData.Substring(iStartIndex, iLength);
        //                                                        m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);


        //                                                    }
        //                                                }
        //                                                else
        //                                                {
        //                                                    sTempEob = sTempData.Substring(iStartIndex, iLengthFix - 1);
        //                                                    m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
        //                                                }
        //                                                iStartIndex += iLength;
        //                                                //get the lenght of remaining string.
        //                                                if (iStartIndex + 23 >= sTempData.Length)
        //                                                    iLengthFix = sTempData.Length - iStartIndex;

        //                                            }

        //                                        }


        //                                        else
        //                                        {
        //                                            iNumEOB++;
        //                                            m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
        //                                        }


        //                                    }
        //                                }
        //                            }
        //                            //Changed for Mits 19415:Format of Explaination of Benifit form is different in RMX from RMWorld
        //                            if (objTempReader != null)
        //                            {
        //                                objTempReader.Close();
        //                            }
        //                            if (objSRdr != null)
        //                            {
        //                                objSRdr.Close();
        //                                objSRdr.Dispose();
        //                            }


        //                            //BRS FL Merge : END


        //                        }
        //                        else
        //                        {
        //                            throw new RMAppException(Globalization.GetString("Functions.EobDescriptionFile.Error"));
        //                        }
        //                    }//Added if block for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
        //                    else
        //                    {
        //                        sSQL = "SELECT CODES_TEXT.CODE_DESC FROM INVDETAIL_X_EOB,CODES_TEXT WHERE " +
        //                            "INVDETAIL_X_EOB.EOB_CODE = CODES_TEXT.CODE_ID AND " +
        //                            "INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + iTempId;
        //                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

        //                        if (objTempReader != null)
        //                        {
        //                            while (objTempReader.Read())
        //                            {
        //                                string sTempEob = "";
        //                                sTempData = objTempReader["CODE_DESC"].ToString();
        //                                sTempData = "- " + sTempData; //Starting of New EOB
        //                                //iNumEOB++;
        //                                //if(sTempData.Length > 23)
        //                                //    m_structEOBDetailRecord.EOBList.Add(iNumEOB,sTempData.Trim().Substring(0,23));      
        //                                //else
        //                                //    m_structEOBDetailRecord.EOBList.Add(iNumEOB,sTempData);
        //                                if (sTempData.Length > 23)
        //                                {

        //                                    int iStartIndex = 0;
        //                                    int iLengthFix = 23;
        //                                    int iLength = 0;
        //                                    int iLastIndexofSpace = 0;
        //                                    string sNextChar = "";
        //                                    while (iStartIndex < sTempData.Length)
        //                                    {
        //                                        iNumEOB++;

        //                                        //code to handle the boundary conditions
        //                                        if (iStartIndex + iLengthFix + 1 <= sTempData.Length)
        //                                        {
        //                                            //find the next character
        //                                            //if it is space , last word will be in same line

        //                                            sNextChar = sTempData.Substring(iStartIndex + iLengthFix, 1);
        //                                            if (sNextChar == " ")
        //                                            {

        //                                                sTempEob = sTempData.Substring(iStartIndex, iLengthFix); //Mits 19415:changed iLengthFix-1 to iLengthFix to extract all the chars before the space
        //                                                iLength = iLengthFix; //Added for Mits 19415:iLength was not getting incremented in this case.Hence duplicacy of code description on next line.
        //                                                m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
        //                                            }
        //                                            //else last word will come into next line. so remove the last word from current line.
        //                                            else
        //                                            {
        //                                                sTempEob = sTempData.Substring(iStartIndex, iLengthFix - 1);
        //                                                iLastIndexofSpace = sTempEob.LastIndexOf(" ");
        //                                                iLength = iLastIndexofSpace;
        //                                                sTempEob = sTempData.Substring(iStartIndex, iLength);
        //                                                m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);

        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            sTempEob = sTempData.Substring(iStartIndex, iLengthFix); //No Carriage return in this case
        //                                            m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
        //                                        }
        //                                        iStartIndex += iLength;
        //                                        //get the lenght of remaining string.
        //                                        if (iStartIndex + 23 >= sTempData.Length)
        //                                            iLengthFix = sTempData.Length - iStartIndex;

        //                                    }

        //                                    //m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData.Trim().Substring(0, 23));
        //                                }


        //                                else
        //                                {
        //                                    iNumEOB++;
        //                                    m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData.Trim());
        //                                }
        //                            }
        //                            objTempReader.Close();
        //                        }
        //                    }
        //                    //*** Custom ***
        //                    if (p_bIsFormatFileFound)
        //                    {
        //                        //extract diagnosis code
        //                        m_structEOBDetailRecord.DiagnosisItems = 0;

        //                        sSQL = "SELECT CODES_TEXT.CODE_DESC FROM INVDETAIL_X_DIAG, CODES_TEXT" +
        //                            " WHERE INVDETAIL_X_DIAG.DIAGNOSIS_CODE=CODES_TEXT.CODE_ID" +
        //                            " AND INVDETAIL_X_DIAG.INVOICE_DETAIL_ID= " + iTempId;
        //                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

        //                        if (objTempReader != null)
        //                        {
        //                            while (objTempReader.Read())
        //                            {
        //                                m_structEOBDetailRecord.DiagnosisItems++;
        //                                sTempData = objTempReader["CODE_DESC"].ToString();
        //                                iLineItem++;
        //                                m_structEOBDetailRecord.DiagnosisList.Add(iLineItem, sTempData.Trim());
        //                            }
        //                            objTempReader.Close();
        //                        }
        //                        iLineItem = 0;
        //                        //extract modifier code
        //                        sSQL = "SELECT CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM INVDETAIL_X_MOD, CODES, CODES_TEXT" +
        //                            " WHERE INVDETAIL_X_MOD.MODIFIER_CODE=CODES_TEXT.CODE_ID" +
        //                            " AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND INVDETAIL_X_MOD.INVOICE_DETAIL_ID=" + iTempId;
        //                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

        //                        if (objTempReader != null)
        //                        {
        //                            while (objTempReader.Read())
        //                            {
        //                                sTempData = objTempReader["SHORT_CODE"].ToString() + " " + objTempReader["CODE_DESC"].ToString();
        //                                iLineItem++;
        //                                m_structEOBDetailRecord.ModifierList.Add(iLineItem, sTempData.Trim());
        //                            }
        //                            objTempReader.Close();
        //                        }
        //                        iLineItem = 0;
        //                        //extract fee schedule name
        //                        iTempId = Conversion.ConvertStrToInteger(objReader["TABLE_CODE"].ToString());
        //                        sSQL = "SELECT FEE_TABLES.USER_TABLE_NAME FROM FEE_TABLES WHERE FEE_TABLES.TABLE_ID = " + iTempId;
        //                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //                        if (objTempReader != null)
        //                        {
        //                            if (objTempReader.Read())
        //                            {
        //                                sTempData = objTempReader["USER_TABLE_NAME"].ToString();
        //                                m_structEOBDetailRecord.FeeSchedule.Token = sTempData;
        //                            }
        //                            objTempReader.Close();
        //                        }

        //                        m_structEOBDetailRecord.Percentile.Token = objReader["PERCENTILE"].ToString();

        //                        iTempId = Conversion.ConvertStrToInteger(objReader["PLACE_OF_SER_CODE"].ToString());
        //                        sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " +
        //                            "CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
        //                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //                        if (objTempReader != null)
        //                        {
        //                            if (objTempReader.Read())
        //                            {
        //                                m_structEOBDetailRecord.PlaceofServiceCode.Token = objTempReader["SHORT_CODE"].ToString();
        //                                m_structEOBDetailRecord.PlaceOfService.Token = objTempReader["CODE_DESC"].ToString();
        //                            }
        //                            objTempReader.Close();
        //                        }

        //                        iTempId = Conversion.ConvertStrToInteger(objReader["TYPE_OF_SER_CODE"].ToString());
        //                        sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " +
        //                            "CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
        //                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //                        if (objTempReader != null)
        //                        {
        //                            if (objTempReader.Read())
        //                            {
        //                                m_structEOBDetailRecord.TypeOfServiceCode.Token = objTempReader["SHORT_CODE"].ToString();
        //                                m_structEOBDetailRecord.TypeOfService.Token = objTempReader["CODE_DESC"].ToString();
        //                            }
        //                            objTempReader.Close();
        //                        }

        //                        m_structEOBDetailRecord.UnitsBilled.Token = Conversion.ConvertStrToLong(objReader["UNITS_BILLED_NUM"].ToString());

        //                        m_structEOBDetailRecord.ScheduledAmount.Token = Conversion.ConvertStrToDouble(objReader["SCHEDULED_AMOUNT"].ToString());
        //                        dTotSchdAmt += Functions.FormatAmount(m_structEOBDetailRecord.ScheduledAmount.Token);

        //                        m_structEOBDetailRecord.AmountReduced.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_REDUCED"].ToString());
        //                        dTotAmtReduced += Functions.FormatAmount(m_structEOBDetailRecord.AmountReduced.Token);

        //                        m_structEOBDetailRecord.AmountSaved.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_SAVED"].ToString());
        //                        dTotAmtSaved += Functions.FormatAmount(m_structEOBDetailRecord.AmountSaved.Token);

        //                        iTempId = Conversion.ConvertStrToInteger(objReader["MODIFIER_CODE"].ToString());
        //                        sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " +
        //                            "CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
        //                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //                        if (objTempReader != null)
        //                        {
        //                            if (objTempReader.Read())
        //                            {
        //                                m_structEOBDetailRecord.ModifierCode.Token = objTempReader["SHORT_CODE"].ToString();
        //                                m_structEOBDetailRecord.Modifier.Token = objTempReader["CODE_DESC"].ToString();
        //                            }
        //                            objTempReader.Close();
        //                        }

        //                        m_structEOBDetailRecord.ContractAmount.Token = Conversion.ConvertStrToDouble(objReader["CONTRACT_AMOUNT"].ToString());
        //                        m_structEOBDetailRecord.Discount.Token = Conversion.ConvertStrToDouble(objReader["DISCOUNT"].ToString());
        //                        m_structEOBDetailRecord.ProviderZipCode.Token = objReader["ZIP_CODE"].ToString();

        //                        m_structEOBDetailRecord.AmountAllowed.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_ALLOWED"].ToString());
        //                        dTotalAmountAllowed += Functions.FormatAmount(m_structEOBDetailRecord.AmountAllowed.Token);

        //                        m_structEOBDetailRecord.BaseAmount.Token = Conversion.ConvertStrToDouble(objReader["BASE_AMOUNT"].ToString());
        //                        dTotalBaseAmount += Functions.FormatAmount(m_structEOBDetailRecord.BaseAmount.Token);

        //                        m_structEOBDetailRecord.DiscountAmount.Token = m_structEOBDetailRecord.BaseAmount.Token * (m_structEOBDetailRecord.Discount.Token / 100);
        //                        dTotalDiscountAmount += Functions.FormatAmount(m_structEOBDetailRecord.DiscountAmount.Token);

        //                        m_structEOBDetailRecord.PerDiemAmount.Token = Conversion.ConvertStrToDouble(objReader["PER_DIEM_AMT"].ToString());
        //                        dTotalPerDiemAmt += Functions.FormatAmount(m_structEOBDetailRecord.PerDiemAmount.Token);

        //                        m_structEOBDetailRecord.StopLossAmount.Token = Conversion.ConvertStrToDouble(objReader["STOP_LOSS_AMT"].ToString());
        //                        dTotalStopLossAmt += Functions.FormatAmount(m_structEOBDetailRecord.StopLossAmount.Token);

        //                        m_structEOBDetailRecord.FeeTableAmount.Token = Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString());
        //                        dTotalFeeTableAmt += Functions.FormatAmount(m_structEOBDetailRecord.FeeTableAmount.Token);

        //                        m_structEOBDetailRecord.PrescripNo.Token = objReader["PRESCRIP_NO"].ToString();
        //                        m_structEOBDetailRecord.DrugName.Token = objReader["DRUG_NAME"].ToString();
        //                        m_structEOBDetailRecord.PrescripDate.Token = Conversion.ToDate(objReader["PRESCRIP_DATE"].ToString()).ToShortDateString();
        //                        //BRS FL Merge  : Umesh

        //                        m_structEOBDetailRecord.PhysicianLicenseNum.Token = objReader["FL_LICENSE"].ToString();
        //                        m_structEOBDetailRecord.RevenueCode.Token = objReader["REV_CODE"].ToString();
        //                        //End

        //                        m_structEOBDetailRecord.DiscOnFeeSchd.Token = 0;
        //                        if (Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString()) > 0 && Conversion.ConvertStrToDouble(objReader["DISCOUNT"].ToString()) > 0)
        //                            m_structEOBDetailRecord.DiscOnFeeSchd.Token = Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString()) - Conversion.ConvertStrToDouble(objReader["AMOUNT_ALLOWED"].ToString());
        //                    }
        //                    else
        //                    {
        //                        m_objFunctions.CheckEOBPageLength(13500, m_objPrintWrapper);  // Checks to see if new page is needed
        //                    }

        //                    objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit", false);
        //                    try
        //                    {
        //                        objFundsTransSplit.MoveTo(Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString()));
        //                        sFromDate = Conversion.ToDate(objFundsTransSplit.FromDate).ToShortDateString();
        //                        sToDate = Conversion.ToDate(objFundsTransSplit.ToDate).ToShortDateString();
        //                        sInvoiceNumber = objFundsTransSplit.InvoiceNumber;
        //                        sInvoiceDate = Conversion.ToDate(objFundsTransSplit.InvoiceDate).ToShortDateString();
        //                        sInvoiceBy = objFundsTransSplit.InvoicedBy;
        //                        objFundsTransSplit.Dispose();
        //                    }
        //                    catch
        //                    {
        //                        sFromDate = string.Empty;
        //                        sToDate = string.Empty;
        //                        sInvoiceNumber = "N/A";
        //                        sInvoiceDate = string.Empty;
        //                        sInvoiceBy = string.Empty;
        //                    }

        //                    if (p_bIsFormatFileFound)
        //                    {
        //                        m_structEOBDetailRecord.ProcedureDate.Token = sFromDate;
        //                        m_structEOBDetailRecord.ProcToDate.Token = sToDate;
        //                        m_structEOBDetailRecord.InvoiceNumber.Token = sInvoiceNumber;
        //                        m_structEOBDetailRecord.InvoiceDate.Token = sInvoiceDate;
        //                        m_structEOBDetailRecord.InvoicedBy.Token = sInvoiceBy;
        //                    }
        //                    else
        //                    {
        //                        m_objPrintWrapper.CurrentX = p_iCurrentX;
        //                        m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
        //                        m_objPrintWrapper.PrintText(sInvoiceNumber.Trim());
        //                        m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight; //***
        //                        m_objPrintWrapper.PrintText(sFromDate);
        //                    }

        //                    //Print remaining data fields
        //                    sTempData = objReader["BILLING_CODE_TEXT"].ToString();

        //                    //*** Default body ***
        //                    if (!p_bIsFormatFileFound)
        //                    {
        //                        m_objPrintWrapper.CurrentX = p_iCurrentX + 500;
        //                        m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
        //                        if (sTempData.Length > 6)
        //                            m_objPrintWrapper.PrintText(sTempData.Substring(0, 6));
        //                        else
        //                            m_objPrintWrapper.PrintText(sTempData);
        //                    }
        //                    else
        //                    {
        //                        m_structEOBDetailRecord.ProcedureCode.Token = sTempData;
        //                        m_structEOBDetailRecord.Procedure.Token = sTempData.Substring(sTempData.IndexOf('-') + 1);
        //                    }

        //                    if (!p_bIsFormatFileFound && iNumEOB > 0)
        //                        m_objPrintWrapper.PrintText(m_structEOBDetailRecord.EOBList[1].ToString());

        //                    //amount billed
        //                    double d = Conversion.ConvertStrToDouble(objReader["AMOUNT_BILLED"].ToString());
        //                    if (p_bIsFormatFileFound)
        //                        m_structEOBDetailRecord.AmountBilled.Token = d;
        //                    dTotalAmountBilled += d;
        //                    if (!p_bIsFormatFileFound)
        //                    {
        //                        sTempData = string.Format("{0:#,###,##0.00}", d);
        //                        sTempData = Functions.FillSpace(12 - sTempData.Length) + sTempData;
        //                        m_objPrintWrapper.CurrentX = p_iCurrentX + 900;
        //                        m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
        //                        m_objPrintWrapper.PrintText(sTempData);
        //                    }

        //                    //amount to pay
        //                    d = Conversion.ConvertStrToDouble(objReader["AMOUNT_TO_PAY"].ToString());
        //                    if (p_bIsFormatFileFound)
        //                        m_structEOBDetailRecord.AmountPaid.Token = d;
        //                    dTotalAmountPaid += d;
        //                    if (!p_bIsFormatFileFound)
        //                    {
        //                        sTempData = string.Format("{0:#,###,##0.00}", d);
        //                        sTempData = Functions.FillSpace(12 - sTempData.Length) + sTempData;
        //                        m_objPrintWrapper.CurrentX = p_iCurrentX + 900;
        //                        m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
        //                        m_objPrintWrapper.PrintText(sTempData);
        //                    }

        //                    //Print remaining EOB lines (see above)
        //                    if (!p_bIsFormatFileFound)
        //                    {
        //                        if (iNumEOB > 1)
        //                        {
        //                            for (int i = 1; i < iNumEOB - 1; i++)
        //                            {
        //                                m_objPrintWrapper.CurrentX = p_iCurrentX + 300;
        //                                m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
        //                                m_objFunctions.CheckEOBPageLength(13800, m_objPrintWrapper); ;
        //                                m_objPrintWrapper.PrintText(Functions.FillSpace(16) + m_structEOBDetailRecord.EOBList[i].ToString());
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        //*** Custom Body - Detail record ***
        //                        m_objFunctions.GetCustomEOBBody(m_structEOBDetailRecord, ref m_structEOBRecord, ref m_objPrintWrapper);
        //                    }
        //                }
        //                m_structEOBDetailRecord.EOBList.Clear();
        //                m_structEOBDetailRecord.DiagnosisList.Clear();
        //            }//END of While
        //            objReader.Close();
        //        }//End of IF

        //        if (bIsPrintTotals)
        //        {
        //            if (p_bIsFormatFileFound)
        //            {
        //                m_structEOBRecord.TotalAmountBilled = dTotalAmountBilled;
        //                m_structEOBRecord.TotalAmountPaid = dTotalAmountPaid;
        //                m_structEOBRecord.TotalScheduledAmount = dTotSchdAmt;
        //                m_structEOBRecord.TotalAmountReduced = dTotAmtReduced;
        //                m_structEOBRecord.TotalAmountSaved = dTotAmtSaved;
        //                m_structEOBRecord.TotalBaseAmount = dTotalBaseAmount;
        //                m_structEOBRecord.TotalAmountAllowed = dTotalAmountAllowed;
        //                m_structEOBRecord.TotalDiscountAmount = dTotalDiscountAmount;
        //                m_structEOBRecord.TotalPerDiemAmt = dTotalPerDiemAmt;
        //                m_structEOBRecord.TotalStopLossAmt = dTotalStopLossAmt;
        //                m_structEOBRecord.TotalFeeTableAmt = dTotalFeeTableAmt;
        //            }
        //            else
        //            {
        //                sTempData = string.Format("{0:#,###,##0.00}", dTotalAmountBilled);
        //                sTempData = Functions.FillSpace(12 - sTempData.Length) + sTempData;
        //                m_objPrintWrapper.SetFontBold(true);
        //                m_objPrintWrapper.CurrentX = p_iCurrentX;
        //                m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
        //                m_objPrintWrapper.PrintText("TOTALS:");
        //                m_objPrintWrapper.CurrentX = p_iCurrentX + 300;
        //                m_objPrintWrapper.PrintText(sTempData);
        //                sTempData = string.Format("{0:#,###,##0.00}", dTotalAmountPaid);
        //                sTempData = Functions.FillSpace(12 - sTempData.Length) + sTempData;
        //                m_objPrintWrapper.CurrentX = p_iCurrentX + 300;
        //                m_objPrintWrapper.PrintText(sTempData);
        //                m_objPrintWrapper.SetFontBold(false);
        //            }
        //        }

        //        if (!p_bIsFormatFileFound)
        //        {
        //            m_objPrintWrapper.CurrentX = p_iCurrentX;
        //            m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
        //            m_objFunctions.CheckEOBPageLength(12300, m_objPrintWrapper); ;
        //            m_objPrintWrapper.PrintText(Functions.FillSpace(4) + Globalization.GetString("PrintEOBAR.EOBBody.ContactUs"));
        //            m_objPrintWrapper.CurrentX = p_iCurrentX;
        //            m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
        //            m_objPrintWrapper.PrintText(Functions.FillSpace(4) + "Sincerely,");
        //            m_objPrintWrapper.CurrentX = p_iCurrentX;
        //            m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
        //            m_objPrintWrapper.CurrentX = p_iCurrentX;
        //            m_objPrintWrapper.CurrentY = p_iCurrentY + dAvgTextHeight;
        //            m_objPrintWrapper.PrintText(Functions.FillSpace(4) + m_objCommon.m_stLoginInfo.Username);
        //        }
        //    }
        //    catch (DataModelException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (RMAppException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBAR.GetPrintCheckEOBBody.ErrorGet"), p_objEx);
        //    }
        //    finally
        //    {
        //        if (objSQL != null)
        //            objSQL = null;

        //        if (objReader != null)
        //            objReader.Dispose();

        //        if (objTempReader != null)
        //            objTempReader.Dispose();

        //        if (objFundsTransSplit != null)
        //            objFundsTransSplit.Dispose();
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends
        #endregion

		#region "GetEOBDetail(Funds p_objFunds,ref CEOBRecord p_structCEOBRecord, long p_lInvoiceId)"
		/// Name		: GetEOBDetail
		/// Author		: Anurag Agarwal
		/// Date Created	: 23 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Get the Print EOB detail items and fill into respective structure for printing EOB active report
		/// </summary>
		/// <param name="p_objFunds">Funds Object</param>
		/// <param name="p_structCEOBRecord">EOB Record</param>
		/// <param name="p_lInvoiceId">Invoice Id</param>
        // akaushik5 Commented for MITS 35846 Starts
        //private void GetEOBDetail(Funds p_objFunds,ref CEOBRecord p_structCEOBRecord, long p_lInvoiceId)
        //{
        //    string sSQL = string.Empty;
        //    bool bIsUseRecord = false;
        //    DbReader objRdr = null;
        //    DbReader objReader = null;
        //    CEOBDetailRecord structCEOBDetailRecord;
        //    FundsTransSplit objFundsTransSplit = null;
        //    long lTempId = 0;
        //    string sStrTemp = string.Empty;
        //    int iNumEOB = 0;
        //    int iLineItem = 0;
        //    double dTotAmtReduced = 0.0;
        //    double dTotSchdAmt = 0.0;
        //    double dTotAmtSaved = 0.0;
        //    double dTotalAmountAllowed = 0.0;
        //    double dTotalBaseAmount = 0.0;
        //    double dTotalDiscountAmount = 0.0;
        //    double dTotalPerDiemAmt = 0.0;
        //    double dTotalStopLossAmt = 0.0;
        //    double dTotalFeeTableAmt = 0.0;
        //    double dTotalAmountBilled = 0.0;
        //    double dTotalAmountPaid = 0.0;
        //    int intRecordCount = 0;

        //    try
        //    {
        //        sSQL = "SELECT INVOICE_DETAIL_ID,BILLING_CODE_TEXT,AMOUNT_BILLED,AMOUNT_TO_PAY,FUNDS_SPLIT_ROW_ID" +
        //            ",TABLE_CODE, PERCENTILE,PLACE_OF_SER_CODE,TYPE_OF_SER_CODE,UNITS_BILLED_NUM" + 
        //            ",SCHEDULED_AMOUNT,AMOUNT_REDUCED,AMOUNT_SAVED, QUANTITY,STORE,CONTRACT_AMOUNT" + 
        //            ",DISCOUNT,ZIP_CODE,BASE_AMOUNT,AMOUNT_ALLOWED,PER_DIEM_AMT,STOP_LOSS_AMT," + 
        //            "FEE_TABLE_AMT,FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE FROM INVOICE_DETAIL," + 
        //            "FUNDS_TRANS_SPLIT WHERE INVOICE_ID = " + p_lInvoiceId + " AND FUNDS_SPLIT_ROW_ID" + 
        //            " = SPLIT_ROW_ID ORDER BY FROM_DATE,INVOICE_DETAIL_ID";

        //        objRdr=DbFactory.GetDbReader(m_sConnectionString ,sSQL);

        //        p_structCEOBRecord.DetailRecordsList = new Hashtable();

        //        if(objRdr != null)
        //        {
        //            while(objRdr.Read())
        //            {
        //                structCEOBDetailRecord = new CEOBDetailRecord();
        //                structCEOBDetailRecord.EOBList = new Hashtable();
        //                structCEOBDetailRecord.DiagnosisList = new Hashtable();
        //                structCEOBDetailRecord.ModifierList = new Hashtable();

        //                bIsUseRecord = false;

        //                //At first check if record is deleted or not
        //                lTempId = Conversion.ConvertStrToLong(objRdr["FUNDS_SPLIT_ROW_ID"].ToString());			
        //                foreach(FundsTransSplit objFundTSplit in p_objFunds.TransSplitList)
        //                {
        //                    if(objFundTSplit.SplitRowId == lTempId)
        //                    {
        //                        bIsUseRecord = true;
        //                        break;
        //                    }
        //                }

        //                iNumEOB = 0;
        //                if(bIsUseRecord)
        //                {
        //                    lTempId = Conversion.ConvertStrToLong(objRdr["INVOICE_DETAIL_ID"].ToString());			

        //                    //... Explanation description(s)
        //                    sSQL = "SELECT CODES_TEXT.CODE_DESC FROM INVDETAIL_X_EOB, CODES_TEXT" + 
        //                        " WHERE INVDETAIL_X_EOB.EOB_CODE = CODES_TEXT.CODE_ID AND " + 
        //                        "INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + lTempId + " AND CODES_TEXT.CODE_ID <> 0";
        //                    objReader=DbFactory.GetDbReader(m_sConnectionString ,sSQL);
        //                    if(objReader != null)
        //                    {
        //                        while(objReader.Read())
        //                            structCEOBDetailRecord.EOBList.Add(++iNumEOB,objReader[0].ToString().Trim());     
        //                        objReader.Close();  
        //                    }

        //                    // ... Diagnosis Code(s)
        //                    sSQL = "SELECT SHORT_CODE FROM INVDETAIL_X_DIAG, CODES WHERE " + 
        //                        "INVDETAIL_X_DIAG.DIAGNOSIS_CODE=CODES.CODE_ID AND " + 
        //                        "INVDETAIL_X_DIAG.INVOICE_DETAIL_ID=" + lTempId + " AND CODES.CODE_ID <> 0";
        //                    objReader=DbFactory.GetDbReader(m_sConnectionString ,sSQL);
        //                    if(objReader != null)
        //                    {
        //                        while(objReader.Read())
        //                            structCEOBDetailRecord.DiagnosisList.Add(++iLineItem,objReader[0].ToString().Trim());     
        //                        objReader.Close();  
        //                    }

        //                    // ... Modifier Code(s)
        //                    sSQL = "SELECT CODES.SHORT_CODE FROM INVDETAIL_X_MOD, CODES WHERE " + 
        //                        "INVDETAIL_X_MOD.MODIFIER_CODE=CODES.CODE_ID AND " + 
        //                        "INVDETAIL_X_MOD.INVOICE_DETAIL_ID=" + lTempId + " AND CODES.CODE_ID <> 0";
        //                    objReader=DbFactory.GetDbReader(m_sConnectionString ,sSQL);
        //                    if(objReader != null)
        //                    {
        //                        while(objReader.Read())
        //                            //Changed by Gagan for MITS 11520 : Start
        //                            //structCEOBDetailRecord.DiagnosisList.Add(++iLineItem,objReader[0].ToString().Trim());     
        //                            structCEOBDetailRecord.ModifierList.Add(++iLineItem, objReader[0].ToString().Trim());
        //                        //Changed by Gagan for MITS 11520 : End
        //                        objReader.Close();  
        //                    }

        //                    // ... Fee Schedule Name
        //                    sSQL = "SELECT FEE_TABLES.USER_TABLE_NAME FROM FEE_TABLES WHERE FEE_TABLES.TABLE_ID = " + 
        //                        Conversion.ConvertStrToLong(objRdr["TABLE_CODE"].ToString());
        //                    objReader=DbFactory.GetDbReader(m_sConnectionString ,sSQL);
        //                    if(objReader != null)
        //                    {
        //                        if(objReader.Read()) 
        //                            structCEOBDetailRecord.FeeSchedule = objReader[0].ToString().Trim();     
        //                        objReader.Close();  
        //                    }

        //                    //... Percentile
        //                    structCEOBDetailRecord.Percentile = objRdr["PERCENTILE"].ToString().Trim(); 

        //                    // ... Place of Service
        //                    sSQL = "SELECT SHORT_CODE, CODE_DESC FROM CODES_TEXT WHERE " +
        //                        "CODES_TEXT.CODE_ID = " + Conversion.ConvertStrToLong(objRdr["PLACE_OF_SER_CODE"].ToString());
        //                    objReader=DbFactory.GetDbReader(m_sConnectionString ,sSQL);
        //                    if(objReader != null)
        //                    {
        //                        if(objReader.Read()) 
        //                        {
        //                            structCEOBDetailRecord.PlaceofServiceCode = objReader["SHORT_CODE"].ToString().Trim();
        //                            structCEOBDetailRecord.PlaceOfService = objReader["CODE_DESC"].ToString().Trim();     
        //                        }
        //                        objReader.Close();  
        //                    }

        //                    // ... Type of Service
        //                    sSQL = "SELECT SHORT_CODE, CODE_DESC FROM CODES_TEXT WHERE " +
        //                        "CODES_TEXT.CODE_ID = " + Conversion.ConvertStrToLong(objRdr["TYPE_OF_SER_CODE"].ToString());
        //                    objReader=DbFactory.GetDbReader(m_sConnectionString ,sSQL);
        //                    if(objReader != null)
        //                    {
        //                        if(objReader.Read()) 
        //                        {
        //                            structCEOBDetailRecord.TypeOfServiceCode = objReader["SHORT_CODE"].ToString().Trim();
        //                            structCEOBDetailRecord.TypeOfService = objReader["CODE_DESC"].ToString().Trim();     
        //                        }
        //                        objReader.Close();  
        //                    }

        //                    // ... Trans Type
        //                    sSQL = "SELECT SHORT_CODE, CODE_DESC FROM CODES_TEXT WHERE " +
        //                        "CODES_TEXT.CODE_ID = " + Conversion.ConvertStrToLong(objRdr["TRANS_TYPE_CODE"].ToString());
        //                    objReader=DbFactory.GetDbReader(m_sConnectionString ,sSQL);
        //                    if(objReader != null)
        //                    {
        //                        if(objReader.Read()) 
        //                        {
        //                            structCEOBDetailRecord.TransTypeCode = objReader["SHORT_CODE"].ToString().Trim();
        //                            structCEOBDetailRecord.TransType = objReader["CODE_DESC"].ToString().Trim();     
        //                        }
        //                        objReader.Close();  
        //                    }

        //                    structCEOBDetailRecord.UnitsBilled = Conversion.ConvertStrToDouble(objRdr["UNITS_BILLED_NUM"].ToString());
        //                    //round to 2 decimal places so totals match what is paid.
        //                    structCEOBDetailRecord.ScheduledAmount = Conversion.ConvertStrToDouble(objRdr["SCHEDULED_AMOUNT"].ToString());
        //                    dTotSchdAmt += Functions.FormatAmount(structCEOBDetailRecord.ScheduledAmount);

        //                    structCEOBDetailRecord.AmountReduced = Conversion.ConvertStrToDouble(objRdr["AMOUNT_REDUCED"].ToString());
        //                    dTotAmtReduced += Functions.FormatAmount(structCEOBDetailRecord.AmountReduced);

        //                    structCEOBDetailRecord.AmountSaved = Conversion.ConvertStrToDouble(objRdr["AMOUNT_SAVED"].ToString());
        //                    dTotAmtSaved += Functions.FormatAmount(structCEOBDetailRecord.AmountSaved);

        //                    structCEOBDetailRecord.ContractAmount = Conversion.ConvertStrToDouble(objRdr["CONTRACT_AMOUNT"].ToString());
        //                    structCEOBDetailRecord.Discount = Conversion.ConvertStrToDouble(objRdr["DISCOUNT"].ToString());
        //                    structCEOBDetailRecord.ProviderZipCode = objRdr["ZIP_CODE"].ToString();

        //                    structCEOBDetailRecord.AmountAllowed = Conversion.ConvertStrToDouble(objRdr["AMOUNT_ALLOWED"].ToString());
        //                    dTotalAmountAllowed += Functions.FormatAmount(structCEOBDetailRecord.AmountAllowed);

        //                    structCEOBDetailRecord.BaseAmount = Conversion.ConvertStrToDouble(objRdr["BASE_AMOUNT"].ToString());
        //                    dTotalBaseAmount += Functions.FormatAmount(structCEOBDetailRecord.BaseAmount);

        //                    structCEOBDetailRecord.DiscountAmount =  structCEOBDetailRecord.BaseAmount * (structCEOBDetailRecord.Discount / 100);
        //                    dTotalDiscountAmount += Functions.FormatAmount(structCEOBDetailRecord.DiscountAmount); 

        //                    structCEOBDetailRecord.PerDiemAmount = Conversion.ConvertStrToDouble(objRdr["PER_DIEM_AMT"].ToString());
        //                    dTotalPerDiemAmt += Functions.FormatAmount(structCEOBDetailRecord.PerDiemAmount);

        //                    structCEOBDetailRecord.StopLossAmount = Conversion.ConvertStrToDouble(objRdr["STOP_LOSS_AMT"].ToString());
        //                    dTotalStopLossAmt += Functions.FormatAmount(structCEOBDetailRecord.StopLossAmount);

        //                    structCEOBDetailRecord.FeeTableAmount = Conversion.ConvertStrToDouble(objRdr["FEE_TABLE_AMT"].ToString());
        //                    dTotalFeeTableAmt += Functions.FormatAmount(structCEOBDetailRecord.FeeTableAmount);

        //                    // ... From/To/Invoice # of Split
        //                    objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit",false);
        //                    try
        //                    {
        //                        objFundsTransSplit.MoveTo(Conversion.ConvertStrToInteger(objRdr["FUNDS_SPLIT_ROW_ID"].ToString())); 
        //                        structCEOBDetailRecord.ProcedureDate = objFundsTransSplit.FromDate;
        //                        structCEOBDetailRecord.ProcToDate = objFundsTransSplit.ToDate;     
        //                        structCEOBDetailRecord.InvoiceNumber = objFundsTransSplit.InvoiceNumber;
        //                        structCEOBDetailRecord.InvoiceDate = objFundsTransSplit.InvoiceDate;

        //                    }
        //                    catch
        //                    {
        //                        structCEOBDetailRecord.ProcedureDate = string.Empty;
        //                        structCEOBDetailRecord.ProcToDate = string.Empty;     
        //                        structCEOBDetailRecord.InvoiceNumber = "N/A";
        //                        structCEOBDetailRecord.InvoiceDate = string.Empty;
        //                    }

        //                    sStrTemp = objRdr["BILLING_CODE_TEXT"].ToString();

        //                    if(sStrTemp.IndexOf('-') != -1)
        //                    {
        //                        structCEOBDetailRecord.ProcedureCode = sStrTemp.Substring(0,sStrTemp.IndexOf('-') - 1);
        //                        structCEOBDetailRecord.Procedure = sStrTemp.Substring(sStrTemp.IndexOf('-') + 1);
        //                    }
        //                    else
        //                    {
        //                        structCEOBDetailRecord.ProcedureCode = sStrTemp;
        //                        structCEOBDetailRecord.Procedure = sStrTemp;
        //                    }

        //                    structCEOBDetailRecord.AmountBilled = Conversion.ConvertStrToDouble(objRdr["AMOUNT_BILLED"].ToString());
        //                    dTotalAmountBilled += structCEOBDetailRecord.AmountBilled;

        //                    structCEOBDetailRecord.AmountPaid = Conversion.ConvertStrToDouble(objRdr["AMOUNT_TO_PAY"].ToString());
        //                    dTotalAmountPaid += structCEOBDetailRecord.AmountPaid;

        //                    // Add detail record to collection
        //                    p_structCEOBRecord.DetailRecordsList.Add(++intRecordCount,structCEOBDetailRecord);    
        //                }
        //            }
        //            objRdr.Close();
        //        }

        //        p_structCEOBRecord.TotalAmountBilled = dTotalAmountBilled;
        //        p_structCEOBRecord.TotalAmountPaid = dTotalAmountPaid;
        //        p_structCEOBRecord.TotalScheduledAmount = dTotSchdAmt;
        //        p_structCEOBRecord.TotalAmountReduced = dTotAmtReduced;
        //        p_structCEOBRecord.TotalAmountSaved = dTotAmtSaved;
        //        p_structCEOBRecord.TotalBaseAmount = dTotalBaseAmount;
        //        p_structCEOBRecord.TotalAmountAllowed = dTotalAmountAllowed;
        //        p_structCEOBRecord.TotalDiscountAmount = dTotalDiscountAmount;
        //        p_structCEOBRecord.TotalPerDiemAmt = dTotalPerDiemAmt;
        //        p_structCEOBRecord.TotalStopLossAmt = dTotalStopLossAmt;
        //        p_structCEOBRecord.TotalFeeTableAmt = dTotalFeeTableAmt;
        //    }
        //    catch( DataModelException p_objEx )
        //    {
        //        throw p_objEx ;
        //    }
        //    catch( RMAppException p_objEx )
        //    {
        //        throw p_objEx ;
        //    }
        //    catch( Exception p_objEx )
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBAR.GetEOBDetail.ErrorGet") , p_objEx );				
        //    }			
        //    finally
        //    {
        //        if (objReader != null)
        //        {
        //            objReader.Close();
        //            objReader.Dispose();
        //        }

        //        if (objRdr != null)
        //        {
        //            objRdr.Close();
        //            objRdr.Dispose();
        //        }

        //        if(objFundsTransSplit != null)
        //            objFundsTransSplit.Dispose();
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends
        #endregion

        #region "GetDefaultEOBDetail(int p_iInvoiceId, string p_sOtherName, string p_sClaimantName, bool p_bIsClaimantCopy)"
        /// Name		: GetDefaultEOBDetail
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Get the Print EOB detail default items and fill into respective structure for printing EOB active report
		/// </summary>
		/// <param name="p_iInvoiceId">Invloice Id</param>
		/// <param name="p_sOtherName">Other Name</param>
		/// <param name="p_sClaimantName">Claimant Name</param>
		/// <param name="p_bIsClaimantCopy">Flag for getting the details of claimant</param>
        // akaushik5 Commented for MITS 35846 Starts
        //private void GetDefaultEOBDetail(int p_iInvoiceId, string p_sOtherName, string p_sClaimantName, bool p_bIsClaimantCopy)
        //{
        //    string sSQL = string.Empty;
        //    StringBuilder objSQL = null;
        //    bool bIsPrintTotals = false;
        //    bool bIsUseRecord = false;
        //    DbReader objReader = null;
        //    DbReader objTempReader = null;
        //    ArrayList arrEOBLine = null;
        //    FundsTransSplit objFundsTransSplit = null;
        //    long lTempId = 0;
        //    string sTempData = string.Empty;
        //    int iNumEOB = 0;
        //    double dAvgTextHeight = 0.0;
        //    double dAvgPrintWidth = 0.0;
        //    double dTotalAmountBilled = 0.0;
        //    double dTotalAmountPaid = 0.0;
        //    double dCurrX = 0;
        //    double dCurrY = 0;
        //    double dCurrentEOBX = 0.0;
        //    string sFromDate = string.Empty;
        //    string sToDate = string.Empty;
        //    string sInvoiceNumber = string.Empty;
        //    string sInvoiceDate = string.Empty;
        //    string sInvoiceBy = string.Empty;

        //    try
        //    {
        //        if (p_bIsClaimantCopy)
        //        {
        //            dCurrY = 2000;
        //            dCurrX = 500;
        //        }
        //        else
        //        {
        //            dCurrY = 4900;
        //            dCurrX = 500;
        //        }

        //        dAvgTextHeight = m_objPrintWrapper.GetTextHeight("W");
        //        dAvgPrintWidth = m_objPrintWrapper.PageWidth;

        //        m_objPrintWrapper.CurrentX = dCurrX;
        //        m_objPrintWrapper.CurrentY = dCurrY;

        //        if (p_sOtherName == string.Empty)
        //        {
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Othername1"));
        //            m_objPrintWrapper.CurrentX = dCurrX;
        //            dCurrY += dAvgTextHeight;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Othername2"));

        //        }
        //        else
        //        {
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Othername3") + Functions.FillSpace(1) + p_sOtherName + Functions.FillSpace(1) + Globalization.GetString("PrintEOBAR.EOBBody.Othername4"));
        //            m_objPrintWrapper.CurrentX = dCurrX;
        //            dCurrY += dAvgTextHeight;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Othername5"));
        //        }

        //        m_objPrintWrapper.CurrentX = dCurrX;
        //        dCurrY += dAvgTextHeight;
        //        dCurrY += dAvgTextHeight * 2;
        //        m_objPrintWrapper.CurrentY = dCurrY;
        //        m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.ItemsText"));

        //        m_objPrintWrapper.CurrentX = dCurrX;
        //        dCurrY += dAvgTextHeight * 2;
        //        m_objPrintWrapper.CurrentY = dCurrY;
        //        m_objPrintWrapper.SetFontBold(true);

        //        //Tab(6); "INVOICE"; Tab(21); "PROCEDURE"; Tab(32); "PROC."; Tab(39); "EXPLANATION"; Tab(66); "AMOUNT"; Tab(78); "AMOUNT"
        //        m_objPrintWrapper.PrintText("INVOICE");
        //        m_objPrintWrapper.CurrentX = dCurrX + 1600;
        //        m_objPrintWrapper.PrintText("PROCEDURE");
        //        m_objPrintWrapper.CurrentX = dCurrX + 3200;
        //        m_objPrintWrapper.PrintText("PROC.");
        //        m_objPrintWrapper.CurrentX = dCurrX + 4100;
        //        m_objPrintWrapper.PrintText("EXPLANATION");
        //        m_objPrintWrapper.CurrentX = dCurrX + 6800;
        //        m_objPrintWrapper.PrintText("AMOUNT");
        //        m_objPrintWrapper.CurrentX = dCurrX + 8200;
        //        m_objPrintWrapper.PrintText("AMOUNT");
        //        m_objPrintWrapper.CurrentX = dCurrX;
        //        m_objPrintWrapper.CurrentY = dCurrY + dAvgTextHeight;

        //        //Tab(6); "NUMBER"; Tab(21); "DATE"; Tab(32); "CODE"; Tab(66); "BILLED"; Tab(78); "PAID"
        //        m_objPrintWrapper.PrintText("NUMBER");
        //        m_objPrintWrapper.CurrentX = dCurrX + 1600;
        //        m_objPrintWrapper.PrintText("DATE");
        //        m_objPrintWrapper.CurrentX = dCurrX + 3200;
        //        m_objPrintWrapper.PrintText("CODE");
        //        m_objPrintWrapper.CurrentX = dCurrX + 6800;
        //        m_objPrintWrapper.PrintText("BILLED");
        //        m_objPrintWrapper.CurrentX = dCurrX + 8200;
        //        m_objPrintWrapper.PrintText("PAID");
        //        m_objPrintWrapper.CurrentX = dCurrX;
        //        dCurrY += dAvgTextHeight * 2;
        //        m_objPrintWrapper.CurrentY = dCurrY;
        //        m_objPrintWrapper.SetFontBold(false);

        //        objSQL = new StringBuilder();
        //        objSQL.Append("SELECT INVOICE_DETAIL_ID,BILLING_CODE_TEXT,AMOUNT_BILLED,AMOUNT_TO_PAY,FUNDS_SPLIT_ROW_ID");
        //        objSQL.Append(",TABLE_CODE, PERCENTILE,PLACE_OF_SER_CODE,TYPE_OF_SER_CODE,UNITS_BILLED_NUM");
        //        objSQL.Append(",SCHEDULED_AMOUNT,AMOUNT_REDUCED,AMOUNT_SAVED, QUANTITY,STORE");
        //        objSQL.Append(",CONTRACT_AMOUNT,DISCOUNT,ZIP_CODE,BASE_AMOUNT,AMOUNT_ALLOWED");
        //        objSQL.Append(",PER_DIEM_AMT,STOP_LOSS_AMT,FEE_TABLE_AMT");
        //        objSQL.Append(" FROM INVOICE_DETAIL, FUNDS_TRANS_SPLIT WHERE INVOICE_ID = " + p_iInvoiceId);
        //        objSQL.Append(" AND FUNDS_SPLIT_ROW_ID = SPLIT_ROW_ID ORDER BY FROM_DATE");

        //        objReader = DbFactory.GetDbReader(m_sConnectionString, objSQL.ToString());

        //        if (objReader != null)
        //        {
        //            while (objReader.Read())
        //            {
        //                bIsUseRecord = false;

        //                //At first check if record is deleted or not
        //                lTempId = Conversion.ConvertStrToLong(objReader["FUNDS_SPLIT_ROW_ID"].ToString());

        //                foreach (FundsTransSplit objFundsTSplit in m_objFunds.TransSplitList)
        //                {
        //                    if (objFundsTSplit.SplitRowId == lTempId)
        //                    {
        //                        bIsUseRecord = true;
        //                        break;
        //                    }
        //                }

        //                iNumEOB = 0;

        //                if (bIsUseRecord)
        //                {
        //                    bIsPrintTotals = true;
        //                    lTempId = Conversion.ConvertStrToLong(objReader["INVOICE_DETAIL_ID"].ToString());

        //                    sSQL = "SELECT CODES_TEXT.CODE_DESC FROM INVDETAIL_X_EOB,CODES_TEXT WHERE " +
        //                        "INVDETAIL_X_EOB.EOB_CODE = CODES_TEXT.CODE_ID AND " +
        //                        "INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + lTempId;
        //                    objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

        //                    arrEOBLine = new ArrayList();

        //                    //if(objTempReader != null)
        //                    //{
        //                    //    while(objTempReader.Read())
        //                    //    {
        //                    //        sTempData = objTempReader["CODE_DESC"].ToString();
        //                    //        arrEOBLine.Add(sTempData);
        //                    //        iNumEOB++;
        //                    //    }
        //                    //    objTempReader.Close();
        //                    //}

        //                    //MITS  :12090  Umesh
        //                    if (objTempReader != null)
        //                    {
        //                        while (objTempReader.Read())
        //                        {
        //                            string sTempEob = "";
        //                            sTempData = objTempReader["CODE_DESC"].ToString().Trim();

        //                            sTempData = "- " + sTempData; //Starting of New EOB


        //                            //iNumEOB++;
        //                            if (sTempData.Length > 23)
        //                            {

        //                                int iStartIndex = 0;
        //                                int iLengthFix = 23;
        //                                int iLength = 0;
        //                                int iLastIndexofSpace = 0;
        //                                string sNextChar = "";
        //                                while (iStartIndex < sTempData.Length)
        //                                {
        //                                    iNumEOB++;

        //                                    //code to handle the boundary conditions
        //                                    if (iStartIndex + iLengthFix + 1 <= sTempData.Length)
        //                                    {
        //                                        //find the next character
        //                                        //if it is space , last word will be in same line

        //                                        sNextChar = sTempData.Substring(iStartIndex + iLengthFix, 1);
        //                                        if (sNextChar == " ")
        //                                        {

        //                                            sTempEob = sTempData.Substring(iStartIndex, iLengthFix - 1);
        //                                            arrEOBLine.Add(sTempEob);
        //                                        }
        //                                        //else last word will come into next line. so remove the last word from current line.
        //                                        else
        //                                        {
        //                                            sTempEob = sTempData.Substring(iStartIndex, iLengthFix - 1);
        //                                            iLastIndexofSpace = sTempEob.LastIndexOf(" ");
        //                                            iLength = iLastIndexofSpace;
        //                                            sTempEob = sTempData.Substring(iStartIndex, iLength);
        //                                            arrEOBLine.Add(sTempEob);


        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        sTempEob = sTempData.Substring(iStartIndex, iLengthFix - 1);
        //                                        arrEOBLine.Add(sTempEob);
        //                                    }
        //                                    iStartIndex += iLength;
        //                                    //get the lenght of remaining string.
        //                                    if (iStartIndex + 23 >= sTempData.Length)
        //                                        iLengthFix = sTempData.Length - iStartIndex;

        //                                }

        //                            }


        //                            else
        //                            {
        //                                iNumEOB++;
        //                                arrEOBLine.Add(sTempData.Trim());
        //                            }


        //                        }
        //                        objTempReader.Close();
        //                    }

        //                    //*** Default body ***
        //                    //Call subCheckEOBPageLength(13500)  ' Checks to see if new page is needed

        //                    objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit", false);
        //                    try
        //                    {
        //                        objFundsTransSplit.MoveTo(Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString()));
        //                        sFromDate = Conversion.ToDate(objFundsTransSplit.FromDate).ToShortDateString();
        //                        sToDate = Conversion.ToDate(objFundsTransSplit.ToDate).ToShortDateString();
        //                        sInvoiceNumber = objFundsTransSplit.InvoiceNumber;
        //                        sInvoiceDate = Conversion.ToDate(objFundsTransSplit.InvoiceDate).ToShortDateString();
        //                        sInvoiceBy = objFundsTransSplit.InvoicedBy;
        //                        objFundsTransSplit.Dispose();
        //                    }
        //                    catch
        //                    {
        //                        sFromDate = string.Empty;
        //                        sToDate = string.Empty;
        //                        sInvoiceNumber = "N/A";
        //                        sInvoiceDate = string.Empty;
        //                        sInvoiceBy = string.Empty;
        //                    }

        //                    m_objPrintWrapper.CurrentX = dCurrX;
        //                    m_objPrintWrapper.CurrentY = dCurrY;
        //                    m_objPrintWrapper.PrintText(sInvoiceNumber.Trim());
        //                    m_objPrintWrapper.CurrentX = dCurrX + 1600;
        //                    m_objPrintWrapper.CurrentY = dCurrY;
        //                    m_objPrintWrapper.PrintText(sFromDate);

        //                    //Print remaining data fields
        //                    sTempData = objReader["BILLING_CODE_TEXT"].ToString();
        //                    m_objPrintWrapper.CurrentX = dCurrX + 3200;
        //                    m_objPrintWrapper.CurrentY = dCurrY;
        //                    if (sTempData.Length > 6)
        //                        m_objPrintWrapper.PrintText(sTempData.Substring(0, 6));
        //                    else
        //                        m_objPrintWrapper.PrintText(sTempData);

        //                    if (iNumEOB > 0)
        //                    {
        //                        dCurrentEOBX = dCurrX + 4100;
        //                        m_objPrintWrapper.CurrentX = dCurrX + 4100;
        //                        m_objPrintWrapper.CurrentY = dCurrY;
        //                        if (arrEOBLine[0].ToString().Length > 20)
        //                            m_objPrintWrapper.PrintText(arrEOBLine[0].ToString().Substring(0, 20));
        //                        else
        //                            m_objPrintWrapper.PrintText(arrEOBLine[0].ToString());
        //                    }

        //                    //amount billed
        //                    double d = Conversion.ConvertStrToDouble(objReader["AMOUNT_BILLED"].ToString());
        //                    dTotalAmountBilled += d;
        //                    sTempData = string.Format("{0:#,###,##0.00}", d);
        //                    m_objPrintWrapper.CurrentX = dCurrX + 6800;
        //                    m_objPrintWrapper.CurrentY = dCurrY;
        //                    m_objPrintWrapper.PrintText(sTempData);

        //                    //amount to pay
        //                    d = Conversion.ConvertStrToDouble(objReader["AMOUNT_TO_PAY"].ToString());
        //                    dTotalAmountPaid += d;
        //                    sTempData = string.Format("{0:#,###,##0.00}", d);
        //                    m_objPrintWrapper.CurrentX = dCurrX + 8200;
        //                    m_objPrintWrapper.CurrentY = dCurrY;
        //                    m_objPrintWrapper.PrintText(sTempData);

        //                    //Print remaining EOB lines (see above)
        //                    if (iNumEOB > 1)
        //                    {
        //                        //for(int i = 1; i < iNumEOB; i++)
        //                        //{
        //                        //    sTempData += arrEOBLine[i].ToString();
        //                        //    sTempData += ", ";
        //                        //}
        //                        //if(sTempData.Substring(sTempData.Length - 1) == ", ")
        //                        //    sTempData = sTempData.Substring(1,sTempData.Length - 2);
        //                        //MITS 11435 : Umesh
        //                        for (int i = 1; i < iNumEOB; i++)
        //                        {
        //                            m_objFunctions.CheckEOBPageLength(13800, m_objPrintWrapper);
        //                            //m_objPrintWrapper.CurrentX = dCurrX + 900;
        //                            m_objPrintWrapper.CurrentX = dCurrentEOBX;
        //                            dCurrY += dAvgTextHeight;
        //                            m_objPrintWrapper.CurrentY = dCurrY;
        //                            m_objPrintWrapper.PrintText(arrEOBLine[i].ToString());
        //                        }
        //                    }
        //                }
        //                dCurrY += dAvgTextHeight;
        //            }//END of While
        //            objReader.Close();
        //        }//End of IF

        //        if (bIsPrintTotals)
        //        {
        //            sTempData = Functions.FormatAmount(dTotalAmountBilled).ToString();
        //            m_objPrintWrapper.SetFontBold(true);
        //            m_objPrintWrapper.CurrentX = dCurrX + 2900;
        //            dCurrY = dCurrY + dAvgTextHeight * 2;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText("TOTALS:");
        //            m_objPrintWrapper.CurrentX = dCurrX + 6800;
        //            m_objPrintWrapper.PrintText(sTempData);
        //            sTempData = Functions.FormatAmount(dTotalAmountPaid).ToString();
        //            m_objPrintWrapper.CurrentX = dCurrX + 8200;
        //            m_objPrintWrapper.PrintText(sTempData);
        //            m_objPrintWrapper.SetFontBold(false);
        //        }

        //        m_objPrintWrapper.CurrentX = dCurrX;
        //        dCurrY += dAvgTextHeight * 3;
        //        m_objPrintWrapper.CurrentY = dCurrY;
        //        m_objFunctions.CheckEOBPageLength(12300, m_objPrintWrapper); ;
        //        m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.ContactUs"));
        //        m_objPrintWrapper.CurrentX = dCurrX;
        //        dCurrY += dAvgTextHeight * 2;
        //        m_objPrintWrapper.CurrentY = dCurrY;
        //        m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Sincerely"));
        //        m_objPrintWrapper.CurrentX = dCurrX;
        //        dCurrY += dAvgTextHeight * 2;
        //        m_objPrintWrapper.CurrentY = dCurrY;
        //        m_objPrintWrapper.PrintText(m_objCommon.m_stLoginInfo.Username);

        //        if (!p_bIsClaimantCopy)
        //        {
        //            m_objPrintWrapper.CurrentX = dCurrX + 2500;
        //            dCurrY += dAvgTextHeight * 2;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.ClaimantCopy"));
        //            m_objPrintWrapper.CurrentX = dCurrX + 2500;
        //            dCurrY += dAvgTextHeight * 2;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Cc") + Functions.FillSpace(1) + p_sClaimantName);
        //        }
        //    }
        //    catch (DataModelException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (RMAppException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBAR.GetDefaultEOBDetail.ErrorGet"), p_objEx);
        //    }
        //    finally
        //    {
        //        if (objSQL != null)
        //            objSQL = null;

        //        if (objReader != null)
        //            objReader.Dispose();

        //        if (objTempReader != null)
        //            objTempReader.Dispose();

        //        if (objFundsTransSplit != null)
        //            objFundsTransSplit.Dispose();

        //        if (arrEOBLine != null)
        //            arrEOBLine = null;
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends
		#endregion

        #region "GetDefaultEOBHeader(string p_sRegClaimantName,string p_sClaimantName,string p_sClaimantAddr1,string p_sClaimantAddr2,string p_sClaimantAddr3,string p_sClaimantAddr4,string p_sClaimantCityStateZip,string p_sOtherName,string p_sClaimNumber, bool p_bIsClaimantCopy)"
        /// Name		: GetDefaultEOBHeader
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Get the Print EOB Default Format Header
		/// </summary>
		/// <param name="p_sRegClaimantName">Reg Claimant Name</param>
		/// <param name="p_sClaimantName">Claimant Name</param>
		/// <param name="p_sClaimantAddr1">Claimant Address1</param>
		/// <param name="p_sClaimantAddr2">Claimant Address1</param>
		/// <param name="p_sClaimantCityStateZip">Claimant City, State, Zip</param>
		/// <param name="p_sOtherName">Other Name</param>
		/// <param name="p_sClaimNumber">Claim Number</param>
		/// <param name="p_bIsClaimantCopy">Claimant Copy</param>
        // akaushik5 Commented for MITS 35846 Starts
        //private void GetDefaultEOBHeader(string p_sRegClaimantName, string p_sClaimantName, string p_sClaimantAddr1, string p_sClaimantAddr2, string p_sClaimantCityStateZip, string p_sOtherName, string p_sClaimNumber, bool p_bIsClaimantCopy)
        //{
        //    double dAvgTextHeight = 0.0;
        //    double dCurrX = 0;
        //    double dCurrY = 0;

        //    dAvgTextHeight = m_objPrintWrapper.GetTextHeight("W");

        //    try
        //    {
        //        if (!p_bIsClaimantCopy)
        //        {
        //            m_objPrintWrapper.SetFont(12);
        //            m_objPrintWrapper.SetFontBold(true);
        //            m_objPrintWrapper.CurrentY = 1250;
        //            m_objPrintWrapper.CurrentX = 3850;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBHeader.EOB"));
        //        }

        //        m_objPrintWrapper.SetFont(10);
        //        m_objPrintWrapper.SetFontBold(false);

        //        if (p_bIsClaimantCopy)
        //        {
        //            m_objPrintWrapper.CurrentX = dCurrX;
        //            dCurrY += 800;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.RE"));
        //            m_objPrintWrapper.CurrentX = dCurrX + 500;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText") + Functions.FillSpace(1) + p_sClaimNumber);
        //            m_objPrintWrapper.CurrentX = dCurrX + 500;
        //            dCurrY += dAvgTextHeight;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName") + Functions.FillSpace(1) + p_sClaimantName);
        //            m_objPrintWrapper.CurrentX = dCurrX;
        //            m_objPrintWrapper.CurrentY = dCurrY + dAvgTextHeight * 2;
        //            dCurrY += dAvgTextHeight;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.Name") + Functions.FillSpace(1) + p_sOtherName + ",");
        //        }
        //        else
        //        {
        //            dCurrX = 500;
        //            dCurrY = 2000;
        //            m_objPrintWrapper.CurrentX = dCurrX;
        //            m_objPrintWrapper.CurrentY = dCurrY;

        //            m_objPrintWrapper.PrintText(DateTime.Now.ToLongDateString());
        //            m_objPrintWrapper.CurrentX = dCurrX;
        //            dCurrY += dAvgTextHeight * 4;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText(p_sRegClaimantName);
        //            m_objPrintWrapper.CurrentX = dCurrX;
        //            dCurrY += dAvgTextHeight;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText(p_sClaimantAddr1);
        //            if (p_sClaimantAddr2 != string.Empty)
        //            {
        //                m_objPrintWrapper.CurrentX = dCurrX;
        //                dCurrY += dAvgTextHeight;
        //                m_objPrintWrapper.CurrentY = dCurrY;
        //                m_objPrintWrapper.PrintText(p_sClaimantAddr1);
        //            }
        //            m_objPrintWrapper.CurrentX = dCurrX;
        //            dCurrY += dAvgTextHeight;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText(p_sClaimantCityStateZip);

        //            m_objPrintWrapper.CurrentX = dCurrX;
        //            dCurrY += dAvgTextHeight;
        //            m_objPrintWrapper.CurrentY = dCurrY;

        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.RE"));
        //            m_objPrintWrapper.CurrentX = dCurrX + 500;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText") + Functions.FillSpace(1) + p_sClaimNumber);
        //            m_objPrintWrapper.CurrentX = dCurrX + 500;
        //            dCurrY += dAvgTextHeight;
        //            m_objPrintWrapper.CurrentY = dCurrY;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName") + Functions.FillSpace(1) + p_sClaimantName);
        //            m_objPrintWrapper.CurrentX = dCurrX;
        //            m_objPrintWrapper.CurrentY = dCurrY + dAvgTextHeight * 2;
        //            dCurrY += dAvgTextHeight;
        //            m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.Name") + Functions.FillSpace(1) + p_sOtherName + ",");
        //        }

        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBAR.GetDefaultEOBHeader.ErrorGet"), p_objEx);
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends
        #endregion

		#region "IsFieldExists(DbReader p_objDBReader,string p_sFieldName)"
		/// Name		: IsFieldExists
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Checks for the input field existance in the passed list of columns
		/// </summary>
		/// <param name="p_objDBReader">Data Reader object containing the recordset</param>
		/// <param name="p_sFieldName">Field Name to be check</param>
		/// <returns>Flag saying that whether field exist or not</returns>
		private bool IsFieldExists(DbReader p_objDBReader,string p_sFieldName)
		{
			DataTable objDataTable;
			try
			{
				if(p_objDBReader != null)
				{
					//Deb Underwriters
                    //objDataTable = p_objDBReader.GetSchemaTable(); 
                    //if(objDataTable != null)
                    //{
                    //    foreach (DataColumn objDataColumn in objDataTable.Columns)
                    //    {
                    //        if(objDataColumn.ColumnName == p_sFieldName)
                    //            return true;
                    //    }
                    //}
                    int _columncount = p_objDBReader.FieldCount;
                    for (System.Int32 iCol = 0; iCol < _columncount; iCol++)
                    {
                        if (p_objDBReader.GetName(iCol) == p_sFieldName)
                        {
                            return true;
                        }
                    }
				}
				return false;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintEOBAR.IsFieldExists.CompareError", m_iClientId), p_objEx);				
			}
		}
		#endregion

        //#region "IsSuppFieldExist(DbReader p_objDBReader,string p_sFieldName)"
        ///// Name		: IsSuppFieldExist
        ///// Author		: Anurag Agarwal
        ///// Date Created	: 10 jan 2005		
        ///// ************************************************************
        ///// Amendment History
        ///// ************************************************************
        ///// Date Amended   *   Amendment   *    Author
        /////                *               *
        /////                *               *
        ///// ************************************************************
        ///// <summary>
        ///// Checks for the input field existance in the passed list of columns
        ///// </summary>
        ///// <param name="p_sFieldName">Field Name to be check</param>
        ///// <param name="p_objSupplementals">List of supplimental columns</param>
        ///// <returns>Flag saying that whether field exist or not</returns>
        // akaushik5 Commented for MITS 35846 Starts
        //private bool IsSuppFieldExist(Supplementals p_objSupplementals, string p_sFieldName)
        //{
        //    try
        //    {
        //        foreach(SupplementalField objSupplementalField in p_objSupplementals)
        //        {
        //            if(objSupplementalField.FieldName == p_sFieldName)
        //                return true;
        //        }
        //        return false;
        //    }
        //    catch( DataModelException p_objEx )
        //    {
        //        throw p_objEx ;
        //    }
        //    catch( Exception p_objEx )
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBAR.Initialize.ErrorInit") , p_objEx );				
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends
        // #endregion

        #region "GetDefaultPayeeHTMLTemplate(string sName, string sAddr1, string sAddr2, string sAddr3,string sAddr4, string strStateCode, string strStateDesc,string sCity, string sRegClaimantName, string sClaimantAddr1, string sClaimantAddr2, string sClaimantAddr3,string sClaimantAddr4, string sClaimantCity, string sClaimNumberText,string sClaimantName)"
        /// <summary>
        /// The function returns the default payee HTML template
        /// Author: Debabrata Biswas MITS Issue 17183
        /// Date:   30 july 2009
        /// </summary>
        /// <returns>sDefaultPayeeHTMLtext</returns>
        // akaushik5 Commented for MITS 35846 Starts
        //private StringBuilder GetDefaultPayeeHTMLTemplate(string sName, string sAddr1, string sAddr2, string strStateCode, string strStateDesc,
        //    string sCity, string sRegClaimantName, string sClaimantAddr1, string sClaimantAddr2, string sClaimantCity, string sClaimNumberText,
        //    string sClaimantName)
        //{
        //    try
        //    {
        //        StringBuilder sDefaultPayeeHTMLtext = new StringBuilder(null);

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<HTML>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<HEAD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</HEAD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BODY>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<CENTER>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<H1>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBHeader.EOB"));
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</H1>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</CENTER>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(DateTime.Now.ToLongDateString());
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TABLE>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sName.Trim() + "</TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sAddr1 + "</TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
        //        if (sAddr2 != string.Empty)
        //        {
        //            sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
        //            sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sAddr2 + "</TD>");
        //            sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
        //        }

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sCity + ", " + ((string)(strStateCode + " " + strStateDesc)).Trim() + " " + m_structEOBRecord.PayeePostalCode + "</TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sRegClaimantName + " " + sClaimantAddr1 + " " + " " + sClaimantAddr2 + " " + sClaimantCity + "</TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TABLE>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBReport.RE") + " " + Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText") + " " + sClaimNumberText);
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName") + " " + sClaimantName);
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBReport.Name") + " " + sRegClaimantName);
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");

        //        if (sName == string.Empty)
        //        {
        //            sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername1"));
        //            sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //            sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername2"));
        //            sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        }
        //        else
        //        {
        //            sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername3") + " " + sName + " " + Globalization.GetString("PrintEOBAR.EOBBody.Othername4"));
        //            sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //            sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername5"));
        //            sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        }
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.ItemsText"));
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");

        //        //Dynamic Multirow table
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TABLE CELLPADDING=\"1\" BORDER=\"1\" BORDERCOLOR=\"WHITE\">");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>INVOICE NUMBER</B>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>PROCEDURE DATE</B>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>PROC. CODE</B>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>EXPLANATION</B>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD ALIGN=\"RIGHT\">");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>AMOUNT BILLED</B>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD ALIGN=\"RIGHT\">");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>AMOUNT PAID</B>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR TYPE=\"MULTIROW\">");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD VALIGN=\"TOP\">");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{INVOICE_NUMBER}");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD VALIGN=\"TOP\">");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{PROCEDURE_DATE}");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD VALIGN=\"TOP\">");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{PROCEDURE_CODE}");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD VALIGN=\"TOP\">");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{EXPLANATION}");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD ALIGN=\"RIGHT\" VALIGN=\"TOP\">");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{AMOUNT_BILLED}");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD ALIGN=\"RIGHT\" VALIGN=\"TOP\">");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{AMOUNT_PAID}");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>TOTAL</B>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD STYLE=\"FONT-WEIGHT: BOLD; TEXT-ALIGN: RIGHT;\">");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{TOTAL_AMOUNT_BILLED}");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD STYLE=\"FONT-WEIGHT: BOLD; TEXT-ALIGN: RIGHT;\">");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{TOTAL_AMOUNT_PAID}");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TABLE>");

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.ContactUs"));
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("Sincerely,");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(m_objCommon.m_stLoginInfo.Username);

        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.ClaimantCopy"));
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Cc") + " " + sClaimantName);
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</BODY>");
        //        sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</HTML>");

        //        return sDefaultPayeeHTMLtext;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.GetDefaultPayeeHTMLTemplate"), p_objEx);
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends
        #endregion

        #region "GetDefaultClaimantHTMLTemplate(string sClaimNumberText, string sClaimantName, string sName)"
        /// <summary>
        /// Author: Debabrata Biswas MITS Issue 17183
        /// Date:   30 july 2009
        /// This function returns the default Claimant HTML template
        /// </summary>
        /// <returns></returns>
        // akaushik5 Commented for MITS 35846 Starts
        //private StringBuilder GetDefaultClaimantHTMLTemplate(string sClaimNumberText, string sClaimantName, string sName)
        //{
        //    try
        //    {
        //        StringBuilder sDefaultClaimantHTMLtext = new StringBuilder(null);

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<HTML>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<HEAD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</HEAD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BODY>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBReport.RE") + " " + Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText") + " " + sClaimNumberText);
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName") + " " + sClaimantName);
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBReport.Name") + " " + sName);
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");

        //        if (sName == string.Empty)
        //        {
        //            sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername1"));
        //            sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
        //            sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername2"));
        //            sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
        //        }
        //        else
        //        {
        //            sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername3") + " " + sName + " " + Globalization.GetString("PrintEOBAR.EOBBody.Othername4"));
        //            sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
        //            sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername5"));
        //            sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
        //        }
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.ItemsText"));
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");

        //        //Dynamic Multirow table
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TABLE CELLPADDING=\"1\" BORDER=\"1\" BORDERCOLOR=\"WHITE\">");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TR>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>INVOICE NUMBER</B>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>PROCEDURE DATE</B>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>PROC. CODE</B>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>EXPLANATION</B>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>AMOUNT BILLED</B>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>AMOUNT PAID</B>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TR>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TR TYPE=\"MULTIROW\">");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{INVOICE_NUMBER}");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{PROCEDURE_DATE}");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{PROCEDURE_CODE}");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{EXPLANATION}");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD ALIGN=\"RIGHT\">");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{AMOUNT_BILLED}");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD ALIGN=\"RIGHT\">");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{AMOUNT_PAID}");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TR>");


        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TR>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>TOTAL</B>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD STYLE=\"FONT-WEIGHT: BOLD; TEXT-ALIGN: RIGHT;\">");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{TOTAL_AMOUNT_BILLED}");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD STYLE=\"FONT-WEIGHT: BOLD; TEXT-ALIGN: RIGHT;\">");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{TOTAL_AMOUNT_PAID}");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TR>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TABLE>");

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.ContactUs"));
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("Sincerely,");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(m_objCommon.m_stLoginInfo.Username);

        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</BODY>");
        //        sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</HTML>");

        //        return sDefaultClaimantHTMLtext;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.GetDefaultClaimantHTMLTemplate"), p_objEx);
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends
        #endregion

        #region "ProcessMultiRowEOBHTMLtables(string sHTML)"
        /// <summary>
        /// Author: Debabrata Biswas MITS Issue 17183
        /// This section handles multirow html table.
        /// </summary>
        /// <param name="sHTML"></param>
        /// <param name="p_iInvoiceId"></param>
        /// <returns></returns>
        // akaushik5 Commented for MITS 35846 Starts
        //private XmlDocument ProcessMultiRowEOBHTMLtables(StringBuilder sHTML, int p_iInvoiceId)
        //{
        //    XmlDocument xHTMLdocDOM;
        //    XmlNodeList xMultiRowTR;
        //    XmlNodeList xTD;
        //    XmlNode xTempNode;

        //    string sTableColumns = string.Empty;
        //    string sTableRows = string.Empty;
        //    string sAttribAndValue = string.Empty;
        //    string sTempCol = string.Empty;
        //    string[] sTempArray;
        //    string[] sTableRowArray;
        //    string sLength = "0";

        //    try
        //    {
        //        xHTMLdocDOM = new XmlDocument();
        //        xHTMLdocDOM.LoadXml(sHTML.ToString());
        //        xMultiRowTR = xHTMLdocDOM.SelectNodes("//TABLE/TR[@TYPE='MULTIROW']");

        //        foreach (XmlNode xTR in xMultiRowTR)
        //        {
        //            xTD = xTR.SelectNodes("./TD");

        //            foreach (XmlNode xEOBField in xTD)
        //            {
        //                sAttribAndValue = "";
        //                foreach (XmlAttribute xAttrb in xEOBField.Attributes)
        //                {
        //                    if (sAttribAndValue == "")
        //                        sAttribAndValue = xAttrb.Name + "=" + "\"" + xAttrb.Value + "\"";
        //                    else
        //                        sAttribAndValue = sAttribAndValue + " " + xAttrb.Name + "=" + "\"" + xAttrb.Value + "\"";
        //                }

        //                sTempCol = xEOBField.InnerText;
        //                sTempCol = sTempCol.Substring(1, sTempCol.Length - 2);
        //                sLength = "0";

        //                if (sTempCol.IndexOf(',') != -1)
        //                {
        //                    sTempArray = sTempCol.Split(',');
        //                    sTempCol = sTempArray[0];
        //                    sLength = sTempArray[1];
        //                }

        //                sTempCol = "{" + sTempCol + "," + sLength + "," + sAttribAndValue + "}";

        //                if (sTableColumns == string.Empty)
        //                    sTableColumns = sTempCol;
        //                else
        //                    sTableColumns = sTableColumns + "|" + sTempCol;
        //            }
        //            sTableRows = this.PrintEOBHTMLTABLE(p_iInvoiceId, sTableColumns);
        //            sTableRowArray = sTableRows.Split('|');
        //            Array.Reverse(sTableRowArray);

        //            xTR.InnerXml = "";

        //            foreach (string sTemp in sTableRowArray)
        //            {
        //                xTempNode = xHTMLdocDOM.CreateElement("TR");
        //                xTempNode.InnerXml = sTemp.Replace("&", "&amp;");
        //                xTR.ParentNode.InsertAfter(xTempNode, xTR);
        //            }

        //            sTableColumns = string.Empty;
        //        }
        //        return xHTMLdocDOM;
        //    }
        //    //Added for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
        //    catch (RMAppException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    //Added for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.ProcessMultiRowEOBHTMLtables"), p_objEx);
        //    }
        //    finally
        //    {
        //        xHTMLdocDOM = null;
        //        xMultiRowTR = null;
        //        xTD = null;
        //        sTempArray = null;
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends
        #endregion

        #region "CreateHTMLdocument(xHTMLdom,m_objFunds)"
        /// <summary>
        /// Author: Debabrata Biswas MITS Issue 17183
        /// This section outputs the complete HTML structure.
        /// </summary>
        /// <param name="xHTMLdom"></param>
        /// <param name="m_structEOBRecord"></param>
        /// <param name="m_structEOBDetailRecord"></param>
        /// <param name="m_objFunds"></param>
        /// <returns></returns>
        // akaushik5 Commented for MITS 35846 Starts
        //private XmlDocument CreateHTMLdocument(XmlDocument xHTMLdom, EOBRecord m_structEOBRecord, EOBDetailRecord m_structEOBDetailRecord, Funds m_objFunds)
        //{
        //    XmlDocument xTempHTMLdom;
        //    MemoryStream p_MemoryStream;
        //    StreamReader p_objSReader;
        //    string sCurrentToken;
        //    string sProcessedToken;
        //    string sHTMLdom = string.Empty;
        //    int iNextInput;

        //    try
        //    {
        //        p_MemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xHTMLdom.InnerXml));
        //        p_objSReader = new StreamReader(p_MemoryStream);

        //        while ((iNextInput = p_objSReader.Peek()) != -1)
        //        {
        //            sCurrentToken = m_objFunctions.GetCurrentHTMLToken(p_objSReader, '<', '>');

        //            if (sCurrentToken.StartsWith("{") && sCurrentToken.EndsWith("}"))
        //                sProcessedToken = m_objFunctions.ProcessHTMLToken(sCurrentToken, 0, m_structEOBRecord, m_structEOBDetailRecord, m_objFunds);
        //            else
        //                sProcessedToken = sCurrentToken;

        //            sHTMLdom += sProcessedToken;
        //        }

        //        xTempHTMLdom = new XmlDocument();
        //        xTempHTMLdom.LoadXml(sHTMLdom.ToString().Replace("&amp;", "&").Replace("&", "&amp;"));

        //        return xTempHTMLdom;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.CreateHTMLdocument"), p_objEx);
        //    }
        //    finally
        //    {
        //        xTempHTMLdom = null;
        //        p_MemoryStream = null;
        //        p_objSReader = null;
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends
        #endregion

        #region "PrintEOBHTMLTABLE(int p_iInvoiceId, string p_sClaimNumberText, string p_sClaimantName, string p_sName, string p_sOtherName, bool p_bIsFormatFileFound, int p_iCurrentX, int p_iCurrentY)"
        /// Name		: PrintEOBHTMLTABLE
        /// Author		: Debabrata Biswas MITS Issue 17183
        /// Date Created	: 24 Juk 2009		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Print EOB; Introduction And Body.Format; File Is the; Custom; Files; of
        /// http-like tags <b>.  If a file cannot be found, a Default format is used.
        /// </summary>
        /// <param name="p_iInvoiceId">Invoice Id</param>
        /// <param name="p_sClaimNumberText">Claim Number Text</param>
        /// <param name="p_sClaimantName">Claimaint Name</param>
        /// <param name="p_sName">Name</param>
        /// <param name="p_sOtherName">Other Name</param>
        /// <param name="p_bIsFormatFileFound">Flag for Format file</param>
        /// <param name="p_iCurrentX">X-axis value</param>
        /// <param name="p_iCurrentY">Y-axis value</param>
        // akaushik5 Commented for MITS 35846 Starts
        //private string PrintEOBHTMLTABLE(int p_iInvoiceId, string sTableColumns)
        //{
        //    bool bIsPrintTotals = false;
        //    bool bIsUseRecord = false;
        //    StringBuilder objSQL = null;
        //    DbReader objReader = null;
        //    DbReader objTempReader = null;
        //    FundsTransSplit objFundsTransSplit = null;
        //    int iTempId = 0;
        //    double dTempId = 0.0;
        //    int iNumEOB = 0;
        //    int iLineItem = 0;
        //    string sSQL = string.Empty;
        //    string sTempData = string.Empty;
        //    string sTempCode = string.Empty;
        //    double dTotAmtReduced = 0.0;
        //    double dTotSchdAmt = 0.0;
        //    double dTotAmtSaved = 0.0;
        //    double dTotalAmountAllowed = 0.0;
        //    double dTotalBaseAmount = 0.0;
        //    double dTotalDiscountAmount = 0.0;
        //    double dTotalPerDiemAmt = 0.0;
        //    double dTotalStopLossAmt = 0.0;
        //    double dTotalFeeTableAmt = 0.0;
        //    double dTotalAmountBilled = 0.0;
        //    double dTotalAmountPaid = 0.0;
        //    string sFromDate = string.Empty;
        //    string sToDate = string.Empty;
        //    string sInvoiceNumber = string.Empty;
        //    string sInvoiceDate = string.Empty;
        //    string sInvoiceBy = string.Empty;
        //    string sHTMLcellData = string.Empty;
        //    string sHTMLrowData = string.Empty;
        //    string sTDData = string.Empty;
        //    string[] TDList;
        //    string[] sTempFormat;
        //    string sAttributes = string.Empty;
        //    int iDataLength = 0;
        //    string sTD = string.Empty;

        //    try
        //    {
        //        objSQL = new StringBuilder();
        //        objSQL.Append("SELECT INVOICE_DETAIL_ID,BILLING_CODE_TEXT,AMOUNT_BILLED,AMOUNT_TO_PAY,FUNDS_SPLIT_ROW_ID");
        //        objSQL.Append(",TABLE_CODE, PERCENTILE,PLACE_OF_SER_CODE,TYPE_OF_SER_CODE,UNITS_BILLED_NUM");
        //        objSQL.Append(",SCHEDULED_AMOUNT,AMOUNT_REDUCED,AMOUNT_SAVED, QUANTITY,STORE, MODIFIER_CODE");
        //        objSQL.Append(",CONTRACT_AMOUNT,DISCOUNT,ZIP_CODE,BASE_AMOUNT,AMOUNT_ALLOWED");
        //        objSQL.Append(",PER_DIEM_AMT,STOP_LOSS_AMT,FEE_TABLE_AMT, PRESCRIP_NO, DRUG_NAME, PRESCRIP_DATE");
        //        //BRS FL Merge
        //        objSQL.Append(",FL_LICENSE, REV_CODE");
        //        //End
        //        objSQL.Append(" FROM INVOICE_DETAIL, FUNDS_TRANS_SPLIT WHERE INVOICE_ID = " + p_iInvoiceId);
        //        objSQL.Append(" AND FUNDS_SPLIT_ROW_ID = SPLIT_ROW_ID  ORDER BY FROM_DATE");

        //        objReader = DbFactory.GetDbReader(m_sConnectionString, objSQL.ToString());

        //        if (objReader != null)
        //        {
        //            while (objReader.Read())
        //            {
        //                m_structEOBDetailRecord.EOBList = new Hashtable();
        //                m_structEOBDetailRecord.EOBListDetails = new Hashtable();
        //                m_structEOBDetailRecord.DiagnosisList = new Hashtable();
        //                m_structEOBDetailRecord.DiagnosisCodeList = new Hashtable();
        //                m_structEOBDetailRecord.DiagnosisDetails = new Hashtable();
        //                m_structEOBDetailRecord.ModifierList = new Hashtable();

        //                TDList = sTableColumns.Split('|');

        //                bIsUseRecord = false;

        //                //At first check if record is deleted or not
        //                iTempId = Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString());

        //                foreach (FundsTransSplit objFundsTSplit in m_objFunds.TransSplitList)
        //                {
        //                    if (objFundsTSplit.SplitRowId == iTempId)
        //                    {
        //                        bIsUseRecord = true;
        //                        break;
        //                    }
        //                }

        //                iNumEOB = 0;

        //                if (bIsUseRecord)
        //                {
        //                    bIsPrintTotals = true;
        //                    iTempId = Conversion.ConvertStrToInteger(objReader["INVOICE_DETAIL_ID"].ToString());
        //                    //Added for MITS 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
        //                    bool bUseCodeDescriptionFile = Convert.ToBoolean(Functions.GetValueFromConfigFile(Functions.TAG_USEEOBDESCRIPTIONFILE));
        //                    if (bUseCodeDescriptionFile)//Added if block for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
        //                    {
        //                        string sDesciptionFileName = Functions.GetFormatFilePath()
        //                                                     + @"\" + Functions.GetValueFromConfigFile(Functions.TAG_EOBDESCRIPTIONFILE);
        //                        if (File.Exists(sDesciptionFileName))
        //                        {
        //                            string sFileText = null;
        //                            string sNewText = null;
        //                            string sOldText = null;
        //                            int iPoint = 0;

        //                            StreamReader objSRdr = null;
        //                            objSRdr = new StreamReader(sDesciptionFileName);
        //                            sFileText = objSRdr.ReadToEnd();

        //                            sSQL = "SELECT CODES.SHORT_CODE FROM INVDETAIL_X_EOB, CODES";
        //                            sSQL = sSQL + " WHERE INVDETAIL_X_EOB.EOB_CODE = CODES.CODE_ID ";
        //                            sSQL = sSQL + " AND INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + iTempId;
        //                            objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

        //                            if (objTempReader != null)
        //                            {
        //                                while (objTempReader.Read())
        //                                {
        //                                    sTempData = "@" + objTempReader.GetString("SHORT_CODE").Trim();
        //                                    iPoint = sFileText.IndexOf(sTempData, 0);
        //                                    if (iPoint >= 0)
        //                                    {
        //                                        sOldText = sFileText.Substring(iPoint + 1);
        //                                        iPoint = sOldText.IndexOf("@");
        //                                        sNewText = sOldText.Substring(0, iPoint - 1);

        //                                        if (iNumEOB > 0)
        //                                            sNewText = "- " + sNewText;
        //                                        else
        //                                            sNewText = "  " + sNewText;

        //                                        iNumEOB++;
        //                                        m_structEOBDetailRecord.EOBList.Add(iNumEOB, sNewText.Trim());
        //                                        m_structEOBDetailRecord.EOBListDetails.Add(iNumEOB, objTempReader.GetString("SHORT_CODE").Trim() + " " + sNewText.Trim());
        //                                    }
        //                                }
        //                                objTempReader.Close();
        //                            }
        //                            if (objSRdr != null)
        //                            {
        //                                objSRdr.Close();
        //                                objSRdr.Dispose();
        //                            }
        //                        }
        //                        else
        //                        {
        //                            throw new RMAppException(Globalization.GetString("Functions.EobDescriptionFile.Error"));
        //                        }
        //                    }//Added if block for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
        //                    else
        //                    {
        //                        sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM INVDETAIL_X_EOB,CODES_TEXT WHERE ";
        //                        sSQL = sSQL + "INVDETAIL_X_EOB.EOB_CODE = CODES_TEXT.CODE_ID AND ";
        //                        sSQL = sSQL + "INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + iTempId;

        //                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

        //                        if (objTempReader != null)
        //                        {
        //                            while (objTempReader.Read())
        //                            {
        //                                string sTempEob = "";
        //                                sTempData = objTempReader["CODE_DESC"].ToString().Trim();
        //                                sTempData = "- " + sTempData;

        //                                iNumEOB++;
        //                                m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData.Trim());
        //                                m_structEOBDetailRecord.EOBListDetails.Add(iNumEOB, objTempReader["SHORT_CODE"].ToString().Trim() + " " + sTempData.Trim());
        //                            }
        //                            objTempReader.Close();
        //                        }
        //                    }

        //                    //extract diagnosis list
        //                    sSQL = "SELECT CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM INVDETAIL_X_DIAG, CODES_TEXT";
        //                    sSQL = sSQL + " WHERE INVDETAIL_X_DIAG.DIAGNOSIS_CODE=CODES_TEXT.CODE_ID";
        //                    sSQL = sSQL + " AND INVDETAIL_X_DIAG.INVOICE_DETAIL_ID= " + iTempId;

        //                    objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

        //                    if (objTempReader != null)
        //                    {
        //                        iLineItem = 0;
        //                        while (objTempReader.Read())
        //                        {
        //                            sTempData = objTempReader["CODE_DESC"].ToString();
        //                            sTempCode = objTempReader["SHORT_CODE"].ToString();
        //                            iLineItem++;
        //                            m_structEOBDetailRecord.DiagnosisList.Add(iLineItem, sTempData.Trim());
        //                            m_structEOBDetailRecord.DiagnosisCodeList.Add(iLineItem, sTempCode.Trim());
        //                            m_structEOBDetailRecord.DiagnosisDetails.Add(iLineItem, sTempCode.Trim() + "  " + sTempData.Trim());
        //                        }
        //                        objTempReader.Close();
        //                    }

        //                    //extract modifier code
        //                    sSQL = "SELECT CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM INVDETAIL_X_MOD, CODES, CODES_TEXT";
        //                    sSQL = sSQL + " WHERE INVDETAIL_X_MOD.MODIFIER_CODE=CODES_TEXT.CODE_ID";
        //                    sSQL = sSQL + " AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND INVDETAIL_X_MOD.INVOICE_DETAIL_ID=" + iTempId;

        //                    objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

        //                    if (objTempReader != null)
        //                    {
        //                        iLineItem = 0;
        //                        while (objTempReader.Read())
        //                        {
        //                            sTempData = objTempReader["SHORT_CODE"].ToString() + " " + objTempReader["CODE_DESC"].ToString();
        //                            iLineItem++;
        //                            m_structEOBDetailRecord.ModifierList.Add(iLineItem, sTempData.Trim());
        //                        }
        //                        objTempReader.Close();
        //                    }

        //                    //extract fee schedule name
        //                    iTempId = Conversion.ConvertStrToInteger(objReader["TABLE_CODE"].ToString());
        //                    sSQL = "SELECT FEE_TABLES.USER_TABLE_NAME FROM FEE_TABLES WHERE FEE_TABLES.TABLE_ID = " + iTempId;
        //                    objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //                    if (objTempReader != null)
        //                    {
        //                        if (objTempReader.Read())
        //                        {
        //                            sTempData = objTempReader["USER_TABLE_NAME"].ToString();
        //                            m_structEOBDetailRecord.FeeSchedule.Token = sTempData;
        //                        }
        //                        objTempReader.Close();
        //                    }

        //                    m_structEOBDetailRecord.Percentile.Token = objReader["PERCENTILE"].ToString();

        //                    iTempId = Conversion.ConvertStrToInteger(objReader["PLACE_OF_SER_CODE"].ToString());
        //                    sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " +
        //                        "CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
        //                    objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //                    if (objTempReader != null)
        //                    {
        //                        if (objTempReader.Read())
        //                        {
        //                            m_structEOBDetailRecord.PlaceofServiceCode.Token = objTempReader["SHORT_CODE"].ToString();
        //                            m_structEOBDetailRecord.PlaceOfService.Token = objTempReader["CODE_DESC"].ToString();
        //                        }
        //                        objTempReader.Close();
        //                    }

        //                    iTempId = Conversion.ConvertStrToInteger(objReader["TYPE_OF_SER_CODE"].ToString());
        //                    sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " +
        //                        "CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
        //                    objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //                    if (objTempReader != null)
        //                    {
        //                        if (objTempReader.Read())
        //                        {
        //                            m_structEOBDetailRecord.TypeOfServiceCode.Token = objTempReader["SHORT_CODE"].ToString();
        //                            m_structEOBDetailRecord.TypeOfService.Token = objTempReader["CODE_DESC"].ToString();
        //                        }
        //                        objTempReader.Close();
        //                    }

        //                    m_structEOBDetailRecord.UnitsBilled.Token = Conversion.ConvertStrToLong(objReader["UNITS_BILLED_NUM"].ToString());

        //                    m_structEOBDetailRecord.ScheduledAmount.Token = Conversion.ConvertStrToDouble(objReader["SCHEDULED_AMOUNT"].ToString());
        //                    dTotSchdAmt += Functions.FormatAmount(m_structEOBDetailRecord.ScheduledAmount.Token);

        //                    m_structEOBDetailRecord.AmountReduced.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_REDUCED"].ToString());
        //                    dTotAmtReduced += Functions.FormatAmount(m_structEOBDetailRecord.AmountReduced.Token);

        //                    m_structEOBDetailRecord.AmountSaved.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_SAVED"].ToString());
        //                    dTotAmtSaved += Functions.FormatAmount(m_structEOBDetailRecord.AmountSaved.Token);

        //                    m_structEOBDetailRecord.ContractAmount.Token = Conversion.ConvertStrToDouble(objReader["CONTRACT_AMOUNT"].ToString());
        //                    m_structEOBDetailRecord.Discount.Token = Conversion.ConvertStrToDouble(objReader["DISCOUNT"].ToString());
        //                    m_structEOBDetailRecord.ProviderZipCode.Token = objReader["ZIP_CODE"].ToString();

        //                    m_structEOBDetailRecord.AmountAllowed.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_ALLOWED"].ToString());
        //                    dTotalAmountAllowed += Functions.FormatAmount(m_structEOBDetailRecord.AmountAllowed.Token);

        //                    m_structEOBDetailRecord.BaseAmount.Token = Conversion.ConvertStrToDouble(objReader["BASE_AMOUNT"].ToString());
        //                    dTotalBaseAmount += Functions.FormatAmount(m_structEOBDetailRecord.BaseAmount.Token);

        //                    m_structEOBDetailRecord.DiscountAmount.Token = m_structEOBDetailRecord.BaseAmount.Token * (m_structEOBDetailRecord.Discount.Token / 100);
        //                    dTotalDiscountAmount += Functions.FormatAmount(m_structEOBDetailRecord.DiscountAmount.Token);

        //                    m_structEOBDetailRecord.PerDiemAmount.Token = Conversion.ConvertStrToDouble(objReader["PER_DIEM_AMT"].ToString());
        //                    dTotalPerDiemAmt += Functions.FormatAmount(m_structEOBDetailRecord.PerDiemAmount.Token);

        //                    m_structEOBDetailRecord.StopLossAmount.Token = Conversion.ConvertStrToDouble(objReader["STOP_LOSS_AMT"].ToString());
        //                    dTotalStopLossAmt += Functions.FormatAmount(m_structEOBDetailRecord.StopLossAmount.Token);

        //                    m_structEOBDetailRecord.FeeTableAmount.Token = Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString());
        //                    dTotalFeeTableAmt += Functions.FormatAmount(m_structEOBDetailRecord.FeeTableAmount.Token);

        //                    m_structEOBDetailRecord.PrescripNo.Token = objReader["PRESCRIP_NO"].ToString();
        //                    m_structEOBDetailRecord.DrugName.Token = objReader["DRUG_NAME"].ToString();
        //                    m_structEOBDetailRecord.PrescripDate.Token = Conversion.ToDate(objReader["PRESCRIP_DATE"].ToString()).ToShortDateString();
        //                    m_structEOBDetailRecord.PhysicianLicenseNum.Token = objReader["FL_LICENSE"].ToString();
        //                    m_structEOBDetailRecord.RevenueCode.Token = objReader["REV_CODE"].ToString();

        //                    m_structEOBDetailRecord.DiscOnFeeSchd.Token = 0;
        //                    if (Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString()) > 0 && Conversion.ConvertStrToDouble(objReader["DISCOUNT"].ToString()) > 0)
        //                        m_structEOBDetailRecord.DiscOnFeeSchd.Token = Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString()) - Conversion.ConvertStrToDouble(objReader["AMOUNT_ALLOWED"].ToString());

        //                    objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit", false);
        //                    try
        //                    {
        //                        objFundsTransSplit.MoveTo(Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString()));
        //                        m_structEOBDetailRecord.ProcedureDate.Token = Conversion.ToDate(objFundsTransSplit.FromDate).ToShortDateString();
        //                        m_structEOBDetailRecord.ProcToDate.Token = Conversion.ToDate(objFundsTransSplit.ToDate).ToShortDateString();
        //                        m_structEOBDetailRecord.InvoiceNumber.Token = objFundsTransSplit.InvoiceNumber;
        //                        m_structEOBDetailRecord.InvoiceDate.Token = Conversion.ToDate(objFundsTransSplit.InvoiceDate).ToShortDateString();
        //                        m_structEOBDetailRecord.InvoicedBy.Token = objFundsTransSplit.InvoicedBy;
        //                    }
        //                    catch
        //                    {
        //                        m_structEOBDetailRecord.ProcedureDate.Token = string.Empty;
        //                        m_structEOBDetailRecord.ProcToDate.Token = string.Empty;
        //                        m_structEOBDetailRecord.InvoiceNumber.Token = "N/A";
        //                        m_structEOBDetailRecord.InvoiceDate.Token = string.Empty;
        //                        m_structEOBDetailRecord.InvoicedBy.Token = string.Empty;
        //                    }

        //                    //Print remaining data fields
        //                    sTempData = objReader["BILLING_CODE_TEXT"].ToString();

        //                    m_structEOBDetailRecord.ProcedureCode.Token = sTempData;
        //                    m_structEOBDetailRecord.Procedure.Token = sTempData.Substring(sTempData.IndexOf('-') + 1);

        //                    dTempId = Conversion.ConvertStrToDouble(objReader["AMOUNT_BILLED"].ToString());
        //                    m_structEOBDetailRecord.AmountBilled.Token = dTempId;

        //                    dTotalAmountBilled += dTempId;

        //                    //amount to pay
        //                    dTempId = Conversion.ConvertStrToDouble(objReader["AMOUNT_TO_PAY"].ToString());
        //                    m_structEOBDetailRecord.AmountPaid.Token = dTempId;
        //                    dTotalAmountPaid += dTempId;

        //                    foreach (string TD in TDList)
        //                    {
        //                        sAttributes = "";
        //                        sTD = TD.Substring(1, TD.Length - 2);
        //                        sTempFormat = sTD.Split(',');

        //                        sTD = "{" + sTempFormat[0] + "}";
        //                        iDataLength = int.Parse(sTempFormat[1]);
        //                        sAttributes = " " + sTempFormat[2];

        //                        sTDData = m_objFunctions.ProcessHTMLtableToken(sTD, iDataLength, ref m_structEOBRecord, ref m_structEOBDetailRecord, objFundsTransSplit);
        //                        sHTMLcellData = sHTMLcellData + "<TD" + sAttributes + ">" + sTDData + "</TD>";
        //                    }
        //                    if (sHTMLrowData == string.Empty)
        //                        sHTMLrowData = sHTMLcellData;
        //                    else
        //                        sHTMLrowData = sHTMLrowData + "|" + sHTMLcellData;

        //                    sHTMLcellData = string.Empty;
        //                }
        //            }//END of While
        //            objReader.Close();
        //        }//End of IF

        //        //Print Totals
        //        if (bIsPrintTotals)
        //        {
        //            m_structEOBRecord.TotalAmountBilled = dTotalAmountBilled;
        //            m_structEOBRecord.TotalAmountPaid = dTotalAmountPaid;
        //            m_structEOBRecord.TotalScheduledAmount = dTotSchdAmt;
        //            m_structEOBRecord.TotalAmountReduced = dTotAmtReduced;
        //            m_structEOBRecord.TotalAmountSaved = dTotAmtSaved;
        //            m_structEOBRecord.TotalBaseAmount = dTotalBaseAmount;
        //            m_structEOBRecord.TotalAmountAllowed = dTotalAmountAllowed;
        //            m_structEOBRecord.TotalDiscountAmount = dTotalDiscountAmount;
        //            m_structEOBRecord.TotalPerDiemAmt = dTotalPerDiemAmt;
        //            m_structEOBRecord.TotalStopLossAmt = dTotalStopLossAmt;
        //            m_structEOBRecord.TotalFeeTableAmt = dTotalFeeTableAmt;
        //        }
        //        return sHTMLrowData;
        //    }
        //    catch (DataModelException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (RMAppException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.PrintEOBHTMLTABLE"), p_objEx);
        //    }
        //    finally
        //    {
        //        if (objSQL != null)
        //            objSQL = null;

        //        if (objReader != null)
        //            objReader.Dispose();

        //        if (objTempReader != null)
        //            objTempReader.Dispose();

        //        if (objFundsTransSplit != null)
        //            objFundsTransSplit.Dispose();

        //        m_structEOBDetailRecord.EOBList.Clear();
        //        m_structEOBDetailRecord.EOBListDetails.Clear();
        //        m_structEOBDetailRecord.DiagnosisList.Clear();
        //        m_structEOBDetailRecord.ModifierList.Clear();
        //        m_structEOBDetailRecord.DiagnosisCodeList.Clear();
        //    }
        //}
        // akaushik5 Commented for MITS 35846 Ends
        #endregion

		#endregion

		#region CheckPrintSQLPostCheck , Used in PrintChecks Namespace
		/// Name		: CheckPrintSQLPostCheck
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Build the Post Check SQL string
		/// </summary>
		/// <param name="p_iAccountId">Account Id</param>
		/// <param name="p_sOrderByField">Sort By string</param>
		/// <param name="p_iBatchNum">Batch Number</param>
		/// <returns>Returns the SQL string for PostChecks</returns>
		// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        public string CheckPrintSQLPostCheck(int p_iAccountId, string p_sOrderByField, int p_iBatchNum, int p_iDistributionType)
		{
			SysSettings objSysSettings = null ;
			CCacheFunctions objCCacheFunctions = null ;
			StringBuilder sbSQL = null;

			string sAlias = "" ;
			string sTemp = "" ;
			
			try
			{
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);//sharishkumar Jira 835
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString, m_iClientId);
				sbSQL = new StringBuilder();


				if (m_sDBType == Constants.DB_ACCESS )
					sAlias = " AS " ;
				else
					sAlias = " " ;

				sbSQL.Append( " SELECT FUNDS.FIRST_NAME, FUNDS.LAST_NAME, FUNDS.DATE_OF_CHECK," );
				sbSQL.Append( " FUNDS.AMOUNT " + sAlias + " FUNDS_AMOUNT," );
				sbSQL.Append( " FUNDS.TRANS_ID " + sAlias + " FUNDS_TRANS_ID," );
				sbSQL.Append( " FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE," );
				sbSQL.Append( " FUNDS_TRANS_SPLIT.AMOUNT, FUNDS_TRANS_SPLIT.FROM_DATE," );
				sbSQL.Append( " FUNDS.CTL_NUMBER, FUNDS.CLAIM_NUMBER," );
				sbSQL.Append( " FUNDS.CLAIM_ID, FUNDS.TRANS_NUMBER," );
				sbSQL.Append( " FUNDS.AUTO_CHECK_FLAG, FUNDS.PAYEE_TYPE_CODE" );
                // start - Added By Nikhil on 08/06/14
                sbSQL.Append(" ,FUNDS_TRANS_SPLIT.POLCVG_ROW_ID ");
                // end - Added By Nikhil on 08/06/14
				if( objSysSettings.UseFundsSubAcc )
					sbSQL.Append( " ,FUNDS.SUB_ACCOUNT_ID" );
			
				sbSQL.Append( ", FUNDS.PAYEE_EID" );
                // zmohammad Added for MITS 38364 Starts
                //sbSQL.Append( " FROM FUNDS,FUNDS_TRANS_SPLIT " );
                if (p_sOrderByField == "BANK_ACC_SUB")
                {
                    sbSQL.Append(", BANK_ACC_SUB.SUB_ACC_NAME");
                }

                sbSQL.Append(" FROM FUNDS JOIN FUNDS_TRANS_SPLIT ON FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");
                if (p_sOrderByField == "BANK_ACC_SUB")
                {
                    sbSQL.Append(" LEFT JOIN BANK_ACC_SUB ON BANK_ACC_SUB.SUB_ROW_ID = FUNDS.SUB_ACCOUNT_ID ");
                }
                // zmohammad Added for MITS 38364 Ends
				sbSQL.Append( " WHERE FUNDS.BATCH_NUMBER = " + p_iBatchNum );
				sbSQL.Append( " AND FUNDS.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort( "P" , "CHECK_STATUS" ) );
				sbSQL.Append( " AND FUNDS.ACCOUNT_ID = " + p_iAccountId  );
				sbSQL.Append( " AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID" );
				sbSQL.Append( " AND FUNDS.PAYMENT_FLAG <> 0" );
				sbSQL.Append( " AND FUNDS.VOID_FLAG = 0" );
				// Vaibhav 09/15/2006: this tax mapping check is not in use in other Printchecks reports. 
                //Geeta 08/06/08 : Mits 12399
				sbSQL.Append( " AND FUNDS.PAYEE_EID NOT IN (SELECT TAX_EID FROM TAX_MAPPING)" );
				// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                sbSQL.AppendFormat(" AND FUNDS.DSTRBN_TYPE_CODE = {0} ", p_iDistributionType);
				sbSQL.Append( " ORDER BY " );

				if( p_sOrderByField == null )
					p_sOrderByField = "" ;

				p_sOrderByField = p_sOrderByField.Trim();

				if( p_sOrderByField != "" && p_sOrderByField != "BANK_ACC_SUB" )
					sbSQL.Append( p_sOrderByField + "," ) ;
                //changed by gagan for mits 10425 : start
                else if (p_sOrderByField == "BANK_ACC_SUB")
                    sbSQL.Append("BANK_ACC_SUB.SUB_ACC_NAME, "); // zmohammad Added for MITS 38364
					//sbSQL.Append( "FUNDS.SUB_ACCOUNT_ID, " ) ;
                //changed by gagan for mits 10425 : end
				sbSQL.Append( "FUNDS.TRANS_ID,FUNDS_TRANS_SPLIT.SPLIT_ROW_ID" );
				sTemp = sbSQL.ToString();
			
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintEOBAR.CheckPrintSQLPostCheck.SqlBuildError", m_iClientId), p_objEx);//sharishkumar Jira 835				
			}
			finally
			{
				objSysSettings = null ;
				objCCacheFunctions = null ;
				sbSQL = null ;
			}
			return( sTemp );	
		}

		#endregion 
	}
}
