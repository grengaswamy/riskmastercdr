using System;
using C1.C1PrintDocument;
using System.Drawing ;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common ;
using System.IO;

namespace Riskmaster.Application.PrintEOB
{
	/**************************************************************
	 * $File		: PrintWrapper.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/05/2005
	 * $Author		: Vaibhav Kaushik
	 * $Comment		: PrintWrapper class has basic functions to create Print Document.
	 * $Source		:  	
	**************************************************************/
	internal class PrintWrapper
	{
		#region Variable Declarations
		/// <summary>
		/// Private variable to store C1PrintDocumnet object instance.
		/// </summary>
		C1PrintDocument m_objPrintDoc = null ;
		/// <summary>
		/// Private variable to store CurrentX
		/// </summary>
		private double m_dblCurrentX = 0 ;
		/// <summary>
		/// Private variable to store CurrentY
		/// </summary>
		private double m_dblCurrentY = 0 ;
		/// <summary>
		/// Private variable to store Font object.
		/// </summary>
		private Font m_objFont = null ;
		/// <summary>
		/// Private variable to store Text object.
		/// </summary>
		private RenderText m_objText = null ;
		/// <summary>
		/// Private variable to store PageWidth
		/// </summary>
		private double m_dblPageWidth = 0.0 ;
		/// <summary>
		/// Private variable to store PageHeight
		/// </summary>
		private double m_dblPageHeight = 0.0 ;	
	
        private int m_iClientId=0;
		#endregion 

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>		
		internal PrintWrapper(int p_iClientId)
		{			
			m_objPrintDoc = new C1PrintDocument();
			m_objPrintDoc.DefaultUnit = UnitTypeEnum.Twip ;			
			m_objPrintDoc.PageSettings.Margins.Top = 0 ;
			m_objPrintDoc.PageSettings.Margins.Left = 0 ;
			m_objPrintDoc.PageSettings.Margins.Bottom = 0 ;
			m_objPrintDoc.PageSettings.Margins.Right = 0 ;	
			m_iClientId = p_iClientId;	
		}
		#endregion 

		/// <summary>
		/// Initialize the document
		/// </summary>
		/// <param name="p_bIsLandscape">IsLandscape</param>
		internal void StartDoc( bool p_bIsLandscape )
		{		
			Rectangle objRect ;

			try
			{
				m_objPrintDoc.PageSettings.Landscape = p_bIsLandscape ;

				//objRect = m_objPrintDoc.PageSettings.Bounds ;			
				//m_dblPageHeight = ( objRect.Height / 100 ) * 1440 ;
				//m_dblPageWidth = ( objRect.Width / 100 ) * 1440  ;
				if( p_bIsLandscape )
				{
					m_dblPageHeight = 8.5 * 1440 ;
					m_dblPageWidth = 11 * 1440  ;
					// JP ??? Is this correct?     m_dblPageHeight += 700 ;
				}
				else
				{
					m_dblPageHeight = 11 * 1440 ;
					m_dblPageWidth = 8.5 * 1440  ;
					// JP ??? Is this correct?     m_dblPageWidth += 700 ;
				}
				m_objPrintDoc.StartDoc();
			
				m_objFont = new Font( "Arial" , 8  );		
				m_objText = new RenderText( m_objPrintDoc );			
				m_objText.Style.Font = m_objFont ;
				m_objText.Style.WordWrap = false ;
				m_objText.Width = m_dblPageWidth ;		
			}
			catch( RMAppException p_objEx )
			{								
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.StartDoc.Error", m_iClientId), p_objEx);				
			}				
		}
		
		/// <summary>
		/// Read property for PageHeight.
		/// </summary>
		internal double PageHeight 
		{
			get
			{
				return( m_dblPageHeight ) ;
			}			
		}
		/// <summary>
		/// Read property for PageWidth.
		/// </summary>
		internal double PageWidth 
		{
			get
			{
				return( m_dblPageWidth ) ;
			}			
		}
		/// <summary>
		/// Read/Write property for CurrentX.
		/// </summary>
		internal double CurrentX 
		{
			get
			{
				return( m_dblCurrentX ) ;
			}
			set
			{
				m_dblCurrentX = value ;
			}
		}
		/// <summary>
		/// Read/Write property for CurrentY.
		/// </summary>
		internal double CurrentY 
		{
			get
			{
				return( m_dblCurrentY ) ;
			}
			set
			{
				m_dblCurrentY = value ;
			}
		}		

		/// <summary>
		/// Read property for FontSize.
		/// </summary>
		internal double FontSize 
		{
			get
			{
				return( m_objFont.Size) ;
			}
		}

		/// <summary>
		/// Read property for FontSize.
		/// </summary>
		internal string FontName 
		{
			get
			{
				return(m_objFont.Name) ;
			}
		}

		/// <summary>
		/// Render Text on the document.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		internal void PrintText( string p_sPrintText )
		{
			try
			{
				m_objText.Text = p_sPrintText + "\r" ;	
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect( m_dblCurrentX , m_dblCurrentY , m_objText );			
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintWrapper.PrintText.Error",m_iClientId) , p_objEx );				
			}
		}
		
		/// <summary>
		/// Render Text on the document with no "\r" on the end.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		internal void PrintTextNoCr( string p_sPrintText )
		{
			try
			{
				m_objText.Text = p_sPrintText + "" ;			
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect( m_dblCurrentX , m_dblCurrentY , m_objText );						
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintWrapper.PrintTextNoCr.Error",m_iClientId) , p_objEx );				
			}
		}

		/// <summary>
		/// Print text such that it ends at given X Co-ordinate.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		internal void PrintTextEndAt( string p_sPrintText )
		{
			try
			{
				this.CurrentX = m_dblCurrentX - this.GetTextWidth( p_sPrintText ) ;
				this.PrintText( p_sPrintText );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintTextEndAt.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Set Font 
		/// </summary>
		/// <param name="p_sFontName">Font Name</param>
		/// <param name="p_dblFontSize">Font Size</param>
		internal void SetFont( string p_sFontName , double p_dblFontSize )
		{
			try
			{
				m_objFont = new Font( p_sFontName , (float)p_dblFontSize );
				m_objText.Style.Font = m_objFont ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFont.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Set Font 
		/// </summary>
		/// <param name="p_dblFontSize">Font Size</param>
		internal void SetFont(double p_dblFontSize)
		{
			try
			{
				m_objFont = new Font( m_objFont.Name , (float)p_dblFontSize );
				m_objText.Style.Font = m_objFont ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFont.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Set Font 
		/// </summary>
		/// <param name="p_sFontName">Font Name</param>
		internal void SetFont( string p_sFontName)
		{
			try
			{
				m_objFont = new Font( p_sFontName , m_objFont.Size);
				m_objText.Style.Font = m_objFont ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFont.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Set Font Bold
		/// </summary>
		/// <param name="p_bBold">Bool flag</param>
		internal void SetFontBold( bool p_bBold )
		{		
			try
			{
				if( p_bBold )
					m_objFont = new Font( m_objFont.Name , m_objFont.Size , FontStyle.Bold );
				else	
					m_objFont = new Font( m_objFont.Name , m_objFont.Size );
				
				m_objText.Style.Font = m_objFont ;

			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintWrapper.SetFontBold.Error", m_iClientId), p_objEx );				
			}
 
		}
		/// <summary>
		/// Get text width.
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Width</returns>
		internal double GetTextWidth( string p_sText )
		{
			try
			{
				m_objText.Text = p_sText + "" ;
				return( m_objText.BoundWidth );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintWrapper.GetTextWidth.Error", m_iClientId), p_objEx );				
			}
		}

		/// <summary>
		/// Get Text Height
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Height</returns>
		internal double GetTextHeight( string p_sText )
		{
			try
			{
				m_objText.Text = p_sText + "" ;
				return( m_objText.BoundHeight );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintWrapper.GetTextHeight.Error", m_iClientId), p_objEx );				
			}
		}

		/// <summary>
		/// Save the C1PrintDocument in PDF format.
		/// </summary>
		/// <param name="p_sPath">Path</param>
		internal void Save( string p_sPath )
		{
			try
			{
				m_objPrintDoc.ExportToPDF( p_sPath , false );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintWrapper.Save.Error", m_iClientId), p_objEx );				
			}
		}

		/// <summary>
		/// Save the C1PrintDocument in PDF format.
		/// </summary>
		/// <param name="p_sPath">Path</param>
		internal void GetPDFStream( MemoryStream p_objMemoryStream )
		{
			try
			{
				m_objPrintDoc.Save( p_objMemoryStream);
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintWrapper.Save.Error", m_iClientId), p_objEx );				
			}
		}

		/// <summary>
		/// Call the end event.
		/// </summary>
		internal void EndDoc()
		{
			try
			{
				m_objPrintDoc.EndDoc();
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintWrapper.EndDoc.Error", m_iClientId), p_objEx );				
			}
		}

		/// <summary>
		/// Print Line
		/// </summary>
		/// <param name="p_dblFromX">From X</param>
		/// <param name="p_dblFromY">From Y</param>
		/// <param name="p_dblToX">To X</param>
		/// <param name="p_dblToY">To Y</param>
		internal void PrintLine( double p_dblFromX , double p_dblFromY , double p_dblToX , double p_dblToY )
		{
			try
			{
				m_objPrintDoc.RenderDirectLine( p_dblFromX , p_dblFromY , p_dblToX , p_dblToY );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintWrapper.PrintLine.Error", m_iClientId), p_objEx );				
			}
		}

		/// <summary>
		/// Start a new page.
		/// </summary>
		internal void NewPage()
		{
			try
			{
				m_objPrintDoc.NewPage();
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintWrapper.NewPage.Error", m_iClientId), p_objEx );				
			}
		}

		/// <summary>
		/// Print the image on the page, with its actual size.
		/// </summary>
		internal void PrintImage(EOBRecord p_structEOBRecord)
		{	

			Bitmap objBitMapIamge = null ;
			ImageAlignDef objAlignDef = null ;

			double dblWidth = 0.0 ;
			double dblHeight = 0.0 ;
			double dblTopX = 0.0 ;
			double dblTopY = 0.0 ;
			
			try
			{
				if(p_structEOBRecord.LogoPath == string.Empty)
					return;
				else if(!File.Exists(p_structEOBRecord.LogoPath))
					return;

				objBitMapIamge = new Bitmap(p_structEOBRecord.LogoPath,true);
				objAlignDef = new ImageAlignDef( ImageAlignHorzEnum.Center , ImageAlignVertEnum.Center , false , false , true , false , false );
				//m_objPrintDoc.PageLayer = DocumentPageLayerEnum.Background ;
			
				//
				// Image height is in pixel. By dividing it from resolution, Get the height in Inch.
				// covert inch to twips by multiplying 1440. 
				//

				dblWidth  = ( objBitMapIamge.Width / objBitMapIamge.HorizontalResolution ) * 1440 ;
				dblHeight = ( objBitMapIamge.Height / objBitMapIamge.VerticalResolution )* 1440 ;

				//
				// Get the top( X, Y ) by aligining as per page width 7 height. 
				//
                //nadim for 14513...client wants to align this image at top left,otherwise it would align at centre

                //dblTopX = ( this.PageWidth - dblWidth )/2 ;
                //dblTopY = ( this.PageHeight - dblHeight )/2 ;
                dblTopX = 5.0;
                dblTopY = 5.0;
                //nadim for 14513
				m_objPrintDoc.RenderDirectImage( dblTopX , dblTopY , objBitMapIamge , dblWidth , dblHeight , objAlignDef );
				m_objPrintDoc.PageLayer = DocumentPageLayerEnum.Main ;			
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintWrapper.PrintImage.Error", m_iClientId), p_objEx );				
			}
			finally
			{
				objBitMapIamge = null ;
				objAlignDef = null ;

			}
		}	
	}
}
