using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.PrintEOB
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   22 December 2004
	///Purpose :   This is the class corresponding to the EOB Report (Active Report).
	///			   It contains methods related to the report layout, report fields initialization and populating them with data.
	/// </summary>
	public class rptEOB : CommonEOB
	{
        private int m_iClientId = 0;

		public rptEOB()
		{
			InitializeReport();
		}

		public rptEOB(string p_sConnectionString, string p_sUserName, int p_iClientId):base(p_sConnectionString, p_sUserName, p_iClientId)
		{
			InitializeReport();
            m_iClientId = p_iClientId;
		}

		#region Methods
		/// <summary>
		/// This method would invoke the method of base class to initialize the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void rptEOB_DataInitialize(object p_objsender, System.EventArgs p_objeArgs)
		{ 
			base.DataInitializeBase(ref p_objsender,ref p_objeArgs);			
		}
		/// <summary>
		/// This method would invoke the method of base class to fill data in the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void rptEOB_FetchData(object p_objsender, FetchEventArgs p_objeArgs)
		{
			base.FetchDataBase(ref p_objsender,ref p_objeArgs);

			 //for hospital bills, no CPT code is used so pop in trans type for description
			if(Fields["PROCEDURE"].Value.ToString() == string.Empty)
				Fields["PROCEDURE"].Value = Fields["TRANS_TYPE"].Value; 
    
		}
		/// <summary>
		/// This method would invoke the method of base class to do the page settings before start printing 
		/// the report and also to print the soft error log.		
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void rptEOB_ReportStart(object p_objsender, System.EventArgs p_objeArgs)
		{
			base.ReportStartBase(ref p_objsender,ref p_objeArgs);			
		}

		private void GroupHeader1_AfterPrint(object sender, System.EventArgs eArgs)
		{
            //lblTotalBilled = lblMaxBilled;
            //lblTotalPaid = lblMaxPaid;
		}

		private void GroupFooter1_Format(object sender, System.EventArgs eArgs)
		{
            //lblTotalBilled = lblMaxBilled;
            //lblTotalPaid = lblMaxPaid;
		}
		#endregion

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.TextBox Field38 = null;
		private DataDynamics.ActiveReports.TextBox Field13 = null;
		private DataDynamics.ActiveReports.Shape Shape1 = null;
		private DataDynamics.ActiveReports.Shape Shape2 = null;
		private DataDynamics.ActiveReports.Shape Shape3 = null;
		private DataDynamics.ActiveReports.Shape Shape4 = null;
		private DataDynamics.ActiveReports.Shape Shape5 = null;
		private DataDynamics.ActiveReports.Shape Shape6 = null;
		private DataDynamics.ActiveReports.Shape Shape7 = null;
		private DataDynamics.ActiveReports.Shape Shape8 = null;
		private DataDynamics.ActiveReports.Shape Shape9 = null;
		private DataDynamics.ActiveReports.Shape Shape10 = null;
		private DataDynamics.ActiveReports.Shape Shape11 = null;
		private DataDynamics.ActiveReports.Shape Shape12 = null;
		private DataDynamics.ActiveReports.Shape Shape13 = null;
		private DataDynamics.ActiveReports.Shape Shape14 = null;
		private DataDynamics.ActiveReports.Shape Shape15 = null;
		private DataDynamics.ActiveReports.Shape Shape16 = null;
		private DataDynamics.ActiveReports.Label Label1 = null;
		private DataDynamics.ActiveReports.Label Label2 = null;
		private DataDynamics.ActiveReports.Label Label3 = null;
		private DataDynamics.ActiveReports.Label Label4 = null;
		private DataDynamics.ActiveReports.Label Label5 = null;
		private DataDynamics.ActiveReports.Label Label6 = null;
		private DataDynamics.ActiveReports.Label Label7 = null;
		private DataDynamics.ActiveReports.Label Label8 = null;
		private DataDynamics.ActiveReports.Label Label9 = null;
		private DataDynamics.ActiveReports.Label Label10 = null;
		private DataDynamics.ActiveReports.Label Label11 = null;
		private DataDynamics.ActiveReports.Label Label12 = null;
		private DataDynamics.ActiveReports.Label Label13 = null;
		private DataDynamics.ActiveReports.Label Label14 = null;
		private DataDynamics.ActiveReports.Label Label15 = null;
		private DataDynamics.ActiveReports.Label Label16 = null;
		private DataDynamics.ActiveReports.TextBox Field1 = null;
		private DataDynamics.ActiveReports.TextBox Field2 = null;
		private DataDynamics.ActiveReports.TextBox Field3 = null;
		private DataDynamics.ActiveReports.TextBox Field4 = null;
		private DataDynamics.ActiveReports.TextBox Field5 = null;
		private DataDynamics.ActiveReports.TextBox Field6 = null;
		private DataDynamics.ActiveReports.TextBox Field7 = null;
		private DataDynamics.ActiveReports.TextBox Field8 = null;
		private DataDynamics.ActiveReports.TextBox Field9 = null;
		private DataDynamics.ActiveReports.TextBox Field10 = null;
		private DataDynamics.ActiveReports.TextBox Field11 = null;
		private DataDynamics.ActiveReports.TextBox Field12 = null;
		private DataDynamics.ActiveReports.TextBox Field14 = null;
		private DataDynamics.ActiveReports.Label Label17 = null;
		private DataDynamics.ActiveReports.Label Label18 = null;
		private DataDynamics.ActiveReports.Label Label19 = null;
		private DataDynamics.ActiveReports.Label Label20 = null;
		private DataDynamics.ActiveReports.Shape Shape17 = null;
		private DataDynamics.ActiveReports.Label Label21 = null;
		private DataDynamics.ActiveReports.Label Label22 = null;
		private DataDynamics.ActiveReports.Label Label23 = null;
		private DataDynamics.ActiveReports.Shape Shape18 = null;
		private DataDynamics.ActiveReports.Label Label24 = null;
		private DataDynamics.ActiveReports.Label Label25 = null;
		private DataDynamics.ActiveReports.TextBox Field15 = null;
		private DataDynamics.ActiveReports.Line Line1 = null;
		private DataDynamics.ActiveReports.TextBox Field16 = null;
		private DataDynamics.ActiveReports.Label Label26 = null;
		private DataDynamics.ActiveReports.Shape Shape19 = null;
		private DataDynamics.ActiveReports.Shape Shape20 = null;
		private DataDynamics.ActiveReports.Label Label27 = null;
		private DataDynamics.ActiveReports.Label Label28 = null;
		private DataDynamics.ActiveReports.TextBox Field17 = null;
		private DataDynamics.ActiveReports.TextBox Field18 = null;
		private DataDynamics.ActiveReports.Label Label29 = null;
		private DataDynamics.ActiveReports.Shape Shape22 = null;
		private DataDynamics.ActiveReports.Label Label30 = null;
		private DataDynamics.ActiveReports.Shape Shape23 = null;
		private DataDynamics.ActiveReports.Label Label31 = null;
		private DataDynamics.ActiveReports.Shape Shape24 = null;
		private DataDynamics.ActiveReports.Label Label32 = null;
		private DataDynamics.ActiveReports.TextBox Field19 = null;
		private DataDynamics.ActiveReports.Shape Shape25 = null;
		private DataDynamics.ActiveReports.Label Label33 = null;
		private DataDynamics.ActiveReports.TextBox Field20 = null;
		private DataDynamics.ActiveReports.TextBox Field21 = null;
		private DataDynamics.ActiveReports.Shape Shape26 = null;
		private DataDynamics.ActiveReports.Shape Shape27 = null;
		private DataDynamics.ActiveReports.Shape Shape28 = null;
		private DataDynamics.ActiveReports.Label Label34 = null;
		private DataDynamics.ActiveReports.Label Label35 = null;
		private DataDynamics.ActiveReports.Label Label36 = null;
		private DataDynamics.ActiveReports.TextBox Field22 = null;
		private DataDynamics.ActiveReports.TextBox Field23 = null;
		private DataDynamics.ActiveReports.TextBox Field24 = null;
		private DataDynamics.ActiveReports.TextBox Field25 = null;
		private DataDynamics.ActiveReports.Label Label37 = null;
		private DataDynamics.ActiveReports.Label Label38 = null;
		private DataDynamics.ActiveReports.Label Label39 = null;
		private DataDynamics.ActiveReports.Label Label40 = null;
		private DataDynamics.ActiveReports.Label Label41 = null;
		private DataDynamics.ActiveReports.TextBox Field37 = null;
		private DataDynamics.ActiveReports.TextBox Field39 = null;
		private DataDynamics.ActiveReports.TextBox lblPageCount = null;
		private DataDynamics.ActiveReports.Label Label59 = null;
		private DataDynamics.ActiveReports.Line Line38 = null;
		private DataDynamics.ActiveReports.Line Line37 = null;
		private DataDynamics.ActiveReports.Line Line39 = null;
		private DataDynamics.ActiveReports.Line Line40 = null;
		private DataDynamics.ActiveReports.GroupHeader GroupHeader1 = null;
		private DataDynamics.ActiveReports.GroupHeader GroupHeader2 = null;
		private DataDynamics.ActiveReports.Line Line = null;
		private DataDynamics.ActiveReports.Line Line26 = null;
		private DataDynamics.ActiveReports.Line Line27 = null;
		private DataDynamics.ActiveReports.Line Line28 = null;
		private DataDynamics.ActiveReports.Line Line29 = null;
		private DataDynamics.ActiveReports.Line Line30 = null;
		private DataDynamics.ActiveReports.Line Line31 = null;
		private DataDynamics.ActiveReports.Line Line32 = null;
		private DataDynamics.ActiveReports.Line Line33 = null;
		private DataDynamics.ActiveReports.Label Label = null;
		private DataDynamics.ActiveReports.Label Label60 = null;
		private DataDynamics.ActiveReports.Line Line34 = null;
		private DataDynamics.ActiveReports.Label Label61 = null;
		private DataDynamics.ActiveReports.Label Label62 = null;
		private DataDynamics.ActiveReports.Label Label63 = null;
		private DataDynamics.ActiveReports.Label Label64 = null;
		private DataDynamics.ActiveReports.Label Label65 = null;
		private DataDynamics.ActiveReports.Label Label66 = null;
		private DataDynamics.ActiveReports.Label Label67 = null;
		private DataDynamics.ActiveReports.Line Line35 = null;
		private DataDynamics.ActiveReports.Line Line12 = null;
		private DataDynamics.ActiveReports.Line Line13 = null;
		private DataDynamics.ActiveReports.Line Line14 = null;
		private DataDynamics.ActiveReports.Line Line15 = null;
		private DataDynamics.ActiveReports.Line Line16 = null;
		private DataDynamics.ActiveReports.Line Line17 = null;
		private DataDynamics.ActiveReports.Line Line18 = null;
		private DataDynamics.ActiveReports.Line Line19 = null;
		private DataDynamics.ActiveReports.Line Line20 = null;
		private DataDynamics.ActiveReports.Line Line21 = null;
		private DataDynamics.ActiveReports.Line Line36 = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.TextBox Field33 = null;
		private DataDynamics.ActiveReports.TextBox Field32 = null;
		private DataDynamics.ActiveReports.TextBox Field31 = null;
		private DataDynamics.ActiveReports.TextBox Field30 = null;
		private DataDynamics.ActiveReports.TextBox Field29 = null;
		private DataDynamics.ActiveReports.TextBox Field28 = null;
		private DataDynamics.ActiveReports.TextBox Field27 = null;
		private DataDynamics.ActiveReports.TextBox Field26 = null;
		private DataDynamics.ActiveReports.Line Line2 = null;
		private DataDynamics.ActiveReports.Line Line3 = null;
		private DataDynamics.ActiveReports.Line Line4 = null;
		private DataDynamics.ActiveReports.Line Line5 = null;
		private DataDynamics.ActiveReports.Line Line6 = null;
		private DataDynamics.ActiveReports.Line Line7 = null;
		private DataDynamics.ActiveReports.Line Line8 = null;
		private DataDynamics.ActiveReports.Line Line9 = null;
		private DataDynamics.ActiveReports.Line Line10 = null;
		private DataDynamics.ActiveReports.Line Line11 = null;
		private DataDynamics.ActiveReports.Line Line22 = null;
		private DataDynamics.ActiveReports.TextBox Field34 = null;
		private DataDynamics.ActiveReports.GroupFooter GroupFooter2 = null;
		private DataDynamics.ActiveReports.GroupFooter GroupFooter1 = null;
		private DataDynamics.ActiveReports.TextBox lblMaxBilled = null;
		private DataDynamics.ActiveReports.TextBox lblMaxPaid = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		private DataDynamics.ActiveReports.Shape Shape29 = null;
		private DataDynamics.ActiveReports.TextBox TextBox1 = null;
		private DataDynamics.ActiveReports.TextBox TextBox = null;
		private DataDynamics.ActiveReports.Label Label58 = null;
		private DataDynamics.ActiveReports.Label Label57 = null;
		private DataDynamics.ActiveReports.Label Label56 = null;
		private DataDynamics.ActiveReports.Line Line25 = null;
		private DataDynamics.ActiveReports.Line Line24 = null;
		private DataDynamics.ActiveReports.Line Line23 = null;
		private DataDynamics.ActiveReports.Label Label55 = null;
		private DataDynamics.ActiveReports.Label Label54 = null;
		private DataDynamics.ActiveReports.Label Label53 = null;
		private DataDynamics.ActiveReports.Label Label52 = null;
		private DataDynamics.ActiveReports.Label Label51 = null;
		public void InitializeReport()
		{
			try
            {   
                this.Document.Printer.PrinterName = "";
			this.LoadLayout(this.GetType(), "Riskmaster.Application.PrintEOB.rptEOB.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.GroupHeader1 = ((DataDynamics.ActiveReports.GroupHeader)(this.Sections["GroupHeader1"]));
			this.GroupHeader2 = ((DataDynamics.ActiveReports.GroupHeader)(this.Sections["GroupHeader2"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.GroupFooter2 = ((DataDynamics.ActiveReports.GroupFooter)(this.Sections["GroupFooter2"]));
			this.GroupFooter1 = ((DataDynamics.ActiveReports.GroupFooter)(this.Sections["GroupFooter1"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Field38 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[0]));
			this.Field13 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[1]));
			this.Shape1 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[2]));
			this.Shape2 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[3]));
			this.Shape3 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[4]));
			this.Shape4 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[5]));
			this.Shape5 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[6]));
			this.Shape6 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[7]));
			this.Shape7 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[8]));
			this.Shape8 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[9]));
			this.Shape9 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[10]));
			this.Shape10 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[11]));
			this.Shape11 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[12]));
			this.Shape12 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[13]));
			this.Shape13 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[14]));
			this.Shape14 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[15]));
			this.Shape15 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[16]));
			this.Shape16 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[17]));
			this.Label1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[18]));
			this.Label2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[19]));
			this.Label3 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[20]));
			this.Label4 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[21]));
			this.Label5 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[22]));
			this.Label6 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[23]));
			this.Label7 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[24]));
			this.Label8 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[25]));
			this.Label9 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[26]));
			this.Label10 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[27]));
			this.Label11 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[28]));
			this.Label12 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[29]));
			this.Label13 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[30]));
			this.Label14 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[31]));
			this.Label15 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[32]));
			this.Label16 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[33]));
			this.Field1 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[34]));
			this.Field2 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[35]));
			this.Field3 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[36]));
			this.Field4 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[37]));
			this.Field5 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[38]));
			this.Field6 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[39]));
			this.Field7 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[40]));
			this.Field8 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[41]));
			this.Field9 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[42]));
			this.Field10 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[43]));
			this.Field11 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[44]));
			this.Field12 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[45]));
			this.Field14 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[46]));
			this.Label17 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[47]));
			this.Label18 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[48]));
			this.Label19 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[49]));
			this.Label20 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[50]));
			this.Shape17 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[51]));
			this.Label21 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[52]));
			this.Label22 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[53]));
			this.Label23 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[54]));
			this.Shape18 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[55]));
			this.Label24 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[56]));
			this.Label25 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[57]));
			this.Field15 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[58]));
			this.Line1 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[59]));
			this.Field16 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[60]));
			this.Label26 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[61]));
			this.Shape19 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[62]));
			this.Shape20 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[63]));
			this.Label27 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[64]));
			this.Label28 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[65]));
			this.Field17 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[66]));
			this.Field18 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[67]));
			this.Label29 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[68]));
			this.Shape22 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[69]));
			this.Label30 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[70]));
			this.Shape23 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[71]));
			this.Label31 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[72]));
			this.Shape24 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[73]));
			this.Label32 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[74]));
			this.Field19 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[75]));
			this.Shape25 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[76]));
			this.Label33 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[77]));
			this.Field20 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[78]));
			this.Field21 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[79]));
			this.Shape26 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[80]));
			this.Shape27 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[81]));
			this.Shape28 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[82]));
			this.Label34 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[83]));
			this.Label35 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[84]));
			this.Label36 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[85]));
			this.Field22 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[86]));
			this.Field23 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[87]));
			this.Field24 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[88]));
			this.Field25 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[89]));
			this.Label37 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[90]));
			this.Label38 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[91]));
			this.Label39 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[92]));
			this.Label40 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[93]));
			this.Label41 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[94]));
			this.Field37 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[95]));
			this.Field39 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[96]));
			this.lblPageCount = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[97]));
			this.Label59 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[98]));
			this.Line38 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[99]));
			this.Line37 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[100]));
			this.Line39 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[101]));
			this.Line40 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[102]));
			this.Line = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[0]));
			this.Line26 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[1]));
			this.Line27 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[2]));
			this.Line28 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[3]));
			this.Line29 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[4]));
			this.Line30 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[5]));
			this.Line31 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[6]));
			this.Line32 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[7]));
			this.Line33 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[8]));
			this.Label = ((DataDynamics.ActiveReports.Label)(this.GroupHeader2.Controls[9]));
			this.Label60 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader2.Controls[10]));
			this.Line34 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[11]));
			this.Label61 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader2.Controls[12]));
			this.Label62 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader2.Controls[13]));
			this.Label63 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader2.Controls[14]));
			this.Label64 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader2.Controls[15]));
			this.Label65 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader2.Controls[16]));
			this.Label66 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader2.Controls[17]));
			this.Label67 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader2.Controls[18]));
			this.Line35 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[19]));
			this.Line12 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[20]));
			this.Line13 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[21]));
			this.Line14 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[22]));
			this.Line15 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[23]));
			this.Line16 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[24]));
			this.Line17 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[25]));
			this.Line18 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[26]));
			this.Line19 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[27]));
			this.Line20 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[28]));
			this.Line21 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[29]));
			this.Line36 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[30]));
			this.Field33 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
			this.Field32 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
			this.Field31 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
			this.Field30 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[3]));
			this.Field29 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
			this.Field28 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[5]));
			this.Field27 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[6]));
			this.Field26 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[7]));
			this.Line2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[8]));
			this.Line3 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[9]));
			this.Line4 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[10]));
			this.Line5 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[11]));
			this.Line6 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[12]));
			this.Line7 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[13]));
			this.Line8 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[14]));
			this.Line9 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[15]));
			this.Line10 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[16]));
			this.Line11 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[17]));
			this.Line22 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[18]));
			this.Field34 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[19]));
			this.lblMaxBilled = ((DataDynamics.ActiveReports.TextBox)(this.GroupFooter1.Controls[0]));
			this.lblMaxPaid = ((DataDynamics.ActiveReports.TextBox)(this.GroupFooter1.Controls[1]));
			this.Shape29 = ((DataDynamics.ActiveReports.Shape)(this.PageFooter.Controls[0]));
			this.TextBox1 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[1]));
			this.TextBox = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[2]));
			this.Label58 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[3]));
			this.Label57 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[4]));
			this.Label56 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[5]));
			this.Line25 = ((DataDynamics.ActiveReports.Line)(this.PageFooter.Controls[6]));
			this.Line24 = ((DataDynamics.ActiveReports.Line)(this.PageFooter.Controls[7]));
			this.Line23 = ((DataDynamics.ActiveReports.Line)(this.PageFooter.Controls[8]));
			this.Label55 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[9]));
			this.Label54 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[10]));
			this.Label53 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[11]));
			this.Label52 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[12]));
			this.Label51 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[13]));
			// Attach Report Events
			this.DataInitialize += new System.EventHandler(this.rptEOB_DataInitialize);
			this.FetchData += new FetchEventHandler(this.rptEOB_FetchData);
			this.ReportStart += new System.EventHandler(this.rptEOB_ReportStart);
			this.GroupHeader1.AfterPrint += new System.EventHandler(this.GroupHeader1_AfterPrint);
			this.GroupHeader1.Format += new System.EventHandler(this.GroupFooter1_Format);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintEOBAR.InitializeReportEOB.Error",m_iClientId),p_objException);
			}
		}

		#endregion
	}
}
