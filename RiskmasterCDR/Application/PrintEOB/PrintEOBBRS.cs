﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using C1.C1Pdf;//Debabrata Biswas MITS Issue 17183
using DataDynamics.ActiveReports.Export.Pdf;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.PrintEOB
{
	/// <summary>
	/// Summary description for PrintEOBBRS.
	/// </summary>
    public class PrintEOBBRS : IDisposable
	{
		#region "Constructor & Destructor"

		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		public PrintEOBBRS(string p_sUserName , string p_sPassword, string p_sDsnName, string p_sDsnId, int p_iClientId)
		{		
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			m_sDsnId  = p_sDsnId;
            m_iClientId = p_iClientId;//rkaur27
			this.Initialize();
			 
		}		
		/// <summary>
		/// Destructor
		/// </summary>
		~PrintEOBBRS()
        {            
            Dispose();
		}
        private bool _isDisposed = false;
        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
                // akaushik5 Added for MITS 35846 Starts
                if (this.m_objFunctions != null)
                {
                    this.m_objFunctions.Dispose();
                    this.m_objFunctions = null;
                }
                // akaushik5 Added for MITS 35846 Ends
                GC.SuppressFinalize(this);
            }
        }
        
		#endregion

		#region "Member Variables"
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = string.Empty;

		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = string.Empty;

		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = string.Empty;

		/// <summary>
		/// Private variable to store DSN Id
		/// </summary>
		private string m_sDsnId = string.Empty;

		/// <summary>
		/// Private variable to store Security DSN Name
		/// </summary>
		private string m_sSecurityDsnName = string.Empty;

		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;

		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = string.Empty;

		/// <summary>
		/// Wrapper class object for PrintDocument component to generate PDF
		/// </summary>
		private PrintWrapper m_objPrintWrapper = null; 

		/// <summary>
		/// Private variable for storing the Funds Object
		/// </summary>
		private Funds m_objFunds = null;

		/// <summary>
		/// Structure for EOB Detail Record
		/// </summary>
		private EOBDetailRecord m_structEOBDetailRecord;

		/// <summary>
		/// Structure for EOB Record
		/// </summary>
		private EOBRecord m_structEOBRecord;

		/// <summary>
		/// Instianate the Functions class object
		/// </summary>
		private Functions m_objFunctions = null;

		/// <summary>
		/// Instianate the Functions class object
		/// </summary>
		private Common m_objCommon = null;

		private const double XINCREASEWIDTH = 1000.00;

        /// <summary>
        /// Private variable for Direct to Printer Flag
        /// </summary>
        public bool m_bDirectToPrinter = false;
        /// <summary>
        /// Private variable for Printer selected Flag
        /// </summary>
        public bool m_bIsPrinterSelected = false;
		/// <summary>
		/// Constant char for '<'
		/// </summary>
		public const string PRINTEOBBRS_PDFSAVEPATH = "PrintEOBBRS_PdfSavePath";

        // akaushik5 Added for MITS 35846 Starts
        /// <summary>
        /// The filing state
        /// </summary>
        private Dictionary<string, string> FilingState = null;

        private bool isDestinationPathExist = false;
        // akaushik5 Added for MITS 35846 Ends

        private int m_iClientId = 0;//rkaur27
		#endregion
		
		#region "Public Functions"
        // akaushik5 Added for MITS 35846 Starts
        /// <summary>
        /// Prints the eob report.
        /// </summary>
        /// <param name="p_iTransId">The p_i trans identifier.</param>
        /// <param name="p_bIsSilent">if set to <c>true</c> [P_B is silent].</param>
        /// <param name="p_sClaimId">The P_S claim identifier.</param>
        /// <param name="p_sPDFSaveFilePath">The P_S PDF save file path.</param>
        /// <param name="p_sSaveFileName">Name of the P_S save file.</param>
        public void PrintEOBReport(int p_iTransId, bool p_bIsSilent, string p_sClaimId, out string p_sPDFSaveFilePath, out string p_sSaveFileName)
        {
            using (Funds objFunds = this.m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds)
            {
                objFunds.MoveTo(p_iTransId);
                this.PrintEOBReport(p_iTransId, p_bIsSilent, p_sClaimId, out p_sPDFSaveFilePath, out p_sSaveFileName, objFunds);
            }
        }
        // akaushik5 Added for MITS 35846 Ends


        /// Name		: PrintEOBReport
        /// Author		: Anurag Agarwal
        /// Date Created	: 13 Jan 2005		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Overloaded function for PrintEOBReport. Prints EOB to an Active Reports EOB
        /// </summary>
        /// <param name="p_iTransId">Funds TransId</param>
        /// <param name="p_bIsSilent">Flag for enabling/disabling the No Data found exceptions</param>
        /// <param name="p_sState">State Id</param>
        /// <param name="p_bIsPostCheck">Flag for Post Checks</param>
        /// <param name="p_sFileName">File Name</param>
        /// <param name="p_sDestFolderPath">Destination folder Path for saving the PDF files</param>
        /// <param name="p_sPDFSaveFilePath">Saved PDF file name as output parameter. The naming convention of file will be TransID_StateId</param>
        public void PrintEOBReport(int p_iTransId, bool p_bIsSilent, string p_sClaimId, out string p_sPDFSaveFilePath, out string p_sSaveFileName, Funds objFunds)
        {
            ArrayList arrTempList = null;
            PrintWrapper objPrintWrapper = null;
            CommonEOB objCommonEOB = null;
            PdfExport objPdfExport = null;
            string sDestFolderPath = "";
            // akaushik5 Commented for MITS 35846 Starts
            //RMConfigurator objConfig = null;
            // akaushik5 Commented for MITS 35846 Ends
            bool bIsPostCheck = false;
            string sStateId = string.Empty;
            string sSql = string.Empty;
            DbReader objReader = null;
            try
            {
                //Debabrata Biswas
                //Implement Hook to call HTML EOB Templete depending upon the config settings.
                //Debabrata Biswas MITS Issue 17183
                //call HTML report template if Riskmaster.config has the EOBClaimHTMLFormatFile and EOBPayeeHTMLFormatFile nodes
                // akaushik5 Changed for MITS 35846 Starts
                //sSql = "SELECT FILING_STATE_ID FROM CLAIM WHERE CLAIM_ID = " + p_sClaimId;
                //    objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                //if (objReader != null)
                //{
                //    if (objReader.Read())
                //    {
                //        sStateId = objReader["FILING_STATE_ID"].ToString();
                //    }
                //    objReader.Close();
                //}
                //objConfig = new RMConfigurator();
                sStateId = this.GetFilingStateId(p_sClaimId);
                // akaushik5 Changed for MITS 35846 Ends

                if (RMConfigurationManager.GetNameValueSectionSettings("PrintEOB", m_sConnectionString, m_iClientId)["EOBClaimHTMLFormatFile"] != null
                    && RMConfigurationManager.GetNameValueSectionSettings("PrintEOB", m_sConnectionString, m_iClientId)["EOBPayeeHTMLFormatFile"] != null)
                {
                    p_sPDFSaveFilePath = string.Empty;
                    sDestFolderPath = Functions.GetSavePath();
                    p_sSaveFileName = string.Empty;
                    if (!Directory.Exists(sDestFolderPath))
                        Directory.CreateDirectory(sDestFolderPath);

                    m_objFunctions.PrintEOBHTMLReport(p_iTransId, p_bIsSilent, sStateId, bIsPostCheck, arrTempList, out p_sPDFSaveFilePath, out objCommonEOB, objFunds);

                    if (objCommonEOB != null)
                    {
                        if (p_sPDFSaveFilePath == string.Empty)
                            p_sPDFSaveFilePath = sDestFolderPath + @"\" + p_iTransId + "_" + sStateId + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                        objPdfExport = new PdfExport();
                        objPdfExport.Export(objCommonEOB.Document, p_sPDFSaveFilePath);
                    }
                    if(!string.IsNullOrEmpty(p_sPDFSaveFilePath))
                    {
                        p_sSaveFileName = p_sPDFSaveFilePath.Substring(p_sPDFSaveFilePath.LastIndexOf("\\") + 1);
                    }
                }
                else
                {
                    p_sSaveFileName = p_iTransId + "_" + sStateId + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                    sDestFolderPath = Functions.GetSavePath();

                    if (!Directory.Exists(sDestFolderPath))
                        Directory.CreateDirectory(sDestFolderPath);

                    p_sPDFSaveFilePath = sDestFolderPath + @"\" + p_sSaveFileName;

                    m_objFunctions.PrintEOBReport(p_iTransId, p_bIsSilent, sStateId, bIsPostCheck, arrTempList, out objPrintWrapper, out objCommonEOB);
                    if (objPrintWrapper != null || objCommonEOB != null)
                    {
                        if (objPrintWrapper != null)
                        {
                            objPrintWrapper.Save(p_sPDFSaveFilePath);
                        }
                        else
                        {
                            objPdfExport = new PdfExport();
                            objPdfExport.Export(objCommonEOB.Document, p_sPDFSaveFilePath);
                        }
                        //Deb : MITS 32174
                        if (m_objFunctions.m_bDirectToPrinter == true && (objPrintWrapper != null || objCommonEOB != null) && Functions.IsPrinterSelected(m_sConnectionString,m_iClientId))
                            //m_objFunctions.PrintCheckJob(p_sPDFSaveFilePath);
                            m_objFunctions.PrintCheckJobNEW(p_sPDFSaveFilePath);
                    }
                    else
                        p_sPDFSaveFilePath = string.Empty;
                    //}
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAR.PrintEOBReport.ErrorCreate",m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objPrintWrapper != null)
                    objPrintWrapper = null;

                if (objCommonEOB != null)
                    objCommonEOB.Dispose();

                if (objPdfExport != null)
                    objPdfExport.Dispose();
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                // akaushik5 Commented for MITS 35846 Starts
                //objConfig = null;
                // akaushik5 Commented for MITS 35846 Ends
            }
        }

        #region PrintEobReport
        ///// Name		: PrintEOBReport
        ///// Author		: Anurag Agarwal
        ///// Date Created	: 13 Jan 2005		
        ///// ************************************************************
        ///// Amendment History
        ///// ************************************************************
        ///// Date Amended   *   Amendment   *    Author
        /////                *               *
        /////                *               *
        ///// ************************************************************
        ///// <summary>
        ///// Prints EOB to an Active Reports EOB
        ///// </summary>
        ///// <param name="p_iTransId">Funds TransId</param>
        ///// <param name="p_bIsSilent">Flag for enabling/disabling the No Data found exceptions</param>
        ///// <param name="p_sState">State Id</param>
        ///// <param name="p_bIsPostCheck">Flag for Post Checks</param>
        ///// <param name="p_arrChkPrtList">Array List containing the list of printed checks</param>
        ///// <param name="p_objPrintWrapper">Object of printWrapper class containing the PDF doc. 
        ///// This will populated if report is generated through component one</param>
        ///// <param name="p_objEOBReport">Object of Common EOB class containing the PDF doc. 
        ///// This will populated if report is generated through Active Reports</param>
        //private void PrintEOBReport(int p_iTransId, bool p_bIsSilent, string p_sState, bool p_bIsPostCheck, ArrayList p_arrChkPrtList, out PrintWrapper p_objPrintWrapper, out CommonEOB p_objEOBReport)
        //{
        //    string sSQL = string.Empty;
        //    string sTempData = string.Empty;
        //    int iInvoiceId = 0;
        //    string sName = string.Empty;
        //    DbReader objReader = null;
        //    Funds objFunds = null;
        //    BrsInvoice objBrsInvoice = null;
        //    Entity objEntity = null;
        //    CEOBRecord structCEOBRecord;
        //    Supplementals objSupplementals = null;
        //    LocalCache objLocalCache = null;
        //    string strStateDesc = string.Empty;
        //    string strStateCode = string.Empty;
        //    string sJurisdiction = string.Empty;
        //    string sFileNumber = string.Empty;
        //    string sClaimantFileName = string.Empty;
        //    string sPayeeFileName = string.Empty;
        //    int iClaimantEid = 0;
        //    string sTransDateText = string.Empty;
        //    string sClaimNumberText = string.Empty;
        //    string sFirstName = string.Empty;
        //    string sLastName = string.Empty;
        //    string sClaimantName = string.Empty;
        //    string sRegClaimantName = string.Empty;
        //    string sClaimantAddr1 = string.Empty;
        //    string sClaimantAddr2 = string.Empty;
        //    string sClaimantCity = string.Empty;
        //    string sClaimantTaxId = string.Empty;
        //    bool bIsICNFieldPresent = false;
        //    string sFTSRowIDs = string.Empty;
        //    int iLineCount = 0;
        //    string sThisCode = string.Empty;
        //    string sLastCode = string.Empty;
        //    string sThisCodeID = string.Empty;
        //    string sLastCodeID = string.Empty;
        //    int iDivisionId = 0;
        //    PdfExport objPDF_AR = null;
        //    rptEOB objrptEOB = null;
        //    rptEOB_TX objrptEOB_TX = null;
        //    rptEOP objrptEOP = null;
        //    rptEOR objrptEOR = null;

        //    //Initializing the Output parameters
        //    p_objEOBReport = null;
        //    p_objPrintWrapper = null;

        //    try
        //    {
        //        structCEOBRecord = new CEOBRecord();

        //        objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
        //        try
        //        {
        //            objFunds.MoveTo(p_iTransId);
        //        }
        //        catch
        //        {
        //            if (!p_bIsSilent)
        //                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.SaveItem"));
        //            return;
        //        }

        //        sSQL = "SELECT STATES.STATE_ID, CLAIM.FILE_NUMBER  FROM CLAIM,STATES WHERE CLAIM_ID = " +
        //            objFunds.ClaimId + " AND FILING_STATE_ID = STATES.STATE_ROW_ID";
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                sJurisdiction = objReader["STATE_ID"].ToString();
        //                sFileNumber = objReader["FILE_NUMBER"].ToString();
        //            }
        //            objReader.Close();
        //        }

        //        if (p_sState.Trim() == string.Empty)
        //            p_sState = sJurisdiction;

        //        if (objFunds.Supplementals.Count() > 0)
        //        {
        //            objSupplementals = objFunds.Supplementals;
        //            if (Conversion.ConvertObjToInt(objFunds.Supplementals["TRANS_ID"], m_iClientId.Value) == p_iTransId)
        //            {
        //                if (IsSuppFieldExist(objSupplementals, "DCN_TEXT"))
        //                {
        //                    structCEOBRecord.DCN = objSupplementals["DCN_TEXT"].Value.ToString();
        //                    structCEOBRecord.OtherRef1 = objSupplementals["OTHER_REF_1_TEXT"].Value.ToString().Trim();
        //                    structCEOBRecord.OtherRef2 = objSupplementals["OTHER_REF_2_TEXT"].Value.ToString().Trim();
        //                    structCEOBRecord.ICNControlNumber = objSupplementals["ICN_TEXT"].Value.ToString().Trim();
        //                }
        //                else
        //                {
        //                    structCEOBRecord.DCN = string.Empty;
        //                    structCEOBRecord.OtherRef1 = string.Empty;
        //                    structCEOBRecord.OtherRef2 = string.Empty;
        //                    structCEOBRecord.ICNControlNumber = string.Empty;
        //                }
        //            }
        //            bIsICNFieldPresent = IsSuppFieldExist(objSupplementals, "ICN_TEXT");
        //            objSupplementals.Dispose();
        //        }

        //        if (bIsICNFieldPresent)
        //        {
        //            structCEOBRecord.ReasonCodes = string.Empty;
        //            foreach (FundsTransSplit objFundsTransSplit in objFunds.TransSplitList)
        //            {
        //                if (sFTSRowIDs == string.Empty)
        //                    sFTSRowIDs = objFundsTransSplit.SplitRowId.ToString();
        //                else
        //                    sFTSRowIDs += ", " + objFundsTransSplit.SplitRowId.ToString();
        //            }

        //            sSQL = "SELECT DISTINCT(INVDETAIL_EOR_CODE.EOR_CODE_ID), INVDETAIL_EOR_CODE.EOR_CODE, MESSAGE " +
        //                "FROM INVDETAIL_X_EOR, INVDETAIL_EOR_CODE WHERE INVDETAIL_X_EOR.EOR_CODE_ID = " +
        //                "INVDETAIL_EOR_CODE.EOR_CODE_ID AND INVDETAIL_X_EOR.TRANS_ID =" + objFunds.TransId +
        //                " AND INVDETAIL_X_EOR.FTS_ROW_ID IN (" + sFTSRowIDs + ")ORDER BY " +
        //                "INVDETAIL_EOR_CODE.EOR_CODE, INVDETAIL_EOR_CODE.EOR_CODE_ID";
        //            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

        //            if (objReader != null)
        //            {
        //                while (objReader.Read())
        //                {
        //                    sThisCode = objReader["EOR_CODE"].ToString();
        //                    if (sThisCode != sLastCode)
        //                    {
        //                        if (structCEOBRecord.ReasonCodes == string.Empty)
        //                        {
        //                            structCEOBRecord.ReasonCodes = iLineCount.ToString();
        //                            structCEOBRecord.CodeMessages = iLineCount + " - " + objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
        //                        }
        //                        else
        //                        {
        //                            structCEOBRecord.ReasonCodes += "," + iLineCount;
        //                            structCEOBRecord.CodeMessages += iLineCount + " - " + objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
        //                        }
        //                        iLineCount++;
        //                    }
        //                    else
        //                    {
        //                        if (structCEOBRecord.CodeMessages == string.Empty)
        //                            structCEOBRecord.CodeMessages = objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
        //                        else
        //                            structCEOBRecord.CodeMessages += objReader["MESSAGE"].ToString().Trim() + Functions.CRLF;
        //                    }
        //                    sLastCode = objReader["EOR_CODE"].ToString();
        //                }
        //                objReader.Close();
        //            }
        //        }//end fo If
        //        else
        //        {
        //            structCEOBRecord.ReasonCodes = string.Empty;
        //            structCEOBRecord.CodeMessages = string.Empty;
        //        }

        //        //Use first Trans Code
        //        foreach (FundsTransSplit objFundsTransSplit in objFunds.TransSplitList)
        //        {
        //            structCEOBRecord.PaymentCode = objFundsTransSplit.TransTypeCode.ToString();
        //            structCEOBRecord.Code = objFundsTransSplit.InvoiceNumber;
        //            structCEOBRecord.PeriodCovered = Conversion.GetDate(objFundsTransSplit.FromDate) + " - " +
        //                Conversion.GetDate(objFundsTransSplit.ToDate);
        //        }
        //        //Open up invoice record
        //        objBrsInvoice = (BrsInvoice)m_objDataModelFactory.GetDataModelObject("BrsInvoice", false);
        //        try
        //        {
        //            objBrsInvoice.MoveToTransId(p_iTransId);
        //        }
        //        catch
        //        {
        //            if (!p_bIsSilent)
        //                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems"));
        //            return;
        //        }

        //        objLocalCache = new LocalCache(m_sConnectionString);

        //        iInvoiceId = objBrsInvoice.InvoiceId;
        //        if (iInvoiceId == 0)
        //            return;

        //        //Make sure we have detail items (exit if none)
        //        sSQL = "SELECT COUNT(*) FROM INVOICE_DETAIL WHERE INVOICE_ID = " + iInvoiceId;
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                if (Conversion.ConvertObjToInt(objReader[0]) == 0), m_iClientId
        //                {
        //                    if (!p_bIsSilent)
        //                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems"));
        //                    return;
        //                }
        //            }
        //            objReader.Close();
        //        }

        //        //... Payee Info
        //        sName = ((string)(objBrsInvoice.FirstNameText + " " + objBrsInvoice.LastNameText)).Trim();
        //        structCEOBRecord.PayeeFirstName = objBrsInvoice.FirstNameText;
        //        structCEOBRecord.PayeeLastName = objBrsInvoice.LastNameText;
        //        structCEOBRecord.PayeeFullName = sName;
        //        structCEOBRecord.PayeeAddr1 = objBrsInvoice.Addr1Text;
        //        structCEOBRecord.PayeeAddr2 = objBrsInvoice.Addr2Text;
        //        objLocalCache.GetStateInfo(objBrsInvoice.StateId, ref strStateCode, ref strStateDesc);
        //        structCEOBRecord.PayeeState = ((string)(strStateCode + " " + strStateDesc)).Trim();
        //        structCEOBRecord.PayeePostalCode = objBrsInvoice.PostalCodeText;
        //        structCEOBRecord.PayeeCity = objBrsInvoice.CityText;
        //        structCEOBRecord.ClaimNumber = objBrsInvoice.ClaimNumberText;
        //        structCEOBRecord.OrigInvDate = Conversion.GetDate(objBrsInvoice.DttmRcdAdded);
        //        structCEOBRecord.OrigInvUser = objBrsInvoice.AddedByUser;
        //        iClaimantEid = objBrsInvoice.ClaimantEid;
        //        //Changed by Gagan for MITS 11520 : Start
        //        if (objBrsInvoice.TransDateText == "" || objBrsInvoice.TransDateText == null)
        //            sTransDateText = objFunds.TransDate;
        //        else
        //            sTransDateText = objBrsInvoice.TransDateText;
        //        structCEOBRecord.TransDate = sTransDateText;
        //        //sTransDateText = objBrsInvoice.TransDateText; 
        //        //Changed by Gagan for MITS 11520 : End
        //        sClaimNumberText = objBrsInvoice.ClaimNumberText;

        //        //... Payee Tax Id
        //        objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
        //        try
        //        {
        //            objEntity.MoveTo(objFunds.PayeeEid);
        //            structCEOBRecord.PayeeTaxID = objEntity.TaxId;
        //            structCEOBRecord.VendorNumber = objEntity.TaxId;
        //            //Changed by Gagan for MITS 11520 : Start
        //            structCEOBRecord.PayeeNPI = objEntity.NPINumber;
        //            //Changed by Gagan for MITS 11520 : End
        //        }
        //        catch { }

        //        try
        //        {
        //            objEntity.MoveTo(iClaimantEid);

        //            sFirstName = objEntity.FirstName;
        //            sLastName = objEntity.LastName;
        //            sClaimantName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
        //            sRegClaimantName = ((string)(sFirstName + " " + sLastName)).Trim();
        //            sClaimantAddr1 = objEntity.Addr1;
        //            sClaimantAddr2 = objEntity.Addr2;
        //            objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
        //            if (strStateCode != string.Empty || strStateDesc != string.Empty)
        //                structCEOBRecord.ClaimantState = ((string)(strStateCode + " " + strStateDesc)).Trim().Substring(0, 4);
        //            structCEOBRecord.ClaimantCity = objEntity.City;
        //            structCEOBRecord.ClaimantPostalCode = objEntity.ZipCode;
        //            sClaimantCity = structCEOBRecord.ClaimantCity + ", " + structCEOBRecord.ClaimantState
        //                + " " + structCEOBRecord.ClaimantPostalCode;
        //            sClaimantTaxId = objEntity.TaxId;

        //            structCEOBRecord.ClaimantFullName = sClaimantName;
        //            structCEOBRecord.ClaimantLastName = sLastName;
        //            structCEOBRecord.ClaimantFirstName = sFirstName;
        //            structCEOBRecord.ClaimantAddr1 = sClaimantAddr1;
        //            structCEOBRecord.ClaimantAddr2 = sClaimantAddr2;
        //            structCEOBRecord.ClaimantSSN = sClaimantTaxId;
        //            if (sClaimantAddr1 != string.Empty)
        //                sTempData = sClaimantAddr1;
        //            if (sClaimantAddr2 != string.Empty)
        //                sTempData += Functions.CRLF + sClaimantAddr2;
        //            if (sClaimantCity != string.Empty)
        //                sTempData += Functions.CRLF + sClaimantCity;
        //            structCEOBRecord.ClaimantFullAddress = sTempData;
        //        }
        //        catch { }

        //        int iReportedEID = 0;

        //        //... Event Date Info
        //        sSQL = "SELECT DATE_OF_EVENT, DATE_REPORTED, RPTD_BY_EID FROM EVENT,CLAIM WHERE " +
        //            "CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + objFunds.ClaimId;
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                structCEOBRecord.EventDate = objReader["DATE_OF_EVENT"].ToString();
        //                structCEOBRecord.DateReported = objReader["DATE_REPORTED"].ToString();
        //                iReportedEID = Conversion.ConvertStrToInteger(objReader["RPTD_BY_EID"].ToString());
        //            }
        //            objReader.Close();
        //        }

        //        //... Reported By Name/Address
        //        try
        //        {
        //            objEntity.MoveTo(iReportedEID);
        //            sFirstName = objEntity.FirstName;
        //            sLastName = objEntity.LastName;
        //            structCEOBRecord.RptdFullName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
        //        }
        //        catch { }

        //        //... Employee department chain info
        //        sSQL = "SELECT ORG_HIERARCHY.* FROM EMPLOYEE,ORG_HIERARCHY WHERE EMPLOYEE.EMPLOYEE_EID=" +
        //            iClaimantEid + " AND ORG_HIERARCHY.DEPARTMENT_EID = EMPLOYEE.DEPT_ASSIGNED_EID";
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["DEPARTMENT_EID"].ToString()), ref structCEOBRecord.EmpDeptCode, ref structCEOBRecord.EmpDepartment);
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["FACILITY_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpFacility);
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["LOCATION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpLocation);
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["DIVISION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpDivision);
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["REGION_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpRegion);
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["COMPANY_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpCompany);
        //                objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(objReader["CLIENT_EID"].ToString()), ref sTempData, ref structCEOBRecord.EmpClient);

        //                //Get the address of the facility
        //                iDivisionId = Conversion.ConvertStrToInteger(objReader["DIVISION_EID"].ToString());

        //                try
        //                {
        //                    objEntity.MoveTo(iDivisionId);
        //                    structCEOBRecord.DivisionName = objEntity.FirstName + objEntity.LastName;
        //                    structCEOBRecord.DivisionTaxID = objEntity.TaxId;
        //                    structCEOBRecord.EmpDivisionNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                    if (objEntity.Addr2 != string.Empty)
        //                        structCEOBRecord.DivisionNameAddr += structCEOBRecord.DivisionName + Functions.CRLF +
        //                            objEntity.Addr1 + Functions.CRLF + objEntity.Addr2;
        //                    else
        //                        structCEOBRecord.DivisionNameAddr += structCEOBRecord.DivisionName + Functions.CRLF +
        //                            objEntity.Addr1;

        //                    if (objEntity.City != string.Empty)
        //                    {
        //                        objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
        //                        structCEOBRecord.DivisionNameAddr += Functions.CRLF + objEntity.City + ", " +
        //                            strStateCode + " " + strStateDesc + " " + objEntity.ZipCode;
        //                    }
        //                }
        //                catch (RecordNotFoundException p_objRecordNotFoundException)
        //                {
        //                    p_objRecordNotFoundException = null;
        //                }

        //                //Get the address of the facility
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["FACILITY_EID"].ToString()));
        //                    structCEOBRecord.EmpFacilityNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                    if (objEntity.Addr1 != string.Empty)
        //                        structCEOBRecord.EmpFacilityAddress = objEntity.Addr1;
        //                    if (objEntity.Addr2 != string.Empty)
        //                        structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.Addr2;
        //                    if (objEntity.City != string.Empty || objEntity.StateId != 0 || objEntity.ZipCode != string.Empty)
        //                    {
        //                        objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
        //                        structCEOBRecord.EmpFacilityAddress += Functions.CRLF + objEntity.City + ", " +
        //                            strStateCode + " " + strStateDesc + " " + objEntity.ZipCode;
        //                    }
        //                }
        //                catch (RecordNotFoundException p_objRecordNotFoundException)
        //                {
        //                    p_objRecordNotFoundException = null;
        //                }

        //                //... Get NAICS codes for all of the hierarchy levels
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["DEPARTMENT_EID"].ToString()));
        //                    structCEOBRecord.EmpDepartmentNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["LOCATION_EID"].ToString()));
        //                    structCEOBRecord.EmpLocationNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["REGION_EID"].ToString()));
        //                    structCEOBRecord.EmpRegionNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["OPERATION_EID"].ToString()));
        //                    structCEOBRecord.EmpOperationNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["COMPANY_EID"].ToString()));
        //                    structCEOBRecord.EmpCompanyNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }
        //                try
        //                {
        //                    objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader["CLIENT_EID"].ToString()));
        //                    structCEOBRecord.EmpCompanyNAICS = objLocalCache.GetShortCode(objEntity.NaicsCode);
        //                }
        //                catch { }

        //            }
        //            objReader.Close();
        //        }

        //        sSQL = "SELECT LAST_NAME,FIRST_NAME,ABBREVIATION,CURRENT_ADJ_FLAG,PHONE1 FROM ENTITY," +
        //            "CLAIM_ADJUSTER WHERE ADJUSTER_EID=ENTITY_ID AND CLAIM_ID=" + objFunds.ClaimId;
        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //        if (objReader != null)
        //        {
        //            while (objReader.Read())
        //            {
        //                structCEOBRecord.AdjusterAbbrev = objReader["ABBREVIATION"].ToString();
        //                structCEOBRecord.AdjusterFullName = ((string)(objReader["FIRST_NAME"].ToString() + " " + objReader["LAST_NAME"].ToString())).Trim();
        //                structCEOBRecord.AdjusterLastName = objReader["LAST_NAME"].ToString();
        //                //Arnab: MITS-10722 - Getting Adjuster phone no
        //                structCEOBRecord.AdjusterPhone1 = Conversion.ConvertObjToStr(objReader["PHONE1"]);
        //                //MITS-10722 End
        //                if (Conversion.ConvertObjToInt(objReader["CURRENT_ADJ_FLAG"], m_iClientId) != 0)
        //                    break;
        //            }
        //            objReader.Close();
        //        }

        //        structCEOBRecord.BatchNumber = objFunds.BatchNumber.ToString();
        //        structCEOBRecord.CheckNumber = string.Empty;
        //        structCEOBRecord.PatientAccountNumber = string.Empty;
        //        structCEOBRecord.FeeOrCustomary = string.Empty;
        //        structCEOBRecord.ControlNumber = objFunds.CtlNumber;
        //        structCEOBRecord.ReceiptDate = string.Empty;

        //        if (p_bIsPostCheck)
        //            structCEOBRecord.CheckNumber = objFunds.TransNumber.ToString();
        //        else
        //        {
        //            if (p_arrChkPrtList != null)
        //            {
        //                foreach (PrintedCheckDetail structPrintedCheckDetail in p_arrChkPrtList)
        //                {
        //                    if (structPrintedCheckDetail.TransId == p_iTransId)
        //                        structCEOBRecord.CheckNumber = structPrintedCheckDetail.CheckNum.ToString();
        //                }
        //            }
        //        }

        //        m_structEOBRecord.CheckNumber = structCEOBRecord.CheckNumber;
        //        //Fetching EOB details
        //        GetEOBDetail(objFunds, ref structCEOBRecord, iInvoiceId);


        //        //Changed by Gagan for MITS 11520 : Start
        //        //SSN can have format as ###-##-#### or ##-#######
        //        if (structCEOBRecord.ClaimantSSN != "")
        //        {
        //            if (structCEOBRecord.ClaimantSSN.Length == 11)
        //                structCEOBRecord.ClaimantSSN = "XXX-XX-" + structCEOBRecord.ClaimantSSN.Substring(7, 4);
        //            else if (structCEOBRecord.ClaimantSSN.Length == 10)
        //                structCEOBRecord.ClaimantSSN = "XX-XXX" + structCEOBRecord.ClaimantSSN.Substring(6, 4);
        //        }
        //        //Changed by Gagan for MITS 11520 : End


        //        switch (p_sState)
        //        {
        //            case MI:
        //                objrptEOB = new rptEOB(m_sConnectionString, m_objCommon.m_stLoginInfo.Username);
        //                objrptEOB.EOBReportData = structCEOBRecord;

        //                objrptEOB.Run();
        //                p_objEOBReport = objrptEOB;
        //                break;

        //            case TX:
        //                objrptEOB_TX = new rptEOB_TX(m_sConnectionString, m_objCommon.m_stLoginInfo.Username);
        //                objrptEOB_TX.EOBReportData = structCEOBRecord;
        //                objrptEOB_TX.Run();
        //                p_objEOBReport = objrptEOB_TX;
        //                break;

        //            case "EOR":
        //                objrptEOR = new rptEOR(m_sConnectionString, m_objCommon.m_stLoginInfo.Username);
        //                objrptEOR.EOBReportData = structCEOBRecord;
        //                objrptEOR.Run();
        //                p_objEOBReport = objrptEOR;
        //                break;

        //            case "EOP":
        //                objrptEOP = new rptEOP(m_sConnectionString, m_objCommon.m_stLoginInfo.Username);
        //                objrptEOP.EOBReportData = structCEOBRecord;
        //                objrptEOP.Run();
        //                p_objEOBReport = objrptEOP;
        //                break;

        //            default:
        //                GetPrintCheckEOB(objFunds, true, out p_objPrintWrapper);
        //                break;
        //        }

        //        //Stamp audit info on invoice record
        //        sSQL = "UPDATE INVOICE SET DTTM_EOB_PRINTED = '" + Conversion.ToDbDateTime(DateTime.Now) + "', " +
        //            "EOB_PRINTED_USER = '" + m_objCommon.m_stLoginInfo.Username + "' WHERE INVOICE_ID = " + iInvoiceId;
        //        m_objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);

        //        ////TODO Logging component is in process to be finalized, As that will be finalized this will be done
        //        //MsgBox "Explanation Of Benefits (EOB) has been printed.", vbInformation + vbOKOnly, "Bill Review System"

        //    }
        //    catch (DataModelException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (RMAppException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("PrintEOBAR.PrintEOBReport.ErrorCreate"), p_objEx);
        //    }
        //    finally
        //    {
        //        if (objReader != null)
        //            objReader.Dispose();

        //        if (objFunds != null)
        //            objFunds.Dispose();

        //        if (objBrsInvoice != null)
        //            objBrsInvoice.Dispose();

        //        if (objEntity != null)
        //            objEntity.Dispose();

        //        if (objSupplementals != null)
        //            objSupplementals.Dispose();

        //        if (objLocalCache != null)
        //            objLocalCache.Dispose();

        //        if (objPDF_AR != null)
        //            objPDF_AR.Dispose();

        //        if (objrptEOB != null)
        //            objrptEOB.Dispose();

        //        if (objrptEOB_TX != null)
        //            objrptEOB_TX.Dispose();

        //        if (objrptEOP != null)
        //            objrptEOP.Dispose();

        //        if (objrptEOR != null)
        //            objrptEOR.Dispose();
        //    }
        //}
        #endregion

		#region "PrintEOBReport(int p_iTransID, bool p_bIsSilent, out string p_sPDFFileName)"
		/// Name		: PrintEOBReport
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// THis function is called to print EOB report, It uses format text file for reading report format details
		/// Format File is the Custom files of dml (dorn mark-up lang) tags <b> (like  xml/http).
		/// If a file cannot be found, a Default, internal layout format is used.
		/// </summary>
		/// <param name="p_iTransID">Funds TransId</param>
		/// <param name="p_bIsSilent">Flag for enabling/disabling the No Data found exceptions</param>
		/// <param name="p_sPDFFileName">Saved PDF file name as output parameter. The naming convention of file will be TransID</param>
		public void PrintEOBReport(int p_iTransID, bool p_bIsSilent, out string p_sPDFSaveFilePath , out string p_sSaveFileName )
		{
			string sSQL = string.Empty;
			int iTransId = 0;
			int iDeptAssignEId = 0;
			string sTempData = string.Empty; 
			double dAvgTextHeight = 0.0;
			double dAvgPrintWidth = 0.0;
			int iInvoiceId = 0;
			string sName = string.Empty; 
			DbReader objReader = null;
            DbReader objReader1 = null;
			BrsInvoice objBrsInvoice = null;
			Claim objClaim = null;
			State objState = null;
			Funds objFunds = null;
			DataSet objDataSet = null;
			Entity objEntity = null;
			Employee objEmployee = null;
			StreamReader objFormatFileStream = null;
			LocalCache objLocalCache = null;
			string strStateDesc = string.Empty;
			string strStateCode = string.Empty;
			string sJurisdiction = string.Empty; 
			int iStateId = 0; 
			string sClaimantFileName = string.Empty; 
			string sPayeeFileName = string.Empty;
			bool bIsFormatFileFound = false;

			int iClaimantEid = 0;
			string sTransDateText = string.Empty;
			string sClaimNumberText = string.Empty; 
			string sFirstName = string.Empty;
			string sLastName = string.Empty;
			string sClaimantName = string.Empty;
			string sRegClaimantName = string.Empty;
			string sAddr1 = string.Empty;
			string sAddr2 = string.Empty;
            string sAddr3 = string.Empty; // JIRA 6420 syadav55
            string sAddr4 = string.Empty; // JIRA 6420 syadav55
			string sCity = "";
			string sClaimantAddr1 = string.Empty;
			string sClaimantAddr2 = string.Empty;
            string sClaimantAddr3 = string.Empty; // JIRA 6420 syadav55
            string sClaimantAddr4 = string.Empty; // JIRA 6420 syadav55
			string sClaimantCity = string.Empty;
			string sClaimantTaxId = string.Empty;
			int iReportedEID = 0;
			string sPostalCodeText = string.Empty; 
			double dCurrX = 0;
			double dCurrY = 0;
			string sDestFolderPath = "";
			RMConfigurator objConfig = null;

			//initialize the output parameter
			p_sPDFSaveFilePath = string.Empty;
            p_sSaveFileName = string.Empty ;
			try
			{	
                //if( !m_objDataModelFactory.Context.InternalSettings.SysSettings.IsBRSInstalled )
                //    return ;

				iTransId = p_iTransID;
				m_objPrintWrapper.StartDoc(false);

				objConfig = new RMConfigurator();

				dAvgTextHeight = m_objPrintWrapper.GetTextHeight("W");
				dAvgPrintWidth = m_objPrintWrapper.PageWidth;
				
				objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds",false);
				try
				{
					objFunds.MoveTo(iTransId);
				}
				catch
				{
					if(!p_bIsSilent)
                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.SaveItem", m_iClientId));//sharishkumar Jira 835
					return;
				}
				m_objFunds = objFunds;

				objBrsInvoice = (BrsInvoice)m_objDataModelFactory.GetDataModelObject("BrsInvoice",false);
				try
				{
					objBrsInvoice.MoveToTransId(iTransId);
				}
				catch
				{
					if(!p_bIsSilent)
                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems", m_iClientId));//sharishkumar Jira 835
					return;
				}

				iInvoiceId = objBrsInvoice.InvoiceId;
				sName = objBrsInvoice.FirstNameText + " " + objBrsInvoice.LastNameText;

				objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim",false);
				objClaim.MoveTo(m_objFunds.ClaimId);
				iStateId = objClaim.FilingStateId;
				objClaim.Dispose();

				objState = (State)m_objDataModelFactory.GetDataModelObject("State",false);
				objState.MoveTo(iStateId);
				sJurisdiction = objState.StateId;
				objState.Dispose();
                m_structEOBRecord.CheckNumber = m_objFunds.TransNumber.ToString();
                sSQL = "SELECT FL_LICENSE,ZIP_CODE FROM INVOICE_DETAIL WHERE INVOICE_ID=" + iInvoiceId;
                objReader1 = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader1 != null)
                {
                    if (objReader1.Read())
                    {
                        m_structEOBRecord.FirstLicense = objReader1["FL_LICENSE"].ToString();
                        m_structEOBRecord.FirstBillingPostalCode = objReader1["ZIP_CODE"].ToString();
                    }

                }
                if(objReader1!=null)
                objReader1.Close();

            sSQL = "SELECT INVOICE_NUMBER FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID=" + iTransId;
                objReader1 = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader1 != null)
                {
                    if (objReader1.Read())
                    {
                        m_structEOBRecord.FirstInvoice = objReader1["INVOICE_NUMBER"].ToString();
                    }

                }
                if (objReader1 != null)
                    objReader1.Close();
                sSQL = null;

                if (m_objFunctions.m_bPrintClaimantEOB)
                {
                    //sClaimantFileName = Functions.GetFormatFilePath(m_iClientId) + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMFILE);
                    sClaimantFileName = Functions.GetFormatFilePath(m_iClientId) + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMFILE, m_sConnectionString, m_iClientId);//rkaur27

                    if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true" && m_iClientId == 0)
                    {
                        if ((sJurisdiction == string.Empty) || !File.Exists(sClaimantFileName))
                            sClaimantFileName = Functions.GetFormatFilePath(m_iClientId) + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMFILE, m_sConnectionString, m_iClientId);//rkaur27
                        //sClaimantFileName = m_objFunctions.GetValueFromConfigFile(Functions.TAG_FORMATFILEPATH)
                        //                    + @"\" + m_objFunctions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMFILE);
                    } // if
                }
                if (m_objFunctions.m_bPrintPayeeEOB)
                {
                    //sPayeeFileName = m_objFunctions.GetValueFromConfigFile(Functions.TAG_FORMATFILEPATH)
                    //                   + @"\" + sJurisdiction + m_objFunctions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEFILE);
                    sPayeeFileName = Functions.GetFormatFilePath(m_iClientId)
                                     + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEFILE, m_sConnectionString, m_iClientId);//rkaur27

                    if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true" && m_iClientId == 0)
                    {
                        if ((sJurisdiction == string.Empty) || !File.Exists(sPayeeFileName))
                            sPayeeFileName = Functions.GetFormatFilePath(m_iClientId)
                             + Functions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEFILE, m_sConnectionString, m_iClientId);//rkaur27
                        //sPayeeFileName = m_objFunctions.GetValueFromConfigFile(Functions.TAG_FORMATFILEPATH)
                        //    + @"\" + m_objFunctions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEFILE);
                    } // if
                }





				bIsFormatFileFound = true;
                if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true")
                {
                    if (!File.Exists(sClaimantFileName) && m_objFunctions.m_bPrintClaimantEOB)
                        bIsFormatFileFound = false;
                    else
                        bIsFormatFileFound = true;

                    if (bIsFormatFileFound && m_objFunctions.m_bPrintPayeeEOB)
                        if (!File.Exists(sPayeeFileName))
                            bIsFormatFileFound = false;
                }
                else
                {
                    MemoryStream objMemDesc = CommonFunctions.RetreiveTempFileFromDB(sClaimantFileName, "", m_sConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                    if (objMemDesc == null && m_objFunctions.m_bPrintClaimantEOB)
                    {
                        bIsFormatFileFound = false;
                    }
                    else
                    {
                        bIsFormatFileFound = true;
                    }
                    if (bIsFormatFileFound && m_objFunctions.m_bPrintPayeeEOB) {
                        objMemDesc = CommonFunctions.RetreiveTempFileFromDB(sPayeeFileName, "", m_sConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                        if (objMemDesc == null)
                        {
                            bIsFormatFileFound = false;
                        }
                    }
                }




				objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);

				if(bIsFormatFileFound)
				{
					m_structEOBRecord.CurrentX1Margin = 0;
					m_structEOBRecord.CurrentX2Margin = 0;
					m_structEOBRecord.CurrentY1Margin = 0;
					m_structEOBRecord.CurrentY2Margin = 0;
					m_structEOBRecord.CurrentAlign = Functions.ALIGN_LEFT;
					m_structEOBRecord.PayeeFirstName = objBrsInvoice.FirstNameText;  
					m_structEOBRecord.PayeeLastName = objBrsInvoice.LastNameText;
					m_structEOBRecord.PayeeFullName = sName.Trim();
					m_structEOBRecord.PayeeAddr1 = objBrsInvoice.Addr1Text;
					m_structEOBRecord.PayeeAddr2 = objBrsInvoice.Addr2Text;
                    m_structEOBRecord.PayeeAddr3 = objBrsInvoice.Addr3Text; // JIRA 6420 syadav55
                    m_structEOBRecord.PayeeAddr4 = objBrsInvoice.Addr4Text; // JIRA 6420 syadav55
					objLocalCache.GetStateInfo(objBrsInvoice.StateId,ref strStateCode,ref strStateDesc);
					m_structEOBRecord.PayeeState = ((string)(strStateCode + " " + strStateDesc)).Trim();
					m_structEOBRecord.PayeePostalCode = objBrsInvoice.PostalCodeText;
					m_structEOBRecord.PayeeCity = objBrsInvoice.CityText;
					m_structEOBRecord.ClaimNumber = objBrsInvoice.ClaimNumberText;
					m_structEOBRecord.OrigInvDate = Conversion.GetDate(objBrsInvoice.DttmRcdAdded); 
					m_structEOBRecord.OrigInvUser = objBrsInvoice.AddedByUser; 
				}
				iStateId = objBrsInvoice.StateId;
				objLocalCache.GetStateInfo(objBrsInvoice.StateId,ref strStateCode,ref strStateDesc);
				sPostalCodeText = objBrsInvoice.PostalCodeText;
				iClaimantEid = objBrsInvoice.ClaimantEid;
				sTransDateText = objBrsInvoice.TransDateText; 
				sClaimNumberText = objBrsInvoice.ClaimNumberText;
				sAddr1 = objBrsInvoice.Addr1Text;
				sAddr2 = objBrsInvoice.Addr2Text;
                sAddr3 = objBrsInvoice.Addr3Text; // JIRA 6420 syadav55
                sAddr4 = objBrsInvoice.Addr4Text; // JIRA 6420 syadav55
				sCity = objBrsInvoice.CityText;
 
				objBrsInvoice.Dispose();

				objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity",false);
				if(bIsFormatFileFound)
				{
					try
					{
						objEntity.MoveTo(m_objFunds.PayeeEid);
						m_structEOBRecord.PayeeTaxID = objEntity.TaxId;
					}
					catch
					{
						m_structEOBRecord.PayeeTaxID = string.Empty; 
					} 

					objEmployee = (Employee)m_objDataModelFactory.GetDataModelObject("Employee",false);
					try
					{
						objEmployee.MoveTo(iClaimantEid);
						iDeptAssignEId = objEmployee.DeptAssignedEid;
						objEntity.MoveTo(iDeptAssignEId);
						m_structEOBRecord.EmpDeptCode = objEntity.Abbreviation;
						m_structEOBRecord.EmpDepartment = objEntity.LastName;  
					}
					catch
					{
						m_structEOBRecord.EmpDeptCode = string.Empty;
						m_structEOBRecord.EmpDepartment = string.Empty; 
					} 

					sSQL = "SELECT DATE_OF_EVENT, DATE_REPORTED, RPTD_BY_EID FROM EVENT,CLAIM WHERE " + 
						"CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + m_objFunds.ClaimId;
					objReader=DbFactory.GetDbReader(m_sConnectionString ,sSQL);
					if(objReader != null)
					{
						if(objReader.Read())
						{
							m_structEOBRecord.EventDate = objReader["DATE_OF_EVENT"].ToString(); 
							m_structEOBRecord.DateReported = objReader["DATE_REPORTED"].ToString(); 
							iReportedEID = Conversion.ConvertStrToInteger(objReader["RPTD_BY_EID"].ToString());
						}
						objReader.Close();  
					}

					sSQL = "SELECT ENTITY_ID,LAST_NAME,FACILITY_EID,LOCATION_EID,DIVISION_EID,REGION_EID,OPERATION_EID" + 
						",COMPANY_EID,CLIENT_EID FROM ENTITY, ORG_HIERARCHY WHERE DEPARTMENT_EID=" + iDeptAssignEId;
					objDataSet = DbFactory.GetDataSet(m_sConnectionString,sSQL,m_iClientId);//sharishkumar Jira 835
					if(objDataSet != null && objDataSet.Tables.Count > 0)
					{
						foreach(DataRow objDataRow in objDataSet.Tables[0].Select("FACILITY_EID=ENTITY_ID"))
							m_structEOBRecord.EmpFacility = objDataRow["LAST_NAME"].ToString();  

						foreach(DataRow objDataRow in objDataSet.Tables[0].Select("LOCATION_EID=ENTITY_ID"))
							m_structEOBRecord.EmpLocation = objDataRow["LAST_NAME"].ToString();

						foreach(DataRow objDataRow in objDataSet.Tables[0].Select("DIVISION_EID=ENTITY_ID"))
							m_structEOBRecord.EmpDivision = objDataRow["LAST_NAME"].ToString();

						foreach(DataRow objDataRow in objDataSet.Tables[0].Select("REGION_EID=ENTITY_ID"))
							m_structEOBRecord.EmpRegion = objDataRow["LAST_NAME"].ToString();

						foreach(DataRow objDataRow in objDataSet.Tables[0].Select("OPERATION_EID=ENTITY_ID"))
							m_structEOBRecord.EmpOperation = objDataRow["LAST_NAME"].ToString();

						foreach(DataRow objDataRow in objDataSet.Tables[0].Select("COMPANY_EID=ENTITY_ID"))
							m_structEOBRecord.EmpCompany = objDataRow["LAST_NAME"].ToString();

						foreach(DataRow objDataRow in objDataSet.Tables[0].Select("CLIENT_EID=ENTITY_ID"))
							m_structEOBRecord.EmpClient = objDataRow["LAST_NAME"].ToString();
					}
					if(objDataSet != null)
						objDataSet.Dispose();

					sSQL = "SELECT LAST_NAME,FIRST_NAME,ABBREVIATION,CURRENT_ADJ_FLAG FROM ENTITY," + 
						"CLAIM_ADJUSTER WHERE ADJUSTER_EID=ENTITY_ID AND CLAIM_ID=" + m_objFunds.ClaimId;
					objReader=DbFactory.GetDbReader(m_sConnectionString ,sSQL);
					if(objReader != null)
					{
						while(objReader.Read())
						{
							m_structEOBRecord.AdjusterAbbrev = objReader["ABBREVIATION"].ToString(); 
							m_structEOBRecord.AdjusterFullName = ((string)(objReader["FIRST_NAME"].ToString() + " " + objReader["LAST_NAME"].ToString())).Trim();
							m_structEOBRecord.AdjusterLastName = objReader["LAST_NAME"].ToString();
							if(Conversion.ConvertObjToInt(objReader["CURRENT_ADJ_FLAG"], m_iClientId) != 0)
								break;
						}
						objReader.Close();  
					}
				}	
				//Make sure we have detail items (exit if none)
				sSQL = "SELECT COUNT(*) FROM INVOICE_DETAIL WHERE INVOICE_ID = " + iInvoiceId;
				objReader=DbFactory.GetDbReader(m_sConnectionString ,sSQL);
				if(objReader != null)
				{
					if(objReader.Read())
					{
                        if (Conversion.ConvertObjToInt(objReader[0], m_iClientId) == 0)
						{
							if(!p_bIsSilent)
                                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems", m_iClientId));//sharishkumar Jira 835
							return;
						}
					}
					objReader.Close();  
				}	
				//abisht
                if (m_objFunctions.m_bPrintPayeeEOB || (m_objFunctions.m_bPrintClaimantEOB && iClaimantEid != 0))
                {
                    sTempData = m_objPrintWrapper.FontName;
				    m_objPrintWrapper.SetFont("Courier New");
                } // if
				
				//This prints the default EOB Form
                if (m_objFunctions.m_bPrintPayeeEOB)
				{
                    if(!bIsFormatFileFound)
				    {
				        this.PrintEOBHeader(m_objPrintWrapper);
     
				        dCurrX = 500;
				        dCurrY = 2000;

				        m_objPrintWrapper.CurrentX = dCurrX;
				        dCurrY += dAvgTextHeight * 2;
				        m_objPrintWrapper.CurrentY = dCurrY;  
				        m_objPrintWrapper.PrintText(sName.Trim());
				        dCurrY += dAvgTextHeight;
				        m_objPrintWrapper.CurrentY = dCurrY;
				        m_objPrintWrapper.PrintText(sAddr1);
				        if(sAddr2 != string.Empty)
				        {
					        dCurrY += dAvgTextHeight;
					        m_objPrintWrapper.CurrentY = dCurrY;
					        m_objPrintWrapper.PrintText(sAddr2);
				        }
                        if (sAddr3 != string.Empty)
                        {
                            dCurrY += dAvgTextHeight;
                            m_objPrintWrapper.CurrentY = dCurrY;
                            m_objPrintWrapper.PrintText(sAddr3);
                        }
                        if (sAddr4 != string.Empty)
                        {
                            dCurrY += dAvgTextHeight;
                            m_objPrintWrapper.CurrentY = dCurrY;
                            m_objPrintWrapper.PrintText(sAddr4);
                        }
                        //m_objPrintWrapper.PrintText(sAddr3); // JIRA 6420 syadav55
                        //m_objPrintWrapper.PrintText(sAddr4); // JIRA 6420 syadav55

				        dCurrY += dAvgTextHeight;
				        m_objPrintWrapper.CurrentY = dCurrY;
				        objLocalCache.GetStateInfo(iStateId,ref strStateCode,ref strStateDesc);
				        m_objPrintWrapper.PrintText(sCity + ", " + ((string)(strStateCode + " " + strStateDesc)).Trim() + " " + m_structEOBRecord.PayeePostalCode);
				    } 
				} // if

				//Lookup Claimant Name/Address info
				try
				{
					objEntity.MoveTo(iClaimantEid);

					sFirstName = objEntity.FirstName;
					sLastName = objEntity.LastName;
					sClaimantName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
					sRegClaimantName = ((string)(sFirstName + " " + sLastName)).Trim();
					sClaimantAddr1 = objEntity.Addr1;
					sClaimantAddr2 = objEntity.Addr2;
                    sClaimantAddr3 = objEntity.Addr3; // JIRA 6420 syadav55
                    sClaimantAddr4 = objEntity.Addr4; // JIRA 6420 syadav55
					objLocalCache.GetStateInfo(objEntity.StateId,ref strStateCode,ref strStateDesc);
					m_structEOBRecord.ClaimantState = ((string)(strStateCode + " " + strStateDesc)).Trim();
					m_structEOBRecord.ClaimantCity = objEntity.City;
					m_structEOBRecord.ClaimantPostalCode = objEntity.ZipCode;
					sClaimantCity = m_structEOBRecord.ClaimantCity + ", " + m_structEOBRecord.ClaimantState
						+ " " + m_structEOBRecord.ClaimantPostalCode;
					sClaimantTaxId = objEntity.TaxId;
					if(bIsFormatFileFound)
					{
						m_structEOBRecord.ClaimantFullName = sClaimantName;
						m_structEOBRecord.ClaimantLastName = sLastName;
						m_structEOBRecord.ClaimantFirstName = sFirstName;
						m_structEOBRecord.ClaimantAddr1 = sClaimantAddr1;
						m_structEOBRecord.ClaimantAddr2 = sClaimantAddr2;
                        m_structEOBRecord.ClaimantAddr3 = sClaimantAddr3; // JIRA 6420 syadav55
                        m_structEOBRecord.ClaimantAddr4 = sClaimantAddr4; // JIRA 6420 syadav55
						m_structEOBRecord.TransDate = sTransDateText;
						m_structEOBRecord.ClaimantSSN = sClaimantTaxId; 
					}
				}
				catch
				{
					if(bIsFormatFileFound)
					{
						m_structEOBRecord.ClaimantFullName = string.Empty;
						m_structEOBRecord.ClaimantLastName = string.Empty;
						m_structEOBRecord.ClaimantFirstName = string.Empty;
						m_structEOBRecord.ClaimantAddr1 = string.Empty;
						m_structEOBRecord.ClaimantAddr2 = string.Empty;
                        m_structEOBRecord.ClaimantAddr3 = string.Empty; // JIRA 6420 syadav55
                        m_structEOBRecord.ClaimantAddr4 = string.Empty; // JIRA 6420 syadav55
						m_structEOBRecord.TransDate = string.Empty;
						m_structEOBRecord.ClaimantSSN = string.Empty; 
					}
				}

                if (m_objFunctions.m_bPrintPayeeEOB || (m_objFunctions.m_bPrintClaimantEOB && iClaimantEid != 0))
                {
                    sTempData = m_objPrintWrapper.FontName;
                    m_objPrintWrapper.SetFont("Courier New");
                } // if

                if (bIsFormatFileFound && m_objFunctions.m_bPrintPayeeEOB)
                {
                    if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true" && m_iClientId != 0)
                    {
                        MemoryStream objMemoryStream = CommonFunctions.RetreiveTempFileFromDB(sPayeeFileName, "", m_sConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                        objFormatFileStream = new StreamReader(objMemoryStream);
                    }
                    else
                    {
                        objFormatFileStream = File.OpenText(sPayeeFileName);
                    }
                }
				//This prints the default EOB Form
                //abisht
                if (m_objFunctions.m_bPrintPayeeEOB)
                {
                    if (!bIsFormatFileFound)
                    {

                        m_objPrintWrapper.CurrentX = dCurrX;
                        dCurrY += dAvgTextHeight;
                        m_objPrintWrapper.CurrentY = dCurrY;

                        m_objPrintWrapper.PrintText(sRegClaimantName);
                        m_objPrintWrapper.CurrentX += m_objPrintWrapper.GetTextWidth(sRegClaimantName) + XINCREASEWIDTH * 0.2;
                        m_objPrintWrapper.PrintText(sClaimantAddr1);
                        string sControlWidth2Consider = sClaimantAddr1;
                        if (sClaimantAddr2 != string.Empty)
                        {
                            m_objPrintWrapper.CurrentX += m_objPrintWrapper.GetTextWidth(sControlWidth2Consider) + XINCREASEWIDTH * 0.2;
                            m_objPrintWrapper.PrintText(sClaimantAddr2);
                            sControlWidth2Consider = sClaimantAddr2;
                        }
                        if (sClaimantAddr3 != string.Empty)
                        {
                            m_objPrintWrapper.CurrentX += m_objPrintWrapper.GetTextWidth(sControlWidth2Consider) + XINCREASEWIDTH * 0.2;
                            m_objPrintWrapper.PrintText(sClaimantAddr3);
                            sControlWidth2Consider = sClaimantAddr3;
                        }
                        if (sClaimantAddr4 != string.Empty)
                        {
                            m_objPrintWrapper.CurrentX += m_objPrintWrapper.GetTextWidth(sClaimantAddr3) + XINCREASEWIDTH * 0.2;
                            m_objPrintWrapper.PrintText(sClaimantAddr4);
                            sControlWidth2Consider = sClaimantAddr4;
                        }
                        //m_objPrintWrapper.PrintText(sClaimantAddr3); // JIRA 6420 syadav55
                        //m_objPrintWrapper.PrintText(sClaimantAddr4); // JIRA 6420 syadav55

                        m_objPrintWrapper.CurrentX += m_objPrintWrapper.GetTextWidth(sControlWidth2Consider) + XINCREASEWIDTH * 0.2;
                        m_objPrintWrapper.PrintText(sClaimantCity);
                    }
                    else
                    {
                        //Print Custom File
                        m_structEOBRecord.FirstPrint = true;



                        //Custom
                        m_objFunctions.GetCustomEOBHeader(objFormatFileStream, ref m_structEOBRecord, m_objPrintWrapper, m_objFunds);

                        //Print body of letter
                        m_objFunctions.GetColumns(objFormatFileStream, ref m_structEOBDetailRecord);





                    }

                    if (bIsFormatFileFound)
                    {
                        try
                        {
                            objEntity.MoveTo(iReportedEID);
                            m_structEOBRecord.RptdFullName = objEntity.FirstName != string.Empty ? objEntity.LastName + ", " + objEntity.FirstName : objEntity.LastName;
                        }
                        catch
                        { }
                    }

                    //Print body of letter
                    this.PrintEOBBody(iInvoiceId, sClaimNumberText, sClaimantName, sRegClaimantName, sName, bIsFormatFileFound, 500, Convert.ToInt32(m_objPrintWrapper.CurrentY));





                    if (bIsFormatFileFound)
                    {
                        m_objFunctions.GetCustomEOBHeader(objFormatFileStream, ref m_structEOBRecord, m_objPrintWrapper, m_objFunds);



                    }
                    else
                    {
                        dCurrY = m_objPrintWrapper.CurrentY + dAvgTextHeight * 2;
                        m_objPrintWrapper.CurrentY = dCurrY;
                        m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.ClaimantCopy", m_iClientId));//sharishkumar Jira 835
                        dCurrY += dAvgTextHeight * 2;
                        m_objPrintWrapper.CurrentY = dCurrY;
                        m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Cc", m_iClientId) + Functions.FillSpace(1) + sClaimantName);//sharishkumar Jira 835
                    }
                } // if



                if (bIsFormatFileFound && m_objFunctions.m_bPrintPayeeEOB)
					objFormatFileStream.Close();
                //Print copy for claimant
                if (m_objFunctions.m_bPrintClaimantEOB)
                {
                    if (iClaimantEid > 0)
                    {
                        if (m_objFunctions.m_bPrintPayeeEOB)
					        m_objPrintWrapper.NewPage();


                        if (bIsFormatFileFound)
                        {
                            m_structEOBRecord.FirstPrint = true;
                            if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true" && m_iClientId != 0)
                            {
                                MemoryStream objMemoryStream = CommonFunctions.RetreiveTempFileFromDB(sClaimantFileName, "", m_sConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                                objFormatFileStream = new StreamReader(objMemoryStream);
                            }
                            else
                            {
                                objFormatFileStream = File.OpenText(sClaimantFileName);
                            }
                            m_objFunctions.GetCustomEOBHeader(objFormatFileStream, ref m_structEOBRecord, m_objPrintWrapper, m_objFunds);
                            m_objFunctions.GetColumns(objFormatFileStream, ref m_structEOBDetailRecord);







                        }

                        //Print body of letter
                        this.PrintEOBBody(iInvoiceId, sClaimNumberText, sClaimantName, sName, string.Empty, bIsFormatFileFound, 500, 1000);

                        if (bIsFormatFileFound)
                        {
                            m_objFunctions.GetCustomEOBHeader(objFormatFileStream, ref m_structEOBRecord, m_objPrintWrapper, m_objFunds);


                            objFormatFileStream.Close();
                        }
                    }
                    else
                    {
                        //TODO Logging component is in process to be finalized, As that will be finalized this will be done
                        //If Not bSilent Then MsgBox "Claimant for claimant copy not found.  No Claimant Copy will be printed.", 48, "Claimant Missing"
                    }

                } // if

				m_objPrintWrapper.EndDoc();
                
				
				p_sPDFSaveFilePath = iTransId + "_" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                sDestFolderPath = Functions.GetSavePath();
				
				if( ! Directory.Exists( sDestFolderPath ) )
					Directory.CreateDirectory( sDestFolderPath );

                p_sSaveFileName = p_sPDFSaveFilePath;
				p_sPDFSaveFilePath = sDestFolderPath + @"\" + p_sPDFSaveFilePath;
                //abisht
                if(m_objFunctions.m_bPrintClaimantEOB || m_objFunctions.m_bPrintPayeeEOB)
				    m_objPrintWrapper.Save(p_sPDFSaveFilePath);
                //abisht
                //Deb : MITS 32174
                if (m_objFunctions.m_bDirectToPrinter == true && (m_objFunctions.m_bPrintClaimantEOB || m_objFunctions.m_bPrintPayeeEOB) && Functions.IsPrinterSelected(m_sConnectionString,m_iClientId))
                    //m_objFunctions.PrintCheckJob(p_sPDFSaveFilePath);
                    m_objFunctions.PrintCheckJobNEW(p_sPDFSaveFilePath);
				if(sTempData != string.Empty)
					m_objPrintWrapper.SetFont(sTempData);

				//Stamp audit info on invoice record
				sSQL = "UPDATE INVOICE SET DTTM_EOB_PRINTED = '" + Conversion.ToDbDateTime(DateTime.Now) + "', " +
					"EOB_PRINTED_USER = '" + m_objCommon.m_stLoginInfo.Username + "' WHERE INVOICE_ID = " + iInvoiceId;
				m_objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);  

				m_objFunctions.m_objColumnsSelected.Clear();
				m_objFunctions.m_objSpaceWidth.Clear();
			}	
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBReport.ErrorPrint", m_iClientId), p_objEx);//sharishkumar Jira 835				
			}
			finally
			{
				if(objClaim != null)
					objClaim.Dispose();

				if(objState != null)
					objState.Dispose();

				if(objDataSet != null)
					objDataSet.Dispose();

				if(objEmployee != null)
					objEmployee.Dispose();

				if(objReader != null)
					objReader.Dispose();

				if(objFunds != null)
					objFunds.Dispose();

				if(objBrsInvoice != null)
					objBrsInvoice.Dispose();

				if(objEntity != null)
					objEntity.Dispose();

				if(objFormatFileStream != null)
					objFormatFileStream = null;

				if(objLocalCache != null)
					objLocalCache.Dispose();
			}	
		}
		#endregion

        #region "PrintEOBHTMLReport(int p_iTransID, bool p_bIsSilent, out string p_sPDFFileName)"
        /// Name		: PrintEOBHTMLReport
        /// Author		: Debabrata Biswas MITS Issue 17183
        /// Date Created	: 20 Jul 2009
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// THis function is called to print EOB report, It uses format XHTML file for reading report format details
        /// Format File is the Custom files XHTML (dorn mark-up lang) tags <b> (like  xml/http).
        /// If a file cannot be found, a Default, internal layout format is used.
        /// </summary>
        /// <param name="p_iTransID">Funds TransId</param>
        /// <param name="p_bIsSilent">Flag for enabling/disabling the No Data found exceptions</param>
        /// <param name="p_sPDFFileName">Saved PDF file name as output parameter. The naming convention of file will be TransID</param>
        public void PrintEOBHTMLReport(int p_iTransID, bool p_bIsSilent, out string p_sPDFSaveFilePath, out string p_sSaveFileName)
        {
            string sSQL = string.Empty;
            int iTransId = 0;
            int iDeptAssignEId = 0;
            string sTempData = string.Empty;
            int iInvoiceId = 0;
            string sName = string.Empty;
            DbReader objReader = null;
            DbReader objReader1 = null;
            BrsInvoice objBrsInvoice = null;
            Claim objClaim = null;
            State objState = null;
            Funds objFunds = null;
            DataSet objDataSet = null;
            Entity objEntity = null;
            Employee objEmployee = null;
            StreamReader objFormatFileStream = null;
            LocalCache objLocalCache = null;
            string strStateDesc = string.Empty;
            string strStateCode = string.Empty;
            string sJurisdiction = string.Empty;
            int iStateId = 0;
            string sClaimantFileName = string.Empty;
            string sPayeeFileName = string.Empty;
            bool bIsFormatFileFound = false;

            int iClaimantEid = 0;
            string sTransDateText = string.Empty;
            string sClaimNumberText = string.Empty;
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            string sClaimantName = string.Empty;
            string sRegClaimantName = string.Empty;
            string sAddr1 = string.Empty;
            string sAddr2 = string.Empty;
            string sAddr3 = string.Empty; // JIRA 6420 syadav55
            string sAddr4 = string.Empty; // JIRA 6420 syadav55
            string sCity = "";
            string sClaimantAddr1 = string.Empty;
            string sClaimantAddr2 = string.Empty;
            string sClaimantAddr3 = string.Empty; // JIRA 6420 syadav55
            string sClaimantAddr4 = string.Empty; // JIRA 6420 syadav55
            string sClaimantCity = string.Empty;
            string sClaimantTaxId = string.Empty;
            int iReportedEID = 0;
            string sPostalCodeText = string.Empty;
            string sDestFolderPath = "";
            RMConfigurator objConfig = null;

            //initialize the output parameter
            p_sPDFSaveFilePath = string.Empty;
            p_sSaveFileName = string.Empty;
            StringBuilder sPayeeHTMLtext;
            StringBuilder sClaimantHTMLtext;
            C1PdfDocument objPdfDoc;
            try
            {
                iTransId = p_iTransID;

                objConfig = new RMConfigurator();
                objPdfDoc = new C1PdfDocument();
                objPdfDoc.Clear();
                objPdfDoc.CurrentPage = 0;

                System.Drawing.Font fFont = new System.Drawing.Font("Courier New", 10);
                System.Drawing.Brush brBrush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(0, 0, 0));
                System.Drawing.RectangleF rcPage = objPdfDoc.PageRectangle;
                rcPage.Inflate(-25, -25);

                objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                try
                {
                    objFunds.MoveTo(iTransId);
                }
                catch
                {
                    if (!p_bIsSilent)
                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.SaveItem", m_iClientId));//sharishkumar Jira 835
                    return;
                }
                m_objFunds = objFunds;

                objBrsInvoice = (BrsInvoice)m_objDataModelFactory.GetDataModelObject("BrsInvoice", false);
                try
                {
                    objBrsInvoice.MoveToTransId(iTransId);
                }
                catch
                {
                    if (!p_bIsSilent)
                        throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems", m_iClientId));//sharishkumar Jira 835
                    return;
                }

                iInvoiceId = objBrsInvoice.InvoiceId;
                sName = objBrsInvoice.FirstNameText + " " + objBrsInvoice.LastNameText;

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(m_objFunds.ClaimId);
                iStateId = objClaim.FilingStateId;
                objClaim.Dispose();

                objState = (State)m_objDataModelFactory.GetDataModelObject("State", false);
                objState.MoveTo(iStateId);
                sJurisdiction = objState.StateId;
                objState.Dispose();

                m_structEOBRecord.CheckNumber = m_objFunds.TransNumber.ToString();

                sSQL = "SELECT FL_LICENSE,ZIP_CODE FROM INVOICE_DETAIL WHERE INVOICE_ID=" + iInvoiceId;
                objReader1 = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader1 != null)
                {
                    if (objReader1.Read())
                    {
                        m_structEOBRecord.FirstLicense = objReader1["FL_LICENSE"].ToString();
                        m_structEOBRecord.FirstBillingPostalCode = objReader1["ZIP_CODE"].ToString();
                    }

                }
                if (objReader1 != null)
                    objReader1.Close();

                sSQL = "SELECT INVOICE_NUMBER, INVOICE_DATE, FROM_DATE, TO_DATE FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID=" + iTransId;

                objReader1 = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader1 != null)
                {
                    if (objReader1.Read())
                    {
                        string sFromDate = string.Empty;
                        string sToDate = string.Empty;

                        m_structEOBRecord.FirstInvoice = objReader1["INVOICE_NUMBER"].ToString();
                        m_structEOBRecord.FirstInvoiceDate = objReader1["INVOICE_DATE"].ToString();
                    }

                }
                if (objReader1 != null)
                    objReader1.Close();
                sSQL = null;

                string sFromDateFirst = string.Empty;
                string sFromDateLast = string.Empty;

                sSQL = "SELECT FROM_DATE FROM FUNDS_TRANS_SPLIT WHERE SPLIT_ROW_ID=(SELECT MIN(SPLIT_ROW_ID) FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID=" + iTransId + ")";

                objReader1 = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader1 != null)
                {
                    if (objReader1.Read())
                    {
                        sFromDateFirst = objReader1["FROM_DATE"].ToString();
                        sFromDateFirst = Conversion.ToDate(sFromDateFirst).ToShortDateString();
                    }

                }
                if (objReader1 != null)
                    objReader1.Close();
                sSQL = null;

                sSQL = "SELECT FROM_DATE FROM FUNDS_TRANS_SPLIT WHERE SPLIT_ROW_ID=(SELECT MAX(SPLIT_ROW_ID) FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID=" + iTransId + ")";

                objReader1 = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader1 != null)
                {
                    if (objReader1.Read())
                    {
                        sFromDateLast = objReader1["FROM_DATE"].ToString();
                        sFromDateLast = Conversion.ToDate(sFromDateLast).ToShortDateString();
                    }

                }
                if (objReader1 != null)
                    objReader1.Close();
                sSQL = null;

                m_structEOBRecord.DateOfService = sFromDateFirst + " to " + sFromDateLast;

                if (m_objFunctions.m_bPrintClaimantEOB)
                {
                    sClaimantFileName = Functions.GetFormatFilePath(m_iClientId)
                                             + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMHTMLFILE, m_sConnectionString, m_iClientId);//rkaur27
                    if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true" && m_iClientId == 0)
                    {
                        if ((sJurisdiction == string.Empty) || !File.Exists(sClaimantFileName))
                            sClaimantFileName = Functions.GetFormatFilePath(m_iClientId)
                                                    + Functions.GetValueFromConfigFile(Functions.TAG_EOBCLAIMHTMLFILE, m_sConnectionString, m_iClientId);//rkaur27
                    }
                }

                if (m_objFunctions.m_bPrintPayeeEOB)
                {
                    sPayeeFileName = Functions.GetFormatFilePath(m_iClientId)
                                         + sJurisdiction + Functions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEHTMLFILE, m_sConnectionString, m_iClientId);//rkaur27
                    if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true" && m_iClientId == 0)
                    {
                        if ((sJurisdiction == string.Empty) || !File.Exists(sPayeeFileName))
                            sPayeeFileName = Functions.GetFormatFilePath(m_iClientId)
                           + Functions.GetValueFromConfigFile(Functions.TAG_EOBPAYEEHTMLFILE, m_sConnectionString, m_iClientId);//rkaur27
                    }
                }

                bIsFormatFileFound = true;
                if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true")
                {
                    if (!File.Exists(sClaimantFileName) && m_objFunctions.m_bPrintClaimantEOB)
                        bIsFormatFileFound = false;

                    if (bIsFormatFileFound && m_objFunctions.m_bPrintPayeeEOB)
                        if (!File.Exists(sPayeeFileName))
                            bIsFormatFileFound = false;
                }
                else
                {
                    MemoryStream objMemDesc = CommonFunctions.RetreiveTempFileFromDB(sClaimantFileName, "", m_sConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                    if (objMemDesc == null && m_objFunctions.m_bPrintClaimantEOB)
                    {
                        bIsFormatFileFound = false;
                    }
                    else
                    {
                        bIsFormatFileFound = true;
                    }
                    if (bIsFormatFileFound && m_objFunctions.m_bPrintPayeeEOB)
                    {
                        objMemDesc = CommonFunctions.RetreiveTempFileFromDB(sPayeeFileName, "", m_sConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                        if (objMemDesc == null)
                        {
                            bIsFormatFileFound = false;
                        }
                    }
                }

                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

                if (bIsFormatFileFound)
                {
                    m_structEOBRecord.PayeeFirstName = objBrsInvoice.FirstNameText;
                    m_structEOBRecord.PayeeLastName = objBrsInvoice.LastNameText;
                    m_structEOBRecord.PayeeFullName = sName.Trim();
                    m_structEOBRecord.PayeeAddr1 = objBrsInvoice.Addr1Text;
                    m_structEOBRecord.PayeeAddr2 = objBrsInvoice.Addr2Text;
                    m_structEOBRecord.PayeeAddr3 = objBrsInvoice.Addr3Text; // JIRA 6420 syadav55
                    m_structEOBRecord.PayeeAddr4 = objBrsInvoice.Addr4Text; // JIRA 6420 syadav55
                    objLocalCache.GetStateInfo(objBrsInvoice.StateId, ref strStateCode, ref strStateDesc);
                    m_structEOBRecord.PayeeState = ((string)(strStateCode + " " + strStateDesc)).Trim();
                    m_structEOBRecord.PayeePostalCode = objBrsInvoice.PostalCodeText;
                    m_structEOBRecord.PayeeCity = objBrsInvoice.CityText;
                    m_structEOBRecord.ClaimNumber = objBrsInvoice.ClaimNumberText;
                    m_structEOBRecord.OrigInvDate = Conversion.GetDate(objBrsInvoice.DttmRcdAdded);
                    m_structEOBRecord.OrigInvUser = objBrsInvoice.AddedByUser;
                }
                iStateId = objBrsInvoice.StateId;
                objLocalCache.GetStateInfo(objBrsInvoice.StateId, ref strStateCode, ref strStateDesc);
                sPostalCodeText = objBrsInvoice.PostalCodeText;
                iClaimantEid = objBrsInvoice.ClaimantEid;
                sTransDateText = objBrsInvoice.TransDateText;
                sClaimNumberText = objBrsInvoice.ClaimNumberText;
                sAddr1 = objBrsInvoice.Addr1Text;
                sAddr2 = objBrsInvoice.Addr2Text;
                sAddr3 = objBrsInvoice.Addr3Text; // JIRA 6420 syadav55
                sAddr4 = objBrsInvoice.Addr4Text; // JIRA 6420 syadav55
                sCity = objBrsInvoice.CityText;

                objBrsInvoice.Dispose();

                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                if (bIsFormatFileFound)
                {
                    try
                    {
                        objEntity.MoveTo(m_objFunds.PayeeEid);
                        m_structEOBRecord.PayeeTaxID = objEntity.TaxId;
                    }
                    catch
                    {
                        m_structEOBRecord.PayeeTaxID = string.Empty;
                    }

                    objEmployee = (Employee)m_objDataModelFactory.GetDataModelObject("Employee", false);
                    try
                    {
                        objEmployee.MoveTo(iClaimantEid);
                        iDeptAssignEId = objEmployee.DeptAssignedEid;
                        objEntity.MoveTo(iDeptAssignEId);
                        m_structEOBRecord.EmpDeptCode = objEntity.Abbreviation;
                        m_structEOBRecord.EmpDepartment = objEntity.LastName;
                    }
                    catch
                    {
                        m_structEOBRecord.EmpDeptCode = string.Empty;
                        m_structEOBRecord.EmpDepartment = string.Empty;
                    }

                    sSQL = "SELECT DATE_OF_EVENT, DATE_REPORTED, RPTD_BY_EID FROM EVENT,CLAIM WHERE " +
                        "CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + m_objFunds.ClaimId;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            m_structEOBRecord.EventDate = objReader["DATE_OF_EVENT"].ToString();
                            m_structEOBRecord.DateReported = objReader["DATE_REPORTED"].ToString();
                            iReportedEID = Conversion.ConvertStrToInteger(objReader["RPTD_BY_EID"].ToString());
                        }
                        objReader.Close();
                    }

                    sSQL = "SELECT ENTITY_ID,LAST_NAME,FACILITY_EID,LOCATION_EID,DIVISION_EID,REGION_EID,OPERATION_EID" +
                        ",COMPANY_EID,CLIENT_EID FROM ENTITY, ORG_HIERARCHY WHERE DEPARTMENT_EID=" + iDeptAssignEId;
                    objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL,m_iClientId);//sharishkumar Jira 835
                    if (objDataSet != null && objDataSet.Tables.Count > 0)
                    {
                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("FACILITY_EID=ENTITY_ID"))
                            m_structEOBRecord.EmpFacility = objDataRow["LAST_NAME"].ToString();

                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("LOCATION_EID=ENTITY_ID"))
                            m_structEOBRecord.EmpLocation = objDataRow["LAST_NAME"].ToString();

                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("DIVISION_EID=ENTITY_ID"))
                            m_structEOBRecord.EmpDivision = objDataRow["LAST_NAME"].ToString();

                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("REGION_EID=ENTITY_ID"))
                            m_structEOBRecord.EmpRegion = objDataRow["LAST_NAME"].ToString();

                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("OPERATION_EID=ENTITY_ID"))
                            m_structEOBRecord.EmpOperation = objDataRow["LAST_NAME"].ToString();

                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("COMPANY_EID=ENTITY_ID"))
                            m_structEOBRecord.EmpCompany = objDataRow["LAST_NAME"].ToString();

                        foreach (DataRow objDataRow in objDataSet.Tables[0].Select("CLIENT_EID=ENTITY_ID"))
                            m_structEOBRecord.EmpClient = objDataRow["LAST_NAME"].ToString();
                    }
                    if (objDataSet != null)
                        objDataSet.Dispose();

                    sSQL = "SELECT LAST_NAME,FIRST_NAME,ABBREVIATION,CURRENT_ADJ_FLAG FROM ENTITY," +
                        "CLAIM_ADJUSTER WHERE ADJUSTER_EID=ENTITY_ID AND CLAIM_ID=" + m_objFunds.ClaimId +
                        "AND CURRENT_ADJ_FLAG = -1" ;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            m_structEOBRecord.AdjusterAbbrev = objReader["ABBREVIATION"].ToString();
                            m_structEOBRecord.AdjusterFullName = ((string)(objReader["FIRST_NAME"].ToString() + " " + objReader["LAST_NAME"].ToString())).Trim();
                            m_structEOBRecord.AdjusterLastName = objReader["LAST_NAME"].ToString();
                        }
                        objReader.Close();
                    }
                }
                //Make sure we have detail items (exit if none)
                sSQL = "SELECT COUNT(*) FROM INVOICE_DETAIL WHERE INVOICE_ID = " + iInvoiceId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        if (Conversion.ConvertObjToInt(objReader[0], m_iClientId) == 0)
                        {
                            if (!p_bIsSilent)
                                throw new RMAppException(Globalization.GetString("PrintEOBAR.Error.NoBillItems", m_iClientId));//sharishkumar Jira 835
                            return;
                        }
                    }
                    objReader.Close();
                }

                //Lookup Claimant Name/Address info
                try
                {
                    objEntity.MoveTo(iClaimantEid);

                    sFirstName = objEntity.FirstName;
                    sLastName = objEntity.LastName;
                    sClaimantName = sFirstName == string.Empty ? sLastName : sLastName + ", " + sFirstName;
                    sRegClaimantName = ((string)(sFirstName + " " + sLastName)).Trim();
                    sClaimantAddr1 = objEntity.Addr1;
                    sClaimantAddr2 = objEntity.Addr2;
                    sClaimantAddr3 = objEntity.Addr3; // JIRA 6420 syadav55
                    sClaimantAddr4 = objEntity.Addr4; // JIRA 6420 syadav55
                    objLocalCache.GetStateInfo(objEntity.StateId, ref strStateCode, ref strStateDesc);
                    m_structEOBRecord.ClaimantState = ((string)(strStateCode + " " + strStateDesc)).Trim();
                    m_structEOBRecord.ClaimantCity = objEntity.City;
                    m_structEOBRecord.ClaimantPostalCode = objEntity.ZipCode;
                    sClaimantCity = m_structEOBRecord.ClaimantCity + ", " + m_structEOBRecord.ClaimantState
                        + " " + m_structEOBRecord.ClaimantPostalCode;
                    sClaimantTaxId = objEntity.TaxId;
                    if (bIsFormatFileFound)
                    {
                        m_structEOBRecord.ClaimantFullName = sClaimantName;
                        m_structEOBRecord.ClaimantLastName = sLastName;
                        m_structEOBRecord.ClaimantFirstName = sFirstName;
                        m_structEOBRecord.ClaimantAddr1 = sClaimantAddr1;
                        m_structEOBRecord.ClaimantAddr2 = sClaimantAddr2;
                        m_structEOBRecord.ClaimantAddr3 = sClaimantAddr3; // JIRA 6420 syadav55
                        m_structEOBRecord.ClaimantAddr4 = sClaimantAddr4; // JIRA 6420 syadav55
                        m_structEOBRecord.TransDate = sTransDateText;
                        m_structEOBRecord.ClaimantSSN = sClaimantTaxId;
                    }
                }
                catch
                {
                    if (bIsFormatFileFound)
                    {
                        m_structEOBRecord.ClaimantFullName = string.Empty;
                        m_structEOBRecord.ClaimantLastName = string.Empty;
                        m_structEOBRecord.ClaimantFirstName = string.Empty;
                        m_structEOBRecord.ClaimantAddr1 = string.Empty;
                        m_structEOBRecord.ClaimantAddr2 = string.Empty;
                        m_structEOBRecord.ClaimantAddr3 = string.Empty; // JIRA 6420 syadav55
                        m_structEOBRecord.ClaimantAddr4 = string.Empty; // JIRA 6420 syadav55
                        m_structEOBRecord.TransDate = string.Empty;
                        m_structEOBRecord.ClaimantSSN = string.Empty;
                    }
                }

                if (bIsFormatFileFound)
                {
                    try
                    {
                        objEntity.MoveTo(iReportedEID);
                        m_structEOBRecord.RptdFullName = objEntity.FirstName != string.Empty ? objEntity.LastName + ", " + objEntity.FirstName : objEntity.LastName;
                    }
                    catch
                    { }
                }

                //This prints the default EOB Form
                if (m_objFunctions.m_bPrintPayeeEOB)
                {
                    if (!bIsFormatFileFound)
                    {
                        //Get default HTML template
                        sPayeeHTMLtext = GetDefaultPayeeHTMLTemplate(sName, sAddr1, sAddr2, sAddr3, sAddr4, strStateCode, strStateDesc, sCity,
                            sRegClaimantName, sClaimantAddr1, sClaimantAddr2, sClaimantAddr3, sClaimantAddr4, sClaimantCity, sClaimNumberText, sClaimantName);
                    }
                    else
                    {
                        //Print HTML template Custom File
                        objFormatFileStream = File.OpenText(sPayeeFileName);
                        sPayeeHTMLtext = new StringBuilder(objFormatFileStream.ReadToEnd());
                    }
                    XmlDocument xPayeeHTMLdoc = ProcessMultiRowEOBHTMLtables(sPayeeHTMLtext, iInvoiceId);
                    xPayeeHTMLdoc = CreateHTMLdocument(xPayeeHTMLdoc,m_structEOBRecord,m_structEOBDetailRecord,m_objFunds);
                    for (int start = 0; ; )
                    {
                        start=objPdfDoc.DrawStringHtml(xPayeeHTMLdoc.InnerXml, fFont, System.Drawing.Brushes.Black, rcPage,start);
                        if (start >= int.MaxValue)
                            break;
                        objPdfDoc.NewPage();
                    }
                }

                //Print copy for claimant
                if (m_objFunctions.m_bPrintClaimantEOB)
                {
                    if (iClaimantEid > 0)
                    {
                        if (m_objFunctions.m_bPrintPayeeEOB)
                        {
                            objPdfDoc.NewPage();
                        }

                        if (!bIsFormatFileFound)
                        {
                            sClaimantHTMLtext = GetDefaultClaimantHTMLTemplate(sClaimNumberText, sClaimantName, sName);
                        }
                        else
                        {
                            if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true" && m_iClientId != 0)
                            {
                                MemoryStream objMemoryStream = CommonFunctions.RetreiveTempFileFromDB(sClaimantFileName, "", m_sConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                                objFormatFileStream = new StreamReader(objMemoryStream);
                            }
                            else
                            {
                                objFormatFileStream = File.OpenText(sClaimantFileName);
                            }
                            sClaimantHTMLtext = new StringBuilder(objFormatFileStream.ReadToEnd());
                        }
                        XmlDocument xClaimantHTMLdoc = ProcessMultiRowEOBHTMLtables(sClaimantHTMLtext, iInvoiceId);
                        xClaimantHTMLdoc = CreateHTMLdocument(xClaimantHTMLdoc, m_structEOBRecord, m_structEOBDetailRecord, m_objFunds);
                        for (int start = 0; ; )
                        {
                            start=objPdfDoc.DrawStringHtml(xClaimantHTMLdoc.InnerXml, fFont, System.Drawing.Brushes.Black, rcPage,start);
                            if (start >= int.MaxValue)
                                break;
                            objPdfDoc.NewPage();
                        }
                    }
                }


                if (objFormatFileStream != null)
                {
                    objFormatFileStream.Close();
                    objFormatFileStream.Dispose();
                    objFormatFileStream = null;
                }

                objPdfDoc.DocumentInfo.Title = "EOB Report";

                p_sPDFSaveFilePath = iTransId + "_" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                sDestFolderPath = Functions.GetSavePath();

                if (!Directory.Exists(sDestFolderPath))
                    Directory.CreateDirectory(sDestFolderPath);

                p_sSaveFileName = p_sPDFSaveFilePath;
                p_sPDFSaveFilePath = sDestFolderPath + @"\" + p_sPDFSaveFilePath;
                //abisht
                if (m_objFunctions.m_bPrintClaimantEOB || m_objFunctions.m_bPrintPayeeEOB)
                    objPdfDoc.Save(p_sPDFSaveFilePath);
                //abisht
                //Deb : MITS 32174
                if (m_objFunctions.m_bDirectToPrinter == true && (m_objFunctions.m_bPrintClaimantEOB || m_objFunctions.m_bPrintPayeeEOB) && Functions.IsPrinterSelected(m_sConnectionString,m_iClientId))
                    //m_objFunctions.PrintCheckJob(p_sPDFSaveFilePath);
                    m_objFunctions.PrintCheckJobNEW(p_sPDFSaveFilePath);
                //if (sTempData != string.Empty)//todo
                //    m_objPrintWrapper.SetFont(sTempData);

                //Stamp audit info on invoice record
                sSQL = "UPDATE INVOICE SET DTTM_EOB_PRINTED = '" + Conversion.ToDbDateTime(DateTime.Now) + "', " +
                    "EOB_PRINTED_USER = '" + m_objCommon.m_stLoginInfo.Username + "' WHERE INVOICE_ID = " + iInvoiceId;
                m_objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);

                m_objFunctions.m_objColumnsSelected.Clear();
                m_objFunctions.m_objSpaceWidth.Clear();
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBReport.PrintEOBHTMLReport", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objClaim != null)
                    objClaim.Dispose();

                if (objState != null)
                    objState.Dispose();

                if (objDataSet != null)
                    objDataSet.Dispose();

                if (objEmployee != null)
                    objEmployee.Dispose();

                if (objReader != null)
                    objReader.Dispose();

                if (objFunds != null)
                    objFunds.Dispose();

                if (objBrsInvoice != null)
                    objBrsInvoice.Dispose();

                if (objEntity != null)
                    objEntity.Dispose();

                if (objFormatFileStream != null)
                {
                    objFormatFileStream.Close();
                    objFormatFileStream.Dispose();
                    objFormatFileStream = null;
                }

                if (objLocalCache != null)
                    objLocalCache.Dispose();

                sPayeeHTMLtext=null;
                sClaimantHTMLtext=null;
            }
        }
        #endregion

		#endregion	
		
		#region "Private Functions"

		#region "Initialize"
		/// Name		: Initialize
		/// Author		: Anurag Agarwal
		/// Date Created	: 23 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Initialize the DataModal factory class object and Connection string and other class level variables
		/// </summary>
		private void Initialize()
		{
			try
			{
				m_objDataModelFactory = new DataModelFactory( m_sDsnName , m_sUserName , m_sPassword,m_iClientId );	//sharishkumar
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;

				m_objCommon = new Common();

				m_objCommon.m_stLoginInfo.Username = m_sUserName;
				m_objCommon.m_stLoginInfo.DsnName = m_sDsnName;
				m_objCommon.m_stLoginInfo.DsnId = m_sDsnId;

				m_objPrintWrapper = new PrintWrapper(m_iClientId);
				m_structEOBRecord = new EOBRecord();
				m_structEOBDetailRecord = new EOBDetailRecord();
	
				m_objFunctions = new Functions(m_sDsnId, m_sUserName,m_sConnectionString,m_sPassword,m_sDsnName, m_iClientId);//rkaur27
                m_bDirectToPrinter = m_objFunctions.m_bDirectToPrinter;
                m_bIsPrinterSelected = Functions.IsPrinterSelected(m_sConnectionString,m_iClientId);
			}	
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintEOBAR.Initialize.ErrorInit", m_iClientId), p_objEx);	//sharishkumar Jira 835			
			}			
		}	
		#endregion

		#region "PrintEOBBody(int p_iInvoiceId, string p_sClaimNumberText, string p_sClaimantName, string p_sName, string p_sOtherName, bool p_bIsFormatFileFound, int p_iCurrentX, int p_iCurrentY)"
		/// Name		: PrintEOBBody
		/// Author		: Anurag Agarwal
		/// Date Created	: 06 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Print EOB; Introduction And Body.Format; File Is the; Custom; Files; of
		/// http-like tags <b>.  If a file cannot be found, a Default format is used.
		/// </summary>
		/// <param name="p_iInvoiceId">Invoice Id</param>
		/// <param name="p_sClaimNumberText">Claim Number Text</param>
		/// <param name="p_sClaimantName">Claimaint Name</param>
		/// <param name="p_sName">Name</param>
		/// <param name="p_sOtherName">Other Name</param>
		/// <param name="p_bIsFormatFileFound">Flag for Format file</param>
		/// <param name="p_iCurrentX">X-axis value</param>
		/// <param name="p_iCurrentY">Y-axis value</param>
		private void PrintEOBBody(int p_iInvoiceId, string p_sClaimNumberText, string p_sClaimantName, string p_sName, string p_sOtherName, bool p_bIsFormatFileFound, int p_iCurrentX, int p_iCurrentY)
		{

			double dAvgTextHeight = 0.0;
			double dAvgPrintWidth = 0.0;
			bool bIsPrintTotals = false;
			bool bIsUseRecord = false;
			StringBuilder objSQL = null;
			DbReader objReader = null;
			DbReader objTempReader = null;
			FundsTransSplit objFundsTransSplit = null;
			int iTempId = 0;
			double dTempId=0.0;
			int iNumEOB = 0;
			int iLineItem = 0;
			string sSQL = string.Empty;
			string sTempData = string.Empty; 
			double dTotAmtReduced = 0.0;
			double dTotSchdAmt = 0.0;
			double dTotAmtSaved = 0.0;
			double dTotalAmountAllowed = 0.0;
			double dTotalBaseAmount = 0.0;
			double dTotalDiscountAmount = 0.0;
			double dTotalPerDiemAmt = 0.0;
			double dTotalStopLossAmt = 0.0;
			double dTotalFeeTableAmt = 0.0;
			double dTotalAmountBilled = 0.0;
			double dTotalAmountPaid = 0.0;
            double dCurrentEOBX = 0.0;
			string sFromDate = string.Empty;
			string sToDate = string.Empty;
			string sInvoiceNumber = string.Empty;
			string sInvoiceDate = string.Empty; 
			string sInvoiceBy = string.Empty;
            bool sDescriptionFileExists = false;
			try
			{

				dAvgTextHeight = m_objPrintWrapper.GetTextHeight("W"); 
				dAvgPrintWidth = m_objPrintWrapper.PageWidth;

				//Print main body & introduction
				if(!p_bIsFormatFileFound)
				{
					m_objPrintWrapper.CurrentX = p_iCurrentX; //**
					m_objPrintWrapper.CurrentY = p_iCurrentY + 2 * dAvgTextHeight; //**

					//*** Default body - Detail record ***
                    m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.RE", m_iClientId) + Functions.FillSpace(1) + Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText", m_iClientId) + Functions.FillSpace(1) + p_sClaimNumberText);//sharishkumar Jira 835

					m_objPrintWrapper.CurrentX = p_iCurrentX;
					m_objPrintWrapper.CurrentY = m_objPrintWrapper.CurrentY + dAvgTextHeight;
                    m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName", m_iClientId) + Functions.FillSpace(1) + p_sClaimantName);//sharishkumar Jira 835
					m_objPrintWrapper.CurrentY = m_objPrintWrapper.CurrentY + 2 * dAvgTextHeight; //***
                    m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBReport.Name", m_iClientId) + Functions.FillSpace(1) + p_sName);//sharishkumar Jira 835
					
					m_objPrintWrapper.CurrentY += dAvgTextHeight;

					if(p_sOtherName == string.Empty)
					{
                        m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Othername1", m_iClientId));//sharishkumar Jira 835
						m_objPrintWrapper.CurrentY = m_objPrintWrapper.CurrentY + dAvgTextHeight; //***
                        m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Othername2", m_iClientId));//sharishkumar Jira 835
					}
					else
					{
                        m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Othername3", m_iClientId) + Functions.FillSpace(1) + p_sOtherName + Functions.FillSpace(1) + Globalization.GetString("PrintEOBAR.EOBBody.Othername4", m_iClientId));//sharishkumar Jira 835
						m_objPrintWrapper.CurrentY = m_objPrintWrapper.CurrentY + dAvgTextHeight; //***
                        m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.Othername5", m_iClientId));//sharishkumar Jira 835
					}

					m_objPrintWrapper.CurrentY = m_objPrintWrapper.CurrentY + dAvgTextHeight; //***
                    m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.ItemsText", m_iClientId));//sharishkumar Jira 835
					m_objPrintWrapper.CurrentY = m_objPrintWrapper.CurrentY + 2 * dAvgTextHeight; //***
					m_objPrintWrapper.PrintText(string.Empty); 

					m_objPrintWrapper.SetFontBold(true);

					//Tab(6); "INVOICE"; Tab(21); "PROCEDURE"; Tab(32); "PROC."; Tab(39); "EXPLANATION"; Tab(66); "AMOUNT"; Tab(78); "AMOUNT"
                    //Geeta 09/24/07 : Mits 10328
					m_objPrintWrapper.PrintText("INVOICE");
					m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 3;
					m_objPrintWrapper.PrintText("PROCEDURE");
					//m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 4.5;  //zalam mits:-10813 03/31/2008
                    m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 4.2;
					m_objPrintWrapper.PrintText("PROC.");
                    //m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 6;    //zalam mits:-10813 03/31/2008
                    m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 5;
					m_objPrintWrapper.PrintText("EXPLANATION");
					m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 8;
					m_objPrintWrapper.PrintText("AMOUNT");
					m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 10;
					m_objPrintWrapper.PrintText("AMOUNT");
					m_objPrintWrapper.CurrentX = p_iCurrentX;
					m_objPrintWrapper.CurrentY = m_objPrintWrapper.CurrentY + dAvgTextHeight;

					//Tab(6); "NUMBER"; Tab(21); "DATE"; Tab(32); "CODE"; Tab(66); "BILLED"; Tab(78); "PAID"
					m_objPrintWrapper.PrintText("NUMBER");
					m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 3;
					m_objPrintWrapper.PrintText("DATE");
                    //m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 4.5;   //zalam mits:-10813 03/31/2008
                    m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 4.2;
					m_objPrintWrapper.PrintText("CODE");
					m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 8;
					m_objPrintWrapper.PrintText("BILLED");
					m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 10;
					m_objPrintWrapper.PrintText("PAID");
					m_objPrintWrapper.SetFontBold(false);
				}

				objSQL = new StringBuilder();
				objSQL.Append("SELECT INVOICE_DETAIL_ID,BILLING_CODE_TEXT,AMOUNT_BILLED,AMOUNT_TO_PAY,FUNDS_SPLIT_ROW_ID");
				objSQL.Append(",TABLE_CODE, PERCENTILE,PLACE_OF_SER_CODE,TYPE_OF_SER_CODE,UNITS_BILLED_NUM");
				objSQL.Append(",SCHEDULED_AMOUNT,AMOUNT_REDUCED,AMOUNT_SAVED, QUANTITY,STORE, MODIFIER_CODE");
				objSQL.Append(",CONTRACT_AMOUNT,DISCOUNT,ZIP_CODE,BASE_AMOUNT,AMOUNT_ALLOWED");
				objSQL.Append(",PER_DIEM_AMT,STOP_LOSS_AMT,FEE_TABLE_AMT, PRESCRIP_NO, DRUG_NAME, PRESCRIP_DATE");
                //BRS FL Merge
                objSQL.Append(",FL_LICENSE, REV_CODE");
                //End
				objSQL.Append(" FROM INVOICE_DETAIL, FUNDS_TRANS_SPLIT WHERE INVOICE_ID = " + p_iInvoiceId);
				objSQL.Append(" AND FUNDS_SPLIT_ROW_ID = SPLIT_ROW_ID  ORDER BY FROM_DATE");

				objReader = DbFactory.GetDbReader(m_sConnectionString, objSQL.ToString());

				if(objReader != null)
				{
					while(objReader.Read())
					{
						m_structEOBDetailRecord.EOBList = new Hashtable();
						m_structEOBDetailRecord.DiagnosisList = new Hashtable();
						m_structEOBDetailRecord.ModifierList = new Hashtable();

						bIsUseRecord = false;

						//At first check if record is deleted or not
						iTempId = Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString());		

						foreach(FundsTransSplit objFundsTSplit in m_objFunds.TransSplitList)
						{
							if(objFundsTSplit.SplitRowId == iTempId)
							{
								bIsUseRecord = true;
								break;
							}
						}

						iNumEOB = 0;

						if(bIsUseRecord)
						{
							bIsPrintTotals = true;
							iTempId = Conversion.ConvertStrToInteger(objReader["INVOICE_DETAIL_ID"].ToString());

                            //BRS FL Merge : Umesh
                            //florida eob support for alternative descriptions

                            //Added for MITS 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
                            bool bUseCodeDescriptionFile = Convert.ToBoolean(Functions.GetValueFromConfigFile(Functions.TAG_USEEOBDESCRIPTIONFILE, m_sConnectionString, m_iClientId));//rkaur27
                            if (bUseCodeDescriptionFile)//Added if block for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
                            {
                            string sDesciptionFileName = Functions.GetFormatFilePath(m_iClientId)
                                                         + Functions.GetValueFromConfigFile(Functions.TAG_EOBDESCRIPTIONFILE, m_sConnectionString, m_iClientId);//rkaur27

                            if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true" && m_iClientId == 0)
                            {
                                MemoryStream objMemDesc = CommonFunctions.RetreiveTempFileFromDB(sDesciptionFileName, "", m_sConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                                if (objMemDesc != null) { sDescriptionFileExists = true; } else { sDescriptionFileExists = false; }
                            }
                            else
                            {
                                sDescriptionFileExists = File.Exists(sDesciptionFileName);
                            }

                            if (sDescriptionFileExists)
                            {

                                
                                string sFileText = null;
                                string sNewText = null;
                                string sOldText = null;
                                int iPoint = 0;
                               
                                StreamReader objSRdr = null;
                                if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true" && m_iClientId != 0)
                                {
                                    MemoryStream objMemoryStream = CommonFunctions.RetreiveTempFileFromDB(sDesciptionFileName, "", m_sConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                                    objSRdr = new StreamReader(objMemoryStream);
                                }
                                else
                                {
                                    objSRdr = new StreamReader(sDesciptionFileName);
                                }
                                sFileText = objSRdr.ReadToEnd();
                                

                                sSQL = "SELECT CODES.SHORT_CODE FROM INVDETAIL_X_EOB, CODES";
                                sSQL = sSQL + " WHERE INVDETAIL_X_EOB.EOB_CODE = CODES.CODE_ID ";
                                sSQL = sSQL + " AND INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + iTempId;
                                objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                

                                while (objTempReader.Read())
                                {
                                    sTempData = "@" + objTempReader.GetString("SHORT_CODE").Trim();
                                    iPoint = sFileText.IndexOf(sTempData, 0);
                                    if (iPoint >= 0)
                                    {
                                        sOldText = sFileText.Substring(iPoint + 1);
                                        iPoint = sOldText.IndexOf("@");
                                        sNewText = sOldText.Substring(0, iPoint - 1);
                                        //iNumEOB++;
                                        //abisht Doing the same thing for alternative descriptions as was done by Umesh for MITS 12090 for normal descriptions
                                        string sTempEob = "";
                                        sTempData = sNewText;
                                        //m_structEOBDetailRecord.EOBList.Add(iNumEOB, sNewText);
                                        if(iNumEOB > 0)
                                            sTempData = "- " + sTempData; //Starting of New EOB
                                        else
                                        {
                                            sTempData = "  " + sTempData; //First EOB
                                        } // else

                                        //iNumEOB++;
                                        if (sTempData.Length > 23)
                                        {

                                            int iStartIndex = 0;
                                            int iLengthFix = 23;
                                            int iLength = 0;
                                            int iLastIndexofSpace = 0;
                                            string sNextChar = "";
                                            while (iStartIndex < sTempData.Length)
                                            {
                                                iNumEOB++;

                                                //code to handle the boundary conditions
                                                if (iStartIndex + iLengthFix + 1 <= sTempData.Length)
                                                {
                                                    //find the next character
                                                    //if it is space , last word will be in same line

                                                    sNextChar = sTempData.Substring(iStartIndex + iLengthFix, 1);
                                                    if (sNextChar == " ")
                                                    {

                                                        sTempEob = sTempData.Substring(iStartIndex, iLengthFix); //Mits 19415:changed iLengthFix-1 to iLengthFix to extract all the chars before the space
                                                        iLength = iLengthFix; //Added for Mits 19415:iLength was not getting incremented in this case.Hence duplicacy of code description on next line.
                                                        m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
                                                    }
                                                    //else last word will come into next line. so remove the last word from current line.
                                                    else
                                                    {
                                                        sTempEob = sTempData.Substring(iStartIndex, iLengthFix-1); 
                                                        iLastIndexofSpace = sTempEob.LastIndexOf(" ");
                                                        iLength = iLastIndexofSpace;
                                                        sTempEob = sTempData.Substring(iStartIndex, iLength);
                                                        m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);

                                                    }
                                                }
                                                else
                                                {
                                                    sTempEob = sTempData.Substring(iStartIndex, iLengthFix-1);
                                                    m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
                                                }
                                                iStartIndex += iLength;
                                                //get the lenght of remaining string.
                                                if (iStartIndex + 23 >= sTempData.Length)
                                                    iLengthFix = sTempData.Length - iStartIndex;

                                            }

                                            //m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData.Trim().Substring(0, 23));
                                        }


                                        else
                                        {
                                            iNumEOB++;
                                            m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData.Trim());
                                        }
                                    }



                                }
                                if (objTempReader != null)
                                {
                                    objTempReader.Close();
                                }
                                if (objSRdr != null)
                                {
                                    objSRdr.Close();
                                    objSRdr.Dispose();
                                }


                                //BRS FL Merge : END


                            }
                            else
                            {
                                throw new RMAppException(Globalization.GetString("Functions.EobDescriptionFile.Error", m_iClientId));//sharishkumar Jira 835
                            }
                            }//Added if block for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld

                            else
                            {
							sSQL = "SELECT CODES_TEXT.CODE_DESC FROM INVDETAIL_X_EOB,CODES_TEXT WHERE " + 
								"INVDETAIL_X_EOB.EOB_CODE = CODES_TEXT.CODE_ID AND " + 
								"INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + iTempId;
							objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

							if(objTempReader != null)
							{
								while(objTempReader.Read())
								{
                                    string sTempEob = "";
                                    sTempData = objTempReader["CODE_DESC"].ToString().Trim();
                                    //MITS  :12090  Umesh
                                    sTempData = "- " + sTempData; //Starting of New EOB


                                    //iNumEOB++;
                                    if (sTempData.Length > 23)
                                    {

                                        int iStartIndex = 0;
                                        int iLengthFix = 23;
                                        int iLength = 0;
                                        int iLastIndexofSpace = 0;
                                        string sNextChar = "";
                                        while (iStartIndex < sTempData.Length)
                                        {
                                            iNumEOB++;

                                            //code to handle the boundary conditions
                                            if (iStartIndex + iLengthFix + 1 <= sTempData.Length)
                                            {
                                                //find the next character
                                                //if it is space , last word will be in same line
                                                
                                                sNextChar = sTempData.Substring(iStartIndex + iLengthFix, 1);
                                                if (sNextChar ==" ")
                                                {
                                                    
                                                    sTempEob = sTempData.Substring(iStartIndex, iLengthFix); //Mits 19415:changed iLengthFix-1 to iLengthFix to extract all the chars before the space
                                                    iLength = iLengthFix; //Added for Mits 19415:iLength was not getting incremented in this case.Hence duplicacy of code description on next line.
                                                    m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
                                                }
                                                //else last word will come into next line. so remove the last word from current line.
                                                else
                                                {
                                                    sTempEob = sTempData.Substring(iStartIndex, iLengthFix-1);
                                                    iLastIndexofSpace = sTempEob.LastIndexOf(" ");
                                                    iLength = iLastIndexofSpace;
                                                    sTempEob = sTempData.Substring(iStartIndex, iLength);
                                                    m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);

                                                }
                                            }
                                            else
                                            {
                                                sTempEob = sTempData.Substring(iStartIndex, iLengthFix);//No Carriage return in this case
                                                m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempEob);
                                            }
                                            iStartIndex += iLength;
                                            //get the lenght of remaining string.
                                            if (iStartIndex + 23 >= sTempData.Length)
                                               iLengthFix = sTempData.Length - iStartIndex;

                                        }

                                        //m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData.Trim().Substring(0, 23));
                                    }


                                    else
                                    {
                                        iNumEOB++;
                                        m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData.Trim());
                                    }

                                    //End MITS  :12090
								}
								objTempReader.Close();
                                }
							}
							//*** Custom ***
							if(p_bIsFormatFileFound)
							{
								sSQL = "SELECT CODES_TEXT.CODE_DESC FROM INVDETAIL_X_DIAG, CODES_TEXT" + 
									" WHERE INVDETAIL_X_DIAG.DIAGNOSIS_CODE=CODES_TEXT.CODE_ID" + 
									" AND INVDETAIL_X_DIAG.INVOICE_DETAIL_ID= " + iTempId;
								objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

								if(objTempReader != null)
								{
									iLineItem=0;
									while(objTempReader.Read())
									{
										sTempData = objTempReader["CODE_DESC"].ToString();
										iLineItem++; 
										m_structEOBDetailRecord.DiagnosisList.Add(iLineItem,sTempData.Trim());
									}
									objTempReader.Close();
								}

								//extract modifier code
								sSQL = "SELECT CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM INVDETAIL_X_MOD, CODES, CODES_TEXT" + 
									" WHERE INVDETAIL_X_MOD.MODIFIER_CODE=CODES_TEXT.CODE_ID" + 
									" AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND INVDETAIL_X_MOD.INVOICE_DETAIL_ID=" + iTempId;
								objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

								if(objTempReader != null)
								{
									iLineItem=0;
									while(objTempReader.Read())
									{
										sTempData = objTempReader["SHORT_CODE"].ToString() + " " + objTempReader["CODE_DESC"].ToString();
										iLineItem++; 
										m_structEOBDetailRecord.ModifierList.Add(iLineItem,sTempData.Trim());
									}
									objTempReader.Close();
								}

								//extract fee schedule name
								iTempId = Conversion.ConvertStrToInteger(objReader["TABLE_CODE"].ToString());
								sSQL = "SELECT FEE_TABLES.USER_TABLE_NAME FROM FEE_TABLES WHERE FEE_TABLES.TABLE_ID = " + iTempId;
								objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
								if(objTempReader != null)
								{
									if(objTempReader.Read())
									{
										sTempData = objTempReader["USER_TABLE_NAME"].ToString();
										m_structEOBDetailRecord.FeeSchedule.Token = sTempData;   
									}
									objTempReader.Close();
								}

								m_structEOBDetailRecord.Percentile.Token  = objReader["PERCENTILE"].ToString();

								iTempId = Conversion.ConvertStrToInteger(objReader["PLACE_OF_SER_CODE"].ToString());			
								sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " + 
									"CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
								objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
								if(objTempReader != null)
								{
									if(objTempReader.Read())
									{
										m_structEOBDetailRecord.PlaceofServiceCode.Token = objTempReader["SHORT_CODE"].ToString(); 
										m_structEOBDetailRecord.PlaceOfService.Token = objTempReader["CODE_DESC"].ToString();
									}
									objTempReader.Close();
								}

								iTempId = Conversion.ConvertStrToInteger(objReader["TYPE_OF_SER_CODE"].ToString());			
								sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " + 
									"CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
								objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
								if(objTempReader != null)
								{
									if(objTempReader.Read())
									{
										m_structEOBDetailRecord.TypeOfServiceCode.Token = objTempReader["SHORT_CODE"].ToString(); 
										m_structEOBDetailRecord.TypeOfService.Token = objTempReader["CODE_DESC"].ToString();
									}
									objTempReader.Close();
								}

								m_structEOBDetailRecord.UnitsBilled.Token = Conversion.ConvertStrToLong(objReader["UNITS_BILLED_NUM"].ToString());

								m_structEOBDetailRecord.ScheduledAmount.Token = Conversion.ConvertStrToDouble(objReader["SCHEDULED_AMOUNT"].ToString());
                                dTotSchdAmt += Functions.FormatAmount(m_structEOBDetailRecord.ScheduledAmount.Token, m_iClientId);

								m_structEOBDetailRecord.AmountReduced.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_REDUCED"].ToString());
                                dTotAmtReduced += Functions.FormatAmount(m_structEOBDetailRecord.AmountReduced.Token, m_iClientId);

								m_structEOBDetailRecord.AmountSaved.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_SAVED"].ToString());
                                dTotAmtSaved += Functions.FormatAmount(m_structEOBDetailRecord.AmountSaved.Token, m_iClientId);

								m_structEOBDetailRecord.ContractAmount.Token = Conversion.ConvertStrToDouble(objReader["CONTRACT_AMOUNT"].ToString());
								m_structEOBDetailRecord.Discount.Token = Conversion.ConvertStrToDouble(objReader["DISCOUNT"].ToString());
								m_structEOBDetailRecord.ProviderZipCode.Token = objReader["ZIP_CODE"].ToString();

								m_structEOBDetailRecord.AmountAllowed.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_ALLOWED"].ToString());
                                dTotalAmountAllowed += Functions.FormatAmount(m_structEOBDetailRecord.AmountAllowed.Token, m_iClientId);

								m_structEOBDetailRecord.BaseAmount.Token = Conversion.ConvertStrToDouble(objReader["BASE_AMOUNT"].ToString());
                                dTotalBaseAmount += Functions.FormatAmount(m_structEOBDetailRecord.BaseAmount.Token, m_iClientId);

								m_structEOBDetailRecord.DiscountAmount.Token =  m_structEOBDetailRecord.BaseAmount.Token * (m_structEOBDetailRecord.Discount.Token / 100);
                                dTotalDiscountAmount += Functions.FormatAmount(m_structEOBDetailRecord.DiscountAmount.Token, m_iClientId); 

								m_structEOBDetailRecord.PerDiemAmount.Token = Conversion.ConvertStrToDouble(objReader["PER_DIEM_AMT"].ToString());
                                dTotalPerDiemAmt += Functions.FormatAmount(m_structEOBDetailRecord.PerDiemAmount.Token, m_iClientId);

								m_structEOBDetailRecord.StopLossAmount.Token = Conversion.ConvertStrToDouble(objReader["STOP_LOSS_AMT"].ToString());
                                dTotalStopLossAmt += Functions.FormatAmount(m_structEOBDetailRecord.StopLossAmount.Token, m_iClientId);

								m_structEOBDetailRecord.FeeTableAmount.Token = Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString());
                                dTotalFeeTableAmt += Functions.FormatAmount(m_structEOBDetailRecord.FeeTableAmount.Token, m_iClientId);

								m_structEOBDetailRecord.PrescripNo.Token = objReader["PRESCRIP_NO"].ToString();
								m_structEOBDetailRecord.DrugName.Token = objReader["DRUG_NAME"].ToString();
								m_structEOBDetailRecord.PrescripDate.Token = Conversion.ToDate(objReader["PRESCRIP_DATE"].ToString()).ToShortDateString();
                                //BRS FL Merge  : Umesh
                                m_structEOBDetailRecord.PhysicianLicenseNum.Token = objReader["FL_LICENSE"].ToString();
                                m_structEOBDetailRecord.RevenueCode.Token = objReader["REV_CODE"].ToString();
                                //End

								m_structEOBDetailRecord.DiscOnFeeSchd.Token = 0;
								if(Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString()) > 0 && Conversion.ConvertStrToDouble(objReader["DISCOUNT"].ToString()) > 0)
									m_structEOBDetailRecord.DiscOnFeeSchd.Token = Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString()) - Conversion.ConvertStrToDouble(objReader["AMOUNT_ALLOWED"].ToString());
							}
							else
							{
                                //Geeta 09/24/07 : Mits 10328
                                m_objFunctions.CheckEOBPageLength(14500, m_objPrintWrapper);  // Checks to see if new page is needed
							}

							objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit",false);
							try
							{
								objFundsTransSplit.MoveTo(Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString())); 
								sFromDate = Conversion.ToDate(objFundsTransSplit.FromDate).ToShortDateString();
								sToDate = Conversion.ToDate(objFundsTransSplit.ToDate).ToShortDateString();     
								sInvoiceNumber = objFundsTransSplit.InvoiceNumber;
								sInvoiceDate = Conversion.ToDate(objFundsTransSplit.InvoiceDate).ToShortDateString();
								sInvoiceBy = objFundsTransSplit.InvoicedBy; 
								objFundsTransSplit.Dispose();
							}
							catch
							{
								sFromDate = string.Empty;
								sToDate = string.Empty;     
								sInvoiceNumber = "N/A";
								sInvoiceDate = string.Empty;
								sInvoiceBy = string.Empty;
							}

							if(p_bIsFormatFileFound)
							{
								m_structEOBDetailRecord.ProcedureDate.Token = sFromDate;
								m_structEOBDetailRecord.ProcToDate.Token = sToDate;
								m_structEOBDetailRecord.InvoiceNumber.Token = sInvoiceNumber;
								m_structEOBDetailRecord.InvoiceDate.Token = sInvoiceDate;
								m_structEOBDetailRecord.InvoicedBy.Token = sInvoiceBy;  
							}
							else
							{
								m_objPrintWrapper.CurrentX = p_iCurrentX;
								m_objPrintWrapper.CurrentY = m_objPrintWrapper.CurrentY + dAvgTextHeight;
								m_objPrintWrapper.PrintText(sInvoiceNumber.Trim());
								m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 3;
								m_objPrintWrapper.PrintText(sFromDate);  
							}

							//Print remaining data fields
							sTempData = objReader["BILLING_CODE_TEXT"].ToString();

							//*** Default body ***
							if(!p_bIsFormatFileFound)	
							{
                                //m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 4.5;    //zalam mits:-10813 03/31/2008
                                m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 4.2;
								if(sTempData.Length > 6)
									m_objPrintWrapper.PrintText(sTempData.Substring(0,6));   
								else
									m_objPrintWrapper.PrintText(sTempData);   
							}
							else
							{
								m_structEOBDetailRecord.ProcedureCode.Token = sTempData;
								m_structEOBDetailRecord.Procedure.Token = sTempData.Substring(sTempData.IndexOf('-') + 1);     
							}
                            dCurrentEOBX = p_iCurrentX + XINCREASEWIDTH * 5;
							if(!p_bIsFormatFileFound && iNumEOB > 0)
							{
                                //m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 6;    //zalam mits:-10813 03/31/2008
                                m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 5;
								m_objPrintWrapper.PrintText(m_structEOBDetailRecord.EOBList[1].ToString());
							}

							//amount billed
							dTempId = Conversion.ConvertStrToDouble(objReader["AMOUNT_BILLED"].ToString());
							if(p_bIsFormatFileFound)
								m_structEOBDetailRecord.AmountBilled.Token = dTempId;
							dTotalAmountBilled += dTempId;
							if(!p_bIsFormatFileFound)
							{
								sTempData = string.Format("{0:#,###,##0.00}",dTempId);
								sTempData = Functions.FillSpace(12 - sTempData.Length) + sTempData;
                                //m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 7.5;    //zalam mits:-10813 03/31/2008
                               // m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 7.75;
                                m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 7.3;
								m_objPrintWrapper.PrintText(sTempData);  
							}

							//amount to pay
							dTempId = Conversion.ConvertStrToDouble(objReader["AMOUNT_TO_PAY"].ToString());
							if(p_bIsFormatFileFound)
								m_structEOBDetailRecord.AmountPaid.Token = dTempId;
							dTotalAmountPaid += dTempId;
							if(!p_bIsFormatFileFound)
							{
								sTempData = string.Format("{0:#,###,##0.00}",dTempId);
								sTempData = Functions.FillSpace(12 - sTempData.Length) + sTempData;
                                //m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 9.5;    //zalam mits:-10813 03/31/2008
                              //  m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 9.75;
                                m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 9.3;
								m_objPrintWrapper.PrintText(sTempData);  
							}			

							//Print remaining EOB lines (see above)
							if(!p_bIsFormatFileFound)
							{
								if(iNumEOB > 1)
								{
									for(int i = 2; i <= iNumEOB; i++)
									{
                                        m_objPrintWrapper.CurrentY += dAvgTextHeight;
                                        m_objPrintWrapper.CurrentX = dCurrentEOBX;
                                        //Geeta 09/24/07 : Mits 10328
                                        m_objFunctions.CheckEOBPageLength(14500, m_objPrintWrapper);
										m_objPrintWrapper.PrintText(m_structEOBDetailRecord.EOBList[i].ToString());
									}
								}
							}
							else
							{
								//*** Custom Body - Detail record ***
								m_objFunctions.GetCustomEOBBody(m_structEOBDetailRecord, ref m_structEOBRecord,ref m_objPrintWrapper);  
							}
						}
						m_structEOBDetailRecord.EOBList.Clear();
						m_structEOBDetailRecord.DiagnosisList.Clear();
						//Mukul Added 3/2/2007 MITS 8960 Modifielist is not cleared
						m_structEOBDetailRecord.ModifierList.Clear();
					}//END of While
					objReader.Close(); 
				}//End of IF

				if(!p_bIsFormatFileFound)
					m_objPrintWrapper.CurrentY += 2 * dAvgTextHeight;  
				//Print Totals
				if(bIsPrintTotals)
				{
					if(p_bIsFormatFileFound)
					{
						m_structEOBRecord.TotalAmountBilled = dTotalAmountBilled;
						m_structEOBRecord.TotalAmountPaid = dTotalAmountPaid;
						m_structEOBRecord.TotalScheduledAmount = dTotSchdAmt;
						m_structEOBRecord.TotalAmountReduced = dTotAmtReduced;
						m_structEOBRecord.TotalAmountSaved = dTotAmtSaved;
						m_structEOBRecord.TotalBaseAmount = dTotalBaseAmount;
						m_structEOBRecord.TotalAmountAllowed = dTotalAmountAllowed;
						m_structEOBRecord.TotalDiscountAmount = dTotalDiscountAmount;
						m_structEOBRecord.TotalPerDiemAmt = dTotalPerDiemAmt;
						m_structEOBRecord.TotalStopLossAmt = dTotalStopLossAmt;
						m_structEOBRecord.TotalFeeTableAmt = dTotalFeeTableAmt;
					}
					else
					{
						sTempData = string.Format("{0:#,###,##0.00}",dTotalAmountBilled);
						sTempData = Functions.FillSpace(12 - sTempData.Length) + sTempData;
						m_objPrintWrapper.SetFontBold(true);
						m_objPrintWrapper.CurrentX = p_iCurrentX;
						m_objPrintWrapper.PrintText("TOTALS:"); 
                        //m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 7.5;    //zalam mits:-10813 03/31/2008
                        m_objPrintWrapper.CurrentX = p_iCurrentX + XINCREASEWIDTH * 7.3;
						m_objPrintWrapper.PrintText(sTempData);
						sTempData = string.Format("{0:#,###,##0.00}",dTotalAmountPaid);
						sTempData = Functions.FillSpace(12 - sTempData.Length) + sTempData;
                        //m_objPrintWrapper.CurrentX = p_iCurrentX + +XINCREASEWIDTH * 9.5;   //zalam mits:-10813 03/31/2008
                        m_objPrintWrapper.CurrentX = p_iCurrentX + +XINCREASEWIDTH * 9.3;
						m_objPrintWrapper.PrintText(sTempData);		
						m_objPrintWrapper.SetFontBold(false);
					}
				}

				if(!p_bIsFormatFileFound)
				{
					m_objPrintWrapper.CurrentX = 500;
					m_objPrintWrapper.CurrentY = m_objPrintWrapper.CurrentY + 2 * dAvgTextHeight;
                    //Geeta 09/24/07 : Mits 10328
                    m_objFunctions.CheckEOBPageLength(14500, m_objPrintWrapper);
                    m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBBody.ContactUs", m_iClientId)); //sharishkumar Jira 835
					m_objPrintWrapper.CurrentY = m_objPrintWrapper.CurrentY + 2 * dAvgTextHeight;
					m_objPrintWrapper.PrintText("Sincerely,");
					m_objPrintWrapper.CurrentY = m_objPrintWrapper.CurrentY + 2 * dAvgTextHeight;
					m_objPrintWrapper.PrintText(m_objCommon.m_stLoginInfo.Username);
					m_objPrintWrapper.CurrentY = m_objPrintWrapper.CurrentY + 2 * dAvgTextHeight;
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.ErrorPrint", m_iClientId), p_objEx);	//sharishkumar Jira 835			
			}
			finally
			{
				if(objSQL != null)
					objSQL = null;

				if(objReader != null)
					objReader.Dispose();

				if(objTempReader != null)
					objTempReader.Dispose();

				if(objFundsTransSplit != null)
					objFundsTransSplit.Dispose();
			}
		}
		#endregion

        #region "PrintEOBHTMLTABLE(int p_iInvoiceId, string p_sClaimNumberText, string p_sClaimantName, string p_sName, string p_sOtherName, bool p_bIsFormatFileFound, int p_iCurrentX, int p_iCurrentY)"
        /// Name		: PrintEOBHTMLTABLE
        /// Author		: Debabrata Biswas MITS Issue 17183
        /// Date Created	: 24 Juk 2009		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Print EOB; Introduction And Body.Format; File Is the; Custom; Files; of
        /// http-like tags <b>.  If a file cannot be found, a Default format is used.
        /// </summary>
        /// <param name="p_iInvoiceId">Invoice Id</param>
        /// <param name="p_sClaimNumberText">Claim Number Text</param>
        /// <param name="p_sClaimantName">Claimaint Name</param>
        /// <param name="p_sName">Name</param>
        /// <param name="p_sOtherName">Other Name</param>
        /// <param name="p_bIsFormatFileFound">Flag for Format file</param>
        /// <param name="p_iCurrentX">X-axis value</param>
        /// <param name="p_iCurrentY">Y-axis value</param>
        private string PrintEOBHTMLTABLE(int p_iInvoiceId, string sTableColumns)
        {
            bool bIsPrintTotals = false;
            bool bIsUseRecord = false;
            StringBuilder objSQL = null;
            DbReader objReader = null;
            DbReader objTempReader = null;
            FundsTransSplit objFundsTransSplit = null;
            int iTempId = 0;
            double dTempId = 0.0;
            int iNumEOB = 0;
            int iLineItem = 0;
            string sSQL = string.Empty;
            string sTempData = string.Empty;
            string sTempCode = string.Empty;
            double dTotAmtReduced = 0.0;
            double dTotSchdAmt = 0.0;
            double dTotAmtSaved = 0.0;
            double dTotalAmountAllowed = 0.0;
            double dTotalBaseAmount = 0.0;
            double dTotalDiscountAmount = 0.0;
            double dTotalPerDiemAmt = 0.0;
            double dTotalStopLossAmt = 0.0;
            double dTotalFeeTableAmt = 0.0;
            double dTotalAmountBilled = 0.0;
            double dTotalAmountPaid = 0.0;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sInvoiceNumber = string.Empty;
            string sInvoiceDate = string.Empty;
            string sInvoiceBy = string.Empty;
            string sHTMLcellData = string.Empty;
            string sHTMLrowData = string.Empty;
            string sTDData = string.Empty;
            string[] TDList;
            string[] sTempFormat;
            string sAttributes = string.Empty;
            int iDataLength = 0;
            string sTD = string.Empty;
            bool sDescriptionFileExists = false;

            try
            {
                objSQL = new StringBuilder();
                objSQL.Append("SELECT INVOICE_DETAIL_ID,BILLING_CODE_TEXT,AMOUNT_BILLED,AMOUNT_TO_PAY,FUNDS_SPLIT_ROW_ID");
                objSQL.Append(",TABLE_CODE, PERCENTILE,PLACE_OF_SER_CODE,TYPE_OF_SER_CODE,UNITS_BILLED_NUM");
                objSQL.Append(",SCHEDULED_AMOUNT,AMOUNT_REDUCED,AMOUNT_SAVED, QUANTITY,STORE, MODIFIER_CODE");
                objSQL.Append(",CONTRACT_AMOUNT,DISCOUNT,ZIP_CODE,BASE_AMOUNT,AMOUNT_ALLOWED");
                objSQL.Append(",PER_DIEM_AMT,STOP_LOSS_AMT,FEE_TABLE_AMT, PRESCRIP_NO, DRUG_NAME, PRESCRIP_DATE");
                //BRS FL Merge
                objSQL.Append(",FL_LICENSE, REV_CODE");
                //End
                objSQL.Append(" FROM INVOICE_DETAIL, FUNDS_TRANS_SPLIT WHERE INVOICE_ID = " + p_iInvoiceId);
                objSQL.Append(" AND FUNDS_SPLIT_ROW_ID = SPLIT_ROW_ID  ORDER BY FROM_DATE");

                objReader = DbFactory.GetDbReader(m_sConnectionString, objSQL.ToString());

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        m_structEOBDetailRecord.EOBList = new Hashtable();
                        m_structEOBDetailRecord.EOBListDetails = new Hashtable();
                        m_structEOBDetailRecord.DiagnosisList = new Hashtable();
                        m_structEOBDetailRecord.DiagnosisCodeList = new Hashtable();
                        m_structEOBDetailRecord.DiagnosisDetails = new Hashtable();
                        m_structEOBDetailRecord.ModifierList = new Hashtable();
                        
                        TDList = sTableColumns.Split('|');

                        bIsUseRecord = false;

                        //At first check if record is deleted or not
                        iTempId = Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString());

                        foreach (FundsTransSplit objFundsTSplit in m_objFunds.TransSplitList)
                        {
                            if (objFundsTSplit.SplitRowId == iTempId)
                            {
                                bIsUseRecord = true;
                                break;
                            }
                        }

                        iNumEOB = 0;

                        if (bIsUseRecord)
                        {
                            bIsPrintTotals = true;
                            iTempId = Conversion.ConvertStrToInteger(objReader["INVOICE_DETAIL_ID"].ToString());
                            //Added for MITS 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
                            bool bUseCodeDescriptionFile = Convert.ToBoolean(Functions.GetValueFromConfigFile(Functions.TAG_USEEOBDESCRIPTIONFILE, m_sConnectionString, m_iClientId));//rkaur27
                            if (bUseCodeDescriptionFile)//Added if block for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
                            {
                                string sDesciptionFileName = Functions.GetFormatFilePath(m_iClientId)
                                                              + Functions.GetValueFromConfigFile(Functions.TAG_EOBDESCRIPTIONFILE, m_sConnectionString, m_iClientId);//rkaur27

                                if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() != "true" && m_iClientId == 0)
                                {
                                    MemoryStream objMemDesc = CommonFunctions.RetreiveTempFileFromDB(sDesciptionFileName, "", m_sConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                                    if (objMemDesc != null) { sDescriptionFileExists = true; } else { sDescriptionFileExists = false; }
                                }
                                else
                                {
                                    sDescriptionFileExists = File.Exists(sDesciptionFileName);
                                }

                               
                                if (sDescriptionFileExists)
                                {
                                    string sFileText = null;
                                    string sNewText = null;
                                    string sOldText = null;
                                    int iPoint = 0;

                                    StreamReader objSRdr = null;
                                    if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true" && m_iClientId != 0)
                                    {
                                        MemoryStream objMemoryStream = CommonFunctions.RetreiveTempFileFromDB(sDesciptionFileName, "", m_sConnectionString, m_iClientId, CommonFunctions.StorageType.Permanent);
                                        objSRdr = new StreamReader(objMemoryStream);
                                    }
                                    else
                                    {
                                        objSRdr = new StreamReader(sDesciptionFileName);
                                    }
                                    sFileText = objSRdr.ReadToEnd();

                                    sSQL = "SELECT CODES.SHORT_CODE FROM INVDETAIL_X_EOB, CODES";
                                    sSQL = sSQL + " WHERE INVDETAIL_X_EOB.EOB_CODE = CODES.CODE_ID ";
                                    sSQL = sSQL + " AND INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + iTempId;
                                    objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                    if (objTempReader != null)
                                    {
                                        while (objTempReader.Read())
                                        {
                                            sTempData = "@" + objTempReader.GetString("SHORT_CODE").Trim();
                                            iPoint = sFileText.IndexOf(sTempData, 0);
                                            if (iPoint >= 0)
                                            {
                                                sOldText = sFileText.Substring(iPoint + 1);
                                                iPoint = sOldText.IndexOf("@");
                                                sNewText = sOldText.Substring(0, iPoint - 1);

                                                if (iNumEOB > 0)
                                                    sNewText = "- " + sNewText;
                                                else
                                                    sNewText = "  " + sNewText;

                                                iNumEOB++;
                                                m_structEOBDetailRecord.EOBList.Add(iNumEOB, sNewText.Trim());
                                                m_structEOBDetailRecord.EOBListDetails.Add(iNumEOB, objTempReader.GetString("SHORT_CODE").Trim() + " " + sNewText.Trim());
                                            }
                                        }
                                        objTempReader.Close();
                                    }
                                    if (objSRdr != null)
                                    {
                                        objSRdr.Close();
                                        objSRdr.Dispose();
                                    }
                                }
                                else
                                {
                                    throw new RMAppException(Globalization.GetString("Functions.EobDescriptionFile.Error", m_iClientId));//sharishkumar Jira 835
                                }
                            }//Added if block for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
                            else
                            {
                                sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM INVDETAIL_X_EOB,CODES_TEXT WHERE ";
                                sSQL = sSQL + "INVDETAIL_X_EOB.EOB_CODE = CODES_TEXT.CODE_ID AND ";
                                sSQL = sSQL + "INVDETAIL_X_EOB.INVOICE_DETAIL_ID = " + iTempId;

                                objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                if (objTempReader != null)
                                {
                                    while (objTempReader.Read())
                                    {
                                        string sTempEob = "";
                                        sTempData = objTempReader["CODE_DESC"].ToString().Trim();
                                        sTempData = "- " + sTempData;

                                        iNumEOB++;
                                        m_structEOBDetailRecord.EOBList.Add(iNumEOB, sTempData.Trim());
                                        m_structEOBDetailRecord.EOBListDetails.Add(iNumEOB, objTempReader["SHORT_CODE"].ToString().Trim() + " " + sTempData.Trim());
                                    }
                                    objTempReader.Close();
                                }
                            }

                            //extract diagnosis list
                            sSQL = "SELECT CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM INVDETAIL_X_DIAG, CODES_TEXT";
                            sSQL = sSQL + " WHERE INVDETAIL_X_DIAG.DIAGNOSIS_CODE=CODES_TEXT.CODE_ID";
                            sSQL = sSQL + " AND INVDETAIL_X_DIAG.INVOICE_DETAIL_ID= " + iTempId;

                            objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                            if (objTempReader != null)
                            {
                                iLineItem = 0;
                                while (objTempReader.Read())
                                {
                                    sTempData = objTempReader["CODE_DESC"].ToString();
                                    sTempCode = objTempReader["SHORT_CODE"].ToString();
                                    iLineItem++;
                                    m_structEOBDetailRecord.DiagnosisList.Add(iLineItem, sTempData.Trim());
                                    m_structEOBDetailRecord.DiagnosisCodeList.Add(iLineItem, sTempCode.Trim());
                                    m_structEOBDetailRecord.DiagnosisDetails.Add(iLineItem, sTempCode.Trim() + "  " + sTempData.Trim());
                                }
                                objTempReader.Close();
                            }

                            //extract modifier code
                            sSQL = "SELECT CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM INVDETAIL_X_MOD, CODES, CODES_TEXT";
                            sSQL = sSQL + " WHERE INVDETAIL_X_MOD.MODIFIER_CODE=CODES_TEXT.CODE_ID";
                            sSQL = sSQL + " AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND INVDETAIL_X_MOD.INVOICE_DETAIL_ID=" + iTempId;

                            objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                            if (objTempReader != null)
                            {
                                iLineItem = 0;
                                while (objTempReader.Read())
                                {
                                    sTempData = objTempReader["SHORT_CODE"].ToString() + " " + objTempReader["CODE_DESC"].ToString();
                                    iLineItem++;
                                    m_structEOBDetailRecord.ModifierList.Add(iLineItem, sTempData.Trim());
                                }
                                objTempReader.Close();
                            }

                            //extract fee schedule name
                            iTempId = Conversion.ConvertStrToInteger(objReader["TABLE_CODE"].ToString());
                            sSQL = "SELECT FEE_TABLES.USER_TABLE_NAME FROM FEE_TABLES WHERE FEE_TABLES.TABLE_ID = " + iTempId;
                            objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                            if (objTempReader != null)
                            {
                                if (objTempReader.Read())
                                {
                                    sTempData = objTempReader["USER_TABLE_NAME"].ToString();
                                    m_structEOBDetailRecord.FeeSchedule.Token = sTempData;
                                }
                                objTempReader.Close();
                            }

                            m_structEOBDetailRecord.Percentile.Token = objReader["PERCENTILE"].ToString();

                            iTempId = Conversion.ConvertStrToInteger(objReader["PLACE_OF_SER_CODE"].ToString());
                            sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " +
                                "CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
                            objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                            if (objTempReader != null)
                            {
                                if (objTempReader.Read())
                                {
                                    m_structEOBDetailRecord.PlaceofServiceCode.Token = objTempReader["SHORT_CODE"].ToString();
                                    m_structEOBDetailRecord.PlaceOfService.Token = objTempReader["CODE_DESC"].ToString();
                                }
                                objTempReader.Close();
                            }

                            iTempId = Conversion.ConvertStrToInteger(objReader["TYPE_OF_SER_CODE"].ToString());
                            sSQL = "SELECT CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM " +
                                "CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + iTempId;
                            objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                            if (objTempReader != null)
                            {
                                if (objTempReader.Read())
                                {
                                    m_structEOBDetailRecord.TypeOfServiceCode.Token = objTempReader["SHORT_CODE"].ToString();
                                    m_structEOBDetailRecord.TypeOfService.Token = objTempReader["CODE_DESC"].ToString();
                                }
                                objTempReader.Close();
                            }

                            m_structEOBDetailRecord.UnitsBilled.Token = Conversion.ConvertStrToLong(objReader["UNITS_BILLED_NUM"].ToString());

                            m_structEOBDetailRecord.ScheduledAmount.Token = Conversion.ConvertStrToDouble(objReader["SCHEDULED_AMOUNT"].ToString());
                            dTotSchdAmt += Functions.FormatAmount(m_structEOBDetailRecord.ScheduledAmount.Token, m_iClientId);

                            m_structEOBDetailRecord.AmountReduced.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_REDUCED"].ToString());
                            dTotAmtReduced += Functions.FormatAmount(m_structEOBDetailRecord.AmountReduced.Token, m_iClientId);

                            m_structEOBDetailRecord.AmountSaved.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_SAVED"].ToString());
                            dTotAmtSaved += Functions.FormatAmount(m_structEOBDetailRecord.AmountSaved.Token, m_iClientId);

                            m_structEOBDetailRecord.ContractAmount.Token = Conversion.ConvertStrToDouble(objReader["CONTRACT_AMOUNT"].ToString());
                            m_structEOBDetailRecord.Discount.Token = Conversion.ConvertStrToDouble(objReader["DISCOUNT"].ToString());
                            m_structEOBDetailRecord.ProviderZipCode.Token = objReader["ZIP_CODE"].ToString();

                            m_structEOBDetailRecord.AmountAllowed.Token = Conversion.ConvertStrToDouble(objReader["AMOUNT_ALLOWED"].ToString());
                            dTotalAmountAllowed += Functions.FormatAmount(m_structEOBDetailRecord.AmountAllowed.Token, m_iClientId);

                            m_structEOBDetailRecord.BaseAmount.Token = Conversion.ConvertStrToDouble(objReader["BASE_AMOUNT"].ToString());
                            dTotalBaseAmount += Functions.FormatAmount(m_structEOBDetailRecord.BaseAmount.Token, m_iClientId);

                            m_structEOBDetailRecord.DiscountAmount.Token = m_structEOBDetailRecord.BaseAmount.Token * (m_structEOBDetailRecord.Discount.Token / 100);
                            dTotalDiscountAmount += Functions.FormatAmount(m_structEOBDetailRecord.DiscountAmount.Token, m_iClientId);

                            m_structEOBDetailRecord.PerDiemAmount.Token = Conversion.ConvertStrToDouble(objReader["PER_DIEM_AMT"].ToString());
                            dTotalPerDiemAmt += Functions.FormatAmount(m_structEOBDetailRecord.PerDiemAmount.Token, m_iClientId);

                            m_structEOBDetailRecord.StopLossAmount.Token = Conversion.ConvertStrToDouble(objReader["STOP_LOSS_AMT"].ToString());
                            dTotalStopLossAmt += Functions.FormatAmount(m_structEOBDetailRecord.StopLossAmount.Token, m_iClientId);

                            m_structEOBDetailRecord.FeeTableAmount.Token = Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString());
                            dTotalFeeTableAmt += Functions.FormatAmount(m_structEOBDetailRecord.FeeTableAmount.Token, m_iClientId);

                            m_structEOBDetailRecord.PrescripNo.Token = objReader["PRESCRIP_NO"].ToString();
                            m_structEOBDetailRecord.DrugName.Token = objReader["DRUG_NAME"].ToString();
                            m_structEOBDetailRecord.PrescripDate.Token = Conversion.ToDate(objReader["PRESCRIP_DATE"].ToString()).ToShortDateString();
                            m_structEOBDetailRecord.PhysicianLicenseNum.Token = objReader["FL_LICENSE"].ToString();
                            m_structEOBDetailRecord.RevenueCode.Token = objReader["REV_CODE"].ToString();

                            m_structEOBDetailRecord.DiscOnFeeSchd.Token = 0;
                            if (Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString()) > 0 && Conversion.ConvertStrToDouble(objReader["DISCOUNT"].ToString()) > 0)
                                m_structEOBDetailRecord.DiscOnFeeSchd.Token = Conversion.ConvertStrToDouble(objReader["FEE_TABLE_AMT"].ToString()) - Conversion.ConvertStrToDouble(objReader["AMOUNT_ALLOWED"].ToString());

                            objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit", false);
                            try
                            {
                                objFundsTransSplit.MoveTo(Conversion.ConvertStrToInteger(objReader["FUNDS_SPLIT_ROW_ID"].ToString()));
                                m_structEOBDetailRecord.ProcedureDate.Token = Conversion.ToDate(objFundsTransSplit.FromDate).ToShortDateString();
                                m_structEOBDetailRecord.ProcToDate.Token = Conversion.ToDate(objFundsTransSplit.ToDate).ToShortDateString();
                                m_structEOBDetailRecord.InvoiceNumber.Token = objFundsTransSplit.InvoiceNumber;
                                m_structEOBDetailRecord.InvoiceDate.Token = Conversion.ToDate(objFundsTransSplit.InvoiceDate).ToShortDateString();
                                m_structEOBDetailRecord.InvoicedBy.Token = objFundsTransSplit.InvoicedBy;
                            }
                            catch
                            {
                                m_structEOBDetailRecord.ProcedureDate.Token = string.Empty;
                                m_structEOBDetailRecord.ProcToDate.Token = string.Empty;
                                m_structEOBDetailRecord.InvoiceNumber.Token = "N/A";
                                m_structEOBDetailRecord.InvoiceDate.Token = string.Empty;
                                m_structEOBDetailRecord.InvoicedBy.Token = string.Empty;
                            }

                            //Print remaining data fields
                            sTempData = objReader["BILLING_CODE_TEXT"].ToString();

                            m_structEOBDetailRecord.ProcedureCode.Token = sTempData;
                            m_structEOBDetailRecord.Procedure.Token = sTempData.Substring(sTempData.IndexOf('-') + 1);

                            dTempId = Conversion.ConvertStrToDouble(objReader["AMOUNT_BILLED"].ToString());
                            m_structEOBDetailRecord.AmountBilled.Token = dTempId;
                            
                            dTotalAmountBilled += dTempId;
                            
                            //amount to pay
                            dTempId = Conversion.ConvertStrToDouble(objReader["AMOUNT_TO_PAY"].ToString());
                            m_structEOBDetailRecord.AmountPaid.Token = dTempId;
                            dTotalAmountPaid += dTempId;

                            foreach (string TD in TDList)
                            {
                                sAttributes = "";
                                sTD = TD.Substring(1, TD.Length - 2);
                                sTempFormat = sTD.Split(',');

                                sTD = "{" + sTempFormat[0] + "}";
                                iDataLength = int.Parse(sTempFormat[1]);
                                sAttributes = " " + sTempFormat[2];

                                sTDData = m_objFunctions.ProcessHTMLtableToken(sTD, iDataLength, ref m_structEOBRecord, ref m_structEOBDetailRecord, objFundsTransSplit);
                                sHTMLcellData = sHTMLcellData + "<TD" + sAttributes  + ">" + sTDData + "</TD>";
                            }
                            if(sHTMLrowData==string.Empty)
                                sHTMLrowData = sHTMLcellData;
                            else
                                sHTMLrowData = sHTMLrowData + "|" + sHTMLcellData;

                            sHTMLcellData = string.Empty;
                        }
                    }//END of While
                    objReader.Close();
                }//End of IF

                //Print Totals
                if (bIsPrintTotals)
                {
                        m_structEOBRecord.TotalAmountBilled = dTotalAmountBilled;
                        m_structEOBRecord.TotalAmountPaid = dTotalAmountPaid;
                        m_structEOBRecord.TotalScheduledAmount = dTotSchdAmt;
                        m_structEOBRecord.TotalAmountReduced = dTotAmtReduced;
                        m_structEOBRecord.TotalAmountSaved = dTotAmtSaved;
                        m_structEOBRecord.TotalBaseAmount = dTotalBaseAmount;
                        m_structEOBRecord.TotalAmountAllowed = dTotalAmountAllowed;
                        m_structEOBRecord.TotalDiscountAmount = dTotalDiscountAmount;
                        m_structEOBRecord.TotalPerDiemAmt = dTotalPerDiemAmt;
                        m_structEOBRecord.TotalStopLossAmt = dTotalStopLossAmt;
                        m_structEOBRecord.TotalFeeTableAmt = dTotalFeeTableAmt;
                }
                return sHTMLrowData;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.PrintEOBHTMLTABLE", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objSQL != null)
                    objSQL = null;

                if (objReader != null)
                    objReader.Dispose();

                if (objTempReader != null)
                    objTempReader.Dispose();

                if (objFundsTransSplit != null)
                    objFundsTransSplit.Dispose();

                m_structEOBDetailRecord.EOBList.Clear();
                m_structEOBDetailRecord.EOBListDetails.Clear();
                m_structEOBDetailRecord.DiagnosisList.Clear();
                m_structEOBDetailRecord.ModifierList.Clear();
                m_structEOBDetailRecord.DiagnosisCodeList.Clear();
            }
        }
        #endregion

		#region "PrintEOBHeader(PrintWrapper p_objPrintWrapper)"
		/// Name		: PrintEOBHeader
		/// Author		: Anurag Agarwal
		/// Date Created	: 18 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Default format header
		/// </summary>
		/// <param name="p_objPrintWrapper">object of class use to generate the PDF file</param>
		private void PrintEOBHeader(PrintWrapper p_objPrintWrapper)
		{
			try
			{
				m_objPrintWrapper.SetFont(12);
				m_objPrintWrapper.SetFontBold(true);
				m_objPrintWrapper.CurrentY = 1250;
				m_objPrintWrapper.CurrentX = 3850;
                m_objPrintWrapper.PrintText(Globalization.GetString("PrintEOBAR.EOBHeader.EOB", m_iClientId)); //sharishkumar Jira 835

				m_objPrintWrapper.SetFont(10);
				m_objPrintWrapper.SetFontBold(false);

				m_objPrintWrapper.CurrentX = 500;
				m_objPrintWrapper.CurrentY = 2000;
				m_objPrintWrapper.PrintText(DateTime.Now.ToLongDateString());
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintEOBAR.PrintEOBHeader.ErrorPrint", m_iClientId), p_objEx);			//sharishkumar Jira 835	
			}
		}
		#endregion

        #region "GetDefaultPayeeHTMLTemplate(string sName, string sAddr1, string sAddr2, string sAddr3,string sAddr4 string strStateCode, string strStateDesc,string sCity, string sRegClaimantName, string sClaimantAddr1, string sClaimantAddr2, string sClaimantAddr3,string sClaimantAddr4, string sClaimantCity, string sClaimNumberText,string sClaimantName)"
        /// <summary>
        /// The function returns the default payee HTML template
        /// Author: Debabrata Biswas MITS Issue 17183
        /// Date:   30 july 2009
        /// </summary>
        /// <returns>sDefaultPayeeHTMLtext</returns>
        private StringBuilder GetDefaultPayeeHTMLTemplate(string sName, string sAddr1, string sAddr2, string sAddr3, string sAddr4, string strStateCode, string strStateDesc,
            string sCity, string sRegClaimantName, string sClaimantAddr1, string sClaimantAddr2, string sClaimantAddr3, string sClaimantAddr4, string sClaimantCity, string sClaimNumberText,
            string sClaimantName)
        {
            try
            {
                StringBuilder sDefaultPayeeHTMLtext = new StringBuilder(null);

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<HTML>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<HEAD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</HEAD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BODY>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<CENTER>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<H1>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBHeader.EOB", m_iClientId));//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</H1>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</CENTER>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(DateTime.Now.ToLongDateString());
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TABLE>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sName.Trim() + "</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sAddr1 + "</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
                if (sAddr2 != string.Empty)
                {
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sAddr2 + "</TD>");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
                }
                if (sAddr3 != string.Empty)
                {
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sAddr3 + "</TD>");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
                }
                if (sAddr4 != string.Empty)
                {
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sAddr4 + "</TD>");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
                }
                //sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sAddr3 + "</TD>"); // JIRA 6420 syadav55
                //sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sAddr4 + "</TD>"); // JIRA 6420 syadav55

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sCity + ", " + ((string)(strStateCode + " " + strStateDesc)).Trim() + " " + m_structEOBRecord.PayeePostalCode + "</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>" + sRegClaimantName + " " + sClaimantAddr1 + " " + " " + sClaimantAddr2 + " " + " " + sClaimantAddr3 + " " + " " + sClaimantAddr4 + " " + sClaimantCity + "</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TABLE>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBReport.RE", m_iClientId) + " " + Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText", m_iClientId) + " " + sClaimNumberText);//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName", m_iClientId) + " " + sClaimantName);//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBReport.Name", m_iClientId) + " " + sRegClaimantName);//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");

                if (sName == string.Empty)
                {
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername1", m_iClientId));//sharishkumar Jira 835
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername2", m_iClientId));//sharishkumar Jira 835
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                }
                else
                {
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername3", m_iClientId) + " " + sName + " " + Globalization.GetString("PrintEOBAR.EOBBody.Othername4", m_iClientId));//sharishkumar Jira 835
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername5", m_iClientId));//sharishkumar Jira 835
                    sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                }
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.ItemsText", m_iClientId));//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");

                //Dynamic Multirow table
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TABLE CELLPADDING=\"1\" BORDER=\"1\" BORDERCOLOR=\"WHITE\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>INVOICE NUMBER</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>PROCEDURE DATE</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>PROC. CODE</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>EXPLANATION</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD ALIGN=\"RIGHT\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>AMOUNT BILLED</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD ALIGN=\"RIGHT\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>AMOUNT PAID</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR TYPE=\"MULTIROW\">");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD VALIGN=\"TOP\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{INVOICE_NUMBER}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD VALIGN=\"TOP\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{PROCEDURE_DATE}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD VALIGN=\"TOP\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{PROCEDURE_CODE}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD VALIGN=\"TOP\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{EXPLANATION}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD ALIGN=\"RIGHT\" VALIGN=\"TOP\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{AMOUNT_BILLED}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD ALIGN=\"RIGHT\" VALIGN=\"TOP\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{AMOUNT_PAID}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TR>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<B>TOTAL</B>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD STYLE=\"FONT-WEIGHT: BOLD; TEXT-ALIGN: RIGHT;\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{TOTAL_AMOUNT_BILLED}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<TD STYLE=\"FONT-WEIGHT: BOLD; TEXT-ALIGN: RIGHT;\">");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("{TOTAL_AMOUNT_PAID}");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TD>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TR>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</TABLE>");

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.ContactUs", m_iClientId));//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("Sincerely,");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(m_objCommon.m_stLoginInfo.Username);

                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.ClaimantCopy", m_iClientId));//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("<BR />");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Cc", m_iClientId) + " " + sClaimantName);//sharishkumar Jira 835
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</BODY>");
                sDefaultPayeeHTMLtext = sDefaultPayeeHTMLtext.Append("</HTML>");

                return sDefaultPayeeHTMLtext;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.GetDefaultPayeeHTMLTemplate", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
        }
        #endregion

        #region "GetDefaultClaimantHTMLTemplate(string sClaimNumberText, string sClaimantName, string sName)"
        /// <summary>
        /// Author: Debabrata Biswas MITS Issue 17183
        /// Date:   30 july 2009
        /// This function returns the default Claimant HTML template
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetDefaultClaimantHTMLTemplate(string sClaimNumberText, string sClaimantName, string sName)
        {
            try
            {
                StringBuilder sDefaultClaimantHTMLtext = new StringBuilder(null);

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<HTML>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<HEAD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</HEAD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BODY>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBReport.RE", m_iClientId) + " " + Globalization.GetString("PrintEOBAR.EOBReport.ClaimNumberText", m_iClientId) + " " + sClaimNumberText);//sharishkumar Jira 835
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBReport.ClaimantName", m_iClientId) + " " + sClaimantName);//sharishkumar Jira 835
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBReport.Name", m_iClientId) + " " + sName);//sharishkumar Jira 835
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");

                if (sName == string.Empty)
                {
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername1", m_iClientId));//sharishkumar Jira 835
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername2",m_iClientId));//sharishkumar Jira 835
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                }
                else
                {
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername3",m_iClientId) + " " + sName + " " + Globalization.GetString("PrintEOBAR.EOBBody.Othername4", m_iClientId));//sharishkumar Jira 835
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.Othername5",m_iClientId));//sharishkumar Jira 835
                    sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                }
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.ItemsText", m_iClientId));//sharishkumar Jira 835
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");

                //Dynamic Multirow table
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TABLE CELLPADDING=\"1\" BORDER=\"1\" BORDERCOLOR=\"WHITE\">");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TR>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>INVOICE NUMBER</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>PROCEDURE DATE</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>PROC. CODE</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>EXPLANATION</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>AMOUNT BILLED</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>AMOUNT PAID</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TR>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TR TYPE=\"MULTIROW\">");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{INVOICE_NUMBER}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{PROCEDURE_DATE}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{PROCEDURE_CODE}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{EXPLANATION}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD ALIGN=\"RIGHT\">");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{AMOUNT_BILLED}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD ALIGN=\"RIGHT\">");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{AMOUNT_PAID}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TR>");


                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TR>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<B>TOTAL</B>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD STYLE=\"FONT-WEIGHT: BOLD; TEXT-ALIGN: RIGHT;\">");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{TOTAL_AMOUNT_BILLED}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<TD STYLE=\"FONT-WEIGHT: BOLD; TEXT-ALIGN: RIGHT;\">");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("{TOTAL_AMOUNT_PAID}");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TD>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TR>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</TABLE>");

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(Globalization.GetString("PrintEOBAR.EOBBody.ContactUs", m_iClientId));//sharishkumar Jira 835
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("Sincerely,");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("<BR />");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append(m_objCommon.m_stLoginInfo.Username);

                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</BODY>");
                sDefaultClaimantHTMLtext = sDefaultClaimantHTMLtext.Append("</HTML>");

                return sDefaultClaimantHTMLtext;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.GetDefaultClaimantHTMLTemplate", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
        }
        #endregion

        #region "ProcessMultiRowEOBHTMLtables(string sHTML)"
        /// <summary>
        /// Author: Debabrata Biswas MITS Issue 17183
        /// This section handles multirow html table.
        /// </summary>
        /// <param name="sHTML"></param>
        /// <param name="p_iInvoiceId"></param>
        /// <returns></returns>
        private XmlDocument ProcessMultiRowEOBHTMLtables(StringBuilder sHTML, int p_iInvoiceId)
        {
            XmlDocument xHTMLdocDOM;
            XmlNodeList xMultiRowTR;
            XmlNodeList xTD;
            XmlNode xTempNode;
            
            string sTableColumns = string.Empty;
            string sTableRows = string.Empty;
            string sAttribAndValue = string.Empty;
            string sTempCol = string.Empty;
            string[] sTempArray;
            string[] sTableRowArray; 
            string sLength="0";

            try
            {
                xHTMLdocDOM = new XmlDocument();
                xHTMLdocDOM.LoadXml(sHTML.ToString());
                xMultiRowTR = xHTMLdocDOM.SelectNodes("//TABLE/TR[@TYPE='MULTIROW']");

                foreach (XmlNode xTR in xMultiRowTR)
                {
                    xTD = xTR.SelectNodes("./TD");

                    foreach (XmlNode xEOBField in xTD)
                    {
                        sAttribAndValue = "";
                        foreach (XmlAttribute xAttrb in xEOBField.Attributes)
                        {
                            if (sAttribAndValue == "")
                                sAttribAndValue = xAttrb.Name + "=" + "\"" + xAttrb.Value + "\"";
                            else
                                sAttribAndValue = sAttribAndValue + " " + xAttrb.Name + "=" + "\"" + xAttrb.Value + "\"";
                        }

                        sTempCol = xEOBField.InnerText;
                        sTempCol = sTempCol.Substring(1, sTempCol.Length - 2);
                        sLength = "0";

                        if (sTempCol.IndexOf(',') != -1)
                        {
                            sTempArray = sTempCol.Split(',');
                            sTempCol = sTempArray[0];
                            sLength = sTempArray[1];
                        }

                        sTempCol = "{" + sTempCol + "," + sLength + "," + sAttribAndValue + "}";

                        if (sTableColumns == string.Empty)
                            sTableColumns = sTempCol;
                        else
                            sTableColumns = sTableColumns + "|" + sTempCol;
                    }
                    sTableRows = this.PrintEOBHTMLTABLE(p_iInvoiceId, sTableColumns);
                    sTableRowArray = sTableRows.Split('|');
                    Array.Reverse(sTableRowArray);
                        
                    xTR.InnerXml = "";

                    foreach (string sTemp in sTableRowArray)
                    {
                        xTempNode = xHTMLdocDOM.CreateElement("TR");
                        xTempNode.InnerXml = sTemp.Replace("&","&amp;");
                        xTR.ParentNode.InsertAfter(xTempNode, xTR);
                    }

                    sTableColumns = string.Empty;
                }
                return xHTMLdocDOM;
            }
            //Added for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            //Added for Mits 19415:Format of Explaination of Benefit form is different in RMX from RMWorld
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.ProcessMultiRowEOBHTMLtables", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                xHTMLdocDOM=null;
                xMultiRowTR=null;
                xTD=null;
                sTempArray = null;
            }
        }
        #endregion

        #region "CreateHTMLdocument(xHTMLdom,m_objFunds)"
        /// <summary>
        /// Author: Debabrata Biswas MITS Issue 17183
        /// This section outputs the complete HTML structure.
        /// </summary>
        /// <param name="xHTMLdom"></param>
        /// <param name="m_structEOBRecord"></param>
        /// <param name="m_structEOBDetailRecord"></param>
        /// <param name="m_objFunds"></param>
        /// <returns></returns>
        private XmlDocument CreateHTMLdocument(XmlDocument xHTMLdom,EOBRecord m_structEOBRecord, EOBDetailRecord m_structEOBDetailRecord,Funds m_objFunds)
        {
           XmlDocument xTempHTMLdom;
           MemoryStream p_MemoryStream;
           StreamReader p_objSReader;
           string sCurrentToken;
           string sProcessedToken;
           string sHTMLdom=string.Empty;
           int iNextInput;

           try
           {
               p_MemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xHTMLdom.InnerXml));
               p_objSReader = new StreamReader(p_MemoryStream);

               while ((iNextInput = p_objSReader.Peek()) != -1)
               {
                   sCurrentToken = m_objFunctions.GetCurrentHTMLToken(p_objSReader, '<', '>');

                   if (sCurrentToken.StartsWith("{") && sCurrentToken.EndsWith("}"))
                       sProcessedToken = m_objFunctions.ProcessHTMLToken(sCurrentToken, 0, m_structEOBRecord, m_structEOBDetailRecord, m_objFunds);
                   else
                       sProcessedToken = sCurrentToken;

                   sHTMLdom += sProcessedToken;
               }

               xTempHTMLdom = new XmlDocument();
               xTempHTMLdom.LoadXml(sHTMLdom.ToString().Replace("&amp;","&").Replace("&","&amp;"));
               
               return xTempHTMLdom;
           }
           catch (Exception p_objEx)
           {
               throw new RMAppException(Globalization.GetString("PrintEOBBRS.PrintEOBBody.CreateHTMLdocument", m_iClientId), p_objEx);//sharishkumar Jira 835
           }
           finally
           {
               xTempHTMLdom = null;
               p_MemoryStream=null;
               p_objSReader=null;
           }
        }
        #endregion

        /// <summary>
        /// Gets the filing state identifier.
        /// </summary>
        /// <param name="p_sClaimId">The P_S claim identifier.</param>
        /// <returns></returns>
        private string GetFilingStateId(string p_sClaimId)
        {
            if (object.ReferenceEquals(this.FilingState, null))
            {
                this.FilingState = new Dictionary<string, string>();
            }

            if (this.FilingState.ContainsKey(p_sClaimId))
            {
                return this.FilingState[p_sClaimId];
            }

            string sSql = string.Format("SELECT FILING_STATE_ID FROM CLAIM WHERE CLAIM_ID = {0}", p_sClaimId);
            string sStateId = string.Empty;
            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSql))
            {
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sStateId = objReader["FILING_STATE_ID"].ToString();
                    }
                    objReader.Close();
                }
            }
            this.FilingState.Add(p_sClaimId, sStateId);
            return sStateId;
        }

        #endregion
    }
}

