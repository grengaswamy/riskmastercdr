using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.Application.PrintEOB
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   22 December 2004
	///Purpose :   This is the class corresponding to the EOB Report (Active Report).
	///			   It contains methods related to the report layout, report fields initialization and populating them with data.
	/// </summary>
	internal class rptEOP : CommonEOB 
	{
        private int m_iClientId = 0;

		public rptEOP()
		{
			InitializeReport();
		}
		
		public rptEOP(string p_sConnectionString, string p_sUserName, int p_iClientId):base(p_sConnectionString,p_sUserName, p_iClientId)
		{
			InitializeReport();
            m_iClientId = p_iClientId;
		}

		#region Methods
		/// <summary>
		/// This method would invoke the method of base class to initialize the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void rptEOP_DataInitialize(object p_objsender, System.EventArgs p_objeArgs)
		{
			base.DataInitializeBase(ref p_objsender,ref p_objeArgs);			
		}
		/// <summary>
		/// This method would invoke the method of base class to fill data in the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void rptEOP_FetchData(object p_objsender, FetchEventArgs p_objeArgs)
		{
			base.FetchDataBase(ref p_objsender,ref p_objeArgs);
    
		}
		/// <summary>
		/// This method would invoke the method of base class to do the page settings before start printing 
		/// the report and also to print the soft error log.		
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void rptEOP_ReportStart(object p_objsender, System.EventArgs p_objeArgs)
		{
			base.ReportStartBase(ref p_objsender,ref p_objeArgs);			
		}

		#endregion

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Label Label17 = null;
		private DataDynamics.ActiveReports.Label Label67 = null;
		private DataDynamics.ActiveReports.Label Label68 = null;
		private DataDynamics.ActiveReports.Label Label69 = null;
		private DataDynamics.ActiveReports.Label Label76 = null;
		private DataDynamics.ActiveReports.Label Label91 = null;
		private DataDynamics.ActiveReports.Label Label92 = null;
		private DataDynamics.ActiveReports.Label Label93 = null;
		private DataDynamics.ActiveReports.Line Line24 = null;
		private DataDynamics.ActiveReports.Label Label94 = null;
		private DataDynamics.ActiveReports.Label Label95 = null;
		private DataDynamics.ActiveReports.TextBox Field63 = null;
		private DataDynamics.ActiveReports.Label Label96 = null;
		private DataDynamics.ActiveReports.Label Label97 = null;
		private DataDynamics.ActiveReports.Label Label98 = null;
		private DataDynamics.ActiveReports.Label Label99 = null;
		private DataDynamics.ActiveReports.Label Label100 = null;
		private DataDynamics.ActiveReports.Label Label101 = null;
		private DataDynamics.ActiveReports.Label Label102 = null;
		private DataDynamics.ActiveReports.Label Label103 = null;
		private DataDynamics.ActiveReports.Label Label104 = null;
		private DataDynamics.ActiveReports.Label Label105 = null;
		private DataDynamics.ActiveReports.Line Line25 = null;
		private DataDynamics.ActiveReports.Label Label107 = null;
		private DataDynamics.ActiveReports.Label Label108 = null;
		private DataDynamics.ActiveReports.Label Label109 = null;
		private DataDynamics.ActiveReports.Label Label110 = null;
		private DataDynamics.ActiveReports.Label Label111 = null;
		private DataDynamics.ActiveReports.Label Label112 = null;
		private DataDynamics.ActiveReports.Label Label113 = null;
		private DataDynamics.ActiveReports.Label Label114 = null;
		private DataDynamics.ActiveReports.Label Label115 = null;
		private DataDynamics.ActiveReports.Label Label116 = null;
		private DataDynamics.ActiveReports.Label Label117 = null;
		private DataDynamics.ActiveReports.Label Label118 = null;
		private DataDynamics.ActiveReports.TextBox Field88 = null;
		private DataDynamics.ActiveReports.TextBox Field89 = null;
		private DataDynamics.ActiveReports.TextBox Field90 = null;
		private DataDynamics.ActiveReports.TextBox Field91 = null;
		private DataDynamics.ActiveReports.TextBox Field93 = null;
		private DataDynamics.ActiveReports.TextBox Field94 = null;
		private DataDynamics.ActiveReports.TextBox Field95 = null;
		private DataDynamics.ActiveReports.TextBox Field96 = null;
		private DataDynamics.ActiveReports.TextBox Field97 = null;
		private DataDynamics.ActiveReports.TextBox Field98 = null;
		private DataDynamics.ActiveReports.TextBox Field100 = null;
		private DataDynamics.ActiveReports.TextBox Field101 = null;
		private DataDynamics.ActiveReports.TextBox Field102 = null;
		private DataDynamics.ActiveReports.TextBox Field104 = null;
		private DataDynamics.ActiveReports.TextBox Field105 = null;
		private DataDynamics.ActiveReports.TextBox Field106 = null;
		private DataDynamics.ActiveReports.TextBox Field107 = null;
		private DataDynamics.ActiveReports.TextBox Field108 = null;
		private DataDynamics.ActiveReports.TextBox Field109 = null;
		private DataDynamics.ActiveReports.TextBox Field110 = null;
		private DataDynamics.ActiveReports.TextBox Field111 = null;
		private DataDynamics.ActiveReports.TextBox Field112 = null;
		private DataDynamics.ActiveReports.TextBox Field113 = null;
		private DataDynamics.ActiveReports.TextBox Field114 = null;
		private DataDynamics.ActiveReports.TextBox Field115 = null;
		private DataDynamics.ActiveReports.TextBox Field116 = null;
		private DataDynamics.ActiveReports.TextBox Field117 = null;
		private DataDynamics.ActiveReports.TextBox Field118 = null;
		private DataDynamics.ActiveReports.TextBox Field119 = null;
		private DataDynamics.ActiveReports.TextBox Field120 = null;
		private DataDynamics.ActiveReports.TextBox Field121 = null;
		private DataDynamics.ActiveReports.TextBox Field17 = null;
		private DataDynamics.ActiveReports.GroupHeader GroupHeader1 = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.Line Line22 = null;
		private DataDynamics.ActiveReports.Line Line23 = null;
		private DataDynamics.ActiveReports.GroupFooter GroupFooter1 = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		public void InitializeReport()
		{
			try
            {
                this.Document.Printer.PrinterName = "";
				this.LoadLayout(this.GetType(), "Riskmaster.Application.PrintEOB.rptEOP.rpx");
				this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
				this.GroupHeader1 = ((DataDynamics.ActiveReports.GroupHeader)(this.Sections["GroupHeader1"]));
				this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
				this.GroupFooter1 = ((DataDynamics.ActiveReports.GroupFooter)(this.Sections["GroupFooter1"]));
				this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
				this.Label17 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[0]));
				this.Label67 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
				this.Label68 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
				this.Label69 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[3]));
				this.Label76 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[4]));
				this.Label91 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[5]));
				this.Label92 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[6]));
				this.Label93 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[7]));
				this.Line24 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[8]));
				this.Label94 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[9]));
				this.Label95 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[10]));
				this.Field63 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[11]));
				this.Label96 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[12]));
				this.Label97 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[13]));
				this.Label98 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[14]));
				this.Label99 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[15]));
				this.Label100 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[16]));
				this.Label101 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[17]));
				this.Label102 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[18]));
				this.Label103 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[19]));
				this.Label104 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[20]));
				this.Label105 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[21]));
				this.Line25 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[22]));
				this.Label107 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[23]));
				this.Label108 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[24]));
				this.Label109 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[25]));
				this.Label110 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[26]));
				this.Label111 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[27]));
				this.Label112 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[28]));
				this.Label113 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[29]));
				this.Label114 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[30]));
				this.Label115 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[31]));
				this.Label116 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[32]));
				this.Label117 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[33]));
				this.Label118 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[34]));
				this.Field88 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[35]));
				this.Field89 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[36]));
				this.Field90 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[37]));
				this.Field91 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[38]));
				this.Field93 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[39]));
				this.Field94 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[40]));
				this.Field95 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[41]));
				this.Field96 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[42]));
				this.Field97 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[43]));
				this.Field98 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[44]));
				this.Field100 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[45]));
				this.Field101 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[46]));
				this.Field102 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[47]));
				this.Field104 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[48]));
				this.Field105 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[49]));
				this.Field106 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[50]));
				this.Field107 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[51]));
				this.Field108 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[52]));
				this.Field109 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[53]));
				this.Field110 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[54]));
				this.Field111 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[55]));
				this.Field112 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[56]));
				this.Field113 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[57]));
				this.Field114 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[58]));
				this.Field115 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[59]));
				this.Field116 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[60]));
				this.Field117 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[61]));
				this.Field118 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[62]));
				this.Field119 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[63]));
				this.Field120 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[64]));
				this.Field121 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[65]));
				this.Field17 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[66]));
				this.Line22 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[0]));
				this.Line23 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[1]));
				// Attach Report Events
				this.DataInitialize += new System.EventHandler(this.rptEOP_DataInitialize);
				this.FetchData += new FetchEventHandler(this.rptEOP_FetchData);
				this.ReportStart += new System.EventHandler(this.rptEOP_ReportStart);
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintEOBAR.InitializeReportEOB.Error", m_iClientId), p_objException);
			}
		}

		#endregion
	}
}
