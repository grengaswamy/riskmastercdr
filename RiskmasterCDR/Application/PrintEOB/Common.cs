using System;
using System.Collections;
 
namespace Riskmaster.Application.PrintEOB
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   22 December 2004
	///Purpose :   This class contains the common structure declarations used in PrintEOB component classes.
	/// </summary>
	internal class Common
	{
		/// <summary>
		/// Structure to hold the Login User Information
		/// </summary>
		internal LoginInfo m_stLoginInfo;
	}

	#region "Structure Declaration"

	#region "Struct EOBRecord"
	/// Name		: EOBRecord
	/// Author		: Anurag Agarwal
	/// Date Created	: 24 Dec 2004		
	/// ************************************************************
	/// Amendment History
	/// ************************************************************
	/// Date Amended   *   Amendment   *    Author
	///                *               *
	///                *               *
	/// ************************************************************
	/// <summary>
	/// Structure to hold the EOB record 
	/// </summary>
	internal struct EOBRecord
	{
		/// <summary>
		/// Stores the CurrentAlign value
		/// </summary>
		public string CurrentAlign;

		/// <summary>
		/// Stores the CurrentX1Margin value
		/// </summary>
		public double CurrentX1Margin;

		/// <summary>
		/// Stores the CurrentY1Margin value
		/// </summary>
		public double CurrentY1Margin;

		/// <summary>
		/// Stores the CurrentX2Margin value
		/// </summary>
		public double CurrentX2Margin;

		/// <summary>
		/// Stores the CurrentY2Margin value
		/// </summary>
		public double CurrentY2Margin;

		/// <summary>
		/// Stores the LogoX value
		/// </summary>
		public double LogoX;

		/// <summary>
		/// Stores the LogoY value
		/// </summary>
		public double LogoY;

		/// <summary>
		/// Stores the LogoWidth value
		/// </summary>
		public double LogoWidth;

		/// <summary>
		/// Stores the LogoHeight value
		/// </summary>
		public double LogoHeight;

		/// <summary>
		/// Stores the LogoPath value
		/// </summary>
		public string LogoPath;

		/// <summary>
		/// Stores the FirstPrint value
		/// </summary>
		public bool FirstPrint;

		/// <summary>
		/// Stores the DetailItemSpecified value
		/// </summary>
		public int DetailItemSpecified;

		/// <summary>
		/// Stores the Title value
		/// </summary>
		public string Title;

		/// <summary>
		/// Stores the SubTitle value
		/// </summary>
		public string SubTitle;

		/// <summary>
		/// Stores the TitleAlign value
		/// </summary>
		public string TitleAlign;

		/// <summary>
		/// Stores the TitleFont value
		/// </summary>
		public double TitleFont;

		/// <summary>
		/// Stores the TitleBold value
		/// </summary>
		public int TitleBold;

		/// <summary>
		/// Stores the SubTitleAlign value
		/// </summary>
		public string SubTitleAlign;

		/// <summary>
		/// Stores the SubTitleFont value
		/// </summary>
		public double SubTitleFont;

		/// <summary>
		/// Stores the SubTitleBold value
		/// </summary>
		public int SubTitleBold;

		/// <summary>
		/// Stores the ClaimNumber value
		/// </summary>
		public string ClaimNumber;

		/// <summary>
		/// Stores the ClaimantFullName value
		/// </summary>
		public string ClaimantFullName;

		/// <summary>
		/// Stores the ClaimantLastName value
		/// </summary>
		public string ClaimantLastName;

		/// <summary>
		/// Stores the ClaimantFirstName value
		/// </summary>
		public string ClaimantFirstName;

		/// <summary>
		/// Stores the ClaimantAddr1 value
		/// </summary>
		public string ClaimantAddr1;

		/// <summary>
		/// Stores the ClaimantAddr2 value
		/// </summary>
		public string ClaimantAddr2;

        /// <summary>
        /// Stores the ClaimantAddr3 value
        /// </summary>
        public string ClaimantAddr3;

        /// <summary>
        /// Stores the ClaimantAddr4 value
        /// </summary>
        public string ClaimantAddr4;

		/// <summary>
		/// Stores the ClaimantCity value
		/// </summary>
		public string ClaimantCity;

		/// <summary>
		/// Stores the ClaimantState value
		/// </summary>
		public string ClaimantState;

		/// <summary>
		/// Stores the ClaimantPostalCode value
		/// </summary>
		public string ClaimantPostalCode;

		/// <summary>
		/// Stores the PayeeLastName value
		/// </summary>
		public string PayeeLastName;

		/// <summary>
		/// Stores the PayeeFirstName value
		/// </summary>
		public string PayeeFirstName;

		/// <summary>
		/// Stores the PayeeFullName value
		/// </summary>
		public string PayeeFullName;

		/// <summary>
		/// Stores the PayeeAddr1 value
		/// </summary>
		public string PayeeAddr1;

		/// <summary>
		/// Stores the PayeeAddr2 value
		/// </summary>
		public string PayeeAddr2;

        /// <summary>
        /// /// Stores the PayeeAddr3 value
        /// </summary>
        public string PayeeAddr3;

        /// <summary>
        /// Stores the PayeeAddr4 value
        /// </summary>
        public string PayeeAddr4;

		/// <summary>
		/// Stores the PayeeCity value
		/// </summary>
		public string PayeeCity;

		/// <summary>
		/// Stores the PayeeState value
		/// </summary>
		public string PayeeState;

		/// <summary>
		/// Stores the PayeePostalCode value
		/// </summary>
		public string PayeePostalCode;

		/// <summary>
		/// Stores the TransDate value
		/// </summary>
		public string TransDate;

		/// <summary>
		/// Stores the TotalAmountPaid value
		/// </summary>
		public double TotalAmountPaid;

		/// <summary>
		/// Stores the TotalAmountBilled value
		/// </summary>
		public double TotalAmountBilled;

		/// <summary>
		/// Stores the TotalAmountReduced value
		/// </summary>
		public double TotalAmountReduced;

		/// <summary>
		/// Stores the TotalScheduledAmount value
		/// </summary>
		public double TotalScheduledAmount;

		/// <summary>
		/// Stores the TotalAmountSaved value
		/// </summary>
		public double TotalAmountSaved;

		/// <summary>
		/// Stores the ClaimantSSN value
		/// </summary>
		public string ClaimantSSN;

		/// <summary>
		/// Stores the EventDate value
		/// </summary>
		public string EventDate;

		/// <summary>
		/// Stores the DateReported value
		/// </summary>
		public string DateReported;

		/// <summary>
		/// Stores the RptdFullName value
		/// </summary>
		public string RptdFullName;

		/// <summary>
		/// Stores the PayeeTaxID value
		/// </summary>
		public string PayeeTaxID;

		/// <summary>
		/// Stores the TotalAmountAllowed value
		/// </summary>
		public double TotalAmountAllowed;

		/// <summary>
		/// Stores the TotalBaseAmount value
		/// </summary>
		public double TotalBaseAmount;

		/// <summary>
		/// Stores the TotalDiscountAmount value
		/// </summary>
		public double TotalDiscountAmount;

		/// <summary>
		/// Stores the TotalPerDiemAmt value
		/// </summary>
		public double TotalPerDiemAmt;

		/// <summary>
		/// Stores the TotalStopLossAmt value
		/// </summary>
		public double TotalStopLossAmt;

		/// <summary>
		/// Stores the TotalFeeTableAmt value
		/// </summary>
		public double TotalFeeTableAmt;

		/// <summary>
		/// Stores the EmpDeptCode value
		/// </summary>
		public string EmpDeptCode;

		/// <summary>
		/// Stores the EmpDepartment value
		/// </summary>
		public string EmpDepartment;

		/// <summary>
		/// Stores the EmpFacility value
		/// </summary>
		public string EmpFacility;

		/// <summary>
		/// Stores the EmpLocation value
		/// </summary>
		public string EmpLocation;

		/// <summary>
		/// Stores the EmpDivision value
		/// </summary>
		public string EmpDivision;

		/// <summary>
		/// Stores the EmpRegion value
		/// </summary>
		public string EmpRegion;

		/// <summary>
		/// Stores the EmpOperation value
		/// </summary>
		public string EmpOperation;

		/// <summary>
		/// Stores the EmpCompany value
		/// </summary>
		public string EmpCompany;

        //Ashish Ahuja Mits 33681 : Start
        public string EMP_DIV_CITY;
        public string EMP_DIV_STATE;
        public string EMP_DIV_POSTAL_CODE;
        //Ashish Ahuja Mits 33681 : End

        //skhare7 MITS 23373
        /// <summary>
        /// Stores the EmpDiv Add
        /// </summary>
        public string EMP_DIV_ADDR1;
      
        public string EMP_DIV_ADDR2;
        public string EMP_DIV_ADDR3;
        public string EMP_DIV_ADDR4;
        /// <summary>
        /// Stores the EmpDEpt Add
        /// </summary>
        public string EMP_DEPT_ADDR1;
       
        public string EMP_DEPT_ADDR2;
        public string EMP_DEPT_ADDR3;
        public string EMP_DEPT_ADDR4;
        /// <summary>
        /// Stores the EmpFac Add
        /// </summary>
        public string EMP_FAC_ADDR1;
       
        public string EMP_FAC_ADDR2;
        public string EMP_FAC_ADDR3;
        public string EMP_FAC_ADDR4;
        // <summary>
        /// Stores the EmpLocation Add
        /// </summary>
        public string EMP_LOC_ADDR1;

        public string EMP_LOC_ADDR2;
        public string EMP_LOC_ADDR3;
        public string EMP_LOC_ADDR4;
        // <summary>
        /// Stores the EmpRegion Add
        /// </summary>
        public string EMP_REG_ADDR1;

        public string EMP_REG_ADDR2;
        public string EMP_REG_ADDR3;
        public string EMP_REG_ADDR4;
        // <summary>
        /// Stores the EmpClient Add
        /// </summary>
        public string EMP_CLIENT_ADDR1;

        public string EMP_CLIENT_ADDR2;
        public string EMP_CLIENT_ADDR3;
        public string EMP_CLIENT_ADDR4;

        // <summary>
        /// Stores the EmpComp Add
        /// </summary>
        public string EMP_COMP_ADDR1;

        public string EMP_COMP_ADDR2;
        public string EMP_COMP_ADDR3;
        public string EMP_COMP_ADDR4;
        // <summary>
        /// Stores the EmpCertificate Add
        /// </summary>
        public string EMP_CERT_NUM;

        // <summary>
        /// Stores the EmpOP Add
        /// </summary>
        public string EMP_OP_ADDR1;

        public string EMP_OP_ADDR2;
        public string EMP_OP_ADDR3;
        public string EMP_OP_ADDR4;
        public string EMP_ADDRESS1;
        //skhare7 MITS 23373 end
		/// <summary>
		/// Stores the EmpClient value
		/// </summary>
		public string EmpClient;

		/// <summary>
		/// Stores the AdjusterAbbrev value
		/// </summary>
		public string AdjusterAbbrev;

		/// <summary>
		/// Stores the AdjusterFullName value
		/// </summary>
		public string AdjusterFullName;

		/// <summary>
		/// Stores the AdjusterLastName value
		/// </summary>
		public string AdjusterLastName;
        
        //Arnab: MITS-10722 - Added field for Adjuster Phone Number
        /// <summary>
        /// Stores the Adjuster Phone Number value
        /// </summary>
        public string AdjusterPhone1;
        //MITS-10722 End

		/// <summary>
		/// Stores the OrigInvDate value
		/// </summary>
		public string OrigInvDate;

		/// <summary>
		/// Stores the OrigInvUser value
		/// </summary>
		public string OrigInvUser;
        //BRS FL Merge

        public string FirstInvoice ;
        public string FirstInvoiceDate;//Debabrata Biswas
        public string DateOfService;//Debabrata Biswas
        public string FirstLicense;
        public string CheckNumber;
        public string FirstBillingPostalCode;
	}
	#endregion
	
	#region "Struct EOBDetailRecord"
	/// Name		: EOBDetailRecord
	/// Author		: Anurag Agarwal
	/// Date Created	: 24 Dec 2004		
	/// ************************************************************
	/// Amendment History
	/// ************************************************************
	/// Date Amended   *   Amendment   *    Author
	///                *               *
	///                *               *
	/// ************************************************************
	/// <summary>
	/// Structure to hold the EOB Detail record 
	/// </summary>
	internal struct EOBDetailRecord
	{
		/// <summary>
		/// Stores the InvoiceNumber value
		/// </summary>
		public DetailStringRecord InvoiceNumber;

		/// <summary>
		/// Stores the InvoiceDate value
		/// </summary>
		public DetailStringRecord InvoiceDate;

		/// <summary>
		/// Stores the InvoicedBy value
		/// </summary>
		public DetailStringRecord InvoicedBy;

		/// <summary>
		/// Stores the FeeSchedule value
		/// </summary>
		public DetailStringRecord FeeSchedule;

		/// <summary>
		/// Stores the ProcedureDate value
		/// </summary>
		public DetailStringRecord ProcedureDate;

		/// <summary>
		/// Stores the ProcToDate value
		/// </summary>
		public DetailStringRecord ProcToDate;

		/// <summary>
		/// Stores the Procedure value
		/// </summary>
		public DetailStringRecord Procedure;

		/// <summary>
		/// Stores the ProcedureCode value
		/// </summary>
		public DetailStringRecord ProcedureCode;

		/// <summary>
		/// Stores the EOBAlign value
		/// </summary>
		public string EOBAlign;

		/// <summary>
		/// Stores the EOBWidth value
		/// </summary>
		public int EOBWidth;

		/// <summary>
		/// Stores the EOBDataWidth value
		/// </summary>
		public int EOBDataWidth;

		/// <summary>
		/// Stores the EOBLineItems value
		/// </summary>
		public int EOBLineItems;

		/// <summary>
		/// Stores the EOBLine(10) value
		/// </summary>
		public string [] EOBLine;

		/// <summary>
		/// Stores the EOB value
		/// </summary>
		public Hashtable EOBList;

        /// <summary>
        /// Stores the EOB Detals
        /// </summary>
        public Hashtable EOBListDetails;


		/// <summary>
		/// Stores the AmountBilled value
		/// </summary>
		public DetailDoubleRecord AmountBilled;

		/// <summary>
		/// Stores the AmountPaid value
		/// </summary>
		public DetailDoubleRecord AmountPaid;

		/// <summary>
		/// Stores the ColumnsSelected value
		/// </summary>
		public int ColumnsSelected;

		/// <summary>
		/// Stores the PlaceOfService value
		/// </summary>
		public DetailStringRecord PlaceOfService;

		/// <summary>
		/// Stores the PlaceofServiceCode value
		/// </summary>
		public DetailStringRecord PlaceofServiceCode;

		/// <summary>
		/// Stores the TypeOfService value
		/// </summary>
		public DetailStringRecord TypeOfService;

		/// <summary>
		/// Stores the TypeOfServiceCode value
		/// </summary>
		public DetailStringRecord TypeOfServiceCode;

		/// <summary>
		/// Stores the UnitsBilled value
		/// </summary>
		public DetailDoubleRecord UnitsBilled;

		/// <summary>
		/// Stores the ScheduledAmount value
		/// </summary>
		public DetailDoubleRecord ScheduledAmount;

		/// <summary>
		/// Stores the AmountReduced value
		/// </summary>
		public DetailDoubleRecord AmountReduced;

		/// <summary>
		/// Stores the AmountSaved value
		/// </summary>
		public DetailDoubleRecord AmountSaved;

		/// <summary>
		/// Stores the Modifier value
		/// </summary>
		public DetailStringRecord Modifier;

		/// <summary>
		/// Stores the ModifierCode value
		/// </summary>
		public DetailStringRecord ModifierCode;

		/// <summary>
		/// Stores the Quantity value
		/// </summary>
		public DetailStringRecord Quantity;

		/// <summary>
		/// Stores the QuantityCode value
		/// </summary>
		public DetailStringRecord QuantityCode;

		/// <summary>
		/// Stores the Store value
		/// </summary>
		public DetailStringRecord Store;

		/// <summary>
		/// Stores the StoreCode value
		/// </summary>
		public DetailStringRecord StoreCode;

		/// <summary>
		/// Stores the Percentile value
		/// </summary>
		public DetailStringRecord Percentile;

		/// <summary>
		/// Stores the DiagnosisAlign value
		/// </summary>
		public string DiagnosisAlign;

		/// <summary>
		/// Stores the DiagnosisWidth value
		/// </summary>
		public int DiagnosisWidth;

		/// <summary>
		/// Stores the DiagnosisDataWidth value
		/// </summary>
		public int DiagnosisDataWidth;

		/// <summary>
		/// Stores the DiagnosisItems value
		/// </summary>
		public int DiagnosisItems;

		/// <summary>
		/// Stores the Diagnosis(10) value
		/// </summary>
		public string [] Diagnosis;

		/// <summary>
		/// Stores the Diagnosis list
		/// </summary>
		public Hashtable DiagnosisList;

        /// <summary>
        /// Stores the DiagnosisCode list
        /// </summary>
        public Hashtable DiagnosisCodeList;     //Debabrata Biswas

        /// <summary>
        /// Stores the DiagnosisDetails list i.e. short code description
        /// </summary>
        public Hashtable DiagnosisDetails;     //Debabrata Biswas

		/// <summary>
		/// Stores the ContractAmount value
		/// </summary>
		public DetailDoubleRecord ContractAmount;

		/// <summary>
		/// Stores the Discount value
		/// </summary>
		public DetailDoubleRecord Discount;

		/// <summary>
		/// Stores the ProviderZipCode value
		/// </summary>
		public DetailStringRecord ProviderZipCode;

		/// <summary>
		/// Stores the AmountAllowed value
		/// </summary>
		public DetailDoubleRecord AmountAllowed;

		/// <summary>
		/// Stores the BaseAmount value
		/// </summary>
		public DetailDoubleRecord BaseAmount;

		/// <summary>
		/// Stores the DiscountAmount value
		/// </summary>
		public DetailDoubleRecord DiscountAmount;

		/// <summary>
		/// Stores the PerDiemAmount value
		/// </summary>
		public DetailDoubleRecord PerDiemAmount;

		/// <summary>
		/// Stores the StopLossAmount value
		/// </summary>
		public DetailDoubleRecord StopLossAmount;

		/// <summary>
		/// Stores the FeeTableAmount value
		/// </summary>
		public DetailDoubleRecord FeeTableAmount;

		/// <summary>
		/// Stores the ModifierAlign value
		/// </summary>
		public string ModifierAlign;

		/// <summary>
		/// Stores the ModifierWidth value
		/// </summary>
		public int ModifierWidth;

		/// <summary>
		/// Stores the ModifierDataWidth value
		/// </summary>
		public int ModifierDataWidth;

		/// <summary>
		/// Stores the Modifier List
		/// </summary>
		public Hashtable ModifierList;

		/// <summary>
		/// Stores the ModCode List
		/// </summary>
		public Hashtable ModCodeList;

		/// <summary>
		/// Stores the EOBLine List
		/// </summary>
		public Hashtable EOBLineList;

		/// <summary>
		/// Stores the DiscOnFeeSchd value
		/// </summary>
		public DetailDoubleRecord DiscOnFeeSchd;

		/// <summary>
		/// Stores the PrescripNo value
		/// </summary>
		public DetailStringRecord PrescripNo;

		/// <summary>
		/// Stores the DrugName value
		/// </summary>
		public DetailStringRecord DrugName;

		/// <summary>
		/// Stores the PrescripDate value
		/// </summary>
		public DetailStringRecord PrescripDate;

        //BRS FL Merge
        public DetailStringRecord PhysicianLicenseNum;

        public DetailStringRecord RevenueCode;
	}
	#endregion

	#region "Struct CEOBRecord"
	/// Name		: CEOBRecord
	/// Author		: Anurag Agarwal
	/// Date Created	: 24 Dec 2004		
	/// ************************************************************
	/// Amendment History
	/// ************************************************************
	/// Date Amended   *   Amendment   *    Author
	///                *               *
	///                *               *
	/// ************************************************************
	/// <summary>
	/// Structure to hold the EOB record 
	/// </summary>
	internal struct CEOBRecord
	{
		/// <summary>
		/// Stores the claimnumber value
		/// </summary>
		public string ClaimNumber;

		/// <summary>
		/// Stores the claimantfullname value
		/// </summary>
		public string ClaimantFullName;

		/// <summary>
		/// Stores the claimantlastname value
		/// </summary>
		public string ClaimantLastName;

		/// <summary>
		/// Stores the claimantfirstname value
		/// </summary>
		public string ClaimantFirstName;

		/// <summary>
		/// Stores the claimantaddr1 value
		/// </summary>
		public string ClaimantAddr1;

		/// <summary>
		/// Stores the claimantaddr2 value
		/// </summary>
		public string ClaimantAddr2;

        /// <summary>
        /// Stores the claimantaddr3 value
        /// </summary>
        public string ClaimantAddr3;

        /// <summary>
        /// Stores the claimantaddr4 value
        /// </summary>
        public string ClaimantAddr4;

		/// <summary>
		/// Stores the claimantcity value
		/// </summary>
		public string ClaimantCity;

		/// <summary>
		/// Stores the claimantstate value
		/// </summary>
		public string ClaimantState;

		/// <summary>
		/// Stores the claimantpostalcode value
		/// </summary>
		public string ClaimantPostalCode;

		/// <summary>
		/// Stores the payeelastname value
		/// </summary>
		public string PayeeLastName;

		/// <summary>
		/// Stores the payeefirstname value
		/// </summary>
		public string PayeeFirstName;

		/// <summary>
		/// Stores the payeefullname value
		/// </summary>
		public string PayeeFullName;

		/// <summary>
		/// Stores the payeeaddr1 value
		/// </summary>
		public string PayeeAddr1;

		/// <summary>
		/// Stores the payeeaddr2 value
		/// </summary>
		public string PayeeAddr2;

        /// <summary>
        /// Stores the payeeaddr3 value
        /// </summary>
        public string PayeeAddr3;

        /// <summary>
        /// Stores the payeeaddr4 value
        /// </summary>
        public string PayeeAddr4;

		/// <summary>
		/// Stores the payeecity value
		/// </summary>
		public string PayeeCity;

		/// <summary>
		/// Stores the payeestate value
		/// </summary>
		public string PayeeState;

		/// <summary>
		/// Stores the payeepostalcode value
		/// </summary>
		public string PayeePostalCode;

		/// <summary>
		/// Stores the transdate value
		/// </summary>
		public string TransDate;

		/// <summary>
		/// Stores the totalamountpaid value
		/// </summary>
		public double TotalAmountPaid;

		/// <summary>
		/// Stores the totalamountbilled value
		/// </summary>
		public double TotalAmountBilled;

		/// <summary>
		/// Stores the totalamountreduced value
		/// </summary>
		public double TotalAmountReduced;

		/// <summary>
		/// Stores the totalscheduledamount value
		/// </summary>
		public double TotalScheduledAmount;

		/// <summary>
		/// Stores the totalamountsaved value
		/// </summary>
		public double TotalAmountSaved;

		/// <summary>
		/// Stores the claimantssn value
		/// </summary>
		public string ClaimantSSN;

		/// <summary>
		/// Stores the claimantfulladdress value
		/// </summary>
		public string ClaimantFullAddress;

		/// <summary>
		/// Stores the eventdate value
		/// </summary>
		public string EventDate;

		/// <summary>
		/// Stores the datereported value
		/// </summary>
		public string DateReported;

		/// <summary>
		/// Stores the rptdfullname value
		/// </summary>
		public string RptdFullName;

		/// <summary>
		/// Stores the payeetaxid value
		/// </summary>
		public string PayeeTaxID;

        //Changed by Gagan for MITS 11520 : Start
        /// <summary>
        /// Stores the payee npi number value
        /// </summary>
        public string PayeeNPI;
        //Changed by Gagan for MITS 11520 : End

		/// <summary>
		/// Stores the totalamountallowed value
		/// </summary>
		public double TotalAmountAllowed;

		/// <summary>
		/// Stores the totalbaseamount value
		/// </summary>
		public double TotalBaseAmount;

		/// <summary>
		/// Stores the totaldiscountamount value
		/// </summary>
		public double TotalDiscountAmount;

		/// <summary>
		/// Stores the totalperdiemamt value
		/// </summary>
		public double TotalPerDiemAmt;

		/// <summary>
		/// Stores the totalstoplossamt value
		/// </summary>
		public double TotalStopLossAmt;

		/// <summary>
		/// Stores the totalfeetableamt value
		/// </summary>
		public double TotalFeeTableAmt;

		/// <summary>
		/// Stores the empdeptcode value
		/// </summary>
		public string EmpDeptCode;

		/// <summary>
		/// Stores the empdepartment value
		/// </summary>
		public string EmpDepartment;

		/// <summary>
		/// Stores the empfacility value
		/// </summary>
		public string EmpFacility;

		/// <summary>
		/// Stores the emplocation value
		/// </summary>
		public string EmpLocation;

		/// <summary>
		/// Stores the empdivision value
		/// </summary>
		public string EmpDivision;

		/// <summary>
		/// Stores the empregion value
		/// </summary>
		public string EmpRegion;

		/// <summary>
		/// Stores the empoperation value
		/// </summary>
		public string EmpOperation;

		/// <summary>
		/// Stores the empcompany value
		/// </summary>
		public string EmpCompany;
        //skhare7 MITS 23373
        /// <summary>
        /// Stores the EmpDiv Add
        /// </summary>
        public string EMP_DIV_ADDR1;

        public string EMP_DIV_ADDR2;
        public string EMP_DIV_ADDR3;
        public string EMP_DIV_ADDR4;
        /// <summary>
        /// Stores the EmpDEpt Add
        /// </summary>
        public string EMP_DEPT_ADDR1;

        public string EMP_DEPT_ADDR2;
        public string EMP_DEPT_ADDR3;
        public string EMP_DEPT_ADDR4;
        /// <summary>
        /// Stores the EmpFac Add
        /// </summary>
        public string EMP_FAC_ADDR1;

        public string EMP_FAC_ADDR2;
        public string EMP_FAC_ADDR3;
        public string EMP_FAC_ADDR4;
        // <summary>
        /// Stores the EmpLocation Add
        /// </summary>
        public string EMP_LOC_ADDR1;

        public string EMP_LOC_ADDR2;
        public string EMP_LOC_ADDR3;
        public string EMP_LOC_ADDR4;
        // <summary>
        /// Stores the EmpRegion Add
        /// </summary>
        public string EMP_REG_ADDR1;

        public string EMP_REG_ADDR2;
        public string EMP_REG_ADDR3;
        public string EMP_REG_ADDR4;
        // <summary>
        /// Stores the EmpClient Add
        /// </summary>
        public string EMP_CLIENT_ADDR1;

        public string EMP_CLIENT_ADDR2;
        public string EMP_CLIENT_ADDR3;
        public string EMP_CLIENT_ADDR4;

        // <summary>
        /// Stores the EmpComp Add
        /// </summary>
        public string EMP_COMP_ADDR1;

        public string EMP_COMP_ADDR2;
        public string EMP_COMP_ADDR3;
        public string EMP_COMP_ADDR4;
        // <summary>
        /// Stores the EmpCertificate Add
        /// </summary>
        public string EMP_CERT_NUM;

        // <summary>
        /// Stores the EmpOP Add
        /// </summary>
        public string EMP_OP_ADDR1;

        public string EMP_OP_ADDR2;
        public string EMP_OP_ADDR3;
        public string EMP_OP_ADDR4;

        public string EMP_ADDRESS1;
        //skhare7 MITS 23373 End

		/// <summary>
		/// Stores the empclient value
		/// </summary>
		public string EmpClient;

		/// <summary>
		/// Stores the empdepartmentnaics value
		/// </summary>
		public string EmpDepartmentNAICS;

		/// <summary>
		/// Stores the empfacilitynaics value
		/// </summary>
		public string EmpFacilityNAICS;

		/// <summary>
		/// Stores the emplocationnaics value
		/// </summary>
		public string EmpLocationNAICS;

		/// <summary>
		/// Stores the empdivisionnaics value
		/// </summary>
		public string EmpDivisionNAICS;

		/// <summary>
		/// Stores the empregionnaics value
		/// </summary>
		public string EmpRegionNAICS;

		/// <summary>
		/// Stores the empoperationnaics value
		/// </summary>
		public string EmpOperationNAICS;

		/// <summary>
		/// Stores the empcompanynaics value
		/// </summary>
		public string EmpCompanyNAICS;

		/// <summary>
		/// Stores the empclientnaics value
		/// </summary>
		public string EmpClientNAICS;

		/// <summary>
		/// Stores the adjusterabbrev value
		/// </summary>
		public string AdjusterAbbrev;

		/// <summary>
		/// Stores the adjusterfullname value
		/// </summary>
		public string AdjusterFullName;

		/// <summary>
		/// Stores the adjusterlastname value
		/// </summary>
		public string AdjusterLastName;

        //Arnab: MITS-10722 - Added field for Adjuster Phone Number
        /// <summary>
        /// Stores the Adjuster Phone Number value
        /// </summary>
        public string AdjusterPhone1;
        //MITS-10722 End

		/// <summary>
		/// Stores the physicianfullname value
		/// </summary>
		public string PhysicianFullName;

		/// <summary>
		/// Stores the physicianaddress value
		/// </summary>
		public string PhysicianAddress;

		/// <summary>
		/// Stores the physiciantaxid value
		/// </summary>
		public string PhysicianTaxID;

		/// <summary>
		/// Stores the filenumber value
		/// </summary>
		public string FileNumber;

		/// <summary>
		/// Stores the Detailrecords List
		/// </summary>
		public Hashtable DetailRecordsList;

		/// <summary>
		/// Stores the empfacilityaddress value
		/// </summary>
		public string EmpFacilityAddress;

		/// <summary>
		/// Stores the empfacilitytaxid value
		/// </summary>
		public string EmpFacilityTaxID;

		/// <summary>
		/// Stores the batchnumber value
		/// </summary>
		public string BatchNumber;

		/// <summary>
		/// Stores the checknumber value
		/// </summary>
		public string CheckNumber;

		/// <summary>
		/// Stores the vendornumber value
		/// </summary>
		public string VendorNumber;

		/// <summary>
		/// Stores the clientnumber value
		/// </summary>
		public string ClientNumber;

		/// <summary>
		/// Stores the lossdate value
		/// </summary>
		public string LossDate;

		/// <summary>
		/// Stores the paymentcode value
		/// </summary>
		public string PaymentCode;

		/// <summary>
		/// Stores the periodcovered value
		/// </summary>
		public string PeriodCovered;

		/// <summary>
		/// Stores the code value
		/// </summary>
		public string Code;

		/// <summary>
		/// Stores the dcn value
		/// </summary>
		public string DCN;

		/// <summary>
		/// Stores the patientaccountnumber value
		/// </summary>
		public string PatientAccountNumber;

		/// <summary>
		/// Stores the otherref1 value
		/// </summary>
		public string OtherRef1;

		/// <summary>
		/// Stores the otherref2 value
		/// </summary>
		public string OtherRef2;

		/// <summary>
		/// Stores the feeorcustomary value
		/// </summary>
		public string FeeOrCustomary;

		/// <summary>
		/// Stores the recommendedallowance value
		/// </summary>
		public string RecommendedAllowance;

		/// <summary>
		/// Stores the payeefullname2 value
		/// </summary>
		public string PayeeFullName2;

		/// <summary>
		/// Stores the payeefullnameaddr value
		/// </summary>
		public string PayeeFullNameAddr;

		/// <summary>
		/// Stores the employeeid value
		/// </summary>
		public string EmployeeID;

		/// <summary>
		/// Stores the controlnumber value
		/// </summary>
		public string ControlNumber;

		/// <summary>
		/// Stores the librarynumber value
		/// </summary>
		public string LibraryNumber;

		/// <summary>
		/// Stores the receiptdate value
		/// </summary>
		public string ReceiptDate;

		/// <summary>
		/// Stores the divisionname value
		/// </summary>
		public string DivisionName;

		/// <summary>
		/// Stores the divisionnameaddr value
		/// </summary>
		public string DivisionNameAddr;

		/// <summary>
		/// Stores the divisiontaxid value
		/// </summary>
		public string DivisionTaxID;

		/// <summary>
		/// Stores the codemessages value
		/// </summary>
		public string CodeMessages;

		/// <summary>
		/// Stores the reasoncodes value
		/// </summary>
		public string ReasonCodes;

		/// <summary>
		/// Stores the icncontrolnumber value
		/// </summary>
		public string ICNControlNumber;

		/// <summary>
		/// Stores the originvdate value
		/// </summary>
		public string OrigInvDate;

		/// <summary>
		/// Stores the originvuser value
		/// </summary>
		public string OrigInvUser;
	}
	#endregion

	#region "Struct CEOBDetailRecord"
	/// Name		: CEOBDetailRecord
	/// Author		: Anurag Agarwal
	/// Date Created	: 30 Dec 2004		
	/// ************************************************************
	/// Amendment History
	/// ************************************************************
	/// Date Amended   *   Amendment   *    Author
	///                *               *
	///                *               *
	/// ************************************************************
	/// <summary>
	/// Structure to hold the EOB Detail record 
	/// </summary>
	internal struct CEOBDetailRecord
	{
		/// <summary>
		/// Stores the invoicenumber value
		/// </summary>
		public string InvoiceNumber;

		/// <summary>
		/// Stores the invoicedate value
		/// </summary>
		public string InvoiceDate;

		/// <summary>
		/// Stores the feeschedule value
		/// </summary>
		public string FeeSchedule;

		/// <summary>
		/// Stores the proceduredate value
		/// </summary>
		public string ProcedureDate;

		/// <summary>
		/// Stores the proctodate value
		/// </summary>
		public string ProcToDate;

		/// <summary>
		/// Stores the procedure value
		/// </summary>
		public string Procedure;

		/// <summary>
		/// Stores the procedurecode value
		/// </summary>
		public string ProcedureCode;

		/// <summary>
		/// Stores the amountbilled value
		/// </summary>
		public double AmountBilled;

		/// <summary>
		/// Stores the amountpaid value
		/// </summary>
		public double AmountPaid;

		/// <summary>
		/// Stores the placeofservice value
		/// </summary>
		public string PlaceOfService;

		/// <summary>
		/// Stores the placeofservicecode value
		/// </summary>
		public string PlaceofServiceCode;

		/// <summary>
		/// Stores the typeofservice value
		/// </summary>
		public string TypeOfService;

		/// <summary>
		/// Stores the typeofservicecode value
		/// </summary>
		public string TypeOfServiceCode;

		/// <summary>
		/// Stores the transtype value
		/// </summary>
		public string TransType;

		/// <summary>
		/// Stores the transtypecode value
		/// </summary>
		public string TransTypeCode;

		/// <summary>
		/// Stores the unitsbilled value
		/// </summary>
		public double UnitsBilled;

		/// <summary>
		/// Stores the scheduledamount value
		/// </summary>
		public double ScheduledAmount;

		/// <summary>
		/// Stores the amountreduced value
		/// </summary>
		public double AmountReduced;

		/// <summary>
		/// Stores the amountsaved value
		/// </summary>
		public double AmountSaved;

		/// <summary>
		/// Stores the modifier value
		/// </summary>
		public string Modifier;

		/// <summary>
		/// Stores the modifiercode value
		/// </summary>
		public string ModifierCode;

		/// <summary>
		/// Stores the percentile value
		/// </summary>
		public string Percentile;

		/// <summary>
		/// Stores the contractamount value
		/// </summary>
		public double ContractAmount;

		/// <summary>
		/// Stores the discount value
		/// </summary>
		public double Discount;

		/// <summary>
		/// Stores the providerzipcode value
		/// </summary>
		public string ProviderZipCode;

		/// <summary>
		/// Stores the amountallowed value
		/// </summary>
		public double AmountAllowed;

		/// <summary>
		/// Stores the baseamount value
		/// </summary>
		public double BaseAmount;

		/// <summary>
		/// Stores the discountamount value
		/// </summary>
		public double DiscountAmount;

		/// <summary>
		/// Stores the perdiemamount value
		/// </summary>
		public double PerDiemAmount;

		/// <summary>
		/// Stores the stoplossamount value
		/// </summary>
		public double StopLossAmount;

		/// <summary>
		/// Stores the feetableamount value
		/// </summary>
		public double FeeTableAmount;

		/// <summary>
		/// Stores the eob List
		/// </summary>
		public Hashtable EOBList;

		/// <summary>
		/// Stores the modifier List
		/// </summary>
		public Hashtable ModifierList;

		/// <summary>
		/// Stores the modcode List
		/// </summary>
		public Hashtable ModCodeList;

		/// <summary>
		/// Stores the diagnosis list
		/// </summary>
		public Hashtable DiagnosisList;

		/// <summary>
		/// Stores the disconfeeschd value
		/// </summary>
		public double DiscOnFeeSchd;

		/// <summary>
		/// Stores the prescripno value
		/// </summary>
		public double PrescripNo;

		/// <summary>
		/// Stores the drugname value
		/// </summary>
		public double DrugName;

		/// <summary>
		/// Stores the prescripdate value
		/// </summary>
		public double PrescripDate;
	}
	#endregion

	#region "Struct DetailStringRecord"
	/// Name		: DetailStringRecord
	/// Author		: Anurag Agarwal
	/// Date Created	: 24 Dec 2004		
	/// ************************************************************
	/// Amendment History
	/// ************************************************************
	/// Date Amended   *   Amendment   *    Author
	///                *               *
	///                *               *
	/// ************************************************************
	/// <summary>
	/// Structure to hold the EOB record detail
	/// </summary>
	internal struct DetailStringRecord
	{
		/// <summary>
		/// Stores the Token value for EOB record as string
		/// </summary>
		public string Token;

		/// <summary>
		/// Stores the Alignment
		/// </summary>
		public string Alignment;

		/// <summary>
		/// Stores the Width
		/// </summary>
		public int Width;

		/// <summary>
		/// Stores the Datawidth
		/// </summary>
		public int Datawidth;
	}
	#endregion

	#region "Struct DetailDoubleRecord"
	/// Name		: DetailDoubleRecord
	/// Author		: Anurag Agarwal
	/// Date Created	: 24 Dec 2004		
	/// ************************************************************
	/// Amendment History
	/// ************************************************************
	/// Date Amended   *   Amendment   *    Author
	///                *               *
	///                *               *
	/// ************************************************************
	/// <summary>
	/// Structure to hold the EOB record detail
	/// </summary>
	internal struct DetailDoubleRecord
	{
		/// <summary>
		/// Stores the Token value for EOB record as double.
		/// </summary>
		public double Token;

		/// <summary>
		/// Stores the Alignment
		/// </summary>
		public string Alignment;

		/// <summary>
		/// Stores the Width
		/// </summary>
		public int Width;

		/// <summary>
		/// Stores the Datawidth
		/// </summary>
		public int Datawidth;
	}
	#endregion

	#region "Struct LoginInfo"
	/// Name		: LoginInfo
	/// Author		: Anurag Agarwal
	/// Date Created	: 27 Dec 2004		
	/// ************************************************************
	/// Amendment History
	/// ************************************************************
	/// Date Amended   *   Amendment   *    Author
	///                *               *
	///                *               *
	/// ************************************************************
	/// <summary>
	/// Structure to hold database Logged in User informtaion
	/// </summary>
	public struct LoginInfo
	{
		/// <summary>
		/// Stores the Username
		/// </summary>
		public string Username;

		/// <summary>
		/// Stores the DSN name
		/// </summary>
		public string DsnName;

		/// <summary>
		/// Stores the DSN Id
		/// </summary>
		public string DsnId;

	}
	#endregion

	#endregion

}
