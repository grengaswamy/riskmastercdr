using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.Application.PrintEOB
{	
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   22 December 2004
	///Purpose :   This is the class corresponding to the EOB Report (Active Report).
	///			   It contains methods related to the report layout, report fields initialization and populating them with data.
	/// </summary>
	internal class rptEOR : CommonEOB 
	{
        private int m_iClientId = 0;
		public rptEOR()
		{
			InitializeReport();
		}

        public rptEOR(string p_sConnectionString, string p_sUserName, int p_iClientId)
            : base(p_sConnectionString, p_sUserName, p_iClientId)
		{
			InitializeReport();
            m_iClientId = p_iClientId;
		}

		#region Methods
		/// <summary>
		/// This method would invoke the method of base class to initialize the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void rptEOR_DataInitialize(object p_objsender, System.EventArgs p_objeArgs)
		{
			base.DataInitializeBase(ref p_objsender,ref p_objeArgs);			
		}
		/// <summary>
		/// This method would invoke the method of base class to fill data in the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void rptEOR_FetchData(object p_objsender, FetchEventArgs p_objeArgs)
		{
			base.FetchDataBase(ref p_objsender,ref p_objeArgs);

			//for hospital bills, no CPT code is used so pop in trans type for description
			if(Fields["PROCEDURE"].Value.ToString() == string.Empty)
				Fields["PROCEDURE"].Value = Fields["TRANS_TYPE"].Value; 

			if(Fields["CODE"].Value.ToString() == string.Empty)
				Fields["CODE"].Value = Fields["PROCEDURE_CODE"].Value; 
    
		}
		/// <summary>
		/// This method would invoke the method of base class to do the page settings before start printing 
		/// the report and also to print the soft error log.		
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void rptEOR_ReportStart(object p_objsender, System.EventArgs p_objeArgs)
		{
			base.ReportStartBase(ref p_objsender,ref p_objeArgs);			
		}

		#endregion

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Shape Shape5 = null;
		private DataDynamics.ActiveReports.Label Label17 = null;
		private DataDynamics.ActiveReports.Label Label26 = null;
		private DataDynamics.ActiveReports.Label Label37 = null;
		private DataDynamics.ActiveReports.Label Label60 = null;
		private DataDynamics.ActiveReports.Label Label61 = null;
		private DataDynamics.ActiveReports.Label Label62 = null;
		private DataDynamics.ActiveReports.Label Label63 = null;
		private DataDynamics.ActiveReports.Label Label64 = null;
		private DataDynamics.ActiveReports.Label Label65 = null;
		private DataDynamics.ActiveReports.TextBox Field41 = null;
		private DataDynamics.ActiveReports.TextBox Field42 = null;
		private DataDynamics.ActiveReports.TextBox Field43 = null;
		private DataDynamics.ActiveReports.Label Label66 = null;
		private DataDynamics.ActiveReports.Label Label67 = null;
		private DataDynamics.ActiveReports.Label Label68 = null;
		private DataDynamics.ActiveReports.Label Label69 = null;
		private DataDynamics.ActiveReports.Label Label70 = null;
		private DataDynamics.ActiveReports.Label Label71 = null;
		private DataDynamics.ActiveReports.Label Label72 = null;
        private DataDynamics.ActiveReports.Label LabelDCN = null; //rsharma220 MITS 32303
		private DataDynamics.ActiveReports.Label Label73 = null;
		private DataDynamics.ActiveReports.Label Label74 = null;
		private DataDynamics.ActiveReports.Label Label76 = null;
		private DataDynamics.ActiveReports.Label Label77 = null;
		private DataDynamics.ActiveReports.Label Label5 = null;
		private DataDynamics.ActiveReports.Label Label78 = null;
		private DataDynamics.ActiveReports.Shape Shape30 = null;
		private DataDynamics.ActiveReports.Label Label79 = null;
		private DataDynamics.ActiveReports.Shape Shape31 = null;
		private DataDynamics.ActiveReports.Label Label80 = null;
		private DataDynamics.ActiveReports.Shape Shape32 = null;
		private DataDynamics.ActiveReports.Label Label81 = null;
		private DataDynamics.ActiveReports.Shape Shape33 = null;
		private DataDynamics.ActiveReports.Label Label82 = null;
		private DataDynamics.ActiveReports.Shape Shape34 = null;
		private DataDynamics.ActiveReports.Label Label83 = null;
		private DataDynamics.ActiveReports.Shape Shape35 = null;
		private DataDynamics.ActiveReports.Label Label84 = null;
		private DataDynamics.ActiveReports.TextBox Field59 = null;
		private DataDynamics.ActiveReports.TextBox Field60 = null;
		private DataDynamics.ActiveReports.TextBox Field64 = null;
		private DataDynamics.ActiveReports.TextBox Field65 = null;
		private DataDynamics.ActiveReports.TextBox Field66 = null;
		private DataDynamics.ActiveReports.TextBox Field67 = null;
		private DataDynamics.ActiveReports.TextBox Field68 = null;
		private DataDynamics.ActiveReports.TextBox Field69 = null;
		private DataDynamics.ActiveReports.TextBox Field76 = null;
		private DataDynamics.ActiveReports.TextBox Field77 = null;
		private DataDynamics.ActiveReports.TextBox Field78 = null;
        private DataDynamics.ActiveReports.TextBox Field79 = null; //rsharma220 MITS 32303
		private DataDynamics.ActiveReports.GroupHeader GroupHeader1 = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.TextBox Field26 = null;
		private DataDynamics.ActiveReports.TextBox Field27 = null;
		private DataDynamics.ActiveReports.TextBox Field29 = null;
		private DataDynamics.ActiveReports.TextBox Field30 = null;
		private DataDynamics.ActiveReports.TextBox Field31 = null;
		private DataDynamics.ActiveReports.TextBox Field32 = null;
		private DataDynamics.ActiveReports.TextBox Field33 = null;
		private DataDynamics.ActiveReports.TextBox Field34 = null;
		private DataDynamics.ActiveReports.Line Line2 = null;
		private DataDynamics.ActiveReports.Line Line3 = null;
		private DataDynamics.ActiveReports.Line Line4 = null;
		private DataDynamics.ActiveReports.Line Line6 = null;
		private DataDynamics.ActiveReports.Line Line7 = null;
		private DataDynamics.ActiveReports.Line Line8 = null;
		private DataDynamics.ActiveReports.Line Line9 = null;
		private DataDynamics.ActiveReports.Line Line10 = null;
		private DataDynamics.ActiveReports.Line Line11 = null;
		private DataDynamics.ActiveReports.Line Line23 = null;
		private DataDynamics.ActiveReports.Line Line33 = null;
		private DataDynamics.ActiveReports.GroupFooter GroupFooter1 = null;
		private DataDynamics.ActiveReports.Line Line24 = null;
		private DataDynamics.ActiveReports.Line Line25 = null;
		private DataDynamics.ActiveReports.Line Line26 = null;
		private DataDynamics.ActiveReports.Line Line27 = null;
		private DataDynamics.ActiveReports.Line Line28 = null;
		private DataDynamics.ActiveReports.Line Line29 = null;
		private DataDynamics.ActiveReports.Line Line30 = null;
		private DataDynamics.ActiveReports.Line Line31 = null;
		private DataDynamics.ActiveReports.Line Line32 = null;
		private DataDynamics.ActiveReports.Label Label91 = null;
		private DataDynamics.ActiveReports.TextBox lblTotalBilled = null;
		private DataDynamics.ActiveReports.TextBox lblTotalFee = null;
		private DataDynamics.ActiveReports.TextBox lblTotalRecommended = null;
		private DataDynamics.ActiveReports.Line Line22 = null;
		private DataDynamics.ActiveReports.TextBox Field75 = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		private DataDynamics.ActiveReports.Shape Shape36 = null;
		private DataDynamics.ActiveReports.Label Label85 = null;
		private DataDynamics.ActiveReports.Label Label86 = null;
		private DataDynamics.ActiveReports.Label Label87 = null;
		private DataDynamics.ActiveReports.Label Label88 = null;
		private DataDynamics.ActiveReports.Label Label89 = null;
		private DataDynamics.ActiveReports.TextBox Field58 = null;
		private DataDynamics.ActiveReports.Label Label90 = null;
		public void InitializeReport()
		{
			try
            {
                this.Document.Printer.PrinterName = "";
				this.LoadLayout(this.GetType(), "Riskmaster.Application.PrintEOB.rptEOR.rpx");
				this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
				this.GroupHeader1 = ((DataDynamics.ActiveReports.GroupHeader)(this.Sections["GroupHeader1"]));
				this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
				this.GroupFooter1 = ((DataDynamics.ActiveReports.GroupFooter)(this.Sections["GroupFooter1"]));
				this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
				this.Shape5 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[0]));
				this.Label17 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
				this.Label26 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
				this.Label37 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[3]));
				this.Label60 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[4]));
				this.Label61 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[5]));
				this.Label62 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[6]));
				this.Label63 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[7]));
				this.Label64 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[8]));
				this.Label65 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[9]));
				this.Field41 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[10]));
				this.Field42 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[11]));
				this.Field43 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[12]));
				this.Label66 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[13]));
				this.Label67 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[14]));
				this.Label68 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[15]));
				this.Label69 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[16]));
				this.Label70 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[17]));
				this.Label71 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[18]));
                //rsharma220 MITS 32303
                this.Label72 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[20]));
                this.LabelDCN = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[19]));
                this.Label73 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[21]));
				this.Label74 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[22]));
				this.Label76 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[23]));
				this.Label77 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[24]));
				this.Label5 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[25]));
				this.Label78 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[26]));
				this.Shape30 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[27]));
				this.Label79 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[28]));
				this.Shape31 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[29]));
				this.Label80 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[30]));
				this.Shape32 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[31]));
				this.Label81 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[32]));
				this.Shape33 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[33]));
				this.Label82 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[34]));
				this.Shape34 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[35]));
				this.Label83 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[36]));
				this.Shape35 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[37]));
				this.Label84 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[38]));
				this.Field59 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[39]));
				this.Field60 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[40]));
				this.Field64 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[41]));
				this.Field65 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[42]));
				this.Field66 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[43]));
				this.Field67 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[44]));
				this.Field68 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[45]));
				this.Field69 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[46]));
				this.Field76 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[47]));
				this.Field77 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[48]));
				this.Field78 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[49]));
                this.Field79 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[50]));

				this.Field26 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
				this.Field27 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
				this.Field29 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
				this.Field30 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[3]));
				this.Field31 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
				this.Field32 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[5]));
				this.Field33 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[6]));
				this.Field34 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[7]));
				this.Line2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[8]));
				this.Line3 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[9]));
				this.Line4 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[10]));
				this.Line6 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[11]));
				this.Line7 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[12]));
				this.Line8 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[13]));
				this.Line9 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[14]));
				this.Line10 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[15]));
				this.Line11 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[16]));
				this.Line23 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[17]));
				this.Line33 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[18]));
				this.Line24 = ((DataDynamics.ActiveReports.Line)(this.GroupFooter1.Controls[0]));
				this.Line25 = ((DataDynamics.ActiveReports.Line)(this.GroupFooter1.Controls[1]));
				this.Line26 = ((DataDynamics.ActiveReports.Line)(this.GroupFooter1.Controls[2]));
				this.Line27 = ((DataDynamics.ActiveReports.Line)(this.GroupFooter1.Controls[3]));
				this.Line28 = ((DataDynamics.ActiveReports.Line)(this.GroupFooter1.Controls[4]));
				this.Line29 = ((DataDynamics.ActiveReports.Line)(this.GroupFooter1.Controls[5]));
				this.Line30 = ((DataDynamics.ActiveReports.Line)(this.GroupFooter1.Controls[6]));
				this.Line31 = ((DataDynamics.ActiveReports.Line)(this.GroupFooter1.Controls[7]));
				this.Line32 = ((DataDynamics.ActiveReports.Line)(this.GroupFooter1.Controls[8]));
				this.Label91 = ((DataDynamics.ActiveReports.Label)(this.GroupFooter1.Controls[9]));
				this.lblTotalBilled = ((DataDynamics.ActiveReports.TextBox)(this.GroupFooter1.Controls[10]));
				this.lblTotalFee = ((DataDynamics.ActiveReports.TextBox)(this.GroupFooter1.Controls[11]));
				this.lblTotalRecommended = ((DataDynamics.ActiveReports.TextBox)(this.GroupFooter1.Controls[12]));
				this.Line22 = ((DataDynamics.ActiveReports.Line)(this.GroupFooter1.Controls[13]));
				this.Field75 = ((DataDynamics.ActiveReports.TextBox)(this.GroupFooter1.Controls[14]));
				this.Shape36 = ((DataDynamics.ActiveReports.Shape)(this.PageFooter.Controls[0]));
				this.Label85 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
				this.Label86 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[2]));
				this.Label87 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[3]));
				this.Label88 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[4]));
				this.Label89 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[5]));
				this.Field58 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[6]));
				this.Label90 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[7]));
				// Attach Report Events
				this.DataInitialize += new System.EventHandler(this.rptEOR_DataInitialize);
				this.FetchData += new FetchEventHandler(this.rptEOR_FetchData);
				this.ReportStart += new System.EventHandler(this.rptEOR_ReportStart);
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintEOBAR.InitializeReportEOB.Error", m_iClientId), p_objException);
			}
		}

		#endregion
	}
}
