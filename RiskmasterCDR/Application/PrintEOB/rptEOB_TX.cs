using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.Application.PrintEOB
{	
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   22 December 2004
	///Purpose :   This is the class corresponding to the EOB Report (Active Report).
	///			   It contains methods related to the report layout, report fields initialization and populating them with data.
	/// </summary>
	internal class rptEOB_TX : CommonEOB
	{
        private int m_iClientId = 0;

		public rptEOB_TX()
		{
			InitializeReport();
		}

		public rptEOB_TX(string p_sConnectionString, string p_sUserName, int p_iClientId):base(p_sConnectionString, p_sUserName,p_iClientId)
		{
			InitializeReport();
            m_iClientId = p_iClientId;
		}

		#region Methods
		/// <summary>
		/// This method would invoke the method of base class to initialize the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void rptEOB_TX_DataInitialize(object p_objsender, System.EventArgs p_objeArgs)
		{
			base.DataInitializeBase(ref p_objsender,ref p_objeArgs);			
		}
		/// <summary>
		/// This method would invoke the method of base class to fill data in the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void rptEOB_TX_FetchData(object p_objsender, FetchEventArgs p_objeArgs)
		{
			base.FetchDataBase(ref p_objsender,ref p_objeArgs);
    
		}
		/// <summary>
		/// This method would invoke the method of base class to do the page settings before start printing 
		/// the report and also to print the soft error log.		
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void rptEOB_TX_ReportStart(object p_objsender, System.EventArgs p_objeArgs)
		{
			base.ReportStartBase(ref p_objsender,ref p_objeArgs);			
		}

		#endregion

		#region ActiveReports Designer generated code

        //Geeta 12/07/07: MITS 10744 Code modified to fix the alignment of EOB title
        //and some other fields 
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
        private DataDynamics.ActiveReports.GroupFooter GroupFooter1 = null;
        private DataDynamics.ActiveReports.PageFooter PageFooter = null;
        private DataDynamics.ActiveReports.Detail Detail = null;
        private DataDynamics.ActiveReports.GroupHeader GroupHeader1 = null;
        private DataDynamics.ActiveReports.Shape Shape1 = null;
        private DataDynamics.ActiveReports.Shape Shape2 = null;
        private DataDynamics.ActiveReports.Shape Shape3 = null;
        private DataDynamics.ActiveReports.Shape Shape4 = null;
        private DataDynamics.ActiveReports.Shape Shape5 = null;
        private DataDynamics.ActiveReports.Shape Shape6 = null;
        private DataDynamics.ActiveReports.Label Label1 = null;
        private DataDynamics.ActiveReports.Label Label2 = null;
        private DataDynamics.ActiveReports.Label Label3 = null;
        private DataDynamics.ActiveReports.Label Label4 = null;
        private DataDynamics.ActiveReports.Label Label5 = null;
        private DataDynamics.ActiveReports.Label Label11 = null;
        private DataDynamics.ActiveReports.TextBox Field1 = null;
        private DataDynamics.ActiveReports.TextBox Field3 = null;
        private DataDynamics.ActiveReports.TextBox Field11 = null;
        private DataDynamics.ActiveReports.Label Label17 = null;
        private DataDynamics.ActiveReports.Label Label18 = null;
        private DataDynamics.ActiveReports.Label Label19 = null;
        private DataDynamics.ActiveReports.Label Label20 = null;
        private DataDynamics.ActiveReports.Shape Shape18 = null;
        private DataDynamics.ActiveReports.Label Label24 = null;
        private DataDynamics.ActiveReports.Label Label25 = null;
        private DataDynamics.ActiveReports.TextBox Field15 = null;
        private DataDynamics.ActiveReports.Label Label26 = null;
        private DataDynamics.ActiveReports.Shape Shape19 = null;
        private DataDynamics.ActiveReports.Shape Shape20 = null;
        private DataDynamics.ActiveReports.Label Label27 = null;
        private DataDynamics.ActiveReports.Label Label28 = null;
        private DataDynamics.ActiveReports.TextBox Field17 = null;
        private DataDynamics.ActiveReports.TextBox Field18 = null;
        private DataDynamics.ActiveReports.Shape Shape21 = null;
        private DataDynamics.ActiveReports.Label Label29 = null;
        private DataDynamics.ActiveReports.Label Label31 = null;
        private DataDynamics.ActiveReports.Shape Shape24 = null;
        private DataDynamics.ActiveReports.Label Label32 = null;
        private DataDynamics.ActiveReports.TextBox Field19 = null;
        private DataDynamics.ActiveReports.Shape Shape25 = null; 
        private DataDynamics.ActiveReports.Label Label33 = null;
        private DataDynamics.ActiveReports.TextBox Field20 = null;
        private DataDynamics.ActiveReports.TextBox Field21 = null;
        private DataDynamics.ActiveReports.Shape Shape26 = null;
        private DataDynamics.ActiveReports.Label Label34 = null;
        private DataDynamics.ActiveReports.TextBox Field22 = null;
        private DataDynamics.ActiveReports.Label Label42 = null;
        private DataDynamics.ActiveReports.Label Label43 = null;
        private DataDynamics.ActiveReports.Label Label44 = null;
        private DataDynamics.ActiveReports.Label Label46 = null;
        private DataDynamics.ActiveReports.Label Label47 = null;
        private DataDynamics.ActiveReports.Label Label48 = null;
        private DataDynamics.ActiveReports.Label Label49 = null;
        private DataDynamics.ActiveReports.Label Label50 = null;
        private DataDynamics.ActiveReports.TextBox Field37 = null;
        private DataDynamics.ActiveReports.TextBox Field40 = null;
        private DataDynamics.ActiveReports.Label Label59 = null;
        private DataDynamics.ActiveReports.TextBox Field41 = null;
        private DataDynamics.ActiveReports.Line  Line23 = null;
        private DataDynamics.ActiveReports.Line  Line1 = null;
        private DataDynamics.ActiveReports.Line  Line15 =null;
        private DataDynamics.ActiveReports.Label Label45 = null;
        private DataDynamics.ActiveReports.Line  Line12 = null;
        private DataDynamics.ActiveReports.Line  Line13 = null;
        private DataDynamics.ActiveReports.Line  Line14 = null;
        private DataDynamics.ActiveReports.Line  Line16 = null;
        private DataDynamics.ActiveReports.Line  Line17 = null;
        private DataDynamics.ActiveReports.Line  Line18 = null;
        private DataDynamics.ActiveReports.Line  Line19 =null;
        private DataDynamics.ActiveReports.Line Line20 = null;            
        private DataDynamics.ActiveReports.TextBox Field26 = null;
        private DataDynamics.ActiveReports.TextBox Field27 = null;
        private DataDynamics.ActiveReports.TextBox Field28 = null;
        private DataDynamics.ActiveReports.TextBox Field29 = null;
        private DataDynamics.ActiveReports.TextBox Field30 = null;
        private DataDynamics.ActiveReports.TextBox Field31 = null;
        private DataDynamics.ActiveReports.TextBox Field32 = null;
        private DataDynamics.ActiveReports.TextBox Field33 = null;
        private DataDynamics.ActiveReports.TextBox Field34 = null;
        private DataDynamics.ActiveReports.Line Line2 = null;
        private DataDynamics.ActiveReports.Line Line3 = null;
        private DataDynamics.ActiveReports.Line Line4 = null;
        private DataDynamics.ActiveReports.Line Line5 = null;
        private DataDynamics.ActiveReports.Line Line6 = null;
        private DataDynamics.ActiveReports.Line Line7 = null;
        private DataDynamics.ActiveReports.Line Line8 = null;
        private DataDynamics.ActiveReports.Line Line9 = null;
        private DataDynamics.ActiveReports.Line Line10 = null;
        private DataDynamics.ActiveReports.Line Line11 = null;
        private DataDynamics.ActiveReports.Line Line22 = null;
        private DataDynamics.ActiveReports.Label Label51 = null;
        private DataDynamics.ActiveReports.Label Label52 = null;
        private DataDynamics.ActiveReports.Label Label60 = null;
        private DataDynamics.ActiveReports.Label Label61 = null;


		public void InitializeReport()
		{
			try
            {
                this.Document.Printer.PrinterName = "";
                //Geeta 12/07/07: MITS 10744 Code modified to fix the alignment of EOB title
                //and some other fields 
                this.LoadLayout(this.GetType(), "Riskmaster.Application.PrintEOB.rptEOB_TX.rpx");
				this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
				this.GroupHeader1 = ((DataDynamics.ActiveReports.GroupHeader)(this.Sections["GroupHeader1"]));
				this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
				this.GroupFooter1 = ((DataDynamics.ActiveReports.GroupFooter)(this.Sections["GroupFooter1"]));
				this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
                this.Shape1 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[0]));
                this.Shape2 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[1]));
                this.Shape3 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[2]));
                this.Shape4 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[3]));
                this.Shape5 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[4]));
                this.Shape6 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[5]));
                this.Label1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[6]));
                this.Label2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[7]));
                this.Label3 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[8]));
                this.Label4 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[9]));
                this.Label5 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[10]));
                this.Label11 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[11]));
                this.Field1 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[12]));
                this.Field3 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[13]));
                this.Field11 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[14]));
                this.Label17 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[15]));
                this.Label18 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[16]));
                this.Label19 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[17]));
                this.Label20 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[18]));
                this.Shape18 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[19]));
                this.Label24 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[20]));
                this.Label25 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[21]));
                this.Field15 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[22]));
                this.Label26 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[23]));
                this.Shape19 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[24]));
                this.Shape20 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[25]));
                this.Label27 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[26]));
                this.Label28 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[27]));
                this.Field17 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[28]));
                this.Field18 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[29]));
                this.Shape21 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[30]));
                this.Label29 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[31]));
                this.Label31 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[32]));
                this.Shape24 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[33]));
                this.Label32 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[34]));
                this.Field19 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[35]));
                this.Shape25 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[36]));
                this.Label33 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[37]));
                this.Field20 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[38]));
                this.Field21 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[39]));
                this.Shape26 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[40]));
                this.Label34 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[41]));
                this.Field22 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[42]));
                this.Label42 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[43]));
                this.Label43 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[44]));
                this.Label44 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[45]));
                this.Label46 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[46]));
                this.Label47 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[47]));
                this.Label48 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[48]));
                this.Label49 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[49]));
                this.Label50 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[50]));
                this.Field37 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[51]));
                this.Field40 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[52]));
                this.Label59 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[53]));
                this.Field41 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[54]));
                this.Line23 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[55]));
                this.Line1 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[56]));
                this.Line15 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[57]));
                this.Label45 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[58]));
                this.Line12 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[59]));
                this.Line13 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[60]));
                this.Line14 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[61]));
                this.Line16 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[62]));
                this.Line17 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[63]));
                this.Line18 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[64]));
                this.Line19 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[65]));
                this.Line20 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[66]));                
                this.Field26 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
                this.Field27 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
                this.Field28 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
                this.Field29 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[3]));
                this.Field30 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
                this.Field31 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[5]));
                this.Field32 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[6]));
                this.Field33 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[7]));
                this.Field34 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[8]));
                this.Line2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[9]));                
                this.Line4 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[10]));
                this.Line5 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[11]));
                this.Line6 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[12]));
                this.Line7 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[13]));
                this.Line8 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[14]));
                this.Line9 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[15]));
                this.Line10 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[16]));
                this.Line11 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[17]));
                this.Line22 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[18]));
                this.Line3 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[19]));
                this.Label51 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[0]));
                this.Label52 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
                this.Label60 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[2]));
                this.Label61 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[3]));
            
				// Attach Report Events
				this.DataInitialize += new System.EventHandler(this.rptEOB_TX_DataInitialize);
				this.FetchData += new FetchEventHandler(this.rptEOB_TX_FetchData);
				this.ReportStart += new System.EventHandler(this.rptEOB_TX_ReportStart);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintEOBAR.InitializeReportEOB.Error",m_iClientId),p_objException);
			}
		}

		#endregion
	}
}

