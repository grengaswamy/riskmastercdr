using System;
using System.Collections ;

namespace Riskmaster.Application.TimeExpanse
{
	/// <summary>
	/// Author  :   Anurag Agarwal
	/// Dated   :   19 Nov 2004 
	/// Purpose :   Represents Invoice Details Item List.  
	/// </summary>
	internal class InvoiceDetails : CollectionBase
	{

		/// <summary>
		/// Default Constructor
		/// </summary>
		public InvoiceDetails()
		{
		}

		/// <summary>
		/// Add an item in the list.
		/// </summary>
		/// <param name="p_objInvoiceDetail">Financial Summary Item</param>
		/// <returns>Position on which the item is inserted.</returns>
		internal int Add(InvoiceDetail p_objInvoiceDetail)
		{
			return List.Add( p_objInvoiceDetail );			
		}		


		/// <summary>
		/// Remove the item form the list.
		/// </summary>
		/// <param name="p_objInvoiceDetail">Financial Summary Item</param>
		internal void Remove( InvoiceDetail p_objInvoiceDetail )
		{
			List.Remove( p_objInvoiceDetail );
		} 

		/// <summary>
		/// Check the existence of the item in the list.
		/// </summary>
		/// <param name="p_objInvoiceDetail">Financial Summary Item</param>
		/// <returns>Return the status of the item. "True" if item is in the list.</returns>
		internal bool Contains( InvoiceDetail p_objInvoiceDetail )
		{
			return List.Contains( p_objInvoiceDetail );
		}
		/// <summary>
		/// Get the index of the item in the list.
		/// </summary>
		/// <param name="p_objInvoiceDetail">Financial Summary Item</param>
		/// <returns>Return the integer index.</returns>
		internal int IndexOf( InvoiceDetail p_objInvoiceDetail )
		{
			return List.IndexOf( p_objInvoiceDetail );
		}

		/// <summary>
		/// Indexing for InvoiceDetail
		/// </summary>
		internal InvoiceDetail this[int p_iIndex]
		{
			get { return ( InvoiceDetail )List[p_iIndex]; }
			set { List[p_iIndex] = value; }
		}


	}
}
