using System;

namespace Riskmaster.Application.TimeExpanse
{
	/// <summary>
	/// Author  :   Anurag Agarwal
	/// Dated   :   19 Nov 2004 
	/// Purpose :   Represents a Invoice Item.  
	/// </summary>
	internal class Invoice
	{

		#region Variable Declaration

		/// <summary>
		/// Private variable for Invoice Number	
		/// </summary>
		private string m_sInvoiceNumber = string.Empty;

		/// <summary>
		/// Private variable for Invoice Date	
		/// </summary>
		private string m_sInvoiceDate = string.Empty;

		/// <summary>
		/// Private variable for Invoice Amount	
		/// </summary>
		private string m_sInvoiceAmount = string.Empty;

		/// <summary>
		/// Private variable for Bill Amount	
		/// </summary>
		private string m_sBillAmount = string.Empty;

		/// <summary>
		/// Private variable for Item Amount	
		/// </summary>
		private string m_sItemAmount = string.Empty;

		/// <summary>
		/// Private variable for Check Status	
		/// </summary>
		private string m_sCheckStatus = string.Empty;

		/// <summary>
		/// Private variable for Check Date
		/// </summary>
		private string m_sCheckDate = string.Empty;

		/// <summary>
		/// Private variable for Vendor Id	
		/// </summary>
		private long m_lVendorId = 0;

		/// <summary>
		/// Private variable for Vandor Name	
		/// </summary>
		private string m_sVendorName = string.Empty;

		/// <summary>
		/// Private variable for Row Id	
		/// </summary>
		private int m_iId = 0;

		/// <summary>
		/// Private variable for Posted	
		/// </summary>
		private bool m_bPosted = false;

		/// <summary>
		/// Private variable for Invoice Trans Id	
		/// </summary>
		private long m_lInvTransId = 0;

		/// <summary>
		/// Private variable for Posted Id	
		/// </summary>
		private long m_lPostedId = 0;

		/// <summary>
		/// Private variable for Allocated	
		/// </summary>
		private bool m_bAllocated = false;

		/// <summary>
		/// Private variable for Claim Number
		/// </summary>
		private string m_sClaimNumber = string.Empty;

		/// <summary>
		/// Private variable for Claim ID
		/// </summary>
		private int m_iClaimId = 0;

		#endregion

		#region Constructors

		/// <summary>
		/// Default Constructor
		/// </summary>
		public Invoice()
		{
		}

		#endregion

		#region Properties
		
		/// <summary>
		/// Read/Write property for Claim Id.
		/// </summary>
		internal int ClaimId  
		{
			get
			{
				return m_iClaimId;
			}
			set
			{
				m_iClaimId = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Claim Number.
		/// </summary>
		internal string ClaimNumber  
		{
			get
			{
				return m_sClaimNumber;
			}
			set
			{
				m_sClaimNumber = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Allocated.
		/// </summary>
		internal bool Allocated  
		{
			get
			{
				return m_bAllocated;
			}
			set
			{
				m_bAllocated = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Row Id.
		/// </summary>
		internal int Id  
		{
			get
			{
				return m_iId;
			}
			set
			{
				m_iId = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Vendor Name.
		/// </summary>
		internal string VendorName  
		{
			get
			{
				return m_sVendorName;
			}
			set
			{
				m_sVendorName = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Vendor Id.
		/// </summary>
		internal long VendorId  
		{
			get
			{
				return m_lVendorId;
			}
			set
			{
				m_lVendorId = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Check Date.
		/// </summary>
		internal string CheckDate  
		{
			get
			{
				return m_sCheckDate;
			}
			set
			{
				m_sCheckDate = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Check Status.
		/// </summary>
		internal string CheckStatus  
		{
			get
			{
				return m_sCheckStatus;
			}
			set
			{
				m_sCheckStatus = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Bill Amount.
		/// </summary>
		internal string BillAmount  
		{
			get
			{
				return m_sBillAmount;
			}
			set
			{
				m_sBillAmount = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Total Item Amount.
		/// </summary>
		internal string ItemAmount  
		{
			get
			{
				return m_sItemAmount;
			}
			set
			{
				m_sItemAmount = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Invoice Amount.
		/// </summary>
		internal string InvoiceAmount  
		{
			get
			{
				return m_sInvoiceAmount;
			}
			set
			{
				m_sInvoiceAmount = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Invoice Date.
		/// </summary>
		internal string InvoiceDate  
		{
			get
			{
				return m_sInvoiceDate;
			}
			set
			{
				m_sInvoiceDate = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Invoice Number.
		/// </summary>
		internal string InvoiceNumber  
		{
			get
			{
				return m_sInvoiceNumber;
			}
			set
			{
				m_sInvoiceNumber = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Posted.
		/// </summary>
		internal bool Posted  
		{
			get
			{
				return m_bPosted;
			}
			set
			{
				m_bPosted = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Posted Id.
		/// </summary>
		internal long PostedId  
		{
			get
			{
				return m_lPostedId;
			}
			set
			{
				m_lPostedId = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Invoice Trans Id.
		/// </summary>
		internal long InvTransId  
		{
			get
			{
				return m_lInvTransId;
			}
			set
			{
				m_lInvTransId = value ;
			}
		}	

		#endregion
	}
}
