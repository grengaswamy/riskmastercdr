using System;

namespace Riskmaster.Application.TimeExpanse
{
	/// <summary>
	/// Author  :   Anurag Agarwal
	/// Dated   :   19 Nov 2004 
	/// Purpose :   Represents a Rate Table Item.  
	/// </summary>
	internal class RateTable
	{

		#region Constructors

		/// <summary>
		/// Default Constructor
		/// </summary>
		public RateTable()
		{
		
		}

		#endregion

		#region Variable Declaration

		/// <summary>
		/// Private variable for Rate table Id	
		/// </summary>
		private int m_iId = 0;

		/// <summary>
		/// Private variable for Customer Name
		/// </summary>
		private string m_sCustomerName = string.Empty;

		/// <summary>
		/// Private variable for Table Name
		/// </summary>
		private string m_sTableName = string.Empty;

		/// <summary>
		/// Private variable for Customer Id	
		/// </summary>
		private long m_lCustomerId = 0;

		/// <summary>
		/// Private variable for Effective date
		/// </summary>
		private string m_sEffectiveDate = string.Empty;

		/// <summary>
		/// Private variable for Expiration date
		/// </summary>
		private string m_sExpirationDate = string.Empty;

		#endregion

		#region Properties
		
		/// <summary>
		/// Read/Write property for Customer Name.
		/// </summary>
		internal string CustomerName  
		{
			get
			{
				return m_sCustomerName;
			}
			set
			{
				m_sCustomerName = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Customer Name.
		/// </summary>
		internal string ExpirationDate  
		{
			get
			{
				return m_sExpirationDate;
			}
			set
			{
				m_sExpirationDate = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Customer Name.
		/// </summary>
		internal string EffectiveDate  
		{
			get
			{
				return m_sEffectiveDate;
			}
			set
			{
				m_sEffectiveDate = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Customer Id.
		/// </summary>
		internal long CustomerId  
		{
			get
			{
				return m_lCustomerId;
			}
			set
			{
				m_lCustomerId = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Table Name.
		/// </summary>
		internal string TableName  
		{
			get
			{
				return m_sTableName;
			}
			set
			{
				m_sTableName = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Rate Table ID.
		/// </summary>
		internal int Id  
		{
			get
			{
				return m_iId;
			}
			set
			{
				m_iId = value ;
			}
		}

		#endregion

	}
}
