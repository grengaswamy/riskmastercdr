using System;
using System.Xml;
using System.Globalization;
using System.Resources;
using System.Collections;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;

namespace Riskmaster.Application.MMIHistorySummary
{
	/// <summary>
	/// Summary description for MMIHistorySummary.
	/// </summary>
	public class MMIHistorySummary : IDisposable
	{
		#region "Constructor & Destructor"

		#region Constructor
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		/// <param name="p_iUserId">User Id</param>
		/// <param name="p_iUserGroupId">User Group Id</param>		
        public MMIHistorySummary(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iUserId, int p_iUserGroupId, int p_iClientId)
		{		
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			m_iUserId = p_iUserId ;
			m_iGroupId = p_iUserGroupId ;
            m_iClientId = p_iClientId;
			this.Initialize();
		}		
		#endregion

		#region Destructor
		/// <summary>
		/// Destructor
		/// </summary>
		~MMIHistorySummary()
		{
            //if( m_objDataModelFactory != null )
            //{
            //    m_objDataModelFactory.UnInitialize();
            //    m_objDataModelFactory = null ;				
            //}
            Dispose();
			m_objXmlDocument = null;	
		}
		#endregion

		#endregion

		#region "Member Variables"

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;

		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;

		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;

		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;

		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = "";
		
		/// <summary>
		///Represents the login name of the Logged in User
		/// </summary>
		private string m_sLoginName = "";

		/// <summary>
		/// Represents the User Id.
		/// </summary>
		private int m_iUserId = 0;

		/// <summary>
		/// The Group id 
		/// </summary>
		private int m_iGroupId = 0;

		/// <summary>
		/// XML Document
		/// </summary>
		private XmlDocument m_objXmlDocument = null;

        private int m_iClientId = 0;
	
		#endregion

		#region Property declaration

		/// <summary>
		/// Gets & sets the user id
		/// </summary>
		public int UserId{get{return m_iUserId ;}set{m_iUserId = value;}} 


		/// <summary>
		/// Set the Login Name
		/// </summary>
		public string LoginName {get{return m_sLoginName;}set{m_sLoginName=value;}}


		/// <summary>
		/// Set the Group Id Property
		/// </summary>
		public int GroupId{get{return m_iGroupId;}set{m_iGroupId=value;}}


		#endregion

		#region "Public Function"

		#region "SaveMMIHistory(int p_iClaimId,int p_iEmployeeId, XmlDocument p_objXmlDoc)"
		/// Name		: SaveMMIHistory
		/// Author		: Mohit Yadav
		/// Date Created	: 17 January 2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Saves the data
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <param name="p_iEmployeeId">Employee Id</param>
		/// <param name="p_objXmlDoc">Xml Document</param>
		/// <returns>Returns XML containing required data to print.</returns>
		public XmlDocument SaveMMIHistory(int p_iClaimId,int p_iEmployeeId,int p_iPiRowId, XmlDocument p_objXmlDoc)//Neha Added to save MMidate from person involved
		{
			
			//XmlElement objRootElement = null;
			//XmlElement objXmlNode = null;
			XmlDocument objXmlDocument = null;
			
			//string sReturnXML = string.Empty; 
			string sSQL = string.Empty;
			XmlElement objElement=null;					//used for parsing the input xml

			string sMMIDate = string.Empty;
			string sExamDate = string.Empty;
			string sChangedBy = string.Empty;
			string sApprovedBy = string.Empty;
			double dDisability = 0;
			string sReason = string.Empty;
			int iPhyEid = 0;
			int iNextUID = 0;

			//string sMaxDate = string.Empty;

			Claim objClaim = null;
			int iEventId = 0;
			int iPIRowId = 0;

			DbConnection objConn=null;
            DbReader objReaderPI = null;

			bool bReturn = false;

			try
			{
				//if(p_iClaimId == 0)
					//bReturn = false;
                if (p_iClaimId == 0 && p_iPiRowId == 0)//Neha changed for case mangtment at pi level
                    bReturn = false;

				if(p_iEmployeeId == 0)
					bReturn = false;

				if(!bReturn)
				{
                    if (p_iClaimId > 0)//Neha changed for case mangtment at pi level
                    {
                        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(p_iClaimId);

                        iEventId = objClaim.EventId;

                        //DbReader objReaderPI = null;
                        //DbReader objReaderPiXMmiHist = null;

                        sSQL = "SELECT * FROM PERSON_INVOLVED WHERE PERSON_INVOLVED.EVENT_ID = " + iEventId +
                            "AND PERSON_INVOLVED.PI_EID = " + p_iEmployeeId;

                        objReaderPI = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        if (objReaderPI != null)
                        {
                            if (objReaderPI.Read())
                            {
                                iPIRowId = Convert.ToInt32(objReaderPI["PI_ROW_ID"].ToString());
                            }
                        }
                    }
                    else if (p_iPiRowId > 0)//Neha changed for case mangtment at pi level
                        iPIRowId = p_iPiRowId;//Neha changed for case mangtment at pi level

					objElement=(XmlElement)p_objXmlDoc.SelectSingleNode("//MMIDate");
					sMMIDate=objElement.InnerText;

					objElement=(XmlElement)p_objXmlDoc.SelectSingleNode("//DateExam");
					sExamDate=objElement.InnerText;

					//objElement=(XmlElement)p_objXmlDoc.SelectSingleNode("//ChangedBy");
					sChangedBy=this.m_sUserName;

					objElement=(XmlElement)p_objXmlDoc.SelectSingleNode("//ApprovedBy");
					sApprovedBy=objElement.InnerText;

					objElement=(XmlElement)p_objXmlDoc.SelectSingleNode("//Disability");
					dDisability=Conversion.ConvertStrToDouble(objElement.InnerText);

					objElement=(XmlElement)p_objXmlDoc.SelectSingleNode("//Reason");
					sReason=objElement.InnerText;

					objElement=(XmlElement)p_objXmlDoc.SelectSingleNode("//Physician_cid");
					iPhyEid=Conversion.ConvertStrToInteger(objElement.InnerText);

					objConn = DbFactory.GetDbConnection(m_sConnectionString);
					objConn.Open();

					if (sMMIDate!="")
					{
                        iNextUID = Utilities.GetNextUID(m_sConnectionString, "PI_X_MMI_HIST", m_iClientId);
						sSQL="INSERT INTO PI_X_MMI_HIST (PI_X_MMIROW_ID, PI_ROW_ID, MMI_DATE, EXAM_DATE, PERCENT_DISABILITY, PHYSICIAN_EID, ADDED_BY_USER, APPROVED_BY_USER, REASON, DTTM_RCD_ADDED)"+
							" VALUES("+iNextUID.ToString()+",'"+iPIRowId+"','"+Conversion.GetDate(sMMIDate)+"','"+Conversion.GetDate(sExamDate)+"','"+dDisability+"','"+iPhyEid+"','"+sChangedBy+"','"+sApprovedBy+"','"+sReason+"','"+Conversion.GetDate(System.DateTime.Today.ToShortDateString())+"')";

					}
					objConn.ExecuteNonQuery(sSQL);
				}

				objXmlDocument = new XmlDocument();

				//objXmlDocument = this.LoadListMMIHistory(p_iClaimId,p_iEmployeeId);

				return objXmlDocument;

			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("MMIHistorySummary.SaveMMIHistory.ErrorSummary", m_iClientId),p_objException);
			}
			finally
			{
				//objXmlDocument = null;
				//objRootElement = null;
				if (objClaim !=null) objClaim.Dispose();
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                if (objReaderPI != null)
                    objReaderPI.Dispose();
			} 
		}
		#endregion

		#region "LoadListMMIHistory(int p_iClaimId,int p_iEmployeeId)"
		/// Name		: LoadListMMIHistory
		/// Author		: Mohit Yadav
		/// Date Created	: 8 September 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// fetches the data in XML format
		/// </summary>
		/// <param name="p_iClaimId">ClaimId</param>
		/// <param name="p_iEmployeeId">ClaimId</param>
		/// <returns>Returns XML containing required data to print.</returns>
		public XmlDocument LoadListMMIHistory(int p_iClaimId,int p_iEmployeeId,int p_iPiRowId)
		{
			
			XmlElement objRootElement = null;
			XmlElement objXmlNode = null;
			XmlDocument objXmlDocument = null;
			
			string sReturnXML = string.Empty; 
			string sSQL = string.Empty;

			string sMaxDate = string.Empty;

			Claim objClaim = null;
			int iEventId = 0;
			int iPIRowId = 0;
            DbReader objReaderPI = null;
            DbReader objReaderPiXMmiHist = null;

			try
			{
				objXmlDocument = new XmlDocument();

				if(p_iClaimId == 0 && p_iPiRowId == 0)//changed for case mangtment at pi level
					return objXmlDocument;

				if(p_iEmployeeId == 0)
					return objXmlDocument;
                if (p_iClaimId > 0)
                {
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(p_iClaimId);

                    iEventId = objClaim.EventId;

                    //DbReader objReaderPI = null;
                    //DbReader objReaderPiXMmiHist = null;

                    sSQL = "SELECT * FROM PERSON_INVOLVED WHERE PERSON_INVOLVED.EVENT_ID = " + iEventId +
                        "AND PERSON_INVOLVED.PI_EID = " + p_iEmployeeId;
                    objReaderPI = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                }
                else if (p_iPiRowId > 0)//neha start
                {
                    sSQL = "SELECT * FROM PERSON_INVOLVED WHERE PERSON_INVOLVED.PI_ROW_ID = " + p_iPiRowId +
                       "AND PERSON_INVOLVED.PI_EID = " + p_iEmployeeId;
                    objReaderPI = DbFactory.GetDbReader(m_sConnectionString, sSQL);
 
                }//neh aend
				if(objReaderPI != null)
				{
					objRootElement = objXmlDocument.CreateElement("MMIHistory");
					objRootElement.SetAttribute("username",this.m_sUserName);
                    if (p_iClaimId > 0)//Neha
                    {
                        objRootElement.SetAttribute("claimnumber", objClaim.ClaimNumber.ToString());
                        objRootElement.SetAttribute("claimid", objClaim.ClaimId.ToString());
                    }
                    else//Neha
                    {
                        objRootElement.SetAttribute("claimnumber", "");
                    }
					if(objReaderPI.Read())
					{
						iPIRowId = Convert.ToInt32(objReaderPI["PI_ROW_ID"].ToString());
						sMaxDate = GetMaxDate(Convert.ToInt32(iPIRowId));
						objRootElement.SetAttribute("maxdate",sMaxDate);
						objRootElement.SetAttribute("pirowid",iPIRowId.ToString());
						objXmlDocument.AppendChild(objRootElement);

						sSQL = "SELECT * FROM PI_X_MMI_HIST WHERE PI_X_MMI_HIST.PI_ROW_ID = " + iPIRowId +
							//sgoel6 MITS 14887 04/27/2009
                            " AND (DELETED_FLAG <> -1 OR DELETED_FLAG IS NULL) " +
                            "ORDER BY PI_X_MMIROW_ID ASC";
						objReaderPiXMmiHist=DbFactory.GetDbReader(m_sConnectionString,sSQL);
						if(objReaderPiXMmiHist != null)
						{
							while(objReaderPiXMmiHist.Read())
							{
								objXmlNode = objXmlDocument.CreateElement("data");
								objXmlNode.SetAttribute("MMIRowid", objReaderPiXMmiHist["PI_X_MMIROW_ID"].ToString());
								objXmlNode.SetAttribute("PIRowid",objReaderPiXMmiHist["PI_ROW_ID"].ToString());
								objXmlNode.SetAttribute("MMIDate",objReaderPiXMmiHist["MMI_DATE"].ToString());
								objXmlNode.SetAttribute("ExamDate",objReaderPiXMmiHist["EXAM_DATE"].ToString());
								objXmlNode.SetAttribute("Disability",objReaderPiXMmiHist["PERCENT_DISABILITY"].ToString());
								objXmlNode.SetAttribute("PhysicianEid",objReaderPiXMmiHist["PHYSICIAN_EID"].ToString());
								objXmlNode.SetAttribute("Physician",GetEntity(Convert.ToInt32(objReaderPiXMmiHist["PHYSICIAN_EID"].ToString())));
								objXmlNode.SetAttribute("AddedByUser",objReaderPiXMmiHist["ADDED_BY_USER"].ToString());
								objXmlNode.SetAttribute("ApprovedByUser",objReaderPiXMmiHist["APPROVED_BY_USER"].ToString());
								objXmlNode.SetAttribute("Reason",objReaderPiXMmiHist["REASON"].ToString());
								objRootElement.AppendChild(objXmlNode);
							}							
						}
					}
					else
					{
						objXmlDocument.AppendChild(objRootElement);
					}
				}

                //objReaderPiXMmiHist = null;
                //objReaderPI = null;

				return objXmlDocument;

			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("MMIHistorySummary.LoadListMMIHistory.ErrorSummary", m_iClientId), p_objException);
			}
			finally
			{
				objXmlDocument = null;
				objRootElement = null;
				if (objClaim !=null) objClaim.Dispose();
                if (objReaderPiXMmiHist != null)
                    objReaderPiXMmiHist.Dispose();
                if (objReaderPI != null)
                    objReaderPI.Dispose();
			} 
		}
		#endregion

        public void Dispose()
        {
            if (m_objDataModelFactory != null)

                m_objDataModelFactory.Dispose();
        }
				
        #region DeleteMMIHistory(string p_sMMIRowId, XmlDocument p_objXmlDoc)
        /// Name		: DeleteMMIHistory
        /// Author		: Sameer Goel
        /// Date Created	: 27 April 2009
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Deletes the selected MMI Dates
        /// </summary>
        /// <param name="p_sMMIRowId"></param>
        /// <param name="p_objXmlDoc"></param>
        /// <returns></returns>
        public XmlDocument DeleteMMIHistory(string p_sMMIRowId, XmlDocument p_objXmlDoc)
        {
            XmlDocument objXmlDocument = new XmlDocument();
            
            string sSQL = string.Empty;
            string sMMIDate = string.Empty;
            XmlElement objElement = null;					//used for parsing the input xml
            
            DbConnection objConn = null;
            DbReader objReaderPI = null;

            bool bReturn = false;

            try
            {
                if (p_sMMIRowId == null || p_sMMIRowId == "")
                    bReturn = false;

                if (!bReturn)
                {
                    //Open Connection
                    objConn = DbFactory.GetDbConnection(m_sConnectionString);
                    objConn.Open();

                    //Construct the update query for soft delete
                    sSQL = "UPDATE PI_X_MMI_HIST SET DELETED_FLAG = -1 WHERE PI_X_MMIROW_ID IN (";
                    sSQL = sSQL + p_sMMIRowId;
                    sSQL = sSQL + ")";
                    //Execute the update query
                    objConn.ExecuteNonQuery(sSQL);

                    //Construct the select query to get the latest MMI Date Entry
                    sSQL = "SELECT MMI_DATE FROM PI_X_MMI_HIST WHERE PI_ROW_ID IN ";
                    sSQL = sSQL + "( SELECT PI_ROW_ID FROM PI_X_MMI_HIST WHERE PI_X_MMIROW_ID IN ( ";
                    sSQL = sSQL + p_sMMIRowId + ")) AND (DELETED_FLAG IS NULL OR DELETED_FLAG <> -1) ";
                    sSQL = sSQL + "ORDER BY PI_X_MMIROW_ID DESC";
                    //Execute the select query
                    sMMIDate = Convert.ToString(objConn.ExecuteScalar(sSQL));                    
                }

                objElement = objXmlDocument.CreateElement("MMIDate");
                objElement.SetAttribute("value", sMMIDate);
                objXmlDocument.AppendChild(objElement);

                return objXmlDocument;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MMIHistorySummary.DeleteMMIHistory.ErrorSummary", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State.Equals(ConnectionState.Open))
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }                
            }
        }
        #endregion
        #endregion

		#region "Private Functions"

		#region "Initialize"
		/// Name		: Initialize
		/// Author		: Mohit Yadav
		/// Date Created	: 7 September 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Initialize the DataModal factory class object and Connection string;
		/// </summary>
		private void Initialize()
		{
			try
			{
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
				m_objXmlDocument = new XmlDocument(); 
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("MMIHistorySummary.Initialize.ErrorInit", m_iClientId), p_objEx);				
			}			
		}	
		#endregion

		#region "GetEntity(int p_iCode)"
		/// Name		: GetEntity
		/// Author		: Mohit Yadav
		/// Date Created	: 9 September 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Fetches the FirstName and LastName for the EmployeeId passed and return a string
		/// after concanate the strings.
		/// </summary>
		/// <param name="p_iCode">Employee ID</param>
		/// <returns>String containing FirstName and LastName</returns>
		private string GetEntity(int p_iCode)
		{
			string sFirstName="";
			string sLastName="";
			string sName="";

			Entity objEntity = null;

			try
			{
				objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity",false);
				objEntity.MoveTo(p_iCode);
			{
				sFirstName = objEntity.FirstName;
				sLastName = objEntity.LastName;
				if (sLastName!="" && sFirstName!="")
					sName = sLastName + "," + sFirstName;
				else if (sLastName!="")
					sName = sLastName;
				else if (sFirstName!="")
					sName = sFirstName;
			}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("MMIHistorySummary.GetEntity.ErrorGet", m_iClientId), p_objException);
			}
			finally
			{
				if(objEntity != null)
					objEntity.Dispose();
			}

			return sName.Trim();
		}
		#endregion

		#region "GetMaxDate(int p_iCode)"
		/// Name		: GetMaxDate
		/// Author		: Mohit Yadav
		/// Date Created	: 12 September 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Fetches the Maximum date for the PIRowID passed and return a string
		/// </summary>
		/// <param name="p_iCode">Employee ID</param>
		/// <returns>String containing Maximum date</returns>
		private string GetMaxDate(int p_iCode)
		{
			string sMaxDate="";
			string sSQL = string.Empty;
			DbReader objReaderPiXMmiHist = null;

			try
			{
				sSQL = "SELECT Max(MMI_DATE) FROM PI_X_MMI_HIST WHERE PI_X_MMI_HIST.PI_ROW_ID = " + p_iCode;
                //sgoel6 MITS 04/28/2009
                sSQL = sSQL + " AND (DELETED_FLAG IS NULL OR DELETED_FLAG <> -1) ";                
				objReaderPiXMmiHist=DbFactory.GetDbReader(m_sConnectionString,sSQL);
				if(objReaderPiXMmiHist != null)
				{
					while(objReaderPiXMmiHist.Read())
					{
						sMaxDate = objReaderPiXMmiHist[0].ToString();
					}
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("MMIHistorySummary.GetMaxDate.ErrorGet", m_iClientId), p_objException);
			}
			finally
			{
				if(objReaderPiXMmiHist != null)
					objReaderPiXMmiHist.Dispose();
			}

			return sMaxDate.Trim();
		}
		#endregion

		#endregion
	}
}
