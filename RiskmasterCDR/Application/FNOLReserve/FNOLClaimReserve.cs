﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml;
using System.Collections;
using Riskmaster.Application.Reserves;
using Riskmaster.Security;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;
namespace Riskmaster.Application.FNOLReserve
{
    ///************************************************************** 
    ///* $File				: FNOLClaimReserve.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 02-15-2013
    ///* $Author			: Sonam Pahariya
    ///***************************************************************	
    /// <summary>	
    ///	FNOLClaimReserve definition 
    /// </summary>
    public class FNOLClaimReserve
    {
        #region Variable Declaration
        /// <summary>
        /// Connection String
        /// </summary>
        private string m_sConnectString = string.Empty;
        private int m_iClientId = 0;//dvatsa
        protected UserLogin m_userLogin = null;
        private DataModelFactory m_objDataModelFactory = null;
        #endregion
        #region Constructors
        /// <summary>
        ///	Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sConnectString">Connection string</param>
        public FNOLClaimReserve(string p_sConnectString, UserLogin p_UserLogin,int p_iClientId)//dvatsa
        {
            m_sConnectString = p_sConnectString;
            m_userLogin = p_UserLogin;
            m_iClientId = p_iClientId;
        }
        #endregion

        /// <summary>
        /// Gets Grid data for FNOL claim reserves
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns>xmldocument</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlElement objElement = null;
            string sSQL = string.Empty;            
            DbReader objRead = null;
            DbReader objReader = null;
            DbReader objReadUnit = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            LocalCache objLocalCache = null;
            string sClaimantName = string.Empty;
            string sUnitID = string.Empty;
            int iUnitRowID = 0;
            int iPolCvgRowId = 0;
            string sUnitType = string.Empty;
            bool bSuccess = false;
            int iCoverageType = 0;
            string sClaimId = string.Empty;
            string sCovText = string.Empty;
            try
            {
                sClaimId = p_objXmlDocument.SelectSingleNode("//ClaimID").InnerText;
                objLocalCache = new LocalCache(m_sConnectString,m_iClientId);//dvatsa
                sSQL = "SELECT * FROM FNOL_RESERVE WHERE CLAIM_ID = " + sClaimId;
                objRead = DbFactory.GetDbReader(m_sConnectString, sSQL);

                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//FNOLResList");
                m_objDataModelFactory = new DataModelFactory(m_userLogin,m_iClientId);//dvatsa
                while (objRead.Read())
                {
                    objLstRow = p_objXmlDocument.CreateElement("listrow");
                    
                    objRowTxt = p_objXmlDocument.CreateElement("RowId");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("FNOL_RESERVE_ROW_ID"));
                    objLstRow.AppendChild(objRowTxt);

                    objRowTxt = p_objXmlDocument.CreateElement("Claimant");
                    objRowTxt.InnerText = objLocalCache.GetEntityLastFirstName(Conversion.CastToType<int>(Conversion.ConvertObjToStr(objRead.GetValue("CLAIMANT_EID")), out bSuccess));
                    objLstRow.AppendChild(objRowTxt);

                    objRowTxt = p_objXmlDocument.CreateElement("Policy");
                    sSQL = "";
                    sSQL = "SELECT POLICY_NAME FROM POLICY WHERE POLICY_ID = '" + Conversion.ConvertObjToStr(objRead.GetValue("POLICY_ID")) + "'";
                    objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                    while (objReader.Read())
                    {
                        objRowTxt.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("POLICY_NAME"));
                    }                                  
                    objLstRow.AppendChild(objRowTxt);
                    objReader.Close();

                    objRowTxt = p_objXmlDocument.CreateElement("Unit");
                    sSQL = "";
                    iUnitRowID = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objRead.GetValue("UNIT_ID")), out bSuccess);                    
                    sSQL = "SELECT UNIT_ID, UNIT_TYPE FROM POLICY_X_UNIT WHERE POLICY_UNIT_ROW_ID= '" + Conversion.ConvertObjToStr(objRead.GetValue("UNIT_ID")) + "'";
                    objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                    while (objReader.Read())
                    {
                        sUnitID = Conversion.ConvertObjToStr(objReader.GetValue("UNIT_ID"));
                        sUnitType = Conversion.ConvertObjToStr(objReader.GetValue("UNIT_TYPE"));
                        if(string.Compare(sUnitType, "V", true) == 0)
                        {
                            sSQL = "";
                            sSQL = "SELECT VIN FROM VEHICLE WHERE UNIT_ID = '" + sUnitID + "'";
                            objReadUnit = DbFactory.GetDbReader(m_sConnectString, sSQL);
                            while (objReadUnit.Read())
                            {
                                objRowTxt.InnerText = Conversion.ConvertObjToStr(objReadUnit.GetValue("VIN"));
                            }
                            objReadUnit.Close();
                        }
                        else if(string.Compare(sUnitType, "P", true) == 0)
                        {
                            sSQL = "";
                            sSQL = "SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = '" + sUnitID + "'";
                            objReadUnit = DbFactory.GetDbReader(m_sConnectString, sSQL);
                            while (objReadUnit.Read())
                            {
                                objRowTxt.InnerText = Conversion.ConvertObjToStr(objReadUnit.GetValue("PIN"));
                            }
                            objReadUnit.Close();
                        }
                            //MITS 30123 - start
                        else if (string.Compare(sUnitType, "s", true) == 0)
                        {
                            sSQL = "";
                            sSQL = "SELECT NAME FROM SITE_UNIT WHERE SITE_ID = '" + sUnitID + "'";
                            objReadUnit = DbFactory.GetDbReader(m_sConnectString, sSQL);
                            while (objReadUnit.Read())
                            {
                                objRowTxt.InnerText = Conversion.ConvertObjToStr(objReadUnit.GetValue("NAME"));
                            }
                            objReadUnit.Close();
                        }
                        else if (string.Compare(sUnitType, "SU", true) == 0)
                        {
                            sSQL = "";
                            sSQL = "SELECT ENTITY.LAST_NAME FROM ENTITY, OTHER_UNIT WHERE ENTITY.ENTITY_ID = OTHER_UNIT.ENTITY_ID AND OTHER_UNIT_ID = '" + sUnitID + "'";
                            objReadUnit = DbFactory.GetDbReader(m_sConnectString, sSQL);
                            while (objReadUnit.Read())
                            {
                                objRowTxt.InnerText = Conversion.ConvertObjToStr(objReadUnit.GetValue("LAST_NAME"));
                            }
                            objReadUnit.Close();
                        }
                        //MITS 30123 - End
                    }
                    objLstRow.AppendChild(objRowTxt);
                    objReader.Close();
                    using (PolicyXCvgType objPolCvg = (PolicyXCvgType)m_objDataModelFactory.GetDataModelObject("PolicyXCvgType", false))
                    {
                        iPolCvgRowId = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objRead.GetValue("COVERAGE_ID")), out bSuccess);
                        objPolCvg.MoveTo(iPolCvgRowId);
                        iCoverageType = objPolCvg.CoverageTypeCode;
                        sCovText = GetCoverageText(iUnitRowID, iPolCvgRowId, iCoverageType);
                    }
                    objRowTxt = p_objXmlDocument.CreateElement("Coverage");
                    if (string.IsNullOrEmpty(sCovText))
                    {
                        objRowTxt.InnerText = objLocalCache.GetCodeDesc(iCoverageType);
                    }
                    else
                    {
                        objRowTxt.InnerText = sCovText;
                    }
                    objLstRow.AppendChild(objRowTxt);

                    objRowTxt = p_objXmlDocument.CreateElement("LossType");
                    objRowTxt.InnerText = objLocalCache.GetCodeDesc(Conversion.CastToType<int>(Conversion.ConvertObjToStr(objRead.GetValue("LOSS_ID")), out bSuccess));
                    objLstRow.AppendChild(objRowTxt);

                    objRowTxt = p_objXmlDocument.CreateElement("ResExst");
                    if (string.Compare(Conversion.ConvertObjToStr(objRead.GetValue("RESERVE_CREATED")), "-1", true) == 0)
                    {
                        objRowTxt.InnerText = "Yes";
                    }
                    else if (string.Compare(Conversion.ConvertObjToStr(objRead.GetValue("RESERVE_CREATED")), "0", true) == 0)
                    {
                        objRowTxt.InnerText = "No";
                    }
                    //else
                    //{
                    //    objRowTxt.InnerText = "-1";
                    //}
                    objLstRow.AppendChild(objRowTxt);

                    objElement.AppendChild(objLstRow);
                }
                objRead.Close();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FNOLClaimReserve.Get.Error",m_iClientId), p_objException);//dvatsa-JIRA 3012
            }
            finally
            {
                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }
                if (objReadUnit != null)
                {
                    objReadUnit.Close();
                }
                if (objReader != null)
                {
                    objReader.Close();
                }
            }
            return p_objXmlDocument;
        }


        //rupal:start,policy system interface
        private string GetCoverageText(int iPolicyUnitRowId, int iPolicyCvgRowId, int iCoverageTypeCode)
        {
            string sCoverageText = string.Empty;
            StringBuilder sSQL = null;
            try
            {
                sSQL = new StringBuilder();
                sSQL.Append(" SELECT DISTINCT");
                sSQL.Append(" CASE(CNT)");
                if (DbFactory.IsOracleDatabase(m_sConnectString))
                {
                    sSQL.Append(" WHEN 1 THEN A.SHORT_CODE || ' ' || A.CODE_DESC ");
                }
                else
                {
                    sSQL.Append(" WHEN 1 THEN A.SHORT_CODE + ' ' + A.CODE_DESC ");
                }
                sSQL.Append(" ELSE");
                if (DbFactory.IsOracleDatabase(m_sConnectString))
                {
                    sSQL.Append(" CASE(NVL(COVERAGE_TEXT,''))");
                }
                else
                {
                    sSQL.Append(" CASE(ISNULL(COVERAGE_TEXT,''))");
                }
                sSQL.Append(" WHEN '' THEN A.CODE_DESC ");
                sSQL.Append(" ELSE COVERAGE_TEXT  ");
                sSQL.Append(" END  ");
                sSQL.Append(" END COVERAGE_TEXT");
                sSQL.Append(" FROM ");
                sSQL.Append(" ( ");
                sSQL.Append(" SELECT CODE_DESC, COVERAGE_TEXT, C.COVERAGE_TYPE_CODE ,CVG_SEQUENCE_NO,B.CNT, SHORT_CODE FROM ");
                sSQL.Append(" ( ");
                sSQL.Append(" SELECT COVERAGE_TYPE_CODE,COUNT(*) CNT FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE=" + iCoverageTypeCode + " GROUP BY COVERAGE_TYPE_CODE");
                sSQL.Append(" )B,");
                sSQL.Append(" (");
                sSQL.Append(" SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID, CODE_DESC, COVERAGE_TEXT, SHORT_CODE,COVERAGE_TYPE_CODE ,CVG_SEQUENCE_NO");
                sSQL.Append(" FROM POLICY_X_CVG_TYPE,CODES_TEXT");
                sSQL.Append(" WHERE");
                sSQL.Append(" POLICY_UNIT_ROW_ID =" + iPolicyUnitRowId + " AND POLCVG_ROW_ID=" + iPolicyCvgRowId);
                sSQL.Append(" AND CODES_TEXT.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE	");
                sSQL.Append(" ) C WHERE B.COVERAGE_TYPE_CODE = C.COVERAGE_TYPE_CODE");
                sSQL.Append(" )A ");
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL.ToString()))
                {
                    if (objReader.Read())
                    {
                        sCoverageText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                }
                return sCoverageText;

            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("FNOLClaimReserve.Get.Error",m_iClientId), objException);//dvatsa-JIRA 3012
            }
            finally
            {
                sSQL = null;
            }
        }


        /// <summary>
        /// Gets reserve data for FNOL reserves setup
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns>xmldocument</returns>
        public XmlDocument GetResSetUp(int iClaimId)
        {
            XmlDocument xmlReserve = new XmlDocument();
            XmlDocument xmlClaimant = new XmlDocument();
            XmlNode xmlClaimantNode = null;
            try
            {                
                ReserveFunds objReserve = new ReserveFunds(m_sConnectString, m_userLogin,m_iClientId);
                xmlClaimant = objReserve.GetClaimants(iClaimId);
                xmlReserve = objReserve.GetLookupXml(iClaimId, 0, true);
                //xmlClaimantNode = xmlClaimant.SelectSingleNode("Claimants");
                xmlClaimantNode = xmlReserve.ImportNode(xmlClaimant.DocumentElement, true);
                xmlReserve.SelectSingleNode("BOB").AppendChild(xmlClaimantNode);               
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FNOLClaimReserve.GetResSetUp.Error",m_iClientId), p_objException);//dvatsa-JIRA 3012
            }
            finally
            {
               
            }
            return xmlReserve;
        }

        /// <summary>
        /// Save reserve setups for FNOL reserves
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns>xmldocument</returns>
        public XmlDocument SaveResSetUp(XmlDocument p_objXmlDocument)
        {
            try
            {
                XmlElement objElm = null;
                int iNextUID = 0;
                string sRowId = string.Empty;
                string sClaimantEID = string.Empty;
                string sPolicyID = string.Empty;
                string sUnitID = string.Empty;
                string sCoverageID = string.Empty;
                string sLossID = string.Empty;
                string sClaimId = string.Empty;
                StringBuilder sbSql = new StringBuilder();
                DbConnection objConn = null;
                bool bExist = false;

                if (p_objXmlDocument.SelectSingleNode("FNOLResSetup/control[@name='ClaimId']") != null)
                {
                    objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("FNOLResSetup/control[@name='ClaimId']");
                    if (objElm != null)
                        sClaimId = objElm.InnerText;
                }
                foreach (XmlElement xmlElemFNOL in p_objXmlDocument.SelectNodes("//FNOLReserve"))
                {

                    if (xmlElemFNOL.SelectSingleNode("control[@name='RowId']") != null)
                    {
                        objElm = (XmlElement)xmlElemFNOL.SelectSingleNode("control[@name='RowId']");
                        if (objElm != null)
                            sRowId = objElm.InnerText;
                    }

                    objElm = (XmlElement)xmlElemFNOL.SelectSingleNode("control[@name='Claimant']");
                    if (objElm != null)
                        sClaimantEID = objElm.InnerText;

                    objElm = (XmlElement)xmlElemFNOL.SelectSingleNode("control[@name='Policy']");
                    if (objElm != null)
                        sPolicyID = objElm.InnerText;

                    objElm = (XmlElement)xmlElemFNOL.SelectSingleNode("control[@name='Unit']");
                    if (objElm != null)
                        sUnitID = objElm.InnerText;

                    objElm = (XmlElement)xmlElemFNOL.SelectSingleNode("control[@name='CoverageType']");
                    if (objElm != null)
                        sCoverageID = objElm.InnerText;
                    if (sCoverageID.Contains('#'))
                        sCoverageID = sCoverageID.Split('#')[0];

                    objElm = (XmlElement)xmlElemFNOL.SelectSingleNode("control[@name='LossType']");
                    if (objElm != null)
                        sLossID = objElm.InnerText;

                    bExist = CheckExistingFNOLEntry(sClaimId, sClaimantEID, sPolicyID, sUnitID, sCoverageID, sLossID);
                    if (!bExist)
                    {
                        sbSql.Length = 0;
                        if (sRowId != "")
                        {
                            sbSql = sbSql.Append("UPDATE FNOL_RESERVE SET LOSS_ID = '" + sLossID + "', ");
                            sbSql = sbSql.Append("POLICY_ID = '" + sPolicyID + "', CLAIMANT_EID = '" + sClaimantEID + "', ");
                            sbSql = sbSql.Append("UNIT_ID = '" + sUnitID + "', COVERAGE_ID = '" + sCoverageID + "' ");
                            sbSql = sbSql.Append("WHERE FNOL_RESERVE_ROW_ID = " + sRowId);

                        }
                        else
                        {

                            iNextUID = Utilities.GetNextUID(m_sConnectString, "FNOL_RESERVE",m_iClientId);//dvatsa
                            sbSql = sbSql.Append("INSERT INTO FNOL_RESERVE ");
                            sbSql = sbSql.Append("(FNOL_RESERVE_ROW_ID, POLICY_ID, CLAIM_ID, CLAIMANT_EID, UNIT_ID, COVERAGE_ID, LOSS_ID)");
                            sbSql = sbSql.Append(" VALUES(");
                            sbSql = sbSql.Append(iNextUID.ToString() + ",'" + sPolicyID);
                            sbSql = sbSql.Append("', '" + sClaimId + "'");
                            sbSql = sbSql.Append(", '" + sClaimantEID + "', '" + sUnitID + "'");
                            sbSql = sbSql.Append(", '" + sCoverageID + "', '" + sLossID + "')");
                        }

                        objConn = DbFactory.GetDbConnection(m_sConnectString);
                        objConn.Open();
                        objConn.ExecuteNonQuery(sbSql.ToString());
                    }
                }

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FNOLClaimReserve.SaveResSetUp.Error",m_iClientId), p_objException);//dvatsa-JIRA 3012
            }
            finally
            {

            }
            return p_objXmlDocument;
        }

        /// <summary>
        /// Check existing entries FNOL reserves to avoid duplicates
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns>xmldocument</returns>
        private bool CheckExistingFNOLEntry(string sclaimID, string sClaimant, string sPolicy, string sUnit, string sCoverage, string sLossType)
        {
            bool bExist = false;
            string sSql = string.Empty;
            DataSet objDS = new DataSet();
            DataRow[] dtRow = null;
            try
            {
                sSql = "SELECT CLAIM_ID, POLICY_ID, CLAIMANT_EID, UNIT_ID, COVERAGE_ID, LOSS_ID FROM FNOL_RESERVE";
                objDS = DbFactory.GetDataSet(m_sConnectString, sSql,m_iClientId);
                if (objDS != null && objDS.Tables[0].Rows.Count > 0)
                {
                    dtRow = objDS.Tables[0].Select("CLAIM_ID = " + Conversion.ConvertStrToInteger(sclaimID) + " AND POLICY_ID = " + Conversion.ConvertStrToInteger(sPolicy) + " AND CLAIMANT_EID = " + Conversion.ConvertStrToInteger(sClaimant) + " AND UNIT_ID = " + Conversion.ConvertStrToInteger(sUnit) + " AND COVERAGE_ID = " + Conversion.ConvertStrToInteger(sCoverage) + " AND LOSS_ID = " + Conversion.ConvertStrToInteger(sLossType));
                    if (dtRow.Length > 0)
                    {
                        bExist = true;
                    }
                }                
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (objDS != null)
                    objDS.Dispose();
            }
            return bExist;
        }

        /// <summary>
        /// save and create reserve for FNOL reserves setups
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns>xmldocument</returns>
         public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            DbReader objRead = null;
            DbReader objReadRes = null;
            int iClaimId = 0;
            int iFNOLResRowid = 0;
            int iClaimantEid = 0;
            int iPolicyID = 0;
            int iResPolicyID = 0;
            int iCoverageID = 0;
            int iCoverageType = 0;
            int iReserveTypeCode = 0;
            int iLossTypeCode = 0;
            //int iClaimType = 0;
            int iPolicyLOB = 0;
            int iResStatus = 0;
            int iUnitId = 0;
            int iPolUnitRowId = 0;
            double dAmount = 0.0;
            bool bSuccess = false;
            bool bResExist = false;
            bool bResCreated = false;
            int ircRowID = 0;
            int iCvgloss = 0;
            int iLOB = 0;           
            string sReserveTypeCode = string.Empty;
            string sReserveType = string.Empty;
            LocalCache objLocalCache = null;
            Claim objClaim = null;
            DbConnection objConn = null;
            Riskmaster.Settings.ColLobSettings objLOB = null;
            Riskmaster.Settings.LobSettings objLOBSettings = null;
            System.Collections.Hashtable objErrors = new System.Collections.Hashtable();
            try
            {
                objLocalCache = new LocalCache(m_sConnectString,m_iClientId);
                ReserveFunds objReserve = new ReserveFunds(m_sConnectString, m_userLogin,m_iClientId);//dvatsa
                iClaimId = Conversion.ConvertStrToInteger(p_objXmlDocument.SelectSingleNode("//ClaimID").InnerText);
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin,m_iClientId);//dvatsa
                }

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(iClaimId);
                //iClaimType = objClaim.ClaimTypeCode;
                iPolicyLOB = objClaim.PolicyLOBCode;
                iLOB = objClaim.LineOfBusCode;
                //Instantiate the LOB Settings Class
                objLOB = new Riskmaster.Settings.ColLobSettings(m_sConnectString,m_iClientId);//dvatsa
                objLOBSettings = objLOB[iLOB];
                if (objLOBSettings != null)
                {
                    if (objLOBSettings.ResByClmType)
                    {
                        sSQL = " SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE = " + iLOB.ToString() + " AND CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode.ToString();
                    }
                    else
                    {
                        sSQL = " SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE = " + iLOB.ToString();
                    }
                }
                objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                while (objReader.Read())
                {
                    if (string.IsNullOrEmpty(sReserveTypeCode))
                    {
                        sReserveTypeCode = "|" + Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE")) + "|";
                    }
                    else
                    {
                        sReserveTypeCode = sReserveTypeCode + Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE")) + "|";
                    }
                }
                objReader.Close();

                iResStatus = objLocalCache.GetCodeId("O", "RESERVE_STATUS");
                
                sSQL = "SELECT * FROM FNOL_RESERVE WHERE CLAIM_ID = " + iClaimId.ToString();
                objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                while (objReader.Read())
                {
                    bResCreated = false;
                    //AddReserve(int p_iClaimID, int p_iClaimantEID, int p_iPolicyID, int p_iCoverageID, int p_iReserveTypeCode, double p_dAmount, int iStatusCode, string sReason, int p_iLossTypeCode, int p_idisabilityCode,int p_iReserveCat)
                    iFNOLResRowid = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReader.GetValue("FNOL_RESERVE_ROW_ID")), out bSuccess);
                    iClaimantEid = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReader.GetValue("CLAIMANT_EID")), out bSuccess);
                    iPolicyID = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReader.GetValue("POLICY_ID")), out bSuccess);
                    iUnitId = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReader.GetValue("UNIT_ID")), out bSuccess);
                    iCoverageID = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReader.GetValue("COVERAGE_ID")), out bSuccess);
                    iLossTypeCode = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReader.GetValue("LOSS_ID")), out bSuccess);

                    using (PolicyXCvgType objPolCvg = (PolicyXCvgType)m_objDataModelFactory.GetDataModelObject("PolicyXCvgType", false))
                    {
                        objPolCvg.MoveTo(iCoverageID);
                        iCoverageType = objPolCvg.CoverageTypeCode;
                    }                   

                    sSQL = string.Empty;
                   // sSQL = "SELECT RESERVE_TYPE, RESERVE_AMNT FROM FNOL_CLM_RESERVE WHERE CLAIM_TYPE = '" + iClaimType.ToString() + "' AND COVERAGE_TYPE = '" + iCoverageType.ToString() + "' AND LOSS_TYPE = '" + iLossTypeCode.ToString() + "'";
                    sSQL = "SELECT L.RESERVE_TYPE, L.RESERVE_AMOUNT FROM LOSS_RESERVE_MAPPING L INNER JOIN CVG_LOSS_LOB_MAPPING C ON L.CVG_LOS_LOB_ROWID = C.ROW_ID AND C.POLICY_LOB  = '" + iPolicyLOB.ToString() + "' AND CVG_TYPE_CODE = '" + iCoverageType.ToString() + "' AND LOSS_CODE = '" + iLossTypeCode.ToString() + "'";
                    
                    //sSQL = "SELECT RESERVE_TYPE, RESERVE_AMNT FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB = '" + iPolicyLOB.ToString() + "' AND CVG_TYPE_CODE = '" + iCoverageType.ToString() + "' AND LOSS_CODE = '" + iLossTypeCode.ToString() + "'";
                    objRead = DbFactory.GetDbReader(m_sConnectString, sSQL);
                    while (objRead.Read())
                    {
                        iReserveTypeCode = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objRead.GetValue("RESERVE_TYPE")), out bSuccess);
                        dAmount = Conversion.CastToType<double>(Conversion.ConvertObjToStr(objRead.GetValue("RESERVE_AMOUNT")), out bSuccess);

                        if (iReserveTypeCode > 0)
                        {
                            sReserveType = "|" + iReserveTypeCode.ToString() + "|";
                            foreach (Claimant objClaimant in objClaim.ClaimantList)
                            {
                                if (objClaimant.ClaimantEid == iClaimantEid)
                                {
                                    foreach (ReserveCurrent objRC in objClaimant.ReserveCurrentList)
                                    {
                                        sSQL = string.Empty;
                                        sSQL = "SELECT PU.POLICY_ID, PC.POLICY_UNIT_ROW_ID FROM RESERVE_CURRENT RC, COVERAGE_X_LOSS CL, POLICY_X_CVG_TYPE PC,POLICY_X_UNIT PU WHERE CL.CVG_LOSS_ROW_ID = RC.POLCVG_LOSS_ROW_ID AND PC.POLCVG_ROW_ID = CL.POLCVG_ROW_ID AND PU.POLICY_UNIT_ROW_ID = PC.POLICY_UNIT_ROW_ID AND RC.RC_ROW_ID = '" + objRC.RcRowId.ToString() + "'";
                                        objReadRes = DbFactory.GetDbReader(m_sConnectString, sSQL);
                                        while (objReadRes.Read())
                                        {
                                            iResPolicyID = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReadRes.GetValue("POLICY_ID")), out bSuccess);
                                            iPolUnitRowId = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReadRes.GetValue("POLICY_UNIT_ROW_ID")), out bSuccess);
                                        }
                                        objReadRes.Close();

                                        using (CvgXLoss objCvgLoss = (CvgXLoss)m_objDataModelFactory.GetDataModelObject("CvgXLoss", false))
                                        {
                                            objCvgLoss.MoveTo(objRC.CoverageLossId);
                                            iCvgloss = objCvgLoss.LossCode;
                                        }

                                        if (iPolicyID == iResPolicyID && iUnitId == iPolUnitRowId && objRC.CoverageId == iCoverageID && iCvgloss == iLossTypeCode && objRC.ReserveTypeCode == iReserveTypeCode)
                                        {
                                            bResExist = true;
                                            bResCreated = true;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            if (!bResExist && sReserveTypeCode.Contains(sReserveType))
                            {
                                objReserve.AddReserve(ref objErrors, iClaimId, iClaimantEid, iPolicyID, iCoverageID, iReserveTypeCode,0, dAmount, iResStatus, "", iLossTypeCode, 0, 0, out ircRowID);
                                bResCreated = true;
                            }
                            bResExist = false;
                        }
                    }
                    objRead.Close();

                    sSQL = string.Empty;
                    if (bResCreated)
                    {
                        sSQL = "UPDATE FNOL_RESERVE SET RESERVE_CREATED = '-1' WHERE FNOL_RESERVE_ROW_ID = " + iFNOLResRowid.ToString();
                    }
                    else
                    {
                        sSQL = "UPDATE FNOL_RESERVE SET RESERVE_CREATED = '0' WHERE FNOL_RESERVE_ROW_ID = " + iFNOLResRowid.ToString();
                    }
                    objConn = DbFactory.GetDbConnection(m_sConnectString);
                    objConn.Open();
                    objConn.ExecuteNonQuery(sSQL);
                   
                }                
                    //AddReserve(int p_iClaimID, int p_iClaimantEID, int p_iPolicyID, int p_iCoverageID, int p_iReserveTypeCode, double p_dAmount, int iStatusCode, string sReason, int p_iLossTypeCode, int p_idisabilityCode,int p_iReserveCat)
                  objReader.Close();

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FNOLClaimReserve.Save.Error",m_iClientId), p_objException);//dvatsa-JIRA 3012
            }
            finally
            {
                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }
                if (objReadRes != null)
                {
                    objReadRes.Close();
                }
                if (objReader != null)
                {
                    objReader.Close();
                }
            }
            return p_objXmlDocument;
        }

         /// <summary>
         /// Delete entry from Grid for FNOL claim reserves
         /// </summary>
         /// <param name="p_objXmlDocument"></param>
         /// <returns>xmldocument</returns>
         public XmlDocument Delete(XmlDocument p_objXmlDocument)
         {
             string sRowId = string.Empty;
             string sSQL = string.Empty;
             XmlElement objElm = null;
             DbConnection objConn = null;
             try
             {
                 if (p_objXmlDocument.SelectSingleNode("/FNOLClmRes/control[@name='FNOLClmResGrid']") != null)
                 {
                     objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("/FNOLClmRes/control[@name='FNOLClmResGrid']");
                     if (objElm != null)
                         sRowId = objElm.InnerText;
                 }
                 if (!string.IsNullOrEmpty(sRowId))
                 {

                     sSQL = "DELETE FROM FNOL_RESERVE WHERE FNOL_RESERVE_ROW_ID =  " + sRowId;
                     objConn = DbFactory.GetDbConnection(m_sConnectString);
                     objConn.Open();
                     objConn.ExecuteNonQuery(sSQL);
                 }
             }
             catch (RMAppException p_objException)
             {
                 throw p_objException;
             }
             catch (Exception p_objException)
             {
                 throw new RMAppException(Globalization.GetString("FNOLClaimReserve.Delete.Error",m_iClientId), p_objException);//dvatsa-JIRA 3012
             }
             finally
             {

             }
             return p_objXmlDocument;
         }
    }
}
