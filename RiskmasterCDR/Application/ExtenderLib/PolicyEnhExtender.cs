using System;
using Riskmaster.DataModel ;


namespace Riskmaster.Application.ExtenderLib
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   24th,Aug 2004
	///Purpose :   Class PolicyEnhExtender used to Invoke Policy
	///Functions present in this class are already present in other classes. Hence not re-written.
	/// </summary>
	public class PolicyEnhExtender: Extender
	{

		#region Variable Declarations

        /// <summary>
        /// Internal variable for PolicyEnh
        /// </summary>
		internal PolicyEnh m_objPolEnh=null;

		/// <summary>
		/// Internal variable for Supplementals
		/// </summary>
		internal Supplementals m_objSupp=null;

		#endregion

		#region Constructor

		/// <summary>
		/// Default Constructor
		/// </summary>
		public PolicyEnhExtender(){}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_sUserName"></param>
        /// <param name="p_sPassword"></param>
        /// <param name="p_sDsnName"></param>
        /// <param name="p_sConnectString"></param>
        /// <param name="p_iClientId"></param>
        public PolicyEnhExtender(string p_sUserName, string p_sPassword, string p_sDsnName, string p_sConnectString, int p_iClientId) :
            base(p_sUserName, p_sPassword, p_sDsnName, p_sConnectString, p_iClientId) { }

		#endregion

		#region Public Properties

		/// <summary>
		/// Public Read Only Property PolicyEnh
		/// </summary>
		public PolicyEnh objPolicyEnh
		{
			get
			{
				return m_objPolEnh;
			}
		}

        /// <summary>
        /// Gets or sets the insured identifier.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        /// <value>
        /// The insured identifier.
        /// </value>
        public int InsuredID { get; set; }

		#endregion

        /// <summary>
        /// For FROI.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public int iCalculateMonths(string StartDate, string EndDate)
        {
            if (StartDate != "" && EndDate != "")
            {
                //DateDiff("m", StartDate, EndDate) Mod 12
                return ((Convert.ToDateTime(StartDate)).Month - (Convert.ToDateTime(EndDate)).Month + 12 * ((Convert.ToDateTime(StartDate)).Year - (Convert.ToDateTime(EndDate)).Year));
            }
            else
            {
                return 0;
            }

        }
	}//end class
}// end namespace
