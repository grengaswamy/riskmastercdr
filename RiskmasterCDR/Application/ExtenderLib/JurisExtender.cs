using System;
using Riskmaster.DataModel;
using Riskmaster.Security; 
using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.ExceptionTypes;
using System.Data; 

namespace Riskmaster.Application.ExtenderLib
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   24th,Aug 2004
	///Purpose :   This class provides an extension environment for
	///	the JURIS population scripts. All Public top-level
	///	methods and properties are exposed to the scripts
	///	as built-in functions/methods.
	/// </summary>
	public class JURISExtender: Extender
	{

		#region Variable Declarations

		/// <summary>
		/// Internal variable for JURISOptionsMajor
		/// </summary>
		internal JURISOptionsMajor  m_objOptions=null;		

		/// <summary>
		/// Internal variable for TPA Entity
		/// </summary>
		internal Entity m_objTPA=null;

        /// <summary>
        /// Internal string m_sIncidentDesc_objExt.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        internal string m_sIncidentDesc_objExt = string.Empty;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructor to create instance of JURISExtender. Maps to base class constructor Extender.
		/// </summary>
		/// <param name="p_sUserName">User Login Name</param>
		/// <param name="p_sPassword">User Login Password</param>
		/// <param name="p_sDsnName">User Dsn Name</param>
		/// <param name="p_sConnectString">Connection String</param>
        public JURISExtender(string p_sUserName, string p_sPassword, string p_sDsnName, string p_sConnectString, int p_iClientId) :
            base(p_sUserName, p_sPassword, p_sDsnName, p_sConnectString, p_iClientId) { }//sharishkumar Jira 827

		#endregion

		#region Public Properties

		/// The Following public properties were moved from the base Extender Class
		/// while doing integration with FROI.
		/// <summary>
		/// Public Read Only Property Options
		/// </summary>
		public JURISOptionsMajor objOptions
		{
			get
			{
				return m_objOptions;
			}
		}

		/// <summary>
		/// Public Read Only Property TPA
		/// </summary>
		public Entity objTPA
		{
			get
			{
				return m_objTPA;
			}
		}

		/// <summary>
		/// Read Only Property for Company Entity
		/// </summary>
		public Entity objCompany
		{
			get
			{
				return m_objCompany;
			}
		}

		/// <summary>
		/// Public Read Only Property for Event
		/// </summary>
		public Event objEvent
		{
			get
			{
				return m_objEvent;
			}
		}

		/// <summary>
		/// Public Read Only Property for Claim
		/// </summary>
		public Claim objClaim
		{
			get
			{
				return m_objClaim;
			}
		}

        /// <summary>
        /// Gets the read only property for PI Employee.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        /// <value>
        /// The object pi.
        /// </value>
        public PiEmployee objPi
        {
            get 
            {
                return m_objPiEmp;
            }
        }
		//Added for Froi Migration- Mits 33585,33586,33587
        public int ClaimNumberOption_objExt
        {
            get
            {
                return objOptions.CarrierClaimNumberOption;
            }
        }

		/// <summary>
		/// Read Only Property for Administrator Entity
		/// </summary>
		public Entity objAdministrator
		{
			get
			{
				return m_objAdministrator;
			}
		}

		/// <summary>
		/// Read Only Property for Carrier Entity
		/// </summary>
		public Entity objCarrier
		{
			get
			{
				return m_objCarrier;
			}
		}

		/// <summary>
		/// Claim Id
		/// </summary>
		public int ClaimID
		{
			get
			{
				return m_iClaimID;
			}
		}

		/// <summary>
		/// Event Id
		/// </summary>
		public int EventID
		{
			get
			{
				return m_iEventID;
			}
		}

		/// <summary>
		/// Company EID
		/// </summary>
		public int CompanyEID
		{
			get
			{
				return m_iCompanyEID;
			}
		}

		/// <summary>
		/// Read Only Property EntityXSelfInsured
		/// </summary>
		//Added for Froi Migration- Mits 33585,33586,33587
        public EntityXSelfInsured objSelfInsured
		{
			get
			{
				return m_objSelfInsured;
			}
		}

		/// <summary>
		/// Read Only Property EntityXSelfInsured.Added for Froi Migration- Mits 33585,33586,33587
		/// </summary>
		public EntityXSelfInsured EntityXSelfInsured
		{
			get
			{
				return m_objSelfInsured;
			}
		}

		/// <summary>
		/// Read Only Property Supplementals
		/// </summary>
		public Supplementals Supplementals
		{
			get
			{
				return m_objSupp;
			}
		}

		/// <summary>
		/// Read Only Property Patient
		/// </summary>
		public Patient objPatient
		{
			get
			{
				return m_objPatient;
			}
		}
		//Added for Froi Migration- Mits 33585,33586,33587
        public JurisLicenseOrCode objCodeLicense
        {
            get
            {
                return m_objJurisLicenseOrCode;
            }
        } 

        // akaushik5 Added for MITS 37849 Starts
        /// <summary>
        /// Gets the s incident desc_obj ext.
        /// </summary>
        /// <value>
        /// The s incident desc_obj ext.
        /// </value>
        public string sIncidentDesc_objExt
        {
            get
            {
                return this.m_sIncidentDesc_objExt;
            }
        }
        // akaushik5 Added for MITS 37849 Ends

		#endregion

		#region Methods

		/// <summary>
		/// Gets Name Title and Phone
		/// </summary>
		/// <param name="p_sName">Name</param>
		/// <param name="p_sTitle">Title</param>
		/// <param name="p_sPhone">Phone</param>
		public void GetPreparerInfo(ref string p_sName, ref string p_sTitle,ref string p_sPhone)
		{
			string sSQL="";
			string sPerson="";
			DbReader objRdr=null;
			try
			{
				if (m_objOptions==null)
                    throw new RMAppException(Globalization.GetString("JURISExtender.GetPreparerInfo.InvalidOptions", m_iClientId));//sharishkumar Jira 827

				if (m_objClaim==null)
					return;

				switch(m_objOptions.JURISPreparer)
				{
					case 1:		//WORKSTATION USER
						GetPreparerInfoXML("JurisOptions");
						p_sName=m_sName;
						p_sTitle=m_sTitle;
                        //caggarwal4 MITS 36830 (Preparer information is not pulled in on state forms)
                        //p_sTitle=m_sTitle;
                        p_sPhone = this.m_sPhone;
						break;
                    #region Case 2 Edited By User
                    case 2:		//EDITED BY USER
						 if (object.ReferenceEquals(m_objEvent, null))
                        {
                            return;
                        }

                        if (object.ReferenceEquals(m_objClaim, null))
                        {
                            return;
                        }

                        if (m_objEvent.EventId > 0)
                        {
                            sSQL = string.Format("SELECT UPDATED_BY_USER FROM EVENT WHERE EVENT_ID = {0}", m_objEvent.EventId);
                        }

                        if (m_objClaim.ClaimId > 0)
                        {
                            sSQL = string.Format("SELECT UPDATED_BY_USER FROM CLAIM WHERE CLAIM_ID = {0}", m_objClaim.ClaimId);
                        }

                        using (objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (objRdr != null && objRdr.Read())
                            {
                                sPerson = objRdr.GetString(0);
                            }
                        }

                        sSQL = "SELECT USER_TABLE.LAST_NAME, USER_TABLE.FIRST_NAME, USER_TABLE.TITLE, " +
                            "USER_TABLE.OFFICE_PHONE FROM USER_DETAILS_TABLE, USER_TABLE WHERE " +
                            "USER_DETAILS_TABLE.LOGIN_NAME = '" + sPerson +
                            "' AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID";

                        objRdr = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(0), sSQL);
                        if (objRdr != null)
                        {
                            if (objRdr.Read())
                            {
                                //Get it from USER_DETAILS_TABLE
                                p_sName = objRdr.GetString("FIRST_NAME") + " " + objRdr.GetString("LAST_NAME");
                                p_sTitle = objRdr.GetString("TITLE");
                                p_sPhone = objRdr.GetString("OFFICE_PHONE");
                            }
                        }
                        break;
                    #endregion
                    #region Case 3 Added By User
                    case 3:
                        if (m_objEvent == null) return;
                        if (m_objClaim == null) return;
                        if (m_objEvent.EventId > 0)
                            sSQL = "SELECT ADDED_BY_USER FROM EVENT WHERE EVENT_ID = " + m_objEvent.EventId;
                        if (m_objClaim.ClaimId > 0)
                            sSQL = "SELECT ADDED_BY_USER FROM CLAIM WHERE CLAIM_ID = " + m_objClaim.ClaimId;
                        objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL);
                        if (objRdr != null)
                        {
                            if (objRdr.Read())
                            {
                                sPerson = objRdr.GetString(0);
                            }
                        }
                        if (objRdr != null) objRdr.Dispose();

                        sSQL = "SELECT USER_TABLE.LAST_NAME, USER_TABLE.FIRST_NAME, USER_TABLE.TITLE, " +
                            "USER_TABLE.OFFICE_PHONE FROM USER_DETAILS_TABLE, USER_TABLE WHERE " +
                            "USER_DETAILS_TABLE.LOGIN_NAME = '" + sPerson +
                            "' AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID";
                        objRdr = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), sSQL);//sharishkumar Jira 827
                        if (objRdr != null)
                        {
                            if (objRdr.Read())
                            {
                                //Get it from USER_DETAILS_TABLE
                                p_sName = objRdr.GetString("FIRST_NAME") + " " + objRdr.GetString("LAST_NAME");
                                p_sTitle = objRdr.GetString("TITLE");
                                p_sPhone = objRdr.GetString("OFFICE_PHONE");
                            }
                        }
                        break;
                    #endregion

                    #region Case 4 TPA Call In
                    case 4:
                        if (m_objClaim != null)
                        {
                            sSQL = "SELECT TC_PREPARER_NAME, TC_PREPARER_TITLE, TC_PREPARER_PHONE FROM CLAIM_SUPP " +
                                "WHERE CLAIM_ID = " + m_objClaim.ClaimId;
                            objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL);
                            if (objRdr != null)
                            {
                                if (objRdr.Read())
                                {
                                    p_sName = objRdr.GetString("TC_PREPARER_NAME");
                                    p_sTitle = objRdr.GetString("TC_PREPARER_TITLE");
                                    p_sPhone = objRdr.GetString("TC_PREPARER_PHONE");
                                }
                            }
                        }
                        break;
                    #endregion
                    
                    #region Case 5 Adjuster
                    case 5:
                        if (!object.ReferenceEquals(this.objClaim, null))
                        {
                            if (this.objClaim.AdjusterList.Count.Equals(default(int)))
                            {
                                break;
                            }

                            if (!this.objClaim.CurrentAdjuster.AdjRowId.Equals(default(int)))
                            {
                                p_sName = string.Format("{0} {1}", this.objClaim.CurrentAdjuster.AdjusterEntity.FirstName, this.objClaim.CurrentAdjuster.AdjusterEntity.LastName);
                                p_sTitle = this.objClaim.CurrentAdjuster.AdjusterEntity.Title;
                                p_sPhone = this.objClaim.CurrentAdjuster.AdjusterEntity.Phone1;
                            }
                            else
                            {
                                using (ClaimAdjuster claimAdjuster = objClaim.Context.Factory.GetDataModelObject("ClaimAdjuster", false) as ClaimAdjuster)
                                {
                                    (claimAdjuster as INavigation).Filter = string.Format("CLAIM_ID={0}", this.objClaim.ClaimId);
                                    claimAdjuster.MoveLast();

                                    p_sName = string.Format("{0} {1}", claimAdjuster.AdjusterEntity.FirstName, claimAdjuster.AdjusterEntity.LastName);
                                    p_sTitle = claimAdjuster.AdjusterEntity.Title;
                                    p_sPhone = claimAdjuster.AdjusterEntity.Phone1;
                                }
                            }
                        }
                        break;
                    #endregion
                    //caggarwal4 MITS 36830 (Preparer information is not pulled in on state forms) end
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("JURISExtender.GetPrepareInfo.JURISError", m_iClientId), p_objEx);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}
		}


		/// <summary>
		/// Returns Contact details
		/// </summary>
		/// <param name="p_sName">Contact Name</param>
		/// <param name="p_sTitle">Contact Title</param>
		/// <param name="p_sPhone">Contact Phone</param>
		public void GetTCContactInfo_objExt(ref string p_sName, ref string p_sTitle, ref string p_sPhone)
		{
			DbReader objRdr=null;			
			try
			{                
				if(m_objClaim == null)
                    throw new RMAppException(Globalization.GetString("JURISExtender.GetTCContactInfo_objExt.InvalidClaim", m_iClientId)); //sharishkumar Jira 827

				objRdr=DbFactory.GetDbReader(m_sConnectString ,
					"SELECT TC_CONTACT_NAME, TC_CONTACT_TITLE, TC_CONTACT_PHONE FROM CLAIM_SUPP WHERE CLAIM_ID = " +
					m_objClaim.ClaimId);
 
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						p_sName=objRdr.GetString("TC_CONTACT_NAME");
						p_sTitle=objRdr.GetString("TC_CONTACT_TITLE");
						p_sPhone=objRdr.GetString("TC_CONTACT_PHONE");
					}
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("JURISExtender.GetTCContactInfo_objExt.DataError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}
		}


		/// <summary>
		/// Returns true if record exists. Else inserts a new record in JURIS_OPTIONS.
		/// </summary>
		/// <returns>Boolean true/false</returns>
		public bool DefaultRecordExist()
		{
			DbReader objRdr=null;			
			DbReader objRdrEnt=null;
			int iRecCount =0;
			int iClaimAdminDefaultEID=0;
			JURISOptionsMajor objOldOptions = null;
			try
			{
                objOldOptions = new JURISOptionsMajor(m_sConnectString, m_sUserName, m_sPassword, m_sDsnName, m_iClientId);//sharishkumar Jira 827
				objRdr=DbFactory.GetDbReader(m_sConnectString ,
					"SELECT SELECTED_EID, JURIS_ROW_ID FROM JURIS_OPTIONS WHERE SELECTED_EID = -1 AND JURIS_ROW_ID = -1" ); 
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						return true;
					}
				}
				if(objRdr!=null)objRdr.Dispose();

				objRdr=DbFactory.GetDbReader(m_sConnectString ,
					"SELECT PRINT_CLAIMS_ADMIN,PRINT_OSHA_DESC,HIERARCHY_LEVEL, USE_DEF_CARRIER, FROI_PREPARER FROM SYS_PARMS" ); 
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						objOldOptions.EmployerLevel = Conversion.GetOrgHierarchyLevel(  objRdr.GetString ("HIERARCHY_LEVEL"));
						objOldOptions.JURISPreparer =objRdr.GetInt32("FROI_PREPARER");
						objOldOptions.ClaimAdminOption  =objRdr.GetInt32("PRINT_CLAIMS_ADMIN");
						objOldOptions.PrintOSHADesc = objRdr.GetInt32("PRINT_OSHA_DESC");
						if (objOldOptions.ClaimAdminOption ==4)
						{
							objRdrEnt=DbFactory.GetDbReader(m_sConnectString ,
								"SELECT * FROM ENTITY, GLOSSARY WHERE ((ENTITY.ENTITY_TABLE_ID = GLOSSARY.TABLE_ID)" +
								" AND (GLOSSARY.SYSTEM_TABLE_NAME = 'WC_DEF_CLAIM_ADMIN')) AND ENTITY.DELETED_FLAG = 0"); 
							if (objRdrEnt!=null) 
							{
								if(objRdrEnt.Read())
								{
									if (iClaimAdminDefaultEID==0)
										iClaimAdminDefaultEID= objRdrEnt.GetInt32("ENTITY_ID");
									iRecCount++;
								}
							}
						}
					}
				}
				if (iRecCount==1)
					objOldOptions.ClaimAdminDefaultEID = iClaimAdminDefaultEID;
				else
					objOldOptions.ClaimAdminDefaultEID =0;
				objOldOptions.CarrierOption=3;
				objOldOptions.SelectedEID =-1;
				objOldOptions.JurisRowId=-1;
				if (objOldOptions.SaveData(-1,-1,""))
					return true;
				else
					return false;				
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("JURISExtender.DefaultRecordExist.DataError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				if(objRdrEnt!=null)objRdrEnt.Dispose();
				objOldOptions=null;
			}
		}

		/// <summary>
		/// WC_AL_SUPP
		/// </summary>
		/// <param name="p_sDateDTG">TF_LAST_DATE</param>
		public void AL_FormTFLastDate_Write(string p_sDateDTG)
		{
			DbReader objRdr=null;
			DataTable  objDtTbl=null;
			DataRow[] arrRows=null;
			DbConnection objConn=null;
			try
			{
				objRdr=DbFactory.GetDbReader(m_sConnectString ,"SELECT * FROM WC_AL_SUPP WHERE CLAIM_ID = "+ m_objClaim.ClaimId);
				objDtTbl=objRdr.GetSchemaTable();
				arrRows = objDtTbl.Select("ColumnName='CLAIMADMIN_TPA_EID'");
				if(arrRows.GetLength(0)==1)
				{
					objConn = DbFactory.GetDbConnection(m_sConnectString );
					objConn.Open();
					objConn.ExecuteNonQuery( "UPDATE WC_AL_SUPP SET TF_LAST_DATE = '" + p_sDateDTG + "'");
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("JURISExtender.AL_FormTFLastDate_Write.SaveError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				if(objConn!=null)
				{
					objConn.Dispose();
				}
				objDtTbl=null;
				arrRows=null;
			}			
		}

		/// <summary>
		/// Gets WC_AL_SUPP
		/// </summary>
		/// <returns>TF_LAST_DATE</returns>
		public string AL_FormTFLastDate_Fetch()
		{
			DbReader objRdr=null;
			string sLastDt="";
			try
			{
				objRdr=DbFactory.GetDbReader(m_sConnectString ,"SELECT * FROM WC_AL_SUPP WHERE CLAIM_ID = "+ m_objClaim.ClaimId);
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						sLastDt= objRdr.GetString("TF_LAST_DATE");
					}
				}
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("JURISExtender.AL_FormTFLastDate_Fetch.DataError",m_iClientId) , p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}			
			return sLastDt;
		}

        /// <summary>
        /// For FROI.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public int iCalculateMonths(string StartDate, string EndDate)
        {
            if (StartDate != "" && EndDate != "")
            {
                //DateDiff("m", StartDate, EndDate) Mod 12
                return ((Convert.ToDateTime(StartDate)).Month - (Convert.ToDateTime(EndDate)).Month + 12 * ((Convert.ToDateTime(StartDate)).Year - (Convert.ToDateTime(EndDate)).Year));
            }
            else
            {
                return 0;
            }

        }
		#endregion

	}//End class
}//End namespace
