using System;
using System.Data ;
using Riskmaster.Db ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Common ;


namespace Riskmaster.Application.ExtenderLib
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   24th,Aug 2004
	///Purpose :   Base class to FROI_OPTIONS & JURIS_OPTIONS. Contains common functionality
	///to both the classes FROIExtender & JURISExtender.
	/// </summary>
	public class OptionsMajor
	{

		#region Variable Declarations

		/// <summary>Private variable to store connection string</summary>
		protected string m_sConnectString ="";

		/// <summary>Private variable to store User Login</summary>
		protected string m_sUserLoginName ="";

		/// <summary>Private variable to store User Password</summary>
		protected string m_sUserPwd ="";

		/// <summary>Private variable to store User DSN</summary>
		protected string m_sDsnName ="";

		/// <summary>Internal variable for CarrierOrgHierarchyLevel property</summary>
		internal int m_iCarrierOrgHierarchyLevel=0;

		/// <summary>Private variable for DataHasChanged property</summary>
		protected bool m_bDataHasChanged=false;

		/// <summary>Internal variable for ClaimAdminOrgHierarchyLevel property</summary>
		internal int m_iClaimAdminOrgHierarchyLevel=0;

		/// <summary>Private variable for VirginiaParentLevel property</summary>
		internal int m_iVirginiaParentLevel=0;

		/// <summary>Internal variable for ClaimAdminDefaultEID property</summary>
		internal int m_iClaimAdminDefaultEID=0;

		/// <summary>Internal Variable for the property CarrierDefaultEID</summary>
		internal int m_iCarrierDefaultEID=0;

		/// <summary>USE_DEF_CARRIER column from table FROI_OPTIONS</summary>
		internal int m_iCarrierOption=0;

		/// <summary>Internal variable for ClaimAdminOption property</summary>
		internal int m_iClaimAdminOption=0;

		/// <summary>Private variable for PrintOSHADesc property</summary>
		protected int m_iPrintOSHADesc=0;

		/// <summary>Internal variable for EmployerLevel property</summary>
		internal int m_iEmployerLevel=0;

		/// <summary>Private variable for SelectedEID property</summary>
		protected int m_iSelectedEID=0;

		/// <summary>Private variable for FROIPreparer property</summary>
		protected int m_iFROIPreparer=0;

		/// <summary>Private variable to store JURIS_ROW_ID</summary>
		protected int m_iJurisRowID=0;
        protected int m_iClientId = 0;//sharishkumar Jira 827
        /// <summary>
        /// The m_ carrier claim number option.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        protected int m_CarrierClaimNumberOption = 0;

		/// <summary>Internal variable for ClaimAdminTPAEID property</summary>
		internal int m_iClaimAdminTPAEID=0;

		#endregion

		#region Constructors

		/// <summary>
		/// Base class constructor
		/// </summary>
		/// <param name="p_sConnectString">Connection String</param>
		/// <param name="p_sUserLogin">User Login Name</param>
		/// <param name="p_sUserPwd">User Password</param>
		/// <param name="p_sDsnName">User DSN Name</param>
        public OptionsMajor(string p_sConnectString, string p_sUserLogin, string p_sUserPwd, string p_sDsnName, int p_iClientId)//sharishkumar Jira 827
		{
            m_sConnectString=p_sConnectString;
			m_sUserLoginName= p_sUserLogin;
			m_sUserPwd = p_sUserPwd;
			m_sDsnName= p_sDsnName;
            m_iClientId = p_iClientId;//sharishkumar Jira 827
		}

		/// <summary>
		/// Base class constructor
		/// </summary>
		/// <param name="p_sConnectString">Connection String</param>
		/// <param name="p_sUserLogin">User Login Name</param>
        public OptionsMajor(string p_sConnectString, string p_sUserLogin, int p_iClientId)
		{
			m_sConnectString=p_sConnectString;
			m_sUserLoginName= p_sUserLogin;
            m_iClientId = p_iClientId;//sharishkumar Jira 827
		}

		//default constructor
		public OptionsMajor(){}

		#endregion

		#region Methods

		/// <summary>
		/// Clears and resets the internal variables
		/// </summary>
		/// <returns></returns>
		public virtual bool ClearObject()
		{
			m_iCarrierOrgHierarchyLevel = -1;
			m_iClaimAdminOrgHierarchyLevel = -1;
			m_bDataHasChanged = false;
			m_iSelectedEID = -1;
			m_iEmployerLevel = -1;
			m_iPrintOSHADesc = -1;
			m_iClaimAdminOption = -1;
			m_iCarrierOption = -1;
			m_iCarrierDefaultEID = -1;
			m_iClaimAdminDefaultEID = -1;
			m_iVirginiaParentLevel = -1;
			return true;
		}

		/// Name		: LoadDataFromSysParms
		/// Author		: Pankaj Chowdhury
		/// Date Created: 08/24/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *							Author
		/// 01/17/2004		Added Check for column existence			Pankaj
		///************************************************************
		/// <summary>Get data from SYS_PARMS</summary>
		/// <returns>Void</returns>
		protected bool LoadDataFromSysParms()
		{
			DbReader objRdr = null;
			DataTable  objDtTbl = null;
			DataRow[] arrRows = null;  
			try
			{
				objRdr=DbFactory.GetDbReader(m_sConnectString ,"SELECT * FROM SYS_PARMS");
				objDtTbl=objRdr.GetSchemaTable();
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						m_iSelectedEID=0;						
						arrRows = objDtTbl.Select("ColumnName='EMPLOYER_LEVEL'");
						if(arrRows.GetLength(0)==1)
							m_iEmployerLevel= objRdr.GetInt32("EMPLOYER_LEVEL"); 
						arrRows = objDtTbl.Select("ColumnName='PRINT_OSHA_DESC'");
						if(arrRows.GetLength(0)==1)
							m_iPrintOSHADesc= objRdr.GetInt32("PRINT_OSHA_DESC"); 
						arrRows = objDtTbl.Select("ColumnName='FROI_PREPARER'");
						if(arrRows.GetLength(0)==1)
							m_iFROIPreparer= objRdr.GetInt32("FROI_PREPARER"); 
						arrRows = objDtTbl.Select("ColumnName='PRINT_CLAIM_ADMIN'");
						if(arrRows.GetLength(0)==1)
							m_iClaimAdminOption = objRdr.GetInt32("PRINT_CLAIM_ADMIN"); 
						arrRows = objDtTbl.Select("ColumnName='USE_DEF_CARRIER'");
						if(arrRows.GetLength(0)==1)
							m_iCarrierOption = objRdr.GetInt32("USE_DEF_CARRIER"); 
					}
				}
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("OptionsMajor.LoadDataFromSysParms.DataLoadError",m_iClientId) , p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				if(objDtTbl!=null)objDtTbl.Dispose(); 
				arrRows = null;
			}
			return true;
		}

		#endregion

	}// End class
} // end namespace
