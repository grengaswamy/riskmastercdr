using System;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel ;


namespace Riskmaster.Application.ExtenderLib
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   24th,Aug 2004
	///Purpose :   Exposed interface to populate the Extender Objects.
	/// </summary>
	public class Populater: PopulateExtender 
	{     
		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sUserName">User Name</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">Dsn Name</param>
        public Populater(string p_sUserName, string p_sPassword, string p_sDsnName, int p_iClientId) :
			base(p_sUserName,p_sPassword,p_sDsnName,p_iClientId){}//sharishkumar Jira 827

		#endregion

		#region Methods
		/// <summary>
		/// Prepares Juris Options
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>int</returns>
		public int GetJURISPreparerSource(int p_iClaimID)
		{
			Claim objClaim=null;
			PiEmployee objPIEmp=null;
			Event objEvent =null;
			int iPrepSource=0;
			Functions objCf=null;
			try
			{
				//load claim object
				objClaim = (Claim)m_objDmf.GetDataModelObject("Claim",false);
				if (p_iClaimID>0)
					objClaim.MoveTo (p_iClaimID);
				else
                    throw new InvalidValueException(Globalization.GetString("PopulateExtender.InvalidClaim", m_iClientId));//sharishkumar Jira 827

				objPIEmp = (PiEmployee)m_objDmf.GetDataModelObject("PiEmployee",false);				
				objEvent = (Event)m_objDmf.GetDataModelObject("Event",false);

				//load event
				if (objClaim.EventId>0)
					objEvent.MoveTo (objClaim.EventId);
				
				foreach(Claimant objClmnt in objClaim.ClaimantList)
				{
					// load person involved
					objPIEmp = objEvent.PiList.GetPiByEID(objClmnt.ClaimantEid) as PiEmployee ;
					break;
				}

				objCf = new Functions(m_sConnectString, m_iClientId); 
				if(objPIEmp!=null)
                    iPrepSource = objCf.GetJURISPreparerSource(objPIEmp.DeptAssignedEid, objClaim.FilingStateId, m_iClientId);//sharishkumar Jira 827
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("Populater.GetJURISPreparerSource.AppError", m_iClientId), p_objEx);//sharishkumar Jira 827
			}
			finally
			{
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
                if (objPIEmp != null)
                {
                    objPIEmp.Dispose();
                }
                if (objEvent != null)
                {
                    objEvent.Dispose();
                }
				objCf=null;
			}
			return iPrepSource;
		}

        /// <summary>
        /// Prepares Froi options
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <returns>int</returns>
		public int GetFROIPreparerSource(int p_iClaimID)
		{
			Claim objClaim=null;
			PiEmployee objPIEmp=null;
			Event objEvent =null;
			int iPrepSource=0;
			Functions objCf=null;
			try
			{
				//populate claim
				objClaim = (Claim)m_objDmf.GetDataModelObject("Claim",false);
				if (p_iClaimID>0)
					objClaim.MoveTo (p_iClaimID);
				else
                    throw new InvalidValueException(Globalization.GetString("PopulateExtender.InvalidClaim", m_iClientId));//sharishkumar Jira 827

				objPIEmp = (PiEmployee)m_objDmf.GetDataModelObject("PiEmployee",false);				
				objEvent = (Event)m_objDmf.GetDataModelObject("Event",false);

				//populate event
				if (objClaim.EventId>0)
					objEvent.MoveTo (objClaim.EventId);
				
				foreach(Claimant objClmnt in objClaim.ClaimantList)
				{
					objPIEmp = objEvent.PiList.GetPiByEID(objClmnt.ClaimantEid) as PiEmployee ;
					break;
				}

                objCf = new Functions(m_sConnectString, m_iClientId); 
				if(objPIEmp!=null)
                    iPrepSource = objCf.GetFROIPreparerSource(objPIEmp.DeptAssignedEid, objClaim.FilingStateId, m_iClientId);//sharishkumar Jira 827
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("Populater.GetFROIPreparerSource.AppError", m_iClientId), p_objEx);//sharishkumar Jira 827
			}
			finally
			{
				objClaim=null;
				objPIEmp=null;
				objEvent =null;
				objCf=null;
			}
			return iPrepSource;
		}

		/// <summary>
		/// Dispose method
		/// </summary>
		public override void Dispose()
		{
            base.Dispose(); 
		}

		#endregion

	}// end class
}// end namespace
