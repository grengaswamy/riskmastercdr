using System;
using System.Data;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Text;
using System.Xml; 


namespace Riskmaster.Application.ExtenderLib
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   24th,Aug 2004
	///Purpose :   Acts as a layer to the FROI_OPTIONS
	/// </summary>
	public class FROIOptionsMajor: OptionsMajor 
	{

		#region Variable Declarations

		/// <summary>ATTACH_TO_CLAIM column from table FROI_OPTIONS</summary>
		internal int m_iAttachToClaimByDefault_FROI=0;

		/// <summary>USE_TITLE column from table FROI_OPTIONS</summary>
		internal int m_iUseTitle=0;

		/// <summary>WORK_LOSS_FIELD column from table FROI_OPTIONS</summary>
		internal int m_iWorkLossField=0;

		/// <summary>FORCE_PRINT_NCCI column from table FROI_OPTIONS</summary>
		internal int m_iNCCIForce=0;

		/// <summary>SUSPRES_DEFLT_TIME column from table FROI_OPTIONS</summary>
		internal int m_iSuspressDefaultTime=0;

		/// <summary>CONTACT_OPTION column from table FROI_OPTIONS</summary>
		internal int m_iContactOption=0;

		/// <summary>NE_PARENT_OPTION column from table FROI_OPTIONS</summary>
		internal int m_iNEParentOption=0;

		/// <summary>NE_PARENT_DEF_ID column from table FROI_OPTIONS</summary>
		internal int m_iNEParentDefaultID=0;

		/// <summary>NE_PARENT_ORG_HIER column from table FROI_OPTIONS</summary>
		internal int m_iNEParentOrgLevel=0;

		/// <summary>TN_PARENT_OPTION column from table FROI_OPTIONS</summary>
		internal int m_iTNParentOption=0;

		/// <summary>TN_PARENT_DEF_ID column from table FROI_OPTIONS</summary>
		internal int m_iTNParentDefaultID=0;

		/// <summary>TN_PARENT_ORG_HIER column from table FROI_OPTIONS</summary>
		internal int m_iTNParentOrgLevel=0;

		/// <summary>VA_PARENT_OPTION column from table FROI_OPTIONS</summary>
		internal int m_iVAParentOption=0;

		/// <summary>VA_PARENT_DEF_ID column from table FROI_OPTIONS</summary>
		internal int m_iVAParentDefaultID=0;

		/// <summary>PRINT_CLMNUMB_TOP column from table FROI_OPTIONS</summary>
		internal int m_iPrintClaimNumberAtTop=0;

		/// <summary>Private variable for PersonNotifiedOption property</summary>
		internal int m_iPersonNotifiedOption=0;

		/// <summary>Private variable for BaseOrgHierarchy property</summary>
		internal int m_iBaseOrgHierarchy=0;

		#endregion

		#region Constructors

		/// <summary>Class Constructor</summary>
		/// <param name="p_sConnectString">Connection String</param>
		/// <param name="p_sUserLogin">User Login Name</param>
		/// <param name="p_iSelectedEID">SELECTED_EID</param>
        public FROIOptionsMajor(string p_sConnectString, string p_sUserLogin, int p_iSelectedEID, int p_iClientId) ://sharishkumar Jira 827
            base(p_sConnectString, p_sUserLogin, p_iClientId)
		{
			m_sConnectString=p_sConnectString;
			m_iSelectedEID = p_iSelectedEID;
            m_iClientId = p_iClientId;//sharishkumar Jira 827
			LoadData(p_iSelectedEID);
		}


		/// <summary>
		/// Constructor with Connection String, User Login
		/// </summary>
		/// <param name="p_sConnectString">Connection String</param>
		/// <param name="p_sUserLogin">User Login</param>
		public FROIOptionsMajor(string p_sConnectString, string p_sUserLogin, int p_iClientId):
            base(p_sConnectString, p_sUserLogin, p_iClientId) { }

		/// <summary>
		/// Constructor with Connection String, User Login
		/// </summary>
		/// <param name="p_sConnectString">Connection String</param>
		/// <param name="p_sUserLogin">User Login</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">Dsn Name</param>
        public FROIOptionsMajor(string p_sConnectString, string p_sUserLogin, string p_sPassword, string p_sDsnName, int p_iClientId) :
            base(p_sConnectString, p_sUserLogin, p_sPassword, p_sDsnName, p_iClientId) { }//sharishkumar Jira 827


		/// <summary>
		/// Default Constructor. To be used when we dont need any instance data
		/// </summary>
		public FROIOptionsMajor(){}

		#endregion

		#region Public Properties

		/// <summary>Property PersonNotifiedOption</summary>		
		public int PersonNotifiedOption
		{
			get
			{
				return m_iPersonNotifiedOption;
			}	
			set
			{
				if(m_iPersonNotifiedOption!=value)
				{
					m_bDataHasChanged=true; 
					m_iPersonNotifiedOption=value;
				}
			}
		}


		/// <summary>Property FROIPreparer</summary>		
		public int FROIPreparer
		{
			get
			{
				return m_iFROIPreparer;
			}	
			set
			{
				if(m_iFROIPreparer!=value)
				{
					m_bDataHasChanged=true; 
					m_iFROIPreparer=value;
				}
			}
		}


		/// <summary>Property ConnectionString</summary>		
		public string ConnectionString
		{
			get
			{
				return m_sConnectString;
			}	
			set
			{
				if(m_sConnectString!=value)
				{
					m_sConnectString= value;
				}
			}
		}

		/// <summary>Property AttachToClaimByDefaultFROI</summary>		
		public int AttachToClaimByDefaultFROI
		{
			get
			{
				return m_iAttachToClaimByDefault_FROI;
			}	
			set
			{
				if(m_iAttachToClaimByDefault_FROI!=value)
				{
					m_bDataHasChanged=true; 
					m_iAttachToClaimByDefault_FROI= value;
				}
			}
		}

		/// <summary>Property UseTitle</summary>		
		public int UseTitle
		{
			get
			{
				return m_iUseTitle;
			}	
			set
			{
				if(m_iUseTitle!=value)
				{
					m_bDataHasChanged=true; 
					m_iUseTitle= value;
				}
			}
		}

		/// <summary>Property WorkLossField</summary>		
		public int WorkLossField
		{
			get
			{
				return m_iWorkLossField;
			}	
			set
			{
				if(m_iWorkLossField!=value)
				{
					m_bDataHasChanged=true; 
					m_iWorkLossField= value;
				}
			}
		}

		/// <summary>Property NCCIForce</summary>		
		public int NCCIForce
		{
			get
			{
				return m_iNCCIForce;
			}	
			set
			{
				if(m_iNCCIForce!=value)
				{
					m_bDataHasChanged=true; 
					m_iNCCIForce=value;
				}
			}
		}

		/// <summary>Property SuspressDefaultTime</summary>		
		public int SuspressDefaultTime
		{
			get
			{
				return m_iSuspressDefaultTime;
			}	
			set
			{
				if(m_iSuspressDefaultTime!=value)
				{
					m_bDataHasChanged=true; 
					m_iSuspressDefaultTime=value;
				}
			}
		}

		/// <summary>Property ContactOption</summary>		
		public int ContactOption
		{
			get
			{
				return m_iContactOption;
			}	
			set
			{
				if(m_iContactOption!=value)
				{
					m_bDataHasChanged=true; 
					m_iContactOption=value;
				}
			}
		}

		/// <summary>Property NEParentOption</summary>		
		public int NEParentOption
		{
			get
			{
				return m_iNEParentOption;
			}	
			set
			{
				if(m_iNEParentOption!=value)
				{
					m_bDataHasChanged=true; 
					m_iNEParentOption=value;
				}
			}
		}

		/// <summary>Property NEParentDefaultID</summary>		
		public int NEParentDefaultID
		{
			get
			{
				return m_iNEParentDefaultID;
			}	
			set
			{
				if(m_iNEParentDefaultID!=value)
				{
					m_bDataHasChanged=true; 
					m_iNEParentDefaultID=value;
				}
			}
		}

		/// <summary>Property NEParentOrgLevel</summary>		
		public int NEParentOrgLevel
		{
			get
			{
				return m_iNEParentOrgLevel;
			}	
			set
			{
				if(m_iNEParentOrgLevel!=value)
				{
					m_bDataHasChanged=true; 
					m_iNEParentOrgLevel=value;
				}
			}
		}

		/// <summary>Property TNParentOption</summary>		
		public int TNParentOption
		{
			get
			{
				return m_iTNParentOption;
			}	
			set
			{
				if(m_iTNParentOption!=value)
				{
					m_bDataHasChanged=true; 
					m_iTNParentOption=value;
				}
			}
		}

		/// <summary>Property TNParentDefaultID</summary>		
		public int TNParentDefaultID
		{
			get
			{
				return m_iTNParentDefaultID;
			}	
			set
			{
				if(m_iTNParentDefaultID!=value)
				{
					m_bDataHasChanged=true; 
					m_iTNParentDefaultID=value;
				}
			}
		}

		/// <summary>Property TNParentOrgLevel</summary>		
		public int TNParentOrgLevel
		{
			get
			{
				return m_iTNParentOrgLevel;
			}	
			set
			{
				if(m_iTNParentOrgLevel!=value)
				{
					m_bDataHasChanged=true; 
					m_iTNParentOrgLevel=value;
				}
			}
		}

		/// <summary>Property VAParentOption</summary>		
		public int VAParentOption
		{
			get
			{
				return m_iVAParentOption;
			}	
			set
			{
				if(m_iVAParentOption!=value)
				{
					m_bDataHasChanged=true; 
					m_iVAParentOption=value;
				}
			}
		}

		/// <summary>Property VAParentDefaultID</summary>		
		public int VAParentDefaultID
		{
			get
			{
				return m_iVAParentDefaultID;
			}	
			set
			{
				if(m_iVAParentDefaultID!=value)
				{
					m_bDataHasChanged=true; 
					m_iVAParentDefaultID=value;
				}
			}
		}

		/// <summary>Property PrintClaimNumberAtTop</summary>		
		public int PrintClaimNumberAtTop
		{
			get
			{
				return m_iPrintClaimNumberAtTop;
			}	
			set
			{
				if(m_iPrintClaimNumberAtTop!=value)
				{
					m_bDataHasChanged=true; 
					m_iPrintClaimNumberAtTop=value;
				}
			}
		}


		/// <summary>Property BaseOrgHierarchy</summary>		
		public int BaseOrgHierarchy
		{
			get
			{
				return m_iBaseOrgHierarchy;
			}	
			set
			{
				if(m_iBaseOrgHierarchy!=value)
				{
					m_bDataHasChanged=true; 
					m_iBaseOrgHierarchy=value;
				}
			}
		}

		/// Following properties were moved from the base class OptionsMajor to here 
		/// while doing FROI Integration
		/// <summary>Property CarrierOrgHierarchyLevel</summary>		
		public int CarrierOrgHierarchyLevel
		{
			get
			{
				return m_iCarrierOrgHierarchyLevel;
			}	
			set
			{
				if(m_iCarrierOrgHierarchyLevel!=value)
				{
					m_bDataHasChanged=true; 
					m_iCarrierOrgHierarchyLevel=value;
				}
			}
		}

		/// <summary>Property ClaimAdminOrgHierarchyLevel</summary>		
		public int ClaimAdminOrgHierarchyLevel
		{
			get
			{
				return m_iClaimAdminOrgHierarchyLevel;
			}	
			set
			{
				if(m_iClaimAdminOrgHierarchyLevel!=value)
				{
					m_bDataHasChanged=true; 
					m_iClaimAdminOrgHierarchyLevel=value;
				}
			}
		}

		/// <summary>Property DataChanged</summary>		
		public bool DataHasChanged
		{
			get
			{
				return m_bDataHasChanged;
			}	
		}

		/// <summary>Property VirginiaParentLevel</summary>		
		public int VirginiaParentLevel
		{
			get
			{
				return m_iVirginiaParentLevel;
			}	
			set
			{
				if(m_iVirginiaParentLevel!=value)
				{
					m_bDataHasChanged=true; 
					m_iVirginiaParentLevel=value;
				}
			}
		}

		/// <summary>Property ClaimAdminDefaultEID</summary>		
		public int ClaimAdminDefaultEID
		{
			get
			{
				return m_iClaimAdminDefaultEID;
			}	
			set
			{
				if(m_iClaimAdminDefaultEID!=value)
				{
					m_bDataHasChanged=true; 
					m_iClaimAdminDefaultEID=value;
				}
			}
		}

		/// <summary>Property CarrierDefaultEID</summary>		
		public int CarrierDefaultEID
		{
			get
			{
				return m_iCarrierDefaultEID;
			}	
			set
			{
				if(m_iCarrierDefaultEID!=value)
				{
					m_bDataHasChanged=true; 
					m_iCarrierDefaultEID=value;
				}
			}
		}

		/// <summary>Property CarrierDefaultEID</summary>		
		public int CarrierOption
		{
			get
			{
				return m_iCarrierOption ;
			}	
			set
			{
				if(m_iCarrierOption!=value)
				{
					m_bDataHasChanged=true; 
					m_iCarrierOption=value;
				}
			}
		}

		
		/// <summary>Property ClaimAdminOption</summary>		
		public int ClaimAdminOption
		{
			get
			{
				return m_iClaimAdminOption;
			}	
			set
			{
				if(m_iCarrierDefaultEID!=value)
				{
					m_bDataHasChanged=true; 
					m_iClaimAdminOption=value;
				}
			}
		}

		/// <summary>Property PrintOSHADesc</summary>		
		public int PrintOSHADesc
		{
			get
			{
				return m_iPrintOSHADesc;
			}	
			set
			{
				if(m_iPrintOSHADesc!=value)
				{
					m_bDataHasChanged=true; 
					m_iPrintOSHADesc=value;
				}
			}
		}

		/// <summary>Property EmployerLevel</summary>		
		public int EmployerLevel
		{
			get
			{
				return m_iEmployerLevel;
			}	
			set
			{
				if(m_iEmployerLevel!=value)
				{
					m_bDataHasChanged=true; 
					m_iEmployerLevel=value;
				}
			}
		}

		/// <summary>Property SelectedEID</summary>		
		public int SelectedEID
		{
			get
			{
				return m_iSelectedEID;
			}	
			set
			{
				if(m_iSelectedEID!=value)
				{
					m_bDataHasChanged=true; 
					m_iSelectedEID=value;
				}
			}
		}


		/// <summary>Property ClaimAdminTPAEID</summary>	
		public int ClaimAdminTPAEID
		{
			get
			{
				return m_iClaimAdminTPAEID;
			}	
			set
			{
				if(m_iClaimAdminTPAEID!=value)
				{
					m_bDataHasChanged=true; 
					m_iClaimAdminTPAEID=value;
				}
			}
		}


		/// <summary>Property JurisRowId</summary>		
		public int JurisRowId
		{
			get
			{
				return m_iJurisRowID;
			}	
			set
			{
				if(m_iJurisRowID!=value)
				{
					m_bDataHasChanged=true; 
					m_iJurisRowID=value;
				}
			}
		}

		#endregion

		#region Methods

		/// <summary>Reset all internal variables</summary>
		public override bool ClearObject()
		{
			base.ClearObject(); 
			m_iCarrierOrgHierarchyLevel = -1;
			m_iClaimAdminOrgHierarchyLevel = -1;
			m_bDataHasChanged = false;
			m_iSelectedEID = -1;
			m_iEmployerLevel = -1;
			m_iPrintOSHADesc = -1;
			m_iFROIPreparer = -1;
			m_iClaimAdminOption = -1;
			m_iCarrierOption = -1;
			m_iCarrierDefaultEID = -1;
			m_iClaimAdminDefaultEID = -1;
			m_iVirginiaParentLevel = -1;
			m_iAttachToClaimByDefault_FROI = -1;
			m_iNCCIForce = -1;
			m_iUseTitle = 0;
			m_iWorkLossField = -1;
			m_iSuspressDefaultTime = -1;
			m_iContactOption = -1;
			m_iNEParentOption = -1;
			m_iNEParentDefaultID = -1;
			m_iNEParentOrgLevel = -1;
			m_iTNParentOption = -1;
			m_iTNParentDefaultID = -1;
			m_iTNParentOrgLevel = -1;
			m_iVAParentOption = -1;
			m_iVAParentDefaultID = -1;
			m_iPrintClaimNumberAtTop = 0;
			m_iClaimAdminTPAEID = 0;
			m_iPersonNotifiedOption=-1;
			return true;
		}

		
		/// <summary>
		/// Loads Data from Database
		/// </summary>
		/// <param name="p_iSelectedEID">Selected EID</param>
		/// <returns>Boolean</returns>
		private bool LoadData(int p_iSelectedEID)
		{
			DbReader objRdr=null;	
			DataRow[] arrRows=null;		
			bool bDataFound = false;
			DataTable objDtTbl=null;
			try
			{
				if (!ClearObject()){ return false;}
				switch(p_iSelectedEID)
				{
					case 0:
						if (!LoadDataFromSysParms()){ return false;}
						break;
					default:			
						objRdr=DbFactory.GetDbReader(m_sConnectString,"SELECT * FROM FROI_OPTIONS WHERE SELECTED_EID = " + p_iSelectedEID);
						objDtTbl=objRdr.GetSchemaTable();
						if (objRdr!=null)
						{	
							if(objRdr.Read())
							{
								bDataFound=true;
								m_bDataHasChanged=false;
								m_iCarrierOrgHierarchyLevel= objRdr.GetInt32 ("CARRIER_ORG_LEVEL");
								m_iClaimAdminOrgHierarchyLevel=objRdr.GetInt32 ("CLMADMIN_ORG_LEVEL");
								m_iSelectedEID=objRdr.GetInt32 ("SELECTED_EID");
								m_iEmployerLevel=objRdr.GetInt32 ("EMPLOYER_LEVEL");
								m_iPrintOSHADesc=objRdr.GetInt32 ("PRINT_OSHA_DESC");
								m_iFROIPreparer=objRdr.GetInt32 ("FROI_PREPARER");
								m_iClaimAdminOption=objRdr.GetInt32 ("PRINT_CLAIM_ADMIN");
								m_iCarrierOption=objRdr.GetInt32 ("USE_DEF_CARRIER");
								m_iCarrierDefaultEID=objRdr.GetInt32 ("DEF_CARRIER_EID");
								m_iClaimAdminDefaultEID=objRdr.GetInt32 ("DEF_CLAM_ADMIN_EID");
								m_iVirginiaParentLevel =objRdr.GetInt32 ("VIRGIN_PARNT_LEVEL");
								m_iAttachToClaimByDefault_FROI =objRdr.GetInt32 ("ATTACH_TO_CLAIM");
								m_iUseTitle =objRdr.GetInt32 ("USE_TITLE");
								m_iNCCIForce =objRdr.GetInt32 ("FORCE_PRINT_NCCI");
								m_iWorkLossField =objRdr.GetInt32 ("WORK_LOSS_FIELD");
								m_iSuspressDefaultTime =objRdr.GetInt32 ("SUSPRES_DEFLT_TIME");
								m_iContactOption =objRdr.GetInt32 ("CONTACT_OPTION");
								m_iJurisRowID =objRdr.GetInt32 ("JURIS_ROW_ID");
								m_iNEParentOption =objRdr.GetInt32 ("NE_PARENT_OPTION");
								m_iNEParentDefaultID =objRdr.GetInt32 ("NE_PARENT_DEF_ID");
								m_iNEParentOrgLevel =objRdr.GetInt32 ("NE_PARENT_ORG_HIER");
								m_iTNParentOption =objRdr.GetInt32 ("TN_PARENT_OPTION");
								m_iTNParentDefaultID =objRdr.GetInt32 ("TN_PARENT_DEF_ID");
								m_iTNParentOrgLevel =objRdr.GetInt32 ("TN_PARENT_ORG_HIER");
								m_iVAParentOption =objRdr.GetInt32 ("VA_PARENT_OPTION");
								m_iVAParentDefaultID =objRdr.GetInt32 ("VA_PARENT_DEF_ID");
								m_iBaseOrgHierarchy =objRdr.GetInt32 ("BASE_LEVEL");
																  
									arrRows = objDtTbl.Select("ColumnName='PRINT_CLMNUMB_TOP'");
								if(arrRows.GetLength(0)==1)
									m_iPrintClaimNumberAtTop=objRdr.GetInt32 ("PRINT_CLMNUMB_TOP");

								arrRows = objDtTbl.Select("ColumnName='CLAIMADMIN_TPA_EID'");
								if(arrRows.GetLength(0)==1)
									m_iClaimAdminTPAEID=objRdr.GetInt32 ("CLAIMADMIN_TPA_EID");

								arrRows = objDtTbl.Select("ColumnName='PERSON_NOTFIED_OPT'");
								if(arrRows.GetLength(0)==1)
									m_iPersonNotifiedOption =objRdr.GetInt32 ("PERSON_NOTFIED_OPT");
							}
							if (!bDataFound)
							{
								if (!LoadDataFromSysParms() ) return false;
							}
						}
						break;
				}
				if (m_iNCCIForce <1 )m_iNCCIForce=2;
				if (m_iWorkLossField < 1) m_iWorkLossField = 1;
				if (m_iContactOption < 1) m_iContactOption = 2;
			}	
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("FROIOptionsMajor.LoadData.DataError",m_iClientId) , p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				objDtTbl=null;
				arrRows=null;
			}
			return true;	
		}

		//	XML Input string format for SaveData
		// <FROI>
		//		<PersonNotifiedOption>1</PersonNotifiedOption>
		//		<FROIPreparer>2</FROIPreparer>
		//		<AttachToClaimByDefaultFROI>3</AttachToClaimByDefaultFROI>
		//		<UseTitle>4</UseTitle>
		//		<WorkLossField>5</WorkLossField>
		//		<NCCIForce>6</NCCIForce>
		//		<SuspressDefaultTime>7</SuspressDefaultTime>
		//		<ContactOption>8</ContactOption>
		//		<NEParentOption>9</NEParentOption>
		//		<NEParentDefaultID>10</NEParentDefaultID>
		//		<NEParentOrgLevel>11</NEParentOrgLevel>
		//		<TNParentOption>12</TNParentOption>
		//		<TNParentDefaultID>13</TNParentDefaultID>
		//		<TNParentOrgLevel>14</TNParentOrgLevel>
		//		<VAParentOption>15</VAParentOption>
		//		<VAParentDefaultID>16</VAParentDefaultID>
		//		<PrintClaimNumberAtTop>17</PrintClaimNumberAtTop>
		//		<BaseOrgHierarchy>18</BaseOrgHierarchy>
		//		<CarrierOrgHierarchyLevel>19</CarrierOrgHierarchyLevel>
		//		<ClaimAdminOrgHierarchyLevel>20</ClaimAdminOrgHierarchyLevel>
		//		<VirginiaParentLevel>21</VirginiaParentLevel>
		//		<ClaimAdminDefaultEID>22</ClaimAdminDefaultEID>
		//		<CarrierDefaultEID>23</CarrierDefaultEID>
		//		<CarrierOption>24</CarrierOption>
		//		<ClaimAdminOption>25</ClaimAdminOption>
		//		<PrintOSHADesc>26</PrintOSHADesc>
		//		<EmployerLevel>27</EmployerLevel>
		//		<ClaimAdminTPAEID>28</ClaimAdminTPAEID>
		//	</FROI>
		/// Name		: SaveData
		/// Author		: Pankaj Chowdhury
		/// Date Created: 08/24/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment			*		Author
		/// 01/17/2004		Modified with xml input			Pankaj
		///************************************************************
		/// <summary>
		/// Save the Object data to the database
		/// </summary>
		/// <param name="p_iSelectedEID">Selected Eid</param>
		/// <param name="p_iJurisRowID">JURIS Row Id</param>
		/// <param name="p_sInputData">Input data for saving as xml string</param>
		/// <returns>Boolean True or False</returns>
		public bool SaveData(int p_iSelectedEID, int p_iJurisRowID, string p_sInputData)
		{
			DbWriter objWriter= null;
			DataRow[] arrRows=null;
			DbReader objRdr=null;
			DataTable objDtTbl=null;
			DbConnection objConn=null;
			XmlDocument objXmlDoc=null;
			XmlNodeList objFROINodLst=null;
			XmlElement objElem = null;
			
			try
			{
				if ((p_iSelectedEID <-2 )||(p_iSelectedEID ==0))
                    throw new Exception(Globalization.GetString("FROIOptionsMajor.SaveData.InvalidSelectId", m_iClientId));//sharishkumar Jira 827

				try
				{	
					//Blank Input string is allowed for default record
					if (p_sInputData.Trim()!="")
					{
						//Load the Xml Documents
						objXmlDoc =  new XmlDocument();
						objXmlDoc.LoadXml(p_sInputData);					
						objFROINodLst =  objXmlDoc.GetElementsByTagName("FROI");
						objElem = (XmlElement)objFROINodLst.Item(0); 
						//Populate the member variables
						m_iPersonNotifiedOption = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("PersonNotifiedOption")[0].InnerXml);  
						m_iFROIPreparer = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("FROIPreparer")[0].InnerXml);  
						m_iAttachToClaimByDefault_FROI = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("AttachToClaimByDefaultFROI")[0].InnerXml);
						m_iUseTitle = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("UseTitle")[0].InnerXml);
						m_iWorkLossField = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("WorkLossField")[0].InnerXml);
						m_iNCCIForce = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("NCCIForce")[0].InnerXml);
						m_iSuspressDefaultTime = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("SuspressDefaultTime")[0].InnerXml);
						m_iContactOption = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("ContactOption")[0].InnerXml);
						m_iNEParentOption = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("NEParentOption")[0].InnerXml);
						m_iNEParentDefaultID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("NEParentDefaultID")[0].InnerXml);
						m_iNEParentOrgLevel = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("NEParentOrgLevel")[0].InnerXml);
						m_iTNParentOption = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("TNParentOption")[0].InnerXml);
						m_iTNParentDefaultID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("TNParentDefaultID")[0].InnerXml);
						m_iTNParentOrgLevel = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("TNParentOrgLevel")[0].InnerXml);
						m_iVAParentOption = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("VAParentOption")[0].InnerXml);
						m_iVAParentDefaultID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("VAParentDefaultID")[0].InnerXml);
						m_iPrintClaimNumberAtTop = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("PrintClaimNumberAtTop")[0].InnerXml);
						m_iBaseOrgHierarchy = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("BaseOrgHierarchy")[0].InnerXml);
						m_iCarrierOrgHierarchyLevel = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("CarrierOrgHierarchyLevel")[0].InnerXml);
						m_iClaimAdminOrgHierarchyLevel = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("ClaimAdminOrgHierarchyLevel")[0].InnerXml);
						m_iVirginiaParentLevel = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("VirginiaParentLevel")[0].InnerXml);
						m_iClaimAdminDefaultEID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("ClaimAdminDefaultEID")[0].InnerXml);
						m_iCarrierDefaultEID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("CarrierDefaultEID")[0].InnerXml);
						m_iCarrierOption = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("CarrierOption")[0].InnerXml);
						m_iClaimAdminOption = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("ClaimAdminOption")[0].InnerXml);
						m_iPrintOSHADesc = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("PrintOSHADesc")[0].InnerXml);
						m_iEmployerLevel = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("EmployerLevel")[0].InnerXml);
						m_iClaimAdminTPAEID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("ClaimAdminTPAEID")[0].InnerXml);											
					}
				}
				catch(Exception p_objEx)
				{
                    throw new RMAppException(Globalization.GetString("FROIOptionsMajor.SaveData.XmlErr", m_iClientId), p_objEx);//sharishkumar Jira 827
				}

				objWriter=DbFactory.GetDbWriter(m_sConnectString );
				objWriter.Tables.Add("FROI_OPTIONS");
				objRdr=DbFactory.GetDbReader  (m_sConnectString,  "SELECT * FROM FROI_OPTIONS WHERE SELECTED_EID = "+ 
					p_iSelectedEID + " AND JURIS_ROW_ID = " + p_iJurisRowID);
				if(objRdr!=null)
				{
					if(objRdr.Read())  
					{
						// Existing Record, add filter for the update query.
						objWriter.Where.Add(" SELECTED_EID=" + p_iSelectedEID + " AND JURIS_ROW_ID=" + p_iJurisRowID);
					}
					else
					{
						objWriter.Fields.Add("ADDED_BY_USER", m_sUserLoginName );						
						objWriter.Fields.Add("DTTM_RCD_ADDED",  DateTime.Now.ToString("yyyyMMddHHmmss") );
					}					
				}										
				objWriter.Fields.Add("CARRIER_ORG_LEVEL", m_iCarrierOrgHierarchyLevel);
				objWriter.Fields.Add("CLMADMIN_ORG_LEVEL", m_iClaimAdminOrgHierarchyLevel);
				objWriter.Fields.Add("SELECTED_EID", p_iSelectedEID);
				objWriter.Fields.Add("EMPLOYER_LEVEL", m_iEmployerLevel);
				objWriter.Fields.Add("PRINT_OSHA_DESC", m_iPrintOSHADesc);
				objWriter.Fields.Add("FROI_PREPARER", m_iFROIPreparer);
				objWriter.Fields.Add("PRINT_CLAIM_ADMIN", m_iClaimAdminOption);
				objWriter.Fields.Add("USE_DEF_CARRIER", m_iCarrierOption);
				objWriter.Fields.Add("DEF_CARRIER_EID", m_iCarrierDefaultEID);
				objWriter.Fields.Add("DEF_CLAM_ADMIN_EID", m_iClaimAdminDefaultEID);
				objWriter.Fields.Add("VIRGIN_PARNT_LEVEL", m_iVirginiaParentLevel);
				objWriter.Fields.Add("ATTACH_TO_CLAIM", m_iAttachToClaimByDefault_FROI );
				objWriter.Fields.Add("USE_TITLE", m_iUseTitle );
				objWriter.Fields.Add("DTTM_RCD_LAST_UPD", DateTime.Now.ToString("yyyyMMddHHmmss"));
				objWriter.Fields.Add("UPDATED_BY_USER", m_sUserLoginName );
				objWriter.Fields.Add("FORCE_PRINT_NCCI", m_iNCCIForce);
				objWriter.Fields.Add("WORK_LOSS_FIELD", m_iWorkLossField);
				objWriter.Fields.Add("SUSPRES_DEFLT_TIME", m_iSuspressDefaultTime);
				objWriter.Fields.Add("CONTACT_OPTION", m_iContactOption);
				objWriter.Fields.Add("JURIS_ROW_ID", p_iJurisRowID);
				objWriter.Fields.Add("NE_PARENT_OPTION", m_iNEParentOption);
				objWriter.Fields.Add("NE_PARENT_DEF_ID", m_iNEParentDefaultID);
				objWriter.Fields.Add("NE_PARENT_ORG_HIER", m_iNEParentOrgLevel);
				objWriter.Fields.Add("TN_PARENT_OPTION", m_iTNParentOption);
				objWriter.Fields.Add("TN_PARENT_DEF_ID", m_iTNParentDefaultID);
				objWriter.Fields.Add("TN_PARENT_ORG_HIER", m_iTNParentOrgLevel);
				objWriter.Fields.Add("VA_PARENT_OPTION", m_iVAParentOption);
				objWriter.Fields.Add("VA_PARENT_DEF_ID", m_iVAParentDefaultID);
				objWriter.Fields.Add("BASE_LEVEL",  m_iBaseOrgHierarchy );

				objDtTbl= objRdr.GetSchemaTable();  
					
				arrRows = objDtTbl.Select("ColumnName='PRINT_CLMNUMB_TOP'");
				if(arrRows.GetLength(0)==1)
					objWriter.Fields.Add("PRINT_CLMNUMB_TOP", Convert.ToInt32(m_iPrintClaimNumberAtTop));

				arrRows = objDtTbl.Select("ColumnName='CLAIMADMIN_TPA_EID'");
				if(arrRows.GetLength(0)==1)
					objWriter.Fields.Add("CLAIMADMIN_TPA_EID", Convert.ToInt32(m_iClaimAdminTPAEID));

				objWriter.Execute();
				m_bDataHasChanged=false;
			}
			catch(RMAppException p_objEx)
			{ 
				throw p_objEx ;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("FROIOptionsMajor.SaveData.DataError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if (objRdr != null)	objRdr.Dispose();
				if(objConn!=null)objConn.Dispose();
				arrRows=null;
				objWriter =null;
				objDtTbl=null;
				objXmlDoc=null;
				objFROINodLst=null;
				objElem = null;
			}
			return true;
		}

		/// <summary>
		/// Load FROI_OPTIONS
		/// </summary>
		/// <param name="p_iAssignedDept">Department ID</param>
		/// <returns>Boolean</returns>
        internal bool LoadDataByAssignedDepartment(int p_iAssignedDept)
		{
			StringBuilder sbOrgs= null;
			StringBuilder sbSQL = null;
			DbReader objRdr=null;
			try
			{
				sbOrgs= new StringBuilder() ;
				sbSQL = new StringBuilder();

				// the case of lAssignedDept<-1 has been brought to the following statement
				//-1 is the default record in FROI_OPTIONS. 'something very wrong
				if ( !ClearObject()|| p_iAssignedDept<-1)return false;

				switch(p_iAssignedDept)
				{
					case 0:
						//as of 07/15/2002 there is no provision for a valid client id of 0
						return false;
					default:
						sbOrgs.Append(p_iAssignedDept.ToString() ) ;
						for(int iCtr =1005; iCtr <= 1011; iCtr++)
						{
                            sbOrgs.Append (",");
                            sbOrgs.Append(Functions.GetOrgParentByDepartID(m_sConnectString, p_iAssignedDept, iCtr, m_iClientId)); //sharishkumar Jira 827
						}

						sbSQL.Append ("SELECT FROI_OPTIONS.*,ENTITY.ENTITY_TABLE_ID FROM FROI_OPTIONS,ENTITY");
						sbSQL.Append (" WHERE FROI_OPTIONS.SELECTED_EID IN (") ;
						sbSQL.Append(sbOrgs.ToString());
						sbSQL.Append(") AND FROI_OPTIONS.SELECTED_EID = ENTITY.ENTITY_ID");
						sbSQL.Append(" AND FROI_OPTIONS.JURIS_ROW_ID = ") ;
						sbSQL.Append(m_iJurisRowID);
						sbSQL.Append(" ORDER BY ENTITY.ENTITY_TABLE_ID DESC");

						objRdr=DbFactory.GetDbReader(m_sConnectString,sbSQL.ToString() );
						if (objRdr!=null)
						{
							if (objRdr.Read())
							{
								AssignData(objRdr);
								return true;
							}
						}
						if(objRdr!=null)objRdr.Dispose();
						
						//check second method
						sbSQL.Remove(0,sbSQL.Length );
						sbSQL.Append ("SELECT FROI_OPTIONS.*,ENTITY.ENTITY_TABLE_ID FROM FROI_OPTIONS,ENTITY");
						sbSQL.Append (" WHERE FROI_OPTIONS.SELECTED_EID IN (");
						sbSQL.Append (sbOrgs.ToString());
						sbSQL.Append (") AND FROI_OPTIONS.SELECTED_EID = ENTITY.ENTITY_ID");
						sbSQL.Append (" AND FROI_OPTIONS.JURIS_ROW_ID = -1 ORDER BY ENTITY.ENTITY_TABLE_ID DESC");

						objRdr=DbFactory.GetDbReader(m_sConnectString,sbSQL.ToString() );
						if (objRdr!=null)
						{					
							if (objRdr.Read())
							{
								AssignData(objRdr);
								return true;
							}
						}
						if(objRdr!=null)objRdr.Dispose();

						//check third method
						sbSQL.Remove(0,sbSQL.Length );
						sbSQL.Append ("SELECT FROI_OPTIONS.* FROM FROI_OPTIONS WHERE FROI_OPTIONS.SELECTED_EID = -1");
						sbSQL.Append (" AND FROI_OPTIONS.JURIS_ROW_ID = ");
						sbSQL.Append (m_iJurisRowID );

						objRdr=DbFactory.GetDbReader(m_sConnectString,sbSQL.ToString() );
						if (objRdr!=null)
						{	
							if (objRdr.Read())
							{
								AssignData(objRdr);
								return true;
							}
						}
						if(objRdr!=null)objRdr.Dispose();

						//check fourth method
						sbSQL.Remove(0,sbSQL.Length );
						sbSQL.Append ("SELECT FROI_OPTIONS.* FROM FROI_OPTIONS");
						sbSQL.Append (" WHERE FROI_OPTIONS.SELECTED_EID = -1");
						sbSQL.Append (" AND FROI_OPTIONS.JURIS_ROW_ID = -1");

						objRdr=DbFactory.GetDbReader(m_sConnectString,sbSQL.ToString() );
						if (objRdr!=null)
						{	
							if (objRdr.Read())
							{
								AssignData(objRdr);
								return true;
							}
						}
						if(objRdr!=null)objRdr.Dispose();

						//If we have gotten to here, there is no valid FROI options record.
						//Create a default value record and use it.
						if (CreateDefaultRecord()!=-1)
							return false;
			
						if(m_iNCCIForce < 1) m_iNCCIForce = 2;
					    if (m_iWorkLossField < 1) m_iWorkLossField = 1;
					    if (m_iContactOption < 1) m_iContactOption = 2;                        
						return true;
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("FROIOptionsMajor.LoadDataByAssignedDepartment.DataLoadError",m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				sbSQL=null;
				sbOrgs =null;
			}
		}


		/// <summary>Assigns values to the instance variables</summary>
		/// <param name="p_objRdr">DbReader</param>
		private void AssignData(DbReader p_objRdr)
		{
			DataRow[] arrRows=null;
			DataTable objDtTbl=null;
			try
			{
						m_iCarrierOrgHierarchyLevel= p_objRdr.GetInt32("CARRIER_ORG_LEVEL") ;
						m_iClaimAdminOrgHierarchyLevel= p_objRdr.GetInt32("CLMADMIN_ORG_LEVEL") ;
						m_bDataHasChanged= false;
						m_iSelectedEID= p_objRdr.GetInt32("SELECTED_EID") ;
						m_iEmployerLevel= p_objRdr.GetInt32("EMPLOYER_LEVEL");
						m_iPrintOSHADesc= p_objRdr.GetInt32("PRINT_OSHA_DESC") ;
						m_iFROIPreparer= p_objRdr.GetInt32("FROI_PREPARER") ;
						m_iClaimAdminOption= p_objRdr.GetInt32("PRINT_CLAIM_ADMIN") ;
						m_iCarrierOption= p_objRdr.GetInt32("USE_DEF_CARRIER") ;
						m_iCarrierDefaultEID= p_objRdr.GetInt32("DEF_CARRIER_EID") ;
						m_iClaimAdminDefaultEID= p_objRdr.GetInt32("DEF_CLAM_ADMIN_EID");						
						m_iVirginiaParentLevel= p_objRdr.GetInt32("VIRGIN_PARNT_LEVEL") ;
						m_iAttachToClaimByDefault_FROI= p_objRdr.GetInt32("ATTACH_TO_CLAIM") ;
						m_iUseTitle= p_objRdr.GetInt32("USE_TITLE") ;
						m_iNCCIForce= p_objRdr.GetInt32("FORCE_PRINT_NCCI") ;
						m_iWorkLossField= p_objRdr.GetInt32("WORK_LOSS_FIELD") ;
						m_iSuspressDefaultTime= p_objRdr.GetInt32("SUSPRES_DEFLT_TIME") ;
						m_iContactOption= p_objRdr.GetInt32("CONTACT_OPTION") ;

						objDtTbl= p_objRdr.GetSchemaTable(); 
						arrRows = objDtTbl.Select("ColumnName='JURIS_ROW_ID'");
						if(arrRows.GetLength(0)==1)
							m_iJurisRowID = p_objRdr.GetInt32("JURIS_ROW_ID") ;

						arrRows = objDtTbl.Select("ColumnName='PRINT_CLMNUMB_TOP'");
						if(arrRows.GetLength(0)==1)
							m_iPrintClaimNumberAtTop= p_objRdr.GetInt32("PRINT_CLMNUMB_TOP") ;

						arrRows = objDtTbl.Select("ColumnName='CLAIMADMIN_TPA_EID'");
						if(arrRows.GetLength(0)==1)
							m_iClaimAdminTPAEID = p_objRdr.GetInt32("CLAIMADMIN_TPA_EID") ;

						arrRows = objDtTbl.Select("ColumnName='NE_PARENT_OPTION'");
						if(arrRows.GetLength(0)==1)
						{
							m_iNEParentOption = p_objRdr.GetInt32("NE_PARENT_OPTION");
							m_iNEParentDefaultID = p_objRdr.GetInt32("NE_PARENT_DEF_ID");
							m_iNEParentOrgLevel = p_objRdr.GetInt32("NE_PARENT_ORG_HIER");
							m_iTNParentOption = p_objRdr.GetInt32("TN_PARENT_OPTION");
							m_iTNParentDefaultID = p_objRdr.GetInt32("TN_PARENT_DEF_ID");
							m_iTNParentOrgLevel = p_objRdr.GetInt32("TN_PARENT_ORG_HIER");
							m_iVAParentOption = p_objRdr.GetInt32("VA_PARENT_OPTION");
							m_iVAParentDefaultID = p_objRdr.GetInt32("VA_PARENT_DEF_ID");
						}

						arrRows = objDtTbl.Select("ColumnName='PERSON_NOTFIED_OPT'");
						if(arrRows.GetLength(0)==1)
							m_iNEParentOption = p_objRdr.GetInt32("PERSON_NOTFIED_OPT");
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("FROIOptionsMajor.AssignData.AssignError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				arrRows=null;
				objDtTbl=null;
			}
		}


		/// <summary>Creates Default Record in case of no FROI Options</summary>
		private int CreateDefaultRecord()
		{
			DbReader objRdr=null;
			FROIOptionsMajor objFroi = null;
			DbReader objRdrEnt =null;
			try
			{				
				objFroi =new FROIOptionsMajor(m_sConnectString, m_sUserLoginName, m_iSelectedEID,m_iClientId);//sharishkumar Jira 827

				objRdr=DbFactory.GetDbReader(m_sConnectString ,"SELECT PRINT_CLAIMS_ADMIN,PRINT_OSHA_DESC,HIERARCHY_LEVEL, USE_DEF_CARRIER, FROI_PREPARER FROM SYS_PARMS");
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						objFroi.m_iEmployerLevel = Conversion.GetOrgHierarchyLevel(objRdr.GetString("HIERARCHY_LEVEL") ) ;
                        objFroi.m_iFROIPreparer = objRdr.GetInt32("FROI_PREPARER");
						objFroi.m_iClaimAdminOption = objRdr.GetInt32("PRINT_CLAIMS_ADMIN");
						if (objFroi.m_iClaimAdminOption ==4)
						{
							objRdrEnt=DbFactory.GetDbReader(m_sConnectString ,"SELECT * FROM ENTITY, GLOSSARY WHERE ((ENTITY.ENTITY_TABLE_ID = GLOSSARY.TABLE_ID) AND (GLOSSARY.SYSTEM_TABLE_NAME = 'WC_DEF_CLAIM_ADMIN')) AND ENTITY.DELETED_FLAG = 0");
							if (objRdrEnt!=null )
							{
								if (objRdrEnt.Read())
								{
									objFroi.m_iClaimAdminDefaultEID =objRdrEnt.GetInt32("ENTITY_ID");                                    
								}
							}
						}
						objFroi.m_iPrintOSHADesc = objRdr.GetInt32("PRINT_OSHA_DESC");
						objFroi.m_iUseTitle =1;
						objFroi.m_iSelectedEID =-1;
						objFroi.m_iJurisRowID=-1;
					}
				}
				objFroi.m_iJurisRowID =-1;
				if (objFroi.SaveData(-1,-1,""))
					return -1;
				else 
					return 0;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OptionsMajor.CreateDefaultRecord.AssigError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				if(objRdrEnt!=null)objRdrEnt.Dispose();
				objFroi=null;
			}
		}

		#endregion

	}//class end
}//namespace end
