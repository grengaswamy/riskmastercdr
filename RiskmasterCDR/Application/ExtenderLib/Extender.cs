using System;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using System.Text;
using System.Xml;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Data.Common;
using Riskmaster.Settings;

namespace Riskmaster.Application.ExtenderLib
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   24th,Aug 2004
	///Purpose :   Base class for FROIExtender and JURISExtender. All common functionalities
	///of these two classes have been put in this common base class
	/// </summary>
	public class Extender
	{
		#region Variable Declarations
		
		/// <summary>
		/// Variable to store User Name
		/// </summary>
		protected string m_sUserName="";

        /// <summary>
        /// Gets or sets the FDF document file.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        /// <value>
        /// The FDF document file.
        /// </value>
        public string FDFDocFile { get; set; }

        /// <summary>
        /// Gets or sets the PDF filename.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        /// <value>
        /// The PDF filename.
        /// </value>
        public string PDFFilename { get; set; }

		/// <summary>
		/// Variable to store password
		/// </summary>
		protected string m_sPassword="";

		/// <summary>
		/// Variable to store connection dsn
		/// </summary>
		protected string m_sDsnName="";

		/// <summary>
		/// Connection string connecting to the database
		/// </summary>
		protected string m_sConnectString="";

		/// <summary>
		/// Internal PIEmployee variable
		/// </summary>
		internal PiEmployee m_objPiEmp=null;

		/// <summary>
		/// Internal variable storing event object
		/// </summary>
		internal Event m_objEvent=null;

		/// <summary>
		/// Entity Company attached to this class
		/// </summary>
		internal Entity m_objCompany=null;

		/// <summary>
		/// Claim attached to this class
		/// </summary>
		internal Claim m_objClaim=null;

        /// <summary>
        /// The m_obj jur.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        internal Jurisdictionals m_objJur = null;

        /// <summary>
        /// The m_obj code license.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        internal JurisLicenseOrCode m_objJurisLicenseOrCode = null;

		/// <summary>
		/// Entity Administrator variable
		/// </summary>
		internal Entity m_objAdministrator=null;

		/// <summary>
		/// Variable for CompanyEID
		/// </summary>
		internal int m_iCompanyEID=0;

		/// <summary>
		/// Entity Carrier
		/// </summary>
		internal Entity m_objCarrier=null;

		/// <summary>
		/// XML String from USER_PREF_XML
		/// </summary>
		internal string m_sXMLText="";

		/// <summary>
		/// SelfInsured Class Variable
		/// </summary>
		internal EntityXSelfInsured m_objSelfInsured=null;

		/// <summary>
		/// Protected variable to store Name
		/// </summary>
		protected string m_sName="";
		
		/// <summary>
		/// Protected variable to store Title
		/// </summary>
		protected string m_sTitle="";

		/// <summary>
		/// Protected variable to store Phone
		/// </summary>
		protected string m_sPhone="";

		/// <summary>
		/// Internal Supplementals Object
		/// </summary>
		internal Supplementals m_objSupp=null;

		/// <summary>
		/// Patient Entity
		/// </summary>
		internal Patient m_objPatient=null;

		/// <summary>
		/// Claim id variable
		/// </summary>
		internal int m_iClaimID=0;

		/// <summary>
		/// Internal variable for Event ID
		/// </summary>
		internal int m_iEventID=0;

        protected int m_iClientId = 0;//sharishkumar Jira 827

        public Jurisdictionals objJur { get; set; } //Added for Froi Migration- Mits 33585,33586,33587

		/// <summary>
		/// Enumerator for existence of XML in USER_PREF_XML
		/// </summary>
		public enum enumXMLExists : int
		{
			iNoFieldData = 2,
			iNoOptions = 4,
			iOptions = 8,
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Base class constructor for FROIExtender & JURISExtender
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		/// <param name="p_sConnectString">Connection String</param>
        public Extender(string p_sUserName, string p_sPassword, string p_sDsnName, string p_sConnectString, int p_iClientId)//sharishkumar Jira 827
		{
			//populate the internal variables
			m_sUserName=p_sUserName;
			m_sPassword=p_sPassword;
			m_sDsnName= p_sDsnName;
			m_sConnectString= p_sConnectString;
            m_iClientId = p_iClientId;//sharishkumar Jira 827
		}
       
		/// Name		: Extender
		/// Author		: Pankaj Chowdhury
		/// Date Created: 01/17/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 01/17/2004			Added			Pankaj
		///************************************************************
		/// <summary>
		/// Constructor with User Name, Password and Connection String
		/// </summary>
		/// <param name="p_sUserName">User Name</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sConnectString">Connection String</param>
        public Extender(string p_sUserName, string p_sPassword, string p_sConnectString, int p_iClientId)
		{
            m_iClientId = p_iClientId;//sharishkumar Jira 827
			//populate the internal variables
			m_sUserName=p_sUserName;
			m_sPassword=p_sPassword;			
			m_sConnectString= p_sConnectString;    
		}

		/// <summary>
		/// Default constructor for accessing functions for which instance might not be required to create
		/// This can be used in case we don't want to create a datamodelfactory in instantiating or don't have
		/// userid password and still want to access some function which are not dependent on instance data.
		/// Most of such functions are already static, still given this provision.
		/// </summary>
		public Extender(){}

		#endregion

		#region Public Properties

		/// <summary>
		/// Property UserPrefXML. Maps to XML_TEXT in USER_PREF_XML
		/// </summary>
		public string UserPrefXML
		{
			get
			{
				return m_sXMLText;
			}
			set
			{
				m_sXMLText=value;

			}
		}

        // akaushk5 Commnetd for MITS 37849 Starts
        //public string sIncidentDesc_objExt { get; set; }//Added for Froi Migration- Mits 33585,33586,33587
        // akaushk5 Commnetd for MITS 37849 Ends

		#endregion

		#region Methods
		/// <summary>
		/// Gets pay period on the basis of Pay type code
		/// </summary>
		/// <param name="p_sPayTypeCode">String Paytype code</param>
		/// <returns>Pay Period string</returns>
		protected string GetPayPeriodBase(string p_sPayTypeCode)
		{
			if(p_sPayTypeCode.Trim()==""||p_sPayTypeCode==null)
				return "";
			switch(p_sPayTypeCode.ToUpper())
			{
				case "B":
					return "WEEK";
				default:
					return "";
			}
		}


		/// <summary>
		/// Gets Agency ID and Phone number
		/// </summary>
		/// <returns>PHONE from WCP_STATE_AGENCY</returns>
		protected string GetStateAgencyPhoneNumberBase()
		{
			int iTableID= 0;
			int iFuncID=0;
			int iAgencyID=0;
			int iLen = 0;
			DbReader objRdr=null;
			string sStAgPhNo ="" ;
			StringBuilder sbSQL= null;
			Common.LocalCache objChache=null;			
			try
			{
				if(m_objPiEmp==null)
					return "";

				sbSQL= new StringBuilder();
				//Get table ID
				objChache = new Common.LocalCache(m_sConnectString,m_iClientId);
				iTableID= objChache.GetTableId("CA_AGENCY_FUNCTION");
				if (objChache!=null) objChache.Dispose();

				if (iTableID<1) return "";
				sbSQL.Append ("SELECT DISTINCT CODES.CODE_ID FROM CODES, CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
				sbSQL.Append (" AND CODES.TABLE_ID = ");
				sbSQL.Append (iTableID );
				sbSQL.Append (" AND CODES_TEXT.CODE_DESC = 'Information and Assistance Unit' AND DELETED_FLAG = 0");

				objRdr=DbFactory.GetDbReader(m_sConnectString ,sbSQL.ToString());
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iFuncID = objRdr.GetInt32("CODE_ID") ;
					}
				}
				if(objRdr!=null)objRdr.Dispose();

				if ((iFuncID<1) || (m_objPiEmp.PiEntity == null))return "";
				sbSQL.Remove(0,sbSQL.Length );

				iLen = m_objPiEmp.PiEntity.ZipCode.Length;
                if(iLen > 5 )
					iLen = 5;
				// Get Agency_ID on the basis of the previous CODE_ID
				sbSQL.Append ("SELECT AGENCY_ID FROM WCP_AGNCY_SERV_ZIP WHERE FUNCTION_CODE_ID = ");
				sbSQL.Append (iFuncID);
				sbSQL.Append (" AND ZIP_CODE  = '");
				sbSQL.Append (m_objPiEmp.PiEntity.ZipCode.Substring(0,iLen));
				sbSQL.Append("' AND DELETED_FLAG = 0") ;
				objRdr=DbFactory.GetDbReader(m_sConnectString ,sbSQL.ToString());
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iAgencyID  = objRdr.GetInt32("AGENCY_ID") ;
					}
				}
				if(objRdr!=null)objRdr.Dispose();

				if (iAgencyID<1)return "";
				sbSQL.Remove(0,sbSQL.Length );

				//Get PHone from WCP_STATE_AGENCY on the basis of Agency ID
				sbSQL.Append ("SELECT PHONE FROM WCP_STATE_AGENCY WHERE agency_id = ");
				sbSQL.Append (iAgencyID);
				sbSQL.Append (" AND DELETED_FLAG = 0"); 
				objRdr=DbFactory.GetDbReader(m_sConnectString ,sbSQL.ToString());
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						sStAgPhNo  = objRdr.GetString("PHONE") ;
					}
				}

				return sStAgPhNo;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("Extender.GetStateAgencyPhoneNumber.AgencyErr", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				if (objChache!=null)objChache.Dispose();
				sbSQL=null;
			}
		}

		/// <summary>
		/// Gets hourly rate for the Employee
		/// </summary>
		/// <param name="p_sPayTypeCode">Pay Type Code</param>
		/// <returns>Hourly Rate</returns>
		public double GetHourlyRate(string p_sPayTypeCode)
		{

			//Check if PiEmployee has not been populated
			if ((m_objPiEmp==null)||(p_sPayTypeCode.Trim()=="")) return 0;

			if (m_objPiEmp.HourlyRate !=0.0 )
				return m_objPiEmp.HourlyRate;
			else if( (m_objPiEmp.WeeklyRate!=0.0 )&& (m_objPiEmp.WeeklyHours !=0.0 ))
				//divide by zero protection
				return m_objPiEmp.WeeklyRate / m_objPiEmp.WeeklyHours ;
			else
			{
				switch(p_sPayTypeCode.ToUpper())
				{
					case "W":		//Weekly Rate
						return m_objPiEmp.PayAmount/40; 
					case "H":		//Hourly Rate
						return m_objPiEmp.PayAmount ;
					case "B":		//Bi-Weekly Rate
						return m_objPiEmp.PayAmount/80;
					case "M":		//Monthly Rate
						return (m_objPiEmp.PayAmount *12)/2080;
					case "S":		//"S" added for as shipped product
						return m_objPiEmp.PayAmount/ 2080;
					case "SA":		//Salary Rate
						return m_objPiEmp.PayAmount/ 2080;
					case "D":		//Daily Rate
						return m_objPiEmp.PayAmount /8;
					case "SM":		//Semi-Monthly Rate
						return (m_objPiEmp.PayAmount * 24) /2080;
					default:
						return 0;
				}
			}
		}


		//Added for Froi Migration- Mits 33585,33586,33587

        public string FormatDate(string date) 
        {
            return Conversion.GetDBDateFormat(date, "MM/dd/yyyy");
        }

        /// <summary>
        /// Formats the date with zeros.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public string FormatDateWithZeros(string date)
        {
            return this.FormatDate(date);
        }

        /// <summary>
        /// Ases the long.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public int AsLong(object value)
        { 
            bool success;
            int retValue;
            retValue = Conversion.CastToType<int>(value.ToString(), out success);
            return retValue;
        }

        /// <summary>
        /// Ss the get state code.
        /// </summary>
        /// <param name="lStateID">The l state identifier.</param>
        /// <returns></returns>
        public string sGetStateCode(string lStateID)
        {
            if (string.IsNullOrEmpty(lStateID) || lStateID.Equals("0"))
                return string.Empty;

			//Modified for FROI Migration - Starts
             Common.LocalCache objChache = null;

            if (m_objClaim != null)
            {
                return this.m_objClaim.Context.LocalCache.GetStateCode(Conversion.ConvertStrToInteger(lStateID));
            }
            else
            {
                objChache = new Common.LocalCache(m_sConnectString,m_iClientId);
                return objChache.GetStateCode(Conversion.ConvertStrToInteger(lStateID));
              
            }
			//Modified for FROI Migration - Ends

        }

        /// <summary>
        /// Ss the get state desc.
        /// </summary>
        /// <param name="stateId">The state identifier.</param>
        /// <returns></returns>
        public string sGetStateDesc(int stateId)
        {
            string stateName = string.Empty;
            string stateAbbr = string.Empty;
            if (stateId != default(int))
            {
			//Modified for FROI Migration - Starts
                Common.LocalCache objChache = null;
                if (m_objClaim != null)
                {
                    this.m_objClaim.Context.LocalCache.GetStateInfo(stateId, ref stateAbbr, ref stateName);
                }
                else
                {
                    objChache = new Common.LocalCache(m_sConnectString,m_iClientId);
                    objChache.GetStateInfo(stateId, ref stateAbbr, ref stateName);
                }
				//Modified for FROI Migration - Ends
            }
            return stateName;
        }

        /// <summary>
        /// Ss the get state agency phone number_ ca.
        /// </summary>
        /// <returns></returns>
        public string sGetStateAgencyPhoneNumber_CA()
        {
            return this.GetStateAgencyPhoneNumberBase();
        }
        /// <summary>
        /// Ss the get code desc.
        /// </summary>
        /// <param name="codeId">The code identifier.</param>
        /// <returns></returns>
        public string sGetCodeDesc(int codeId)
        {
		//Modified for FROI Migration - Starts
            Common.LocalCache objChache = null;
            if (m_objClaim != null)
            {
                return this.m_objClaim.Context.LocalCache.GetCodeDesc(codeId);
            }
            else
            {
                objChache = new Common.LocalCache(m_sConnectString,m_iClientId);
                return objChache.GetCodeDesc(codeId);
            }
			//Modified for FROI Migration - Ends
        }

        /// <summary>
        /// Ds the get hourly rate.
        /// </summary>
        /// <param name="p_sPayTypeCode">The P_S pay type code.</param>
        /// <returns></returns>
        public double dGetHourlyRate(string p_sPayTypeCode)
        {
            return this.GetHourlyRate(p_sPayTypeCode);
        }

        /// <summary>
        /// Ss the get short code.
        /// </summary>
        /// <param name="codeId">The code identifier.</param>
        /// <returns></returns>
        public string sGetShortCode(int codeId)
        {
            if (!object.ReferenceEquals(this.m_objClaim, null))
            {
                return this.m_objClaim.Context.LocalCache.GetShortCode(codeId);
            }
            else
            {
                using (LocalCache localCache = new Common.LocalCache(m_sConnectString, m_iClientId))
                {
                    return localCache.GetShortCode(codeId);
                }
            }
        }

        private List<DbDataRecord> lstRecords = null;
        private int currentIndex = 0;

        public string PDFCRLF = Environment.NewLine;
        /// <summary>
        /// Creates the record set.
        /// </summary>
        /// <param name="sql">The SQL.</param>
        /// <returns></returns>
        public int CreateRecordSet(string sql)
        {
            using (DbReader dbReader = DbFactory.GetDbReader(this.m_sConnectString, sql))
            {
                lstRecords = dbReader.Cast<DbDataRecord>().ToList();
            }
            return currentIndex;
        }

        /// <summary>
        /// Dbeofs the specified count.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <returns></returns>
        public bool DBEOF(int count)
        {
            if (lstRecords == null || lstRecords.Count == 0)
                return true;

            if (lstRecords.Count <= currentIndex)
            {
                currentIndex = lstRecords.Count - 1;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the data string.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="FieldName">Name of the field.</param>
        /// <returns></returns>
        public string GetDataString(int count, object FieldName)
        {
            if (lstRecords.Count == 0)
            {
                return string.Empty; 
            }
            DbDataRecord currentrecord = lstRecords.ElementAt(currentIndex);
            if (FieldName.GetType() == typeof(short))
            {
                short? fieldindex = FieldName as short?;
                if (fieldindex.HasValue)
                {
                    return currentrecord.GetValue(fieldindex.Value - 1).ToString();
                }
            }
            return currentrecord.GetValue(currentrecord.GetOrdinal(FieldName.ToString())).ToString();
        }

        /// <summary>
        /// Gets the data string.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="FieldName">Name of the field.</param>
        /// <returns></returns>
        public string GetDataString(int count, string FieldName)
        {
            if (lstRecords.Count == 0)
            {
                return string.Empty;
            }
            DbDataRecord currentrecord = lstRecords.ElementAt(currentIndex);
            return currentrecord.GetValue(currentrecord.GetOrdinal(FieldName.ToString())).ToString();
        }

        /// <summary>
        /// Gets the data long.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="FieldName">Name of the field.</param>
        /// <returns></returns>
        public int GetDataInteger(int count, object FieldName)
        {
            if (lstRecords.Count == 0)
            {
                return default(int);
            }

            DbDataRecord currentrecord = lstRecords.ElementAt(currentIndex);
            if (FieldName.GetType() == typeof(short))
            {
                short? fieldindex = FieldName as short?;
                if (fieldindex.HasValue)
                {
                    return Conversion.ConvertStrToInteger(currentrecord.GetValue(fieldindex.Value - 1).ToString());
                }
            }
            return Conversion.ConvertStrToInteger(currentrecord.GetValue(currentrecord.GetOrdinal(FieldName.ToString())).ToString());
        }

        /// <summary>
        /// Gets the data long.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="FieldName">Name of the field.</param>
        /// <returns></returns>
        public int GetDataLong(int count, object FieldName)
        {
            if (lstRecords.Count == 0)
            {
                return default(int);
            }

            DbDataRecord currentrecord = lstRecords.ElementAt(currentIndex);
            if (FieldName.GetType() == typeof(short))
            {
                short? fieldindex = FieldName as short?;
                if (fieldindex.HasValue)
                {
                    return Conversion.ConvertStrToInteger(currentrecord.GetValue(fieldindex.Value -1).ToString());
                }
            }
            return Conversion.ConvertStrToInteger(currentrecord.GetValue(currentrecord.GetOrdinal(FieldName.ToString())).ToString());
        }

        public double GetDataDouble(int count, object FieldName)
        {
            if (lstRecords.Count == 0)
            {
                return default(double);
            }

            DbDataRecord currentrecord = lstRecords.ElementAt(currentIndex);
            if (FieldName.GetType() == typeof(short))
            {
                short? fieldindex = FieldName as short?;
                if (fieldindex.HasValue)
                {
                    return Conversion.ConvertStrToDouble(currentrecord.GetValue(fieldindex.Value - 1).ToString());
                }
            }
            return Conversion.ConvertStrToDouble(currentrecord.GetValue(currentrecord.GetOrdinal(FieldName.ToString())).ToString());
        }


        /// <summary>
        /// Closes the recordset.
        /// </summary>
        /// <param name="count">The count.</param>
        public void closeRecordset(int count)
        { 
            this.lstRecords = null;
            this.currentIndex = 0;
        }

        /// <summary>
        /// Is the calculate years.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public int iCalculateYears(string startDate, string endDate)
        {
            //return Utilities.GetDateDiffInYears(startDate, endDate);
            return Utilities.GetDateDiffInYears(Conversion.GetDate(startDate), Conversion.GetDate(endDate), 0);
        }

        public int dbmovenext(int count)
        {
            return ++currentIndex;
        }

        /// <summary>
        /// Gets the rm application.
        /// </summary>
        /// <value>
        /// The rm application.
        /// </value>
        public DataModelContextExtension rmApp
        {
            get {
                return new DataModelContextExtension(this.m_objEvent.Context);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public class DbFactoryExtension
        {
            /// <summary>
            /// Ds the b_ get database make.
            /// </summary>
            /// <param name="connection">The connection.</param>
            /// <returns></returns>
            public int DB_GetDBMake(Riskmaster.Db.DbConnection connection)
            {
                if (connection.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class DataModelContextExtension
        {
            private Context context;

            /// <summary>
            /// Gets the internal settings.
            /// </summary>
            /// <value>
            /// The internal settings.
            /// </value>
            public SysParms InternalSettings
            {
                get
                {
                    return this.context.InternalSettings;
                }
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="DataModelContextExtension"/> class.
            /// </summary>
            /// <param name="context">The context.</param>
            public DataModelContextExtension(Context context)
            {
                this.context = context;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="DataModelContextExtension"/> class.
            /// </summary>
            public DataModelContextExtension()
            {
                // TODO: Complete member initialization
            }

            /// <summary>
            /// Gets the i navigation.
            /// </summary>
            /// <param name="objDataObject">The object data object.</param>
            /// <returns></returns>
            public INavigation GetINavigation(DataObject objDataObject)
            {
                return objDataObject as INavigation;
            }
        }

        /// <summary>
        /// Gets the rocket.
        /// </summary>
        /// <value>
        /// The rocket.
        /// </value>
        public DbFactoryExtension rocket
        {
            get
            {
                return new DbFactoryExtension();
            }
        }

        /// <summary>
        /// Gets the database connection2.
        /// </summary>
        /// <value>
        /// The database connection2.
        /// </value>
        public Riskmaster.Db.DbConnection dbConnection2
        {
            get
            {
                return DbFactory.GetDbConnection(this.m_sConnectString);
            }
        }

        /// <summary>
        /// Gets the database connection1.
        /// </summary>
        /// <value>
        /// The database connection1.
        /// </value>
        public Riskmaster.Db.DbConnection dbConnection1
        {
            get
            {
                return DbFactory.GetDbConnection(this.m_sConnectString);
            }
        }

        /// <summary>
        /// Gets the entity.
        /// </summary>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns></returns>
        public Entity GetEntity(int entityId)
        {
            Entity objEntity = default(Entity);

            switch (this.GetType().Name)
            {
                case "PolicyEnhExtender":
                    objEntity = (this as PolicyEnhExtender).m_objPolEnh.Context.Factory.GetDataModelObject("Entity", false) as Entity;
                    break;
                default:
                    objEntity = this.m_objEvent.Context.Factory.GetDataModelObject("Entity", false) as Entity;
                    break;
            }

            if (!object.ReferenceEquals(objEntity, null))
            {
                objEntity.MoveTo(entityId);
            }

            return objEntity;
        }

        /// <summary>
        /// Formats the time.
        /// </summary>
        /// <param name="time">The time.</param>
        /// <returns></returns>
        public string FormatTime(string time)
        {
            return !string.IsNullOrEmpty(time)?Conversion.GetTimeAMPM(time):string.Empty;
        }

        /// <summary>
        /// Ss the name of the get org.
        /// </summary>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns></returns>
        public string sGetOrgName(int entityId)
        {
            string orgName = string.Empty;
 
            if (entityId > 0)
            {
                using (Entity objEntity = this.GetEntity(entityId))
                {
                    orgName = objEntity.LastName;        
                }
            }
            return orgName;
        }

        
        /// <summary>
        /// GetMonthname. MITS 33585,33586,33587 - Modified for Froi Migration
        /// </summary>
        /// <param name="date">date.</param>
        /// <returns></returns>
        public string sGetMonthName(string date)
        {
            return Conversion.GetDBDateFormat(date, "MMMM");
        }



        /// <summary>
        /// Ds the get weekly rate.
        /// </summary>
        /// <param name="p_sPayTypeCode">The P_S pay type code.</param>
        /// <returns></returns>
        public double dGetWeeklyRate(string p_sPayTypeCode)
        {
            return this.GetWeeklyRate(p_sPayTypeCode);
        }
        // akaushik5 Added for MITS XXXXX Ends

        /// <summary>
		/// Gets Weekly Rate for the employee
		/// </summary>
		/// <param name="p_sPayTypeCode">Pay Type Code</param>
		/// <returns>Weekly Rate</returns>
		public double GetWeeklyRate(string p_sPayTypeCode)
		{
			//Check if PiEmployee has not been populated
			if ((m_objPiEmp==null)||(p_sPayTypeCode.Trim()=="")) return 0;

			if (m_objPiEmp.WeeklyRate  !=0.0 )
				return m_objPiEmp.HourlyRate;
			else
			{
				switch(p_sPayTypeCode.ToUpper())
				{
					case "W":		//Weekly Rate
						return m_objPiEmp.PayAmount;
					case "H":		//Hourly Rate
						return m_objPiEmp.PayAmount * 40;
					case "B":		//Bi-Weekly Rate
						return m_objPiEmp.PayAmount/2;
					case "M":		//Monthly Rate
						return (m_objPiEmp.PayAmount *12)/52;
					case "SA":		//Salary Rate
						return m_objPiEmp.PayAmount/ 52;
					case "D":		//Daily Rate
						return m_objPiEmp.PayAmount *5;
					case "SM":		//Semi-Monthly Rate
						return (m_objPiEmp.PayAmount * 24) /52;
					default:
						return 0;
				}
			}
		}

		/// <summary>
		/// Gets Pay Rate based on Pay type Code
		/// </summary>
		/// <param name="p_sPayTypeCode">Pay Type Code</param>
		/// <returns>Pay Rate</returns>
		public double GetPayRate(string p_sPayTypeCode)
		{
			//Check if PiEmployee has not been populated
			if ((m_objPiEmp==null)||(p_sPayTypeCode.Trim()=="")) return 0;

				switch(p_sPayTypeCode.ToUpper())
				{
					case "B":		//Bi-Weekly Rate
						return m_objPiEmp.PayAmount/2;
					default:
						return 0;
				}
		}
		//Added for Froi Migration- Mits 33585,33586,33587
		public int iGetHoursPerShift(int p_iWeeklyHours)
		{
			return GetHoursPerShift(p_iWeeklyHours);
		}
		/// <summary>
		/// Determine the number of hours worked per shift based on
		///	hours worked per week divided by days worked per week.
		/// </summary>
		/// <param name="p_iWeeklyHours">Total weekly hours</param>
		/// <returns>Hours worked per shift </returns>
		public int GetHoursPerShift(int p_iWeeklyHours)
		{
			//Check if PiEmployee has not been populated
			if (m_objPiEmp==null) return 0;

			int iHrs=0;
			if (m_objPiEmp.WorkMonFlag) iHrs++;
			if (m_objPiEmp.WorkTueFlag) iHrs++;
			if (m_objPiEmp.WorkWedFlag) iHrs++;
			if (m_objPiEmp.WorkThuFlag) iHrs++;
			if (m_objPiEmp.WorkFriFlag) iHrs++;
			if (m_objPiEmp.WorkSatFlag) iHrs++;
			if (m_objPiEmp.WorkSunFlag) iHrs++;

			if (iHrs >0) 
				return p_iWeeklyHours/ iHrs;
			else
				return 0;
		}

		/// <summary>
		/// Gets Administrator ID
		/// </summary>
		/// <param name="p_iAdminLvl">AdminLevel</param>
		/// <returns>Weekly Rate</returns>
		internal int GetAdministratorID(int p_iAdminLvl)
		{
				int iAdminID=0;
				switch(p_iAdminLvl)
				{
					case 1:		
						iAdminID =  0;
						break;
					case 2:
						if(m_objCompany!=null) 
							iAdminID= m_objCompany.EntityId;
						break;
					case 3:
						if(m_objClaim!=null)
						{
							foreach (ClaimAdjuster objAdj in m_objClaim.AdjusterList) 
							{
								iAdminID= objAdj.AdjusterEntity.EntityId; 
								break;
							}
						}
						break;
					case 4:
						iAdminID= GetAdministratorIDDefault();
						break;
					default:
						iAdminID= 0;
						break;
				}
			return iAdminID;
		}

		/// <summary>Get Administrator Entity ID</summary>
		/// <returns>ENTITY_ID</returns>
		private int GetAdministratorIDDefault()
		{
			int iEntityID=0;
			DbReader objRdr=null;
			StringBuilder sbSQL = null;			
			try
			{
				sbSQL= new StringBuilder();
				sbSQL.Append ("SELECT DISTINCT ENTITY.ENTITY_ID FROM ENTITY,GLOSSARY");
				sbSQL.Append (" WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'WC_DEF_CLAIM_ADMIN'");
				sbSQL.Append (" AND ENTITY.ENTITY_TABLE_ID = GLOSSARY.TABLE_ID");
				sbSQL.Append (" ORDER BY ENTITY.ENTITY_ID ASC");

				objRdr=DbFactory.GetDbReader(m_sConnectString ,sbSQL.ToString() );
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iEntityID= objRdr.GetInt32("ENTITY_ID"); 
					}
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("Extender.GetAdministratorIDDefault.DataError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				sbSQL=null;
			}
			return iEntityID;
		}

		/// <summary>
		/// Gets DATE_RETURNED from PI_X_WORK_LOSS
		/// </summary>
		/// <param name="p_sConnectString">Connection String</param>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>DATE_RETURNED from PI_X_WORK_LOSS</returns>
        public static string GetDateReturned(string p_sConnectString, int p_iClaimID, int p_iClientId)
		{
			DbReader objRdr=null;
			string sDateRtnd ="";
			StringBuilder sbSQL = null;			
			try
			{
				sbSQL= new StringBuilder();
				sbSQL.Append ("SELECT DATE_RETURNED");
				sbSQL.Append (" FROM PI_X_WORK_LOSS WHERE PI_ROW_ID = ");
				sbSQL.Append (p_iClaimID);
				sbSQL.Append (" ORDER BY DATE_LAST_WORKED DESC");

				objRdr=DbFactory.GetDbReader(p_sConnectString ,sbSQL.ToString());
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						sDateRtnd= objRdr.GetString(0); 
					}
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("Extender.GetDateReturned.DataError", p_iClientId), p_objExp);
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				sbSQL=null;
			}
			return sDateRtnd;
		}

		/// <summary>
		/// Returns Entity against an Entity ID. Though the client using this component
		/// can create an entity directly from Datamodel still this has functionality has
		/// been given as it was in the old component. Moreover this is helpful in case the
		/// client does not have access to datamodel or does not want to use it.
		/// </summary>
		/// <param name="p_objDmf">DatamodelFactory object</param>
		/// <param name="p_iEntityID">Entity ID of the entity to be created</param>
		/// <returns>Entity Object</returns>
        public static Entity GetEntity(DataModelFactory p_objDmf, int p_iEntityID, int p_iClientId)//sharishkumar Jira 827
		{
			Entity  objEntity=null;
			try
			{
				if(p_iEntityID>0)
				{
					objEntity = (Entity)p_objDmf.GetDataModelObject("Entity",false);
					objEntity.MoveTo (p_iEntityID);
				}
				return objEntity; 
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("Extender.GetEntity.AppError", p_iClientId), p_objEx);//sharishkumar Jira 827
			}
		}


		/// <summary>
		/// Returns Patient against a Patient ID. Though the client using this component
		/// can create an entity directly from Datamodel still this has functionality has
		/// been given as it was in the old component. Moreover this is helpful in case the
		/// client does not have access to datamodel or does not want to use it.
		/// </summary>
		/// <param name="p_objDmf">DatamodelFactory object</param>
		/// <param name="p_iPatientID">Patient ID of the entity to be created</param>
		/// <returns>Entity Object</returns>
        public static Patient GetPatient(DataModelFactory p_objDmf, int p_iPatientID,int p_iClientId)//sharishkumar Jira 827
		{
			Patient objPatient=null;
			try
			{
				if (p_iPatientID>0)
				{
					objPatient = (Patient)p_objDmf.GetDataModelObject("Patient",false);
					objPatient.MoveTo (p_iPatientID);
				}
				return objPatient; 
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("Extender.GetPatient.AppError",p_iClientId), p_objEx);//sharishkumar Jira 827
			}
		}


		/// <summary>
		/// Populates Extender Object Instance.
		/// Populate the various instance datamodel objects within this class
		/// Works for both of the base classes FROIOptionsMajor & JURISOptionsMajor
		/// </summary>
		/// <param name="p_objOptions">OptionsMajor</param>
		/// <param name="p_objDmf">Datamodel factory object needed to create objects</param>
		/// <param name="p_sClsType">Differentiator betwee class type FROIOptionsMajor & JURISOptionsMajor</param>
		internal void PopulateObjExt(DataModelFactory p_objDmf,OptionsMajor p_objOptions, string p_sClsType)
		{
			int iTest=0;
			bool bDone=false;
			try
			{
				if (m_objPiEmp==null) return;
				if (m_objPiEmp.DeptAssignedEid >0) 
				{
					// call different child class function based on the class type
					if (p_sClsType=="FROIOptionsMajor")
                        bDone = (p_objOptions as FROIOptionsMajor).LoadDataByAssignedDepartment(m_objPiEmp.DeptAssignedEid);
					else if (p_sClsType=="JURISOptionsMajor")
                        bDone = (p_objOptions as JURISOptionsMajor).LoadDataByAssignedDepartment(m_objPiEmp.DeptAssignedEid);
					else
                        throw new RMAppException(Globalization.GetString("Extender.InvalidClassType", m_iClientId));//sharishkumar Jira 827

					if (bDone)
					{
						//Load the Entity Company with data
						m_iCompanyEID=Functions.GetOrgParentByDepartID(m_sConnectString,m_objPiEmp.DeptAssignedEid,
                            p_objOptions.m_iEmployerLevel, m_iClientId);//sharishkumar Jira 827
						if (m_iCompanyEID!=0)
						{							
							m_objCompany  = (Entity)p_objDmf.GetDataModelObject("Entity",false);
							if (m_iCompanyEID>0)
								m_objCompany.MoveTo(m_iCompanyEID);
						}
						else
						{
							m_objCompany=null;
							m_iCompanyEID=0;
						}
						
						//Load the Entity Carrier with data
						m_objCarrier = (Entity)p_objDmf.GetDataModelObject("Entity",false);
						switch(p_objOptions.m_iCarrierOption) 
						{
							case 1:
								iTest= Functions.GetOrgParentByDepartID(m_sConnectString ,
									m_objPiEmp.DeptAssignedEid ,p_objOptions.m_iCarrierOrgHierarchyLevel,m_iClientId);//sharishkumar Jira 827
								if (iTest !=0)
									m_objCarrier.MoveTo(iTest);  
								break;	
							case 2:
								iTest=p_objOptions.m_iCarrierDefaultEID;
								if (iTest!=0)
									m_objCarrier.MoveTo(iTest);
								break;
							case 3:
                                // akaushik5 Changed for MITS 37849 Starts
                                //if (m_objClaim != null)
                                //{
                                //    if (m_objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                                //    {
                                //        if (m_objClaim.PrimaryPolicyId != 0)
                                //        {
                                //            iTest = m_objClaim.PrimaryPolicy.InsurerEntity.EntityId;
                                //            if (iTest != 0)
                                //                m_objCarrier.MoveTo(iTest);
                                //        }
                                //    }
                                //}
                                
                                if (m_objClaim != null)
                                {
                                    if (m_objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1)
                                    {
                                        if (m_objClaim.PrimaryPolicyIdEnh != 0)
                                        {
                                            iTest = m_objClaim.PrimaryPolicyEnh.InsurerEntity.EntityId;
                                            if (iTest != 0)
                                                m_objCarrier.MoveTo(iTest);
                                        }
                                    }
                                    else
                                    {
                                        if (m_objClaim.PrimaryPolicyId != 0)
                                        {
                                            iTest = m_objClaim.PrimaryPolicy.InsurerEntity.EntityId;
                                            if (iTest != 0)
                                                m_objCarrier.MoveTo(iTest);
                                        }
                                    }
                                }
                                // akaushik5 Changed for MITS 37849 Ends
                                break;
						}
					}
				}
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objEx)
			{
				throw new DataModelException(Globalization.GetString("Extender.PopulateObjExt.InitializeError",m_iClientId) , p_objEx);
			}
		}


		/// <summary>
		///  Load the Administrator object of the extender
		/// </summary>
		/// <param name="p_objDmf">DataModelFactory Object</param>
		/// <param name="p_objOptions">OptionsMajor</param>
		/// <param name="p_sClsType">Differentiator betwee class type FROIOptionsMajor & JURISOptionsMajor</param>/// 
		internal void PopulateObjExtObjAdministrator(DataModelFactory p_objDmf, OptionsMajor p_objOptions, 
			string p_sClsType)
		{
			int iTest=0;
			bool bDone=false;
			try
			{
				if (m_objPiEmp==null)return;
				if (m_objPiEmp.DeptAssignedEid >0) 
				{
					// call different child class function based on the class type
					if (p_sClsType=="FROIOptionsMajor")
                        bDone = (p_objOptions as FROIOptionsMajor).LoadDataByAssignedDepartment(m_objPiEmp.DeptAssignedEid);
					else if (p_sClsType=="JURISOptionsMajor")
                        bDone = (p_objOptions as JURISOptionsMajor).LoadDataByAssignedDepartment(m_objPiEmp.DeptAssignedEid);
					else
                        throw new RMAppException(Globalization.GetString("Extender.InvalidClassType", m_iClientId));//sharishkumar Jira 827

					if (bDone)
					{
						// Populate the Administrator object from datamodel factory
						m_objAdministrator = (Entity)p_objDmf.GetDataModelObject("Entity",false);
						switch(p_objOptions.m_iClaimAdminOption)
						{
							case 1:		//Nothing
								m_objAdministrator=null;
								break;
							case 2:		//Employer Org-Hierarchy level
								iTest=Functions.GetOrgParentByDepartID (m_sConnectString, 
									m_objPiEmp.DeptAssignedEid,p_objOptions.m_iClaimAdminOrgHierarchyLevel,m_iClientId);//sharishkumar Jira 827
								break;
							case 3:		//current adjuster
								iTest= GetAdministratorID(3);
								break;
							case 4:		//default admin
								iTest= p_objOptions.m_iClaimAdminDefaultEID;
								break;
							case 5:		//TPA admin
								iTest= p_objOptions.m_iClaimAdminTPAEID;
								break;
							default:	//bad error
								m_objAdministrator=null;
								break;
						}
						if(iTest>0)
							m_objAdministrator.MoveTo(iTest);
						else
							m_objAdministrator=null;
					}
				}
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objEx)
			{
                throw new DataModelException(Globalization.GetString("FROIExtender.PopulateObjExtObjAdministrator.InitializeError", m_iClientId), p_objEx);//sharishkumar Jira 827
			}
		}


		/// <summary>
		/// Gets User Preference XML
		/// </summary>
		/// <returns>int for Success</returns>
		private int XMLExists()
		{
			DbReader objRdr=null;
			bool bRead=false;
			DataModelFactory objDmf=null;
			try
			{

                objDmf = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId); //sharishkumar Jira 827
				objRdr=objDmf.Context.DbConn.ExecuteReader("SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID =" + 
					objDmf.Context.RMUser.UserId + " AND PREF_XML IS NOT NULL");
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						bRead=true;
						m_sXMLText= objRdr.GetString ("PREF_XML");                       
					}
				}
				if (!bRead)
					return (int)enumXMLExists.iNoFieldData;

				if (m_sXMLText.IndexOf("FROIOPTIONS",0,1)>0 )
					return (int)enumXMLExists.iOptions;
				else
					return (int)enumXMLExists.iNoOptions;  

			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("FROIExtender.XMLExists.DataError",m_iClientId) , p_objExp);
			}
			finally
			{
				if(objDmf!=null)objDmf.Dispose();
				if(objRdr!=null)objRdr.Dispose();
			}
		}

		/// <summary>
		/// Get ENTITY_X_SELFINSUR from ENTITY_X_SELFINSUR
		/// </summary>
		/// <param name="p_iAssignedDept">Department Id</param>
		/// <param name="p_ojbDmf">DataModelFactory</param>
        internal void GetSelfInsured(DataModelFactory p_ojbDmf, int p_iAssignedDept, int p_iClientId)//sharishkumar Jira 827
		{
			DbReader objRdr=null;			
			StringBuilder sbOrgs = null; 
			try
			{                
				if (m_objClaim==null)return;
				sbOrgs= new StringBuilder();
				sbOrgs.Append (p_iAssignedDept.ToString());
				for(int iCtr = 1005 ; iCtr <=1011; iCtr ++)
				{
					// make a list of of department id's
					sbOrgs.Append(",");
                    sbOrgs.Append(Functions.GetOrgParentByDepartID(m_sConnectString, p_iAssignedDept, iCtr, m_iClientId));//sharishkumar Jira 827
				}
				objRdr=DbFactory.GetDbReader(m_sConnectString ,
					"SELECT ENTITY_X_SELFINSUR.ENTITY_ID, ENTITY_X_SELFINSUR.SI_ROW_ID, ENTITY.ENTITY_TABLE_ID" +
					" FROM ENTITY_X_SELFINSUR, ENTITY WHERE ENTITY_X_SELFINSUR.ENTITY_ID IN (" + sbOrgs.ToString() +
					") AND ENTITY_X_SELFINSUR.JURIS_ROW_ID = " + m_objClaim.FilingStateId +
					" AND ENTITY.ENTITY_ID = ENTITY_X_SELFINSUR.ENTITY_ID ORDER BY ENTITY.ENTITY_TABLE_ID DESC");
 
				if (objRdr!=null)
				{
                    if (objRdr.Read())
                    {
                        //Populate the SelfInsured Entity
                        m_objSelfInsured = (EntityXSelfInsured)p_ojbDmf.GetDataModelObject("EntityXSelfInsured", false);
                        m_objSelfInsured.MoveTo(objRdr.GetInt32("SI_ROW_ID"));
                    }
                    else 
                    {
                        m_objSelfInsured = null;
                    }
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("FROIExtender.GetSelfInsured.DataError",m_iClientId) , p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				sbOrgs=null;
			}
		}

		
		/// <summary>
		/// Gets the Name, Phone & Title from the USER_PREF_XML
		/// Base class implementation
		/// </summary>
		/// <param name="p_sOptionType"> can hold possible values FROIOptions|JurisOptions</param>
		protected void GetPreparerInfoXML(string p_sOptionType)
		{
			XmlDocument objDoc=null;
			XmlNode  objNode=null;
			try
			{
				if (XMLExists() >3)
				{
					objDoc= new XmlDocument();
					objDoc.LoadXml(m_sXMLText);
					objNode=objDoc.SelectSingleNode("//" + p_sOptionType);
					if (objNode!=null)
					{
						//populate the values read from xml
						m_sName = objNode.Attributes["name"].InnerText;
						m_sTitle= objNode.Attributes["title"].InnerText;
						m_sPhone= objNode.Attributes["phone"].InnerText;
					}
				}
			}
			catch(Exception  p_objEx)
			{
				throw new RMAppException(Globalization.GetString("Extender.GetPreparerInfoXML.XmlError",m_iClientId),p_objEx);//sharishkumar Jira 827
			}
			finally
			{
				objDoc=null;
				objNode=null;
			}
		}
		//Added for Froi Migration- Mits 33585,33586,33587
        public static string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }
        public static string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }
		//Added for Froi Migration- Mits 33585,33586,33587
        public static string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        public static string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }

        public string f_sFormatPhone(string sTempPhone)
        {
            string sIsNumber = string.Empty;
            string f_sFormatPhone = string.Empty;
            double Num;

            sTempPhone = sTempPhone.Trim() + "";

            if (string.IsNullOrEmpty(sTempPhone) || sTempPhone == "(" || sTempPhone == "0" || sTempPhone.Length < 7)
            {
                return sTempPhone;
            }

            bool isNum = double.TryParse(Right(sTempPhone, 1), out Num);

            if (!isNum)
            {
                sIsNumber = "N";//jlt strip missing Ext:
            }

            while (sIsNumber == "N")
            {
                if (!isNum)
                {
                    sTempPhone = Left(sTempPhone, sTempPhone.Length - 1);
                    if (sTempPhone.Length == 0)                           //jlt donot mess with this 03/07/2002
                    {
                        sIsNumber = "Y";
                    }
                }
                else
                {
                    sIsNumber = "Y";
                }

                if (sTempPhone.Length == 0)
                {
                    sIsNumber = "Y";
                }
            }

            if (sTempPhone.Length < 7)
            {
                return sTempPhone; //jlt no real data
            }
            if (sTempPhone.IndexOf("-") > 0)
            {
                f_sFormatPhone = sTempPhone;             //jlt presume pre formatted
                return f_sFormatPhone;
            }

            sTempPhone = sTempPhone.Replace("(", "");
            sTempPhone = sTempPhone.Replace(")", "");
            sTempPhone = sTempPhone.Replace("-", "");
            sTempPhone = sTempPhone.Replace("Ext:", "");
            sTempPhone = sTempPhone.Replace(" ", "");
            sTempPhone = sTempPhone.Trim();

            if (sTempPhone == "")
            {
                return "";
            }

            if (sTempPhone.Length < 7)
            {
                f_sFormatPhone = "(invalid #)";
                return f_sFormatPhone;
            }

            if (!isNum)
            {
                f_sFormatPhone = "(invalid #)";
                return f_sFormatPhone;
            }

            if (sTempPhone.Length > 10 && Left(sTempPhone, 1) == "1")
            {
                sTempPhone = Mid(sTempPhone, 2);
            }

            switch (sTempPhone.Length)
            {
                case 7:                     //jlt local number
                    f_sFormatPhone = Mid(sTempPhone, 1, 3) + "-" + Mid(sTempPhone, 4);
                    break;

                case 10:                    //jlt presumed to have area code
                    f_sFormatPhone = "(" + Mid(sTempPhone, 1, 3) + ") " + Mid(sTempPhone, 4, 3) + "-" + Mid(sTempPhone, 7);
                    break;

                default:                      //jlt may have leading 1 or ext #
                    f_sFormatPhone = "(" + Mid(sTempPhone, 1, 3) + ") " + Mid(sTempPhone, 4, 3) + "-" + Mid(sTempPhone, 7, 4) + " Ext:" + Mid(sTempPhone, 11);
                    break;
            }
            return f_sFormatPhone;
        }
		#endregion

	}	// End of class Extender
}		// End of name space
