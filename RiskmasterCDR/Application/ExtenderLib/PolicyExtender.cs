using System;
using Riskmaster.DataModel ;


namespace Riskmaster.Application.ExtenderLib
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   24th,Aug 2004
	///Purpose :   Class PolicyExtender used to Invoke Policy
	///Functions present in this class are already present in other classes. Hence not re-written.
	/// </summary>
	public class PolicyExtender
	{

		#region Variable Declaration

		/// <summary>
		/// Internal variable Policy
		/// </summary>
		internal Policy m_objPolicy=null;

		/// <summary>
		/// Internal variable for Supplementals
		/// </summary>
		internal Supplementals m_obbSupp=null; 

		#endregion

		#region Constructor

		/// <summary>
		/// Default Constructor
		/// </summary>
		public PolicyExtender()	{}

		#endregion

		#region Public Properties

		/// <summary>
		/// Public Read Only Property Policy
		/// </summary>
		public Policy objPolicy
		{
			get
			{
				return m_objPolicy;
			}
		}

		#endregion

	}// end class
}// end namespace
