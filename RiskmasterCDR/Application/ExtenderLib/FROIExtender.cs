using System;
using Riskmaster.DataModel;
using Riskmaster.Security; 
using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.ExceptionTypes;
using Microsoft.VisualBasic;
using System.Text ;
using System.Data;
using Riskmaster.Settings; 



namespace Riskmaster.Application.ExtenderLib
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   24th,Aug 2004
	///Purpose :   This class provides an extension environment for
	///the FROI population scripts. All Public top-level
	///methods and properties are exposed to the scripts
	///as built-in functions/methods.
	/// </summary>
	public class FROIExtender : Extender 
	{

		#region Variable Declaration

		/// New member variable for FROI Integration
		/// <summary>
		/// Private Variable for New Line Char
		/// </summary>
        private string m_sCrLf=@"\r\n";

        /// <summary>
        /// Internal variable for TPA Entity.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        internal Entity m_objTPA = null;

        /// <summary>
		/// Internal variable storing vehicle object
		/// </summary>
		internal Vehicle m_objVehicle=null;

        /// <summary>
        /// Internal variable for UnitXClaim Object
        /// </summary>
		internal UnitXClaim m_objUnitXClaim=null;

		/// <summary>
		/// Internal variable for Contact Object
		/// </summary>
		internal Entity m_objContact=null;

		/// <summary>
		/// Internal variable for Person Notified
		/// </summary>
		internal Entity m_objPersonNotified=null;

		/// <summary>
		/// Internal variable for Facility
		/// </summary>
		private Entity m_objFac=null;

		/// <summary>
		/// Private m_sFacilityHCFA_objExt
		/// </summary>
		private string m_sFacilityHCFA_objExt="";

		/// <summary>
		/// Private m_sUFDistReportNumber_objExt
		/// </summary>
		private string m_sUFDistReportNumber_objExt="";

		/// <summary>
		/// Private m_sMainFormCP_objExt
		/// </summary>
		private string m_sMainFormCP_objExt="";

		/// <summary>
		/// Private m_sAttachmentCP_objExt
		/// </summary>
		private string m_sAttachmentCP_objExt="";

		/// <summary>
		/// Internal variable
		/// </summary>
		internal Entity m_objJurisParent_objExt=null;

		/// <summary>
		/// Variable for FroiOptionsMajor
		/// </summary>
		internal FROIOptionsMajor m_objOptions=null;
		
		/// <summary>
		/// Internal string m_sIncidentDesc_objExt
		/// </summary>
		internal string m_sIncidentDesc_objExt="";

		/// <summary>
		/// Internal string m_sOccupation_objExt
		/// </summary>
		internal string m_sOccupation_objExt="";

        /// <summary>
        /// Internal string m_sMedWatchDescription_objExt //Added for FROI Migration 
        /// </summary>
        internal string m_sMedWatchDescription_objExt = "";

		/// <summary>
		/// Internal string variable m_sMainFormRT_objExt
		/// </summary>
		internal string m_sMainFormRT_objExt="";

		/// <summary>
		/// Internal string variable m_sAttachmentRT_objExt
		/// </summary>
		internal string m_sAttachmentRT_objExt="";

		/// <summary>
		/// Internal int variable m_iWorkLossOption_objExt
		/// </summary>
		internal int m_iWorkLossOption_objExt=0;

		/// <summary>
		/// Internal int variable m_iContactOption_objExt
		/// </summary>
		internal int m_iContactOption_objExt=0;

		/// <summary>
		/// Internal int variable m_iAttachToClaimByDefault_FROI
		/// </summary>
		internal int m_iAttachToClaimByDefault_FROI=0;

		/// <summary>
		/// Internal int variable m_iPrintClaimNumberAtTop
		/// </summary>
		internal int m_iPrintClaimNumberAtTop=0;

		/// <summary>
		/// Internal entity VAPI
		/// </summary>
		internal Entity m_objVAPI=null;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructor maps to the base class constructor Extender
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">Dsn for connection</param>
		/// <param name="p_sConnectString">Connection String</param>
        public FROIExtender(string p_sUserName, string p_sPassword, string p_sDsnName, string p_sConnectString, int p_iClientId) :
            base(p_sUserName, p_sPassword, p_sDsnName, p_sConnectString, p_iClientId) { }//sharishkumar Jira 827

		/// Name		: FROIExtender
		/// Author		: Pankaj Chowdhury
		/// Date Created: 01/17/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 01/17/2004			Added			Pankaj
		///************************************************************
		/// <summary>
		/// Constructor maps to the base class constructor Extender
		/// </summary>
		/// <param name="p_sUserName">User Name</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sConnectString">Connection String</param>
        public FROIExtender(string p_sUserName, string p_sPassword, string p_sConnectString, int p_iClientId) : 
			base( p_sUserName,  p_sPassword,p_sConnectString,p_iClientId){}

		/// <summary>
		/// Default constructor for accessing functions.
		/// This might not be used always. Still has been provide 
		/// for accessing few of the functions which are not dependent on instance data
		/// </summary>
		public FROIExtender(): base(){}

		#endregion
		
		#region Public Properties
        
        //m_sOccupation_objExt
        //m_iWorkLossOption_objExt

        /// <summary>
        /// Public Read Only Occupation.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        public string Occupation_objExt
        {
            get
            {
                return m_sOccupation_objExt;
            }
        }

        /// <summary>
        /// Public Read Only Occupation.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        public string sAttachmentRT_objExt
        {
            get
            {
                return m_sAttachmentRT_objExt;
            }
        }


        /// <summary>
        /// Public Read Only sMedWatchDescription_objExt.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        public string sMedWatchDescription_objExt
        {
            get
            {
                return m_sMedWatchDescription_objExt;
            }
        }

         /// <summary>
        /// Public Read Only sMainFormRT_objExt.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        public string sMainFormRT_objExt
        {
            get
            {
                return m_sMainFormRT_objExt;
            }
        }

         /// <summary>
        /// Public Read Only sMainFormRT_objExt.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        public string sUFDistReportNumber_objExt
        {
            get
            {
                return m_sUFDistReportNumber_objExt;
            }
        }
        
        /// <summary>
        /// Public Read Only sMainFormCP_objExt.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        public string sMainFormCP_objExt
        {
            get
            {
                return m_sMainFormCP_objExt;
            }
        }

        /// <summary>
        /// Public Read Only Occupation.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        public int WorkLossOption_objExt
        {
            get
            {
                return m_iWorkLossOption_objExt;
            }
        }


        /// <summary>
        /// Public Read Only Property TPA.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        public Entity objJurisParent_objExt
        {
            get
            {
                return m_objJurisParent_objExt;
            }
        }

        /// <summary>
        /// Public Read Only Property TPA.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        public Entity objTPA
        {
            get
            {
                return m_objTPA;
            }
        }

        /// <summary>
        /// Public Read Only Property for ClaimNumberOption.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        public int ClaimNumberOption_objExt //Added for Froi Migration- Mits 33585,33586,33587
        {
            get
            {
                return m_objOptions.CarrierOption;
            }
        }

		/// <summary>
		/// Public Read Only Property for Vehicle
		/// </summary>
		public Vehicle objVehicle
		{
			get
			{
				return m_objVehicle;
			}
		}

		/// <summary>
		/// Public Read Only Property UnitXClaim
		/// </summary>
		public UnitXClaim objUnitXClaim
		{
			get
			{
				return m_objUnitXClaim;
			}
		}

		/// <summary>
		/// Public Read Only Property Contact Entity
		/// </summary>
		public Entity objContact
		{
			get
			{
				return m_objContact;
			}
		}

		/// <summary>
		/// Public Read Only Property VAPI Entity
		/// </summary>
		public Entity objVAPI
		{
			get
			{
				return m_objVAPI;
			}
		}

		/// <summary>
		/// Public Read Only Property PersonNotified Entity
		/// </summary>
		public Entity objPersonNotified
		{
			get
			{
				return m_objPersonNotified;
			}
		}

		/// <summary>
		/// Public Read Only Property Facility Entity
		/// </summary>
		public Entity objFacility
		{
			get
			{
				return m_objFac;
			}
		}

		/// <summary>
		/// Read Only Property for Carrier Entity
		/// </summary>
		public Entity objCarrier
		{
			get
			{
				return m_objCarrier;
			}
		}

		/// <summary>
		/// Public Read Only Property Options.Added duplicate property for Froi Migration- Mits 33585,33586,33587
		/// </summary>
		public FROIOptionsMajor Options
		{
			get
			{
				return m_objOptions;
			}
		}

        /// <summary>
        /// Public Read Only Property Options
        /// </summary>
        public FROIOptionsMajor objOptions
        {
            get
            {
                return m_objOptions;
            }
        }

		/// <summary>
		/// Public Read Only Property Claim
		/// </summary>
		public Claim objClaim 
		{
			get
			{
				return m_objClaim;
			}
		}

		/// <summary>
		/// Public Read Only Property Event
		/// </summary>
		public Event objEvent
		{
			get
			{
				return m_objEvent;
			}
		}

		/// <summary>
		/// Public Read Only Property Company
		/// </summary>
		public Entity objCompany
		{
			get
			{
				return m_objCompany;
			}
		}

		/// <summary>
		/// Public Read Only Property New Line
		/// </summary>
		public string pdfCrLf
		{
			get 
			{
				return m_sCrLf;
			}
		}

		/// <summary>
		/// Public Read Only Property Employee
		/// </summary>
		public PiEmployee objPI
		{
			get
			{
				return m_objPiEmp;
			}
		}

		/// <summary>
		/// Read Only Property for Administrator Entity
		/// </summary>
		public Entity objAdministrator
		{
			get
			{
				return m_objAdministrator;
			}
		}

		/// <summary>
		/// Public Property for Claim Id
		/// </summary>
		public int ClaimID
		{
			get
			{
				return m_iClaimID;
			}
		}

		/// <summary>
		/// Public Property for Event Id
		/// </summary>
		public int EventID
		{
			get
			{
				return m_iEventID;
			}
		}

		/// <summary>
		/// Public Property for Company EId
		/// </summary>
		public int CompanyEID
		{
			get
			{
				return m_iCompanyEID;
			}
		}

        /// <summary>
        /// Read Only Property EntityXSelfInsured 
        /// Added for PDS script compatibility.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        public EntityXSelfInsured objSelfInsured
        {
            get
            {
                return m_objSelfInsured;
            }
        }

		/// <summary>
		/// Read Only Property EntityXSelfInsured
		/// </summary>
		public EntityXSelfInsured objEntityXSelfInsured
		{
			get
			{
				return m_objSelfInsured;
			}
		}

		/// <summary>
		/// Read Only Property Supplementals
		/// </summary>
		public Supplementals objSupp
		{
			get
			{
				return m_objSupp;
			}
		}

		/// <summary>
		/// Read Only Property Patient
		/// </summary>
		public Patient objPatient
		{
			get
			{
				return m_objPatient;
			}
		}

        // akaushik5 Added for MITS 37849 Starts
        /// <summary>
        /// Gets the s incident desc_obj ext.
        /// </summary>
        /// <value>
        /// The s incident desc_obj ext.
        /// </value>
        public string sIncidentDesc_objExt
        {
            get
            {
                return this.m_sIncidentDesc_objExt;
            }
        }
        // akaushik5 Added for MITS 37849 Ends


		#endregion
		
		#region Methods

		/// <summary>
		/// Gets Name title and Phone
		/// </summary>
		/// <param name="p_sName">Name</param>
		/// <param name="p_sTitle">Title</param>
		/// <param name="p_sPhone">Phone</param>
		public void GetPreparerInfo(ref string p_sName, ref string p_sTitle,ref string p_sPhone)
		{
			string sSQL="";
			string sPerson="";
			DbReader objRdr=null;
			try
			{
                if (m_objOptions==null)
                    throw new RMAppException(Globalization.GetString("FROIExtender.GetPreparerInfo.InvalidOptions", m_iClientId));//sharishkumar Jira 827

				switch(m_objOptions.FROIPreparer)
				{
						#region Case 1
					case 1:		//WORKSTATION USER
						
						GetPreparerInfoXML("FROIOptions");
						p_sName=m_sName;
						p_sTitle=m_sTitle;
                        // akaushik5 Changed for MITS 37849 Starts
                        //p_sTitle = m_sTitle;
                        p_sPhone = this.m_sPhone;
                        // akaushik5 Changed for MITS 37849 Starts
						break;
						#endregion

                    #region Case 2 EDITED BY USER
                    case 2:		
                        // akaushik5 Added for MITS 37849 Starts
                        if (object.ReferenceEquals(m_objEvent, null))
                        {
                            return;
                        }

                        if (object.ReferenceEquals(m_objClaim, null))
                        {
                            return;
                        }

                        if (m_objEvent.EventId > 0)
                        {
                            sSQL = string.Format("SELECT UPDATED_BY_USER FROM EVENT WHERE EVENT_ID = {0}", m_objEvent.EventId);
                        }

                        if (m_objClaim.ClaimId > 0)
                        {
                            sSQL = string.Format("SELECT UPDATED_BY_USER FROM CLAIM WHERE CLAIM_ID = {0}", m_objClaim.ClaimId);
                        }

                        using (objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (objRdr != null && objRdr.Read())
                            {
                                sPerson = objRdr.GetString(0);
                            }
                        }

                        sSQL = "SELECT USER_TABLE.LAST_NAME, USER_TABLE.FIRST_NAME, USER_TABLE.TITLE, " +
                            "USER_TABLE.OFFICE_PHONE FROM USER_DETAILS_TABLE, USER_TABLE WHERE " +
                            "USER_DETAILS_TABLE.LOGIN_NAME = '" + sPerson +
                            "' AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID";

                        objRdr = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(0), sSQL);
                        if (objRdr != null)
                        {
                            if (objRdr.Read())
                            {
                                //Get it from USER_DETAILS_TABLE
                                p_sName = objRdr.GetString("FIRST_NAME") + " " + objRdr.GetString("LAST_NAME");
                                p_sTitle = objRdr.GetString("TITLE");
                                p_sPhone = objRdr.GetString("OFFICE_PHONE");
                            }
                        }
                        break;
                    // akaushik5 Added for MITS 37849 Ends
                    #endregion

						#region Case 3
					case 3:
						if (m_objEvent==null) return;
						if (m_objClaim ==null) return;
						if (m_objEvent.EventId >0)
							sSQL= "SELECT ADDED_BY_USER FROM EVENT WHERE EVENT_ID = " + m_objEvent.EventId ;
						if (m_objClaim.ClaimId >0 )
							sSQL = "SELECT ADDED_BY_USER FROM CLAIM WHERE CLAIM_ID = " + m_objClaim.ClaimId ;
						objRdr=DbFactory.GetDbReader(m_sConnectString ,sSQL);
						if (objRdr!=null)
						{
							if(objRdr.Read())
							{
								sPerson = objRdr.GetString(0); 
							}
						}
						if(objRdr!=null)objRdr.Dispose();

						sSQL="SELECT USER_TABLE.LAST_NAME, USER_TABLE.FIRST_NAME, USER_TABLE.TITLE, " +
							"USER_TABLE.OFFICE_PHONE FROM USER_DETAILS_TABLE, USER_TABLE WHERE " + 
							"USER_DETAILS_TABLE.LOGIN_NAME = '" + sPerson + 
							"' AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID" ;
						objRdr=DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId) ,sSQL);//sharishkumar Jira 827
						if (objRdr!=null)
						{
							if(objRdr.Read())
							{
								//Get it from USER_DETAILS_TABLE
								p_sName = objRdr.GetString("FIRST_NAME") + " " + objRdr.GetString("LAST_NAME"); 
								p_sTitle = objRdr.GetString("TITLE"); 
								p_sPhone = objRdr.GetString("OFFICE_PHONE"); 
							}
						}
						break;
						#endregion

						#region Case 4
					case 4:
						if (m_objClaim !=null)
						{
							sSQL="SELECT TC_PREPARER_NAME, TC_PREPARER_TITLE, TC_PREPARER_PHONE FROM CLAIM_SUPP " + 
								"WHERE CLAIM_ID = " + m_objClaim.ClaimId;
							objRdr=DbFactory.GetDbReader(m_sConnectString  ,sSQL);
							if (objRdr!=null)
							{
								if(objRdr.Read())
								{
									p_sName = objRdr.GetString("TC_PREPARER_NAME"); 
									p_sTitle = objRdr.GetString("TC_PREPARER_TITLE"); 
									p_sPhone = objRdr.GetString("TC_PREPARER_PHONE"); 
								}
							}
						}
						break;
						#endregion
                    // akaushik5 Added for MITS 37849 Starts
                        #region Case 5 Adjuster
                    case 5:
                        if (!object.ReferenceEquals(this.objClaim, null))
                        {
                            if(this.objClaim.AdjusterList.Count.Equals(default(int)))
                            {
                                break;
                            }

                            if (!this.objClaim.CurrentAdjuster.AdjRowId.Equals(default(int)))
                            {
                                p_sName = string.Format("{0} {1}", this.objClaim.CurrentAdjuster.AdjusterEntity.FirstName, this.objClaim.CurrentAdjuster.AdjusterEntity.LastName);
                                p_sTitle = this.objClaim.CurrentAdjuster.AdjusterEntity.Title;
                                p_sPhone = this.objClaim.CurrentAdjuster.AdjusterEntity.Phone1;
                            }
                            else
                            {
                                using (ClaimAdjuster claimAdjuster = objClaim.Context.Factory.GetDataModelObject("ClaimAdjuster", false) as ClaimAdjuster)
                                {
                                    (claimAdjuster as INavigation).Filter = string.Format("CLAIM_ID={0}", this.objClaim.ClaimId);
                                    claimAdjuster.MoveLast();

                                    p_sName = string.Format("{0} {1}", claimAdjuster.AdjusterEntity.FirstName, claimAdjuster.AdjusterEntity.LastName);
                                    p_sTitle = claimAdjuster.AdjusterEntity.Title;
                                    p_sPhone = claimAdjuster.AdjusterEntity.Phone1;
                                }
                            }
                        }
                        break;
                    #endregion
                    // akaushik5 Added for MITS 37849 Starts
				}
			}
			catch(RMAppException p_objEx)
			{ 
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FROIExtender.GetPrepareInfo.FROIError", m_iClientId), p_objEx);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}
		}
		
        /// <summary>
        /// Added for the PDS migration issues.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        /// <param name="lEntityID"></param>
        /// <returns></returns>
        public string sGetOrgAbbreviation(string lEntityID)
        {
            DbReader objRdr = null;
            StringBuilder sbSQL = null;
            string Abbrevation = string.Empty;
            try
            {
                sbSQL = new StringBuilder();

                sbSQL.Append("SELECT ABBREVIATION");
                sbSQL.Append(" FROM ENTITY WHERE ENTITY_ID = ");
                sbSQL.Append(lEntityID);
             
                objRdr = DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString());
                if (objRdr != null)
                {
                    while (objRdr.Read())
                    {
                        Abbrevation=objRdr.GetString(0);
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("FROIExtender.sGetOrgAbbreviation.DataError",m_iClientId),
                    p_objExp);//sharishkumar Jira 827
            }
            finally
            {
                if (objRdr != null) objRdr.Dispose();
                sbSQL = null;
            }
            return Abbrevation;
        }

		/// <summary>Gets Injury Description from PI_X_DIAGNOSIS, CODES_TEXT</summary>
		/// <returns>Injury Description CODE_DESC from CODES_TEXT</returns>
		public string GetInjuryDescription()
		{
			DbReader objRdr=null;
			StringBuilder  sbInjury = null;
			StringBuilder sbSQL = null;			
			LocalCache objCache = null;
			try
			{
				sbInjury = new StringBuilder();
				sbSQL= new StringBuilder();

				if (m_objPiEmp==null)
                    throw new RMAppException(Globalization.GetString("FROIExtender.InvalidPiEmp", m_iClientId));//sharishkumar Jira 827

				sbSQL.Append ("SELECT CODES_TEXT.CODE_DESC FROM PI_X_DIAGNOSIS, CODES_TEXT");
				sbSQL.Append (" WHERE PI_X_DIAGNOSIS.PI_ROW_ID = ");
				sbSQL.Append (m_objPiEmp.PiRowId );
				sbSQL.Append (" AND PI_X_DIAGNOSIS.DIAGNOSIS_CODE = CODES_TEXT.CODE_ID");

				objRdr=DbFactory.GetDbReader(m_sConnectString ,sbSQL.ToString());
				if (objRdr!=null)
				{
					while(objRdr.Read())
					{
						if (sbInjury.ToString()==""  )
						{
							sbInjury.Append("DIAGNOSIS:  ");
							sbInjury.Append(objRdr.GetString(0));
						}
						else						
						{
							sbInjury.Append(",")  ;
							sbInjury.Append(  objRdr.GetString(0)); 
						}
					}
				}
				if (sbInjury.ToString()=="" )
				{
					objCache = new LocalCache(m_sConnectString,m_iClientId); 
					switch(objCache.GetShortCode(m_objPiEmp.DisabilityCode)) 
					{
						case "ILL":
							sbInjury.Append(GetIllnessDesc());
							break;
						case "INJ":
							sbInjury.Append(GetInjuryDesc());
							break;
					}
				}
			}
			catch(RMAppException p_objEx)
			{ 
				throw p_objEx ;
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("FROIExtender.GetInjuryDescription.DataError",m_iClientId) ,
                    p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				if(objCache!=null)objCache.Dispose();  
				sbSQL=null;
			}
			return sbInjury.ToString() ;
		}


		/// <summary>Gets Illness Description from CODES_TEXT, PERSON_INVOLVED</summary>
		/// <returns>CODE_DESC from CODES_TEXT, PERSON_INVOLVED</returns>
		private string GetIllnessDesc()
		{
			DbReader objRdr=null;
			StringBuilder sbIllDesc = null ;
			StringBuilder sbSQL = null;			
			try
			{
				sbIllDesc =new StringBuilder();
				sbSQL= new StringBuilder();

				if (m_objPiEmp==null)
                    throw new RMAppException(Globalization.GetString("FROIExtender.InvalidPiEmp", m_iClientId));//sharishkumar Jira 827

				sbSQL.Append ("SELECT CODE_DESC FROM CODES_TEXT, PERSON_INVOLVED");
				sbSQL.Append (" WHERE PERSON_INVOLVED.PI_ROW_ID = ");
				sbSQL.Append (m_objPiEmp.PiRowId );
				sbSQL.Append (" AND PERSON_INVOLVED.ILLNESS_CODE = CODES_TEXT.CODE_ID");

				objRdr=DbFactory.GetDbReader(m_sConnectString ,sbSQL.ToString());

				if (objRdr!=null)
				{
					while(objRdr.Read())
					{
						if (sbIllDesc.ToString()=="" )
						{
							sbIllDesc.Append(objRdr.GetString(0));
						}
						else
						{
							sbIllDesc.Append(",");
							sbIllDesc.Append(objRdr.GetString(0));
						}
					}
				}
				if (sbIllDesc.ToString()!="" )
					return "ILLNESS:  " + sbIllDesc.ToString() ;
				else
					return "";
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("FROIExtender.GetIllnessDesc.DataError",m_iClientId) ,
                    p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				sbSQL=null;
				sbIllDesc = null;
			}
		}


		/// <summary>Gets Injury Description from CODES_TEXT, PI_X_INJURY</summary>
		/// <returns>CODE_DESC from CODES_TEXT, PI_X_INJURY</returns>
		private string GetInjuryDesc()
		{
			DbReader objRdr=null;
			StringBuilder sbDesc = null;
			StringBuilder sbSQL= null;
			try
			{
				sbDesc =new StringBuilder();
				sbSQL= new StringBuilder();

				if (m_objPiEmp==null)
                    throw new RMAppException(Globalization.GetString("FROIExtender.InvalidPiEmp", m_iClientId));//sharishkumar Jira 827

				sbSQL.Append ("SELECT CODE_DESC FROM CODES_TEXT, PI_X_INJURY");
				sbSQL.Append (" WHERE PI_X_INJURY.PI_ROW_ID = ");
				sbSQL.Append (m_objPiEmp.PiRowId );
				sbSQL.Append (" AND PI_X_INJURY.INJURY_CODE = CODES_TEXT.CODE_ID");

				objRdr=DbFactory.GetDbReader(m_sConnectString ,sbSQL.ToString());

				if (objRdr!=null)
				{
					while(objRdr.Read())
					{
						if (sbDesc.ToString()=="" )
						{
							sbDesc.Append(objRdr.GetString(0));
						}
						else
						{
							sbDesc.Append(",");
							sbDesc.Append(objRdr.GetString(0));
						}
					}
				}

				if (sbDesc.ToString()!="" )
					return "Injury:  " + sbDesc.ToString() ;
				else
					return "";

			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("FROIExtender.GetInjuryDesc.DataError",m_iClientId) ,
                    p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				sbSQL=null;
				sbDesc=null;
			}
		}


		/// <summary>
		/// Gets Payperiod
		/// </summary>
		/// <param name="p_sPayTypeCode">Pay type code</param>
		/// <returns>pay period</returns>
		public string GetPayPeriod(string p_sPayTypeCode)
		{
			return GetPayPeriodBase(p_sPayTypeCode) ;			
		}


		/// <summary>
		/// Gets State Agency Phone no
		/// </summary>
		/// <returns>pay period</returns>
		public string GetStateAgencyPhoneNumber()
		{
			return GetStateAgencyPhoneNumberBase(); 
		}


		/// <summary>Gets Person Notified EId from POSTAL CODE Supplementals</summary>
		/// <param name="p_objDmf">Data Model Factory</param>
		/// <returns>PERS_NOTIF_EID</returns>
		private int GetPersonNotifiedEID(DataModelFactory p_objDmf)
		{
			string sPostalCode="";
			DataTable objDtTbl=null;
			DataRow[] arrRows=null;
			DbReader objRdr=null;
			Common.LocalCache objChache=null;
			int iEID= 0;
			try
			{
                objChache = new Common.LocalCache(p_objDmf.Context.DbConn.ConnectionString, m_iClientId);
				sPostalCode= objChache.GetStateCode(m_objClaim.FilingStateId);
				objChache=null;
				objRdr=DbFactory.GetDbReader(m_sConnectString ,"SELECT * FROM WC_" + sPostalCode + 
						"_SUPP WHERE CLAIM_ID = "+ m_objClaim.ClaimId  );
				objDtTbl=objRdr.GetSchemaTable();
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						arrRows = objDtTbl.Select("ColumnName='PERS_NOTIF_EID'");
						//Check it column is present in DB
						if(arrRows.GetLength(0)==1)
							iEID= objRdr.GetInt32("PERS_NOTIF_EID");
					}
				}
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("FROIExtender.GetPersonNotifiedEID.DataError",m_iClientId) ,
                    p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				if (objChache!=null)objChache.Dispose();
				arrRows=null;
				objDtTbl=null;
			}
			return iEID;
		}



		/// <summary>Gets PATIENT_ID from PERSON_INVOLVED</summary>
		/// <param name="p_sConnectString">Connection string</param>
		/// <param name="p_iEventID">EVENT_ID</param>
		/// <returns>PATIENT_ID</returns>
        internal static int GetPatientID(string p_sConnectString, int p_iEventID, int p_iClientId)//sharishkumar Jira 827
		{
			DbReader objRdr=null;
			int iPatientID= 0;
			try
			{                
				objRdr=DbFactory.GetDbReader(p_sConnectString ,"SELECT PATIENT_ID FROM PERSON_INVOLVED " +
					"WHERE EVENT_ID = " + p_iEventID + " AND PI_TYPE_CODE = 238" );
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iPatientID= objRdr.GetInt32("PATIENT_ID");
					}
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("FROIExtender.GetPatientID.DataError", p_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();				
			}
			return iPatientID;
		}


		/// <summary>Gets DISPLAY_ID from SYS_EVENT_CAT</summary>
		/// <param name="p_sConnectString">Connection String</param>			
		/// <param name="p_iCodeID">CODES.CODE_ID</param>			
		/// <returns>DISPLAY_ID</returns>
        public static int GetEventCategory(string p_sConnectString, int p_iCodeID, int p_iClientId)//sharishkumar Jira 827
		{
			DbReader objRdr=null;
			int iDisplayID= 0;
			try
			{                
				objRdr=DbFactory.GetDbReader(p_sConnectString ,"SELECT DISPLAY_ID FROM SYS_EVENT_CAT, "
					+"CODES WHERE CODES.CODE_ID = " + p_iCodeID + " AND EVENT_CAT_CODE = CODES.RELATED_CODE_ID");
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iDisplayID= objRdr.GetInt32("DISPLAY_ID");
					}
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("FROIExtender.GetEventCategory.DataError", p_iClientId),
                    p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}
			return iDisplayID;
		}

        /// <summary>Gets DISPLAY_ID from SYS_EVENT_CAT</summary>
        /// <param name="p_iCodeID">CODES.CODE_ID</param>			
        /// <returns>DISPLAY_ID</returns>
		/// Added for FROI Migration - Starts
        public int GetEventCategory(int p_iCodeID)
        {
            DbReader objRdr = null;
            int iDisplayID = 0;
            try
            {
                objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT DISPLAY_ID FROM SYS_EVENT_CAT, "
                    + "CODES WHERE CODES.CODE_ID = " + p_iCodeID + " AND EVENT_CAT_CODE = CODES.RELATED_CODE_ID");
                if (objRdr != null)
                {
                    if (objRdr.Read())
                    {
                        iDisplayID = objRdr.GetInt32("DISPLAY_ID");
                    }
                }
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("FROIExtender.GetEventCategory.DataError",m_iClientId),
                    p_objExp);//sharishkumar Jira 827
            }
            finally
            {
                if (objRdr != null) objRdr.Dispose();
            }
            return iDisplayID;
        }



		/// <summary>Gets CONTACT_NAME</summary>
		/// <param name="p_iOption">OPTION</param>
		internal void GetContact(int p_iOption)
		{
			DbReader objRdr=null;
			bool bDone=false;
			string sSQL="SELECT TC_CONTACT_NAME, TC_CONTACT_TITLE, TC_CONTACT_PHONE FROM CLAIM_SUPP" +
				" WHERE CLAIM_ID = "+ m_objClaim.ClaimId + " AND TC_CONTACT_NAME IS NOT NULL";
			try
			{                
				switch(p_iOption)
				{
						#region Case 1
					case 1:
						objRdr=DbFactory.GetDbReader(m_sConnectString ,sSQL);
						if (objRdr!=null)
						{
							if(objRdr.Read())
							{
								//populate contact variables
								m_objContact.LastName  = objRdr.GetString("TC_CONTACT_NAME");
								m_objContact.Title  = objRdr.GetString("TC_CONTACT_TITLE");
								m_objContact.Phone1   = objRdr.GetString("TC_CONTACT_PHONE");
							}
						}
						break;
						#endregion

						#region Case 2
					case 2:
						m_objContact.LastName = m_objCompany.Contact  ;
						m_objContact.Phone1 = m_objCompany.Phone1 ;  
						break;
						#endregion

						#region Case 3
					case 3:
						objRdr=DbFactory.GetDbReader(m_sConnectString ,sSQL);
						if (objRdr!=null)
						{
							if(objRdr.Read())
							{
								bDone=true;
								//populate contact variables
								m_objContact.LastName  = objRdr.GetString("TC_CONTACT_NAME");
								m_objContact.Title  = objRdr.GetString("TC_CONTACT_TITLE");
								m_objContact.Phone1   = objRdr.GetString("TC_CONTACT_PHONE");
							}
						}
						if (!bDone)
						{
							m_objContact.LastName = m_objCompany.LastName ;
							m_objContact.Phone1 = m_objCompany.Phone1 ;  
						}
						break;
						#endregion

						#region default
					default:
						break;
						#endregion
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("FROIExtender.GetContact.DataError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}

		}

		/// <summary>
		/// Gets the Person notified
		/// </summary>
		/// <param name="p_objDmf">DataModelFactory</param>
		internal void GetPersonNotified(DataModelFactory p_objDmf)
		{
			DbReader objRdr=null;
			int iOption=0; 
			int iEntity=0;
			bool bFound=false;
			string sSql="";
			try
			{   
				if (m_objOptions.PersonNotifiedOption ==-1)
					iOption=3;
				else
					iOption= m_objOptions.PersonNotifiedOption;

				//Get object from datamodelfactory
				m_objPersonNotified  = (Entity )p_objDmf.GetDataModelObject("Entity",false);
				iEntity=GetPersonNotifiedEID(p_objDmf) ;
				if (iEntity>0)
				{
					m_objPersonNotified .MoveTo(iEntity);
				}

				sSql="SELECT *  FROM CLAIM_SUPP WHERE CLAIM_ID = "+ m_objClaim.ClaimId ;
				switch(iOption)
				{
						#region Case 1
					case 1:		//only claim TC supp fields
						// Both case 1 and 3 do the same thing. Thus put together.
						#endregion

						#region Case 2
					case 3:		//TC supp fields if filled in, else Jurisdictional Supp person notified
						objRdr=DbFactory.GetDbReader(m_sConnectString ,sSql);
						if (objRdr!=null)
						{
							if(objRdr.Read())
							{
								bFound=true;
								m_objPersonNotified.LastName  = objRdr.GetString("TC_REPORTED_NAME");
								m_objPersonNotified.Title  = objRdr.GetString("TC_CONTACT_TITLE");
								m_objPersonNotified.Phone1   = objRdr.GetString("TC_CONTACT_PHONE");
							}
							if (!bFound) m_objPersonNotified =null;
						}
						break;
						#endregion

						#region Case 3
					case 2:		//only department contact person notified
						if (m_objPiEmp!=null)
						{
							if (m_objPiEmp.DeptAssignedEid >0)
							{
								Entity objE  = (Entity )p_objDmf.GetDataModelObject("Entity",false);
								objE.MoveTo(m_objPiEmp.DeptAssignedEid); 
								m_objPersonNotified.LastName = objE.LastName; 
								m_objPersonNotified.Title = objE.Title;
								m_objPersonNotified.Phone1  = objE.Phone1;
								objE=null;
							}
							else
								m_objPersonNotified=null;
						}

						break;
						#endregion
				}
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("FROIExtender.GetPersonNotified.DataError",m_iClientId) ,
                    p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}
		}


		/// <summary>
		/// Get Facility Details
		/// </summary>
		/// <param name="p_objDmf">DataModelFactory</param>
        internal void GetFacility(DataModelFactory p_objDmf)//sharishkumar Jira 827
		{
			string sAdr4="";
			Common.LocalCache objChache=null;
			try
			{
				m_objFac= (Entity)p_objDmf.GetDataModelObject("Entity",false);
				m_objFac.LastName =p_objDmf.Context.InternalSettings.SysSettings.OCFacName;
				m_objFac.Addr1  =p_objDmf.Context.InternalSettings.SysSettings.OCFacAddr1; 
				m_objFac.Addr2 =p_objDmf.Context.InternalSettings.SysSettings.OCFacAddr2; 
				m_objFac.City  =p_objDmf.Context.InternalSettings.SysSettings.OCFacAddr3;
                //m_objFac.Addr4 = p_objDmf.Context.InternalSettings.SysSettings.OCFacAddr4; // JIRA 6420 syadav55 
                // npadhy - This is state. If we want to include Address3 and 4 for this as well. We will have to Alter SYS_SETTINGS
				sAdr4 =p_objDmf.Context.InternalSettings.SysSettings.OCFacAddr4; 
				m_objFac.Phone1 =p_objDmf.Context.InternalSettings.SysSettings.OCFacPhone; 
				m_objFac.Contact  =p_objDmf.Context.InternalSettings.SysSettings.OCFacContact;
				m_sFacilityHCFA_objExt= p_objDmf.Context.InternalSettings.SysSettings.OCFacHCFANo;
			
				if (m_sFacilityHCFA_objExt=="")
					m_sFacilityHCFA_objExt = "000000";

				if (sAdr4!="")
				{
                    objChache = new Common.LocalCache(p_objDmf.Context.DbConn.ConnectionString, m_iClientId);
					m_objFac.StateId =  objChache.GetStateRowID(sAdr4.Substring(0,2)); 
					m_objFac.ZipCode = sAdr4;
					objChache.Dispose();
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("FROIExtender.GetFacility.AppError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if (objChache!=null)objChache.Dispose();
			}
		}


		/// <summary>Gets TEST_DATE,LAB_TEST,RESULT from EVENT_X_MEDW_TEST</summary>
		/// <param name="p_iEventID">EVENT_ID</param>
        internal void GetRelevantTests(int p_iEventID)//sharishkumar Jira 827
		{
			DbReader objRdr=null;
			StringBuilder sbTmp=null;
			int iCount=0;
			const string Spacer = "                    ";
			try
			{                
				sbTmp=new StringBuilder();
				objRdr=DbFactory.GetDbReader(m_sConnectString ,
					"SELECT TEST_DATE,LAB_TEST,RESULT" +
					" FROM EVENT_X_MEDW_TEST WHERE EVENT_X_MEDW_TEST.EVENT_ID = "+ 
					p_iEventID+" ORDER BY TEST_DATE ");
				if (objRdr!=null)
				{
					while(objRdr.Read())
					{
						sbTmp.Append(Conversion.GetDBDateFormat(objRdr.GetString(0),"d") + Spacer+ 
							objRdr.GetString(1)+ "\n" + "Results:" +  Spacer + objRdr.GetString(2));
						iCount++;
					}
				}
				if (iCount>0)
				{
					sbTmp.Append("See attached");
					m_sMainFormRT_objExt= sbTmp.ToString();   
					if (iCount>3)
					{
						m_sAttachmentRT_objExt= Globalization.GetString("FROIExtender.GetRelevantTests.Msg",m_iClientId) + Spacer +
                            m_sUFDistReportNumber_objExt;//sharishkumar Jira 827
					}
				}
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("FROIExtender.GetRelevantTests.DataError",m_iClientId) ,
                    p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				sbTmp = null;
			}
		}

		/// <summary>
		/// GetUFDistReportNumber
		/// </summary>
		internal void GetUFDistReportNumber()
		{
			m_sUFDistReportNumber_objExt=m_sFacilityHCFA_objExt + "-" + m_objEvent.EventMedwatch.ReportYear + "-" +
			   String.Format("0000",m_objEvent.EventMedwatch.ReportSerialNo); 
		}

        /// <summary>
     

        /// <summary>
        /// Is the calculate months.Added for Froi Migration- Mits 33585,33586,33587
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public int iCalculateMonths(string startDate, string endDate)
        {
            return Utilities.GetDateDiffInYears(Conversion.GetDate(startDate), Conversion.GetDate(endDate), m_iClientId);
        }

        /// <summary>
        /// CONCOM_PRODUCT Details
        /// </summary>
        /// <param name="p_iEventID">EVENT_ID</param>
        internal void GetConcomitantProducts(int p_iEventID)//sharishkumar Jira 827
		{
			DbReader objRdr=null;
			StringBuilder sbTmp=null;
			int iCount=0;
			const string spacer="                    ";
			try
			{                
				sbTmp=new StringBuilder();
				objRdr=DbFactory.GetDbReader(m_sConnectString ,
					"SELECT FROM_DATE,TO_DATE,CONCOM_PRODUCT FROM EV_X_CONCOM_PROD WHERE EVENT_ID = " +
					p_iEventID + " ORDER BY FROM_DATE"); 
				if (objRdr!=null)
				{
					while(objRdr.Read())
					{
						sbTmp.Append(Conversion.GetDBDateFormat(objRdr.GetString("FROM_DATE"),"d") + spacer + 
							Conversion.GetDBDateFormat(objRdr.GetString("TO_DATE"),"d") + spacer + 
							objRdr.GetString("CONCOM_PRODUCT"));  
						iCount++;
					}
				}

				if (iCount>3)
					sbTmp.Append("See attached");
				
				m_sMainFormCP_objExt = sbTmp.ToString(); 
				if (iCount>3)
				{
					m_sAttachmentCP_objExt = " Concomitant Medical Products - (Continued)"   +  
						m_sUFDistReportNumber_objExt;
				}
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("FROIExtender.GetConcomitantProducts.DataError",m_iClientId) ,
                    p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}
		}


		/// <summary>
		/// Gets the TopInsured
		/// </summary>
        /// <param name="p_objDmf">DataModelFactory Object</param>
        //tkatsarski: 04/27/15 Start changes for RMA-8854: MITS 37905 - Tennessee FROI opens with "an error occurred while getting Top Insured" message
        internal void GetTopInsured(DataModelFactory p_objDmf)
        {
            string sFldName = string.Empty;
            int iTopIns = 0;
            StringBuilder sSql = new StringBuilder();
            int policyId = default(int);
            bool useEnhancePolicy = default(bool);
            try
            {
                m_objJurisParent_objExt = null;
                useEnhancePolicy = this.m_objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag.Equals(-1);
                policyId = useEnhancePolicy ? this.m_objClaim.PrimaryPolicyEnh.PolicyId : this.m_objClaim.PrimaryPolicy.PolicyId;

                if (policyId.Equals(default(int)))
                {
                    return;
                }

                sSql.Append("SELECT ENTITY.ENTITY_TABLE_ID, ENTITY.ENTITY_ID FROM ENTITY")
                    .AppendFormat(" JOIN {0} ON {0}.INSURED_EID = ENTITY.ENTITY_ID ", useEnhancePolicy ? "POLICY_X_INSRD_ENH" : "POLICY_X_INSURED")
                    .AppendFormat(" WHERE POLICY_ID = {0} ORDER BY ENTITY.ENTITY_TABLE_ID ASC", policyId);

                using (DbReader objRdrEntity = DbFactory.GetDbReader(m_sConnectString, sSql.ToString()))
                {
                    if (!object.ReferenceEquals(objRdrEntity, null))
                    {
                        while (objRdrEntity.Read())
                        {
                            sFldName = Conversion.EntityTableIdToOrgTableName(objRdrEntity.GetInt32("ENTITY_TABLE_ID"));
                            sSql.Clear();
                            sSql.Append("SELECT * FROM ORG_HIERARCHY")
                                .AppendFormat(" WHERE DEPARTMENT_EID = {0} AND {1}_EID = {2}", m_objPiEmp.DeptAssignedEid, sFldName, objRdrEntity.GetInt32("ENTITY_ID"));

                            using (DbReader objRdrOrg = DbFactory.GetDbReader(m_sConnectString, sSql.ToString()))
                            {
                                if (!object.ReferenceEquals(objRdrOrg, default(DbReader)) && objRdrOrg.Read())
                                {
                                    iTopIns = objRdrEntity.GetInt32("ENTITY_ID");
                                    break;
                                }
                            }
                        }
                    }
                }

                if (iTopIns > 0)
                {
                    m_objJurisParent_objExt = GetEntity(p_objDmf, iTopIns, m_iClientId);//sharishkumar Jira 827
                }
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("FROIExtender.GetTopInsured.DataError", m_iClientId), p_objExp);//sharishkumar Jira 827
            }
        }
        //tkatsarski: 04/27/15 End changes for RMA-8854

		#endregion

	}	// end class
}		// end namespace
