﻿using System;
using System.Data;
using System.IO;
using System.Xml;
using System.Collections;
using System.Text;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Application.FileStorage;
using Riskmaster.Application.SecurityManagement;
using Riskmaster.Security;
using Riskmaster.Models; 
using System.Collections.Generic;
using Amyuni.PDFCreator;
using System.Threading;
using Riskmaster.Settings;
namespace Riskmaster.Application.DocumentManagement
{
    ///************************************************************** 
    ///* $File		: DocumentManager.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 29-Oct-2004 
    ///* $Author	: Aditya Babbar
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    ///* Amendment  -->
    ///  1.Date		: 7 Feb 06 
    ///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
    ///				  violation exception so it can be correctly called. 
    ///    Author	: Sumit
    ///**************************************************************
    /// <summary>	
    /// Manages all operations related to documents, attachments 
    /// and folders including add, update, copy, transfer and move.				
    /// </summary>
    public class DocumentManager
    {
        
        #region Constants

        /// <summary>
        /// FolderId of Recycle Bin for all users
        /// </summary>
        private const long RECYCLE_BIN_ID = -1;

        /// <summary>
        /// Folder name of Recycle Bin for all users
        /// </summary>
        private const string RECYCLE_BIN_NAME = "Recycle Bin";

        /// <summary>
        /// Function IDs for User Document related security permission
        /// </summary>
        private const int USERDOC_SID = 50;
        private const int USERDOC_VIEW_SID = 51;
        private const int USERDOC_CREATE_SID = 80;
        private const int USERDOC_MODIFY_SID = 81;
        private const int USERDOC_DELETE_SID = 82;
        private const int USERDOC_VIEWPLAY_SID = 83;
        private const int USERDOC_PRINT_SID = 84;
        private const int USERDOC_DETATCH_SID = 85;
        private const int USERDOC_EXPORT_SID = 86;
        private const int USERDOC_TRANSFER_SID = 87;
        private const int USERDOC_COPY_SID = 88;

        private const int ATTDOC_ATTACHTO_SID = 8;
        private const int ATTDOC_DELETE_SID = 9;
        //MITS 27764 hlv 5/23/12 begin
        private const int ATTDOC_ADDNEW_SID_EX   = 1;
        private const int ATTDOC_DELETE_SID_EX   = 2;
        private const int ATTDOC_EDIT_SID_EX     = 3;
        private const int ATTDOC_EMAIL_SID_EX    = 4;
        private const int ATTDOC_DOWNLOAD_SID_EX = 5;
        //MITS 27764 hlv 5/23/12 end

        private const string FUNCNAME_COPY = "copy";
        private const string FUNCNAME_CREATE = "create";
        private const string FUNCNAME_DELETE = "delete";
        private const string FUNCNAME_DETATCH = "detach";
        private const string FUNCNAME_EXPORT = "export";
        private const string FUNCNAME_MODIFY = "modify";
        private const string FUNCNAME_PRINT = "print";
        private const string FUNCNAME_TRANSFER = "transfer";
        private const string FUNCNAME_VIEW = "view";
        private const string FUNCNAME_VIEWPLAY = "viewplay";
        //MITS 27764 hlv 5/23/12
        private const string FUNCNAME_DOWNLOAD = "download"; 

        //Nikhil Garg		Defect No: 2397		Dated: 03/10/2006
        private string m_sFormName = string.Empty;
        private const System.Single CONVERSION_FACTOR = 1024f;//1 MB = 1024 KB,1KB = 1024 BYTES
        #endregion

        #region Member Variables

        /// <summary>		
        /// Database connection string
        /// </summary>
        private string m_sConnectionString;

        /// <summary>
        /// Security DB connection string
        /// </summary>
        private string m_sSecurityConnectionString; //MITS 27764 hlv 5/23/12

        /// <summary>		
        /// User Login Name
        /// </summary>
        private string m_sUserLoginName;

        /// <summary>		
        /// Security Id
        /// </summary>
        private long m_lSecurityId;

        /// <summary>		
        /// Table name to which documents is attached.
        /// </summary>
        private string m_sTableName;

        /// <summary>		
        /// Storage Type. 
        /// </summary>
        private StorageType m_enmDocumentStorageType;

        /// <summary>		
        /// Destination Storage Path. For database storage
        /// it is the connection string, and for file storage
        /// it is the folder path.
        /// </summary>
        private string m_sDestinationStoragePath;

        /// <summary>		
        /// m_userLogin: Passed from Adapter and used to check permission 
        /// </summary>
        protected UserLogin m_userLogin;

        private int m_iStorageId;

        // akaushik5 Added for MITS 30748 Starts
        /// <summary>
        /// Indicates document read is in process 
        /// </summary>
        public static bool IsReadingDocument;
        // akaushik5 Added for MITS 30748 Starts

        /// <summary>
        /// Private variable for Client Id
        /// </summary>
        private int m_iClientId = 0;

        #endregion

        #region Properties


        /* All string properties are set to string.Empty if a
		 * null value is assigned to them. This ensures a more robust
		 * component free of NullValueExceptions generated inadvertantly
		 * if the client sets a string property to null. 
		 * */

        /// <summary>
        /// Database Connection String
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return m_sConnectionString;
            }
            set
            {
                if (value != null)
                {
                    m_sConnectionString = value;
                }
                else
                {
                    m_sConnectionString = string.Empty;
                }
            }
        }

        /// <summary>
        /// Security Database Connection String
        /// </summary>
        public string SecurityConnectionString
        {
            get 
            {
                return m_sSecurityConnectionString;
            }
            set 
            {
                if (value != null)
                {
                    m_sSecurityConnectionString = value;
                }
                else
                {
                    m_sSecurityConnectionString = string.Empty;
                }
            }
        }

        /// <summary>
        /// User Login Name
        /// </summary>
        public string UserLoginName
        {
            get
            {
                return m_sUserLoginName;
            }
            set
            {
                if (value != null)
                {
                    m_sUserLoginName = value;
                }
                else
                {
                    m_sUserLoginName = string.Empty;
                }
            }
        }

        /// <summary>
        /// Security Id
        /// </summary>
        public long SecurityId
        {
            get
            {
                return m_lSecurityId;
            }
            set
            {
                m_lSecurityId = value;
            }
        }

        /// <summary>
        /// Table name to which document is attached.
        /// </summary>
        public string TableName
        {
            get
            {
                return m_sTableName;
            }
            set
            {
                if (value != null)
                {
                    m_sTableName = value;
                }
                else
                {
                    m_sTableName = string.Empty;
                }
            }
        }

        /// <summary>
        /// Storage Type. If set, it overrides the value set 
        /// for the application from the configuration file.
        /// </summary>
        public StorageType DocumentStorageType
        {
            get
            {
                return m_enmDocumentStorageType;
            }
            set
            {
                m_enmDocumentStorageType = value;
            }
        }

        /// <summary>
        /// Document Storage Path. If set, it overrides the value set 
        /// for the application from the configuration file.
        /// </summary>
        public string DestinationStoragePath
        {
            get
            {
                return m_sDestinationStoragePath;
            }
            set
            {
                if (value != null)
                {
                    m_sDestinationStoragePath = value;
                }
                else
                {
                    m_sDestinationStoragePath = string.Empty;
                }
            }
        }
        /// <summary>
        /// This will track updating the stored document in database server. Tanuj
        /// </summary>
        public int StorageId
        {
            get
            {
                return m_iStorageId;
            }
            set
            {
                m_iStorageId = value;
            }
        }

        #endregion

        #region Constructor

        /// Name			: DocumentManager
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************

        /// <summary>
        /// Constructor
        /// </summary>
        //		public DocumentManager()
        //		{		
        //			m_sConnectionString = string.Empty;
        //			m_sUserLoginName = string.Empty;
        //			m_lSecurityId = 0;
        //			m_sTableName = string.Empty;
        //			m_enmDocumentStorageType = StorageType.FileSystemStorage;
        //			m_sDestinationStoragePath = string.Empty; 
        //		}

        /// <summary>
        /// Constructor: 
        /// </summary>
        /// <param name="oLogin"> Riskmaster.Security.UserLogin object</param>

        public DocumentManager(UserLogin oLogin, int p_iClientId)
        {
            m_sConnectionString = string.Empty;
            m_sUserLoginName = string.Empty;
            m_lSecurityId = 0;
            m_sTableName = string.Empty;
            m_enmDocumentStorageType = (StorageType)oLogin.objRiskmasterDatabase.DocPathType ;//rkaur27 : RMACLOUD - 1125
            m_sDestinationStoragePath = string.Empty;
            m_userLogin = oLogin;
            m_iClientId = p_iClientId;
        }

        /// <summary>
        /// Constructor: 
        /// </summary>
        /// <param name="oLogin"> Riskmaster.Security.UserLogin object</param>
        /// <param name="p_lpsid"> Security Id</param>
        public DocumentManager(UserLogin oLogin, long p_lpsid, int p_iClientId)
        {
            m_sConnectionString = string.Empty;
            m_sUserLoginName = string.Empty;
            m_lSecurityId = p_lpsid;
            m_sTableName = string.Empty;
            m_enmDocumentStorageType = (StorageType)oLogin.objRiskmasterDatabase.DocPathType;//rkaur27: RMACLOUD - 1125
            m_sDestinationStoragePath = string.Empty;
            m_userLogin = oLogin;
            m_iClientId = p_iClientId;
        }

        #endregion

        /// <summary>
        /// Enum for display order (order by clause)
        /// </summary>
        private enum DOCSORTORDER : int
        {
            NONE,
            ASC_NAME,
            ASC_SUBJECT,
            ASC_TYPE,
            ASC_CLASS,
            ASC_CATEGORY,
            ASC_CREATE_DATE,
            DEC_NAME,
            DEC_SUBJECT,
            DEC_TYPE,
            DEC_CLASS,
            DEC_CATEGORY,
            DEC_CREATE_DATE
        }

        #region Public Functions

        /// Name			: AddFolder
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Creates new folder record in the database table DOC_FOLDERS.
        /// Uses ConnectionString and UserLoginName property values.
        /// </summary>
        /// <param name="p_lParentId"> 
        ///		Parent FolderId of the folder to be created.
        /// </param>
        /// <param name="p_sFolderName"> 
        ///		Name of the folder to be created.
        ///	</param>
        /// <param name="p_lFolderId">
        ///		Out parameter. Returns FolderId for newly created folder.
        ///	</param>
        /// <returns>0 - Success</returns> 
        public int AddFolder(long p_lParentId, string p_sFolderName,
            out long p_lFolderId)
        {
            int iReturnValue = 0;

            DbReader objDbReader = null;
            string sSQL = string.Empty;

            DbWriter objDbWriter = null;

            try
            {
                RaiseInitializationException();

                p_sFolderName = p_sFolderName.Trim();

                // Replace special characters from the folder name
                p_sFolderName = p_sFolderName.Replace("/", String.Empty);
                p_sFolderName = p_sFolderName.Replace(@"\", String.Empty);
                p_sFolderName = p_sFolderName.Replace("%", String.Empty);
                p_sFolderName = p_sFolderName.Replace(":", String.Empty);
                p_sFolderName = p_sFolderName.Replace("*", String.Empty);
                p_sFolderName = p_sFolderName.Replace("?", String.Empty);
                p_sFolderName = p_sFolderName.Replace("<", String.Empty);
                p_sFolderName = p_sFolderName.Replace(">", String.Empty);
                p_sFolderName = p_sFolderName.Replace("|", String.Empty);
                //amrit:added for mits:14170
                p_sFolderName = p_sFolderName.Replace("'", "''");
                //changes ends              
                if (p_sFolderName.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.AddFolder.InvalidFolderName", m_iClientId));
                }

                sSQL = "SELECT FOLDER_ID "
                    + " FROM DOC_FOLDERS "
                    + " WHERE FOLDER_NAME='" + p_sFolderName + "'"
                    + " AND PARENT_ID=" + p_lParentId.ToString()
                    + " AND USER_ID='" + this.UserLoginName + "'";

                objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                if (objDbReader.Read())
                {
                    // Folder already exists
                    p_lFolderId = Convert.ToInt64(objDbReader.GetValue("FOLDER_ID"));
                    objDbReader.Close();
                    objDbReader = null;
                    return iReturnValue;
                }

                objDbReader.Close();
                objDbReader = null;

                // Check whether parent folder exists
                if (p_lParentId != 0)
                {
                    sSQL = "SELECT FOLDER_ID "
                        + " FROM DOC_FOLDERS "
                        + " WHERE FOLDER_ID=" + p_lParentId.ToString()
                        + " AND USER_ID='" + this.UserLoginName + "'";

                    objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                    if (!objDbReader.Read())
                    {
                        // Parent Folder does not exist
                        throw new InvalidValueException(Globalization.GetString("DocumentManager.AddFolder.InvalidParentId", m_iClientId));
                    }
                }
                //amrit:added for mits:14170
                p_sFolderName = p_sFolderName.Replace("''", "'");
                //amrit:changes ends
                p_lFolderId = Utilities.GetNextUID(this.ConnectionString,
                    "DOC_FOLDERS", m_iClientId);

                objDbWriter = DbFactory.GetDbWriter(this.ConnectionString);

                objDbWriter.Tables.Add("DOC_FOLDERS");

                objDbWriter.Fields.Add("FOLDER_ID", p_lFolderId);
                objDbWriter.Fields.Add("PARENT_ID", p_lParentId);
                objDbWriter.Fields.Add("FOLDER_NAME", p_sFolderName);
                objDbWriter.Fields.Add("FOLDER_PATH", DBNull.Value);
                objDbWriter.Fields.Add("USER_ID", this.UserLoginName);

                objDbWriter.Execute();

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.AddFolder.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }

                objDbWriter = null;
            }

            return iReturnValue;
        }

        /// Name			: GetAttachments
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************		
        /// <summary>
        /// Get data for attachments in form of XML
        /// Uses TableName and ConnectionString property values.
        /// </summary>
        /// <param name="p_sFormName">
        ///		Form name set as an attribute in root element of output XML
        /// </param>
        /// <param name="p_lRecordId">
        ///		Specific record id to which the document is attached. 
        ///	</param>
        /// <param name="p_sAttachmentsXML">
        ///		Out parameter. Returns attachments data in form of XML
        ///	</param>
        /// <returns>0 - Success</returns> 	
        public int GetAttachments(int lSortId, string p_sFormName, long p_lRecordId,
            out string p_sAttachmentsXML)
        {
            int iReturnValue = 0;
            DataSet objDataSet = null;
            DataTable objDocumentTable = null;

            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objFolderElement = null;
            XmlElement objXmlElement = null;

            XmlDocument objXml = null;      // Umesh
            XmlNode objSessionDSN = null; // Umesh
            //DbReader objRdr=null;//Umesh
            string sSQL = string.Empty;
            int iSortOrder = 0;
            //Mona:PaperVisionMerge : Animesh Inserted 
            DbConnection objCon = null;
            //Animesh Insertion Ends
            LocalCache objCache = null;
            try
            {
                RaiseInitializationException();
            //Mona:PaperVisionMerge : Animesh Inserted 
                objCon = DbFactory.GetDbConnection(this.ConnectionString);
                objCon.Open();  
                //Starts- Nitesh: 12/05/2006 Changed SQL for sorting on codes
                sSQL = "SELECT DOCUMENT.DOCUMENT_ID,DOCUMENT_NAME,"
                    + " DOCUMENT.DOCUMENT_FILENAME,DOCUMENT.KEYWORDS, "
                    + " DOCUMENT.CREATE_DATE,DOCUMENT.NOTES,DOCUMENT.FOLDER_ID,"
                    + " DOCUMENT.USER_ID, (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID =DOCUMENT.DOCUMENT_CLASS) AS DOCUMENT_CLASS,"
                    + " (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT.DOCUMENT_CATEGORY) AS DOCUMENT_CATEGORY,"
                    + " DOCUMENT.DOCUMENT_SUBJECT, "
                    + " (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT.DOCUMENT_TYPE) AS DOCUMENT_TYPE, DOCUMENT.DOC_INTERNAL_TYPE, DOCUMENT.DOCUMENT_FILEPATH "
                    + " FROM DOCUMENT ,DOCUMENT_ATTACH ";
                //End- Nitesh: 12/05/2006 Changed SQL for sorting on codes
                if (this.TableName.Length == 0)
                {
                    //Starts: Code Commented By Nitesh: 12/05/2006 
                    // Get all attachments
                    //					sSQL = "SELECT DOCUMENT.DOCUMENT_ID,DOCUMENT.DOCUMENT_NAME,"
                    //						+ " DOCUMENT.DOCUMENT_FILENAME,DOCUMENT.KEYWORDS,"
                    //						+ " DOCUMENT.CREATE_DATE, DOCUMENT.NOTES, DOCUMENT.FOLDER_ID,"
                    //						+ " DOCUMENT.USER_ID, DOCUMENT.DOCUMENT_CLASS,"
                    //						+ " DOCUMENT.DOCUMENT_CATEGORY, DOCUMENT.DOCUMENT_SUBJECT,"
                    //						+ " DOCUMENT.DOCUMENT_TYPE"
                    //						+ " FROM DOCUMENT, DOCUMENT_ATTACH "
                    //Ends Commented By Nitesh: 12/05/2006 
                    sSQL = sSQL + " WHERE DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID";
                    //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                    if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                        sSQL = sSQL + " AND NVL(DOCUMENT.ISAT_PV,2) <> -1 ";
                    else
                        sSQL = sSQL + " AND ISNULL(DOCUMENT.ISAT_PV,2) <> -1 ";

                    //Animesh Insertion Ends
                }
                else if (p_lRecordId == 0)
                {
                    //Starts: Code Commented By Nitesh: 12/05/2006 
                    // Get all attachments for a particular table
                    //					sSQL = "SELECT DOCUMENT.DOCUMENT_ID,DOCUMENT.DOCUMENT_NAME,"
                    //						+ " DOCUMENT.DOCUMENT_FILENAME,DOCUMENT.KEYWORDS,"
                    //						+ " DOCUMENT.CREATE_DATE, DOCUMENT.NOTES, DOCUMENT.FOLDER_ID,"
                    //						+ " DOCUMENT.USER_ID, DOCUMENT.DOCUMENT_CLASS,"
                    //						+ " DOCUMENT.DOCUMENT_CATEGORY, DOCUMENT.DOCUMENT_SUBJECT,"
                    //						+ " DOCUMENT.DOCUMENT_TYPE"
                    //						+ " FROM DOCUMENT, DOCUMENT_ATTACH "
                    //Ends Commented By Nitesh: 12/05/2006 
                    sSQL = sSQL + " WHERE UPPER(DOCUMENT_ATTACH.TABLE_NAME)=UPPER('" + this.TableName + "')"
                    + " AND DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID";
                    //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                    if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                        sSQL = sSQL + " AND NVL(DOCUMENT.ISAT_PV,2) <> -1 ";
                    else
                        sSQL = sSQL + " AND ISNULL(DOCUMENT.ISAT_PV,2) <> -1 ";

                    //Animesh Insertion Ends
                }
                else
                {
                    //Starts: Code Commented By Nitesh: 12/05/2006 
                    // Get all attachments for a particular table and a particular record
                    //					sSQL = "SELECT DOCUMENT.DOCUMENT_ID,DOCUMENT.DOCUMENT_NAME,"
                    //						+ " DOCUMENT.DOCUMENT_FILENAME,DOCUMENT.KEYWORDS,"
                    //						+ " DOCUMENT.CREATE_DATE, DOCUMENT.NOTES, DOCUMENT.FOLDER_ID,"
                    //						+ " DOCUMENT.USER_ID, DOCUMENT.DOCUMENT_CLASS,"
                    //						+ " DOCUMENT.DOCUMENT_CATEGORY, DOCUMENT.DOCUMENT_SUBJECT,"
                    //						+ " DOCUMENT.DOCUMENT_TYPE"
                    //						+ " FROM DOCUMENT, DOCUMENT_ATTACH "
                    //Ends Commented By Nitesh: 12/05/2006 
                    sSQL = sSQL + " WHERE UPPER(DOCUMENT_ATTACH.TABLE_NAME)=UPPER('" + this.TableName + "')"
                    + " AND DOCUMENT_ATTACH.RECORD_ID=" + p_lRecordId.ToString()
                    + " AND DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID";
                    //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                    if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                        sSQL = sSQL + " AND NVL(DOCUMENT.ISAT_PV,2) <> -1 ";
                    else
                        sSQL = sSQL + " AND ISNULL(DOCUMENT.ISAT_PV,2) <> -1 ";

                    //Animesh Insertion Ends
                }

                // *BEGIN* JP 12-1-2006  Added sorting code for attachments case
                switch (lSortId)
                {
                    case (int)DOCSORTORDER.ASC_NAME:
                        sSQL = sSQL + " ORDER BY DOCUMENT_NAME ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_NAME;
                        break;
                    case (int)DOCSORTORDER.ASC_SUBJECT:
                        sSQL = sSQL + " ORDER BY DOCUMENT_SUBJECT ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_SUBJECT;
                        break;
                    case (int)DOCSORTORDER.ASC_TYPE:
                        sSQL = sSQL + " ORDER BY DOCUMENT_TYPE ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_TYPE;
                        break;
                    case (int)DOCSORTORDER.ASC_CLASS:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CLASS ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CLASS;
                        break;
                    case (int)DOCSORTORDER.ASC_CATEGORY:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CATEGORY ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CATEGORY;
                        break;
                    case (int)DOCSORTORDER.ASC_CREATE_DATE:
                        sSQL = sSQL + " ORDER BY CREATE_DATE ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CREATE_DATE;
                        break;
                    case (int)DOCSORTORDER.DEC_NAME:
                        sSQL = sSQL + " ORDER BY DOCUMENT_NAME DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_NAME;
                        break;
                    case (int)DOCSORTORDER.DEC_SUBJECT:
                        sSQL = sSQL + " ORDER BY DOCUMENT_SUBJECT DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_SUBJECT;
                        break;
                    case (int)DOCSORTORDER.DEC_TYPE:
                        sSQL = sSQL + " ORDER BY DOCUMENT_TYPE DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_TYPE;
                        break;
                    case (int)DOCSORTORDER.DEC_CLASS:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CLASS DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CLASS;
                        break;
                    case (int)DOCSORTORDER.DEC_CATEGORY:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CATEGORY DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CATEGORY;
                        break;
                    case (int)DOCSORTORDER.DEC_CREATE_DATE:
                        sSQL = sSQL + " ORDER BY CREATE_DATE DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CREATE_DATE;
                        break;
                    default:
                        sSQL = sSQL + " ORDER BY DOCUMENT_NAME ASC";
                        lSortId = (int)DOCSORTORDER.ASC_NAME;
                        iSortOrder = (int)DOCSORTORDER.ASC_NAME;
                        break;
                }
                objCon.Close();   //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                // *END* JP 12-1-2006  Added sorting code for attachments case

                objDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);
                objDocumentTable = objDataSet.Tables[0];

                objXmlDocument = new XmlDocument();
                objRootElement = objXmlDocument.CreateElement("data");
                objRootElement.SetAttribute("name", "Attachments");
                objRootElement.SetAttribute("pid", p_lRecordId.ToString());
                objRootElement.SetAttribute("sid", this.SecurityId.ToString());
                objRootElement.SetAttribute("table", this.TableName);
                objRootElement.SetAttribute("form", p_sFormName);
                objRootElement.SetAttribute("orderby", iSortOrder.ToString());  // JP 12-1-2006: Support for sorting added to attachments

                //check for permission, get psid from the request if it is available
                m_sFormName = p_sFormName;
                CheckDocListPermissions(ref objRootElement, Convert.ToInt32(m_lSecurityId));

                objXmlDocument.AppendChild(objRootElement);

                objFolderElement = objXmlDocument.CreateElement("folder");
                objFolderElement.SetAttribute("pid", "0");

                objRootElement.AppendChild(objFolderElement);


                objCache = new LocalCache(m_sConnectionString,m_iClientId);

                foreach (DataRow objRow in objDocumentTable.Rows)
                {
                    objXmlElement = objXmlDocument.CreateElement("document");

                    objXmlElement.SetAttribute("pid", objRow["DOCUMENT_ID"].ToString());
                    objXmlElement.SetAttribute("title", objRow["DOCUMENT_NAME"].ToString());
                    objXmlElement.SetAttribute("folderid", objRow["FOLDER_ID"].ToString());
                    objXmlElement.SetAttribute("keywords", objRow["KEYWORDS"].ToString());
                    objXmlElement.SetAttribute("createdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objRow["CREATE_DATE"].ToString()), "d"));
                    objXmlElement.SetAttribute("filename", objRow["DOCUMENT_FILENAME"].ToString());
                    objXmlElement.SetAttribute("userid", objRow["USER_ID"].ToString());
                    objXmlElement.SetAttribute("subject", objRow["DOCUMENT_SUBJECT"].ToString());
                    objXmlElement.SetAttribute("type", objRow["DOCUMENT_TYPE"].ToString());
                    objXmlElement.SetAttribute("class", objRow["DOCUMENT_CLASS"].ToString());
                    objXmlElement.SetAttribute("category", objRow["DOCUMENT_CATEGORY"].ToString());

                    //Manoj - LSS Interface
                    objXmlElement.SetAttribute("docinternaltype", objRow["DOC_INTERNAL_TYPE"].ToString());
                    objXmlElement.SetAttribute("filepath", objRow["DOCUMENT_FILEPATH"].ToString());

                    objXmlElement.InnerText = objRow["NOTES"].ToString();

                    objFolderElement.AppendChild(objXmlElement);
                    objXmlElement = null;
                }
                // 12/01/07 REM Umesh
                objXml = new XmlDocument();

                string strContent = RMSessionManager.GetCustomContent(m_iClientId);//psharma206
                if (! string.IsNullOrEmpty(strContent))
                {
                    objXml.LoadXml(strContent);
                    //objRdr.Dispose();
                    RemoveButton(objXml, objXmlDocument, "//RMAdminSettings/DocumentManagement/Attachments_Email", "//data/@email_allowed");
                    RemoveButton(objXml, objXmlDocument, "//RMAdminSettings/DocumentManagement/Attachments_Delete", "//data/@att_delete_allowed");

                }
                
                // REM End
                p_sAttachmentsXML = objXmlDocument.OuterXml;

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetAttachments.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetAttachments.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                    objXml = null;
                    objSessionDSN = null;
                }
                //if (objRdr != null)
                //{
                //    objRdr.Close();
                //    objRdr = null;
                //}

                if (objDocumentTable != null)
                {
                    objDocumentTable.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objXmlElement = null;
                objFolderElement = null;
                objRootElement = null;
                objXmlDocument = null;
                if (objCon != null)//Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                {
                    objCon.Dispose();
                    objCon = null;
                } 
            }


            return iReturnValue;
        }
        public int GetAttachmentsNew(int lSortId, string p_sFormName, long p_lRecordId,long p_lPageNumber,
            out DocumentList p_objDocumentList)
        {
            int iReturnValue = 0;
            DataSet objDataSet = null;
            DataTable objDocumentTable = null;

            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objFolderElement = null;
            XmlElement objXmlElement = null;

            XmlDocument objXml = null;      // Umesh
            XmlNode objSessionDSN = null; // Umesh
            //DbReader objRdr=null;//Umesh
            string sSQL = string.Empty;
            int iSortOrder = 0;
            LocalCache objCache = null;
            SysSettings objSysSetting = null;

            long lDocRecordCount = 0;
            long lTotalRecordCount = 0;
            long lTotalPageCount = 0;
            long lPageSize = 0; //Deb : MITS 25598
            long lPageStart = 0;
            long lPageEnd = 0;
            DbConnection objCon = null;  //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            bool bConverted = false;
            try
            {
                objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);
			    lPageSize = GetRecordsPerPage();//Deb : MITS 25598
                RaiseInitializationException();
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objCon = DbFactory.GetDbConnection(this.ConnectionString);
                objCon.Open();  
                //Starts- Nitesh: 12/05/2006 Changed SQL for sorting on codes
                sSQL = "SELECT DOCUMENT.UPLOAD_STATUS,DOCUMENT.DOCUMENT_ID,DOCUMENT_NAME,"//smishra25:Adding Upload status
                    + " DOCUMENT.DOCUMENT_FILENAME,DOCUMENT.KEYWORDS, "
                    + " DOCUMENT.CREATE_DATE,DOCUMENT.NOTES,DOCUMENT.FOLDER_ID,"
                    + " DOCUMENT.USER_ID, (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID =DOCUMENT.DOCUMENT_CLASS) AS DOCUMENT_CLASS,"
                    + " (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT.DOCUMENT_CATEGORY) AS DOCUMENT_CATEGORY,"
                    + " DOCUMENT.DOCUMENT_SUBJECT, "
                    + " (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT.DOCUMENT_TYPE) AS DOCUMENT_TYPE, DOCUMENT.DOC_INTERNAL_TYPE, DOCUMENT.DOCUMENT_FILEPATH "

                    //tanwar2 - ImageRight - start
                    + " ,DOCUMENT.IR_TRANSFER_STATUS "
                    //tanwar2 - ImageRight - end

                    + " FROM DOCUMENT ,DOCUMENT_ATTACH ";
                //End- Nitesh: 12/05/2006 Changed SQL for sorting on codes
                if (this.TableName.Length == 0)
                {
                    //Starts: Code Commented By Nitesh: 12/05/2006 
                    // Get all attachments
                    //					sSQL = "SELECT DOCUMENT.DOCUMENT_ID,DOCUMENT.DOCUMENT_NAME,"
                    //						+ " DOCUMENT.DOCUMENT_FILENAME,DOCUMENT.KEYWORDS,"
                    //						+ " DOCUMENT.CREATE_DATE, DOCUMENT.NOTES, DOCUMENT.FOLDER_ID,"
                    //						+ " DOCUMENT.USER_ID, DOCUMENT.DOCUMENT_CLASS,"
                    //						+ " DOCUMENT.DOCUMENT_CATEGORY, DOCUMENT.DOCUMENT_SUBJECT,"
                    //						+ " DOCUMENT.DOCUMENT_TYPE"
                    //						+ " FROM DOCUMENT, DOCUMENT_ATTACH "
                    //Ends Commented By Nitesh: 12/05/2006 
                    sSQL = sSQL + " WHERE DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID";
                    //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                    if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                        sSQL = sSQL + " AND NVL(DOCUMENT.ISAT_PV,2) <> -1 ";
                    else
                        sSQL = sSQL + " AND ISNULL(DOCUMENT.ISAT_PV,2) <> -1 ";

                    //Animesh Insertion Ends
                }
                else if (p_lRecordId == 0)
                {
                    //Starts: Code Commented By Nitesh: 12/05/2006 
                    // Get all attachments for a particular table
                    //					sSQL = "SELECT DOCUMENT.DOCUMENT_ID,DOCUMENT.DOCUMENT_NAME,"
                    //						+ " DOCUMENT.DOCUMENT_FILENAME,DOCUMENT.KEYWORDS,"
                    //						+ " DOCUMENT.CREATE_DATE, DOCUMENT.NOTES, DOCUMENT.FOLDER_ID,"
                    //						+ " DOCUMENT.USER_ID, DOCUMENT.DOCUMENT_CLASS,"
                    //						+ " DOCUMENT.DOCUMENT_CATEGORY, DOCUMENT.DOCUMENT_SUBJECT,"
                    //						+ " DOCUMENT.DOCUMENT_TYPE"
                    //						+ " FROM DOCUMENT, DOCUMENT_ATTACH "
                    //Ends Commented By Nitesh: 12/05/2006 
                    sSQL = sSQL + " WHERE UPPER(DOCUMENT_ATTACH.TABLE_NAME)=UPPER('" + this.TableName + "')"
                    + " AND DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID";
                    //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                    if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                        sSQL = sSQL + " AND NVL(DOCUMENT.ISAT_PV,2) <> -1 ";
                    else
                        sSQL = sSQL + " AND ISNULL(DOCUMENT.ISAT_PV,2) <> -1 ";

                    //Animesh Insertion Ends
                }
                else
                {
                    //Starts: Code Commented By Nitesh: 12/05/2006 
                    // Get all attachments for a particular table and a particular record
                    //					sSQL = "SELECT DOCUMENT.DOCUMENT_ID,DOCUMENT.DOCUMENT_NAME,"
                    //						+ " DOCUMENT.DOCUMENT_FILENAME,DOCUMENT.KEYWORDS,"
                    //						+ " DOCUMENT.CREATE_DATE, DOCUMENT.NOTES, DOCUMENT.FOLDER_ID,"
                    //						+ " DOCUMENT.USER_ID, DOCUMENT.DOCUMENT_CLASS,"
                    //						+ " DOCUMENT.DOCUMENT_CATEGORY, DOCUMENT.DOCUMENT_SUBJECT,"
                    //						+ " DOCUMENT.DOCUMENT_TYPE"
                    //						+ " FROM DOCUMENT, DOCUMENT_ATTACH "
                    //Ends Commented By Nitesh: 12/05/2006 
                    sSQL = sSQL + " WHERE UPPER(DOCUMENT_ATTACH.TABLE_NAME)=UPPER('" + this.TableName + "')"
                    + " AND DOCUMENT_ATTACH.RECORD_ID=" + p_lRecordId.ToString()
                    + " AND DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID";
                    //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                    if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                        sSQL = sSQL + " AND NVL(DOCUMENT.ISAT_PV,2) <> -1 ";
                    else
                        sSQL = sSQL + " AND ISNULL(DOCUMENT.ISAT_PV,2) <> -1 ";

                    //Animesh Insertion Ends
                }

                // *BEGIN* JP 12-1-2006  Added sorting code for attachments case
                switch (lSortId)
                {
                    case (int)DOCSORTORDER.ASC_NAME:
                        if (objSysSetting.OracleCaseIns == 1 || objSysSetting.OracleCaseIns == -1)
                        {
                            sSQL = sSQL + " ORDER BY UPPER(DOCUMENT_NAME) ASC, UPPER(DOCUMENT_FILENAME) ASC"; //zmohammad MITS 37157,JIRA 4804                        
                        }
                        else
                        {
                            sSQL = sSQL + " ORDER BY DOCUMENT_NAME ASC, DOCUMENT_FILENAME ASC"; //aaggarwal29 : MITS 37558,JIRA 6367 on UI, we are using combination of both the values, hence adding it to order by clause
                        }
                        iSortOrder = (int)DOCSORTORDER.ASC_NAME;
                        break;
                    case (int)DOCSORTORDER.ASC_SUBJECT:
                        if (objSysSetting.OracleCaseIns == 1 || objSysSetting.OracleCaseIns == -1)
                        {
                            sSQL = sSQL + " ORDER BY UPPER(DOCUMENT_SUBJECT) ASC"; //zmohammad MITS 37157,JIRA 4804 
                        }
                        else
                        {
                            sSQL = sSQL + " ORDER BY DOCUMENT_SUBJECT ASC";
                        }
                        iSortOrder = (int)DOCSORTORDER.ASC_SUBJECT;
                        break;
                    case (int)DOCSORTORDER.ASC_TYPE:
                        sSQL = sSQL + " ORDER BY DOCUMENT_TYPE ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_TYPE;
                        break;
                    case (int)DOCSORTORDER.ASC_CLASS:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CLASS ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CLASS;
                        break;
                    case (int)DOCSORTORDER.ASC_CATEGORY:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CATEGORY ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CATEGORY;
                        break;
                    case (int)DOCSORTORDER.ASC_CREATE_DATE:
                        sSQL = sSQL + " ORDER BY CREATE_DATE ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CREATE_DATE;
                        break;
                    case (int)DOCSORTORDER.DEC_NAME:
                        if (objSysSetting.OracleCaseIns == 1 || objSysSetting.OracleCaseIns == -1)
                        {
                            sSQL = sSQL + " ORDER BY UPPER(DOCUMENT_NAME) DESC, UPPER(DOCUMENT_FILENAME) DESC"; //zmohammad MITS 37157,JIRA 4804                        
                        }
                        else
                        {
                            sSQL = sSQL + " ORDER BY DOCUMENT_NAME DESC, DOCUMENT_FILENAME DESC"; //aaggarwal29 : MITS 37558,JIRA 6367 on UI, we are using combination of both the values, hence adding it to order by clause
                        }
                        iSortOrder = (int)DOCSORTORDER.DEC_NAME;
                        break;
                    case (int)DOCSORTORDER.DEC_SUBJECT:
                        if (objSysSetting.OracleCaseIns == 1 || objSysSetting.OracleCaseIns == -1)
                        {
                         //   sSQL = sSQL + " ORDER BY UPPER(DOCUMENT_SUBJECT) ASC"; //zmohammad MITS 37157,JIRA 4804 
                            sSQL = sSQL + " ORDER BY UPPER(DOCUMENT_SUBJECT) DESC";   //vgupta79 for RMA-9300
                        }
                        else
                        {
                            sSQL = sSQL + " ORDER BY DOCUMENT_SUBJECT DESC";
                        }
                        iSortOrder = (int)DOCSORTORDER.DEC_SUBJECT;
                        break;
                    case (int)DOCSORTORDER.DEC_TYPE:
                        sSQL = sSQL + " ORDER BY DOCUMENT_TYPE DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_TYPE;
                        break;
                    case (int)DOCSORTORDER.DEC_CLASS:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CLASS DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CLASS;
                        break;
                    case (int)DOCSORTORDER.DEC_CATEGORY:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CATEGORY DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CATEGORY;
                        break;
                    case (int)DOCSORTORDER.DEC_CREATE_DATE:
                        sSQL = sSQL + " ORDER BY CREATE_DATE DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CREATE_DATE;
                        break;
                    default:
                        sSQL = sSQL + " ORDER BY DOCUMENT_NAME ASC, DOCUMENT_FILENAME ASC"; //aaggarwal29 : MITS 37558,JIRA 6367 on UI, we are using combination of both the values, hence adding it to order by clause
                        lSortId = (int)DOCSORTORDER.ASC_NAME;
                        iSortOrder = (int)DOCSORTORDER.ASC_NAME;
                        break;
                }
                objCon.Close();  //Mona:PaperVisionMerge : Animesh
                // *END* JP 12-1-2006  Added sorting code for attachments case
                p_objDocumentList = new DocumentList();
                
                // akaushik5 Added for MITS 30748 Starts
                Random random = new Random();
                for (int i = 0; i <= 10; i++)
                { 
                    if(Document.IsUpdatingDocument)
                    {
                        Thread.Sleep(random.Next(100, (i + 1) * 100));
                    }
                    else
                    {
                        break;
                    }                
                }

                IsReadingDocument = true;
                // akaushik5 Added for MITS 30748 Ends

                objDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL,m_iClientId);

                // akaushik5 Added for MITS 30748 Starts
                IsReadingDocument = false;
                // akaushik5 Added for MITS 30748 Ends

                objDocumentTable = objDataSet.Tables[0];

                //getting the Document record count
                if (objDataSet.Tables.Count > 0)
                {
                    lDocRecordCount = objDocumentTable.Rows.Count;
                }

                //getting the total record count
                lTotalRecordCount = lDocRecordCount;

                //getting the page count to be added to the attribute of the data element
                lTotalPageCount = lTotalRecordCount / lPageSize;
                if (lTotalRecordCount > lTotalPageCount * lPageSize)
                {
                    lTotalPageCount = lTotalPageCount + 1;
                }

                if (lTotalPageCount == 0)
                {
                    lTotalPageCount = 1;
                }

                //objXmlDocument = new XmlDocument();
                //objRootElement = objXmlDocument.CreateElement("data");
                //objRootElement.SetAttribute("name", "Attachments");
                //objRootElement.SetAttribute("pid", p_lRecordId.ToString());
                //objRootElement.SetAttribute("sid", this.SecurityId.ToString());
                //objRootElement.SetAttribute("table", this.TableName);
                //objRootElement.SetAttribute("form", p_sFormName);
                //objRootElement.SetAttribute("orderby", iSortOrder.ToString());  // JP 12-1-2006: Support for sorting added to attachments
                p_objDocumentList = new DocumentList();
                p_objDocumentList.Name = "Attachments";
                p_objDocumentList.Pid = p_lRecordId.ToString();
                p_objDocumentList.Sid = this.SecurityId.ToString();
                p_objDocumentList.Documents = new List<DocumentType>();
                //check for permission, get psid from the request if it is available
                m_sFormName = p_sFormName;
               // CheckDocListPermissions(ref objRootElement, Convert.ToInt32(m_lSecurityId));
                CheckDocumentListPermissions(ref p_objDocumentList, Convert.ToInt32(m_lSecurityId));
                //objXmlDocument.AppendChild(objRootElement);

                //objFolderElement = objXmlDocument.CreateElement("folder");
                //objFolderElement.SetAttribute("pid", "0");

                //objRootElement.AppendChild(objFolderElement);

                if (p_lPageNumber > 1)
                {
                    p_objDocumentList.Previouspage = (p_lPageNumber - 1).ToString();
                    p_objDocumentList.Firstpage = "1";
                }
                if (p_lPageNumber < lTotalPageCount)
                {
                    if (p_lPageNumber == 0) { p_lPageNumber = 1; }
                    p_objDocumentList.Nextpage = ((int)p_lPageNumber + 1).ToString();
                    p_objDocumentList.Lastpage = lTotalPageCount.ToString();
                }
                if (p_lPageNumber > lTotalPageCount)
                {
                    p_objDocumentList.Pagenumber = lTotalPageCount.ToString();
                    p_lPageNumber = lTotalPageCount;
                }
                else
                {
                    p_objDocumentList.Pagenumber = p_lPageNumber.ToString();
                }
                p_objDocumentList.Pagecount = lTotalPageCount.ToString();
                p_objDocumentList.Orderby = iSortOrder.ToString();

                //objDocumentList.Folders.Add(objFolder);
                //getting the loop starting point and end point
                if (p_lPageNumber == 1)
                    lPageStart = 1;
                else
                    lPageStart = ((p_lPageNumber - 1) * lPageSize) + 1;

                lPageEnd = lPageStart + lPageSize - 1;
                if (lPageEnd > lTotalRecordCount)
                    lPageEnd = lTotalRecordCount;

                long lIndex = lPageStart;
                int iDocIndex = 0;
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                ////Document Rows

                p_objDocumentList.Documents = new System.Collections.Generic.List<DocumentType>();
                while (lIndex <= lPageEnd)
                {
                    iDocIndex = (int)(lIndex - 1);
                    DocumentType objDocument = new DocumentType();
                    objDocument.Title = objDocumentTable.Rows[iDocIndex]["DOCUMENT_NAME"].ToString();
                    objDocument.FolderId = objDocumentTable.Rows[iDocIndex]["FOLDER_ID"].ToString();
                    objDocument.Keywords = objDocumentTable.Rows[iDocIndex]["KEYWORDS"].ToString();
                    objDocument.CreateDate = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDocumentTable.Rows[iDocIndex]["CREATE_DATE"].ToString()), "d");
                    objDocument.FileName = objDocumentTable.Rows[iDocIndex]["DOCUMENT_FILENAME"].ToString();
                    objDocument.UserName = objDocumentTable.Rows[iDocIndex]["USER_ID"].ToString();
                    objDocument.Subject = objDocumentTable.Rows[iDocIndex]["DOCUMENT_SUBJECT"].ToString();
                    objDocument.DocumentsType = objDocumentTable.Rows[iDocIndex]["DOCUMENT_TYPE"].ToString();
                    objDocument.DocumentClass = objDocumentTable.Rows[iDocIndex]["DOCUMENT_CLASS"].ToString();
                    objDocument.DocumentCategory = objDocumentTable.Rows[iDocIndex]["DOCUMENT_CATEGORY"].ToString();
                    objDocument.Notes = objDocumentTable.Rows[iDocIndex]["NOTES"].ToString();
                    objDocument.Pid = objDocumentTable.Rows[iDocIndex]["DOCUMENT_ID"].ToString();
                    objDocument.UserId = objDocumentTable.Rows[iDocIndex]["USER_ID"].ToString();
                    //Manoj - LSS Interface
                    objDocument.DocInternalType = objDocumentTable.Rows[iDocIndex]["DOC_INTERNAL_TYPE"].ToString();
                    objDocument.FilePath = objDocumentTable.Rows[iDocIndex]["DOCUMENT_FILEPATH"].ToString();
                    //smishra25:Adding filesize to the DocumentType object
                    //mkaran2 : Adding UploadStatus : MITS 33747
                   // objDocument.Upload_Status = Convert.ToInt32(objDocumentTable.Rows[iDocIndex]["UPLOAD_STATUS"]);
                    objDocument.Upload_Status = Conversion.CastToType<Int32>(objDocumentTable.Rows[iDocIndex]["UPLOAD_STATUS"].ToString(), out bConverted);                  
                    

                    if (string.Compare(objDocumentTable.Rows[iDocIndex]["UPLOAD_STATUS"].ToString(), "1") == 0)
                    {
                        objDocument.FileSizeInMB = -1;
                    }
                    else
                    {
                        SetDocumentFileSize(ref objDocument);//smishra25:Adding filesize
                    }

                    //tanwar2 - ImageRight - start
                    if (string.Compare(objDocumentTable.Rows[iDocIndex]["IR_TRANSFER_STATUS"].ToString(), "-1") == 0)
                    {
                        objDocument.bIRUploadStatus = true;
                    }
                    else
                    {
                        objDocument.bIRUploadStatus = false;
                    }
                    //tanwar2 - ImageRight - end

                    p_objDocumentList.Documents.Add(objDocument);
                    lIndex = lIndex + 1;
                }

                //Added for REM-Nadim(5/4/2009) 
                string sEmail_allowed = string.Empty;
                string sDelete_allowed = string.Empty;
                              
                objXml = new XmlDocument();
                string strContent = RMSessionManager.GetCustomContent(m_iClientId);
                if (!string.IsNullOrEmpty(strContent))
                {
                    objXml.LoadXml(strContent);
                    //objRdr.Dispose();
                    //RemoveButton(objXml, objXmlDocument, "//RMAdminSettings/DocumentManagement/Attachments_Email", "//data/@email_allowed");
                    //RemoveButton(objXml, objXmlDocument, "//RMAdminSettings/DocumentManagement/Attachments_Delete", "//data/@att_delete_allowed");
                    sEmail_allowed = objXml.SelectSingleNode("//RMAdminSettings/DocumentManagement/Attachments_Email").InnerText;
                    sDelete_allowed = objXml.SelectSingleNode("//RMAdminSettings/DocumentManagement/Attachments_Delete").InnerText;
                    if (sEmail_allowed == "0")
                    { p_objDocumentList.Email_allowed = "0"; }
                    else
                    { p_objDocumentList.Email_allowed = "1"; }

                    if (sDelete_allowed == "0")
                    { p_objDocumentList.Att_delete_allowed = "0"; }
                    else
                    { p_objDocumentList.Att_delete_allowed = "1"; }
                }//if
                
                //p_sAttachmentsXML = objXmlDocument.OuterXml;
                //Added for REM-Nadim(5/4/2009) 
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetAttachments.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetAttachments.Exception", m_iClientId), p_objException);
            }
            finally
            {
                // akaushik5 Added for MITS 30748 Starts
                IsReadingDocument = false;
                // akaushik5 Added for MITS 30748 Ends

                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                    objXml = null;
                    objSessionDSN = null;
                }
                //if (objRdr != null)
                //{
                //    objRdr.Close();
                //    objRdr = null;
                //}

                if (objDocumentTable != null)
                {
                    objDocumentTable.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objXmlElement = null;
                objFolderElement = null;
                objRootElement = null;
                objXmlDocument = null;
                if (objCon != null)//Mona:PaperVisionMerge : Animesh
                {
                    objCon.Dispose();
                    objCon = null;
                }
            }


            return iReturnValue;
        }

        /// Name			: GetFolderDetails
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************		
        /// <summary>
        /// Gets details for a folder.
        /// Uses ConnectionString and UserLoginName property values.
        /// </summary>
        /// <param name="p_lFolderId">
        ///		Id of the folder whose details are required.
        ///	</param>
        /// <param name="p_sFolderDetailsXML">
        ///		Out parameter.Returns folder details in form of XML.
        ///	</param>
        /// <returns>0 - Success</returns>
        public int GetFolderDetails(long p_lFolderId, out string p_sFolderDetailsXML)
        {
            int iReturnValue = 0;

            string sSQL = string.Empty;
            DbReader objDbReader = null;

            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objFolderElement = null;

            try
            {
                RaiseInitializationException();

                objXmlDocument = new XmlDocument();
                objRootElement = objXmlDocument.CreateElement("data");
                objRootElement.SetAttribute("name", "FolderDetails");

                objXmlDocument.AppendChild(objRootElement);

                sSQL = "SELECT FOLDER_NAME, FOLDER_ID, PARENT_ID, FOLDER_PATH, "
                    + " USER_ID"
                    + " FROM DOC_FOLDERS "
                    + " WHERE FOLDER_ID=" + p_lFolderId
                    + " AND USER_ID='" + this.UserLoginName + "'";

                objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                if (objDbReader.Read())
                {
                    objFolderElement = objXmlDocument.CreateElement("folder");
                    objFolderElement.InnerText = objDbReader.GetString("FOLDER_NAME");
                    objFolderElement.SetAttribute("folderid", objDbReader.GetValue("FOLDER_ID").ToString());
                    objFolderElement.SetAttribute("pid", objDbReader.GetValue("PARENT_ID").ToString());
                    objFolderElement.SetAttribute("folderpath", objDbReader.GetString("FOLDER_PATH"));
                    objFolderElement.SetAttribute("userid", objDbReader.GetString("USER_ID"));
                    objRootElement.AppendChild(objFolderElement);
                }

                p_sFolderDetailsXML = objXmlDocument.OuterXml;

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetFolderDetails.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetFolderDetails.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader = null;
                }

                objFolderElement = null;
                objRootElement = null;
                objXmlDocument = null;
            }

            return iReturnValue;
        }

        /// Name			: GetFoldersAsOption
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************		
        /// <summary>
        /// Returns the folder details in form of XML including the 
        /// folder hierarchy.
        /// Uses UserLoginName and ConnectionString property values.
        /// </summary>
        /// <param name="p_lSelectFolder">
        ///		A specific attribute 'def' is set in the xml 
        ///		tag for this selected folder
        /// </param>
        /// <param name="p_sFoldersXML">
        ///		Out parameter. Returns the folder details as XML.
        ///	</param>
        /// <returns>0 - Success</returns>
        public int GetFoldersAsOption(long p_lSelectFolder, out string p_sFoldersXML)
        {
            int iReturnValue = 0;

            DataSet objDataSet = null;
            DataTable objDocFoldersTable = null;

            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objXmlElement = null;

            /* A key based collection is simulated using 2 ArrayLists
             * arrParentFolderNodes stores the Xml nodes for each folder
             * and arrParentFolderKeys stores the key corresponding to 
             * to each xml node at the same index 
             * */
            ArrayList arrParentFolderNodes = new ArrayList();
            ArrayList arrParentFolderKeys = new ArrayList();

            long lParentId = 0;
            long lFolderId = 0;
            string sNewElementName = string.Empty;
            int iParentNodeIndex = 0;
            XmlElement objParentNode = null;
            string sParentNodeName = string.Empty;
            int iNodeLevel = 0;

            string sSQL = string.Empty;

            try
            {
                RaiseInitializationException();

                /* Assumption - Parent folder id is always less than
                 * the children folder ids.
                 * */
                sSQL = "SELECT FOLDER_ID,PARENT_ID,FOLDER_NAME"
                    + " FROM DOC_FOLDERS "
                    + " WHERE USER_ID='" + this.UserLoginName + "'"
                    + " ORDER BY PARENT_ID,FOLDER_NAME";

                objDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);
                objDocFoldersTable = objDataSet.Tables[0];

                objXmlDocument = new XmlDocument();
                objRootElement = objXmlDocument.CreateElement("data");

                objXmlDocument.AppendChild(objRootElement);

                foreach (DataRow objRow in objDocFoldersTable.Rows)
                {
                    lParentId = Conversion.ConvertStrToLong(objRow["PARENT_ID"].ToString());
                    lFolderId = Conversion.ConvertStrToLong(objRow["FOLDER_ID"].ToString());

                    objParentNode = null;
                    objXmlElement = null;

                    if (lParentId == 0)
                    {
                        objParentNode = objRootElement;
                        sNewElementName = "dir1";
                    }
                    else
                    {
                        /* The Xml to be generated in the following format
                         * 		<data>
                         *			<dir1 parent="0" id="-1">Recycle Bin</dir1> 
                         *			<dir1 parent="0" id="19">
                         *				Test1 
                         *				<dir2 parent="19" id="20">Test2</dir2> 
                         *				<dir2 parent="19" id="21">Test22</dir2> 
                         *			</dir1>
                         * 		</data>
                         * The xml tag name for each folder is driven by its depth
                         * Level 0 is assumed to be the root folder e.g All child folders
                         * of root e.g. Test1 at level 1 are represented by tag <dir1> and
                         * all child folders of Test1 are represented by <dir2>				 * 
                         * */
                        sNewElementName = string.Empty;

                        // Retrieve index of the parent folderid from the simulated collection
                        iParentNodeIndex = arrParentFolderKeys.IndexOf(lParentId.ToString() + "Key");
                        // Retrieve parent node for the current folder
                        objParentNode = (XmlElement)arrParentFolderNodes[iParentNodeIndex];
                        sParentNodeName = objParentNode.Name;
                        // Determine the node level for the current folder
                        iNodeLevel = Conversion.ConvertStrToInteger(sParentNodeName.Substring(sParentNodeName.Length - 1)) + 1;
                        // Determine the tag name for representing the new folder
                        sNewElementName = sParentNodeName.Substring(0, sParentNodeName.Length - 1) + iNodeLevel.ToString();
                    }
                    objXmlElement = objXmlDocument.CreateElement(sNewElementName);

                    if (p_lSelectFolder == lFolderId)
                    {
                        objXmlElement.SetAttribute("def", "1");
                    }

                    objXmlElement.SetAttribute("parent", lParentId.ToString());
                    objXmlElement.SetAttribute("id", lFolderId.ToString());
                    objXmlElement.InnerText = objRow["FOLDER_NAME"].ToString();

                    // Append the xml node for the current folder under its parent node
                    objParentNode.AppendChild(objXmlElement);

                    // Insert key and the current node into the collection
                    arrParentFolderKeys.Add(lFolderId.ToString() + "Key");
                    arrParentFolderNodes.Add(objXmlElement);
                }
                List<Folder> objlist = new List<Folder>();
                Folder obj = new Folder();
                obj.FolderName = "My Documents";
                obj.FolderID = "0";
                objlist.Add(obj);
                string foldername = "";
                foreach (XmlNode node in objXmlDocument.SelectNodes("//data/dir1"))
                {
                    foldername = "";
                    obj = new Folder();
                    obj.FolderName = node.SelectSingleNode("text()").Value;
                    obj.FolderID = node.Attributes["id"].Value;
                    objlist.Add(obj);
                    foldername = obj.FolderName;
                    foreach (XmlNode nodechid in node.SelectNodes("dir2"))
                    {
                        obj = new Folder();
                        obj.FolderName = " " + nodechid.SelectSingleNode("text()").Value;
                        obj.FolderID = nodechid.Attributes["id"].Value;
                        objlist.Add(obj);

                    }
                }
                p_sFoldersXML = objXmlDocument.OuterXml;

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetFoldersAsOption.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetFolderAsOption.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }

                if (objDocFoldersTable != null)
                {
                    objDocFoldersTable.Dispose();
                }

                objParentNode = null;
                objXmlElement = null;
                objRootElement = null;
                objXmlDocument = null;
            }
            return iReturnValue;
        }
        /// Name			: GetFoldersAsOption
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************		
        /// <summary>
        /// Returns the folder details in form of XML including the 
        /// folder hierarchy.
        /// Uses UserLoginName and ConnectionString property values.
        /// </summary>
        /// <param name="p_lSelectFolder">
        ///		A specific attribute 'def' is set in the xml 
        ///		tag for this selected folder
        /// </param>
        /// <param name="p_sFoldersXML">
        ///		Out parameter. Returns the folder details as XML.
        ///	</param>
        /// <returns>0 - Success</returns>
        public int GetFoldersAsOptionNew(long p_lSelectFolder, ref List<Folder> objFolders)
        {
            int iReturnValue = 0;

            DataSet objDataSet = null;
            DataTable objDocFoldersTable = null;

            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objXmlElement = null;

            /* A key based collection is simulated using 2 ArrayLists
             * arrParentFolderNodes stores the Xml nodes for each folder
             * and arrParentFolderKeys stores the key corresponding to 
             * to each xml node at the same index 
             * */
            ArrayList arrParentFolderNodes = new ArrayList();
            ArrayList arrParentFolderKeys = new ArrayList();

            long lParentId = 0;
            long lFolderId = 0;
            string sNewElementName = string.Empty;
            int iParentNodeIndex = 0;
            XmlElement objParentNode = null;
            string sParentNodeName = string.Empty;
            int iNodeLevel = 0;

            string sSQL = string.Empty;

            try
            {
                RaiseInitializationException();

                /* Assumption - Parent folder id is always less than
                 * the children folder ids.
                 * */
                sSQL = "SELECT FOLDER_ID,PARENT_ID,FOLDER_NAME"
                    + " FROM DOC_FOLDERS "
                    + " WHERE USER_ID='" + this.UserLoginName + "'"
                    + " ORDER BY PARENT_ID,FOLDER_NAME";

                objDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);
                objDocFoldersTable = objDataSet.Tables[0];

                objXmlDocument = new XmlDocument();
                objRootElement = objXmlDocument.CreateElement("data");

                objXmlDocument.AppendChild(objRootElement);

                foreach (DataRow objRow in objDocFoldersTable.Rows)
                {
                    lParentId = Conversion.ConvertStrToLong(objRow["PARENT_ID"].ToString());
                    lFolderId = Conversion.ConvertStrToLong(objRow["FOLDER_ID"].ToString());

                    objParentNode = null;
                    objXmlElement = null;

                    if (lParentId == 0)
                    {
                        objParentNode = objRootElement;
                        sNewElementName = "dir1";
                    }
                    else
                    {
                        /* The Xml to be generated in the following format
                         * 		<data>
                         *			<dir1 parent="0" id="-1">Recycle Bin</dir1> 
                         *			<dir1 parent="0" id="19">
                         *				Test1 
                         *				<dir2 parent="19" id="20">Test2</dir2> 
                         *				<dir2 parent="19" id="21">Test22</dir2> 
                         *			</dir1>
                         * 		</data>
                         * The xml tag name for each folder is driven by its depth
                         * Level 0 is assumed to be the root folder e.g All child folders
                         * of root e.g. Test1 at level 1 are represented by tag <dir1> and
                         * all child folders of Test1 are represented by <dir2>				 * 
                         * */
                        sNewElementName = string.Empty;

                        // Retrieve index of the parent folderid from the simulated collection
                        iParentNodeIndex = arrParentFolderKeys.IndexOf(lParentId.ToString() + "Key");
                        // Retrieve parent node for the current folder
                        objParentNode = (XmlElement)arrParentFolderNodes[iParentNodeIndex];
                        sParentNodeName = objParentNode.Name;
                        // Determine the node level for the current folder
                        iNodeLevel = Conversion.ConvertStrToInteger(sParentNodeName.Substring(sParentNodeName.Length - 1)) + 1;
                        // Determine the tag name for representing the new folder
                        sNewElementName = sParentNodeName.Substring(0, sParentNodeName.Length - 1) + iNodeLevel.ToString();
                    }
                    objXmlElement = objXmlDocument.CreateElement(sNewElementName);

                    if (p_lSelectFolder == lFolderId)
                    {
                        objXmlElement.SetAttribute("def", "1");
                    }

                    objXmlElement.SetAttribute("parent", lParentId.ToString());
                    objXmlElement.SetAttribute("id", lFolderId.ToString());
                    objXmlElement.InnerText = objRow["FOLDER_NAME"].ToString();

                    // Append the xml node for the current folder under its parent node
                    objParentNode.AppendChild(objXmlElement);

                    // Insert key and the current node into the collection
                    arrParentFolderKeys.Add(lFolderId.ToString() + "Key");
                    arrParentFolderNodes.Add(objXmlElement);
                }
                objFolders = new List<Folder>();
                Folder obj = new Folder();
                obj.FolderName = "My Documents";
                obj.FolderID = "0";
                objFolders.Add(obj);
                string foldername = "";
                foreach (XmlNode node in objXmlDocument.SelectNodes("//data/dir1"))
                {
                    foldername = "";
                    obj = new Folder();
                    obj.FolderName = node.SelectSingleNode("text()").Value;
                    obj.FolderID = node.Attributes["id"].Value;
                    objFolders.Add(obj);
                    foldername = obj.FolderName;
                    foreach (XmlNode nodechid in node.SelectNodes("dir2"))
                    {
                        obj = new Folder();
                        obj.FolderName = "&nbsp;&nbsp;" + nodechid.SelectSingleNode("text()").Value;
                        obj.FolderID = nodechid.Attributes["id"].Value;
                        objFolders.Add(obj);

                    }
                }


            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetFoldersAsOption.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetFolderAsOption.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }

                if (objDocFoldersTable != null)
                {
                    objDocFoldersTable.Dispose();
                }

                objParentNode = null;
                objXmlElement = null;
                objRootElement = null;
                objXmlDocument = null;
            }
            return iReturnValue;
        }

        /// Name			: GetDocuments
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///	25/05/2005		*	Implemented paging	* Mohit Yadav
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Returns information about documents and folders present
        /// in the root folder.
        /// Uses UserLoginName and ConnectionString property values. 
        /// </summary>
        /// <param name="p_sDocumentsXML">
        ///		Out parameter. Returns information about documents in Xml
        ///		format
        ///	</param>
        ///	<param name="p_lPageNumber">
        ///		Input parameter for getting the pagenumber
        ///	</param>		
        /// <returns>0 - Success</returns>
        public int GetDocuments(int lSortId, long p_lPageNumber, out string p_sDocumentsXML)
        {
            //*********************************************
            //Changes made by Mohit Yadav to include paging
            //*********************************************
            //p_objDocumentList = new DocumentList();
            return GetDocuments(lSortId, 0L, 0L, "My Documents", "My Documents", p_lPageNumber, out p_sDocumentsXML);
            //*********************************************
        }
        public int GetDocumentsNew(int lSortId, long p_lPageNumber, out DocumentList p_objDocumentList)
        {
            //*********************************************
            //Changes made by Mohit Yadav to include paging
            //*********************************************
            p_objDocumentList = new DocumentList();
            return GetDocumentsNew(lSortId, 0L, 0L, "My Documents", "My Documents", p_lPageNumber, out p_objDocumentList);
            //*********************************************
        }


        /// Name			: GetDocuments
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///	25/05/2005		*	Implemented paging	* Mohit Yadav
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Returns information about documents and folders present
        /// in a particular folder, in Xml format. 
        /// Uses UserLoginName and ConnectionString properties. 
        /// </summary>
        /// <param name="p_lFolderId">
        ///		Id of the folder whose information about documents
        ///		and child folders is required.
        ///	</param>
        /// <param name="p_sDocumentsXML">
        ///		Out parameter. 
        ///		Returns information about documents in Xml format.
        ///	</param>
        ///	<param name="p_lPageNumber">
        ///		Input parameter for getting the pagenumber
        ///	</param>		
        /// <returns>0 - Success</returns>
        public int GetDocumentsNew(int lSortId, long p_lFolderId, long p_lPageNumber, out DocumentList p_objDocumentList)
        {
            DbReader objDbReader = null;

            long lParentFolderId = 0;
            string sFolderName = string.Empty;
            string sParentFolderName = string.Empty;

            string sSQL = string.Empty;

            if (p_lFolderId == 0)
            {
                //*********************************************
                //Changes made by Mohit Yadav to include paging
                //*********************************************
                return GetDocumentsNew(lSortId, 0L, 0L, "My Documents", "My Documents", p_lPageNumber, out p_objDocumentList);
                //*********************************************
            }

            try
            {
                RaiseInitializationException();

                //Check if the user has USERDOC_VIEW permission
                //if(!(m_userLogin.IsAllowedEx(USERDOC_SID) && m_userLogin.IsAllowed(USERDOC_VIEW_SID)))
                //{
                //	throw new PermissionViolationException(RMPermissions.RMO_VIEW,USERDOC_VIEW_SID);
                //}

                sSQL = "SELECT FOLDER_NAME "
                    + " FROM DOC_FOLDERS "
                    + " WHERE FOLDER_ID=" + p_lFolderId.ToString()
                    + " AND USER_ID='" + this.UserLoginName + "'";

                objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                if (!objDbReader.Read())
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.GetDocuments.InvalidFolderId", m_iClientId));
                }

                objDbReader.Close();
                objDbReader = null;

                sSQL = "SELECT PARENT_ID, FOLDER_NAME "
                    + " FROM DOC_FOLDERS "
                    + " WHERE FOLDER_ID=" + p_lFolderId.ToString()
                    + " AND USER_ID='" + this.UserLoginName + "'";

                objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                if (objDbReader.Read())
                {
                    lParentFolderId = Convert.ToInt64(objDbReader.GetValue("PARENT_ID"));
                    sFolderName = objDbReader.GetString("FOLDER_NAME");
                }

                objDbReader.Close();
                objDbReader = null;

                if (lParentFolderId != 0)
                {
                    sSQL = "SELECT FOLDER_NAME "
                        + " FROM DOC_FOLDERS "
                        + " WHERE FOLDER_ID=" + lParentFolderId.ToString()
                        + " AND USER_ID='" + this.UserLoginName + "'";

                    objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                    if (objDbReader.Read())
                    {
                        sParentFolderName = objDbReader.GetString("FOLDER_NAME");
                    }
                }
                else
                {
                    sParentFolderName = "My Documents";
                }

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetDocuments.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader = null;
                }
            }
            //*********************************************
            //Changes made by Mohit Yadav to include paging
            //*********************************************
            return GetDocumentsNew(lSortId, p_lFolderId, lParentFolderId, sFolderName, sParentFolderName, p_lPageNumber,out p_objDocumentList);
            //*********************************************
        }
        public int GetDocuments(int lSortId, long p_lFolderId, long p_lPageNumber, out string p_sDocumentsXML)
        {
            DbReader objDbReader = null;

            long lParentFolderId = 0;
            string sFolderName = string.Empty;
            string sParentFolderName = string.Empty;

            string sSQL = string.Empty;

            if (p_lFolderId == 0)
            {
                //*********************************************
                //Changes made by Mohit Yadav to include paging
                //*********************************************
                return GetDocuments(lSortId, 0L, 0L, "My Documents", "My Documents", p_lPageNumber, out p_sDocumentsXML);
                //*********************************************
            }

            try
            {
                RaiseInitializationException();

                //Check if the user has USERDOC_VIEW permission
                //if(!(m_userLogin.IsAllowedEx(USERDOC_SID) && m_userLogin.IsAllowed(USERDOC_VIEW_SID)))
                //{
                //	throw new PermissionViolationException(RMPermissions.RMO_VIEW,USERDOC_VIEW_SID);
                //}

                sSQL = "SELECT FOLDER_NAME "
                    + " FROM DOC_FOLDERS "
                    + " WHERE FOLDER_ID=" + p_lFolderId.ToString()
                    + " AND USER_ID='" + this.UserLoginName + "'";

                objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                if (!objDbReader.Read())
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.GetDocuments.InvalidFolderId", m_iClientId));
                }

                objDbReader.Close();
                objDbReader = null;

                sSQL = "SELECT PARENT_ID, FOLDER_NAME "
                    + " FROM DOC_FOLDERS "
                    + " WHERE FOLDER_ID=" + p_lFolderId.ToString()
                    + " AND USER_ID='" + this.UserLoginName + "'";

                objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                if (objDbReader.Read())
                {
                    lParentFolderId = Convert.ToInt64(objDbReader.GetValue("PARENT_ID"));
                    sFolderName = objDbReader.GetString("FOLDER_NAME");
                }

                objDbReader.Close();
                objDbReader = null;

                if (lParentFolderId != 0)
                {
                    sSQL = "SELECT FOLDER_NAME "
                        + " FROM DOC_FOLDERS "
                        + " WHERE FOLDER_ID=" + lParentFolderId.ToString()
                        + " AND USER_ID='" + this.UserLoginName + "'";

                    objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                    if (objDbReader.Read())
                    {
                        sParentFolderName = objDbReader.GetString("FOLDER_NAME");
                    }
                }
                else
                {
                    sParentFolderName = "My Documents";
                }

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetDocuments.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader = null;
                }
            }
            //*********************************************
            //Changes made by Mohit Yadav to include paging
            //*********************************************
            return GetDocuments(lSortId, p_lFolderId, lParentFolderId, sFolderName, sParentFolderName, p_lPageNumber, out p_sDocumentsXML);
            //*********************************************
        }
        /// Name			: GetDocuments
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Returns information about documents and folders present
        /// in a particular folder, in Xml format. 
        /// Uses UserLoginName and ConnectionString property values. 
        /// </summary>
        /// <param name="p_lFolderId">
        ///		Id of the folder whose information about documents
        ///		and child folders is required.
        ///	</param>
        /// <param name="p_lParentFolderId">
        ///		Id of the parent folder whose information about
        ///		documents and child folders is required.
        ///	</param>
        /// <param name="p_sFolderName">
        ///		Name of the folder whose information about documents
        ///		and child folders is required.
        ///	</param>
        /// <param name="p_sParentFolderName">
        ///		Name of the parent folder whose information about documents
        ///		and child folders is required.
        ///	</param>
        /// <param name="p_sDocumentsXML">
        ///		Out parameter. Returns information about documents in Xml
        ///		format
        ///	</param>		
        /// <returns>0 - Success</returns>
        public int GetDocuments(int lSortId, long p_lFolderId, long p_lParentFolderId,
            string p_sFolderName, string p_sParentFolderName,
            out string p_sDocumentsXML)
        {
            int iReturnValue = 0;

            DataSet objFoldersDataSet = null;
            DataSet objDocumentsDataSet = null;

            DataTable objDocumentTable = null;
            DataTable objDocFoldersTable = null;

            DbReader objDbReader = null;

            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objFolderElement = null;
            XmlElement objXmlElement = null;

            long lParentFolderId = 0;
            string sSQL = string.Empty;
            LocalCache objCache = null;
            DbConnection objCon = null;//Mona:PaperVisionMerge : Animesh
            try
            {
                RaiseInitializationException();
                objCon = DbFactory.GetDbConnection(this.ConnectionString); //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objCon.Open(); 
                //Check if the user has USERDOC_VIEW permission
                //if(!(m_userLogin.IsAllowedEx(USERDOC_SID) && m_userLogin.IsAllowed(USERDOC_VIEW_SID)))
                //{
                //	throw new PermissionViolationException(RMPermissions.RMO_VIEW,USERDOC_VIEW_SID);
                //}

                if (p_lFolderId != 0)
                {
                    sSQL = "SELECT PARENT_ID "
                        + " FROM DOC_FOLDERS "
                        + " WHERE FOLDER_ID=" + p_lFolderId.ToString()
                        + " AND USER_ID='" + this.UserLoginName + "'";

                    objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                    if (!objDbReader.Read())
                    {
                        throw new InvalidValueException(Globalization.GetString("DocumentManager.GetDocuments.InvalidFolderId", m_iClientId));
                    }
                    else
                    {
                        lParentFolderId = objDbReader.GetInt("PARENT_ID");
                        if (lParentFolderId != p_lParentFolderId)
                        {
                            throw new InvalidValueException(Globalization.GetString("DocumentManager.GetDocuments.InvalidParentFolderId", m_iClientId));
                        }
                    }
                }


                objXmlDocument = new XmlDocument();
                objRootElement = objXmlDocument.CreateElement("data");
                objRootElement.SetAttribute("name", p_sFolderName);
                objRootElement.SetAttribute("pid", p_lFolderId.ToString());
                objRootElement.SetAttribute("sid", this.SecurityId.ToString());
                CheckDocListPermissions(ref objRootElement, USERDOC_SID);
                objXmlDocument.AppendChild(objRootElement);

                objFolderElement = objXmlDocument.CreateElement("folder");
                objFolderElement.SetAttribute("pid", p_lParentFolderId.ToString());
                objFolderElement.InnerText = p_sParentFolderName;

                objRootElement.AppendChild(objFolderElement);

                // Get Child Folders
                sSQL = "SELECT FOLDER_ID, FOLDER_NAME "
                    + " FROM DOC_FOLDERS "
                    + " WHERE PARENT_ID=" + p_lFolderId.ToString()
                    + " AND USER_ID='" + this.UserLoginName + "'"
                    + " ORDER BY FOLDER_NAME";


                objFoldersDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);
                objDocFoldersTable = objFoldersDataSet.Tables[0];


                foreach (DataRow objRow in objDocFoldersTable.Rows)
                {
                    objXmlElement = objXmlDocument.CreateElement("folder");

                    objXmlElement.SetAttribute("pid", objRow["FOLDER_ID"].ToString());

                    objXmlElement.InnerText = objRow["FOLDER_NAME"].ToString();

                    if (Conversion.ConvertStrToLong(objRow["FOLDER_ID"].ToString()) != RECYCLE_BIN_ID)
                    {
                        objFolderElement.AppendChild(objXmlElement);
                    }
                    else
                    {
                        objFolderElement.InsertBefore(objXmlElement, objFolderElement.FirstChild);
                    }

                    objXmlElement = null;
                }

                // Get all documents
                sSQL = "SELECT DOCUMENT_ID,DOCUMENT_NAME,DOCUMENT_FILENAME,KEYWORDS, "
                    + " CREATE_DATE,NOTES,USER_ID, DOCUMENT_CLASS, DOCUMENT_CATEGORY, "
                    + " DOCUMENT_SUBJECT, DOCUMENT_TYPE, FOLDER_ID, DOC_INTERNAL_TYPE, DOCUMENT_FILEPATH "
                    + " FROM DOCUMENT "
                    + " WHERE FOLDER_ID=" + p_lFolderId.ToString()
                    + " AND USER_ID='" + this.UserLoginName + "'";
                    //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                    if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                        sSQL = sSQL + " AND NVL(ISAT_PV,2) <> -1 ";
                    else
                        sSQL = sSQL + " AND ISNULL(ISAT_PV,2) <> -1 ";
                    sSQL = sSQL + " ORDER BY DOCUMENT_NAME";

                    objCon.Close();  
                    //Animesh Insertion Ends

                    objDocumentsDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);
                objDocumentTable = objDocumentsDataSet.Tables[0];


                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                foreach (DataRow objRow in objDocumentTable.Rows)
                {
                    objXmlElement = objXmlDocument.CreateElement("document");

                    objXmlElement.SetAttribute("pid", objRow["DOCUMENT_ID"].ToString());
                    objXmlElement.SetAttribute("title", objRow["DOCUMENT_NAME"].ToString());
                    objXmlElement.SetAttribute("folderid", objRow["FOLDER_ID"].ToString());
                    objXmlElement.SetAttribute("keywords", objRow["KEYWORDS"].ToString());
                    objXmlElement.SetAttribute("createdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objRow["CREATE_DATE"].ToString()), "d"));
                    objXmlElement.SetAttribute("filename", objRow["DOCUMENT_FILENAME"].ToString());
                    objXmlElement.SetAttribute("userid", objRow["USER_ID"].ToString());
                    objXmlElement.SetAttribute("subject", objRow["DOCUMENT_SUBJECT"].ToString());
                    objXmlElement.SetAttribute("type", objCache.GetCodeDesc(Conversion.ConvertStrToInteger(objRow["DOCUMENT_TYPE"].ToString())));
                    objXmlElement.SetAttribute("class", objCache.GetCodeDesc(Conversion.ConvertStrToInteger(objRow["DOCUMENT_CLASS"].ToString())));
                    objXmlElement.SetAttribute("category", objCache.GetCodeDesc(Conversion.ConvertStrToInteger(objRow["DOCUMENT_CATEGORY"].ToString())));

                    //Manoj - LSS Interface
                    objXmlElement.SetAttribute("docinternaltype", objRow["DOC_INTERNAL_TYPE"].ToString());
                    objXmlElement.SetAttribute("filepath", objRow["DOCUMENT_FILEPATH"].ToString());

                    objXmlElement.InnerText = objRow["NOTES"].ToString();

                    objFolderElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                }

                p_sDocumentsXML = objXmlDocument.OuterXml;

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetDocuments.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetDocuments.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader = null;
                }

                if (objDocumentsDataSet != null)
                {
                    objDocumentsDataSet.Dispose();
                    objDocumentsDataSet = null;
                }

                if (objFoldersDataSet != null)
                {
                    objFoldersDataSet.Dispose();
                    objFoldersDataSet = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();

                }
                objDocumentTable = null;
                objDocFoldersTable = null;

                objXmlElement = null;
                objFolderElement = null;
                objRootElement = null;
                objXmlDocument = null;
                if (objCon != null)//Mona:PaperVisionMerge : Animesh
                {
                    objCon.Dispose();
                    objCon = null;
                } 

            }

            return iReturnValue;
        }
        #region public int GetDocuments(int pisortid, long p_lFolderId,long p_lParentFolderId, string p_sFolderName, string p_sParentFolderName, long p_lPageNumber, out string p_sDocumentsXML)

        /// Name			: GetDocuments
        /// Author			: Mohit Yadav
        /// Date Created	: 25/05/2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Returns information about documents and folders present
        /// in a particular folder, in Xml format. 
        /// Uses UserLoginName and ConnectionString property values. 
        /// </summary>
        ///  <param name="lSortId">
        ///		Sort order of the records
        ///	</param>
        /// <param name="p_lFolderId">
        ///		Id of the folder whose information about documents
        ///		and child folders is required.
        ///	</param>
        /// <param name="p_lParentFolderId">
        ///		Id of the parent folder whose information about
        ///		documents and child folders is required.
        ///	</param>
        /// <param name="p_sFolderName">
        ///		Name of the folder whose information about documents
        ///		and child folders is required.
        ///	</param>
        /// <param name="p_sParentFolderName">
        ///		Name of the parent folder whose information about documents
        ///		and child folders is required.
        ///	</param>
        ///	<param name="p_lPageNumber">
        ///		Page Number of the requested page based on the Total no. of information/Page Size.
        ///	</param>
        /// <param name="p_sDocumentsXML">
        ///		Out parameter. Returns information about documents in Xml
        ///		format
        ///	</param>		
        /// <returns>0 - Success</returns>
        public int GetDocumentsNew(int lSortId, long p_lFolderId, long p_lParentFolderId, string p_sFolderName,
            string p_sParentFolderName, long p_lPageNumber, out DocumentList objDocumentList)
        {
            int iReturnValue = 0;
            SysSettings objSysSetting = null;  //added by vgupta79
            DataSet objFoldersDataSet = null;
            DataSet objDocumentsDataSet = null;
            DataSet objRecycleBinFoldersDataSet = null;

            DataTable objDocumentTable = null;
            DataTable objDocFoldersTable = null;
            DataTable objRecycleBinDocFoldersTable = null;

            DbReader objDbReader = null;

            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objFolderElement = null;
            XmlElement objXmlElement = null;
            XmlDocument objXml = null;      // Umesh
            XmlNode objSessionDSN = null; // Umesh
            //DbReader objRdr=null;//Umesh

            long lParentFolderId = 0;
            string sSQL = string.Empty;
            string sSQLRecycleBin = string.Empty;

            long lFolderRecordCount = 0;
            long lRecycleBinFolderRecordCount = 0;
            long lTotalFolderCount = 0;
            long lDocRecordCount = 0;
            long lTotalRecordCount = 0;
            long lPageSize = 10;
            long lTotalPageCount = 0;
            long lPageStart = 0;
            long lPageEnd = 0;
            int iSortOrder = 0;
            LocalCache objCache = null;
            //abisht MITS 9045
            DbConnection objCon = null;
            string sOraCaseIns = string.Empty;
            long lFileSize = 0;
            double dSizeInMB = 0;
            string sOraPVFlag = string.Empty; //Mona:PaperVisionMerge : Animesh
            try
            {
                RaiseInitializationException();
                objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);
                //Check if the user has USERDOC_VIEW permission
                //if(!(m_userLogin.IsAllowedEx(USERDOC_SID) && m_userLogin.IsAllowed(USERDOC_VIEW_SID)))
                //{
                //	throw new PermissionViolationException(RMPermissions.RMO_VIEW,USERDOC_VIEW_SID);
                //}

                //abisht MITS 9045
                objCon = DbFactory.GetDbConnection(this.ConnectionString);
                objCon.Open();


                if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                {
                    sOraCaseIns = " UPPER(USER_ID) ";
                    sOraPVFlag = " AND NVL(ISAT_PV,2) <> -1 "; //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                }
                else
                {
                    sOraCaseIns = "USER_ID";
                    sOraPVFlag = " AND ISNULL(ISAT_PV,2) <> -1 "; //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                }
                //End abisht MITS 9045
                if (p_lFolderId != 0)
                {
                    sSQL = "SELECT PARENT_ID "
                        + " FROM DOC_FOLDERS "
                        + " WHERE FOLDER_ID=" + p_lFolderId.ToString()
                        //abisht MITS 9045
                        + " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'";

                    objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                    if (!objDbReader.Read())
                    {
                        throw new InvalidValueException(Globalization.GetString("DocumentManager.GetDocuments.InvalidFolderId", m_iClientId));
                    }
                    else
                    {
                        lParentFolderId = objDbReader.GetInt("PARENT_ID");
                        if (lParentFolderId != p_lParentFolderId)
                        {
                            throw new InvalidValueException(Globalization.GetString("DocumentManager.GetDocuments.InvalidParentFolderId", m_iClientId));
                        }
                    }
                }

                // Get Child Folders without Recycle Bin
                sSQL = "SELECT FOLDER_ID, FOLDER_NAME "
                    + " FROM DOC_FOLDERS "
                    + " WHERE PARENT_ID=" + p_lFolderId.ToString()
                    //abisht MITS 9045
                    //+ " AND USER_ID='" + this.UserLoginName + "'" 
                    + " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'"
                    + " AND FOLDER_NAME != 'Recycle Bin'";

                switch (lSortId)
                {
                    case (int)DOCSORTORDER.ASC_NAME:
                        sSQL = sSQL + " ORDER BY FOLDER_NAME ASC";
                        break;
                    case (int)DOCSORTORDER.DEC_NAME:
                        sSQL = sSQL + " ORDER BY FOLDER_NAME DESC";
                        break;
                    default:
                        sSQL = sSQL + " ORDER BY FOLDER_NAME ASC";
                        break;
                }


                objFoldersDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);
                objDocFoldersTable = objFoldersDataSet.Tables[0];

                //getting the Folders record count

                if (objFoldersDataSet.Tables.Count > 0)
                {
                    lFolderRecordCount = objDocFoldersTable.Rows.Count;
                }

                //Get Recycle Bin

                sSQLRecycleBin = "SELECT FOLDER_ID, FOLDER_NAME "
                    + " FROM DOC_FOLDERS "
                    + " WHERE PARENT_ID=" + p_lFolderId.ToString()
                    //abisht MITS 9045
                    //+ " AND USER_ID='" + this.UserLoginName + "'" 
                    + " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'"
                    + " AND FOLDER_NAME = 'Recycle Bin'";
                switch (lSortId)
                {
                    case (int)DOCSORTORDER.ASC_NAME:
                        sSQL = sSQL + " ORDER BY FOLDER_NAME ASC";
                        break;
                    case (int)DOCSORTORDER.DEC_NAME:
                        sSQL = sSQL + " ORDER BY FOLDER_NAME DESC";
                        break;
                    default:
                        sSQL = sSQL + " ORDER BY FOLDER_NAME ASC";
                        break;
                }


                objRecycleBinFoldersDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQLRecycleBin, m_iClientId);
                objRecycleBinDocFoldersTable = objRecycleBinFoldersDataSet.Tables[0];

                //getting the Recycle Bin Folders record count

                if (objRecycleBinFoldersDataSet.Tables.Count > 0)
                {
                    lRecycleBinFolderRecordCount = objRecycleBinDocFoldersTable.Rows.Count;
                }

                //Total Folder Count
                lTotalFolderCount = lFolderRecordCount + lRecycleBinFolderRecordCount;

                // Get all documents
                //smishra25:Added upload status to check if the document has been uploaded
                sSQL = "SELECT DOCUMENT_ID,UPLOAD_STATUS,DOCUMENT_NAME,DOCUMENT_FILENAME,KEYWORDS, "
                    + " CREATE_DATE,NOTES,USER_ID, (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT_CLASS) AS DOCUMENT_CLASS,"
                    + " (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT_CATEGORY) AS DOCUMENT_CATEGORY,"
                    + " DOCUMENT_SUBJECT, "
                    + "(SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT_TYPE) AS DOCUMENT_TYPE, "
                    + " FOLDER_ID, DOC_INTERNAL_TYPE, DOCUMENT_FILEPATH"
                    + " FROM DOCUMENT "
                    + " WHERE FOLDER_ID=" + p_lFolderId.ToString()
                    //abisht MITS 9045
                    //+ " AND USER_ID='" + this.UserLoginName + "'";
                    + " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'" 
                    + sOraPVFlag ; //Mona:PaperVisionMerge : Animesh Inserted MITS 16697

              
                switch (lSortId)
                {
                    case (int)DOCSORTORDER.ASC_NAME:
                        if (objSysSetting.OracleCaseIns == 1 || objSysSetting.OracleCaseIns == -1)
                        {
                            sSQL = sSQL + " ORDER BY UPPER(DOCUMENT_NAME) ASC, UPPER(DOCUMENT_FILENAME) ASC"; //vgupta79 RMA-9594                      
                        }
                        else
                        {
                            sSQL = sSQL + " ORDER BY DOCUMENT_NAME ASC, DOCUMENT_FILENAME ASC"; //aaggarwal29 : MITS 37558,JIRA 6367 on UI, we are using combination of both the values, hence adding it to order by clause
                        }
                        
                        iSortOrder = (int)DOCSORTORDER.ASC_NAME;
                        break;
                    case (int)DOCSORTORDER.ASC_SUBJECT:
                        if (objSysSetting.OracleCaseIns == 1 || objSysSetting.OracleCaseIns == -1)
                        {
                            sSQL = sSQL + " ORDER BY UPPER(DOCUMENT_SUBJECT) ASC";   //vgupta79 RMA-9594                      
                        }
                        else
                        {
                            sSQL = sSQL + " ORDER BY DOCUMENT_SUBJECT ASC";
                        }

                        iSortOrder = (int)DOCSORTORDER.ASC_SUBJECT;
                        break;
                    case (int)DOCSORTORDER.ASC_TYPE:
                        sSQL = sSQL + " ORDER BY DOCUMENT_TYPE ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_TYPE;
                        break;
                    case (int)DOCSORTORDER.ASC_CLASS:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CLASS ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CLASS;
                        break;
                    case (int)DOCSORTORDER.ASC_CATEGORY:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CATEGORY ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CATEGORY;
                        break;
                    case (int)DOCSORTORDER.ASC_CREATE_DATE:
                        sSQL = sSQL + " ORDER BY CREATE_DATE ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CREATE_DATE;
                        break;
                    case (int)DOCSORTORDER.DEC_NAME:

                        if (objSysSetting.OracleCaseIns == 1 || objSysSetting.OracleCaseIns == -1)
                        {
                            sSQL = sSQL + " ORDER BY UPPER(DOCUMENT_NAME) DESC, UPPER(DOCUMENT_FILENAME) DESC"; //vgupta79 RMA-9594
                        }
                        else
                        {
                            sSQL = sSQL + " ORDER BY DOCUMENT_NAME DESC, DOCUMENT_FILENAME DESC"; //aaggarwal29 : MITS 37558,JIRA 6367 on UI, we are using combination of both the values, hence adding it to order by clause
                        }
 
                        iSortOrder = (int)DOCSORTORDER.DEC_NAME;
                        break;
                    case (int)DOCSORTORDER.DEC_SUBJECT:

                        if (objSysSetting.OracleCaseIns == 1 || objSysSetting.OracleCaseIns == -1)
                        {
                            sSQL = sSQL + " ORDER BY UPPER(DOCUMENT_SUBJECT) DESC";   //vgupta79 RMA-9594
                        }
                        else
                        {
                            sSQL = sSQL + " ORDER BY DOCUMENT_SUBJECT DESC";
                        }
                        iSortOrder = (int)DOCSORTORDER.DEC_SUBJECT;
                        break;
                    case (int)DOCSORTORDER.DEC_TYPE:
                        sSQL = sSQL + " ORDER BY DOCUMENT_TYPE DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_TYPE;
                        break;
                    case (int)DOCSORTORDER.DEC_CLASS:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CLASS DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CLASS;
                        break;
                    case (int)DOCSORTORDER.DEC_CATEGORY:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CATEGORY DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CATEGORY;
                        break;
                    case (int)DOCSORTORDER.DEC_CREATE_DATE:
                        sSQL = sSQL + " ORDER BY CREATE_DATE DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CREATE_DATE;
                        break;
                    default:

                        if (objSysSetting.OracleCaseIns == 1 || objSysSetting.OracleCaseIns == -1)
                        {
                            sSQL = sSQL + " ORDER BY UPPER(DOCUMENT_NAME) ASC, UPPER(DOCUMENT_FILENAME) ASC";  //vgupta79 RMA-9594                      
                        }
                        else
                        {
                            sSQL = sSQL + " ORDER BY DOCUMENT_NAME ASC, DOCUMENT_FILENAME ASC"; //aaggarwal29 : MITS 37558,JIRA 6367 on UI, we are using combination of both the values, hence adding it to order by clause
                        }

                        lSortId = (int)DOCSORTORDER.ASC_NAME;
                        iSortOrder = (int)DOCSORTORDER.ASC_NAME;
                        break;
                }

                objDocumentsDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);
                objDocumentTable = objDocumentsDataSet.Tables[0];

                //getting the Document record count

                if (objDocumentsDataSet.Tables.Count > 0)
                {
                    lDocRecordCount = objDocumentTable.Rows.Count;
                }

                //getting the total record count
                lTotalRecordCount = lTotalFolderCount + lDocRecordCount;

                //getting the page count to be added to the attribute of the data element
                lTotalPageCount = lTotalRecordCount / lPageSize;
                if (lTotalRecordCount > lTotalPageCount * lPageSize)
                {
                    lTotalPageCount = lTotalPageCount + 1;
                }

                if (lTotalPageCount == 0)
                {
                    lTotalPageCount = 1;
                }

                /*objXmlDocument = new XmlDocument(); 
                objRootElement = objXmlDocument.CreateElement("data"); 
                objRootElement.SetAttribute("name",p_sFolderName);
                objRootElement.SetAttribute("pid",p_lFolderId.ToString());
                objRootElement.SetAttribute("sid",this.SecurityId.ToString() ;*/
                objDocumentList = new DocumentList();
                objDocumentList.Name = p_sFolderName;
                objDocumentList.Pid = p_lFolderId.ToString();
                objDocumentList.Sid = this.SecurityId.ToString();
                CheckDocumentListPermissions(ref objDocumentList, USERDOC_SID);

                if (p_lPageNumber > 1)
                {
                    objDocumentList.Previouspage = (p_lPageNumber - 1).ToString();
                    objDocumentList.Firstpage = "1";
                }
                if (p_lPageNumber < lTotalPageCount)
                {
                    if (p_lPageNumber == 0) { p_lPageNumber = 1; }
                    objDocumentList.Nextpage = ((int)p_lPageNumber + 1).ToString();
                    objDocumentList.Lastpage = lTotalPageCount.ToString();
                }
                if (p_lPageNumber > lTotalPageCount)
                {
                    objDocumentList.Pagenumber = lTotalPageCount.ToString();
                    p_lPageNumber = lTotalPageCount;
                }
                else
                {
                    objDocumentList.Pagenumber = p_lPageNumber.ToString();
                }
                objDocumentList.Pagecount = lTotalPageCount.ToString();
                objDocumentList.Orderby = iSortOrder.ToString();

                Folder objFolder = new Folder();
                objFolder.FolderID = p_lParentFolderId.ToString();
                objFolder.FolderName = p_sParentFolderName;

                objDocumentList.TopFolder = objFolder;
                objDocumentList.Folders = new System.Collections.Generic.List<Folder>();
                //objDocumentList.Folders.Add(objFolder);
                //getting the loop starting point and end point
                if (p_lPageNumber == 1)
                    lPageStart = 1;
                else
                    lPageStart = ((p_lPageNumber - 1) * lPageSize) + 1;

                lPageEnd = lPageStart + lPageSize - 1;
                if (lPageEnd > lTotalRecordCount)
                    lPageEnd = lTotalRecordCount;

                long lIndex = lPageStart;
                int iFolderIndex = 0;
                int iDocIndex = 0;

                //Recycle Bin Folder Rows
                while ((lIndex <= lRecycleBinFolderRecordCount) && (lIndex <= lPageEnd))
                {
                    iFolderIndex = (int)(lIndex - 1);
                    objFolder = new Folder();
                    //iFolderIndex = (int)(lIndex - 1);
                    //objXmlElement = objXmlDocument.CreateElement("folder");
                    //objXmlElement.SetAttribute("pid",objRecycleBinDocFoldersTable.Rows[iFolderIndex]["FOLDER_ID"].ToString());
                    //
                    //objXmlElement.InnerText = objRecycleBinDocFoldersTable.Rows[iFolderIndex]["FOLDER_NAME"].ToString();
                    objFolder.FolderID = objRecycleBinDocFoldersTable.Rows[iFolderIndex]["FOLDER_ID"].ToString();
                    objFolder.FolderName = objRecycleBinDocFoldersTable.Rows[iFolderIndex]["FOLDER_NAME"].ToString();

                    if (Conversion.ConvertStrToLong(objRecycleBinDocFoldersTable.Rows[iFolderIndex]["FOLDER_ID"].ToString()) != RECYCLE_BIN_ID)
                    {
                        //objFolderElement.AppendChild(objXmlElement);
                        objDocumentList.Folders.Add(objFolder);

                    }
                    else
                    {
                        //objFolderElement.InsertBefore(objXmlElement,objFolderElement.FirstChild);
                        objDocumentList.Folders.Insert(0, objFolder);
                    }
                    //objXmlElement = null;

                    lIndex = lIndex + 1;
                }
               // lIndex = 0;
                //Folder Rows
                while ((lIndex <= lTotalFolderCount) && (lIndex <= lPageEnd))
                {
                    objFolder = new Folder();
                    iFolderIndex = (int)(lIndex - lRecycleBinFolderRecordCount - 1);
                    /*objXmlElement = objXmlDocument.CreateElement("folder"); 
                    objXmlElement.SetAttribute("pid",objDocFoldersTable.Rows[iFolderIndex]["FOLDER_ID"].ToString());
                    objXmlElement.InnerText = objDocFoldersTable.Rows[iFolderIndex]["FOLDER_NAME"].ToString();
                    */
                    objFolder.FolderID = objDocFoldersTable.Rows[iFolderIndex]["FOLDER_ID"].ToString();
                    objFolder.FolderName = objDocFoldersTable.Rows[iFolderIndex]["FOLDER_NAME"].ToString();
                    if (Conversion.ConvertStrToLong(objDocFoldersTable.Rows[iFolderIndex]["FOLDER_ID"].ToString()) != RECYCLE_BIN_ID)
                    {
                        //objFolderElement.AppendChild(objXmlElement);
                        objDocumentList.Folders.Add(objFolder);
                    }
                    else
                    {
                        //objFolderElement.InsertBefore(objXmlElement,objFolderElement.FirstChild);
                        objDocumentList.Folders.Insert(0, objFolder);
                    }
                    //objXmlElement = null;

                    lIndex = lIndex + 1;
                }

                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                //Document Rows
                iDocIndex = 0;
                objDocumentList.Documents = new System.Collections.Generic.List<DocumentType>();
                while (lIndex <= lPageEnd)
                {

                    iDocIndex = (int)(lIndex - lTotalFolderCount - 1);
                    DocumentType objDocument = new DocumentType();
                    objDocument.Title = objDocumentTable.Rows[iDocIndex]["DOCUMENT_NAME"].ToString();
                    objDocument.FolderId = objDocumentTable.Rows[iDocIndex]["FOLDER_ID"].ToString();
                    objDocument.Keywords = objDocumentTable.Rows[iDocIndex]["KEYWORDS"].ToString();
                    objDocument.CreateDate = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDocumentTable.Rows[iDocIndex]["CREATE_DATE"].ToString()), "d");
                    objDocument.FileName = objDocumentTable.Rows[iDocIndex]["DOCUMENT_FILENAME"].ToString();
                    objDocument.UserName = objDocumentTable.Rows[iDocIndex]["USER_ID"].ToString();
                    objDocument.Subject = objDocumentTable.Rows[iDocIndex]["DOCUMENT_SUBJECT"].ToString();
                    objDocument.DocumentsType = objDocumentTable.Rows[iDocIndex]["DOCUMENT_TYPE"].ToString();
                    objDocument.DocumentClass = objDocumentTable.Rows[iDocIndex]["DOCUMENT_CLASS"].ToString();
                    objDocument.DocumentCategory = objDocumentTable.Rows[iDocIndex]["DOCUMENT_CATEGORY"].ToString();
                    objDocument.Notes = objDocumentTable.Rows[iDocIndex]["NOTES"].ToString();
                    objDocument.Pid = objDocumentTable.Rows[iDocIndex]["DOCUMENT_ID"].ToString();
                    objDocument.UserId = objDocumentTable.Rows[iDocIndex]["USER_ID"].ToString();
                    //Manoj - LSS Interface
                    objDocument.DocInternalType = objDocumentTable.Rows[iDocIndex]["DOC_INTERNAL_TYPE"].ToString();
                    objDocument.FilePath = objDocumentTable.Rows[iDocIndex]["DOCUMENT_FILEPATH"].ToString();
                    //smishra25:Adding filesize to the DocumentType object
                    if (string.Compare(objDocumentTable.Rows[iDocIndex]["UPLOAD_STATUS"].ToString(), "1") == 0)
                    {
                        objDocument.FileSizeInMB = -1;
                    }
                    else
                    {
                        SetDocumentFileSize(ref objDocument);//smishra25:Adding filesize
                    }
                    objDocumentList.Documents.Add(objDocument);



                    lIndex = lIndex + 1;


                }
                //Added for REM-Nadim(5/4/2009) 

                //Add move_allowed attribute
                //XmlNode objDataNode =objXmlDocument.SelectSingleNode("//data");
                //XmlElement objDataElement=(XmlElement)objDataNode;
                //objDataElement.SetAttribute("move_allowed","1");
                objDocumentList.Move_allowed = "1";
                //Added for Rem
                string sEmail_allowed = string.Empty;
                string sTransfer_allowed = string.Empty;
                string sCopy_allowed = string.Empty;
                string sMove_allowed = string.Empty;

                
                objXml = new XmlDocument();
                string strContent = RMSessionManager.GetCustomContent(m_iClientId);
                if (! string.IsNullOrEmpty(strContent))
                    {
                        objXml.LoadXml(strContent);
                        sEmail_allowed = objXml.SelectSingleNode("//RMAdminSettings/DocumentManagement/Files_Email").InnerText;
                        sTransfer_allowed = objXml.SelectSingleNode("//RMAdminSettings/DocumentManagement/Files_Transfer").InnerText;
                        sCopy_allowed = objXml.SelectSingleNode("//RMAdminSettings/DocumentManagement/Files_Copy").InnerText;
                        sMove_allowed = objXml.SelectSingleNode("//RMAdminSettings/DocumentManagement/Files_Move").InnerText;

                        //objRdr.Dispose();
                        //RemoveButton(objXml, objXmlDocument, "//RMAdminSettings/DocumentManagement/Files_Email", "//data/@email_allowed");
                        //RemoveButton(objXml, objXmlDocument, "//RMAdminSettings/DocumentManagement/Files_Transfer", "//data/@transfer_allowed");
                        //RemoveButton(objXml, objXmlDocument, "//RMAdminSettings/DocumentManagement/Files_Copy", "//data/@copy_allowed");
                        //RemoveButton(objXml, objXmlDocument, "//RMAdminSettings/DocumentManagement/Files_Move", "//data/@move_allowed");
                        if (sEmail_allowed == "0")
                        {
                            objDocumentList.Email_allowed = "0";
                        }
                        else {
                            objDocumentList.Email_allowed = "1";
                        }

                        if (sTransfer_allowed == "0")
                        {
                            objDocumentList.Transfer_allowed = "0";
                        }
                        else
                        {
                            objDocumentList.Transfer_allowed = "1";
                        }

                        if (sCopy_allowed == "0")
                        {
                            objDocumentList.Copy_allowed = "0";
                        }
                        else
                        {
                            objDocumentList.Copy_allowed = "1";
                        }

                        if (sMove_allowed == "0")
                        {
                            objDocumentList.Move_allowed = "0";
                        }
                        else
                        {
                            objDocumentList.Move_allowed = "1";
                        }
                        //objDocumentList.Email_allowed = "0";
                        //objDocumentList.Transfer_allowed = "0";
                        //objDocumentList.Copy_allowed = "0";
                        //objDocumentList.Move_allowed = "0";
                    }

                //p_sDocumentsXML = objXmlDocument.OuterXml; 
                //Added for REM-Nadim(5/4/2009) 

            }

            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetDocuments.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetDocuments.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader = null;
                }

                if (objDocumentsDataSet != null)
                {
                    objDocumentsDataSet.Dispose();
                    objDocumentsDataSet = null;
                }

                if (objFoldersDataSet != null)
                {
                    objFoldersDataSet.Dispose();
                    objFoldersDataSet = null;
                }

                if (objRecycleBinFoldersDataSet != null)
                {
                    objRecycleBinFoldersDataSet.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objDocumentTable = null;
                objDocFoldersTable = null;

                objXmlElement = null;
                objFolderElement = null;
                objRootElement = null;
                objXmlDocument = null;
                objXml = null;
                objSessionDSN = null;
                //if (objRdr != null)
                //{
                //    objRdr.Close();
                //    objRdr = null;
                //}
                //abisht MITS 9045
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
            }

            return iReturnValue;
        }


        #endregion

        #region public int GetDocuments(int pisortid, long p_lFolderId,long p_lParentFolderId, string p_sFolderName, string p_sParentFolderName, long p_lPageNumber, out string p_sDocumentsXML)

        /// Name			: GetDocuments
        /// Author			: Mohit Yadav
        /// Date Created	: 25/05/2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Returns information about documents and folders present
        /// in a particular folder, in Xml format. 
        /// Uses UserLoginName and ConnectionString property values. 
        /// </summary>
        ///  <param name="lSortId">
        ///		Sort order of the records
        ///	</param>
        /// <param name="p_lFolderId">
        ///		Id of the folder whose information about documents
        ///		and child folders is required.
        ///	</param>
        /// <param name="p_lParentFolderId">
        ///		Id of the parent folder whose information about
        ///		documents and child folders is required.
        ///	</param>
        /// <param name="p_sFolderName">
        ///		Name of the folder whose information about documents
        ///		and child folders is required.
        ///	</param>
        /// <param name="p_sParentFolderName">
        ///		Name of the parent folder whose information about documents
        ///		and child folders is required.
        ///	</param>
        ///	<param name="p_lPageNumber">
        ///		Page Number of the requested page based on the Total no. of information/Page Size.
        ///	</param>
        /// <param name="p_sDocumentsXML">
        ///		Out parameter. Returns information about documents in Xml
        ///		format
        ///	</param>		
        /// <returns>0 - Success</returns>
        public int GetDocuments(int lSortId, long p_lFolderId, long p_lParentFolderId, string p_sFolderName,
            string p_sParentFolderName, long p_lPageNumber, out string p_sDocumentsXML)
        {
            int iReturnValue = 0;

            DataSet objFoldersDataSet = null;
            DataSet objDocumentsDataSet = null;
            DataSet objRecycleBinFoldersDataSet = null;

            DataTable objDocumentTable = null;
            DataTable objDocFoldersTable = null;
            DataTable objRecycleBinDocFoldersTable = null;

            DbReader objDbReader = null;

            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objFolderElement = null;
            XmlElement objXmlElement = null;
            XmlDocument objXml = null;      // Umesh
            XmlNode objSessionDSN = null; // Umesh
            //DbReader objRdr=null;//Umesh

            long lParentFolderId = 0;
            string sSQL = string.Empty;
            string sSQLRecycleBin = string.Empty;

            long lFolderRecordCount = 0;
            long lRecycleBinFolderRecordCount = 0;
            long lTotalFolderCount = 0;
            long lDocRecordCount = 0;
            long lTotalRecordCount = 0;
            long lPageSize = 10;
            long lTotalPageCount = 0;
            long lPageStart = 0;
            long lPageEnd = 0;
            int iSortOrder = 0;
            LocalCache objCache = null;
            //abisht MITS 9045
            DbConnection objCon = null;
            string sOraCaseIns = string.Empty;
            string sOraPVFlag = string.Empty;//Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            try
            {
                RaiseInitializationException();

                //Check if the user has USERDOC_VIEW permission
                //if(!(m_userLogin.IsAllowedEx(USERDOC_SID) && m_userLogin.IsAllowed(USERDOC_VIEW_SID)))
                //{
                //	throw new PermissionViolationException(RMPermissions.RMO_VIEW,USERDOC_VIEW_SID);
                //}

                //abisht MITS 9045
                objCon = DbFactory.GetDbConnection(this.ConnectionString);
                objCon.Open();


                if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                {
                    sOraCaseIns = " UPPER(USER_ID) ";
                    sOraPVFlag = " AND NVL(ISAT_PV,2) <> -1 "; //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                }
                else
                {
                    sOraCaseIns = "USER_ID";
                    sOraPVFlag = " AND ISNULL(ISAT_PV,2) <> -1 ";//Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                }
                //End abisht MITS 9045
                if (p_lFolderId != 0)
                {
                    sSQL = "SELECT PARENT_ID "
                        + " FROM DOC_FOLDERS "
                        + " WHERE FOLDER_ID=" + p_lFolderId.ToString()
                        //abisht MITS 9045
                        + " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'";

                    objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                    if (!objDbReader.Read())
                    {
                        throw new InvalidValueException(Globalization.GetString("DocumentManager.GetDocuments.InvalidFolderId", m_iClientId));
                    }
                    else
                    {
                        lParentFolderId = objDbReader.GetInt("PARENT_ID");
                        if (lParentFolderId != p_lParentFolderId)
                        {
                            throw new InvalidValueException(Globalization.GetString("DocumentManager.GetDocuments.InvalidParentFolderId", m_iClientId));
                        }
                    }
                }

                // Get Child Folders without Recycle Bin
                sSQL = "SELECT FOLDER_ID, FOLDER_NAME "
                    + " FROM DOC_FOLDERS "
                    + " WHERE PARENT_ID=" + p_lFolderId.ToString()
                    //abisht MITS 9045
                    //+ " AND USER_ID='" + this.UserLoginName + "'" 
                    + " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'"
                    + " AND FOLDER_NAME != 'Recycle Bin'";

                switch (lSortId)
                {
                    case (int)DOCSORTORDER.ASC_NAME:
                        sSQL = sSQL + " ORDER BY FOLDER_NAME ASC";
                        break;
                    case (int)DOCSORTORDER.DEC_NAME:
                        sSQL = sSQL + " ORDER BY FOLDER_NAME DESC";
                        break;
                    default:
                        sSQL = sSQL + " ORDER BY FOLDER_NAME ASC";
                        break;
                }


                objFoldersDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);
                objDocFoldersTable = objFoldersDataSet.Tables[0];

                //getting the Folders record count

                if (objFoldersDataSet.Tables.Count > 0)
                {
                    lFolderRecordCount = objDocFoldersTable.Rows.Count;
                }

                //Get Recycle Bin

                sSQLRecycleBin = "SELECT FOLDER_ID, FOLDER_NAME "
                    + " FROM DOC_FOLDERS "
                    + " WHERE PARENT_ID=" + p_lFolderId.ToString()
                    //abisht MITS 9045
                    //+ " AND USER_ID='" + this.UserLoginName + "'" 
                    + " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'"
                    + " AND FOLDER_NAME = 'Recycle Bin'";
                switch (lSortId)
                {
                    case (int)DOCSORTORDER.ASC_NAME:
                        sSQL = sSQL + " ORDER BY FOLDER_NAME ASC";
                        break;
                    case (int)DOCSORTORDER.DEC_NAME:
                        sSQL = sSQL + " ORDER BY FOLDER_NAME DESC";
                        break;
                    default:
                        sSQL = sSQL + " ORDER BY FOLDER_NAME ASC";
                        break;
                }


                objRecycleBinFoldersDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQLRecycleBin,m_iClientId);//psharma206
                objRecycleBinDocFoldersTable = objRecycleBinFoldersDataSet.Tables[0];

                //getting the Recycle Bin Folders record count

                if (objRecycleBinFoldersDataSet.Tables.Count > 0)
                {
                    lRecycleBinFolderRecordCount = objRecycleBinDocFoldersTable.Rows.Count;
                }

                //Total Folder Count
                lTotalFolderCount = lFolderRecordCount + lRecycleBinFolderRecordCount;

                // Get all documents

                sSQL = "SELECT DOCUMENT_ID,DOCUMENT_NAME,DOCUMENT_FILENAME,KEYWORDS, "
                    + " CREATE_DATE,NOTES,USER_ID, (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT_CLASS) AS DOCUMENT_CLASS,"
                    + " (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT_CATEGORY) AS DOCUMENT_CATEGORY,"
                    + " DOCUMENT_SUBJECT, "
                    + "(SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT_TYPE) AS DOCUMENT_TYPE, "
                    + " FOLDER_ID, DOC_INTERNAL_TYPE, DOCUMENT_FILEPATH"
                    + " FROM DOCUMENT "
                    + " WHERE FOLDER_ID=" + p_lFolderId.ToString()
                    //abisht MITS 9045
                    //+ " AND USER_ID='" + this.UserLoginName + "'";
                    + " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'"
                    + sOraPVFlag; //Mona:PaperVisionMerge : Animesh Inserted MITS 16697

                switch (lSortId)
                {
                    case (int)DOCSORTORDER.ASC_NAME:
                        sSQL = sSQL + " ORDER BY DOCUMENT_NAME ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_NAME;
                        break;
                    case (int)DOCSORTORDER.ASC_SUBJECT:
                        sSQL = sSQL + " ORDER BY DOCUMENT_SUBJECT ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_SUBJECT;
                        break;
                    case (int)DOCSORTORDER.ASC_TYPE:
                        sSQL = sSQL + " ORDER BY DOCUMENT_TYPE ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_TYPE;
                        break;
                    case (int)DOCSORTORDER.ASC_CLASS:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CLASS ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CLASS;
                        break;
                    case (int)DOCSORTORDER.ASC_CATEGORY:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CATEGORY ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CATEGORY;
                        break;
                    case (int)DOCSORTORDER.ASC_CREATE_DATE:
                        sSQL = sSQL + " ORDER BY CREATE_DATE ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CREATE_DATE;
                        break;
                    case (int)DOCSORTORDER.DEC_NAME:
                        sSQL = sSQL + " ORDER BY DOCUMENT_NAME DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_NAME;
                        break;
                    case (int)DOCSORTORDER.DEC_SUBJECT:
                        sSQL = sSQL + " ORDER BY DOCUMENT_SUBJECT DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_SUBJECT;
                        break;
                    case (int)DOCSORTORDER.DEC_TYPE:
                        sSQL = sSQL + " ORDER BY DOCUMENT_TYPE DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_TYPE;
                        break;
                    case (int)DOCSORTORDER.DEC_CLASS:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CLASS DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CLASS;
                        break;
                    case (int)DOCSORTORDER.DEC_CATEGORY:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CATEGORY DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CATEGORY;
                        break;
                    case (int)DOCSORTORDER.DEC_CREATE_DATE:
                        sSQL = sSQL + " ORDER BY CREATE_DATE DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CREATE_DATE;
                        break;
                    default:
                        sSQL = sSQL + " ORDER BY DOCUMENT_NAME ASC";
                        lSortId = (int)DOCSORTORDER.ASC_NAME;
                        iSortOrder = (int)DOCSORTORDER.ASC_NAME;
                        break;
                }

                objDocumentsDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);
                objDocumentTable = objDocumentsDataSet.Tables[0];

                //getting the Document record count

                if (objDocumentsDataSet.Tables.Count > 0)
                {
                    lDocRecordCount = objDocumentTable.Rows.Count;
                }

                //getting the total record count
                lTotalRecordCount = lTotalFolderCount + lDocRecordCount;

                //getting the page count to be added to the attribute of the data element
                lTotalPageCount = lTotalRecordCount / lPageSize;
                if (lTotalRecordCount > lTotalPageCount * lPageSize)
                {
                    lTotalPageCount = lTotalPageCount + 1;
                }

                if (lTotalPageCount == 0)
                {
                    lTotalPageCount = 1;
                }

                objXmlDocument = new XmlDocument();
                objRootElement = objXmlDocument.CreateElement("data");
                objRootElement.SetAttribute("name", p_sFolderName);
                objRootElement.SetAttribute("pid", p_lFolderId.ToString());
                objRootElement.SetAttribute("sid", this.SecurityId.ToString());
                CheckDocListPermissions(ref objRootElement, USERDOC_SID);

                if (p_lPageNumber > 1)
                {
                    objRootElement.SetAttribute("previouspage", (p_lPageNumber - 1).ToString());
                    objRootElement.SetAttribute("firstpage", "1");
                }
                if (p_lPageNumber < lTotalPageCount)
                {
                    if (p_lPageNumber == 0) { p_lPageNumber = 1; }
                    objRootElement.SetAttribute("nextpage", ((int)p_lPageNumber + 1).ToString());
                    objRootElement.SetAttribute("lastpage", lTotalPageCount.ToString());
                }

                objRootElement.SetAttribute("pagenumber", p_lPageNumber.ToString());
                objRootElement.SetAttribute("pagecount", lTotalPageCount.ToString());
                objRootElement.SetAttribute("orderby", iSortOrder.ToString());
                objXmlDocument.AppendChild(objRootElement);

                objFolderElement = objXmlDocument.CreateElement("folder");
                objFolderElement.SetAttribute("pid", p_lParentFolderId.ToString());
                //objFolderElement.InnerText = p_sParentFolderName;
                objRootElement.AppendChild(objFolderElement);

                //getting the loop starting point and end point
                if (p_lPageNumber == 1)
                    lPageStart = 1;
                else
                    lPageStart = ((p_lPageNumber - 1) * lPageSize) + 1;

                lPageEnd = lPageStart + lPageSize - 1;
                if (lPageEnd > lTotalRecordCount)
                    lPageEnd = lTotalRecordCount;

                long lIndex = lPageStart;
                int iFolderIndex = 0;
                int iDocIndex = 0;

                //Recycle Bin Folder Rows
                while ((lIndex <= lRecycleBinFolderRecordCount) && (lIndex <= lPageEnd))
                {
                    iFolderIndex = (int)(lIndex - 1);
                    objXmlElement = objXmlDocument.CreateElement("folder");
                    objXmlElement.SetAttribute("pid", objRecycleBinDocFoldersTable.Rows[iFolderIndex]["FOLDER_ID"].ToString());

                    objXmlElement.InnerText = objRecycleBinDocFoldersTable.Rows[iFolderIndex]["FOLDER_NAME"].ToString();


                    if (Conversion.ConvertStrToLong(objRecycleBinDocFoldersTable.Rows[iFolderIndex]["FOLDER_ID"].ToString()) != RECYCLE_BIN_ID)
                    {
                        objFolderElement.AppendChild(objXmlElement);
                    }
                    else
                    {
                        objFolderElement.InsertBefore(objXmlElement, objFolderElement.FirstChild);
                    }
                    objXmlElement = null;

                    lIndex = lIndex + 1;
                }

                //Folder Rows
                while ((lIndex <= lTotalFolderCount) && (lIndex <= lPageEnd))
                {
                    iFolderIndex = (int)(lIndex - lRecycleBinFolderRecordCount - 1);
                    objXmlElement = objXmlDocument.CreateElement("folder");
                    objXmlElement.SetAttribute("pid", objDocFoldersTable.Rows[iFolderIndex]["FOLDER_ID"].ToString());
                    objXmlElement.InnerText = objDocFoldersTable.Rows[iFolderIndex]["FOLDER_NAME"].ToString();

                    if (Conversion.ConvertStrToLong(objDocFoldersTable.Rows[iFolderIndex]["FOLDER_ID"].ToString()) != RECYCLE_BIN_ID)
                    {
                        objFolderElement.AppendChild(objXmlElement);
                    }
                    else
                    {
                        objFolderElement.InsertBefore(objXmlElement, objFolderElement.FirstChild);
                    }
                    objXmlElement = null;

                    lIndex = lIndex + 1;
                }

                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                //Document Rows
                iDocIndex = 0;
                while (lIndex <= lPageEnd)
                {
                    iDocIndex = (int)(lIndex - lTotalFolderCount - 1);
                    objXmlElement = objXmlDocument.CreateElement("document");
                    objXmlElement.SetAttribute("pid", objDocumentTable.Rows[iDocIndex]["DOCUMENT_ID"].ToString());
                    objXmlElement.SetAttribute("title", objDocumentTable.Rows[iDocIndex]["DOCUMENT_NAME"].ToString());
                    objXmlElement.SetAttribute("folderid", objDocumentTable.Rows[iDocIndex]["FOLDER_ID"].ToString());
                    objXmlElement.SetAttribute("keywords", objDocumentTable.Rows[iDocIndex]["KEYWORDS"].ToString());
                    objXmlElement.SetAttribute("createdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDocumentTable.Rows[iDocIndex]["CREATE_DATE"].ToString()), "d"));
                    objXmlElement.SetAttribute("filename", objDocumentTable.Rows[iDocIndex]["DOCUMENT_FILENAME"].ToString());
                    objXmlElement.SetAttribute("userid", objDocumentTable.Rows[iDocIndex]["USER_ID"].ToString());
                    objXmlElement.SetAttribute("subject", objDocumentTable.Rows[iDocIndex]["DOCUMENT_SUBJECT"].ToString());
                    objXmlElement.SetAttribute("type", objDocumentTable.Rows[iDocIndex]["DOCUMENT_TYPE"].ToString());
                    objXmlElement.SetAttribute("class", objDocumentTable.Rows[iDocIndex]["DOCUMENT_CLASS"].ToString());
                    objXmlElement.SetAttribute("category", objDocumentTable.Rows[iDocIndex]["DOCUMENT_CATEGORY"].ToString());

                    //Manoj - LSS Interface
                    objXmlElement.SetAttribute("docinternaltype", objDocumentTable.Rows[iDocIndex]["DOC_INTERNAL_TYPE"].ToString());
                    objXmlElement.SetAttribute("filepath", objDocumentTable.Rows[iDocIndex]["DOCUMENT_FILEPATH"].ToString());

                    objXmlElement.InnerText = objDocumentTable.Rows[iDocIndex]["NOTES"].ToString();
                    objFolderElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    lIndex = lIndex + 1;
                }

                //Added for REM-Nadim(5/4/2009) 
                //Add move_allowed attribute
                XmlNode objDataNode = objXmlDocument.SelectSingleNode("//data");
                XmlElement objDataElement = (XmlElement)objDataNode;
                objDataElement.SetAttribute("move_allowed", "1");

                objXml = new XmlDocument();
                string strContent = RMSessionManager.GetCustomContent(m_iClientId);//psharma206
                if (! string.IsNullOrEmpty(strContent))
                    {
                        objXml.LoadXml(strContent);
                        //objRdr.Dispose();
                        RemoveButton(objXml, objXmlDocument, "//RMAdminSettings/DocumentManagement/Files_Email", "//data/@email_allowed");
                        RemoveButton(objXml, objXmlDocument, "//RMAdminSettings/DocumentManagement/Files_Transfer", "//data/@transfer_allowed");
                        RemoveButton(objXml, objXmlDocument, "//RMAdminSettings/DocumentManagement/Files_Copy", "//data/@copy_allowed");
                        RemoveButton(objXml, objXmlDocument, "//RMAdminSettings/DocumentManagement/Files_Move", "//data/@move_allowed");
                    }
                //Added for REM-Nadim(5/4/2009) 

                p_sDocumentsXML = objXmlDocument.OuterXml;

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetDocuments.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetDocuments.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader = null;
                }

                if (objDocumentsDataSet != null)
                {
                    objDocumentsDataSet.Dispose();
                    objDocumentsDataSet = null;
                }

                if (objFoldersDataSet != null)
                {
                    objFoldersDataSet.Dispose();
                    objFoldersDataSet = null;
                }

                if (objRecycleBinFoldersDataSet != null)
                {
                    objRecycleBinFoldersDataSet.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objDocumentTable = null;
                objDocFoldersTable = null;

                objXmlElement = null;
                objFolderElement = null;
                objRootElement = null;
                objXmlDocument = null;
                objXml = null;
                objSessionDSN = null;
                //if (objRdr != null)
                //{
                //    objRdr.Close();
                //    objRdr = null;
                //}
                //abisht MITS 9045
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
            }

            return iReturnValue;
        }


        #endregion
       

        /// Name			: GetDocumentDetails
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Retrieves document details in Xml format
        /// Uses UserLoginName and ConnectionString property values.
        /// </summary>
        /// <param name="p_lDocumentId">Document Id</param>
        /// <param name="iPSID">Security Id</param>
        /// <param name="p_sDocumentXML">
        ///		Out parameter. Document details in Xml format.
        ///	</param>
        /// <returns>0 - Success</returns>
        public int GetDocumentDetails(long p_lDocumentId, int iPSID, string p_sScreenFlag, out string p_sDocumentXML)
        {
            int iReturnValue = 0;
            Document objDocument = null;
            XmlDocument oXmlDocument = null;
            XmlElement oDataElement = null;
            XmlNode oDocumentNode = null;
            XmlNode oNode = null;
            XmlDocument objXml = null;      // Umesh
            XmlNode objSessionDSN = null; // Umesh
            //DbReader objRdr=null;//Umesh

            try
            {
                RaiseInitializationException();

                objDocument = new Document(m_iClientId);

                objDocument.DocumentId = p_lDocumentId;
                objDocument.ConnectionString = this.ConnectionString;
                objDocument.UserLoginName = this.UserLoginName;

                objDocument.Load();
                
                objDocument.GetDocumentXml(out p_sDocumentXML);
                oXmlDocument = new XmlDocument();
                oXmlDocument.LoadXml(p_sDocumentXML);
                oDataElement = (XmlElement)oXmlDocument.SelectSingleNode("//data");

                CheckDocPermissions(ref oDataElement, iPSID);

                //Add psid to the response
                oDocumentNode = oXmlDocument.SelectSingleNode("//Document");
                oNode = oXmlDocument.CreateElement("", "psid", "");
                oNode.InnerText = iPSID.ToString();
                oDocumentNode.AppendChild(oNode);
                //Rem Umesh
                //Added for REM-Nadim(5/4/2009) 
                //Add move_allowed attribute
                XmlNode objDataNode = oXmlDocument.SelectSingleNode("//data");
                XmlElement objDataElement = (XmlElement)objDataNode;
                objDataElement.SetAttribute("move_allowed", "1");

                objXml = new XmlDocument();
                string strContent = RMSessionManager.GetCustomContent(m_iClientId);
                if (! string.IsNullOrEmpty(strContent))
                    {
                        objXml.LoadXml(strContent);
                        if (p_sScreenFlag == "Files")
                        {
                            RemoveButton(objXml, oXmlDocument, "//RMAdminSettings/DocumentManagement/Document_View", "//data/@view_allowed");
                            RemoveButton(objXml, oXmlDocument, "//RMAdminSettings/DocumentManagement/Document_Email", "//data/@email_allowed");
                            RemoveButton(objXml, oXmlDocument, "//RMAdminSettings/DocumentManagement/Document_Transfer", "//data/@transfer_allowed");
                            RemoveButton(objXml, oXmlDocument, "//RMAdminSettings/DocumentManagement/Document_Copy", "//data/@copy_allowed");
                            RemoveButton(objXml, oXmlDocument, "//RMAdminSettings/DocumentManagement/Document_Download", "//data/@download_allowed");
                            RemoveButton(objXml, oXmlDocument, "//RMAdminSettings/DocumentManagement/Document_Move", "//data/@move_allowed");
                        }
                        else
                        {
                            RemoveButton(objXml, oXmlDocument, "//RMAdminSettings/DocumentManagement/Attachments_P_Email", "//data/@email_allowed");
                            RemoveButton(objXml, oXmlDocument, "//RMAdminSettings/DocumentManagement/Attachments_P_Download", "//data/@download_allowed");
                        }
                    }

                //Added for REM-Nadim(5/4/2009) 

                p_sDocumentXML = oXmlDocument.OuterXml;
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetDocumentDetails.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
                objXml = null;
                objSessionDSN = null;
            }

            return iReturnValue;
        }

        public void UpdateUploadStatusOnUIError(long p_lDocumentId, int p_iUploadStatus)
        {
            Document objDocument = null;
            objDocument = new Document(m_iClientId);

            objDocument.DocumentId = p_lDocumentId;
            objDocument.ConnectionString = this.ConnectionString;
            objDocument.UserLoginName = this.UserLoginName;

            objDocument.Load();
            
            //added by Nitin for Mits 20402
            //if error was occured while uploading that document id then , updating status = 3
            if (objDocument.UploadStatus != 3 && objDocument.UploadStatus != -1 && p_iUploadStatus == 3)
            {
                UpdateAsyncEntryOnException(objDocument);
            }
        }

        
        /// <summary>
        /// Added by Nitin to get File Size
        /// </summary>
        /// <param name="p_sFileName"></param>
        /// <returns></returns>
        public long GetFileSize(string p_sFileName)
        {
            long lFileSize = 0;
            FileStorageManager objFileStorageManager = null;
            try
            {
                //get size of document
                objFileStorageManager = new FileStorageManager(this.DocumentStorageType,
                   this.DestinationStoragePath,m_iClientId);

                lFileSize = objFileStorageManager.GetFileSize(p_sFileName);
            }
            catch 
            {
                //do nothing ,it will just send lFileSize to 0
                lFileSize = 0;
            }
            finally
            {
                objFileStorageManager = null;
            }
            return lFileSize;
        }


        /// Name			: MoveDocuments
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        ///	Moves documents to a different folder.
        ///	Uses ConnectionString property value.
        /// </summary>
        /// <param name="p_sDocumentIds">
        ///		Comma separated list of DocumentIds to be moved.
        ///	</param>
        /// <param name="p_lNewFolderId">
        ///		Id of the new folder
        ///	</param>
        /// <returns>0 - Success</returns>
        public int MoveDocuments(string p_sDocumentIds, long p_lNewFolderId)
        {
            int iReturnValue = 0;

            string sDelimiter = ",";
            string[] arrsDocumentIds = null;

            StringBuilder sbWhereClause = null;

            DbWriter objDbWriter = null;

            try
            {
                RaiseInitializationException();

                if (p_sDocumentIds.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.MoveDocuments.InvalidDocumentIDs", m_iClientId));
                }

                sbWhereClause = new StringBuilder();

                arrsDocumentIds = p_sDocumentIds.Split(sDelimiter.ToCharArray());

                foreach (string sDocumentId in arrsDocumentIds)
                {
                    if (sbWhereClause.Length == 0)
                    {
                        sbWhereClause.Append(" DOCUMENT_ID=" + sDocumentId);
                    }
                    else
                    {
                        sbWhereClause.Append(" OR DOCUMENT_ID=" + sDocumentId);
                    }
                }


                objDbWriter = DbFactory.GetDbWriter(this.ConnectionString);

                objDbWriter.Tables.Add("DOCUMENT");

                objDbWriter.Fields.Add("FOLDER_ID", p_lNewFolderId);

                objDbWriter.Where.Add("(" + sbWhereClause.ToString() + ") AND USER_ID='" + this.UserLoginName + "'");

                objDbWriter.Execute();
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.MoveDocuments.Exception", m_iClientId), p_objException);
            }
            finally
            {
                sbWhereClause = null;
                objDbWriter = null;
            }

            return iReturnValue;

        }

        /// Name			: UpdateDocument
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        ///	Updates Document Details.
        /// Uses ConnectionString and UserLoginName property values.
        /// </summary>
        /// <param name="p_sDocumentXML">
        ///		Document Details in Xml format
        ///	</param>
        ///	<param name="iPSID">parent security ID</param>
        /// <returns>0 - Success</returns>
        public int UpdateDocument(string p_sDocumentXML, int iPSID)
        {
            int iReturnValue = 0;
            Document objDocument = null;
            int iSecurityID = 0;

            try
            {
                RaiseInitializationException();

                //Check if the user has MODIFY permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_MODIFY);
                if ((!m_userLogin.IsAllowedEx(iSecurityID)) && (iSecurityID != 0))
                {
                    throw new PermissionViolationException(RMPermissions.RMO_UPDATE, iSecurityID);
                }

                objDocument = new Document(m_iClientId);

                objDocument.ConnectionString = this.ConnectionString;
                objDocument.UserLoginName = this.UserLoginName;

                iReturnValue = objDocument.Update(p_sDocumentXML);

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.UpdateDocument.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
            }

            return iReturnValue;
        }
        public int UpdateDocument(Riskmaster.Models.DocumentType p_sDocument, int iPSID)
        {
            int iReturnValue = 0;
            Document objDocument = null;
            int iSecurityID = 0;

            try
            {
                RaiseInitializationException();

                //Check if the user has MODIFY permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_MODIFY);
                if ((!m_userLogin.IsAllowedEx(iSecurityID)) && (iSecurityID != 0))
                {
                    throw new PermissionViolationException(RMPermissions.RMO_UPDATE, iSecurityID);
                }

                objDocument = new Document(m_iClientId);

                objDocument.ConnectionString = this.ConnectionString;
                objDocument.UserLoginName = this.UserLoginName;

                iReturnValue = objDocument.Update(p_sDocument);

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.UpdateDocument.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
            }

            return iReturnValue;
        }
        public int UpdateDocumentFile(long p_lDocumentId, int iPSID, byte[] p_objDocumentFile)
        {
            int iReturnValue = 0;
            //int iSecurityID = 0;

            Document objDocument = null;
            FileStorageManager objFileStorageManager = null;

           

            try
            {
                RaiseInitializationException();

                //Mohit Yadav changed the security check from AL to Adaptor
                //Check if the user has USERDOC_VIEWPLAY permission
                //iSecurityID = GetSecurityID(iPSID, FUNCNAME_VIEW);
                //if((!m_userLogin.IsAllowedEx(iSecurityID)) && (iSecurityID !=0))
                //{
                //	throw new PermissionViolationException(RMPermissions.RMO_VIEW,iSecurityID);
                //}

                objDocument = new Document(m_iClientId);

                objDocument.ConnectionString = this.ConnectionString;
                objDocument.UserLoginName = this.UserLoginName;

                objDocument.DocumentId = p_lDocumentId;
                objDocument.Load();

                //MITS 10062 Add support for Linked Documents from RMWorld
                string sActualPath = GetActualDocFilePath(objDocument.FilePath);
                objFileStorageManager = new FileStorageManager(this.DocumentStorageType, sActualPath, m_iClientId);

                objFileStorageManager.UpdateDocumentFile(p_objDocumentFile, objDocument.FileName);

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.RetrieveDocument.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
                objFileStorageManager = null;
            }

            return iReturnValue;
        }
        /// Name			: RetrieveDocument
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************	
        /// <summary>
        /// Retrieves Document file.
        /// </summary>
        /// <param name="p_lDocumentId">
        ///		DocumentId whose file is to be retrieved.
        ///	</param>
        /// <param name="p_objDocumentFile">
        ///		Out parameter. Returns document file.
        ///	</param>
        ///	<param name="iPSID">parent security ID</param>
        /// <returns>0 - Success</returns> 		
        public int RetrieveDocument(long p_lDocumentId, int iPSID, out MemoryStream p_objDocumentFile)
        {
            int iReturnValue = 0;
            //int iSecurityID = 0;

            Document objDocument = null;
            FileStorageManager objFileStorageManager = null;

            p_objDocumentFile = null;
            string sClaimNumber = string.Empty;//rsushilaggar MITS 22040
            string sRetrivedFileName = string.Empty;//rsushilaggar MITS 22040

            try
            {
                RaiseInitializationException();

                //Mohit Yadav changed the security check from AL to Adaptor
                //Check if the user has USERDOC_VIEWPLAY permission
                //iSecurityID = GetSecurityID(iPSID, FUNCNAME_VIEW);
                //if((!m_userLogin.IsAllowedEx(iSecurityID)) && (iSecurityID !=0))
                //{
                //	throw new PermissionViolationException(RMPermissions.RMO_VIEW,iSecurityID);
                //}

                objDocument = new Document(m_iClientId);

                objDocument.ConnectionString = this.ConnectionString;
                objDocument.UserLoginName = this.UserLoginName;

                objDocument.DocumentId = p_lDocumentId;
                objDocument.Load();

                //MITS 10062 Add support for Linked Documents from RMWorld
                string sActualPath = GetActualDocFilePath(objDocument.FilePath);
                objFileStorageManager = new FileStorageManager(this.DocumentStorageType, sActualPath, m_iClientId);

                //Start rsushilaggar Date 02/03/2011 MITS 22040 Merging the fdf file with pdf to email pdf file.
                if (objDocument.FileName.Contains(".fdf"))
                {
                    sClaimNumber = GetClaimNumber(Convert.ToInt32(p_lDocumentId), m_userLogin.objRiskmasterDatabase.ConnectionString);
                    sRetrivedFileName = MergeFdfPdf(sActualPath + "\\" + objDocument.FileName, m_userLogin.objRiskmasterDatabase.ConnectionString, sClaimNumber,objDocument.Title);//added by amitosh for mits 24517 
                    sRetrivedFileName = sRetrivedFileName.Substring(sRetrivedFileName.LastIndexOf("\\") + 1, sRetrivedFileName.Length - sRetrivedFileName.LastIndexOf("\\") - 1);
                }
                else
                {
                    sRetrivedFileName = objDocument.FileName;
                }
                
                objFileStorageManager.RetrieveFile(sRetrivedFileName, out p_objDocumentFile);
                //End rsushilaggar

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.RetrieveDocument.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
                objFileStorageManager = null;
            }

            return iReturnValue;
        }


        public int RetrieveLargeDocument(long p_lDocumentId, int iPSID, out Stream p_objDocumentFile,ref string p_sOutPutStreamFilePath)
        {
            int iReturnValue = 0;
            //int iSecurityID = 0;
            Document objDocument = null;
            FileStorageManager objFileStorageManager = null;
            p_objDocumentFile = null;
            
            try
            {
                RaiseInitializationException();

                //Mohit Yadav changed the security check from AL to Adaptor
                //Check if the user has USERDOC_VIEWPLAY permission
                //iSecurityID = GetSecurityID(iPSID, FUNCNAME_VIEW);
                //if((!m_userLogin.IsAllowedEx(iSecurityID)) && (iSecurityID !=0))
                //{
                //	throw new PermissionViolationException(RMPermissions.RMO_VIEW,iSecurityID);
                //}

                objDocument = new Document(m_iClientId);

                objDocument.ConnectionString = this.ConnectionString;
                objDocument.UserLoginName = this.UserLoginName;

                objDocument.DocumentId = p_lDocumentId;
                objDocument.Load();

                //MITS 10062 Add support for Linked Documents from RMWorld
                string sActualPath = GetActualDocFilePath(objDocument.FilePath);
                objFileStorageManager = new FileStorageManager(this.DocumentStorageType, sActualPath, m_iClientId);

                objFileStorageManager.RetrieveLargeFile(objDocument.FileName, out p_objDocumentFile, ref p_sOutPutStreamFilePath);
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.RetrieveDocument.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
                objFileStorageManager = null;
            }

            return iReturnValue;
        }

        /// Name			: CopyDocuments
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Creates copy of documents for specified users.
        /// </summary>
        /// <param name="p_sDocumentIds">
        ///		Comma separated list of DocumentIds.
        ///	</param>
        /// <param name="p_sUserLoginNames">
        ///		Comma separated list of UserLoginNames.
        ///	</param>
        /// <returns>0 - Success</returns> 				
        public int CopyDocuments(string p_sDocumentIds, string p_sUserLoginNames)
        {
            int iReturnValue = 0;

            Document objDocument = null;
            FileStorageManager objFileStorageManager = null;

            string[] arrsDocumentIds = null;
            string[] arrsUserLoginNames = null;

            string sExistingDocumentFileName = string.Empty;
            string sNewDocumentFileName = string.Empty;

            try
            {
                RaiseInitializationException();

                //Check if the user has USERDOC_COPY permission
                //if(!m_userLogin.IsAllowedEx(USERDOC_COPY_SID))
                //{
                //	throw new PermissionViolationException(RMPermissions.RMO_USERDOC_COPY,USERDOC_COPY_SID);
                //}

                if (p_sDocumentIds.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.InvalidDocumentIDs", m_iClientId));
                }

                if (p_sUserLoginNames.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.UserLoginNames", m_iClientId));
                }

                arrsDocumentIds = p_sDocumentIds.Split(",".ToCharArray());
                arrsUserLoginNames = p_sUserLoginNames.Split(",".ToCharArray());


                objFileStorageManager = new FileStorageManager(this.DocumentStorageType,
                    this.DestinationStoragePath, m_iClientId);

                // TODO - Implement Transaction
                foreach (string sDocumentId in arrsDocumentIds)
                {
                    objDocument = null;
                    sExistingDocumentFileName = string.Empty;

                    objDocument = new Document(m_iClientId);
                    objDocument.ConnectionString = this.ConnectionString;
                    objDocument.UserLoginName = this.UserLoginName;
                    objDocument.DocumentId = Conversion.ConvertStrToLong(sDocumentId);
                    objDocument.Load();

                    //MITS 10062 Add support for Linked Documents from RMWorld
                    string sActualFilePath = GetActualDocFilePath(objDocument.FilePath);
                    objFileStorageManager.DestinationStoragePath = sActualFilePath;

                    sExistingDocumentFileName = objDocument.FileName;

                    foreach (string sUserLoginName in arrsUserLoginNames)
                    {
                        // Create a copy of the document file 
                        objFileStorageManager.GetSafeFileName(sExistingDocumentFileName,
                            out sNewDocumentFileName);
                        //Added by  Amitosh for Mits 23973 (02/23/2011)		
                        string[] arrsFileNameParts = null;

                        if (sNewDocumentFileName != "")
                        {
                            arrsFileNameParts = sNewDocumentFileName.Split("\\".ToCharArray());
                            foreach (string sFName in arrsFileNameParts)
                            {
                                if (sFName.IndexOf('.') > 0)
                                {
                                    sNewDocumentFileName = sFName;
                                }
                            }
                        }

                     // end Amitosh
                        if (sNewDocumentFileName.Length == 0)
                        {
                            throw new InvalidValueException(Globalization.GetString("DocumentManager.InvalidSafeFileName", m_iClientId));
                        }

                        objFileStorageManager.CopyFile(sExistingDocumentFileName,
                            sNewDocumentFileName);

                        // Reset DocumentId to 0 and call Update()
                        // It would create a new record in the database
                        objDocument.DocumentId = 0;
                        objDocument.FileName = sNewDocumentFileName;

                        // Copy document to root folder of the user
                        objDocument.FolderId = 0;

                        objDocument.UserLoginName = sUserLoginName;
                        objDocument.Update();
                    }
                }

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.CopyDocuments.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
                objFileStorageManager = null;
            }

            return iReturnValue;
        }

        /// Name			: TransferDocuments
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Transfers documents to specified users.
        /// </summary>
        /// <param name="p_sDocumentIds">
        ///		Comma separated list of DocumentIds.
        ///	</param>
        /// <param name="p_sUserLoginNames">
        ///		Comma separated list of UserIds.
        ///	</param>
        /// <returns>0 - Success</returns> 						
        public int TransferDocuments(string p_sDocumentIds, string p_sUserLoginNames)
        {
            int iReturnValue = 0;

            try
            {
                RaiseInitializationException();

                //Check if the user has USERDOC_TRANSFER permission
                //if(!m_userLogin.IsAllowedEx(USERDOC_TRANSFER_SID))
                //{
                //	throw new PermissionViolationException(RMPermissions.RMO_USERDOC_TRANSFER,USERDOC_TRANSFER_SID);
                //}

                if (p_sDocumentIds.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.InvalidDocumentIDs", m_iClientId));
                }

                if (p_sUserLoginNames.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.UserLoginNames", m_iClientId));
                }

                // TODO - Implement Transaction 

                // Create copy of the documents for the specified users
                this.CopyDocuments(p_sDocumentIds, p_sUserLoginNames);

                // Delete original documents

                // This call would move the document to Recycle Bin.
                this.Delete(p_sDocumentIds, string.Empty, 0);

                // This call would actually delete the documents
                this.Delete(p_sDocumentIds, string.Empty, 0);

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.TransferDocuments.Exception", m_iClientId), p_objException);
            }
            finally
            {

            }

            return iReturnValue;
        }

        /// Name			: AddDocument
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************	
        /// <summary>
        /// Add new document in the store.
        /// </summary>
        /// <param name="p_sDocumentXML">
        ///		Document properties in Xml format.
        ///	</param>
        /// <param name="p_objDocumentFile">
        ///		Document file.
        ///	</param>
        /// <param name="p_lDocumentID">
        ///		Out parameter. ID of the newly added document.
        ///	</param>
        ///	<param name="iPSID">parent security ID</param>
        /// <returns>0 - Success</returns> 								
        public int AddLargeStream(string p_sDocumentXML,Stream  p_objDocumentFile,int iPSID,string p_sFileSize)
        {
            int iReturnValue = 0;
            
            Document objDocument = null;
            FileStorageManager objFileStorageManager = null;
            string sSafeFileName = string.Empty;
            string[] arrsFileNameParts = null;
            try
            {
                RaiseInitializationException();

                objDocument = new Document(m_iClientId);
                objDocument.ConnectionString = this.ConnectionString;
                //changed by Nitin to load document object
                objDocument.Load(p_sDocumentXML);

                objFileStorageManager = new FileStorageManager(this.DocumentStorageType,
                    this.DestinationStoragePath, m_iClientId);

                objFileStorageManager.GetSafeFileName(objDocument.FileName,
                    out sSafeFileName);

                if (sSafeFileName.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.InvalidSafeFileName", m_iClientId));
                }
                //****************************************************
                //Changes made by Mohit Yadav to get only the filename
                //****************************************************
                
                if (sSafeFileName != "" && sSafeFileName != objDocument.FileName)
                {
                    arrsFileNameParts = sSafeFileName.Split("\\".ToCharArray());
                    foreach (string sFName in arrsFileNameParts)
                    {
                        if (sFName.IndexOf('.') > 0)
                        {
                            sSafeFileName = sFName;
                        }
                    }
                }
                objDocument.FileName = sSafeFileName;
                //****************************************************
                objFileStorageManager.StoreLargeFile(p_objDocumentFile, objDocument.FileName,p_sFileSize);

                objDocument.UploadStatus = -1;
                objDocument.Update();
                iReturnValue = 1;
            }
            catch (InitializationException p_objInitializationException)
            {
                UpdateAsyncEntryOnException(objDocument);
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                UpdateAsyncEntryOnException(objDocument);
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                UpdateAsyncEntryOnException(objDocument);
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                UpdateAsyncEntryOnException(objDocument);
                throw new RMAppException(Globalization.GetString("DocumentManager.AddDocument.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                UpdateAsyncEntryOnException(objDocument);
                throw new RMAppException(Globalization.GetString("DocumentManager.AddDocument.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
                objFileStorageManager = null;
            }

            return iReturnValue;
        }

        public int AddDocument(string p_sDocumentXML, MemoryStream p_objDocumentFile, int iPSID,
            out long p_lDocumentID)
        {
            int iReturnValue = 0;
            int iSecurityID = 0;

            Document objDocument = null;
            FileStorageManager objFileStorageManager = null;
            string sSafeFileName = string.Empty;

            p_lDocumentID = 0;
            XmlDocument objDoc = new XmlDocument();    //pmittal5 MITS 13126 08/26/08
            try
            {
                RaiseInitializationException();

                //pmittal5 MITS 13126 08/26/08
                if (p_sDocumentXML != "")
                {
                    objDoc.LoadXml(p_sDocumentXML);
                }
                // For adm tracking only, get the formname out of the message
                if (iPSID == 13000)
                {
                    //XmlDocument objDoc=new XmlDocument();
                    //objDoc.LoadXml(p_sDocumentXML);
                    //Nitesh 25 May : Replaced tag name FormName with AttachTable
                    //XmlNode oNodeTmp = objDoc.SelectSingleNode("//FormName");
                    XmlNode oNodeTmp = objDoc.SelectSingleNode("//AttachTable");
                    if (oNodeTmp != null)
                        m_sFormName = oNodeTmp.InnerText;
                }

                //pmittal5 MITS 13126 08/26/08  - No need to check Module Security Permissions in case of WCPdf forms.
                if ((XmlElement)objDoc.SelectSingleNode("//WCPdfForm") == null)
                {
                    //End - pmittal5
                    //Check if the user has USERDOC_CREATE or Attachments permission
                    iSecurityID = GetSecurityID(iPSID, FUNCNAME_CREATE);
                    if ((!m_userLogin.IsAllowedEx(iSecurityID)) && (iSecurityID != 0))
                    {
                        throw new PermissionViolationException(RMPermissions.RMO_CREATE, iSecurityID);
                    }
                }
                if (m_iStorageId != 0)
                {

                    objFileStorageManager = new FileStorageManager(this.DocumentStorageType,
                        this.DestinationStoragePath, m_iClientId);
                    objFileStorageManager.StorageId = m_iStorageId;
                    objFileStorageManager.UpdateFile(p_objDocumentFile);
                    return iReturnValue;
                }
                objDocument = new Document(m_iClientId);
                objDocument.ConnectionString = this.ConnectionString;
                objDocument.CreateNewDocument(p_sDocumentXML);
                p_lDocumentID = objDocument.DocumentId;

                // TODO - Implement Transaction

                objFileStorageManager = new FileStorageManager(this.DocumentStorageType,
                    this.DestinationStoragePath, m_iClientId);

                objFileStorageManager.GetSafeFileName(objDocument.FileName,
                    out sSafeFileName);

                if (sSafeFileName.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.InvalidSafeFileName", m_iClientId));
                }
                //****************************************************
                //Changes made by Mohit Yadav to get only the filename
                //****************************************************
                string[] arrsFileNameParts = null;
                if (sSafeFileName != "")
                {
                    arrsFileNameParts = sSafeFileName.Split("\\".ToCharArray());
                    foreach (string sFName in arrsFileNameParts)
                    {
                        if (sFName.IndexOf('.') > 0)
                        {
                            sSafeFileName = sFName;
                        }
                    }
                }
                objDocument.FileName = sSafeFileName;
                //****************************************************
                objFileStorageManager.StoreFile(p_objDocumentFile, objDocument.FileName);

                objDocument.Update();
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.AddDocument.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.AddDocument.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
                objFileStorageManager = null;
            }

            return iReturnValue;
        }

        public void CreateNewDocument(string p_sDocumentXML,int iPSID,ref string p_NewFileName,ref long p_NewDocumentId)
        {
            int iSecurityID = 0;
            string sSafeFileName = string.Empty;
            Document objDocument = null;
            XmlDocument objDoc = new XmlDocument();    //pmittal5 MITS 13126 08/26/08
            FileStorageManager objFileStorageManager = null;
            string[] arrsFileNameParts = null;
            try
            {
                RaiseInitializationException();

                //pmittal5 MITS 13126 08/26/08
                if (p_sDocumentXML != "")
                {
                    objDoc.LoadXml(p_sDocumentXML);
                }
                // For adm tracking only, get the formname out of the message
                if (iPSID == 13000)
                {
                    //XmlDocument objDoc=new XmlDocument();
                    //objDoc.LoadXml(p_sDocumentXML);
                    //Nitesh 25 May : Replaced tag name FormName with AttachTable
                    //XmlNode oNodeTmp = objDoc.SelectSingleNode("//FormName");
                    XmlNode oNodeTmp = objDoc.SelectSingleNode("//AttachTable");
                    if (oNodeTmp != null)
                        m_sFormName = oNodeTmp.InnerText;
                }

                //pmittal5 MITS 13126 08/26/08  - No need to check Module Security Permissions in case of WCPdf forms.
                if ((XmlElement)objDoc.SelectSingleNode("//WCPdfForm") == null)
                {
                    //End - pmittal5
                    //Check if the user has USERDOC_CREATE or Attachments permission
                    iSecurityID = GetSecurityID(iPSID, FUNCNAME_CREATE);
                    if ((!m_userLogin.IsAllowedEx(iSecurityID)) && (iSecurityID != 0))
                    {
                        throw new PermissionViolationException(RMPermissions.RMO_CREATE, iSecurityID);
                    }
                }

                objDocument = new Document(m_iClientId);
                objDocument.ConnectionString = this.ConnectionString;
                
                objDocument.Load(p_sDocumentXML);

                objFileStorageManager = new FileStorageManager(this.DocumentStorageType,
                   this.DestinationStoragePath, m_iClientId);

                objFileStorageManager.GetSafeFileName(objDocument.FileName,
                  out sSafeFileName);
                
                if (sSafeFileName.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.InvalidSafeFileName", m_iClientId));
                }
                //****************************************************
                //Changes made by Mohit Yadav to get only the filename
                //****************************************************

                if (sSafeFileName != "")
                {
                    arrsFileNameParts = sSafeFileName.Split("\\".ToCharArray());
                    foreach (string sFName in arrsFileNameParts)
                    {
                        if (sFName.IndexOf('.') > 0)
                        {
                            sSafeFileName = sFName;
                        }
                    }
                }
                objDocument.FileName = sSafeFileName;
                objDocument.UploadStatus = 1;
                objDocument.Update();
                p_NewDocumentId = objDocument.DocumentId;
                p_NewFileName = sSafeFileName;

                //rsharma220 MITS 33205
                LogClaimActivity("New", p_NewDocumentId.ToString());
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.AddDocument.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.AddDocument.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
            }
        }


        /// Name			: Delete
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Deletes the specified documents, folders and all the child folders
        /// along with the documents inside recursively .
        /// </summary>
        /// <param name="p_sDocumentIds">
        ///		Comma separated list of DocumentIds.
        ///	</param>
        /// <param name="p_sFolderIds">
        ///		Comma separated list of FolderIds.
        ///	</param>
        ///	<param name="iPSID">parent security ID</param>
        /// <returns>0 - Success</returns> 										
        public int Delete(string p_sDocumentIds, string p_sFolderIds, int iPSID)
        {
            int iReturnValue = 0;
            //int iSecurityID = 0;
            string sDelimiter = ",";
            string[] arrsDocumentIds = null;
            string[] arrsFolderIds = null;

            // Stores 'where' clause for DocumentIds passed through p_sDocumentIds
            StringBuilder sbDocumentsWhereClause = null;
            // Stores 'where' clause for FolderIds passed through p_sFolderIds
            StringBuilder sbFoldersWhereClause = null;
            // Stores complete 'where' clause including both DocumentIds and FolderIds
            string sWhereClause = null;
            StringBuilder sbFileInProcessErrMsg = null;
            
            StringBuilder sbDeleteIds = null;
            StringBuilder sbRecycleIds = null;

            StringBuilder sbDocumentFileNamesToBeDeleted = null;
            string[] arrsFileNamesToBeDeleted = null;
            string[] arrsFilePath = null;
            int iDeletedFileIndex = 0;

            string sSQL = null;

            DataSet objDataSet = null;
            DataTable objDocumentTable = null;

            DataSet objChildFoldersDataSet = null;
            DataTable objDocFoldersTable = null;
            StringBuilder sbChildFolderIds = null;

            DbConnection objDbConnection = null;
            DbTransaction objDbTransaction = null;
            

            FileStorageManager objFileStorageManager = null;
            //abisht MITS 9045
            DbConnection objCon = null;
            string sOraCaseIns = string.Empty;

            try
            {
                RaiseInitializationException();

                //Mohit Yadav changed the security check from AL to Adaptor
                //Check for USERDOC_DELETE security
                //iSecurityID = GetSecurityID(iPSID, FUNCNAME_DELETE);
                //if( (!m_userLogin.IsAllowedEx(iSecurityID)) && (iSecurityID !=0))
                //{
                //	throw new PermissionViolationException(RMPermissions.RMO_DELETE, iSecurityID);
                //}

                //abisht MITS 9045
                objCon = DbFactory.GetDbConnection(this.ConnectionString);
                objCon.Open();


                if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                {
                    sOraCaseIns = " UPPER(USER_ID) ";
                }
                else
                {
                    sOraCaseIns = "USER_ID";
                }

                if (p_sDocumentIds.Length == 0 && p_sFolderIds.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.Delete.InvalidIDs", m_iClientId));
                }

                //Added by Nitin for large document upload
                //check if file to the delete is in process of uploading or not
                // npadhy If p_sDocumentIds is blank then It is Folder, and we do not need to validate it
                if (!string.IsNullOrEmpty(p_sDocumentIds))
                {
                    sbFileInProcessErrMsg = ValidateFileBeforeDelete(ref p_sDocumentIds);
                }
                
                if (p_sDocumentIds == "" && sbFileInProcessErrMsg != null)
                {
                    throw new RMAppException(sbFileInProcessErrMsg.ToString());
                }

                sbFoldersWhereClause = new StringBuilder();
                if (p_sFolderIds.Length > 0)
                {
                    arrsFolderIds = p_sFolderIds.Split(sDelimiter.ToCharArray());
                    foreach (string sFolderId in arrsFolderIds)
                    {
                        if (sbFoldersWhereClause.Length == 0)
                        {
                            sbFoldersWhereClause.Append(" FOLDER_ID=" + sFolderId);
                        }
                        else
                        {
                            sbFoldersWhereClause.Append(" OR FOLDER_ID=" + sFolderId);
                        }
                    }
                    // Get all child folders of all the folders to be deleted
                    sSQL = "SELECT FOLDER_ID "
                        + " FROM DOC_FOLDERS "
                        + " WHERE PARENT_ID IN (" + p_sFolderIds + ") "
                        //abisht MITS 9045
                        //+ " AND USER_ID='" + this.UserLoginName + "'";
                        + " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'";

                    objChildFoldersDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);

                    objDocFoldersTable = objChildFoldersDataSet.Tables[0];

                    sbChildFolderIds = new StringBuilder();

                    foreach (DataRow objRow in objDocFoldersTable.Rows)
                    {
                        if (sbChildFolderIds.Length == 0)
                        {
                            sbChildFolderIds.Append(objRow["FOLDER_ID"].ToString());
                        }
                        else
                        {
                            sbChildFolderIds.Append("," + objRow["FOLDER_ID"].ToString());
                        }
                    }

                    // Call Delete recursively to delete all child folders and their documents
                    if (sbChildFolderIds.Length > 0)
                    {
                        this.Delete(string.Empty, sbChildFolderIds.ToString(), iPSID);
                    }

                }

                sbDocumentsWhereClause = new StringBuilder();
                if (p_sDocumentIds.Length > 0)
                {
                    arrsDocumentIds = p_sDocumentIds.Split(sDelimiter.ToCharArray());
                    foreach (string sDocumentId in arrsDocumentIds)
                    {
                        if (sbDocumentsWhereClause.Length == 0)
                        {
                            sbDocumentsWhereClause.Append(" DOCUMENT_ID=" + sDocumentId);
                        }
                        else
                        {
                            sbDocumentsWhereClause.Append(" OR DOCUMENT_ID=" + sDocumentId);
                        }
                    }
                }


                if (sbDocumentsWhereClause.Length > 0 && sbFoldersWhereClause.Length > 0)
                {
                    sWhereClause = sbDocumentsWhereClause.ToString() + " OR " + sbFoldersWhereClause.ToString();
                }
                else if (sbDocumentsWhereClause.Length > 0)
                {
                    sWhereClause = sbDocumentsWhereClause.ToString();
                }
                else
                {
                    sWhereClause = sbFoldersWhereClause.ToString();
                }

                // Get details for all documents to be deleted including those inside 
                // folders that are to be deleted
                sSQL = "SELECT DOCUMENT_ID,FOLDER_ID,DOCUMENT_FILENAME, DOCUMENT_FILEPATH "
                    + " FROM DOCUMENT "
                    + " WHERE (" + sWhereClause + ") ";

                //If from be from User Docuemnts, then check for user name
                if (iPSID == USERDOC_SID)
                {
                    //abisht MITS 9045
                    //sSQL += " AND USER_ID='" + this.UserLoginName + "'";
                    sSQL += " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'";
                }

                objDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);

                objDocumentTable = objDataSet.Tables[0];

                // Reset DocumentIds where clause. It would be built again
                // including information about documents inside the folders
                // to be deleted
                sbDocumentsWhereClause = null;

                sbDocumentsWhereClause = new StringBuilder();
                sbDeleteIds = new StringBuilder();
                sbRecycleIds = new StringBuilder();
                sbDocumentFileNamesToBeDeleted = new StringBuilder();
                arrsFilePath = new string[objDocumentTable.Rows.Count];

                iDeletedFileIndex = 0;
                foreach (DataRow objRow in objDocumentTable.Rows)
                {
                    // Rebuild DocumentId where clause to include documents inside the 
                    // deleted folders				
                    if (sbDocumentsWhereClause.Length == 0)
                    {
                        sbDocumentsWhereClause.Append(" DOCUMENT_ID=" + objRow["DOCUMENT_ID"].ToString());
                    }
                    else
                    {
                        sbDocumentsWhereClause.Append(" OR DOCUMENT_ID=" + objRow["DOCUMENT_ID"].ToString());
                    }

                    if (Conversion.ConvertStrToLong(objRow["FOLDER_ID"].ToString()) == RECYCLE_BIN_ID)
                    {
                        // Document is present in Recycle Bin. To be deleted permanently.
                        if (sbDeleteIds.Length == 0)
                        {
                            sbDeleteIds.Append(objRow["DOCUMENT_ID"].ToString());
                        }
                        else
                        {
                            sbDeleteIds.Append("," + objRow["DOCUMENT_ID"].ToString());
                        }

                        //Get the filepath value from database
                        arrsFilePath[iDeletedFileIndex] = objRow["DOCUMENT_FILEPATH"].ToString();
                        iDeletedFileIndex++;
                        if (sbDocumentFileNamesToBeDeleted.Length == 0)
                        {
                            sbDocumentFileNamesToBeDeleted.Append(objRow["DOCUMENT_FILENAME"].ToString());
                        }
                        else
                        {
                            sbDocumentFileNamesToBeDeleted.Append(@"|" + objRow["DOCUMENT_FILENAME"].ToString());
                        }
                    }
                    else
                    {
                        // Document to be moved to Recycle Bin.
                        if (sbRecycleIds.Length == 0)
                        {
                            sbRecycleIds.Append(objRow["DOCUMENT_ID"].ToString());
                        }
                        else
                        {
                            sbRecycleIds.Append("," + objRow["DOCUMENT_ID"].ToString());
                        }

                    }
                }

                objDbConnection = DbFactory.GetDbConnection(this.ConnectionString);
                objDbConnection.Open();

                //rsharma220 MITS 33205
                LogClaimActivity("DEL", p_sDocumentIds);

                // TODO - Transaction management to be implemented
                // to incorporate file system rollbacks as well
                objDbTransaction = objDbConnection.BeginTransaction();

                if (sbDeleteIds.Length > 0)
                {
                    // Delete documents permanently
                    sSQL = "DELETE FROM DOCUMENT "
                        + " WHERE DOCUMENT_ID IN (" + sbDeleteIds.ToString() + ")";

                    objDbConnection.ExecuteNonQuery(sSQL, objDbTransaction);
                }

                if (sbRecycleIds.Length > 0)
                {
                    // Check for Recycle Bin folder for the user
                    bool bRecycleBinCreated = false;

                    this.CheckRecycleBinForUser(out bRecycleBinCreated);

                    if (!bRecycleBinCreated)
                    {
                        this.CreateRecycleBin();
                    }

                    // Move documents to Recycle Bin
                    sSQL = "UPDATE DOCUMENT "
                        + " SET FOLDER_ID=" + RECYCLE_BIN_ID.ToString()
                        + " WHERE DOCUMENT_ID IN (" + sbRecycleIds.ToString() + ") ";
                        //+ " AND USER_ID='" + this.UserLoginName + "'";
                        //Comment by kuladeep for mits:24855
                        //abisht MITS 9045
                        //+ " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'";

                    objDbConnection.ExecuteNonQuery(sSQL, objDbTransaction);
                }

                if (sbDocumentsWhereClause.Length > 0)
                {
                    // Remove attachments
                    sSQL = "DELETE FROM DOCUMENT_ATTACH "
                        + " WHERE " + sbDocumentsWhereClause.ToString();

                    objDbConnection.ExecuteNonQuery(sSQL, objDbTransaction);
                }

                if (sbFoldersWhereClause.Length > 0)
                {
                    // Delete Folders
                    sSQL = "DELETE FROM DOC_FOLDERS "
                        + " WHERE (" + sbFoldersWhereClause.ToString() + ")"
                        //+ " AND USER_ID='" + this.UserLoginName + "'";
                        //abisht MITS 9045
                        + " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'";

                    objDbConnection.ExecuteNonQuery(sSQL, objDbTransaction);
                }

                if (sbDocumentFileNamesToBeDeleted.Length > 0)
                {
                    arrsFileNamesToBeDeleted = sbDocumentFileNamesToBeDeleted.ToString().Split(@"|".ToCharArray());

                    iDeletedFileIndex = 0;
                    objFileStorageManager = new FileStorageManager(this.DocumentStorageType, this.DestinationStoragePath, m_iClientId);
                    foreach (string sDocumentFileNameToBeDeleted in arrsFileNamesToBeDeleted)
                    {
                        //MITS 10062 Add support for Linked Documents from RMWorld
                        string sActualPath = GetActualDocFilePath(arrsFilePath[iDeletedFileIndex]);
                        objFileStorageManager.DestinationStoragePath = sActualPath;
                        objFileStorageManager.DeleteFile(sDocumentFileNameToBeDeleted);
                    }
                }

                objDbTransaction.Commit();

                //Added by Nitin for large document upload
                //checking if there is any in process file then raise exception
                if (sbFileInProcessErrMsg != null)
                {
                    objDbTransaction.Dispose();
                    objDbTransaction = null;
                    throw new RMAppException(sbFileInProcessErrMsg.ToString());
                }
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }

                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }
                throw new RMAppException(Globalization.GetString("DocumentManager.Delete.Exception", m_iClientId), p_objException);
            }
            finally
            {
                sbDocumentsWhereClause = null;
                sbFoldersWhereClause = null;
                sbDeleteIds = null;
                sbRecycleIds = null;
                sbChildFolderIds = null;
                sbDocumentFileNamesToBeDeleted = null;
                
                if (objDocumentTable != null)
                    objDocumentTable.Dispose();
                objFileStorageManager = null;

                if (objDbTransaction != null)
                {
                    objDbTransaction.Dispose();
                    objDbTransaction = null;
                }

                if (objDbConnection != null)
                {
                    objDbConnection.Close();
                    objDbConnection.Dispose();
                }
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }

                objDocFoldersTable = null;

                if (objChildFoldersDataSet != null)
                {
                    objChildFoldersDataSet.Dispose();
                    objChildFoldersDataSet = null;
                }
                //abisht MITS 9045
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
            }

            return iReturnValue;
        }

        //rsharma220 MITS 33205 Start
        private void LogClaimActivity(string sOptType, string p_sDocumentIds)
        {
            bool bAllowLog = false;
            string sLogText = string.Empty;
            LocalCache objCache = null;
            SysSettings objSettings = null;
            StringBuilder sbText = null;
            int iTableId = 0;
            int iOptType = 0;
            int iActType = 0;
            string recordId = string.Empty;
            string username = string.Empty;
            string[] arrsDocumentIds = null;
            string sDelimiter = ",";
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            try
            {
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);//psharma206 
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                username = this.UserLoginName;
                bAllowLog = objSettings.ClaimActivityLog;
                if (bAllowLog)
                {
                    iTableId = objCache.GetTableId("DOCUMENT");
                    iOptType = objCache.GetCodeId(sOptType, "OPERATION_TYPE");
                    bAllowLog = CommonFunctions.bClaimActLogSetup(m_sConnectionString, iTableId, iOptType, ref sLogText, ref iActType, m_iClientId);
                    if (bAllowLog)
                    {
                        sbText = new StringBuilder();

                        arrsDocumentIds = p_sDocumentIds.Split(sDelimiter.ToCharArray());
                        foreach (string sDocumentId in arrsDocumentIds)
                        {
                            sbText.Append("Document: ");
                            sSQL = "SELECT DOCUMENT_NAME FROM DOCUMENT_ATTACH, DOCUMENT, CLAIM WHERE DOCUMENT_ATTACH.RECORD_ID=CLAIM.CLAIM_ID"
                                                 + " AND DOCUMENT.DOCUMENT_ID=DOCUMENT_ATTACH.DOCUMENT_ID AND DOCUMENT.DOCUMENT_ID =" + sDocumentId;
                            objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                            if (objDbReader.Read())
                            {
                                sbText.Append(objDbReader.GetString("DOCUMENT_NAME") + " ");
                            }
                        }

                        //mkaran2 : MITS 32088 : Just Code Position changed to avoid null checks
                        if (sbText.Length > 0)
                            sbText.Append(" - ");
                        sbText.Append(sLogText);
                        if (sbText.Length > 0)
                            sLogText = sbText.ToString();

                        sSQL = "SELECT CLAIM.CLAIM_ID FROM DOCUMENT_ATTACH, DOCUMENT, CLAIM WHERE DOCUMENT_ATTACH.RECORD_ID=CLAIM.CLAIM_ID"
                                                   + " AND DOCUMENT.DOCUMENT_ID=DOCUMENT_ATTACH.DOCUMENT_ID AND DOCUMENT.DOCUMENT_ID =" + arrsDocumentIds[0];
                        recordId = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnectionString, sSQL));
                        
                        CommonFunctions.CreateClaimActivityLog(m_sConnectionString, username, recordId, iActType, sLogText, m_iClientId);

                        bAllowLog = false;
                        //mkaran2 : MITS 32088 
                    }

                    //if (sbText.Length > 0)
                    //    sbText.Append(" - ");
                    //sbText.Append(sLogText);
                    //if (sbText.Length > 0)
                    //    sLogText = sbText.ToString();

                    //sSQL = "SELECT CLAIM.CLAIM_ID FROM DOCUMENT_ATTACH, DOCUMENT, CLAIM WHERE DOCUMENT_ATTACH.RECORD_ID=CLAIM.CLAIM_ID"
                    //                           + " AND DOCUMENT.DOCUMENT_ID=DOCUMENT_ATTACH.DOCUMENT_ID AND DOCUMENT.DOCUMENT_ID =" + arrsDocumentIds[0];
                    //recordId = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnectionString, sSQL));
                    
                    //if (bAllowLog)
                    //CommonFunctions.CreateClaimActivityLog(m_sConnectionString, username, recordId, iActType, sLogText);

                    //bAllowLog = false;                   
                }
            }

            catch (Exception e)
            {
                throw new RMAppException("LogClaimActivity.Save.Exception", e);
            }
            finally
            {
                sbText = null;
                if (objDbReader != null)
                    objDbReader.Dispose();
            }
        }
        //rsharma220 MITS 33205 End
        
        /// Name			: GetUsers
        /// Author			: Mohit Yadav
        /// Date Created	: 09-May-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// DocumentManager.GetUsers of Application layer that Loads the users details 
        /// that are allowed to login to a current datasource.
        /// </summary>
        ///
        /// <param name="p_sCurrentDSN">An current DSN.</param>
        /// <param name="p_objXmlDocOut">An current DSN.</param>
        /// <param name="iPSID">Security ID.</param>
        /// Structure of the output XML is:
        ///		<user id="" firstname="" lastname="" username="" email=""/>
        /// <returns>A boolean values that represents the success/failure.</returns>
        ///	<returns>A XML with user details.</returns>	
        public bool GetUsers(string p_sCurrentDSN, int iPSID, out string p_objXmlDocOut)
        {
            bool bReturnValue = false;

            XmlElement objRootElement = null;
            XmlAttribute objAttribute = null;
            XmlNode objXmlNode = null;

            int i = 0, j = 0;
            Users objUsers = new Users(m_iClientId);
            DataSources objDataSources = new DataSources(m_iClientId);
            UserLogins objUserLogins = new UserLogins(m_iClientId);
            ArrayList arrlstDataSourcesCol = new ArrayList();
            ArrayList arrlstLoginCol = new ArrayList();
            ArrayList arrlstUserCol = new ArrayList();
            ArrayList p_arrlstPermToLogin = new ArrayList();

            XmlDocument objXmlDocOut = null;
            p_objXmlDocOut = string.Empty;

            try
            {

                if (!objDataSources.Load(ref arrlstDataSourcesCol))
                    return bReturnValue;

                if (!objUsers.Load(ref arrlstUserCol))
                    return bReturnValue;

                if (!objUserLogins.Load(ref arrlstLoginCol))
                    return bReturnValue;

                if (arrlstLoginCol.Count < 1)
                    return bReturnValue;

                for (i = 0; i < arrlstLoginCol.Count; i++)
                {
                    string sUserLoginName = ((UserLogins)arrlstLoginCol[i]).LoginName;
                    int iUserLoginID = ((UserLogins)arrlstLoginCol[i]).UserId;
                    string sDataSourceName = ((UserLogins)arrlstLoginCol[i]).objRiskmasterDatabase.DataSourceName;
                    string sCurrentDataSourceName = p_sCurrentDSN;
                    p_arrlstPermToLogin.Add(sDataSourceName + "^~^~^" + iUserLoginID + "^~^~^" + sUserLoginName + "^~^~^" + sCurrentDataSourceName);
                }

                for (i = 0; i < p_arrlstPermToLogin.Count; i++)
                {
                    ArrayList arrlstTemp = new ArrayList();
                    SplitForStringPattern(p_arrlstPermToLogin[i].ToString(), "^~^~^", ref arrlstTemp);
                    for (j = 0; j < arrlstUserCol.Count; j++)
                    {
                        int iUserID = ((Users)arrlstUserCol[j]).UserId;
                        string sFirstName = ((Users)arrlstUserCol[j]).FirstName;
                        string sLastName = ((Users)arrlstUserCol[j]).LastName;
                        string sEmail = ((Users)arrlstUserCol[j]).Email;
                        if (arrlstTemp[1].ToString() == Convert.ToString(iUserID))
                            p_arrlstPermToLogin[i] = p_arrlstPermToLogin[i] + "^~^~^" + sFirstName + "^~^~^" + sLastName + "^~^~^" + sEmail;
                    }
                }

                objXmlDocOut = new XmlDocument();

                objXmlNode = objXmlDocOut.CreateElement("data");
                objAttribute = objXmlDocOut.CreateAttribute("", "psid", "");
                objAttribute.InnerText = iPSID.ToString();
                objXmlNode.Attributes.Append(objAttribute);
                objXmlDocOut.AppendChild(objXmlNode);

                for (i = 0; i < p_arrlstPermToLogin.Count; i++)
                {
                    ArrayList arrlstTemp = new ArrayList();
                    SplitForStringPattern(p_arrlstPermToLogin[i].ToString(), "^~^~^", ref arrlstTemp);
                    if (arrlstTemp[0].ToString() == arrlstTemp[3].ToString())
                    {
                        objRootElement = objXmlDocOut.CreateElement("user");
                        objRootElement.SetAttribute("id", arrlstTemp[1].ToString());
                        objRootElement.SetAttribute("username", arrlstTemp[2].ToString());
                        objRootElement.SetAttribute("firstname", arrlstTemp[4].ToString());
                        objRootElement.SetAttribute("lastname", arrlstTemp[5].ToString());
                        objRootElement.SetAttribute("email", arrlstTemp[6].ToString());
                        objXmlNode.AppendChild(objRootElement);
                    }
                }

                bReturnValue = true;

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetUsers.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetUsers.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objRootElement = null;
            }

            p_objXmlDocOut = objXmlDocOut.OuterXml;
            return bReturnValue;
        }
        public bool GetUsersNew(string p_sCurrentDSN, int iPSID, out UsersList objList)
        {
            bool bReturnValue = false;

            XmlElement objRootElement = null;
            XmlAttribute objAttribute = null;
            XmlNode objXmlNode = null;

            int i = 0, j = 0;
            Users objUsers = new Users(m_iClientId);
            DataSources objDataSources = new DataSources(m_iClientId);
            UserLogins objUserLogins = new UserLogins(m_iClientId);
            ArrayList arrlstDataSourcesCol = new ArrayList();
            ArrayList arrlstLoginCol = new ArrayList();
            ArrayList arrlstUserCol = new ArrayList();
            ArrayList p_arrlstPermToLogin = new ArrayList();

            XmlDocument objXmlDocOut = null;

            objList = new UsersList();
            try
            {

                if (!objDataSources.Load(ref arrlstDataSourcesCol))
                    return bReturnValue;

                if (!objUsers.Load(ref arrlstUserCol))
                    return bReturnValue;

                if (!objUserLogins.Load(ref arrlstLoginCol))
                    return bReturnValue;

                if (arrlstLoginCol.Count < 1)
                    return bReturnValue;

                for (i = 0; i < arrlstLoginCol.Count; i++)
                {
                    string sUserLoginName = ((UserLogins)arrlstLoginCol[i]).LoginName;
                    int iUserLoginID = ((UserLogins)arrlstLoginCol[i]).UserId;
                    string sDataSourceName = ((UserLogins)arrlstLoginCol[i]).objRiskmasterDatabase.DataSourceName;
                    string sCurrentDataSourceName = p_sCurrentDSN;
                    p_arrlstPermToLogin.Add(sDataSourceName + "^~^~^" + iUserLoginID + "^~^~^" + sUserLoginName + "^~^~^" + sCurrentDataSourceName);
                }

                for (i = 0; i < p_arrlstPermToLogin.Count; i++)
                {
                    ArrayList arrlstTemp = new ArrayList();
                    SplitForStringPattern(p_arrlstPermToLogin[i].ToString(), "^~^~^", ref arrlstTemp);
                    for (j = 0; j < arrlstUserCol.Count; j++)
                    {
                        int iUserID = ((Users)arrlstUserCol[j]).UserId;
                        string sFirstName = ((Users)arrlstUserCol[j]).FirstName;
                        string sLastName = ((Users)arrlstUserCol[j]).LastName;
                        string sEmail = ((Users)arrlstUserCol[j]).Email;
                        if (arrlstTemp[1].ToString() == Convert.ToString(iUserID))
                            p_arrlstPermToLogin[i] = p_arrlstPermToLogin[i] + "^~^~^" + sFirstName + "^~^~^" + sLastName + "^~^~^" + sEmail;
                    }
                }

                objXmlDocOut = new XmlDocument();

                objXmlNode = objXmlDocOut.CreateElement("data");
                objAttribute = objXmlDocOut.CreateAttribute("", "psid", "");
                objAttribute.InnerText = iPSID.ToString();
                objXmlNode.Attributes.Append(objAttribute);
                objXmlDocOut.AppendChild(objXmlNode);
                objList = new UsersList();
                for (i = 0; i < p_arrlstPermToLogin.Count; i++)
                {
                    ArrayList arrlstTemp = new ArrayList();
                    SplitForStringPattern(p_arrlstPermToLogin[i].ToString(), "^~^~^", ref arrlstTemp);
                    /*if (arrlstTemp[0].ToString() == arrlstTemp[3].ToString())
                    {
                        objRootElement = objXmlDocOut.CreateElement("user");
                        objRootElement.SetAttribute("id", arrlstTemp[1].ToString());
                        objRootElement.SetAttribute("username", arrlstTemp[2].ToString());
                        objRootElement.SetAttribute("firstname", arrlstTemp[4].ToString());
                        objRootElement.SetAttribute("lastname", arrlstTemp[5].ToString());
                        objRootElement.SetAttribute("email", arrlstTemp[6].ToString());
                        objXmlNode.AppendChild(objRootElement);
                    }*/

                    if (arrlstTemp[0].ToString() == arrlstTemp[3].ToString())
                    {
                        Riskmaster.Models.User user = new Riskmaster.Models.User();
                        user.Id = arrlstTemp[1].ToString();
                        user.UserName = arrlstTemp[2].ToString();
                        user.FirstName = arrlstTemp[4].ToString();
                        user.LastName = arrlstTemp[5].ToString();
                        user.Email = arrlstTemp[6].ToString();
                        objList.Users.Add(user);
                    }
                }

                bReturnValue = true;

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetUsers.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetUsers.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objRootElement = null;
            }

            return bReturnValue;
        }
        /// Name			: GetBaseSecurityIDForTable
        /// Author			: Nitesh Deedwania
        /// Date Created	: 25 May-2006
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// DocumentManager.GetBaseSecurityIDForTable of Application 
        /// layer gets Base Securiy ID for particular table(i.e. Admin Tracking tables).
        /// </summary>
        ///
        /// <param name="iPSID">An current base security ID.</param>
        /// <param name="sTableName">An current system table name.</param>
        /// Structure of the output XML is:
        /// <returns>return Integer new base ID for a specified table or same ID in case of failure.</returns>
        ///	<returns>A XML with user details.</returns>	
        public int GetBaseSecurityIDForTable(int iPSID, string sTableName)
        {
            if (iPSID == 13000) //This is for Administrative Tracking Tables
            {
                DbReader objReader = null;
                try
                {
                    string sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE GLOSSARY_TYPE_CODE = 468 AND GLOSSARY.SYSTEM_TABLE_NAME='" + sTableName.ToUpper() + "'";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader.Read())
                        iPSID = 5000000 + Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId) * 20;
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("DocumentManagement.GetBaseSecurityIDForTable.Error", m_iClientId), p_objException);
                }
                finally
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                return iPSID;
            }
            else
                return iPSID;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sClaimNumber"></param>
        /// <param name="iLOB"></param>
        /// <returns></returns>
        public int FetchLOBAndClaimId(string sClaimNumber,out int iLOB)
        {
            int iClaimId = 0;
            bool bSuccessClaimId = false;
            bool bSuccessLOB = false;
            try
            {
                iLOB = 0;
                //added to replace en-dash, em-dash and single inverted comma - mits 26371 - tanwar2 - 12/21/2011
                sClaimNumber = StandardizeClaimNumber(sClaimNumber);

                string sSQL = string.Format(" SELECT CLAIM_ID,LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_NUMBER = '{0}'",sClaimNumber);
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        if (objReader.GetValue(0) != null)
                        {

                            iClaimId = Conversion.CastToType<int>(objReader.GetValue(0).ToString(), out bSuccessClaimId);
                            iLOB = Conversion.CastToType<int>(objReader.GetValue(1).ToString(), out bSuccessLOB);
                            if (!bSuccessClaimId || !bSuccessLOB)
                            {
                                throw new RMAppException(Globalization.GetString("DocumentManager.FetchClaimDetails.Exception", m_iClientId));
                            }
                            else
                            {
                                return iClaimId;
                            }
                        }
                        else
                        {
                            throw new RMAppException(Globalization.GetString("DocumentManager.FetchClaimDetails.Exception", m_iClientId));
                        }

                    }
                    else
                    {
                        throw new RMAppException(Globalization.GetString("DocumentManager.FetchClaimId.Exception", m_iClientId));
                    }
                }
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new Exception(Globalization.GetString("DocumentManager.FetchClaimDetails.Exception", m_iClientId), p_objException);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sEventNumber"></param>
        /// <returns></returns>
        //rsharma220 MITS 35659
        public int FetchEventId(string sEventNumber)
        {
            int iEventId = 0;
            bool bSuccessEventId = false;

            try
            {

                //added to replace en-dash, em-dash and single inverted comma - mits 26371 - tanwar2 - 12/21/2011
                sEventNumber = StandardizeClaimNumber(sEventNumber);

                string sSQL = string.Format(" SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = '{0}'", sEventNumber);
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        if (objReader.GetValue(0) != null)
                        {

                            iEventId = Conversion.CastToType<int>(objReader.GetValue(0).ToString(), out bSuccessEventId);

                            if (!bSuccessEventId)
                            {
                                throw new RMAppException(Globalization.GetString("DocumentManager.FetchEventDetails.Exception", m_iClientId));
                            }
                            else
                            {
                                return iEventId;
                            }
                        }
                        else
                        {
                            throw new RMAppException(Globalization.GetString("DocumentManager.FetchEventDetails.Exception", m_iClientId));
                        }

                    }
                    else
                    {
                        throw new RMAppException(Globalization.GetString("DocumentManager.FetchEventId.Exception", m_iClientId));
                    }
                }
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new Exception(Globalization.GetString("DocumentManager.FetchClaimDetails.Exception", m_iClientId), p_objException);
            }

        }

        #region Sending Documents via EMail
        /// <summary>
        /// Sendig the multiple documents to multiple users
        /// </summary>
        /// <param name="p_sUserName">User name</param>
        /// <param name="p_sConnStringName">Security Connection String</param>
        /// <param name="p_sEmailIds">EMail Ids of all the recievers</param>
        /// <param name="p_sMessage">Message of the mail</param>
        /// <param name="p_sDocPath">Document Path</param>
        /// <param name="p_sDocumentIds">Document Names of the documents to be sent</param>
        /// <param name="iPSID">parent security ID</param>
        /// <returns>True if successful else false.</returns>
        public bool EmailDocuments(string p_sUserName, string p_sConnStringName, string p_sEmailIds,
            string p_sMessage, string p_sDocPath, string p_sDocumentIds, int iPSID, string p_sSubject)
        {
            string[] arrsDocumentIds = null;
            string sDelimiter = ",";
            string sEmail = string.Empty;
            bool bRetVal = false;
            Mailer objMail = null;
            int iSecurityID = 0;
            Document objDocument = null;
            FileStorageManager objFileStorageManager = null;
            string sRetrivedFileName = string.Empty;

            try
            {
                RaiseInitializationException();

                //Check if the user has TRANSFER permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_TRANSFER);
                if ((!m_userLogin.IsAllowedEx(iSecurityID)) && (iSecurityID != 0))
                {
                    throw new PermissionViolationException(RMPermissions.RMO_USERDOC_TRANSFER, iSecurityID);
                }

                if (p_sUserName != "")
                {
                    sEmail = GetEmailId(p_sConnStringName, p_sUserName);
                    if (sEmail == "")
                    {
                        throw new InvalidValueException(Globalization.GetString("DocumentManager.Email.InvalidSenderID", m_iClientId));
                    }
                }
                else
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.Email.InvalidUserName", m_iClientId));
                }

                if (p_sEmailIds.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.Email.InvalidIDs", m_iClientId));
                }

                if (p_sDocumentIds.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.Email.InvalidDocIds", m_iClientId));
                }

                if ((sEmail.Length > 0) && (p_sEmailIds.Length > 0) && (p_sDocumentIds.Length > 0))
                {
                    objMail = new Mailer(m_iClientId);
                    objMail.To = p_sEmailIds;
                    objMail.From = sEmail;
                    objMail.Body = p_sMessage;
                    //pmahli MITS 10739 
                    objMail.Subject = p_sSubject;

                    //Add all the files as attachment
                    arrsDocumentIds = p_sDocumentIds.Split(sDelimiter.ToCharArray());
                    objDocument = new Document(m_iClientId);
                    objFileStorageManager = new FileStorageManager(this.DocumentStorageType,
                        this.DestinationStoragePath, m_iClientId);
                    string sClaimNumber = string.Empty;//rsushilaggar MITS 22040
                    foreach (string sDocId in arrsDocumentIds)
                    {
                        objDocument.ConnectionString = this.ConnectionString;
                        objDocument.UserLoginName = this.UserLoginName;
                        objDocument.DocumentId = long.Parse(sDocId);
                        objDocument.Load();

                        //MITS 10062 Add support for Linked Documents from RMWorld
                        string sActualPath = GetActualDocFilePath(objDocument.FilePath);
                        objFileStorageManager.DestinationStoragePath = sActualPath;

                        
                        //Change by kuladeep for mits:29824 Start
                        //objFileStorageManager.RetrieveFile(objDocument.FileName, string.Empty, out sRetrivedFileName);
                        objFileStorageManager.RetrieveFile(objDocument.FileName, string.Empty, p_sUserName, out sRetrivedFileName);
                        //Change by kuladeep for mits:29824 End
                
                        //rsushilaggar MITS 22040 Date 02/03/2011 Merging the fdf file with pdf to email pdf file.
                        if (string.IsNullOrEmpty(sClaimNumber))
                        {
                            sClaimNumber = GetClaimNumber(Convert.ToInt32(sDocId), m_userLogin.objRiskmasterDatabase.ConnectionString);
                        }

                        if (sRetrivedFileName != string.Empty)
                        {
                            //rsushilaggar Merging the fdf file with pdf to email pdf file.
                            if (sRetrivedFileName.Contains(".fdf"))
                            {
                                sRetrivedFileName = MergeFdfPdf(sRetrivedFileName, m_userLogin.objRiskmasterDatabase.ConnectionString, sClaimNumber,objDocument.Title);//added by amitosh for mits 24517 
                            }
                            objMail.AddAttachment(sRetrivedFileName);
                        }
                    }

                    if (sRetrivedFileName != string.Empty)
                    {
                        objMail.SendMail();
                    }

                    bRetVal = true;
                }
            }
            catch (Exception p_objException)
            {
                bRetVal = false;
                //pmahli MITS 10739
                if (p_objException.Source.ToString() == "Riskmaster.Application.FileStorage")
                    throw new RMAppException(Globalization.GetString("DocumentManager.FileRetriveError", m_iClientId), p_objException);
                else
                    throw new RMAppException(Globalization.GetString("DocumentManagement.EmailDocuments.SendError", m_iClientId), p_objException);
            }
            finally
            {
                if (objMail != null)
                    objMail.Dispose();
            }
            return (bRetVal);
        }

        /// <summary>
        /// Author - Rahul Sushil Aggarwal
        /// MITS - 22040 
        /// Date - 02/03/2011
        /// Purpose - This function is used for merging fdf and pdf
        /// </summary>
        /// <param name="sFdfPath"></param>
        /// <param name="p_sConnStringName"></param>
        /// <param name="p_sClaimNumber"></param>
        /// <returns></returns>
        public static string MergeFdfPdf(string sFdfPath, string p_sConnStringName,string p_sClaimNumber,string sDocumentTitle)//added by amitosh for mits 24517 
        {

            string sMergeFinalPDF = string.Empty;
            string sTemp = string.Empty;
            string sFdfPathTemp = string.Empty;
            string sPdfPathReturn = string.Empty;
            string sPdfBasePath = string.Empty;
            string sFroiTempDirName = string.Empty;
            string sFdfFileName = string.Empty;
            string sPdfFileName = string.Empty;
            string sTempFile = string.Empty;
            string sSQL = string.Empty;
            DbReader objReader = null;
            try
            {
                sFdfFileName = sFdfPath.Substring((sFdfPath.LastIndexOf("\\") + 1), (sFdfPath.Length - sFdfPath.LastIndexOf("\\") - 1));
                //rsushilaggar MITS 24517 Date 03/30/2011
                //sPdfFileName = sFdfFileName.Substring(p_sClaimNumber.Length, (sFdfFileName.LastIndexOf("-") == -1 ? sFdfFileName.LastIndexOf(".") : sFdfFileName.LastIndexOf("-")) - p_sClaimNumber.Length) + ".pdf";

                sPdfFileName = sFdfFileName.Substring(p_sClaimNumber.Length, sFdfFileName.Length - p_sClaimNumber.Length);
                sPdfFileName = sPdfFileName.Substring(0, sPdfFileName.LastIndexOf('.')) + ".pdf";

                sSQL = "SELECT FILE_NAME FROM WCP_FORMS WHERE UPPER(FILE_NAME) = '" + sPdfFileName.ToUpper() + "'";
                objReader = DbFactory.GetDbReader(p_sConnStringName, sSQL);
                if (objReader.Read())
                    sTempFile = objReader.GetValue(0).ToString();
                if (string.IsNullOrEmpty(sTempFile))
                {
					//zmohammad JIRA 1811 / MITS 34869 Start
                    if (sPdfFileName.Contains("-"))
                    {
                    sPdfFileName = sPdfFileName.Substring(0, sPdfFileName.LastIndexOf('-')) + ".pdf";
                        sSQL = "SELECT FILE_NAME FROM WCP_FORMS WHERE UPPER(FILE_NAME) = '" + sPdfFileName.ToUpper() + "'";
                    }
                    objReader = DbFactory.GetDbReader(p_sConnStringName, sSQL);
                    if (objReader.Read())
                        sTempFile = objReader.GetValue(0).ToString();
                }
                if (string.IsNullOrEmpty(sTempFile))
                {
                    sSQL = "SELECT PDF_FILE_NAME FROM JURIS_FORMS WHERE UPPER(PDF_FILE_NAME) = '" + sPdfFileName.ToUpper() + "'";
                    if (sPdfFileName.Contains("-"))
                    {
                        sPdfFileName = sPdfFileName.Substring(0, sPdfFileName.LastIndexOf('-')) + ".pdf";
                        sSQL = "SELECT PDF_FILE_NAME FROM JURIS_FORMS WHERE UPPER(PDF_FILE_NAME) = '" + sPdfFileName.ToUpper() + "'";
                    }
                    objReader = DbFactory.GetDbReader(p_sConnStringName, sSQL);
                    if (objReader.Read())
                        sTempFile = objReader.GetValue(0).ToString();
                }
								//zmohammad JIRA 1811 / MITS 34869 End
                //End rsushilaggar
                if (!string.IsNullOrEmpty(sTempFile))
                {
                    sPdfBasePath = GetPdfPath(sDocumentTitle, sPdfFileName);//added by amitosh for mits 24517 
                    sPdfBasePath = sPdfBasePath + sPdfFileName;

                    sFroiTempDirName = sFdfPath.Substring(0, sFdfPath.LastIndexOf("\\"));

                    sPdfPathReturn = sFroiTempDirName + "\\" + (sFdfFileName).Replace(".fdf", ".pdf");

                    // sPdfPathReturn = sFdfPath.Replace(".fdf", ".pdf");

                    acPDFCreatorLib.Initialize();
                    acPDFCreatorLib.SetLicenseKey("CSC Financial Services Group", "07EFCDAB01000100C9D1AB2346F36D68B637892D59E2B2D416C18F0397811F5604016685DBC54D7928B23578A87E09BFEB3D9FC4FE9F");

                    // Open the first PDF document from file
                    System.IO.FileStream file1 = new System.IO.FileStream(sPdfBasePath, FileMode.Open, FileAccess.Read);
                    Amyuni.PDFCreator.IacDocument PdfDoc1 = new Amyuni.PDFCreator.IacDocument(null);
                    PdfDoc1.Open(file1, "");

                    // Open the second fdf document from file
                    System.IO.FileStream file2 = new System.IO.FileStream(sFdfPath, FileMode.Open, FileAccess.Read);
                    Amyuni.PDFCreator.IacDocument PdfDoc2 = new Amyuni.PDFCreator.IacDocument(null);
                    PdfDoc2.Open(file2, "");

                    PdfDoc2.LockAllObjects(true);
                    PdfDoc1.Merge(PdfDoc2, 1);

                    Random rd = new Random();

                    string sTempfilename = rd.Next().ToString();
                    sTempfilename = sFroiTempDirName + "\\" + sTempfilename + ".pdf";

                    // save the result to a third file
                    System.IO.FileStream file3 = new System.IO.FileStream(sTempfilename, FileMode.Create, FileAccess.Write);
                    PdfDoc1.Save(file3);
                    file3.Close();

                    file1.Close();
                    file1.Dispose();
                    file2.Close();
                    file2.Dispose();

                    file3.Close();
                    file3.Dispose();
                    PdfDoc1.LockAllObjects(true);
                    PdfDoc1.Dispose();
                    PdfDoc2.Dispose();

                    Lock_objects(sTempfilename, sPdfPathReturn);
                    //Delete the file
                    //System.IO.File.Delete(sFdfPath);
                }
            }
            catch (Exception exc)
            {

                throw (exc);
            }
            finally
            {
                acPDFCreatorLib.Terminate();

            }
            return sPdfPathReturn;
        }

        /// <summary>
        /// Author - Rahul Sushil Aggarwal
        /// MITS - 22040 
        /// Date - 02/03/2011
        /// Purpose - This function is used to get the pdf from the path.
        /// </summary>
        /// <param name="sFdfPath"></param>
        /// <param name="p_sConnStringName"></param>
        /// <param name="p_sClaimNumber"></param>
        /// <returns></returns>
        private static string GetPdfPath(string sDocumentTitle,string sPDFFileName)
        {
            string sBasepath = string.Empty;
            string sTemp = string.Empty;

            try
            {
				//zmohammad JIRA 1811 / MITS 34869 Start
                if (sPDFFileName.Contains("_"))
                {
                    sPDFFileName = sPDFFileName.Substring(0, sPDFFileName.IndexOf("_"));
                }
                if (string.Compare(sDocumentTitle,"SROI") == 0 )
                {
                    sTemp = "RiskmasterUI\\UI\\pdf-forms\\"+sPDFFileName.ToUpper() + "\\";
                }
                else
                {
                    sTemp = "RiskmasterUI\\UI\\pdf-forms\\";
                }
                sBasepath = Riskmaster.Common.RMConfigurator.BasePath;
                sBasepath = sBasepath.Remove((sBasepath.Length - 10), 10);
                sBasepath = sBasepath + sTemp;

            }
            catch (Exception exc)
            {
                throw exc;
            }

            return sBasepath;


        }

        /// <summary>
        /// Author - Rahul Sushil Aggarwal
        /// MITS - 22040 
        /// Date - 02/03/2011
        /// Purpose - This function is used to get the claim number by document id.
        /// </summary>
        /// <param name="sFdfPath"></param>
        /// <param name="p_sConnStringName"></param>
        /// <param name="p_sClaimNumber"></param>
        /// <returns></returns>
        private static string GetClaimNumber(int p_iDocumentId, string p_sDbConnstring)
        { 
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            string sClaimNumber = string.Empty;
            try
            {
                sSQL = "SELECT CLAIM_NUMBER FROM CLAIM INNER JOIN  DOCUMENT_ATTACH ON CLAIM.CLAIM_ID = DOCUMENT_ATTACH.RECORD_ID WHERE DOCUMENT_ID = " + p_iDocumentId;
                using (objDbReader = DbFactory.GetDbReader(p_sDbConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            sClaimNumber = objDbReader.GetString("CLAIM_NUMBER");
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }


            return sClaimNumber;
        }

        /// <summary>
        /// Author - Rahul Sushil Aggarwal
        /// MITS - 22040 
        /// Date - 02/03/2011
        /// Purpose - This function is used to get the actual pdf from the temp pdf.
        /// </summary>
        /// <param name="sFdfPath"></param>
        /// <param name="p_sConnStringName"></param>
        /// <param name="p_sClaimNumber"></param>
        /// <returns></returns>
        public static void Lock_objects(string filenameandpath, string name)
        {

            System.IO.FileStream file1 = new System.IO.FileStream(filenameandpath, FileMode.Open, FileAccess.Read);

            Amyuni.PDFCreator.IacDocument PdfDoc = new Amyuni.PDFCreator.IacDocument(null);

            PdfDoc.Open(file1, "");

            //need to loop through all the field objects and make them non editable.
            try
            {

                for (int i = 1; i <= PdfDoc.PageCount; i++)
                {
                    PdfDoc.CurrentPage = PdfDoc.GetPage(i);
                    //Declare ArrayList and fill it with all of the objects in the 
                    //current PDF document
                    ArrayList arList = (ArrayList)PdfDoc.GetPage(i).Attribute("Objects").Value;
                    foreach (Amyuni.PDFCreator.IacObject obj in arList)
                    {
						//zmohammad JIRA 1811 / MITS 34869 Start
                        if (obj.Attribute("Annotation") != null)
                        {
                            obj.Attribute("Annotation").Value = false;
                        }
                    }
                }

                // Open the PDF document file.
                string sFilePathAndNameSave = name;
                //save doc to new file
                System.IO.FileStream fs2 = new System.IO.FileStream(sFilePathAndNameSave, FileMode.Create, FileAccess.Write, FileShare.Read);
                PdfDoc.Save(fs2, Amyuni.PDFCreator.IacFileSaveOption.acFileSaveAll);
                //PdfDoc.Save(fs2);
                fs2.Close();
                file1.Dispose();
                //System.IO.File.Delete(filenameandpath); 

            }
            catch (Exception exc)
            {

                throw (exc);
            }

        }

        #endregion

        #endregion

        #region Private Functions

        #region Get EMail id

        /// <summary>
        /// Gets the e-mail id
        /// </summary>
        /// <param name="p_sConnectionString">Security Connection String</param>
        /// <param name="p_sLogInName">Log-in name of the user</param>
        /// <returns>E-mail id of the user</returns>
        private string GetEmailId(string p_sConnectionString, string p_sLogInName)
        {
            string sRetVal = "";
            string sSQL = "";
            DataSet objDS = null;

            try
            {
                sSQL = "SELECT USER_TABLE.EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.LOGIN_NAME='" + p_sLogInName + "'";
                objDS = DbFactory.GetDataSet(p_sConnectionString, sSQL, m_iClientId);
                sRetVal = Conversion.ConvertObjToStr(objDS.Tables[0].Rows[0]["EMAIL_ADDR"]);

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManagement.GetEmailId.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objDS != null) objDS.Dispose();
            }
            return (sRetVal);
        }

        #endregion

        #region Get Document Name

        /// Name			: GetDocumentName
        /// Author			: Mohit Yadav
        /// Date Created	: 06-June-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Getting the Document Name corresponding the Id as the input parameter
        /// </summary>
        /// <param name="p_sDocumentId">
        ///		Specified DocumentId.
        ///	</param>
        /// <returns>0 - Success</returns> 										
        private string GetDocumentName(string p_sDocumentId)
        {
            string sRetVal = "";
            string sSQL = "";
            DataSet objDS = null;

            try
            {
                sSQL = "SELECT DOCUMENT_ID,FOLDER_ID,DOCUMENT_FILENAME FROM DOCUMENT WHERE " +
                    " DOCUMENT_ID = '" + p_sDocumentId + "' AND USER_ID='" + this.UserLoginName + "'";
                objDS = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);
                sRetVal = Conversion.ConvertObjToStr(objDS.Tables[0].Rows[0]["DOCUMENT_FILENAME"]);

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManagement.GetEmailId.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objDS != null) objDS.Dispose();
            }
            return (sRetVal);
        }

        #endregion

        /// Name			: CreateRecycleBin
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Creates Recycle Bin folder record for the current user.
        /// Uses UserLoginName and ConnectionString property values.
        /// </summary>
        /// <returns>0 - Success</returns>
        private int CreateRecycleBin()
        {
            int iReturnValue = 0;

            DbWriter objDbWriter = null;

            try
            {
                objDbWriter = DbFactory.GetDbWriter(this.ConnectionString);

                objDbWriter.Tables.Add("DOC_FOLDERS");
                objDbWriter.Fields.Add("FOLDER_ID", -1);
                objDbWriter.Fields.Add("PARENT_ID", 0);
                objDbWriter.Fields.Add("FOLDER_NAME", RECYCLE_BIN_NAME);
                objDbWriter.Fields.Add("FOLDER_PATH", DBNull.Value);
                objDbWriter.Fields.Add("USER_ID", this.UserLoginName);

                objDbWriter.Execute();


            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.CreateRecycleBin.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDbWriter = null;
            }

            return iReturnValue;
        }

        /// Name			: CheckRecycleBinForUser
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Checks Recycle Bin folder record for the current user.
        /// Uses UserLoginName and ConnectionString property values.
        /// </summary>
        /// <param name="p_bRecycleBinCreated">
        ///		Out parameter. Returns true if Recycle Bin is already
        ///		created for the user, else false.
        /// </param>
        /// <returns>0 - Success</returns>
        private int CheckRecycleBinForUser(out bool p_bRecycleBinCreated)
        {
            int iReturnValue = 0;

            DbReader objDbReader = null;

            string sSQL = null;

            try
            {
                sSQL = "SELECT FOLDER_ID "
                    + " FROM DOC_FOLDERS "
                    + " WHERE FOLDER_ID=-1 "
                    + " AND USER_ID='" + this.UserLoginName + "'";

                objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                if (objDbReader.Read())
                {
                    // Recycle Bin has already been created for the user
                    p_bRecycleBinCreated = true;
                }
                else
                {
                    p_bRecycleBinCreated = false;
                }

            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.CheckRecycleBinForUser.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
            }

            return iReturnValue;
        }

        /// Name			: RaiseInitializationException
        /// Author			: Aditya Babbar
        /// Date Created	: 28-Jan-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Raises InitializationException if required properties
        /// for DocumentManager object are not set.
        /// </summary>
        private void RaiseInitializationException()
        {
            if (this.ConnectionString.Length == 0)
            {
                throw new InitializationException(Globalization.GetString("DocumentManager.ConnectionStringNotSet", m_iClientId));
            }

            if (this.UserLoginName.Length == 0)
            {
                throw new InitializationException(Globalization.GetString("DocumentManager.UserLoginNameNotSet", m_iClientId));
            }
        }

        /// Name			: RemoveButton
        /// Author			: Umesh
        /// Date Created	: 05-Jan-2007
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Remove Buttons on basis of Customization-> Custom setting
        /// <param name="p_objSearchDocument">
        /// Input Xml from which the setting of buttons is decided.
        /// </param>
        /// <param name="p_objRemoveDocument">
        /// Input Xml in which the attibutes corresponding to buttons are set
        /// </param>
        /// </summary>
        private void RemoveButton(XmlDocument p_objSearchDocument, XmlDocument p_objRemoveDocument,
            string p_sNodeSearchName, string p_sAttrRemoveName)
        {
            XmlNode objSearch = null;
            try
            {
                objSearch = p_objSearchDocument.SelectSingleNode(p_sNodeSearchName);
                if (objSearch != null)
                {

                    if (objSearch.InnerText != "-1")
                    {

                        XmlNode objAttr = p_objRemoveDocument.SelectSingleNode(p_sAttrRemoveName);
                        objAttr.InnerText = "0";
                    }

                }
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXmlException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.RemoveButton.error", m_iClientId), p_objXmlException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.RemoveButton.error", m_iClientId), p_objException);
            }
            finally
            {
                objSearch = null;

            }
        }

        #region SplitForStringPattern(string p_sString,string p_sPattern,ref ArrayList p_arrlstSplit)
        /// Name			: SplitForStringPattern
        /// Author			: Mohit Yadav
        /// Date Created	: 10-May-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Split a string based on some string pattern.
        /// </summary>
        /// <param name="p_sString">String to be splitted.</param>
        /// <param name="p_sPattern">Pattern with in the string.</param>
        /// <param name="p_arrlstSplit">Array containing the splitted sub strings.</param>
        /// <returns>A boolean values that represents the success/failure.</returns>

        private bool SplitForStringPattern(string p_sString, string p_sPattern, ref ArrayList p_arrlstSplit)
        {
            bool bReturn = false;

            int iIndexOf = 0;

            try
            {
                iIndexOf = p_sString.IndexOf(p_sPattern);

                if (iIndexOf == -1)
                {
                    p_arrlstSplit.Add(p_sString);
                    bReturn = true;
                    return bReturn;
                }
                p_arrlstSplit.Add(p_sString.Substring(0, iIndexOf));

                string sAgain = p_sString.Substring(iIndexOf + p_sPattern.Length, p_sString.Length - (iIndexOf + p_sPattern.Length));
                SplitForStringPattern(sAgain, p_sPattern, ref p_arrlstSplit);
                bReturn = true;
            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("DocumentManagementAdaptor.SplitForStringPattern.SplitForStringPatternError", m_iClientId), p_objException);
            }

            finally
            {
            }
            return bReturn;

        }
        #endregion

        #region Check Document List Permission

        /// <summary>
        /// Check permissions for add new/move/email/delete/copy related to document list
        /// operations. 
        /// </summary>
        /// <param name="oDataElement">data XML element in the output</param>
        /// <param name="iPSID">parent security ID</param>
        private void CheckDocListPermissions(ref XmlElement oDataElement, int iPSID)
        {
            int iSecurityID = 0;
            try
            {
                oDataElement.SetAttribute("psid", iPSID.ToString());

                //Check for User Document Add New permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_CREATE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                {
                    oDataElement.SetAttribute("create_allowed", "", "1");
                    oDataElement.SetAttribute("createfolder_allowed", "", "1");
                }
                else
                {
                    oDataElement.SetAttribute("create_allowed", "", "0");
                    oDataElement.SetAttribute("createfolder_allowed", "", "0");
                }

                //Check for Attachment Add New permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_CREATE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                {
                    oDataElement.SetAttribute("att_create_allowed", "", "1");
                }
                else
                {
                    oDataElement.SetAttribute("att_create_allowed", "", "0");
                }

                //Check for User Document Delete permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_DELETE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("delete_allowed", "", "1");
                else
                    oDataElement.SetAttribute("delete_allowed", "", "0");

                //Check for Attachment Delete permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_DELETE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("att_delete_allowed", "", "1");
                else
                    oDataElement.SetAttribute("att_delete_allowed", "", "0");

                //Check for User Document Transfer permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_TRANSFER);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("transfer_allowed", "", "1");
                else
                    oDataElement.SetAttribute("transfer_allowed", "", "0");

                //Check for Attachment Transfer permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_TRANSFER);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("att_transfer_allowed", "", "1");
                else
                    oDataElement.SetAttribute("att_transfer_allowed", "", "0");

                //Check for User Document Copy permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_COPY);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("copy_allowed", "", "1");
                else
                    oDataElement.SetAttribute("copy_allowed", "", "0");

                //Check for Attachment Copy permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_COPY);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("att_copy_allowed", "", "1");
                else
                    oDataElement.SetAttribute("att_copy_allowed", "", "0");

                //Check for email permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_TRANSFER);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("email_allowed", "", "1");
                else
                    oDataElement.SetAttribute("email_allowed", "", "0");
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManagement.CheckDocListPermissions.Error", m_iClientId), p_objException);
            }
        }

        #endregion
        #region Check Document List Permission

        /// <summary>
        /// Check permissions for add new/move/email/delete/copy related to document list
        /// operations. 
        /// </summary>
        /// <param name="oDataElement">data XML element in the output</param>
        /// <param name="iPSID">parent security ID</param>
        private void CheckDocumentListPermissions(ref DocumentList oDocList, int iPSID)
        {
            int iSecurityID = 0;
            try
            {

                oDocList.Psid = iPSID.ToString();
                //Check for User Document Add New permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_CREATE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                {
                    //oDataElement.SetAttribute("create_allowed", "", "1");
                    //oDataElement.SetAttribute("createfolder_allowed", "", "1");
                    oDocList.Create_allowed = "1";
                    oDocList.Createfolder_allowed = "1";
                }
                else
                {
                    //oDataElement.SetAttribute("create_allowed", "", "0");
                    //oDataElement.SetAttribute("createfolder_allowed", "", "0");
                    oDocList.Copy_allowed = "0";
                    oDocList.Createfolder_allowed = "0";
                }

                //Check for Attachment Add New permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_CREATE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                {

                    oDocList.Att_create_allowed = "1";

                }
                else
                {
                    oDocList.Att_create_allowed = "0";
                }

                //Check for User Document Delete permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_DELETE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDocList.Delete_allowed = "1";
                else
                    oDocList.Delete_allowed = "0";

                //Check for Attachment Delete permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_DELETE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDocList.Att_delete_allowed = "1";
                else
                    oDocList.Att_delete_allowed = "0";

                //Check for User Document Transfer permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_TRANSFER);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDocList.Transfer_allowed = "1";
                else
                    oDocList.Transfer_allowed = "0";

                //Check for Attachment Transfer permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_TRANSFER);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDocList.Att_transfer_allowed = "1";
                else
                    oDocList.Att_transfer_allowed = "0";

                //Check for User Document Copy permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_COPY);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDocList.Copy_allowed = "1";
                else
                    oDocList.Copy_allowed = "0";

                //Check for Attachment Copy permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_COPY);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDocList.Att_copy_allowed = "1";
                else
                    oDocList.Att_copy_allowed = "0";

                //Check for email permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_TRANSFER);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDocList.Email_allowed = "1";
                else
                    oDocList.Email_allowed = "0";
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManagement.CheckDocListPermissions.Error", m_iClientId), p_objException);
            }
        }

        #endregion
        #region Check Document Permission
        /// <summary>
        /// Check permissions for add new/move/email/delete/copy related to a single document
        /// operations. 
        /// </summary>
        /// <param name="oDataElement">data XML element in the output</param>
        /// <param name="iPSID">parent security ID</param>
        private void CheckDocPermissions(ref XmlElement oDataElement, int iPSID)
        {
            int iSecurityID = 0;

            try
            {
                //Check for view permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_VIEW);
                // BSB 03.22.2006  As discussed with JP,MHamann and CRuiz
                // there is no functional difference between "view" and download
                // for a user.  Hence let's remove the "view" button in all cases.
                //				if( m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                //					oDataElement.SetAttribute("view_allowed","","1");
                //				else
                oDataElement.SetAttribute("view_allowed", "", "0");

                //Check for Add New permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_CREATE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("create_allowed", "", "1");
                else
                    oDataElement.SetAttribute("create_allowed", "", "0");

                //Check for edit permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_MODIFY);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("edit_allowed", "", "1");
                else
                    oDataElement.SetAttribute("edit_allowed", "", "0");

                //Check for Delete permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_DELETE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("delete_allowed", "", "1");
                else
                    oDataElement.SetAttribute("delete_allowed", "", "0");

                //Check for Transfer permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_TRANSFER);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("transfer_allowed", "", "1");
                else
                    oDataElement.SetAttribute("transfer_allowed", "", "0");

                //Check for Copy permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_COPY);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("copy_allowed", "", "1");
                else
                    oDataElement.SetAttribute("copy_allowed", "", "0");

                //Check for Download permission 
                //MITS 27764 hlv 5/23/12 begin
                if (iPSID < 5000000)
                    iSecurityID = GetSecurityID(iPSID, FUNCNAME_DOWNLOAD);  
                else
                    iSecurityID = GetSecurityID(iPSID, FUNCNAME_VIEW);
                //MITS 27764 hlv 5/23/12 end

                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("download_allowed", "", "1");
                else
                    oDataElement.SetAttribute("download_allowed", "", "0");

                //Check for email permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_TRANSFER);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("email_allowed", "", "1");
                else
                    oDataElement.SetAttribute("email_allowed", "", "0");

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManagement.CheckDocPermissions.Error", m_iClientId), p_objException);
            }
        }

        private int GetSecurityID(int iPSID, string sFunctionName)
        {
            int iSecurityID = 0;
            sFunctionName = sFunctionName.ToLower();
            string sOraCaseIns = "";
            //PSARIN MITS 8563
            using (DbConnection objCon = DbFactory.GetDbConnection(this.ConnectionString))
            {
                objCon.Open();
                if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                {
                    sOraCaseIns = " UPPER(GLOSSARY.SYSTEM_TABLE_NAME) ";
                }
                else
                {
                    sOraCaseIns = "GLOSSARY.SYSTEM_TABLE_NAME";
                }
                //Nikhil Garg		Defect No: 2397		Dated: 03/10/2006
                if (iPSID == 13000) //This is for Administrative Tracking Tables
                {
                    string sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE GLOSSARY_TYPE_CODE = 468 AND " + sOraCaseIns + "='" + m_sFormName.ToUpper() + "'";
                    DbReader objReader = objCon.ExecuteReader(sSQL);
                    if (objReader.Read())
                        iPSID = 5000000 + Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId) * 20;
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                }
            }

            //MITS 27764 hlv 5/23/12 begin
            int tmpPSID = -1;
            using (DbConnection objCon = DbFactory.GetDbConnection(this.SecurityConnectionString))
            {
                objCon.Open();
                if (iPSID != USERDOC_SID)
                {
                    //Raman MITS 31579 : 2/18/2013
                    //Sometimes the function_name is "attachment" also at some places
                    //DbReader objReader = objCon.ExecuteReader(" select FUNC_ID from FUNCTION_LIST where PARENT_ID = " + iPSID.ToString() + " and UPPER(FUNCTION_NAME) = 'ATTACHMENTS' ");
                    DbReader objReader = objCon.ExecuteReader(" select FUNC_ID from FUNCTION_LIST where PARENT_ID = " + iPSID.ToString() + " and UPPER(FUNCTION_NAME) like 'ATTACHMENT%' ");

                    if (objReader.Read())
                        tmpPSID = Convert.ToInt32(objReader.GetValue(0));

                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                }
                objCon.Close();
            }
            //MITS 27764 hlv 5/23/12 end

            //PSARIN MITS 8563
            //If it's from Attachments
            if (iPSID != USERDOC_SID)
            {
                switch (sFunctionName)
                {
                    case FUNCNAME_CREATE:
                        //iSecurityID = iPSID + ATTDOC_ATTACHTO_SID;
                        iSecurityID = (iPSID < 5000000? tmpPSID * 100 + ATTDOC_ADDNEW_SID_EX : iPSID + ATTDOC_ATTACHTO_SID); //MITS 27764 hlv 5/23/12
                        break;
                    case FUNCNAME_DELETE:
                        //iSecurityID = iPSID + ATTDOC_DELETE_SID;
                        iSecurityID = (iPSID < 5000000? tmpPSID * 100 + ATTDOC_DELETE_SID_EX : iPSID + ATTDOC_DELETE_SID); //MITS 27764 hlv 5/23/12
                        break;
                    case FUNCNAME_TRANSFER:
                        //iSecurityID = iPSID + ATTDOC_ATTACHTO_SID;
                        iSecurityID = (iPSID < 5000000? tmpPSID * 100 + ATTDOC_EMAIL_SID_EX : iPSID + ATTDOC_ATTACHTO_SID); //MITS 27764 hlv 5/23/12
                        break;
                    case FUNCNAME_COPY:
                        iSecurityID = iPSID + ATTDOC_ATTACHTO_SID;
                        break;
                    //MITS 27764 hlv 5/23/12 begin
                    case FUNCNAME_MODIFY:
                        iSecurityID = (iPSID < 5000000? tmpPSID * 100 + ATTDOC_EDIT_SID_EX : iPSID + ATTDOC_ATTACHTO_SID);
                        break;
                    case FUNCNAME_DOWNLOAD:
                        iSecurityID = tmpPSID * 100 + ATTDOC_DOWNLOAD_SID_EX;
                        break;
                    //MITS 27764 hlv 5/23/12 end
                    default:
                        iSecurityID = iPSID;
                        break;
                }
            }
            else if (iPSID == USERDOC_SID)
            {
                switch (sFunctionName)
                {
                    case FUNCNAME_COPY:
                        iSecurityID = USERDOC_COPY_SID;
                        break;
                    case FUNCNAME_CREATE:
                        iSecurityID = USERDOC_CREATE_SID;
                        break;
                    case FUNCNAME_DELETE:
                        iSecurityID = USERDOC_DELETE_SID;
                        break;
                    case FUNCNAME_DETATCH:
                        iSecurityID = USERDOC_DETATCH_SID;
                        break;
                    case FUNCNAME_EXPORT:
                        iSecurityID = USERDOC_EXPORT_SID;
                        break;
                    case FUNCNAME_MODIFY:
                        iSecurityID = USERDOC_MODIFY_SID;
                        break;
                    case FUNCNAME_PRINT:
                        iSecurityID = USERDOC_PRINT_SID;
                        break;
                    case FUNCNAME_TRANSFER:
                        iSecurityID = USERDOC_TRANSFER_SID;
                        break;
                    case FUNCNAME_DOWNLOAD:
                        iSecurityID = USERDOC_VIEW_SID;  //MITS 27764 hlv 5/23/12
                        break;
                    case FUNCNAME_VIEW:
                        iSecurityID = USERDOC_VIEW_SID;
                        break;
                    case FUNCNAME_VIEWPLAY:
                        iSecurityID = USERDOC_VIEWPLAY_SID;
                        break;
                }
            }

            return iSecurityID;
        }

        #endregion

        /// <summary>
        /// For FileSystemStorage, the files are not necessarily stored in the
        /// folder specified in the DocPath when "Link Document" feature is used
        /// in RMWorld. The following three kinds of file path will handled:
        /// 1. begin with "\\computerName" or "DirveLetter:", use this as actual file path
        /// 2. Not null and not case 1, treat it as subfolder of DocPath
        /// 3. If null, use DocPath
        /// MITS 10062 Add support for Linked Documents from RMWorld
        /// </summary>
        /// <param name="sFilePath">Value from Document.Document_FilePath</param>
        /// <returns></returns>
        private string GetActualDocFilePath(string sFilePath)
        {
            string sDocFilePath = this.DestinationStoragePath;

            if (sFilePath == null)
                return sDocFilePath;

            if (this.DocumentStorageType == StorageType.DatabaseStorage || sFilePath.Length < 3)
                return sDocFilePath;

            //For FileSystemStorage, need to check for file path in the DB
            if ((sFilePath.Substring(0, 2) == @"\\") ||
                (sFilePath.Substring(1, 1) == ":"))
            {
                sDocFilePath = sFilePath;
            }
            else if (sFilePath.Length > 0)
                sDocFilePath += @"\" + sFilePath;

            return sDocFilePath;
        }

        /// <summary>
        /// Added by Nitin for large document upload
        /// </summary>
        /// <param name="p_sDocumentIds"></param>
        /// <returns></returns>
        private StringBuilder ValidateFileBeforeDelete(ref string p_sDocumentIds)
        {
            string sSQL = string.Empty;
            StringBuilder sbFileInProcessErrMsg = null;
            DbReader objReader = null;
            int iInProcessFilesCount = 0;

            try
            {
                //check uploading status of file ,if it is in process then donot delete the file raise exception
                sSQL = "SELECT DOCUMENT_ID,DOCUMENT_FILENAME,DOCUMENT_NAME "
                    + " FROM DOCUMENT "
                    + " WHERE (DOCUMENT_ID IN (" + p_sDocumentIds + ") AND UPLOAD_STATUS = 1) ";

                objReader = DbFactory.ExecuteReader(this.ConnectionString, sSQL);

                sbFileInProcessErrMsg = new StringBuilder();
                sbFileInProcessErrMsg.Append("Files ");

                while (objReader.Read())
                {
                    if (p_sDocumentIds.Contains("," + objReader.GetValue("DOCUMENT_ID").ToString()))
                    {
                        p_sDocumentIds = p_sDocumentIds.Replace("," + objReader.GetValue("DOCUMENT_ID").ToString(), "");
                    }
                    else if (p_sDocumentIds.Contains(objReader.GetValue("DOCUMENT_ID").ToString() + ","))
                    {
                        p_sDocumentIds = p_sDocumentIds.Replace(objReader.GetValue("DOCUMENT_ID").ToString() + ",", "");
                    }
                    else
                    {
                        p_sDocumentIds = p_sDocumentIds.Replace(objReader.GetValue("DOCUMENT_ID").ToString(), "");
                    }
                    sbFileInProcessErrMsg.Append(objReader.GetValue("DOCUMENT_NAME").ToString() + "(" + objReader.GetValue("DOCUMENT_FILENAME").ToString() + "), ");
                    iInProcessFilesCount += 1;
                }

                if (iInProcessFilesCount == 0)
                {
                    return null;
                }
                
                sbFileInProcessErrMsg = sbFileInProcessErrMsg.Remove(sbFileInProcessErrMsg.ToString().LastIndexOf(','), 1);
                sbFileInProcessErrMsg.Append(" cant not be Deleted,their Uploading is in Process");
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.Delete.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader = null;
                }
            }

            return sbFileInProcessErrMsg;
        }
        
        
        
        /// <summary>
        /// Added by Nitin to set upload status to error i,e 3
        /// </summary>
        /// <param name="p_objDocument"></param>
        /// <param name="iPSId"></param>
        private void UpdateAsyncEntryOnException(Document p_objDocument)
        {
            try
            {
                p_objDocument.UploadStatus = 3;
                p_objDocument.Update();
            }
            catch (Exception)
            {
                
            }
        }
        /// <summary>
        /// Sets the file size of document
        /// </summary>
        /// <param name="objDocument">DocumentType Object</param>
        private void SetDocumentFileSize(ref DocumentType objDocument)
        {
            long lFileSize = 0;
            double dSizeInMB = 0;
            try
            {
                lFileSize = GetFileSize(objDocument.FileName);//smishra25:Adding filesize
                dSizeInMB = (lFileSize / CONVERSION_FACTOR) / CONVERSION_FACTOR;
                objDocument.FileSizeInMB = Math.Round(dSizeInMB, 4, MidpointRounding.AwayFromZero);
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.GetDocuments.Exception", m_iClientId), p_objException);
            }
        }
        
        #endregion

		        /// <summary>
        /// Gets the number records displayed per page 
        /// Deb : MITS 25598
        /// </summary>
        /// <returns></returns>
        public int GetRecordsPerPage()
        {
            StringBuilder sbSQL = null;
            int iReturn = 0;
            DbReader objReader = null;

            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE");
                sbSQL.Append(" WHERE PARM_NAME = 'DOCLIST_USER_LIMIT' ");
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader.Read())
                {
                    iReturn = Conversion.ConvertObjToInt(objReader.GetValue("STR_PARM_VALUE"), m_iClientId);
                    if (iReturn < 10)
                        iReturn = 10;//Record per Page should greater than equal to 10
                }
                else
                {
                    iReturn = 10;//Record per Page should greater than equal to 10
                }
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                }
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                sbSQL = null;
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objReader = null;
            }
            return iReturn;
        }

        /// <summary>
        /// Removes en-dash and em-dash from the string supplied. Also corrects the spring to prevent sql injection
        /// Added for mits 26371 - 12/21/2011
        /// </summary>
        /// <param name="sClaimNumber">String to standardize</param>
        /// <returns></returns>
        public static string StandardizeClaimNumber(string sClaimNumber)
        {
            StringBuilder sbStandardizedString = new StringBuilder(sClaimNumber.Length);
            foreach (char c in sClaimNumber)
            {
                switch (c)
                {
                    case '\u2013':
                    case '\u2014':
                        sbStandardizedString.Append('-');
                        break;
                    case '\'': sbStandardizedString.Append("\'\'");
                        break;
                    default:
                        sbStandardizedString.Append(c);
                        break;
                }
            }
            return sbStandardizedString.ToString();
        }
        /// <summary>
        /// Added by Amitosh for mits 27409
        /// </summary>
        /// <param name="p_iUnitRowId"></param>
        /// <returns></returns>
        public int GetBaseSecurityIDForUnit(string p_sUnitRowId)
        {
            string sSQL = "SELECT CODES_TEXT.SHORT_CODE FROM CODES_TEXT,CLAIM,UNIT_X_CLAIM WHERE UNIT_X_CLAIM.CLAIM_ID = CLAIM.CLAIM_ID AND CLAIM.LINE_OF_BUS_CODE = CODES_TEXT.CODE_ID and UNIT_X_CLAIM.UNIT_ROW_ID = " + p_sUnitRowId;
            int iSecurityId = 0;
            
            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {
                if(objReader.Read())
                {
                    switch (Conversion.ConvertObjToStr(objReader.GetValue(0)))
                    {
                        case "VA":
                            iSecurityId = 8100;//Security Id for child of  Vehicle Accident
                            break;
                        case "GC":
                            iSecurityId = 56000;//Security Id for child of  General Claim
                            break;
                    }
                }
            }
                return iSecurityId;
        }

    }
}
