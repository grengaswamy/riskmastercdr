﻿using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Threading;

namespace Riskmaster.Application.DocumentManagement
{
    ///************************************************************** 
    ///* $File		: Document.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 29-Oct-2004 
    ///* $Author	: Aditya Babbar
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************	
    /// Amendment History
    /// ************************************************************
    /// Date Amended	*	Author		*	Amendment
    ///	11/17/2006		*	Manoj		*	LSS Documents changes
    ///					*				*	
    ///************************************************************			
    /// <summary>	
    /// Encapsulates Document information and provides methods to retrieve
    ///	and update these details.
    /// </summary>
    public class Document
    {
        #region Constants

        /// <summary>
        /// FolderId of Recycle Bin for all users
        /// </summary>
        private const long RECYCLE_BIN_ID = -1;

        /// <summary>
        /// Xml tag name for data
        /// </summary>
        private const string DATA_XMLTAGNAME = "data";

        /// <summary>
        /// Xml tag name for Document
        /// </summary>
        private const string DOCUMENT_XMLTAGNAME = "Document";

        /// <summary>
        /// Xml tag name for DocumentId
        /// </summary>
        private const string DOCUMENTID_XMLTAGNAME = "DocumentId";

        /// <summary>
        /// Xml tag name for FolderId
        /// </summary>
        private const string FOLDERID_XMLTAGNAME = "FolderId";

        /// <summary>
        /// Xml tag name for CreateDate
        /// </summary>
        private const string CREATEDATE_XMLTAGNAME = "CreateDate";

        /// <summary>
        /// Xml tag name for Category
        /// </summary>
        private const string CATEGORY_XMLTAGNAME = "Category";

        /// <summary>
        /// Xml tag name for Category Code
        /// </summary>
        private const string CATEGORY_CODE_XMLTAGNAME = "Category-cid";

        /// <summary>
        /// Xml tag name for Name
        /// </summary>
        private const string NAME_XMLTAGNAME = "Name";

        /// <summary>
        /// Xml tag name for Class
        /// </summary>
        private const string CLASS_XMLTAGNAME = "Class";

        /// <summary>
        /// Xml tag name for Class Code
        /// </summary>
        private const string CLASS_CODE_XMLTAGNAME = "Class-cid";

        /// <summary>
        /// Xml tag name for Subject
        /// </summary>
        private const string SUBJECT_XMLTAGNAME = "Subject";

        /// <summary>
        /// Xml tag name for Type
        /// </summary>
        private const string TYPE_XMLTAGNAME = "Type";

        /// <summary>
        /// Xml tag name for Type Code
        /// </summary>
        private const string TYPE_CODE_XMLTAGNAME = "Type-cid";

        /// <summary>
        /// Xml tag name for Notes
        /// </summary>
        private const string NOTES_XMLTAGNAME = "Notes";

        /// <summary>
        /// Xml tag name for UserLoginName
        /// </summary>
        private const string USERLOGINNAME_XMLTAGNAME = "UserLoginName";

        /// <summary>
        /// Xml tag name for FileName
        /// </summary>
        private const string FILENAME_XMLTAGNAME = "FileName";

        /// <summary>
        /// Xml tag name for FilePath
        /// </summary>
        private const string FILEPATH_XMLTAGNAME = "FilePath";

        /// <summary>
        /// Xml tag name for Keywords
        /// </summary>
        private const string KEYWORDS_XMLTAGNAME = "Keywords";

        /// <summary>
        /// Xml tag name for AttachTable
        /// </summary>
        private const string ATTACHTABLE_XMLTAGNAME = "AttachTable";

        /// <summary>
        /// Xml tag name for AttachRecordId
        /// </summary>
        private const string ATTACHRECORDID_XMLTAGNAME = "AttachRecordId";

        
        /// <summary>
        /// Xml tag name for Upload Status , added by Nitin 
        /// </summary>
        private const string UPLOADSTATUS_XMLTAGNAME = "UploadStatus";

        /// <summary>
        /// Xml tag name for DocInternalType
        /// </summary>
        private const string DOCINTERNALTYPE_XMLTAGNAME = "DocInternalType";			//Manoj - LSS Interface

        #endregion

        #region Member Variables

        /// <summary>		
        /// DocumentId
        /// </summary>
        private long m_lDocumentId;

        /// <summary>		
        /// File Name
        /// </summary>
        private string m_sFileName;

        /// <summary>		
        /// File Path
        /// </summary>
        private string m_sFilePath;

        /// <summary>		
        /// Document Name
        /// </summary>
        private string m_sTitle;

        /// <summary>		
        /// Keywords
        /// </summary>
        private string m_sKeywords;

        /// <summary>		
        /// Notes
        /// </summary>
        private string m_sNotes;

        /// <summary>		
        /// FolderId
        /// </summary>
        private long m_lFolderId;

        /// <summary>		
        /// Create Date
        /// </summary>
        private string m_sCreateDate;

        /// <summary>		
        /// The table to which document is attached.
        /// </summary>
        private string m_sAttachTable;

        /// <summary>		
        /// The record id to which document is attached.
        /// </summary>
        private long m_lAttachRecordId;

        /// <summary>		
        /// User Login Name
        /// </summary>
        private string m_sUserLoginName;

        /// <summary>		
        /// Subject
        /// </summary>
        private string m_sSubject;

        /// <summary>		
        /// Type
        /// </summary>
        private long m_lType;

        /// <summary>		
        /// Class
        /// </summary>
        private long m_lClass;

        /// <summary>		
        /// Category
        /// </summary>
        private long m_lCategory;

        /// <summary>		
        /// Category
        /// </summary>
        private long m_lDocInternalType;				//Manoj - LSS Interface

        /// <summary>		
        /// Database Connection String
        /// </summary>
        private string m_sConnectionString;

        /// <summary>
        /// Async status of Uploading document
        /// </summary>
        private int m_iUploadStatus = 0;

        /// <summary>		
        /// Indicates whether properties for Document object are loaded.
        /// </summary>
        private bool m_bDocumentPropertiesLoaded;

        // akaushik5 Added for MITS 30748 Starts
        /// <summary>
        /// Indicates document update is in progress.
        /// </summary>
        public static bool IsUpdatingDocument;
        // akaushik5 Added for MITS 30748 Ends

        /// <summary>		
        /// ClientId
        /// </summary>
        private int m_iClientId;
        #endregion

        #region Properties

        /* All string properties are set to string.Empty if a
		 * null value is assigned to them. This ensures a more robust
		 * component free of NullValueExceptions generated inadvertantly
		 * if the client sets a string property to null. 
		 * */

        /// <summary>
        /// DocumentId
        /// </summary>
        public long DocumentId
        {
            get
            {
                return m_lDocumentId;
            }
            set
            {
                m_lDocumentId = value;
            }
        }

        /// <summary>
        /// File Name
        /// </summary>
        public string FileName
        {
            get
            {
                return m_sFileName;
            }
            set
            {
                if (value != null)
                {
                    m_sFileName = value;
                }
                else
                {
                    m_sFileName = string.Empty;
                }
            }
        }

        /// <summary>
        /// File Path
        /// </summary>
        public string FilePath
        {
            get
            {
                return m_sFilePath;
            }
            set
            {
                if (value != null)
                {
                    m_sFilePath = value;
                }
                else
                {
                    m_sFilePath = string.Empty;
                }
            }
        }

        /// <summary>
        /// Document Name
        /// </summary>
        public string Title
        {
            get
            {
                return m_sTitle;
            }
            set
            {
                if (value != null)
                {
                    m_sTitle = value;
                    //rsushilaggar MITS 17281 Date 11/02/2010
                    if (m_sTitle.Length > 100)
                        m_sTitle = m_sTitle.Substring(0, 100);
                }
                else
                {
                    m_sTitle = string.Empty;
                }
            }
        }

        /// <summary>
        /// Keywords
        /// </summary>
        public string Keywords
        {
            get
            {
                return m_sKeywords;
            }
            set
            {
                if (value != null)
                {
                    m_sKeywords = value;
                    if (m_sKeywords.Length > 200)
                        m_sKeywords = m_sKeywords.Substring(0, 200);
                }
                else
                {
                    m_sKeywords = string.Empty;
                }
            }
        }

        /// <summary>
        /// Notes
        /// </summary>
        public string Notes
        {
            get
            {
                return m_sNotes;
            }
            set
            {
                if (value != null)
                {
                    m_sNotes = value;
                    if (m_sNotes.Length > 200)
                        m_sNotes = m_sNotes.Substring(0, 200);
                }
                else
                {
                    m_sNotes = string.Empty;
                }
            }
        }

        /// <summary>
        /// Folder Id
        /// </summary>
        public long FolderId
        {
            get
            {
                return m_lFolderId;
            }
            set
            {
                m_lFolderId = value;
            }
        }


        /// <summary>
        /// Create Date
        /// </summary>
        public string CreateDate
        {
            get
            {
                return m_sCreateDate;
            }
            set
            {
                if (value != null)
                {
                    m_sCreateDate = value;
                }
                else
                {
                    m_sCreateDate = string.Empty;
                }
            }
        }

        /// <summary>
        /// The table to which document is attached
        /// </summary>
        public string AttachTable
        {
            get
            {
                return m_sAttachTable;
            }
            set
            {
                if (value != null)
                {
                    m_sAttachTable = value;
                }
                else
                {
                    m_sAttachTable = string.Empty;
                }
            }
        }

        /// <summary>
        /// Record Id to which document is attached
        /// </summary>
        public long AttachRecordId
        {
            get
            {
                return m_lAttachRecordId;
            }
            set
            {
                m_lAttachRecordId = value;
            }
        }

        /// <summary>
        /// User Login Name
        /// </summary>
        public string UserLoginName
        {
            get
            {
                return m_sUserLoginName;
            }
            set
            {
                if (value != null)
                {
                    m_sUserLoginName = value;
                }
                else
                {
                    m_sUserLoginName = string.Empty;
                }
            }
        }

        /// <summary>
        /// Subject
        /// </summary>
        public string Subject
        {
            get
            {
                return m_sSubject;
            }
            set
            {
                if (value != null)
                {
                    m_sSubject = value;
                    if (m_sSubject.Length > 50)
                        m_sSubject = m_sSubject.Substring(0, 50);
                }
                else
                {
                    m_sSubject = string.Empty;
                }
            }
        }

        /// <summary>
        /// Type
        /// </summary>
        public long Type
        {
            get
            {
                return m_lType;
            }
            set
            {
                m_lType = value;
            }
        }

        /// <summary>
        /// Class
        /// </summary>
        public long Class
        {
            get
            {
                return m_lClass;
            }
            set
            {
                m_lClass = value;
            }
        }

        /// <summary>
        /// Category
        /// </summary>
        public long Category
        {
            get
            {
                return m_lCategory;
            }
            set
            {
                m_lCategory = value;
            }
        }

        //Manoj - LSS Interface
        /// <summary>
        /// Doc Internal Type
        /// </summary>
        public long DocInternalType
        {
            get
            {
                return m_lDocInternalType;
            }
            set
            {
                m_lDocInternalType = value;
            }
        }

        /// <summary>
        /// Database Connection String
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return m_sConnectionString;
            }
            set
            {
                if (value != null)
                {
                    m_sConnectionString = value;
                }
                else
                {
                    m_sConnectionString = string.Empty;
                }
            }
        }

        /// <summary>
        /// Upload Status of large document
        /// </summary>
        public int UploadStatus
        {
            get
            {
                return m_iUploadStatus;
            }
            set
            {
                m_iUploadStatus = value;
            }
        }


        #endregion

        #region Constructor

        /// Name			: Document
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************			
        /// <summary>
        /// Constructor
        /// </summary>
        public Document(int p_iClientId)
        {
            this.m_sFileName = string.Empty;
            this.m_sFilePath = string.Empty;
            this.m_sTitle = string.Empty;
            this.m_sKeywords = string.Empty;
            this.m_sNotes = string.Empty;
            this.m_lFolderId = 0;
            this.m_lDocumentId = 0;
            this.m_sCreateDate = string.Empty;
            this.m_sAttachTable = string.Empty;
            this.m_lAttachRecordId = 0;
            this.m_sUserLoginName = string.Empty;
            this.m_sSubject = string.Empty;
            this.m_lType = 0;
            this.m_lClass = 0;
            this.m_lCategory = 0;
            this.m_lDocInternalType = 0;		//Manoj - LSS Interface
            this.m_sConnectionString = string.Empty;
            this.m_bDocumentPropertiesLoaded = false;
            this.m_iClientId = p_iClientId;
        }

        #endregion

        #region Public Functions

        /// Name			: Load
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************	
        /// <summary>
        /// Loads the properties of the Document from the database.
        /// Uses DocumentId, UserLoginName and ConnectionString 
        /// property values.
        /// </summary>
        /// <returns>0 - Success</returns>
        public int Load()
        {
            int iReturnValue = 0;

            DbReader objDbReader = null;
            string sSQL = string.Empty;

            try
            {
                RaiseInitializationException();

                this.m_bDocumentPropertiesLoaded = false;

                sSQL = "SELECT DOCUMENT_NAME,DOCUMENT_FILENAME,DOCUMENT_FILEPATH,"
                    + " KEYWORDS,CREATE_DATE,NOTES,FOLDER_ID,USER_ID, DOCUMENT_CLASS, "
                    + " DOCUMENT_CATEGORY, DOCUMENT_SUBJECT, DOCUMENT_TYPE, DOC_INTERNAL_TYPE, UPLOAD_STATUS "
                    + "	FROM DOCUMENT "
                    + " WHERE DOCUMENT_ID=" + this.DocumentId.ToString();
                // JP 03.03.2006   TR 2409    This breaks attachments which do not care who created the doc.  Plus, this security is handled at higher layers so this is not necessary.   + " AND USER_ID='" + this.UserLoginName + "'"; 

                objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                if (objDbReader.Read())
                {
                    this.Title = objDbReader.GetString("DOCUMENT_NAME");
                    this.Keywords = objDbReader.GetString("KEYWORDS");
                    this.CreateDate = objDbReader.GetString("CREATE_DATE");
                    this.FileName = objDbReader.GetString("DOCUMENT_FILENAME");
                    this.FilePath = objDbReader.GetString("DOCUMENT_FILEPATH");
                    this.Notes = objDbReader.GetString("NOTES");
                    this.UserLoginName = objDbReader.GetString("USER_ID");
                    this.FolderId = Convert.ToInt64(objDbReader.GetValue("FOLDER_ID"));
                    this.Class = Convert.ToInt64(objDbReader.GetValue("DOCUMENT_CLASS"));
                    this.Type = Convert.ToInt64(objDbReader.GetValue("DOCUMENT_TYPE"));
                    this.Subject = objDbReader.GetString("DOCUMENT_SUBJECT");
                    this.Category = Convert.ToInt64(objDbReader.GetValue("DOCUMENT_CATEGORY"));
                    //Added by Nitin for Uploading status , will be use in Large document upload
                    if (objDbReader.GetValue("UPLOAD_STATUS") != DBNull.Value)
                        this.UploadStatus = (int)objDbReader.GetValue("UPLOAD_STATUS");

                    //Manoj - LSS Interface
                    this.DocInternalType = Convert.ToInt64(objDbReader.GetValue("DOC_INTERNAL_TYPE"));

                    this.m_bDocumentPropertiesLoaded = true;
                }

                objDbReader.Close();
                objDbReader = null;

                if (FolderId == 0)
                {
                    sSQL = " SELECT RECORD_ID, TABLE_NAME "
                        + " FROM DOCUMENT_ATTACH "
                        + " WHERE DOCUMENT_ID=" + this.DocumentId.ToString();

                    objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);

                    if (objDbReader.Read())
                    {
                        this.AttachTable = objDbReader.GetString("TABLE_NAME");
                        this.AttachRecordId = Convert.ToInt64(objDbReader.GetValue("RECORD_ID"));
                    }
                }
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Document.Load.Exception",m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }

            }

            return iReturnValue;
        }

        /// Name			: Update
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Updates the properties of the Document record in the database.
        /// Creates a new document record in the database, 
        /// if it does not exist
        /// </summary>
        /// <returns>0 - Success</returns>
        public int Update()
        {
            int iReturnValue = 0;

            DbConnection objDbConnection = null;
            DbTransaction objDbTransaction = null;
            DbWriter objDbWriter = null;

            long lNewDocumentId = 0;

            try
            {
                RaiseInitializationException();

                if (this.AttachTable.Length > 0)
                {
                    // A deleted document cannot be attached
                    if (this.FolderId == RECYCLE_BIN_ID)
                    {
                        this.AttachTable = string.Empty;
                        this.AttachRecordId = 0;
                    }
                    else
                    {
                        // An attached document cannot be inside a folder
                        this.FolderId = 0;
                    }
                }
                objDbConnection = DbFactory.GetDbConnection(this.ConnectionString);
                objDbConnection.Open();
                objDbTransaction = objDbConnection.BeginTransaction();

                objDbWriter = DbFactory.GetDbWriter(objDbConnection);


                objDbWriter.Tables.Add("DOCUMENT");
                objDbWriter.Fields.Add("DOCUMENT_NAME", this.Title);
                objDbWriter.Fields.Add("KEYWORDS", this.Keywords);
                objDbWriter.Fields.Add("DOCUMENT_FILENAME", this.FileName);
                objDbWriter.Fields.Add("DOCUMENT_FILEPATH", this.FilePath);
                objDbWriter.Fields.Add("NOTES", this.Notes);
                objDbWriter.Fields.Add("FOLDER_ID", this.FolderId);
                objDbWriter.Fields.Add("USER_ID", this.UserLoginName);
                objDbWriter.Fields.Add("DOCUMENT_CATEGORY", this.Category);
                objDbWriter.Fields.Add("DOCUMENT_CLASS", this.Class);
                objDbWriter.Fields.Add("DOCUMENT_SUBJECT", this.Subject);
                objDbWriter.Fields.Add("DOCUMENT_TYPE", this.Type);
                objDbWriter.Fields.Add("UPLOAD_STATUS", this.UploadStatus);
                //pmittal5 Mits 13646 12/08/08
                objDbWriter.Fields.Add("MCM_TRANSFER_STATUS", 0);

                if (this.DocumentId != 0)
                {
                    // Update existing record				
                    objDbWriter.Where.Add(" DOCUMENT_ID=" + this.DocumentId.ToString());
                }
                else
                {
                    // Insert new record
                    lNewDocumentId = Utilities.GetNextUID(this.ConnectionString,
                                                            "DOCUMENT", m_iClientId);

                    objDbWriter.Fields.Add("DOCUMENT_ID", lNewDocumentId);
                   // objDbWriter.Fields.Add("CREATE_DATE", DateTime.Now.ToString("yyyyMMddhhmmss"));
                    objDbWriter.Fields.Add("CREATE_DATE", DateTime.Now.ToString("yyyyMMddHHmmss"));
                    objDbWriter.Fields.Add("ARCHIVE_LEVEL", 0);
                    objDbWriter.Fields.Add("SECURITY_LEVEL", 0);
                    objDbWriter.Fields.Add("DOC_INTERNAL_TYPE", 2);
                    objDbWriter.Fields.Add("DOCUMENT_CREAT_APP", DBNull.Value);
                    objDbWriter.Fields.Add("DOCUMENT_EXPDTTM", 0);
                    objDbWriter.Fields.Add("DOCUMENT_ACTIVE", 0);

                }

                // akaushik5 Added for MITS 30748 Starts
                Random random = new Random();
                for (int i = 0; i <= 10; i++)
                {
                    if (DocumentManager.IsReadingDocument)
                    {
                        Thread.Sleep(random.Next(100, (i + 1) * 100));
                    }
                    else
                    {
                        break;
                    }
                }

                IsUpdatingDocument = true;
                // akaushik5 Added for MITS 30748 Ends
                objDbWriter.Execute(objDbTransaction);

                objDbWriter.Reset(true);

                if (this.AttachTable.Length > 0)
                {
                    objDbWriter.Tables.Add("DOCUMENT_ATTACH");
                    objDbWriter.Fields.Add("TABLE_NAME", this.AttachTable);
                    //objDbWriter.Fields.Add("RECORD_ID", this.AttachRecordId);   
                    objDbWriter.Fields.Add("RECORD_ID", (long)this.AttachRecordId);   //pmittal5 Mits 13767 01/13/09 

                    if (lNewDocumentId != 0)
                    {
                        // Insert new record
                        objDbWriter.Fields.Add("DOCUMENT_ID", lNewDocumentId);
                    }
                    else
                    {
                        // Update existing record
                        objDbWriter.Where.Add(" DOCUMENT_ID=" + this.DocumentId.ToString());
                    }

                    objDbWriter.Execute(objDbTransaction);
                }

                if (lNewDocumentId != 0)
                {
                    this.DocumentId = lNewDocumentId;
                }

                objDbTransaction.Commit();
                // akaushik5 Added for MITS 30748 Starts
                IsUpdatingDocument = false;
                // akaushik5 Added for MITS 30748 Ends

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }

                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }
                throw new RMAppException(Globalization.GetString("Document.Update.Exception",m_iClientId), p_objException);
            }
            finally
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Dispose();
                    objDbTransaction = null;
                }

                objDbWriter = null;

                if (objDbConnection != null)
                {
                    objDbConnection.Close();
                    objDbConnection.Dispose();
                }

                // akaushik5 Added for MITS 30748 Starts
                IsUpdatingDocument = false;
                // akaushik5 Added for MITS 30748 Ends
            }

            return iReturnValue;
        }

        /// Name			: Load
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment           *    Author
        ///	21-Jan-2008	    *	Added Conditions	*   Arnab Mukherjee
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Loads the properties of the Document object 
        /// using the input Xml parameter
        /// </summary>
        /// <param name="p_sDocumentXml"> 
        ///		Document properties to be loaded in Xml Format
        ///	</param>
        /// <returns>0 - Success</returns>
        public int Load(string p_sDocumentXml)
        {
            int iReturnValue = 0;

            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlNode objDocumentNode = null;
            XmlNode objXmlNode = null;

            try
            {
                RaiseInitializationException();

                objXmlDocument = new XmlDocument();
                objXmlDocument.LoadXml(p_sDocumentXml);

                objRootElement = objXmlDocument.DocumentElement;
                objDocumentNode = objRootElement.FirstChild;

                //MITS-10753 - Added the condition for checking the existance for the XML nodes
                if (objDocumentNode.SelectSingleNode(DOCUMENTID_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(DOCUMENTID_XMLTAGNAME);
                    this.DocumentId = Common.Conversion.ConvertStrToLong(objXmlNode.InnerText);
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(FOLDERID_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(FOLDERID_XMLTAGNAME);
                    this.FolderId = Common.Conversion.ConvertStrToLong(objXmlNode.InnerText);
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(CREATEDATE_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(CREATEDATE_XMLTAGNAME);
                    this.CreateDate = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(CATEGORY_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(CATEGORY_XMLTAGNAME);
                    this.Category = Common.Conversion.ConvertStrToLong(objXmlNode.InnerText);
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(NAME_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(NAME_XMLTAGNAME);
                    this.Title = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(CLASS_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(CLASS_XMLTAGNAME);
                    this.Class = Common.Conversion.ConvertStrToLong(objXmlNode.InnerText);
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(SUBJECT_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(SUBJECT_XMLTAGNAME);
                    this.Subject = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(TYPE_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(TYPE_XMLTAGNAME);
                    this.Type = Common.Conversion.ConvertStrToLong(objXmlNode.InnerText);
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(NOTES_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(NOTES_XMLTAGNAME);
                    this.Notes = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(USERLOGINNAME_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(USERLOGINNAME_XMLTAGNAME);
                    this.UserLoginName = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(FILENAME_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(FILENAME_XMLTAGNAME);
                    this.FileName = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(FILEPATH_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(FILEPATH_XMLTAGNAME);
                    this.FilePath = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(KEYWORDS_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(KEYWORDS_XMLTAGNAME);
                    this.Keywords = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(ATTACHTABLE_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(ATTACHTABLE_XMLTAGNAME);
                    this.AttachTable = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(ATTACHRECORDID_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(ATTACHRECORDID_XMLTAGNAME);
                    this.AttachRecordId = Common.Conversion.ConvertStrToLong(objXmlNode.InnerText);
                    objXmlNode = null;
                }
                //MITS-10753 End

                this.m_bDocumentPropertiesLoaded = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXmlException)
            {
                throw new RMAppException(Globalization.GetString("Document.Load.XmlException",m_iClientId), p_objXmlException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Document.Load.Exception",m_iClientId), p_objException);
            }
            finally
            {
                objXmlNode = null;
                objDocumentNode = null;
                objRootElement = null;
                objXmlDocument = null;
            }
            return iReturnValue;
        }
        public int Load(Riskmaster.Models.DocumentType p_sDocument)
        {
            int iReturnValue = 0;


            try
            {
                RaiseInitializationException();



                //MITS-10753 - Added the condition for checking the existance for the XML nodes
                this.DocumentId = p_sDocument.DocumentId;
                this.FolderId = Common.Conversion.ConvertStrToLong(p_sDocument.FolderId);
                this.CreateDate = p_sDocument.CreateDate;
                this.Category = Common.Conversion.ConvertStrToLong(p_sDocument.Category.Id.ToString());
                this.Title = p_sDocument.Title;
                this.Class = Common.Conversion.ConvertStrToLong(p_sDocument.Class.Id.ToString());
                this.Subject = p_sDocument.Subject;// objXmlNode.InnerText;
                this.Type = Common.Conversion.ConvertStrToLong(p_sDocument.Type.Id.ToString());
                this.Notes = p_sDocument.Notes; //objXmlNode.InnerText;
                this.FileName = p_sDocument.FileName;// objXmlNode.InnerText;
                this.FilePath = p_sDocument.FilePath;// objXmlNode.InnerText;
                this.Keywords = p_sDocument.Keywords; //objXmlNode.InnerText;
                this.AttachTable = p_sDocument.AttachTable;// objXmlNode.InnerText;
                this.AttachRecordId = Common.Conversion.ConvertStrToLong(p_sDocument.AttachRecordId.ToString());
                //MITS-10753 End

                this.m_bDocumentPropertiesLoaded = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXmlException)
            {
                throw new RMAppException(Globalization.GetString("Document.Load.XmlException",m_iClientId), p_objXmlException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Document.Load.Exception",m_iClientId), p_objException);
            }
            finally
            {

            }
            return iReturnValue;
        }
        /// Name			: Update
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Updates the properties of the Document object in the database
        /// using the input Xml parameter. A new document record is created
        /// if it does not exist.
        /// </summary>
        /// <param name="p_sDocumentXml"> 
        ///		Document properties to be updated in Xml Format
        ///	</param>
        /// <returns>0 - Success</returns>
        public int Update(string p_sDocumentXml)
        {
            int iReturnValue = 0;

            try
            {
                RaiseInitializationException();

                // Load the property values
                this.Load(p_sDocumentXml);

                // Update values in the database
                iReturnValue = this.Update();

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Document.Update.Exception", m_iClientId), p_objException);
            }
            finally
            {

            }
            return iReturnValue;
        }
        public int Update(Riskmaster.Models.DocumentType p_sDocument)
        {
            int iReturnValue = 0;

            try
            {
                RaiseInitializationException();

                // Load the property values
                this.Load(p_sDocument);

                // Update values in the database
                iReturnValue = this.Update();

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Document.Update.Exception", m_iClientId), p_objException);
            }
            finally
            {

            }
            return iReturnValue;
        }
        /// Name			: GetDocumentXml
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Get document details in Xml format from the property values.
        /// </summary>
        /// <param name="p_sDocumentXml">
        ///		Out Parameter. Returns Document details in Xml format.
        ///	</param>
        /// <returns>0 - Success</returns>		
        public int GetDocumentXml(out string p_sDocumentXml)
        {
            int iReturnValue = 0;
            LocalCache objCache = null;

            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objDocumentElement = null;
            XmlElement objXmlElement = null;

            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;

            try
            {
                RaiseInitializationException();

                objXmlDocument = new XmlDocument();
                objRootElement = objXmlDocument.CreateElement(DATA_XMLTAGNAME);

                objXmlDocument.AppendChild(objRootElement);

                objDocumentElement = objXmlDocument.CreateElement(DOCUMENT_XMLTAGNAME);
                objRootElement.AppendChild(objDocumentElement);

                if (this.m_bDocumentPropertiesLoaded == true)
                {
                    objCache = new LocalCache(this.ConnectionString, m_iClientId);

                    objXmlElement = objXmlDocument.CreateElement(DOCUMENTID_XMLTAGNAME);
                    objXmlElement.InnerText = this.DocumentId.ToString();
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(FOLDERID_XMLTAGNAME);
                    objXmlElement.InnerText = this.FolderId.ToString();
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(CREATEDATE_XMLTAGNAME);
                    objXmlElement.InnerText = this.CreateDate;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(CATEGORY_XMLTAGNAME);
                    if (this.Category == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objCache.GetCodeInfo((int)this.Category, ref sShortCode, ref sCodeDesc);
                        objXmlElement.InnerText = sShortCode + " - " + sCodeDesc;
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(CATEGORY_CODE_XMLTAGNAME);
                    if (this.Category == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objXmlElement.InnerText = this.Category.ToString();
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(NAME_XMLTAGNAME);
                    objXmlElement.InnerText = this.Title;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(CLASS_XMLTAGNAME);
                    if (this.Class == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objCache.GetCodeInfo((int)this.Class, ref sShortCode, ref sCodeDesc);
                        objXmlElement.InnerText = sShortCode + " - " + sCodeDesc;
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(CLASS_CODE_XMLTAGNAME);
                    if (this.Class == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objXmlElement.InnerText = this.Class.ToString();
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(SUBJECT_XMLTAGNAME);
                    objXmlElement.InnerText = this.Subject;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(TYPE_XMLTAGNAME);
                    if (this.Type == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objCache.GetCodeInfo((int)this.Type, ref sShortCode, ref sCodeDesc);
                        objXmlElement.InnerText = sShortCode + " - " + sCodeDesc;
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(TYPE_CODE_XMLTAGNAME);
                    if (this.Type == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objXmlElement.InnerText = this.Type.ToString();
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(NOTES_XMLTAGNAME);
                    objXmlElement.InnerText = this.Notes;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(USERLOGINNAME_XMLTAGNAME);
                    objXmlElement.InnerText = this.UserLoginName;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(FILENAME_XMLTAGNAME);
                    objXmlElement.InnerText = this.FileName;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(FILEPATH_XMLTAGNAME);
                    objXmlElement.InnerText = this.FilePath;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(KEYWORDS_XMLTAGNAME);
                    objXmlElement.InnerText = this.Keywords;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(ATTACHTABLE_XMLTAGNAME);
                    objXmlElement.InnerText = this.AttachTable;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    //Manoj - LSS Interface
                    objXmlElement = objXmlDocument.CreateElement(DOCINTERNALTYPE_XMLTAGNAME);
                    if (this.DocInternalType == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objXmlElement.InnerText = this.DocInternalType.ToString();
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(ATTACHRECORDID_XMLTAGNAME);
                    if (this.AttachRecordId == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objXmlElement.InnerText = this.AttachRecordId.ToString();
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                   //Nitin , for UploadStatus
                    objXmlElement = objXmlDocument.CreateElement(UPLOADSTATUS_XMLTAGNAME);
                    if (this.UploadStatus == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objXmlElement.InnerText = this.UploadStatus.ToString();
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;
                }

                p_sDocumentXml = objXmlDocument.OuterXml;

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXmlException)
            {
                throw new RMAppException(Globalization.GetString("Document.GetDocumentXml.XmlException", m_iClientId), p_objXmlException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Document.GetDocumentXml.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objXmlElement = null;
                objRootElement = null;
                objDocumentElement = null;
                objXmlDocument = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }
            }

            return iReturnValue;
        }

        /// Name			: CreateNewDocument
        /// Author			: Aditya Babbar
        /// Date Created	: 29-Oct-2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Create new Document record in the database.
        /// </summary>
        /// <param name="p_sDocumentXml">
        ///		Document details in xml format.
        ///	</param>
        /// <returns>0 - Success</returns>			
        public int CreateNewDocument(string p_sDocumentXml)
        {
            int iReturnValue = 0;

            try
            {
                RaiseInitializationException();

                this.Load(p_sDocumentXml);
                iReturnValue = this.Update();
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXmlException)
            {
                throw new RMAppException(Globalization.GetString("Document.CreateNewDocument.XmlException", m_iClientId), p_objXmlException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Document.CreateNewDocument.Exception", m_iClientId), p_objException);
            }
            finally
            {

            }

            return iReturnValue;
        }

        #endregion

        #region Private Functions

        /// Name			: RaiseInitializationException
        /// Author			: Aditya Babbar
        /// Date Created	: 28-Jan-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Raises InitializationException if required properties
        /// for Document object are not set.
        /// </summary>
        private void RaiseInitializationException()
        {
            if (this.ConnectionString.Length == 0)
            {
                throw new InitializationException(Globalization.GetString("Document.ConnectionStringNotSet", m_iClientId));
            }

        }

        #endregion
    }
}
