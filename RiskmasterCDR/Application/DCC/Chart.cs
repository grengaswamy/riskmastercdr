using System;
using System.Drawing;
using System.IO;
using System.Collections;
using C1.Win.C1Chart;

using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.Application.DCC
{
	/// <summary>	
	/// Author  :  Neelima Dabral
	/// Date    :  16 February 2005
	/// Purpose :  Riskmaster.Application.DCC.Chart class is used to generate a chart.	
	/// </summary>
	internal class Chart
	{

		#region Class Members			

		/// <summary>		
		/// C1Chart
		/// </summary>
		private C1Chart m_objChart=null;		

		/// <summary>		
		/// Chart Type
		/// </summary>
		private int m_iChartType=1;
        private int m_iClientId = 0;//sonali - cloud

		#endregion

		#region Constructor
		/// <summary>
		/// Constructor
		/// </summary>		
        internal Chart(int p_iClientId)
		{		
			m_objChart = new C1Chart();
			m_objChart.Reset();			
		}
		#endregion

		#region Methods

		/// <summary>
		/// This method will set the title for the chart.
		/// </summary>
		/// <param name="p_sTitle">Title of the chart</param>
		/// <param name="p_sFontName">Font name of the chart title</param>
		/// <param name="p_fFontSize">Font size of the chart title</param>		
		internal void SetTitle(string p_sTitle,string p_sFontName, float p_fFontSize)
		{
			Title objChartHeader = null;
			try
			{
				objChartHeader = m_objChart.Header;
				objChartHeader.Text =p_sTitle;			
				objChartHeader.Style.Font = new Font(p_sFontName, p_fFontSize,System.Drawing.FontStyle.Bold);				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Chart.SetTitle.ErrorSet",m_iClientId),p_objException);//sonali
			}
			finally
			{
				objChartHeader = null;
			}
		}

		/// <summary>
		/// This method will generate the memory stream corresponding to the chart generated.
		/// </summary>
		/// <returns>Binary Stream corresponding to the chart</returns>
		internal MemoryStream RenderToStream()
		{
			MemoryStream objStream = null;
			try
			{
				objStream = new MemoryStream();

				m_objChart.SaveImage(objStream,System.Drawing.Imaging.ImageFormat.Jpeg);
				return objStream;
			
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Chart.RenderToStream.Error",m_iClientId),p_objException);
			}
			finally
			{
				if(objStream != null)
				{
					objStream.Close();
					objStream = null;
				}
			}
		}

		/// <summary>
		/// This method will set the chart type (2DPie/3DPie/2DBar/3DBar).
		/// </summary>
		/// <param name="p_iChartType">Chart Type</param>
		internal void SetChartType(int p_iChartType)
		{
			try
			{
				m_iChartType = p_iChartType;
				switch (p_iChartType)
				{
					case 1:
					case 2:
						m_objChart.ChartGroups[0].ChartType = Chart2DTypeEnum.Pie;
						break;
					case 3:					
					case 4:
						m_objChart.ChartGroups[0].ChartType = Chart2DTypeEnum.Bar;
						break;
					default:					
						throw new InvalidValueException
							(Globalization.GetString("Chart.SetChartType.InvalidValue",m_iClientId));
						
				}
				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Chart.SetChartType.Error",m_iClientId),p_objException);
			}			
		}
		/// <summary>
		/// This method will set the width of the chart.
		/// </summary>
		/// <param name="p_iWidth">Width of the chart</param>
		internal void SetWidth(int p_iWidth)
		{
			try
			{
				m_objChart.Width = p_iWidth;				
			}
		
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Chart.SetWidth.Error",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method will set the height of the chart.
		/// </summary>
		/// <param name="p_iHeight">Height of the chart</param>
		internal void SetHeight(int p_iHeight)
		{
			try
			{
				m_objChart.Height = p_iHeight;				
			}
		
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Chart.SetHeight.Error",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method will set the back color of the chart.
		/// </summary>
		///<param name="p_objColor">Back Color</param>
		internal void SetBackColor(System.Drawing.Color p_objColor)
		{
			try
			{
				m_objChart.BackColor = p_objColor;				
			}		
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Chart.SetBackColor.Error",m_iClientId),p_objException);
			}
		}		
		/// <summary>
		/// This method will set the legend for the chart.
		/// </summary>
		internal void SetLegend()
		{
			Legend objLegend = null;
			try
			{
				objLegend = m_objChart.Legend;
				objLegend.Visible = true;
				objLegend.Orientation = C1.Win.C1Chart.LegendOrientationEnum.Vertical;
				objLegend.Compass=	C1.Win.C1Chart.CompassEnum.South;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Chart.SetLegend.Error",m_iClientId),p_objException);
			}
			finally
			{
				objLegend = null;
			}
		}

		/// <summary>
		/// This method will draw the chart by adding the data points and the legend.
		/// </summary>
		/// <param name="p_arrlstData">Chart Data</param>
		/// <param name="p_arrlstLegend">Chart Legend</param>
		internal void DrawChart(ArrayList p_arrlstData,ArrayList p_arrlstLegend)
		{
			ChartData		 objChartData = null;
			ChartDataSeries  objChartDataSeries = null;
			Label			 objChartLabel = null;
			AttachMethodData objLabelData=null;
			try
			{
					
				objChartData = m_objChart.ChartGroups[0].ChartData;
				objChartData.SeriesList.Clear();
				for(int iDataPoint=0; iDataPoint < p_arrlstLegend.Count; iDataPoint++)
				{
					objChartDataSeries =  objChartData.SeriesList.AddNewSeries();					
					objChartDataSeries.PointData.Length =1;
					
					//   For Bar Graph
					if ((m_iChartType == 3) || (m_iChartType == 4))
						objChartDataSeries.X[0] =  iDataPoint;

					objChartDataSeries.Y[0] =  p_arrlstData[iDataPoint];

					//*************Changed by: Mohit Yadav for Bug No. 000667*********
					if (iDataPoint <= (p_arrlstLegend.Count-1))
						objChartDataSeries.Label =  p_arrlstLegend[iDataPoint].ToString();		
					
					//   Adding Labels for bar graph					
					if ((m_iChartType == 3) || (m_iChartType == 4))
					{

						objChartLabel = m_objChart.ChartLabels.LabelsCollection.AddNewLabel();
						objChartLabel.AttachMethod = AttachMethodEnum.DataIndex;

						objLabelData = objChartLabel.AttachMethodData;
						objLabelData.GroupIndex = 0;
						objLabelData.PointIndex = 0;
						objLabelData.SeriesIndex = iDataPoint;

						objChartLabel.Text = p_arrlstData[iDataPoint].ToString();
						objChartLabel.Compass = LabelCompassEnum.North;
						objChartLabel.Connected = false;
						objChartLabel.Offset = 0;
						objChartLabel.Visible = true;
					}
				}

				//For Bar Graph , setting the space between the cluster of bars
				if ((m_iChartType == 3) || (m_iChartType == 4))
				{
					m_objChart.ChartGroups[0].Bar.ClusterWidth = 90;
					//*********Changed by: Mohit Yadav for Bug No. 000667******
					//m_objChart.ChartGroups[0].Bar.ClusterOverlap=100;
					m_objChart.ChartGroups[0].Bar.ClusterOverlap=95;
				}			
				
				// 3D settings
				if (m_iChartType == 2) // 3D Pie Graph
				{
					m_objChart.ChartArea.PlotArea.View3D.Depth = 45;
					m_objChart.ChartArea.PlotArea.View3D.Elevation = 45;
				}
				else if (m_iChartType == 4) // 3D Bar Graph
				{
					m_objChart.ChartArea.PlotArea.View3D.Depth = 5;
					m_objChart.ChartArea.PlotArea.View3D.Elevation = 10;
					m_objChart.ChartArea.PlotArea.View3D.Rotation = 15;
				}
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Chart.DrawChart.Error",m_iClientId),p_objException);
			}
			finally
			{
				objChartData = null;
				if	(objChartLabel != null)
				{
					objChartLabel.Dispose();
					objChartLabel = null;
				}
				objChartDataSeries = null;
				objLabelData=null;
			}
		}
		/// <summary>
		/// This method will destroy the chart object.
		/// </summary>
		internal void Dispose()
		{
			if(m_objChart != null)
			{
				m_objChart.Dispose();
				m_objChart =null;
			}
		}

		
		#endregion

	}
}
	