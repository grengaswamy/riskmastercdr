using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Collections;
using Riskmaster.Security;

namespace Riskmaster.Application.ClaimHistory
{

	/**************************************************************
		 * $File		: ClaimStatusHistory.cs
		 * $Revision	: 1.0.0.0
		 * $Date		: 08/18/2005
		 * $Author		: Mihika Agrawal
		 * $Comment		: Has functions to display the Status History for a claim
		 * $Source		:  	
		**************************************************************/

	/// <summary>
	/// ClaimStatusHistory class
	/// </summary>
	public class ClaimStatusHistory
	{
		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the Claim date for effective date trigger
		/// </summary>
		private string m_sClaimDate="";
		/// <summary>
		/// Private variable to store the System date for effective date trigger
		/// </summary>
		private string m_sSystemDate="";

        /// <summary>
        /// Private variable to store the Client Id
        /// </summary>
        private int m_iClientId;

		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;

		private const int FIXED_LENGTH_STRING=10;

        public UserLogin m_userLogin = null;//vkumar258 ML Changes
       

		#endregion

		#region Constructor
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
		public ClaimStatusHistory(string p_sDsnName , string p_sUserName , string p_sPassword, int p_iClientId)
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
		}

        public ClaimStatusHistory(UserLogin p_userLogin, string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId = 0)//vkumar258 ML Changes
        {
            m_userLogin = p_userLogin;
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Public Methods

		/// Name		: GetClaimStatusHistory
		/// Author		: Mihika Agrawal
		/// Date Created: 08/18/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the Claim Status History for a particular claim.
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument GetClaimStatusHistory(XmlDocument p_objXmlDoc)
		{
			XmlElement objElm = null;
			XmlElement objNewElm = null;
			XmlElement objRowNode = null;
            //Added by Shivendu for MITS 16148
            XmlElement objElmClaimNumber = null;
			DbReader objReader = null;
			LocalCache objCache = null;
			Claim c = null;

			string sSQL = string.Empty;
			string sCodeID = string.Empty;
			string sClaimID = string.Empty;
			string sTriggerDate=string.Empty;
					
			int iClaimID=0;
            
            string sClaimant = string.Empty; //nadim-17282
            XmlElement objElmClaimantName = null; //nadim-17282
			
			try
			{
				// 'ChangedBy' is set to the current user logged in
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ChangedBy");
				objElm.InnerText = m_sUserName;

				m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);	
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
				//Mukul Added 4/24/07 MITS 9312
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ClaimId");
				sClaimID = objElm.InnerText;
				iClaimID=Conversion.ConvertStrToInteger(sClaimID);
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//triggerdate");
				sTriggerDate = objElm.InnerText;
				if(sTriggerDate!="")
					m_sClaimDate=sTriggerDate;
				SetEffectiveDate(iClaimID);
				// Query to select the various codes from the database.
				sSQL = "SELECT DISTINCT CODES.CODE_ID,  CODES.SHORT_CODE , CODE_DESC, CODES.RELATED_CODE_ID ";
				sSQL = sSQL + "FROM CODES, CODES_TEXT, GLOSSARY ";
				sSQL = sSQL + "WHERE GLOSSARY.SYSTEM_TABLE_NAME='CLAIM_STATUS' AND ";
				sSQL = sSQL + "GLOSSARY.TABLE_ID = CODES.TABLE_ID AND ";
				sSQL = sSQL + "CODES.CODE_ID = CODES_TEXT.CODE_ID AND ";
				sSQL = sSQL + "(CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ";
				sSQL = sSQL + "CODES_TEXT.LANGUAGE_CODE = 1033";
				//Mukul Added 4/24/07 MITS 9312
				sSQL+=" AND ((CODES.TRIGGER_DATE_FIELD IS NULL)";
				sSQL+=" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ";
				sSQL+=" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + m_sClaimDate + "' AND CODES.EFF_END_DATE IS NULL) ";
				sSQL+=" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + m_sClaimDate + "') ";
				sSQL+=" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + m_sClaimDate + "' AND CODES.EFF_END_DATE>='" + m_sClaimDate + "') ";
				sSQL+=" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ";
				sSQL+="	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + m_sSystemDate + "' AND CODES.EFF_END_DATE IS NULL) ";
				sSQL+="	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + m_sSystemDate + "') ";
				sSQL+=" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + m_sSystemDate + "' AND CODES.EFF_END_DATE>='" + m_sSystemDate + "'))";

				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//CodeId");
				sCodeID = objElm.InnerText;

				// Populating the status types.
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//StatusType");

				//append blank row
				objNewElm = p_objXmlDoc.CreateElement("level");
				objNewElm.SetAttribute("id","0");
				objNewElm.SetAttribute("name",string.Empty);
				objElm.AppendChild(objNewElm);

				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
				while(objReader.Read())
				{
					objNewElm = p_objXmlDoc.CreateElement("level");
					objNewElm.SetAttribute("id", objReader.GetInt32("CODE_ID").ToString());
					objNewElm.SetAttribute("name", objReader.GetString("SHORT_CODE") + " - " + objReader.GetString("CODE_DESC"));
					if(objReader.GetInt32("CODE_ID").ToString() == sCodeID)
						objNewElm.SetAttribute("selected", "1");
					objElm.AppendChild(objNewElm);
				}
                objReader.Close();

				objCache = new LocalCache(m_sConnectionString,m_iClientId);

				
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ClaimStatusHistory");

				c = (DataModel.Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
				if(iClaimID>0)
				{
					c.MoveTo(Conversion.ConvertStrToInteger(sClaimID));

                   //nadim-17282
                    if (c.EventId != 0 && c.PrimaryClaimant != null)
                    {
                        sClaimant = c.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                    }
                  

                    //Start by Shivendu to fetch the claim number for MITS 16148
                    if (c != null)
                    {
                        objElmClaimNumber = (XmlElement)p_objXmlDoc.SelectSingleNode("//ClaimNumber");
                        objElmClaimNumber.InnerText = c.ClaimNumber;
                        //nadim-17282
                        objElmClaimantName = (XmlElement)p_objXmlDoc.SelectSingleNode("//ClaimantName");
                        objElmClaimantName.InnerText = sClaimant;
                    }
                    //End by Shivendu to fetch the claim number for MITS 16148

					//By Nikhil		Dated: 24th Aug 2005
					//Sorted the List on Date + RowId 
				
					SortedList objSortedList=new SortedList();
					string sKeyValue=string.Empty;
					foreach (ClaimStatusHist cl in c.ClaimStatusHistList)
					{
						sKeyValue=cl.DateStatusChgd + FixedLengthIdString(cl.ClStatusRowId.ToString());
						objSortedList.Add(sKeyValue,cl);
					}
				
					ClaimStatusHist objClaimStatusHist=null;
					for (int i=objSortedList.Count-1;i>=0;i--)
					{
						objClaimStatusHist=objSortedList.GetByIndex(i) as ClaimStatusHist;

						objRowNode = objElm.OwnerDocument.CreateElement("Row");
					
						// Node for Status
						objNewElm = objRowNode.OwnerDocument.CreateElement("Status");
						objNewElm.InnerText = objCache.GetCodeDesc(objClaimStatusHist.StatusCode);
						objRowNode.AppendChild(objNewElm);

						// Node for Date
						objNewElm = objRowNode.OwnerDocument.CreateElement("Date");
						objNewElm.InnerText = Conversion.GetUIDate(objClaimStatusHist.DateStatusChgd,m_userLogin.objUser.NlsCode.ToString(),m_iClientId);
						objRowNode.AppendChild(objNewElm);

						// Node for ChangedBy
						objNewElm = objRowNode.OwnerDocument.CreateElement("ChangedBy");
						objNewElm.InnerText = objClaimStatusHist.StatusChgdBy;
						objRowNode.AppendChild(objNewElm);

						// Node for Reason
						objNewElm = objRowNode.OwnerDocument.CreateElement("Reason");
						objNewElm.InnerText = objClaimStatusHist.Reason;
						objRowNode.AppendChild(objNewElm);

						objElm.AppendChild(objRowNode);
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClaimHistory.GetClaimStatusHistory.Error",m_iClientId), p_objEx);
			}
			finally
			{
                if (objReader != null)
                    objReader.Dispose();
				objElm = null;
				objNewElm = null;
				objRowNode = null;
                if (objCache != null)
                    objCache.Dispose();
                if (m_objDataModelFactory != null)
                    m_objDataModelFactory.Dispose();
				
			}

			return p_objXmlDoc;
		}
		#endregion

		#region Private Methods
		
		/// Name		: FixedLengthIdString
		/// Author		: Nikhil Garg
		/// Date Created: 08/24/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns a fixed length string with 0's appended at the start
		/// </summary>
		/// <param name="p_sId">input Id</param>
		/// <returns>fixed length string</returns>
		private string FixedLengthIdString(string p_sId)
		{
			string sReturn=string.Empty;

			for (int i=0; i < (FIXED_LENGTH_STRING-p_sId.Length); i++)
				sReturn=sReturn+"0";

			sReturn=sReturn+p_sId;

			return sReturn;
		}

		//Mukul Added 4/24/07 MITS 9312
		private void SetEffectiveDate(long p_lRowId)
		{
			string sSql = "";
			DbReader objReader=null;
			try
			{
				sSql = "SELECT DATE_OF_CLAIM FROM CLAIM WHERE CLAIM_ID=" + p_lRowId;
				if(m_sClaimDate == "")
				{
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
					
					if (objReader != null)
					{
						if (objReader.Read ())
						{
							m_sClaimDate = Conversion.ConvertObjToStr(objReader.GetValue("DATE_OF_CLAIM"));
						}
					}
				}
				m_sSystemDate = Conversion.GetDate (DateTime.Now.ToShortDateString());
			}
				//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objExp)
			{
				throw new RMAppException (Globalization.GetString ("ClaimHistory.SetEffectiveDate.GeneralError",m_iClientId), p_objExp);
			}
			finally 
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}

		#endregion
	}
}
