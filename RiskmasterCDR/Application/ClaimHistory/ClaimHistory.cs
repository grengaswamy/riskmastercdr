using System;
using Riskmaster.Db ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.DataModel ;
using Riskmaster.Common ;
using System.Xml;
using System.Text;

namespace Riskmaster.Application.ClaimHistory 
{
	/// <summary>
	/// Summary description for ClaimHistory.
	/// </summary>
	public class ClaimHistory :IDisposable 
	{
	
		/// <summary>
		/// Root tag
		/// </summary>
		private const string ROOT_TAG = "<ClaimHistory></ClaimHistory>";

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
        private int m_iClientId = 0;//sonali jira-882

        public ClaimHistory(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
		{		
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;//sonali jira- 882
			this.Initialize();
		}		
		~ClaimHistory()
		{
            Dispose();
		}

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
                m_objDataModelFactory.Dispose();
        }
		/// <summary>
		/// Initialize objects
		/// </summary>
		private void Initialize()
		{
			try
			{
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);//sonali jira-882
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;				
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("ClaimHistory.Initialize.ErrorInit",m_iClientId) , p_objEx );		//sonali jira-882		
			}			
		}

        public XmlDocument GetEDIHistory(int p_iClaimId)
        {
            string sSql = string.Empty;
            XmlDocument objXMLDocument = new XmlDocument();
            XmlElement objXMLElement = null;
            XmlElement objXMLChildElement = null;
            DbReader objDbReader = null;
            LocalCache objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

            try
            {
                objXMLDocument.LoadXml("<EDIHistory></EDIHistory>");
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("EDIHistory");

                sSql = "SELECT * FROM EDI_HISTORY WHERE CLAIM_ID=" + p_iClaimId;
                objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objDbReader != null)
                {
                    while (objDbReader.Read())
                    {
                        objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewElement("option"), false);
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("Ak1Date", Conversion.GetDBDateFormat(objDbReader["AK1_DATE"].ToString(),"d")), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("Ak1DocId", objDbReader["AK1_DOC_ID"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("AddedByUser", objDbReader["ADDED_BY_USER"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("AttachmentId", objDbReader["ATTACHMENT_ID"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("ClaimId", objDbReader["CLAIM_ID"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("ClaimNumber", objDbReader["CLAIM_NUMBER"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("EDIRowId", objDbReader["EDI_ROW_ID"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("FilingDate", Conversion.GetDBDateFormat(objDbReader["FILING_DATE"].ToString(),"d")), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("FilingMTC", objDbReader["FILING_MTC"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("FilingStatus", objLocalCache.GetCodeDesc(objDbReader.GetInt32("FILING_STATUS"))), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("FilingType", objDbReader["FILING_TYPE"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("Jurisdiction", objLocalCache.GetStateCode(objDbReader.GetInt16("JURISDICTION"))), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LateReason", objLocalCache.GetCodeDesc(objDbReader.GetInt16("LATE_REASON"))), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("MTCCorrection", objDbReader["MTC_CORRECTION"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("MTCCorrectDate", Conversion.GetDBDateFormat(objDbReader["MTC_CORRECT_DATE"].ToString(),"d")), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("SuspensionEffDate", Conversion.GetDBDateFormat(objDbReader["SUSPENSION_EFF_DATE"].ToString(), "d")), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("SuspensionNarr", objDbReader["SUSPENSION_NARR"].ToString()), true));
                        objXMLElement.AppendChild(objXMLChildElement);

                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ClaimHistory.GetEDIHistory.Error", m_iClientId), p_objEx);//sonali jira-882		
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }

            return objXMLDocument;
        }

        // Ayush: MITS:18230, 01/22/2010  - Added one more parameter
        public XmlDocument GetClaimHistory(int p_iClaimantEId, int p_iDefendantEId, int p_iUnitId, int p_iEventId, int p_iEventNumber, int p_iPropertyId)
		{
			StringBuilder objSbuild = new StringBuilder();
			XmlDocument objXMLDocument = new XmlDocument();
			XmlElement objXMLElement = null;
			XmlElement objXMLChildElement = null;
			DbReader objDbReader = null;
			int iLobcode = 0;
			string sEntityName=string.Empty;
            // Ayush: MITS:18230, 01/22/2010 Start
            string iPin = string.Empty;
            // Ayush: MITS:18230, 01/22/2010 End
            //Add by kuladeep Start MITS:32672
            StringBuilder objPolicySbuild = new StringBuilder();
            //Add by kuladeep End MITS:32672
            LocalCache objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

			try
			{

				objSbuild.Append("SELECT CLAIM.DATE_OF_CLAIM, CLAIM.CLAIM_NUMBER, CLAIM.LINE_OF_BUS_CODE, CLAIM.CLAIM_TYPE_CODE, CLAIM.CLAIM_STATUS_CODE, CLAIM.DTTM_CLOSED, CLAIM.CLAIM_ID, CODE1.SHORT_CODE  CTSHORT, CODE1.CODE_DESC  CTDESC, CODE2.SHORT_CODE  STSHORT, CODE2.CODE_DESC  STDESC");
				if(p_iClaimantEId != 0)
				{
					sEntityName=objLocalCache.GetEntityLastFirstName(p_iClaimantEId);
					objSbuild.Append(" FROM CLAIM, CLAIMANT, CODES_TEXT CODE1, CODES_TEXT CODE2");
					objSbuild.Append(" WHERE CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID AND CLAIMANT.CLAIMANT_EID = " + p_iClaimantEId);
				}
				else if(p_iDefendantEId != 0)
				{
					sEntityName=objLocalCache.GetEntityLastFirstName(p_iDefendantEId);
					objSbuild.Append(" FROM CLAIM, DEFENDANT, CODES_TEXT CODE1, CODES_TEXT CODE2");
					objSbuild.Append(" WHERE CLAIM.CLAIM_ID = DEFENDANT.CLAIM_ID AND DEFENDANT.DEFENDANT_EID = " + p_iDefendantEId);
				}
				else if(p_iUnitId != 0)
				{
                    //Add and comment by kuladeep MITS:32672 Start

                    //objSbuild.Append(" ,VEHICLE.VEHICLE_MAKE, VEHICLE.VEHICLE_MODEL, VEHICLE.VIN");
                    //objSbuild.Append(" FROM CLAIM, UNIT_X_CLAIM, CODES_TEXT CODE1, CODES_TEXT CODE2, VEHICLE");
                    //objSbuild.Append(" WHERE CLAIMCLAIM_ID = UNIT_X_CLAIM.CLAIM_ID AND UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.UNIT_ID = " + p_iUnitId);

                    //this Query will fetch record for External Policy
                    objSbuild.Append(",VEHICLE.VEHICLE_MAKE, VEHICLE.VEHICLE_MODEL, VEHICLE.VIN FROM CLAIM,CODES_TEXT CODE1,CODES_TEXT CODE2,VEHICLE,");
                    objSbuild.Append("( SELECT CLAIM_X_POLICY.CLAIM_ID,RS.UNIT_ID FROM (SELECT P.*, UC.CLAIM_ID, UC.UNIT_ID,PD.STAT_UNIT_NUMBER FROM");
                    objSbuild.Append("(UNIT_X_CLAIM UC INNER JOIN POINT_UNIT_DATA PD on UC.UNIT_ID = PD.UNIT_ID AND PD.UNIT_TYPE = 'V'");
                    objSbuild.Append(" INNER JOIN POLICY_X_UNIT PU on UC.UNIT_ID = PU.UNIT_ID AND PU.UNIT_TYPE = 'V')");
                    objSbuild.Append(" INNER JOIN POLICY P on PU.POLICY_ID = P.POLICY_ID) RS, POLICY, POLICY_X_UNIT, CLAIM_X_POLICY, POINT_UNIT_DATA");
                    objSbuild.Append(" WHERE POLICY.POLICY_ID = CLAIM_X_POLICY.POLICY_ID AND POLICY.POLICY_ID = POLICY_X_UNIT.POLICY_ID AND ");
                    objSbuild.Append(" POLICY_X_UNIT.UNIT_ID = POINT_UNIT_DATA.UNIT_ID AND POINT_UNIT_DATA.UNIT_TYPE = 'V' AND POLICY.LOCATION_COMPANY = RS.LOCATION_COMPANY AND ");
                    objSbuild.Append(" POLICY.MASTER_COMPANY = RS.MASTER_COMPANY AND POLICY.POLICY_NUMBER = RS.POLICY_NUMBER AND POLICY.POLICY_SYMBOL = RS.POLICY_SYMBOL AND ");
                    objSbuild.Append(" POLICY.MODULE = RS.MODULE AND POINT_UNIT_DATA.STAT_UNIT_NUMBER = RS.STAT_UNIT_NUMBER AND RS.UNIT_ID = '" + p_iUnitId + "') TBLCLAIM ");
                    objSbuild.Append(" WHERE TBLCLAIM.CLAIM_ID = CLAIM.CLAIM_ID AND VEHICLE.UNIT_ID=TBLCLAIM.UNIT_ID ");

                    //this Query will fetch record for internal Policy
                    objPolicySbuild.Append("SELECT CLAIM.DATE_OF_CLAIM, CLAIM.CLAIM_NUMBER, CLAIM.LINE_OF_BUS_CODE, CLAIM.CLAIM_TYPE_CODE, CLAIM.CLAIM_STATUS_CODE, CLAIM.DTTM_CLOSED, CLAIM.CLAIM_ID, CODE1.SHORT_CODE  CTSHORT, CODE1.CODE_DESC  CTDESC, CODE2.SHORT_CODE  STSHORT, CODE2.CODE_DESC  STDESC");
                    objPolicySbuild.Append(" ,VEHICLE.VEHICLE_MAKE, VEHICLE.VEHICLE_MODEL, VEHICLE.VIN");
                    objPolicySbuild.Append(" FROM CLAIM, UNIT_X_CLAIM, CODES_TEXT CODE1, CODES_TEXT CODE2, VEHICLE");
                    objPolicySbuild.Append(" WHERE CLAIM.CLAIM_ID = UNIT_X_CLAIM.CLAIM_ID AND UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.UNIT_ID = " + p_iUnitId);
                    objPolicySbuild.Append(" AND CODE1.CODE_ID=CLAIM.CLAIM_TYPE_CODE AND CODE1.LANGUAGE_CODE=1033 AND CODE2.CODE_ID=CLAIM.CLAIM_STATUS_CODE");
                    objPolicySbuild.Append(" AND CODE2.LANGUAGE_CODE=1033 ");

                    //Add and comment by kuladeep MITS:32672 End
                    
				}
				else if(p_iEventId != 0)
				{
					objSbuild.Append(" FROM CLAIM, CODES_TEXT CODE1, CODES_TEXT CODE2");
					objSbuild.Append(" WHERE CLAIM.EVENT_ID = " + p_iEventId);
				}
                // Ayush: MITS:18230, 01/22/2010 Start
                    //Start: Neha Suresh Jain, 03/15/2010, to get property details, 04/02/2010, isnull check added for address
                else if (p_iPropertyId != 0)
                {
                   // objSbuild.Append(" ,CLAIM_X_PROPERTY.PIN");
                    //objSbuild.Append(" FROM CLAIM, CLAIM_X_PROPERTY, CODES_TEXT CODE1, CODES_TEXT CODE2");
                    //objSbuild.Append(" ,CLAIM_X_PROPERTY.PIN, CLAIM_X_PROPERTY.DESCRIPTION, (CLAIM_X_PROPERTY.ADDR1 + ' ' + CLAIM_X_PROPERTY.ADDR2) Address, CLAIM_X_PROPERTY.CITY, STATES.STATE_NAME, CLAIM_X_PROPERTY.ZIP_CODE ");
                    objSbuild.Append(" ,CLAIM_X_PROPERTY.PIN");
                    //objSbuild.Append(" ,CLAIM_X_PROPERTY.DESCRIPTION, (CLAIM_X_PROPERTY.ADDR1 + ' ' + isnull(CLAIM_X_PROPERTY.ADDR2,'')) Address, CLAIM_X_PROPERTY.CITY, STATES.STATE_NAME, CLAIM_X_PROPERTY.ZIP_CODE ");                    
                    objSbuild.Append(" FROM CLAIM, CLAIM_X_PROPERTY, CODES_TEXT CODE1, CODES_TEXT CODE2, STATES");
                    objSbuild.Append(" WHERE CLAIM.PROP_ROW_ID = CLAIM_X_PROPERTY.PROP_ROW_ID AND CLAIM.LINE_OF_BUS_CODE = 845 AND STATES.STATE_ROW_ID = CLAIM_X_PROPERTY.STATE_ID AND CLAIM_X_PROPERTY.PROPERTY_ID = " + p_iPropertyId);

                }
                //end: Neha Suresh Jain
                // Ayush: MITS:18230, 01/22/2010 End
				objSbuild.Append(" AND CODE1.CODE_ID=CLAIM.CLAIM_TYPE_CODE AND CODE1.LANGUAGE_CODE=1033 AND CODE2.CODE_ID=CLAIM.CLAIM_STATUS_CODE");
				objSbuild.Append(" AND CODE2.LANGUAGE_CODE=1033 ORDER BY CLAIM.DATE_OF_CLAIM DESC");

                //Add by kuladeep MITS:32672 Start--UNION all the record.
                if (p_iUnitId != 0)
                {
                    objPolicySbuild.Append(" UNION ");
                    objPolicySbuild.Append(Convert.ToString(objSbuild));
                    objSbuild.Length = 0;
                    objSbuild = objPolicySbuild;
                }
                //Add by kuladeep MITS:32672 End

				objXMLDocument.LoadXml(ROOT_TAG);
				objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("ClaimHistory");	
			
				objDbReader = DbFactory.GetDbReader(m_sConnectionString,objSbuild.ToString());
				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewElement("option"),false);
						iLobcode = Conversion.ConvertStrToInteger(objDbReader["LINE_OF_BUS_CODE"].ToString());
						objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LOB_CODE",iLobcode.ToString()),true));
						objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("DateOfClaim",objDbReader["DATE_OF_CLAIM"].ToString()),true));
						objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("ClaimNumber",objDbReader["CLAIM_NUMBER"].ToString()),true));
						if(iLobcode == 241)		
						{
							objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LineOfBusCode","GC"),true));
							objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("FormName","claimgc"),true));
						}
						else if(iLobcode == 242)
						{
							objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LineOfBusCode","VA"),true));
							objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("FormName","claimva"),true));
						}
						else if(iLobcode == 243)
						{
							objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LineOfBusCode","WC"),true));
							objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("FormName","claimwc"),true));
						}
						else if(iLobcode == 844)
						{
							objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LineOfBusCode","DI"),true));
							objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("FormName","claimdi"),true));
						}
                        // Ayush: MITS:18230, 01/22/2010 Start
                        else if (iLobcode == 845)
                        {
                            objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LineOfBusCode", "PC"), true));
                            objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("FormName", "claimpc"), true));
                        }
                        // Ayush: MITS:18230, 01/22/2010 End
						else
						{
							objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LineOfBusCode","-"),true));
							objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("FormName",""),true));
						}
						objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("ClaimTypeCode",objDbReader["CTSHORT"].ToString() + " " + objDbReader["CTDESC"].ToString()),true));
						objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("ClaimStatusCode",objDbReader["STSHORT"].ToString() + " " + objDbReader["STDESC"].ToString()),true));
						objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("DttmClosed",objDbReader["DTTM_CLOSED"].ToString()),true));
						objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("ClaimId",objDbReader["CLAIM_ID"].ToString()),true));
                        //We dont need this to be displayed on the Claim History screen.
                        //Commenting the code for any future references.
                        //Start: Neha Suresh Jain, 03/15/2010, to get property details
                        //if (p_iPropertyId != 0)//If condition Added : Yatharth MITS 22076
                        //{
                        //    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("PIN", objDbReader["PIN"].ToString()), true));
                        //    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("DESCRIPTION", objDbReader["DESCRIPTION"].ToString()), true));
                        //    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("Address", objDbReader["Address"].ToString()), true));
                        //    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("CITY", objDbReader["CITY"].ToString()), true));
                        //    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("STATE_NAME", objDbReader["STATE_NAME"].ToString()), true));
                        //    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("ZIP_CODE", objDbReader["ZIP_CODE"].ToString()), true));
                        //}
                        //end: Neha Suresh Jain

						objXMLElement.AppendChild(objXMLChildElement);

                        // Ayush: MITS:18230, 01/22/2010 Start:
                        if (p_iPropertyId != 0 && sEntityName.Trim() == string.Empty)
                        {
                            sEntityName = Conversion.ConvertObjToStr(objDbReader["PIN"]);
                        }
                        // Ayush: MITS:18230, 01/22/2010 End
						if(p_iUnitId != 0 && sEntityName.Trim()==string.Empty)
							sEntityName = objDbReader["VIN"].ToString() + " * " + objDbReader["VEHICLE_MAKE"].ToString() + " " + objDbReader["VEHICLE_MODEL"].ToString();
					}	
				}

				objXMLDocument.DocumentElement.SetAttribute("SubTitle","["+ sEntityName + "]");

				objDbReader.Close();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ClaimHistory.GetClaimHistory.Error", m_iClientId), p_objEx);//sonali jira-882		
			}
			finally
			{
                if (objDbReader != null)
                    objDbReader.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                if (m_objDataModelFactory != null)
                    m_objDataModelFactory.Dispose();
			}
		
			return objXMLDocument; 
		}

		#region Get new element
		/// Name		: GetNewElement
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creats a new XMLElement with the nodename passed as parameter
		/// </summary>
		/// <param name="p_sNodeName">Node Name</param>
		/// <returns>XML element to be added in the DOM</returns>
		private XmlElement GetNewElement(string p_sNodeName)
		{
			XmlDocument objXDoc;
			XmlElement objXMLElement;

			try
			{
				objXDoc = new XmlDocument();
				objXMLElement = objXDoc.CreateElement(p_sNodeName); 
				return objXMLElement;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("ClaimHistory.GetNewElement.XMLError", m_iClientId), p_objException);//sonali jira-882		
			}
			finally
			{
				objXDoc = null;
				objXMLElement = null;
			}
		}
		#endregion

		/// Name		: GetNewEleWithValue
		/// Author		: Anurag Agarwal
		/// Date Created	: 3 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Creats a new XMLElement with the nodename passed as parameter. 
		/// Also sets the Innertext to the new XMLElement
		/// </summary>
		/// <param name="p_sNewNodeName">New Node to be Created</param>
		/// <param name="p_sNodeValue">Value for the node</param>
		/// <returns>New XML Element with Innertext set</returns>
		internal static XmlElement GetNewEleWithValue(string p_sNewNodeName, string p_sNodeValue)
		{
			XmlDocument objXDoc;
			XmlElement objXMLElement;
			objXDoc = new XmlDocument();
			objXMLElement = objXDoc.CreateElement(p_sNewNodeName); 
			objXMLElement.InnerText = p_sNodeValue;  
			objXDoc = null;
			return objXMLElement;
		}
	}
}
