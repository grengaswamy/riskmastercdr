﻿using System;
using System.Xml;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.Text;
using Riskmaster.Settings;
using Riskmaster.Security;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Web;
using System.Xml.XPath;
using Riskmaster.DataModel;
using Riskmaster.Models;
using Riskmaster.Security.Encryption;

namespace Riskmaster.Application.PolicySystemInterface
{
    /// <summary>
    ///Author  :   smishra54
    ///Dated   :   11/29/2011
    ///Purpose :   Application layer comprises Policy System related operations.
    /// </summary>
    public class PolicySystemInterface
    {
        private string m_sConnectString = string.Empty;
        private string m_sUserName = string.Empty;
        private string m_sDSNName = string.Empty;
        private string m_sPassword = string.Empty;
        //rupal:start, mits 33913
        private ScriptEngine m_ScriptEngine = null;
		  private UserLogin m_oUserLogin;
        private bool m_bScriptfileExists = false;
        private int m_iClientId = 0;
        //rupal:end , mits 33913
        public PolicySystemInterface(string conn,int p_iClientId)
        {
            m_sConnectString = conn;
            m_iClientId = p_iClientId;
        }

        public PolicySystemInterface(UserLogin objUserLogin, int p_iClientId=0)
        {
            m_sConnectString = objUserLogin.objRiskmasterDatabase.ConnectionString;
            m_sUserName = objUserLogin.LoginName;
            m_sDSNName = objUserLogin.objRiskmasterDatabase.DataSourceName;
            m_sPassword = objUserLogin.Password;
            m_iClientId = p_iClientId;
            
            //tanwar2 - Staging
            m_oUserLogin = objUserLogin;
        }
		 //rupal:start, mits 33913
        private ScriptEngine objScriptEngine
        {
            get
            {
                if (m_ScriptEngine == null)
                    m_ScriptEngine = new ScriptEngine(m_sConnectString,ScriptEngine.ScriptTypes.SCRIPT_DOWNLOAD, m_iClientId);
                return m_ScriptEngine;
            }
        }

        public bool ScriptFileExists
        {
            get
            {
                return m_bScriptfileExists;
            }
            set
            {
                m_bScriptfileExists = value;
            }
        }
        //rupal:end, mits 33913
        public XmlDocument GetPolicySystemList(ref int p_iDefaultPolicyId)
        {
            XmlDocument objXmlDocument = null;
            XmlElement objOptionXmlElement = null;
            XmlElement objRootElement = null;
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            bool isRecordsFound = false;
            string sValue = string.Empty;
            string sPolSysTypeCd = string.Empty;
            LocalCache objCache = new LocalCache(m_sConnectString, m_iClientId);
            //tanwar2 - WWIG - Requirement 1.4.1 - start
            SysSettings oSysSettings = null;
            //tanwar2 - WWIG - Requirement 1.4.1 - end
            try
            {
                objXmlDocument = new XmlDocument();
                //sSQL = "SELECT POLICY_SYSTEM_ID,POLICY_SYSTEM_NAME,DEFAULT_POL_FLAG,POLICY_SYSTEM_CODE,CLIENTFILE_FLAG, LOC_COMPNY,MASTER_COMPANY FROM POLICY_X_WEB ORDER BY DEFAULT_POL_FLAG,POLICY_SYSTEM_NAME"; //MITS 35932   
                sSQL = "SELECT POLICY_X_WEB.POLICY_SYSTEM_ID,POLICY_X_WEB.POLICY_SYSTEM_NAME,POLICY_X_WEB.DEFAULT_POL_FLAG,POLICY_X_WEB.POLICY_SYSTEM_CODE,POLICY_X_WEB.CLIENTFILE_FLAG, POLICY_X_WEB.LOC_COMPNY,MASTER_COMPANY, CODES.SHORT_CODE FROM POLICY_X_WEB LEFT JOIN CODES ON POLICY_X_WEB.POLICY_SYSTEM_CODE = CODES.CODE_ID ORDER BY DEFAULT_POL_FLAG,POLICY_SYSTEM_NAME"; //MITS 35932   
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                objRootElement = objXmlDocument.CreateElement("PolicySystemList");
                objXmlDocument.AppendChild(objRootElement);

                while (objDbReader.Read())
                {
                    isRecordsFound = true;
                    objOptionXmlElement = objXmlDocument.CreateElement("option");
                    sPolSysTypeCd = objCache.GetShortCode(Conversion.ConvertObjToInt(objDbReader.GetValue(3), m_iClientId));
                    sValue = Conversion.ConvertObjToStr(objDbReader.GetValue(0)) + "|" + Conversion.ConvertObjToStr(objDbReader.GetValue(4))
                            + "|" + Conversion.ConvertObjToStr(objDbReader.GetValue(5)) + "|" + Conversion.ConvertObjToStr(objDbReader.GetValue(6))
                            + "|" + Conversion.ConvertObjToStr(objDbReader.GetValue(7)).ToUpper() 
                             + "|" + sPolSysTypeCd;
                    objOptionXmlElement.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue(0)));
                    objOptionXmlElement.SetAttribute("policy_system_type", Conversion.ConvertObjToStr(objDbReader.GetValue(3)));
                    objOptionXmlElement.SetAttribute("default_policy_flag", Convert.ToString(objDbReader.GetInt16("DEFAULT_POL_FLAG"))); //JIRA : RMA-10039
                    objOptionXmlElement.SetAttribute("cf_lc_value", sValue);
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue(1));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                    if (Conversion.ConvertObjToBool(objDbReader.GetValue("DEFAULT_POL_FLAG"), 0))
                    {
                        p_iDefaultPolicyId = Conversion.ConvertObjToInt(objDbReader.GetValue(0), m_iClientId);
                    }
                    
                    //tanwar2 - WWIG - Requirement 1.4.1 - start
                    oSysSettings = new SysSettings(m_sConnectString, m_iClientId);
                    objRootElement.SetAttribute("NoRequiredField", oSysSettings.NoReqFieldsForPolicySearch.ToString());
                    //tanwar2 - WWIG - Requirement 1.4.1 - end
                }

                if (!isRecordsFound)
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemList.NoPolicySystemsDefined",m_iClientId));
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemList.PolicySysFetchingError",m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                objOptionXmlElement = null;
                objRootElement = null;
                    if (objCache != null)
                    objCache.Dispose();
            }
            return objXmlDocument;
        }

        public XmlDocument GetUnitXmlData(string sSessionId)
        {
            string sFileContent = string.Empty;
            XmlDocument objXmlDocument = null;
            string sFilePath = string.Empty;
            try
            {

                if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "Unit", sSessionId)))
                {
                    sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "Unit", sSessionId);

                    sFileContent = File.ReadAllText(sFilePath);
                    objXmlDocument = new XmlDocument();
                    objXmlDocument.LoadXml(sFileContent);
                }



            }
            catch (RMAppException p_objException)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemList.PolicySysFetchingError",m_iClientId), p_objException);
            }
            catch (Exception e)
            {
                throw e;
            }

            return objXmlDocument;
        }

        /// <summary>
        /// Reads the xml from the path fror unit and entity download 
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="p_sMode">mode is used to identify unit/Entity download</param>
        /// <returns></returns>
        public XmlDocument GetXmlData(ref string p_sMode,string sSessionId)
        {
            string sFileContent = string.Empty;
            XmlDocument objXmlDocument = null;
            string sSQL = string.Empty;
            string sFilePath = string.Empty;
            try
            {
                if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "entity", sSessionId)))
                {
                    p_sMode = "entity";
                    sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "entity", sSessionId);
                }


                //else if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "property")))
                //{
                //    p_sMode = "property";
                //    sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "property");
                //}
                else if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "driver", sSessionId)))
                {
                    p_sMode = "driver";
                    sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "driver", sSessionId);
                }
                else if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "Unit", sSessionId)))
                {
                    p_sMode = "unit";
                    sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "Unit", sSessionId);
                }//skhare7 Policy interface unit interest
                else if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "UnitInterestList", sSessionId)))
                {
                    p_sMode = "unitinterest";
                    sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "UnitInterestList", sSessionId);
                }
                else if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "CoverageList", sSessionId)))
                {
                    p_sMode = "CoverageList";
                    sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "CoverageList", sSessionId);
                }
                if (File.Exists(sFilePath))
                {
                    sFileContent = File.ReadAllText(sFilePath);
                    objXmlDocument = new XmlDocument();
                    objXmlDocument.LoadXml(sFileContent);
                }
                else
                {
                    p_sMode = "completed";
                }


            }
            catch (RMAppException p_objException)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemList.PolicySysFetchingError",m_iClientId), p_objException);
            }
            catch (Exception e)
            {
                throw e;
            }

            return objXmlDocument;
        }
        /// <summary>
        /// Gets the filename which contains the stored XML for unit/Entity download
        /// </summary>
        /// <param name="p_sPolicySystemId">Policy System Id</param>
        /// <param name="p_sUserName">UserName</param>
        /// <param name="p_sMode"></param>
        /// <returns></returns>
        private string GetFileName(string p_sUserName, string p_sMode,string sSessionId)
        {
          
            return (p_sMode + "_" + p_sUserName +"_"+ sSessionId+ "_tmp" + ".xml");
        }

        public string SaveOptions(string p_sMode, string p_sSelectedValues, int p_iPolicyId, string sAddEntityAs, ref string sUnitNumber, ref string sPolicyExternalId, ref  string sPolicySymbol, ref string sUpdatedIds)
        {
            string sFileContent = string.Empty;
            XmlDocument objXmlDocument = null;
            string sSQL = string.Empty;
            XmlNodeList objNodeList = null;
            string sFilePath = string.Empty;
            string[] arrSelectedValues = null;
            string sOuterXml = string.Empty;
            XmlDocument objDoc = null;
            DataObject objData = null;
            DataModelFactory objDmf = null;
            bool bSuccess = false;
            string sRecordIds = string.Empty;
            DbConnection objCon = null;
            ArrayList objList = null;
            int iIndex = 0;
            //string sTemp = string.Empty;
            LocalCache objCache = null;

            try
            {

              //  sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, p_sMode);
                sFileContent = File.ReadAllText(sFilePath);
                objXmlDocument = new XmlDocument();
                objXmlDocument.LoadXml(sFileContent);

                arrSelectedValues = p_sSelectedValues.Split(',');
                objList = new ArrayList();

                if (string.Equals(p_sMode, "vehicle", StringComparison.InvariantCultureIgnoreCase))
                {
                    objNodeList = objXmlDocument.SelectNodes("//Vin");
                    sSQL = "SELECT UNIT_ID,UNIT_TYPE FROM POLICY_X_UNIT WHERE POLICY_ID= " + p_iPolicyId;
                    using (DbReader oReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                    {
                        while (oReader.Read())
                        {
                            if (string.Equals(Conversion.ConvertObjToStr(oReader.GetValue("UNIT_TYPE")), "V", StringComparison.InvariantCultureIgnoreCase))
                                objList.Add(Conversion.ConvertObjToStr(oReader.GetValue("UNIT_ID")));
                        }
                    }
                }
                else if (string.Equals(p_sMode, "property", StringComparison.InvariantCultureIgnoreCase))
                {
                    objNodeList = objXmlDocument.SelectNodes("//Pin");
                    sSQL = "SELECT UNIT_ID,UNIT_TYPE FROM POLICY_X_UNIT WHERE POLICY_ID= " + p_iPolicyId;
                    using (DbReader oReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                    {
                        while (oReader.Read())
                        {
                            if (string.Equals(Conversion.ConvertObjToStr(oReader.GetValue("UNIT_TYPE")), "P", StringComparison.InvariantCultureIgnoreCase))
                                objList.Add(Conversion.ConvertObjToStr(oReader.GetValue("UNIT_ID")));
                        }
                    }
                }
                else if (string.Equals(p_sMode, "entity", StringComparison.InvariantCultureIgnoreCase))
                {
                    //    objNodeList = objXmlDocument.SelectNodes("//TaxId");
                    //    sSQL = "SELECT INSURED_EID FROM POLICY_X_INSURED WHERE POLICY_ID= " + p_iPolicyId;
                    //    using (DbReader oReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                    //    {
                    //        while (oReader.Read())
                    //        {
                    //            objList.Add(Conversion.ConvertObjToStr(oReader.GetValue("INSURED_EID")));
                    //        }
                    //    }
                }
                else if (string.Equals(p_sMode, "driver", StringComparison.InvariantCultureIgnoreCase))
                {
                    objCache = new LocalCache(m_sConnectString,m_iClientId);
                }
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                foreach (XmlNode objNode in objNodeList)
                {
                    foreach (string sSelectedValue in arrSelectedValues)
                    {

                        if (string.Equals(objNode.InnerText, sSelectedValue.Split('|')[0], StringComparison.InvariantCultureIgnoreCase))
                        {
                            //  sTemp = sAddEntityAs.Split(',')[iIndex];
                            sOuterXml = objNode.ParentNode.ParentNode.OuterXml;
                            objDoc = new XmlDocument();
                            objDoc.LoadXml(sOuterXml);
                            if (string.Equals(p_sMode, "vehicle", StringComparison.InvariantCultureIgnoreCase))
                            {
                                objData = (Vehicle)objDmf.GetDataModelObject("Vehicle", false);
                                if (sSelectedValue.Split('|').Length > 1)
                                    objDoc.SelectSingleNode("//UnitId").InnerText = sSelectedValue.Split('|')[1];
                            }
                            else if (string.Equals(p_sMode, "property", StringComparison.InvariantCultureIgnoreCase))
                            {
                                objData = (PropertyUnit)objDmf.GetDataModelObject("PropertyUnit", false);
                                if (sSelectedValue.Split('|').Length > 1)
                                    objDoc.SelectSingleNode("//PropertyId").InnerText = sSelectedValue.Split('|')[1];
                            }
                            else if (string.Equals(p_sMode, "entity", StringComparison.InvariantCultureIgnoreCase))
                            {
                                objData = (Entity)objDmf.GetDataModelObject("Entity", false);
                                if (sSelectedValue.Split('|').Length > 1)
                                    objDoc.SelectSingleNode("//EntityId").InnerText = sSelectedValue.Split('|')[1];

                                if (!string.IsNullOrEmpty(sAddEntityAs))
                                    objDoc.SelectSingleNode("//EntityTableId").InnerText = sAddEntityAs.Split(',')[iIndex];
                            }
                            else if (string.Equals(p_sMode, "driver", StringComparison.InvariantCultureIgnoreCase))
                            {
                                objData = (Entity)objDmf.GetDataModelObject("Entity", false);
                                if (sSelectedValue.Split('|').Length > 1)
                                    objDoc.SelectSingleNode("//EntityId").InnerText = sSelectedValue.Split('|')[1];

                                if (!string.IsNullOrEmpty(sAddEntityAs))
                                    objDoc.SelectSingleNode("//EntityTableId").InnerText = objCache.GetTableId("DRIVER_OTHER").ToString();
                            }
                            if (sSelectedValue.Split('|').Length > 1)
                            {
                                objData.MoveTo(Conversion.CastToType<int>(sSelectedValue.Split('|')[1], out bSuccess));
                            }

                            objData.PopulateObject(objDoc);
                            objData.Save();



                            switch (p_sMode.ToLower())
                            {
                                case "vehicle":
                                    if (!objList.Contains((objData as Vehicle).UnitId.ToString()))
                                    {
                                        PolicyXUnit objPolicyUnit = (PolicyXUnit)objDmf.GetDataModelObject("PolicyXUnit", false);
                                        objPolicyUnit.PolicyId = p_iPolicyId;
                                        objPolicyUnit.UnitId = (objData as Vehicle).UnitId;
                                        objPolicyUnit.UnitType = "V";
                                        objPolicyUnit.Save();
                                        if (string.IsNullOrEmpty(sRecordIds))
                                            sRecordIds = objPolicyUnit.PolicyUnitRowId.ToString();
                                        else
                                            sRecordIds = sRecordIds + "," + objPolicyUnit.PolicyUnitRowId.ToString();

                                        if (string.IsNullOrEmpty(sUnitNumber))
                                            sUnitNumber = (objData as Vehicle).Vin;
                                        else
                                            sUnitNumber = sRecordIds + "," + (objData as Vehicle).Vin;

                                        objPolicyUnit.Dispose();
                                    }

                                    if (string.IsNullOrEmpty(sUpdatedIds))
                                        sUpdatedIds = (objData as Vehicle).UnitId.ToString();
                                    else
                                        sUpdatedIds = sUpdatedIds + "," + (objData as Vehicle).UnitId.ToString();

                                    break;

                                case "property":
                                    if (!objList.Contains((objData as PropertyUnit).PropertyId.ToString()))
                                    {
                                        PolicyXUnit objPolicyXUnit = (PolicyXUnit)objDmf.GetDataModelObject("PolicyXUnit", false);
                                        objPolicyXUnit.PolicyId = p_iPolicyId;
                                        objPolicyXUnit.UnitId = (objData as PropertyUnit).PropertyId;
                                        objPolicyXUnit.UnitType = "P";
                                        objPolicyXUnit.Save();

                                        if (string.IsNullOrEmpty(sRecordIds))
                                            sRecordIds = objPolicyXUnit.PolicyUnitRowId.ToString();
                                        else
                                            sRecordIds = sRecordIds + " " + objPolicyXUnit.PolicyUnitRowId.ToString();

                                        if (string.IsNullOrEmpty(sUnitNumber))
                                            sUnitNumber = (objData as PropertyUnit).Pin;
                                        else
                                            sUnitNumber = sRecordIds + "," + (objData as PropertyUnit).Pin;

                                        objPolicyXUnit.Dispose();
                                    }

                                    if (string.IsNullOrEmpty(sUpdatedIds))
                                        sUpdatedIds = (objData as PropertyUnit).PropertyId.ToString();
                                    else
                                        sUpdatedIds = sUpdatedIds + "," + (objData as PropertyUnit).PropertyId.ToString();

                                    break;
                                case "entity":
                                //if (!objList.Contains((objData as Entity).EntityId.ToString()))
                                //{
                                //    sSQL = "INSERT INTO POLICY_X_INSURED VALUES(" + p_iPolicyId + "," + (objData as Entity).EntityId + ")";
                                //    objCon = DbFactory.GetDbConnection(m_sConnectString);
                                //    objCon.Open();
                                //    objCon.ExecuteNonQuery(sSQL);
                                //    objCon.Close();

                                //    if (string.IsNullOrEmpty(sRecordIds))
                                //        sRecordIds = (objData as Entity).EntityId.ToString();
                                //    else
                                //        sRecordIds = sRecordIds + " " + (objData as Entity).EntityId.ToString();
                                //}

                                //if (string.IsNullOrEmpty(sUpdatedIds))
                                //    sUpdatedIds = (objData as Entity).EntityId.ToString();
                                //else
                                //    sUpdatedIds = sUpdatedIds + "," + (objData as Entity).EntityId.ToString();

                                //   break;
                                case "driver":
                                    if (!objList.Contains((objData as Entity).EntityId.ToString()))
                                    {
                                        //sSQL = "INSERT INTO POLICY_X_INSURED VALUES(" + p_iPolicyId + "," + (objData as Entity).EntityId + ")";
                                        //objCon = DbFactory.GetDbConnection(m_sConnectString);
                                        //objCon.Open();
                                        //objCon.ExecuteNonQuery(sSQL);
                                        //objCon.Close();
                                        PolicyXEntity objPolicyXEntity = (PolicyXEntity)objDmf.GetDataModelObject("PolicyXEntity", false);
                                        //using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, "SELECT POLICYENTITY_ROWID FROM POLICY_X_ENTITY WHERE POLICY_ID = " + p_iPolicyId + " AND ENTITY_ID = " + (objData as Entity).EntityId + "AND TYPE_CODE =" +  (objData as Entity).EntityTableId))
                                        //{

                                        //}

                                        //objPolicyXEntity.PolicyEid = (objData as Entity).EntityId;
                                        objPolicyXEntity.PolicyId = p_iPolicyId;
                                        objPolicyXEntity.TypeCode = (objData as Entity).EntityTableId;

                                        if (string.IsNullOrEmpty(sRecordIds))
                                            sRecordIds = (objData as Entity).EntityId.ToString();
                                        else
                                            sRecordIds = sRecordIds + " " + (objData as Entity).EntityId.ToString();
                                    }

                                    if (string.IsNullOrEmpty(sUpdatedIds))
                                        sUpdatedIds = (objData as Entity).EntityId.ToString();
                                    else
                                        sUpdatedIds = sUpdatedIds + "," + (objData as Entity).EntityId.ToString();
                                    break;
                            }
                            objData.Dispose();
                            objDoc = null;
                            iIndex = iIndex + 1;
                        }
                    }
                }

                if (objData != null)
                    objData.Dispose();

                if (p_iPolicyId != 0)
                {
                    Policy objPolicy = (Policy)objDmf.GetDataModelObject("Policy", false);
                    objPolicy.MoveTo(p_iPolicyId);
                    //sPolicyExternalId = objPolicy.ExternalPolicyId;
                    sPolicySymbol = objPolicy.PolicySymbol;
                    objPolicy.Dispose();
                }
                if (File.Exists(sFilePath))
                    File.Delete(sFilePath);

            }
            catch (RMAppException p_objException)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemList.PolicySysFetchingError",m_iClientId), p_objException);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objDoc = null;
                if (objDmf != null)
                    objDmf.Dispose();
                if (objData != null)
                    objData.Dispose();
                if (objCon != null)
                    objCon.Dispose();

            }
            return sRecordIds;

        }
        /// <summary>
        /// To validate the selected options for duplicacy
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="p_sMode"></param>
        /// <param name="p_sSelectedValues"></param>
        /// <returns></returns>
        public XmlDocument GetDuplicateOptions(string p_sMode, string p_sSelectedValues)
        {
            string sFileContent = string.Empty;
            XmlDocument objXmlDocument = null;
            string sSQL = string.Empty;
            string sFilePath = string.Empty;
            string sOuterXml = string.Empty;
            DataObject objData = null;
            DataModelFactory objDmf = null;
            string sTableName = string.Empty;
            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            XmlElement objChildElement = null;
            try
            {
                switch (p_sMode.ToLower())
                {
                    case "vehicle":
                        sSQL = "SELECT UNIT_ID,VIN,VEHICLE_MAKE,VEHICLE_MODEL,VEHICLE_YEAR FROM VEHICLE WHERE VIN IN (" + p_sSelectedValues + ")";
                        break;
                    case "property":
                        sSQL = "SELECT PROPERTY_ID,PIN,ADDR1,CITY,ZIP_CODE FROM PROPERTY_UNIT WHERE PIN IN (" + p_sSelectedValues + ")";
                        break;
                    case "entity":
                    case "driver":
                        sSQL = "SELECT ENTITY_ID,FIRST_NAME,LAST_NAME,TAX_ID FROM ENTITY WHERE TAX_ID IN (" + p_sSelectedValues + ")";
                        break;
                }
                objXmlDocument = new XmlDocument();
                objRootElement = objXmlDocument.CreateElement("Document");

                objXmlDocument.AppendChild(objRootElement);


                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    while (objReader.Read())
                    {
                        switch (p_sMode.ToLower())
                        {
                            case "vehicle":
                                objXmlElement = objXmlDocument.CreateElement(p_sMode.ToLower());
                                objRootElement.AppendChild(objXmlElement);
                                objChildElement = objXmlDocument.CreateElement("Vin");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("VIN"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("VehicleMake");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("VEHICLE_MAKE"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("VehicleModel");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("VEHICLE_MODEL"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("VehicleYear");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("VEHICLE_YEAR"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("RowId");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("UNIT_ID"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;

                                break;
                            case "property":
                                objXmlElement = objXmlDocument.CreateElement(p_sMode.ToLower());
                                objRootElement.AppendChild(objXmlElement);
                                objChildElement = objXmlDocument.CreateElement("Pin");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("PIN"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("Addr1");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("ADDR1"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("City");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("CITY"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("ZipCode");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("ZIP_CODE"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("RowId");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("PROPERTY_ID"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                break;
                            case "entity":
                            case "driver":
                                objXmlElement = objXmlDocument.CreateElement(p_sMode.ToLower());
                                objRootElement.AppendChild(objXmlElement);
                                objChildElement = objXmlDocument.CreateElement("FirstName");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("LastName");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("TaxID");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TAX_ID"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("RowId");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("ENTITY_ID"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;

                                break;
                        }
                    }
                }
                return objXmlDocument;
            }
            catch (RMAppException p_objException)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemList.PolicySysFetchingError",m_iClientId), p_objException);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDmf != null)
                    objDmf.Dispose();
                if (objData != null)
                    objData.Dispose();


            }
            return objXmlDocument;
        }

        public void DeleteOldFiles(string p_sMode,string sSessionId)
        {
            if(!string.IsNullOrEmpty(p_sMode))
                p_sMode = p_sMode.ToLower();

            switch (p_sMode)
            {
                case "entity":
                    if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "entity", sSessionId)))
                    {
                        File.Delete(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "entity", sSessionId));
                    }
                    break;
                case "unit":
                    if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "entity", sSessionId)))
                    {
                        File.Delete(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "entity", sSessionId));
                    }
                    if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "unit", sSessionId)))
                    {
                        File.Delete(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "unit", sSessionId));
                    }
                    if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "driver", sSessionId)))
                    {
                        File.Delete(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "driver", sSessionId));
                    }
                    break;
                case "driver":
                    if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "driver", sSessionId)))
                    {
                        File.Delete(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "driver", sSessionId));
                    }

                    if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "entity", sSessionId)))
                    {
                        File.Delete(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "entity", sSessionId));
                    }
                    break;
                case "unitinterest"://skhare7 Point Policy interface Unit interest
                    if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "unitinterestlist", sSessionId)))
                    {
                        File.Delete(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "unitinterestlist", sSessionId));
                    }
                    break; 
                case "coveragelist":
                    
                    if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "coveragelist", sSessionId)))
                    {
                        File.Delete(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "coveragelist", sSessionId));
                    }
                    break; 
            }


        }

        public int SavePolicy(int iPolicyId, string sSessionId)
        {
            string sFilePath = string.Empty;
            DataObject objData = null;
            DataModelFactory objDMF = null;
            string sFileContent = string.Empty;
            XmlDocument objXmlDocument = null;
            string sExternalPolicyId = string.Empty;
            XmlNode objNode = null;
            string sSQL = string.Empty;
            string sPolNumber = string.Empty;
            string sPolModule = string.Empty;
            string sPolSymbol = string.Empty;

            try
            {


                sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "policy", sSessionId);

                if (File.Exists(sFilePath))
                    sFileContent = File.ReadAllText(sFilePath);
                objXmlDocument = new XmlDocument();
                objXmlDocument.LoadXml(sFileContent);
                //objNode = objXmlDocument.SelectSingleNode("//ExternalPolicyId");
                //sExternalPolicyId = objNode.InnerText;

                //objAccordResponse = XElement.Parse(sAccordResponse);

                //oElementNode = objAccordResponse.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                //{
                //    sPolNumber = oElementNode.Value;
                //}

                //oElementNode = objAccordResponse.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");
                //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                //{
                //    sPolSymbol = oElementNode.Value;
                //}

                //oElements = objAccordResponse.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                //    if (oElements != null)
                //    {
                //        foreach (XElement oEle in oElements)
                //        {

                //            oElement = oEle.XPathSelectElement("./OtherIdTypeCd");
                //            oSubElement = oEle.XPathSelectElement("./OtherId");
                //            if ((oElement != null ) &&(oSubElement !=null) &&  string.Equals(oElement.Value, "com.csc_Module"))
                //            {
                //                sPolModule = oSubElement.Value;
                //            }
                //        }
                //    }

                //if (!string.IsNullOrEmpty(sPolModule) && !string.IsNullOrEmpty(sPolSymbol) && !string.IsNullOrEmpty(sPolNumber))
                //{
                //    if (iClaimId > 0)
                //    {
                //        sSQL = "SELECT * FROM POLICY WHERE POLICY_ID  IN ( SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId + " ) AND POLICY_SYMBOL ='" + sPolSymbol + "' AND POLICY_NUMBER ='" + sPolNumber + "' AND MODULE = " + sPolModule;

                //    }
                //    else
                //    {
                //        sSQL = "SELECT POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN ( SELECT POLICY_ID FROM CLAIM_X_POLICY ) AND POLICY_SYMBOL ='" + sPolSymbol + "' AND POLICY_NUMBER ='" + sPolNumber + "' AND MODULE = " + sPolModule;
                //    }
                //}

                //if (!string.IsNullOrEmpty(sSQL))
                //{
                //    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                //    {
                //        if (objReader.Read())
                //        {
                //            iPolicyId = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_ID"), m_iClientId);

                //        }
                //    }
                //}
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                objData = (Policy)objDMF.GetDataModelObject("Policy", false);
                objNode = null;
                if (iPolicyId > 0)
                {
                    objNode = objXmlDocument.SelectSingleNode("//PolicyId");
                    objNode.InnerText = iPolicyId.ToString();
                    objNode = null;

                    objData.MoveTo(iPolicyId);
                }

                objData.PopulateObject(objXmlDocument);

                if ((objData as Policy).PolicyId <= 0)  // insert record in policy_x_insurer table for new policy 
                {
                    if ((objData as Policy).InsurerEid != 0)
                    {
                        PolicyXInsurer objPolIns = (objData as Policy).PolicyXInsurerList.AddNew(); ;
                        objPolIns.InsurerCode = (objData as Policy).InsurerEid;
                        objPolIns.PolicyId = (objData as Policy).PolicyId;
                        objPolIns.ResPercentage = 100.0;
                        objPolIns.PrimaryInsurer = true;
                    }
                }
                else // for existing policy, 
                {
                    if ((objData as Policy).PolicyXInsurerList.Count == 0 && (objData as Policy).InsurerEid != 0) //Count = 0 => record does not exists in POLICY_X_INSURER; InsurereEid != 0 => there is insurer info to be saved
                    {
                        PolicyXInsurer objPolIns = (objData as Policy).PolicyXInsurerList.AddNew(); ;
                        objPolIns.InsurerCode = (objData as Policy).InsurerEid;
                        objPolIns.PolicyId = (objData as Policy).PolicyId;
                        objPolIns.ResPercentage = 100.0;
                        objPolIns.PrimaryInsurer = true;
                    }
                }

                objData.Save();
                if (File.Exists(sFilePath))
                    File.Delete(sFilePath);


                return ((objData as Policy).PolicyId);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDMF != null)
                    objDMF.Dispose();
                if (objData != null)
                    objData.Dispose();
                objXmlDocument = null;


            }

        }
		//rsharma220 MITS 35964 (removed orphan policy concept)
		// aaggarwal29: MITS 34580 ( added new parameter bIsOrphan to the method definition)
        public int CheckPolicyDuplication(int iClaimId, string sAccordResponse, string sPolicySystemId)
        {
            string sFilePath = string.Empty;
            DataObject objData = null;
            DataModelFactory objDMF = null;
            string sFileContent = string.Empty;
            string sExternalPolicyId = string.Empty;
            int iPolicyId = 0;
            string sSQL = string.Empty;
            XElement objAccordResponse = null;
            string sPolNumber = string.Empty;
            string sPolModule = string.Empty;
            string sPolSymbol = string.Empty;
            string sMasterCompany = string.Empty;
            string sLocCompany = string.Empty;
            XElement oElementNode = null;
            //bIsOrphan = false;
            int iCount = 0; 
            try
            {
                objAccordResponse = XElement.Parse(sAccordResponse);
                oElementNode = objAccordResponse.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                {
                    sPolNumber = oElementNode.Value;
                }

                oElementNode = objAccordResponse.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");
                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                {
                    sPolSymbol = oElementNode.Value;
                }

                oElementNode = objAccordResponse.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                if (oElementNode != null)
                {
                    sPolModule = oElementNode.Value;
                }
                oElementNode = objAccordResponse.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                if (oElementNode != null)
                {
                    sMasterCompany = oElementNode.Value;
                }
                oElementNode = objAccordResponse.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                if (oElementNode != null)
                {
                    sLocCompany = oElementNode.Value;
                }

                if (!string.IsNullOrEmpty(sPolModule) && !string.IsNullOrEmpty(sPolSymbol) && !string.IsNullOrEmpty(sPolNumber))
                {
                    if (iClaimId > 0)
                    {
                        
                        //sSQL = "SELECT CASE (SELECT COUNT(*) from CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId + ")	WHEN 0 THEN (SELECT MAX(POLICY_ID) POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN (SELECT POLICY_ID FROM CLAIM_X_POLICY) AND POLICY_SYMBOL ='" + sPolSymbol + "' AND POLICY_NUMBER ='" + sPolNumber + "' AND MODULE = " + sPolModule + " AND MASTER_COMPANY ='" + sMasterCompany + "' AND LOCATION_COMPANY='" + sLocCompany + "' ) ";
                        //sSQL = sSQL + "ELSE (SELECT POLICY_ID FROM POLICY WHERE POLICY_ID IN (SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId + ") AND POLICY_SYSTEM_ID= " + sPolicySystemId + " AND POLICY_SYMBOL ='" + sPolSymbol + "' AND POLICY_NUMBER ='" + sPolNumber + "' AND MODULE = " + sPolModule + " AND MASTER_COMPANY ='" + sMasterCompany + "' AND LOCATION_COMPANY='" + sLocCompany + "' ) END POLICY_ID ";

						// aaggarwal29: MITS 34580 start (modified the query to get orphan records)
                        sSQL = "SELECT COUNT(*) from CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId ;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                iCount = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                            }
                            sSQL = "";
                        }
						//rsharma220 MITS 35964 (removed orphan policy concept)
                        //if (iCount == 0)
                        //{
                        //    sSQL = "SELECT MAX(POLICY_ID) POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN (SELECT POLICY_ID FROM CLAIM_X_POLICY) AND POLICY_SYMBOL ='" + sPolSymbol + "' AND POLICY_NUMBER ='" + sPolNumber + "' AND MODULE = " + sPolModule + " AND MASTER_COMPANY ='" + sMasterCompany + "' AND LOCATION_COMPANY='" + sLocCompany + "'";
                        //    bIsOrphan = true;
                        //}
                        if (iCount > 0)
                        {
                            sSQL = "SELECT POLICY_ID FROM POLICY WHERE POLICY_ID IN (SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId + ") AND POLICY_SYSTEM_ID= " + sPolicySystemId + " AND POLICY_SYMBOL ='" + sPolSymbol + "' AND POLICY_NUMBER ='" + sPolNumber + "' AND MODULE = " + sPolModule + " AND MASTER_COMPANY ='" + sMasterCompany + "' AND LOCATION_COMPANY='" + sLocCompany + "' ";
                        }
						// aaggarwal29: MITS 34580 end
                        
                        //aaggarwal29: MITS 34580: Commenting below code as query has been modified so not required - start
                        /*
                        //Ankit Start Worked for Oracle Issue
                        if (DbFactory.GetDatabaseType(m_sConnectString) == eDatabaseType.DBMS_IS_ORACLE)
                            sSQL = string.Concat(sSQL, " FROM DUAL ");
                        //Ankit End
                         * */
                        //aaggarwal29: MITS 34580: end
                    }
					//rsharma220 MITS 35964 (removed orphan policy concept)
                    //else
                    //{
                    //    sSQL = "SELECT POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN ( SELECT POLICY_ID FROM CLAIM_X_POLICY ) AND POLICY_SYSTEM_ID =" + sPolicySystemId + "AND POLICY_SYMBOL ='" + sPolSymbol + "' AND POLICY_NUMBER ='" + sPolNumber + "' AND MODULE = " + sPolModule + " AND MASTER_COMPANY ='" + sMasterCompany + "' AND LOCATION_COMPANY='"+ sLocCompany+"'";
                    //    bIsOrphan = true; // aaggarwal29: MITS 34580
                    //}
                }

                if (!string.IsNullOrEmpty(sSQL))
                {
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                    {
                        if (objReader.Read())
                        {
                            iPolicyId = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_ID"), m_iClientId);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDMF != null)
                    objDMF.Dispose();
                if (objData != null)
                    objData.Dispose();
                oElementNode = null;
                objAccordResponse = null;
            }
            return iPolicyId;

        }
		//rsharma220 MITS 35964 Start (removed orphan policy concept)
		// aaggarwal29: MITS 34580 ( New method added)
        //public void OrphanPolicyCleanUp(int iPolicyId)
        //{
        //    string sSQL = string.Empty;
                        
        //    try
        //    {
        //        //fetch and delete coverages
        //        DbWriter oWriter = DbFactory.GetDbWriter(m_sConnectString);
        //        sSQL = "DELETE FROM POLICY_X_CVG_TYPE WHERE POLCVG_ROW_ID IN (SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID IN ( SELECT  POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT WHERE POLICY_ID = " + iPolicyId + ") )";
        //        DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);
                
        //        //fetch and delete units
        //        sSQL = "DELETE FROM POLICY_X_UNIT WHERE POLICY_ID = " + iPolicyId;
        //        DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);

        //        //fetch and delete entities(agent, policy level entities, drivers, unit level entities)
        //        sSQL = "DELETE FROM POLICY_X_ENTITY WHERE POLICY_ID = " + iPolicyId;
        //        DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);

        //    }
        //    catch (Exception p_objExp)
        //    {
        //        throw new RMAppException(Globalization.GetString("PolicySystemInterface.OrphanPolicyCleanUp.Error"), p_objExp);
        //    }
        //}
		//rsharma220 MITS 35964 End 


        public void CreateSubTypeEntity(int iEntityId,int iEntityTableId)
        {
            string sSQL = string.Empty;
            string entityTableName = string.Empty;
           // Entity objEntity = null;
            Employee objEmployee = null;
            PiEmployee objPiEmployee = null;
            Patient objPatient = null;
            PiPatient objPiPatient = null;
            MedicalStaff objMedicalStaff = null;
            Physician objPhysician = null;
            DataModelFactory objDMF = null;
            try
            {
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                //objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                //objEntity.MoveTo(iEntityId);
                entityTableName = objDMF.Context.LocalCache.GetTableName(iEntityTableId);

                switch (entityTableName.ToUpper())
                {
                    case "EMPLOYEES":
                        string sEmpNumber = string.Empty;
                        sSQL = "SELECT EMPLOYEE_EID, EMPLOYEE_NUMBER FROM EMPLOYEE WHERE EMPLOYEE_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (!objReader.Read())
                            {
                                objEmployee = (Employee)objDMF.GetDataModelObject("Employee", false);
                                sEmpNumber = "ExtE" + CommonFunctions.GetUniqueRandomNumber();
                                objEmployee.EmployeeNumber = sEmpNumber;
                                objEmployee.EmployeeEid = iEntityId;
                                objEmployee.Save();
                                objEmployee.Dispose();
                            }
                        }
                        
                        break;
                    case "PATIENTS":
                        sSQL = "SELECT PATIENT_ID FROM PATIENT WHERE PATIENT_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (!objReader.Read())
                            {
                                objPatient = (Patient)objDMF.GetDataModelObject("Patient", false);
                                objPatient.PatientEid = iEntityId;
                                objPatient.PatientAcctNo = "ExtP" + CommonFunctions.GetUniqueRandomNumber();
                                objPatient.Save();
                                objPatient.Dispose();
                            }
                        }
                        
                        break;
                  
                    case "MEDICAL_STAFF":
                        sSQL = "SELECT STAFF_EID FROM MED_STAFF WHERE STAFF_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (!objReader.Read())
                            {
                                objMedicalStaff = (MedicalStaff)objDMF.GetDataModelObject("MedicalStaff", false);
                                objMedicalStaff.StaffEid = iEntityId;
                                objMedicalStaff.MedicalStaffNumber = "ExtM" + CommonFunctions.GetUniqueRandomNumber();
                                objMedicalStaff.Save();
                                objMedicalStaff.Dispose();
                            }
                        }
                        break;
                    case "PHYSICIANS":
                        sSQL = "SELECT PHYS_EID FROM PHYSICIAN WHERE PHYS_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (!objReader.Read())
                            {
                                objPhysician = (Physician)objDMF.GetDataModelObject("Physician", false);
                                objPhysician.PhysEid = iEntityId;
                                objPhysician.PhysicianNumber = "ExtPH" + CommonFunctions.GetUniqueRandomNumber();
                                objPhysician.Save();
                                objPhysician.Dispose();
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //if (objEntity != null)
                //    objEntity.Dispose();
                if (objEmployee != null)
                    objEmployee.Dispose();
                if (objPiEmployee != null)
                    objPiEmployee.Dispose();
                if (objPatient != null)
                    objPatient.Dispose();
                if (objPiPatient != null)
                    objPiPatient.Dispose();
                if (objMedicalStaff != null)
                    objMedicalStaff.Dispose();
                if (objPhysician != null)
                    objPhysician.Dispose();
                if (objDMF != null)
                    objDMF.Dispose();
            }
        }

        public int SaveInsuredEntity(bool bIsNewPolicy, int iPolicyId, string sSessionId)
        {
            string sFilePath = string.Empty;
            //bool bReturn = false;
            Entity objData = null;
            DataModelFactory objDMF = null;
            string sFileContent = string.Empty;
            XmlDocument objXmlDocument = null;
            string sExternalPolicyId = string.Empty;
            XmlNode objNode = null;
            int iEntityId = 0;
            try
            {
                sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "insured", sSessionId);

                if (File.Exists(sFilePath))
                    sFileContent = File.ReadAllText(sFilePath);
                objXmlDocument = new XmlDocument();
                objXmlDocument.LoadXml(sFileContent);

                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                objData = (Entity)objDMF.GetDataModelObject("Entity", false);
                
                iEntityId = CheckEntityDuplication(sFileContent, "POLICY_X_INSURED", iPolicyId,string.Empty);
                if (iEntityId > 0)
                {
                    objNode = objXmlDocument.SelectSingleNode("//EntityId");
                    objNode.InnerText = iEntityId.ToString();
                    objNode = null;
                    objData.MoveTo(iEntityId);
                }
                objData.PopulateObject(objXmlDocument);
                objData.Save();
                if(File.Exists(sFilePath))
                    File.Delete(sFilePath);
                return objData.EntityId;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDMF != null)
                    objDMF.Dispose();
                if (objData != null)
                    objData.Dispose();
                objXmlDocument = null;
            }

        }

		
		
         //mits 35925 dbisht6 writing overload to avoid counter 
		public int SaveInsuredEntity(bool bIsNewPolicy, int iPolicyId, string sSessionId,bool ClientFile)
        {
            string sFilePath = string.Empty;
            //bool bReturn = false;
            Entity objData = null;
            DataModelFactory objDMF = null;
            string sFileContent = string.Empty;
            XmlDocument objXmlDocument = null;
            string sExternalPolicyId = string.Empty;
            XmlNode objNode = null;
            int iEntityId = 0;
            int iAddressSeqNum = -1;
            XmlNode xmlNodeElement = null;
            bool bAddressExists = false;
            XmlNode childNode=null;
            XElement addListNode = null;
            EntityXRole objEntityRole = null;
            try
            {
                sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "insured", sSessionId);

                if (File.Exists(sFilePath))
                    sFileContent = File.ReadAllText(sFilePath);
                objXmlDocument = new XmlDocument();
                objXmlDocument.LoadXml(sFileContent);
                if (objXmlDocument.SelectSingleNode("//Entity/AddressSequenceNumber") != null && !string.IsNullOrEmpty(objXmlDocument.SelectSingleNode("//Entity/AddressSequenceNumber").InnerText))        //Worked for JIRA(RMA-813)
					iAddressSeqNum = Convert.ToInt32(objXmlDocument.SelectSingleNode("//Entity/AddressSequenceNumber").InnerText);
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                objData = (Entity)objDMF.GetDataModelObject("Entity", false);
                //addListNode = objXmlDocument.SelectSingleNode("//Entity/EntityXAddressesList");
                childNode = objXmlDocument.SelectSingleNode("//Entity/EntityXAddressesList");
                addListNode = XElement.Parse(childNode.InnerXml.ToString());
                
                //addList.ParentNode.RemoveChild(addList);
                iEntityId = CheckEntityDuplication(sFileContent, "POLICY_X_INSURED", iPolicyId, string.Empty, ClientFile, objData.Context.LocalCache.GetTableId("POLICY_INSURED"));
                if (iEntityId > 0)
                {
                    objData.MoveTo(iEntityId);

                    objNode = objXmlDocument.SelectSingleNode("//EntityId");
                    objNode.InnerText = iEntityId.ToString();
                    objNode = null;
                }
				//RMA-8753 nshah28(Added by ashish)
                string sSearchString = CreateSearchString(sFileContent);
                int iAddressId = CommonFunctions.CheckAddressDuplication(sSearchString, m_sConnectString, m_iClientId);
                childNode.ParentNode.RemoveChild(childNode);
                objData.PopulateObject(objXmlDocument);

                if (iEntityId > 0)
                {
                    //objNode = objXmlDocument.SelectSingleNode("//EntityId");
                    //objNode.InnerText = iEntityId.ToString();
                    //objNode = null;
                  //  objData.MoveTo(iEntityId);
                   
   
                    foreach (EntityXAddresses oEntityXAddresses in objData.EntityXAddressesList)
                    {
                        oEntityXAddresses.PrimaryAddress = 0;
                        if (oEntityXAddresses.Address.AddressSeqNum == iAddressSeqNum)//RMA-8753 nshah28(Added by ashish)
                        {
                            oEntityXAddresses.PrimaryAddress = -1;
                            ////xmlNodeElement = objXmlDocument.SelectSingleNode("//AddressId");
                            //xmlNodeElement.InnerText = Convert.ToString(oEntityXAddresses.AddressId);
                            //xmlNodeElement = objXmlDocument.SelectSingleNode("//Entity/EntityXAddressesList//EntityXAddresses/EntityId");
                            //xmlNodeElement.InnerText = Convert.ToString(iEntityId);
                            ////xelement.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/AddressId").Value= oEntityXAddresses.AddressId.ToString();
                            // xelement.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/EntityId").Value = iEntityId.ToString();
								//RMA-8753 nshah28(Added by ashish) START
                                oEntityXAddresses.Address.Addr1 = addListNode.XPathSelectElement("//Addr1").Value.ToString();

                                oEntityXAddresses.Address.Addr2 = addListNode.XPathSelectElement("//Addr2").Value.ToString();
                            if (addListNode.XPathSelectElement("//Addr3") != null)
                                    oEntityXAddresses.Address.Addr3 = addListNode.XPathSelectElement("//Addr3").Value.ToString();
                            if (addListNode.XPathSelectElement("//Addr4") != null)
                                    oEntityXAddresses.Address.Addr4 = addListNode.XPathSelectElement("//Addr4").Value.ToString();
                                oEntityXAddresses.Address.City = addListNode.XPathSelectElement("//City").Value.ToString();
                                oEntityXAddresses.Address.Country = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//Country").Attribute("codeid").Value.ToString());
                                oEntityXAddresses.Address.State = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//State").Attribute("codeid").Value.ToString());
                            oEntityXAddresses.Email = addListNode.XPathSelectElement("//Email").Value.ToString();
                            oEntityXAddresses.Fax = addListNode.XPathSelectElement("//Fax").Value.ToString();
                                oEntityXAddresses.Address.County = addListNode.XPathSelectElement("//County").Value.ToString();
                                oEntityXAddresses.Address.ZipCode = addListNode.XPathSelectElement("//ZipCode").Value.ToString();
                                oEntityXAddresses.Address.AddressSeqNum = iAddressSeqNum;
                                //AA
                                //string sSearchString = CreateSearchString(sFileContent);
                                oEntityXAddresses.Address.SearchString = sSearchString;
                                oEntityXAddresses.Address.AddressId = iAddressId;
                                oEntityXAddresses.AddressId = iAddressId;
                            oEntityXAddresses.EffectiveDate = addListNode.XPathSelectElement("//EffectiveDate").Value.ToString();
                            oEntityXAddresses.ExpirationDate = addListNode.XPathSelectElement("//ExpirationDate").Value.ToString();
                           // oEntityXAddresses.PrimaryAddress = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//PrimaryAddress").Value.ToString());
                            //oEntityXAddresses.AddressSeqNum = iAddressSeqNum;
                            //RMA-8753 nshah28(Added by ashish) END


                            bAddressExists = true;

                        }

                    }
                    

                }

                if (!bAddressExists)
                {
                    EntityXAddresses entityAddrObj = (EntityXAddresses)objDMF.GetDataModelObject("EntityXAddresses", false);
					//RMA-8753 nshah28(Added by ashish) START
                    entityAddrObj.Address.Addr1 = addListNode.XPathSelectElement("//Addr1").Value.ToString();

                    entityAddrObj.Address.Addr2 = addListNode.XPathSelectElement("//Addr2").Value.ToString();
                    if (addListNode.XPathSelectElement("//Addr3") != null)
                        entityAddrObj.Address.Addr3 = addListNode.XPathSelectElement("//Addr3").Value.ToString();
                    if (addListNode.XPathSelectElement("//Addr4") != null)
                        entityAddrObj.Address.Addr4 = addListNode.XPathSelectElement("//Addr4").Value.ToString();
                    entityAddrObj.Address.City = addListNode.XPathSelectElement("//City").Value.ToString();
                    entityAddrObj.Address.Country = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//Country").Value.ToString());
                    entityAddrObj.Address.State = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//State").Value.ToString());
                    entityAddrObj.Email = addListNode.XPathSelectElement("//Email").Value.ToString();
                    entityAddrObj.Fax = addListNode.XPathSelectElement("//Fax").Value.ToString();
                    entityAddrObj.Address.County = addListNode.XPathSelectElement("//County").Value.ToString();
                    entityAddrObj.Address.ZipCode = addListNode.XPathSelectElement("//ZipCode").Value.ToString();
                    entityAddrObj.Address.Addr1 = addListNode.XPathSelectElement("//Addr1").Value.ToString();
                    entityAddrObj.Address.Addr2 = addListNode.XPathSelectElement("//Addr2").Value.ToString();
                    if (addListNode.XPathSelectElement("//Addr3") != null)
                        entityAddrObj.Address.Addr3 = addListNode.XPathSelectElement("//Addr3").Value.ToString();
                    if (addListNode.XPathSelectElement("//Addr4") != null)
                        entityAddrObj.Address.Addr4 = addListNode.XPathSelectElement("//Addr4").Value.ToString();
                    entityAddrObj.Address.City = addListNode.XPathSelectElement("//City").Value.ToString();
                    entityAddrObj.Address.Country = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//Country").Attribute("codeid").Value.ToString()); //RMA-8753 
                    entityAddrObj.Address.State = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//State").Attribute("codeid").Value.ToString()); //RMA-8753 
                    entityAddrObj.Email = addListNode.XPathSelectElement("//Email").Value.ToString();
                    entityAddrObj.Fax = addListNode.XPathSelectElement("//Fax").Value.ToString();
                    entityAddrObj.Address.County = addListNode.XPathSelectElement("//County").Value.ToString();
                    entityAddrObj.Address.ZipCode = addListNode.XPathSelectElement("//ZipCode").Value.ToString();
                    entityAddrObj.EffectiveDate = addListNode.XPathSelectElement("//EffectiveDate").Value.ToString();
                    entityAddrObj.ExpirationDate = addListNode.XPathSelectElement("//ExpirationDate").Value.ToString();
                    entityAddrObj.PrimaryAddress = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//PrimaryAddress").Value.ToString());
					//entityAddrObj.AddressSeqNum = iAddressSeqNum;
                    entityAddrObj.Address.AddressSeqNum = iAddressSeqNum;
                    //AA
                    //string sSearchString = CreateSearchString(sFileContent);
                    entityAddrObj.Address.SearchString = sSearchString;
                    entityAddrObj.Address.AddressId = iAddressId;
                    entityAddrObj.AddressId = iAddressId;
                    //RMA-8753 nshah28(Added by ashish) END

                    objData.EntityXAddressesList.Add(entityAddrObj);

                }

                //objData = null;

                //objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword);
                //objData = (Entity)objDMF.GetDataModelObject("Entity", false);
                //objData.MoveTo(iEntityId);
                //foreach (EntityXAddresses oEntityXAddresses in objData.EntityXAddressesList)
                //{
                //    oEntityXAddresses.PrimaryAddress = 0;
                //}

                //objData.PopulateObject(objXmlDocument);
                //dbisht6 start for mits 35925
                if (objXmlDocument.SelectSingleNode("//AddressSequenceNumber") != null && !string.IsNullOrEmpty(objXmlDocument.SelectSingleNode("//AddressSequenceNumber").InnerText))          //Worked for JIRA(RMA-813)
                    objData.AddressSequenceNumber = Convert.ToInt32(objXmlDocument.SelectSingleNode("//AddressSequenceNumber").InnerText);
                //dbisht6

                if (objData.Context.InternalSettings.SysSettings.UseEntityRole)
                {
                    if (objData.IsEntityRoleExists(objData.EntityXRoleList, objData.Context.LocalCache.GetTableId("POLICY_INSURED")) < 0)
                    {
                        objEntityRole = (EntityXRole)objDMF.GetDataModelObject("EntityXRole", false);
                        objEntityRole.EntityTableId = objData.Context.LocalCache.GetTableId("POLICY_INSURED");
                        objData.EntityXRoleList.Add(objEntityRole);
                    }
                }
                  objData.Save();
                if(File.Exists(sFilePath))
                    File.Delete(sFilePath);
                return objData.EntityId;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDMF != null)
                    objDMF.Dispose();
                if (objData != null)
                    objData.Dispose();
                objXmlDocument = null;
                
                if (objEntityRole != null)
                {
                    objEntityRole.Dispose();
                    objEntityRole = null;
                }
            }

        }

        //mits 35925 dbisht6 end
        public int SaveInsurerEntity( string sSessionId)
        {
            string sFilePath = string.Empty;
            Entity objData = null;
            DataModelFactory objDMF = null;
            string sFileContent = string.Empty;
            XmlDocument objXmlDocument = null;
           
            int iEntityId = 0;
            bool bDataExists = false;
            string sRefNo = string.Empty;
            EntityXRole objEntityRole = null;
            try
            {
                sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + GetFileName(m_sUserName, "insurer", sSessionId);

                if (File.Exists(sFilePath))
                    sFileContent = File.ReadAllText(sFilePath);
                objXmlDocument = new XmlDocument();
                objXmlDocument.LoadXml(sFileContent);

                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                objData = (Entity)objDMF.GetDataModelObject("Entity", false);

                sRefNo = objXmlDocument.SelectSingleNode("//ReferenceNumber").InnerText;

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT ENTITY_ID FROM ENTITY WHERE REFERENCE_NUMBER='" + sRefNo + "'"))
                {
                    if (objRdr.Read())
                    {
                        iEntityId = objRdr.GetInt32(0);
                        bDataExists = true;
                    }
                }

                if (!bDataExists)
                {
                    objData.PopulateObject(objXmlDocument);

                    if (objData.Context.InternalSettings.SysSettings.UseEntityRole)
                    {
                        objEntityRole = (EntityXRole)objDMF.GetDataModelObject("EntityXRole", false);
                        objEntityRole.EntityTableId = objData.Context.LocalCache.GetTableId("INSURERS");

                        objData.EntityXRoleList.Add(objEntityRole);
                    }
                    objData.Save();
                    iEntityId = objData.EntityId;
                }
                else
                {
                    if (objDMF.Context.InternalSettings.SysSettings.UseEntityRole)
                    {
                        objEntityRole = (EntityXRole)objDMF.GetDataModelObject("EntityXRole", false);
                        int iERRowId = objEntityRole.UpdateEntityXRole(objData.Context.LocalCache.GetTableId("INSURERS"), iEntityId);
                    }
                }
                if (File.Exists(sFilePath))
                    File.Delete(sFilePath);
                return iEntityId;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDMF != null)
                    objDMF.Dispose();
                if (objData != null)
                    objData.Dispose();
                objXmlDocument = null;
                if (objEntityRole != null)
                {
                    objEntityRole.Dispose();
                    objEntityRole = null;
                }
            }

        }


        public int SaveCoverage(string sXML,int iPolicyXCvgRowId,string p_sLossDate)
        {
            PolicyXCvgType objPolicyXCvgType = null;
            DataModelFactory objDMF = null;
            XmlDocument resultDoc = null;
            int iRecordId = 0;
            XmlNode objNode = null;
            
            //tanwar2 - mits 30910 - start
            string sQuery = string.Empty;
            int iDeductibleTypeCode = 0;
            //tanwar2 - mits 30910 - end
            try
            {
                resultDoc = new XmlDocument();
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                resultDoc.LoadXml(sXML);
                

                objPolicyXCvgType = (PolicyXCvgType)objDMF.GetDataModelObject("PolicyXCvgType", false);
                if (iPolicyXCvgRowId > 0)
                {
                    objNode = resultDoc.SelectSingleNode("//PolcvgRowId");
                    objNode.InnerText = iPolicyXCvgRowId.ToString();
                    objNode = null;

                    objPolicyXCvgType.MoveTo(iPolicyXCvgRowId);
                }
                objPolicyXCvgType.PopulateObject(resultDoc);
 //tanwar2 - mits 30910 - start
                if (objPolicyXCvgType.CoverageTypeCode > 0)
                {
                    //Changed by Nikhil.Code review changes
                    
                    sQuery = String.Concat("SELECT CODE1 FROM CODE_X_CODE WHERE CODE2=", objPolicyXCvgType.CoverageTypeCode, " AND DELETED_FLAG=0");
                    iDeductibleTypeCode = DbFactory.ExecuteAsType<int>(m_sConnectString, sQuery);
                    if (iDeductibleTypeCode == 0)
                    {
                        objPolicyXCvgType.DeductibleType = objPolicyXCvgType.Context.LocalCache.GetCodeId("None", "DEDUCTIBLE_TYPE"); // while saving itself, set Deductible type = None if no deductible has been specified.

                    }
                    else
                    {
                        objPolicyXCvgType.DeductibleType = iDeductibleTypeCode;
                    }
                }
                //tanwar2 - mits 30910 - end
                //rupal:start, mits 36081                
                objPolicyXCvgType.CoverageKey = GetCoverageKey(objPolicyXCvgType.PolicyUnitRowId, objPolicyXCvgType.CvgSequenceNo, objPolicyXCvgType.TransSequenceNo);
                //rupal:end, mits 36081
                objPolicyXCvgType.SaveDefaultCoverage = true;//rupal:start, mits 33913, by default this property will be true always
                if (ScriptFileExists)//write condition to check if script file exists at predefined location.mits 33913 start
                {
                    //if script file exists, then execute coverage after save process. 
                    //For OMIG Coverage After Save:
                    /*Parent Coverage will be passed as input parameter and
                    based on conditions written in the script, the parent coverage will either be split into multiple coverages, or some data in the parent coverage will 
                    be changes or parent coverage will not be saved at all with out even getting split.

                    if parent coverage is split, then parent coverage will not be saved and objPolicyXCvgType.SaveDefaultCoverage property will be changed to false                    
                     */
                    
                    try
                    {
                        objPolicyXCvgType.sDateOfLoss = Conversion.GetDate(p_sLossDate);
                        string sPointConnectionString = GetPointConnectionString(objPolicyXCvgType.PolicyUnitRowId);
                        if (!string.IsNullOrEmpty(sPointConnectionString))
                        {
                            objPolicyXCvgType.Context.PointConnectionString = sPointConnectionString;//we will access this in the script through context object                                                
                        }
                        else
                        {
                            Log.Write(Globalization.GetString("PolicySystemInterface.GetPointConnectionString.Error", m_iClientId), m_iClientId);
                        }
                        ExecuteCustomPolicyDownLoadScript(objPolicyXCvgType);
                    }
                    catch (Exception e)
                    {
                        Log.Write(Globalization.GetString("PolicySystemInterface.ExecuteCustomPolicyDownLoadScript.Error", m_iClientId), m_iClientId);
                    }                   
                    if (objPolicyXCvgType.SaveDefaultCoverage) //save parent coverage only if the flag is true
                    {
                objPolicyXCvgType.Save();
                    }
                }
                else
                {                    
                    objPolicyXCvgType.Save();
                }//mits 33913 end
                iRecordId = objPolicyXCvgType.PolcvgRowId;
                objPolicyXCvgType.Dispose();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDMF != null)
                    objDMF.Dispose();
                if (objPolicyXCvgType != null)
                    objPolicyXCvgType.Dispose();
            }
            return iRecordId;
        }

        //rupal:start, mits 36081
        /// <summary>
        /// Get Coverage Key which will be comma separated combination of STAT_UNIT_NUMBER,CVG_SEQ_NUM,TRANS_SEQ_NUM
        /// </summary>
        /// <param name="p_iPolicyUnitRowId">Policy Unit Row ID</param>
        /// <param name="p_sCvgSeqNum">Coverage Sequence Number</param>
        /// <param name="p_sTransSeqNum">Transaction Sequence Number</param>
        /// <returns>Coverage Key</returns>
        private string GetCoverageKey(int p_iPolicyUnitRowId,string p_sCvgSeqNum, string p_sTransSeqNum)
        {
            string sCoverageKey = string.Empty;
            string sSQL = string.Empty;
            int iUnitId = 0;
            string sUnitType = string.Empty;
            string sSarUnit = string.Empty;
            try
            {
                sSQL = "SELECT UNIT_ID,UNIT_TYPE FROM POLICY_X_UNIT WHERE POLICY_UNIT_ROW_ID=" + p_iPolicyUnitRowId;
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    if (objReader.Read())
                    {
                        iUnitId = objReader.GetInt("UNIT_ID");
                        sUnitType = Conversion.ConvertObjToStr(objReader.GetValue("UNIT_TYPE"));
                    }
                }
                sSQL = "SELECT STAT_UNIT_NUMBER FROM POINT_UNIT_DATA WHERE UNIT_ID=" + iUnitId + " AND UNIT_TYPE='" + sUnitType + "'";
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    if (objReader.Read())
                    {
                        sSarUnit = Conversion.ConvertObjToStr(objReader.GetValue("STAT_UNIT_NUMBER"));
                    }
                }
                sCoverageKey = sSarUnit + "," + p_sCvgSeqNum + "," + p_sTransSeqNum;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
            }
            return sCoverageKey;
        }
        //rupal:end, mits 36081

        //rupal:start, omig mits 33913

        /// <summary>
        /// To get Point connection details that are specified at the time of policy system setup through riskmaster UI
        /// </summary>
        /// <param name="p_iPolicyUnitRowId">Poicy Unit Row ID</param>
        /// <returns>Point connection String</returns>
        private string GetPointConnectionString(int p_iPolicyUnitRowId)
        {            
            string sDSN = string.Empty;
            string sUser = string.Empty;
            string sPwd = string.Empty;
            string sPointConnectionStr = string.Empty;
            int iPolicySystemId = 0;
            SetUpPolicySystem objPolInt = null;
            try
            {
                string[] sDSNDetails = null;

                string sSQL = "SELECT POLICY.POLICY_SYSTEM_ID FROM POLICY_X_UNIT INNER JOIN POLICY ON POLICY_X_UNIT.POLICY_ID=POLICY.POLICY_ID WHERE POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + p_iPolicyUnitRowId;
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    if (objReader.Read())
                    {
                        iPolicySystemId = objReader.GetInt(0);
                    }
                }
                if (iPolicySystemId == 0)
                    return sPointConnectionStr;//return blank connection string

                objPolInt = new SetUpPolicySystem(m_sUserName, m_sConnectString, m_iClientId);
                objPolInt.GetPolicyDSNDetails(ref sDSNDetails, iPolicySystemId);
                sPointConnectionStr = sDSNDetails[7];
                return sPointConnectionStr;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objPolInt = null;
            }
        }

        /// <summary>
        /// this function will execute custom scripts policy download. Right now only CoverageAfterSave hook is oprovided for custom script
        /// </summary>
        /// <param name="p_objPolicyXCvgType">policy Coverage Object</param>
        private void ExecuteCustomPolicyDownLoadScript(PolicyXCvgType p_objPolicyXCvgType)
        {            
            string sScriptText = string.Empty;
            string sObjectName = string.Empty;
            ScriptEngine objScriptEngine = null;
          
            try
            {
                //Log.Write("Omig coverage save process start");                
                using (StreamReader sr = new StreamReader(Path.Combine(RMConfigurator.UserDataPath, @"PolicyInterface\Scripts\PolicyDownload.vb")))
                {
                    //Log.Write("script file locate");
                    sScriptText = sr.ReadToEnd();                    
                }
                sScriptText = System.Web.HttpUtility.HtmlDecode(sScriptText);
                objScriptEngine.AddScript(sScriptText);
                objScriptEngine.StartEngine();
                //objScriptEngine.RunScriptMethod("OMIGCoverageSaveProcess", objPolicyXCvgType);
                objScriptEngine.RunScriptMethod("CoverageAfterSave", p_objPolicyXCvgType);

            }
            catch (Exception e)
            {
                throw e;
            }               
        }

        //rupal : end, mits 33913
        public int GetPSMappedCodeIDFromRMXCodeId(int RMXCodeId, string TableName, string ExceptionParameter, string sPolicySystemID) // aaggarwal29: Code mapping change
        {
            string sSQL = string.Empty;
            int PSMappedCodeID = 0;
            DbReader objDbReader = null;
            SysSettings objSettings = null;
            try
            {
                objSettings = new SysSettings(m_sConnectString,m_iClientId);
                if (objSettings.UseCodeMapping)
                {
                    sSQL = "SELECT POLICY_CODE_MAPPING.PS_CODE_ID FROM POLICY_CODE_MAPPING INNER JOIN GLOSSARY ON POLICY_CODE_MAPPING.RMX_TABLE_ID = GLOSSARY.TABLE_ID" +
                            " INNER JOIN CODES ON POLICY_CODE_MAPPING.PS_CODE_ID = CODES.CODE_ID WHERE GLOSSARY.SYSTEM_TABLE_NAME ='" + TableName +
                            "' AND POLICY_CODE_MAPPING.RMX_CODE_ID =" + RMXCodeId + " AND CODES.DELETED_FLAG <> -1"
                            + " AND POLICY_CODE_MAPPING.POLICY_SYSTEM_ID ="+sPolicySystemID; // aaggarwal29: Code mapping change
                    objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                    if (objDbReader.Read())
                    {
                        PSMappedCodeID = Conversion.ConvertObjToInt(objDbReader.GetValue(0), m_iClientId);
                    }
                }
               

                if (int.Equals(PSMappedCodeID, 0))
                {
                    if (TableName == "STATES")
                        return 0;
                    else
                    PSMappedCodeID = RMXCodeId;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(String.Format(Globalization.GetString("PolicySystemInterface.GetPSMappedCodesFromRMXCode.FetchingMappedCodesError",m_iClientId), ExceptionParameter), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                objSettings = null;
            }
            return PSMappedCodeID;
        }

        // new method added to get policy code based on rmx_code + claim_code
        public int GetPSMappedCodeIDFromRMXCodeId(int RMXCodeId, string TableName, string ExceptionParameter, string sPolicySystemID, int ClaimTypeCd)
        {
            string sSQL = string.Empty;
            int PSMappedCodeID = 0;
            DbReader objDbReader = null;
            SysSettings objSettings = null;
            try
            {
                objSettings = new SysSettings(m_sConnectString,m_iClientId);
                if (objSettings.UseCodeMapping)
                {
                    sSQL = "SELECT POLICY_CODE_MAPPING.PS_CODE_ID FROM POLICY_CODE_MAPPING INNER JOIN GLOSSARY ON POLICY_CODE_MAPPING.RMX_TABLE_ID = GLOSSARY.TABLE_ID" +
                            " INNER JOIN CODES ON POLICY_CODE_MAPPING.PS_CODE_ID = CODES.CODE_ID WHERE GLOSSARY.SYSTEM_TABLE_NAME ='" + TableName +
                            "' AND POLICY_CODE_MAPPING.RMX_CODE_ID =" + RMXCodeId + " AND CODES.DELETED_FLAG <> -1"
                            + " AND POLICY_CODE_MAPPING.POLICY_SYSTEM_ID =" + sPolicySystemID   // aaggarwal29: Code mapping change
                            + " AND POLICY_CODE_MAPPING.CLAIM_TYPE_CODE =" + ClaimTypeCd;
                    objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                    if (objDbReader.Read())
                    {
                        PSMappedCodeID = Conversion.ConvertObjToInt(objDbReader.GetValue(0), m_iClientId);
                    }
                }


                if (int.Equals(PSMappedCodeID, 0))
                {
                    if (TableName == "STATES")
                        return 0;
                    else
                        PSMappedCodeID = RMXCodeId;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(String.Format(Globalization.GetString("PolicySystemInterface.GetPSMappedCodesFromRMXCode.FetchingMappedCodesError",m_iClientId), ExceptionParameter), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                objSettings = null;
            }
            return PSMappedCodeID;
        }
        public int GetRMXCodeIdFromPSMappedCode(string PSMappedCode, string TableName, string ExceptionParameter, string sPolicySystemID)
        {
            string sSQL = string.Empty;
            int MappedRMXCodeId = 0;
            DbReader objDbReader = null;
             SysSettings objSettings = null;
            LocalCache objCache = null;
            try
            {
                objSettings = new SysSettings(m_sConnectString,m_iClientId);
                if (objSettings.UseCodeMapping)
                {
                    // Pradyumna 12/14/2013 Gap 16 - Added If Condition to check for policy Search system. If its Staging Area then we need not chk for Policy System ID
                                            sSQL = "SELECT POLICY_CODE_MAPPING.RMX_CODE_ID FROM POLICY_CODE_MAPPING INNER JOIN GLOSSARY ON POLICY_CODE_MAPPING.RMX_TABLE_ID = GLOSSARY.TABLE_ID" +  
											" INNER JOIN CODES ON POLICY_CODE_MAPPING.PS_CODE_ID = CODES.CODE_ID WHERE GLOSSARY.SYSTEM_TABLE_NAME ='" + TableName +
											 "' AND CODES.SHORT_CODE ='" + PSMappedCode + "'" + " AND CODES.DELETED_FLAG <> -1" +
                                            " AND POLICY_CODE_MAPPING.POLICY_SYSTEM_ID = " + sPolicySystemID; 
			    }
                    //aaggarwal29: Code mapping changes end 
                    // Pradyumna Gap 16 addition ends
                    objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                    if (objDbReader.Read())
                    {
                        MappedRMXCodeId = Conversion.ConvertObjToInt(objDbReader.GetValue(0), m_iClientId);
                    }
                
                if (int.Equals(MappedRMXCodeId, 0))
                {
                    objCache = new LocalCache(m_sConnectString,m_iClientId);
                    if(TableName=="STATES")
                        MappedRMXCodeId = objCache.GetStateRowID(PSMappedCode);
                    else
                        MappedRMXCodeId = objCache.GetCodeId(PSMappedCode, TableName);

                    if (int.Equals(MappedRMXCodeId, 0))
                    {
                        throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterface.GetRMXCodeIdFromPSMappedCode.FetchingMappedCodesError",m_iClientId),PSMappedCode, ExceptionParameter));
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterface.GetRMXCodeIdFromPSMappedCode.FetchingMappedCodesError",m_iClientId),PSMappedCode, ExceptionParameter), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                objSettings = null;
                if (objCache != null)
                    objCache.Dispose();
            }
            return MappedRMXCodeId;
        }

        // new method added for MITS 31746 start
        public string GetShortCode(string p_tableNm, string p_CodeDesc)
        {
            string sSQL = string.Empty;
            string sShortCd = string.Empty;
            DbReader objDbReader = null;
              LocalCache oCache = null;
              int iTableID = Int32.MinValue;
            try
            {
                oCache = new LocalCache(m_sConnectString,m_iClientId);
                iTableID = oCache.GetTableId(p_tableNm);

                sSQL = "SELECT CODES_TEXT.SHORT_CODE FROM CODES_TEXT , CODES WHERE CODES.TABLE_ID = " + iTableID +
                        "AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG <> -1 AND CODES_TEXT.CODE_DESC ='" + p_CodeDesc.Trim() + "'";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                while (objDbReader.Read())
                {
                    sShortCd = Conversion.ConvertObjToStr(objDbReader.GetValue(0));
                    break;      //need to read only first value
                    
                }
            }
            catch (Exception ex)
            {
                sShortCd = string.Empty;
                throw ex;
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader = null;
                if (oCache != null)
                    oCache = null;
            }
            return sShortCd;
        }
        // new method added for MITS 31746 end

        public XmlDocument GetEntityList()
        {
            XmlDocument objXmlDocument = null;
            XmlElement objOptionXmlElement = null;
            XmlElement objRootElement = null;
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            LocalCache objCache = null;
            try
            {
                objXmlDocument = new XmlDocument();
                objCache = new LocalCache(m_sConnectString,m_iClientId);
                sSQL = @"SELECT GT.TABLE_NAME, G.TABLE_ID 
							FROM GLOSSARY_TEXT GT, GLOSSARY G 
							WHERE GT.TABLE_ID=G.TABLE_ID AND G.GLOSSARY_TYPE_CODE=7  
                            AND GT.TABLE_ID <> " + objCache.GetTableId("DRIVERS")
                                +" ORDER BY GT.TABLE_NAME";

                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                objRootElement = objXmlDocument.CreateElement("EntityTypeList");
                objXmlDocument.AppendChild(objRootElement);

                while (objDbReader.Read())
                {

                    objOptionXmlElement = objXmlDocument.CreateElement("option");
                    objOptionXmlElement.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue(0)));
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue(1));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                }



            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader = null;
                }
                if (objCache != null)
                {

                    objCache.Dispose();
                    objCache = null;

                }
            }
            return objXmlDocument;
        }

        public int SaveWorkLossUnit(int p_iPolicyId)
        {
            int iPolicyId = 0;
            bool bAdd = true;
            string sSQL = string.Empty;
            DataModelFactory objDmf = null;
            PolicyXUnit objPolicyXUnit = null;
            int iRecordId = 0;
            try
            {
                sSQL = "SELECT POLICY_UNIT_ROW_ID,UNIT_TYPE FROM POLICY_X_UNIT WHERE POLICY_ID= " + p_iPolicyId;
                using (DbReader oReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    while (oReader.Read())
                    {
                        if (string.Equals(Conversion.ConvertObjToStr(oReader.GetValue("UNIT_TYPE")), "E", StringComparison.InvariantCultureIgnoreCase))
                        {
                            bAdd = false;
                            iRecordId = Conversion.ConvertObjToInt(oReader.GetValue("POLICY_UNIT_ROW_ID"), m_iClientId);
                            break;
                        }
                    }
                }

                if (bAdd)
                {
                    objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                    objPolicyXUnit = (PolicyXUnit)objDmf.GetDataModelObject("PolicyXUnit", false);
                    objPolicyXUnit.PolicyId = p_iPolicyId;
                    objPolicyXUnit.UnitId = 1;
                    objPolicyXUnit.UnitType = "E";

                    objPolicyXUnit.Save();
                    iRecordId = objPolicyXUnit.PolicyUnitRowId;
                }


            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objPolicyXUnit != null)
                    objPolicyXUnit.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();

            }
            return iRecordId;
        }

        public DataTable GetPolicySystemInfoById(int iPolicySystemId)
        {
            string sSQL = string.Empty;
            string sPolicySystemTypeCode = string.Empty;
            DataSet objDataset = null;
            DataTable objResult = null;
            LocalCache objCache = null;
            try
            {
                if (iPolicySystemId != 0)
                {
                    sSQL = "SELECT * FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID = " + iPolicySystemId;
                    objDataset = DbFactory.GetDataSet(m_sConnectString, sSQL,m_iClientId);

                    if (objDataset != null && objDataset.Tables[0] != null && objDataset.Tables[0].Rows.Count > 0)
                    {
                        objResult = objDataset.Tables[0];
                        if (Conversion.ConvertObjToInt(objResult.Rows[0]["POLICY_SYSTEM_CODE"], m_iClientId) > 0)
                        {
                            objCache = new LocalCache(m_sConnectString, m_iClientId);
                            sPolicySystemTypeCode = objCache.GetShortCode(Conversion.ConvertObjToInt(objResult.Rows[0]["POLICY_SYSTEM_CODE"], m_iClientId));
                            objResult.Columns.Add("POLICY_SYSTEM_TYPE", Type.GetType("System.String"));
                            objResult.Rows[0]["POLICY_SYSTEM_TYPE"] = sPolicySystemTypeCode;
                        }
                    }
                    else
                        throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemInfoById.NoRecordsFound",m_iClientId));
                }
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemInfoById.NoRecordsFound",m_iClientId), p_objExp);
            }
            finally
            {
                if (objDataset != null)
                    objDataset.Dispose();
                if (objResult != null)
                    objResult.Dispose();
            }
            return objResult;
        }

        //skhare7 R8 Policy Interface Enhancement

        public bool SavePolicyDownloadXMLData(int iTableRowId, string sXMLData, int iPolicySystemId, string sTableName)
        {



            LocalCache objCache = null;
            StringBuilder sbSQL;
            DbConnection objCon = null;
            DbCommand objCmd = null;
            DbParameter objParameter = null;
            int iTableId = 0;
            bool bReturn = false;
            int iPsRowId = 0;
            try
            {
                objCache = new LocalCache(m_sConnectString, m_iClientId);
                objCon = DbFactory.GetDbConnection(m_sConnectString);
                sbSQL = new StringBuilder();
                iTableId = objCache.GetTableId(sTableName);
                sbSQL.Append("SELECT PS_ROW_ID FROM PS_DOWNLOAD_DATA_XML WHERE TABLE_ID = " + iTableId);
                sbSQL.Append(" AND TABLE_ROW_ID = " + iTableRowId);
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString()))
                {
                    if (objReader.Read())
                    {
                        iPsRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                    }
                }
                sbSQL.Remove(0, sbSQL.Length);


                if (iPsRowId == 0)
                {
                    //Ankit Start Worked for Oracle Issue
                    if (DbFactory.GetDatabaseType(m_sConnectString) == eDatabaseType.DBMS_IS_ORACLE)
                    {
                        sbSQL.Append("INSERT INTO PS_DOWNLOAD_DATA_XML (PS_ROW_ID, TABLE_ID, TABLE_ROW_ID,POLICY_SYSTEM_ID,DTTM_RCD_LAST_UPD, DTTM_RCD_ADDED,UPDATED_BY_USER,ADDED_BY_USER, ACORD_XML) VALUES (");
                        sbSQL.Append("SEQ_PS_ROW_ID.NEXTVAL, ");
                    }
                    else
                        sbSQL.Append("INSERT INTO PS_DOWNLOAD_DATA_XML (TABLE_ID, TABLE_ROW_ID,POLICY_SYSTEM_ID,DTTM_RCD_LAST_UPD, DTTM_RCD_ADDED,UPDATED_BY_USER,ADDED_BY_USER, ACORD_XML) VALUES (");
                    //Ankit End
                    sbSQL.Append(iTableId.ToString() + ", ");
                    sbSQL.Append(iTableRowId.ToString() + ", ");
                    sbSQL.Append(iPolicySystemId.ToString() + ", '");
                    sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "', '");
                    sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "', '");
                    sbSQL.Append(m_sUserName.ToString() + "', '");
                    sbSQL.Append("',~XML~ )");

                    objCon.Open();
                    objCmd = objCon.CreateCommand();

                    objParameter = objCmd.CreateParameter();
                    objParameter.Value = sXMLData;
                    objParameter.ParameterName = "XML";
                    objCmd.Parameters.Add(objParameter);

                    objCmd.CommandText = sbSQL.ToString();
                    sbSQL.Remove(0, sbSQL.Length);
                    objCmd.ExecuteNonQuery();
                    bReturn = true;

                }
                else
                {

                    sbSQL.Append("UPDATE PS_DOWNLOAD_DATA_XML SET DTTM_RCD_LAST_UPD='" + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "'");
                    sbSQL.Append(" , UPDATED_BY_USER='" + m_sUserName.ToString() + "', ACORD_XML= ~XML~ ");
                    sbSQL.Append(" WHERE  TABLE_ID = " + iTableId);
                    sbSQL.Append(" AND TABLE_ROW_ID = " + iTableRowId);


                    objCon.Open();
                    objCmd = objCon.CreateCommand();

                    objParameter = objCmd.CreateParameter();
                    objParameter.Value = sXMLData;
                    objParameter.ParameterName = "XML";
                    objCmd.Parameters.Add(objParameter);

                    objCmd.CommandText = sbSQL.ToString();
                    sbSQL.Remove(0, sbSQL.Length);
                    objCmd.ExecuteNonQuery();
                    bReturn = true;

                }

                return bReturn;
            }

            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.SavePolicyDownloadXMLData.ExceptioninSaveXMLData",m_iClientId), p_objExp);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }

            }

        }

        public int GetPolicySystemId(int p_iPolicyId)
        {
            string sSQL = string.Empty;
            int iRecordId = 0;
            try
            {
                sSQL = "SELECT POLICY_SYSTEM_ID FROM POLICY WHERE POLICY_ID= " + p_iPolicyId;
                using (DbReader oReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    if (oReader.Read())
                    {
                        iRecordId = Conversion.ConvertObjToInt(oReader.GetValue("POLICY_SYSTEM_ID"), m_iClientId);
                    }
                }


            }
            catch (Exception e)
            {
                throw e;
            }

            return iRecordId;
        }

        public DataTable GetEndorsementDataInfo(string p_sTableName, int p_iTableRowId, int p_iPolicyId, string p_sUnitTypeCode)
        {

            DataSet objDataset = null;
            DataTable objResult = null;
            LocalCache objCache = null;
            int iTableId = 0;
            StringBuilder sbSQL;
            try
            {
                objCache = new LocalCache(m_sConnectString, m_iClientId);
                iTableId = objCache.GetTableId(p_sTableName);
                sbSQL = new StringBuilder();

                if (p_sTableName != string.Empty && p_iTableRowId != 0)
                {
                    sbSQL.Append("SELECT * FROM PS_ENDORSEMENT ");
                    switch (p_sTableName)
                    {

                        case "POLICY":
                            sbSQL.Append(" WHERE TABLE_ID = " + iTableId + " AND ROW_ID=" + p_iTableRowId + "");

                            break;
                        case "POLICY_X_CVG_TYPE":
                            //need to implement
                            break;
                        case "POLICY_X_UNIT":
                            sbSQL.Append(" , POLICY_X_UNIT  WHERE PS_ENDORSEMENT.ROW_ID=POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND TABLE_ID = " + iTableId + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + p_iTableRowId + " ");
                            sbSQL.Append(" AND POLICY_X_UNIT.POLICY_ID = " + p_iPolicyId + " AND POLICY_X_UNIT.UNIT_TYPE='" + p_sUnitTypeCode + "'");
                            break;

                    }

                }

                objDataset = DbFactory.GetDataSet(m_sConnectString, sbSQL.ToString(),m_iClientId);

                if (objDataset != null && objDataset.Tables[0] != null && objDataset.Tables[0].Rows.Count > 0)
                {
                    objResult = objDataset.Tables[0];
                    sbSQL.Remove(0, sbSQL.Length);
                }


            }

            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetEndorsementDataInfo.NoRecordsFound",m_iClientId), p_objExp);
            }
            finally
            {


                if (objDataset != null)
                {
                    objDataset.Dispose();
                    objDataset = null;
                }

                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }



            }
            return objResult;
        }

        public string GetDownLoadXMLData(string p_sTableName, int p_iTableRowId, int p_iPolicyId)
        {

            DataSet objDataset = null;
            string sObjResult = string.Empty;
            LocalCache objCache = null;
            int iTableId = 0;
            StringBuilder sbSQL;
            try
            {
                objCache = new LocalCache(m_sConnectString, m_iClientId);
                iTableId = objCache.GetTableId(p_sTableName);
                sbSQL = new StringBuilder();

                if (p_sTableName != string.Empty && p_iTableRowId != 0)
                {
                    sbSQL.Append("SELECT ACORD_XML FROM PS_DOWNLOAD_DATA_XML ");

                    switch (p_sTableName)
                    {

                        case "POLICY":
                            sbSQL.Append(" WHERE TABLE_ID = " + iTableId + " AND TABLE_ROW_ID =" + p_iTableRowId + "");

                            break;
                        case "POLICY_X_CVG_TYPE":
                            sbSQL.Append(" WHERE TABLE_ID = " + iTableId + " AND TABLE_ROW_ID =" + p_iTableRowId + "");
                            break;
                        case "POLICY_X_UNIT":
                            sbSQL.Append(" WHERE TABLE_ID = " + iTableId + " AND TABLE_ROW_ID=" + p_iTableRowId + "");
                            //sbSQL.Append(" , POLICY_X_UNIT  WHERE PS_DOWNLOAD_DATA_XML.TABLE_ROW_ID=POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND TABLE_ID = " + iTableId + " AND POLICY_X_UNIT.UNIT_ID=" + p_iTableRowId + " ");
                            //sbSQL.Append(" , AND POLICY_X_UNIT.POLICY_ID = " + p_iPolicyId + " AND POLICY_X_UNIT.UNIT_TYPE=" + p_sUnitTypeCode + " ");
                            break;
                        case "POLICY_X_ENTITY":
                            sbSQL.Append(" , POLICY_X_ENTITY WHERE POLICY_X_ENTITY.POLICYENTITY_ROWID=PS_DOWNLOAD_DATA_XML.TABLE_ROW_ID AND TABLE_ID = " + iTableId + " AND POLICY_X_ENTITY.ENTITY_ID=" + p_iTableRowId + " ");

                            break;
                        //case "PS_ENDORSEMENT":
                        //    sbSQL.Append(" , PS_ENDORSEMENT WHERE PS_ENDORSEMENT.ROW_ID=PS_DOWNLOAD_DATA_XML.TABLE_ROW_ID AND PS_ENDORSEMENT.TABLE_ID = " + iTableId + " AND PS_ENDORSEMENT.ENDORSEMENT_ID=" + p_iTableRowId + " ");
                        //    break;
                    }
                    objDataset = DbFactory.GetDataSet(m_sConnectString, sbSQL.ToString(),m_iClientId);

                }



                if (objDataset != null && objDataset.Tables[0] != null && objDataset.Tables[0].Rows.Count > 0)
                {
                    sObjResult = objDataset.Tables[0].Rows[0]["ACORD_XML"].ToString();
                    sbSQL.Remove(0, sbSQL.Length);

                }


            }

            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetDownLoadXMLData.NoRecordsFound",m_iClientId), p_objExp);
            }
            finally
            {

                if (objDataset != null)
                {
                    objDataset.Dispose();
                    objDataset = null;
                }

                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
            return sObjResult;
        }

        public bool SavePSEndorsementData(string p_sXML, int p_iPolicyId, string p_sTableName, int p_TableRowid)
        {



            LocalCache objCache = null;
            StringBuilder sbSQL;
            DbConnection objCon = null;
            DbCommand objCmd = null;
            DbParameter objParameter = null;

            int iTableId = 0;
            bool bReturn = false;

            IEnumerable<XElement> oElements = null;
            XElement oElement = null;
            string sFormNo = string.Empty;
            string sFormDesc = string.Empty;
            string sEditionDt = string.Empty;
            string sFormTextContent = string.Empty;
            string sFormDataArea = string.Empty;
            string sCscStat = string.Empty;
            string sCscUnit = string.Empty;
            string sCscBldg = string.Empty;
            string sCscLoc = string.Empty;
            string sCscInsLineCd = string.Empty;
            string sCscEZScrn = string.Empty;
            string sItrNo = string.Empty;
            string sRateOp = string.Empty;
            string sEntryDte = string.Empty;
            DbWriter writer = null;
            string sInsertedFormNo = string.Empty;
            try
            {
                XElement oTemplate = XElement.Parse(p_sXML);
                objCache = new LocalCache(m_sConnectString, m_iClientId);
                objCon = DbFactory.GetDbConnection(m_sConnectString);                
                sbSQL = new StringBuilder();
                oElements = oTemplate.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyFormDataRs/form");
                iTableId = objCache.GetTableId(p_sTableName);
                if ((oElements != null) && (oElements.Count() > 0))
                {
                    objCon.Open();
                    sbSQL.Append("DELETE FROM  PS_ENDORSEMENT WHERE POLICY_ID=" + p_iPolicyId + "AND TABLE_ID=" + iTableId.ToString() + "AND ROW_ID=" + p_TableRowid.ToString());

                    objCmd = objCon.CreateCommand();

                    objCmd.CommandText = sbSQL.ToString();
                    sbSQL.Remove(0, sbSQL.Length);
                  
                    objCmd.ExecuteNonQuery();
                    objCmd = null;
                  //  objCon.Close();
                    
                    foreach (XElement objElement in oElements)
                    {
                        oElement = objElement.XPathSelectElement("FormNumber");
                        if (oElement != null)
                        {
                            sFormNo = oElement.Value;
                            oElement = null;
                        }
                        if (sInsertedFormNo.Contains(sFormNo + ","))
                        {
                           continue;//dublicate form data to be skipped.
                        }
                        else
                        {
                           sInsertedFormNo = sInsertedFormNo + sFormNo + ",";
                        }
                        oElement = objElement.XPathSelectElement("FormDesc");
                        if (oElement != null)
                        {
                            sFormDesc = oElement.Value;
                            oElement = null;
                        }
                        oElement = objElement.XPathSelectElement("EditionDt");
                        if (oElement != null)
                        {
                            sEditionDt = oElement.Value;
                            oElement = null;
                        }
                        oElement = objElement.XPathSelectElement("FormTextContent");
                        if (oElement != null)
                        {
                            sFormTextContent = oElement.Value;
                            oElement = null;
                        }
                        oElement = objElement.XPathSelectElement("FormDataArea");
                        if (oElement != null)
                        {
                            sFormDataArea = oElement.Value;
                            oElement = null;
                        }
                        oElement = objElement.XPathSelectElement("IterationNumber");
                        if (oElement != null)
                        {
                            sItrNo = oElement.Value;
                            oElement = null;
                        }

                        oElement = objElement.XPathSelectElement("com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Stat']/OtherId");
                        if (oElement != null)
                        {
                            sCscStat = oElement.Value;
                            oElement = null;

                        }
                        oElement = objElement.XPathSelectElement("com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Unit']/OtherId");
                        if (oElement != null)
                        {

                            sCscUnit = oElement.Value;
                            oElement = null;

                        }
                        oElement = objElement.XPathSelectElement("com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Bldg']/OtherId");
                        if (oElement != null)
                        {
                            sCscBldg = oElement.Value;
                            oElement = null;


                        }
                        oElement = objElement.XPathSelectElement("com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Loc']/OtherId");
                        if (oElement != null)
                        {
                            sCscLoc = oElement.Value;
                            oElement = null;

                        }
                        oElement = objElement.XPathSelectElement("com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                        if (oElement != null)
                        {
                            sCscInsLineCd = oElement.Value;
                            oElement = null;

                        }
                        oElement = objElement.XPathSelectElement("com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_EZScrn']/OtherId");
                        if (oElement != null)
                        {
                            sCscEZScrn = oElement.Value;
                            oElement = null;
                        }
                        oElement = objElement.XPathSelectElement("com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RATEOP']/OtherId");
                        if (oElement != null)
                        {
                            sRateOp = oElement.Value;
                            oElement = null;
                        }
                        oElement = objElement.XPathSelectElement("com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_ENTRYDTE']/OtherId");
                        if (oElement != null)
                        {
                            sEntryDte = oElement.Value;
                            oElement = null;
                        }
                       // objCon.Open();
                        writer = DbFactory.GetDbWriter(objCon);
                       // sbSQL.Append("INSERT INTO  PS_ENDORSEMENT (POLICY_ID, TABLE_ID,ROW_ID,FORM_NUMBER,FORM_DESCRIPTION,STAT,INS_LINE,LOC, ");
                       // sbSQL.Append(" BLDG, UNIT,FORM_ACTION,EZ_SCM,DATA,EDITIONDATE,ITERATIVE, DTTM_RCD_ADDED,ADDED_BY_USER) VALUES (");
                        writer.Tables.Add("PS_ENDORSEMENT");
                        //Ankit Start Worked for Oracle Issue
                        if (DbFactory.GetDatabaseType(m_sConnectString) == eDatabaseType.DBMS_IS_ORACLE)
                        {
                            writer.Fields.Add("ENDORSEMENT_ID", DbFactory.ExecuteScalar(objCon.ConnectionString, "SELECT SEQ_ENDORSEMENT_ID.NEXTVAL FROM DUAL"));
                        }
                        //Ankit End
                        writer.Fields.Add("POLICY_ID", p_iPolicyId);
                        writer.Fields.Add("TABLE_ID", iTableId);
                        writer.Fields.Add("ROW_ID", p_TableRowid);
                        writer.Fields.Add("FORM_NUMBER", sFormNo);
                        writer.Fields.Add("FORM_DESCRIPTION", sFormDesc);
                        writer.Fields.Add("STAT", sCscStat);
                        writer.Fields.Add("INS_LINE", sCscInsLineCd);
                        writer.Fields.Add("LOC", sCscLoc);
                        writer.Fields.Add("BLDG", sCscBldg);
                        writer.Fields.Add("UNIT", sCscUnit);
                        writer.Fields.Add("FORM_ACTION", sFormTextContent);
                        writer.Fields.Add("EZ_SCM", sCscEZScrn);
                        writer.Fields.Add("DATA", sFormDataArea);
                        writer.Fields.Add("EDITIONDATE", sEditionDt);
                        writer.Fields.Add("ITERATIVE", sItrNo);
                        writer.Fields.Add("DTTM_RCD_ADDED", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                        writer.Fields.Add("ADDED_BY_USER", m_sUserName.ToString());
                        writer.Fields.Add("RATEOP", sRateOp);
                        writer.Fields.Add("ENTRYDTE", sEntryDte);
                        writer.Execute();
                        //sbSQL.Append(p_iPolicyId.ToString() + ", ");
                        //sbSQL.Append(iTableId.ToString() + ", ");
                        //sbSQL.Append(p_TableRowid.ToString() + ", ");
                        //sbSQL.Append("~FORMNO~" + " , ");    //sbSQL.Append(sFormNo + "', '");
                        ////sbSQL.Append(iTableId + "', '");
                        //sbSQL.Append("~FORMDESC~" + " , ");    //sbSQL.Append(sFormDesc + "', '");
                        //sbSQL.Append("~CSCSTAT~" + " , ");//sbSQL.Append(sCscStat + "', '");
                        //sbSQL.Append("~INSLINE~" + " , ");        //sbSQL.Append(sCscInsLineCd + "', '");
                        //sbSQL.Append("~LOC~" + " , ");    //sbSQL.Append(sCscLoc + "', '");
                        //sbSQL.Append("~BLDG~" + " , ");  //sbSQL.Append(sCscBldg + "', '");
                        //sbSQL.Append("~UNIT~" + " , ");    //sbSQL.Append(sCscUnit + "', '");
                        //sbSQL.Append("~TEXT~" + " , "); //sbSQL.Append(sFormTextContent + "', '");
                        //sbSQL.Append("~Ez~" + " , ");                            //sbSQL.Append(sCscEZScrn + "', '");
                        //sbSQL.Append("~FORMDATA~" + ",");//sbSQL.Append(sFormDataArea + "', '");
                        //sbSQL.Append("~DATE~" + " , "); //sbSQL.Append(sEditionDt + "', '");
                        //sbSQL.Append("~ITRNO~" + " , '"); //sbSQL.Append(sItrNo + "', '");
                        //sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "', '");

                        //sbSQL.Append(m_sUserName.ToString() + "')");


                        //objCmd = objCon.CreateCommand();

                        //objParameter = objCmd.CreateParameter();
                        //objParameter.Value = sFormNo;
                        //objParameter.ParameterName = "FORMNO";
                        //objCmd.Parameters.Add(objParameter);

                        //objParameter = objCmd.CreateParameter();
                        //objParameter.Value = sFormDesc;
                        //objParameter.ParameterName = "FORMDESC";
                        //objCmd.Parameters.Add(objParameter);

                        //objParameter = objCmd.CreateParameter();
                        //objParameter.Value = sCscStat;
                        //objParameter.ParameterName = "CSCSTAT";
                        //objCmd.Parameters.Add(objParameter);
                        
                        //objParameter = objCmd.CreateParameter();
                        //objParameter.Value = sCscInsLineCd;
                        //objParameter.ParameterName = "INSLINE";
                        //objCmd.Parameters.Add(objParameter);
                        
                        //objParameter = objCmd.CreateParameter();
                        //objParameter.Value = sCscLoc;
                        //objParameter.ParameterName = "LOC";
                        //objCmd.Parameters.Add(objParameter);

                        //objParameter = objCmd.CreateParameter();
                        //objParameter.Value = sCscBldg;
                        //objParameter.ParameterName = "BLDG";
                        //objCmd.Parameters.Add(objParameter);

                        //objParameter = objCmd.CreateParameter();
                        //objParameter.Value = sCscUnit;
                        //objParameter.ParameterName = "UNIT";
                        //objCmd.Parameters.Add(objParameter);

                        //objParameter = objCmd.CreateParameter();
                        //objParameter.Value = sFormTextContent;
                        //objParameter.ParameterName = "TEXT";
                        //objCmd.Parameters.Add(objParameter);

                        //objParameter = objCmd.CreateParameter();
                        //objParameter.Value = sCscEZScrn;
                        //objParameter.ParameterName = "Ez";
                        //objCmd.Parameters.Add(objParameter);

                        //objParameter = objCmd.CreateParameter();
                        //objParameter.Value = sFormDataArea;
                        //objParameter.ParameterName = "FORMDATA";
                        //objCmd.Parameters.Add(objParameter);

                        //objParameter = objCmd.CreateParameter();
                        //objParameter.Value = sEditionDt;
                        //objParameter.ParameterName = "DATE";
                        //objCmd.Parameters.Add(objParameter);

                        //objParameter = objCmd.CreateParameter();
                        //objParameter.Value = sItrNo;
                        //objParameter.ParameterName = "ITRNO";
                        //objCmd.Parameters.Add(objParameter);


                        //objCmd.CommandText = sbSQL.ToString();
                        //sbSQL.Remove(0, sbSQL.Length);
                        //objCmd.ExecuteNonQuery();
                      //  objCon.Close();
                    }
                    //objCon.Close();
                    
                }
                bReturn = true;


                return bReturn;
            }

            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.SaveEndorsementData.ExceptioninSaveXMLData", m_iClientId), p_objExp);
            }
            finally
            {

                writer = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Close();
                    objCon.Dispose();
                }

            }

        }

        public string GetDownLoadXMLNodeSpecificData(string p_sTableName, int p_iTableRowId, int p_iPolicyId, string sNode, bool bItemFlag,string p_sXML)
        {

            string sObjResult = string.Empty;
            string sXMLData = string.Empty;
            XElement Content = null;
            XElement objElement = null;

            try
            {

                if (p_sTableName != string.Empty && p_iTableRowId != 0)
                {
                    if (p_sXML == string.Empty)
                        sXMLData = GetDownLoadXMLData(p_sTableName, p_iTableRowId, p_iPolicyId);
                    else
                        sXMLData = p_sXML;
                    if (sXMLData != string.Empty)
                    {
                        Content = XElement.Parse(sXMLData);
                        if (sNode != string.Empty)
                        {
                            if (!bItemFlag)
                                objElement = Content.XPathSelectElement("//" + sNode + "");
                            else
                            {
                                objElement = Content.XPathSelectElement("//com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='" + sNode + "']/OtherId");
                                if (objElement == null)
                                {
                                    objElement = Content.XPathSelectElement("//ItemIdInfo/OtherIdentifier[OtherIdTypeCd='" + sNode + "']/OtherId");
                                }
                            }
                            sObjResult = objElement.Value;

                        }

                    }
                }

            }

            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetDownLoadXMLData.NoDataFound",m_iClientId), p_objExp);
            }
            finally
            {
                objElement = null;


            }
            return sObjResult;
        }
        //skhare7  Policy Interface End
        // mits 35925 start dbisht6
        public int CheckDriverDuplication(string DriverCheckFieldsString) 
        {
            int DriverEntityId = 0;
            XElement xele = XElement.Parse(DriverCheckFieldsString);
            string sInsuranceLine = xele.XPathSelectElement("//InsuranceLine").Value.ToString();
            string sLocationCompany = xele.XPathSelectElement("//LocationCompany").Value.ToString();
            string sMasterCompany = xele.XPathSelectElement("//MasterCompany").Value.ToString();
            string sPolicySymbol = xele.XPathSelectElement("//PolicySymbol").Value.ToString();
            string sPolicyNumber = xele.XPathSelectElement("//PolicyNumber").Value.ToString();
            string sPolicyModule = xele.XPathSelectElement("//PolicyModule").Value.ToString();
            string sRiskLocationNumber = xele.XPathSelectElement("//RiskLocationNumber").Value.ToString();
            string sRiskSubLocationNumber = xele.XPathSelectElement("//RiskSubLocationNumber").Value.ToString();
            string sProduct = xele.XPathSelectElement("//Product").Value.ToString();
            int    sDriverID = Convert.ToInt32(xele.XPathSelectElement("//DriverID").Value.ToString());
            string sRecordStatus = xele.XPathSelectElement("//RecordStatus").Value.ToString();
            StringBuilder sSql = new StringBuilder();
            Dictionary<string, dynamic> dictParam = new Dictionary<string, dynamic>();
                
            try
            {
                sSql.Append(" SELECT DRIVER_EID FROM DRIVER ");
                sSql.Append(" JOIN POLICY_X_ENTITY ON DRIVER.DRIVER_EID = POLICY_X_ENTITY.ENTITY_ID ");
                sSql.Append(" JOIN POLICY ON POLICY.POLICY_ID = POLICY_X_ENTITY.POLICY_ID ");
                sSql.Append(" JOIN ENTITY ON DRIVER.DRIVER_EID = ENTITY.ENTITY_ID");
                sSql.Append(" WHERE DRIVER.PS_INS_LINE = ~INSLINE~ ");
                dictParam.Add("INSLINE", sInsuranceLine);
                sSql.Append(" AND DRIVER.PS_RISK_LOC = ~RISKLOC~");
                dictParam.Add("RISKLOC", sRiskLocationNumber);
                sSql.Append(" AND DRIVER.PS_RISK_SUB_LOC = ~RISKSUBLOC~ ");
                dictParam.Add("RISKSUBLOC", sRiskSubLocationNumber);
                sSql.Append(" AND DRIVER.PS_PRODUCT = ~PRODUCT~ ");
                dictParam.Add("PRODUCT", sProduct);
                sSql.Append(" AND DRIVER.PS_DRIVER_ID = ~DRIVERID~ ");
                dictParam.Add("DRIVERID", sDriverID);
                sSql.Append(" AND DRIVER.PS_RECORD_STATUS = ~RECORDSTATUS~ ");
                dictParam.Add("RECORDSTATUS", sRecordStatus);
                sSql.Append(" AND POLICY.LOCATION_COMPANY = ~LOCATIONCOMPANY~ ");
                dictParam.Add("LOCATIONCOMPANY",sLocationCompany);
                sSql.Append(" AND POLICY.MASTER_COMPANY   = ~MASTERCOMPANY~ ");
                dictParam.Add("MASTERCOMPANY",sMasterCompany);
                sSql.Append(" AND POLICY.POLICY_SYMBOL = ~POLICYSYMBOL~ ");
                dictParam.Add("POLICYSYMBOL", sPolicySymbol);
                sSql.Append(" AND POLICY.POLICY_NUMBER = ~POLICYNUMBER~ ");
                dictParam.Add("POLICYNUMBER",sPolicyNumber);
                sSql.Append(" AND POLICY.MODULE = ~POLICYMODULE~ ");
                dictParam.Add("POLICYMODULE",sPolicyModule);
                sSql.Append(" AND ENTITY.DELETED_FLAG <> -1 ");
                
                if (!string.IsNullOrEmpty(sSql.ToString()))
                    DriverEntityId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, sSql.ToString(), dictParam), m_iClientId);

                return DriverEntityId;
            }
            catch (Exception e) { throw e; }
            finally
            {
                dictParam = null;
                sSql = null;
                
            }
        }
       // mits 35925 end
	   


	    
 public int SaveEntity(string sXMl,int iPolicyId,string sMode,int iEntityTableId)
        {
            int iRecordId = 0;
            Entity objEntity = null;
            DataModelFactory objDmf = null;
            XmlDocument objDoc = null;
            int iEntityId = 0;
            try
            {
                objDoc = new XmlDocument();
                objDoc.LoadXml(sXMl);
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);

                objEntity = (Entity)objDmf.GetDataModelObject("Entity", false);

                if (string.Equals(sMode,"driver",StringComparison.InvariantCultureIgnoreCase))
                    iEntityId = CheckEntityDuplication(sXMl, "DRIVER", iPolicyId, sMode);
                else
                    iEntityId = CheckEntityDuplication(sXMl, "POLICY_X_ENTITY", iPolicyId, sMode);
                if (iEntityId > 0)
                {
                    XmlNode objNode = objDoc.SelectSingleNode("//EntityId");
                    objNode.InnerText = iEntityId.ToString();
                    objNode = null;
                    objEntity.MoveTo(iEntityId);
                }
                objEntity.PopulateObject(objDoc);
                objEntity.Save();
                iRecordId = objEntity.EntityId;
                CreateSubTypeEntity(iRecordId, iEntityTableId);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objEntity != null)
                    objEntity.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                objDoc = null;
            }
            return iRecordId;
        }
	   
	   
	   //mits 35925 start dbisht6: adding overload to avoid conflicts in enhancement
        public int SaveEntity(string sXMl,int iPolicyId,string sMode,ref int iAddressId,bool bClientFile,int iEntityTableId)
        {
            int iRecordId = 0;
            Entity objEntity = null;
            DataModelFactory objDmf = null;
            XmlDocument objDoc = null;
            int iEntityId = 0;
            XElement xelement=null;
            xelement=XElement.Parse(sXMl);
            XmlNode xmlNodeElement = null;
            int iAddressSeqNum = -1;
            XmlNode xDriverCheckFieldsNode;
            XmlNode childNode = null;
            XElement addListNode = null;
            XmlNode objNode = null;
            bool bAddressExists = false;
            EntityXRole objEntityRole = null;
           try
            {
                objDoc = new XmlDocument();
                objDoc.LoadXml(sXMl);
               
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);

                objEntity = (Entity)objDmf.GetDataModelObject("Entity", false);
                childNode = objDoc.SelectSingleNode("//Entity/EntityXAddressesList");
                addListNode = XElement.Parse(childNode.InnerXml.ToString());
               
                if (string.Equals(sMode, "driver", StringComparison.InvariantCultureIgnoreCase))
                {
                    xDriverCheckFieldsNode = objDoc.SelectSingleNode("//com.csc_DriverDupCheckFields");
                    iEntityId = CheckDriverDuplication((xDriverCheckFieldsNode.OuterXml).ToString());
                    objDoc.SelectSingleNode("//Instance").RemoveChild(xDriverCheckFieldsNode);
                }
                else
                {
                    if (xelement.XPathSelectElement("./Entity/AddressSequenceNumber") != null && !string.IsNullOrEmpty(xelement.XPathSelectElement("./Entity/AddressSequenceNumber").Value))            //Worked for JIRA(RMA-813)
                        iAddressSeqNum= Convert.ToInt32(xelement.XPathSelectElement("./Entity/AddressSequenceNumber").Value);
                     iEntityId = CheckEntityDuplication(sXMl, "POLICY_X_ENTITY", iPolicyId, sMode,bClientFile,iEntityTableId);
                }
				//RMA-8753 nshah28(Added by ashish)
               string sSearchString = CreateSearchString(childNode.InnerXml.ToString());
               iAddressId = CommonFunctions.CheckAddressDuplication(sSearchString, m_sConnectString, m_iClientId); //RMA-8753
                if (iEntityId > 0)
                {
                    objEntity.MoveTo(iEntityId);

                    objNode = objDoc.SelectSingleNode("//EntityId");
                    objNode.InnerText = iEntityId.ToString();
                    objNode = null;
                }
                childNode.ParentNode.RemoveChild(childNode);
                objEntity.PopulateObject(objDoc);


                    if (iEntityId > 0)
                    {
                        //objNode = objDoc.SelectSingleNode("//EntityId");
                        //objNode.InnerText = iEntityId.ToString();
                        //objNode = null;
                        //objEntity.MoveTo(iEntityId);
                        //objEntity.AddressSequenceNumber = iAddressSeqNum;
                       
                        foreach (EntityXAddresses oEntityXAddresses in objEntity.EntityXAddressesList)
                        {
                            oEntityXAddresses.PrimaryAddress = 0;
                           if (oEntityXAddresses.Address.AddressSeqNum== iAddressSeqNum)//RMA-8753 nshah28(Added by ashish)
                            {
                                oEntityXAddresses.PrimaryAddress = -1;
                                //xmlNodeElement = objDoc.SelectSingleNode("//AddressId");
                                //xmlNodeElement.InnerText = Convert.ToString(oEntityXAddresses.AddressId);
                                //xmlNodeElement = objDoc.SelectSingleNode("//Entity/EntityXAddressesList/EntityXAddresses/EntityId");
                                //xmlNodeElement.InnerText = Convert.ToString(iEntityId);
                                
                                ////xmlNodeElement = objXmlDocument.SelectSingleNode("//AddressId");
                                //xmlNodeElement.InnerText = Convert.ToString(oEntityXAddresses.AddressId);
                                //xmlNodeElement = objXmlDocument.SelectSingleNode("//Entity/EntityXAddressesList//EntityXAddresses/EntityId");
                                //xmlNodeElement.InnerText = Convert.ToString(iEntityId);
                                ////xelement.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/AddressId").Value= oEntityXAddresses.AddressId.ToString();
                                // xelement.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/EntityId").Value = iEntityId.ToString();
									//RMA-8753 nshah28(Added by ashish)
                                    oEntityXAddresses.Address.Addr1 = addListNode.XPathSelectElement("//Addr1").Value.ToString();

                                    oEntityXAddresses.Address.Addr2 = addListNode.XPathSelectElement("//Addr2").Value.ToString();
                                if (addListNode.XPathSelectElement("//Addr3") != null)
                                        oEntityXAddresses.Address.Addr3 = addListNode.XPathSelectElement("//Addr3").Value.ToString();
                                if (addListNode.XPathSelectElement("//Addr4") != null)
                                        oEntityXAddresses.Address.Addr4 = addListNode.XPathSelectElement("//Addr4").Value.ToString();
                                    oEntityXAddresses.Address.City = addListNode.XPathSelectElement("//City").Value.ToString();
                                    oEntityXAddresses.Address.Country = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//Country").Attribute("codeid").Value.ToString()); //RMA-8753 
                                    oEntityXAddresses.Address.State = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//State").Attribute("codeid").Value.ToString()); //RMA-8753 
                                oEntityXAddresses.Email = addListNode.XPathSelectElement("//Email").Value.ToString();
                                oEntityXAddresses.Fax = addListNode.XPathSelectElement("//Fax").Value.ToString();
                                    oEntityXAddresses.Address.County = addListNode.XPathSelectElement("//County").Value.ToString();
                                    oEntityXAddresses.Address.ZipCode = addListNode.XPathSelectElement("//ZipCode").Value.ToString();
                                    oEntityXAddresses.Address.AddressSeqNum = iAddressSeqNum;
                                    //AA
                                    //sSearchString = CreateSearchString(addListNode.ToString());
                                    oEntityXAddresses.Address.SearchString = sSearchString;
                                    oEntityXAddresses.Address.AddressId = iAddressId;
                                    oEntityXAddresses.AddressId = iAddressId;
                                oEntityXAddresses.EffectiveDate = addListNode.XPathSelectElement("//EffectiveDate").Value.ToString();
                                oEntityXAddresses.ExpirationDate = addListNode.XPathSelectElement("//ExpirationDate").Value.ToString();
                                //oEntityXAddresses.PrimaryAddress = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//PrimaryAddress").Value.ToString());
                               // oEntityXAddresses.AddressSeqNum = iAddressSeqNum;
                                //RMA-8753 nshah28(Added by ashish) END


                                bAddressExists = true;
                            }

                        }
                                               
                    }

                    if (!bAddressExists)
                    {
                        EntityXAddresses entityAddrObj = (EntityXAddresses)objDmf.GetDataModelObject("EntityXAddresses", false);
						//RMA-8753 nshah28(Added by ashish) START
                            entityAddrObj.Address.Addr1 = addListNode.XPathSelectElement("//Addr1").Value.ToString();

                            entityAddrObj.Address.Addr2 = addListNode.XPathSelectElement("//Addr2").Value.ToString();
                        if(addListNode.XPathSelectElement("//Addr3")!=null)
                                entityAddrObj.Address.Addr3 = addListNode.XPathSelectElement("//Addr3").Value.ToString();
                        if (addListNode.XPathSelectElement("//Addr4") != null)
                                entityAddrObj.Address.Addr4 = addListNode.XPathSelectElement("//Addr4").Value.ToString();

                            entityAddrObj.Address.City = addListNode.XPathSelectElement("//City").Value.ToString();
                            entityAddrObj.Address.Country = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//Country").Attribute("codeid").Value.ToString()); //RMA-8753 
                            entityAddrObj.Address.State = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//State").Attribute("codeid").Value.ToString()); //RMA-8753 
                        entityAddrObj.Email = addListNode.XPathSelectElement("//Email").Value.ToString();
                        entityAddrObj.Fax = addListNode.XPathSelectElement("//Fax").Value.ToString();
                            entityAddrObj.Address.County = addListNode.XPathSelectElement("//County").Value.ToString();
                            entityAddrObj.Address.ZipCode = addListNode.XPathSelectElement("//ZipCode").Value.ToString();
                            entityAddrObj.Address.AddressSeqNum = iAddressSeqNum;
                            //AA
                            //string sSearchString = CreateSearchString(addListNode.ToString());
                            entityAddrObj.Address.SearchString = sSearchString;
                            entityAddrObj.Address.AddressId = iAddressId;
                            entityAddrObj.AddressId = iAddressId;
                        entityAddrObj.EffectiveDate = addListNode.XPathSelectElement("//EffectiveDate").Value.ToString();
                        entityAddrObj.ExpirationDate = addListNode.XPathSelectElement("//ExpirationDate").Value.ToString();
                        entityAddrObj.PrimaryAddress = Conversion.ConvertStrToInteger(addListNode.XPathSelectElement("//PrimaryAddress").Value.ToString());
                       // entityAddrObj.AddressSeqNum = iAddressSeqNum;
                        //RMA-8753 nshah28(Added by ashish) END
                        objEntity.EntityXAddressesList.Add(entityAddrObj);

                    }

                   // objEntity.PopulateObject(objDoc);


                    if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                    {
                        if (objEntity.IsEntityRoleExists(objEntity.EntityXRoleList, iEntityTableId) < 0)
                        {
                            objEntityRole = (EntityXRole)objDmf.GetDataModelObject("EntityXRole", false);
                            objEntityRole.EntityTableId = iEntityTableId;
                            objEntity.EntityXRoleList.Add(objEntityRole);
                        }
                    }
                    objEntity.Save();
                    iRecordId = objEntity.EntityId;
                    CreateSubTypeEntity(iRecordId, iEntityTableId);

                    
                    foreach (EntityXAddresses oEntityXAddresses in objEntity.EntityXAddressesList)
                    {
                        if (oEntityXAddresses.Address.AddressSeqNum == iAddressSeqNum)//RMA-8753 nshah28(Added by ashish)
                        {
                            xmlNodeElement = objDoc.SelectSingleNode("//AddressId");
                            iAddressId = Convert.ToInt32(oEntityXAddresses.AddressId);
                        }

                    }


                    //mits 35925 end

                
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objEntity != null)
                    objEntity.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                objDoc = null;
                
            }
            return iRecordId;
        }

        // mits 35925 dbisht6 : signature of function changed
        //public int StatUnitExists(int iEntityId, string iStatUnitNumber, int iPolicyId, int iPolicySystemId)
        public int StatUnitExists(string iStatUnitNumber, int iPolicyId, int iPolicySystemId)
        {
            int iUnitId=0;
            StringBuilder sbSql = new StringBuilder();
            try
            {
                //sbSql.AppendFormat("SELECT OTHER_UNIT_ID FROM OTHER_UNIT INNER JOIN POINT_UNIT_DATA ON POINT_UNIT_DATA.UNIT_ID = OTHER_UNIT_ID AND POINT_UNIT_DATA.STAT_UNIT_NUMBER = '{0}' INNER JOIN POLICY_X_UNIT ON POLICY_X_UNIT.UNIT_ID = OTHER_UNIT.OTHER_UNIT_ID AND POLICY_X_UNIT.POLICY_ID = {1} AND POLICY_X_UNIT.UNIT_TYPE='SU' INNER JOIN POLICY ON POLICY.POLICY_ID= POLICY_X_UNIT.POLICY_ID AND POLICY.POLICY_SYSTEM_ID = {2}  WHERE OTHER_UNIT.UNIT_TYPE='SU'", iStatUnitNumber, iPolicyId, iPolicySystemId);
                sbSql.AppendFormat("SELECT OTHER_UNIT_ID FROM OTHER_UNIT INNER JOIN POINT_UNIT_DATA ON POINT_UNIT_DATA.UNIT_ID = OTHER_UNIT_ID AND POINT_UNIT_DATA.STAT_UNIT_NUMBER = '{0}' INNER JOIN POLICY_X_UNIT ON POLICY_X_UNIT.UNIT_ID = OTHER_UNIT.OTHER_UNIT_ID AND POLICY_X_UNIT.POLICY_ID = {1} AND POLICY_X_UNIT.UNIT_TYPE='SU' INNER JOIN POLICY ON POLICY.POLICY_ID= POLICY_X_UNIT.POLICY_ID AND POLICY.POLICY_SYSTEM_ID = {2}  WHERE POINT_UNIT_DATA.UNIT_TYPE='SU'", iStatUnitNumber, iPolicyId, iPolicySystemId); //RMA-9195:unit test fix
                using ( DbReader objr = DbFactory.GetDbReader(m_sConnectString, sbSql.ToString()) )
                {
                    if (objr.Read())
                    {
                        iUnitId = Conversion.ConvertObjToInt(objr.GetValue(0), m_iClientId);
                    }

                }

            }
            catch(Exception e)
            {
                iUnitId=0;
            }
            return iUnitId;
        }
        // mits 35925 end
        public int SaveStatUnit(string sXMl, int iPolicyId, int iPolicySystemID, string sStatUnitNo,int iEntityTableId)
        {
            int iRecordId = 0;
            int iEntityId = 0;
            Entity objEntity = null;
            DataModelFactory objDmf = null;
            XmlDocument objDoc = null;
            OtherUnit objOtherUnit = null;
            int iUnitId=0;
            Dictionary<string, int> objReturnVal = new Dictionary<string, int>();
           // int iStatUnitNo = Int32.MinValue;
            try
            {
                objDoc = new XmlDocument();
                objDoc.LoadXml(sXMl);
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                objDoc.SelectSingleNode("//EntityXAddressesList").RemoveAll();
                objEntity = (Entity)objDmf.GetDataModelObject("Entity", false);

                // mits 35925 dbisht6 :entity duplication for stat unit removed 
                iEntityId = CheckEntityDuplication(sXMl, "POLICY_X_INSURED", iPolicyId, string.Empty, false, iEntityTableId);
                if (iEntityId > 0)
                {
                    XmlNode objNode = objDoc.SelectSingleNode("//EntityId");
                    objNode.InnerText = iEntityId.ToString();
                    objNode = null;
                    objEntity.MoveTo(iEntityId);
                }
				 objEntity.PopulateObject(objDoc);
                  objEntity.Save();
                //vsharma205- Jira RMA-7855  Starts
                  objEntity.UpdateEntityRoles(objEntity,iEntityTableId,objReturnVal);
                  //vsharma205- Jira RMA-7855  Ends
			    iUnitId = StatUnitExists(sStatUnitNo, iPolicyId, iPolicySystemID);
                if (iUnitId == 0)
                {
                   
                
                    objOtherUnit = (OtherUnit)objDmf.GetDataModelObject("OtherUnit", false);
                    objOtherUnit.EntityId = objEntity.EntityId;
                    objOtherUnit.UnitType = "SU";
                    objOtherUnit.Save();
                    iRecordId = objOtherUnit.OtherUnitId;
                }
                    //mits 35925 end
                else
                    iRecordId = iUnitId;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objEntity != null)
                    objEntity.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                if (objOtherUnit != null)
                    objOtherUnit.Dispose();
                objDoc = null;
            }
            return iRecordId;
        }

        public void SaveAgents(string sAgentNumber, string sAgentName, int iPolicyId)
        {
            Entity objEntity = null;
            DataModelFactory objDmf = null;
            //string[] arrName = null;
            int iExistingAgent_EntityId = 0;
            LocalCache objCache = null;
            EntityXRole objentityxrole = null;
            try
            {
                objCache = new LocalCache(m_sConnectString, m_iClientId);
                iExistingAgent_EntityId = AgentExists(iPolicyId, objCache.GetTableId("AGENTS"), sAgentName, sAgentNumber);
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId); //RMA-9930 : aaggarwal29
                if (iExistingAgent_EntityId == 0)
                {
                    
                    objEntity = (Entity)objDmf.GetDataModelObject("Entity", false);
                    //arrName = sAgentName.Split(' ');
                    //if (arrName.Length > 0)
                    //{
                    //    //objEntity.FirstName = sAgentName.Substring(0, sAgentName.IndexOf(arrName[arrName.Length - 1]));
                    //    objEntity.LastName = sAgentName;
                    //}
                    //else
                    //{
                    objEntity.LastName = sAgentName;
                    //}

                    objEntity.ReferenceNumber = sAgentNumber;
                    if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                    {
                        objEntity.EntityTableId = 0;
                    }
                    else
                    {
                        objEntity.EntityTableId = objCache.GetTableId("AGENTS");
                    }


                    if (objDmf.Context.InternalSettings.SysSettings.UseEntityRole)
                    {
                        objentityxrole = (EntityXRole)objDmf.GetDataModelObject("EntityXRole", false);

                        objentityxrole.EntityTableId = objCache.GetTableId("AGENTS");

                        objEntity.EntityXRoleList.Add(objentityxrole);

                    }


                    objEntity.Save();

                    SavePolicyXEntity(iPolicyId, objEntity.EntityId, objCache.GetTableId("AGENTS"), 0, "agents");
                }
                else if(iExistingAgent_EntityId >0)
                {
                    SavePolicyXEntity(iPolicyId, iExistingAgent_EntityId, objCache.GetTableId("AGENTS"), 0,"agents");
                    if (objDmf.Context.InternalSettings.SysSettings.UseEntityRole)
                    {

                        objentityxrole = (EntityXRole)objDmf.GetDataModelObject("EntityXRole", false);
                        int iErRowId = objentityxrole.UpdateEntityXRole(objCache.GetTableId("AGENTS"), iExistingAgent_EntityId);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objEntity != null)
                    objEntity.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();

                if (objentityxrole != null)
                {
                    objentityxrole.Dispose();
                    objentityxrole = null;
                }
            }
        }

        public void SaveDriver( string sLicenceDate, string sDriversLicNo, int iEntityId, int iPolicySystemId,int iMaritalStatus) // aaggarwal29: Code mapping change
        {
            Driver objDriver = null;
            DataModelFactory objDmf = null;
            int iDriverId = 0;
            LocalCache objCache = null;

            try
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString,"SELECT DRIVER_ROW_ID FROM DRIVER WHERE DRIVER_EID="+ iEntityId))
                {
                    if (objRdr.Read())
                        iDriverId = Conversion.ConvertObjToInt(objRdr.GetValue(0), m_iClientId);
                }
                
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                objCache = new LocalCache(m_sConnectString, m_iClientId);
                objDriver = (Driver)objDmf.GetDataModelObject("Driver", false);
                if (iDriverId > 0)
                    objDriver.MoveTo(iDriverId);

                objDriver.DriverTypeCode = objCache.GetCodeId("DRIVER_INSURED", "DRIVER_TYPE");
                objDriver.LicenceDate = sLicenceDate;
                objDriver.DriversLicNo = sDriversLicNo;
                objDriver.DriverEId = iEntityId;
                objDriver.MaritalStatCode = iMaritalStatus;

                objDriver.Save();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDriver != null)
                    objDriver.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }




 //mits 35925 start :dbisht6 adding overload to avoid conflicts in enhancement
        public void SaveDriver( string sLicenceDate, string sDriversLicNo, int iEntityId, int iPolicySystemId,int iMaritalStatus,XElement xDriverFields) // aaggarwal29: Code mapping change
        {
            Driver objDriver = null;
            DataModelFactory objDmf = null;
            int iDriverRowId = 0;
            LocalCache objCache = null;
            string sInsuranceLine = xDriverFields.XPathSelectElement("./InsuranceLine").Value.ToString();
            string sRiskLocation = xDriverFields.XPathSelectElement("./RiskLocationNumber").Value.ToString();
            string sRiskSubLocation = xDriverFields.XPathSelectElement("./RiskSubLocationNumber").Value.ToString();
            string sProduct = xDriverFields.XPathSelectElement("./Product").Value.ToString();
            int iPointDriverID  = Convert.ToInt32(xDriverFields.XPathSelectElement("./DriverID").Value.ToString());
            string sRecordStatus = xDriverFields.XPathSelectElement("./RecordStatus").Value.ToString();
            try
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString,"SELECT DRIVER_ROW_ID FROM DRIVER WHERE DRIVER_EID="+ iEntityId))
                {
                    if (objRdr.Read())
                        iDriverRowId = Conversion.ConvertObjToInt(objRdr.GetValue(0), m_iClientId);
                }
                
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                objCache = new LocalCache(m_sConnectString, m_iClientId);
                objDriver = (Driver)objDmf.GetDataModelObject("Driver", false);
                if (iDriverRowId > 0)
                    objDriver.MoveTo(iDriverRowId);

                objDriver.DriverTypeCode = objCache.GetCodeId("DRIVER_INSURED", "DRIVER_TYPE");
                objDriver.LicenceDate = sLicenceDate;
                objDriver.DriversLicNo = sDriversLicNo;
                objDriver.DriverEId = iEntityId;
                objDriver.MaritalStatCode = iMaritalStatus;
                objDriver.DriverID = iPointDriverID;
                objDriver.InsuranceLine = sInsuranceLine;
                objDriver.RiskLocation = sRiskLocation;
                objDriver.RiskSubLocation = sRiskSubLocation;
                objDriver.Product = sProduct;
                objDriver.RecordStatus = sRecordStatus;
                objDriver.Save();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDriver != null)
                    objDriver.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }
        // mits 35925 end 
        public string GetPolicySystemParamteres(string sParamter,int iPolicySystemId)
        {
            string sReturnValue = string.Empty;

            try
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT " + sParamter + " FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID=" + iPolicySystemId))
                {
                    if (objRdr.Read())
                        sReturnValue = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                }

               
            }
            catch (Exception e)
            {
                throw e;
            }

            return sReturnValue;
        }

        public int SaveVehicle(string sXMl, int iPolicyId, string sAccordXML)
        {
            int iRecordId = 0;
            Vehicle objVehicle = null;
            DataModelFactory objDmf = null;
            XmlDocument objDoc = null;
            PolicyXUnit objPolicyXUnit = null;
            int iPolicyUnitId = 0;
            try
            {
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                objPolicyXUnit = (PolicyXUnit)objDmf.GetDataModelObject("PolicyXUnit", false);
                iPolicyUnitId = CheckUnitDuplication(iPolicyId, sAccordXML, "V", 0, GetPolicySystemId(iPolicyId));
                if (iPolicyUnitId > 0)
                {
                    objPolicyXUnit.MoveTo(iPolicyUnitId);
                    iRecordId = objPolicyXUnit.UnitId;
                }
                else
                {
                    objDoc = new XmlDocument();
                    objDoc.LoadXml(sXMl);
                    objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);

                    objVehicle = (Vehicle)objDmf.GetDataModelObject("Vehicle", false);
                    //Check Duplicacy of Entity
                    objVehicle.PopulateObject(objDoc);
                    objVehicle.Save();
                    iRecordId = objVehicle.UnitId;
                }


            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objVehicle != null)
                    objVehicle.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                if (objPolicyXUnit != null)
                    objPolicyXUnit.Dispose();
                objDoc = null;
            }
            return iRecordId;
        }

        public int SaveSiteUnit(string sXMl, int iPolicyId, string sAccordXML)
        {
            int iRecordId = 0;
            SiteUnit objSiteUnit = null;
            DataModelFactory objDmf = null;
            XmlDocument objDoc = null;
            PolicyXUnit objPolicyXUnit = null;
            int iPolicyUnitId = 0;
            try
            {
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                objPolicyXUnit = (PolicyXUnit)objDmf.GetDataModelObject("PolicyXUnit", false);
                iPolicyUnitId = CheckUnitDuplication(iPolicyId, sAccordXML, "S", 0, GetPolicySystemId(iPolicyId));
                if (iPolicyUnitId > 0)
                {
                    objPolicyXUnit.MoveTo(iPolicyUnitId);
                    iRecordId = objPolicyXUnit.UnitId;
                }
                else
                {
                    objDoc = new XmlDocument();
                    objDoc.LoadXml(sXMl);
                    objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);

                    objSiteUnit = (SiteUnit)objDmf.GetDataModelObject("SiteUnit", false);
                    objSiteUnit.PopulateObject(objDoc);
                    objSiteUnit.Save();
                    iRecordId = objSiteUnit.SiteId;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objSiteUnit != null)
                    objSiteUnit.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                if (objPolicyXUnit != null)
                    objPolicyXUnit.Dispose();
                objDoc = null;
            }
            return iRecordId;
        }

        public int SaveProperty(string sXMl, int iPolicyId, string sAccordXML)
        {
            int iRecordId = 0;
            PropertyUnit objProperty = null;
            DataModelFactory objDmf = null;
            XmlDocument objDoc = null;
            PolicyXUnit objPolicyXUnit = null;
            int iPolicyUnitId = 0;

            try
            {   
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);
                objPolicyXUnit = (PolicyXUnit)objDmf.GetDataModelObject("PolicyXUnit", false);
                iPolicyUnitId = CheckUnitDuplication(iPolicyId, sAccordXML, "P", 0, GetPolicySystemId(iPolicyId));
                if (iPolicyUnitId > 0)
                {  
                    objPolicyXUnit.MoveTo(iPolicyUnitId);
                    iRecordId = objPolicyXUnit.UnitId;
                }
                else
                {
                    objDoc = new XmlDocument();
                    objDoc.LoadXml(sXMl);
                    objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);

                    objProperty = (PropertyUnit)objDmf.GetDataModelObject("PropertyUnit", false);
                    //Check Duplicacy of Entity
                    objProperty.PopulateObject(objDoc);
                    objProperty.Save();
                    iRecordId = objProperty.PropertyId;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objProperty != null)
                    objProperty.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                if (objPolicyXUnit != null)
                    objPolicyXUnit.Dispose();
                objDoc = null;
            }
            return iRecordId;
        }

        public int SavePolicyXUnit(int iPolicyId, int iUnitId, string sUnitType,string sAccordXML, int iPolicySystemId)
        {
            int iRecordId = 0;
            PolicyXUnit objPolicyXUnit = null;
            DataModelFactory objDmf = null;
            int iPolicyUnitId = 0;
            try
            {

                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);

                objPolicyXUnit = (PolicyXUnit)objDmf.GetDataModelObject("PolicyXUnit", false);
                iPolicyUnitId = CheckUnitDuplication(iPolicyId, sAccordXML, sUnitType, iUnitId, iPolicySystemId);
                if (iPolicyUnitId > 0)
                    objPolicyXUnit.MoveTo(iPolicyUnitId);


                objPolicyXUnit.PolicyId = iPolicyId;
                objPolicyXUnit.UnitId = iUnitId;
                objPolicyXUnit.UnitType = sUnitType;
                objPolicyXUnit.Save();
                iRecordId = objPolicyXUnit.PolicyUnitRowId;


            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objPolicyXUnit != null)
                    objPolicyXUnit.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
            }
            return iRecordId;
        }
        // Modified accessibility to Public- Pradyumna - MITS 36013
        public int AgentExists(int iPolicyId, int iEntityType, string sAgentName, string sAgentNumber)
        {
            string sSQL = string.Empty;
            StringBuilder strSQL = new StringBuilder();
            int iReturn = 0;
            DbReader objReader = null;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            SysSettings objSettings = null;
            try
            {
                sSQL = "SELECT POLICYENTITY_ROWID FROM POLICY_X_ENTITY WHERE POLICY_ID =" + iPolicyId + " AND TYPE_CODE=" + iEntityType;
                objSettings = new SysSettings(m_sConnectString, m_iClientId);
                using (objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    if (objReader.Read())
                    {
                        iReturn = -1;
                    }
                    else
                    {
                        //sSQL = "SELECT ENTITY_ID FROM ENTITY WHERE LAST_NAME ='" + sAgentName + "' AND REFERENCE_NUMBER = '" + sAgentNumber + "' AND ENTITY_TABLE_ID = " + iEntityType + " ORDER BY ENTITY_ID DESC";
                        strSQL.Append("SELECT ENTITY.ENTITY_ID FROM ENTITY");
                        strSQL.Append("  WHERE ");
                        strSQL.Append(" LAST_NAME = ~LASTNAME~");
                        strDictParams.Add("LASTNAME", sAgentName);
                        strSQL.Append(" AND REFERENCE_NUMBER = ~REFERENCENUMBER~");
                        strDictParams.Add("REFERENCENUMBER", sAgentNumber);
                        if (!objSettings.UseEntityRole)
                            strSQL.Append(" AND ENTITY_TABLE_ID = " + iEntityType);

                        strSQL.Append(" AND ENTITY.DELETED_FLAG=0 ORDER BY ENTITY_ID DESC");//Payal Added(ENTITY.DELETED_FLAG=0) : Worked for JIRA - 10471
                        iReturn = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, strSQL.ToString(), strDictParams), m_iClientId);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objSettings = null;
            }
            return iReturn;

        }

        public int SavePolicyXEntity(int iPolicyId, int iEntityId, int iEntityType,int UnitRowId,string sRoleCode)
        {
            PolicyXEntity objPolicyXEntity = null;
            DataModelFactory objDmf = null;
            int iRecordId = 0;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            StringBuilder sbSql = new StringBuilder();
            try
            {
                sbSql.Append("SELECT POLICYENTITY_ROWID FROM POLICY_X_ENTITY WHERE ");

                sbSql.Append(" ENTITY_ID=~ENTITYID~");
                strDictParams.Add("ENTITYID", iEntityId.ToString());

                sbSql.Append(" AND POLICY_ID =~POLICYID~");
                strDictParams.Add("POLICYID", iPolicyId.ToString());

                sbSql.Append(" AND TYPE_CODE =~TYPECODE~");
                strDictParams.Add("TYPECODE", iEntityType.ToString());

                sbSql.Append(" AND EXTERNAL_ROLE =~ROLECD~");
                strDictParams.Add("ROLECD", sRoleCode);

                //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT POLICYENTITY_ROWID FROM POLICY_X_ENTITY WHERE ENTITY_ID=" + iEntityId + " AND POLICY_ID =" + iPolicyId + " AND TYPE_CODE =" + iEntityType +" AND EXTERNAL_ROLE ='"+ sRoleCode+"'"))
                using (DbReader objRdr = DbFactory.ExecuteReader(m_sConnectString,sbSql.ToString(),strDictParams))
                {
                    if (objRdr.Read())
                        iRecordId = Conversion.ConvertObjToInt(objRdr.GetValue(0), m_iClientId);
                }
                

                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);

                objPolicyXEntity = (PolicyXEntity)objDmf.GetDataModelObject("PolicyXEntity", false);

                if (iRecordId > 0)
                    objPolicyXEntity.MoveTo(iRecordId);


                objPolicyXEntity.PolicyId = iPolicyId;
                objPolicyXEntity.EntityId = iEntityId;
                objPolicyXEntity.TypeCode = iEntityType;
                objPolicyXEntity.PolicyUnitRowid = UnitRowId;
                objPolicyXEntity.ExternalRole = sRoleCode;
                objPolicyXEntity.Save();
                iRecordId = objPolicyXEntity.PolicyEid;


            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objPolicyXEntity != null)
                    objPolicyXEntity.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
            }
            return iRecordId;
        }




  //mits 35925 start :dbisht6 adding overload to avoid conflicts in enhancement
        
       // public int SavePolicyXEntity(int iPolicyId, int iEntityId, int iEntityType,int UnitRowId,string sRoleCode,string s_AddSeqNum,String s_ClientSeqNum)
        public int SavePolicyXEntity(int iPolicyId, int iEntityId, int iEntityType, int UnitRowId, string sRoleCode, int iAddressId)
          {
            PolicyXEntity objPolicyXEntity = null;
            DataModelFactory objDmf = null;
            int iRecordId = 0;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            StringBuilder sbSql = new StringBuilder();
           
            try
            {
               sbSql.Append("SELECT POLICYENTITY_ROWID FROM POLICY_X_ENTITY WHERE ");
               //sbSql.Append("select PE.POLICYENTITY_ROWID,EA.ADDRESS_ID  from POLICY_X_ENTITY PE inner join ENTITY_X_ADDRESSES EA on PE.ADDRESS_ID = EA.ADDRESS_ID where ");
                sbSql.Append(" ENTITY_ID=~ENTITYID~");
                strDictParams.Add("ENTITYID", iEntityId.ToString());

                sbSql.Append(" AND POLICY_ID =~POLICYID~");
                strDictParams.Add("POLICYID", iPolicyId.ToString());

                sbSql.Append(" AND TYPE_CODE =~TYPECODE~");
                strDictParams.Add("TYPECODE", iEntityType.ToString());

                sbSql.Append(" AND EXTERNAL_ROLE =~ROLECD~");
                strDictParams.Add("ROLECD", sRoleCode);
                sbSql.Append(" AND ADDRESS_ID =~ADDRESSID~");
                strDictParams.Add("ADDRESSID",iAddressId.ToString());
                //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT POLICYENTITY_ROWID FROM POLICY_X_ENTITY WHERE ENTITY_ID=" + iEntityId + " AND POLICY_ID =" + iPolicyId + " AND TYPE_CODE =" + iEntityType +" AND EXTERNAL_ROLE ='"+ sRoleCode+"'"))
                using (DbReader objRdr = DbFactory.ExecuteReader(m_sConnectString,sbSql.ToString(),strDictParams))
                {
                    if (objRdr.Read())
                        iRecordId = Conversion.ConvertObjToInt(objRdr.GetValue(0), m_iClientId);
                }
                

                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId);

                objPolicyXEntity = (PolicyXEntity)objDmf.GetDataModelObject("PolicyXEntity", false);

                if (iRecordId > 0)
                    objPolicyXEntity.MoveTo(iRecordId);


                objPolicyXEntity.PolicyId = iPolicyId;
                objPolicyXEntity.EntityId = iEntityId;
                objPolicyXEntity.TypeCode = iEntityType;
                objPolicyXEntity.PolicyUnitRowid = UnitRowId;
                objPolicyXEntity.ExternalRole = sRoleCode;
                objPolicyXEntity.AddressId = iAddressId;
                objPolicyXEntity.Save();
                iRecordId = objPolicyXEntity.PolicyEid;


            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objPolicyXEntity != null)
                    objPolicyXEntity.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
            }
            return iRecordId;
        }
        //mits 35925 end

        // mits 35925 start
        public int GetAddressIDFromEntityID(int entityID, int iAddSeqNum) 
        {
            StringBuilder sSql = new StringBuilder();
            int iAddressID=0;
            Dictionary<string, dynamic> strDictParams = new Dictionary<string,dynamic>();
            sSql.Append(" SELECT ADDRESS_ID FROM ENTITY_X_ADDRESSES WHERE ");
            sSql.Append(" ENTITY_ID = ~ENTITYID~");
            strDictParams.Add("ENTITYID",entityID.ToString());
            sSql.Append(" AND ADDRESS_SEQ_NUM = ~ADDRESSSEQNUM~");
            strDictParams.Add("ADDRESSSEQNUM", iAddSeqNum);
            try 
            {
                using (DbReader objRdr = DbFactory.ExecuteReader(m_sConnectString, sSql.ToString(), strDictParams))
                {
                    if (objRdr.Read())
                        iAddressID = Conversion.ConvertObjToInt(objRdr.GetValue(0), m_iClientId);
                }
            
            
            }
            catch (Exception e) { throw e; }
            finally
            { 
                sSql = null;
                strDictParams = null;
            }
            return iAddressID;
        }
        //mits 35925 end
        
public int CheckEntityDuplication(string p_sXML, string p_sTableName, int p_iPolicyId, string sMode)
        {
            XElement oElement = null;
            string sLastName = string.Empty;
            string sFirstName = string.Empty;
            string sAdd = string.Empty;
            string sTaxId = string.Empty;
            string sEmail = string.Empty;
            string sContact = string.Empty;
            int iZip = 0;
            bool bSuccess = false;
            int iTableId = 0;
            XElement oElements = null;
            StringBuilder sbSql = new StringBuilder();
            LocalCache objCache = null;
            int iEntityID = 0;
            string sPolicyFields = string.Empty;
            SysSettings objSettings = null;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            try
            {
                XElement oTemplate = XElement.Parse(p_sXML);
                oElements = oTemplate.XPathSelectElement("//Entity");
                objSettings = new SysSettings(m_sConnectString, m_iClientId);
                objCache = new LocalCache(m_sConnectString, m_iClientId);
                switch (p_sTableName.ToUpper())
                {
                    case "POLICY_X_INSURED":
                        sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("PolicyHolderDupCheckFields", string.Empty);
                        break;
                    case "POLICY_X_ENTITY":
                        sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("AdditionalInterestDupCheckFields", string.Empty);
                        break;
                    case "DRIVER":
                        sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("DriverDupCheckFields", string.Empty);
                        break;
                }
                string[] arrValues = sPolicyFields.Split(',');
                sbSql.Append("SELECT ENTITY.ENTITY_ID FROM ENTITY ");
                

                sbSql.Append(" WHERE 1=1");
                for (int i = 0; i <= arrValues.Length - 1; i++)
                {
                    if (oElements != null)
                    {
                        oElement = oElements.XPathSelectElement("//" + arrValues[i]);
                        if (oElement != null)
                        {
                            if (string.Compare(arrValues[i], "LastName", false) == 0)
                            {
                                sLastName = oElement.Value;
                                if (sLastName != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.LAST_NAME= ~LASTNAME~");
                                    strDictParams.Add("LASTNAME", sLastName);
                                }
                            }
                            else if (string.Compare(arrValues[i], "FirstName", false) == 0)
                            {
                                sFirstName = oElement.Value;
                                if (sFirstName != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.FIRST_NAME= ~FIRSTNAME~");
                                    strDictParams.Add("FIRSTNAME", sFirstName);
                                }
                            }
                            else if (string.Compare(arrValues[i], "TaxId", false) == 0)
                            {
                                sTaxId = oElement.Value;
                                if (sTaxId != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.TAX_ID= ~TAXID~");
                                    strDictParams.Add("TAXID", sTaxId);
                                }
                            }

                            else if (string.Compare(arrValues[i], "Addr", false) == 0)
                            {
                                oElement = oElements.XPathSelectElement("//Addr1");
                                if (oElement != null)
                                {
                                    sAdd = oElement.Value;
                                    oElement = null;
                                }
                                oElement = oElements.XPathSelectElement("//Addr2");
                                if (oElement != null)
                                {
                                    sAdd = sAdd + oElement.Value;
                                    oElement = null;
                                }
                                oElement = oElements.XPathSelectElement("//Addr3");
                                if (oElement != null)
                                {
                                    sAdd = sAdd + oElement.Value;
                                    oElement = null;
                                }
                                oElement = oElements.XPathSelectElement("//Addr4");
                                if (oElement != null)
                                {
                                    sAdd = sAdd + oElement.Value;
                                    oElement = null;
                                }
                                if (sAdd != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.ADDR1 + ENTITY.ADDR2 + ENTITY.ADDR3 + ENTITY.ADDR4= ~ADDR~");
                                    strDictParams.Add("ADDR", sAdd);
                                }
                            }
                            else if (string.Compare(arrValues[i], "EmailAddress", false) == 0)
                            {
                                sEmail = oElement.Value;
                                if (sEmail != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.EMAIL_ADDRESS= ~EMAIL~");
                                    strDictParams.Add("EMAIL", sEmail);
                                }
                            }
                            else if (string.Compare(arrValues[i], "Contact", false) == 0)
                            {
                                sContact = oElement.Value;
                                if (sContact != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.CONTACT= ~CONTACT~");
                                    strDictParams.Add("CONTACT", sContact);
                                }
                            }
                            else if (string.Compare(arrValues[i], "ZipCode", false) == 0)
                            {
                                iZip = Conversion.CastToType<int>(oElement.Value, out bSuccess);
                                if (iZip != 0)
                                {
                                    sbSql.Append(" AND ENTITY.ZIP_CODE= ~ZIPCODE~");
                                    strDictParams.Add("ZIPCODE", iZip.ToString());
                                }
                            }
                        }
                        oElement = oElements.XPathSelectElement("//EntityTableId");
                        iTableId = Conversion.CastToType<int>(oElement.Value, out bSuccess);
                        oElement = null;
                    }
                }
                if (!objSettings.UseEntityRole)
                {
                    sbSql.Append(" AND ENTITY.ENTITY_TABLE_ID =" + iTableId);
                }

                sbSql.Append(" AND ENTITY.DELETED_FLAG=0 ORDER BY ENTITY.ENTITY_ID DESC");  //Payal Added(ENTITY.DELETED_FLAG=0) : Worked for JIRA - 10471
                if (sbSql != null)
                {
                    iEntityID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, sbSql.ToString(), strDictParams), m_iClientId);
                }

                return iEntityID;
            }

            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objSettings = null;
                if (oElements != null)
                    oElements = null;
                if (oElement != null)
                    oElement = null;
            }
        }

//RMA-8753 nshah28(Added by ashish) START

        private string CreateSearchString(string p_sXML)
        {
            XElement oElements = null;
            string strSearchString = string.Empty;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;
            DataModelFactory objDMF = null;
            string sAddr1 = string.Empty;
            string sAddr2 = string.Empty;
            string sAddr3 = string.Empty;
            string sAddr4 = string.Empty;
            string sCity = string.Empty;
            string sState = string.Empty;
            string sCountry = string.Empty;
            string sCounty = string.Empty;
            string sZipCode = string.Empty;
            string sAddressSeqNum = string.Empty;
		
                XElement oTemplate = XElement.Parse(p_sXML);
                oElements = oTemplate.XPathSelectElement("//Address");
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                if (oElements.XPathSelectElement("//Addr1") != null && !string.IsNullOrEmpty(oElements.XPathSelectElement("//Addr1").Value))
                {
                sAddr1=oElements.XPathSelectElement("//Addr1").Value;
                }
                if (oElements.XPathSelectElement("//Addr2") != null && !string.IsNullOrEmpty(oElements.XPathSelectElement("//Addr2").Value))
                {
                sAddr2=oElements.XPathSelectElement("//Addr2").Value;
                }
                if (oElements.XPathSelectElement("//Addr3") != null && !string.IsNullOrEmpty(oElements.XPathSelectElement("//Addr3").Value))
                {
                sAddr3=oElements.XPathSelectElement("//Addr3").Value;
                }
                if (oElements.XPathSelectElement("//Addr4") != null && !string.IsNullOrEmpty(oElements.XPathSelectElement("//Addr4").Value))
                {
                sAddr4=oElements.XPathSelectElement("//Addr4").Value;
                }
                if (oElements.XPathSelectElement("//City") != null && !string.IsNullOrEmpty(oElements.XPathSelectElement("//City").Value))
                {
                sCity=oElements.XPathSelectElement("//City").Value;
                }
                if (oElements.XPathSelectElement("//State") != null && oElements.XPathSelectElement("//State").Attribute("codeid") != null && !string.IsNullOrEmpty(oElements.XPathSelectElement("//State").Attribute("codeid").Value))
                {
                    objDMF.Context.LocalCache.GetStateInfo(Convert.ToInt32(oElements.XPathSelectElement("//State").Attribute("codeid").Value),ref sStateCode,ref sStateDesc);
                    sState = sStateDesc;
                }
                if (oElements.XPathSelectElement("//Country") != null && oElements.XPathSelectElement("//Country").Attribute("codeid") !=null && !string.IsNullOrEmpty(oElements.XPathSelectElement("//Country").Attribute("codeid").Value))
                {
                    sCountry = objDMF.Context.LocalCache.GetCodeDesc(Convert.ToInt32(oElements.XPathSelectElement("//Country").Attribute("codeid").Value));
                }
                if (oElements.XPathSelectElement("//County") != null && !string.IsNullOrEmpty(oElements.XPathSelectElement("//County").Value))
                {
                sCounty=oElements.XPathSelectElement("//County").Value;
                }
                if (oElements.XPathSelectElement("//ZipCode") != null && !string.IsNullOrEmpty(oElements.XPathSelectElement("//ZipCode").Value))
                {
                sZipCode=oElements.XPathSelectElement("//ZipCode").Value;
                }
                if (oElements.XPathSelectElement("//AddressSeqNum") != null && !string.IsNullOrEmpty(oElements.XPathSelectElement("//AddressSeqNum").Value))
                {
                sAddressSeqNum=oElements.XPathSelectElement("//AddressSeqNum").Value;
                }
               // strSearchString = sAddr1 + sAddr2 + sAddr3 + sAddr4 + sCity + sState + sCounty + sCountry + sZipCode + sAddressSeqNum;
                strSearchString = sAddr1 + sAddr2 + sAddr3 + sAddr4 + sCity + sState + sCountry + sZipCode + sAddressSeqNum; //Remove "County"
                strSearchString = strSearchString.Replace(" ", "").ToLower();
                return strSearchString;
            }
		//RMA-8753 nshah28(Added by ashish) END
//mits 35925 : dbisht6 adding overload to avoida conflict between enhancements
		public int CheckEntityDuplication(string p_sXML, string p_sTableName, int p_iPolicyId, string sMode,bool bClientFile,int iEntityTableId)
        {
            XElement oElement = null;
            string sLastName = string.Empty;
            string sFirstName = string.Empty;
            string sAdd = string.Empty;
            string sTaxId = string.Empty;
            string sEmail = string.Empty;
            string sContact = string.Empty;
            int iZip = 0;
            bool bSuccess = false;
            int iTableId = 0;
            XElement oElements = null;
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sqlQuery = new StringBuilder();
            LocalCache objCache = null;
            int iEntityID = 0;
            string sPolicyFields = string.Empty;
            Dictionary<string, dynamic> strDictParams = new Dictionary<string, dynamic>();
            int iClientSeqNum=-1;
            SysSettings objSettings = null;
           
            try
            {
                XElement oTemplate = XElement.Parse(p_sXML);
                objSettings = new SysSettings(m_sConnectString, m_iClientId);
                oElements = oTemplate.XPathSelectElement("//Entity");
                objCache = new LocalCache(m_sConnectString, m_iClientId);

                
                //dbisht6 mits 35925
                
                # region  client file on case


                if (oElements.XPathSelectElement("//ClientSequenceNumber") != null && !string.IsNullOrEmpty(oElements.XPathSelectElement("//ClientSequenceNumber").Value))      //Worked for JIRA(RMA-813)
                    iClientSeqNum = Convert.ToInt32(oElements.XPathSelectElement("//ClientSequenceNumber").Value);

                if ((bClientFile)&&(iClientSeqNum!=0))
                {
                    if (oElements.XPathSelectElement("//ClientSequenceNumber") != null && !string.IsNullOrEmpty(oElements.XPathSelectElement("//ClientSequenceNumber").Value))      //Worked for JIRA(RMA-813)
                        iClientSeqNum = Convert.ToInt32(oElements.XPathSelectElement("//ClientSequenceNumber").Value);        
                    //sbSql.Append(" SELECT ENTITY.ENTITY_ID FROM ENTITY WHERE 1=1 ");
                    sbSql.Append("SELECT ENTITY.ENTITY_ID FROM ENTITY ");
                    //if (objSettings.UseEntityRole)
                    //{
                    //    sbSql.Append(",ENTITY_X_ROLES");
                    //}

                    sbSql.Append(" WHERE 1=1");

                    sbSql.Append(" AND CAST(CLIENT_SEQ_NUM AS INT) = ~CLIENTSEQNUMBER~ ");
                    strDictParams.Add("CLIENTSEQNUMBER", iClientSeqNum);
                    sbSql.Append(" AND ENTITY.DELETED_FLAG <> -1 ");
                }
                #endregion
                //mits 35925 end
              

                #region   client file off case
                if ((!bClientFile)||iClientSeqNum==0)
                {
                    switch (p_sTableName.ToUpper())
                    {
                        case "POLICY_X_INSURED":
                            sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("PolicyHolderDupCheckFields", string.Empty);
                            break;
                        case "POLICY_X_ENTITY":
                            sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("AdditionalInterestDupCheckFields", string.Empty);
                            break;
                        case "DRIVER":
                            sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("DriverDupCheckFields", string.Empty);
                            break;
                    }


                    string[] arrValues = sPolicyFields.Split(',');
                    sbSql.Append("SELECT ENTITY.ENTITY_ID FROM ENTITY ");
                    //if(objSettings.UseEntityRole)
                    //{
                    //    sbSql.Append(",ENTITY_X_ROLES");
                    //}

                    sbSql.Append(" WHERE 1=1 AND ENTITY.DELETED_FLAG=0 "); //Payal Added(ENTITY.DELETED_FLAG=0) : Worked for JIRA - 10471
                    for (int i = 0; i <= arrValues.Length - 1; i++)
                    {
                        if (oElements != null)
                        {
                            oElement = oElements.XPathSelectElement("//" + arrValues[i]);
                            if (oElement != null)
                            {
                                if (string.Compare(arrValues[i], "LastName", false) == 0)
                                {
                                    sLastName = oElement.Value;
                                    if (sLastName != string.Empty)
                                    {
                                        sbSql.Append(" AND ENTITY.LAST_NAME= ~LASTNAME~");
                                        strDictParams.Add("LASTNAME", sLastName);
                                    }
                                }
                                else if (string.Compare(arrValues[i], "FirstName", false) == 0)
                                {
                                    sFirstName = oElement.Value;
                                    if (sFirstName != string.Empty)
                                    {
                                        sbSql.Append(" AND ENTITY.FIRST_NAME= ~FIRSTNAME~");
                                        strDictParams.Add("FIRSTNAME", sFirstName);
                                    }
                                }
                                else if (string.Compare(arrValues[i], "TaxId", false) == 0)
                                {
                                    sTaxId = oElement.Value;
                                    if (sTaxId != string.Empty)
                                    {
                                        sbSql.Append(" AND ENTITY.TAX_ID= ~TAXID~");
                                        strDictParams.Add("TAXID", sTaxId);
                                    }
                                }

                                else if (string.Compare(arrValues[i], "Addr", false) == 0)
                                {
                                    oElement = oElements.XPathSelectElement("//Addr1");
                                    if (oElement != null)
                                    {
                                        sAdd = oElement.Value;
                                        oElement = null;
                                    }
                                    oElement = oElements.XPathSelectElement("//Addr2");
                                    if (oElement != null)
                                    {
                                        sAdd = sAdd + oElement.Value;
                                        oElement = null;
                                    }
                                    oElement = oElements.XPathSelectElement("//Addr3");
                                    if (oElement != null)
                                    {
                                        sAdd = sAdd + oElement.Value;
                                        oElement = null;
                                    }
                                    oElement = oElements.XPathSelectElement("//Addr4");
                                    if (oElement != null)
                                    {
                                        sAdd = sAdd + oElement.Value;
                                        oElement = null;
                                    }
                                    if (sAdd != string.Empty)
                                    {
                                        sbSql.Append(" AND ENTITY.ADDR1 + ENTITY.ADDR2 + ENTITY.ADDR3 + ENTITY.ADDR4= ~ADDR~");
                                        strDictParams.Add("ADDR", sAdd);
                                    }
                                }
                                else if (string.Compare(arrValues[i], "EmailAddress", false) == 0)
                                {
                                    sEmail = oElement.Value;
                                    if (sEmail != string.Empty)
                                    {
                                        sbSql.Append(" AND ENTITY.EMAIL_ADDRESS= ~EMAIL~");
                                        strDictParams.Add("EMAIL", sEmail);
                                    }
                                }
                                else if (string.Compare(arrValues[i], "Contact", false) == 0)
                                {
                                    sContact = oElement.Value;
                                    if (sContact != string.Empty)
                                    {
                                        sbSql.Append(" AND ENTITY.CONTACT= ~CONTACT~");
                                        strDictParams.Add("CONTACT", sContact);
                                    }
                                }
                                else if (string.Compare(arrValues[i], "ZipCode", false) == 0)
                                {
                                    iZip = Conversion.CastToType<int>(oElement.Value, out bSuccess);
                                    if (iZip != 0)
                                    {
                                        sbSql.Append(" AND ENTITY.ZIP_CODE= ~ZIPCODE~");
                                        strDictParams.Add("ZIPCODE", iZip.ToString());
                                    }
                                }
                            }
                            
                        }
                    }
                }
                #endregion
                if (iEntityTableId == 0)
                {
                oElement = oElements.XPathSelectElement("//EntityTableId");
                iTableId = Conversion.CastToType<int>(oElement.Value, out bSuccess);
                }
                else
                {
                    iTableId = iEntityTableId;
                }
                oElement = null;
                if (!objSettings.UseEntityRole)
                {
                    sbSql.Append(" AND ENTITY.ENTITY_TABLE_ID =" + iTableId);
                }
                //else
                //{
                 
                //        sbSql.Append(" AND  ENTITY.ENTITY_ID = ENTITY_X_ROLES.ENTITY_ID AND ENTITY_X_ROLES.ENTITY_TABLE_ID =" + iTableId);
                 
                //}
                                          
                sbSql.Append("  ORDER BY ENTITY.ENTITY_ID DESC");  
                    if (sbSql != null)
                    {
                        iEntityID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, sbSql.ToString(), strDictParams), m_iClientId);
                    }

                
                

                return iEntityID;
            }

            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objSettings = null;
                if (oElements != null)
                    oElements = null;
                if (oElement != null)
                    oElement = null;
            }
        }

        // mits 35925 end     
        public void SavePolicyunitData(int iUnitId, string sUnitType, string sUnitNumber, string UnitRiskLoc, string UnitRiskSubLoc, string p_sProductcode, string p_sInsLine, string sSiteSeqNumber, string sStatUnitNumber)
        {
           
            DbConnection objCon = null;
            bool bDataExists = false;
            StringBuilder sSQLSelect;
            Dictionary<string, string> strDictParamsSelect = new Dictionary<string, string>();
            DbWriter objwriter=null;
            int rowid;
            // changes made by dheerajbisht for mits no 34842 start
            try
            {
                sSQLSelect = new StringBuilder("SELECT * FROM POINT_UNIT_DATA WHERE UNIT_ID=");
                sSQLSelect.Append(iUnitId);
              
                            
               
                                #region  setting up query for select purpose
                sSQLSelect.Append(" AND UNIT_TYPE= ~sUNITTYPE~ AND UNIT_NUMBER= ~sUNITNUMBER~ ");
                strDictParamsSelect.Add("sUNITTYPE",sUnitType);
                strDictParamsSelect.Add("sUNITNUMBER",sUnitNumber);
                
 
                                if (string.IsNullOrEmpty(UnitRiskLoc.Trim()))
                                {
                                    sSQLSelect.Append("AND UNIT_RISK_LOC is NULL ");
                                }
                                else
                                {
                                    
                                    sSQLSelect.Append(" AND UNIT_RISK_LOC = ~UNITRISKLOC~ ");
                                    strDictParamsSelect.Add("UNITRISKLOC",UnitRiskLoc);
                                }

                                if (string.IsNullOrEmpty(UnitRiskSubLoc.Trim()))
                                {
                                    sSQLSelect.Append(" AND UNIT_RISK_SUB_LOC IS NULL ");
                                }
                                else
                                {
                                    
                                    sSQLSelect.Append(" AND UNIT_RISK_SUB_LOC = ~UNITRISKSUBLOC~ ");
                                    strDictParamsSelect.Add("UNITRISKSUBLOC", UnitRiskSubLoc);
                                }

                                if (string.IsNullOrEmpty(sSiteSeqNumber.Trim()))
                                {
                                    sSQLSelect.Append(" AND SITE_SEQ_NUMBER IS NULL ");
                                }
                                else
                                {
                                    
                                    sSQLSelect.Append(" AND SITE_SEQ_NUMBER = ~sSITESEQNUMBER~ ");
                                    strDictParamsSelect.Add("sSITESEQNUMBER",sSiteSeqNumber);
                                }

                                if (string.IsNullOrEmpty(p_sProductcode.Trim()))
                                {
                                    sSQLSelect.Append(" AND PRODUCT IS NULL ");

                                }
                                else 
                                {
                                    
                                    sSQLSelect.Append(" AND PRODUCT = ~p_sPRODUCTCODE~ ");
                                    strDictParamsSelect.Add("p_sPRODUCTCODE",p_sProductcode);

                                }
                                if (string.IsNullOrEmpty(p_sInsLine.Trim()))
                                {
                                    sSQLSelect.Append(" AND INS_LINE IS NULL ");

                                }
                                else
                                {
                                
                                    sSQLSelect.Append(" AND INS_LINE = ~p_sINSLINE~ ");
                                    strDictParamsSelect.Add("p_sINSLINE",p_sInsLine);
                                }
                                
                                  sSQLSelect.Append(" AND STAT_UNIT_NUMBER = ~sSTATUNITNUMBER~");
                                strDictParamsSelect.Add("sSTATUNITNUMBER",sStatUnitNumber);  
                                #endregion
               
                
                                 
                         
                
                #region new region for dbwriter insert operation


                        objCon = DbFactory.GetDbConnection(this.m_sConnectString);
						objCon.Open();
                        objwriter = DbFactory.GetDbWriter(objCon);
                        objwriter.Tables.Add("POINT_UNIT_DATA");
                        rowid = Utilities.GetNextUID(m_sConnectString, "POINT_UNIT_DATA", m_iClientId);
                        objwriter.Fields.Add("ROW_ID",rowid);
                        objwriter.Fields.Add("UNIT_ID",iUnitId);
                        objwriter.Fields.Add("UNIT_TYPE", sUnitType);

                        objwriter.Fields.Add("UNIT_NUMBER",sUnitNumber);
                        
                
                            if (string.IsNullOrEmpty(UnitRiskLoc.Trim()))
                             {
                                  objwriter.Fields.Add("UNIT_RISK_LOC",null);
                             }
                            else
                            {
                                  objwriter.Fields.Add("UNIT_RISK_LOC",UnitRiskLoc);
                            }
                             
                            if(string.IsNullOrEmpty(UnitRiskSubLoc.Trim()))
                            {
                               objwriter.Fields.Add("UNIT_RISK_SUB_LOC",null);
                            }
                            else
                            {
                            objwriter.Fields.Add("UNIT_RISK_SUB_LOC",UnitRiskSubLoc);
                            }

                            if(string.IsNullOrEmpty(sSiteSeqNumber.Trim()))
                            {
                            objwriter.Fields.Add("SITE_SEQ_NUMBER",null);
                            }
                            else
                            {
                            objwriter.Fields.Add("SITE_SEQ_NUMBER",sSiteSeqNumber);
                            }
                          
                            if(string.IsNullOrEmpty(p_sProductcode.Trim()))
                            {
                            objwriter.Fields.Add("PRODUCT",null);
                            }
                            else
                            {
                            objwriter.Fields.Add("PRODUCT",p_sProductcode);
                            }
                            
                            if(string.IsNullOrEmpty(p_sInsLine.Trim()))
                            {
                            objwriter.Fields.Add("INS_LINE",null);
                            }
                            else
                            {
                            objwriter.Fields.Add("INS_LINE",p_sInsLine);
                            } 
                            
                            if(string.IsNullOrEmpty(sStatUnitNumber.Trim()))
                            {
                            objwriter.Fields.Add("STAT_UNIT_NUMBER",null);
                            }
                            else
                            {
                            objwriter.Fields.Add("STAT_UNIT_NUMBER",sStatUnitNumber);
                            }
                          
                          #endregion
                                
                                object objRdr = (DbFactory.ExecuteScalar(m_sConnectString, sSQLSelect.ToString(),strDictParamsSelect));
                                { 
                                   
                                    if(objRdr!=null)
                                    {
                                    bDataExists = true;
                                    }
                                }

                                if (!bDataExists)
                                {
                                   
                                    objwriter.Execute();
                                    
                                }
                           }
                //changes made by dheerajbisht end

            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objCon != null)
                {
                    objwriter.Dispose();
                    sSQLSelect = null;
                    strDictParamsSelect = null;
                }
            }

        }

        public int CheckUnitDuplication(int p_iPolicyId, string p_sUnitDetailAcordXML, string p_sUnitType,int iUnitId, int p_iPolicySystemId)
        {
            string unitNumber = string.Empty;
            string unitRiskLoc = string.Empty;
            string unitRiskSubLoc = string.Empty;
            string siteSeqNum = string.Empty;
            string insLine = string.Empty;
            string product = string.Empty;
            int iPolicyUnitRowId = 0;
            StringBuilder sbSql = new StringBuilder();
            XElement oElement = null;
            DbConnection objConn = DbFactory.GetDbConnection(m_sConnectString);
            XElement oUnitDetail = XElement.Parse(p_sUnitDetailAcordXML);

            switch (p_sUnitType)
            {
                case "V":
                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/AutoLossInfo/VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_UnitNumber']/OtherId");
                    if (oElement != null)
                        unitNumber = oElement.Value.Trim();

                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/AutoLossInfo/VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_RiskLoc']/OtherId");
                    if (oElement != null)
                        unitRiskLoc = oElement.Value.Trim();

                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/AutoLossInfo/VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_RiskSubLoc']/OtherId");
                    if (oElement != null)
                        unitRiskSubLoc = oElement.Value.Trim();

                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/AutoLossInfo/VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_InsLineCd']/OtherId");
                    if (oElement != null)
                        insLine = oElement.Value.Trim();

                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/AutoLossInfo/VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_Product']/OtherId");
                    if (oElement != null)
                        product = oElement.Value.Trim();

                    sbSql.AppendFormat("SELECT POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT INNER JOIN POINT_UNIT_DATA ON POLICY_X_UNIT.UNIT_ID = POINT_UNIT_DATA.UNIT_ID AND  POLICY_X_UNIT.UNIT_TYPE = POINT_UNIT_DATA.UNIT_TYPE WHERE POLICY_X_UNIT.POLICY_ID = {0} AND POINT_UNIT_DATA.UNIT_TYPE = 'V' AND POINT_UNIT_DATA.UNIT_NUMBER = '{1}' AND POINT_UNIT_DATA.UNIT_RISK_LOC = '{2}' AND POINT_UNIT_DATA.UNIT_RISK_SUB_LOC = '{3}' AND POINT_UNIT_DATA.PRODUCT = '{4}' AND POINT_UNIT_DATA.INS_LINE = '{5}'", p_iPolicyId, unitNumber, unitRiskLoc, unitRiskSubLoc, product, insLine);
                    break;
                case "P":
                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_UnitNumber']/OtherId");
                    if (oElement != null)
                        unitNumber = oElement.Value.Trim();

                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_RiskLoc']/OtherId");
                    if (oElement != null)
                        unitRiskLoc = oElement.Value.Trim();

                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_RiskSubLoc']/OtherId");
                    if (oElement != null)
                        unitRiskSubLoc = oElement.Value.Trim();

                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_InsLineCd']/OtherId");
                    if (oElement != null)
                        insLine = oElement.Value.Trim();

                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_Product']/OtherId");
                    if (oElement != null)
                        product = oElement.Value.Trim();

                    sbSql.AppendFormat("SELECT POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT INNER JOIN POINT_UNIT_DATA ON POLICY_X_UNIT.UNIT_ID = POINT_UNIT_DATA.UNIT_ID AND  POLICY_X_UNIT.UNIT_TYPE = POINT_UNIT_DATA.UNIT_TYPE WHERE POLICY_X_UNIT.POLICY_ID = {0} AND POINT_UNIT_DATA.UNIT_TYPE = 'P' AND POINT_UNIT_DATA.UNIT_NUMBER = '{1}' AND POINT_UNIT_DATA.UNIT_RISK_LOC = '{2}' AND POINT_UNIT_DATA.UNIT_RISK_SUB_LOC = '{3}' AND POINT_UNIT_DATA.PRODUCT = '{4}' AND POINT_UNIT_DATA.INS_LINE = '{5}'", p_iPolicyId, unitNumber, unitRiskLoc, unitRiskSubLoc, product, insLine);
                    break;
                case "S":
                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/WorkCompLocInfo/Location/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_UnitNumber']/OtherId");
                    if (oElement != null)
                        unitNumber = oElement.Value.Trim();

                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/WorkCompLocInfo/Location/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_SiteSequenceNum']/OtherId");
                    if (oElement != null)
                        siteSeqNum = oElement.Value.Trim();

                    sbSql.AppendFormat("SELECT POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT INNER JOIN POINT_UNIT_DATA ON POLICY_X_UNIT.UNIT_ID = POINT_UNIT_DATA.UNIT_ID AND  POLICY_X_UNIT.UNIT_TYPE = POINT_UNIT_DATA.UNIT_TYPE WHERE POLICY_X_UNIT.POLICY_ID = {0} AND POINT_UNIT_DATA.UNIT_TYPE = 'S' AND POINT_UNIT_DATA.UNIT_NUMBER = '{1}' AND POINT_UNIT_DATA.SITE_SEQ_NUMBER = '{2}'", p_iPolicyId, unitNumber, siteSeqNum);
                    break;
                case "SU":
                    oElement = oUnitDetail.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/InsuredOrPrincipal/ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_UnitNumber']/OtherId");
                    if (oElement != null)
                        unitNumber = oElement.Value.Trim();

                   // int iExistingEntityId = CheckEntityDuplicationForStatUnit(p_sUnitDetailAcordXML, p_iPolicyId);
                    sbSql.AppendFormat("SELECT POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT INNER JOIN POINT_UNIT_DATA ON POLICY_X_UNIT.UNIT_ID = POINT_UNIT_DATA.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE = POINT_UNIT_DATA.UNIT_TYPE INNER JOIN POLICY ON POLICY.POLICY_ID = POLICY_X_UNIT.POLICY_ID AND POLICY.POLICY_SYSTEM_ID = {2} WHERE POLICY_X_UNIT.POLICY_ID = {0} AND POINT_UNIT_DATA.UNIT_TYPE = 'SU' AND POINT_UNIT_DATA.UNIT_NUMBER = {1}", p_iPolicyId, unitNumber, p_iPolicySystemId);
                    
                    break;
            }

            if (sbSql != null)
            {
                objConn.Open();
                iPolicyUnitRowId = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sbSql.ToString()), m_iClientId);
            }

            return iPolicyUnitRowId;
        }

        public int CheckEntityDuplicationForStatUnit(string p_sXML, int p_iPolicyId)
        {
            XElement oElement = null;
            string sLastName = string.Empty;
            string sFirstName = string.Empty;
            string sAdd = string.Empty;
            string sTaxId = string.Empty;
            string sEmail = string.Empty;
            string sContact = string.Empty;
            int iZip = 0;
            bool bSuccess = false;
            XElement oElements = null;
            StringBuilder sbSql = new StringBuilder();
            LocalCache objCache = null;
            int iEntityID = 0;
            string[] arrName = null;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            try
            {
                XElement oTemplate = XElement.Parse(p_sXML);
                oElements = oTemplate.XPathSelectElement("//com.csc_InsuredNameIdInfo");
                objCache = new LocalCache(m_sConnectString, m_iClientId);
                string sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("PolicyHolderDupCheckFields", string.Empty);
                string[] arrValues = sPolicyFields.Split(',');
                sbSql.Append("SELECT ENTITY.ENTITY_ID FROM ENTITY");
                sbSql.AppendFormat(" ,OTHER_UNIT,POLICY_X_UNIT WHERE OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.UNIT_TYPE='SU'");
                
                if (oElements != null)
                {
                    for (int i = 0; i <= arrValues.Length - 1; i++)
                    {
                        if (string.Compare(arrValues[i], "LastName", false) == 0)
                        {
                            oElement = oElements.XPathSelectElement("./ItemIdInfo/OtherIdentifier[2]/OtherId");
                            if (oElement != null && !string.IsNullOrEmpty(oElement.Value))
                            {
                                arrName = oElement.Value.Split(' ');
                                if (arrName.Length > 0)
                                    sLastName = arrName[arrName.Length - 1].ToString();
                                sbSql.Append(" AND ENTITY.LAST_NAME= ~LASTNAME~");
                                strDictParams.Add("LASTNAME", sLastName);
                            }
                        }
                        else if (string.Compare(arrValues[i], "FirstName", false) == 0)
                        {
                            oElement = oElements.XPathSelectElement("./ItemIdInfo/OtherIdentifier[2]/OtherId");
                            if (oElement != null && !string.IsNullOrEmpty(oElement.Value))
                            {
                                arrName = oElement.Value.Split(' ');
                                if (arrName.Length > 0)
                                    sFirstName = oElement.Value.Substring(0,  oElement.Value.IndexOf(arrName[arrName.Length - 1]));
                                sbSql.Append(" AND ENTITY.FIRST_NAME= ~FIRSTNAME~");
                                strDictParams.Add("FIRSTNAME", sFirstName);
                            }
                        }
                        else if (string.Compare(arrValues[i], "Addr", false) == 0)
                        {
                            oElement = oElements.XPathSelectElement("//ItemIdInfo/OtherIdentifier[6]/OtherId");
                            if (oElement != null && !string.IsNullOrEmpty(oElement.Value))
                            {
                                sAdd = oElement.Value;
                                sbSql.Append(" AND ENTITY.ADDR1 + ENTITY.ADDR2 + ENTITY.ADDR3 + ENTITY.ADDR4= ~ADDR~");
                                strDictParams.Add("ADDR", sAdd);
                            }
                        }
                        else if (string.Compare(arrValues[i], "EmailAddress", false) == 0)
                        {
                            oElement = oElements.XPathSelectElement("//ItemIdInfo/OtherIdentifier[9]/OtherId");
                            if (oElement != null && !string.IsNullOrEmpty(oElement.Value))
                            {
                                sEmail = oElement.Value;
                                sbSql.Append(" AND ENTITY.EMAIL_ADDRESS= ~EMAIL~");
                                strDictParams.Add("EMAIL", sEmail);
                            }
                        }
                        else if (string.Compare(arrValues[i], "Contact", false) == 0)
                        {
                            oElement = oElements.XPathSelectElement("//ItemIdInfo/OtherIdentifier[10]/OtherId");
                            if (oElement != null && !string.IsNullOrEmpty(oElement.Value))
                            {
                                sContact = oElement.Value;
                                sbSql.Append(" AND ENTITY.CONTACT= ~CONTACT~");
                                strDictParams.Add("CONTACT", sContact);
                            }
                        }
                        else if (string.Compare(arrValues[i], "ZipCode", false) == 0)
                        {
                            oElement = oElements.XPathSelectElement("//ItemIdInfo/OtherIdentifier[8]/OtherId");
                            if (oElement != null && !string.IsNullOrEmpty(oElement.Value))
                            {
                                iZip = Conversion.CastToType<int>(oElement.Value, out bSuccess);
                                if (iZip != 0)
                                {
                                    sbSql.Append(" AND ENTITY.ZIP_CODE= ~ZIPCODE~");
                                    strDictParams.Add("ZIPCODE", iZip.ToString());
                                }
                            }
                        }
                        oElement = null;
                    }
                }
                sbSql.Append(" AND OTHER_UNIT.UNIT_TYPE = POLICY_X_UNIT.UNIT_TYPE AND OTHER_UNIT.OTHER_UNIT_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.POLICY_ID = " + p_iPolicyId);
                if (sbSql != null)
                {
                    iEntityID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, sbSql.ToString(), strDictParams), m_iClientId);
                }

                return iEntityID;
            }

            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (oElements != null)
                    oElements = null;
                if (oElement != null)
                    oElement = null;
            }
        }

        public int CheckCoverageDuplication(int p_iPolicyUnitRowId, string p_sCovDetailAcordXML, string sBaseLOBLine, int iPolicySystemID,string sCvgText) // aaggarwal29: Code mapping change
        {
            string sCovCd = string.Empty;
            string sCovSeqNum = string.Empty;
            string sTransSeqNum = string.Empty;
            string sSublineDesc = string.Empty;
            int iPolicyCovRowId = 0;
            StringBuilder sbSql = new StringBuilder();
            XElement oElement = null;
            DbConnection objConn = DbFactory.GetDbConnection(m_sConnectString);
            XElement oCovDetail = XElement.Parse(p_sCovDetailAcordXML);
            string sPolicySystemID = iPolicySystemID.ToString(); // aaggarwal29: Code mapping change
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(sBaseLOBLine))
            {
                oElement = oCovDetail.XPathSelectElement("//ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_CoverageSeq']/OtherId");
                if (oElement != null)
                    sCovSeqNum = oElement.Value.Trim();

                oElement = oCovDetail.XPathSelectElement("//ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_TrxnSeq']/OtherId");
                if (oElement != null)
                    sTransSeqNum = oElement.Value.Trim();

                oElement = oCovDetail.XPathSelectElement("//com.csc_SublineDesc");
                if (oElement != null && !string.IsNullOrEmpty(oElement.Value))
                    sSublineDesc = oElement.Value.Trim();

                switch (sBaseLOBLine)
                {
                    case "AL":
                    case "CL":
                    case "PL":
                    case "WL":
                        oElement = oCovDetail.XPathSelectElement("//CoverageCd");
                        if (oElement != null)
                            sCovCd = oElement.Value.Trim();
                        break;
                }

                sbSql.AppendFormat("SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE COVERAGE_TYPE_CODE= '{0}' AND POLICY_UNIT_ROW_ID = '{1}' AND CVG_SEQUENCE_NO = '{2}' AND TRANS_SEQ_NO = '{3}'", GetRMXCodeIdFromPSMappedCode(sCovCd, "COVERAGE_TYPE", "CoverageType on CoverageDuplication", sPolicySystemID), p_iPolicyUnitRowId, sCovSeqNum, sTransSeqNum); // aaggarwal29: Code mapping change
                //Start:Added by Nitin goel, MITS 36753,06/14/2014,removed If condition, and dependency on Subline Descrption.
                //if (string.IsNullOrEmpty(sSublineDesc))
                //    sbSql.Append("AND COVERAGE_TEXT IS NULL");
                //else
                //{
                //    //sbSql.AppendFormat("AND COVERAGE_TEXT = '{0}'", sCvgText);
                    sbSql.Append("AND COVERAGE_TEXT = ~COVTEXT~");
                    strDictParams.Add("COVTEXT", sCvgText);
                //}
                //end: added by Nitin goel
                if (sbSql != null)
                {
                    objConn.Open();
                    //iPolicyCovRowId = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sbSql.ToString(), m_iClientId,strDictParams));
                    iPolicyCovRowId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, sbSql.ToString(), strDictParams), m_iClientId);
                }
            }
            return iPolicyCovRowId;
        }

        public string GetPolicyUnitInterestList(int p_iUnitRowId)
        {

            DbReader objDbReader = null;
            string sObjResult = string.Empty;
            LocalCache objCache = null;
             string sShortCode=string.Empty;
             XmlDocument objXmlDocument = null;
             XmlElement objOptionXmlElement = null;
             XmlElement objRootElement = null;
             int iTableid = 0;
            StringBuilder sbSQL;
            try
            {
                objXmlDocument = new XmlDocument();
                objCache = new LocalCache(m_sConnectString, m_iClientId);
               
                sbSQL = new StringBuilder();

                // APEYKOV - JIRA RMA-3786 
                objRootElement = objXmlDocument.CreateElement("PolicySystemList");
                objXmlDocument.AppendChild(objRootElement);

                if (p_iUnitRowId != 0)
                {
                    sbSQL.Append("SELECT POLICY_X_ENTITY.TYPE_CODE ,ENTITY.ENTITY_ID,ENTITY.LAST_NAME,ENTITY.FIRST_NAME,POLICY_SYSTEM_ID");
                    sbSQL.Append(" FROM POLICY_X_ENTITY ,ENTITY,POLICY WHERE POLICY_X_ENTITY.ENTITY_ID=ENTITY.ENTITY_ID AND POLICY.POLICY_ID=POLICY_X_ENTITY.POLICY_ID  AND POLICY_X_ENTITY.POLICY_UNIT_ROW_ID= " + p_iUnitRowId);

                    objDbReader = DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString());
                    // APEYKOV - JIRA RMA-3786 
                    //objRootElement = objXmlDocument.CreateElement("PolicySystemList");
                    //objXmlDocument.AppendChild(objRootElement);
                    //}
             
                while (objDbReader.Read())
                {
                    iTableid = objCache.GetTableId(objCache.GetTableName(Conversion.ConvertObjToInt(objDbReader.GetValue(0), m_iClientId)));
                  
                    sShortCode = objCache.GetShortCode(Conversion.ConvertObjToInt(objDbReader.GetValue(0), m_iClientId));

                    objOptionXmlElement = objXmlDocument.CreateElement("option");
                    objOptionXmlElement.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue(1)));
                    objOptionXmlElement.SetAttribute("PSID", Conversion.ConvertObjToStr(objDbReader.GetValue(4)));

                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue(2) + " " + objDbReader.GetValue(3) + ":" + objCache.GetUserTableName(iTableid));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                    }
                   
                    // APEYKOV - JIRA RMA-3786
                }
                sObjResult = objXmlDocument.InnerXml;
            }

            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetDownLoadXMLData.NoRecordsFound", m_iClientId), p_objExp);
            }
            finally
            {

                if (objDbReader != null)
                {
                    objDbReader.Dispose();
                    objDbReader = null;
                }

                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                 objXmlDocument = null;
                 objOptionXmlElement = null;
                 objRootElement = null;
            }
            return sObjResult;
        }

        public void DeleteTempCoverageFiles(string sFileList)
        {
            string[] sAllFiles = sFileList.Split(',');
            foreach (string sFile in sAllFiles)
            {
                if(!string.IsNullOrEmpty(sFile))
                    if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + sFile))
                    {
                        File.Delete(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + sFile);
                    }
            }
        }

        public int GetPSMappedCode(int RMXCodeId, string TableName, string ExceptionParameter, string sPolicySystemID, int ClaimTypeCd, int LOBCd)
        {
            string sSQL = string.Empty;
            int PSMappedCodeID = 0;
            int iTableId = Int32.MinValue, iIncludeClaimType = Int32.MinValue, iLOB = Int32.MinValue;
            DbReader objDbReader = null;
            SysSettings objSettings = null;
            LocalCache objCache = null;
            LobSettings objLOBSettings = null;
            ColLobSettings objLOB = null;
 
            try
            {
                iLOB = LOBCd;
                objSettings = new SysSettings(m_sConnectString,m_iClientId);
                objCache = new LocalCache(m_sConnectString, m_iClientId);
                objLOB = new ColLobSettings(m_sConnectString,m_iClientId);
                objLOBSettings = objLOB[iLOB];
                iTableId = objCache.GetTableId(TableName);
               
                sSQL = "SELECT INCLUDE_CLAIM_TYPE FROM PS_MAP_TABLES, POLICY_X_WEB WHERE POLICY_X_WEB.POLICY_SYSTEM_ID = " + sPolicySystemID +
                    " AND POLICY_X_WEB.POLICY_SYSTEM_CODE = PS_MAP_TABLES.POLICY_SYSTEM_TYPE_ID AND RMX_TABLE_ID = "+iTableId;
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                if (objDbReader.Read())
                {
                    iIncludeClaimType = Conversion.ConvertObjToInt(objDbReader.GetValue(0), m_iClientId);
                }
                if (iIncludeClaimType == 1 && !objLOBSettings.ResByClmType && TableName=="RESERVE_TYPE")
                {
                    // call the new overloaded method which uses claim type code
                    PSMappedCodeID = GetPSMappedCodeIDFromRMXCodeId(RMXCodeId, TableName, ExceptionParameter, sPolicySystemID, ClaimTypeCd);
                }
                else if (iIncludeClaimType == 1 && TableName != "RESERVE_TYPE")
                {
                    // call the new overloaded method, no need to check for the ResByClmType setting if table is not Reserve_TYpe 
                    PSMappedCodeID = GetPSMappedCodeIDFromRMXCodeId(RMXCodeId, TableName, ExceptionParameter, sPolicySystemID, ClaimTypeCd);
                }
                else
                {
                    // follow the regular call
                    PSMappedCodeID = GetPSMappedCodeIDFromRMXCodeId(RMXCodeId, TableName, ExceptionParameter, sPolicySystemID);
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(String.Format(Globalization.GetString("PolicySystemInterface.GetPSMappedCode.FetchingMappedCodesError",m_iClientId), ExceptionParameter), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                objSettings = null;
                objCache = null;
                objLOB = null;
                objLOBSettings = null;
            }
            return PSMappedCodeID;
        }

        public string GetPolicyDetails(int p_iPolicyId)
        {

            DbReader objDbReader = null;
            string sObjResult = string.Empty;
            LocalCache objCache = null;
            string sShortCode = string.Empty;
            XmlDocument objXmlDocument = null;
            XmlElement objOptionXmlElement = null;
            XmlElement objRootElement = null;
            int iTableid = 0;
            StringBuilder sbSQL;
            try
            {
                objXmlDocument = new XmlDocument();
                objCache = new LocalCache(m_sConnectString, m_iClientId);

                sbSQL = new StringBuilder();

                if (p_iPolicyId != 0)
                {
                    sbSQL.Append("SELECT * FROM POLICY P, POLICY_X_WEB PW WHERE P.POLICY_SYSTEM_ID = PW.POLICY_SYSTEM_ID AND  POLICY_ID = " + p_iPolicyId);
                    objDbReader = DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString());
                    objRootElement = objXmlDocument.CreateElement("FetchPolicyDetails");

                    objXmlDocument.AppendChild(objRootElement);
                }

                while (objDbReader.Read())
                {

                    objOptionXmlElement = objXmlDocument.CreateElement("PolicyID");
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("Policy_ID"));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                    
                    objOptionXmlElement = objXmlDocument.CreateElement("PolicyNumber");
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("Policy_Number"));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                    
                    objOptionXmlElement = objXmlDocument.CreateElement("Module");
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("Module"));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                    
                    objOptionXmlElement = objXmlDocument.CreateElement("PolicySymbol");
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("Policy_Symbol"));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                    
                    objOptionXmlElement = objXmlDocument.CreateElement("MasterCompany");
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("Master_Company"));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                    
                    objOptionXmlElement = objXmlDocument.CreateElement("LocationCompany");
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("Location_Company"));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;

                    objOptionXmlElement = objXmlDocument.CreateElement("SrcLocation");
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                    
                    objOptionXmlElement = objXmlDocument.CreateElement("PolSysUserName");
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("CFW_USERNAME"));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                    
                    objOptionXmlElement = objXmlDocument.CreateElement("PolSysPassword");
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("CFW_Password"));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                    
                    objOptionXmlElement = objXmlDocument.CreateElement("PolicyAction");
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;

                    objOptionXmlElement = objXmlDocument.CreateElement("PointURL");
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("POINT_URL"));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;


                }

                sObjResult = objXmlDocument.InnerXml;


            }

            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicyDetails.ErrorFetchingDetails",m_iClientId), p_objExp);
            }
            finally
            {

                if (objDbReader != null)
                {
                    objDbReader.Dispose();
                    objDbReader = null;
                }

                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objXmlDocument = null;
                objOptionXmlElement = null;
                objRootElement = null;
            }
            return sObjResult;
        }
        //Start: MITS 31601: Neha Suresh Jain, 03/01/2013
        /// <summary>
        /// check and insert if not exist history information for a policy 
        /// </summary>
        /// <param name="p_iPolicyId"></param>
        /// <param name="p_sReasonAmended"></param>
        /// <param name="p_sEnterDate"></param>
        /// <param name="p_sChangeEffDate"></param>
        public void CheckAndSavePolicyAmendHist(int p_iPolicyId, string p_sReasonAmended, string p_sEnterDate, string p_sChangeEffDate)
        {
            string sSQL = string.Empty;
            DbConnection objCon = null;
            bool bDataExists = false;
            DbWriter objDbWriter = null;
            try
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT * FROM POLICY_X_AMENDHIST WHERE POLICY_ID='" + p_iPolicyId + "' AND REASON_AMEND='" + p_sReasonAmended.Replace("'", "''") + "'  AND CHANGE_EFF_DATE='" + p_sChangeEffDate + "' AND ENTER_DATE='" + p_sEnterDate + "'"))
                {
                    if (objRdr.Read())
                    {
                        bDataExists = true;

                    }
                }
                if (!bDataExists)
                {
                    objCon = DbFactory.GetDbConnection(m_sConnectString);
                    objCon.Open();
                    objDbWriter = DbFactory.GetDbWriter(objCon);
                    objDbWriter.Tables.Add("POLICY_X_AMENDHIST");
                    objDbWriter.Fields.Add("ROW_ID", Utilities.GetNextUID(m_sConnectString, "POLICY_X_AMENDHIST", m_iClientId));
                    objDbWriter.Fields.Add("POLICY_ID", p_iPolicyId);
                    objDbWriter.Fields.Add("REASON_AMEND", p_sReasonAmended);
                    objDbWriter.Fields.Add("CHANGE_EFF_DATE", p_sChangeEffDate);
                    objDbWriter.Fields.Add("ENTER_DATE", p_sEnterDate);
                    objDbWriter.Execute();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objCon != null)
                {
                    objCon.Close();
                    objCon.Dispose();
                }
                if (objDbWriter != null)
                {
                   // objDbWriter.Dispose();
                    objDbWriter = null;
                }
            }

        }
        //End: MITS 31601

        /// <summary>
        /// Function will update clientseqnum from point system into entity table,
        /// this is required when it is an RMA entity and not downloaded from point,
        /// thus, client seqnum will not be available.
        /// </summary>
        /// <param name="iEntityID"></param>
        /// <param name="iClientSeqNum"></param>
        
        public void UpdateClientSequenceNumber(long iEntityID, long iClientSeqNum)
        {
            DbWriter objWriter = DbFactory.GetDbWriter(m_sConnectString);
            objWriter.Tables.Add("Entity");
            objWriter.Fields.Add("CLIENT_SEQ_NUM", iClientSeqNum);
            objWriter.Where.Add(" ENTITY_ID = " + iEntityID.ToString());
            objWriter.Execute();
        }

        /// <summary>
        /// Function will update addressseqnum from point system into Entity table,
        /// this is required when it is an RMA entity and not downloaded from point,
        /// thus, addressseqnum will not be available.
        /// </summary>
        /// <param name="iEntityID"></param>
        /// <param name="iAddrSeqNum"></param>
        public void UpdateAddressSequenceNumber(long iAddressID, long iAddrSeqNum)
        {
            DbWriter objWriter = DbFactory.GetDbWriter(m_sConnectString);
            objWriter.Tables.Add("ENTITY_X_ADDRESSES");
            objWriter.Fields.Add("ADDRESS_SEQ_NUM", iAddrSeqNum);
            objWriter.Where.Add(" ADDRESS_ID = " + iAddressID.ToString());
            objWriter.Execute();
        }
		//Policy Staging Changes Start
		//Raman 3/24 : Reverting rmA-6683 : Policy Staging solution should be compliant to policy api 
        //it needs to use base tables and datamodel
        /* 
        public string GetPolicyDataFromStaging(int p_iPolicyStagingId, int p_iPolicySystemId)
            {

                DataModelFactory oFactory = null;
                DataModelFactory oFactoryStgngDmf = null;
                PolicyStgng oPolicyStaging = null;
                EntityStgng oEntityStgng = null;
                XmlDocument xReturnDoc = null;
                XmlNode xRootElement = null;
                XmlNode xPolicySummaryInfo = null;
                XmlNode xInsured = null;
                string sSQL = string.Empty;
                int iInsuredEid = 0;
                bool bSuccess = false;

                string sDocPath = string.Empty;
                xReturnDoc = new XmlDocument();
                try
                {
                    oFactory = new DataModelFactory(m_oUserLogin,m_iClientId );
                    oFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);
                    oPolicyStaging = oFactoryStgngDmf.GetDataModelObject("PolicyStgng", false) as PolicyStgng;

                    oEntityStgng = oFactoryStgngDmf.GetDataModelObject("EntityStgng", false) as EntityStgng;
                    if (p_iPolicyStagingId > 0)
                    {
                        oPolicyStaging.MoveTo(p_iPolicyStagingId);
                    }
                    xRootElement = xReturnDoc.CreateNode(XmlNodeType.Element, "PolicyStaging", string.Empty);

                    xPolicySummaryInfo = xReturnDoc.CreateNode(XmlNodeType.Element, "PolicySummaryInfo", string.Empty);
                    xInsured = xReturnDoc.CreateNode(XmlNodeType.Element, "InsuredInfo", string.Empty);

                    CreateAndAppendNode("PolicyStatusCd", oPolicyStaging.Context.LocalCache.GetCodeDesc(oPolicyStaging.Context.LocalCache.GetCodeId(oPolicyStaging.PolicyStatusCode, "POLICY_STATUS")), xPolicySummaryInfo, xReturnDoc);
                    CreateAndAppendNode("PolicyNumber", oPolicyStaging.PolicyNumber, xPolicySummaryInfo, xReturnDoc);
                    CreateAndAppendNode("EffectiveDt", Conversion.GetDBDateFormat(oPolicyStaging.EffectiveDate, "MM/dd/yyyy"), xPolicySummaryInfo, xReturnDoc);
                    CreateAndAppendNode("ExpirationDt", Conversion.GetDBDateFormat(oPolicyStaging.ExpirationDate, "MM/dd/yyyy"), xPolicySummaryInfo, xReturnDoc);
                    CreateAndAppendNode("LOBCd", oPolicyStaging.PolicyLOB, xPolicySummaryInfo, xReturnDoc);
                    CreateAndAppendNode("csc_AgentPremium", oPolicyStaging.Premium.ToString(), xPolicySummaryInfo, xReturnDoc);
                    // Pradyumna 1/14/2014 - For Inquiry screen display- Start
                    CreateAndAppendNode("CancelledDate", oPolicyStaging.CancelledDate.Trim(), xPolicySummaryInfo, xReturnDoc);
                    CreateAndAppendNode("MasterCompany", oPolicyStaging.MasterCompany.Trim(), xPolicySummaryInfo, xReturnDoc);
                    CreateAndAppendNode("LocCompany", oPolicyStaging.LocCompany.Trim(), xPolicySummaryInfo, xReturnDoc);
                    // Pradyumna 1/14/2014 - For Inquiry screen display- Ends
                    iInsuredEid = Conversion.CastToType<int>(oPolicyStaging.PolicyXInsuredStgng.ToString(), out bSuccess);
                    if (iInsuredEid > 0)
                    {
                        oEntityStgng.MoveTo(iInsuredEid);
                    }
                    CreateAndAppendNode("StateProvCd", oEntityStgng.StateId, xInsured, xReturnDoc);
                    CreateAndAppendNode("CommercialName", (string.Format("{0} {1} {2}", oEntityStgng.FirstName, oEntityStgng.MiddleName, oEntityStgng.LastName)).Trim(), xInsured, xReturnDoc);
                    CreateAndAppendNode("Addr1", oEntityStgng.Addr1, xInsured, xReturnDoc);
                    CreateAndAppendNode("Addr2", oEntityStgng.Addr2, xInsured, xReturnDoc);
                    CreateAndAppendNode("City", oEntityStgng.City, xInsured, xReturnDoc);
                    CreateAndAppendNode("StateProvCd", oEntityStgng.StateId, xInsured, xReturnDoc);
                    CreateAndAppendNode("PostalCode", oEntityStgng.ZipCode, xInsured, xReturnDoc);
                    CreateAndAppendNode("OtherGivenName", oEntityStgng.AlsoKnownAs, xInsured, xReturnDoc);
                    // Pradyumna 1/14/2014 - For Inquiry screen display- Start
                    foreach (PolicyXEntityStgng objPolicyXEntStg in oPolicyStaging.PolicyXEntityListStgng)
                    {
                        //Anu Tennyson : Changed the logic for WWIG and Post WWIG
                        int iAgentTableId = 0;
                        iAgentTableId = oFactory.Context.LocalCache.GetTableId("AGENTS");
                        sSQL = "SELECT COUNT(*) FROM ENTITY_STGNG WHERE ENTITY_ID =  " + objPolicyXEntStg.EntityId + " AND ENTITY_TABLE_ID = " + iAgentTableId;

                        int iAgentExists = Conversion.CastToType<int>((DbFactory.ExecuteScalar(oFactoryStgngDmf.Context.DbConn.ConnectionString, sSQL)).ToString(), out bSuccess);
                        if (iAgentExists == 0)
                        {
                            sSQL = string.Empty;
                            sSQL = "SELECT COUNT(*) FROM ENTITY_STGNG WHERE ENTITY_ID =  " + objPolicyXEntStg.EntityId + " AND ENTITY_TABLE_NAME = 'AGENTS'";
                        }
                        iAgentExists = Conversion.CastToType<int>((DbFactory.ExecuteScalar(oFactoryStgngDmf.Context.DbConn.ConnectionString, sSQL)).ToString(), out bSuccess);
                        //Anu Tennyson Ends for WWIG and Post WWIG
                        if (iAgentExists > 0)
                        {
                            StringBuilder sbAgencyName = new StringBuilder();
                            oEntityStgng = null;
                            oEntityStgng = oFactoryStgngDmf.GetDataModelObject("EntityStgng", false) as EntityStgng;
                            oEntityStgng.MoveTo(objPolicyXEntStg.EntityId);
                            // Agency Number
                            if (oEntityStgng.ReferenceNumber.Trim() != string.Empty)
                            {
                                CreateAndAppendNode("AgencyNum", oEntityStgng.ReferenceNumber.Trim(), xPolicySummaryInfo, xReturnDoc);
                            }
                            // Agency name
                            if (oEntityStgng.FirstName.Trim() != string.Empty)
                            {
                                sbAgencyName.Append(oEntityStgng.FirstName.Trim());
                            }
                            if (oEntityStgng.MiddleName.Trim() != string.Empty)
                            {
                                if (sbAgencyName.Length > 0)
                                    sbAgencyName.Append(" ");
                                sbAgencyName.Append(oEntityStgng.MiddleName.Trim());
                            }
                            if (oEntityStgng.LastName.Trim() != string.Empty)
                            {
                                if (sbAgencyName.Length > 0)
                                    sbAgencyName.Append(" ");
                                sbAgencyName.Append(oEntityStgng.LastName.Trim());
                            }
                            CreateAndAppendNode("AgencyName", sbAgencyName.ToString(), xPolicySummaryInfo, xReturnDoc);
                            break;
                        }
                    }
                    // Pradyumna 1/14/2014 - For Inquiry screen display- Start
                    xRootElement.AppendChild(xPolicySummaryInfo);
                    xRootElement.AppendChild(xInsured);
                    xReturnDoc.AppendChild(xRootElement);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (oPolicyStaging != null)
                        oPolicyStaging.Dispose(); 
                    if (oEntityStgng != null)
                        oEntityStgng.Dispose();
                    if (oFactory != null)
                        oFactory.Dispose();
                }

                return xReturnDoc.OuterXml;
            }

            public string GetUnitList(int p_iStagingPolicyId,int p_iPolicySystemId)
            {
                DataModelFactory oFactory = null;
                DataModelFactory oFactoryStgngDmf = null;
                PolicyStgng oPolicyStaging = null;

                XmlDocument xReturnDoc = null;

                XmlNode xRootNode = null;
                XmlNode xVehicleNode = null;
                XmlNode xPropertyNode = null;
                XmlNode xSiteUnitNode = null;
                XmlNode xOtherUnitNode = null;
                XmlNode xGeneralNode = null;

                string sPUD_ColumnName = " UNIT_NUMBER, UNIT_RISK_LOC, UNIT_RISK_SUB_LOC, PRODUCT, INS_LINE, STAT_UNIT_NUMBER ";
                string sVehicleColumnName = " UNIT_TYPE_CODE, VEHICLE_YEAR, VIN, VEHICLE_MAKE, VEHICLE_MODEL ";
                string sPropertyColumnName = " YEAR_OF_CONS, DESCRIPTION, ADDR1, CITY, STATE_ID, ZIP_CODE ";
                string sSiteColumnName = " NAME, ADDR1, CITY, STATE_ID, ZIP_CODE ";

                string sDocPath = string.Empty;
                xReturnDoc = new XmlDocument();
                xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Units", string.Empty);
                xSiteUnitNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Site", string.Empty);
                xOtherUnitNode = xReturnDoc.CreateNode(XmlNodeType.Element, "OtherUnit", string.Empty);

                string sAddress = string.Empty;
                string sCity = string.Empty;
                string sZip = string.Empty;
                string sState = string.Empty;

                try
                {
                    oFactory = new DataModelFactory(m_oUserLogin,m_iClientId );
                    oFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);
                    oPolicyStaging = oFactoryStgngDmf.GetDataModelObject("PolicyStgng", false) as PolicyStgng;
                    oPolicyStaging.MoveTo(p_iStagingPolicyId);

                    foreach (PolicyXUnitStgng oUnitStgng in oPolicyStaging.PolicyXUnitListStgng)
                    {
                        xGeneralNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Unit", string.Empty);
                        CreateAndAppendNode("QueryString", string.Format("{0}|{1}|{2}", p_iStagingPolicyId, oUnitStgng.PolicyUnitRowId, p_iPolicySystemId), xGeneralNode, xReturnDoc);

                        using (DbReader oDbReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(string.Format("SELECT {0} FROM POINT_UNIT_DATA_STGNG WHERE UNIT_ID={1} AND UNIT_TYPE='{2}'", sPUD_ColumnName, oUnitStgng.UnitId, oUnitStgng.UnitType.Trim())))
                        {
                            if (oDbReader != null)
                            {
                                if (oDbReader.Read())
                                {
                                    if (oDbReader["UNIT_RISK_LOC"] != null && oDbReader["UNIT_RISK_LOC"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("csc_RiskLoc", oDbReader["UNIT_RISK_LOC"].ToString(), xGeneralNode, xReturnDoc);
                                    }

                                    if (oDbReader["UNIT_RISK_SUB_LOC"] != null && oDbReader["UNIT_RISK_SUB_LOC"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("csc_RiskSubLoc", oDbReader["UNIT_RISK_SUB_LOC"].ToString(), xGeneralNode, xReturnDoc);
                                    }

                                    //if (oDbReader["UNIT_NUMBER"] != null && oDbReader["UNIT_NUMBER"] != DBNull.Value)
                                    //{
                                    //    CreateAndAppendNode("csc_UnitNumber", oDbReader["UNIT_NUMBER"].ToString(), xGeneralNode, xReturnDoc);
                                    //}

                                    if (oDbReader["INS_LINE"] != null && oDbReader["INS_LINE"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("csc_InsLineCd", oDbReader["INS_LINE"].ToString(), xGeneralNode, xReturnDoc);
                                    }

                                    if (oDbReader["PRODUCT"] != null && oDbReader["PRODUCT"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("csc_Product", oDbReader["PRODUCT"].ToString(), xGeneralNode, xReturnDoc);
                                    }

                                    if (oDbReader["STAT_UNIT_NUMBER"] != null && oDbReader["STAT_UNIT_NUMBER"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("csc_UnitNumber", oDbReader["STAT_UNIT_NUMBER"].ToString(), xGeneralNode, xReturnDoc);
                                        CreateAndAppendNode("csc_StatUnitNumber", oDbReader["STAT_UNIT_NUMBER"].ToString(), xGeneralNode, xReturnDoc);
                                    }
                                }
                            }
                        }

                        switch (oUnitStgng.UnitType.Trim())
                        {
                            case "v":
                            case "V":
                                xVehicleNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Vehicle", string.Empty);

                                using (DbReader oDbReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(string.Format("SELECT {0} FROM VEHICLE_STGNG WHERE UNIT_ID={1}", sVehicleColumnName, oUnitStgng.UnitId)))
                                {
                                    if (oDbReader != null)
                                    {
                                        if (oDbReader.Read())
                                        {
                                            string sYear = string.Empty;
                                            string sVin = string.Empty;
                                            string sModel = string.Empty;
                                            string sMake = string.Empty;

                                            if (oDbReader["UNIT_TYPE_CODE"] != null && oDbReader["UNIT_TYPE_CODE"] != DBNull.Value)
                                            {
                                                CreateAndAppendNode("csc_UnitStatus", oDbReader["UNIT_TYPE_CODE"].ToString(), xVehicleNode, xReturnDoc);
                                            }

                                            if (oDbReader["VEHICLE_YEAR"] != null && oDbReader["VEHICLE_YEAR"] != DBNull.Value)
                                            {
                                                CreateAndAppendNode("ModelYear", oDbReader["VEHICLE_YEAR"].ToString(), xVehicleNode, xReturnDoc);
                                                sYear = oDbReader["VEHICLE_YEAR"].ToString();
                                            }

                                            if (oDbReader["VIN"] != null && oDbReader["VIN"] != DBNull.Value)
                                            {
                                                CreateAndAppendNode("csc_VehicleIdentificationNumber", oDbReader["VIN"].ToString(), xVehicleNode, xReturnDoc);
                                                sVin = oDbReader["VIN"].ToString();
                                            }

                                            if (oDbReader["VEHICLE_MAKE"] != null && oDbReader["VEHICLE_MAKE"] != DBNull.Value)
                                            {
                                                sMake = oDbReader["VEHICLE_MAKE"].ToString();
                                            }

                                            if (oDbReader["VEHICLE_MODEL"] != null && oDbReader["VEHICLE_MODEL"] != DBNull.Value)
                                            {
                                                sModel = oDbReader["VEHICLE_MODEL"].ToString();
                                            }

                                            CreateAndAppendNode("Manufacturer", string.Format("{0} {1} {2} Vin: {3}", new object[] { sYear, sModel, sMake, sVin }), xVehicleNode, xReturnDoc);
                                        }
                                    }
                                }
                                xGeneralNode.AppendChild(xVehicleNode);
                                break;
                            case "P":
                            case "p":
                                xPropertyNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Property", string.Empty);

                                using (DbReader oDbReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(string.Format("SELECT {0} FROM PROPERTY_UNIT_STGNG WHERE PROPERTY_ID={1}", sPropertyColumnName, oUnitStgng.UnitId)))
                                {
                                    if (oDbReader != null)
                                    {
                                        if (oDbReader.Read())
                                        {
                                            if (oDbReader["YEAR_OF_CONS"] != null && oDbReader["YEAR_OF_CONS"] != DBNull.Value)
                                            {
                                                CreateAndAppendNode("csc_YearBuilt", oDbReader["YEAR_OF_CONS"].ToString(), xPropertyNode, xReturnDoc);
                                            }

                                            if (oDbReader["DESCRIPTION"] != null && oDbReader["DESCRIPTION"] != DBNull.Value)
                                            {
                                                CreateAndAppendNode("csc_UnitDesc", oDbReader["DESCRIPTION"].ToString(), xPropertyNode, xReturnDoc);
                                            }

                                            if (oDbReader["ADDR1"] != null && oDbReader["ADDR1"] != DBNull.Value)
                                            {
                                                sAddress = oDbReader["ADDR1"].ToString();
                                            }

                                            if (oDbReader["CITY"] != null && oDbReader["CITY"] != DBNull.Value)
                                            {
                                                sCity = oDbReader["CITY"].ToString();
                                            }

                                            if (oDbReader["STATE_ID"] != null && oDbReader["STATE_ID"] != DBNull.Value)
                                            {
                                                sState = oDbReader["STATE_ID"].ToString();
                                            }

                                            if (oDbReader["ZIP_CODE"] != null && oDbReader["ZIP_CODE"] != DBNull.Value)
                                            {
                                                sZip = oDbReader["ZIP_CODE"].ToString();
                                            }

                                            CreateAndAppendNode("csc_UnitAddress", string.Format("{0}, {1}, {2}, {3}", new object[] { sAddress, sCity, sState, sZip }), xPropertyNode, xReturnDoc);
                                        }
                                    }
                                }
                                xGeneralNode.AppendChild(xPropertyNode);
                                break;
                            case "s":
                            case "S":
                                xSiteUnitNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Site", string.Empty);

                                using (DbReader oDbReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(string.Format("SELECT {0} FROM SITE_UNIT_STGNG WHERE SITE_ID={1}", sSiteColumnName, oUnitStgng.UnitId)))
                                {
                                    if (oDbReader != null)
                                    {
                                        if (oDbReader.Read())
                                        {
                                            if (oDbReader["NAME"] != null && oDbReader["NAME"] != DBNull.Value)
                                            {
                                                CreateAndAppendNode("csc_UnitDesc", oDbReader["NAME"].ToString(), xSiteUnitNode, xReturnDoc);
                                            }

                                            if (oDbReader["ADDR1"] != null && oDbReader["ADDR1"] != DBNull.Value)
                                            {
                                                sAddress = oDbReader["ADDR1"].ToString();
                                            }

                                            if (oDbReader["CITY"] != null && oDbReader["CITY"] != DBNull.Value)
                                            {
                                                sCity = oDbReader["CITY"].ToString();
                                            }

                                            if (oDbReader["STATE_ID"] != null && oDbReader["STATE_ID"] != DBNull.Value)
                                            {
                                                sState = oDbReader["STATE_ID"].ToString();
                                            }

                                            if (oDbReader["ZIP_CODE"] != null && oDbReader["ZIP_CODE"] != DBNull.Value)
                                            {
                                                sZip = oDbReader["ZIP_CODE"].ToString();
                                            }

                                            CreateAndAppendNode("csc_UnitAddress", string.Format("{0}, {1}, {2}, {3}", new object[] { sAddress, sCity, sState, sZip }), xSiteUnitNode, xReturnDoc);
                                        }
                                    }
                                }
                                xGeneralNode.AppendChild(xSiteUnitNode);
                                break;
                            default:
                                break;
                        }

                        xRootNode.AppendChild(xGeneralNode);
                    }

                    xReturnDoc.AppendChild(xRootNode);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (oPolicyStaging!=null)
                    {
                        oPolicyStaging.Dispose();
                    }
                    if (oPolicyStaging!=null)
                    {
                        oPolicyStaging.Dispose();
                    }
                }

                return xReturnDoc.OuterXml;
            }

            public string GetUnitCoverages(int p_iUnitStagingRowId, int p_iPolicySystemId)
            {
                XmlDocument xDoc = null;
                XmlNode xRootNode = null;
                XmlNode xCvgNode = null;
                DataModelFactory oFactory = null;
                DataModelFactory oFactoryStgngDmf = null;

                int iCodeId = 0;
                try
                {
                    xDoc = new XmlDocument();

                    oFactory = new DataModelFactory(m_oUserLogin, m_iClientId); 
                    oFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);
                    xRootNode = xDoc.CreateNode(XmlNodeType.Element, "Coverages", string.Empty);

                    using (DbReader oDbReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader("SELECT * FROM POLICY_X_CVG_TYPE_STGNG WHERE POLICY_UNIT_ROW_ID=" + p_iUnitStagingRowId))
                    {
                        if (oDbReader != null)
                        {
                            while (oDbReader.Read())
                            {
                                xCvgNode = xDoc.CreateNode(XmlNodeType.Element, "Coverage", string.Empty);
                                if (oDbReader["COVERAGE_TYPE_CODE"] != null && oDbReader["COVERAGE_TYPE_CODE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("CoverageCd", oDbReader["COVERAGE_TYPE_CODE"].ToString(), xCvgNode, xDoc);
                                    iCodeId = GetRMXCodeIdFromPSMappedCode(oDbReader["COVERAGE_TYPE_CODE"].ToString().Trim(), "COVERAGE_TYPE", "CoverageType on Get Unit Coverages", p_iPolicySystemId.ToString());
                                    //iCodeId = oFactoryStgngDmf.Context.LocalCache.GetCodeId(oDbReader["COVERAGE_TYPE_CODE"].ToString().Trim(), "COVERAGE_TYPE");
                                    if (iCodeId > 0)
                                    {
                                        CreateAndAppendNode("Description", oFactory.Context.LocalCache.GetCodeDesc(iCodeId), xCvgNode, xDoc);
                                    }
                                    else
                                    {
                                        CreateAndAppendNode("Description", oDbReader["COVERAGE_TYPE_CODE"].ToString(), xCvgNode, xDoc);
                                    }

                                }
                                if (oDbReader["WRITTEN_PREMIUM"] != null && oDbReader["WRITTEN_PREMIUM"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Premium", oDbReader["WRITTEN_PREMIUM"].ToString(), xCvgNode, xDoc);
                                }
                                else
                                {
                                    CreateAndAppendNode("Premium", "0.00", xCvgNode, xDoc);
                                }
                                if (oDbReader["CVG_SEQUENCE_NO"] != null && oDbReader["CVG_SEQUENCE_NO"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("CvgSeqNum", oDbReader["CVG_SEQUENCE_NO"].ToString(), xCvgNode, xDoc);
                                }
                                if (oDbReader["SELF_INSURE_DEDUCT"] != null && oDbReader["SELF_INSURE_DEDUCT"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Deductible", oDbReader["SELF_INSURE_DEDUCT"].ToString(), xCvgNode, xDoc);
                                }
                                else
                                {
                                    CreateAndAppendNode("Deductible", "0", xCvgNode, xDoc);
                                }
                                if (oDbReader["POLCVG_ROW_ID"] != null && oDbReader["POLCVG_ROW_ID"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("PolCvgRowId", string.Format("{0}|{1}|{2}", oDbReader["POLCVG_ROW_ID"].ToString(), p_iUnitStagingRowId, p_iPolicySystemId), xCvgNode, xDoc);
                                }

                                xRootNode.AppendChild(xCvgNode);
                            }
                        }
                    }

                    xDoc.AppendChild(xRootNode);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (oFactory!=null)
                    {
                        oFactory.Dispose();
                    }
                }
                return xDoc.OuterXml;
            }

            //added by swati agarwal
            public string GetUnitInterestList(int p_iUnitStagingRowId, int p_iPolicySystemId)
            {
                XmlDocument xDoc = null;
                XmlNode xRootNode = null;
                XmlNode xUnitInterestNode = null;
                DataModelFactory oFactoryStgngDmf = null;
                StringBuilder sSQLBuild = null;
                xDoc = new XmlDocument();
                DbReader oDbReader = null;

                try
                {
                    //oFactory = new DataModelFactory(m_oUserLogin, m_iClientId);
                    oFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);
                    sSQLBuild = new StringBuilder();
                    xRootNode = xDoc.CreateNode(XmlNodeType.Element, "InterestList", string.Empty);
                    sSQLBuild.Append("SELECT ENTITY_ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME, ADDR1, ADDR2, CITY, TABLE_NAME");
                    sSQLBuild.Append(" FROM ENTITY_STGNG ES LEFT OUTER JOIN GLOSSARY_TEXT GT ON GT.TABLE_ID = ES.ENTITY_TABLE_ID ");
                    sSQLBuild.Append(" WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM POLICY_X_ENTITY_STGNG WHERE POLICY_UNIT_ROW_ID = " + p_iUnitStagingRowId + ")");
                    oDbReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(sSQLBuild.ToString());

                    if (oDbReader != null)
                    {
                        while (oDbReader.Read())
                        {
                            xUnitInterestNode = xDoc.CreateNode(XmlNodeType.Element, "InterestList", string.Empty);

                            if (oDbReader["ENTITY_ID"] != null && oDbReader["ENTITY_ID"] != DBNull.Value)
                            {
                                CreateAndAppendNode("EntityId", oDbReader["ENTITY_ID"].ToString(), xUnitInterestNode, xDoc);
                            }
                            if (oDbReader["LAST_NAME"] != null && oDbReader["LAST_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Lastname", oDbReader["LAST_NAME"].ToString().Trim(), xUnitInterestNode, xDoc);
                            }
                            if (oDbReader["MIDDLE_NAME"] != null && oDbReader["MIDDLE_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Middlename", oDbReader["MIDDLE_NAME"].ToString().Trim(), xUnitInterestNode, xDoc);
                            }
                            if (oDbReader["FIRST_NAME"] != null && oDbReader["FIRST_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Firstname", oDbReader["FIRST_NAME"].ToString().Trim(), xUnitInterestNode, xDoc);
                            }
                            if (oDbReader["ADDR1"] != null && oDbReader["ADDR1"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Addr1", oDbReader["ADDR1"].ToString().Trim(), xUnitInterestNode, xDoc);
                            }
                            if (oDbReader["ADDR2"] != null && oDbReader["ADDR2"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Addr2", oDbReader["ADDR2"].ToString().Trim(), xUnitInterestNode, xDoc);
                            }
                            if (oDbReader["CITY"] != null && oDbReader["CITY"] != DBNull.Value)
                            {
                                CreateAndAppendNode("City", oDbReader["CITY"].ToString().Trim(), xUnitInterestNode, xDoc);
                            }
                            if (oDbReader["TABLE_NAME"] != null && oDbReader["TABLE_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Role", oDbReader["TABLE_NAME"].ToString().Trim(), xUnitInterestNode, xDoc);
                            }

                            xRootNode.AppendChild(xUnitInterestNode);
                        }
                    }

                    xDoc.AppendChild(xRootNode);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (oDbReader != null)
                    {
                        oDbReader.Dispose();
                    }
                    if (oFactoryStgngDmf!=null)
                    {
                        oFactoryStgngDmf.Dispose();
                    }
                }
                return xDoc.OuterXml;
            }
        */
            public string GetPSEndorsementData(int p_iPolicyId, string p_sTableName, int p_iUnitRowId, int p_iPolicySystemId)
            {
                LocalCache objCache = null;
                XmlDocument xDoc = null;
                XmlNode xRootNode = null;
                XmlNode xFormDataNode = null;
                DataModelFactory oFactory = null;
                DataModelFactory oFactoryStgngDmf = null;
                StringBuilder sSQLBuild = null;
                xDoc = new XmlDocument();
                DbReader oDbReader = null;
                int iTableId = 0;
                //Anu Tennyson Post WWIG handling Starts
                bool bTableIdPresent = false;
                //Anu Tennyson Ends
                try
                {
                    objCache = new LocalCache(m_sConnectString, m_iClientId);
                    oFactory = new DataModelFactory(m_oUserLogin, m_iClientId);
                    oFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);
                    sSQLBuild = new StringBuilder();

                    iTableId = objCache.GetTableId(p_sTableName);
                    xRootNode = xDoc.CreateNode(XmlNodeType.Element, "FormData", string.Empty);
                    sSQLBuild.Append("SELECT * FROM PS_ENDORSEMENT_STGNG WHERE POLICY_ID = " + p_iPolicyId + "  AND TABLE_ID = " + iTableId + " AND ROW_ID = " + p_iUnitRowId);
                    oDbReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(sSQLBuild.ToString());

                    if (oDbReader != null)
                    {
                        while (oDbReader.Read())
                        {
                            bTableIdPresent = true;
                            xFormDataNode = xDoc.CreateNode(XmlNodeType.Element, "FormData", string.Empty);

                            if (oDbReader["FORM_NUMBER"] != null && oDbReader["FORM_NUMBER"] != DBNull.Value)
                            {
                                CreateAndAppendNode("FormNumber", oDbReader["FORM_NUMBER"].ToString(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["FORM_DATE"] != null && oDbReader["FORM_DATE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("FormDate", Conversion.GetDate(oDbReader["FORM_DATE"].ToString().Trim()), xFormDataNode, xDoc);
                            }
                            if (oDbReader["FORM_DESCRIPTION"] != null && oDbReader["FORM_DESCRIPTION"] != DBNull.Value)
                            {
                                CreateAndAppendNode("FormDescription", oDbReader["FORM_DESCRIPTION"].ToString().Trim(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["STAT"] != null && oDbReader["STAT"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Stat", oDbReader["STAT"].ToString().Trim(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["INS_LINE"] != null && oDbReader["INS_LINE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("InsLine", oDbReader["INS_LINE"].ToString().Trim(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["LOC"] != null && oDbReader["LOC"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Loc", oDbReader["LOC"].ToString().Trim(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["BLDG"] != null && oDbReader["BLDG"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Bldg", oDbReader["BLDG"].ToString().Trim(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["UNIT"] != null && oDbReader["UNIT"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Unit", oDbReader["UNIT"].ToString().Trim(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["FORM_ACTION"] != null && oDbReader["FORM_ACTION"] != DBNull.Value)
                            {
                                CreateAndAppendNode("FormAction", oDbReader["FORM_ACTION"].ToString().Trim(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["EZ_SCM"] != null && oDbReader["EZ_SCM"] != DBNull.Value)
                            {
                                CreateAndAppendNode("EzScm", oDbReader["EZ_SCM"].ToString().Trim(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["DATA"] != null && oDbReader["DATA"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Data", oDbReader["DATA"].ToString().Trim(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["ITERATIVE"] != null && oDbReader["ITERATIVE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Iterative", oDbReader["ITERATIVE"].ToString().Trim(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["EDITIONDATE"] != null && oDbReader["EDITIONDATE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("EditionDate", oDbReader["EDITIONDATE"].ToString().Trim(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["RATEOP"] != null && oDbReader["RATEOP"] != DBNull.Value)
                            {
                                CreateAndAppendNode("RateOp", oDbReader["RATEOP"].ToString().Trim(), xFormDataNode, xDoc);
                            }
                            if (oDbReader["ENTRYDTE"] != null && oDbReader["ENTRYDTE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("EntryDte", oDbReader["ENTRYDTE"].ToString().Trim(), xFormDataNode, xDoc);
                            }

                            xRootNode.AppendChild(xFormDataNode);
                        }
                    }

                    if (!bTableIdPresent)
                    {
                        sSQLBuild.Append("SELECT * FROM PS_ENDORSEMENT_STGNG WHERE POLICY_ID = " + p_iPolicyId + "  AND TABLE_NAME = " + p_sTableName + " AND ROW_ID = " + p_iUnitRowId);
                        oDbReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(sSQLBuild.ToString());

                        if (oDbReader != null)
                        {
                            while (oDbReader.Read())
                            {
                                xFormDataNode = xDoc.CreateNode(XmlNodeType.Element, "FormData", string.Empty);

                                if (oDbReader["FORM_NUMBER"] != null && oDbReader["FORM_NUMBER"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("FormNumber", oDbReader["FORM_NUMBER"].ToString(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["FORM_DATE"] != null && oDbReader["FORM_DATE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("FormDate", Conversion.GetDate(oDbReader["FORM_DATE"].ToString().Trim()), xFormDataNode, xDoc);
                                }
                                if (oDbReader["FORM_DESCRIPTION"] != null && oDbReader["FORM_DESCRIPTION"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("FormDescription", oDbReader["FORM_DESCRIPTION"].ToString().Trim(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["STAT"] != null && oDbReader["STAT"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Stat", oDbReader["STAT"].ToString().Trim(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["INS_LINE"] != null && oDbReader["INS_LINE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("InsLine", oDbReader["INS_LINE"].ToString().Trim(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["LOC"] != null && oDbReader["LOC"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Loc", oDbReader["LOC"].ToString().Trim(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["BLDG"] != null && oDbReader["BLDG"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Bldg", oDbReader["BLDG"].ToString().Trim(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["UNIT"] != null && oDbReader["UNIT"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Unit", oDbReader["UNIT"].ToString().Trim(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["FORM_ACTION"] != null && oDbReader["FORM_ACTION"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("FormAction", oDbReader["FORM_ACTION"].ToString().Trim(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["EZ_SCM"] != null && oDbReader["EZ_SCM"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("EzScm", oDbReader["EZ_SCM"].ToString().Trim(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["DATA"] != null && oDbReader["DATA"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Data", oDbReader["DATA"].ToString().Trim(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["ITERATIVE"] != null && oDbReader["ITERATIVE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Iterative", oDbReader["ITERATIVE"].ToString().Trim(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["EDITIONDATE"] != null && oDbReader["EDITIONDATE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("EditionDate", oDbReader["EDITIONDATE"].ToString().Trim(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["RATEOP"] != null && oDbReader["RATEOP"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("RateOp", oDbReader["RATEOP"].ToString().Trim(), xFormDataNode, xDoc);
                                }
                                if (oDbReader["ENTRYDTE"] != null && oDbReader["ENTRYDTE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("EntryDte", oDbReader["ENTRYDTE"].ToString().Trim(), xFormDataNode, xDoc);
                                }

                                xRootNode.AppendChild(xFormDataNode);
                            }
                        }
                    }

                    xDoc.AppendChild(xRootNode);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (oDbReader!=null)
                    {
                        oDbReader.Dispose();
                    }
                    if (oFactory!=null)
                    {
                        oFactory.Dispose();
                    }
                    if (objCache!=null)
                    {
                        objCache.Dispose();
                    }
                }
                return xDoc.OuterXml;
            }

            public string GetStgngPolicyInterestDetail(int p_iEntityStagingId, int p_iPolicySystemId)
            {
                XmlDocument xDoc = null;
                XmlNode xRootNode = null;
                XmlNode xAdditionalInterestNode = null;
                DataModelFactory oFactoryStaging = null;

                StringBuilder sSQLBuild = null;
                xDoc = new XmlDocument();
                DbReader oDbReader = null;

                try
                {
                    sSQLBuild = new StringBuilder();
                    oFactoryStaging = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);
                    xRootNode = xDoc.CreateNode(XmlNodeType.Element, "AdditionalInterestList", string.Empty);
                    sSQLBuild.Append("SELECT FIRST_NAME, MIDDLE_NAME, LAST_NAME, ADDR1, ADDR2, CITY, STATE_ID, ZIP_CODE,COUNTY,TAX_ID, TABLE_NAME, ABBREVIATION, EFF_START_DATE, EFF_END_DATE");
                    sSQLBuild.Append(" FROM ENTITY_STGNG ES LEFT OUTER JOIN GLOSSARY_TEXT GT ON GT.TABLE_ID = ES.ENTITY_TABLE_ID");
                    sSQLBuild.Append(" WHERE ENTITY_ID = " + p_iEntityStagingId);
                    oDbReader = oFactoryStaging.Context.DbConnLookup.ExecuteReader(sSQLBuild.ToString());

                    if (oDbReader != null)
                    {
                        if (oDbReader.Read())
                        {
                            xAdditionalInterestNode = xDoc.CreateNode(XmlNodeType.Element, "AdditionalInterestList", string.Empty);
                            if (oDbReader["TABLE_NAME"] != null && oDbReader["TABLE_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("InterestType", oDbReader["TABLE_NAME"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["LAST_NAME"] != null && oDbReader["LAST_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Lastname", oDbReader["LAST_NAME"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["FIRST_NAME"] != null && oDbReader["FIRST_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Firstname", oDbReader["FIRST_NAME"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["MIDDLE_NAME"] != null && oDbReader["MIDDLE_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Middlename", oDbReader["MIDDLE_NAME"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["ABBREVIATION"] != null && oDbReader["ABBREVIATION"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Abbreviation", oDbReader["ABBREVIATION"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["ADDR1"] != null && oDbReader["ADDR1"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Addr1", oDbReader["ADDR1"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["ADDR2"] != null && oDbReader["ADDR2"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Addr2", oDbReader["ADDR2"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["CITY"] != null && oDbReader["CITY"] != DBNull.Value)
                            {
                                CreateAndAppendNode("City", oDbReader["CITY"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["STATE_ID"] != null && oDbReader["STATE_ID"] != DBNull.Value)
                            {
                                CreateAndAppendNode("State", oDbReader["STATE_ID"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["ZIP_CODE"] != null && oDbReader["ZIP_CODE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("ZipCode", oDbReader["ZIP_CODE"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["TAX_ID"] != null && oDbReader["TAX_ID"] != DBNull.Value)
                            {
                                CreateAndAppendNode("TaxId", oDbReader["TAX_ID"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["EFF_START_DATE"] != null && oDbReader["EFF_START_DATE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("EffectiveDate", Conversion.GetDate(oDbReader["EFF_START_DATE"].ToString().Trim()), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["EFF_END_DATE"] != null && oDbReader["EFF_END_DATE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("ExpirationDate", Conversion.GetDate(oDbReader["EFF_END_DATE"].ToString().Trim()), xAdditionalInterestNode, xDoc);
                            }
                            if (oDbReader["COUNTY"] != null && oDbReader["COUNTY"] != DBNull.Value)
                            {
                                CreateAndAppendNode("County", oDbReader["COUNTY"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                            }

                            xRootNode.AppendChild(xAdditionalInterestNode);
                        }
                    }

                    xDoc.AppendChild(xRootNode);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (oDbReader!= null)
                    {
                        oDbReader.Dispose();
                    }
                    if (oFactoryStaging!=null)
                    {
                        oFactoryStaging.Dispose();
                    }
                }
                return xDoc.OuterXml;
            }

            public bool SavePSEndorsementData(int p_iPolicyId, string p_sTableName, int p_TableRowid, int p_iDownloadedPolicyId, int p_iDownloadedUnitId)
            {
                LocalCache objCache = null;
                StringBuilder sbSQL = null ;
                DbConnection objCon = null;

                DbReader oDbReader = null;
                int iTableId = 0;
                bool bReturn = false;
                DataModelFactory oFactory = null;
                DataModelFactory oFactoryStgngDmf = null;

                string sFormNo = string.Empty;
                string sFormDesc = string.Empty;
                string sEditionDt = string.Empty;
                string sFormTextContent = string.Empty;
                string sFormDataArea = string.Empty;
                string sCscStat = string.Empty;
                string sCscUnit = string.Empty;
                string sCscBldg = string.Empty;
                string sCscLoc = string.Empty;
                string sCscInsLineCd = string.Empty;
                string sCscEZScrn = string.Empty;
                string sItrNo = string.Empty;
                string sRateOp = string.Empty;
                string sEntryDte = string.Empty;
                DbWriter writer = null;
                string sInsertedFormNo = string.Empty;
                //Anu Tennyson : POST andpre WWIG Starts
                bool bIsTableIdPresent = false;
                //Anu Tennyson Ends

                try
                {
                    objCache = new LocalCache(m_sConnectString, m_iClientId); 
                    objCon = DbFactory.GetDbConnection(m_sConnectString);
                    oFactory = new DataModelFactory(m_oUserLogin,m_iClientId );
                    oFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(GetPolicySystemId(p_iDownloadedPolicyId)), m_iClientId);
                    sbSQL = new StringBuilder();

                    iTableId = objCache.GetTableId(p_sTableName);
                    sbSQL.Append("SELECT * FROM PS_ENDORSEMENT_STGNG WHERE POLICY_ID = " + p_iPolicyId + " AND (TABLE_ID = " + iTableId + " OR TABLE_NAME = '" + p_sTableName + "')");
                    sbSQL.Append(" AND ROW_ID = " + p_TableRowid);
                    oDbReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(sbSQL.ToString());
                    if (oDbReader != null)
                    {
                        while (oDbReader.Read())
                        {
                            bIsTableIdPresent = true;
                            sFormNo = oDbReader["FORM_NUMBER"].ToString().Trim();
                            sFormDesc = oDbReader["FORM_DESCRIPTION"].ToString().Trim();
                            sCscStat = oDbReader["STAT"].ToString().Trim();
                            sCscInsLineCd = oDbReader["INS_LINE"].ToString().Trim();
                            sCscLoc = oDbReader["LOC"].ToString().Trim();
                            sCscBldg = oDbReader["BLDG"].ToString().Trim();
                            sCscUnit = oDbReader["UNIT"].ToString().Trim();
                            sFormTextContent = oDbReader["FORM_ACTION"].ToString().Trim();
                            sCscEZScrn = oDbReader["EZ_SCM"].ToString().Trim();
                            sFormDataArea = oDbReader["DATA"].ToString().Trim();
                            sEditionDt = oDbReader["EDITIONDATE"].ToString().Trim();
                            sItrNo = oDbReader["ITERATIVE"].ToString().Trim();
                            sRateOp = oDbReader["RATEOP"].ToString().Trim();
                            sEntryDte = oDbReader["ENTRYDTE"].ToString().Trim();

                            writer = DbFactory.GetDbWriter(objCon);

                            writer.Tables.Add("PS_ENDORSEMENT");
                            writer.Fields.Add("POLICY_ID", p_iDownloadedPolicyId);
                            writer.Fields.Add("TABLE_ID", iTableId);
                            writer.Fields.Add("ROW_ID", p_iDownloadedUnitId);
                            writer.Fields.Add("FORM_NUMBER", sFormNo);
                            writer.Fields.Add("FORM_DESCRIPTION", sFormDesc);
                            writer.Fields.Add("STAT", sCscStat);
                            writer.Fields.Add("INS_LINE", sCscInsLineCd);
                            writer.Fields.Add("LOC", sCscLoc);
                            writer.Fields.Add("BLDG", sCscBldg);
                            writer.Fields.Add("UNIT", sCscUnit);
                            writer.Fields.Add("FORM_ACTION", sFormTextContent);
                            writer.Fields.Add("EZ_SCM", sCscEZScrn);
                            writer.Fields.Add("DATA", sFormDataArea);
                            writer.Fields.Add("EDITIONDATE", sEditionDt);
                            writer.Fields.Add("ITERATIVE", sItrNo);
                            writer.Fields.Add("DTTM_RCD_ADDED", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                            writer.Fields.Add("ADDED_BY_USER", m_sUserName.ToString());
                            writer.Fields.Add("RATEOP", sRateOp);
                            writer.Fields.Add("ENTRYDTE", sEntryDte);
                            writer.Execute();
                        }
                    }
                    oDbReader.Dispose();
                    if (!bIsTableIdPresent)
                    {
                        sbSQL.Append("SELECT * FROM PS_ENDORSEMENT_STGNG WHERE POLICY_ID = " + p_iPolicyId + " AND (TABLE_ID = " + iTableId + " OR TABLE_NAME = '" + p_sTableName + "')");
                        sbSQL.Append(" AND ROW_ID = " + p_TableRowid);
                        oDbReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(sbSQL.ToString());
                        if (oDbReader != null)
                        {
                            while (oDbReader.Read())
                            {
                                sFormNo = oDbReader["FORM_NUMBER"].ToString().Trim();
                                sFormDesc = oDbReader["FORM_DESCRIPTION"].ToString().Trim();
                                sCscStat = oDbReader["STAT"].ToString().Trim();
                                sCscInsLineCd = oDbReader["INS_LINE"].ToString().Trim();
                                sCscLoc = oDbReader["LOC"].ToString().Trim();
                                sCscBldg = oDbReader["BLDG"].ToString().Trim();
                                sCscUnit = oDbReader["UNIT"].ToString().Trim();
                                sFormTextContent = oDbReader["FORM_ACTION"].ToString().Trim();
                                sCscEZScrn = oDbReader["EZ_SCM"].ToString().Trim();
                                sFormDataArea = oDbReader["DATA"].ToString().Trim();
                                sEditionDt = oDbReader["EDITIONDATE"].ToString().Trim();
                                sItrNo = oDbReader["ITERATIVE"].ToString().Trim();
                                sRateOp = oDbReader["RATEOP"].ToString().Trim();
                                sEntryDte = oDbReader["ENTRYDTE"].ToString().Trim();

                                writer = DbFactory.GetDbWriter(objCon);

                                writer.Tables.Add("PS_ENDORSEMENT");
                                writer.Fields.Add("POLICY_ID", p_iDownloadedPolicyId);
                                writer.Fields.Add("TABLE_ID", iTableId);
                                writer.Fields.Add("ROW_ID", p_iDownloadedUnitId);
                                writer.Fields.Add("FORM_NUMBER", sFormNo);
                                writer.Fields.Add("FORM_DESCRIPTION", sFormDesc);
                                writer.Fields.Add("STAT", sCscStat);
                                writer.Fields.Add("INS_LINE", sCscInsLineCd);
                                writer.Fields.Add("LOC", sCscLoc);
                                writer.Fields.Add("BLDG", sCscBldg);
                                writer.Fields.Add("UNIT", sCscUnit);
                                writer.Fields.Add("FORM_ACTION", sFormTextContent);
                                writer.Fields.Add("EZ_SCM", sCscEZScrn);
                                writer.Fields.Add("DATA", sFormDataArea);
                                writer.Fields.Add("EDITIONDATE", sEditionDt);
                                writer.Fields.Add("ITERATIVE", sItrNo);
                                writer.Fields.Add("DTTM_RCD_ADDED", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                writer.Fields.Add("ADDED_BY_USER", m_sUserName.ToString());
                                writer.Fields.Add("RATEOP", sRateOp);
                                writer.Fields.Add("ENTRYDTE", sEntryDte);
                                writer.Execute();
                            }
                        }
                    }

                    bReturn = true;
                    return bReturn;
                }

                catch (Exception p_objExp)
                {
                    if (sbSQL!=null)
                    {
                        Log.Write(sbSQL.ToString(), m_iClientId);
                    }
                    Log.Write(p_objExp.ToString(), m_iClientId);
                    throw new RMAppException(Globalization.GetString("PolicyStagingInterface.SavePSEndorsementData.Error", m_iClientId), p_objExp);
                }
                finally
                {
                    writer = null;
                    if (objCache != null)
                    {
                        objCache.Dispose();
                        objCache = null;
                    }
                    if (objCon != null)
                    {
                        objCon.Close();
                        objCon.Dispose();
                    }
                    if (oFactory!=null)
                    {
                        oFactory.Dispose();
                    }
                    if (oFactoryStgngDmf!= null)
                    {
                        oFactory.Dispose();
                    }
                }
            }
            //change end here by swati

            /// <summary>
            /// Gets Policy Interest list 
            /// </summary>
            /// <param name="p_iPolicyStagingId"></param>
            /// <returns></returns>
            /// Pradyumna WWIG GAP 16 1/6/2014
            public string GetPolicyInterestList(int p_iPolicyStagingId, int p_iPolicySystemId)
            {
                XmlDocument xReturnDoc = null;
                DataModelFactory oDMFStaging = null;
                XmlNode xRootNode = null;
                XmlNode xPolicyInterestNode = null;
                string sSQL = string.Empty;
                int iTableId = 0;
                //Anu Tennyson WWIG Pre and Post
                bool bIsTableIdPrensent = false;
                //Anu Tenyson Ends
                try
                {
                    oDMFStaging = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);
                    xReturnDoc = new XmlDocument();
                    iTableId = oDMFStaging.Context.LocalCache.GetTableId("DRIVERS");
                    sSQL = "SELECT ES.ENTITY_ID, ES.FIRST_NAME, ES.MIDDLE_NAME, ES.LAST_NAME, ES.ADDR1, ES.ADDR2, ES.CITY, GT.TABLE_NAME FROM ENTITY_STGNG ES INNER JOIN POLICY_X_ENTITY_STGNG PXE ON PXE.ENTITY_ID = ES.ENTITY_ID LEFT OUTER JOIN GLOSSARY_TEXT GT ON GT.TABLE_ID = ES.ENTITY_TABLE_ID WHERE PXE.POLICY_ID = " + p_iPolicyStagingId + " AND ES.ENTITY_TABLE_ID <> " + iTableId + " AND PXE.POLICY_UNIT_ROW_ID = 0";
                    //sSQL = "SELECT ENTITY_ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME, ADDR1, ADDR2, CITY, TABLE_NAME FROM ENTITY_STGNG ES LEFT OUTER JOIN GLOSSARY_TEXT GT ON GT.TABLE_ID = ES.ENTITY_TABLE_ID WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM POLICY_X_ENTITY_STGNG WHERE POLICY_ID = " + p_iPolicyStagingId + ")";
                    using (DbReader oDbReader = oDMFStaging.Context.DbConnLookup.ExecuteReader(sSQL))
                    {
                        xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "PolicyInterestList", string.Empty);
                        if (oDbReader != null)
                        {
                            while (oDbReader.Read())
                            {
                                bIsTableIdPrensent = true;
                                xPolicyInterestNode = xReturnDoc.CreateNode(XmlNodeType.Element, "InterestList", string.Empty);

                                if (oDbReader["ENTITY_ID"] != null && oDbReader["ENTITY_ID"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("EntityId", oDbReader["ENTITY_ID"].ToString(), xPolicyInterestNode, xReturnDoc);

                                    if (oDbReader["FIRST_NAME"] != null && oDbReader["FIRST_NAME"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("Firstname", oDbReader["FIRST_NAME"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                    }
                                    if (oDbReader["MIDDLE_NAME"] != null && oDbReader["MIDDLE_NAME"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("Middlename", oDbReader["MIDDLE_NAME"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                    }
                                    if (oDbReader["LAST_NAME"] != null && oDbReader["LAST_NAME"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("Lastname", oDbReader["LAST_NAME"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                    }
                                    if (oDbReader["ADDR1"] != null && oDbReader["ADDR1"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("Addr1", oDbReader["ADDR1"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                    }
                                    if (oDbReader["ADDR2"] != null && oDbReader["ADDR2"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("Addr2", oDbReader["ADDR2"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                    }
                                    if (oDbReader["CITY"] != null && oDbReader["CITY"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("City", oDbReader["CITY"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                    }
                                    if (oDbReader["TABLE_NAME"] != null && oDbReader["TABLE_NAME"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("Role", oDbReader["TABLE_NAME"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                    }
                                }
                                xRootNode.AppendChild(xPolicyInterestNode);
                            }
                        }
                    }
                    if (!bIsTableIdPrensent)
                    {
                        sSQL = "SELECT ES.ENTITY_ID, ES.FIRST_NAME, ES.MIDDLE_NAME, ES.LAST_NAME, ES.ADDR1, ES.ADDR2, ES.CITY, GT.TABLE_NAME FROM ENTITY_STGNG ES INNER JOIN POLICY_X_ENTITY_STGNG PXE ON PXE.ENTITY_ID = ES.ENTITY_ID LEFT OUTER JOIN GLOSSARY_TEXT GT ON GT.TABLE_ID = ES.ENTITY_TABLE_ID WHERE PXE.POLICY_ID = 'DRIVERS' AND ES.ENTITY_TABLE_NAME <> 'DRIVERS' AND PXE.POLICY_UNIT_ROW_ID = 0";
                        //sSQL = "SELECT ENTITY_ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME, ADDR1, ADDR2, CITY, TABLE_NAME FROM ENTITY_STGNG ES LEFT OUTER JOIN GLOSSARY_TEXT GT ON GT.TABLE_ID = ES.ENTITY_TABLE_ID WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM POLICY_X_ENTITY_STGNG WHERE POLICY_ID = " + p_iPolicyStagingId + ")";
                        using (DbReader oDbReader = oDMFStaging.Context.DbConnLookup.ExecuteReader(sSQL))
                        {
                            xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "PolicyInterestList", string.Empty);
                            if (oDbReader != null)
                            {
                                while (oDbReader.Read())
                                {
                                    //bIsTableIdPrensent = true;
                                    xPolicyInterestNode = xReturnDoc.CreateNode(XmlNodeType.Element, "InterestList", string.Empty);

                                    if (oDbReader["ENTITY_ID"] != null && oDbReader["ENTITY_ID"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("EntityId", oDbReader["ENTITY_ID"].ToString(), xPolicyInterestNode, xReturnDoc);

                                        if (oDbReader["FIRST_NAME"] != null && oDbReader["FIRST_NAME"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("Firstname", oDbReader["FIRST_NAME"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                        }
                                        if (oDbReader["MIDDLE_NAME"] != null && oDbReader["MIDDLE_NAME"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("Middlename", oDbReader["MIDDLE_NAME"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                        }
                                        if (oDbReader["LAST_NAME"] != null && oDbReader["LAST_NAME"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("Lastname", oDbReader["LAST_NAME"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                        }
                                        if (oDbReader["ADDR1"] != null && oDbReader["ADDR1"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("Addr1", oDbReader["ADDR1"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                        }
                                        if (oDbReader["ADDR2"] != null && oDbReader["ADDR2"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("Addr2", oDbReader["ADDR2"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                        }
                                        if (oDbReader["CITY"] != null && oDbReader["CITY"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("City", oDbReader["CITY"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                        }
                                        if (oDbReader["TABLE_NAME"] != null && oDbReader["TABLE_NAME"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("Role", oDbReader["TABLE_NAME"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                        }
                                    }
                                    xRootNode.AppendChild(xPolicyInterestNode);
                                }
                            }
                        }
                    }
                    xReturnDoc.AppendChild(xRootNode);
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    if (oDMFStaging != null)
                    {
                        oDMFStaging.Dispose();
                    }
                    xRootNode = null;
                    xPolicyInterestNode = null;
                }

                return xReturnDoc.OuterXml;
            }

            /// <summary>
            /// Gets Policy Driver list 
            /// </summary>
            /// <param name="p_iPolicyStagingId"></param>
            /// <returns></returns>
            /// Pradyumna WWIG GAP 16 1/8/2014
            public string GetDriverList(int p_iPolicyStagingId, int p_iPolicySystemId)
            {
                XmlDocument xReturnDoc = null;
                DataModelFactory oDMFStaging = null;
                XmlNode xRootNode = null;
                XmlNode xPolicyDriverNode = null;
                string sSQL = string.Empty;
                int iTableId = 0;
                //Anu Tennyson POSt and PRE WWIG starts
                bool bIsTableIdPresent = false;
                //Anu Tennyson Ends
                try
                {
                    oDMFStaging = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);
                    xReturnDoc = new XmlDocument();
                    iTableId = oDMFStaging.Context.LocalCache.GetTableId("DRIVERS");
                    sSQL = "SELECT ES.ENTITY_ID, ES.FIRST_NAME, ES.MIDDLE_NAME, ES.LAST_NAME, ES.STATE_ID, DS.DRIVER_TYPE, DS.LICENCE_NUMBER FROM ENTITY_STGNG ES INNER JOIN POLICY_X_ENTITY_STGNG PXE ON PXE.ENTITY_ID = ES.ENTITY_ID INNER JOIN DRIVER_STGNG DS ON DS.DRIVER_EID = ES.ENTITY_ID WHERE PXE.POLICY_ID = " + p_iPolicyStagingId + " AND ES.ENTITY_TABLE_ID = " + iTableId;
                    using (DbReader oDbReader = oDMFStaging.Context.DbConnLookup.ExecuteReader(sSQL))
                    {
                        xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "PolicyDriverList", string.Empty);
                        if (oDbReader != null)
                        {
                            while (oDbReader.Read())
                            {
                                xPolicyDriverNode = xReturnDoc.CreateNode(XmlNodeType.Element, "DriverList", string.Empty);
                                bIsTableIdPresent = true;
                                if (oDbReader["ENTITY_ID"] != null && oDbReader["ENTITY_ID"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("EntityId", oDbReader["ENTITY_ID"].ToString(), xPolicyDriverNode, xReturnDoc);

                                    if (oDbReader["FIRST_NAME"] != null && oDbReader["FIRST_NAME"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("Firstname", oDbReader["FIRST_NAME"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                    }
                                    if (oDbReader["MIDDLE_NAME"] != null && oDbReader["MIDDLE_NAME"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("Middlename", oDbReader["MIDDLE_NAME"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                    }
                                    if (oDbReader["LAST_NAME"] != null && oDbReader["LAST_NAME"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("Lastname", oDbReader["LAST_NAME"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                    }
                                    if (oDbReader["STATE_ID"] != null && oDbReader["STATE_ID"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("State", oDbReader["STATE_ID"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                    }
                                    if (oDbReader["DRIVER_TYPE"] != null && oDbReader["DRIVER_TYPE"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("DriverType", oDbReader["DRIVER_TYPE"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                    }
                                    if (oDbReader["LICENCE_NUMBER"] != null && oDbReader["LICENCE_NUMBER"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("LicenceNum", oDbReader["LICENCE_NUMBER"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                    }
                                }
                                xRootNode.AppendChild(xPolicyDriverNode);
                            }
                        }
                    }
                    if (!bIsTableIdPresent)
                    {
                        sSQL = "SELECT ES.ENTITY_ID, ES.FIRST_NAME, ES.MIDDLE_NAME, ES.LAST_NAME, ES.STATE_ID, DS.DRIVER_TYPE, DS.LICENCE_NUMBER FROM ENTITY_STGNG ES INNER JOIN POLICY_X_ENTITY_STGNG PXE ON PXE.ENTITY_ID = ES.ENTITY_ID INNER JOIN DRIVER_STGNG DS ON DS.DRIVER_EID = ES.ENTITY_ID WHERE PXE.POLICY_ID = " + p_iPolicyStagingId + " AND ES.ENTITY_TABLE_NAME = 'DRIVERS'" ;
                        using (DbReader oDbReader = oDMFStaging.Context.DbConnLookup.ExecuteReader(sSQL))
                        {
                            xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "PolicyDriverList", string.Empty);
                            if (oDbReader != null)
                            {
                                while (oDbReader.Read())
                                {
                                    xPolicyDriverNode = xReturnDoc.CreateNode(XmlNodeType.Element, "DriverList", string.Empty);
                                    bIsTableIdPresent = true;
                                    if (oDbReader["ENTITY_ID"] != null && oDbReader["ENTITY_ID"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("EntityId", oDbReader["ENTITY_ID"].ToString(), xPolicyDriverNode, xReturnDoc);

                                        if (oDbReader["FIRST_NAME"] != null && oDbReader["FIRST_NAME"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("Firstname", oDbReader["FIRST_NAME"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                        }
                                        if (oDbReader["MIDDLE_NAME"] != null && oDbReader["MIDDLE_NAME"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("Middlename", oDbReader["MIDDLE_NAME"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                        }
                                        if (oDbReader["LAST_NAME"] != null && oDbReader["LAST_NAME"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("Lastname", oDbReader["LAST_NAME"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                        }
                                        if (oDbReader["STATE_ID"] != null && oDbReader["STATE_ID"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("State", oDbReader["STATE_ID"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                        }
                                        if (oDbReader["DRIVER_TYPE"] != null && oDbReader["DRIVER_TYPE"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("DriverType", oDbReader["DRIVER_TYPE"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                        }
                                        if (oDbReader["LICENCE_NUMBER"] != null && oDbReader["LICENCE_NUMBER"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("LicenceNum", oDbReader["LICENCE_NUMBER"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                        }
                                    }
                                    xRootNode.AppendChild(xPolicyDriverNode);
                                }
                            }
                        }
                    }
                    xReturnDoc.AppendChild(xRootNode);
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    if (oDMFStaging != null)
                        oDMFStaging.Dispose();
                    xRootNode = null;
                    xPolicyDriverNode = null;
                }

                return xReturnDoc.OuterXml;
            }

            /// <summary>
            /// Get Driver Detail from Staging Table
            /// </summary>
            /// <param name="p_iStagingEntityId"></param>
            /// <returns></returns>
            public string GetDriverDetails(int p_iStagingEntityId, int p_iPolicySystemId)
            {
                DataModelFactory oFactoryStaging = null;
                XmlDocument xReturnDoc = null;
                XmlNode xRootNode = null;
                string sSQL = string.Empty;

                try
                {
                    xReturnDoc = new XmlDocument();
                    oFactoryStaging = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);

                    sSQL = "SELECT DS.DRIVER_TYPE, ES.FIRST_NAME, ES.MIDDLE_NAME, ES.LAST_NAME, ES.SUFFIX_COMMON, ES.BIRTH_DATE, ES.SEX_CODE, DS.MARITAL_STAT_CODE, DS.LICENCE_NUMBER, DS.LICENCE_DATE, DS.RELTN_INSRD, DS.LICENCE_STATE, DS.MVR_IND, DS.DRIVER_STATUS, DS.SR, DS.DRIVER_CLASS, DS.PO1, DS.PO2, DS.PO3, DS.PTO1, DS.PTO2, DS.PTO3 FROM ENTITY_STGNG ES INNER JOIN DRIVER_STGNG DS ON DS.DRIVER_EID = ES.ENTITY_ID WHERE ENTITY_ID = " + p_iStagingEntityId;

                    using (DbReader oDbReader = oFactoryStaging.Context.DbConnLookup.ExecuteReader(sSQL))
                    {
                        xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "DriverDetail", string.Empty);

                        if (oDbReader != null)
                        {
                            if (oDbReader.Read())
                            {
                                if (oDbReader["DRIVER_TYPE"] != null && oDbReader["DRIVER_TYPE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("DriverType", oDbReader["DRIVER_TYPE"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["FIRST_NAME"] != null && oDbReader["FIRST_NAME"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("FirstName", oDbReader["FIRST_NAME"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["MIDDLE_NAME"] != null && oDbReader["MIDDLE_NAME"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("MiddleName", oDbReader["MIDDLE_NAME"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["LAST_NAME"] != null && oDbReader["LAST_NAME"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("LastName", oDbReader["LAST_NAME"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["SUFFIX_COMMON"] != null && oDbReader["SUFFIX_COMMON"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Suffix", oDbReader["SUFFIX_COMMON"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["BIRTH_DATE"] != null && oDbReader["BIRTH_DATE"] != DBNull.Value)
                                {
                                    int iAge = 0;
                                    string sBirthDate = oDbReader["BIRTH_DATE"].ToString().Trim();
                                    if (sBirthDate.Length == 8)
                                    {
                                        string sBirthDateUI = Conversion.GetDBDateFormat(sBirthDate, "MM/dd/yyyy");
                                        CreateAndAppendNode("DOB", sBirthDateUI, xRootNode, xReturnDoc);

                                        DateTime dtBirthDate = Conversion.ToDate(sBirthDate);
                                        iAge = CalculateAge(dtBirthDate);

                                        CreateAndAppendNode("Age", iAge.ToString(), xRootNode, xReturnDoc);
                                    }
                                }
                                if (oDbReader["SEX_CODE"] != null && oDbReader["SEX_CODE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("SexCode", oDbReader["SEX_CODE"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["MARITAL_STAT_CODE"] != null && oDbReader["MARITAL_STAT_CODE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("MaritalStatus", oDbReader["MARITAL_STAT_CODE"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["LICENCE_NUMBER"] != null && oDbReader["LICENCE_NUMBER"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("LicNo", oDbReader["LICENCE_NUMBER"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["LICENCE_DATE"] != null && oDbReader["LICENCE_DATE"] != DBNull.Value)
                                {
                                    string sLicDate = oDbReader["LICENCE_DATE"].ToString().Trim();
                                    if (sLicDate.Length == 8)
                                    {
                                        sLicDate = Conversion.GetDBDateFormat(sLicDate, "MM/dd/yyyy");
                                        CreateAndAppendNode("LicDate", sLicDate, xRootNode, xReturnDoc);
                                    }
                                }
                                if (oDbReader["RELTN_INSRD"] != null && oDbReader["RELTN_INSRD"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Relation", oDbReader["RELTN_INSRD"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["LICENCE_STATE"] != null && oDbReader["LICENCE_STATE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("LicState", oDbReader["LICENCE_STATE"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["MVR_IND"] != null && oDbReader["MVR_IND"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("MVRInd", oDbReader["MVR_IND"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["DRIVER_STATUS"] != null && oDbReader["DRIVER_STATUS"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("DriverStatus", oDbReader["DRIVER_STATUS"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["SR"] != null && oDbReader["SR"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("SR", oDbReader["SR"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["DRIVER_CLASS"] != null && oDbReader["DRIVER_CLASS"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("DriverCls", oDbReader["DRIVER_CLASS"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["PO1"] != null && oDbReader["PO1"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("PO1", oDbReader["PO1"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["PO2"] != null && oDbReader["PO2"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("PO2", oDbReader["PO2"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["PO3"] != null && oDbReader["PO3"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("PO3", oDbReader["PO3"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["PTO1"] != null && oDbReader["PTO1"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("PTO1", oDbReader["PTO1"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["PTO2"] != null && oDbReader["PTO2"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("PTO2", oDbReader["PTO2"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                                if (oDbReader["PTO3"] != null && oDbReader["PTO3"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("PTO3", oDbReader["PTO3"].ToString().Trim(), xRootNode, xReturnDoc);
                                }
                            }
                        }
                        xReturnDoc.AppendChild(xRootNode);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if(oFactoryStaging != null)
                        oFactoryStaging.Dispose();
                    xRootNode = null;
                }

                return xReturnDoc.OuterXml;
            }

            /// <summary>
            /// Calculates Age using BirthDate as an input
            /// </summary>
            /// <param name="dtBirthDate"></param>
            /// <returns>Age</returns>
            private int CalculateAge(DateTime dtBirthDate)
            {
                int iAge = 0;
                DateTime dtCurrent = DateTime.Now;

                iAge = dtCurrent.Year - dtBirthDate.Year;
                if (dtCurrent.Month < dtBirthDate.Month || (dtCurrent.Month == dtBirthDate.Month && dtCurrent.Day < dtBirthDate.Day))
                    iAge--;
                return iAge;
            }

           
            //Policy Staging Changes Start
            //Raman 3/24 : Reverting rmA-6683 : Policy Staging solution should be compliant to policy api 
            //it needs to use base tables and datamodel
        /*
            private XmlDocument GetPolicyXEntityStagingData(int p_iStagingPolicyId, string p_sMode, out bool p_bExists, int p_iPolicyId)
            {
                XmlDocument xReturnDoc = null;
                XmlNode xRootNode = null;
                XmlNode xEntityNode = null;
                XmlNode xInstanceNode = null;
                p_bExists = false;

                PolicyStgng oPolicyStgng = null;
                DataModelFactory oFactory = null;
                DataModelFactory oFactoryStgngDmf = null;
                EntityStgng oEntityStgng = null;
                PolicyXUnitStgng oPolXUnitStgng = null;

                int iAgentsTableId = 0;
                int iInusredTableId = 0;
                int iDriversTableId = 0;
                string sStatUnitNum = string.Empty;

                try
                {
                    xReturnDoc = new XmlDocument();
                    xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Entities", string.Empty);

                    xInstanceNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Instance", string.Empty);
                    oFactory = new DataModelFactory(m_oUserLogin, m_iClientId); ;
                    oFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(GetPolicySystemId(p_iPolicyId)), m_iClientId);
                    oPolicyStgng = oFactoryStgngDmf.GetDataModelObject("PolicyStgng", false) as PolicyStgng;
                    oEntityStgng = oFactoryStgngDmf.GetDataModelObject("EntityStgng", false) as EntityStgng;

                    oPolicyStgng.MoveTo(p_iStagingPolicyId);

                    iAgentsTableId = oFactory.Context.LocalCache.GetTableId("AGENTS");
                    iInusredTableId = oFactory.Context.LocalCache.GetTableId("POLICY_INSURED");
                    iDriversTableId = oFactory.Context.LocalCache.GetTableId("DRIVERS");

                    foreach (PolicyXEntityStgng oPolicyXEntityStgng in oPolicyStgng.PolicyXEntityListStgng)
                    {
                        oEntityStgng.MoveTo(oPolicyXEntityStgng.EntityId);
                        // Pradyumna - Added if/Elseif for Unit Interest - Start
                        if ((string.Equals(p_sMode, "entity", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            if (oEntityStgng.EntityTableId == iAgentsTableId || oEntityStgng.EntityTableName == "AGENTS" || oEntityStgng.EntityTableName == "POLICY_INSURED" || oEntityStgng.EntityTableId == iInusredTableId || oPolicyXEntityStgng.PolicyUnitRowid > 0 || oEntityStgng.EntityTableId == iDriversTableId || oEntityStgng.EntityTableName == "DRIVERS")
                            {
                                continue;
                            }
                        }
                        else if ((string.Equals(p_sMode, "Driver", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            if (oEntityStgng.EntityTableId != iDriversTableId || oEntityStgng.EntityTableName != "DRIVERS")
                            {
                                continue;
                            }
                        }
                        else if ((string.Equals(p_sMode, "unitinterest", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            if (oEntityStgng.EntityTableId == iAgentsTableId || oEntityStgng.EntityTableName == "AGENTS" || oEntityStgng.EntityTableId == iInusredTableId || oEntityStgng.EntityTableName == "POLICY_INSURED" || oPolicyXEntityStgng.PolicyUnitRowid <= 0 || oEntityStgng.EntityTableId == iDriversTableId || oEntityStgng.EntityTableName == "DRIVERS")
                            {
                                continue;
                            }
                        }

                        else if ((string.Equals(p_sMode, "coveragelist", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            if (oEntityStgng.EntityTableId == iAgentsTableId || oEntityStgng.EntityTableName == "AGENTS" || oEntityStgng.EntityTableId == iInusredTableId || oEntityStgng.EntityTableName == "POLICY_INSURED" || oPolicyXEntityStgng.PolicyUnitRowid <= 0 || oEntityStgng.EntityTableId == iDriversTableId || oEntityStgng.EntityTableName == "DRIVERS")
                            {
                                continue;
                            }
                        }

                        if (oPolicyXEntityStgng.PolicyUnitRowid > 0)
                        {
                            oPolXUnitStgng = oFactoryStgngDmf.GetDataModelObject("PolicyXUnitStgng", false) as PolicyXUnitStgng;
                            oPolXUnitStgng.MoveTo(oPolicyXEntityStgng.PolicyUnitRowid);
                            string sSQL = "SELECT STAT_UNIT_NUMBER FROM POINT_UNIT_DATA_STGNG WHERE UNIT_ID = " + oPolXUnitStgng.UnitId + " AND UNIT_TYPE = '" + oPolXUnitStgng.UnitType.Trim() + "'";
                            object objTemp = DbFactory.ExecuteScalar(oFactoryStgngDmf.Context.DbConn.ConnectionString, sSQL);
                            if (objTemp != null)
                            {
                                sStatUnitNum = (objTemp).ToString();
                            }
                        }
                        // Pradyumna - Added if/Elseif for Unit Interest - Ends
                        p_bExists = true;
                        xEntityNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Entity", string.Empty);
                        CreateAndAppendNode("SequenceNumber", oEntityStgng.EntityId.ToString(), xEntityNode, xReturnDoc);
                        CreateAndAppendNode("TaxID", string.Empty, xEntityNode, xReturnDoc);
                        CreateAndAppendNode("FirstName", oEntityStgng.FirstName, xEntityNode, xReturnDoc);
                        CreateAndAppendNode("MiddleName", oEntityStgng.MiddleName, xEntityNode, xReturnDoc);
                        CreateAndAppendNode("LastName", oEntityStgng.LastName, xEntityNode, xReturnDoc);
                        CreateAndAppendNode("RoleCd", oPolicyXEntityStgng.ExternalRole, xEntityNode, xReturnDoc);
                        CreateAndAppendNode("StatUnitNumber", sStatUnitNum, xEntityNode, xReturnDoc);
                        CreateAndAppendNode("TableId", oEntityStgng.EntityTableId.ToString(), xEntityNode, xReturnDoc);

                        xInstanceNode.AppendChild(xEntityNode);
                    }
                    xRootNode.AppendChild(xInstanceNode);
                    xReturnDoc.AppendChild(xRootNode);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    xRootNode = null;
                    xEntityNode = null;
                    xInstanceNode = null;
                    if (oEntityStgng!=null)
                    {
                        oEntityStgng.Dispose();
                    }
                    if (oFactory!=null)
                    {
                        oFactory.Dispose();
                    }
                    if (oFactoryStgngDmf!= null)
                    {
                        oFactoryStgngDmf.Dispose();
                    }
                    if (oPolicyStgng!=null)
                    {
                        oPolicyStgng.Dispose();
                    }
                    if( oPolXUnitStgng != null)
                    {
                        oPolXUnitStgng.Dispose();
                    }
                }

                return xReturnDoc;
            }

            // // Pradyumna 7/22/2014 - Modified - MITS 36800 - Start
            // Method Overload
            private XmlDocument GetPolicyXEntityStagingData(int p_iPolicyId, int p_iStagingPolicyId, string p_sMode, out bool p_bExists)
            {
                XmlDocument xReturnDoc = null;
                XmlNode xRootNode = null;
                XmlNode xEntityNode = null;
                XmlNode xInstanceNode = null;
                p_bExists = false;

                PolicyStgng oPolicyStgng = null;
                DataModelFactory oFactory = null;
                DataModelFactory oFactoryStgngDmf = null;
                EntityStgng oEntityStgng = null;
                PolicyXUnitStgng oPolXUnitStgng = null;

                int iAgentsTableId = 0;
                int iInusredTableId = 0;

                int iDriversTableId = 0;
                string sStatUnitNum = string.Empty;
                string sUnitSQL = string.Empty; // Pradyumna 7/22/2014 - Modified - MITS 36800
                List<UnitInfo> liUnitInfo = new List<UnitInfo>(); // Pradyumna 7/22/2014 - Modified - MITS 36800
                List<UnitInfo> lstUnitDwnloaded = new List<UnitInfo>(); // Pradyumna 7/22/2014 - Modified - MITS 36800
                bool bSuccess = false; // Pradyumna 7/22/2014 - Modified - MITS 36800
                List<int> lstUnitId = new List<int>(); // Pradyumna 7/22/2014 - Modified - MITS 36800

                try
                {
                    xReturnDoc = new XmlDocument();
                    xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Entities", string.Empty);

                    xInstanceNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Instance", string.Empty);
                    oFactory = new DataModelFactory(m_oUserLogin, m_iClientId); ;
                    oFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(GetPolicySystemId(p_iPolicyId)), m_iClientId);
                    oPolicyStgng = oFactoryStgngDmf.GetDataModelObject("PolicyStgng", false) as PolicyStgng;
                    oEntityStgng = oFactoryStgngDmf.GetDataModelObject("EntityStgng", false) as EntityStgng;

                    oPolicyStgng.MoveTo(p_iStagingPolicyId);

                    iAgentsTableId = oFactory.Context.LocalCache.GetTableId("AGENTS");
                    iInusredTableId = oFactory.Context.LocalCache.GetTableId("POLICY_INSURED");
                    iDriversTableId = oFactory.Context.LocalCache.GetTableId("DRIVERS");

                    // Pradyumna 7/22/2014 - Modified - MITS 36800 - Start
                    sUnitSQL = "SELECT UNIT_ID,UNIT_TYPE FROM POLICY_X_UNIT WHERE POLICY_ID = " + p_iPolicyId;
                    using (DbReader objDbRdr = DbFactory.ExecuteReader(oFactory.Context.DbConnLookup.ConnectionString, sUnitSQL))
                    {
                        if (objDbRdr != null)
                        {
                            while (objDbRdr.Read())
                            {
                                UnitInfo stUnitDownloaded = new UnitInfo();
                                if (objDbRdr["UNIT_ID"] != null && objDbRdr["UNIT_ID"] != DBNull.Value)
                                {
                                    stUnitDownloaded.UnitId =  Conversion.CastToType<int>(objDbRdr["UNIT_ID"].ToString(),out bSuccess);
                                }
                                if (objDbRdr["UNIT_TYPE"] != null && objDbRdr["UNIT_TYPE"] != DBNull.Value)
                                {
                                    stUnitDownloaded.UnitType = objDbRdr["UNIT_TYPE"].ToString().Trim();
                                }

                                lstUnitDwnloaded.Add(stUnitDownloaded);
                            }
                        }
                    }

                    foreach (UnitInfo uUnitDwnloaded in lstUnitDwnloaded)
                    {
                        sUnitSQL = "SELECT UNIT_TYPE, STAT_UNIT_NUMBER FROM POINT_UNIT_DATA WHERE UNIT_ID = " + uUnitDwnloaded.UnitId + " AND UNIT_TYPE = '" + uUnitDwnloaded.UnitType + "'";
                        using (DbReader objDbRdr = DbFactory.ExecuteReader(oFactory.Context.DbConnLookup.ConnectionString, sUnitSQL))
                        {
                            if (objDbRdr != null)
                            {
                                while (objDbRdr.Read())
                                {
                                    UnitInfo stUnitInfo = new UnitInfo();

                                    if (objDbRdr["UNIT_TYPE"] != null && objDbRdr["UNIT_TYPE"] != DBNull.Value)
                                    {
                                        stUnitInfo.UnitType = objDbRdr["UNIT_TYPE"].ToString().Trim();
                                    }
                                    if (objDbRdr["STAT_UNIT_NUMBER"] != null && objDbRdr["STAT_UNIT_NUMBER"] != DBNull.Value)
                                    {
                                        stUnitInfo.StatUnitNum = objDbRdr["STAT_UNIT_NUMBER"].ToString().Trim();
                                    }
                                    liUnitInfo.Add(stUnitInfo);
                                }
                            }
                        }
                    }

                    foreach (UnitInfo stUnitDetail in liUnitInfo)
                    {
                        sUnitSQL = "SELECT PUD.UNIT_ID FROM POINT_UNIT_DATA_STGNG PUD INNER JOIN POLICY_X_UNIT_STGNG PXU ON PXU.UNIT_ID = PUD.UNIT_ID WHERE PUD.UNIT_TYPE = '" + stUnitDetail.UnitType + "' AND STAT_UNIT_NUMBER = '" + stUnitDetail.StatUnitNum + "' AND PXU.POLICY_ID = " + p_iStagingPolicyId;
                        using (DbReader objDbRdr = DbFactory.ExecuteReader(oFactoryStgngDmf.Context.DbConnLookup.ConnectionString, sUnitSQL))
                        {
                            if (objDbRdr != null)
                            {
                                while (objDbRdr.Read())
                                {
                                    if (objDbRdr["UNIT_ID"] != null && objDbRdr["UNIT_ID"] != DBNull.Value)
                                    {
                                        int iUnitId = Conversion.CastToType<int>(objDbRdr["UNIT_ID"].ToString(), out bSuccess);
                                        if (iUnitId > 0)
                                            lstUnitId.Add(iUnitId);
                                    }
                                }
                            }
                        }
                    }
                    // Pradyumna 7/22/2014 - Modified - MITS 36800 - End

                    foreach (PolicyXEntityStgng oPolicyXEntityStgng in oPolicyStgng.PolicyXEntityListStgng)
                    {
                        oEntityStgng.MoveTo(oPolicyXEntityStgng.EntityId);
                        // Pradyumna - Added if/Elseif for Unit Interest - Start
                        if ((string.Equals(p_sMode, "entity", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            if (oEntityStgng.EntityTableId == iAgentsTableId || oEntityStgng.EntityTableName == "AGENTS" || oEntityStgng.EntityTableId == iInusredTableId || oEntityStgng.EntityTableName == "POLICY_INSURED" || oPolicyXEntityStgng.PolicyUnitRowid > 0 || oEntityStgng.EntityTableId == iDriversTableId || oEntityStgng.EntityTableName == "DRIVERS")
                            {
                                continue;
                            }
                        }
                        else if ((string.Equals(p_sMode, "Driver", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            if (oEntityStgng.EntityTableId != iDriversTableId || oEntityStgng.EntityTableName != "DRIVERS")
                            {
                                continue;
                            }
                        }
                        else if ((string.Equals(p_sMode, "unitinterest", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            // Pradyumna 7/22/2014 - Modified - MITS 36800
                            if (oEntityStgng.EntityTableId == iAgentsTableId || oEntityStgng.EntityTableName == "AGENTS" || oEntityStgng.EntityTableId == iInusredTableId || oEntityStgng.EntityTableName == "POLICY_INSURED" || (!lstUnitId.Contains(oPolicyXEntityStgng.PolicyUnitRowid)) || oEntityStgng.EntityTableId == iDriversTableId || oEntityStgng.EntityTableName == "DRIVERS")
                            {
                                continue;
                            }
                        }

                        if (oPolicyXEntityStgng.PolicyUnitRowid > 0)
                        {
                            oPolXUnitStgng = oFactoryStgngDmf.GetDataModelObject("PolicyXUnitStgng", false) as PolicyXUnitStgng;
                            oPolXUnitStgng.MoveTo(oPolicyXEntityStgng.PolicyUnitRowid);
                            string sSQL = "SELECT STAT_UNIT_NUMBER FROM POINT_UNIT_DATA_STGNG WHERE UNIT_ID = " + oPolXUnitStgng.UnitId + " AND UNIT_TYPE = '" + oPolXUnitStgng.UnitType.Trim() + "'";
                            object objTemp = DbFactory.ExecuteScalar(oFactoryStgngDmf.Context.DbConnLookup.ConnectionString, sSQL);
                            if (objTemp != null)
                            {
                                sStatUnitNum = (objTemp).ToString();
                            }
                        }
                        // Pradyumna - Added if/Elseif for Unit Interest - Ends
                        p_bExists = true;
                        xEntityNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Entity", string.Empty);
                        CreateAndAppendNode("SequenceNumber", oEntityStgng.EntityId.ToString(), xEntityNode, xReturnDoc);
                        CreateAndAppendNode("TaxID", string.Empty, xEntityNode, xReturnDoc);
                        CreateAndAppendNode("FirstName", oEntityStgng.FirstName, xEntityNode, xReturnDoc);
                        CreateAndAppendNode("MiddleName", oEntityStgng.MiddleName, xEntityNode, xReturnDoc);
                        CreateAndAppendNode("LastName", oEntityStgng.LastName, xEntityNode, xReturnDoc);
                        CreateAndAppendNode("RoleCd", oPolicyXEntityStgng.ExternalRole, xEntityNode, xReturnDoc);
                        CreateAndAppendNode("StatUnitNumber", sStatUnitNum, xEntityNode, xReturnDoc);
                        CreateAndAppendNode("TableId", oEntityStgng.EntityTableId.ToString(), xEntityNode, xReturnDoc);

                        xInstanceNode.AppendChild(xEntityNode);
                    }
                    xRootNode.AppendChild(xInstanceNode);
                    xReturnDoc.AppendChild(xRootNode);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    xRootNode = null;
                    xEntityNode = null;
                    xInstanceNode = null;
                    if (oEntityStgng != null)
                    {
                        oEntityStgng.Dispose();
                    }
                    if (oFactory != null)
                    {
                        oFactory.Dispose();
                    }
                    if (oPolicyStgng != null)
                    {
                        oPolicyStgng.Dispose();
                    }
                    if (oPolXUnitStgng != null)
                    {
                        oPolXUnitStgng.Dispose();
                    }
                }

                return xReturnDoc;
            }
        */
            private struct UnitInfo
            {
                public int UnitId;
                public string UnitType;
                public string StatUnitNum;
            }

            // Pradyumna 7/22/2014 - Modified - MITS 36800 - End
            //Raman 3/24 : Reverting rmA-6683 : Policy Staging solution should be compliant to policy api 
            //it needs to use base tables and datamodel
        /*
            public string GetUnitDetailResult(int p_iPolicySystemId,int p_iStagingUnitRowId, int p_iPolicyId)
            {
                XmlDocument xReturnDoc = null;
                //PolicyStgng oPolicyStgng = null;
                DataModelFactory oFactory = null;
                DataModelFactory oFactoryStgng = null;

                //VehicleStgng oVehicleStgng = null;
                //PropertyUnitStgng oPropUnitStgng = null;
                //OtherUnitStgng oOthrUnitStgng = null;
                //SiteUnitStgng oSiteUnitStgng = null;

                XmlNode xRootNode = null;
                XmlNode xInstanceNode = null;
                XmlNode xUnitNode = null;

                string sSql = string.Empty;
                string sStatUnitNumber = string.Empty;

                try
                {
                    xReturnDoc = new XmlDocument();
                    xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Units", string.Empty);
                    xInstanceNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Instance", string.Empty);
                    if (p_iPolicySystemId<=0)
                    {
                        p_iPolicySystemId = GetPolicySystemId(p_iPolicyId);
                    }

                    oFactoryStgng = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);
                    oFactory = new DataModelFactory(m_oUserLogin, m_iClientId);
                    oPolicyStgng = oFactoryStgng.GetDataModelObject("PolicyStgng", false) as PolicyStgng;
                    oVehicleStgng = oFactoryStgng.GetDataModelObject("VehicleStgng", false) as VehicleStgng;
                    oPropUnitStgng = oFactoryStgng.GetDataModelObject("PropertyUnitStgng", false) as PropertyUnitStgng;
                    oOthrUnitStgng = oFactoryStgng.GetDataModelObject("OtherUnitStgng", false) as OtherUnitStgng;
                    oSiteUnitStgng = oFactoryStgng.GetDataModelObject("SiteUnitStgng", false) as SiteUnitStgng;

                    oPolicyStgng.MoveTo(p_iPolicyStagingId);

                    foreach (PolicyXUnitStgng oPolXUnitStgng in oPolicyStgng.PolicyXUnitListStgng)
                    {
                        if (p_iStagingUnitRowId > 0 && oPolXUnitStgng.PolicyUnitRowId != p_iStagingUnitRowId)
                        {
                            continue;
                        }

                        xUnitNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Unit", string.Empty);

                        sSql = string.Format("SELECT STAT_UNIT_NUMBER, PRODUCT, INS_LINE FROM POINT_UNIT_DATA_STGNG PUDS INNER JOIN POLICY_X_UNIT_STGNG PXUS ON PUDS.UNIT_ID = PXUS.UNIT_ID AND PUDS.UNIT_TYPE=PUDS.UNIT_TYPE WHERE PUDS.UNIT_TYPE='{0}' AND PUDS.UNIT_ID={1} AND PXUS.POLICY_ID={2}", oPolXUnitStgng.UnitType.Trim(), oPolXUnitStgng.UnitId, oPolXUnitStgng.PolicyId);
                        using (DbReader oReader = DbFactory.ExecuteReader(oPolXUnitStgng.Context.DbConnLookup.ConnectionString, sSql))
                        {
                            if (oReader != null)
                            {
                                if (oReader.Read())
                                {
                                    if (oReader["STAT_UNIT_NUMBER"] != null && oReader["STAT_UNIT_NUMBER"] != DBNull.Value)
                                    {
                                        sStatUnitNumber = oReader["STAT_UNIT_NUMBER"].ToString();
                                    }
                                    if (oReader["PRODUCT"] != null && oReader["PRODUCT"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("Product", oReader["PRODUCT"].ToString(), xUnitNode, xReturnDoc);
                                    }
                                    if (oReader["INS_LINE"] != null && oReader["INS_LINE"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("InsLine", oReader["INS_LINE"].ToString(), xUnitNode, xReturnDoc);
                                    }
                                }
                            }
                        }

                        CreateAndAppendNode("UnitNo", sStatUnitNumber, xUnitNode, xReturnDoc);
                        CreateAndAppendNode("StatUnitNo", sStatUnitNumber, xUnitNode, xReturnDoc);
                        CreateAndAppendNode("UnitType", oPolXUnitStgng.UnitType, xUnitNode, xReturnDoc);
                        CreateAndAppendNode("Status", string.Empty, xUnitNode, xReturnDoc);


                        switch (oPolXUnitStgng.UnitType.Trim())
                        {
                            case "v":
                            case "V":
                                if (oPolXUnitStgng.UnitId > 0)
                                {
                                    oVehicleStgng.MoveTo(oPolXUnitStgng.UnitId);
                                }
                                CreateAndAppendNode("Desc", string.Format("{0} {1} VIN: {2} {3}", new object[] { oVehicleStgng.VehicleYear, oVehicleStgng.VehicleModel, oVehicleStgng.Vin, oVehicleStgng.StateRowId }), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Vin", oVehicleStgng.Vin, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("City", string.Empty, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("State", oVehicleStgng.StateRowId, xUnitNode, xReturnDoc);

                                CreateAndAppendNode("VehicleType", oVehicleStgng.UnitTypeCode, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("VehicleYear", oVehicleStgng.VehicleYear.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Model", oVehicleStgng.VehicleModel, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("LeaseAmt", oVehicleStgng.LeaseAmount.ToString(), xUnitNode, xReturnDoc);
                                //added by swati for new staging columns
                                CreateAndAppendNode("Status", oVehicleStgng.Status.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Territory", oVehicleStgng.Territory.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("VehicleSym", oVehicleStgng.VehicleSym.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("PurchDate", oVehicleStgng.PurchDate.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("NewCost", oVehicleStgng.NewCost.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("PrimaryCls", oVehicleStgng.PrimaryCls.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("SecondaryCls", oVehicleStgng.SecondaryCls.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("AirbagDscnt", oVehicleStgng.AirbagDscnt.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("AntiLockBrakes", oVehicleStgng.AntiLockBrakes.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("AntiTheftInd", oVehicleStgng.AntiTheftInd.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("BodyType", oVehicleStgng.BodyType.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("VehicleUse", oVehicleStgng.VehicleUse.ToString(), xUnitNode, xReturnDoc);
                                //change end here by swati
                                break;
                            case "p":
                            case "P":
                                if (oPolXUnitStgng.UnitId > 0)
                                {
                                    oPropUnitStgng.MoveTo(oPolXUnitStgng.UnitId);
                                }
                                CreateAndAppendNode("Desc", oPropUnitStgng.Description, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Vin", oPropUnitStgng.Pin, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("City", oPropUnitStgng.City, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("State", oPropUnitStgng.StateId, xUnitNode, xReturnDoc);

                                CreateAndAppendNode("Addr", oPropUnitStgng.Addr1, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("ZipCode", oPropUnitStgng.ZipCode, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("YearBuilt", oPropUnitStgng.YearOfConstruction.ToString(), xUnitNode, xReturnDoc);
                                //added by swati
                                CreateAndAppendNode("Status", oPropUnitStgng.Status.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Ratebook", oPropUnitStgng.Ratebook.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Premium", oPropUnitStgng.Premium.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Territory", oPropUnitStgng.Territory.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("ProtectionCls", oPropUnitStgng.ProtectionCls.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Deductible", oPropUnitStgng.Deductible.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("NumOfFamilies", oPropUnitStgng.NumOfFamilies.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("ConstrtnType", oPropUnitStgng.ConstrtnType.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("InsideCity", oPropUnitStgng.InsideCity.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Occupance", oPropUnitStgng.Occupance.ToString(), xUnitNode, xReturnDoc);
                                //change end here by swati
                                break;
                            case "s":
                            case "S":
                                if (oPolXUnitStgng.UnitId > 0)
                                {
                                    oSiteUnitStgng.MoveTo(oPolXUnitStgng.UnitId);
                                }
                                CreateAndAppendNode("Desc", oSiteUnitStgng.Name, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Vin", oSiteUnitStgng.SiteNumber, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("City", oSiteUnitStgng.City, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("State", oSiteUnitStgng.StateId, xUnitNode, xReturnDoc);

                                CreateAndAppendNode("Optional", oSiteUnitStgng.Optional, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Addr", string.Format("{0} {1}", oSiteUnitStgng.Adsress1, oSiteUnitStgng.Adsress2), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("PostalCode", oSiteUnitStgng.ZipCode, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Country", oSiteUnitStgng.CountryId, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Phone", oSiteUnitStgng.PhoneNumber, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Contact", oSiteUnitStgng.Contact, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Sic", oSiteUnitStgng.SIC, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Fein", oSiteUnitStgng.FEIN, xUnitNode, xReturnDoc);
                                //added by swati for new staging columns
                                CreateAndAppendNode("Status", oSiteUnitStgng.Status.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("TaxLocation", oSiteUnitStgng.TaxLocation.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("AuditBasis", oSiteUnitStgng.AuditBasis.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("AuditType", oSiteUnitStgng.AuditType.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("InterimAuditor", oSiteUnitStgng.InterimAuditor.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("CheckAuditor", oSiteUnitStgng.CheckAuditor.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("FinalAuditor", oSiteUnitStgng.FinalAuditor.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("UnemplymntNum", oSiteUnitStgng.UnemplymntNum.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("NumOfEmp", oSiteUnitStgng.NumOfEmp.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("GvrngClsCode", oSiteUnitStgng.GvrngClsCode.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("SeqNum", oSiteUnitStgng.SeqNum.ToString(), xUnitNode, xReturnDoc);
                                CreateAndAppendNode("GvrngClsDesc", oSiteUnitStgng.GvrngClsDesc.ToString(), xUnitNode, xReturnDoc);
                                //change end here by swati
                                break;
                            default:
                                if (oPolXUnitStgng.UnitId > 0)
                                {
                                    oOthrUnitStgng.MoveTo(oPolXUnitStgng.UnitId);
                                }
                                CreateAndAppendNode("Desc", string.Empty, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("Vin", string.Empty, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("City", string.Empty, xUnitNode, xReturnDoc);
                                CreateAndAppendNode("State", string.Empty, xUnitNode, xReturnDoc);
                                break;
                        }
                        CreateAndAppendNode("SequenceNumber", oPolXUnitStgng.PolicyUnitRowId.ToString(), xUnitNode, xReturnDoc);

                        xInstanceNode.AppendChild(xUnitNode);
                    }

                    xRootNode.AppendChild(xInstanceNode);
                    xReturnDoc.AppendChild(xRootNode);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (oSiteUnitStgng!=null)
                    {
                        oSiteUnitStgng.Dispose();
                    }
                    if (oOthrUnitStgng!=null)
                    {
                        oOthrUnitStgng.Dispose();
                    }
                    if (oPropUnitStgng!=null)
                    {
                        oPropUnitStgng.Dispose();
                    }
                    if (oVehicleStgng!=null)
                    {
                        oVehicleStgng.Dispose();
                    }
                    if (oFactory!=null)
                    {
                        oFactory.Dispose();
                    }
                    if (oPolicyStgng!=null)
                    {
                        oPolicyStgng.Dispose();
                    }
                }

                return xReturnDoc.OuterXml;
            }

            public XmlDocument GetUnitXmlData(int p_iPolicyStagingId,int p_iPolicyId)
            {
                XmlDocument oXml = new XmlDocument();
                oXml.LoadXml(GetUnitDetailResult(p_iPolicyStagingId, GetPolicySystemId(p_iPolicyId),- 1, p_iPolicyId));
                return oXml;
            }

            public bool SaveUnit(int p_iPolicyId, int p_iStagingUnitRowId, ref int  p_iDownloadedUnitRowId)
            {
                DataModelFactory oFactory = null;
                DataModelFactory oFactoryStgngDmf = null;
                PolicyXUnit oPolicyXUnit = null;
                PolicyXUnitStgng oPolicyXUnitStgng = null;

                VehicleStgng oVehicleStgng = null;
                PropertyUnitStgng oPropUnitStgng = null;
                OtherUnitStgng oOthrUnitStgng = null;
                SiteUnitStgng oSiteUnitStgng = null;

                Vehicle oVehicle = null;
                PropertyUnit oPropUnit = null;
                OtherUnit oOthrUnit = null;
                SiteUnit oSiteUnit = null;

                string sSql = string.Empty;
                string sStatUnitNumber = string.Empty;
                int iPolicyXUnitRowId = 0;
                int iUnitId = 0;
                string sErrorMsg = string.Empty;
                bool bError = false;
                int iPolicyStagingId = 0;
                try
                {
                    bError = ValidateCoverages(p_iPolicyId, p_iStagingUnitRowId, ref sErrorMsg);
                    if (bError)
                    {
                        throw new RMAppException(sErrorMsg);
                    }

                    oFactory = new DataModelFactory(m_oUserLogin, m_iClientId); ;
                    iPolicyStagingId = GetPolicySystemId(p_iPolicyId);
                    oFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(iPolicyStagingId), m_iClientId);
                    oPolicyXUnit = oFactory.GetDataModelObject("PolicyXUnit", false) as PolicyXUnit;
                    oPolicyXUnitStgng = oFactoryStgngDmf.GetDataModelObject("PolicyXUnitStgng", false) as PolicyXUnitStgng;

                    sSql = string.Format("SELECT STAT_UNIT_NUMBER FROM POINT_UNIT_DATA_STGNG PUD INNER JOIN POLICY_X_UNIT_STGNG PXU ON PXU.UNIT_ID=PUD.UNIT_ID AND PXU.UNIT_TYPE=PUD.UNIT_TYPE WHERE PXU.POLICY_UNIT_ROW_ID={0}", p_iStagingUnitRowId);
                    sStatUnitNumber = oFactoryStgngDmf.Context.DbConnLookup.ExecuteString(sSql);

                    oPolicyXUnitStgng.MoveTo(p_iStagingUnitRowId);
                    sSql = string.Format("SELECT POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT PXU INNER JOIN POINT_UNIT_DATA PUD ON PXU.UNIT_ID=PUD.UNIT_ID AND PXU.UNIT_TYPE=PUD.UNIT_TYPE WHERE PXU.POLICY_ID={0} AND PUD.UNIT_TYPE='{1}' AND PUD.STAT_UNIT_NUMBER='{2}'", new object[] { p_iPolicyId, oPolicyXUnitStgng.UnitType.Trim(), sStatUnitNumber });
                    iPolicyXUnitRowId = DbFactory.ExecuteAsType<int>(oFactory.Context.DbConnLookup.ConnectionString, sSql);

                    if (iPolicyXUnitRowId > 0)
                    {
                        oPolicyXUnit.MoveTo(iPolicyXUnitRowId);
                    }
                    else // Added by Pradyumna 03142014 MITS# 35296 - Start
                    {
                        int iExists = 0;
                        bool bSuccess = false;
                        string sMultiUnitDwnload = string.Empty;
                        System.Collections.Specialized.NameValueCollection oCollection = RMConfigurationManager.GetNameValueSectionSettings("PolicyInterface",m_sConnectString, m_iClientId);
                        if (oCollection != null && oCollection["MultiUnitDownload"] != null)
                            sMultiUnitDwnload = oCollection["MultiUnitDownload"];
                        if (!string.IsNullOrEmpty(sMultiUnitDwnload) && string.Compare(sMultiUnitDwnload, "false", true) == 0)
                        {
                            sSql = "SELECT COUNT(*) FROM POLICY_X_UNIT PXU WHERE PXU.POLICY_ID = " + p_iPolicyId;
                            iExists = Conversion.CastToType<int>((DbFactory.ExecuteScalar(oFactory.Context.DbConnLookup.ConnectionString, sSql)).ToString(), out bSuccess);

                            if (iExists > 0)
                            {
                                //sErrorMsg = "A Unit already exists for this policy. Only one unit per Policy can be downloaded.";
                                //throw new RMAppException(sErrorMsg);
                                return false;
                            }
                        }
                    }
                    // Added by Pradyumna 03142014 MITS# 35296 - End
                    switch (oPolicyXUnitStgng.UnitType.Trim())
                    {
                        case "v":
                        case "V":
                            oVehicleStgng = oFactoryStgngDmf.GetDataModelObject("VehicleStgng", false) as VehicleStgng;
                            oVehicle = oFactory.GetDataModelObject("Vehicle", false) as Vehicle;
                            if (oPolicyXUnitStgng.UnitId > 0)
                            {
                                oVehicleStgng.MoveTo(oPolicyXUnitStgng.UnitId);
                            }
                            if (iPolicyXUnitRowId > 0)
                            {
                                oVehicle.MoveTo(oPolicyXUnit.UnitId);
                                oVehicle.DataChanged = true; // Pradyumna 05/20/2014: To Update data in case Policy is re-downloaded
                            }
                            oVehicle.LeaseAmount = oVehicleStgng.LeaseAmount;
                            oVehicle.LeaseExpireDate = oVehicleStgng.LeaseExpireDate;
                            oVehicle.LeaseFlag = oVehicleStgng.LeaseFlag;
                            oVehicle.LeaseNumber = oVehicleStgng.LeaseNumber;
                            oVehicle.LeaseTerm = oVehicleStgng.LeaseTerm;
                            oVehicle.LeasingCoEid = oVehicleStgng.LeasingCoEid;
                            oVehicle.LicenseNumber = oVehicleStgng.LicenseNumber;
                            oVehicle.LicenseRnwlDate = oVehicleStgng.LicenseRnwlDate;
                            //oVehicle.StateRowId = GetRMXCodeIdFromPSMappedCode(oVehicleStgng.StateRowId, "STATES", "StateId on SaveUnit", iPolicyStagingId.ToString());
                            oVehicle.StateRowId = oVehicleStgng.Context.LocalCache.GetStateRowID(oVehicleStgng.StateRowId);
                            //oVehicle.UnitTypeCode = GetRMXCodeIdFromPSMappedCode(oVehicleStgng.UnitTypeCode, "UNIT_TYPE_CODE", "StateId on SaveUnit", iPolicyStagingId.ToString());
                            oVehicle.UnitTypeCode = oVehicleStgng.Context.LocalCache.GetCodeId(oVehicleStgng.UnitTypeCode, "UNIT_TYPE_CODE");
                            oVehicle.VehicleMake = oVehicleStgng.VehicleMake;
                            oVehicle.VehicleModel = oVehicleStgng.VehicleModel;
                            oVehicle.VehicleYear = oVehicleStgng.VehicleYear;
                            oVehicle.Vin = oVehicleStgng.Vin;
                            oVehicle.GrossWeight = oVehicleStgng.GrossWeight;
                            oVehicle.HomeDeptEid = oVehicleStgng.HomeDeptEid;
                            oVehicle.Save();

                            iUnitId = oVehicle.UnitId;
                            break;
                        case "p":
                        case "P":
                            oPropUnitStgng = oFactoryStgngDmf.GetDataModelObject("PropertyUnitStgng", false) as PropertyUnitStgng;
                            oPropUnit = oFactory.GetDataModelObject("PropertyUnit", false) as PropertyUnit;
                            if (oPolicyXUnitStgng.UnitId > 0)
                            {
                                oPropUnitStgng.MoveTo(oPolicyXUnitStgng.UnitId);
                            }
                            if (iPolicyXUnitRowId > 0)
                            {
                                oPropUnit.MoveTo(oPolicyXUnit.UnitId);
                                oPropUnit.DataChanged = true; // Pradyumna 05/20/2014: To Update data in case Policy is re-downloaded
                            }
                            oPropUnit.Addr1 = oPropUnitStgng.Addr1;
                            oPropUnit.Addr2 = oPropUnitStgng.Addr2;
                            oPropUnit.AppraisedDate = oPropUnitStgng.AppraisedDate;
                            oPropUnit.AppraisedValue = oPropUnitStgng.AppraisedValue;
                            oPropUnit.City = oPropUnitStgng.City;
                            //oPropUnit.CountryCode = oPropUnitStgng.CountryCode;
                            oPropUnit.Description = oPropUnitStgng.Description;
                            oPropUnit.LandValue = oPropUnitStgng.LandValue;
                            oPropUnit.Pin = oPropUnitStgng.Pin;
                            oPropUnit.ReplacementValue = oPropUnitStgng.ReplacementValue;
                            //oPropUnit.StateId = GetRMXCodeIdFromPSMappedCode(oPropUnitStgng.StateId, "STATES", "StateId on SaveUnit", iPolicyStagingId.ToString());
                            oPropUnit.StateId = oFactory.Context.LocalCache.GetStateRowID(oPropUnitStgng.StateId);
                            oPropUnit.YearOfConstruction = oPropUnitStgng.YearOfConstruction;
                            oPropUnit.ZipCode = oPropUnitStgng.ZipCode;
                            oPropUnit.Save();

                            iUnitId = oPropUnit.PropertyId;
                            break;
                        case "s":
                        case "S":
                            oSiteUnitStgng = oFactoryStgngDmf.GetDataModelObject("SiteUnitStgng", false) as SiteUnitStgng;
                            oSiteUnit = oFactory.GetDataModelObject("SiteUnit", false) as SiteUnit;
                            if (oPolicyXUnitStgng.UnitId > 0)
                            {
                                oSiteUnitStgng.MoveTo(oPolicyXUnitStgng.UnitId);
                            }
                            if (iPolicyXUnitRowId > 0)
                            {
                                oSiteUnit.MoveTo(oPolicyXUnit.UnitId);
                                oSiteUnit.DataChanged = true; // Pradyumna 05/20/2014: To Update data in case Policy is re-downloaded
                            }
                            oSiteUnit.Adsress1 = oSiteUnitStgng.Adsress1;
                            oSiteUnit.Adsress2 = oSiteUnitStgng.Adsress2;
                            oSiteUnit.City = oSiteUnitStgng.City;
                            oSiteUnit.Contact = oSiteUnitStgng.Contact;
                            //oSiteUnit.CountryId = oSiteUnitStgng.CountryId;
                            oSiteUnit.FEIN = oSiteUnitStgng.FEIN;
                            oSiteUnit.Name = oSiteUnitStgng.Name;
                            oSiteUnit.Optional = oSiteUnitStgng.Optional;
                            oSiteUnit.PhoneNumber = oSiteUnitStgng.PhoneNumber;
                            oSiteUnit.SIC = oSiteUnitStgng.SIC;
                            oSiteUnit.SiteNumber = oSiteUnitStgng.SiteNumber;
                            //oSiteUnit.StateId = GetRMXCodeIdFromPSMappedCode(oSiteUnitStgng.StateId, "STATES", "StateId on SaveUnit", iPolicyStagingId.ToString());
                            oSiteUnit.StateId = oFactory.Context.LocalCache.GetStateRowID(oSiteUnitStgng.StateId);
                            oSiteUnit.ZipCode = oSiteUnitStgng.ZipCode;
                            oSiteUnit.Save();

                            iUnitId = oSiteUnit.SiteId;
                            break;
                        default:
                            break;
                    }

                    if (iUnitId > 0)
                    {
                        oPolicyXUnit.DataChanged = true; // Pradyumna 05/20/2014: To Update data in case Policy is re-downloaded
                        oPolicyXUnit.UnitId = iUnitId;
                        oPolicyXUnit.UnitType = oPolicyXUnitStgng.UnitType.Trim();
                        oPolicyXUnit.PolicyId = p_iPolicyId;
                        oPolicyXUnit.Save();

                        //added by swati
                        p_iDownloadedUnitRowId = oPolicyXUnit.PolicyUnitRowId;
                    }

                    if (iPolicyXUnitRowId <= 0)
                    {
                        sSql = string.Format("INSERT INTO POINT_UNIT_DATA (ROW_ID, UNIT_ID, UNIT_TYPE, STAT_UNIT_NUMBER) VALUES ({0},{1},'{2}','{3}')",
                            new object[] { oFactory.Context.GetNextUID("POINT_UNIT_DATA"), oPolicyXUnit.UnitId, oPolicyXUnitStgng.UnitType.Trim(), sStatUnitNumber });
                        oFactory.Context.DbConnLookup.ExecuteNonQuery(sSql);
                    }
                    // Pradyumna 05/19/2014 - To Update existing Coverages for a unit when Policy is Downloaded again
                    SaveCoverages(p_iPolicyId, p_iStagingUnitRowId, oPolicyXUnit.PolicyUnitRowId);
                    return true;
                }
                catch (RMAppException)
                {
                    throw;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (oSiteUnit!=null)
                    {
                        oSiteUnit.Dispose();
                    }
                    if (oOthrUnit!=null)
                    {
                        oOthrUnit.Dispose();
                    }
                    if (oPropUnit!=null)
                    {
                        oPropUnit.Dispose();
                    }
                    if (oVehicle!=null)
                    {
                        oVehicle.Dispose();
                    }
                    if (oPolicyXUnit != null)
                    {
                        oPolicyXUnit.Dispose();
                    }
                    if (oSiteUnitStgng != null)
                    {
                        oSiteUnitStgng.Dispose();
                    }
                    if (oOthrUnitStgng != null)
                    {
                        oOthrUnitStgng.Dispose();
                    }
                    if (oPropUnitStgng != null)
                    {
                        oPropUnitStgng.Dispose();
                    }
                    if (oVehicleStgng != null)
                    {
                        oVehicleStgng.Dispose();
                    }
                    if (oPolicyXUnitStgng!=null)
                    {
                        oPolicyXUnitStgng.Dispose();
                    }
                    if (oFactory!=null)
                    {
                        oFactory.Dispose();
                    }
                    if (oFactoryStgngDmf!= null)
                    {
                        oFactoryStgngDmf.Dispose();
                    }
                }

            }

            private bool ValidateCoverages(int p_iPolicyId, int p_iStagingUnitRowId, ref string p_sErrorMsg)
            {
                string sSql = string.Empty;

                PolicyXCvgTypeStgng oPolCvgTypeStgng = null;
                DataModelFactory oFactory = null;
                DataModelFactory oFactoryStgngDmf = null;
                HashSet<int> hSetPolCvgRowIds = null;
                int iPolCvgRowid = 0;
                int iCodeId = 0;
                bool bSuccess = false;
                bool bError = false;
                int iPolicySystemId = 0;
                try
                {
                    oFactory = new DataModelFactory(m_oUserLogin, m_iClientId); 
                    iPolicySystemId = GetPolicySystemId(p_iPolicyId);
                    oFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(iPolicySystemId), m_iClientId);
                    oPolCvgTypeStgng = oFactoryStgngDmf.GetDataModelObject("PolicyXCvgTypeStgng", false) as PolicyXCvgTypeStgng;
                    hSetPolCvgRowIds = new HashSet<int>();

                    sSql = "SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE_STGNG WHERE POLICY_UNIT_ROW_ID=" + p_iStagingUnitRowId;
                    using (DbReader oReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(sSql))
                    {
                        if (oReader != null)
                        {
                            while (oReader.Read())
                            {
                                if (oReader["POLCVG_ROW_ID"] != null && oReader["POLCVG_ROW_ID"] != DBNull.Value)
                                {
                                    iPolCvgRowid = Conversion.CastToType<int>(oReader["POLCVG_ROW_ID"].ToString(), out bSuccess);
                                    hSetPolCvgRowIds.Add(iPolCvgRowid);
                                }
                            }
                        }
                    }

                    foreach (int iCvgRowid in hSetPolCvgRowIds)
                    {
                        if (iCvgRowid > 0)
                        {
                            oPolCvgTypeStgng.MoveTo(iCvgRowid);
                        }
                        iCodeId = GetRMXCodeIdFromPSMappedCode(oPolCvgTypeStgng.CoverageTypeCode.Trim(), "COVERAGE_TYPE", "CoverageType on ValidateCoverages", iPolicySystemId.ToString());
                        //iCodeId = oFactoryStgngDmf.Context.LocalCache.GetCodeId(oPolCvgTypeStgng.CoverageTypeCode.Trim(), "COVERAGE_TYPE");

                        if (iCodeId <= 0)
                        {
                            bError = true;
                            p_sErrorMsg = "Missing Code Mapping: " + oPolCvgTypeStgng.CoverageTypeCode.Trim();
                        }
                    }
                }
                catch
                {
                    bError = true;
                    p_sErrorMsg = "Error In ValidateCoverages";
                }
                finally
                {
                    if (oPolCvgTypeStgng != null)
                    {
                        oPolCvgTypeStgng.Dispose();
                    }
                    if (oFactory!=null)
                    {
                        oFactory.Dispose();
                    }
                    if (oFactoryStgngDmf!= null)
                    {
                        oFactoryStgngDmf.Dispose();
                    }
                }
                return bError;
            } 

            public void SaveCoverages(int p_iPolicyId, int p_iStagingUnitRowId, int p_iPolicyUnitRowId)
            {
                string sSql = string.Empty;
                int iCvgTypeCd = 0; // Added by Pradyumna 05/19/2014
                int iPolCvgRwID = 0; // Added by Pradyumna 05/19/2014
                PolicyXCvgTypeStgng oPolCvgTypeStgng = null;
                PolicyXCvgType oPolCvgType = null;
                DataModelFactory oFactory = null;
                DataModelFactory oFactoryStgngDmf = null;
                // Pradyumna 07/18/2014 - MITS 36799 - Start
                Dictionary<int, List<string>> dicCvgTypeValues = new Dictionary<int, List<string>>();
                // Pradyumna 07/18/2014 - MITS 36799 - End
                int iPolCvgRowid = 0;
                bool bSuccess = false;
                int iStagingPolicySystemId = 0;
                try
                {
                    iStagingPolicySystemId = GetPolicySystemId(p_iPolicyId);
                    oFactory = new DataModelFactory(m_oUserLogin, m_iClientId); ;
                    oFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(iStagingPolicySystemId), m_iClientId);
                    oPolCvgTypeStgng = oFactoryStgngDmf.GetDataModelObject("PolicyXCvgTypeStgng", false) as PolicyXCvgTypeStgng;

                    sSql = "SELECT POLCVG_ROW_ID, CLASS, PROD_LINE FROM POLICY_X_CVG_TYPE_STGNG WHERE POLICY_UNIT_ROW_ID=" + p_iStagingUnitRowId;
                    using (DbReader oReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(sSql))
                    {
                        if (oReader != null)
                        {
                            while (oReader.Read())
                            {
                                List<string> lstCvgtypeVal = new List<string>(); // Pradyumna 07/18/2014 - MITS 36799 
                                lstCvgtypeVal.Add(string.Empty);
                                lstCvgtypeVal.Add(string.Empty);

                                if (oReader["POLCVG_ROW_ID"] != null && oReader["POLCVG_ROW_ID"] != DBNull.Value)
                                {
                                    iPolCvgRowid = Conversion.CastToType<int>(oReader["POLCVG_ROW_ID"].ToString(), out bSuccess);
                                }
                                // Pradyumna 07/18/2014 - MITS 36799 - Start
                                if (oReader["CLASS"] != null && oReader["CLASS"] != DBNull.Value)
                                {
                                    string sClassCode = oReader["CLASS"].ToString();
                                    lstCvgtypeVal[0] = sClassCode.Trim();
                                }
                                if (oReader["PROD_LINE"] != null && oReader["PROD_LINE"] != DBNull.Value)
                                {
                                    string sProdLine = oReader["PROD_LINE"].ToString();
                                    lstCvgtypeVal[1] = sProdLine.Trim();
                                }
                                dicCvgTypeValues.Add(iPolCvgRowid, lstCvgtypeVal);
                                // Pradyumna 07/18/2014 - MITS 36799 - Ends
                            }
                        }
                    }
                    // Pradyumna 07/18/2014 - MITS 36799 - Start
                    foreach (var pair in dicCvgTypeValues)
                    {
                        if (pair.Key > 0)
                        {
                            oPolCvgTypeStgng.MoveTo(pair.Key);
                        }
                        oPolCvgType = oFactory.GetDataModelObject("PolicyXCvgType", false) as PolicyXCvgType;
                        // Pradyumna 05/19/2014 - To Update existing Coverages for a unit when Policy is Downloaded again - Start
                        iCvgTypeCd = GetRMXCodeIdFromPSMappedCode(oPolCvgTypeStgng.CoverageTypeCode.Trim(), "COVERAGE_TYPE", "CoverageType on SaveCoverages", iStagingPolicySystemId.ToString());
                        //iCvgTypeCd = oFactoryStgngDmf.Context.LocalCache.GetCodeId(oPolCvgTypeStgng.CoverageTypeCode.Trim(), "COVERAGE_TYPE");
                        sSql = "SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID = " + p_iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE = "
                            + iCvgTypeCd; 
                        if(pair.Value.Count > 0 && !String.IsNullOrEmpty(pair.Value[0]))
                            sSql = sSql + " AND COVERAGE_CLASS_CODE = '" + pair.Value[0] + "'";
                        if (pair.Value.Count > 1 && !String.IsNullOrEmpty(pair.Value[1]))
                            sSql = sSql + " AND PRODLINE = '" + pair.Value[1] + "'";
                        // Pradyumna 07/18/2014 - MITS 36799 - Ends
                        iPolCvgRwID = oFactory.Context.DbConnLookup.ExecuteInt(sSql);

                        if (iPolCvgRwID > 0)
                        {
                            oPolCvgType.MoveTo(iPolCvgRwID);
                            oPolCvgType.DataChanged = true; // Pradyumna 05/20/2014: To Update data in case Policy is re-downloaded
                        }
                        // Pradyumna 05/19/2014 - To Update existing Coverages for a unit when Policy is Downloaded again - End
                        oPolCvgType.BrokerName = oPolCvgTypeStgng.BrokerName.Trim();
                        oPolCvgType.CancelNoticeDays = oPolCvgTypeStgng.CancelNoticeDays;
                        oPolCvgType.ChangeDate = oPolCvgTypeStgng.ChangeDate.Trim();
                        oPolCvgType.ClaimLimit = oPolCvgTypeStgng.ClaimLimit;
                        oPolCvgType.CoverageText = oPolCvgTypeStgng.CoverageText.Trim();

                        oPolCvgType.CoverageTypeCode = iCvgTypeCd;
                        oPolCvgType.CvgSequenceNo = oPolCvgTypeStgng.CvgSequenceNo.Trim();
                        oPolCvgType.EffectiveDate = oPolCvgTypeStgng.EffectiveDate.Trim();
                        oPolCvgType.Exceptions = oPolCvgTypeStgng.Exceptions.Trim();
                        oPolCvgType.ExpirationDate = oPolCvgTypeStgng.ExpirationDate.Trim();
                        oPolCvgType.Exposure = oPolCvgTypeStgng.Exposure;
                        oPolCvgType.FullTermPremium = oPolCvgTypeStgng.FullTermPremium;
                        oPolCvgType.Limit = oPolCvgTypeStgng.Limit;
                        oPolCvgType.NextPolicyId = oPolCvgTypeStgng.NextPolicyId;
                        oPolCvgType.NotificationUid = oPolCvgTypeStgng.NotificationUid;
                        oPolCvgType.OccurrenceLimit = oPolCvgTypeStgng.OccurrenceLimit;
                        oPolCvgType.OrginialPremium = oPolCvgTypeStgng.OrginialPremium;
                        oPolCvgType.PerPersonLimit = oPolCvgTypeStgng.PerPersonLimit;
                        //oPolCvgType.PolicyId = p_iPolicyId;
                        oPolCvgType.PolicyLimit = oPolCvgTypeStgng.PolicyLimit;
                        oPolCvgType.PolicyUnitRowId = p_iPolicyUnitRowId;
                        oPolCvgType.Remarks = oPolCvgTypeStgng.Remarks.Trim();
                        //oPolCvgType.SectionNumCode = oPolCvgTypeStgng.SectionNumCode;
                        oPolCvgType.SelfInsureDeduct = oPolCvgTypeStgng.SelfInsureDeduct;
                        oPolCvgType.TotalPayments = oPolCvgTypeStgng.TotalPayments;
                        oPolCvgType.TotalWrittenPremium = oPolCvgTypeStgng.TotalWrittenPremium;
                        oPolCvgType.TransSequenceNo = oPolCvgTypeStgng.TransSequenceNo.Trim();
                        oPolCvgType.WrittenPremium = oPolCvgTypeStgng.WrittenPremium;

                        // Pradyumna : Added Missing mapped columns - Start
                        oPolCvgType.AnnualStmt = Convert.ToString(oPolCvgTypeStgng.AnnualStmnt);
                        oPolCvgType.CvgClassCode = oPolCvgTypeStgng.Class.Trim();
                        oPolCvgType.LimitCovA = oPolCvgTypeStgng.LimCovA;
                        oPolCvgType.LimitCovB = oPolCvgTypeStgng.LimCovB;
                        oPolCvgType.LimitCovC = oPolCvgTypeStgng.LimCovC;
                        oPolCvgType.LimitCovD = oPolCvgTypeStgng.LimCovD;
                        oPolCvgType.LimitCovE = oPolCvgTypeStgng.LimCovE;
                        oPolCvgType.LimitCovF = oPolCvgTypeStgng.LimCovF;
                        oPolCvgType.ProductLine = oPolCvgTypeStgng.ProductLine.Trim();
                        oPolCvgType.SubLine = oPolCvgTypeStgng.SubLine.Trim();
                        oPolCvgType.WcDedAggr = oPolCvgTypeStgng.WCDedAggr;
                        oPolCvgType.WcDedAmt = oPolCvgTypeStgng.WCDedAmnt;
                        // Pradyumna : Added Missing mapped columns - Ends

                        oPolCvgType.FiringScriptFlag = 2;
                        oPolCvgType.Save();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (oPolCvgTypeStgng!=null)
                    {
                        oPolCvgTypeStgng.Dispose();
                    }
                    if (oPolCvgType != null)
                    {
                        oPolCvgType.Dispose();
                    }
                    if (oFactory!=null)
                    {
                        oFactory.Dispose();
                    }
                    if (oFactoryStgngDmf!= null)
                    {
                        oFactory.Dispose();
                    }
                }
            }
        */
            private void CreateAndAppendNode(string p_sName, string p_sValue, XmlNode p_xBaseNode, XmlDocument p_xDoc)
            {
                XmlNode xNode = p_xDoc.CreateNode(XmlNodeType.Element, p_sName, string.Empty);
                xNode.InnerText = p_sValue;
                p_xBaseNode.AppendChild(xNode);
            }
        /*
            /// <summary>
            /// Retreives Policy search result from Staging Tables based on search criteria and returns resultset in XML format. MITS 33414
            /// </summary>
            /// <param name="oSearchRequest"></param>
            /// <returns>XmlDocument</returns>
            public XmlDocument GetStgPolicySearchResult(PolicySearch oSearchRequest)
            {
                XmlDocument objXmlDocument = null;
                XmlElement objRowXmlElement = null;
                XmlElement objXmlElement = null;
                XmlElement objRootElement = null;
                StringBuilder sbSQL;
                string sWhere = string.Empty;
                string sOperator = string.Empty; // Added by Pradyumna 03072014
                List<int> lstPolicyIds = new List<int>();
                DataModelFactory objDMF = null;
                DbReader objDbReader = null;
                SysSettings oSysSetting = null;
                //PolicyStgng objPolicyStg = null;
                //EntityStgng ObjPolInsuredEntStg = null;
                //bool bSucc = false;

                try
                {
                    objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId );
                    sbSQL = new StringBuilder();
                    //sbSQL.Append("SELECT DISTINCT PS.POLICY_ID FROM POLICY_STGNG PS LEFT OUTER JOIN POLICY_X_INSURED_STGNG PXINS ON PXINS.POLICY_ID = PS.POLICY_ID INNER JOIN ENTITY_STGNG INS ON INS.ENTITY_ID = PXINS.INSURED_EID LEFT OUTER JOIN POLICY_X_ENTITY_STGNG PXE ON PXE.POLICY_ID = PS.POLICY_ID LEFT OUTER JOIN ENTITY_STGNG AGNT ON AGNT.ENTITY_ID = PXE.ENTITY_ID");

                    // Pshekhawat 8/14/2014 - for Policy Search Performance Improvement - Start
                    oSysSetting = new SysSettings(m_sConnectString,m_iClientId );
                    if (oSysSetting.PolicySearchCount > 0)
                    {
                        sbSQL.Append("SELECT TOP " + oSysSetting.PolicySearchCount);
                    }
                    else
                    {
                        sbSQL.Append("SELECT");
                    }
                    sbSQL.Append(" PS.POLICY_ID ,PS.POLICY_STATUS_CODE, PS.POLICY_NUMBER, PS.POLICY_SYMBOL, PS.MODULE, INS.FIRST_NAME, INS.LAST_NAME,");
                    sbSQL.Append(" INS.MIDDLE_NAME, INS.CITY, INS.STATE_ID, PS.EFFECTIVE_DATE, PS.EXPIRATION_DATE, PS.LOCATION_COMPANY, PS.MASTER_COMPANY,");
                    sbSQL.Append(" PS.POLICY_LOB_CODE, AGNT.REFERENCE_NUMBER FROM POLICY_STGNG PS LEFT OUTER JOIN POLICY_X_INSURED_STGNG PXINS ON PXINS.POLICY_ID = PS.POLICY_ID");
                    sbSQL.Append(" INNER JOIN ENTITY_STGNG INS ON INS.ENTITY_ID = PXINS.INSURED_EID LEFT OUTER JOIN POLICY_X_ENTITY_STGNG PXE ON PXE.POLICY_ID = PS.POLICY_ID");
                    sbSQL.Append(" LEFT OUTER JOIN ENTITY_STGNG AGNT ON AGNT.ENTITY_ID = PXE.ENTITY_ID INNER JOIN GLOSSARY G ON G.TABLE_ID = AGNT.ENTITY_TABLE_ID ");

                    int iAgntTableId = 0;
                    //TODO: Commented temporarily
                    iAgntTableId = objDMF.Context.LocalCache.GetTableId("AGENTS");
                    if (iAgntTableId > 0)
                    {
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";
                        //Anu Tennyson Starts Pre POST WWIG Starts
                        sWhere = "AGNT.ENTITY_TABLE_ID = " + iAgntTableId + " OR AGNT.ENTITY_TABLE_NAME = 'AGENTS'";
                        //Anu Tennyson Ends
                    }
                    // Pshekhawat 8/14/2014 - for Policy Search Performance Improvement - Ends

                    if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.PolicySymbol))
                    {
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";

                        if (oSearchRequest.objSearchFilters.PolicySymbol.IndexOf('*', 0) > -1)
                            sOperator = "LIKE";
                        else
                            sOperator = "=";

                        sWhere = sWhere + " PS.POLICY_SYMBOL " + sOperator + " '" + oSearchRequest.objSearchFilters.PolicySymbol.Replace("'", "''").Replace('*','%') + "' ";
                    }

                    if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.Module))
                    {
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";

                        if (oSearchRequest.objSearchFilters.Module.IndexOf('*', 0) > -1)
                            sOperator = "LIKE";
                        else
                            sOperator = "=";

                        sWhere = sWhere + " PS.MODULE " + sOperator + " '" + oSearchRequest.objSearchFilters.Module.Replace("'", "''").Replace('*', '%') + "' ";
                    }

                    if (oSearchRequest.objSearchFilters.LOB > 0)
                    {
                        //int iPolicyLOBCode = GetRMXCodeIdFromPSMappedCode(oSearchRequest.objSearchFilters.LOB.ToString(), "POLICY_CLAIM_LOB", "PolicyLob on GetStgPolicySearchResult", oSearchRequest.objSearchFilters.PolicySystemId.ToString());
                        string sPolicyLob = objDMF.Context.LocalCache.GetShortCode(oSearchRequest.objSearchFilters.LOB);
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + " PS.POLICY_LOB_CODE = '" + sPolicyLob + "' ";
                    }

                    if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.PolicyNumber))
                    {
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";

                        if (oSearchRequest.objSearchFilters.PolicyNumber.IndexOf('*', 0) > -1)
                            sOperator = "LIKE";
                        else
                            sOperator = "=";

                        sWhere = sWhere + " PS.POLICY_NUMBER " + sOperator + " '" + oSearchRequest.objSearchFilters.PolicyNumber.Replace("'", "''").Replace('*', '%') + "' ";
                    }

                    if (oSearchRequest.objSearchFilters.State > 0)
                    {
                        int iStateId = GetRMXCodeIdFromPSMappedCode(oSearchRequest.objSearchFilters.State.ToString(), "STATES", "State on GetStgPolicySearchResult", oSearchRequest.objSearchFilters.PolicySystemId.ToString());
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + " INS.STATE_ID = '" + objDMF.Context.LocalCache.GetStateCode(iStateId) + "' ";
                    }

                    if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.LossDate))
                    {
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + " PS.EFFECTIVE_DATE <= " + Conversion.GetDate(oSearchRequest.objSearchFilters.LossDate.Replace("'", "''")) + " AND PS.EXPIRATION_DATE >= " + Conversion.GetDate(oSearchRequest.objSearchFilters.LossDate.Replace("'", "''"));
                    }

                    // Pradyumna MITS 34932 02/27/2014
                    if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.Agent))
                    {
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";

                        if (oSearchRequest.objSearchFilters.Agent.IndexOf('*', 0) > -1)
                            sOperator = "LIKE";
                        else
                            sOperator = "=";

                        sWhere = sWhere + " AGNT.REFERENCE_NUMBER " + sOperator + " '" + oSearchRequest.objSearchFilters.Agent.Replace('*', '%') + "' ";
                    }

                    if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.Zip))
                    {
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";

                        if (oSearchRequest.objSearchFilters.Zip.IndexOf('*', 0) > -1)
                            sOperator = "LIKE";
                        else
                            sOperator = "=";

                        sWhere = sWhere + " INS.ZIP_CODE " + sOperator + " '" + oSearchRequest.objSearchFilters.Zip.Replace("'", "''").Replace('*', '%') + "' ";
                    }

                    if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.City))
                    {
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";

                        if (oSearchRequest.objSearchFilters.City.IndexOf('*', 0) > -1)
                            sOperator = "LIKE";
                        else
                            sOperator = "=";

                        sWhere = sWhere + " INS.CITY " + sOperator + " '" + oSearchRequest.objSearchFilters.City.Replace("'", "''").Replace('*', '%') + "' ";
                    }

                    //if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.Name))
                    //{
                    //    if (sWhere != string.Empty)
                    //        sWhere = sWhere + " AND ";
                    //    sWhere = sWhere + " INS.LAST_NAME like '" + oSearchRequest.objSearchFilters.Name.Replace("'", "''") + "' ";
                    //}

                    // Pradyumna MITS 34932 02/27/2014
                    if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.InsuredSSN))
                    {
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";

                        if (oSearchRequest.objSearchFilters.InsuredSSN.IndexOf('*', 0) > -1)
                            sOperator = "LIKE";
                        else
                            sOperator = "=";

                        sWhere = sWhere + " INS.TAX_ID " + sOperator + " '" + oSearchRequest.objSearchFilters.InsuredSSN.Replace('*', '%') + "' ";
                    }

                    if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.InsuredLastName))
                    {
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";

                        if (oSearchRequest.objSearchFilters.InsuredLastName.IndexOf('*', 0) > -1)
                            sOperator = "LIKE";
                        else
                            sOperator = "=";

                        sWhere = sWhere + " INS.LAST_NAME " + sOperator + " '" + oSearchRequest.objSearchFilters.InsuredLastName.Replace("'", "''").Replace('*', '%') + "' ";
                    }

                    if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.InsuredFirstName))
                    {
                        if (sWhere != string.Empty)
                            sWhere = sWhere + " AND ";

                        if (oSearchRequest.objSearchFilters.InsuredFirstName.IndexOf('*', 0) > -1)
                            sOperator = "LIKE";
                        else
                            sOperator = "=";

                        sWhere = sWhere + " INS.FIRST_NAME " + sOperator + " '" + oSearchRequest.objSearchFilters.InsuredFirstName.Replace("'", "''").Replace('*', '%') + "' ";
                    }

                    // PSHEKHAWAT 7/25/2014 : JIRA RMA-745 - Start
                    if(!String.IsNullOrEmpty(sWhere))
                        sbSQL.Append(" WHERE " + sWhere);
                    // PSHEKHAWAT 7/25/2014 : JIRA RMA-745 - End

                    // Pshekhawat 8/14/2014 - for Policy Search Performance Improvement - Start
                    using (objDbReader = DbFactory.GetDbReader(GetStagingConnectionString(oSearchRequest.objSearchFilters.PolicySystemId), sbSQL.ToString()))
                    {
                        objXmlDocument = new XmlDocument();
                        objRootElement = objXmlDocument.CreateElement("SearchResults");
                        objXmlDocument.AppendChild(objRootElement);
                        while (objDbReader.Read())
                        {
                            objRowXmlElement = objXmlDocument.CreateElement("Row");

                            objXmlElement = objXmlDocument.CreateElement("status");
                            if (objDbReader["POLICY_STATUS_CODE"] != null && objDbReader["POLICY_STATUS_CODE"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["POLICY_STATUS_CODE"].ToString().Trim() + " " + objDMF.Context.LocalCache.GetCodeDesc(GetRMXCodeIdFromPSMappedCode(objDbReader["POLICY_STATUS_CODE"].ToString().Trim(), "POLICY_STATUS", "Policy Status on GetStgPolicySearchResult", oSearchRequest.objSearchFilters.PolicySystemId.ToString())).Trim();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("policynumber");
                            if (objDbReader["POLICY_NUMBER"] != null && objDbReader["POLICY_NUMBER"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["POLICY_NUMBER"].ToString().Trim();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("policyname");
                            if (objDbReader["POLICY_SYMBOL"] != null && objDbReader["POLICY_SYMBOL"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["POLICY_SYMBOL"].ToString().Trim();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("module");
                            if (objDbReader["MODULE"] != null && objDbReader["MODULE"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["MODULE"].ToString().Trim();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("commercialname");
                            string sInsuredName = string.Empty;
                            if (objDbReader["FIRST_NAME"] != null && objDbReader["FIRST_NAME"] != DBNull.Value)
                                sInsuredName = objDbReader["FIRST_NAME"].ToString().Trim();
                            if (objDbReader["LAST_NAME"] != null && objDbReader["LAST_NAME"] != DBNull.Value)
                            {
                                if (sInsuredName != string.Empty)
                                    sInsuredName = " ";
                                sInsuredName = objDbReader["LAST_NAME"].ToString().Trim();
                            }
                            objXmlElement.InnerText = sInsuredName;
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("sortname");
                            if (sInsuredName.Length >= 15)
                                objXmlElement.InnerText = sInsuredName.Substring(0, 15);
                            else
                                objXmlElement.InnerText = sInsuredName;
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("lastname");
                            if (objDbReader["LAST_NAME"] != null && objDbReader["LAST_NAME"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["LAST_NAME"].ToString().Trim();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("firstname");
                            if (objDbReader["FIRST_NAME"] != null && objDbReader["FIRST_NAME"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["FIRST_NAME"].ToString().Trim();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("middlename");
                            if (objDbReader["MIDDLE_NAME"] != null && objDbReader["MIDDLE_NAME"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["MIDDLE_NAME"].ToString().Trim();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("city");
                            if (objDbReader["CITY"] != null && objDbReader["CITY"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["CITY"].ToString().Trim();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("state");
                            if (objDbReader["STATE_ID"] != null && objDbReader["STATE_ID"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["STATE_ID"].ToString().Trim();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("effectivedate");
                            if (objDbReader["EFFECTIVE_DATE"] != null && objDbReader["EFFECTIVE_DATE"] != DBNull.Value)
                                objXmlElement.InnerText = Conversion.GetDBDateFormat(objDbReader["EFFECTIVE_DATE"].ToString(), "MM/dd/yyyy");
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("expirationdate");
                            if (objDbReader["EXPIRATION_DATE"] != null && objDbReader["EXPIRATION_DATE"] != DBNull.Value)
                                objXmlElement.InnerText = Conversion.GetDBDateFormat(objDbReader["EXPIRATION_DATE"].ToString(), "MM/dd/yyyy");
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("location");
                            if (objDbReader["LOCATION_COMPANY"] != null && objDbReader["LOCATION_COMPANY"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["LOCATION_COMPANY"].ToString();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("mastercompany");
                            if (objDbReader["MASTER_COMPANY"] != null && objDbReader["MASTER_COMPANY"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["MASTER_COMPANY"].ToString();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);


                            objXmlElement = objXmlDocument.CreateElement("agency");
                            if (objDbReader["REFERENCE_NUMBER"] != null && objDbReader["REFERENCE_NUMBER"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["REFERENCE_NUMBER"].ToString().Trim();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("lob");
                            if (objDbReader["POLICY_LOB_CODE"] != null && objDbReader["POLICY_LOB_CODE"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["POLICY_LOB_CODE"].ToString().Trim();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = objXmlDocument.CreateElement("StagingPolicyID");
                            if (objDbReader["POLICY_ID"] != null && objDbReader["POLICY_ID"] != DBNull.Value)
                                objXmlElement.InnerText = objDbReader["POLICY_ID"].ToString();
                            else
                                objXmlElement.InnerText = "";
                            objRowXmlElement.AppendChild(objXmlElement);

                            objRootElement.AppendChild(objRowXmlElement);
                        }
                    }

                    //using (objDbReader = DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString()))
                    //{
                    //    while (objDbReader.Read())
                    //    {
                    //        lstPolicyIds.Add(Convert.ToInt32(objDbReader.GetValue("Policy_ID")));
                    //    }
                    //}

                    //objXmlDocument = new XmlDocument();

                    //if (lstPolicyIds.Count > 0)
                    //{
                    //    objRootElement = objXmlDocument.CreateElement("SearchResults");
                    //    objXmlDocument.AppendChild(objRootElement);


                        //foreach (int iPolicyID in lstPolicyIds)
                        //{
                        //    objPolicyStg = (PolicyStgng)objDMF.GetDataModelObject("PolicyStgng", false);
                        //    objPolicyStg.MoveTo(iPolicyID);

                        //    DataSimpleList objPolicyInsuredStg = objPolicyStg.PolicyXInsuredStgng;
                        //    ObjPolInsuredEntStg = (EntityStgng)objDMF.GetDataModelObject("EntityStgng", false);
                        //    ObjPolInsuredEntStg.MoveTo(Conversion.CastToType<int>(objPolicyInsuredStg.ToString(), out bSucc));

                        //    objRowXmlElement = objXmlDocument.CreateElement("Row");

                        //    objXmlElement = objXmlDocument.CreateElement("status");
                        //    if (!String.IsNullOrEmpty(objPolicyStg.PolicyStatusCode))
                        //        objXmlElement.InnerText = objPolicyStg.PolicyStatusCode.Trim() + " " + objDMF.Context.LocalCache.GetCodeDesc(objDMF.Context.LocalCache.GetCodeId(objPolicyStg.PolicyStatusCode, "POLICY_STATUS")).Trim();
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("policynumber");
                        //    if (!String.IsNullOrEmpty(objPolicyStg.PolicyNumber))
                        //        objXmlElement.InnerText = objPolicyStg.PolicyNumber.Trim();
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("policyname");
                        //    if (!String.IsNullOrEmpty(objPolicyStg.PolicySymbol))
                        //        objXmlElement.InnerText = objPolicyStg.PolicySymbol.Trim();
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("module");
                        //    if (!String.IsNullOrEmpty(objPolicyStg.Module))
                        //        objXmlElement.InnerText = objPolicyStg.Module.Trim();
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("commercialname");
                        //    string sInsuredName = string.Empty;
                        //    if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.FirstName))
                        //        sInsuredName = ObjPolInsuredEntStg.FirstName.Trim();
                        //    if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.LastName))
                        //    {
                        //        if (sInsuredName != string.Empty)
                        //            sInsuredName = " ";
                        //        sInsuredName = ObjPolInsuredEntStg.LastName.Trim();
                        //    }
                        //    objXmlElement.InnerText = sInsuredName;
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("sortname");
                        //    if (sInsuredName.Length >= 15)
                        //        objXmlElement.InnerText = sInsuredName.Substring(0, 15);
                        //    else
                        //        objXmlElement.InnerText = sInsuredName;
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("lastname");
                        //    if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.LastName))
                        //        objXmlElement.InnerText = ObjPolInsuredEntStg.LastName.Trim();
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("firstname");
                        //    if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.FirstName))
                        //        objXmlElement.InnerText = ObjPolInsuredEntStg.FirstName.Trim();
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("middlename");
                        //    if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.MiddleName))
                        //        objXmlElement.InnerText = ObjPolInsuredEntStg.MiddleName.Trim();
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("city");
                        //    if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.City))
                        //        objXmlElement.InnerText = ObjPolInsuredEntStg.City.Trim();
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("state");
                        //    if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.StateId))
                        //    {
                        //        //string sStateCode = string.Empty;
                        //        //string sStateDesc = string.Empty;
                        //        //objDMF.Context.LocalCache.GetStateInfo(objDMF.Context.LocalCache.GetStateRowID(ObjPolInsuredEntStg.StateId), ref sStateCode, ref sStateDesc);
                        //        objXmlElement.InnerText = ObjPolInsuredEntStg.StateId.Trim();
                        //    }
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("effectivedate");
                        //    if (!String.IsNullOrEmpty(objPolicyStg.EffectiveDate))
                        //        objXmlElement.InnerText = Conversion.GetDBDateFormat(objPolicyStg.EffectiveDate, "MM/dd/yyyy");
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("expirationdate");
                        //    if (!String.IsNullOrEmpty(objPolicyStg.ExpirationDate))
                        //        objXmlElement.InnerText = Conversion.GetDBDateFormat(objPolicyStg.ExpirationDate, "MM/dd/yyyy");
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("location");
                        //    objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("mastercompany");
                        //    objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    foreach (PolicyXEntityStgng objPolicyXEntStg in objPolicyStg.PolicyXEntityListStgng)
                        //    {
                        //        int iAgentTableId = 0;
                        //        bool bSuccess = false;
                        //        iAgentTableId = objDMF.Context.LocalCache.GetTableId("AGENTS");
                        //        string sSQL = "SELECT COUNT(*) FROM ENTITY_STGNG WHERE ENTITY_ID =  " + objPolicyXEntStg.EntityId + " AND ENTITY_TABLE_ID = " + iAgentTableId;

                        //        int iAgentExists = Conversion.CastToType<int>((DbFactory.ExecuteScalar(m_sConnectString, sSQL)).ToString(), out bSuccess);

                        //        if (iAgentExists > 0)
                        //        {
                        //            EntityStgng oAgentEnt = (EntityStgng)objDMF.GetDataModelObject("EntityStgng", false);
                        //            oAgentEnt.MoveTo(objPolicyXEntStg.EntityId);
                        //            objXmlElement = objXmlDocument.CreateElement("agency");
                        //            if (!String.IsNullOrEmpty(oAgentEnt.ReferenceNumber))
                        //                objXmlElement.InnerText = oAgentEnt.ReferenceNumber.Trim();
                        //            else
                        //                objXmlElement.InnerText = "";
                        //            objRowXmlElement.AppendChild(objXmlElement);
                        //            break;
                        //        }
                        //    }
                        //    objXmlElement = objXmlDocument.CreateElement("lob");
                        //    if (!String.IsNullOrEmpty(objPolicyStg.PolicyLOB))
                        //        objXmlElement.InnerText = objPolicyStg.PolicyLOB.Trim();
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objXmlElement = objXmlDocument.CreateElement("StagingPolicyID");
                        //    if (objPolicyStg.PolicyId != 0)
                        //        objXmlElement.InnerText = objPolicyStg.PolicyId.ToString();
                        //    else
                        //        objXmlElement.InnerText = "";
                        //    objRowXmlElement.AppendChild(objXmlElement);

                        //    objRootElement.AppendChild(objRowXmlElement);

                        //    objPolicyStg.Dispose();
                        //}
                    //}
                    // Pshekhawat 8/14/2014 - for Policy Search Performance Improvement - Ends

                }
                catch (Exception p_objExp)
                {
                    throw new RMAppException(Globalization.GetString("PolicyStagingInterface.GetStgPolicySearchResult.ErrorFetchingResult", m_iClientId), p_objExp);
                }
                finally
                {
                    objRowXmlElement = null;
                    objXmlElement = null;
                    objRootElement = null;
                    lstPolicyIds = null;
                    sbSQL = null;
                    if (objDMF != null)
                        objDMF.Dispose();
                    if (objDbReader != null)
                        objDbReader.Dispose();
                    //// Pradyumna Test - Start
                    //if (objPolicyStg != null)
                    //    objPolicyStg.Dispose();
                    //if (ObjPolInsuredEntStg != null)
                    //    ObjPolInsuredEntStg.Dispose();
                    //// Pradyumna Test - Ends
                    
                }
                return objXmlDocument;
            }

            /// <summary>
            /// Save Policy and its related data like Insured, Insurer, Broker from Staging Area to rmA
            /// </summary>
            /// <param name="p_iStgPolicyID"></param>
            /// <param name="p_iClaimId"></param>
            /// <returns></returns>
            /// Pradyumna GAP16 MITS 33414 - 12/13/2013 - WWIG
            public Policy SavePolicyFrmStaging(int p_iStgPolicyID, int p_iClaimId, int p_iPolicySystemId)
            {
                int iCodeId = 0;
                int iPolicyId = 0;
                int iInsuredEntityId = 0;
                int iInsurerEntityId = 0;
                int iBrokerEntityId = 0;
                int iAgent_EId = 0;
                bool bSuccess = false;
                string sSQL = string.Empty;
                SysSettings objSettings = null;
                PolicyStgng objStgPolicy = null;
                EntityStgng objPolicyEntityStg = null;
                Policy objPolicy = null;
                DataModelFactory objDMF = null;
                PolicySystemInterface objPolicySys = null;

                bool bIsOrphan = false;
                int iPolicyCount = 0;

                //tanwar2 - staring - start
                DataModelFactory objStagingDMF = null;
                //tanwar2- staging - end
                //Anu Tennyson Starts Pre/POST WWIG Starts
                bool bIsTableIdPresent = false;
                //Anu Tennyson Ends
                try
                {
                    objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                    objPolicySys = new PolicySystemInterface(m_oUserLogin);
                    // Initializing Policy datamodel
                    objPolicy = (Policy)objDMF.GetDataModelObject("Policy", false);

                    //tanwar2 - Initialize staging data model factory object
                    objStagingDMF = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);

                    // Initializing Staging Policy datamodel
                    objStgPolicy = objStagingDMF.GetDataModelObject("PolicyStgng", false) as PolicyStgng;
                    objStgPolicy.MoveTo(p_iStgPolicyID);


                    #region Check for Policy Duplication

                    if (objStgPolicy.EffectiveDate.Trim().Length == 8 && objStgPolicy.ExpirationDate.Trim().Length == 8 && objStgPolicy.PolicyNumber.Length > 0)
                    {
                        if (p_iClaimId > 0)
                        {
                            ////sSQL = "SELECT CASE (SELECT COUNT(*) FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId + ")	WHEN 0 THEN (SELECT MAX(POLICY_ID) POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN (SELECT POLICY_ID FROM CLAIM_X_POLICY) AND EFFECTIVE_DATE ='" + objStgPolicy.EffectiveDate + "' AND EXPIRATION_DATE ='" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER ='" + objStgPolicy.PolicyNumber + "' ) ";
                            ////sSQL = sSQL + "ELSE (SELECT POLICY_ID FROM POLICY WHERE POLICY_ID IN (SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId + ") AND EFFECTIVE_DATE = " + objStgPolicy.EffectiveDate + " AND EXPIRATION_DATE = '" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER = '" + objStgPolicy.PolicyNumber + "' ) END POLICY_ID ";

                            //sSQL = "SELECT COUNT(*) from CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId;
                            //iPolicyCount = DbFactory.ExecuteAsType<int>(m_sConnectString, sSQL);

                            //if (iPolicyCount == 0)
                            //{
                            //    //We are setting bIsOrphan as true irrespective of whether the query returns Policy Id or not
                            //    //If the query returns no policy - the method OrphanPolicyCleanUp will not be called
                            //    sSQL = "SELECT MAX(POLICY_ID) POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN (SELECT POLICY_ID FROM CLAIM_X_POLICY) AND EFFECTIVE_DATE ='" + objStgPolicy.EffectiveDate + "' AND EXPIRATION_DATE ='" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER ='" + objStgPolicy.PolicyNumber + "'";
                            //    bIsOrphan = true;
                            //}
                            //else if (iPolicyCount > 0)
                            //{
                            //    sSQL = "SELECT POLICY_ID FROM POLICY WHERE POLICY_ID IN (SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId + ") AND EFFECTIVE_DATE = " + objStgPolicy.EffectiveDate + " AND EXPIRATION_DATE = '" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER = '" + objStgPolicy.PolicyNumber + "' ";
                            //}

                            // Pradyumna 2/12/2014 - Gap16 - Modified - To validate the screnario where a policy is attached with claim and second policy is checked for Orphan policy. - Start
                            sSQL = "SELECT POLICY_ID FROM POLICY WHERE POLICY_ID IN (SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + p_iClaimId + ") AND EFFECTIVE_DATE = " + objStgPolicy.EffectiveDate + " AND EXPIRATION_DATE = '" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER = '" + objStgPolicy.PolicyNumber + "' ";
                            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                            {
                                if (objReader.Read())
                                {
                                    iPolicyId = Conversion.CastToType<int>(objReader.GetValue("POLICY_ID").ToString(), out bSuccess);
                                }
                            }
                            if (iPolicyId <= 0)
                            {
                                sSQL = "SELECT MAX(POLICY_ID) POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN (SELECT POLICY_ID FROM CLAIM_X_POLICY) AND EFFECTIVE_DATE ='" + objStgPolicy.EffectiveDate + "' AND EXPIRATION_DATE ='" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER ='" + objStgPolicy.PolicyNumber + "'";
                                bIsOrphan = true;
                            }
                            // Pradyumna 2/12/2014 - Gap16 - Ends
                        }
                        else
                        {
                            sSQL = "SELECT POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN ( SELECT POLICY_ID FROM CLAIM_X_POLICY ) AND EFFECTIVE_DATE = " + objStgPolicy.EffectiveDate + " AND EXPIRATION_DATE = '" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER = '" + objStgPolicy.PolicyNumber + "'";
                            bIsOrphan = true;
                        }
                    }

                    if (!string.IsNullOrEmpty(sSQL) && iPolicyId <= 0)
                    {
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                iPolicyId = Conversion.CastToType<int>(objReader.GetValue("POLICY_ID").ToString(), out bSuccess);
                            }
                        }
                    }

                    if (iPolicyId > 0)
                    {
                        if (bIsOrphan)
                        {
                            OrphanPolicyCleanUp(iPolicyId);
                        }
                        objPolicy.MoveTo(iPolicyId);
                        objPolicy.DataChanged = true; // Pradyumna 05/20/2014: To Update data in case Policy is re-downloaded
                    }

                    #endregion

                    #region Populating Policy Datamodel with values from PolicyStaging Datamodel
                    // Policy Name 
                    objPolicy.PolicyName = objStgPolicy.PolicyName.Trim();

                    
                    objPolicy.PolicySystemId = p_iPolicySystemId;

                    // PolicyNumber
                    objPolicy.PolicyNumber = objStgPolicy.PolicyNumber.Trim();

                    // PolicyStatusCode
                    if (objStgPolicy.PolicyStatusCode.Trim().Length > 0)
                    {
                        iCodeId = objPolicySys.GetRMXCodeIdFromPSMappedCode(objStgPolicy.PolicyStatusCode.Trim(), "POLICY_STATUS", "Policy Status on SavePolicyFrmStaging", p_iPolicySystemId.ToString());
                        objPolicy.PolicyStatusCode = iCodeId;
                    }
                    // Premium
                    objPolicy.Premium = objStgPolicy.Premium;

                    // EffectiveDate
                    objPolicy.EffectiveDate = objStgPolicy.EffectiveDate.Trim();

                    // ExpirationDate
                    objPolicy.ExpirationDate = objStgPolicy.ExpirationDate.Trim();

                    // Cancel date
                    objPolicy.CancelDate = objStgPolicy.CancelledDate.Trim();

                    // TriggerClaimFlag
                    objSettings = new SysSettings(m_sConnectString, m_iClientId);
                    if (objSettings.PolicyCvgType == 0)
                        objPolicy.TriggerClaimFlag = true;
                    else
                        objPolicy.TriggerClaimFlag = false;

                    // PrimaryPolicyFlg
                    objPolicy.PrimaryPolicyFlg = true;

                    // InsurerEid
                    objPolicy.InsurerEid = objStgPolicy.InsurerEid;

                    // Policy Symbol
                    objPolicy.PolicySymbol = objStgPolicy.PolicySymbol.Trim();

                    // Currency Code
                    if (objStgPolicy.CurrencyCode.Trim().Length > 0)
                    {
                        iCodeId = objPolicySys.GetRMXCodeIdFromPSMappedCode(objStgPolicy.CurrencyCode.Trim(), "CURRENCY_TYPE", "Currency on SavePolicyFrmStaging", p_iPolicySystemId.ToString());
                        objPolicy.CurrencyCode = iCodeId;
                    }
                    //LOB
                    if (objStgPolicy.PolicyLOB.Trim().Length > 0)
                    {
                        iCodeId = objPolicySys.GetRMXCodeIdFromPSMappedCode(objStgPolicy.PolicyLOB.Trim(), "POLICY_CLAIM_LOB", "LOB on SavePolicyFrmStaging", p_iPolicySystemId.ToString());
                        objPolicy.PolicyLOB = iCodeId;
                    }
                    
                    //Module
                    objPolicy.Module = objStgPolicy.Module.Trim();

                    // Issue date
                    objPolicy.IssueDate = objStgPolicy.IssueDate.Trim();

                    // All States flag
                    objPolicy.AllStatesFlag = objStgPolicy.AllStatesFlag;

                    // BrokerEid
                    //objPolicy.BrokerEid = objStgPolicy.BrokerEid;
                    objPolicyEntityStg = (EntityStgng)objStagingDMF.GetDataModelObject("EntityStgng", false);
                    if (objStgPolicy.BrokerEid > 0)
                    {
                        objPolicyEntityStg.MoveTo(objStgPolicy.BrokerEid);
                        iBrokerEntityId = SaveEntityFrmStaging(objPolicyEntityStg, "BROKER", p_iPolicySystemId);
                    }
                    objPolicyEntityStg.Dispose();
                    objPolicy.BrokerEid = iBrokerEntityId;
                    #endregion

                    // Save Insured Entity
                    DataSimpleList objPolicyInsuredStg = objStgPolicy.PolicyXInsuredStgng;
                    objPolicyEntityStg = objStagingDMF.GetDataModelObject("EntityStgng", false) as EntityStgng;
                    if (objPolicyInsuredStg != null && !string.IsNullOrEmpty(objPolicyInsuredStg.ToString()))
                    {
                        objPolicyEntityStg.MoveTo(Conversion.CastToType<int>(objPolicyInsuredStg.ToString(), out bSuccess));
                        iInsuredEntityId = SaveEntityFrmStaging(objPolicyEntityStg, "INSURED", p_iPolicySystemId);
                    }
                    objPolicyEntityStg.Dispose();

                    // Save Insurer Entity
                    objPolicyEntityStg = objStagingDMF.GetDataModelObject("EntityStgng", false) as EntityStgng;
                    if (objStgPolicy.InsurerEid > 0)
                    {
                        objPolicyEntityStg.MoveTo(objStgPolicy.InsurerEid);
                        iInsurerEntityId = SaveEntityFrmStaging(objPolicyEntityStg, "INSURER", p_iPolicySystemId);
                    }
                    if (iInsurerEntityId > 0)
                    {
                        objPolicy.InsurerEid = iInsurerEntityId;
                    }
                    objPolicyEntityStg.Dispose();

                    // insert record in policy_x_insurer table for new policy 
                    if (objPolicy.PolicyId <= 0)
                    {
                        if (objPolicy.InsurerEid != 0)
                        {
                            PolicyXInsurer objPolIns = objPolicy.PolicyXInsurerList.AddNew();
                            objPolIns.InsurerCode = objPolicy.InsurerEid;
                            objPolIns.PolicyId = objPolicy.PolicyId;
                            objPolIns.ResPercentage = 100.0;
                            objPolIns.PrimaryInsurer = true;
                        }
                    }
                    else // for existing policy, 
                    {
                        //Count = 0 => record does not exists in POLICY_X_INSURER; InsurereEid != 0 => there is insurer info to be saved
                        if (objPolicy.PolicyXInsurerList.Count == 0 && objPolicy.InsurerEid != 0)
                        {
                            PolicyXInsurer objPolIns = objPolicy.PolicyXInsurerList.AddNew(); ;
                            objPolIns.InsurerCode = objPolicy.InsurerEid;
                            objPolIns.PolicyId = objPolicy.PolicyId;
                            objPolIns.ResPercentage = 100.0;
                            objPolIns.PrimaryInsurer = true;
                        }
                    }
                    // To fire script before saving policy
                    objPolicy.FiringScriptFlag = 2;
                    objPolicy.Save();


                    // SAVE data into intoPOLICYXINSURED
                    PolicyXInsuredStgng objPolXInsuredStg = objStagingDMF.GetDataModelObject("PolicyXInsuredStgng", false) as PolicyXInsuredStgng;
                    sSQL = "SELECT COUNT(*) FROM POLICY_X_INSURED WHERE POLICY_ID = " + objPolicy.PolicyId + " AND INSURED_EID = " + iInsuredEntityId;

                    int iExists = Conversion.CastToType<int>((DbFactory.ExecuteScalar(m_sConnectString, sSQL)).ToString(), out bSuccess);
                    if (iExists <= 0)
                    {
                        sSQL = string.Format("INSERT INTO POLICY_X_INSURED (POLICY_ID, INSURED_EID) VALUES ({0},{1})", new object[] { objPolicy.PolicyId, iInsuredEntityId });
                        objDMF.Context.DbConnLookup.ExecuteNonQuery(sSQL);
                    }

                    // Commented (Not Needed)- Save Broker in POLICY_X_ENTITY
                    //objPolicySys.SavePolicyXEntity(objPolicy.PolicyId, iBrokerEntityId, objDMF.Context.LocalCache.GetTableId("BROKER"), 0, "BROKER");

                    // Saving Agents
                    foreach (PolicyXEntityStgng objPolicyXEntStg in objStgPolicy.PolicyXEntityListStgng)
                    {
                        int iAgentTableId = 0;
                        iAgentTableId = objDMF.Context.LocalCache.GetTableId("AGENTS");
                        if (objPolicyXEntStg.EntityId > 0)
                        {
                            sSQL = "SELECT COUNT(*) FROM ENTITY_STGNG WHERE ENTITY_ID =  " + objPolicyXEntStg.EntityId + " AND ENTITY_TABLE_ID = " + iAgentTableId;

                            int iAgentExists = Conversion.CastToType<int>((DbFactory.ExecuteScalar(objStagingDMF.Context.DbConn.ConnectionString, sSQL)).ToString(), out bSuccess);
                            if (iAgentExists == 0)
                            {
                                sSQL = "SELECT COUNT(*) FROM ENTITY_STGNG WHERE ENTITY_ID =  " + objPolicyXEntStg.EntityId + " AND ENTITY_TABLE_NAME = 'AGENTS'";
                                iAgentExists = Conversion.CastToType<int>((DbFactory.ExecuteScalar(objStagingDMF.Context.DbConn.ConnectionString, sSQL)).ToString(), out bSuccess);
                            }

                            if (iAgentExists > 0)
                            {
                                objPolicyEntityStg = objStagingDMF.GetDataModelObject("EntityStgng", false) as EntityStgng;
                                objPolicyEntityStg.MoveTo(objPolicyXEntStg.EntityId);
                                iAgent_EId = objPolicySys.AgentExists(objPolicy.PolicyId, iAgentTableId, objPolicyEntityStg.LastName, objPolicyEntityStg.ReferenceNumber);
                                if (iAgent_EId == 0)
                                {
                                    iAgent_EId = SaveEntityFrmStaging(objPolicyEntityStg, "AGENT", p_iPolicySystemId);
                                    objPolicySys.SavePolicyXEntity(objPolicy.PolicyId, iAgent_EId, iAgentTableId, 0, "agents");
                                }
                                else if (iAgent_EId > 0)
                                {
                                    objPolicySys.SavePolicyXEntity(objPolicy.PolicyId, iAgent_EId, iAgentTableId, 0, "agents");
                                }
                            }
                        }
                    }
                }
                // Pradyumna 05/21/2014: Added to display error message thrown from script editor - Start
                catch (RMAppException p_objExp)
                {
                    foreach (string sError in objPolicy.Context.ScriptValidationErrors.Values)
                    {
                        throw new RMAppException(sError);
                    }
                    throw;
                }// Pradyumna 05/21/2014: Added to display error message thrown from script editor - End
                catch (Exception p_objExp)
                {
                    throw new RMAppException(Globalization.GetString("PolicyStagingInterface.SavePolicyFrmStaging.ErrorSavingStagingPolicy", m_iClientId), p_objExp);
                }
                finally
                {
                    if (objSettings != null)
                        objSettings = null;
                    if (objStgPolicy != null)
                        objStgPolicy.Dispose();
                    if (objPolicyEntityStg != null)
                        objPolicyEntityStg.Dispose();
                    if (objDMF != null)
                        objDMF.Dispose();
                    if (objPolicySys != null)
                        objPolicySys = null;
                    if (objStagingDMF!=null)
                    {
                        objStagingDMF.Dispose();
                    }
                }
                return objPolicy;
            }

            /// <summary>
            /// Saves Entity from Staging tables to rmA 
            /// </summary>
            /// <param name="p_objPolicyEntityStg"></param>
            /// <param name="sEntityType">INSURED, INSURER, AGENT etc.</param>
            /// <returns></returns>
            /// Pradyumna 12/16/2013 - GAP16 MITS 33414
            private int SaveEntityFrmStaging(EntityStgng p_objPolicyEntityStg, string sEntityType, int p_iPolicySystemId)
            {
                Entity objEntity = null;
                DataModelFactory objDMF = null;
                int iEntityId = 0;

                try
                {
                    objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId );

                    objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                    objEntity.DataChanged = true; // Pradyumna 05/20/2014: To Update data in case Policy is re-downloaded
                    #region Check for Entity Duplication

                    if (String.Compare(sEntityType, "INSURED", true) == 0)
                    {
                        iEntityId = CheckEntityDuplication(p_objPolicyEntityStg, "POLICY_X_INSURED", string.Empty, p_iPolicySystemId);
                    }
                    else if (String.Compare(sEntityType, "INSURER", true) == 0)
                    {
                        bool bDataExists = false;
                        using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT ENTITY_ID FROM ENTITY WHERE REFERENCE_NUMBER='" + p_objPolicyEntityStg.ReferenceNumber + "'"))
                        {
                            if (objRdr.Read())
                            {
                                iEntityId = objRdr.GetInt32(0);
                                bDataExists = true;
                            }
                        }
                        if (!bDataExists)
                        {
                            PopulateEntityFrmStaging(ref objEntity, p_objPolicyEntityStg, p_iPolicySystemId);
                            objEntity.Save();
                            iEntityId = objEntity.EntityId;
                        }
                        return iEntityId;
                    }
                    else if (String.Compare(sEntityType, "BROKER", true) == 0)
                    {
                        iEntityId = CheckEntityDuplication(p_objPolicyEntityStg, "POLICY_X_ENTITY", string.Empty, p_iPolicySystemId);
                    }
                    else if (String.Compare(sEntityType, "AGENT", true) == 0)
                    {
                        bool bDataExists = false;
                        using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT ENTITY_ID FROM ENTITY WHERE REFERENCE_NUMBER='" + p_objPolicyEntityStg.ReferenceNumber + "'"))
                        {
                            if (objRdr.Read())
                            {
                                iEntityId = objRdr.GetInt32(0);
                                bDataExists = true;
                            }
                        }
                        if (!bDataExists)
                        {
                            PopulateEntityFrmStaging(ref objEntity, p_objPolicyEntityStg, p_iPolicySystemId);
                            objEntity.Save();
                            iEntityId = objEntity.EntityId;
                        }
                        return iEntityId;
                    }


                    if (iEntityId > 0)
                    {
                        objEntity.EntityId = iEntityId;
                        objEntity.MoveTo(iEntityId);
                    }

                    #endregion

                    PopulateEntityFrmStaging(ref objEntity, p_objPolicyEntityStg,  p_iPolicySystemId);
                    // To fire scripts before saving Entity
                    objEntity.FiringScriptFlag = 2;
                    objEntity.Save();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (p_objPolicyEntityStg != null)
                        p_objPolicyEntityStg.Dispose();
                    if (objEntity != null)
                        objEntity.Dispose();
                    if (objDMF != null)
                        objDMF.Dispose();
                }
                return objEntity.EntityId;
            }

            /// <summary>
            /// Save Entity data from Staging
            /// </summary>
            /// <param name="sSelEntityStgId"></param>
            /// <param name="sTableName"></param>
            /// <param name="iPolicyId"></param>
            /// <param name="sMode"></param>
            /// <returns></returns>
            public int SaveOptionsFrmStaging(int p_iPolicyId, int p_iStagingPolicyId, string p_sSelEntityStgId, string p_sEntityType, string p_sMode)
            {
                int iRecordId = 0;
                int iSelEntityStgId = 0;
                int iEntityTableId = 0;
                bool bSuccess = false;
                Entity objEntity = null;
                DataModelFactory objDmf = null;
                DataModelFactory objFactoryStgngDmf = null;
                EntityStgng objEntityStgng = null;
                PolicySystemInterface objPolicySysInterface = null;
                DbReader objDbReader = null;
                string sSQL = string.Empty;
                int iUnitIdStg = 0;
                string sUnitTypeStg = string.Empty;
                string sUnitType = string.Empty;
                int iPolicyUnitRowId = 0;
                int iEntityId = 0;
                int iPolicySystemId = 0;

                try
                {
                    iPolicySystemId = GetPolicySystemId(p_iPolicyId);
                    objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId );
                    objFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(GetPolicySystemId(p_iPolicyId)), m_iClientId);
                    objPolicySysInterface = new PolicySystemInterface(m_oUserLogin);

                    objEntity = (Entity)objDmf.GetDataModelObject("Entity", false);

                    if (!String.IsNullOrEmpty(p_sSelEntityStgId))
                    {
                        iSelEntityStgId = Conversion.CastToType<int>(p_sSelEntityStgId, out bSuccess);
                    }
                    if (iSelEntityStgId > 0)
                    {
                        objEntityStgng = (EntityStgng)objFactoryStgngDmf.GetDataModelObject("EntityStgng", false);
                        objEntityStgng.MoveTo(iSelEntityStgId);

                        if (string.Equals(p_sMode, "driver", StringComparison.InvariantCultureIgnoreCase))
                            iEntityId = CheckEntityDuplication(objEntityStgng, "DRIVER", p_sEntityType, iPolicySystemId);
                        else
                            iEntityId = CheckEntityDuplication(objEntityStgng, "POLICY_X_ENTITY", p_sEntityType, iPolicySystemId);

                        if (iEntityId > 0)
                        {
                            objEntity.EntityId = iEntityId;
                            objEntity.MoveTo(iEntityId);
                            objEntity.DataChanged = true; // Pradyumna 05/20/2014: To Update data in case Policy is re-downloaded
                        }
                        PopulateEntityFrmStaging(ref objEntity, objEntityStgng, GetPolicySystemId(p_iPolicyId));

                        iEntityTableId = Conversion.CastToType<int>(p_sEntityType, out bSuccess);
                        if (iEntityTableId > 0)
                            objEntity.EntityTableId = iEntityTableId;
                        // To fire scripts before saving Entity
                        objEntity.FiringScriptFlag = 2;
                        objEntity.Save();
                        iRecordId = objEntity.EntityId;

                        objPolicySysInterface.CreateSubTypeEntity(iRecordId);

                        // Check if selected Entity is linked to any unit or not
                        sSQL = string.Format("SELECT UNIT_ID, UNIT_TYPE FROM POLICY_X_UNIT_STGNG WHERE POLICY_UNIT_ROW_ID = (SELECT TOP 1 POLICY_UNIT_ROW_ID FROM POLICY_X_ENTITY_STGNG WHERE ENTITY_ID = {0} AND POLICY_ID = {1})", p_sSelEntityStgId, p_iStagingPolicyId);
                        using (objDbReader = objFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(sSQL))
                        {
                            if (objDbReader != null)
                            {
                                while (objDbReader.Read())
                                {
                                    if (objDbReader["UNIT_ID"] != null && objDbReader["UNIT_ID"] != DBNull.Value)
                                    {
                                        iUnitIdStg = Conversion.CastToType<int>(objDbReader["UNIT_ID"].ToString(), out bSuccess);
                                    }
                                    if (objDbReader["UNIT_TYPE"] != null && objDbReader["UNIT_TYPE"] != DBNull.Value)
                                    {
                                        sUnitTypeStg = objDbReader["UNIT_TYPE"].ToString().Trim();
                                    }
                                }
                            }
                        }

                        if (iUnitIdStg != 0 && !string.IsNullOrEmpty(sUnitTypeStg))
                        {
                            sSQL = string.Format("SELECT PXU.POLICY_UNIT_ROW_ID FROM POINT_UNIT_DATA PUD INNER JOIN POLICY_X_UNIT PXU ON PUD.UNIT_ID = PXU.UNIT_ID AND PUD.UNIT_TYPE = PXU.UNIT_TYPE WHERE PXU.POLICY_ID = {0} AND PUD.STAT_UNIT_NUMBER = (SELECT STAT_UNIT_NUMBER FROM POINT_UNIT_DATA_STGNG WHERE UNIT_ID = {1} AND UNIT_TYPE = '{2}') ", p_iPolicyId, iUnitIdStg, sUnitTypeStg);
                            using (objDbReader = objDmf.Context.DbConnLookup.ExecuteReader(sSQL))
                            {
                                if (objDbReader != null)
                                {
                                    while (objDbReader.Read())
                                    {
                                        if (objDbReader["POLICY_UNIT_ROW_ID"] != null && objDbReader["POLICY_UNIT_ROW_ID"] != DBNull.Value)
                                        {
                                            iPolicyUnitRowId = Conversion.CastToType<int>(objDbReader["POLICY_UNIT_ROW_ID"].ToString(), out bSuccess);
                                        }
                                    }
                                }
                            }
                        }

                        // Check if Driver or Other Entity
                        if (string.Equals(p_sMode, "driver", StringComparison.InvariantCultureIgnoreCase))
                        {
                            int iDriverId = 0;
                            iRecordId = objPolicySysInterface.SavePolicyXEntity(p_iPolicyId, iRecordId, objEntity.EntityTableId, iPolicyUnitRowId, "Driver");
                            DriverStgng oDriverStgng = objFactoryStgngDmf.GetDataModelObject("DriverStgng", false) as DriverStgng;
                            using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT DRIVER_ROW_ID FROM DRIVER_STGNG WHERE DRIVER_EID=" + iSelEntityStgId))
                            {
                                if (objRdr.Read())
                                    iDriverId = Conversion.CastToType<int>(objRdr.GetValue(0).ToString(), out bSuccess);
                            }
                            if (iDriverId > 0)
                                oDriverStgng.MoveTo(iDriverId);
                            objPolicySysInterface.SaveDriver(oDriverStgng.LicenceDate, oDriverStgng.DriversLicNo.Trim(), iRecordId, 0, objDmf.Context.LocalCache.GetCodeId(oDriverStgng.MaritalStatCode.Trim(), "MARITAL_STATUS"));
                        }
                        else
                        {
                            iRecordId = objPolicySysInterface.SavePolicyXEntity(p_iPolicyId, iRecordId, objEntity.EntityTableId, iPolicyUnitRowId, "");
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    if (objEntity != null)
                        objEntity.Dispose();
                    if (objDmf != null)
                        objDmf.Dispose();

                    if (objFactoryStgngDmf != null)
                        objFactoryStgngDmf.Dispose();
                    
                    if (objEntityStgng != null)
                        objEntityStgng.Dispose();
                    if (objDbReader != null)
                        objDbReader.Dispose();
                    objPolicySysInterface = null;
                }
                return iRecordId;
            }

            /// <summary>
            /// Populates values of Entity Datamodel object with EntityStaging datamodel
            /// </summary>
            /// <param name="p_objEntity"></param>
            /// <param name="p_objPolicyEntityStg"></param>
            /// Pradyumna 12/16/2013 - GAP16 - MITS 33414
            private void PopulateEntityFrmStaging(ref Entity p_objEntity, EntityStgng p_objPolicyEntityStg, int p_iPolicySystemId)
            {
                DataModelFactory objDMF = null;
                string sTaxId = string.Empty;
                DataModelFactory objStagingDMF = null;
                try
                {
                    objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword,m_iClientId );

                    objStagingDMF = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);

                    #region Populating Entity Information From Entity Staging Datamodel
                    // Modified to Handled Masked SSN/TaxID
                    sTaxId = objStagingDMF.Context.DbConnLookup.ExecuteString("SELECT TAX_ID FROM ENTITY_STGNG WHERE ENTITY_ID=" + p_objEntity.EntityId);
                    if (!string.IsNullOrEmpty(sTaxId.Trim()))
                    {
                        p_objEntity.TaxId = sTaxId;
                    }
                    p_objEntity.FirstName = p_objPolicyEntityStg.FirstName.Trim();
                    p_objEntity.MiddleName = p_objPolicyEntityStg.MiddleName.Trim();
                    p_objEntity.LastName = p_objPolicyEntityStg.LastName.Trim();
                    //p_objEntity.NameType = objPolicyInsuredStg.Na
                    p_objEntity.Addr1 = p_objPolicyEntityStg.Addr1.Trim();
                    p_objEntity.Addr2 = p_objPolicyEntityStg.Addr2.Trim();
                    p_objEntity.ZipCode = p_objPolicyEntityStg.ZipCode.Trim();
                    p_objEntity.City = p_objPolicyEntityStg.City.Trim();
                    if (p_objPolicyEntityStg.StateId.Trim().Length > 0)
                        p_objEntity.StateId = GetRMXCodeIdFromPSMappedCode(p_objPolicyEntityStg.StateId.Trim(), "STATES", "State on PopulateEntityFrmStaging", p_iPolicySystemId.ToString());
                        //p_objEntity.StateId = objDMF.Context.LocalCache.GetStateRowID(p_objPolicyEntityStg.StateId.Trim());
                    if (p_objPolicyEntityStg.CountryCode.Trim().Length > 0)
                        p_objEntity.CountryCode = GetRMXCodeIdFromPSMappedCode(p_objPolicyEntityStg.CountryCode.Trim(), "COUNTRY", "Country on PopulateEntityFrmStaging", p_iPolicySystemId.ToString());
                        //p_objEntity.CountryCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.CountryCode.Trim(), "COUNTRY");
                    p_objEntity.County = p_objPolicyEntityStg.County.Trim();
                    p_objEntity.BirthDate = p_objPolicyEntityStg.BirthDate.Trim();
                    p_objEntity.EntityTableId = p_objPolicyEntityStg.EntityTableId;
                    //p_objEntity.ClientSequenceNumber = objPolInsuredEntStg.Cl;
                    //p_objEntity.AddressSequenceNumber = objPolInsuredEntStg.Addre;
                    p_objEntity.ParentEid = p_objPolicyEntityStg.ParentEid;
                    p_objEntity.Contact = p_objPolicyEntityStg.Contact.Trim();
                    p_objEntity.Comments = p_objPolicyEntityStg.Comments.Trim();
                    if (p_objPolicyEntityStg.EmailTypeCode.Trim().Length > 0)
                        p_objEntity.EmailTypeCode = GetRMXCodeIdFromPSMappedCode(p_objPolicyEntityStg.EmailTypeCode.Trim(), "EMAIL_TYPE", "Email on PopulateEntityFrmStaging", p_iPolicySystemId.ToString());
                        //p_objEntity.EmailTypeCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.EmailTypeCode.Trim(), "EMAIL_TYPE");
                    p_objEntity.EmailAddress = p_objPolicyEntityStg.EmailAddress.Trim();
                    if (p_objPolicyEntityStg.SexCode.Trim().Length > 0)
                        p_objEntity.SexCode = GetRMXCodeIdFromPSMappedCode(p_objPolicyEntityStg.SexCode.Trim(), "SEX_CODE", "Sex Code on PopulateEntityFrmStaging", p_iPolicySystemId.ToString());
                        //p_objEntity.SexCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.SexCode.Trim(), "SEX_CODE");
                    p_objEntity.EffStartDate = p_objPolicyEntityStg.EffStartDate.Trim();
                    p_objEntity.EffEndDate = p_objPolicyEntityStg.EffEndDate.Trim();
                    if (p_objPolicyEntityStg.BusinessTypeCode.Trim().Length > 0)
                        p_objEntity.BusinessTypeCode = GetRMXCodeIdFromPSMappedCode(p_objPolicyEntityStg.BusinessTypeCode.Trim(), "BUSINESS_TYPE", "Business Type on PopulateEntityFrmStaging", p_iPolicySystemId.ToString());
                        //p_objEntity.BusinessTypeCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.BusinessTypeCode.Trim(), "BUSINESS_TYPE");
                    if (p_objPolicyEntityStg.NatureOfBusiness.Trim().Length > 0)
                        p_objEntity.NatureOfBusiness = p_objPolicyEntityStg.NatureOfBusiness.Trim();
                    if (p_objPolicyEntityStg.SicCode.Trim().Length > 0)
                        p_objEntity.SicCode = GetRMXCodeIdFromPSMappedCode(p_objPolicyEntityStg.SicCode.Trim(), "SIC_CODE", "Sic Code on PopulateEntityFrmStaging", p_iPolicySystemId.ToString());
                        //p_objEntity.SicCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.SicCode.Trim(), "SIC_CODE");
                    p_objEntity.SicCodeDesc = p_objPolicyEntityStg.SicCodeDesc.Trim();
                    p_objEntity.WcFillingNumber = p_objPolicyEntityStg.WcFillingNumber.Trim();
                    p_objEntity.Parent1099EID = p_objPolicyEntityStg.Parent1099EID;
                    p_objEntity.Entity1099Reportable = p_objPolicyEntityStg.Entity1099Reportable;
                    p_objEntity.Title = p_objPolicyEntityStg.Title.Trim();
                    if (p_objPolicyEntityStg.NaicsCode.Trim().Length > 0)
                        p_objEntity.NaicsCode = GetRMXCodeIdFromPSMappedCode(p_objPolicyEntityStg.NaicsCode.Trim(), "SIC_CODE", "NAICS Code on PopulateEntityFrmStaging", p_iPolicySystemId.ToString());
                        //p_objEntity.NaicsCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.NaicsCode.Trim(), "NAICS_CODES");
                    p_objEntity.FreezePayments = p_objPolicyEntityStg.FreezePayments;
                    if (p_objPolicyEntityStg.OrganizationType.Trim().Length > 0)
                        p_objEntity.OrganizationType = GetRMXCodeIdFromPSMappedCode(p_objPolicyEntityStg.OrganizationType.Trim(), "ORGANIZATION_TYPE", "Org Type on PopulateEntityFrmStaging", p_iPolicySystemId.ToString());
                        //p_objEntity.OrganizationType = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.OrganizationType.Trim(), "ORGANIZATION_TYPE");
                    p_objEntity.NPINumber = p_objPolicyEntityStg.NPINumber.Trim();
                    if (p_objPolicyEntityStg.TimeZoneCode.Trim().Length > 0)
                        p_objEntity.TimeZoneCode = GetRMXCodeIdFromPSMappedCode(p_objPolicyEntityStg.TimeZoneCode.Trim(), "TIME_ZONE", "Time ZOne on PopulateEntityFrmStaging", p_iPolicySystemId.ToString());
                        //p_objEntity.TimeZoneCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.TimeZoneCode.Trim(), "TIME_ZONE");
                    //p_objEntity.Prefix = p_objPolicyEntityStg.Prefix.Trim();
                    //p_objEntity.SuffixCommon = objPolInsuredEntStg.SuffixCommon;
                    p_objEntity.SuffixLegal = p_objPolicyEntityStg.SuffixLegal.Trim();
                    p_objEntity.LegalName = p_objPolicyEntityStg.LegalName.Trim();
                    p_objEntity.ReferenceNumber = p_objPolicyEntityStg.ReferenceNumber.Trim();

                    #endregion
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (objDMF != null)
                        objDMF.Dispose();
                    if (p_objPolicyEntityStg != null)
                        p_objPolicyEntityStg.Dispose();
                }
            }

            /// <summary>
            /// Checks for Duplicate Entity based on the Entity Staging parameters - GAP16 MITS 33414
            /// </summary>
            /// <param name="p_objEntity"></param>
            /// <param name="p_sTableName"></param>
            /// <param name="p_sSelEntityType"></param>
            /// <returns></returns>
            /// Pradyumna 12/16/2013 - GAP16 MITS 33414
            private int CheckEntityDuplication(EntityStgng p_objEntity, string p_sTableName, string p_sSelEntityType, int p_iStagingPolicyId)
            {
                string sAdd = string.Empty;
                bool bSuccess = false;
                int iTableId = 0;
                StringBuilder sbSql = new StringBuilder();
                LocalCache objCache = null;
                int iEntityID = 0;
                string sPolicyFields = string.Empty;
                Dictionary<string, string> strDictParams = new Dictionary<string, string>();
                DataModelFactory objStagingDMF = null;
                string sTaxId = string.Empty;
                try
                {
                    objCache = new LocalCache(m_sConnectString, m_iClientId);
                    objStagingDMF = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iStagingPolicyId), m_iClientId);
                    switch (p_sTableName.ToUpper())
                    {
                        case "POLICY_X_INSURED":
                            sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("PolicyHolderDupCheckFields", string.Empty);
                            break;
                        case "POLICY_X_ENTITY":
                            sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("AdditionalInterestDupCheckFields", string.Empty);
                            break;
                        case "DRIVER":
                            sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("DriverDupCheckFields", string.Empty);
                            break;
                    }
                    string[] arrValues = sPolicyFields.Split(',');
                    sbSql.Append("SELECT ENTITY.ENTITY_ID FROM ENTITY WHERE 1=1");

                    if (p_objEntity != null)
                    {
                        for (int i = 0; i <= arrValues.Length - 1; i++)
                        {
                            if (string.Compare(arrValues[i], "LastName", false) == 0)
                            {
                                if (p_objEntity.LastName.Trim() != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.LAST_NAME= ~LASTNAME~");
                                    strDictParams.Add("LASTNAME", p_objEntity.LastName.Trim());
                                }
                            }
                            else if (string.Compare(arrValues[i], "FirstName", false) == 0)
                            {
                                if (p_objEntity.FirstName.Trim() != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.FIRST_NAME= ~FIRSTNAME~");
                                    strDictParams.Add("FIRSTNAME", p_objEntity.FirstName.Trim());
                                }
                            }
                            else if (string.Compare(arrValues[i], "TaxId", false) == 0)
                            {
                                //tanwar2 - tax id can be maxed by changing settings in utility
                                //if (p_objEntity.TaxId.Trim() != string.Empty)
                                //{
                                //    sbSql.Append(" AND ENTITY.TAX_ID= ~TAXID~");
                                //    strDictParams.Add("TAXID", p_objEntity.TaxId.Trim());
                                //}
                                sTaxId = objStagingDMF.Context.DbConnLookup.ExecuteString("SELECT TAX_ID FROM ENTITY_STGNG WHERE ENTITY_ID=" + p_objEntity.EntityId);
                                if (!string.IsNullOrEmpty(sTaxId.Trim()))
                                {
                                    sbSql.Append(" AND ENTITY.TAX_ID= ~TAXID~");
                                    strDictParams.Add("TAXID", sTaxId.Trim());
                                }
                                //tanwar2 - end
                            }

                            else if (string.Compare(arrValues[i].Trim(), "Addr", false) == 0)
                            {
                                if (p_objEntity.Addr1 != string.Empty)
                                {
                                    sAdd = p_objEntity.Addr1.Trim();
                                }

                                if (p_objEntity.Addr2 != string.Empty)
                                {
                                    sAdd = p_objEntity.Addr2.Trim();
                                }
                                if (sAdd != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.ADDR1 + ENTITY.ADDR2= ~ADDR~");
                                    strDictParams.Add("ADDR", sAdd);
                                }
                            }
                            else if (string.Compare(arrValues[i].Trim(), "EmailAddress", false) == 0)
                            {
                                if (p_objEntity.EmailAddress != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.EMAIL_ADDRESS= ~EMAIL~");
                                    strDictParams.Add("EMAIL", p_objEntity.EmailAddress.Trim());
                                }
                            }
                            else if (string.Compare(arrValues[i].Trim(), "Contact", false) == 0)
                            {
                                if (p_objEntity.Contact != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.CONTACT= ~CONTACT~");
                                    strDictParams.Add("CONTACT", p_objEntity.Contact.Trim());
                                }
                            }
                            else if (string.Compare(arrValues[i].Trim(), "ZipCode", false) == 0)
                            {
                                if (p_objEntity.ZipCode != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.ZIP_CODE= ~ZIPCODE~");
                                    strDictParams.Add("ZIPCODE", p_objEntity.ZipCode.Trim());
                                }
                            }
                        }
                        if (String.IsNullOrEmpty(p_sSelEntityType))
                        {
                            if (p_objEntity.EntityTableId > 0)
                                iTableId = p_objEntity.EntityTableId;
                        }
                        else
                        {
                             iTableId = Conversion.CastToType<int>(p_sSelEntityType, out bSuccess);
                        }
                    }
                    sbSql.Append(" AND ENTITY.ENTITY_TABLE_ID = " + iTableId);
                    sbSql.Append(" ORDER BY ENTITY.ENTITY_ID DESC");
                    if (sbSql != null)
                    {
                        object objTemp = DbFactory.ExecuteScalar(m_sConnectString, sbSql.ToString(), strDictParams);
                        if (objTemp != null)
                        {
                            iEntityID = Conversion.CastToType<int>((objTemp).ToString(), out bSuccess);
                        }
                    }

                    return iEntityID;
                }

                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    if (p_objEntity != null)
                        p_objEntity.Dispose();
                    if (strDictParams != null)
                        strDictParams = null;
                    if (objCache != null)
                        objCache.Dispose();
                    sbSql = null;
                }
            }

            /// <summary>
            /// Gets Coverage details from Staging Tables
            /// </summary>
            /// <param name="p_iPolCvgRowId"></param>
            /// <param name="p_iStagingUnitRowId"></param>
            /// <returns></returns>
            public string GetStagingCoverage(int p_iPolicySystemId,int p_iPolCvgRowId, int p_iStagingUnitRowId)
            {
                XmlDocument xDoc = null;
                XmlNode xRootNode = null;
                DataModelFactory oFactory = null;
                DataModelFactory oFactoryStgngDmf = null;
                PolicyXCvgTypeStgng oPolXCvgTypeStgng = null;
                int iCvgCodeId = 0;
                string sSql = string.Empty;
                try
                {
                    xDoc = new XmlDocument();

                    oFactory = new DataModelFactory(m_oUserLogin, m_iClientId); ;
                    oFactoryStgngDmf = DataModelFactory.CreateDataModelFactory(GetStagingConnectionString(p_iPolicySystemId), m_iClientId);

                    oPolXCvgTypeStgng = oFactoryStgngDmf.GetDataModelObject("PolicyXCvgTypeStgng", false) as PolicyXCvgTypeStgng;
                    xRootNode = xDoc.CreateNode(XmlNodeType.Element, "Coverages", string.Empty);
                    if(p_iPolCvgRowId > 0)
                        oPolXCvgTypeStgng.MoveTo(p_iPolCvgRowId);
                    if (oFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                    {
                        oPolXCvgTypeStgng.iMultiCoverage = 1;
                    }

                    iCvgCodeId = GetRMXCodeIdFromPSMappedCode(oPolXCvgTypeStgng.CoverageTypeCode, "COVERAGE_TYPE", "CoverageType on GetStagingCoverage", p_iPolicySystemId.ToString());
                    //iCvgCodeId = oFactory.Context.LocalCache.GetCodeId(oPolXCvgTypeStgng.CoverageTypeCode, "COVERAGE_TYPE");

                    CreateAndAppendNode("CoverageCd", oPolXCvgTypeStgng.CoverageTypeCode.Trim(), xRootNode, xDoc);
                    if (iCvgCodeId > 0)
                    {
                        CreateAndAppendNode("CvgDescription", oFactory.Context.LocalCache.GetCodeDesc(iCvgCodeId), xRootNode, xDoc);
                    }
                    else
                    {
                        CreateAndAppendNode("CvgDescription", oPolXCvgTypeStgng.CoverageTypeCode, xRootNode, xDoc);
                    }
                    CreateAndAppendNode("csc_CoverageSeq", oPolXCvgTypeStgng.CvgSequenceNo, xRootNode, xDoc);
                    CreateAndAppendNode("csc_TrxnSeq", oPolXCvgTypeStgng.TransSequenceNo, xRootNode, xDoc);
                    CreateAndAppendNode("EffectiveDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.EffectiveDate, "MM/dd/yyyy"), xRootNode, xDoc);
                    CreateAndAppendNode("ExpirationDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.ExpirationDate, "MM/dd/yyyy"), xRootNode, xDoc);
                    CreateAndAppendNode("Limit", oPolXCvgTypeStgng.Limit.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("csc_OriginalPremium", oPolXCvgTypeStgng.OrginialPremium.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("csc_WrittenPremium", oPolXCvgTypeStgng.WrittenPremium.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("csc_TotalPremium", oPolXCvgTypeStgng.TotalWrittenPremium.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("csc_FullTermPremium", oPolXCvgTypeStgng.FullTermPremium.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("csc_Exposure", oPolXCvgTypeStgng.Exposure.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("csc_OccurenceLimit", oPolXCvgTypeStgng.OccurrenceLimit.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("csc_Deductible", oPolXCvgTypeStgng.SelfInsureDeduct.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("csc_ClassCd", oPolXCvgTypeStgng.Class, xRootNode, xDoc);
                    CreateAndAppendNode("csc_Subline", oPolXCvgTypeStgng.SubLine, xRootNode, xDoc);
                    CreateAndAppendNode("CovStatus", oPolXCvgTypeStgng.CvgStatus, xRootNode, xDoc);
                    CreateAndAppendNode("State", oPolXCvgTypeStgng.State, xRootNode, xDoc);
                    CreateAndAppendNode("ExtendDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.Extenddate, "MM/dd/yyyy"), xRootNode, xDoc);
                    CreateAndAppendNode("TrxnDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.TransDate, "MM/dd/yyyy"), xRootNode, xDoc);
                    CreateAndAppendNode("EntryDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.EntryDate, "MM/dd/yyyy"), xRootNode, xDoc);
                    CreateAndAppendNode("RetroDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.RetroDate, "MM/dd/yyyy"), xRootNode, xDoc);
                    CreateAndAppendNode("AcctDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.AccountingDate, "MM/dd/yyyy"), xRootNode, xDoc);
                    CreateAndAppendNode("MRProdLine", oPolXCvgTypeStgng.ProductLine, xRootNode, xDoc);
                    CreateAndAppendNode("AnnualStatement", oPolXCvgTypeStgng.AnnualStmnt.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("LimitCovA", oPolXCvgTypeStgng.LimCovA.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("LimitCovB", oPolXCvgTypeStgng.LimCovB.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("LimitCovC", oPolXCvgTypeStgng.LimCovC.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("LimitCovD", oPolXCvgTypeStgng.LimCovD.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("LimitCovE", oPolXCvgTypeStgng.LimCovE.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("LimitCovF", oPolXCvgTypeStgng.LimCovF.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("DedAmt", oPolXCvgTypeStgng.WCDedAmnt.ToString(), xRootNode, xDoc);
                    CreateAndAppendNode("WcDedAggr", oPolXCvgTypeStgng.WCDedAggr.ToString(), xRootNode, xDoc);

                    sSql = string.Format("SELECT INS_LINE, PRODUCT, STAT_UNIT_NUMBER FROM POINT_UNIT_DATA_STGNG PUD INNER JOIN POLICY_X_UNIT_STGNG PXU ON PXU.UNIT_ID=PUD.UNIT_ID AND PXU.UNIT_TYPE=PUD.UNIT_TYPE WHERE PXU.POLICY_UNIT_ROW_ID={0}", p_iStagingUnitRowId);
                    using (DbReader oReader = oFactoryStgngDmf.Context.DbConnLookup.ExecuteReader(sSql))
                    {
                        if (oReader != null)
                        {
                            if (oReader.Read())
                            {
                                if (oReader["INS_LINE"] != null && oReader["INS_LINE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("InsLine", oReader["INS_LINE"].ToString(), xRootNode, xDoc);
                                }
                                if (oReader["PRODUCT"] != null && oReader["PRODUCT"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("ProdLine", oReader["PRODUCT"].ToString(), xRootNode, xDoc);
                                }
                                if (oReader["STAT_UNIT_NUMBER"] != null && oReader["STAT_UNIT_NUMBER"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("StatUnitNumber", oReader["STAT_UNIT_NUMBER"].ToString(), xRootNode, xDoc);
                                }
                            }
                        }
                    }

                    xDoc.AppendChild(xRootNode);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if (oPolXCvgTypeStgng!=null)
                    {
                        oPolXCvgTypeStgng.Dispose();
                    }
                    if (oFactory!=null)
                    {
                        oFactory.Dispose();
                    }
                }
                return xDoc.OuterXml;
            }
        */
            private void OrphanPolicyCleanUp(int p_iPolicyId)
            {
                string sSQL = string.Empty;

                try
                {
                    //Delete coverages
                    sSQL = "DELETE FROM POLICY_X_CVG_TYPE WHERE POLCVG_ROW_ID IN (SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID IN ( SELECT  POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT WHERE POLICY_ID = " + p_iPolicyId + ") )";
                    DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);

                    //Delete units
                    sSQL = "DELETE FROM POLICY_X_UNIT WHERE POLICY_ID = " + p_iPolicyId;
                    DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);

                    //Delete entities(agent, policy level entities, drivers, unit level entities)
                    sSQL = "DELETE FROM POLICY_X_ENTITY WHERE POLICY_ID = " + p_iPolicyId;
                    DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);

                    //Delete Policy Insured
                    sSQL = "DELETE FROM POLICY_X_INSURED WHERE POLICY_ID = " + p_iPolicyId;
                    DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);

                    // Delete Policy Insurer
                    sSQL = "DELETE FROM POLICY_X_INSURER WHERE POLICY_ID = " + p_iPolicyId;
                    DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);

                }
                catch (Exception p_objExp)
                {
                    throw new RMAppException(Globalization.GetString("PolicyStagingInterface.OrphanPolicyCleanUp.Error", m_iClientId), p_objExp);
                }
            }

            public bool GetPolicyUnitConfigSetting()
            {
                bool bRet = true;
                string sMultiUnitDwnload = string.Empty;
                try
                {
                    System.Collections.Specialized.NameValueCollection oCollection = RMConfigurationManager.GetNameValueSectionSettings("PolicyInterface",m_sConnectString ,m_iClientId);
                        if (oCollection != null && oCollection["MultiUnitDownload"] != null)
                            sMultiUnitDwnload = oCollection["MultiUnitDownload"];
                        if (!string.IsNullOrEmpty(sMultiUnitDwnload) && string.Compare(sMultiUnitDwnload, "false", true) == 0)
                        {
                            bRet = false;
                        }
                        else
                        {
                            bRet = true;
                        }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return bRet;
            }

            public static Dictionary<string,string> StagingCache { get; set; }
            private string GetStagingConnectionString(int p_iPolicySystemId)
            {
                string sStagingConnString = string.Empty;
                string sKey = string.Format("{0}_{1}_{2}", m_sDSNName, m_iClientId, p_iPolicySystemId);
                if (StagingCache == null)
                {
                    StagingCache = new Dictionary<string, string>();
                }
                if (StagingCache.ContainsKey(sKey))
                {
                    sStagingConnString =StagingCache[sKey] ;
                }
                else
                {
                    sStagingConnString = DbFactory.ExecuteAsType<string>(m_sConnectString, "SELECT STAGING_DSN_CONN_STR FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID=" + p_iPolicySystemId);
                    sStagingConnString = RMCryptography.DecryptString(sStagingConnString);
                    StagingCache.Add(sKey, sStagingConnString);
                }

                return sStagingConnString;
            }
			//Policy Staging Changes End
		
		
		 }
		 
}


