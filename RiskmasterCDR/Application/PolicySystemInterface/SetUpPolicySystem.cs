﻿
using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.SecurityManagement;
using System.IO;
using Riskmaster.Security.Encryption;
using System.Collections.Generic;
using System.Web.Hosting;

namespace Riskmaster.Application.PolicySystemInterface
{
    public class SetUpPolicySystem
    {
        private string m_sUserName = string.Empty;
        private string m_sConnectString = string.Empty;
        private const string ROOT_TAG = "<Document></Document>";
        private int m_iPolicySysCurrentPage = 0;
        private int m_iPolicySysPageSize = 0;
        private int m_iClientId = 0;
        public int PolicySysCurrentPage
        {
            get { return m_iPolicySysCurrentPage; }
            set { m_iPolicySysCurrentPage = value; }
        }
        public int PolicySysPageSize
        {
            get { return m_iPolicySysPageSize; }
            set { m_iPolicySysPageSize = value; }
        }

        public SetUpPolicySystem(string p_sLoginName, string p_sConnectionString,int p_iClientId)
        {
            m_sUserName = p_sLoginName;
            m_sConnectString = p_sConnectionString;
            m_iClientId = p_iClientId;
        }

        public XmlNode GetVersionDetails(XmlNode pVersionTemplate)
        {
            
            string sFileNm, sFileContent, sVal,sCodeDesc;
            XmlDocument oTemplate = new XmlDocument();
            XmlDocument oDoc = new XmlDocument();
            XmlNode oTest;

            sVal = pVersionTemplate.Attributes["Type"].Value;
            sVal = sVal.Trim().ToUpper();
            sFileNm = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "PolicyInterface");
            int iCodeId = Conversion.ConvertStrToInteger(sVal);
            LocalCache objCache = new LocalCache(m_sConnectString, m_iClientId);
            sCodeDesc = objCache.GetShortCode(iCodeId);
            switch (sCodeDesc)
            {
                case "POINT":
                    
                    sFileNm = RMConfigurator.CombineFilePath(sFileNm, "POINT_VersionVerifier.xml");
                    sFileContent = File.ReadAllText(sFileNm);
                    if (!string.IsNullOrEmpty(sFileContent))
                    {
                        oTemplate.LoadXml(sFileContent);
                        oDoc = pVersionTemplate.OwnerDocument;
                        
                        foreach(XmlNode oNode in oTemplate.SelectNodes("PolicySystem/POINT"))
                        {
                            oTest = oDoc.CreateElement("Version");
                            oTest.InnerText = oNode.Attributes["version"].Value;
                            pVersionTemplate.AppendChild(oTest);
                            oTest = null;
                            
                        }
                    }
                    break;
                case "INTEGRAL":
                    sFileNm = RMConfigurator.CombineFilePath(sFileNm, "INTEGRAL_VersionVerifier.xml");
                    sFileContent = File.ReadAllText(sFileNm);
                    if (!string.IsNullOrEmpty(sFileContent))
                    {
                        oTemplate.LoadXml(sFileContent);
                        oDoc = pVersionTemplate.OwnerDocument;
                        foreach (XmlNode oNode in oTemplate.SelectNodes("PolicySystem/INTEGRAL"))
                        {
                            oTest = oDoc.CreateElement("Version");
                            oTest.InnerText = oNode.Attributes["version"].Value;
                            pVersionTemplate.AppendChild(oTest);
                            oTest = null;
                        }
                    }
                    break;
            }


            return pVersionTemplate;
                 
        }
        public XmlDocument GetById(XmlDocument p_objInputXMLDoc)
        {
            string sSQL = "";
            DbReader objDbReader = null;
            string sPolicySysRowId = "";
            XmlElement objPolicySysXMLEle = null;
            string sConnectionStr = string.Empty, sDataDSN = string.Empty, sDataUser = string.Empty, sDataPwd = string.Empty, sDataLibraryList = string.Empty, sServerNm = string.Empty, sDriverNm = string.Empty;
            LocalCache objLocalCache = null;

            try
            {
                objPolicySysXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//PolicyInterface/PolicySys");
                sPolicySysRowId = objPolicySysXMLEle.GetElementsByTagName("policysystemid").Item(0).InnerText;
                if (sPolicySysRowId != "")
                {
                    //RUPAL:POLICY INTERFACE CHANGE
                    sSQL = "SELECT POLICY_X_WEB.*,CODES_TEXT.CODE_DESC FROM POLICY_X_WEB INNER JOIN CODES_TEXT ON POLICY_X_WEB.POLICY_SYSTEM_CODE = CODES_TEXT.CODE_ID  WHERE POLICY_SYSTEM_ID = " + sPolicySysRowId;
                    objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);

                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            objPolicySysXMLEle.GetElementsByTagName("PolicySysName").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["POLICY_SYSTEM_NAME"]);
                            objPolicySysXMLEle.GetElementsByTagName("MappingTablePrefix").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["TABLE_PREFIX"]); // aaggarwal29: Code mapping change
                            objPolicySysXMLEle.GetElementsByTagName("Domain").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["DOMAIN"].ToString());
                            objPolicySysXMLEle.GetElementsByTagName("License").Item(0).InnerText = PublicFunctions.Decrypt(Conversion.ConvertObjToStr(objDbReader["LICENSE_KEY"].ToString()));
                            objPolicySysXMLEle.GetElementsByTagName("Proxy").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["PROXY"]);
                            objPolicySysXMLEle.GetElementsByTagName("UserName").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["USERNAME"]);
                            objPolicySysXMLEle.GetElementsByTagName("Password").Item(0).InnerText = PublicFunctions.Decrypt(Conversion.ConvertObjToStr(objDbReader["PASSWORD"]));
                            objPolicySysXMLEle.GetElementsByTagName("URLParam").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["URL_PARAMETER"]);
                            objPolicySysXMLEle.GetElementsByTagName("FinancialUpdate").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["FINANCIAL_UPD_FLAG"]);
                            objPolicySysXMLEle.GetElementsByTagName("DefaultPolicy").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["DEFAULT_POL_FLAG"]);
                            objPolicySysXMLEle.GetElementsByTagName("policysystemid").Item(0).InnerText = sPolicySysRowId;
                            
                            if (objPolicySysXMLEle.GetElementsByTagName("RealTimeUpload").Count > 0) //Payal: RMA:7506
                                objPolicySysXMLEle.GetElementsByTagName("RealTimeUpload").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["REAL_TIME_UPLOAD"]); //Payal:RMA-6233

                            if (objPolicySysXMLEle.GetElementsByTagName("UploadServiceUrl").Count > 0) //Payal: RMA:7506
                                objPolicySysXMLEle.GetElementsByTagName("UploadServiceUrl").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["UPLOAD_SERVICE_URL"]); //Payal:RMA-6233
                            
                            //rupal:start
                            ((XmlElement)(objPolicySysXMLEle.GetElementsByTagName("PolicySystemType").Item(0))).SetAttribute("codeid", Conversion.ConvertObjToStr(objDbReader["POLICY_SYSTEM_CODE"]));
                            objPolicySysXMLEle.GetElementsByTagName("PolicySystemType").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["CODE_DESC"]);
                            //rupal:end
                            objPolicySysXMLEle.GetElementsByTagName("TargetDatabase").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["TARGET_DATABASE"]);
                            objPolicySysXMLEle.GetElementsByTagName("LocCompny").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["LOC_COMPNY"]);
                            objPolicySysXMLEle.GetElementsByTagName("MasterCompany").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["Master_Company"]); 
                            objPolicySysXMLEle.GetElementsByTagName("BusinessEntities").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["BUSINESS_ROLES"]);
                            objPolicySysXMLEle.GetElementsByTagName("MajorPerils").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["EXCLUDE_MAJOR_PERIL"]);
                            objPolicySysXMLEle.GetElementsByTagName("IntertestList").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["EXCLUDE_INTEREST"]);

                            objPolicySysXMLEle.GetElementsByTagName("ClientFileFlag").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["CLIENTFILE_FLAG"]);
                            objPolicySysXMLEle.GetElementsByTagName("UseRMAUserForCFW").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["USE_RMA_USER"]);
                            objPolicySysXMLEle.GetElementsByTagName("CFWUserName").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["CFW_USERNAME"]);
                            objPolicySysXMLEle.GetElementsByTagName("CFWUserPassword").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["CFW_PASSWORD"]);
                            objPolicySysXMLEle.GetElementsByTagName("PointURL").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["POINT_URL"]);
                            objPolicySysXMLEle.GetElementsByTagName("Version").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["VERSION"]);

                            if (objPolicySysXMLEle.GetElementsByTagName("WSSearchURL").Count > 0)
                                objPolicySysXMLEle.GetElementsByTagName("WSSearchURL").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["WS_SEARCH_URL"]);

                            if (objPolicySysXMLEle.GetElementsByTagName("WSInquiryURL").Count > 0)
                                objPolicySysXMLEle.GetElementsByTagName("WSInquiryURL").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["WS_INQUIRY_URL"]);

                            if (objPolicySysXMLEle.GetElementsByTagName("CASServerURL").Count > 0)
                                objPolicySysXMLEle.GetElementsByTagName("CASServerURL").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["CAS_SERVER_URL"]);

                            if (objPolicySysXMLEle.GetElementsByTagName("CASServiceURL").Count > 0)
                                objPolicySysXMLEle.GetElementsByTagName("CASServiceURL").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["CAS_SERVICE_URL"]);

                            XmlNode oNew = objPolicySysXMLEle.SelectSingleNode("Versions");
                            oNew.Attributes["Type"].Value = Conversion.ConvertObjToStr(objDbReader["POLICY_SYSTEM_CODE"]);
                           GetVersionDetails(objPolicySysXMLEle.SelectSingleNode("Versions"));
                          // objPolicySysXMLEle.GetElementsByTagName("ClVehicleUnitMapping").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["CL_VEHICLE_UNIT_MAPPING"]);
                          // objPolicySysXMLEle.GetElementsByTagName("ClPropertyUnitMapping").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["CL_PROPERTY_UNIT_MAPPING"]);
                           
                           objPolicySysXMLEle.GetElementsByTagName("DataDSNConnStr").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["DATA_DSN_CONN_STR"]);
                           objPolicySysXMLEle.GetElementsByTagName("ClientDSNConnStr").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["CLIENT_DSN_CONN_STR"]);
                            //Anu Tennyson : Policy Staging Implementation Starts
                           objPolicySysXMLEle.GetElementsByTagName("StagingConn").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["STAGING_DSN_CONN_STR"]);
                            //Anu Tennyson Ends

                           if (objPolicySysXMLEle.GetElementsByTagName("DSNType").Item(0) != null) // Function called from Add DSN details
                            {
                               if (objPolicySysXMLEle.GetElementsByTagName("DSNType").Item(0).InnerText == "DataDSN")
                                   {
                                     sConnectionStr = RMCryptography.DecryptString(Conversion.ConvertObjToStr(objDbReader["DATA_DSN_CONN_STR"]));
                                     GetConnectionFields(sConnectionStr, ref sServerNm, ref sDataDSN, ref sDataLibraryList, ref sDriverNm, ref sDataUser, ref sDataPwd);
                                   }
                               else if(objPolicySysXMLEle.GetElementsByTagName("DSNType").Item(0).InnerText == "ClientDSN")
                                   {
                                      sConnectionStr = RMCryptography.DecryptString(Conversion.ConvertObjToStr(objDbReader["CLIENT_DSN_CONN_STR"]));
                                      GetConnectionFields(sConnectionStr, ref sServerNm, ref sDataDSN, ref sDataLibraryList, ref sDriverNm, ref sDataUser, ref sDataPwd);
                                   }
                               else if (objPolicySysXMLEle.GetElementsByTagName("DSNType").Item(0).InnerText == "StagingConn")
                               {
                                   sConnectionStr = RMCryptography.DecryptString(Conversion.ConvertObjToStr(objDbReader["STAGING_DSN_CONN_STR"]));
                                   GetConnectionFields(sConnectionStr, ref sServerNm, ref sDataDSN, ref sDataLibraryList, ref sDriverNm, ref sDataUser, ref sDataPwd);
                               }
                                   objPolicySysXMLEle.GetElementsByTagName("DataDriverNm").Item(0).InnerText = sDriverNm;
                                   objPolicySysXMLEle.GetElementsByTagName("DataServerNm").Item(0).InnerText = sServerNm;
                                   objPolicySysXMLEle.GetElementsByTagName("DataLibraryList").Item(0).InnerText = sDataLibraryList;
                                   objPolicySysXMLEle.GetElementsByTagName("DataDSN").Item(0).InnerText = sDataDSN;
                                   objPolicySysXMLEle.GetElementsByTagName("DataDSNUser").Item(0).InnerText = sDataUser;
                                   objPolicySysXMLEle.GetElementsByTagName("DataDSNPwd").Item(0).InnerText = sDataPwd;
                             }
                           //smahajan22 mits 36315 start
                           if (objPolicySysXMLEle.GetElementsByTagName("DefaultClmOffice").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("DefaultClmOffice").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["ADDDEFAULT_CLMOFFICE"]);

                           if (objPolicySysXMLEle.GetElementsByTagName("DefaultExaminer").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("DefaultExaminer").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["ADDDEFAULT_EXAMINER"]);

                           if (objPolicySysXMLEle.GetElementsByTagName("DefaultExaminerCD").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("DefaultExaminerCD").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["ADDDEFAULT_EXAMINERCD"]);

                           if (objPolicySysXMLEle.GetElementsByTagName("DefaultAdjustor").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("DefaultAdjustor").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["ADDDEFAULT_ADJUSTOR"]);

                           if (objPolicySysXMLEle.GetElementsByTagName("EntryInPMSP0000For").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("EntryInPMSP0000For").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["ENTRYINPMSP0000FORAL"]);

                           if (objPolicySysXMLEle.GetElementsByTagName("Adjustor").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("Adjustor").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["DEFAULT_ADJUSTOR"]);

                           if (objPolicySysXMLEle.GetElementsByTagName("ClmOffice").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("ClmOffice").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["DEFAULT_CLMOFFICE"]);

                           if (objPolicySysXMLEle.GetElementsByTagName("Examiner").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("Examiner").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["DEFAULT_EXAMINER"]);

                           if (objPolicySysXMLEle.GetElementsByTagName("ExaminerCD").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("ExaminerCD").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["DEFAULT_EXAMINERCD"]);

                           if (objPolicySysXMLEle.GetElementsByTagName("ExemptClaimId").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("ExemptClaimId").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["EXEMPTCLAIMID"]);

                           if (objPolicySysXMLEle.GetElementsByTagName("UpdatePolicyEntities").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("UpdatePolicyEntities").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["UPLOAD_POLICY_ENTITY_FLAG"]);
                            //smahajan22 mits 36315 end
                           //start - Added by Nikhil.Upload voucher amount setting
                           if (objPolicySysXMLEle.GetElementsByTagName("UploadVoucher").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("UploadVoucher").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["UPLOAD_VOUCHER_DRFTPMSPAP00"]);
                            //End - Added by Nikhil.Upload voucher amount setting
                           //aaggarwal29 :RMA-7701 start
                           if (objPolicySysXMLEle.GetElementsByTagName("Payee2inPMSPAP00").Item(0) != null)
                               objPolicySysXMLEle.GetElementsByTagName("Payee2inPMSPAP00").Item(0).InnerText = Conversion.ConvertObjToStr(objDbReader["UPLOAD_PAYEE2_PMSPAP00"]);
                            //aaggarwal29 :RMA-7701 end

                        }
                    }
                    else
                        throw new RMAppException(Globalization.GetString("SetUpPolicySystem.GetById.NoRecordsFound",m_iClientId));
                }

                return p_objInputXMLDoc;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("SetUpPolicySystem.GetById.Error",m_iClientId), p_objExp);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                objPolicySysXMLEle = null;


            }
        }

        public bool Save(XmlDocument p_objInputXMLDoc)
        {
            string sSQL = "";
            DbConnection objCon = null;
            string sPolicySysId = "";
            bool bIsNew = false;
            XmlElement objPolicySysXMLEle = null;
            string sPolicyName = string.Empty;
            string sDomain = string.Empty;
            string sURLParam = string.Empty;
            string sFinancialUpdate = string.Empty;
            string sDefaultPolicy = string.Empty;

            string sPassword = string.Empty;
            string sLicense = string.Empty;
            string sUserName = string.Empty;
            string sProxy = string.Empty;
            string sTargetDatabase = string.Empty;
            bool bSuccess = false;
            string sPolicyType = string.Empty;
            string sVersion = string.Empty;
            string sClientFileFlag = string.Empty;
            string sUseRMAUserForCFW = string.Empty;
            string sCFWUserName = string.Empty;
            LocalCache objCache = null;
            // aaggarwal29: Code mapping changes start
            bool bIsUnique = false; 
            string sMappingTablePrefix = string.Empty;
            //aaggarwal29: Code mapping changes end

            string sLocCompny = string.Empty;
            string sBusinessEntity = string.Empty;
            string sMajorPeril = string.Empty;
            string sInterestList = string.Empty;
            //string sClVehicleUnitMapping = string.Empty;
            //string sClPropertyUnitMapping = string.Empty;
            string sDatatDSNConnStr = string.Empty, sClientDSNConnStr = string.Empty;
            //Anu Tennyson : Staging Implementation Starts
            string sStagingConn = string.Empty;
            bool bRunStagingscripts = false;
            //Anu Tennyson : Ends
            string sCFWPassword = string.Empty;
            string sPointURL = string.Empty;
			string sMasterCompany = string.Empty; 
            string sReleaseVersion = string.Empty;
            //smahajan22 mits 36315 start 
            string sDefaultClmOffice = string.Empty;
            string sDefaultExaminer  = string.Empty;
            string sDefaultExaminerCD = string.Empty;
            string sDefaultAdjustor  = string.Empty;
            string sEntryInPMSP0000For = string.Empty;
            string sAdjustor = string.Empty;
            string sClmOffice = string.Empty;
            string sExaminer = string.Empty;
            string sExaminerCD = string.Empty;
            string sDefaultAdj = string.Empty;
            string sExemptClaimId = string.Empty;
          //start-   Added by Nikhil.Upload voucher amount setting
            string sUploadVoucher = string.Empty;
            // end -  Added by Nikhil.Upload voucher amount setting

            int iUpdatePolicyEntities = 0;
            //smahajan22 mits 36315 end 
            string sWSSearchURL = string.Empty;
            string sWSInquiryURL = string.Empty;
            string sCASServerURL = string.Empty;
            string sCASServiceURL = string.Empty;
            //Payal:RMA-6233 Starts
            string sRealTimeUpload = string.Empty; 
            StringBuilder sSql = new StringBuilder(); 
            DbParameter objParameter = null; 
            DbCommand objCmd = null; 
            string operation=string.Empty;
            string sUploadServiceUrl = string.Empty;
            //Payal:RMA-6233 Ends

            string sUploadPayee2PMSPAP00 = string.Empty;//aaggarwal29: RMA-7701
            try
            {

                objPolicySysXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//PolicyInterface/PolicySys");

                sPolicySysId = objPolicySysXMLEle.GetElementsByTagName("policysystemid").Item(0).InnerText;


                if (sPolicySysId == "")
                {
                    bIsNew = true;
                    sPolicySysId = Utilities.GetNextUID(m_sConnectString, "POLICY_X_WEB", m_iClientId).ToString();
                }


                sPolicyName = objPolicySysXMLEle.GetElementsByTagName("PolicySysName").Item(0).InnerText;
                sDomain = objPolicySysXMLEle.GetElementsByTagName("Domain").Item(0).InnerText;
                sLicense = PublicFunctions.Encrypt(objPolicySysXMLEle.GetElementsByTagName("License").Item(0).InnerText);
                sProxy = objPolicySysXMLEle.GetElementsByTagName("Proxy").Item(0).InnerText;
                sUserName = objPolicySysXMLEle.GetElementsByTagName("UserName").Item(0).InnerText;
                sPassword = PublicFunctions.Encrypt(objPolicySysXMLEle.GetElementsByTagName("Password").Item(0).InnerText);
                sURLParam = objPolicySysXMLEle.GetElementsByTagName("URLParam").Item(0).InnerText;
                sTargetDatabase = objPolicySysXMLEle.GetElementsByTagName("TargetDatabase").Item(0).InnerText;
                sClientFileFlag = objPolicySysXMLEle.GetElementsByTagName("ClientFileFlag").Item(0).InnerText;
                sVersion = objPolicySysXMLEle.GetElementsByTagName("Version").Item(0).InnerText;
                sLocCompny = objPolicySysXMLEle.GetElementsByTagName("LocCompny").Item(0).InnerText;
                sMasterCompany = objPolicySysXMLEle.GetElementsByTagName("MasterCompany").Item(0).InnerText;
                sInterestList = objPolicySysXMLEle.GetElementsByTagName("IntertestList").Item(0).InnerText;
                sMajorPeril = objPolicySysXMLEle.GetElementsByTagName("MajorPerils").Item(0).InnerText;
                sBusinessEntity = objPolicySysXMLEle.GetElementsByTagName("BusinessEntities").Item(0).InnerText;
                // sPolicyType = objPolicySysXMLEle.GetElementsByTagName("PolicySystemType").Item(0).InnerText;
                objCache = new LocalCache(m_sConnectString,m_iClientId);
                //rupal:start,
                //sPolicyType = objCache.GetCodeId("POINT","POLICY_SYSTEM_TYPE").ToString();
                sPolicyType = ((XmlElement)(objPolicySysXMLEle.GetElementsByTagName("PolicySystemType").Item(0))).GetAttribute("codeid");
                //rupal:end
                sMappingTablePrefix = objPolicySysXMLEle.GetElementsByTagName("MappingTablePrefix").Item(0).InnerText; // aaggarwal29: Code mapping change
                sWSSearchURL = objPolicySysXMLEle.GetElementsByTagName("WSSearchURL").Item(0).InnerText;
                sWSInquiryURL = objPolicySysXMLEle.GetElementsByTagName("WSInquiryURL").Item(0).InnerText;
                sCASServerURL = objPolicySysXMLEle.GetElementsByTagName("CASServerURL").Item(0).InnerText;
                sCASServiceURL = objPolicySysXMLEle.GetElementsByTagName("CASServiceURL").Item(0).InnerText;
                if (string.Equals(objPolicySysXMLEle.GetElementsByTagName("FinancialUpdate").Item(0).InnerText, "true", StringComparison.InvariantCultureIgnoreCase))
                {
                    sFinancialUpdate = "-1";
                }
                else
                {
                    sFinancialUpdate = "0";
                }

                sCFWUserName = objPolicySysXMLEle.GetElementsByTagName("CFWUserName").Item(0).InnerText;
                if (string.Equals(objPolicySysXMLEle.GetElementsByTagName("UseRMAUserForCFW").Item(0).InnerText, "true", StringComparison.InvariantCultureIgnoreCase))
                {
                    sUseRMAUserForCFW = "-1";
                }
                else
                {
                    sUseRMAUserForCFW = "0";
                }


                if (string.Equals(objPolicySysXMLEle.GetElementsByTagName("DefaultPolicy").Item(0).InnerText, "true", StringComparison.InvariantCultureIgnoreCase))
                {
                    sDefaultPolicy = "-1";
                }
                else
                {
                    sDefaultPolicy = "0";
                }

                if (string.IsNullOrEmpty(sDomain))
                {
                    sDomain = "0";
                }
                //Payal:RMA-6233 Starts
                sUploadServiceUrl = objPolicySysXMLEle.GetElementsByTagName("UploadServiceUrl").Item(0).InnerText;
                 
               if (string.Equals(objPolicySysXMLEle.GetElementsByTagName("RealTimeUpload").Item(0).InnerText.Trim(), "-1"))
                {
                    sRealTimeUpload = "-1";
                }
                else
               {
                    sRealTimeUpload = "0";
               }

               //Payal:RMA-6233 ends
               // sClVehicleUnitMapping = objPolicySysXMLEle.GetElementsByTagName("ClVehicleUnitMapping").Item(0).InnerText;
               // sClPropertyUnitMapping = objPolicySysXMLEle.GetElementsByTagName("ClPropertyUnitMapping").Item(0).InnerText;
                sCFWPassword = objPolicySysXMLEle.GetElementsByTagName("CFWUserPassword").Item(0).InnerText;
                sPointURL = objPolicySysXMLEle.GetElementsByTagName("PointURL").Item(0).InnerText;
                sDatatDSNConnStr = objPolicySysXMLEle.GetElementsByTagName("DataDSNConnStr").Item(0).InnerText;
                sClientDSNConnStr = objPolicySysXMLEle.GetElementsByTagName("ClientDSNConnStr").Item(0).InnerText;
                //Anu Tennyson : Staging Implementation Starts
                sStagingConn = objPolicySysXMLEle.GetElementsByTagName("StagingConn").Item(0).InnerText;
                //Anu Tennyson : Ends
                sReleaseVersion = GetVersionDetails(objPolicySysXMLEle.GetElementsByTagName("PolicySystemType").Item(0).Attributes["codeid"].Value, "ReleaseVersion", objPolicySysXMLEle.GetElementsByTagName("Version").Item(0).InnerText);
                //smahajan22 mits 36315 start 
            
                sDefaultClmOffice = objPolicySysXMLEle.GetElementsByTagName("DefaultClmOffice").Item(0).InnerText;
                if (string.IsNullOrEmpty(sDefaultClmOffice))
                {
                    sDefaultClmOffice = "0";
                }
                sDefaultExaminer = objPolicySysXMLEle.GetElementsByTagName("DefaultExaminer").Item(0).InnerText;
                if (string.IsNullOrEmpty(sDefaultExaminer))
                {
                    sDefaultExaminer = "0";
                }
                sDefaultExaminerCD = objPolicySysXMLEle.GetElementsByTagName("DefaultExaminerCD").Item(0).InnerText;
                if (string.IsNullOrEmpty(sDefaultExaminerCD))
                {
                    sDefaultExaminerCD = "0";
                }
                sDefaultAdjustor = objPolicySysXMLEle.GetElementsByTagName("DefaultAdjustor").Item(0).InnerText;
                if (string.IsNullOrEmpty(sDefaultAdjustor))
                {
                    sDefaultAdjustor = "0";
                }
                sEntryInPMSP0000For = objPolicySysXMLEle.GetElementsByTagName("EntryInPMSP0000For").Item(0).InnerText;
                if (string.IsNullOrEmpty(sEntryInPMSP0000For))
                {
                    sEntryInPMSP0000For = "0";
                }
                sAdjustor = objPolicySysXMLEle.GetElementsByTagName("Adjustor").Item(0).InnerText;
                sClmOffice = objPolicySysXMLEle.GetElementsByTagName("ClmOffice").Item(0).InnerText;
                sExaminer = objPolicySysXMLEle.GetElementsByTagName("Examiner").Item(0).InnerText;
                sExaminerCD = objPolicySysXMLEle.GetElementsByTagName("ExaminerCD").Item(0).InnerText;
                //sDefaultAdj = objPolicySysXMLEle.GetElementsByTagName("DefaultAdj").Item(0).InnerText;
                sExemptClaimId = objPolicySysXMLEle.GetElementsByTagName("ExemptClaimId").Item(0).InnerText;
                if (string.IsNullOrEmpty(sExemptClaimId))
                {
                    sExemptClaimId = "0";
                }
                //  start - Added by Nikhil.Upload voucher amount setting
                sUploadVoucher = objPolicySysXMLEle.GetElementsByTagName("UploadVoucher").Item(0).InnerText;
                if (string.IsNullOrEmpty(sUploadVoucher))
                {
                    sUploadVoucher = "0";
                }
                //  end - Added by Nikhil.Upload voucher amount setting
                //aaggarwal29: RMA-7701 start
                sUploadPayee2PMSPAP00 = objPolicySysXMLEle.GetElementsByTagName("Payee2inPMSPAP00").Item(0).InnerText;
                if (string.IsNullOrEmpty(sUploadPayee2PMSPAP00))
                {
                    sUploadPayee2PMSPAP00 = "0";
                }
                //aaggarwal29: RMA-7701 end
              //dbisht6 mits 36536
                if (string.Equals(sClientFileFlag.Trim(),"1") )
                {
                    if (string.Equals(objPolicySysXMLEle.GetElementsByTagName("UpdatePolicyEntities").Item(0).InnerText, "true", StringComparison.InvariantCultureIgnoreCase))
                    {

                        iUpdatePolicyEntities = -1;
                    }
                    else
                    {
                        iUpdatePolicyEntities = 0;
                    }
                }
                if (string.Equals(sClientFileFlag.Trim(),"0"))
                {
                    iUpdatePolicyEntities = -1;                    
                }
                //mits 36536 end
                //smahajan22 mits 36315 end
                if (Conversion.CastToType<bool>(objPolicySysXMLEle.GetElementsByTagName("DefaultPolicy").Item(0).InnerText, out bSuccess))
                {
                    sSQL = "UPDATE POLICY_X_WEB SET DEFAULT_POL_FLAG =0 WHERE DEFAULT_POL_FLAG = -1";
                    objCon = DbFactory.GetDbConnection(m_sConnectString);
                    objCon.Open();
                    objCon.ExecuteNonQuery(sSQL);
                    objCon.Close();

                }
 
                //tanwar2 - Policy staging - start
                if (bIsNew && !string.IsNullOrWhiteSpace(sStagingConn))
                {
                    LocalCache oCache = new LocalCache(m_sConnectString, m_iClientId);
                    if (string.Compare(oCache.GetCodeId("STAGING", "POLICY_SYSTEM_TYPE").ToString(), sPolicyType) == 0)
                    {
                        bRunStagingscripts = true;
                    }
                    oCache.Dispose();
                }
                //tanwar2 - Policy Staging - end
                

                if (bIsNew)
                {
     				// aaggarwal29: Code mapping changes start
                    bIsUnique = IsUniqueTablePrefix(sMappingTablePrefix);
                    if (bIsUnique)
                    {
                        if (sCFWPassword == "##########")
                        {
                            sCFWPassword = string.Empty;
                        }
                        
                        //Payal:RMA-6233 Starts
                        /*
                        sSQL = "INSERT INTO POLICY_X_WEB(POLICY_SYSTEM_ID,POLICY_SYSTEM_NAME,DOMAIN,LICENSE_KEY,PROXY,USERNAME,PASSWORD,URL_PARAMETER,DEFAULT_POL_FLAG,FINANCIAL_UPD_FLAG,"+
                                 "POLICY_SYSTEM_CODE,TARGET_DATABASE,VERSION, CLIENTFILE_FLAG,USE_RMA_USER,CFW_USERNAME, TABLE_PREFIX, LOC_COMPNY,EXCLUDE_MAJOR_PERIL,EXCLUDE_INTEREST,BUSINESS_ROLES,Master_Company,CFW_PASSWORD,POINT_URL, DATA_DSN_CONN_STR, CLIENT_DSN_CONN_STR, RELEASE_VERSION," +
                            //Start - Changed by Nikhil.Upload voucher amount setting
                       //"WS_SEARCH_URL,	WS_INQUIRY_URL,	CAS_SERVER_URL,	CAS_SERVICE_URL, ADDDEFAULT_CLMOFFICE,ADDDEFAULT_EXAMINER,ADDDEFAULT_EXAMINERCD,ADDDEFAULT_ADJUSTOR,ENTRYINPMSP0000FORAL,DEFAULT_ADJUSTOR,DEFAULT_CLMOFFICE,DEFAULT_EXAMINER,DEFAULT_EXAMINERCD,EXEMPTCLAIMID,UPLOAD_POLICY_ENTITY_FLAG)" +//smahajan22 mits 36315 
                          "WS_SEARCH_URL,	WS_INQUIRY_URL,	CAS_SERVER_URL,	CAS_SERVICE_URL, ADDDEFAULT_CLMOFFICE,ADDDEFAULT_EXAMINER,ADDDEFAULT_EXAMINERCD,ADDDEFAULT_ADJUSTOR,ENTRYINPMSP0000FORAL,DEFAULT_ADJUSTOR,DEFAULT_CLMOFFICE,DEFAULT_EXAMINER,DEFAULT_EXAMINERCD,EXEMPTCLAIMID,UPLOAD_POLICY_ENTITY_FLAG,UPLOAD_VOUCHER_DRFTPMSPAP00)" +//smahajan22 mits 36315 
                 "VALUES(" + sPolicySysId + ",'" + sPolicyName + "','" + sDomain + "'," +
                                "'" + sLicense + "','" + sProxy + "','" +
                                sUserName + "','" + sPassword + "','" + sURLParam + "'," + sDefaultPolicy + "," + sFinancialUpdate + "," + sPolicyType +
                                ",'" + sTargetDatabase + "','" + sVersion + "'," + sClientFileFlag + "," + sUseRMAUserForCFW + ",'" + sCFWUserName + "','"+

                                sMappingTablePrefix + "','" + sLocCompny + "','" + sMajorPeril + "','" + sInterestList + "','" + sBusinessEntity + "','" + sMasterCompany + "','" + sCFWPassword + "','" + sPointURL + "','" + sDatatDSNConnStr + "','" + sClientDSNConnStr + "','" + sReleaseVersion + "','" +
                            //Start -  Changed by Nikhil.Upload voucher amount setting
                      //sWSSearchURL + "','" + sWSInquiryURL + "','" + sCASServerURL + "','" + sCASServiceURL + "'," + sDefaultClmOffice + "," + sDefaultExaminer + "," + sDefaultExaminerCD + "," + sDefaultAdjustor + "," + sEntryInPMSP0000For + ",'" + sAdjustor + "','" + sClmOffice + "','" + sExaminer + "','" + sExaminerCD + "'," + sExemptClaimId +","+ iUpdatePolicyEntities + ")";//smahajan22 mits 36315 //sanoopsharma - MITS 35932 added Integral fields
                     sWSSearchURL + "','" + sWSInquiryURL + "','" + sCASServerURL + "','" + sCASServiceURL + "'," + sDefaultClmOffice + "," + sDefaultExaminer + "," + sDefaultExaminerCD + "," + sDefaultAdjustor + "," + sEntryInPMSP0000For + ",'" + sAdjustor + "','" + sClmOffice + "','" + sExaminer + "','" + sExaminerCD + "'," + sExemptClaimId +","+ iUpdatePolicyEntities + "," + sUploadVoucher + ")";//smahajan22 mits 36315 //sanoopsharma - MITS 35932 added Integral fields
                    
                    
                   //End - Changed by Nikhil.Upload voucher amount setting
                         * */

                            operation = "INSERT"; 
                            sSql.Append("INSERT INTO POLICY_X_WEB(POLICY_SYSTEM_ID,POLICY_SYSTEM_NAME,DOMAIN,LICENSE_KEY,PROXY,USERNAME,PASSWORD,URL_PARAMETER,DEFAULT_POL_FLAG,FINANCIAL_UPD_FLAG,");
                            sSql.Append("POLICY_SYSTEM_CODE,TARGET_DATABASE,VERSION, CLIENTFILE_FLAG,USE_RMA_USER,CFW_USERNAME, TABLE_PREFIX, LOC_COMPNY,EXCLUDE_MAJOR_PERIL,EXCLUDE_INTEREST,BUSINESS_ROLES,Master_Company,CFW_PASSWORD,POINT_URL, DATA_DSN_CONN_STR, CLIENT_DSN_CONN_STR, STAGING_DSN_CONN_STR, RELEASE_VERSION,");
                            sSql.Append("WS_SEARCH_URL,WS_INQUIRY_URL,CAS_SERVER_URL,CAS_SERVICE_URL,ADDDEFAULT_CLMOFFICE,ADDDEFAULT_EXAMINER,ADDDEFAULT_EXAMINERCD,ADDDEFAULT_ADJUSTOR,ENTRYINPMSP0000FORAL,DEFAULT_ADJUSTOR,DEFAULT_CLMOFFICE,DEFAULT_EXAMINER,DEFAULT_EXAMINERCD,EXEMPTCLAIMID,UPLOAD_POLICY_ENTITY_FLAG,UPLOAD_VOUCHER_DRFTPMSPAP00,REAL_TIME_UPLOAD,UPLOAD_SERVICE_URL,UPLOAD_PAYEE2_PMSPAP00)");
                            sSql.Append(" VALUES( ~PolicySysId~, ");
                            sSql.Append("~PolicyName~, ");
                            sSql.Append("~Domain~, ");
                            sSql.Append("~License~, ");
                            sSql.Append("~Proxy~, ");
                            sSql.Append("~UserName~, ");
                            sSql.Append("~Password~, ");
                            sSql.Append("~URLParam~, ");
                            sSql.Append("~DefaultPolicy~, ");
                            sSql.Append("~FinancialUpdate~, ");
                            sSql.Append("~PolicyType~, ");
                            sSql.Append("~TargetDatabase~, ");
                            sSql.Append("~Version~, ");
                            sSql.Append("~ClientFileFlag~, ");
                            sSql.Append("~UseRMAUserForCFW~, ");
                            sSql.Append("~CFWUserName~, ");
                            sSql.Append("~MappingTablePrefix~, ");
                            sSql.Append("~LocCompny~, ");
                            sSql.Append("~MajorPeril~, ");
                            sSql.Append("~InterestList~, ");
                            sSql.Append("~BusinessEntity~, ");
                            sSql.Append("~MasterCompany~, ");
                            sSql.Append("~CFWPassword~, ");
                            sSql.Append("~PointURL~, ");
                            sSql.Append("~DatatDSNConnStr~, ");
                            sSql.Append("~ClientDSNConnStr~, ");
                        //Anu Tennyson : Policy Staging Implementation Starts
                            sSql.Append("~StagingConnStr~, ");
                        //Anu Tennyson : Ends
                            sSql.Append("~ReleaseVersion~, ");
                            sSql.Append("~WSSearchURL~, ");
                            sSql.Append("~WSInquiryURL~, ");
                            sSql.Append("~CASServerURL~, ");
                            sSql.Append("~CASServiceURL~, ");
                            sSql.Append("~DefaultClmOffice~, ");
                            sSql.Append("~DefaultExaminer~, ");
                            sSql.Append("~DefaultExaminerCD~, ");
                            sSql.Append("~DefaultAdjustor~, ");
                            sSql.Append("~EntryInPMSP0000For~, ");
                            sSql.Append("~Adjustor~, ");
                            sSql.Append("~ClmOffice~, ");
                            sSql.Append("~Examiner~, ");
                            sSql.Append("~ExaminerCD~, ");
                            sSql.Append("~ExemptClaimId~, ");
                            sSql.Append("~UpdatePolicyEntities~, ");
                            sSql.Append("~UploadVoucher~, ");
                            sSql.Append("~RealTimeUpload~, ");
                          //sSql.Append("~UploadServiceUrl~, ) "); // rkaur27: Correction for JIRA:7701 
                            sSql.Append("~UploadServiceUrl~, ");
                            sSql.Append("~UploadPayee2PMSPAP00~ ) ");

                        //Payal:RMA-6233 Ends
                    }
                    else
                    {
                        
                        throw new RMAppException(Globalization.GetString("SetUpPolicySystem.Save.NotAUniqueName",m_iClientId));
                    }
				//aaggarwal29: Code mapping changes end
                }
                else
                {

                  //Payal:RMA-6233 Starts
                        /*
                    sSQL = "UPDATE POLICY_X_WEB SET " +
                            "POLICY_SYSTEM_NAME='" + sPolicyName + "'," +            
                            "DOMAIN='" + sDomain + "'," + "LICENSE_KEY='" + sLicense + "'," + "PROXY='" + sProxy + "'," +
                            "URL_PARAMETER='" + sURLParam + "'," + "USERNAME='" + sUserName + "'," +
                            "FINANCIAL_UPD_FLAG=" + sFinancialUpdate + ", DEFAULT_POL_FLAG=" + sDefaultPolicy + " ,POLICY_SYSTEM_CODE=" + sPolicyType + " ,TARGET_DATABASE='" +
                            sTargetDatabase + "',VERSION='" + sVersion + "',CLIENTFILE_FLAG=" + sClientFileFlag + ",USE_RMA_USER=" + sUseRMAUserForCFW + ",CFW_USERNAME='"+
                            //sCFWUserName + "', LOC_COMPNY='" + sLocCompny + "',EXCLUDE_MAJOR_PERIL='" + sMajorPeril + "' , EXCLUDE_INTEREST='" + sInterestList + "' ,BUSINESS_ROLES='" + sBusinessEntity + "' ,Master_Company='" + sMasterCompany + "'  WHERE POLICY_SYSTEM_ID=" + sPolicySysId;
                            sCFWUserName + "', LOC_COMPNY='" + sLocCompny + "',EXCLUDE_MAJOR_PERIL='" + sMajorPeril + "' , EXCLUDE_INTEREST='" + sInterestList + "' ,BUSINESS_ROLES='" + sBusinessEntity + "' ,Master_Company='" + sMasterCompany + "' ,POINT_URL='" + sPointURL + "', " +
                            " DATA_DSN_CONN_STR = '" + sDatatDSNConnStr + "', CLIENT_DSN_CONN_STR = '" + sClientDSNConnStr + "', RELEASE_VERSION = '" + sReleaseVersion 
						   + "', WS_SEARCH_URL = '" + sWSSearchURL + "', WS_INQUIRY_URL = '" + sWSInquiryURL
                            + "', CAS_SERVER_URL = '" + sCASServerURL + "', CAS_SERVICE_URL = '" + sCASServiceURL  //sanoopsharma - MITS 35932  added Integral fields
                           + "' ,ADDDEFAULT_CLMOFFICE=" + sDefaultClmOffice + ",ADDDEFAULT_EXAMINER=" + sDefaultExaminer + ",ADDDEFAULT_EXAMINERCD=" + sDefaultExaminerCD + //smahajan22 mits 36315 
                            ",ADDDEFAULT_ADJUSTOR=" + sDefaultAdjustor + ",ENTRYINPMSP0000FORAL=" + sEntryInPMSP0000For + ",DEFAULT_ADJUSTOR='" + sAdjustor + "'," + "DEFAULT_CLMOFFICE='" + sClmOffice + "'," + "DEFAULT_EXAMINER='" + sExaminer + "'," + "DEFAULT_EXAMINERCD='" + sExaminerCD + "'," + "EXEMPTCLAIMID='" + sExemptClaimId + "' , UPLOAD_POLICY_ENTITY_FLAG='" + iUpdatePolicyEntities.ToString();//smahajan22 mits 36315
                            if (sCFWPassword != "##########")
                              {
                                  sSQL = sSQL + "' ,CFW_PASSWORD='" + sCFWPassword;
                              }
                            //Start - added by Nikhil.Upload voucher amount setting
                            sSQL = sSQL + "' ,UPLOAD_VOUCHER_DRFTPMSPAP00='" + sUploadVoucher;
                            //End - added by Nikhil.Upload voucher amount setting
                            sSQL = sSQL  + "'   WHERE POLICY_SYSTEM_ID=" + sPolicySysId;
                    */
                            operation = "UPDATE";
                            sSql.Append("UPDATE POLICY_X_WEB SET POLICY_SYSTEM_NAME= ~PolicyName~, ");
                            sSql.Append("DOMAIN= ~Domain~, ");
                            sSql.Append("LICENSE_KEY= ~License~, ");
                            sSql.Append("PROXY= ~Proxy~, ");
                            sSql.Append("USERNAME= ~UserName~, ");
                            sSql.Append("URL_PARAMETER= ~URLParam~, ");
                            sSql.Append("DEFAULT_POL_FLAG= ~DefaultPolicy~, ");
                            sSql.Append("FINANCIAL_UPD_FLAG= ~FinancialUpdate~, ");
                            sSql.Append("POLICY_SYSTEM_CODE= ~PolicyType~, ");
                            sSql.Append("TARGET_DATABASE= ~TargetDatabase~, ");
                            sSql.Append("VERSION= ~Version~, ");
                            sSql.Append("CLIENTFILE_FLAG = ~ClientFileFlag~, ");
                            sSql.Append("USE_RMA_USER= ~UseRMAUserForCFW~, ");
                            sSql.Append("CFW_USERNAME= ~CFWUserName~, ");
                            sSql.Append("LOC_COMPNY= ~LocCompny~, ");
                            sSql.Append("EXCLUDE_MAJOR_PERIL= ~MajorPeril~, ");
                            sSql.Append("EXCLUDE_INTEREST= ~InterestList~, ");
                            sSql.Append("BUSINESS_ROLES= ~BusinessEntity~, ");
                            sSql.Append("Master_Company= ~MasterCompany~, ");
                            if (sCFWPassword != "##########")
                            {
                                sSql.Append("CFW_PASSWORD= ~CFWPassword~, ");
                            
                            }
                            sSql.Append("POINT_URL= ~PointURL~, ");
                            sSql.Append("DATA_DSN_CONN_STR= ~DatatDSNConnStr~, ");
                            sSql.Append("CLIENT_DSN_CONN_STR= ~ClientDSNConnStr~, ");
                    //Anu Tennyson : Policy Interface Implementation Starts
                            sSql.Append("STAGING_DSN_CONN_STR= ~StagingConnStr~, ");
                    //Anu Tennyson : Ends
                            sSql.Append("RELEASE_VERSION= ~ReleaseVersion~, ");
                            sSql.Append("WS_SEARCH_URL= ~WSSearchURL~, ");
                            sSql.Append("WS_INQUIRY_URL= ~WSInquiryURL~, ");
                            sSql.Append("CAS_SERVER_URL= ~CASServerURL~, ");
                            sSql.Append("CAS_SERVICE_URL= ~CASServiceURL~, ");
                            sSql.Append("ADDDEFAULT_CLMOFFICE= ~DefaultClmOffice~, ");
                            sSql.Append("ADDDEFAULT_EXAMINER= ~DefaultExaminer~, ");
                            sSql.Append("ADDDEFAULT_EXAMINERCD= ~DefaultExaminerCD~, ");
                            sSql.Append("ADDDEFAULT_ADJUSTOR= ~DefaultAdjustor~, ");
                            sSql.Append("ENTRYINPMSP0000FORAL= ~EntryInPMSP0000For~, ");
                            sSql.Append("DEFAULT_ADJUSTOR= ~Adjustor~, ");
                            sSql.Append("DEFAULT_CLMOFFICE= ~ClmOffice~, ");
                            sSql.Append("DEFAULT_EXAMINER= ~Examiner~, ");
                            sSql.Append("DEFAULT_EXAMINERCD= ~ExaminerCD~, ");
                            sSql.Append("EXEMPTCLAIMID= ~ExemptClaimId~, ");
                            sSql.Append("UPLOAD_POLICY_ENTITY_FLAG= ~UpdatePolicyEntities~, ");
                            sSql.Append("UPLOAD_VOUCHER_DRFTPMSPAP00= ~UploadVoucher~, ");
                            sSql.Append("REAL_TIME_UPLOAD= ~RealTimeUpload~, ");
                            sSql.Append("UPLOAD_SERVICE_URL= ~UploadServiceUrl~, ");
                            sSql.Append("UPLOAD_PAYEE2_PMSPAP00= ~UploadPayee2PMSPAP00~ ");
                            sSql.Append(" WHERE POLICY_SYSTEM_ID=~PolicySysId~ ");
                           

                }

                objCon = DbFactory.GetDbConnection(m_sConnectString);
                objCon.Open();
               // objCon.ExecuteNonQuery(sSQL);
                 objCmd = objCon.CreateCommand();
                 if (operation == "INSERT")
                 {
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "PolicySysId";
                     objParameter.Value = sPolicySysId;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "PolicyName";
                     objParameter.Value = sPolicyName;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "Domain";
                     objParameter.Value = sDomain;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "License";
                     objParameter.Value = sLicense;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "Proxy";
                     objParameter.Value = sProxy;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "UserName";
                     objParameter.Value = sUserName;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "Password";
                     objParameter.Value = sPassword;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "URLParam";
                     objParameter.Value = sURLParam;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "DefaultPolicy";
                     objParameter.Value = sDefaultPolicy;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "FinancialUpdate";
                     objParameter.Value = sFinancialUpdate;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "PolicyType";
                     objParameter.Value = sPolicyType;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "TargetDatabase";
                     objParameter.Value = sTargetDatabase;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "Version";
                     objParameter.Value = sVersion;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "ClientFileFlag";
                     objParameter.Value = sClientFileFlag;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "UseRMAUserForCFW";
                     objParameter.Value = sUseRMAUserForCFW;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "CFWUserName";
                     objParameter.Value = sCFWUserName;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "MappingTablePrefix";
                     objParameter.Value = sMappingTablePrefix;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "LocCompny";
                     objParameter.Value = sLocCompny;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "MajorPeril";
                     objParameter.Value = sMajorPeril;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "InterestList";
                     objParameter.Value = sInterestList;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "BusinessEntity";
                     objParameter.Value = sBusinessEntity;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "MasterCompany";
                     objParameter.Value = sMasterCompany;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "CFWPassword";
                     objParameter.Value = sCFWPassword;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "PointURL";
                     objParameter.Value = sPointURL;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "DatatDSNConnStr";
                     objParameter.Value = sDatatDSNConnStr;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "ClientDSNConnStr";
                     objParameter.Value = sClientDSNConnStr;
                     objCmd.Parameters.Add(objParameter);
                     //Anu Tennyson : Policy Staging Implementation Starts
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "StagingConnStr";
                     objParameter.Value = sStagingConn;
                     objCmd.Parameters.Add(objParameter);
                     //Anu Tennyson : Ends
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "ReleaseVersion";
                     objParameter.Value = sReleaseVersion;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "WSSearchURL";
                     objParameter.Value = sWSSearchURL;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "WSInquiryURL";
                     objParameter.Value = sWSInquiryURL;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "CASServerURL";
                     objParameter.Value = sCASServerURL;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "CASServiceURL";
                     objParameter.Value = sCASServiceURL;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "DefaultClmOffice";
                     objParameter.Value = sDefaultClmOffice;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "DefaultExaminer";
                     objParameter.Value = sDefaultExaminer;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "DefaultExaminerCD";
                     objParameter.Value = sDefaultExaminerCD;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "DefaultAdjustor";
                     objParameter.Value = sDefaultAdjustor;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "EntryInPMSP0000For";
                     objParameter.Value = sEntryInPMSP0000For;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "Adjustor";
                     objParameter.Value = sAdjustor;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "ClmOffice";
                     objParameter.Value = sClmOffice;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "Examiner";
                     objParameter.Value = sExaminer;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "ExaminerCD";
                     objParameter.Value = sExaminerCD;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "ExemptClaimId";
                     objParameter.Value = sExemptClaimId;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "UpdatePolicyEntities";
                     objParameter.Value = iUpdatePolicyEntities;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "UploadVoucher";
                     objParameter.Value = sUploadVoucher;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "RealTimeUpload";
                     objParameter.Value = sRealTimeUpload;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "UploadServiceUrl";
                     objParameter.Value = sUploadServiceUrl;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "UploadPayee2PMSPAP00";
                     objParameter.Value = sUploadPayee2PMSPAP00;
                     objCmd.Parameters.Add(objParameter);
                 }


                 if (operation == "UPDATE")
                 {


                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "PolicyName";
                     objParameter.Value = sPolicyName;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "Domain";
                     objParameter.Value = sDomain;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "License";
                     objParameter.Value = sLicense;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "Proxy";
                     objParameter.Value = sProxy;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "UserName";
                     objParameter.Value = sUserName;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "URLParam";
                     objParameter.Value = sURLParam;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "DefaultPolicy";
                     objParameter.Value = sDefaultPolicy;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "FinancialUpdate";
                     objParameter.Value = sFinancialUpdate;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "PolicyType";
                     objParameter.Value = sPolicyType;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "TargetDatabase";
                     objParameter.Value = sTargetDatabase;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "Version";
                     objParameter.Value = sVersion;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "ClientFileFlag";
                     objParameter.Value = sClientFileFlag;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "UseRMAUserForCFW";
                     objParameter.Value = sUseRMAUserForCFW;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "CFWUserName";
                     objParameter.Value = sCFWUserName;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "LocCompny";
                     objParameter.Value = sLocCompny;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "MajorPeril";
                     objParameter.Value = sMajorPeril;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "InterestList";
                     objParameter.Value = sInterestList;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "BusinessEntity";
                     objParameter.Value = sBusinessEntity;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "MasterCompany";
                     objParameter.Value = sMasterCompany;
                     objCmd.Parameters.Add(objParameter);
                     if (sCFWPassword != "##########")
                     {
                         objParameter = objCmd.CreateParameter();
                         objParameter.ParameterName = "CFWPassword";
                         objParameter.Value = sCFWPassword;
                         objCmd.Parameters.Add(objParameter);
                     }
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "PointURL";
                     objParameter.Value = sPointURL;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "DatatDSNConnStr";
                     objParameter.Value = sDatatDSNConnStr;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "ClientDSNConnStr";
                     objParameter.Value = sClientDSNConnStr;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     //Anu Tennyson : Policy Staging Implementation Starts
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "StagingConnStr";
                     objParameter.Value = sStagingConn;
                     objCmd.Parameters.Add(objParameter);
                     //Anu Tennyson : Ends
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "ReleaseVersion";
                     objParameter.Value = sReleaseVersion;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "WSSearchURL";
                     objParameter.Value = sWSSearchURL;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "WSInquiryURL";
                     objParameter.Value = sWSInquiryURL;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "CASServerURL";
                     objParameter.Value = sCASServerURL;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "CASServiceURL";
                     objParameter.Value = sCASServiceURL;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "DefaultClmOffice";
                     objParameter.Value = sDefaultClmOffice;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "DefaultExaminer";
                     objParameter.Value = sDefaultExaminer;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "DefaultExaminerCD";
                     objParameter.Value = sDefaultExaminerCD;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "DefaultAdjustor";
                     objParameter.Value = sDefaultAdjustor;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "EntryInPMSP0000For";
                     objParameter.Value = sEntryInPMSP0000For;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "Adjustor";
                     objParameter.Value = sAdjustor;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "ClmOffice";
                     objParameter.Value = sClmOffice;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "Examiner";
                     objParameter.Value = sExaminer;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "ExaminerCD";
                     objParameter.Value = sExaminerCD;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "ExemptClaimId";
                     objParameter.Value = sExemptClaimId;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "UpdatePolicyEntities";
                     objParameter.Value = iUpdatePolicyEntities;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "UploadVoucher";
                     objParameter.Value = sUploadVoucher;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "RealTimeUpload";
                     objParameter.Value = sRealTimeUpload;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "UploadServiceUrl";
                     objParameter.Value = sUploadServiceUrl;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "UploadPayee2PMSPAP00";
                     objParameter.Value = sUploadPayee2PMSPAP00;
                     objCmd.Parameters.Add(objParameter);
                     objParameter = objCmd.CreateParameter();
                     objParameter.ParameterName = "PolicySysId";
                     objParameter.Value = sPolicySysId;
                     objCmd.Parameters.Add(objParameter);



                 }
                
               
                objCmd.CommandText = sSql.ToString();
                objCmd.ExecuteNonQuery();


                //Payal:RMA-6233 Ends
                //rupal:start

                if (bIsNew)     // aaggarwal29: Code mapping changes start
                {
                    bool bOut = false;
                    InsertPSMapTablesIntoGlossary(Conversion.CastToType<int>(sPolicyType, out bOut), sMappingTablePrefix);                   
               }               //aaggarwal29: Code mapping changes end
                //rupal:end

                //RUPAL:MITS 33290
                CreateDBViewsForPointClaimBalancing(sPolicyType);
                //RUPAL:MITS 33290

                return true;

            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("SetUpPolicySystem.Save.Error",m_iClientId), p_objExp);
            }
            finally
            {
                if (objCon != null)
                    objCon.Dispose();

                if (objPolicySysXMLEle != null)
                    objPolicySysXMLEle = null;
                if(objCache!=null)
                    objCache.Dispose();
                //Payal:RMA-6233 Starts
                if (sSql != null)
                    sSql = null;
                if (objParameter != null)
                    objParameter = null;
                if (objCmd != null)
                    objCmd = null;
                //Payal:RMA-6233 Ends
            }
        }

        //rupal:start,mits 33290
        private void  CreateDBViewsForPointClaimBalancing(string sPolicyType)
        {
            
            string sSQL = string.Empty;
            bool bCreateViews = false;
            StringBuilder sb = null;
            try
            {
                sSQL = "SELECT FINANCIAL_UPD_FLAG FROM POLICY_X_WEB WHERE POLICY_SYSTEM_CODE='" + sPolicyType + "' AND FINANCIAL_UPD_FLAG=-1";
                
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    if (objReader.Read())
                    {
                        bCreateViews = true;
                    }
                }
                if (bCreateViews)
                {

                    DropDBViews();

                    sb = new StringBuilder();
                    sb.Append("CREATE VIEW RMA_HOLD_RESCUR_V AS  ");
                    sb.Append("SELECT RESERVE_CURRENT.CLAIM_ID,RESERVE_CURRENT.RC_ROW_ID,RESERVE_CURRENT.RESERVE_AMOUNT,RESERVE_CURRENT.DTTM_RCD_LAST_UPD,  ");
                    sb.Append("RESERVE_CURRENT.POLCVG_LOSS_ROW_ID ");
                    sb.Append("FROM RESERVE_CURRENT ");
                    //sb.Append("INNER JOIN CODES ON RESERVE_CURRENT.RES_STATUS_CODE=CODES.CODE_ID AND CODES.SHORT_CODE='H' ");
                    //reserve status should be compared with parent reserve status
                    sb.Append("INNER JOIN CODES ON RESERVE_CURRENT.RES_STATUS_CODE=CODES.CODE_ID ");
                    sb.Append("INNER JOIN CODES CODES_RELATED ON CODES.RELATED_CODE_ID=CODES_RELATED.CODE_ID AND CODES_RELATED.SHORT_CODE='H'");
                    sb.Append("INNER JOIN GLOSSARY ON GLOSSARY.TABLE_ID=CODES_RELATED.TABLE_ID AND GLOSSARY.SYSTEM_TABLE_NAME='RESERVE_STATUS_PARENT' ");
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW PENDUP_RESHIS_V AS  ");
                    sb.Append("SELECT ACTIVITY_TRACK.ACTIVITY_ROW_ID,ACTIVITY_TRACK.CLAIM_ID,RESERVE_CURRENT.RC_ROW_ID,ACTIVITY_TRACK.CHANGE_AMOUNT, RESERVE_CURRENT.POLCVG_LOSS_ROW_ID,RESERVE_CURRENT.DTTM_RCD_LAST_UPD ");
                    sb.Append("FROM ACTIVITY_TRACK INNER JOIN RESERVE_CURRENT  ");
                    sb.Append("ON ACTIVITY_TRACK.CLAIM_ID=RESERVE_CURRENT.CLAIM_ID AND ACTIVITY_TRACK.FOREIGN_TABLE_ID=74  ");
                    sb.Append("AND ACTIVITY_TRACK.FOREIGN_TABLE_KEY=RESERVE_CURRENT.RC_ROW_ID ");
                    sb.Append("AND ACTIVITY_TRACK.UPLOAD_FLAG=-1 ");
                    //sb.Append("inner join RESERVE_HISTORY on RESERVE_CURRENT.CLAIM_ID=RESERVE_HISTORY.CLAIM_ID AND RESERVE_CURRENT.CLAIMANT_EID=RESERVE_HISTORY.CLAIMANT_EID ");
                    //sb.Append("AND RESERVE_CURRENT.RESERVE_TYPE_CODE=RESERVE_HISTORY.RESERVE_TYPE_CODE AND RESERVE_CURRENT.POLCVG_ROW_ID=RESERVE_HISTORY.POLCVG_ROW_ID ");
                    //sb.Append("AND RESERVE_CURRENT.POLCVG_LOSS_ROW_ID=RESERVE_HISTORY.POLCVG_LOSS_ROW_ID AND RESERVE_CURRENT.POLICY_CVG_SEQNO=RESERVE_HISTORY.POLICY_CVG_SEQNO ");
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW BAL_RMA_PEND_RESERVE_V AS  ");
                    sb.Append("SELECT RC_ROW_ID,CLAIM_ID,POLCVG_LOSS_ROW_ID, SUM(CHANGE_AMOUNT) AS PEND_RESERVE, MIN(DTTM_RCD_LAST_UPD) AS LAST_TRAN_DATE    ");
                    sb.Append("FROM  PENDUP_RESHIS_V    ");
                    sb.Append("GROUP BY RC_ROW_ID, CLAIM_ID,POLCVG_LOSS_ROW_ID   ");
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW PENDUP_PAY_V AS  ");
                    sb.Append("SELECT FUNDS_TRANS_SPLIT.RC_ROW_ID,FUNDS.TRANS_ID,FUNDS_TRANS_SPLIT.DTTM_RCD_LAST_UPD,ACTIVITY_TRACK.ACTIVITY_TYPE AS TRIGGER_TYPE,  ");
                    sb.Append("ACTIVITY_TRACK.CLAIM_ID,  ");
                    //sb.Append("CASE CODES.SHORT_CODE  ");
                    //sb.Append("WHEN 'PO' THEN FUNDS_TRANS_SPLIT.AMOUNT*-1   ");
                    //sb.Append("WHEN 'OA' THEN FUNDS_TRANS_SPLIT.AMOUNT*-1   ");
                    //sb.Append("ELSE FUNDS_TRANS_SPLIT.AMOUNT ");
                    //sb.Append("END AS AMOUNT	 ");
                    sb.Append("CASE ACTIVITY_TRACK.VOID_FLAG ");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append("WHEN 1 THEN NVL(FUNDS_TRANS_SPLIT.AMOUNT,0)*-1  ");
                        sb.Append("ELSE NVL(FUNDS_TRANS_SPLIT.AMOUNT,0) ");
                    }
                    else
                    {
                        sb.Append("WHEN 1 THEN ISNULL(FUNDS_TRANS_SPLIT.AMOUNT,0)*-1  ");
                        sb.Append("ELSE ISNULL(FUNDS_TRANS_SPLIT.AMOUNT,0) ");
                    }                    
                    sb.Append("END AS AMOUNT ");
                    sb.Append("FROM  ACTIVITY_TRACK ");
                    sb.Append("INNER JOIN FUNDS ON FUNDS.TRANS_ID=ACTIVITY_TRACK.FOREIGN_TABLE_KEY AND ACTIVITY_TRACK.FOREIGN_TABLE_ID=85 AND ACTIVITY_TRACK.UPLOAD_FLAG=-1 ");
                    sb.Append("INNER JOIN FUNDS_TRANS_SPLIT ON FUNDS.TRANS_ID=FUNDS_TRANS_SPLIT.TRANS_ID ");
                    sb.Append("INNER JOIN CODES ON CODES.CODE_ID=ACTIVITY_TRACK.ACTIVITY_TYPE ");
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW BAL_RMA_PEND_PAY_V AS  ");
                    sb.Append("SELECT     RC_ROW_ID, CLAIM_ID, SUM(AMOUNT) AS PEND_PAY, MIN(DTTM_RCD_LAST_UPD) AS LAST_TRAN_DATE  ");
                    sb.Append("FROM PENDUP_PAY_V    ");
                    sb.Append("GROUP BY CLAIM_ID,RC_ROW_ID ");
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW BAL_RMA_CLAIM_POLICY_V AS  ");
                    sb.Append("SELECT POLICY.POLICY_SYSTEM_ID,CLAIM.*  FROM CLAIM  ");
                    sb.Append("INNER JOIN CLAIM_X_POLICY ON CLAIM.CLAIM_ID=CLAIM_X_POLICY.CLAIM_ID  ");
                    sb.Append("INNER JOIN POLICY ON CLAIM_X_POLICY.POLICY_ID=POLICY.POLICY_ID   ");
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW BAL_RMA_CLAIM_V AS  ");
                    sb.Append("SELECT CLAIM.CLAIM_NUMBER,CLAIM.CLAIM_ID, CODES_CLAIMTYPE.SHORT_CODE AS CLAIMTYPE,   ");
                    sb.Append("CODES_CLAIM_STATUS.SHORT_CODE AS CLAIMSTATUS,CLAIM.CLAIM_TYPE_CODE,CLAIM_BALANCING_PARMS.CLAIM_TYPE,  ");
                    sb.Append("CASE CLAIM_BALANCING_PARMS.CLAIM_BASED_DATE   ");
                    sb.Append("WHEN -1 THEN CLAIM.DATE_OF_CLAIM    ");
                    sb.Append("ELSE EVENT.DATE_OF_EVENT   ");
                    sb.Append("END AS DATE_OF_CLAIM,  ");
                    //sb.Append("CLAIM.POLICY_SYSTEM_ID AS POLICY_SYSTEM_ID,  ");
                    sb.Append("CLAIM.LINE_OF_BUS_CODE ");
                    sb.Append("FROM	 ");
                    //sb.Append("BAL_RMA_CLAIM_POLICY_V CLAIM INNER JOIN CODES CODES_CLAIMTYPE ON CLAIM.CLAIM_TYPE_CODE=CODES_CLAIMTYPE.CODE_ID   ");
                    sb.Append("CLAIM INNER JOIN CODES CODES_CLAIMTYPE ON CLAIM.CLAIM_TYPE_CODE=CODES_CLAIMTYPE.CODE_ID   ");
                    sb.Append("INNER JOIN CODES CODES_CLAIM_STATUS ON CLAIM.CLAIM_STATUS_CODE=CODES_CLAIM_STATUS.CODE_ID      ");
                    sb.Append("INNER JOIN EVENT ON CLAIM.EVENT_ID=EVENT.EVENT_ID   ");
                    //sb.Append("INNER JOIN CLAIM_BALANCING_PARMS ON CLAIM.POLICY_SYSTEM_ID =  CLAIM_BALANCING_PARMS.POLICY_SYSTEM_ID  ");
                    sb.Append("INNER JOIN CLAIM_BALANCING_PARMS ON 1=1 ");
                    sb.Append("AND ");
                    sb.Append("( ");
                    sb.Append("(CLAIM_BALANCING_PARMS.DATE_OF_CLAIM IS NULL)  ");
                    sb.Append("OR ");
                    sb.Append("( ");
                    sb.Append(" (CLAIM_BALANCING_PARMS.CLAIM_BASED_DATE='-1' AND CLAIM.DATE_OF_CLAIM >= CLAIM_BALANCING_PARMS.DATE_OF_CLAIM )  ");
                    sb.Append("   OR ");
                    sb.Append(" (CLAIM_BALANCING_PARMS.CLAIM_BASED_DATE='0' AND EVENT.DATE_OF_EVENT >= CLAIM_BALANCING_PARMS.DATE_OF_CLAIM )  ");
                    sb.Append(") ");
                    sb.Append(") ");
                    sb.Append("AND (CLAIM_BALANCING_PARMS.CLAIM_TYPE IS NULL OR CLAIM.CLAIM_TYPE_CODE=CLAIM_BALANCING_PARMS.CLAIM_TYPE)   ");
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW RMA_POINT_CODE_MAPPING  AS  ");
                    sb.Append("SELECT  RMA_CODES.CODE_ID AS RMA_CODEID, RMA_CODES.SHORT_CODE AS RMA_SHORT_CODE,  ");
                    sb.Append("POINT_CODES.CODE_ID AS POINT_CODEID, POINT_CODES.SHORT_CODE AS POINT_SHORT_CODE,  ");
                    sb.Append("POLICY_CODE_MAPPING.RMX_TABLE_ID,POLICY_CODE_MAPPING.POLICY_SYSTEM_ID ");
                    sb.Append("FROM POLICY_CODE_MAPPING  ");
                    sb.Append("INNER JOIN CODES RMA_CODES ON RMA_CODES.CODE_ID=POLICY_CODE_MAPPING.RMX_CODE_ID   ");
                    sb.Append("INNER JOIN CODES POINT_CODES ON POINT_CODES.CODE_ID=POLICY_CODE_MAPPING.PS_CODE_ID ");
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW BAL_RMA_COVERAGE_V AS ");
                    sb.Append("SELECT RESERVE_CURRENT.RC_ROW_ID,COVERAGE_X_LOSS.CVG_LOSS_ROW_ID,COVERAGE_X_LOSS.POLCVG_ROW_ID,CODES_TEXT.CODE_DESC AS LOSSCODE, ");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append("NVL(RMA_POINT_CODE_MAPPING.POINT_SHORT_CODE,CODES_TEXT.SHORT_CODE) AS POINTLOSSCODESHORTCODE,  ");
                    }
                    else
                    {
                        sb.Append("ISNULL(RMA_POINT_CODE_MAPPING.POINT_SHORT_CODE,CODES_TEXT.SHORT_CODE) AS POINTLOSSCODESHORTCODE,  ");
                    }
                    sb.Append("POLICY.POLICY_SYSTEM_ID, ");
                    sb.Append("CODES_TEXT.SHORT_CODE AS RMA_LOSS_SHORT_CODE  ");
                    sb.Append("FROM RESERVE_CURRENT   ");
                    sb.Append("INNER JOIN COVERAGE_X_LOSS ON RESERVE_CURRENT.POLCVG_LOSS_ROW_ID=COVERAGE_X_LOSS.CVG_LOSS_ROW_ID  ");
                    sb.Append("INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLCVG_ROW_ID=COVERAGE_X_LOSS.POLCVG_ROW_ID  ");
                    sb.Append("INNER JOIN POLICY_X_UNIT ON POLICY_X_UNIT.POLICY_UNIT_ROW_ID=POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID  ");
                    sb.Append("INNER JOIN POLICY ON POLICY.POLICY_ID=POLICY_X_UNIT.POLICY_ID  ");
                    sb.Append("INNER JOIN CODES_TEXT ON CODES_TEXT.CODE_ID=COVERAGE_X_LOSS.LOSS_CODE  ");
                    sb.Append("LEFT OUTER JOIN RMA_POINT_CODE_MAPPING ON RMA_POINT_CODE_MAPPING.RMA_CODEID=COVERAGE_X_LOSS.LOSS_CODE AND RMA_POINT_CODE_MAPPING.POLICY_SYSTEM_ID=POLICY.POLICY_SYSTEM_ID ");
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW BAL_RMA_RESERVE_V AS ");
                    sb.Append("SELECT RESERVE_CURRENT.RESERVE_AMOUNT  AS RESERVE_AMOUNT,    ");
                    sb.Append("RESERVE_CURRENT.INCURRED_AMOUNT  AS INCURRED_AMOUNT,  ");
                    sb.Append("RESERVE_CURRENT.COLLECTION_TOTAL AS COLLECTION_TOTAL,    ");
                    sb.Append("RESERVE_CURRENT.PAID_TOTAL AS PAID_TOTAL,   ");
                    sb.Append("RESERVE_CURRENT.BALANCE_AMOUNT AS BALANCE_AMOUNT,  ");
                    sb.Append("CLAIMANT.CLAIMANT_NUMBER,CODES_RESTYPE.CODE_DESC AS RESERVETYPE, CODES_RESTYPE.SHORT_CODE AS RESERVETYPESHORTCODE,  ");
                    sb.Append("RESERVE_CURRENT.RC_ROW_ID, CODES_CLAIMSTATUS.SHORT_CODE AS RESERVESTATUSCODE,CODES_CLAIMSTATUS.CODE_DESC AS RESERVESTATUSDESC, ");
                    sb.Append("RESERVE_CURRENT.CLAIM_ID,  RESERVE_CURRENT.CLAIMANT_EID,  RESERVE_CURRENT.POLCVG_LOSS_ROW_ID,  ");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append(" NVL(RMA_POINT_CODE_MAPPING.POINT_SHORT_CODE,CODES_RESTYPE.SHORT_CODE) POINTRESTYPESHORTCODE, ");
                    }
                    else
                    {
                        sb.Append(" ISNULL(RMA_POINT_CODE_MAPPING.POINT_SHORT_CODE,CODES_RESTYPE.SHORT_CODE) POINTRESTYPESHORTCODE, ");
                    }
                    sb.Append("RESERVE_CURRENT.POLICY_CVG_SEQNO,BAL_RMA_COVERAGE_V.POINTLOSSCODESHORTCODE,  ");
                    sb.Append("BAL_RMA_COVERAGE_V.POLICY_SYSTEM_ID,  ");
                    sb.Append("CODES_PARENT_RESTYPE.SHORT_CODE AS PARENT_RES_TYPE, ");
                    sb.Append("BAL_RMA_COVERAGE_V.RMA_LOSS_SHORT_CODE, ");
                    sb.Append("CODES_RES_STATUS_PARENT.SHORT_CODE AS PARENT_RES_STATUS ");
                    sb.Append("FROM RESERVE_CURRENT   ");
                    sb.Append("INNER JOIN  CLAIMANT ON   CLAIMANT.CLAIMANT_EID =  RESERVE_CURRENT.CLAIMANT_EID AND  CLAIMANT.CLAIM_ID =  RESERVE_CURRENT.CLAIM_ID   ");
                    sb.Append("INNER JOIN CODES_TEXT CODES_RESTYPE ON CODES_RESTYPE.CODE_ID =  RESERVE_CURRENT.RESERVE_TYPE_CODE   ");
                    sb.Append("INNER JOIN CODES ON CODES.CODE_ID = CODES_RESTYPE.CODE_ID  ");
                    sb.Append("INNER JOIN CODES_TEXT CODES_CLAIMSTATUS ON RESERVE_CURRENT.RES_STATUS_CODE = CODES_CLAIMSTATUS.CODE_ID   ");
                    sb.Append("INNER JOIN CODES CODES_RES_STATUS ON CODES_RES_STATUS.CODE_ID=CODES_CLAIMSTATUS.CODE_ID ");//TO GET PARENT RESERVE STATUS
                    sb.Append("INNER JOIN  BAL_RMA_COVERAGE_V ON RESERVE_CURRENT.RC_ROW_ID =  BAL_RMA_COVERAGE_V.RC_ROW_ID   ");
                    sb.Append(" LEFT OUTER JOIN RMA_POINT_CODE_MAPPING ON RESERVE_CURRENT.RESERVE_TYPE_CODE=RMA_POINT_CODE_MAPPING.RMA_CODEID AND  RMA_POINT_CODE_MAPPING.POLICY_SYSTEM_ID=BAL_RMA_COVERAGE_V.POLICY_SYSTEM_ID  ");
                    sb.Append("LEFT OUTER JOIN CODES CODES_PARENT_RESTYPE ON CODES.RELATED_CODE_ID=CODES_PARENT_RESTYPE.CODE_ID  ");
                    sb.Append("LEFT OUTER JOIN GLOSSARY ON GLOSSARY.TABLE_ID=CODES_PARENT_RESTYPE.TABLE_ID AND GLOSSARY.SYSTEM_TABLE_NAME='MASTER_RESERVE'  ");
                    sb.Append("LEFT OUTER JOIN CODES CODES_RES_STATUS_PARENT ON CODES_RES_STATUS.RELATED_CODE_ID=CODES_RES_STATUS_PARENT.CODE_ID ");////TO GET PARENT RESERVE STATUS
                    //sb.Append("WHERE BAL_RMA_COVERAGE_V.POLICY_SYSTEM_ID>0 ");
                    sb.Append("WHERE BAL_RMA_COVERAGE_V.POLICY_SYSTEM_ID = (SELECT POLICY_SYSTEM_ID FROM CLAIM_BALANCING_PARMS) ");

                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW BAL_RMA_RESERVELEVEL_V AS SELECT ");

                    sb.Append(" BAL_RMA_CLAIM_V.CLAIM_ID,    ");
                    sb.Append("BAL_RMA_CLAIM_V.CLAIM_NUMBER AS RMA_CLAIM_NUMBER, ");
                    sb.Append("BAL_RMA_CLAIM_V.DATE_OF_CLAIM, ");
                    sb.Append("BAL_RMA_RESERVE_V.RC_ROW_ID,     ");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append("BAL_RMA_RESERVE_V.CLAIMANT_EID, NVL(ENTITY.FIRST_NAME, '') || ' ' ||  ENTITY.LAST_NAME AS CLAIMANT, ");
                    }
                    else
                    {
                        sb.Append("BAL_RMA_RESERVE_V.CLAIMANT_EID, LTRIM(ISNULL( ENTITY.FIRST_NAME, '') + ' ' +  ENTITY.LAST_NAME) AS CLAIMANT, ");
                    }
                    sb.Append(" BAL_RMA_RESERVE_V.CLAIMANT_NUMBER, ");
                    sb.Append(" BAL_RMA_RESERVE_V.RESERVETYPE, ");
                    sb.Append(" BAL_RMA_RESERVE_V.RESERVESTATUSCODE,");
                    sb.Append("BAL_RMA_RESERVE_V.POINTRESTYPESHORTCODE,  ");
                    sb.Append("BAL_RMA_RESERVE_V.POLCVG_LOSS_ROW_ID,  ");
                    sb.Append("BAL_RMA_RESERVE_V.POINTLOSSCODESHORTCODE,    ");
                    sb.Append("BAL_RMA_RESERVE_V.POLICY_CVG_SEQNO POLCOVSEQ, ");
                    sb.Append(" BAL_RMA_CLAIM_V.CLAIMSTATUS,");
                    sb.Append(" BAL_RMA_CLAIM_V.CLAIMTYPE,            ");
                    sb.Append("BAL_RMA_RESERVE_V.POLICY_SYSTEM_ID,    ");
                    sb.Append(" BAL_RMA_RESERVE_V.RESERVE_AMOUNT, ");
                    sb.Append(" BAL_RMA_RESERVE_V.INCURRED_AMOUNT,  ");
                    sb.Append(" BAL_RMA_RESERVE_V.COLLECTION_TOTAL,   ");
                    sb.Append(" BAL_RMA_RESERVE_V.PAID_TOTAL,      ");
                    sb.Append(" BAL_RMA_RESERVE_V.BALANCE_AMOUNT, ");
                    sb.Append(" BAL_RMA_PEND_PAY_V.PEND_PAY,     ");
                    sb.Append(" BAL_RMA_PEND_PAY_V.LAST_TRAN_DATE AS PEND_PAY_DATE,       ");
                    sb.Append(" BAL_RMA_PEND_RESERVE_V.PEND_RESERVE AS PEND_RESERVE,   ");
                    sb.Append(" BAL_RMA_PEND_RESERVE_V.LAST_TRAN_DATE AS PEND_RESERVE_DATE, ");
                    sb.Append(" RMA_HOLD_RESCUR_V.RESERVE_AMOUNT AS HOLD_RESERVE,   ");
                    sb.Append("RMA_HOLD_RESCUR_V.DTTM_RCD_LAST_UPD AS HOLD_RESERVE_DATE, ");
                    sb.Append("BAL_RMA_RESERVE_V.PARENT_RES_TYPE, ");
                    sb.Append("BAL_RMA_CLAIM_V.LINE_OF_BUS_CODE, ");
                    sb.Append("BAL_RMA_RESERVE_V.RMA_LOSS_SHORT_CODE, ");
                    sb.Append("BAL_RMA_RESERVE_V.PARENT_RES_STATUS ");   
                    sb.Append("FROM RMA_HOLD_RESCUR_V     ");
                    sb.Append("RIGHT OUTER JOIN    ");
                    sb.Append("BAL_RMA_RESERVE_V   ");
                    //sb.Append("INNER JOIN  BAL_RMA_CLAIM_V ON  BAL_RMA_CLAIM_V.CLAIM_ID =  BAL_RMA_RESERVE_V.CLAIM_ID AND BAL_RMA_CLAIM_V.POLICY_SYSTEM_ID=BAL_RMA_RESERVE_V.POLICY_SYSTEM_ID  ");
                    sb.Append("INNER JOIN  BAL_RMA_CLAIM_V ON  BAL_RMA_CLAIM_V.CLAIM_ID =  BAL_RMA_RESERVE_V.CLAIM_ID ");
                    sb.Append("ON RMA_HOLD_RESCUR_V.RC_ROW_ID =  BAL_RMA_RESERVE_V.RC_ROW_ID AND RMA_HOLD_RESCUR_V.CLAIM_ID=BAL_RMA_RESERVE_V.CLAIM_ID   ");
                    sb.Append("LEFT OUTER JOIN   BAL_RMA_PEND_RESERVE_V ON  BAL_RMA_RESERVE_V.RC_ROW_ID =  BAL_RMA_PEND_RESERVE_V.RC_ROW_ID   ");
                    sb.Append("LEFT OUTER JOIN   BAL_RMA_PEND_PAY_V ON  BAL_RMA_RESERVE_V.RC_ROW_ID =  BAL_RMA_PEND_PAY_V.RC_ROW_ID    ");
                    sb.Append("LEFT OUTER JOIN   ENTITY ON  BAL_RMA_RESERVE_V.CLAIMANT_EID =  ENTITY.ENTITY_ID  ");
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    

                    sb.Clear();
                    sb.Append(" CREATE VIEW BAL_P_RESERVELEVEL_V AS  ");
                    sb.Append("SELECT  CLAIM_BALANCING.*,  ");
                    sb.Append("CLAIM.CLAIM_NUMBER RMACLAIMNUMBER, ");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append("CLAIM_BALANCING.P_RESVTYPE ||'('||CLAIM_BALANCING.P_RESVNO||')' AS POINTRESTYPEINRMAFORMAT  ");
                    }
                    else
                    {
                        sb.Append("CLAIM_BALANCING.P_RESVTYPE+'('+CAST(CLAIM_BALANCING.P_RESVNO AS VARCHAR(10))+')' AS POINTRESTYPEINRMAFORMAT  ");
                    }
                    sb.Append("FROM  ");
                    sb.Append("CLAIM_BALANCING INNER JOIN   ");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append("BAL_RMA_CLAIM_V CLAIM ON  LTRIM(CLAIM.CLAIM_NUMBER,'0')=LTRIM(CLAIM_BALANCING.P_CLAIM_NUMBER,'0') ");
                    }
                    else
                    {
                        sb.Append("BAL_RMA_CLAIM_V CLAIM ON  SUBSTRING(CLAIM.CLAIM_NUMBER,PATINDEX('%[^0]%',CLAIM.CLAIM_NUMBER),LEN(CLAIM.CLAIM_NUMBER))=SUBSTRING(CLAIM_BALANCING.P_CLAIM_NUMBER,PATINDEX('%[^0]%',CLAIM_BALANCING.P_CLAIM_NUMBER),LEN(CLAIM_BALANCING.P_CLAIM_NUMBER))  ");
                    }
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW RMA_RSV_MOVE_HIST_V AS                      SELECT CLAIM.CLAIM_NUMBER,CLAIMANT.CLAIMANT_NUMBER,RESERVE_MOVE_HIST.POLCOVSEQ,");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append("  NVL(POLLOSSTYPE.SHORT_CODE,LOSSCODE.SHORT_CODE) LOSS_CODE,NVL(POLRSVTYPE.SHORT_CODE,RMRSVTYPE.SHORT_CODE) RSV_TYPE");

                    }
                    else
                    {
                        sb.Append("  ISNULL(POLLOSSTYPE.SHORT_CODE,LOSSCODE.SHORT_CODE) LOSS_CODE,ISNULL(POLRSVTYPE.SHORT_CODE,RMRSVTYPE.SHORT_CODE) RSV_TYPE");
                    }
                    sb.Append("  FROM RESERVE_CURRENT,RESERVE_MOVE_HIST,CLAIM,CLAIMANT,CODES LOSSCODE  ,");
                    sb.Append("  CODES POLRSVTYPE,CODES RMRSVTYPE, POLICY_CODE_MAPPING RSV_MAPPING,POLICY_CODE_MAPPING LOSS_MAPPING,CLAIM_BALANCING_PARMS,");
                    sb.Append("   CODES POLLOSSTYPE                 WHERE RESERVE_MOVE_HIST.RC_ROW_ID=RESERVE_CURRENT.RC_ROW_ID");
                    sb.Append("   AND RESERVE_CURRENT.CLAIM_ID= CLAIM.CLAIM_ID    AND CLAIM.CLAIM_ID=CLAIMANT.CLAIM_ID");
                    sb.Append("  AND RESERVE_MOVE_HIST.CLAIMANT_EID= CLAIMANT.CLAIMANT_EID   AND RESERVE_MOVE_HIST.LOSS_CODE=LOSSCODE.CODE_ID");
                    sb.Append("     AND RSV_MAPPING.POLICY_SYSTEM_ID = CLAIM_BALANCING_PARMS.POLICY_SYSTEM_ID        AND RSV_MAPPING.RMX_CODE_ID= RESERVE_MOVE_HIST.RESERVE_TYPE_CODE");
                    sb.Append("  AND POLRSVTYPE.CODE_ID= RSV_MAPPING.PS_CODE_ID  AND RMRSVTYPE.CODE_ID= RESERVE_MOVE_HIST.RESERVE_TYPE_CODE   AND ");
                    sb.Append("     LOSS_MAPPING.POLICY_SYSTEM_ID = CLAIM_BALANCING_PARMS.POLICY_SYSTEM_ID   AND LOSS_MAPPING.RMX_CODE_ID= RESERVE_MOVE_HIST.LOSS_CODE");
                    sb.Append("     AND POLLOSSTYPE.CODE_ID= LOSS_MAPPING.PS_CODE_ID");
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW BALANCE_RMA_POINT_ALL_V AS ");
                    sb.Append("SELECT BAL_RMA_RESERVELEVEL_V.RMA_CLAIM_NUMBER, ");
                    sb.Append("BAL_P_RESERVELEVEL_V.P_CLAIM_NUMBER AS P_CLAIM_NUMBER, ");
                    sb.Append("BAL_P_RESERVELEVEL_V.P_CLMTSEQ AS P_CLMTSEQ,   ");
                    sb.Append("BAL_P_RESERVELEVEL_V.P_POLCOVSEQ AS P_POLCOVSEQ, ");
                    sb.Append("BAL_P_RESERVELEVEL_V.P_RESVTYPE AS P_RESVTYPE,   ");
                    sb.Append("BAL_P_RESERVELEVEL_V.P_RESVNO AS P_RESVNO,  ");
                    sb.Append("CASE BAL_RMA_RESERVELEVEL_V.PAID_TOTAL ");
                    sb.Append("WHEN 0 THEN BAL_RMA_RESERVELEVEL_V.COLLECTION_TOTAL ");
                    sb.Append("ELSE ");
                    sb.Append("CASE BAL_RMA_RESERVELEVEL_V.COLLECTION_TOTAL ");
                    sb.Append("WHEN 0 THEN BAL_RMA_RESERVELEVEL_V.PAID_TOTAL ");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append("ELSE NVL(BAL_RMA_RESERVELEVEL_V.PAID_TOTAL,0)-NVL(BAL_RMA_RESERVELEVEL_V.COLLECTION_TOTAL,0) ");
                    }
                    else
                    {
                        sb.Append("ELSE ISNULL(BAL_RMA_RESERVELEVEL_V.PAID_TOTAL,0)-ISNULL(BAL_RMA_RESERVELEVEL_V.COLLECTION_TOTAL,0) ");
                    }
                    sb.Append("END ");
                    sb.Append("END AS RMA_PAID_TOTAL, ");
                    sb.Append("BAL_RMA_RESERVELEVEL_V.BALANCE_AMOUNT AS RMA_CURRESV, ");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append("NVL(BAL_RMA_RESERVELEVEL_V.BALANCE_AMOUNT,0) + NVL(BAL_P_RESERVELEVEL_V.P_CURRESV,0) AS SUMRESV, ");
                    }
                    else
                    {
                        sb.Append("ISNULL(BAL_RMA_RESERVELEVEL_V.BALANCE_AMOUNT,0) + ISNULL(BAL_P_RESERVELEVEL_V.P_CURRESV,0) AS SUMRESV, ");
                    }
                    sb.Append("BAL_RMA_RESERVELEVEL_V.PARENT_RES_TYPE,  ");
                    sb.Append("CASE PARENT_RES_TYPE  ");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append("WHEN 'R' THEN NVL(BAL_P_RESERVELEVEL_V.P_CURRESV,0)*-1  ");
                    }
                    else
                    {
                        sb.Append("WHEN 'R' THEN ISNULL(BAL_P_RESERVELEVEL_V.P_CURRESV,0)*-1  ");
                    }
                    sb.Append("ELSE BAL_P_RESERVELEVEL_V.P_CURRESV  ");
                    sb.Append("END AS P_CURRESV, ");
                    sb.Append("BAL_RMA_RESERVELEVEL_V.PEND_PAY AS RMA_PEND_PAY,  ");
                    sb.Append("BAL_RMA_RESERVELEVEL_V.PEND_PAY_DATE AS RMA_PEND_PAY_DATE,   ");
                    sb.Append("BAL_RMA_RESERVELEVEL_V.PEND_RESERVE AS RMA_PEND_RESERVE, ");
                    sb.Append(" BAL_RMA_RESERVELEVEL_V.PEND_RESERVE_DATE AS RMA_PEND_RESERVE_DATE,  ");
                    sb.Append(" BAL_RMA_RESERVELEVEL_V.HOLD_RESERVE AS RMA_HOLD_RESERVE, ");
                    sb.Append(" BAL_RMA_RESERVELEVEL_V.HOLD_RESERVE_DATE AS RMA_HOLD_RESERVE_DATE, ");
                    sb.Append(" BAL_P_RESERVELEVEL_V.P_PAID_TOTAL AS P_PAID_TOTAL, ");
                    sb.Append(" BAL_P_RESERVELEVEL_V.P_OFFICE AS CLAIM_OFFICE, ");
                    sb.Append(" BAL_P_RESERVELEVEL_V.P_CLAIMTYPE AS P_CLAIMTYPE, ");
                    sb.Append(" BAL_P_RESERVELEVEL_V.P_CLAIM_STATUS AS P_CLAIMSTATUS, ");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append("NVL( BAL_P_RESERVELEVEL_V.P_LOSSDATE,  BAL_RMA_RESERVELEVEL_V.DATE_OF_CLAIM) AS P_LOSSDTE, ");
                        sb.Append("NVL( BAL_RMA_RESERVELEVEL_V.CLAIMANT,'') AS CLAIMANT, ");
                    }
                    else
                    {
                        sb.Append("ISNULL( BAL_P_RESERVELEVEL_V.P_LOSSDATE,  BAL_RMA_RESERVELEVEL_V.DATE_OF_CLAIM) AS P_LOSSDTE, ");
                        sb.Append("ISNULL( BAL_RMA_RESERVELEVEL_V.CLAIMANT,'') AS CLAIMANT, ");
                    }
                    sb.Append("BAL_RMA_RESERVELEVEL_V.CLAIMTYPE AS RMA_CLAIM_TYPE,  ");
                    sb.Append("BAL_RMA_RESERVELEVEL_V.CLAIMSTATUS AS RMA_CLAIM_STATUS,");
                    sb.Append("BAL_RMA_RESERVELEVEL_V.RC_ROW_ID AS RMA_RC_ROW_ID,   ");
                    sb.Append("BAL_RMA_RESERVELEVEL_V.POLICY_SYSTEM_ID,  ");
                    sb.Append("BAL_P_RESERVELEVEL_V.P_TRANS_SEQ, ");
                    sb.Append("BAL_RMA_RESERVELEVEL_V.LINE_OF_BUS_CODE, ");
                    sb.Append("BAL_RMA_RESERVELEVEL_V.COLLECTION_TOTAL, ");
                    sb.Append("BAL_P_RESERVELEVEL_V.P_LOSSCODE, ");
                    sb.Append("BAL_RMA_RESERVELEVEL_V.RMA_LOSS_SHORT_CODE, ");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append("NVL(BAL_P_RESERVELEVEL_V.P_RECSTATUS,'') AS P_RECSTATUS,");
                        sb.Append("NVL(BAL_P_RESERVELEVEL_V.P_MASTERCO,'') AS P_MASTERCO,  ");
                    }
                    else
                    {
                        sb.Append("ISNULL(BAL_P_RESERVELEVEL_V.P_RECSTATUS,'') AS P_RECSTATUS,");
                        sb.Append("ISNULL(BAL_P_RESERVELEVEL_V.P_MASTERCO,'') AS P_MASTERCO,  ");
                    }
                    sb.Append("BAL_RMA_RESERVELEVEL_V.PARENT_RES_STATUS ");
                    sb.Append("FROM  BAL_P_RESERVELEVEL_V FULL OUTER JOIN   ");
                    sb.Append(" BAL_RMA_RESERVELEVEL_V ON  BAL_P_RESERVELEVEL_V.RMACLAIMNUMBER =  BAL_RMA_RESERVELEVEL_V.RMA_CLAIM_NUMBER AND  BAL_P_RESERVELEVEL_V.RMACLAIMNUMBER IS NOT NULL AND       ");
                    sb.Append(" BAL_P_RESERVELEVEL_V.P_CLMTSEQ =  BAL_RMA_RESERVELEVEL_V.CLAIMANT_NUMBER AND  ");
                    sb.Append(" BAL_P_RESERVELEVEL_V.P_POLCOVSEQ =  BAL_RMA_RESERVELEVEL_V.POLCOVSEQ AND    ");
                    sb.Append(" BAL_P_RESERVELEVEL_V.POINTRESTYPEINRMAFORMAT =  BAL_RMA_RESERVELEVEL_V.POINTRESTYPESHORTCODE AND  ");
                    sb.Append(" BAL_P_RESERVELEVEL_V.P_LOSSCODE =  BAL_RMA_RESERVELEVEL_V.POINTLOSSCODESHORTCODE ");



                    sb.Append(" FULL OUTER JOIN");


                    sb.Append(" RMA_RSV_MOVE_HIST_V ON BAL_P_RESERVELEVEL_V.RMACLAIMNUMBER <> RMA_RSV_MOVE_HIST_V.CLAIM_NUMBER AND ");
                    sb.Append(" BAL_P_RESERVELEVEL_V.P_CLMTSEQ <> RMA_RSV_MOVE_HIST_V.CLAIMANT_NUMBER AND ");
                    sb.Append(" BAL_P_RESERVELEVEL_V.P_POLCOVSEQ <> RMA_RSV_MOVE_HIST_V.POLCOVSEQ AND ");
                    sb.Append(" BAL_P_RESERVELEVEL_V.POINTRESTYPEINRMAFORMAT <> RMA_RSV_MOVE_HIST_V.RSV_TYPE AND ");
                    sb.Append(" BAL_P_RESERVELEVEL_V.P_LOSSCODE <> RMA_RSV_MOVE_HIST_V.LOSS_CODE");

                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());

                    sb.Clear();
                    sb.Append("CREATE VIEW BALANCE_RMA_POINT_OB_V AS ");
                    sb.Append("SELECT BALANCE_RMA_POINT_ALL_V.*  ");
                    sb.Append("FROM  BALANCE_RMA_POINT_ALL_V ");
                    sb.Append("WHERE ");
                    sb.Append("P_RECSTATUS <> 'T' AND ");
                    sb.Append("( ");
                    if (DbFactory.IsOracleDatabase(m_sConnectString))
                    {
                        sb.Append("(NVL(RMA_PAID_TOTAL, 0) <> NVL(P_PAID_TOTAL, 0)) OR ");
                        sb.Append("(NVL(RMA_CURRESV, 0) <> NVL(P_CURRESV, 0))  ");
                    }
                    else
                    {
                        sb.Append("(CAST(ISNULL(RMA_PAID_TOTAL, 0) AS DECIMAL(10,2)) <> CAST(ISNULL(P_PAID_TOTAL, 0) AS DECIMAL(10,2))) OR  ");
                        sb.Append("(CAST(ISNULL(RMA_CURRESV, 0) AS DECIMAL(10,2)) <> CAST(ISNULL(P_CURRESV, 0) AS DECIMAL(10,2)))  ");
                    }
                    sb.Append("OR RMA_CLAIM_NUMBER IS NULL OR P_CLAIM_NUMBER IS NULL ");
                    sb.Append(") ");
                    DbFactory.ExecuteNonQuery(m_sConnectString, sb.ToString());
                }
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("SetUpPolicySystem.CreateDBViewsForPointClaimBalancing.Error",m_iClientId), p_objExp);
            }
            finally
            {
                sb = null;
            }
        }
        //rupal:end,mits 33290

        //rupal:start
        public bool RefreshPolicySystems()
        {
            DbReader objReader = null;            
            bool bSuccess = false;            
            string sSQL = string.Empty;            
            int iPolicysystemTypeId = 0;
                     
            string sMappingTablePrefix = string.Empty;	// aaggarwal29: Code mapping change
            try
            {
                
                
                sSQL = "SELECT POLICY_SYSTEM_CODE,TABLE_PREFIX FROM POLICY_X_WEB"; // aaggarwal29: Code mapping change
                objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                while (objReader.Read())
                {
                    iPolicysystemTypeId = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReader.GetValue("POLICY_SYSTEM_CODE")), out bSuccess);
                    
                    
                    sMappingTablePrefix = objReader.GetString("TABLE_PREFIX"); // aaggarwal29: Code mapping change
                    InsertPSMapTablesIntoGlossary(iPolicysystemTypeId, sMappingTablePrefix); // aaggarwal29: Code mapping change
                }
                //rupal:end, mita 33290               
                CreateDBViewsForPointClaimBalancing(iPolicysystemTypeId.ToString());
                //rupal:end, mita 33290
                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("SetUpPolicySystem.RefreshPolicySystems.Error",m_iClientId), p_objExp);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();              
            }
            
        }

        //rupal:start, mits 332980
        private void DropDBViews()
        {
            //RUPAL:MITS 33290
            //DROP VIEWS FIRST AND THEN RECREATE VIEWS
            string sSQL = string.Empty;
            List<string> lstDBViews = new List<string>();
            lstDBViews.Add("BALANCE_RMA_POINT_OB_V");
            lstDBViews.Add("BAL_P_ReserveLevel_V");
            lstDBViews.Add("RMA_HOLD_RESCUR_V");
            lstDBViews.Add("PENDUP_RESHIS_V");
            lstDBViews.Add("BAL_RMA_PEND_RESERVE_V");
            lstDBViews.Add("PENDUP_PAY_V");
            lstDBViews.Add("BAL_RMA_PEND_PAY_V");
            lstDBViews.Add("BAL_RMA_CLAIM_POLICY_V");
            lstDBViews.Add("BAL_RMA_Claim_V");
            lstDBViews.Add("BAL_RMA_Coverage_V");
            lstDBViews.Add("BAL_RMA_Reserve_V");
            lstDBViews.Add("BAL_RMA_ReserveLevel_V");
            lstDBViews.Add("RMA_POINT_CODE_MAPPING");
            lstDBViews.Add("RMA_RSV_MOVE_HIST_V");
            lstDBViews.Add("BAL_P_ReserveLevel_V");
            lstDBViews.Add("BALANCE_RMA_Point_ALL_V");
            foreach (string sView in lstDBViews)
            {
                try
                {
                    sSQL = "DROP VIEW " + sView;
                    DbFactory.ExecuteNonQuery(m_sConnectString, sSQL);
                }
                catch (Exception e)
                {
                }
            }           
        }
        //rupal:end
        private bool InsertPSMapTablesIntoGlossary(int iPolicySystemTypeId, string p_SystemTableName) // aaggarwal29: Code mapping change
        {
            DbReader objReader = null;            
            DbReader objReaderGlos = null;
            LocalCache objCache = null;
            bool bSuccess = false;
            int iCount = 0;
            int iPSTableId = 0;
            string sPSTableName = string.Empty;
            // string sPolSysTeypeName = string.Empty;      // aaggarwal29: Code mapping change
            string sMultipleRelationship = string.Empty;
            string sSQL = string.Empty;
            string sSQLInsert = string.Empty;
            int iRMXTableId = 0;
            int iRowId = 0;
            string sPSTableNameCopy = string.Empty;

            DbConnection objCon = null;
            DataTable dtPsMapTables = new DataTable();
            try
            {
                objCon = DbFactory.GetDbConnection(m_sConnectString);
                objCon.Open();
                objCache = new LocalCache(m_sConnectString,m_iClientId);
                // sPolSysTeypeName = objCache.GetCodeDesc(iPolicySystemTypeId);         // aaggarwal29: Code mapping change
               
                sSQL = "SELECT RMX_TABLE_ID,MULTIPLE_RELATIONSHIP,ROW_ID FROM PS_MAP_TABLES WHERE POLICY_SYSTEM_TYPE_ID=" + iPolicySystemTypeId;
               
                using (objReader = objCon.ExecuteReader(sSQL))
                {
                    dtPsMapTables.Load(objReader);
                }

                foreach(DataRow dr in dtPsMapTables.Rows)
                {
                    iRMXTableId = Conversion.CastToType<int>(Conversion.ConvertObjToStr(dr["RMX_TABLE_ID"]), out bSuccess);
                    iRowId = Conversion.CastToType<int>(Conversion.ConvertObjToStr(dr["ROW_ID"]), out bSuccess);
                    sMultipleRelationship = Conversion.ConvertObjToStr(dr["MULTIPLE_RELATIONSHIP"]);

                    // aaggarwal29: Code mapping changes start
                    // sPSTableName = sPolSysTeypeName + "_" + objCache.GetTableName(iRMXTableId);
                     sPSTableName = p_SystemTableName + "_" + objCache.GetTableName(iRMXTableId);
                    //aaggarwal29: Code mapping changes end
                    sPSTableNameCopy = sPSTableName;
                   
                    if (sMultipleRelationship == "1")
                    {
                        sPSTableName = sPSTableName + "(M)";
                        //checkif glossary already conrtains a row for the table with out appending (M) in the table name
                        //if the table already exists which means, client has decided later on to support multiple relationship on this table, so we will just change the name of the table by appending (M) in the table name
                        sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + sPSTableNameCopy + "'";
                        objReaderGlos = objCon.ExecuteReader(sSQL);
                        if (objReaderGlos.Read())
                        {
                            int iTableId = objReaderGlos.GetInt(0);
                            objReaderGlos.Close();
                            // checking if glossary already contains table names with M appended , if already contains table with M then it will not use 
                            // the Update query , system is throwing error on refreshing the policysystem page in case of multirelationship ON ,as below update query 
                            //again try to execute and throw duplicate entry present error;
                            sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + sPSTableName + "'";
                            objReaderGlos = objCon.ExecuteReader(sSQL);
                            if (!objReaderGlos.Read())
                            {
                                sSQL = "UPDATE GLOSSARY SET SYSTEM_TABLE_NAME='" + sPSTableName + "' WHERE TABLE_ID=" + iTableId;
                                objCon.ExecuteNonQuery(sSQL);
                                sSQL = "UPDATE GLOSSARY_TEXT SET TABLE_NAME='" + sPSTableName + "' WHERE TABLE_ID=" + iTableId;
                                objCon.ExecuteNonQuery(sSQL);
                            }
                            objReaderGlos.Close();
                        }
                        objReaderGlos.Close(); // incase there is no data read, reader does not get closed and so exception at later read so closing here again..
                    }
                    else
                    {
                        //check if earlier the table supported multiple_relationships but later on decided not to support multiple_relation ship, in that case we need to rename the table by removing the '(M)' phrase  from the table name
                        sPSTableNameCopy = sPSTableName + "(M)";
                        sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + sPSTableNameCopy + "'";
                        objReaderGlos = objCon.ExecuteReader(sSQL);
                        if (objReaderGlos.Read())
                        {
                            int iTableId = objReaderGlos.GetInt(0);
                            objReaderGlos.Close();
                            sSQL = "UPDATE GLOSSARY SET SYSTEM_TABLE_NAME='" + sPSTableName + "' WHERE TABLE_ID=" + iTableId;
                            objCon.ExecuteNonQuery(sSQL);
                            sSQL = "UPDATE GLOSSARY_TEXT SET TABLE_NAME='" + sPSTableName + "' WHERE TABLE_ID=" + iTableId;
                            objCon.ExecuteNonQuery(sSQL);
                        }
                        
                        objReaderGlos.Close();
                    }
                    //checkif glossary already conrtains a row for the table
                    sSQL = "SELECT COUNT(TABLE_ID) FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + sPSTableName + "'";
                    objReaderGlos = objCon.ExecuteReader(sSQL);
                    if (objReaderGlos.Read())
                    {
                        iCount = objReaderGlos.GetInt(0);
                        objReaderGlos.Close();
                        if (iCount == 0) //if table not already exists=> insert
                        {
                            //get next table id
                            sSQL = "SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'GLOSSARY'";
                            objReaderGlos = objCon.ExecuteReader(sSQL);
                            if (objReaderGlos.Read())
                            {
                                iPSTableId = objReaderGlos.GetInt(0);
                                objReaderGlos.Close();
                                //update next table id
                                string sQuery = String.Format("UPDATE GLOSSARY SET NEXT_UNIQUE_ID={0} WHERE SYSTEM_TABLE_NAME='GLOSSARY'", (iPSTableId + 1));
                                objCon.ExecuteNonQuery(sQuery);

                            }
                            //insert into glossary
                            sSQLInsert = "INSERT INTO GLOSSARY (TABLE_ID, SYSTEM_TABLE_NAME, GLOSSARY_TYPE_CODE, NEXT_UNIQUE_ID, " +
                            "DELETED_FLAG, REQD_REL_TABL_FLAG, ATTACHMENTS_FLAG, REQD_IND_TABL_FLAG, LINE_OF_BUS_FLAG) " +
                            "VALUES(" + iPSTableId + ",'" + sPSTableName + "',3,1,0,0,0,0,0)";
                            objCon.ExecuteNonQuery(sSQLInsert);

                            //insert into glossary text
                            //insert the values into the GLOSSARY_TEXT table
                            sSQLInsert = "INSERT INTO GLOSSARY_TEXT (TABLE_ID, TABLE_NAME, LANGUAGE_CODE) VALUES(" + iPSTableId + ",'" + sPSTableName + "',1033)";
                            objCon.ExecuteNonQuery(sSQLInsert);

                            //update glosary timestamp
                            string sTimeStamp = DateTime.Now.ToString("yyyyMMddHHmm00");
                            sSQLInsert = "UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '" + sTimeStamp + "'  WHERE SYSTEM_TABLE_NAME = '" + sPSTableName + "'";
                            objCon.ExecuteNonQuery(sSQLInsert);

                            //update PS_MAP_TABLE for PS_TABLE_ID
                            // aaggarwal29: Code mapping changes start
                            //sSQLInsert = string.Format("UPDATE PS_MAP_TABLES SET PS_TABLE_ID={0} WHERE ROW_ID={1}", iPSTableId, iRowId);
                            //DbFactory.ExecuteNonQuery(m_sConnectString, sSQLInsert);
                            //aaggarwal29: Code mapping changes end
                        }
                    }
                }
                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("SetUpPolicySystem.InsertPSMapTablesIntoGlossary.Error",m_iClientId), p_objExp);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if (objReaderGlos != null)
                    objReaderGlos.Dispose();
                if (objCache != null)
                    objCache.Dispose();
                if (objCon != null)
                    objCon.Dispose();
                if (dtPsMapTables != null)
                    dtPsMapTables.Dispose();
                
            }
        }
        //rupal:end

        public bool UpdatePassword(XmlDocument p_objInputXMLDoc)
        {
            string sSQL = "";
            DbConnection objCon = null;
            string sPolicySysId = "";

            XmlElement objPolicySysXMLEle = null;

            string sPassword = string.Empty;

            try
            {

                objPolicySysXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//PolicyInterface/PolicySys/ChangePassword");

                sPolicySysId = objPolicySysXMLEle.GetElementsByTagName("policysystemid").Item(0).InnerText;


                if (sPolicySysId == "")
                {
                    throw new RMAppException(Globalization.GetString("SetUpPolicySystem.GetById.NoRecordsFound",m_iClientId));
                }


                sPassword = PublicFunctions.Encrypt(objPolicySysXMLEle.GetElementsByTagName("NewPassword").Item(0).InnerText);
                sSQL = "UPDATE POLICY_X_WEB SET PASSWORD='" + sPassword + "'" +
                         " WHERE POLICY_SYSTEM_ID=" + sPolicySysId;

                objCon = DbFactory.GetDbConnection(m_sConnectString);
                objCon.Open();
                objCon.ExecuteNonQuery(sSQL);

                return true;

            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("SetUpPolicySystem.Save.Error",m_iClientId), p_objExp);
            }
            finally
            {
                if (objCon != null)
                    objCon.Dispose();

                if (objPolicySysXMLEle != null)
                    objPolicySysXMLEle = null;

            }
        }

        public XmlDocument LoadData()
        {

            string sSQL = string.Empty;
            DbReader objDbReader = null;

            XmlDocument objXMLDocument = null;
            XmlElement objXMLChildElement = null;
            XmlElement objXMLElement = null;
            LocalCache objLocalCache = null;
            string sApplicableLevel = string.Empty;
            string sDiscountName = string.Empty;

            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            int iCounter = 0;

            int iNoOfRecords = 0;

            try
            {

                objLocalCache = new LocalCache(m_sConnectString,m_iClientId);
                objXMLDocument = new XmlDocument();
                objXMLDocument.LoadXml(ROOT_TAG);


                iCounter = 0;
                sSQL = "SELECT * FROM POLICY_X_WEB ORDER BY POLICY_SYSTEM_ID DESC";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);

                iNoOfRecords = 0;

                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");

                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewElement("PolicySystemList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("PolicySystemList");

                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewElement("listhead"), false);
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("polsysname", "Policy System Name"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("MappingTablePrefix", "Mapping Table Prefix"), true)); // aaggarwal29: Code mapping change
                
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("PolicySystemType", "Policy System Type"), true)); // aaggarwal29: Code mapping change
                //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("domain", "Domain"), true)); // aaggarwal29: Code mapping change
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("CASServiceURL", "CAS Service URL"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("financialconn", "Financial Update"), true));
                //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("license", "License"), true));// aaggarwal29: Code mapping change
                //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("proxy", "Proxy"), true));// aaggarwal29: Code mapping change
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("urlparams", "URL Parameters"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("version", "Version"), true));
                
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("tag", "Tag"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("policysystemid", "Policy System Id"), true));
                objXMLElement.AppendChild(objXMLChildElement);

                if (objDbReader != null)
                {
                    while (objDbReader.Read())
                    {

                        if (iNoOfRecords < (PolicySysCurrentPage - 1) * PolicySysPageSize)
                        {
                            iNoOfRecords++;
                            continue;
                        }

                        if (iNoOfRecords >= PolicySysCurrentPage * PolicySysPageSize)
                            break;
                        objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewElement("option"), false);
                        objXMLChildElement.SetAttribute("ref", "/Document/Document/PolicySystemList/option[" + (++iCounter).ToString() + "]");
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("polsysname", Conversion.ConvertObjToStr(objDbReader.GetValue("POLICY_SYSTEM_NAME"))), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("MappingTablePrefix", Conversion.ConvertObjToStr(objDbReader.GetValue("TABLE_PREFIX"))), true));  // aaggarwal29: Code mapping change
                        //averma62 MITS - 27928  objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("domain", Conversion.ConvertObjToStr(objDbReader.GetValue("DOMAIN"))), true));
                        //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("domain", Conversion.ConvertObjToStr(objDbReader.GetInt64("DOMAIN"))), true));  //averma62 MITS - 27928 // aaggarwal29: Code mapping change
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("financialconn", objDbReader.GetInt32("FINANCIAL_UPD_FLAG") == -1 ? "True" : "False"), true));
                        //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("license", PublicFunctions.Decrypt(Conversion.ConvertObjToStr(objDbReader.GetValue("LICENSE_KEY")))), true)); // aaggarwal29: Code mapping change
                        //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("proxy", Conversion.ConvertObjToStr(objDbReader.GetValue("PROXY"))), true)); // aaggarwal29: Code mapping change
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("urlparams", Conversion.ConvertObjToStr(objDbReader.GetValue("URL_PARAMETER"))), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("version", Conversion.ConvertObjToStr(objDbReader.GetValue("VERSION"))), true));
                        sShortCode = Conversion.ConvertObjToStr(objDbReader.GetValue("POLICY_SYSTEM_CODE"));
                        sDesc = objLocalCache.GetCodeDesc(Conversion.ConvertObjToInt(sShortCode, m_iClientId));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("PolicySystemType", sDesc), true));  // aaggarwal29: Code mapping change
                        string sTag = Conversion.ConvertObjToStr(objDbReader.GetValue("POLICY_SYSTEM_NAME")) + "," + Conversion.ConvertObjToStr(objDbReader.GetValue("DOMAIN")) + "," + Conversion.ConvertObjToStr(objDbReader.GetValue("FINANCIAL_UPD_FLAG")) +   //averma62 MITS - 27928
                             "," + Conversion.ConvertObjToStr(objDbReader.GetValue("LICENSE_KEY")) + "," + Conversion.ConvertObjToStr(objDbReader.GetValue("PROXY")) + "," + Conversion.ConvertObjToStr(objDbReader.GetValue("URL_PARAMETER")) +
                             "," + Conversion.ConvertObjToStr(objDbReader.GetValue("POLICY_SYSTEM_ID"));


                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("tag", sTag), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("policysystemid", Conversion.ConvertObjToStr(objDbReader.GetValue("POLICY_SYSTEM_ID"))), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("CASServiceURL", Conversion.ConvertObjToStr(objDbReader.GetValue("CAS_SERVICE_URL"))), true));
                        
                        objXMLElement.AppendChild(objXMLChildElement);
                    }
                }

                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/Document/PolicySystemList/option[" + (++iCounter).ToString() + "]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewElement("polsysname"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewElement("MappingTablePrefix"), false)); // aaggarwal29: Code mapping change
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewElement("PolicySystemType"), false));  // aaggarwal29: Code mapping change
                //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewElement("domain"), false)); // aaggarwal29: Code mapping change
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewElement("financialconn"), false));
                //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewElement("license"), false)); // aaggarwal29: Code mapping change
                //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewElement("proxy"), false)); // aaggarwal29: Code mapping change
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewElement("urlparams"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewElement("version"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewElement("tag"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewElement("policysystemid"), false));
               
                objXMLElement.AppendChild(objXMLChildElement);

                sSQL = "SELECT count(*) Count FROM POLICY_X_WEB";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                int totalRows = 0;
                int totalPages = 0;
                if (objDbReader.Read())
                {
                    totalRows = Conversion.ConvertStrToInteger(objDbReader["Count"].ToString());
                    totalPages = totalRows / PolicySysPageSize;
                    if ((totalRows % PolicySysPageSize) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewElement("Document"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewElement("PolicySystemList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document/PolicySystemList");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewEleWithValue("hdTotalPages", totalPages.ToString()), true);
                objXMLElement.AppendChild(objXMLChildElement);

                return objXMLDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("SetUpPolicySystem.LoadData.Err",m_iClientId), p_objEx);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                objXMLDocument = null;
                objXMLChildElement = null;
                objXMLElement = null;
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
        }

        public XmlDocument Delete(XmlDocument p_objXmlDocument)
        {
		// aaggarwal29: Code mapping changes start
            string sRowId = string.Empty, sPolicySystemName = string.Empty, sTableNm = string.Empty, sTableSuffix = string.Empty, sTableNmForMultiRelationship;
           
            string sMappingTablePrefix = string.Empty;
            XmlElement objElm = null;
            DbConnection objConn = null;
            string sSQL = string.Empty;
            int iPolicySystemCode = 0,iTableId = 0 ;
    
          //  DbReader oReader, oReaderNew;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//DeletionId");
                if (objElm != null)
                    sRowId = objElm.InnerText;

                objConn = DbFactory.GetDbConnection(m_sConnectString);
                objConn.Open();

                if (sRowId != "")
                {
                  sSQL = "SELECT POLICY_SYSTEM_CODE, TABLE_PREFIX FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID=" + sRowId;
                  using (DbReader oReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                  {
                    //  if (oReader != null)
                      //{
                      while (oReader.Read())
                      {
                          iPolicySystemCode = Conversion.ConvertObjToInt(oReader.GetValue("POLICY_SYSTEM_CODE"), m_iClientId);

                          sMappingTablePrefix = Conversion.ConvertObjToStr(oReader.GetValue("TABLE_PREFIX"));
                          break;

                          //  }
                      }
                  }
                                        
                    sSQL = string.Empty;
                    sSQL = "SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID IN (SELECT RMX_TABLE_ID FROM PS_MAP_TABLES WHERE POLICY_SYSTEM_TYPE_ID = "+iPolicySystemCode+")";
                    using (DbReader oReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                    {
                        while (oReader.Read())
                        {
                            sTableSuffix = Conversion.ConvertObjToStr(oReader[0]);
                            sTableNm = sMappingTablePrefix + "_" + sTableSuffix;
                            sTableNmForMultiRelationship = sTableNm + "(M)";
                            sSQL = string.Empty;
                            sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME IN ('" + sTableNm + "','" + sTableNmForMultiRelationship + "')";
                            using (DbReader oReaderNew = DbFactory.GetDbReader(m_sConnectString, sSQL))
                            {
                                while (oReaderNew.Read())
                                {
                                    iTableId = Conversion.ConvertObjToInt(oReaderNew[0], m_iClientId);
                                    sSQL = string.Empty;
                                    sSQL = "DELETE FROM GLOSSARY WHERE TABLE_ID = " + iTableId;
                                    // sSQL = String.Concat(sSQL, "DELETE FROM GLOSSARY_TEXT WHERE TABLE_ID =" + iTableId);

                                    objConn.ExecuteNonQuery(sSQL);
                                    //objConnNew.Close();

                                    sSQL = "DELETE FROM GLOSSARY_TEXT WHERE TABLE_ID =" + iTableId;
                                    objConn.ExecuteNonQuery(sSQL);

                                    Log.Write(String.Format("Table ID {0} deleted from GLOSSARY and GLOSSARY_TEXT tables ", iTableId), m_iClientId);
                                    break;
                                }
                            }
                           
                        }
                    }
                    sSQL = string.Empty;
                    sSQL = "DELETE FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID=" + sRowId;
                    //sSQL = String.Concat(sSQL, "DELETE FROM POLICY_CODE_MAPPING WHERE POLICY_SYSTEM_ID=" + sRowId);
                    objConn.ExecuteNonQuery(sSQL);
                    sSQL ="DELETE FROM POLICY_CODE_MAPPING WHERE POLICY_SYSTEM_ID=" + sRowId;
                    objConn.ExecuteNonQuery(sSQL);
                    Log.Write(String.Format("Policy System ID {0} deleted from POLICY_X_WEB and POLICY_CODE_MAPPING tables ", sRowId), m_iClientId);
                    // delete entries from glossary table also
                    //get policy system code first, 
                    // then list of tables to be deleted
                    // then build table name to delete them
                    //delete from policy_code_mapping
                }
                //aaggarwal29: Code mapping changes end
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("SetUpPolicySystem.Delete.Error",m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }

        private XmlElement GetNewElement(string p_sNodeName)
        {
            XmlDocument objXDoc;
            XmlElement objXMLElement;
            objXDoc = new XmlDocument();
            objXMLElement = objXDoc.CreateElement(p_sNodeName);
            objXDoc = null;
            return objXMLElement;
        }
        private XmlElement GetNewEleWithValue(string p_sNewNodeName, string p_sNodeValue)
        {
            XmlDocument objXDoc;
            XmlElement objXMLElement;
            objXDoc = new XmlDocument();
            objXMLElement = objXDoc.CreateElement(p_sNewNodeName);
            objXMLElement.InnerText = p_sNodeValue;
            objXDoc = null;
            return objXMLElement;
        }
        // aaggarwal29: Code mapping changes start ;new methods
        

        private bool IsUniqueTablePrefix(string p_sMappingTablePrefix) //private bool IsUniqueSystemTableName(string p_sSystemTableName)
        {
            bool bResult = true;
            int iOutput = 0;
            string sSQL = "";
            DbConnection objCon = null;
            //sSQL = "SELECT COUNT(*) FROM POLICY_X_WEB WHERE SYS_TABLE_NAME ='" + p_sSystemTableName.Trim() + "'";
            sSQL = "SELECT COUNT(*) FROM POLICY_X_WEB WHERE TABLE_PREFIX ='" + p_sMappingTablePrefix.Trim() + "'";
            objCon = DbFactory.GetDbConnection(m_sConnectString);
            objCon.Open();
            iOutput = objCon.ExecuteInt(sSQL);
            if (iOutput == 0)
                bResult = true;
            else if (iOutput > 0)
                bResult = false;

            objCon.Close();
            return bResult;
        }

        public void GetPolicyDSNDetails(ref string[] sDSNDetails, int p_iPolicySystemId)
        {
            //start - Changed by Nikhil.Upload voucher amount setting
           // sDSNDetails = new string[21];
            sDSNDetails = new string[23];
            //end - Changed by Nikhil.Upload voucher amount setting
            bool bStatus = false;
            string sConnectionStr = string.Empty, sDataDSN = string.Empty, sDataUser = string.Empty, sDataPwd = string.Empty, sDataLibraryList = string.Empty, sServerNm = string.Empty, sDriverNm = string.Empty;
            try
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT * FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID=" + p_iPolicySystemId))
                {
                    if (objRdr.Read())
                    {
                        sConnectionStr = RMCryptography.DecryptString(Conversion.ConvertObjToStr(objRdr.GetValue(24))); // decrypting connection string
                        // Function will split the connection string for server name, Data DSN, Library list (for iSeries DB2), Driver name (for iSeries DB2), Data DSN User, Data DSN Password   
                        bStatus = GetConnectionFields(sConnectionStr, ref sServerNm, ref sDataDSN, ref sDataLibraryList, ref sDriverNm, ref sDataUser, ref sDataPwd);
                         sDSNDetails[0]  = sServerNm;
                         sDSNDetails[1]  = sDataDSN;
                         sDSNDetails[2]  = sDataLibraryList;
                         sDSNDetails[3]  = sDriverNm;
                         sDSNDetails[4]  = sDataUser;
                         sDSNDetails[5]  = sDataPwd;
                         sDSNDetails[6] = Conversion.ConvertObjToStr(objRdr.GetValue("POLICY_SYSTEM_NAME")); //policy system name
                         sDSNDetails[7]  = sConnectionStr;
						 //smahajan22 mits 36315 start
                         sDSNDetails[8] = Conversion.ConvertObjToStr(objRdr.GetValue("CLIENTFILE_FLAG"));  //client file flag
                         sDSNDetails[9] = Conversion.ConvertObjToStr(objRdr.GetValue("RELEASE_VERSION"));  //release version
                         sDSNDetails[10] = Conversion.ConvertObjToStr(objRdr.GetValue("ADDDEFAULT_CLMOFFICE")); //ADDDEFAULT_CLMOFFICE
                         sDSNDetails[11] = Conversion.ConvertObjToStr(objRdr.GetValue("ADDDEFAULT_EXAMINER")); //ADDDEFAULT_EXAMINER
                         sDSNDetails[12] = Conversion.ConvertObjToStr(objRdr.GetValue("ADDDEFAULT_ADJUSTOR")); //ADDDEFAULT_ADJUSTOR
                         sDSNDetails[13] = Conversion.ConvertObjToStr(objRdr.GetValue("ADDDEFAULT_EXAMINERCD")); //ADDDEFAULT_EXAMINERCD
                         sDSNDetails[14] = Conversion.ConvertObjToStr(objRdr.GetValue("DEFAULT_CLMOFFICE")); //DEFAULT_CLMOFFICE
                         sDSNDetails[15] = Conversion.ConvertObjToStr(objRdr.GetValue("DEFAULT_ADJUSTOR")); //DEFAULT_ADJUSTOR
                         sDSNDetails[16] = Conversion.ConvertObjToStr(objRdr.GetValue("DEFAULT_EXAMINER")); //DEFAULT_EXAMINER
                         sDSNDetails[17] = Conversion.ConvertObjToStr(objRdr.GetValue("ENTRYINPMSP0000FORAL")); //ENTRYINPMSP0000FORAL
                         sDSNDetails[18] = Conversion.ConvertObjToStr(objRdr.GetValue("DEFAULT_EXAMINERCD")); //DEFAULT_EXAMINERCD
                         sDSNDetails[19] = Conversion.ConvertObjToStr(objRdr.GetValue("EXEMPTCLAIMID")); //EXEMPTCLAIMID
						//dbisht6 mits 36536 
                         sDSNDetails[20] = Conversion.ConvertObjToStr(objRdr.GetValue("UPLOAD_POLICY_ENTITY_FLAG")); //UPLOAD_POLICY_ENTITY_FLAG
						//	mits 36536 end
                        //smahajan22 mits 36315 end

                         //start - Added by Nikhil.Upload voucher amount setting
                         sDSNDetails[21] = Conversion.ConvertObjToStr(objRdr.GetValue("UPLOAD_VOUCHER_DRFTPMSPAP00")); //UPLOAD_VOUCHER_DRFTPMSPAP00
                        //start - Added by Nikhil.Upload voucher amount setting
                         sDSNDetails[22] = Conversion.ConvertObjToStr(objRdr.GetValue("UPLOAD_PAYEE2_PMSPAP00")); //aaggarwal29 : RMA-7701

                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public string CreatePolSysConnectionString(string[] sDSNDetails)
        {
            StringBuilder connBldr = new StringBuilder();
            string strConnectionString = string.Empty;

            if (sDSNDetails[0].ToLower().IndexOf("iseries") > -1)
            {
                connBldr.AppendFormat("Driver={{{0}}};", sDSNDetails[0]);
                connBldr.AppendFormat("System={0};", sDSNDetails[1]);
                connBldr.AppendFormat("Naming=SQL;");
                connBldr.AppendFormat("DefaultLibraries={0};", sDSNDetails[2] + "," + sDSNDetails[3]);
                connBldr.AppendFormat("UID={0};", sDSNDetails[4]);
                connBldr.AppendFormat("PWD={0};", sDSNDetails[5]);
                strConnectionString = connBldr.ToString();
            }
            else
            {
                strConnectionString = ADODbFactory.BuildRMConnectionString(sDSNDetails[0], sDSNDetails[1], sDSNDetails[2], sDSNDetails[4], sDSNDetails[5]);
            }
            return strConnectionString;
        }
        public bool IsValidPolicyDSN(string[] sDSNDetails, int p_iPolicySystemId, out string sConnectionString)
        {
            string sConnPwd = string.Empty,sDSNField = string.Empty;
            try
            {
                sConnectionString = string.Empty;
                if (sDSNDetails[5] == "##########" && p_iPolicySystemId !=0)
                {
                if (sDSNDetails[6] == "DataDSN")
                    {
                       sDSNField = "DATA_DSN_CONN_STR";
                    }
                else if (sDSNDetails[6] == "ClientDSN")
                   {
                      sDSNField = "CLIENT_DSN_CONN_STR";
                    }
                else if (sDSNDetails[6] == "StagingConn")
                {
                    sDSNField = "STAGING_DSN_CONN_STR";
                }
                
                sConnPwd = RMCryptography.DecryptString(Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnectString, "select " + sDSNField + " from POLICY_X_WEB WHERE POLICY_SYSTEM_ID=" + p_iPolicySystemId)));
                    if (string.IsNullOrEmpty(sConnPwd))
                    {
                        throw new Exception("Password not set for the connection string");
                    }
                    sDSNDetails[5] = sConnPwd.Substring(sConnPwd.ToUpper().IndexOf("PWD=") + 4).Replace(";","");
                }
                sConnectionString = CreatePolSysConnectionString(sDSNDetails);
                if(sDSNDetails[6] == "StagingConn")
                {
                    using (DbConnection objStgConnection = DbFactory.GetDbConnection(sConnectionString))
                    {
                        return true;
                    }
              }
                using (DbReader objRdr = DbFactory.GetDbReader(sConnectionString, "SELECT * FROM PMSPCL42 WHERE 0 = 1"))
                {
                   return true;
                }
            }
            catch (Exception ex)
            { 
                throw ex;
            }
            return false;
        }
        public bool GetConnectionFields(string sConnectionStr, ref string p_sServer, ref string p_sDatabase, ref string p_sLibraryList, ref string p_sDriver, ref string p_sUid, ref string p_sPwd)
        {
            bool bReturn = false;
            int iPos = 0, iCount = 0, iLastPos = 0;
            string sAdditional = string.Empty, sDbq = "", s = "", sKey = "";
            DataSources p_objDS = null;
            string sValue = ""; //sConnectionStr = "", 
            const string DRIVER_KEY = "driver";
            const string DATABASE_KEY = "database";
            const string DATABASESYBASE_KEY = "db";   //--  Sybase
            const string SERVER_KEY = "server";
            const string SERVERSYBASE_KEY = "srvr";   //--  Sybase
            const string SERVERDB2_KEY = "dbalias";   //--  DB2
            const string DBQ_KEY = "dbq";
            const string UID = "uid";
            const string PWD = "pwd";

            const string SYSTEM_KEY = "system";
            const string LIBRARYLIST_KEY = "defaultlibraries";
            const string USER_KEY = "userid";
            const string USER_PASSWORD = "password";
            try
            {
                //--  We can edit only DSN-Less type of the connections
                if (sConnectionStr == string.Empty)
                    return bReturn;
                //if(sConnectionStr.ToLower().IndexOf("iseries") > 0)
                //{
                iLastPos = 0;
                do
                {
                    iPos = sConnectionStr.IndexOf(";", iLastPos);
                    if (iPos == -1)
                        s = sConnectionStr.Substring(iLastPos, sConnectionStr.Length);
                    else
                    {
                        s = sConnectionStr.Substring(iLastPos, iPos - iLastPos);
                        iPos = iPos + 1;
                        if (iPos >= sConnectionStr.Length)
                            iPos = 0;  //--  Last one
                    }

                    if (s != "")
                    {
                        iCount = s.IndexOf("=");
                        if (iCount > 0)
                        {
                            sKey = s.Substring(0, iCount);
                            sValue = s.Substring(iCount + 1);
                            switch (sKey.ToLower())
                            {
                                case DRIVER_KEY:
                                    p_sDriver = sValue.Substring(1, sValue.Length - 2);
                                    break;
                                case DATABASE_KEY:
                                    p_sDatabase = sValue;
                                    break;
                                case LIBRARYLIST_KEY: // for iSeries DB2
                                    p_sDatabase = sValue.Substring(0, sValue.IndexOf(","));
                                    p_sLibraryList = sValue.Substring(sValue.IndexOf(",") + 1);
                                    break;
                                case DATABASESYBASE_KEY:
                                    p_sDatabase = sValue;
                                    break;
                                case SERVER_KEY:
                                    p_sServer = sValue;
                                    break;
                                case SYSTEM_KEY: // for iSeries DB2
                                    p_sServer = sValue;
                                    break;
                                case SERVERSYBASE_KEY:
                                    p_sServer = sValue;
                                    break;
                                case SERVERDB2_KEY:
                                    p_sServer = sValue;
                                    break;
                                case DBQ_KEY:
                                    sDbq = sValue;
                                    break;
                                case UID:
                                    p_sUid = sValue;
                                    break;
                                case USER_KEY: // for iSeries DB2
                                    p_sUid = sValue;
                                    break;
                                case PWD:
                                    p_sPwd = sValue;
                                    break;
                                case USER_PASSWORD: // for iSeries DB2
                                    p_sPwd = sValue;
                                    break;

                                default:
                                    //p_sAddtionalParams = p_sAddtionalParams + s + ";";
                                    break;
                            }
                        }
                    }
                    iLastPos = iPos;
                } while (iLastPos > 0);

                //}
                //else
                //{
                //    p_sServer = string.Empty;
                //    p_sDatabase = string.Empty;
                //    p_sLibraryList = string.Empty;
                //    p_sDriver = string.Empty;
                //    PublicFunctions.m_Security.SetupData(-1,ref p_sServer,ref p_sDatabase,ref p_sDriver,ref sAdditional,out p_objDS);
                //}
                bReturn = true;
            }
            catch (Exception p_objException)
            {
                //throw new DataModelException(Globalization.GetString("RMWinSecurity.SetupData.SetupDataError"), p_objException);
            }
            finally
            {
                //objDataSources = null;
            }
            return bReturn;
        }

        /// <summary>
        /// Function to fetch attribute values from nodes on POINT_VersionVerifier.xml
        /// </summary>
        /// <param name="sPolicySystemTypeCode"></param>
        /// <param name="sAttributeName"></param>
        /// <param name="sVersion"></param>
        /// <returns></returns>
        private string GetVersionDetails(string sPolicySystemTypeCode, string sAttributeName, string sVersion)
            {
            string sFileNm, sFileContent, sVal, sCodeDesc;
            XmlDocument oTemplate = new XmlDocument();

            sFileNm = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "PolicyInterface");

            // sPolicySystemTypeCode
            int iCodeId = Conversion.ConvertStrToInteger(sPolicySystemTypeCode);
            LocalCache objCache = new LocalCache(m_sConnectString,m_iClientId);
            sCodeDesc = objCache.GetShortCode(iCodeId);
            switch (sCodeDesc)
                {
                case "POINT":

                    sFileNm = RMConfigurator.CombineFilePath(sFileNm, "POINT_VersionVerifier.xml");
                    sFileContent = File.ReadAllText(sFileNm);
                    if (!string.IsNullOrEmpty(sFileContent))
                        {
                        oTemplate.LoadXml(sFileContent);

                        foreach (XmlNode oNode in oTemplate.SelectNodes("PolicySystem/POINT"))
                            {
                            if (oNode.Attributes["version"].Value == sVersion)
                                {
                                return oNode.Attributes[sAttributeName].Value;
                                }

                            }
                        }
                    break;
                }


            return string.Empty;

            }

        
    }

    //tanwar2 - Policy Staging
    //ASP.NET is not designed for async operations
    //An async process may end abruptly if app-pool/domain recycles
    //This class lets app-pool know that the app pool is being used
    //and that this should not be recycle now.
    //url: http://haacked.com/archive/2011/10/16/the-dangers-of-implementing-recurring-background-tasks-in-asp-net.aspx/
    public class JobHost : IRegisteredObject
    {
        private readonly object _lock = new object();
        private bool _shuttingDown;
        private readonly string _scriptFileName = "";

        public JobHost()
        {
            HostingEnvironment.RegisterObject(this);
        }

        /// <summary>
        /// This method is called by app domain when it is shutting down.
        /// </summary>
        /// <param name="immediate"></param>
        public void Stop(bool immediate)
        {
            //if lock exists the system will wait
            //for lock to be released
            //This will buy us time to complete the dbUpgrade operation.
            lock (_lock)
            {
                _shuttingDown = true;
            }
            HostingEnvironment.UnregisterObject(this);
        }

        /// <summary>
        /// Creates required table structure for staging database
        /// </summary>
        /// <param name="p_sStagingConnectionString"></param>
        public void ExecuteDBScriptForStaging(string p_sStagingConnectionString)
        {
            lock (_lock)
            {
                //if app-domain is shuttinging down - we can do nothing but abort the process. 
                if (_shuttingDown)
                {
                    return;
                }
                //RMXdBUpgradeWizard.UpdateFunctions.ExecuteSQLScriptExtended(p_sStagingConnectionString, _scriptFileName, string.Empty, true);
            }
        }
    }
}
