﻿
using System;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.Text;
using Riskmaster.Settings;
using Riskmaster.Security;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Web;
using System.Xml.XPath;
using Riskmaster.DataModel;

namespace Riskmaster.Application.PolicySystemInterface
{
    public class ValidatePolicy
    {
        private string m_sConnectString = string.Empty;
        private string m_sUserName = string.Empty;
        private string m_sDSNName = string.Empty;
        private string m_sPassword = string.Empty;
        private int m_iClientId = 0;
       
        public ValidatePolicy(string p_sConn,int p_iClientId)
        {
            m_sConnectString = p_sConn;
            m_iClientId = p_iClientId;
        }

        public ValidatePolicy(UserLogin objUserLogin,int p_iClientId)
        {
            m_sConnectString = objUserLogin.objRiskmasterDatabase.ConnectionString;
            m_sUserName = objUserLogin.LoginName;
            m_sDSNName = objUserLogin.objRiskmasterDatabase.DataSourceName;
            m_sPassword = objUserLogin.Password;
            m_iClientId = p_iClientId;
        }

        public bool GetPolicyTriggerClaimFlag(int p_sPolicyId)
        {
            string oSQL = string.Empty;
            DbReader objReader = null;
            bool bTriggerClaimFlag = false;

            try
            {
                oSQL = "SELECT TRIGGER_CLAIM_FLAG FROM POLICY WHERE POLICY_ID = " + p_sPolicyId;
                objReader = DbFactory.GetDbReader(m_sConnectString, oSQL);
                if (objReader.Read())
                {
                    bTriggerClaimFlag = Conversion.ConvertObjToBool(objReader.GetValue(0), m_iClientId);
                }
            }
            catch (Exception e)
            { 
                throw e;
            }
            finally 
            { 
                if (objReader != null)
                    objReader.Dispose();
            }

            return bTriggerClaimFlag;
        }
        //nsachdeva2 - 7/26/2012
        //public DbReader PolicyValidation(string p_sPolicyId, string p_sEventDate, string p_sClaimDate, string p_sCurrencyCode)
        public DbReader PolicyValidation(string p_sPolicyId, string p_sEventDate, string p_sClaimDate, string p_sCurrencyCode, string p_sPolicyLobCode)
        {
            StringBuilder sbSQL = null;
            LocalCache objCache = null;
            SysSettings objSettings = null;
         //   bool bSuccess = false;
            DbReader objReader = null;
           
            try
            {
                sbSQL = new StringBuilder();
                objCache = new LocalCache(m_sConnectString,m_iClientId);

                sbSQL.Append("SELECT POLICY_NAME,POLICY_ID,POLICY.POLICY_SYSTEM_ID FROM POLICY ");
                sbSQL.Append(" WHERE ((POLICY.POLICY_STATUS_CODE IN (" + objCache.GetCodeId("I", "POLICY_STATUS") + "," + objCache.GetCodeId("E", "POLICY_STATUS") + "))");
                sbSQL.Append(" OR (POLICY.POLICY_STATUS_CODE = " + objCache.GetCodeId("C", "POLICY_STATUS") + " ");
                sbSQL.Append("  AND POLICY.CANCEL_DATE > '" + p_sClaimDate + "'");
                sbSQL.Append("  AND POLICY.TRIGGER_CLAIM_FLAG <> 0)");
                sbSQL.Append(" OR (POLICY.POLICY_STATUS_CODE = " + objCache.GetCodeId("C", "POLICY_STATUS"));
                sbSQL.Append("  AND POLICY.CANCEL_DATE > '" + p_sEventDate + "'");
                sbSQL.Append(" AND POLICY.TRIGGER_CLAIM_FLAG = 0))");
                //JIRA 1342 ajohari2: Start
                //sbSQL.Append(" AND ((POLICY.EFFECTIVE_DATE <= '" + p_sEventDate + "'");
                //sbSQL.Append(" AND POLICY.EXPIRATION_DATE >= '" + p_sEventDate + "'");
                //sbSQL.Append(" AND POLICY.TRIGGER_CLAIM_FLAG = 0)");
                //sbSQL.Append(" OR (POLICY.EFFECTIVE_DATE <= '" + p_sClaimDate + "'");
                //sbSQL.Append(" AND POLICY.EXPIRATION_DATE >= '" + p_sClaimDate + "'");
                //sbSQL.Append(" AND POLICY.TRIGGER_CLAIM_FLAG <> 0))");
                //JIRA 1342 ajohari2: End
                sbSQL.Append(" AND ((POLICY.TRIGGER_CLAIM_FLAG = 0)");
                sbSQL.Append(" OR (POLICY.TRIGGER_CLAIM_FLAG <> 0))");
                sbSQL.Append(" AND POLICY.PRIMARY_POLICY_FLG <>0");

                
                objSettings = new SysSettings(m_sConnectString,m_iClientId);
                //if (Conversion.ConvertStrToBool(objSettings.UseMultiCurrency.ToString()))
                //{
                //    sbSQL.Append(" AND  POLICY.CURRENCY_CODE = " + p_sCurrencyCode);

                //}

              
                sbSQL.Append(" AND POLICY.POLICY_ID IN(" + p_sPolicyId + ")");

                //avipinsrivas Start : As discussed with Sangeet this condition will apply for both policies(external & internal) 
                //if(objSettings.UsePolicyInterface)// && (GetPolicySystemId(Conversion.ConvertStrToInteger(p_sPolicyId))>0))
                sbSQL.Append(" AND POLICY.POLICY_LOB_CODE in (0," + p_sPolicyLobCode+")"); //nsachdeva2 - 7/26/2012
                //p_sPolicyId will have comma separated list of all the policies attched with claim , hence GetPolicySystemId()
                //method will return 0 and policy lob code check is getting missed
                // if a claim has multiple policies, including external and internal
                // policy lob code will be matched for external policies and will be taken as zero for internal policies
                // any external policy whose lob is diff from the one in claim will not be attched to claim while saving.
                objReader = DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString());
                

            }
            catch (Exception e)
            {
                if (objReader != null)
                    objReader.Dispose();
            }
            finally
            {
                sbSQL = null;
                if (objSettings != null)
                    objSettings = null;
                if (objCache != null)
                    objCache.Dispose();
            }
            return objReader;
        }

        /// <summary>
        /// To fetch Policy Number using policy Id
        /// </summary>
        /// <param name="p_sPolicyId"></param>
        /// <returns></returns>
        public string GetPolicyNumber(int p_sPolicyId)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            string sPolicyNum = string.Empty;

            try
            {
                sSQL = "SELECT POLICY_NUMBER FROM POLICY WHERE POLICY_ID = " + p_sPolicyId;
                objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                if (objReader.Read())
                {
                    sPolicyNum = objReader.GetString(0);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }

            return sPolicyNum;
        }

       
    }
}
