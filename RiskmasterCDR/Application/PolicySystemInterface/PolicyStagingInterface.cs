﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.DataModel;
using Riskmaster.Security;
using System.Xml;
using Riskmaster.Common;
using System.IO;
using Riskmaster.Db;
using Riskmaster.Models;
using Riskmaster.Settings;
using Riskmaster.ExceptionTypes;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Riskmaster.Application.PolicySystemInterface
{
    /// <summary>
    /// Contains methods related to rmA Policy Staging download and inquiry
    /// </summary>
    public class PolicyStagingInterface
    {
        private string m_sConnectString = string.Empty;
        private string m_sUserName = string.Empty;
        private string m_sDSNName = string.Empty;
        private string m_sPassword = string.Empty;
        private UserLogin m_oUserLogin;
        private int m_iClientId = 0;

        /// <summary>
        /// Gets Policy Details for Inquiry screen display
        /// </summary>
        /// <param name="p_oUserLogin"></param>
        public PolicyStagingInterface(UserLogin p_oUserLogin,int p_iClientId)
        {
            m_oUserLogin = p_oUserLogin;
            m_sConnectString = p_oUserLogin.objRiskmasterDatabase.ConnectionString;
            m_sUserName = p_oUserLogin.LoginName;
            m_sDSNName = p_oUserLogin.objRiskmasterDatabase.DataSourceName;
            m_sPassword = p_oUserLogin.Password;
            m_iClientId = p_iClientId;
        }

        public string GetPolicyDataFromStaging(int p_iPolicyStagingId)
        {

            DataModelFactory oFactory = null;
            PolicyStgng oPolicyStaging = null;
            EntityStgng oEntityStgng = null;
            XmlDocument xReturnDoc = null;
            XmlNode xRootElement = null;
            XmlNode xPolicySummaryInfo = null;
            XmlNode xInsured = null;
            string sSQL = string.Empty;
            int iInsuredEid = 0;
            bool bSuccess = false;

            string sDocPath = string.Empty;
            xReturnDoc = new XmlDocument();
            try
            {
                oFactory = new DataModelFactory(m_oUserLogin, m_iClientId);
                oPolicyStaging = oFactory.GetDataModelObject("PolicyStgng", false) as PolicyStgng;
                oEntityStgng = oFactory.GetDataModelObject("EntityStgng", false) as EntityStgng;
                if (p_iPolicyStagingId > 0)
                {
                    oPolicyStaging.MoveTo(p_iPolicyStagingId);
                }
                xRootElement = xReturnDoc.CreateNode(XmlNodeType.Element, "PolicyStaging", string.Empty);
                
                xPolicySummaryInfo = xReturnDoc.CreateNode(XmlNodeType.Element, "PolicySummaryInfo", string.Empty);
                xInsured = xReturnDoc.CreateNode(XmlNodeType.Element, "InsuredInfo", string.Empty);

                CreateAndAppendNode("PolicyStatusCd", oPolicyStaging.Context.LocalCache.GetCodeDesc(oPolicyStaging.Context.LocalCache.GetCodeId(oPolicyStaging.PolicyStatusCode, "POLICY_STATUS")), xPolicySummaryInfo, xReturnDoc);
                CreateAndAppendNode("PolicyNumber", oPolicyStaging.PolicyNumber, xPolicySummaryInfo, xReturnDoc);
                CreateAndAppendNode("EffectiveDt", Conversion.GetDBDateFormat(oPolicyStaging.EffectiveDate, "MM/dd/yyyy"), xPolicySummaryInfo, xReturnDoc);
                CreateAndAppendNode("ExpirationDt", Conversion.GetDBDateFormat(oPolicyStaging.ExpirationDate, "MM/dd/yyyy"), xPolicySummaryInfo, xReturnDoc);
                CreateAndAppendNode("LOBCd", oPolicyStaging.PolicyLOB, xPolicySummaryInfo, xReturnDoc);
                CreateAndAppendNode("csc_AgentPremium", oPolicyStaging.Premium.ToString(), xPolicySummaryInfo, xReturnDoc);
                // Pradyumna 1/14/2014 - For Inquiry screen display- Start
                CreateAndAppendNode("CancelledDate", oPolicyStaging.CancelledDate.Trim(), xPolicySummaryInfo, xReturnDoc);
                CreateAndAppendNode("MasterCompany", oPolicyStaging.MasterCompany.Trim(), xPolicySummaryInfo, xReturnDoc);
                CreateAndAppendNode("LocCompany", oPolicyStaging.LocCompany.Trim(), xPolicySummaryInfo, xReturnDoc);
                // Pradyumna 1/14/2014 - For Inquiry screen display- Ends
                iInsuredEid = Conversion.CastToType<int>(oPolicyStaging.PolicyXInsuredStgng.ToString(), out bSuccess);
                if (iInsuredEid > 0)
                {
                    oEntityStgng.MoveTo(iInsuredEid);
                }
                CreateAndAppendNode("StateProvCd", oEntityStgng.StateId, xInsured, xReturnDoc);
                CreateAndAppendNode("CommercialName", (string.Format("{0} {1} {2}", oEntityStgng.FirstName, oEntityStgng.MiddleName, oEntityStgng.LastName)).Trim(), xInsured, xReturnDoc);
                CreateAndAppendNode("Addr1", oEntityStgng.Addr1, xInsured, xReturnDoc);
                CreateAndAppendNode("Addr2", oEntityStgng.Addr2, xInsured, xReturnDoc);
                CreateAndAppendNode("City", oEntityStgng.City, xInsured, xReturnDoc);
                CreateAndAppendNode("StateProvCd", oEntityStgng.StateId, xInsured, xReturnDoc);
                CreateAndAppendNode("PostalCode", oEntityStgng.ZipCode, xInsured, xReturnDoc);
                CreateAndAppendNode("OtherGivenName", oEntityStgng.AlsoKnownAs, xInsured, xReturnDoc);
                // Pradyumna 1/14/2014 - For Inquiry screen display- Start
                foreach (PolicyXEntityStgng objPolicyXEntStg in oPolicyStaging.PolicyXEntityListStgng)
                {
                    int iAgentTableId = 0;
                    iAgentTableId = oFactory.Context.LocalCache.GetTableId("AGENTS");
                    sSQL = "SELECT COUNT(*) FROM ENTITY_STGNG WHERE ENTITY_ID =  " + objPolicyXEntStg.EntityId + " AND ENTITY_TABLE_ID = " + iAgentTableId;

                    int iAgentExists = Conversion.CastToType<int>((DbFactory.ExecuteScalar(m_sConnectString, sSQL)).ToString(), out bSuccess);
                    if (iAgentExists > 0)
                    {
                        StringBuilder sbAgencyName = new StringBuilder();
                        oEntityStgng = null;
                        oEntityStgng = oFactory.GetDataModelObject("EntityStgng", false) as EntityStgng;
                        oEntityStgng.MoveTo(objPolicyXEntStg.EntityId);
                        // Agency Number
                        if (oEntityStgng.ReferenceNumber.Trim() != string.Empty)
                        {
                            CreateAndAppendNode("AgencyNum", oEntityStgng.ReferenceNumber.Trim(), xPolicySummaryInfo, xReturnDoc);
                        }
                        // Agency name
                        if (oEntityStgng.FirstName.Trim() != string.Empty)
                        {
                            sbAgencyName.Append(oEntityStgng.FirstName.Trim());
                        }
                        if (oEntityStgng.MiddleName.Trim() != string.Empty)
                        {
                            if (sbAgencyName.Length > 0)
                                sbAgencyName.Append(" ");
                            sbAgencyName.Append(oEntityStgng.MiddleName.Trim());
                        }
                        if (oEntityStgng.LastName.Trim() != string.Empty)
                        {
                            if (sbAgencyName.Length > 0)
                                sbAgencyName.Append(" ");
                            sbAgencyName.Append(oEntityStgng.LastName.Trim());
                        }
                        CreateAndAppendNode("AgencyName", sbAgencyName.ToString(), xPolicySummaryInfo, xReturnDoc);
                        break;
                    }
                }
                // Pradyumna 1/14/2014 - For Inquiry screen display- Start
                xRootElement.AppendChild(xPolicySummaryInfo);
                xRootElement.AppendChild(xInsured);
                xReturnDoc.AppendChild(xRootElement);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oPolicyStaging != null)
                    oPolicyStaging.Dispose(); 
                if (oEntityStgng != null)
                    oEntityStgng.Dispose();
                if (oFactory != null)
                    oFactory.Dispose();
            }

            return xReturnDoc.OuterXml;
        }

        public string GetUnitList(int p_iStagingPolicyId)
        {
            DataModelFactory oFactory = null;
            PolicyStgng oPolicyStaging = null;
            
            XmlDocument xReturnDoc = null;
            
            XmlNode xRootNode = null;
            XmlNode xVehicleNode = null;
            XmlNode xPropertyNode = null;
            XmlNode xSiteUnitNode = null;
            XmlNode xOtherUnitNode = null;
            XmlNode xGeneralNode = null;

            string sPUD_ColumnName = " UNIT_NUMBER, UNIT_RISK_LOC, UNIT_RISK_SUB_LOC, PRODUCT, INS_LINE, STAT_UNIT_NUMBER ";
            string sVehicleColumnName = " UNIT_TYPE_CODE, VEHICLE_YEAR, VIN, VEHICLE_MAKE, VEHICLE_MODEL ";
            string sPropertyColumnName = " YEAR_OF_CONS, DESCRIPTION, ADDR1, CITY, STATE_ID, ZIP_CODE ";
            string sSiteColumnName = " NAME, ADDR1, CITY, STATE_ID, ZIP_CODE ";

            string sDocPath = string.Empty;
            xReturnDoc = new XmlDocument();
            xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Units", string.Empty);
            xSiteUnitNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Site", string.Empty);
            xOtherUnitNode = xReturnDoc.CreateNode(XmlNodeType.Element, "OtherUnit", string.Empty);

            string sAddress = string.Empty;
            string sCity = string.Empty;
            string sZip = string.Empty;
            string sState = string.Empty;

            try
            {
                oFactory = new DataModelFactory(m_oUserLogin,m_iClientId);
                oPolicyStaging = oFactory.GetDataModelObject("PolicyStgng", false) as PolicyStgng;
                oPolicyStaging.MoveTo(p_iStagingPolicyId);

                foreach (PolicyXUnitStgng oUnitStgng in oPolicyStaging.PolicyXUnitListStgng)
                {
                    xGeneralNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Unit", string.Empty);
                    CreateAndAppendNode("QueryString", string.Format("{0}|{1}", p_iStagingPolicyId, oUnitStgng.PolicyUnitRowId), xGeneralNode, xReturnDoc);

                    using (DbReader oDbReader = oFactory.Context.DbConnLookup.ExecuteReader(string.Format("SELECT {0} FROM POINT_UNIT_DATA_STGNG WHERE UNIT_ID={1} AND UNIT_TYPE='{2}'", sPUD_ColumnName, oUnitStgng.UnitId, oUnitStgng.UnitType.Trim())))
                    {
                        if (oDbReader != null)
                        {
                            if (oDbReader.Read())
                            {
                                if (oDbReader["UNIT_RISK_LOC"] != null && oDbReader["UNIT_RISK_LOC"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("csc_RiskLoc", oDbReader["UNIT_RISK_LOC"].ToString(), xGeneralNode, xReturnDoc);
                                }

                                if (oDbReader["UNIT_RISK_SUB_LOC"] != null && oDbReader["UNIT_RISK_SUB_LOC"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("csc_RiskSubLoc", oDbReader["UNIT_RISK_SUB_LOC"].ToString(), xGeneralNode, xReturnDoc);
                                }

                                //if (oDbReader["UNIT_NUMBER"] != null && oDbReader["UNIT_NUMBER"] != DBNull.Value)
                                //{
                                //    CreateAndAppendNode("csc_UnitNumber", oDbReader["UNIT_NUMBER"].ToString(), xGeneralNode, xReturnDoc);
                                //}

                                if (oDbReader["INS_LINE"] != null && oDbReader["INS_LINE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("csc_InsLineCd", oDbReader["INS_LINE"].ToString(), xGeneralNode, xReturnDoc);
                                }

                                if (oDbReader["PRODUCT"] != null && oDbReader["PRODUCT"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("csc_Product", oDbReader["PRODUCT"].ToString(), xGeneralNode, xReturnDoc);
                                }

                                if (oDbReader["STAT_UNIT_NUMBER"] != null && oDbReader["STAT_UNIT_NUMBER"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("csc_UnitNumber", oDbReader["STAT_UNIT_NUMBER"].ToString(), xGeneralNode, xReturnDoc);
                                    CreateAndAppendNode("csc_StatUnitNumber", oDbReader["STAT_UNIT_NUMBER"].ToString(), xGeneralNode, xReturnDoc);
                                }
                            }
                        }
                    }

                    switch (oUnitStgng.UnitType.Trim())
                    {
                        case "v":
                        case "V":
                            xVehicleNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Vehicle", string.Empty);

                            using (DbReader oDbReader = oFactory.Context.DbConnLookup.ExecuteReader(string.Format("SELECT {0} FROM VEHICLE_STGNG WHERE UNIT_ID={1}", sVehicleColumnName, oUnitStgng.UnitId)))
                            {
                                if (oDbReader != null)
                                {
                                    if (oDbReader.Read())
                                    {
                                        string sYear = string.Empty;
                                        string sVin = string.Empty;
                                        string sModel = string.Empty;
                                        string sMake = string.Empty;

                                        if (oDbReader["UNIT_TYPE_CODE"] != null && oDbReader["UNIT_TYPE_CODE"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("csc_UnitStatus", oDbReader["UNIT_TYPE_CODE"].ToString(), xVehicleNode, xReturnDoc);
                                        }

                                        if (oDbReader["VEHICLE_YEAR"] != null && oDbReader["VEHICLE_YEAR"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("ModelYear", oDbReader["VEHICLE_YEAR"].ToString(), xVehicleNode, xReturnDoc);
                                            sYear = oDbReader["VEHICLE_YEAR"].ToString();
                                        }

                                        if (oDbReader["VIN"] != null && oDbReader["VIN"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("csc_VehicleIdentificationNumber", oDbReader["VIN"].ToString(), xVehicleNode, xReturnDoc);
                                            sVin = oDbReader["VIN"].ToString();
                                        }

                                        if (oDbReader["VEHICLE_MAKE"] != null && oDbReader["VEHICLE_MAKE"] != DBNull.Value)
                                        {
                                            sMake = oDbReader["VEHICLE_MAKE"].ToString();
                                        }

                                        if (oDbReader["VEHICLE_MODEL"] != null && oDbReader["VEHICLE_MODEL"] != DBNull.Value)
                                        {
                                            sModel = oDbReader["VEHICLE_MODEL"].ToString();
                                        }

                                        CreateAndAppendNode("Manufacturer", string.Format("{0} {1} {2} Vin: {3}", new object[] { sYear, sModel, sMake, sVin }), xVehicleNode, xReturnDoc);
                                    }
                                }
                            }
                            xGeneralNode.AppendChild(xVehicleNode);
                            break;
                        case "P":
                        case "p":
                            xPropertyNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Property", string.Empty);

                            using (DbReader oDbReader = oFactory.Context.DbConnLookup.ExecuteReader(string.Format("SELECT {0} FROM PROPERTY_UNIT_STGNG WHERE PROPERTY_ID={1}", sPropertyColumnName, oUnitStgng.UnitId)))
                            {
                                if (oDbReader != null)
                                {
                                    if (oDbReader.Read())
                                    {
                                        if (oDbReader["YEAR_OF_CONS"] != null && oDbReader["YEAR_OF_CONS"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("csc_YearBuilt", oDbReader["YEAR_OF_CONS"].ToString(), xPropertyNode, xReturnDoc);
                                        }

                                        if (oDbReader["DESCRIPTION"] != null && oDbReader["DESCRIPTION"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("csc_UnitDesc", oDbReader["DESCRIPTION"].ToString(), xPropertyNode, xReturnDoc);
                                        }

                                        if (oDbReader["ADDR1"] != null && oDbReader["ADDR1"] != DBNull.Value)
                                        {
                                            sAddress = oDbReader["ADDR1"].ToString();
                                        }

                                        if (oDbReader["CITY"] != null && oDbReader["CITY"] != DBNull.Value)
                                        {
                                            sCity = oDbReader["CITY"].ToString();
                                        }

                                        if (oDbReader["STATE_ID"] != null && oDbReader["STATE_ID"] != DBNull.Value)
                                        {
                                            sState = oDbReader["STATE_ID"].ToString();
                                        }

                                        if (oDbReader["ZIP_CODE"] != null && oDbReader["ZIP_CODE"] != DBNull.Value)
                                        {
                                            sZip = oDbReader["ZIP_CODE"].ToString();
                                        }

                                        CreateAndAppendNode("csc_UnitAddress", string.Format("{0}, {1}, {2}, {3}", new object[] { sAddress, sCity, sState, sZip }), xPropertyNode, xReturnDoc);
                                    }
                                }
                            }
                            xGeneralNode.AppendChild(xPropertyNode);
                            break;
                        case "s":
                        case "S":
                            xSiteUnitNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Site", string.Empty);

                            using (DbReader oDbReader = oFactory.Context.DbConnLookup.ExecuteReader(string.Format("SELECT {0} FROM SITE_UNIT_STGNG WHERE SITE_ID={1}", sSiteColumnName, oUnitStgng.UnitId)))
                            {
                                if (oDbReader != null)
                                {
                                    if (oDbReader.Read())
                                    {
                                        if (oDbReader["NAME"] != null && oDbReader["NAME"] != DBNull.Value)
                                        {
                                            CreateAndAppendNode("csc_UnitDesc", oDbReader["NAME"].ToString(), xSiteUnitNode, xReturnDoc);
                                        }

                                        if (oDbReader["ADDR1"] != null && oDbReader["ADDR1"] != DBNull.Value)
                                        {
                                            sAddress = oDbReader["ADDR1"].ToString();
                                        }

                                        if (oDbReader["CITY"] != null && oDbReader["CITY"] != DBNull.Value)
                                        {
                                            sCity = oDbReader["CITY"].ToString();
                                        }

                                        if (oDbReader["STATE_ID"] != null && oDbReader["STATE_ID"] != DBNull.Value)
                                        {
                                            sState = oDbReader["STATE_ID"].ToString();
                                        }

                                        if (oDbReader["ZIP_CODE"] != null && oDbReader["ZIP_CODE"] != DBNull.Value)
                                        {
                                            sZip = oDbReader["ZIP_CODE"].ToString();
                                        }

                                        CreateAndAppendNode("csc_UnitAddress", string.Format("{0}, {1}, {2}, {3}", new object[] { sAddress, sCity, sState, sZip }), xSiteUnitNode, xReturnDoc);
                                    }
                                }
                            }
                            xGeneralNode.AppendChild(xSiteUnitNode);
                            break;
                        default:
                            break;
                    }

                    xRootNode.AppendChild(xGeneralNode);
                }

                xReturnDoc.AppendChild(xRootNode);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oPolicyStaging!=null)
                {
                    oPolicyStaging.Dispose();
                }
                if (oPolicyStaging!=null)
                {
                    oPolicyStaging.Dispose();
                }
            }

            return xReturnDoc.OuterXml;
        }

        public string GetUnitCoverages(int p_iUnitStagingRowId)
        {
            XmlDocument xDoc = null;
            XmlNode xRootNode = null;
            XmlNode xCvgNode = null;
            DataModelFactory oFactory = null;

            int iCodeId = 0;
            try
            {
                xDoc = new XmlDocument();

                oFactory = new DataModelFactory(m_oUserLogin,m_iClientId);
                xRootNode = xDoc.CreateNode(XmlNodeType.Element, "Coverages", string.Empty);

                using (DbReader oDbReader = oFactory.Context.DbConnLookup.ExecuteReader("SELECT * FROM POLICY_X_CVG_TYPE_STGNG WHERE POLICY_UNIT_ROW_ID=" + p_iUnitStagingRowId))
                {
                    if (oDbReader != null)
                    {
                        while (oDbReader.Read())
                        {
                            xCvgNode = xDoc.CreateNode(XmlNodeType.Element, "Coverage", string.Empty);
                            if (oDbReader["COVERAGE_TYPE_CODE"] != null && oDbReader["COVERAGE_TYPE_CODE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("CoverageCd", oDbReader["COVERAGE_TYPE_CODE"].ToString(), xCvgNode, xDoc);
                                iCodeId = oFactory.Context.LocalCache.GetCodeId(oDbReader["COVERAGE_TYPE_CODE"].ToString().Trim(), "COVERAGE_TYPE");
                                if (iCodeId > 0)
                                {
                                    CreateAndAppendNode("Description", oFactory.Context.LocalCache.GetCodeDesc(iCodeId), xCvgNode, xDoc);
                                }
                                else
                                {
                                    CreateAndAppendNode("Description", oDbReader["COVERAGE_TYPE_CODE"].ToString(), xCvgNode, xDoc);
                                }

                            }
                            if (oDbReader["WRITTEN_PREMIUM"] != null && oDbReader["WRITTEN_PREMIUM"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Premium", oDbReader["WRITTEN_PREMIUM"].ToString(), xCvgNode, xDoc);
                            }
                            else
                            {
                                CreateAndAppendNode("Premium", "0.00", xCvgNode, xDoc);
                            }
                            if (oDbReader["CVG_SEQUENCE_NO"] != null && oDbReader["CVG_SEQUENCE_NO"] != DBNull.Value)
                            {
                                CreateAndAppendNode("CvgSeqNum", oDbReader["CVG_SEQUENCE_NO"].ToString(), xCvgNode, xDoc);
                            }
                            if (oDbReader["SELF_INSURE_DEDUCT"] != null && oDbReader["SELF_INSURE_DEDUCT"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Deductible", oDbReader["SELF_INSURE_DEDUCT"].ToString(), xCvgNode, xDoc);
                            }
                            else
                            {
                                CreateAndAppendNode("Deductible", "0", xCvgNode, xDoc);
                            }
                            if (oDbReader["POLCVG_ROW_ID"] != null && oDbReader["POLCVG_ROW_ID"] != DBNull.Value)
                            {
                                CreateAndAppendNode("PolCvgRowId", string.Format("{0}|{1}", oDbReader["POLCVG_ROW_ID"].ToString(), p_iUnitStagingRowId), xCvgNode, xDoc);
                            }

                            xRootNode.AppendChild(xCvgNode);
                        }
                    }
                }

                xDoc.AppendChild(xRootNode);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
            }
            return xDoc.OuterXml;
        }

        //added by swati agarwal
        public string GetUnitInterestList(int p_iUnitStagingRowId)
        {
            XmlDocument xDoc = null;
            XmlNode xRootNode = null;
            XmlNode xUnitInterestNode = null;
            DataModelFactory oFactory = null;
            StringBuilder sSQLBuild = null;
            xDoc = new XmlDocument();
            DbReader oDbReader = null;

            try
            {
                oFactory = new DataModelFactory(m_oUserLogin,m_iClientId);
                sSQLBuild = new StringBuilder();
                xRootNode = xDoc.CreateNode(XmlNodeType.Element, "InterestList", string.Empty);
                sSQLBuild.Append("SELECT ENTITY_ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME, ADDR1, ADDR2, CITY, TABLE_NAME");
                sSQLBuild.Append(" FROM ENTITY_STGNG ES LEFT OUTER JOIN GLOSSARY_TEXT GT ON GT.TABLE_ID = ES.ENTITY_TABLE_ID ");
                sSQLBuild.Append(" WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM POLICY_X_ENTITY_STGNG WHERE POLICY_UNIT_ROW_ID = " + p_iUnitStagingRowId + ")");
                oDbReader = oFactory.Context.DbConnLookup.ExecuteReader(sSQLBuild.ToString());

                if (oDbReader != null)
                {
                    while (oDbReader.Read())
                    {
                        xUnitInterestNode = xDoc.CreateNode(XmlNodeType.Element, "InterestList", string.Empty);

                        if (oDbReader["ENTITY_ID"] != null && oDbReader["ENTITY_ID"] != DBNull.Value)
                        {
                            CreateAndAppendNode("EntityId", oDbReader["ENTITY_ID"].ToString(), xUnitInterestNode, xDoc);
                        }
                        if (oDbReader["LAST_NAME"] != null && oDbReader["LAST_NAME"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Lastname", oDbReader["LAST_NAME"].ToString().Trim(), xUnitInterestNode, xDoc);
                        }
                        if (oDbReader["MIDDLE_NAME"] != null && oDbReader["MIDDLE_NAME"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Middlename", oDbReader["MIDDLE_NAME"].ToString().Trim(), xUnitInterestNode, xDoc);
                        }
                        if (oDbReader["FIRST_NAME"] != null && oDbReader["FIRST_NAME"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Firstname", oDbReader["FIRST_NAME"].ToString().Trim(), xUnitInterestNode, xDoc);
                        }
                        if (oDbReader["ADDR1"] != null && oDbReader["ADDR1"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Addr1", oDbReader["ADDR1"].ToString().Trim(), xUnitInterestNode, xDoc);
                        }
                        if (oDbReader["ADDR2"] != null && oDbReader["ADDR2"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Addr2", oDbReader["ADDR2"].ToString().Trim(), xUnitInterestNode, xDoc);
                        }
                        if (oDbReader["CITY"] != null && oDbReader["CITY"] != DBNull.Value)
                        {
                            CreateAndAppendNode("City", oDbReader["CITY"].ToString().Trim(), xUnitInterestNode, xDoc);
                        }
                        if (oDbReader["TABLE_NAME"] != null && oDbReader["TABLE_NAME"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Role", oDbReader["TABLE_NAME"].ToString().Trim(), xUnitInterestNode, xDoc);
                        }

                        xRootNode.AppendChild(xUnitInterestNode);
                    }
                }

                xDoc.AppendChild(xRootNode);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oDbReader != null)
                {
                    oDbReader.Dispose();
                }
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
            }
            return xDoc.OuterXml;
        }

        public string GetPSEndorsementData(int p_iPolicyId, string p_sTableName, int p_iUnitRowId)
        {
            LocalCache objCache = null;
            XmlDocument xDoc = null;
            XmlNode xRootNode = null;
            XmlNode xFormDataNode = null;
            DataModelFactory oFactory = null;
            StringBuilder sSQLBuild = null;
            xDoc = new XmlDocument();
            DbReader oDbReader = null;
            int iTableId = 0;
            try
            {
                objCache = new LocalCache(m_sConnectString, m_iClientId);
                oFactory = new DataModelFactory(m_oUserLogin, m_iClientId);
                sSQLBuild = new StringBuilder();

                iTableId = objCache.GetTableId(p_sTableName);
                xRootNode = xDoc.CreateNode(XmlNodeType.Element, "FormData", string.Empty);
                sSQLBuild.Append("SELECT * FROM PS_ENDORSEMENT_STGNG WHERE POLICY_ID = " + p_iPolicyId + "  AND TABLE_ID = " + iTableId + " AND ROW_ID = " + p_iUnitRowId);
                oDbReader = oFactory.Context.DbConnLookup.ExecuteReader(sSQLBuild.ToString());

                if (oDbReader != null)
                {
                    while (oDbReader.Read())
                    {
                        xFormDataNode = xDoc.CreateNode(XmlNodeType.Element, "FormData", string.Empty);

                        if (oDbReader["FORM_NUMBER"] != null && oDbReader["FORM_NUMBER"] != DBNull.Value)
                        {
                            CreateAndAppendNode("FormNumber", oDbReader["FORM_NUMBER"].ToString(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["FORM_DATE"] != null && oDbReader["FORM_DATE"] != DBNull.Value)
                        {
                            CreateAndAppendNode("FormDate", Conversion.GetDate(oDbReader["FORM_DATE"].ToString().Trim()), xFormDataNode, xDoc);
                        }
                        if (oDbReader["FORM_DESCRIPTION"] != null && oDbReader["FORM_DESCRIPTION"] != DBNull.Value)
                        {
                            CreateAndAppendNode("FormDescription", oDbReader["FORM_DESCRIPTION"].ToString().Trim(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["STAT"] != null && oDbReader["STAT"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Stat", oDbReader["STAT"].ToString().Trim(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["INS_LINE"] != null && oDbReader["INS_LINE"] != DBNull.Value)
                        {
                            CreateAndAppendNode("InsLine", oDbReader["INS_LINE"].ToString().Trim(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["LOC"] != null && oDbReader["LOC"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Loc", oDbReader["LOC"].ToString().Trim(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["BLDG"] != null && oDbReader["BLDG"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Bldg", oDbReader["BLDG"].ToString().Trim(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["UNIT"] != null && oDbReader["UNIT"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Unit", oDbReader["UNIT"].ToString().Trim(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["FORM_ACTION"] != null && oDbReader["FORM_ACTION"] != DBNull.Value)
                        {
                            CreateAndAppendNode("FormAction", oDbReader["FORM_ACTION"].ToString().Trim(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["EZ_SCM"] != null && oDbReader["EZ_SCM"] != DBNull.Value)
                        {
                            CreateAndAppendNode("EzScm", oDbReader["EZ_SCM"].ToString().Trim(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["DATA"] != null && oDbReader["DATA"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Data", oDbReader["DATA"].ToString().Trim(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["ITERATIVE"] != null && oDbReader["ITERATIVE"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Iterative", oDbReader["ITERATIVE"].ToString().Trim(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["EDITIONDATE"] != null && oDbReader["EDITIONDATE"] != DBNull.Value)
                        {
                            CreateAndAppendNode("EditionDate", oDbReader["EDITIONDATE"].ToString().Trim(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["RATEOP"] != null && oDbReader["RATEOP"] != DBNull.Value)
                        {
                            CreateAndAppendNode("RateOp", oDbReader["RATEOP"].ToString().Trim(), xFormDataNode, xDoc);
                        }
                        if (oDbReader["ENTRYDTE"] != null && oDbReader["ENTRYDTE"] != DBNull.Value)
                        {
                            CreateAndAppendNode("EntryDte", oDbReader["ENTRYDTE"].ToString().Trim(), xFormDataNode, xDoc);
                        }

                        xRootNode.AppendChild(xFormDataNode);
                    }
                }

                xDoc.AppendChild(xRootNode);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oDbReader!=null)
                {
                    oDbReader.Dispose();
                }
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
                if (objCache!=null)
                {
                    objCache.Dispose();
                }
            }
            return xDoc.OuterXml;
        }

        public string GetStgngPolicyInterestDetail(int p_iEntityStagingId)
        {
            XmlDocument xDoc = null;
            XmlNode xRootNode = null;
            XmlNode xAdditionalInterestNode = null;
            DataModelFactory oFactory = null;
            
            StringBuilder sSQLBuild = null;
            xDoc = new XmlDocument();
            DbReader oDbReader = null;

            try
            {
                sSQLBuild = new StringBuilder();
                oFactory = new DataModelFactory(m_oUserLogin,m_iClientId);
                xRootNode = xDoc.CreateNode(XmlNodeType.Element, "AdditionalInterestList", string.Empty);
                sSQLBuild.Append("SELECT FIRST_NAME, MIDDLE_NAME, LAST_NAME, ADDR1, ADDR2, CITY, STATE_ID, ZIP_CODE,COUNTY,TAX_ID, TABLE_NAME, ABBREVIATION, EFF_START_DATE, EFF_END_DATE");
                sSQLBuild.Append(" FROM ENTITY_STGNG ES LEFT OUTER JOIN GLOSSARY_TEXT GT ON GT.TABLE_ID = ES.ENTITY_TABLE_ID");
                sSQLBuild.Append(" WHERE ENTITY_ID = " + p_iEntityStagingId);
                oDbReader = oFactory.Context.DbConnLookup.ExecuteReader(sSQLBuild.ToString());

                if (oDbReader != null)
                {
                    if (oDbReader.Read())
                    {
                        xAdditionalInterestNode = xDoc.CreateNode(XmlNodeType.Element, "AdditionalInterestList", string.Empty);
                        if (oDbReader["TABLE_NAME"] != null && oDbReader["TABLE_NAME"] != DBNull.Value)
                        {
                            CreateAndAppendNode("InterestType", oDbReader["TABLE_NAME"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["LAST_NAME"] != null && oDbReader["LAST_NAME"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Lastname", oDbReader["LAST_NAME"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["FIRST_NAME"] != null && oDbReader["FIRST_NAME"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Firstname", oDbReader["FIRST_NAME"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["MIDDLE_NAME"] != null && oDbReader["MIDDLE_NAME"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Middlename", oDbReader["MIDDLE_NAME"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["ABBREVIATION"] != null && oDbReader["ABBREVIATION"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Abbreviation", oDbReader["ABBREVIATION"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["ADDR1"] != null && oDbReader["ADDR1"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Addr1", oDbReader["ADDR1"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["ADDR2"] != null && oDbReader["ADDR2"] != DBNull.Value)
                        {
                            CreateAndAppendNode("Addr2", oDbReader["ADDR2"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["CITY"] != null && oDbReader["CITY"] != DBNull.Value)
                        {
                            CreateAndAppendNode("City", oDbReader["CITY"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["STATE_ID"] != null && oDbReader["STATE_ID"] != DBNull.Value)
                        {
                            CreateAndAppendNode("State", oDbReader["STATE_ID"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["ZIP_CODE"] != null && oDbReader["ZIP_CODE"] != DBNull.Value)
                        {
                            CreateAndAppendNode("ZipCode", oDbReader["ZIP_CODE"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["TAX_ID"] != null && oDbReader["TAX_ID"] != DBNull.Value)
                        {
                            CreateAndAppendNode("TaxId", oDbReader["TAX_ID"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["EFF_START_DATE"] != null && oDbReader["EFF_START_DATE"] != DBNull.Value)
                        {
                            CreateAndAppendNode("EffectiveDate", Conversion.GetDate(oDbReader["EFF_START_DATE"].ToString().Trim()), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["EFF_END_DATE"] != null && oDbReader["EFF_END_DATE"] != DBNull.Value)
                        {
                            CreateAndAppendNode("ExpirationDate", Conversion.GetDate(oDbReader["EFF_END_DATE"].ToString().Trim()), xAdditionalInterestNode, xDoc);
                        }
                        if (oDbReader["COUNTY"] != null && oDbReader["COUNTY"] != DBNull.Value)
                        {
                            CreateAndAppendNode("County", oDbReader["COUNTY"].ToString().Trim(), xAdditionalInterestNode, xDoc);
                        }

                        xRootNode.AppendChild(xAdditionalInterestNode);
                    }
                }

                xDoc.AppendChild(xRootNode);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oDbReader!= null)
                {
                    oDbReader.Dispose();
                }
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
            }
            return xDoc.OuterXml;
        }

        public bool SavePSEndorsementData(int p_iPolicyId, string p_sTableName, int p_TableRowid, int p_iDownloadedPolicyId, int p_iDownloadedUnitId)
        {
            LocalCache objCache = null;
            StringBuilder sbSQL;
            DbConnection objCon = null;

            DbReader oDbReader = null;
            int iTableId = 0;
            bool bReturn = false;
            DataModelFactory oFactory = null;

            string sFormNo = string.Empty;
            string sFormDesc = string.Empty;
            string sEditionDt = string.Empty;
            string sFormTextContent = string.Empty;
            string sFormDataArea = string.Empty;
            string sCscStat = string.Empty;
            string sCscUnit = string.Empty;
            string sCscBldg = string.Empty;
            string sCscLoc = string.Empty;
            string sCscInsLineCd = string.Empty;
            string sCscEZScrn = string.Empty;
            string sItrNo = string.Empty;
            string sRateOp = string.Empty;
            string sEntryDte = string.Empty;
            DbWriter writer = null;
            string sInsertedFormNo = string.Empty;

            try
            {
                objCache = new LocalCache(m_sConnectString,m_iClientId);
                objCon = DbFactory.GetDbConnection(m_sConnectString);
                oFactory = new DataModelFactory(m_oUserLogin,m_iClientId);
                sbSQL = new StringBuilder();

                iTableId = objCache.GetTableId(p_sTableName);
                sbSQL.Append("SELECT * FROM PS_ENDORSEMENT_STGNG WHERE POLICY_ID = " + p_iPolicyId + " AND TABLE_ID = " + iTableId);
                sbSQL.Append(" AND ROW_ID = " + p_TableRowid);
                oDbReader = oFactory.Context.DbConnLookup.ExecuteReader(sbSQL.ToString());
                if (oDbReader != null)
                {
                    while (oDbReader.Read())
                    {
                        sFormNo = oDbReader["FORM_NUMBER"].ToString().Trim();
                        sFormDesc = oDbReader["FORM_DESCRIPTION"].ToString().Trim();
                        sCscStat = oDbReader["STAT"].ToString().Trim();
                        sCscInsLineCd = oDbReader["INS_LINE"].ToString().Trim();
                        sCscLoc = oDbReader["LOC"].ToString().Trim();
                        sCscBldg = oDbReader["BLDG"].ToString().Trim();
                        sCscUnit = oDbReader["UNIT"].ToString().Trim();
                        sFormTextContent = oDbReader["FORM_ACTION"].ToString().Trim();
                        sCscEZScrn = oDbReader["EZ_SCM"].ToString().Trim();
                        sFormDataArea = oDbReader["DATA"].ToString().Trim();
                        sEditionDt = oDbReader["EDITIONDATE"].ToString().Trim();
                        sItrNo = oDbReader["ITERATIVE"].ToString().Trim();
                        sRateOp = oDbReader["RATEOP"].ToString().Trim();
                        sEntryDte = oDbReader["ENTRYDTE"].ToString().Trim();

                        writer = DbFactory.GetDbWriter(objCon);

                        writer.Tables.Add("PS_ENDORSEMENT");
                        writer.Fields.Add("POLICY_ID", p_iDownloadedPolicyId);
                        writer.Fields.Add("TABLE_ID", iTableId);
                        writer.Fields.Add("ROW_ID", p_iDownloadedUnitId);
                        writer.Fields.Add("FORM_NUMBER", sFormNo);
                        writer.Fields.Add("FORM_DESCRIPTION", sFormDesc);
                        writer.Fields.Add("STAT", sCscStat);
                        writer.Fields.Add("INS_LINE", sCscInsLineCd);
                        writer.Fields.Add("LOC", sCscLoc);
                        writer.Fields.Add("BLDG", sCscBldg);
                        writer.Fields.Add("UNIT", sCscUnit);
                        writer.Fields.Add("FORM_ACTION", sFormTextContent);
                        writer.Fields.Add("EZ_SCM", sCscEZScrn);
                        writer.Fields.Add("DATA", sFormDataArea);
                        writer.Fields.Add("EDITIONDATE", sEditionDt);
                        writer.Fields.Add("ITERATIVE", sItrNo);
                        writer.Fields.Add("DTTM_RCD_ADDED", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                        writer.Fields.Add("ADDED_BY_USER", m_sUserName.ToString());
                        writer.Fields.Add("RATEOP", sRateOp);
                        writer.Fields.Add("ENTRYDTE", sEntryDte);
                        writer.Execute();
                    }
                }

                bReturn = true;
                return bReturn;
            }

            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicyStagingInterface.SavePSEndorsementData.Error",m_iClientId), p_objExp);
            }
            finally
            {
                writer = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Close();
                    objCon.Dispose();
                }
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
            }
        }
        //change end here by swati

        /// <summary>
        /// Gets Policy Interest list 
        /// </summary>
        /// <param name="p_iPolicyStagingId"></param>
        /// <returns></returns>
        /// Pradyumna WWIG GAP 16 1/6/2014
        public string GetPolicyInterestList(int p_iPolicyStagingId)
        {
            XmlDocument xReturnDoc = null;
            DataModelFactory oDMF = null;
            XmlNode xRootNode = null;
            XmlNode xPolicyInterestNode = null;
            string sSQL = string.Empty;
            int iTableId = 0;
            try
            {
                oDMF = new DataModelFactory(m_oUserLogin,m_iClientId);
                xReturnDoc = new XmlDocument();
                iTableId = oDMF.Context.LocalCache.GetTableId("DRIVERS");
                sSQL = "SELECT ES.ENTITY_ID, ES.FIRST_NAME, ES.MIDDLE_NAME, ES.LAST_NAME, ES.ADDR1, ES.ADDR2, ES.CITY, GT.TABLE_NAME FROM ENTITY_STGNG ES INNER JOIN POLICY_X_ENTITY_STGNG PXE ON PXE.ENTITY_ID = ES.ENTITY_ID LEFT OUTER JOIN GLOSSARY_TEXT GT ON GT.TABLE_ID = ES.ENTITY_TABLE_ID WHERE PXE.POLICY_ID = " + p_iPolicyStagingId + " AND ES.ENTITY_TABLE_ID <> " + iTableId + " AND PXE.POLICY_UNIT_ROW_ID = 0";
                //sSQL = "SELECT ENTITY_ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME, ADDR1, ADDR2, CITY, TABLE_NAME FROM ENTITY_STGNG ES LEFT OUTER JOIN GLOSSARY_TEXT GT ON GT.TABLE_ID = ES.ENTITY_TABLE_ID WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM POLICY_X_ENTITY_STGNG WHERE POLICY_ID = " + p_iPolicyStagingId + ")";
                using (DbReader oDbReader = oDMF.Context.DbConnLookup.ExecuteReader(sSQL))
                {
                    xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "PolicyInterestList", string.Empty);
                    if (oDbReader != null)
                    {
                        while (oDbReader.Read())
                        {
                            xPolicyInterestNode = xReturnDoc.CreateNode(XmlNodeType.Element, "InterestList", string.Empty);

                            if (oDbReader["ENTITY_ID"] != null && oDbReader["ENTITY_ID"] != DBNull.Value)
                            {
                                CreateAndAppendNode("EntityId", oDbReader["ENTITY_ID"].ToString(), xPolicyInterestNode, xReturnDoc);

                                if (oDbReader["FIRST_NAME"] != null && oDbReader["FIRST_NAME"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Firstname", oDbReader["FIRST_NAME"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                }
                                if (oDbReader["MIDDLE_NAME"] != null && oDbReader["MIDDLE_NAME"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Middlename", oDbReader["MIDDLE_NAME"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                }
                                if (oDbReader["LAST_NAME"] != null && oDbReader["LAST_NAME"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Lastname", oDbReader["LAST_NAME"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                }
                                if (oDbReader["ADDR1"] != null && oDbReader["ADDR1"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Addr1", oDbReader["ADDR1"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                }
                                if (oDbReader["ADDR2"] != null && oDbReader["ADDR2"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Addr2", oDbReader["ADDR2"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                }
                                if (oDbReader["CITY"] != null && oDbReader["CITY"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("City", oDbReader["CITY"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                }
                                if (oDbReader["TABLE_NAME"] != null && oDbReader["TABLE_NAME"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Role", oDbReader["TABLE_NAME"].ToString().Trim(), xPolicyInterestNode, xReturnDoc);
                                }
                            }
                            xRootNode.AppendChild(xPolicyInterestNode);
                        }
                    }
                }
                xReturnDoc.AppendChild(xRootNode);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (oDMF != null)
                {
                    oDMF.Dispose();
                }
                xRootNode = null;
                xPolicyInterestNode = null;
            }

            return xReturnDoc.OuterXml;
        }

        /// <summary>
        /// Gets Policy Driver list 
        /// </summary>
        /// <param name="p_iPolicyStagingId"></param>
        /// <returns></returns>
        /// Pradyumna WWIG GAP 16 1/8/2014
        public string GetDriverList(int p_iPolicyStagingId)
        {
            XmlDocument xReturnDoc = null;
            DataModelFactory oDMF = null;
            XmlNode xRootNode = null;
            XmlNode xPolicyDriverNode = null;
            string sSQL = string.Empty;
            int iTableId = 0;
            try
            {
                oDMF = new DataModelFactory(m_oUserLogin,m_iClientId);
                xReturnDoc = new XmlDocument();
                iTableId = oDMF.Context.LocalCache.GetTableId("DRIVERS");
                sSQL = "SELECT ES.ENTITY_ID, ES.FIRST_NAME, ES.MIDDLE_NAME, ES.LAST_NAME, ES.STATE_ID, DS.DRIVER_TYPE, DS.LICENCE_NUMBER FROM ENTITY_STGNG ES INNER JOIN POLICY_X_ENTITY_STGNG PXE ON PXE.ENTITY_ID = ES.ENTITY_ID INNER JOIN DRIVER_STGNG DS ON DS.DRIVER_EID = ES.ENTITY_ID WHERE PXE.POLICY_ID = " + p_iPolicyStagingId + " AND ES.ENTITY_TABLE_ID = " + iTableId;
                using (DbReader oDbReader = oDMF.Context.DbConnLookup.ExecuteReader(sSQL))
                {
                    xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "PolicyDriverList", string.Empty);
                    if (oDbReader != null)
                    {
                        while (oDbReader.Read())
                        {
                            xPolicyDriverNode = xReturnDoc.CreateNode(XmlNodeType.Element, "DriverList", string.Empty);

                            if (oDbReader["ENTITY_ID"] != null && oDbReader["ENTITY_ID"] != DBNull.Value)
                            {
                                CreateAndAppendNode("EntityId", oDbReader["ENTITY_ID"].ToString(), xPolicyDriverNode, xReturnDoc);

                                if (oDbReader["FIRST_NAME"] != null && oDbReader["FIRST_NAME"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Firstname", oDbReader["FIRST_NAME"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                }
                                if (oDbReader["MIDDLE_NAME"] != null && oDbReader["MIDDLE_NAME"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Middlename", oDbReader["MIDDLE_NAME"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                }
                                if (oDbReader["LAST_NAME"] != null && oDbReader["LAST_NAME"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Lastname", oDbReader["LAST_NAME"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                }
                                if (oDbReader["STATE_ID"] != null && oDbReader["STATE_ID"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("State", oDbReader["STATE_ID"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                }
                                if (oDbReader["DRIVER_TYPE"] != null && oDbReader["DRIVER_TYPE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("DriverType", oDbReader["DRIVER_TYPE"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                }
                                if (oDbReader["LICENCE_NUMBER"] != null && oDbReader["LICENCE_NUMBER"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("LicenceNum", oDbReader["LICENCE_NUMBER"].ToString().Trim(), xPolicyDriverNode, xReturnDoc);
                                }
                            }
                            xRootNode.AppendChild(xPolicyDriverNode);
                        }
                    }
                }
                xReturnDoc.AppendChild(xRootNode);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (oDMF != null)
                    oDMF.Dispose();
                xRootNode = null;
                xPolicyDriverNode = null;
            }

            return xReturnDoc.OuterXml;
        }

        /// <summary>
        /// Get Driver Detail from Staging Table
        /// </summary>
        /// <param name="p_iStagingEntityId"></param>
        /// <returns></returns>
        public string GetDriverDetails(int p_iStagingEntityId)
        {
            DataModelFactory oFactory = null;
            XmlDocument xReturnDoc = null;
            XmlNode xRootNode = null;
            string sSQL = string.Empty;

            try
            {
                xReturnDoc = new XmlDocument();
                oFactory = new DataModelFactory(m_oUserLogin,m_iClientId);

                sSQL = "SELECT DS.DRIVER_TYPE, ES.FIRST_NAME, ES.MIDDLE_NAME, ES.LAST_NAME, ES.SUFFIX_COMMON, ES.BIRTH_DATE, ES.SEX_CODE, DS.MARITAL_STAT_CODE, DS.LICENCE_NUMBER, DS.LICENCE_DATE, DS.RELTN_INSRD, DS.LICENCE_STATE, DS.MVR_IND, DS.DRIVER_STATUS, DS.SR, DS.DRIVER_CLASS, DS.PO1, DS.PO2, DS.PO3, DS.PTO1, DS.PTO2, DS.PTO3 FROM ENTITY_STGNG ES INNER JOIN DRIVER_STGNG DS ON DS.DRIVER_EID = ES.ENTITY_ID WHERE ENTITY_ID = " + p_iStagingEntityId;

                using (DbReader oDbReader = oFactory.Context.DbConnLookup.ExecuteReader(sSQL))
                {
                    xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "DriverDetail", string.Empty);

                    if (oDbReader != null)
                    {
                        if (oDbReader.Read())
                        {
                            if (oDbReader["DRIVER_TYPE"] != null && oDbReader["DRIVER_TYPE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("DriverType", oDbReader["DRIVER_TYPE"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["FIRST_NAME"] != null && oDbReader["FIRST_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("FirstName", oDbReader["FIRST_NAME"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["MIDDLE_NAME"] != null && oDbReader["MIDDLE_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("MiddleName", oDbReader["MIDDLE_NAME"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["LAST_NAME"] != null && oDbReader["LAST_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("LastName", oDbReader["LAST_NAME"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["SUFFIX_COMMON"] != null && oDbReader["SUFFIX_COMMON"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Suffix", oDbReader["SUFFIX_COMMON"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["BIRTH_DATE"] != null && oDbReader["BIRTH_DATE"] != DBNull.Value)
                            {
                                int iAge = 0;
                                string sBirthDate = oDbReader["BIRTH_DATE"].ToString().Trim();
                                if (sBirthDate.Length == 8)
                                {
                                    string sBirthDateUI = Conversion.GetDBDateFormat(sBirthDate, "MM/dd/yyyy");
                                    CreateAndAppendNode("DOB", sBirthDateUI, xRootNode, xReturnDoc);

                                    DateTime dtBirthDate = Conversion.ToDate(sBirthDate);
                                    iAge = CalculateAge(dtBirthDate);

                                    CreateAndAppendNode("Age", iAge.ToString(), xRootNode, xReturnDoc);
                                }
                            }
                            if (oDbReader["SEX_CODE"] != null && oDbReader["SEX_CODE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("SexCode", oDbReader["SEX_CODE"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["MARITAL_STAT_CODE"] != null && oDbReader["MARITAL_STAT_CODE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("MaritalStatus", oDbReader["MARITAL_STAT_CODE"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["LICENCE_NUMBER"] != null && oDbReader["LICENCE_NUMBER"] != DBNull.Value)
                            {
                                CreateAndAppendNode("LicNo", oDbReader["LICENCE_NUMBER"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["LICENCE_DATE"] != null && oDbReader["LICENCE_DATE"] != DBNull.Value)
                            {
                                string sLicDate = oDbReader["LICENCE_DATE"].ToString().Trim();
                                if (sLicDate.Length == 8)
                                {
                                    sLicDate = Conversion.GetDBDateFormat(sLicDate, "MM/dd/yyyy");
                                    CreateAndAppendNode("LicDate", sLicDate, xRootNode, xReturnDoc);
                                }
                            }
                            if (oDbReader["RELTN_INSRD"] != null && oDbReader["RELTN_INSRD"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Relation", oDbReader["RELTN_INSRD"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["LICENCE_STATE"] != null && oDbReader["LICENCE_STATE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("LicState", oDbReader["LICENCE_STATE"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["MVR_IND"] != null && oDbReader["MVR_IND"] != DBNull.Value)
                            {
                                CreateAndAppendNode("MVRInd", oDbReader["MVR_IND"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["DRIVER_STATUS"] != null && oDbReader["DRIVER_STATUS"] != DBNull.Value)
                            {
                                CreateAndAppendNode("DriverStatus", oDbReader["DRIVER_STATUS"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["SR"] != null && oDbReader["SR"] != DBNull.Value)
                            {
                                CreateAndAppendNode("SR", oDbReader["SR"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["DRIVER_CLASS"] != null && oDbReader["DRIVER_CLASS"] != DBNull.Value)
                            {
                                CreateAndAppendNode("DriverCls", oDbReader["DRIVER_CLASS"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["PO1"] != null && oDbReader["PO1"] != DBNull.Value)
                            {
                                CreateAndAppendNode("PO1", oDbReader["PO1"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["PO2"] != null && oDbReader["PO2"] != DBNull.Value)
                            {
                                CreateAndAppendNode("PO2", oDbReader["PO2"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["PO3"] != null && oDbReader["PO3"] != DBNull.Value)
                            {
                                CreateAndAppendNode("PO3", oDbReader["PO3"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["PTO1"] != null && oDbReader["PTO1"] != DBNull.Value)
                            {
                                CreateAndAppendNode("PTO1", oDbReader["PTO1"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["PTO2"] != null && oDbReader["PTO2"] != DBNull.Value)
                            {
                                CreateAndAppendNode("PTO2", oDbReader["PTO2"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                            if (oDbReader["PTO3"] != null && oDbReader["PTO3"] != DBNull.Value)
                            {
                                CreateAndAppendNode("PTO3", oDbReader["PTO3"].ToString().Trim(), xRootNode, xReturnDoc);
                            }
                        }
                    }
                    xReturnDoc.AppendChild(xRootNode);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if(oFactory != null)
                    oFactory.Dispose();
                xRootNode = null;
            }
			
            return xReturnDoc.OuterXml;
        }

        /// <summary>
        /// Calculates Age using BirthDate as an input
        /// </summary>
        /// <param name="dtBirthDate"></param>
        /// <returns>Age</returns>
        private int CalculateAge(DateTime dtBirthDate)
        {
            int iAge = 0;
            DateTime dtCurrent = DateTime.Now;

            iAge = dtCurrent.Year - dtBirthDate.Year;
            if (dtCurrent.Month < dtBirthDate.Month || (dtCurrent.Month == dtBirthDate.Month && dtCurrent.Day < dtBirthDate.Day))
                iAge--;
            return iAge;
        }

        public XmlDocument GetXmlData(int p_iStagingPolicyId, string p_sRequestMode, ref string p_sMode)
        {
            string sFileContent = string.Empty;
            XmlDocument objXmlDocument = null;
            string sSQL = string.Empty;
            string sFilePath = string.Empty;
            bool bExists = false;

            try
            {
                switch (p_sRequestMode.ToUpper())
                {
                    case "ENTITY":
                        objXmlDocument = GetPolicyXEntityStagingData(p_iStagingPolicyId,p_sRequestMode, out bExists);
                        if (!bExists)
                        {
                            GetXmlData(p_iStagingPolicyId, "Driver", ref p_sMode);
                        }
                        else
                        {
                            p_sMode = "entity";
                        }
                        break;
                    case "DRIVER":
                        objXmlDocument = GetPolicyXEntityStagingData(p_iStagingPolicyId, p_sRequestMode, out bExists);
                        if (!bExists)
                        {
                            GetXmlData(p_iStagingPolicyId, "Unit", ref p_sMode);
                        }
                        else
                        {
                            p_sMode = "driver";
                        }
                        break;
                    case "UNIT":
                        objXmlDocument = new XmlDocument();
                        p_sMode = "unit";
                        break;
                    case "UNITINTEREST":
                        objXmlDocument = GetPolicyXEntityStagingData(p_iStagingPolicyId, p_sRequestMode, out bExists);
                        if (bExists)
                        {
                            p_sMode = "unitinterest";
                        }
                        else
                        {
                            p_sMode = "completed";
                        }
                        break;
                    default:
                        p_sMode = "completed";
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }
            return objXmlDocument;
        }

        private XmlDocument GetPolicyXEntityStagingData(int p_iStagingPolicyId, string p_sMode, out bool p_bExists)
        {
            XmlDocument xReturnDoc = null;
            XmlNode xRootNode = null;
            XmlNode xEntityNode = null;
            XmlNode xInstanceNode = null;
            p_bExists = false;

            PolicyStgng oPolicyStgng = null;
            DataModelFactory oFactory = null;
            EntityStgng oEntityStgng = null;
            PolicyXUnitStgng oPolXUnitStgng = null;

            int iAgentsTableId = 0;
            int iInusredTableId = 0;
            int iDriversTableId = 0;
            string sStatUnitNum = string.Empty;

            try
            {
                xReturnDoc = new XmlDocument();
                xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Entities", string.Empty);

                xInstanceNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Instance", string.Empty);
                oFactory = new DataModelFactory(m_oUserLogin,m_iClientId);
                oPolicyStgng = oFactory.GetDataModelObject("PolicyStgng", false) as PolicyStgng;
                oEntityStgng = oFactory.GetDataModelObject("EntityStgng", false) as EntityStgng;

                oPolicyStgng.MoveTo(p_iStagingPolicyId);

                iAgentsTableId = oFactory.Context.LocalCache.GetTableId("AGENTS");
                iInusredTableId = oFactory.Context.LocalCache.GetTableId("POLICY_INSURED");
                iDriversTableId = oFactory.Context.LocalCache.GetTableId("DRIVERS");

                foreach (PolicyXEntityStgng oPolicyXEntityStgng in oPolicyStgng.PolicyXEntityListStgng)
                {
                    oEntityStgng.MoveTo(oPolicyXEntityStgng.EntityId);
                    // Pradyumna - Added if/Elseif for Unit Interest - Start
                    if ((string.Equals(p_sMode, "entity", StringComparison.InvariantCultureIgnoreCase)))
                    {
                        if (oEntityStgng.EntityTableId == iAgentsTableId || oEntityStgng.EntityTableId == iInusredTableId || oPolicyXEntityStgng.PolicyUnitRowid > 0 || oEntityStgng.EntityTableId == iDriversTableId)
                        {
                            continue;
                        }
                    }
                    else if ((string.Equals(p_sMode, "Driver", StringComparison.InvariantCultureIgnoreCase)))
                    {
                        if (oEntityStgng.EntityTableId != iDriversTableId)
                        {
                            continue;
                        }
                    }
                    else if ((string.Equals(p_sMode, "unitinterest", StringComparison.InvariantCultureIgnoreCase)))
                    {
                        if (oEntityStgng.EntityTableId == iAgentsTableId || oEntityStgng.EntityTableId == iInusredTableId || oPolicyXEntityStgng.PolicyUnitRowid <= 0 || oEntityStgng.EntityTableId == iDriversTableId)
                        {
                            continue;
                        }
                    }

                    if (oPolicyXEntityStgng.PolicyUnitRowid > 0)
                    {
                        oPolXUnitStgng = oFactory.GetDataModelObject("PolicyXUnitStgng", false) as PolicyXUnitStgng;
                        oPolXUnitStgng.MoveTo(oPolicyXEntityStgng.PolicyUnitRowid);
                        string sSQL = "SELECT STAT_UNIT_NUMBER FROM POINT_UNIT_DATA_STGNG WHERE UNIT_ID = " + oPolXUnitStgng.UnitId + " AND UNIT_TYPE = '" + oPolXUnitStgng.UnitType.Trim() + "'";
                        object objTemp = DbFactory.ExecuteScalar(m_sConnectString, sSQL);
                        if (objTemp != null)
                        {
                            sStatUnitNum = (objTemp).ToString();
                        }
                    }
                    // Pradyumna - Added if/Elseif for Unit Interest - Ends
                    p_bExists = true;
                    xEntityNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Entity", string.Empty);
                    CreateAndAppendNode("SequenceNumber", oEntityStgng.EntityId.ToString(), xEntityNode, xReturnDoc);
                    CreateAndAppendNode("TaxID", string.Empty, xEntityNode, xReturnDoc);
                    CreateAndAppendNode("FirstName", oEntityStgng.FirstName, xEntityNode, xReturnDoc);
                    CreateAndAppendNode("MiddleName", oEntityStgng.MiddleName, xEntityNode, xReturnDoc);
                    CreateAndAppendNode("LastName", oEntityStgng.LastName, xEntityNode, xReturnDoc);
                    CreateAndAppendNode("RoleCd", oPolicyXEntityStgng.ExternalRole, xEntityNode, xReturnDoc);
                    CreateAndAppendNode("StatUnitNumber", sStatUnitNum, xEntityNode, xReturnDoc);
					CreateAndAppendNode("TableId", oEntityStgng.EntityTableId.ToString(), xEntityNode, xReturnDoc);

                    xInstanceNode.AppendChild(xEntityNode);
                }
                xRootNode.AppendChild(xInstanceNode);
                xReturnDoc.AppendChild(xRootNode);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                xRootNode = null;
				xEntityNode = null;
				xInstanceNode = null;
                if (oEntityStgng!=null)
                {
                    oEntityStgng.Dispose();
                }
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
                if (oPolicyStgng!=null)
                {
                    oPolicyStgng.Dispose();
                }
				if( oPolXUnitStgng != null)
				{
                    oPolXUnitStgng.Dispose();
				}
            }

            return xReturnDoc;
        }

        public string GetUnitDetailResult(int p_iPolicyStagingId, int p_iStagingUnitRowId)
        {
            XmlDocument xReturnDoc = null;
            PolicyStgng oPolicyStgng = null;
            DataModelFactory oFactory = null;

            VehicleStgng oVehicleStgng = null;
            PropertyUnitStgng oPropUnitStgng = null;
            OtherUnitStgng oOthrUnitStgng = null;
            SiteUnitStgng oSiteUnitStgng = null;

            XmlNode xRootNode = null;
            XmlNode xInstanceNode = null;
            XmlNode xUnitNode = null;

            string sSql = string.Empty;
            string sStatUnitNumber = string.Empty;

            try
            {
                xReturnDoc = new XmlDocument();
                xRootNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Units", string.Empty);
                xInstanceNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Instance", string.Empty);

                oFactory = new DataModelFactory(m_oUserLogin,m_iClientId);
                oPolicyStgng = oFactory.GetDataModelObject("PolicyStgng", false) as PolicyStgng;
                oVehicleStgng = oFactory.GetDataModelObject("VehicleStgng", false) as VehicleStgng;
                oPropUnitStgng = oFactory.GetDataModelObject("PropertyUnitStgng", false) as PropertyUnitStgng;
                oOthrUnitStgng = oFactory.GetDataModelObject("OtherUnitStgng", false) as OtherUnitStgng;
                oSiteUnitStgng = oFactory.GetDataModelObject("SiteUnitStgng", false) as SiteUnitStgng;

                oPolicyStgng.MoveTo(p_iPolicyStagingId);

                foreach (PolicyXUnitStgng oPolXUnitStgng in oPolicyStgng.PolicyXUnitListStgng)
                {
                    if (p_iStagingUnitRowId > 0 && oPolXUnitStgng.PolicyUnitRowId != p_iStagingUnitRowId)
                    {
                        continue;
                    }

                    xUnitNode = xReturnDoc.CreateNode(XmlNodeType.Element, "Unit", string.Empty);

                    sSql = string.Format("SELECT STAT_UNIT_NUMBER, PRODUCT, INS_LINE FROM POINT_UNIT_DATA_STGNG PUDS INNER JOIN POLICY_X_UNIT_STGNG PXUS ON PUDS.UNIT_ID = PXUS.UNIT_ID AND PUDS.UNIT_TYPE=PUDS.UNIT_TYPE WHERE PUDS.UNIT_TYPE='{0}' AND PUDS.UNIT_ID={1} AND PXUS.POLICY_ID={2}", oPolXUnitStgng.UnitType.Trim(), oPolXUnitStgng.UnitId, oPolXUnitStgng.PolicyId);
                    using (DbReader oReader = DbFactory.ExecuteReader(oPolXUnitStgng.Context.DbConnLookup.ConnectionString, sSql))
                    {
                        if (oReader != null)
                        {
                            if (oReader.Read())
                            {
                                if (oReader["STAT_UNIT_NUMBER"] != null && oReader["STAT_UNIT_NUMBER"] != DBNull.Value)
                                {
                                    sStatUnitNumber = oReader["STAT_UNIT_NUMBER"].ToString();
                                }
                                if (oReader["PRODUCT"] != null && oReader["PRODUCT"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("Product", oReader["PRODUCT"].ToString(), xUnitNode, xReturnDoc);
                                }
                                if (oReader["INS_LINE"] != null && oReader["INS_LINE"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("InsLine", oReader["INS_LINE"].ToString(), xUnitNode, xReturnDoc);
                                }
                            }
                        }
                    }

                    CreateAndAppendNode("UnitNo", sStatUnitNumber, xUnitNode, xReturnDoc);
                    CreateAndAppendNode("StatUnitNo", sStatUnitNumber, xUnitNode, xReturnDoc);
                    CreateAndAppendNode("UnitType", oPolXUnitStgng.UnitType, xUnitNode, xReturnDoc);
                    CreateAndAppendNode("Status", string.Empty, xUnitNode, xReturnDoc);


                    switch (oPolXUnitStgng.UnitType.Trim())
                    {
                        case "v":
                        case "V":
                            if (oPolXUnitStgng.UnitId > 0)
                            {
                                oVehicleStgng.MoveTo(oPolXUnitStgng.UnitId);
                            }
                            CreateAndAppendNode("Desc", string.Format("{0} {1} VIN: {2} {3}", new object[] { oVehicleStgng.VehicleYear, oVehicleStgng.VehicleModel, oVehicleStgng.Vin, oVehicleStgng.StateRowId }), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Vin", oVehicleStgng.Vin, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("City", string.Empty, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("State", oVehicleStgng.StateRowId, xUnitNode, xReturnDoc);

                            CreateAndAppendNode("VehicleType", oVehicleStgng.UnitTypeCode, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("VehicleYear", oVehicleStgng.VehicleYear.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Model", oVehicleStgng.VehicleModel, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("LeaseAmt", oVehicleStgng.LeaseAmount.ToString(), xUnitNode, xReturnDoc);
                            //added by swati for new staging columns
                            CreateAndAppendNode("Status", oVehicleStgng.Status.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Territory", oVehicleStgng.Territory.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("VehicleSym", oVehicleStgng.VehicleSym.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("PurchDate", oVehicleStgng.PurchDate.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("NewCost", oVehicleStgng.NewCost.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("PrimaryCls", oVehicleStgng.PrimaryCls.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("SecondaryCls", oVehicleStgng.SecondaryCls.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("AirbagDscnt", oVehicleStgng.AirbagDscnt.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("AntiLockBrakes", oVehicleStgng.AntiLockBrakes.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("AntiTheftInd", oVehicleStgng.AntiTheftInd.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("BodyType", oVehicleStgng.BodyType.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("VehicleUse", oVehicleStgng.VehicleUse.ToString(), xUnitNode, xReturnDoc);
                            //change end here by swati
                            break;
                        case "p":
                        case "P":
                            if (oPolXUnitStgng.UnitId > 0)
                            {
                                oPropUnitStgng.MoveTo(oPolXUnitStgng.UnitId);
                            }
                            CreateAndAppendNode("Desc", oPropUnitStgng.Description, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Vin", oPropUnitStgng.Pin, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("City", oPropUnitStgng.City, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("State", oPropUnitStgng.StateId, xUnitNode, xReturnDoc);

                            CreateAndAppendNode("Addr", oPropUnitStgng.Addr1, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("ZipCode", oPropUnitStgng.ZipCode, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("YearBuilt", oPropUnitStgng.YearOfConstruction.ToString(), xUnitNode, xReturnDoc);
                            //added by swati
                            CreateAndAppendNode("Status", oPropUnitStgng.Status.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Ratebook", oPropUnitStgng.Ratebook.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Premium", oPropUnitStgng.Premium.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Territory", oPropUnitStgng.Territory.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("ProtectionCls", oPropUnitStgng.ProtectionCls.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Deductible", oPropUnitStgng.Deductible.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("NumOfFamilies", oPropUnitStgng.NumOfFamilies.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("ConstrtnType", oPropUnitStgng.ConstrtnType.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("InsideCity", oPropUnitStgng.InsideCity.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Occupance", oPropUnitStgng.Occupance.ToString(), xUnitNode, xReturnDoc);
                            //change end here by swati
                            break;
                        case "s":
                        case "S":
                            if (oPolXUnitStgng.UnitId > 0)
                            {
                                oSiteUnitStgng.MoveTo(oPolXUnitStgng.UnitId);
                            }
                            CreateAndAppendNode("Desc", oSiteUnitStgng.Name, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Vin", oSiteUnitStgng.SiteNumber, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("City", oSiteUnitStgng.City, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("State", oSiteUnitStgng.StateId, xUnitNode, xReturnDoc);

                            CreateAndAppendNode("Optional", oSiteUnitStgng.Optional, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Addr", string.Format("{0} {1}", oSiteUnitStgng.Adsress1, oSiteUnitStgng.Adsress2), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("PostalCode", oSiteUnitStgng.ZipCode, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Country", oSiteUnitStgng.CountryId, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Phone", oSiteUnitStgng.PhoneNumber, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Contact", oSiteUnitStgng.Contact, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Sic", oSiteUnitStgng.SIC, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Fein", oSiteUnitStgng.FEIN, xUnitNode, xReturnDoc);
                            //added by swati for new staging columns
                            CreateAndAppendNode("Status", oSiteUnitStgng.Status.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("TaxLocation", oSiteUnitStgng.TaxLocation.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("AuditBasis", oSiteUnitStgng.AuditBasis.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("AuditType", oSiteUnitStgng.AuditType.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("InterimAuditor", oSiteUnitStgng.InterimAuditor.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("CheckAuditor", oSiteUnitStgng.CheckAuditor.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("FinalAuditor", oSiteUnitStgng.FinalAuditor.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("UnemplymntNum", oSiteUnitStgng.UnemplymntNum.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("NumOfEmp", oSiteUnitStgng.NumOfEmp.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("GvrngClsCode", oSiteUnitStgng.GvrngClsCode.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("SeqNum", oSiteUnitStgng.SeqNum.ToString(), xUnitNode, xReturnDoc);
                            CreateAndAppendNode("GvrngClsDesc", oSiteUnitStgng.GvrngClsDesc.ToString(), xUnitNode, xReturnDoc);
                            //change end here by swati
                            break;
                        default:
                            if (oPolXUnitStgng.UnitId > 0)
                            {
                                oOthrUnitStgng.MoveTo(oPolXUnitStgng.UnitId);
                            }
                            CreateAndAppendNode("Desc", string.Empty, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("Vin", string.Empty, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("City", string.Empty, xUnitNode, xReturnDoc);
                            CreateAndAppendNode("State", string.Empty, xUnitNode, xReturnDoc);
                            break;
                    }
                    CreateAndAppendNode("SequenceNumber", oPolXUnitStgng.PolicyUnitRowId.ToString(), xUnitNode, xReturnDoc);

                    xInstanceNode.AppendChild(xUnitNode);
                }

                xRootNode.AppendChild(xInstanceNode);
                xReturnDoc.AppendChild(xRootNode);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oSiteUnitStgng!=null)
                {
                    oSiteUnitStgng.Dispose();
                }
                if (oOthrUnitStgng!=null)
                {
                    oOthrUnitStgng.Dispose();
                }
                if (oPropUnitStgng!=null)
                {
                    oPropUnitStgng.Dispose();
                }
                if (oVehicleStgng!=null)
                {
                    oVehicleStgng.Dispose();
                }
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
                if (oPolicyStgng!=null)
                {
                    oPolicyStgng.Dispose();
                }
            }

            return xReturnDoc.OuterXml;
        }

        public XmlDocument GetUnitXmlData(int p_iPolicyStagingId)
        {
            XmlDocument oXml = new XmlDocument();
            oXml.LoadXml(GetUnitDetailResult(p_iPolicyStagingId, -1));
            return oXml;
        }

        public bool SaveUnit(int p_iPolicyId, int p_iStagingUnitRowId, ref int  p_iDownloadedUnitRowId)
        {
            DataModelFactory oFactory = null;
            PolicyXUnit oPolicyXUnit = null;
            PolicyXUnitStgng oPolicyXUnitStgng = null;

            VehicleStgng oVehicleStgng = null;
            PropertyUnitStgng oPropUnitStgng = null;
            OtherUnitStgng oOthrUnitStgng = null;
            SiteUnitStgng oSiteUnitStgng = null;

            Vehicle oVehicle = null;
            PropertyUnit oPropUnit = null;
            OtherUnit oOthrUnit = null;
            SiteUnit oSiteUnit = null;

            string sSql = string.Empty;
            string sStatUnitNumber = string.Empty;
            int iPolicyXUnitRowId = 0;
            int iUnitId = 0;
            string sErrorMsg = string.Empty;
            bool bError = false;
            try
            {
                bError = ValidateCoverages(p_iPolicyId, p_iStagingUnitRowId, ref sErrorMsg);
                if (bError)
                {
                    throw new RMAppException(sErrorMsg);
                }

                oFactory = new DataModelFactory(m_oUserLogin,m_iClientId);
                oPolicyXUnit = oFactory.GetDataModelObject("PolicyXUnit", false) as PolicyXUnit;
                oPolicyXUnitStgng = oFactory.GetDataModelObject("PolicyXUnitStgng", false) as PolicyXUnitStgng;

                sSql = string.Format("SELECT STAT_UNIT_NUMBER FROM POINT_UNIT_DATA_STGNG PUD INNER JOIN POLICY_X_UNIT_STGNG PXU ON PXU.UNIT_ID=PUD.UNIT_ID AND PXU.UNIT_TYPE=PUD.UNIT_TYPE WHERE PXU.POLICY_UNIT_ROW_ID={0}", p_iStagingUnitRowId);
                sStatUnitNumber = oFactory.Context.DbConnLookup.ExecuteString(sSql);

                oPolicyXUnitStgng.MoveTo(p_iStagingUnitRowId);
                sSql = string.Format("SELECT POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT PXU INNER JOIN POINT_UNIT_DATA PUD ON PXU.UNIT_ID=PUD.UNIT_ID AND PXU.UNIT_TYPE=PUD.UNIT_TYPE WHERE PXU.POLICY_ID={0} AND PUD.UNIT_TYPE='{1}' AND PUD.STAT_UNIT_NUMBER='{2}'", new object[] { p_iPolicyId, oPolicyXUnitStgng.UnitType.Trim(), sStatUnitNumber });
                iPolicyXUnitRowId = DbFactory.ExecuteAsType<int>(oFactory.Context.DbConnLookup.ConnectionString, sSql);

                if (iPolicyXUnitRowId > 0)
                {
                    oPolicyXUnit.MoveTo(iPolicyXUnitRowId);
                }
                else // Added by Pradyumna 03142014 MITS# 35296 - Start
                {
                    int iExists = 0;
                    bool bSuccess = false;
                    string sMultiUnitDwnload = string.Empty;
                    System.Collections.Specialized.NameValueCollection oCollection = RMConfigurationManager.GetNameValueSectionSettings("PolicyInterface", m_sConnectString, m_iClientId);
                    if (oCollection != null && oCollection["MultiUnitDownload"] != null)
                        sMultiUnitDwnload = oCollection["MultiUnitDownload"];
                    if (!string.IsNullOrEmpty(sMultiUnitDwnload) && string.Compare(sMultiUnitDwnload, "false", true) == 0)
                    {
                        sSql = "SELECT COUNT(*) FROM POLICY_X_UNIT PXU WHERE PXU.POLICY_ID = " + p_iPolicyId;
                        iExists = Conversion.CastToType<int>((DbFactory.ExecuteScalar(oFactory.Context.DbConnLookup.ConnectionString, sSql)).ToString(), out bSuccess);

                        if (iExists > 0)
                        {
                            //sErrorMsg = "A Unit already exists for this policy. Only one unit per Policy can be downloaded.";
                            //throw new RMAppException(sErrorMsg);
                            return false;
                        }
                    }
                }
                // Added by Pradyumna 03142014 MITS# 35296 - End
                switch (oPolicyXUnitStgng.UnitType.Trim())
                {
                    case "v":
                    case "V":
                        oVehicleStgng = oFactory.GetDataModelObject("VehicleStgng", false) as VehicleStgng;
                        oVehicle = oFactory.GetDataModelObject("Vehicle", false) as Vehicle;
                        if (oPolicyXUnitStgng.UnitId > 0)
                        {
                            oVehicleStgng.MoveTo(oPolicyXUnitStgng.UnitId);
                        }
                        if (iPolicyXUnitRowId > 0)
                        {
                            oVehicle.MoveTo(oPolicyXUnit.UnitId);
                            oVehicle.DataChanged = true; // Pradyumna MITS# 36013 05/20/2014: To Update data in case Policy is re-downloaded
                        }
                        oVehicle.LeaseAmount = oVehicleStgng.LeaseAmount;
                        oVehicle.LeaseExpireDate = oVehicleStgng.LeaseExpireDate;
                        oVehicle.LeaseFlag = oVehicleStgng.LeaseFlag;
                        oVehicle.LeaseNumber = oVehicleStgng.LeaseNumber;
                        oVehicle.LeaseTerm = oVehicleStgng.LeaseTerm;
                        oVehicle.LeasingCoEid = oVehicleStgng.LeasingCoEid;
                        oVehicle.LicenseNumber = oVehicleStgng.LicenseNumber;
                        oVehicle.LicenseRnwlDate = oVehicleStgng.LicenseRnwlDate;
                        oVehicle.StateRowId = oVehicleStgng.Context.LocalCache.GetStateRowID(oVehicleStgng.StateRowId);
                        oVehicle.UnitTypeCode = oVehicleStgng.Context.LocalCache.GetCodeId(oVehicleStgng.UnitTypeCode, "UNIT_TYPE_CODE");
                        oVehicle.VehicleMake = oVehicleStgng.VehicleMake;
                        oVehicle.VehicleModel = oVehicleStgng.VehicleModel;
                        oVehicle.VehicleYear = oVehicleStgng.VehicleYear;
                        oVehicle.Vin = oVehicleStgng.Vin;
                        oVehicle.GrossWeight = oVehicleStgng.GrossWeight;
                        oVehicle.HomeDeptEid = oVehicleStgng.HomeDeptEid;
                        oVehicle.Save();

                        iUnitId = oVehicle.UnitId;
                        break;
                    case "p":
                    case "P":
                        oPropUnitStgng = oFactory.GetDataModelObject("PropertyUnitStgng", false) as PropertyUnitStgng;
                        oPropUnit = oFactory.GetDataModelObject("PropertyUnit", false) as PropertyUnit;
                        if (oPolicyXUnitStgng.UnitId > 0)
                        {
                            oPropUnitStgng.MoveTo(oPolicyXUnitStgng.UnitId);
                        }
                        if (iPolicyXUnitRowId > 0)
                        {
                            oPropUnit.MoveTo(oPolicyXUnit.UnitId);
                            oPropUnit.DataChanged = true; // Pradyumna MITS# 36013 05/20/2014: To Update data in case Policy is re-downloaded
                        }
                        oPropUnit.Addr1 = oPropUnitStgng.Addr1;
                        oPropUnit.Addr2 = oPropUnitStgng.Addr2;
                        oPropUnit.AppraisedDate = oPropUnitStgng.AppraisedDate;
                        oPropUnit.AppraisedValue = oPropUnitStgng.AppraisedValue;
                        oPropUnit.City = oPropUnitStgng.City;
                        //oPropUnit.CountryCode = oPropUnitStgng.CountryCode;
                        oPropUnit.Description = oPropUnitStgng.Description;
                        oPropUnit.LandValue = oPropUnitStgng.LandValue;
                        oPropUnit.Pin = oPropUnitStgng.Pin;
                        oPropUnit.ReplacementValue = oPropUnitStgng.ReplacementValue;
                        oPropUnit.StateId = oFactory.Context.LocalCache.GetStateRowID(oPropUnitStgng.StateId);
                        oPropUnit.YearOfConstruction = oPropUnitStgng.YearOfConstruction;
                        oPropUnit.ZipCode = oPropUnitStgng.ZipCode;
                        oPropUnit.Save();

                        iUnitId = oPropUnit.PropertyId;
                        break;
                    case "s":
                    case "S":
                        oSiteUnitStgng = oFactory.GetDataModelObject("SiteUnitStgng", false) as SiteUnitStgng;
                        oSiteUnit = oFactory.GetDataModelObject("SiteUnit", false) as SiteUnit;
                        if (oPolicyXUnitStgng.UnitId > 0)
                        {
                            oSiteUnitStgng.MoveTo(oPolicyXUnitStgng.UnitId);
                        }
                        if (iPolicyXUnitRowId > 0)
                        {
                            oSiteUnit.MoveTo(oPolicyXUnit.UnitId);
                            oSiteUnit.DataChanged = true; // Pradyumna MITS# 36013 05/20/2014: To Update data in case Policy is re-downloaded
                        }
                        oSiteUnit.Adsress1 = oSiteUnitStgng.Adsress1;
                        oSiteUnit.Adsress2 = oSiteUnitStgng.Adsress2;
                        oSiteUnit.City = oSiteUnitStgng.City;
                        oSiteUnit.Contact = oSiteUnitStgng.Contact;
                        //oSiteUnit.CountryId = oSiteUnitStgng.CountryId;
                        oSiteUnit.FEIN = oSiteUnitStgng.FEIN;
                        oSiteUnit.Name = oSiteUnitStgng.Name;
                        oSiteUnit.Optional = oSiteUnitStgng.Optional;
                        oSiteUnit.PhoneNumber = oSiteUnitStgng.PhoneNumber;
                        oSiteUnit.SIC = oSiteUnitStgng.SIC;
                        oSiteUnit.SiteNumber = oSiteUnitStgng.SiteNumber;
                        oSiteUnit.StateId = oFactory.Context.LocalCache.GetStateRowID(oSiteUnitStgng.StateId);
                        oSiteUnit.ZipCode = oSiteUnitStgng.ZipCode;
                        oSiteUnit.Save();

                        iUnitId = oSiteUnit.SiteId;
                        break;
                    default:
                        break;
                }

                if (iUnitId > 0)
                {
                    oPolicyXUnit.DataChanged = true; // Pradyumna MITS# 36013 05/20/2014: To Update data in case Policy is re-downloaded
                    oPolicyXUnit.UnitId = iUnitId;
                    oPolicyXUnit.UnitType = oPolicyXUnitStgng.UnitType.Trim();
                    oPolicyXUnit.PolicyId = p_iPolicyId;
                    oPolicyXUnit.Save();

                    //added by swati
                    p_iDownloadedUnitRowId = oPolicyXUnit.PolicyUnitRowId ;
                }

                if (iPolicyXUnitRowId <= 0)
                {
                    sSql = string.Format("INSERT INTO POINT_UNIT_DATA (ROW_ID, UNIT_ID, UNIT_TYPE, STAT_UNIT_NUMBER) VALUES ({0},{1},'{2}','{3}')",
                        new object[] { oFactory.Context.GetNextUID("POINT_UNIT_DATA"), oPolicyXUnit.UnitId, oPolicyXUnitStgng.UnitType.Trim(), sStatUnitNumber });
                    oFactory.Context.DbConnLookup.ExecuteNonQuery(sSql);
                }
                // Pradyumna MITS# 36013 05/19/2014 - To Update existing Coverages for a unit when Policy is Downloaded again
                SaveCoverages(p_iPolicyId, p_iStagingUnitRowId, oPolicyXUnit.PolicyUnitRowId);
                return true;
            }
            catch (RMAppException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oSiteUnit!=null)
                {
                    oSiteUnit.Dispose();
                }
                if (oOthrUnit!=null)
                {
                    oOthrUnit.Dispose();
                }
                if (oPropUnit!=null)
                {
                    oPropUnit.Dispose();
                }
                if (oVehicle!=null)
                {
                    oVehicle.Dispose();
                }
                if (oPolicyXUnit != null)
                {
                    oPolicyXUnit.Dispose();
                }
                if (oSiteUnitStgng != null)
                {
                    oSiteUnitStgng.Dispose();
                }
                if (oOthrUnitStgng != null)
                {
                    oOthrUnitStgng.Dispose();
                }
                if (oPropUnitStgng != null)
                {
                    oPropUnitStgng.Dispose();
                }
                if (oVehicleStgng != null)
                {
                    oVehicleStgng.Dispose();
                }
                if (oPolicyXUnitStgng!=null)
                {
                    oPolicyXUnitStgng.Dispose();
                }
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
            }

        }

        private bool ValidateCoverages(int p_iPolicyId, int p_iStagingUnitRowId, ref string p_sErrorMsg)
        {
            string sSql = string.Empty;

            PolicyXCvgTypeStgng oPolCvgTypeStgng = null;
            DataModelFactory oFactory = null;

            HashSet<int> hSetPolCvgRowIds = null;
            int iPolCvgRowid = 0;
            int iCodeId = 0;
            bool bSuccess = false;
            bool bError = false;
            try
            {
                oFactory = new DataModelFactory(m_oUserLogin,m_iClientId);
                oPolCvgTypeStgng = oFactory.GetDataModelObject("PolicyXCvgTypeStgng", false) as PolicyXCvgTypeStgng;
                hSetPolCvgRowIds = new HashSet<int>();

                sSql = "SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE_STGNG WHERE POLICY_UNIT_ROW_ID=" + p_iStagingUnitRowId;
                using (DbReader oReader = oFactory.Context.DbConnLookup.ExecuteReader(sSql))
                {
                    if (oReader != null)
                    {
                        while (oReader.Read())
                        {
                            if (oReader["POLCVG_ROW_ID"] != null && oReader["POLCVG_ROW_ID"] != DBNull.Value)
                            {
                                iPolCvgRowid = Conversion.CastToType<int>(oReader["POLCVG_ROW_ID"].ToString(), out bSuccess);
                                hSetPolCvgRowIds.Add(iPolCvgRowid);
                            }
                        }
                    }
                }

                foreach (int iCvgRowid in hSetPolCvgRowIds)
                {
                    if (iCvgRowid > 0)
                    {
                        oPolCvgTypeStgng.MoveTo(iCvgRowid);
                    }
                    iCodeId = oFactory.Context.LocalCache.GetCodeId(oPolCvgTypeStgng.CoverageTypeCode.Trim(), "COVERAGE_TYPE");

                    if (iCodeId <= 0)
                    {
                        bError = true;
                        p_sErrorMsg = "Missing Code Mapping: " + oPolCvgTypeStgng.CoverageTypeCode.Trim();
                    }
                }
            }
            catch
            {
                bError = true;
                p_sErrorMsg = "Error In ValidateCoverages";
            }
            finally
            {
                if (oPolCvgTypeStgng != null)
                {
                    oPolCvgTypeStgng.Dispose();
                }
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
            }
            return bError;
        } 

        public void SaveCoverages(int p_iPolicyId, int p_iStagingUnitRowId, int p_iPolicyUnitRowId)
        {
            string sSql = string.Empty;
            int iCvgTypeCd = 0; // Added by Pradyumna MITS# 36013 05/19/2014
            int iPolCvgRwID = 0; // Added by Pradyumna MITS# 36013 05/19/2014
            PolicyXCvgTypeStgng oPolCvgTypeStgng = null;
            PolicyXCvgType oPolCvgType = null;
            DataModelFactory oFactory = null;

            HashSet<int> hSetPolCvgRowIds = null;
            int iPolCvgRowid = 0;
            bool bSuccess = false;
            try
            {
                oFactory = new DataModelFactory(m_oUserLogin,m_iClientId);
                oPolCvgTypeStgng = oFactory.GetDataModelObject("PolicyXCvgTypeStgng", false) as PolicyXCvgTypeStgng;
                hSetPolCvgRowIds = new HashSet<int>();

                sSql = "SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE_STGNG WHERE POLICY_UNIT_ROW_ID=" + p_iStagingUnitRowId;
                using (DbReader oReader = oFactory.Context.DbConnLookup.ExecuteReader(sSql))
                {
                    if (oReader != null)
                    {
                        while (oReader.Read())
                        {
                            if (oReader["POLCVG_ROW_ID"] != null && oReader["POLCVG_ROW_ID"] != DBNull.Value)
                            {
                                iPolCvgRowid = Conversion.CastToType<int>(oReader["POLCVG_ROW_ID"].ToString(), out bSuccess);
                                hSetPolCvgRowIds.Add(iPolCvgRowid);
                            }
                        }
                    }
                }

                foreach (int iCvgRowid in hSetPolCvgRowIds)
                {
                    if (iCvgRowid > 0)
                    {
                        oPolCvgTypeStgng.MoveTo(iCvgRowid);
                    }
                    oPolCvgType = oFactory.GetDataModelObject("PolicyXCvgType", false) as PolicyXCvgType;
                    // Pradyumna MITS# 36013 05/19/2014 - To Update existing Coverages for a unit when Policy is Downloaded again - Start
                    iCvgTypeCd = oFactory.Context.LocalCache.GetCodeId(oPolCvgTypeStgng.CoverageTypeCode.Trim(), "COVERAGE_TYPE");
                    sSql = "SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID = " + p_iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE = " + iCvgTypeCd;

                    iPolCvgRwID = oFactory.Context.DbConnLookup.ExecuteInt(sSql);

                    if (iPolCvgRwID > 0)
                    {
                        oPolCvgType.MoveTo(iPolCvgRwID);
                        oPolCvgType.DataChanged = true; // Pradyumna 05/20/2014: To Update data in case Policy is re-downloaded
                    }
                    // Pradyumna MITS# 36013 05/19/2014 - To Update existing Coverages for a unit when Policy is Downloaded again - End
                    oPolCvgType.BrokerName = oPolCvgTypeStgng.BrokerName;
                    oPolCvgType.CancelNoticeDays = oPolCvgTypeStgng.CancelNoticeDays;
                    oPolCvgType.ChangeDate = oPolCvgTypeStgng.ChangeDate;
                    oPolCvgType.ClaimLimit = oPolCvgTypeStgng.ClaimLimit;
                    oPolCvgType.CoverageText = oPolCvgTypeStgng.CoverageText;
                    oPolCvgType.CoverageTypeCode = oFactory.Context.LocalCache.GetCodeId(oPolCvgTypeStgng.CoverageTypeCode.Trim(), "COVERAGE_TYPE");
                    oPolCvgType.CvgSequenceNo = oPolCvgTypeStgng.CvgSequenceNo;
                    oPolCvgType.EffectiveDate = oPolCvgTypeStgng.EffectiveDate;
                    oPolCvgType.Exceptions = oPolCvgTypeStgng.Exceptions;
                    oPolCvgType.ExpirationDate = oPolCvgTypeStgng.ExpirationDate;
                    oPolCvgType.Exposure = oPolCvgTypeStgng.Exposure;
                    oPolCvgType.FullTermPremium = oPolCvgTypeStgng.FullTermPremium;
                    oPolCvgType.Limit = oPolCvgTypeStgng.Limit;
                    oPolCvgType.NextPolicyId = oPolCvgTypeStgng.NextPolicyId;
                    oPolCvgType.NotificationUid = oPolCvgTypeStgng.NotificationUid;
                    oPolCvgType.OccurrenceLimit = oPolCvgTypeStgng.OccurrenceLimit;
                    oPolCvgType.OrginialPremium = oPolCvgTypeStgng.OrginialPremium;
                    oPolCvgType.PerPersonLimit = oPolCvgTypeStgng.PerPersonLimit;
                    //oPolCvgType.PolicyId = p_iPolicyId;
                    oPolCvgType.PolicyLimit = oPolCvgTypeStgng.PolicyLimit;
                    oPolCvgType.PolicyUnitRowId = p_iPolicyUnitRowId;
                    oPolCvgType.Remarks = oPolCvgTypeStgng.Remarks;
                    //oPolCvgType.SectionNumCode = oPolCvgTypeStgng.SectionNumCode;
                    oPolCvgType.SelfInsureDeduct = oPolCvgTypeStgng.SelfInsureDeduct;
                    oPolCvgType.TotalPayments = oPolCvgTypeStgng.TotalPayments;
                    oPolCvgType.TotalWrittenPremium = oPolCvgTypeStgng.TotalWrittenPremium;
                    oPolCvgType.TransSequenceNo = oPolCvgTypeStgng.TransSequenceNo;
                    oPolCvgType.WrittenPremium = oPolCvgTypeStgng.WrittenPremium;

                    oPolCvgType.FiringScriptFlag = 2;
                    oPolCvgType.Save();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oPolCvgTypeStgng!=null)
                {
                    oPolCvgTypeStgng.Dispose();
                }
                if (oPolCvgType != null)
                {
                    oPolCvgType.Dispose();
                }
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
            }
        }

        private void CreateAndAppendNode(string p_sName, string p_sValue, XmlNode p_xBaseNode, XmlDocument p_xDoc)
        {
            XmlNode xNode = p_xDoc.CreateNode(XmlNodeType.Element, p_sName, string.Empty);
            xNode.InnerText = p_sValue;
            p_xBaseNode.AppendChild(xNode);
        }

        /// <summary>
        /// Retreives Policy search result from Staging Tables based on search criteria and returns resultset in XML format. MITS 33414
        /// </summary>
        /// <param name="oSearchRequest"></param>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetStgPolicySearchResult(PolicySearch oSearchRequest)
        {
            XmlDocument objXmlDocument = null;
            XmlElement objRowXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            StringBuilder sbSQL;
            string sWhere = string.Empty;
            string sOperator = string.Empty; // Added by Pradyumna 03072014
            List<int> lstPolicyIds = new List<int>();
            DataModelFactory objDMF = null;
            DbReader objDbReader = null;
            PolicyStgng objPolicyStg = null;
            EntityStgng ObjPolInsuredEntStg = null;
            bool bSucc = false;

            try
            {
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT DISTINCT PS.POLICY_ID FROM POLICY_STGNG PS LEFT OUTER JOIN POLICY_X_INSURED_STGNG PXINS ON PXINS.POLICY_ID = PS.POLICY_ID INNER JOIN ENTITY_STGNG INS ON INS.ENTITY_ID = PXINS.INSURED_EID LEFT OUTER JOIN POLICY_X_ENTITY_STGNG PXE ON PXE.POLICY_ID = PS.POLICY_ID LEFT OUTER JOIN ENTITY_STGNG AGNT ON AGNT.ENTITY_ID = PXE.ENTITY_ID");

                if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.PolicySymbol))
                {
                    if (sWhere != string.Empty)
                        sWhere = sWhere + " AND ";

                    if (oSearchRequest.objSearchFilters.PolicySymbol.IndexOf('*', 0) > -1)
                        sOperator = "LIKE";
                    else
                        sOperator = "=";

                    sWhere = sWhere + " PS.POLICY_SYMBOL " + sOperator + " '" + oSearchRequest.objSearchFilters.PolicySymbol.Replace("'", "''").Replace('*','%') + "' ";
                }

                if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.Module))
                {
                    if (sWhere != string.Empty)
                        sWhere = sWhere + " AND ";

                    if (oSearchRequest.objSearchFilters.Module.IndexOf('*', 0) > -1)
                        sOperator = "LIKE";
                    else
                        sOperator = "=";

                    sWhere = sWhere + " PS.MODULE " + sOperator + " '" + oSearchRequest.objSearchFilters.Module.Replace("'", "''").Replace('*', '%') + "' ";
                }

                if (oSearchRequest.objSearchFilters.LOB > 0)
                {
                    string sPolicyLob = objDMF.Context.LocalCache.GetShortCode(oSearchRequest.objSearchFilters.LOB);
                    if (sWhere != string.Empty)
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + " PS.POLICY_LOB_CODE = '" + sPolicyLob + "' ";
                }

                if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.PolicyNumber))
                {
                    if (sWhere != string.Empty)
                        sWhere = sWhere + " AND ";

                    if (oSearchRequest.objSearchFilters.PolicyNumber.IndexOf('*', 0) > -1)
                        sOperator = "LIKE";
                    else
                        sOperator = "=";

                    sWhere = sWhere + " PS.POLICY_NUMBER " + sOperator + " '" + oSearchRequest.objSearchFilters.PolicyNumber.Replace("'", "''").Replace('*', '%') + "' ";
                }

                if (oSearchRequest.objSearchFilters.State > 0)
                {
                    if (sWhere != string.Empty)
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + " INS.STATE_ID = '" + objDMF.Context.LocalCache.GetStateCode(oSearchRequest.objSearchFilters.State) + "' ";
                }

                if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.LossDate))
                {
                    if (sWhere != string.Empty)
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + " PS.EFFECTIVE_DATE <= " + Conversion.GetDate(oSearchRequest.objSearchFilters.LossDate.Replace("'", "''")) + " AND PS.EXPIRATION_DATE >= " + Conversion.GetDate(oSearchRequest.objSearchFilters.LossDate.Replace("'", "''"));
                }

                // Pradyumna MITS 34932 02/27/2014
                if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.Agent))
                {
                    if (sWhere != string.Empty)
                        sWhere = sWhere + " AND ";

                    if (oSearchRequest.objSearchFilters.Agent.IndexOf('*', 0) > -1)
                        sOperator = "LIKE";
                    else
                        sOperator = "=";

                    sWhere = sWhere + " AGNT.REFERENCE_NUMBER " + sOperator + " '" + oSearchRequest.objSearchFilters.Agent.Replace('*', '%') + "' ";
                }

                if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.Zip))
                {
                    if (sWhere != string.Empty)
                        sWhere = sWhere + " AND ";

                    if (oSearchRequest.objSearchFilters.Zip.IndexOf('*', 0) > -1)
                        sOperator = "LIKE";
                    else
                        sOperator = "=";

                    sWhere = sWhere + " INS.ZIP_CODE " + sOperator + " '" + oSearchRequest.objSearchFilters.Zip.Replace("'", "''").Replace('*', '%') + "' ";
                }

                if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.City))
                {
                    if (sWhere != string.Empty)
                        sWhere = sWhere + " AND ";

                    if (oSearchRequest.objSearchFilters.City.IndexOf('*', 0) > -1)
                        sOperator = "LIKE";
                    else
                        sOperator = "=";

                    sWhere = sWhere + " INS.CITY " + sOperator + " '" + oSearchRequest.objSearchFilters.City.Replace("'", "''").Replace('*', '%') + "' ";
                }

                //if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.Name))
                //{
                //    if (sWhere != string.Empty)
                //        sWhere = sWhere + " AND ";
                //    sWhere = sWhere + " INS.LAST_NAME like '" + oSearchRequest.objSearchFilters.Name.Replace("'", "''") + "' ";
                //}

                // Pradyumna MITS 34932 02/27/2014
                if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.InsuredSSN))
                {
                    if (sWhere != string.Empty)
                        sWhere = sWhere + " AND ";

                    if (oSearchRequest.objSearchFilters.InsuredSSN.IndexOf('*', 0) > -1)
                        sOperator = "LIKE";
                    else
                        sOperator = "=";

                    sWhere = sWhere + " INS.TAX_ID " + sOperator + " '" + oSearchRequest.objSearchFilters.InsuredSSN.Replace('*', '%') + "' ";
                }

                if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.InsuredLastName))
                {
                    if (sWhere != string.Empty)
                        sWhere = sWhere + " AND ";

                    if (oSearchRequest.objSearchFilters.InsuredLastName.IndexOf('*', 0) > -1)
                        sOperator = "LIKE";
                    else
                        sOperator = "=";

                    sWhere = sWhere + " INS.LAST_NAME " + sOperator + " '" + oSearchRequest.objSearchFilters.InsuredLastName.Replace("'", "''").Replace('*', '%') + "' ";
                }

                if (!string.IsNullOrEmpty(oSearchRequest.objSearchFilters.InsuredFirstName))
                {
                    if (sWhere != string.Empty)
                        sWhere = sWhere + " AND ";

                    if (oSearchRequest.objSearchFilters.InsuredFirstName.IndexOf('*', 0) > -1)
                        sOperator = "LIKE";
                    else
                        sOperator = "=";

                    sWhere = sWhere + " INS.FIRST_NAME " + sOperator + " '" + oSearchRequest.objSearchFilters.InsuredFirstName.Replace("'", "''").Replace('*', '%') + "' ";
                }

                // PSHEKHAWAT 7/25/2014 : JIRA RMA-745 - Start
                if(!string.IsNullOrEmpty(sWhere))
                    sbSQL.Append(" WHERE " + sWhere);
                // PSHEKHAWAT 7/25/2014 : JIRA RMA-745 - End

                using (objDbReader = DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString()))
                {
                    while (objDbReader.Read())
                    {
                        lstPolicyIds.Add(Convert.ToInt16(objDbReader.GetValue("Policy_ID")));
                    }
                }

                objXmlDocument = new XmlDocument();

                if (lstPolicyIds.Count > 0)
                {
                    objRootElement = objXmlDocument.CreateElement("SearchResults");
                    objXmlDocument.AppendChild(objRootElement);


                    foreach (int iPolicyID in lstPolicyIds)
                    {
                        objPolicyStg = (PolicyStgng)objDMF.GetDataModelObject("PolicyStgng", false);
                        objPolicyStg.MoveTo(iPolicyID);

                        DataSimpleList objPolicyInsuredStg = objPolicyStg.PolicyXInsuredStgng;
                        ObjPolInsuredEntStg = (EntityStgng)objDMF.GetDataModelObject("EntityStgng", false);
                        ObjPolInsuredEntStg.MoveTo(Conversion.CastToType<int>(objPolicyInsuredStg.ToString(), out bSucc));

                        objRowXmlElement = objXmlDocument.CreateElement("Row");

                        objXmlElement = objXmlDocument.CreateElement("status");
                        if (!String.IsNullOrEmpty(objPolicyStg.PolicyStatusCode))
                            objXmlElement.InnerText = objPolicyStg.PolicyStatusCode.Trim() + " " + objDMF.Context.LocalCache.GetCodeDesc(objDMF.Context.LocalCache.GetCodeId(objPolicyStg.PolicyStatusCode, "POLICY_STATUS")).Trim();
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("policynumber");
                        if (!String.IsNullOrEmpty(objPolicyStg.PolicyNumber))
                            objXmlElement.InnerText = objPolicyStg.PolicyNumber.Trim();
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("policyname");
                        if (!String.IsNullOrEmpty(objPolicyStg.PolicySymbol))
                            objXmlElement.InnerText = objPolicyStg.PolicySymbol.Trim();
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("module");
                        if (!String.IsNullOrEmpty(objPolicyStg.Module))
                            objXmlElement.InnerText = objPolicyStg.Module.Trim();
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("commercialname");
                        string sInsuredName = string.Empty;
                        if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.FirstName))
                            sInsuredName = ObjPolInsuredEntStg.FirstName.Trim();
                        if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.LastName))
                        {
                            if (sInsuredName != string.Empty)
                                sInsuredName = " ";
                            sInsuredName = ObjPolInsuredEntStg.LastName.Trim();
                        }
                        objXmlElement.InnerText = sInsuredName;
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("sortname");
                        if (sInsuredName.Length >= 15)
                            objXmlElement.InnerText = sInsuredName.Substring(0, 15);
                        else
                            objXmlElement.InnerText = sInsuredName;
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("lastname");
                        if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.LastName))
                            objXmlElement.InnerText = ObjPolInsuredEntStg.LastName.Trim();
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("firstname");
                        if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.FirstName))
                            objXmlElement.InnerText = ObjPolInsuredEntStg.FirstName.Trim();
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("middlename");
                        if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.MiddleName))
                            objXmlElement.InnerText = ObjPolInsuredEntStg.MiddleName.Trim();
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("city");
                        if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.City))
                            objXmlElement.InnerText = ObjPolInsuredEntStg.City.Trim();
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("state");
                        if (!String.IsNullOrEmpty(ObjPolInsuredEntStg.StateId))
                        {
                            //string sStateCode = string.Empty;
                            //string sStateDesc = string.Empty;
                            //objDMF.Context.LocalCache.GetStateInfo(objDMF.Context.LocalCache.GetStateRowID(ObjPolInsuredEntStg.StateId), ref sStateCode, ref sStateDesc);
                            objXmlElement.InnerText = ObjPolInsuredEntStg.StateId.Trim();
                        }
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("effectivedate");
                        if (!String.IsNullOrEmpty(objPolicyStg.EffectiveDate))
                            objXmlElement.InnerText = Conversion.GetDBDateFormat(objPolicyStg.EffectiveDate, "MM/dd/yyyy");
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("expirationdate");
                        if (!String.IsNullOrEmpty(objPolicyStg.ExpirationDate))
                            objXmlElement.InnerText = Conversion.GetDBDateFormat(objPolicyStg.ExpirationDate, "MM/dd/yyyy");
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("location");
                        objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("mastercompany");
                        objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        foreach (PolicyXEntityStgng objPolicyXEntStg in objPolicyStg.PolicyXEntityListStgng)
                        {
                            int iAgentTableId = 0;
                            bool bSuccess = false;
                            iAgentTableId = objDMF.Context.LocalCache.GetTableId("AGENTS");
                            string sSQL = "SELECT COUNT(*) FROM ENTITY_STGNG WHERE ENTITY_ID =  " + objPolicyXEntStg.EntityId + " AND ENTITY_TABLE_ID = " + iAgentTableId;

                            int iAgentExists = Conversion.CastToType<int>((DbFactory.ExecuteScalar(m_sConnectString, sSQL)).ToString(), out bSuccess);

                            if (iAgentExists > 0)
                            {
                                EntityStgng oAgentEnt = (EntityStgng)objDMF.GetDataModelObject("EntityStgng", false);
                                oAgentEnt.MoveTo(objPolicyXEntStg.EntityId);
                                objXmlElement = objXmlDocument.CreateElement("agency");
                                if (!String.IsNullOrEmpty(oAgentEnt.ReferenceNumber))
                                    objXmlElement.InnerText = oAgentEnt.ReferenceNumber.Trim();
                                else
                                    objXmlElement.InnerText = "";
                                objRowXmlElement.AppendChild(objXmlElement);
                                break;
                            }
                        }
                        objXmlElement = objXmlDocument.CreateElement("lob");
                        if (!String.IsNullOrEmpty(objPolicyStg.PolicyLOB))
                            objXmlElement.InnerText = objPolicyStg.PolicyLOB.Trim();
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objXmlElement = objXmlDocument.CreateElement("StagingPolicyID");
                        if (objPolicyStg.PolicyId != 0)
                            objXmlElement.InnerText = objPolicyStg.PolicyId.ToString();
                        else
                            objXmlElement.InnerText = "";
                        objRowXmlElement.AppendChild(objXmlElement);

                        objRootElement.AppendChild(objRowXmlElement);

                        objPolicyStg.Dispose();
                    }
                }

            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicyStagingInterface.GetStgPolicySearchResult.ErrorFetchingResult",m_iClientId), p_objExp);
            }
            finally
            {
                objRowXmlElement = null;
                objXmlElement = null;
                objRootElement = null;
                lstPolicyIds = null;
                sbSQL = null;
                if (objDMF != null)
                    objDMF.Dispose();
                if (objDbReader != null)
                    objDbReader.Dispose();
                if (objPolicyStg != null)
                    objPolicyStg.Dispose();
                if (ObjPolInsuredEntStg != null)
                    ObjPolInsuredEntStg.Dispose();
            }
            return objXmlDocument;
        }

        /// <summary>
        /// Save Policy and its related data like Insured, Insurer, Broker from Staging Area to rmA
        /// </summary>
        /// <param name="iStgPolicyID"></param>
        /// <param name="iClaimId"></param>
        /// <returns></returns>
        /// Pradyumna GAP16 MITS 33414 - 12/13/2013 - WWIG
        public Policy SavePolicyFrmStaging(int iStgPolicyID, int iClaimId)
        {
            int iCodeId = 0;
            int iPolicyId = 0;
            int iInsuredEntityId = 0;
            int iInsurerEntityId = 0;
            int iBrokerEntityId = 0;
            int iAgent_EId = 0;
            bool bSuccess = false;
            string sSQL = string.Empty;
            SysSettings objSettings = null;
            PolicyStgng objStgPolicy = null;
            EntityStgng objPolicyEntityStg = null;
            Policy objPolicy = null;
            DataModelFactory objDMF = null;
            PolicySystemInterface objPolicySys = null;

            bool bIsOrphan = false;
            int iPolicyCount = 0;

            try
            {
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                objPolicySys = new PolicySystemInterface(m_oUserLogin, m_iClientId);
                // Initializing Policy datamodel
                objPolicy = (Policy)objDMF.GetDataModelObject("Policy", false);
                // Initializing Staging Policy datamodel
                objStgPolicy = (PolicyStgng)objDMF.GetDataModelObject("PolicyStgng", false);
                objStgPolicy.MoveTo(iStgPolicyID);


                #region Check for Policy Duplication

                if (objStgPolicy.EffectiveDate.Trim().Length == 8 && objStgPolicy.ExpirationDate.Trim().Length == 8 && objStgPolicy.PolicyNumber.Length > 0)
                {
                    if (iClaimId > 0)
                    {
                        ////sSQL = "SELECT CASE (SELECT COUNT(*) FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId + ")	WHEN 0 THEN (SELECT MAX(POLICY_ID) POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN (SELECT POLICY_ID FROM CLAIM_X_POLICY) AND EFFECTIVE_DATE ='" + objStgPolicy.EffectiveDate + "' AND EXPIRATION_DATE ='" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER ='" + objStgPolicy.PolicyNumber + "' ) ";
                        ////sSQL = sSQL + "ELSE (SELECT POLICY_ID FROM POLICY WHERE POLICY_ID IN (SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId + ") AND EFFECTIVE_DATE = " + objStgPolicy.EffectiveDate + " AND EXPIRATION_DATE = '" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER = '" + objStgPolicy.PolicyNumber + "' ) END POLICY_ID ";

                        //sSQL = "SELECT COUNT(*) from CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId;
                        //iPolicyCount = DbFactory.ExecuteAsType<int>(m_sConnectString, sSQL);

                        //if (iPolicyCount == 0)
                        //{
                        //    //We are setting bIsOrphan as true irrespective of whether the query returns Policy Id or not
                        //    //If the query returns no policy - the method OrphanPolicyCleanUp will not be called
                        //    sSQL = "SELECT MAX(POLICY_ID) POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN (SELECT POLICY_ID FROM CLAIM_X_POLICY) AND EFFECTIVE_DATE ='" + objStgPolicy.EffectiveDate + "' AND EXPIRATION_DATE ='" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER ='" + objStgPolicy.PolicyNumber + "'";
                        //    bIsOrphan = true;
                        //}
                        //else if (iPolicyCount > 0)
                        //{
                        //    sSQL = "SELECT POLICY_ID FROM POLICY WHERE POLICY_ID IN (SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId + ") AND EFFECTIVE_DATE = " + objStgPolicy.EffectiveDate + " AND EXPIRATION_DATE = '" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER = '" + objStgPolicy.PolicyNumber + "' ";
                        //}
                        
                        // Pradyumna 2/12/2014 - Gap16 - Modified - To validate the screnario where a policy is attached with claim and second policy is checked for Orphan policy. - Start
                        sSQL = "SELECT POLICY_ID FROM POLICY WHERE POLICY_ID IN (SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId + ") AND EFFECTIVE_DATE = " + objStgPolicy.EffectiveDate + " AND EXPIRATION_DATE = '" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER = '" + objStgPolicy.PolicyNumber + "' ";
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                iPolicyId = Conversion.CastToType<int>(objReader.GetValue("POLICY_ID").ToString(), out bSuccess);
                            }
                        }
                        if (iPolicyId <= 0)
                        {
                            sSQL = "SELECT MAX(POLICY_ID) POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN (SELECT POLICY_ID FROM CLAIM_X_POLICY) AND EFFECTIVE_DATE ='" + objStgPolicy.EffectiveDate + "' AND EXPIRATION_DATE ='" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER ='" + objStgPolicy.PolicyNumber + "'";
                            bIsOrphan = true;
                        }
                        // Pradyumna 2/12/2014 - Gap16 - Ends
                    }
                    else
                    {
                        sSQL = "SELECT POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN ( SELECT POLICY_ID FROM CLAIM_X_POLICY ) AND EFFECTIVE_DATE = " + objStgPolicy.EffectiveDate + " AND EXPIRATION_DATE = '" + objStgPolicy.ExpirationDate + "' AND POLICY_NUMBER = '" + objStgPolicy.PolicyNumber + "'";
                        bIsOrphan = true;
                    }
                }

                if (!string.IsNullOrEmpty(sSQL) && iPolicyId <= 0)
                {
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                    {
                        if (objReader.Read())
                        {
                            iPolicyId = Conversion.CastToType<int>(objReader.GetValue("POLICY_ID").ToString(), out bSuccess);
                        }
                    }
                }

                if (iPolicyId > 0)
                {
                    if (bIsOrphan)
                    {
                        OrphanPolicyCleanUp(iPolicyId);
                    }
                    objPolicy.MoveTo(iPolicyId);
                    objPolicy.DataChanged = true; // Pradyumna MITS# 36013 05/20/2014: To Update data in case Policy is re-downloaded
                }

                #endregion

                #region Populating Policy Datamodel with values from PolicyStaging Datamodel
                // Policy Name 
                objPolicy.PolicyName = objStgPolicy.PolicyName.Trim();

                // PolicyNumber
                objPolicy.PolicyNumber = objStgPolicy.PolicyNumber.Trim();

                // PolicyStatusCode
                if (objStgPolicy.PolicyStatusCode.Trim().Length > 0)
                {
                    iCodeId = objPolicySys.GetRMXCodeIdFromPSMappedCode(objStgPolicy.PolicyStatusCode.Trim(), "POLICY_STATUS", "Policy Status on SavePolicyFrmStaging", "");
                    objPolicy.PolicyStatusCode = iCodeId;
                }
                // Premium
                objPolicy.Premium = objStgPolicy.Premium;

                // EffectiveDate
                objPolicy.EffectiveDate = objStgPolicy.EffectiveDate.Trim();

                // ExpirationDate
                objPolicy.ExpirationDate = objStgPolicy.ExpirationDate.Trim();

                // Cancel date
                objPolicy.CancelDate = objStgPolicy.CancelledDate.Trim();

                // TriggerClaimFlag
                objSettings = new SysSettings(m_sConnectString,m_iClientId);
                if (objSettings.PolicyCvgType == 0)
                    objPolicy.TriggerClaimFlag = true;
                else
                    objPolicy.TriggerClaimFlag = false;

                // PrimaryPolicyFlg
                objPolicy.PrimaryPolicyFlg = true;

                // InsurerEid
                objPolicy.InsurerEid = objStgPolicy.InsurerEid;

                // Policy Symbol
                objPolicy.PolicySymbol = objStgPolicy.PolicySymbol.Trim();

                // Currency Code
                if (objStgPolicy.CurrencyCode.Trim().Length > 0)
                {
                    iCodeId = objPolicySys.GetRMXCodeIdFromPSMappedCode(objStgPolicy.CurrencyCode.Trim(), "CURRENCY_TYPE", "Currency on SavePolicyFrmStaging", "");
                    objPolicy.CurrencyCode = iCodeId;
                }
                //LOB
                if (objStgPolicy.PolicyLOB.Trim().Length > 0)
                {
                    iCodeId = objPolicySys.GetRMXCodeIdFromPSMappedCode(objStgPolicy.PolicyLOB.Trim(), "POLICY_CLAIM_LOB", "LOB on SavePolicyFrmStaging", "");
                    objPolicy.PolicyLOB = iCodeId;
                }
                //Module
                objPolicy.Module = objStgPolicy.Module.Trim();

                // Issue date
                objPolicy.IssueDate = objStgPolicy.IssueDate.Trim();

                // All States flag
                objPolicy.AllStatesFlag = objStgPolicy.AllStatesFlag;

                // BrokerEid
                //objPolicy.BrokerEid = objStgPolicy.BrokerEid;
                objPolicyEntityStg = (EntityStgng)objDMF.GetDataModelObject("EntityStgng", false);
                if (objStgPolicy.BrokerEid > 0)
                {
                    objPolicyEntityStg.MoveTo(objStgPolicy.BrokerEid);
                    iBrokerEntityId = SaveEntityFrmStaging(objPolicyEntityStg, "BROKER");
                }
                objPolicyEntityStg.Dispose();
                objPolicy.BrokerEid = iBrokerEntityId;
                #endregion

                // Save Insured Entity
                DataSimpleList objPolicyInsuredStg = objStgPolicy.PolicyXInsuredStgng;
                objPolicyEntityStg = (EntityStgng)objDMF.GetDataModelObject("EntityStgng", false);
                if (objPolicyInsuredStg != null && !string.IsNullOrEmpty(objPolicyInsuredStg.ToString()))
                {
                    objPolicyEntityStg.MoveTo(Conversion.CastToType<int>(objPolicyInsuredStg.ToString(), out bSuccess));
                    iInsuredEntityId = SaveEntityFrmStaging(objPolicyEntityStg, "INSURED");
                }
                objPolicyEntityStg.Dispose();

                // Save Insurer Entity
                objPolicyEntityStg = (EntityStgng)objDMF.GetDataModelObject("EntityStgng", false);
                if (objStgPolicy.InsurerEid > 0)
                {
                    objPolicyEntityStg.MoveTo(objStgPolicy.InsurerEid);
                    iInsurerEntityId = SaveEntityFrmStaging(objPolicyEntityStg, "INSURER");
                }
                if (iInsurerEntityId > 0)
                {
                    objPolicy.InsurerEid = iInsurerEntityId;
                }
                objPolicyEntityStg.Dispose();

                // insert record in policy_x_insurer table for new policy 
                if (objPolicy.PolicyId <= 0)
                {
                    if (objPolicy.InsurerEid != 0)
                    {
                        PolicyXInsurer objPolIns = objPolicy.PolicyXInsurerList.AddNew();
                        objPolIns.InsurerCode = objPolicy.InsurerEid;
                        objPolIns.PolicyId = objPolicy.PolicyId;
                        objPolIns.ResPercentage = 100.0;
                        objPolIns.PrimaryInsurer = true;
                    }
                }
                else // for existing policy, 
                {
                    //Count = 0 => record does not exists in POLICY_X_INSURER; InsurereEid != 0 => there is insurer info to be saved
                    if (objPolicy.PolicyXInsurerList.Count == 0 && objPolicy.InsurerEid != 0)
                    {
                        PolicyXInsurer objPolIns = objPolicy.PolicyXInsurerList.AddNew(); ;
                        objPolIns.InsurerCode = objPolicy.InsurerEid;
                        objPolIns.PolicyId = objPolicy.PolicyId;
                        objPolIns.ResPercentage = 100.0;
                        objPolIns.PrimaryInsurer = true;
                    }
                }
                // To fire script before saving policy
                objPolicy.FiringScriptFlag = 2;
                objPolicy.Save();


                // SAVE data into intoPOLICYXINSURED
                PolicyXInsuredStgng objPolXInsuredStg = (PolicyXInsuredStgng)objDMF.GetDataModelObject("PolicyXInsuredStgng", false);
                sSQL = "SELECT COUNT(*) FROM POLICY_X_INSURED WHERE POLICY_ID = " + objPolicy.PolicyId + " AND INSURED_EID = " + iInsuredEntityId;

                int iExists = Conversion.CastToType<int>((DbFactory.ExecuteScalar(m_sConnectString, sSQL)).ToString(), out bSuccess);
                if (iExists <= 0)
                {
                    sSQL = string.Format("INSERT INTO POLICY_X_INSURED (POLICY_ID, INSURED_EID) VALUES ({0},{1})", new object[] { objPolicy.PolicyId, iInsuredEntityId });
                    objDMF.Context.DbConnLookup.ExecuteNonQuery(sSQL);
                }

                // Commented (Not Needed)- Save Broker in POLICY_X_ENTITY
                //objPolicySys.SavePolicyXEntity(objPolicy.PolicyId, iBrokerEntityId, objDMF.Context.LocalCache.GetTableId("BROKER"), 0, "BROKER");

                // Saving Agents
                foreach (PolicyXEntityStgng objPolicyXEntStg in objStgPolicy.PolicyXEntityListStgng)
                {
                    int iAgentTableId = 0;
                    iAgentTableId = objDMF.Context.LocalCache.GetTableId("AGENTS");
                    if (objPolicyXEntStg.EntityId > 0)
                    {
                        sSQL = "SELECT COUNT(*) FROM ENTITY_STGNG WHERE ENTITY_ID =  " + objPolicyXEntStg.EntityId + " AND ENTITY_TABLE_ID = " + iAgentTableId;

                        int iAgentExists = Conversion.CastToType<int>((DbFactory.ExecuteScalar(m_sConnectString, sSQL)).ToString(), out bSuccess);

                        if (iAgentExists > 0)
                        {
                            objPolicyEntityStg = (EntityStgng)objDMF.GetDataModelObject("EntityStgng", false);
                            objPolicyEntityStg.MoveTo(objPolicyXEntStg.EntityId);
                            iAgent_EId = objPolicySys.AgentExists(objPolicy.PolicyId, iAgentTableId, objPolicyEntityStg.LastName, objPolicyEntityStg.ReferenceNumber);
                            if (iAgent_EId == 0)
                            {
                                iAgent_EId = SaveEntityFrmStaging(objPolicyEntityStg, "AGENT");
                                objPolicySys.SavePolicyXEntity(objPolicy.PolicyId, iAgent_EId, iAgentTableId, 0, "agents");
                            }
                            else if (iAgent_EId > 0)
                            {
                                objPolicySys.SavePolicyXEntity(objPolicy.PolicyId, iAgent_EId, iAgentTableId, 0, "agents");
                            }
                        }
                    }
                }
            }
            // Pradyumna 05/21/2014: Added to display error message thrown from script editor - Start
            catch (RMAppException p_objExp)
            {
                foreach (string sError in objPolicy.Context.ScriptValidationErrors.Values)
                {
                    throw new RMAppException(sError);
                }
                throw;
            }// Pradyumna MITS# 36013 05/21/2014: Added to display error message thrown from script editor - End
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicyStagingInterface.SavePolicyFrmStaging.ErrorSavingStagingPolicy",m_iClientId), p_objExp);
            }
            finally
            {
                if (objSettings != null)
                    objSettings = null;
                if (objStgPolicy != null)
                    objStgPolicy.Dispose();
                if (objPolicyEntityStg != null)
                    objPolicyEntityStg.Dispose();
                if (objDMF != null)
                    objDMF.Dispose();
                if (objPolicySys != null)
                    objPolicySys = null;
            }
            return objPolicy;
        }

        /// <summary>
        /// Saves Entity from Staging tables to rmA 
        /// </summary>
        /// <param name="p_objPolicyEntityStg"></param>
        /// <param name="sEntityType">INSURED, INSURER, AGENT etc.</param>
        /// <returns></returns>
        /// Pradyumna 12/16/2013 - GAP16 MITS 33414
        private int SaveEntityFrmStaging(EntityStgng p_objPolicyEntityStg, string sEntityType)
        {
            Entity objEntity = null;
            DataModelFactory objDMF = null;
            int iEntityId = 0;

            try
            {
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);

                objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                objEntity.DataChanged = true; // Pradyumna MITS# 36013 05/20/2014: To Update data in case Policy is re-downloaded
                #region Check for Entity Duplication

                if (String.Compare(sEntityType, "INSURED", true) == 0)
                {
                    iEntityId = CheckEntityDuplication(p_objPolicyEntityStg, "POLICY_X_INSURED", string.Empty);
                }
                else if (String.Compare(sEntityType, "INSURER", true) == 0)
                {
                    bool bDataExists = false;
                    using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT ENTITY_ID FROM ENTITY WHERE REFERENCE_NUMBER='" + p_objPolicyEntityStg.ReferenceNumber + "'"))
                    {
                        if (objRdr.Read())
                        {
                            iEntityId = objRdr.GetInt32(0);
                            bDataExists = true;
                        }
                    }
                    if (!bDataExists)
                    {
                        PopulateEntityFrmStaging(ref objEntity, p_objPolicyEntityStg);
                        objEntity.Save();
                        iEntityId = objEntity.EntityId;
                    }
                    return iEntityId;
                }
                else if (String.Compare(sEntityType, "BROKER", true) == 0)
                {
                    iEntityId = CheckEntityDuplication(p_objPolicyEntityStg, "POLICY_X_ENTITY", string.Empty);
                }
                else if (String.Compare(sEntityType, "AGENT", true) == 0)
                {
                    bool bDataExists = false;
                    using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT ENTITY_ID FROM ENTITY WHERE REFERENCE_NUMBER='" + p_objPolicyEntityStg.ReferenceNumber + "'"))
                    {
                        if (objRdr.Read())
                        {
                            iEntityId = objRdr.GetInt32(0);
                            bDataExists = true;
                        }
                    }
                    if (!bDataExists)
                    {
                        PopulateEntityFrmStaging(ref objEntity, p_objPolicyEntityStg);
                        objEntity.Save();
                        iEntityId = objEntity.EntityId;
                    }
                    return iEntityId;
                }


                if (iEntityId > 0)
                {
                    objEntity.EntityId = iEntityId;
                    objEntity.MoveTo(iEntityId);
                }

                #endregion

                PopulateEntityFrmStaging(ref objEntity, p_objPolicyEntityStg);
                // To fire scripts before saving Entity
                objEntity.FiringScriptFlag = 2;
                objEntity.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (p_objPolicyEntityStg != null)
                    p_objPolicyEntityStg.Dispose();
                if (objEntity != null)
                    objEntity.Dispose();
                if (objDMF != null)
                    objDMF.Dispose();
            }
            return objEntity.EntityId;
        }

        /// <summary>
        /// Save Entity data from Staging
        /// </summary>
        /// <param name="sSelEntityStgId"></param>
        /// <param name="sTableName"></param>
        /// <param name="iPolicyId"></param>
        /// <param name="sMode"></param>
        /// <returns></returns>
        public int SaveOptionsFrmStaging(int p_iPolicyId, int p_iStagingPolicyId, string p_sSelEntityStgId, string p_sEntityType, string p_sMode)
        {
            int iRecordId = 0;
            int iSelEntityStgId = 0;
            int iEntityTableId = 0;
            bool bSuccess = false;
            Entity objEntity = null;
            DataModelFactory objDmf = null;
            EntityStgng objEntityStgng = null;
            PolicySystemInterface objPolicySysInterface = null;
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            int iUnitIdStg = 0;
            string sUnitTypeStg = string.Empty;
            string sUnitType = string.Empty;
            int iPolicyUnitRowId = 0;
            int iEntityId = 0;

            try
            {
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                objPolicySysInterface = new PolicySystemInterface(m_oUserLogin, m_iClientId);

                objEntity = (Entity)objDmf.GetDataModelObject("Entity", false);

                if (!String.IsNullOrEmpty(p_sSelEntityStgId))
                {
                    iSelEntityStgId = Conversion.CastToType<int>(p_sSelEntityStgId, out bSuccess);
                }
                if (iSelEntityStgId > 0)
                {
                    objEntityStgng = (EntityStgng)objDmf.GetDataModelObject("EntityStgng", false);
                    objEntityStgng.MoveTo(iSelEntityStgId);

                    if (string.Equals(p_sMode, "driver", StringComparison.InvariantCultureIgnoreCase))
                        iEntityId = CheckEntityDuplication(objEntityStgng, "DRIVER", p_sEntityType);
                    else
                        iEntityId = CheckEntityDuplication(objEntityStgng, "POLICY_X_ENTITY", p_sEntityType);

                    if (iEntityId > 0)
                    {
                        objEntity.EntityId = iEntityId;
                        objEntity.MoveTo(iEntityId);
                        objEntity.DataChanged = true; // Pradyumna MITS# 36013 05/20/2014: To Update data in case Policy is re-downloaded
                    }
                    PopulateEntityFrmStaging(ref objEntity, objEntityStgng);

                    iEntityTableId = Conversion.CastToType<int>(p_sEntityType, out bSuccess);
                    if (iEntityTableId > 0)
                        objEntity.EntityTableId = iEntityTableId;
                    // To fire scripts before saving Entity
                    objEntity.FiringScriptFlag = 2;
                    objEntity.Save();
                    iRecordId = objEntity.EntityId;

                    objPolicySysInterface.CreateSubTypeEntity(iRecordId);

                    // Check if selected Entity is linked to any unit or not
                    sSQL = string.Format("SELECT UNIT_ID, UNIT_TYPE FROM POLICY_X_UNIT_STGNG WHERE POLICY_UNIT_ROW_ID = (SELECT TOP 1 POLICY_UNIT_ROW_ID FROM POLICY_X_ENTITY_STGNG WHERE ENTITY_ID = {0} AND POLICY_ID = {1})", p_sSelEntityStgId, p_iStagingPolicyId);
                    using (objDbReader = objDmf.Context.DbConnLookup.ExecuteReader(sSQL))
                    {
                        if (objDbReader != null)
                        {
                            while (objDbReader.Read())
                            {
                                if (objDbReader["UNIT_ID"] != null && objDbReader["UNIT_ID"] != DBNull.Value)
                                {
                                    iUnitIdStg = Conversion.CastToType<int>(objDbReader["UNIT_ID"].ToString(), out bSuccess);
                                }
                                if (objDbReader["UNIT_TYPE"] != null && objDbReader["UNIT_TYPE"] != DBNull.Value)
                                {
                                    sUnitTypeStg = objDbReader["UNIT_TYPE"].ToString().Trim();
                                }
                            }
                        }
                    }

                    if (iUnitIdStg != 0 && !string.IsNullOrEmpty(sUnitTypeStg))
                    {
                        sSQL = string.Format("SELECT PXU.POLICY_UNIT_ROW_ID FROM POINT_UNIT_DATA PUD INNER JOIN POLICY_X_UNIT PXU ON PUD.UNIT_ID = PXU.UNIT_ID AND PUD.UNIT_TYPE = PXU.UNIT_TYPE WHERE PXU.POLICY_ID = {0} AND PUD.STAT_UNIT_NUMBER = (SELECT STAT_UNIT_NUMBER FROM POINT_UNIT_DATA_STGNG WHERE UNIT_ID = {1} AND UNIT_TYPE = '{2}') ", p_iPolicyId, iUnitIdStg, sUnitTypeStg);
                        using (objDbReader = objDmf.Context.DbConnLookup.ExecuteReader(sSQL))
                        {
                            if (objDbReader != null)
                            {
                                while (objDbReader.Read())
                                {
                                    if (objDbReader["POLICY_UNIT_ROW_ID"] != null && objDbReader["POLICY_UNIT_ROW_ID"] != DBNull.Value)
                                    {
                                        iPolicyUnitRowId = Conversion.CastToType<int>(objDbReader["POLICY_UNIT_ROW_ID"].ToString(), out bSuccess);
                                    }
                                }
                            }
                        }
                    }

                    // Check if Driver or Other Entity
                    if (string.Equals(p_sMode, "driver", StringComparison.InvariantCultureIgnoreCase))
                    {
                        int iDriverId = 0;
                        iRecordId = objPolicySysInterface.SavePolicyXEntity(p_iPolicyId, iRecordId, objEntity.EntityTableId, iPolicyUnitRowId, "Driver");
                        DriverStgng oDriverStgng = objDmf.GetDataModelObject("DriverStgng", false) as DriverStgng;
                        using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT DRIVER_ROW_ID FROM DRIVER_STGNG WHERE DRIVER_EID=" + iSelEntityStgId))
                        {
                            if (objRdr.Read())
                                iDriverId = Conversion.CastToType<int>(objRdr.GetValue(0).ToString(), out bSuccess);
                        }
                        if (iDriverId > 0)
                            oDriverStgng.MoveTo(iDriverId);
                        objPolicySysInterface.SaveDriver(oDriverStgng.LicenceDate, oDriverStgng.DriversLicNo.Trim(), iRecordId, 0, objDmf.Context.LocalCache.GetCodeId(oDriverStgng.MaritalStatCode.Trim(), "MARITAL_STATUS"));
                    }
                    else
                    {
                        iRecordId = objPolicySysInterface.SavePolicyXEntity(p_iPolicyId, iRecordId, objEntity.EntityTableId, iPolicyUnitRowId, "");
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objEntity != null)
                    objEntity.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                if (objEntityStgng != null)
                    objEntityStgng.Dispose();
                if (objDbReader != null)
                    objDbReader.Dispose();
                objPolicySysInterface = null;
            }
            return iRecordId;
        }

        /// <summary>
        /// Populates values of Entity Datamodel object with EntityStaging datamodel
        /// </summary>
        /// <param name="p_objEntity"></param>
        /// <param name="p_objPolicyEntityStg"></param>
        /// Pradyumna 12/16/2013 - GAP16 - MITS 33414
        private void PopulateEntityFrmStaging(ref Entity p_objEntity, EntityStgng p_objPolicyEntityStg)
        {
            DataModelFactory objDMF = null;
            string sTaxId = string.Empty;
            try
            {
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);

                #region Populating Entity Information From Entity Staging Datamodel
                // Modified to Handled Masked SSN/TaxID
                sTaxId = p_objEntity.Context.DbConnLookup.ExecuteString("SELECT TAX_ID FROM ENTITY_STGNG WHERE ENTITY_ID=" + p_objEntity.EntityId);
                if (!string.IsNullOrEmpty(sTaxId.Trim()))
                {
                    p_objEntity.TaxId = sTaxId;
                }
                p_objEntity.FirstName = p_objPolicyEntityStg.FirstName.Trim();
                p_objEntity.MiddleName = p_objPolicyEntityStg.MiddleName.Trim();
                p_objEntity.LastName = p_objPolicyEntityStg.LastName.Trim();
                //p_objEntity.NameType = objPolicyInsuredStg.Na
                p_objEntity.Addr1 = p_objPolicyEntityStg.Addr1.Trim();
                p_objEntity.Addr2 = p_objPolicyEntityStg.Addr2.Trim();
                p_objEntity.ZipCode = p_objPolicyEntityStg.ZipCode.Trim();
                p_objEntity.City = p_objPolicyEntityStg.City.Trim();
                if (p_objPolicyEntityStg.StateId.Trim().Length > 0)
                    p_objEntity.StateId = objDMF.Context.LocalCache.GetStateRowID(p_objPolicyEntityStg.StateId.Trim());
                if (p_objPolicyEntityStg.CountryCode.Trim().Length > 0)
                    p_objEntity.CountryCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.CountryCode.Trim(), "COUNTRY");
                p_objEntity.County = p_objPolicyEntityStg.County.Trim();
                p_objEntity.BirthDate = p_objPolicyEntityStg.BirthDate.Trim();
                p_objEntity.EntityTableId = p_objPolicyEntityStg.EntityTableId;
                //p_objEntity.ClientSequenceNumber = objPolInsuredEntStg.Cl;
                //p_objEntity.AddressSequenceNumber = objPolInsuredEntStg.Addre;
                p_objEntity.ParentEid = p_objPolicyEntityStg.ParentEid;
                p_objEntity.Contact = p_objPolicyEntityStg.Contact.Trim();
                p_objEntity.Comments = p_objPolicyEntityStg.Comments.Trim();
                if (p_objPolicyEntityStg.EmailTypeCode.Trim().Length > 0)
                    p_objEntity.EmailTypeCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.EmailTypeCode.Trim(), "EMAIL_TYPE");
                p_objEntity.EmailAddress = p_objPolicyEntityStg.EmailAddress.Trim();
                if (p_objPolicyEntityStg.SexCode.Trim().Length > 0)
                    p_objEntity.SexCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.SexCode.Trim(), "SEX_CODE");
                p_objEntity.EffStartDate = p_objPolicyEntityStg.EffStartDate.Trim();
                p_objEntity.EffEndDate = p_objPolicyEntityStg.EffEndDate.Trim();
                if (p_objPolicyEntityStg.BusinessTypeCode.Trim().Length > 0)
                    p_objEntity.BusinessTypeCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.BusinessTypeCode.Trim(), "BUSINESS_TYPE");
                if (p_objPolicyEntityStg.NatureOfBusiness.Trim().Length > 0)
                    p_objEntity.NatureOfBusiness = p_objPolicyEntityStg.NatureOfBusiness.Trim();
                if (p_objPolicyEntityStg.SicCode.Trim().Length > 0)
                    p_objEntity.SicCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.SicCode.Trim(), "SIC_CODE");
                p_objEntity.SicCodeDesc = p_objPolicyEntityStg.SicCodeDesc.Trim();
                p_objEntity.WcFillingNumber = p_objPolicyEntityStg.WcFillingNumber.Trim();
                p_objEntity.Parent1099EID = p_objPolicyEntityStg.Parent1099EID;
                p_objEntity.Entity1099Reportable = p_objPolicyEntityStg.Entity1099Reportable;
                p_objEntity.Title = p_objPolicyEntityStg.Title.Trim();
                if (p_objPolicyEntityStg.NaicsCode.Trim().Length > 0)
                    p_objEntity.NaicsCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.NaicsCode.Trim(), "NAICS_CODES");
                p_objEntity.FreezePayments = p_objPolicyEntityStg.FreezePayments;
                if (p_objPolicyEntityStg.OrganizationType.Trim().Length > 0)
                    p_objEntity.OrganizationType = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.OrganizationType.Trim(), "ORGANIZATION_TYPE");
                p_objEntity.NPINumber = p_objPolicyEntityStg.NPINumber.Trim();
                if (p_objPolicyEntityStg.TimeZoneCode.Trim().Length > 0)
                    p_objEntity.TimeZoneCode = objDMF.Context.LocalCache.GetCodeId(p_objPolicyEntityStg.TimeZoneCode.Trim(), "TIME_ZONE");
                //p_objEntity.Prefix = p_objPolicyEntityStg.Prefix.Trim();
                //p_objEntity.SuffixCommon = objPolInsuredEntStg.SuffixCommon;
                p_objEntity.SuffixLegal = p_objPolicyEntityStg.SuffixLegal.Trim();
                p_objEntity.LegalName = p_objPolicyEntityStg.LegalName.Trim();
                p_objEntity.ReferenceNumber = p_objPolicyEntityStg.ReferenceNumber.Trim();

                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objDMF != null)
                    objDMF.Dispose();
                if (p_objPolicyEntityStg != null)
                    p_objPolicyEntityStg.Dispose();
            }
        }

        /// <summary>
        /// Checks for Duplicate Entity based on the Entity Staging parameters - GAP16 MITS 33414
        /// </summary>
        /// <param name="p_objEntity"></param>
        /// <param name="p_sTableName"></param>
        /// <param name="p_sSelEntityType"></param>
        /// <returns></returns>
        /// Pradyumna 12/16/2013 - GAP16 MITS 33414
        private int CheckEntityDuplication(EntityStgng p_objEntity, string p_sTableName, string p_sSelEntityType)
        {
            string sAdd = string.Empty;
            bool bSuccess = false;
            int iTableId = 0;
            StringBuilder sbSql = new StringBuilder();
            LocalCache objCache = null;
            int iEntityID = 0;
            string sPolicyFields = string.Empty;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            string sTaxId = string.Empty;
            try
            {
                objCache = new LocalCache(m_sConnectString, m_iClientId);
                switch (p_sTableName.ToUpper())
                {
                    case "POLICY_X_INSURED":
                        sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("PolicyHolderDupCheckFields", string.Empty);
                        break;
                    case "POLICY_X_ENTITY":
                        sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("AdditionalInterestDupCheckFields", string.Empty);
                        break;
                    case "DRIVER":
                        sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("DriverDupCheckFields", string.Empty);
                        break;
                }
                string[] arrValues = sPolicyFields.Split(',');
                sbSql.Append("SELECT ENTITY.ENTITY_ID FROM ENTITY WHERE 1=1");

                if (p_objEntity != null)
                {
                    for (int i = 0; i <= arrValues.Length - 1; i++)
                    {
                        if (string.Compare(arrValues[i], "LastName", false) == 0)
                        {
                            if (p_objEntity.LastName.Trim() != string.Empty)
                            {
                                sbSql.Append(" AND ENTITY.LAST_NAME= ~LASTNAME~");
                                strDictParams.Add("LASTNAME", p_objEntity.LastName.Trim());
                            }
                        }
                        else if (string.Compare(arrValues[i], "FirstName", false) == 0)
                        {
                            if (p_objEntity.FirstName.Trim() != string.Empty)
                            {
                                sbSql.Append(" AND ENTITY.FIRST_NAME= ~FIRSTNAME~");
                                strDictParams.Add("FIRSTNAME", p_objEntity.FirstName.Trim());
                            }
                        }
                        else if (string.Compare(arrValues[i], "TaxId", false) == 0)
                        {
                            //tanwar2 - tax id can be maxed by changing settings in utility
                            //if (p_objEntity.TaxId.Trim() != string.Empty)
                            //{
                            //    sbSql.Append(" AND ENTITY.TAX_ID= ~TAXID~");
                            //    strDictParams.Add("TAXID", p_objEntity.TaxId.Trim());
                            //}
                            sTaxId = p_objEntity.Context.DbConnLookup.ExecuteString("SELECT TAX_ID FROM ENTITY_STGNG WHERE ENTITY_ID=" + p_objEntity.EntityId);
                            if (!string.IsNullOrEmpty(sTaxId.Trim()))
                            {
                                sbSql.Append(" AND ENTITY.TAX_ID= ~TAXID~");
                                strDictParams.Add("TAXID", sTaxId.Trim());
                            }
                            //tanwar2 - end
                        }

                        else if (string.Compare(arrValues[i].Trim(), "Addr", false) == 0)
                        {
                            if (p_objEntity.Addr1 != string.Empty)
                            {
                                sAdd = p_objEntity.Addr1.Trim();
                            }

                            if (p_objEntity.Addr2 != string.Empty)
                            {
                                sAdd = p_objEntity.Addr2.Trim();
                            }
                            if (sAdd != string.Empty)
                            {
                                sbSql.Append(" AND ENTITY.ADDR1 + ENTITY.ADDR2= ~ADDR~");
                                strDictParams.Add("ADDR", sAdd);
                            }
                        }
                        else if (string.Compare(arrValues[i].Trim(), "EmailAddress", false) == 0)
                        {
                            if (p_objEntity.EmailAddress != string.Empty)
                            {
                                sbSql.Append(" AND ENTITY.EMAIL_ADDRESS= ~EMAIL~");
                                strDictParams.Add("EMAIL", p_objEntity.EmailAddress.Trim());
                            }
                        }
                        else if (string.Compare(arrValues[i].Trim(), "Contact", false) == 0)
                        {
                            if (p_objEntity.Contact != string.Empty)
                            {
                                sbSql.Append(" AND ENTITY.CONTACT= ~CONTACT~");
                                strDictParams.Add("CONTACT", p_objEntity.Contact.Trim());
                            }
                        }
                        else if (string.Compare(arrValues[i].Trim(), "ZipCode", false) == 0)
                        {
                            if (p_objEntity.ZipCode != string.Empty)
                            {
                                sbSql.Append(" AND ENTITY.ZIP_CODE= ~ZIPCODE~");
                                strDictParams.Add("ZIPCODE", p_objEntity.ZipCode.Trim());
                            }
                        }
                    }
                    if (String.IsNullOrEmpty(p_sSelEntityType))
                    {
                        if (p_objEntity.EntityTableId > 0)
                            iTableId = p_objEntity.EntityTableId;
                    }
                    else
                    {
                         iTableId = Conversion.CastToType<int>(p_sSelEntityType, out bSuccess);
                    }
                }
                sbSql.Append(" AND ENTITY.ENTITY_TABLE_ID = " + iTableId);
                sbSql.Append(" ORDER BY ENTITY.ENTITY_ID DESC");
                if (sbSql != null)
                {
                    object objTemp = DbFactory.ExecuteScalar(m_sConnectString, sbSql.ToString(), strDictParams);
                    if (objTemp != null)
                    {
                        iEntityID = Conversion.CastToType<int>((objTemp).ToString(), out bSuccess);
                    }
                }

                return iEntityID;
            }

            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (p_objEntity != null)
                    p_objEntity.Dispose();
                if (strDictParams != null)
                    strDictParams = null;
                if (objCache != null)
                    objCache.Dispose();
                sbSql = null;
            }
        }

        /// <summary>
        /// Gets Coverage details from Staging Tables
        /// </summary>
        /// <param name="p_iPolCvgRowId"></param>
        /// <param name="p_iStagingUnitRowId"></param>
        /// <returns></returns>
        public string GetStagingCoverage(int p_iPolCvgRowId, int p_iStagingUnitRowId)
        {
            XmlDocument xDoc = null;
            XmlNode xRootNode = null;
            DataModelFactory oFactory = null;
            PolicyXCvgTypeStgng oPolXCvgTypeStgng = null;
            int iCvgCodeId = 0;
            string sSql = string.Empty;
            try
            {
                xDoc = new XmlDocument();

                oFactory = new DataModelFactory(m_oUserLogin,m_iClientId);
                oPolXCvgTypeStgng = oFactory.GetDataModelObject("PolicyXCvgTypeStgng", false) as PolicyXCvgTypeStgng;
                xRootNode = xDoc.CreateNode(XmlNodeType.Element, "Coverages", string.Empty);
                if(p_iPolCvgRowId > 0)
                    oPolXCvgTypeStgng.MoveTo(p_iPolCvgRowId);
                iCvgCodeId = oFactory.Context.LocalCache.GetCodeId(oPolXCvgTypeStgng.CoverageTypeCode, "COVERAGE_TYPE");

                CreateAndAppendNode("CoverageCd", oPolXCvgTypeStgng.CoverageTypeCode.Trim(), xRootNode, xDoc);
                if (iCvgCodeId > 0)
                {
                    CreateAndAppendNode("CvgDescription", oFactory.Context.LocalCache.GetCodeDesc(iCvgCodeId), xRootNode, xDoc);
                }
                else
                {
                    CreateAndAppendNode("CvgDescription", oPolXCvgTypeStgng.CoverageTypeCode, xRootNode, xDoc);
                }
                CreateAndAppendNode("csc_CoverageSeq", oPolXCvgTypeStgng.CvgSequenceNo, xRootNode, xDoc);
                CreateAndAppendNode("csc_TrxnSeq", oPolXCvgTypeStgng.TransSequenceNo, xRootNode, xDoc);
                CreateAndAppendNode("EffectiveDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.EffectiveDate, "MM/dd/yyyy"), xRootNode, xDoc);
                CreateAndAppendNode("ExpirationDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.ExpirationDate, "MM/dd/yyyy"), xRootNode, xDoc);
                CreateAndAppendNode("Limit", oPolXCvgTypeStgng.Limit.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("csc_OriginalPremium", oPolXCvgTypeStgng.OrginialPremium.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("csc_WrittenPremium", oPolXCvgTypeStgng.WrittenPremium.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("csc_TotalPremium", oPolXCvgTypeStgng.TotalWrittenPremium.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("csc_FullTermPremium", oPolXCvgTypeStgng.FullTermPremium.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("csc_Exposure", oPolXCvgTypeStgng.Exposure.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("csc_OccurenceLimit", oPolXCvgTypeStgng.OccurrenceLimit.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("csc_Deductible", oPolXCvgTypeStgng.SelfInsureDeduct.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("csc_ClassCd", oPolXCvgTypeStgng.Class, xRootNode, xDoc);
                CreateAndAppendNode("csc_Subline", oPolXCvgTypeStgng.SubLine, xRootNode, xDoc);
                CreateAndAppendNode("CovStatus", oPolXCvgTypeStgng.CvgStatus, xRootNode, xDoc);
                CreateAndAppendNode("State", oPolXCvgTypeStgng.State, xRootNode, xDoc);
                CreateAndAppendNode("ExtendDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.Extenddate, "MM/dd/yyyy"), xRootNode, xDoc);
                CreateAndAppendNode("TrxnDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.TransDate, "MM/dd/yyyy"), xRootNode, xDoc);
                CreateAndAppendNode("EntryDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.EntryDate, "MM/dd/yyyy"), xRootNode, xDoc);
                CreateAndAppendNode("RetroDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.RetroDate, "MM/dd/yyyy"), xRootNode, xDoc);
                CreateAndAppendNode("AcctDt", Conversion.GetDBDateFormat(oPolXCvgTypeStgng.AccountingDate, "MM/dd/yyyy"), xRootNode, xDoc);
                CreateAndAppendNode("MRProdLine", oPolXCvgTypeStgng.ProductLine, xRootNode, xDoc);
                CreateAndAppendNode("AnnualStatement", oPolXCvgTypeStgng.AnnualStmnt.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("LimitCovA", oPolXCvgTypeStgng.LimCovA.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("LimitCovB", oPolXCvgTypeStgng.LimCovB.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("LimitCovC", oPolXCvgTypeStgng.LimCovC.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("LimitCovD", oPolXCvgTypeStgng.LimCovD.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("LimitCovE", oPolXCvgTypeStgng.LimCovE.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("LimitCovF", oPolXCvgTypeStgng.LimCovF.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("DedAmt", oPolXCvgTypeStgng.WCDedAmnt.ToString(), xRootNode, xDoc);
                CreateAndAppendNode("WcDedAggr", oPolXCvgTypeStgng.WCDedAggr.ToString(), xRootNode, xDoc);

                sSql = string.Format("SELECT INS_LINE, PRODUCT, STAT_UNIT_NUMBER FROM POINT_UNIT_DATA_STGNG PUD INNER JOIN POLICY_X_UNIT_STGNG PXU ON PXU.UNIT_ID=PUD.UNIT_ID AND PXU.UNIT_TYPE=PUD.UNIT_TYPE WHERE PXU.POLICY_UNIT_ROW_ID={0}", p_iStagingUnitRowId);
                using (DbReader oReader = oFactory.Context.DbConnLookup.ExecuteReader(sSql))
                {
                    if (oReader != null)
                    {
                        if (oReader.Read())
                        {
                            if (oReader["INS_LINE"] != null && oReader["INS_LINE"] != DBNull.Value)
                            {
                                CreateAndAppendNode("InsLine", oReader["INS_LINE"].ToString(), xRootNode, xDoc);
                            }
                            if (oReader["PRODUCT"] != null && oReader["PRODUCT"] != DBNull.Value)
                            {
                                CreateAndAppendNode("ProdLine", oReader["PRODUCT"].ToString(), xRootNode, xDoc);
                            }
                            if (oReader["STAT_UNIT_NUMBER"] != null && oReader["STAT_UNIT_NUMBER"] != DBNull.Value)
                            {
                                CreateAndAppendNode("StatUnitNumber", oReader["STAT_UNIT_NUMBER"].ToString(), xRootNode, xDoc);
                            }
                        }
                    }
                }

                xDoc.AppendChild(xRootNode);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (oPolXCvgTypeStgng!=null)
                {
                    oPolXCvgTypeStgng.Dispose();
                }
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
            }
            return xDoc.OuterXml;
        }

        private void OrphanPolicyCleanUp(int p_iPolicyId)
        {
            string sSQL = string.Empty;

            try
            {
                //Delete coverages
                sSQL = "DELETE FROM POLICY_X_CVG_TYPE WHERE POLCVG_ROW_ID IN (SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID IN ( SELECT  POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT WHERE POLICY_ID = " + p_iPolicyId + ") )";
                DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);

                //Delete units
                sSQL = "DELETE FROM POLICY_X_UNIT WHERE POLICY_ID = " + p_iPolicyId;
                DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);

                //Delete entities(agent, policy level entities, drivers, unit level entities)
                sSQL = "DELETE FROM POLICY_X_ENTITY WHERE POLICY_ID = " + p_iPolicyId;
                DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);

                //Delete Policy Insured
                sSQL = "DELETE FROM POLICY_X_INSURED WHERE POLICY_ID = " + p_iPolicyId;
                DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);

                // Delete Policy Insurer
                sSQL = "DELETE FROM POLICY_X_INSURER WHERE POLICY_ID = " + p_iPolicyId;
                DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);

            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicyStagingInterface.OrphanPolicyCleanUp.Error",m_iClientId), p_objExp);
            }
        }

        public bool GetPolicyUnitConfigSetting()
        {
            bool bRet = true;
            string sMultiUnitDwnload = string.Empty;
            try
            {
                System.Collections.Specialized.NameValueCollection oCollection = RMConfigurationManager.GetNameValueSectionSettings("PolicyInterface", m_sConnectString, m_iClientId);
                    if (oCollection != null && oCollection["MultiUnitDownload"] != null)
                        sMultiUnitDwnload = oCollection["MultiUnitDownload"];
                    if (!string.IsNullOrEmpty(sMultiUnitDwnload) && string.Compare(sMultiUnitDwnload, "false", true) == 0)
                    {
                        bRet = false;
                    }
                    else
                    {
                        bRet = true;
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return bRet;
        }
    }
}
