﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Riskmaster.Db;
using System.IO;
using Riskmaster.Common;

namespace Riskmaster.Application.PolicySystemInterface
    {
    class ScriptContainerMgr
        {
        class AssemblyCacheItem
            {
            internal byte[] PE;
            internal ScriptEngine.CompileKey Key;
            internal Assembly AssemblyInstance = null;
            internal Type ContainerType = null;
            internal AssemblyCacheItem() { }

            }

        private static ScriptContainerMgr m_objSingleton = null;
        static public ScriptContainerMgr Manager { get { return m_objSingleton; } }
       
        static ScriptContainerMgr()
            {
            m_objSingleton = new ScriptContainerMgr();
            }

        //Enforce Singleton
        private ScriptContainerMgr()
            {
            AssemblyCache = new Dictionary<string, AssemblyCacheItem>();
            }

        private System.Collections.Generic.Dictionary<string, AssemblyCacheItem> AssemblyCache;

        //If the Assembly is accepted then you get a CompileKey in return.
        internal ScriptEngine.CompileKey CompiledAssemblyReady(byte[] RawAssembly, string sConnectionString)
            {
            //Create the Key
            AssemblyCacheItem objItem = new AssemblyCacheItem();
            objItem.Key = new ScriptEngine.CompileKey(sConnectionString);
            objItem.PE = RawAssembly;

            //Remove any existing Assembly Bytes
            if (AssemblyCache.ContainsKey(objItem.Key.DataSourceKey))
                AssemblyCache.Remove(objItem.Key.DataSourceKey);

            //Stash the Assembly bytes and key for later use in determining whether to have an engine recompile
            AssemblyCache.Add(objItem.Key.DataSourceKey, objItem);

            return objItem.Key;
            }

        //Return null if there is no compiled Assembly to work with.
        internal object GetCurrentScriptContainer(IRMXScriptEngineHost host, ScriptEngine.CompileKey key, int p_iClientId)
            {

            AssemblyCacheItem objItem;

            //What to do if we don't have a good copy of the assembly for this datasource?
            if (!AssemblyCache.ContainsKey(key.DataSourceKey))
                return null;

            objItem = AssemblyCache[key.DataSourceKey];


            //If needed - load assembly into this AppDomain
            if (objItem.AssemblyInstance == null)
                objItem.AssemblyInstance = AppDomain.CurrentDomain.Load(objItem.PE);

            //Get Instance of Container Class
            if (objItem.ContainerType == null)
                objItem.ContainerType = objItem.AssemblyInstance.GetType("ScriptContainer", true);

            //Initialize the Container class instance with a reference back to us - used for global value lookups.
            Object objContainer = System.Activator.CreateInstance(objItem.ContainerType);
            MethodInfo InitMethod = objItem.ContainerType.GetMethod("SetScriptEngineHost");
            InitMethod.Invoke(objContainer, new object[] { host as IRMXScriptEngineHost });
            Log.Write("inside  get current script container", p_iClientId);
            //Return Fully Primed Script Container
            return objContainer;
            }

        internal ScriptEngine.CompileKey GetCurrentCompileKey(string sConnectionString)
            {
            String sCanonKey = ScriptEngine.CompileKey.Canonicalize(sConnectionString);
            AssemblyCacheItem objItem;
            if (AssemblyCache.ContainsKey(sCanonKey))
                {
                objItem = AssemblyCache[sCanonKey];
                return objItem.Key;
                }
            else
                return null;
            }
        internal bool AssemblyBytesExistFor(string sConnectionString)
            {
            String sCanonKey = ScriptEngine.CompileKey.Canonicalize(sConnectionString);
            return AssemblyCache.ContainsKey(sCanonKey);
            }
        internal byte[] LoadBinary(string sConnectionString)
            {
            //if assembly bytes exists in cache return the bytes from assembly cache
            String sCanonKey = ScriptEngine.CompileKey.Canonicalize(sConnectionString);
            AssemblyCacheItem objItem;
            string m_sAssemblyFilePath = "";
            string sCoverageScriptFilePath = "";
            DateTime dtScriptFileLastModified = DateTime.Now; ;
            DateTime dtScriptDLLLastModified = dtScriptFileLastModified;
            //return the assembly bytes from cache if they exists
            if (AssemblyCache.ContainsKey(sCanonKey))
                {
                objItem = AssemblyCache[sCanonKey];
                return objItem.PE;
                }
            //No Pre-Compiled Bytes available.            
            byte[] pe;
            //check the time stamp of script and dll file
            sCoverageScriptFilePath = Path.Combine(RMConfigurator.UserDataPath, @"PolicyInterface\Scripts\" + ScriptEngine.ScriptName);
            m_sAssemblyFilePath = Path.Combine(RMConfigurator.UserDataPath, @"PolicyInterface\Scripts\" + ScriptEngine.ScriptAssemblyName);
            if (File.Exists(sCoverageScriptFilePath))//check if script file exists
                {
                //check if dll for the script exists
                m_sAssemblyFilePath = Path.Combine(RMConfigurator.UserDataPath, @"PolicyInterface\" + ScriptEngine.ScriptAssemblyName);
                if (File.Exists(m_sAssemblyFilePath))//if assembly is already compiled
                    {
                    using (System.IO.FileStream stmReader = new System.IO.FileStream(m_sAssemblyFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                        {
                        pe = new byte[stmReader.Length];
                        stmReader.Read(pe, 0, (int)stmReader.Length);
                        stmReader.Close();
                        }

                    objItem = new AssemblyCacheItem();
                    objItem.PE = pe;
                    objItem.Key = new ScriptEngine.CompileKey(sConnectionString);

                    CompiledAssemblyReady(pe, sConnectionString);

                    return pe;
                    }
                }
            return null;
            }

        //TODO... May help with debugging to be able to dump all "Dynamic Assemblies" and PDB's to disk...
        //public bool DumpBinaryToDisk()
        //{

        //}

        }
    }
