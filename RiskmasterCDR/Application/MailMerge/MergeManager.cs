﻿/**********************************************************************************************
*   Date     |  MITS/JIRA      | Programmer | Description                                            *
**********************************************************************************************
* 08/08/2014 | 36030/RMA-337   | nshah28    | Changes for save mail merge setting 
**********************************************************************************************/

using System;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.Text;
using System.IO;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Application.DocumentManagement;
using Riskmaster.Settings;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.DataModel;
using Riskmaster.Application.PaperVisionWrapper;
using Riskmaster.Application.MediaViewWrapper;
using Riskmaster.Application.FileStorage;
using System.Text.RegularExpressions;
using System.Linq;
namespace Riskmaster.Application.MailMerge
{
    /// <summary>
    ///Author  :   Parag Sarin
    ///Dated   :   29th November 2004
    ///Purpose :   This class Creates, Edits, Fetches merge information.
    /// </summary>


    public class MergeManager : IManager
    {
        #region Constant declaration
        /// <summary>
        /// Claim merge
        /// </summary>
        private const int CLAIM_MERGE = 1;
        /// <summary>
        /// Event merge
        /// </summary>
        private const int EVENT_MERGE = 2;
        /// <summary>
        /// Employee merge
        /// </summary>
        private const int EMPLOYEE_MERGE = 3;
        /// <summary>
        /// Entity merge
        /// </summary>
        private const int ENTITY_MERGE = 4;
        /// <summary>
        /// Vehicle merge
        /// </summary>
        private const int VEHICLE_MERGE = 5;
        /// <summary>
        /// Policy merge
        /// </summary>
        private const int POLICY_MERGE = 6;
        /// <summary>
        /// Payment merge
        /// </summary>
        private const int PAYMENT_MERGE = 7;
        /// <summary>
        /// Patient merge
        /// </summary>
        private const int PATIENT_MERGE = 8;
        /// <summary>
        /// Physician merge
        /// </summary>
        private const int PHYSICIAN_MERGE = 9;
        /// <summary>
        /// Medical staff merge
        /// </summary>
        private const int MED_STAFF_MERGE = 10;
        /// <summary>
        /// Constant representing the integer value of disablility plan merge
        /// </summary>
        private const int DIS_PLAN_SEARCH_MERGE = 11;
        /// <summary>
        /// Admin tracking merge
        /// </summary>
        private const int AT_MERGE = 20;
        //Shruti
        /// <summary>
        /// Leave Plan merge
        /// </summary>
        private const int LEAVE_PLAN_MERGE = 23;
        //Shruti
        /// <summary>
        /// Policy Billing merge
        /// </summary>
        private const int POLICY_BILLING_MERGE = 22;
        // Start Naresh Enhanced Policy Merge
        /// <summary>
        /// Enhanced Policy merge
        /// </summary>
        private const int ENHANCED_POLICY_MERGE = 21;
        // End Naresh Enhanced Policy Merge
        //Anu Tennyson for MITS 18291 : STARTS 10/29/2009
        private const int PROPERTY_MERGE = 12;
        //Anu Tennyson for MITS 18291 : ENDS

        //Amandeep Driver Merge
        /// <summary>
        /// Driver merge
        /// </summary>
        private const int DRIVER_MERGE = 24;
        #endregion

        #region Variable declaration
        /// <summary>
        /// Merge class object reference
        /// </summary>
        private Merge m_objMerge = null;
        /// <summary>
        /// User id
        /// </summary>
        private long m_lUID = 0;
        /// <summary>
        /// Group Ids
        /// </summary>
        ArrayList m_arrlstcolGroupIds = null;
        /// <summary>
        /// Secure DB connection string
        /// </summary>
        private string m_sSecDSN = "";
        /// <summary>
        /// Password
        /// </summary>
        private string m_sPwd = "";
        /// <summary>
        /// Connection string to database
        /// </summary>
        private string m_sDSN = "";
        /// <summary>
        /// User Id
        /// </summary>
        private string m_sUID = "";
        /// <summary>
        /// Template storage path
        /// </summary>
        private string m_sTemplatePath = "";
        /// <summary>
        /// Document storage DSN
        /// </summary>
        private string m_sDocStorageDSN = "";
        /// <summary>
        /// Document storage path type
        /// </summary>
        private long m_lDocPathType = 0;
        private UserLogin m_userLogin = null;
        //dvatsa
        private int m_iClientId = 0;
        #endregion

        #region Property declaration
        /// <summary>
        /// Secure DB connection string
        /// </summary>
        public string SecurityDSN
        {
            get
            {
                return m_sSecDSN;
            }
            set
            {
                m_sSecDSN = value;

            }
        }
        /// <summary>
        /// Merge object
        /// </summary>
        public Merge CurMerge
        {
            get
            {
                return m_objMerge;
            }
        }
        /// <summary>
        /// Password
        /// </summary>
        public override string Pwd
        {
            get
            {
                return m_sPwd;
            }
            set
            {
                m_sPwd = value;
            }
        }
        /// <summary>
        /// User Id
        /// </summary>
        public override string UID
        {
            get
            {
                return m_sUID;
            }
            set
            {
                m_sUID = value;
            }
        }
        /// <summary>
        /// Connection string to DB
        /// </summary>
        public override string DSN
        {
            get
            {
                return m_sDSN;
            }
            set
            {
                m_sDSN = value;
            }
        }
        /// <summary>
        /// Template storage path
        /// </summary>
        public override string TemplatePath
        {
            get
            {
                return m_sTemplatePath;
            }
            set
            {
                try
                {
                    m_sTemplatePath = value;
                    if (!m_sTemplatePath.Substring(m_sTemplatePath.Length - 1, 1).Equals(@"\"))
                    {
                        m_sTemplatePath = m_sTemplatePath + @"\";
                    }
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("MergeManager.TemplatePath.SetError", m_iClientId), p_objException);//dvatsa-cloud
                }

            }
        }
        /// <summary>
        /// Document storage DSN
        /// </summary>
        public override string DocStorageDSN
        {
            get
            {
                return m_sDocStorageDSN;
            }
            set
            {
                m_sDocStorageDSN = value;
            }
        }
        /// <summary>
        /// Document path type
        /// </summary>
        public override long DocPathType
        {
            get
            {
                return m_lDocPathType;
            }
            set
            {
                m_lDocPathType = value;

            }
        }
        #endregion

        #region MergeManager class constructor
        /// <summary>
        /// MergeManager class default constructor
        /// </summary>
        public MergeManager(int p_iClientId) { m_iClientId = p_iClientId; }
        /// <summary>
        /// MergeManager class Overloaded constructor
        /// </summary>
        /// <param name="p_sConnectString">Connection string</param>
        /// <param name="p_sSecConnectString">Secure DB Connection string</param>
        /// <param name="p_sUID">User id</param>
        /*
        public MergeManager(string p_sConnectString,string p_sSecConnectString,string p_sUID)
        {
            this.DSN=p_sConnectString;
            this.SecurityDSN=p_sSecConnectString;
            this.UID=p_sUID;
            m_objMerge=new Merge();
            m_objMerge.Manager=this;
            FetchPermissionInfo();
        }
        */
        public MergeManager(UserLogin p_userLogin, string p_sSecConnectString, string p_sUID, int p_iClientId)//dvatsa
        {
            m_iClientId = p_iClientId;//dvatsa-cloud
            m_userLogin = p_userLogin;
            this.DSN = p_userLogin.objRiskmasterDatabase.ConnectionString;
            this.SecurityDSN = p_sSecConnectString;
            this.UID = p_sUID;
            m_objMerge = new Merge(p_userLogin, p_iClientId);
            m_objMerge.SecurityDSN = p_sSecConnectString;
            m_objMerge.Manager = this;
            FetchPermissionInfo();
        }
        public MergeManager(UserLogin p_userLogin, int p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_userLogin = p_userLogin;
            this.DSN = p_userLogin.objRiskmasterDatabase.ConnectionString;
            m_objMerge = new Merge(p_userLogin, m_iClientId);//rkaur27
            m_objMerge.Manager = this;
            FetchPermissionInfo();
        }
        #endregion

        #region MergeManager class destructor
        /// <summary>
        /// MergeManager class Destructor
        /// </summary>
        ~MergeManager()
        {
            if (m_objMerge != null)
                m_objMerge.Manager = null;
            m_objMerge = null;
            m_arrlstcolGroupIds = null;
            m_sDocStorageDSN = null;
            m_sTemplatePath = null;
            m_sUID = null;
            m_sDSN = null;
            m_sPwd = null;
            m_sSecDSN = null;

        }
        #endregion

        #region Function to convert table name to category id
        /// Name		: RMTableNameToCatId
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function converts Table name to Category id
        /// </summary>
        /// <param name="p_lCatId">Table name</param>
        /// <returns> Category id</returns>
        public long RMTableNameToCatId(string p_sTableName)
        {
            long lCatId = 0;
            if (p_sTableName != null)
            {
                p_sTableName = p_sTableName.ToUpper();
            }
            try
            {
                switch (p_sTableName)
                {
                    case "CLAIM":
                        lCatId = CLAIM_MERGE;
                        break;
                    case "EVENT":
                        lCatId = EVENT_MERGE;
                        break;
                    //shruti, 21st dec 2006, MITS 7911 starts
                    case "PEOPLE":
                        lCatId = ENTITY_MERGE;
                        break;
                    //shruti, 21st dec 2006, MITS 7911 ends
                    case "ENTITYMAINT": //gagnihotri MITS 11536 Added case for Entity Maintenance screen.
                    case "ENTITY":
                        lCatId = ENTITY_MERGE;
                        break;
                    case "EMPLOYEE":
                        lCatId = EMPLOYEE_MERGE;
                        break;
                    case "VEHICLE":
                        lCatId = VEHICLE_MERGE;
                        break;
                    case "POLICY":
                        lCatId = POLICY_MERGE;
                        break;
                    case "FUNDS":
                        lCatId = PAYMENT_MERGE;
                        break;
                    case "PATIENT":
                        lCatId = PATIENT_MERGE;
                        break;
                    case "PHYSICIAN":
                        lCatId = PHYSICIAN_MERGE;
                        break;
                    case "STAFF":
                        lCatId = MED_STAFF_MERGE;
                        break;
                    case "PLAN":
                        lCatId = DIS_PLAN_SEARCH_MERGE;
                        break;
                    //Shruti Leave Plan Merge
                    case "LEAVEPLAN":
                        lCatId = LEAVE_PLAN_MERGE;
                        break;
                    //Shruti Policy Billing Merge
                    case "POLICYBILLING":
                        lCatId = POLICY_BILLING_MERGE;
                        break;
                    // Start Naresh Enhanced Policy Merge
                    case "POLICYENH":
                        lCatId = ENHANCED_POLICY_MERGE;
                        break;
                    // End Naresh Enhanced Policy Merge
                    //Anu Tennyson  for MITS 18291 : STARTS 10/29/2009
                    case "PROPERTYUNIT":
                        lCatId = PROPERTY_MERGE;
                        break;
                    //Anu Tennyson for MITS 18291 : ENDS
                    //Aman Driver Merge
                    case "DRIVER":
                        lCatId = DRIVER_MERGE;
                        break;
                    //Aman Driver Merge
                    default:
                        lCatId = Generic.GetSingleLong("20", "GLOSSARY", "SYSTEM_TABLE_NAME='"
                            + p_sTableName.ToUpper() + "' AND GLOSSARY_TYPE_CODE=468", m_sDSN, m_iClientId);
                        if (lCatId != 20)
                        {
                            throw new RMAppException(Globalization.GetString("MergeManager.MergeNotSupported.Error", m_iClientId));//dvatsa-cloud
                        }
                        break;
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MergeManager.RMTableNameToCatId.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            return lCatId;
        }
        #endregion

        #region Function to load template information
        /// Name		: MoveTo
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function loads template information
        /// </summary>
        /// <param name="p_lDocId">Template id</param>
        public int MoveTo(long p_lDocId)
        {
            int iMoved = 0;
            try
            {
                m_objMerge = new Merge(m_userLogin, m_iClientId);//rkaur27
                m_objMerge.Manager = null;
                m_objMerge.Manager = (MergeManager)this;
                m_objMerge.DSN = this.DSN;
                m_objMerge.SecurityDSN = this.SecurityDSN;
                m_objMerge.DocStorageDSN = this.DocStorageDSN;
                m_objMerge.StorageType = this.DocPathType;
                m_objMerge.Load(p_lDocId, this.DocPathType);
                iMoved = 1;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MergeManager.MoveTo.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            return iMoved;
        }
        #endregion

        #region Function to convert category id to table name
        /// Name		: CatIdToRMTableName
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function converts Category id to Table name
        /// </summary>
        /// <param name="p_lCatId">Category id </param>
        /// <returns>Table name</returns>
        public string CatIdToRMTableName(long p_lCatId)
        {
            string sTableName = "";
            switch (p_lCatId)
            {
                case CLAIM_MERGE:
                    sTableName = "CLAIM";
                    break;
                case EVENT_MERGE:
                    sTableName = "EVENT";
                    break;
                case ENTITY_MERGE:
                    sTableName = "ENTITY";
                    break;
                //Shruti for 11838
                case EMPLOYEE_MERGE:
                    sTableName = "ENTITY";
                    break;
                case VEHICLE_MERGE:
                    sTableName = "VEHICLE";
                    break;
                case POLICY_MERGE:
                    sTableName = "POLICY";
                    break;
                case PAYMENT_MERGE:
                    sTableName = "FUNDS";
                    break;
                case PATIENT_MERGE:
                    sTableName = "PATIENT";
                    break;
                case PHYSICIAN_MERGE:
                    sTableName = "PHYSICIAN";
                    break;
                //Shruti for 11838
                case MED_STAFF_MERGE:
                    sTableName = "STAFF";
                    break;
                case POLICY_BILLING_MERGE:
                    sTableName = "POLICYBILLING";
                    break;
                // Start Naresh MITS 9727 Merge docs to be displayed in Attach Section
                case ENHANCED_POLICY_MERGE:
                    sTableName = "POLICYENH";
                    break;
                // End Naresh MITS 9727 Merge docs to be displayed in Attach Section
                //Shruti for 11838
                case DIS_PLAN_SEARCH_MERGE:
                    sTableName = "PLAN";
                    break;
                //Shruti for 11838
                case LEAVE_PLAN_MERGE:
                    sTableName = "LEAVEPLAN";
                    break;
                //Anu Tennyson for MITS 18291 : STARTS 10/29/2009
                case PROPERTY_MERGE:
                    sTableName = "PROPERTYUNIT";
                    break;
                //Anu Tennyson for MITS 18291 : ENDS 
                //Aman Driver Enh
                case DRIVER_MERGE:
                    sTableName = "DRIVER";
                    break;
                //Aman Driver Enh
                default:
                    sTableName = "";
                    break;
            }
            return sTableName;
        }
        #endregion

        #region Function that fetches permission information
        /// Name		: FetchPermissionInfo
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function fetches the permission information
        /// </summary>
        private void FetchPermissionInfo()
        {
            string sSQL = "";
            m_arrlstcolGroupIds = new ArrayList();
            DbReader objReader = null;
            try
            {
                sSQL = " SELECT GROUP_ID FROM USER_MEMBERSHIP"
                    + " WHERE USER_ID=" + m_sUID.ToString();
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);
                while (objReader.Read())
                {
                    m_arrlstcolGroupIds.Add(Conversion.ConvertObjToStr(
                        objReader.GetValue("GROUP_ID")));
                }
                objReader.Close();
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MergeManager.FetchPermissionInfo.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;
            }
        }
        #endregion



        #region Function to get existing templates
        /// Name		: GetAvailTemplates
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function returns the list of available templates
        /// </summary>
        /// <param name="p_lCatId">Category id</param>
        /// <param name="p_sTableQualify">Table name in case of Admin tracking</param>
        /// <returns>Xml string containing the list of available templates</returns>
        public XmlDocument GetAvailTemplates(long p_lCatId, string p_sTableQualify, long p_lMergeDocumentType, long p_lMergeDocumentFormat, long p_lStateId, string p_sFormName, long p_lLOB)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            string sInGroupsClause = "";
            objDOM = new XmlDocument();
            objElemParent = objDOM.CreateElement("AvailableTemplates");
            objDOM.AppendChild(objElemParent);
            objElemChild = objDOM.CreateElement("Templates");
            objDOM.FirstChild.AppendChild(objElemChild);
            string sSQL = "";
            DbReader objReader = null;
            DbReader rdr = null;    //added by swati
            string sRecipient = string.Empty;	//added by swati
            try
            {
                sInGroupsClause = " IN (";
                for (int i = 0; i < m_arrlstcolGroupIds.Count; i++)
                {
                    if (i != 0)
                    {
                        sInGroupsClause = sInGroupsClause + " , " + m_arrlstcolGroupIds[i].ToString();
                    }
                    else
                    {
                        sInGroupsClause = sInGroupsClause + m_arrlstcolGroupIds[i].ToString();
                    }
                }
                sInGroupsClause = sInGroupsClause + " ) ";
                if (p_lCatId == 20)
                {
                    if (p_sTableQualify == "")
                    {
                        throw new RMAppException("MergeManager.GetAvailTemplates.NoTableSpecified");
                    }
                    else
                    {
                        p_sTableQualify = p_sTableQualify.ToUpper();
                    }
                }
                //changed by swati MITS # 36930
                //spahariya MITS 28867 - 07/04/2012
                //sSQL = "SELECT DISTINCT MERGE_FORM.FORM_ID,FORM_NAME,FORM_DESC,CAT_NAME,MERGE_FORM.CAT_ID "
                //sSQL = "SELECT DISTINCT MERGE_FORM.FORM_ID,FORM_NAME,FORM_DESC,CAT_NAME,MERGE_FORM.CAT_ID,MERGE_FORM.SEND_MAIL_FLAG,MERGE_FORM.DESIGNATED_RECIPIENT "
                sSQL = "SELECT DISTINCT MERGE_FORM.FORM_ID,FORM_NAME,FORM_DESC,CAT_NAME,MERGE_FORM.CAT_ID,MERGE_FORM.SEND_MAIL_FLAG "
                    + " FROM MERGE_FORM,MERGE_CAT,MERGE_FORM_PERM "
                    + " WHERE MERGE_FORM.CAT_ID = MERGE_CAT.CAT_ID "
                    + " AND MERGE_FORM.FORM_ID = MERGE_FORM_PERM.FORM_ID "
                    + " AND MERGE_FORM.CAT_ID=" + p_lCatId.ToString();
                if (p_lCatId == 20)
                {
                    sSQL += " AND MERGE_FORM.TABLE_QUALIFY='" + p_sTableQualify + "'";
                }
                sSQL += " AND ( (MERGE_FORM_PERM.USER_ID=0 AND MERGE_FORM_PERM.GROUP_ID=0)"
                    + " OR (MERGE_FORM_PERM.USER_ID = " + m_sUID.ToString();
                if (m_arrlstcolGroupIds.Count != 0)
                {
                    sSQL += " OR MERGE_FORM_PERM.GROUP_ID" + sInGroupsClause;
                }
                sSQL += ") )";

                //crawford enhancements..modified by Raman Bhatia
                if (p_lMergeDocumentType != 0)
                    sSQL = sSQL + " AND MERGE_DOC_TYPE_CODE = " + p_lMergeDocumentType.ToString();
                if (p_lMergeDocumentFormat != 0)
                    sSQL = sSQL + " AND MERGE_FORMAT_TYPE_CODE = " + p_lMergeDocumentFormat.ToString();

                if (p_lLOB > 0)
                    sSQL = sSQL + " AND (MERGE_FORM.LINE_OF_BUS_CODE = " + p_lLOB + " OR MERGE_FORM.LINE_OF_BUS_CODE = -1 OR MERGE_FORM.LINE_OF_BUS_CODE IS NULL)";

                if (p_lStateId != -1)
                    sSQL = sSQL + " AND (STATE_ID = " + p_lStateId + " OR STATE_ID = -1 )";

                // Start Naresh MITS 8419 11/17/2006
                sSQL = sSQL + " ORDER BY FORM_NAME";
                // End Naresh MITS 8419 11/17/2006
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);
                while (objReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("Template");
                    objElemTemp.SetAttribute("Category", Conversion.ConvertObjToStr(objReader.GetValue("CAT_ID")));
                    objElemTemp.SetAttribute("TemplateDesc", Conversion.ConvertObjToStr(objReader.GetValue("FORM_NAME")));
                    objElemTemp.SetAttribute("TemplateId", Conversion.ConvertObjToStr(objReader.GetValue("FORM_ID")));
                    //spahariya
                    objElemTemp.SetAttribute("SendMailFlag", Conversion.ConvertObjToStr(objReader.GetValue("SEND_MAIL_FLAG")));
                    //added by swati AIC Gap 8 MITS # 36930 11/14/2014
                    sSQL = string.Empty;
                    sRecipient = string.Empty;
                    sSQL = "SELECT TABLE_NAME, DESIGNATED_RECIPIENT_ID, IS_PRIMARY_CURRENT FROM FORM_RECIPIENT,GLOSSARY_TEXT WHERE TABLE_ID = DESIGNATED_RECIPIENT_ID AND FORM_ID = " + Conversion.ConvertObjToInt(objReader.GetValue("FORM_ID"), m_iClientId);
                    rdr = DbFactory.GetDbReader(m_sDSN, sSQL);
                    while (rdr.Read())
                    {
                        if (string.IsNullOrEmpty(sRecipient))
                        {
                            sRecipient = Conversion.ConvertObjToStr(rdr.GetValue("TABLE_NAME")) + "," + Conversion.ConvertObjToStr(rdr.GetValue("DESIGNATED_RECIPIENT_ID")) + "," + Conversion.ConvertObjToStr(rdr.GetValue("IS_PRIMARY_CURRENT"));
                        }
                        else
                        {
                            sRecipient = sRecipient + "|" + Conversion.ConvertObjToStr(rdr.GetValue("TABLE_NAME")) + "," + Conversion.ConvertObjToStr(rdr.GetValue("DESIGNATED_RECIPIENT_ID")) + "," + Conversion.ConvertObjToStr(rdr.GetValue("IS_PRIMARY_CURRENT"));
                        }
                    }
                    rdr.Close();
                    //objElemTemp.SetAttribute("MailRecipient", Conversion.ConvertObjToStr(objReader.GetValue("DESIGNATED_RECIPIENT")));
                    objElemTemp.SetAttribute("MailRecipient", sRecipient);
                    //change end here by swati
                    objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
                }
                objReader.Close();
                return objDOM;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MergeManager.GetAvailTemplates.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (rdr != null)
                {
                    rdr.Close();
                    rdr.Dispose();
                }
                objDOM = null;
                objElemTemp = null;
                objElemChild = null;
                objElemParent = null;
                sInGroupsClause = null;
            }
        }
        #endregion

        #region Function to Get / save Mail Merge options in user preferences
        //Mits:36030 JIRA RMA-337 nshah28 start
        /// <summary>
        /// //Mits:36030 JIRA RMA-337 nshah28 adding parameters for mail merge
        /// //Mits 23683 Neha get mail merge option from user preferences.
        /// </summary>
        /// <param name="sUserID"></param>
        /// <param name="sTableName"></param>
        /// <param name="bRecordSelect"></param>
        /// <param name="bAttach"></param>
        /// <param name="bAllstates"></param>
        /// <param name="bJurisdiction"></param>
        /// <param name="bMergedocumenttype"></param>
        /// <param name="bMergedocumentformat"></param>
        /// <param name="bLettername"></param>
        /// <param name="bSaveMySettings"></param>
        /// <returns></returns>
        public XmlDocument GetMailMergeOptions(string sUserID, string sTableName, string bRecordSelect, string bAttach, string bAllstates, string bJurisdiction, string bMergedocumenttype, string bMergedocumentformat, string bLettername, string bSaveMySettings)
        {
            XmlDocument objUserPrefXML = null;
            string sUserPrefsXML = "";
            using (DbReader objRdr = DbFactory.GetDbReader(m_sDSN, "Select PREF_XML from USER_PREF_XML where USER_ID=" + sUserID.ToString()))
            {
                if (objRdr.Read())
                {
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objRdr.GetValue("PREF_XML"));
                    sUserPrefsXML = sUserPrefsXML.Trim();
                }
                //objRdr.Close();
            }

            objUserPrefXML = new XmlDocument();
            if (sUserPrefsXML != "")
            {
                objUserPrefXML.LoadXml(sUserPrefsXML);
            }
            // Create the xml if it doesn't exist in the database
            CreateUserPreferXML(objUserPrefXML);
            XmlNode nodeRecordSelect = objUserPrefXML.SelectSingleNode("//setting/mergemail/recordselect/@selected");

            if (bRecordSelect == "0")
            {
                bRecordSelect = nodeRecordSelect.InnerText;
            }
            else
            {
                if (bRecordSelect.CompareTo(nodeRecordSelect.InnerText) != 0)
                    nodeRecordSelect.InnerText = bRecordSelect;
            }
            XmlNode nodeAttach = objUserPrefXML.SelectSingleNode("//setting/mergemail/attach/@selected");
            if (bAttach == "0")
            {
                bAttach = nodeAttach.InnerText;
            }
            else
            {
                if (bAttach.CompareTo(nodeAttach.InnerText) != 0)
                    nodeAttach.InnerText = bAttach;
            }

            XmlNode nodeAllstates = objUserPrefXML.SelectSingleNode("//setting/mergemail/AllStates/@selected");
            if (bAllstates == "1")
            {
                bAllstates = nodeAllstates.InnerText;
            }
            else
            {
                if (bAllstates.CompareTo(nodeAllstates.InnerText) != 0)
                    nodeAllstates.InnerText = bAllstates;
            }

            XmlNode nodeJurisdiction = objUserPrefXML.SelectSingleNode("//setting/mergemail/Jurisdiction/@value");
            if (bJurisdiction == "")
            {
                bJurisdiction = nodeJurisdiction.InnerText;
            }
            else
            {
                if (bJurisdiction.CompareTo(nodeJurisdiction.InnerText) != 0)
                    nodeJurisdiction.InnerText = bJurisdiction;
            }

            XmlNode nodeMergeDocumentType = objUserPrefXML.SelectSingleNode("//setting/mergemail/MergeDocumentType/@value");
            if (bMergedocumenttype == "")
            {
                bMergedocumenttype = nodeMergeDocumentType.InnerText;
            }
            else
            {
                if (bMergedocumenttype.CompareTo(nodeMergeDocumentType.InnerText) != 0)
                    nodeMergeDocumentType.InnerText = bMergedocumenttype;
            }


            XmlNode nodeMergeDocumentFormat = objUserPrefXML.SelectSingleNode("//setting/mergemail/MergeDocumentFormat/@value");
            if (bMergedocumentformat == "")
            {
                bMergedocumentformat = nodeMergeDocumentFormat.InnerText;
            }
            else
            {
                if (bMergedocumentformat.CompareTo(nodeMergeDocumentFormat.InnerText) != 0)
                    nodeMergeDocumentFormat.InnerText = bMergedocumentformat;
            }


            XmlNode nodeSaveMySettings = objUserPrefXML.SelectSingleNode("//setting/mergemail/SaveMySettings/@selected");
            if (bSaveMySettings == "0")
            {
                bSaveMySettings = nodeSaveMySettings.InnerText;
            }
            else
            {
                if (bSaveMySettings.CompareTo(nodeSaveMySettings.InnerText) != 0)
                    nodeSaveMySettings.InnerText = bSaveMySettings;
            }


            XmlNode nodeLettername = objUserPrefXML.SelectSingleNode("//setting/mergemail/Lettername/@value");
            if (bLettername == "0")
            {
                bLettername = nodeLettername.InnerText;
            }
            else
            {
                if (bLettername.CompareTo(nodeLettername.InnerText) != 0)
                    nodeLettername.InnerText = bLettername;
            }

            return objUserPrefXML;
        }
        //Mits:36030 JIRA RMA-337 nshah28 end

        //Mits:36030 JIRA RMA-337 nshah28 start
        /// <summary>
        /// //Mits 23683 Neha save mail merge option in user preferences.
        /// Mits:36030 JIRA RMA-337 nshah28 adding parameters for mail merge
        /// </summary>
        /// <param name="sUserID"></param>
        /// <param name="sTableName"></param>
        /// <param name="bRecordSelect"></param>
        /// <param name="bAttach"></param>
        /// <param name="bAllstates"></param>
        /// <param name="bJurisdiction"></param>
        /// <param name="bMergedocumenttype"></param>
        /// <param name="bMergedocumentformat"></param>
        /// <param name="bLettername"></param>
        /// <param name="bSaveMySettings"></param>
        public void SaveMailMergeOptions(string sUserID, string sTableName, string bRecordSelect, string bAttach, string bAllstates, string bJurisdiction, string bMergedocumenttype, string bMergedocumentformat, string bLettername, string bSaveMySettings)
        {
            XmlDocument objUserPrefXML = null;
            DbConnection objConn = null;
            string sUserPrefsXML = "";
            StringBuilder sbSql = null;
            DbCommand objCmd = null;
            DbParameter objParam = null;
            bool bStatus = false;
            using (DbReader objRdr = DbFactory.GetDbReader(m_sDSN, "Select PREF_XML from USER_PREF_XML where USER_ID=" + sUserID.ToString()))
            {
                if (objRdr.Read())
                {
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objRdr.GetValue("PREF_XML"));
                    bStatus = true;
                    sUserPrefsXML = sUserPrefsXML.Trim();
                }

            }

            objUserPrefXML = new XmlDocument();
            if (sUserPrefsXML != "")
            {
                objUserPrefXML.LoadXml(sUserPrefsXML);
            }
            CreateUserPreferXML(objUserPrefXML);

            XmlNode nodeRecordSelect = objUserPrefXML.SelectSingleNode("//setting/mergemail/recordselect/@selected");

            nodeRecordSelect.InnerText = bRecordSelect;

            XmlNode nodeAttach = objUserPrefXML.SelectSingleNode("//setting/mergemail/attach/@selected");

            nodeAttach.InnerText = bAttach;

            XmlNode nodeAllstates = objUserPrefXML.SelectSingleNode("//setting/mergemail/AllStates/@selected");

            nodeAllstates.InnerText = bAllstates;

            XmlNode nodeJurisdiction = objUserPrefXML.SelectSingleNode("//setting/mergemail/Jurisdiction/@value");

            nodeJurisdiction.InnerText = bJurisdiction;

            XmlNode nodeMergeDocumentType = objUserPrefXML.SelectSingleNode("//setting/mergemail/MergeDocumentType/@value");

            nodeMergeDocumentType.InnerText = bMergedocumenttype;

            XmlNode nodeMergeDocumentFormat = objUserPrefXML.SelectSingleNode("//setting/mergemail/MergeDocumentFormat/@value");

            nodeMergeDocumentFormat.InnerText = bMergedocumentformat;


            XmlNode nodeSaveMySettings = objUserPrefXML.SelectSingleNode("//setting/mergemail/SaveMySettings/@selected");
            nodeSaveMySettings.InnerText = bSaveMySettings;


            XmlNode nodeLettername = objUserPrefXML.SelectSingleNode("//setting/mergemail/Lettername/@value");

            nodeLettername.InnerText = bLettername;

            sUserPrefsXML = objUserPrefXML.InnerXml.ToString();
            objConn = DbFactory.GetDbConnection(m_sDSN);
            objConn.Open();
            sbSql = new StringBuilder();
            objCmd = objConn.CreateCommand();
            objParam = objCmd.CreateParameter();
            objParam.Direction = ParameterDirection.Input;
            objParam.Value = sUserPrefsXML;
            objParam.ParameterName = "XML";
            objParam.SourceColumn = "PREF_XML";
            objCmd.Parameters.Add(objParam);
            if (bStatus)
                sbSql.Append("UPDATE USER_PREF_XML SET PREF_XML=~XML~ WHERE USER_ID=" + sUserID);
            else
                sbSql.Append("INSERT INTO USER_PREF_XML VALUES(" + sUserID + ",~XML~)");
            objCmd.CommandText = sbSql.ToString();
            objCmd.ExecuteNonQuery();
        }
        //Mits:36030 JIRA RMA-337 nshah28 end

        /// <summary>// Neha Mits 23683
        /// Create Default User Prefernce Xml.
        /// </summary>
        /// <param name="p_objUserPrefXML">User Preference Xml Doc</param>
        /// 
        private void CreateUserPreferXML(XmlDocument p_objUserPrefXML)
        {
            XmlElement objTempNode = null;
            XmlElement objParentNode = null;
            try
            {
                // Create setting node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                if (objTempNode == null)
                {
                    objTempNode = p_objUserPrefXML.CreateElement("setting");
                    p_objUserPrefXML.AppendChild(objTempNode);
                }


                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                    objTempNode = p_objUserPrefXML.CreateElement("mergemail");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail/attach");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail");
                    objTempNode = p_objUserPrefXML.CreateElement("attach");
                    objTempNode.SetAttribute("selected", "0");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail/recordselect");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail");
                    objTempNode = p_objUserPrefXML.CreateElement("recordselect");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "0");
                    objParentNode.AppendChild(objTempNode);
                }

                //Mits:36030 JIRA RMA-337 nshah28 start
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail/AllStates");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail");
                    objTempNode = p_objUserPrefXML.CreateElement("AllStates");
                    //show this true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail/Jurisdiction");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail");
                    objTempNode = p_objUserPrefXML.CreateElement("Jurisdiction");

                    objTempNode.SetAttribute("value", "");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail/MergeDocumentType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail");
                    objTempNode = p_objUserPrefXML.CreateElement("MergeDocumentType");

                    objTempNode.SetAttribute("value", "");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail/MergeDocumentFormat");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail");
                    objTempNode = p_objUserPrefXML.CreateElement("MergeDocumentFormat");

                    objTempNode.SetAttribute("value", "");
                    objParentNode.AppendChild(objTempNode);
                }

                //savemysettings
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail/SaveMySettings");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail");
                    objTempNode = p_objUserPrefXML.CreateElement("SaveMySettings");

                    objTempNode.SetAttribute("selected", "0");
                    objParentNode.AppendChild(objTempNode);
                }


                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail/Lettername");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//mergemail");
                    objTempNode = p_objUserPrefXML.CreateElement("Lettername");

                    objTempNode.SetAttribute("value", "0");
                    objParentNode.AppendChild(objTempNode);
                }
                //Mits:36030 JIRA RMA-337 nshah28 end

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.CreateUserPreferXML.Err", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                objTempNode = null;
                objParentNode = null;
            }
        }
        #endregion
        #region Function to send email copy of merge lettre to designated recipient
        /// <summary>spahariya 07/10/2012 - MITS 28867
        /// Send email copy of merge lettre to designated recipient
        /// </summary>
        /// 
        public string
            SendMailRTF(string p_sFileName, MemoryStream objMemory, string sTemplateId,
                                    string p_sName, string p_sTable, string p_sType, string p_sRecordId,
                                    int iPsid, string p_sFormName, string p_sSendEmail, string p_sMailRecipient)
        {
            string sToEmail = string.Empty;
            string sSubject = string.Empty;
            string sFromEmail = string.Empty;
            string sBody = string.Empty;
            // Pradyumna 11/11/2014 MITS 36930 - Start
            string[] aSubVariables = null;
            string[] aBodyVariables = null;
            bool bResult = false; 
            //string sSubText1 = string.Empty;
            //string sSubVar1 = string.Empty;
            //string sSubText2 = string.Empty;
            //string sSubVar2 = string.Empty; 
            //string sBodyText1 = string.Empty;
            //string sBodyVar1 = string.Empty;
            //string sBodyText2 = string.Empty;
            //string sBodyVar2 = string.Empty;
            //string sBodyText3 = string.Empty;
            // Pradyumna 11/11/2014 MITS 36930 - End
            string sVariable = string.Empty;
            string[] arrVar = new string[4];
            string svar = string.Empty;
            string sMessage = string.Empty;
            string[] sRecipient = null;	//added by swati
            bool bEntityAttached = false;
            //int iPolicyID = 0;
            StringBuilder sbSQL = new StringBuilder();
            DataModelFactory objDataModelFactory = null;
            //Policy objPolicy = null;
            Mailer objMailer = null;
            FileStream objFileStream = null;
            BinaryWriter objBinaryWriter = null;
            LocalCache objCache = null;
            Claim objClaim = null;
            Event objEvent = null;
            //Employee objEmp = null;
            int atpos = 0;
            int dotpos = 0;
            try
            {
                objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//dvatsa-cloud
                if (string.Compare(p_sTable, "claim", true) == 0)
                {
                    objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));

                    objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                    objEvent.MoveTo(objClaim.EventId);
                    // Pradyumna 11/11/2014 MITS 36930 - Commented - start
                    //using (DbReader objRdr = DbFactory.GetDbReader(m_sDSN, "SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID =" + p_sRecordId))
                    //{
                    //    if (objRdr.Read())
                    //    {
                    //        iPolicyID = Common.Conversion.ConvertObjToInt(objRdr.GetValue("POLICY_ID"));
                    //    }
                    //    if (iPolicyID != 0)
                    //    {
                    //        objDataModelFactory = new DataModelFactory(m_userLogin);
                    //        objPolicy = (Policy)objDataModelFactory.GetDataModelObject("Policy", false);
                    //        objPolicy.MoveTo(iPolicyID);
                    //    }
                    //}
                    // Pradyumna 11/11/2014 MITS 36930 - Commented - End
                }
                else if (string.Compare(p_sTable, "event", true) == 0)
                {
                    objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                    objEvent.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
                }

                #region Get email from and Create mail subject and body with variable info

                objCache = new LocalCache(m_sDSN, m_iClientId);
                // Pradyumna 11/11/2014 MITS 36930 - Modified - Start
                //sbSQL = sbSQL.Append("SELECT FORM_ID, EMAIL_FROM, SUBJ_TEXT1, SUBJ_VAR_CODE1, SUBJ_TEXT2, SUBJ_VAR_CODE2, BODY_TEXT1, BODY_VAR_CODE1, BODY_TEXT2, BODY_VAR_CODE2, BODY_TEXT3 ");
                sbSQL = sbSQL.Append("SELECT FORM_ID, EMAIL_FROM, EMAIL_SUBJECT_TEXT, EMAIL_BODY_TEXT ");
                sbSQL = sbSQL.Append(" FROM MERGE_FORM_EMAIL_DETAIL");
                sbSQL = sbSQL.Append(" WHERE FORM_ID = " + sTemplateId);

                using (DbReader objRdr = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString()))
                {
                    if (objRdr.Read())
                    {
                        sFromEmail = Common.Conversion.ConvertObjToStr(objRdr.GetValue("EMAIL_FROM"));
                        sSubject = CommonFunctions.HTMLCustomDecode(Convert.ToString(objRdr.GetValue("EMAIL_SUBJECT_TEXT")));
                        sBody = CommonFunctions.HTMLCustomDecode(Convert.ToString(objRdr.GetValue("EMAIL_BODY_TEXT")));
                        //sSubText1 = Common.Conversion.ConvertObjToStr(objRdr.GetValue("SUBJ_TEXT1"));
                        //sSubVar1 = objCache.GetShortCode(Common.Conversion.ConvertObjToInt(objRdr.GetValue("SUBJ_VAR_CODE1")));
                        //sSubText2 = Common.Conversion.ConvertObjToStr(objRdr.GetValue("SUBJ_TEXT2"));
                        //sSubVar2 = objCache.GetShortCode(Common.Conversion.ConvertObjToInt(objRdr.GetValue("SUBJ_VAR_CODE2")));
                        //sBodyText1 = Common.Conversion.ConvertObjToStr(objRdr.GetValue("BODY_TEXT1"));
                        //sBodyVar1 = objCache.GetShortCode(Common.Conversion.ConvertObjToInt(objRdr.GetValue("BODY_VAR_CODE1")));
                        //sBodyText2 = Common.Conversion.ConvertObjToStr(objRdr.GetValue("BODY_TEXT2"));
                        //sBodyVar2 = objCache.GetShortCode(Common.Conversion.ConvertObjToInt(objRdr.GetValue("BODY_VAR_CODE2")));
                        //sBodyText3 = Common.Conversion.ConvertObjToStr(objRdr.GetValue("BODY_TEXT3"));
                    }
                }
                //arrVar[0] = sSubVar1;
                //arrVar[1] = sSubVar2;
                //arrVar[2] = sBodyVar1;
                //arrVar[3] = sBodyVar2;

                //sSubject = sSubject + sSubText1;
                //sBody = sBody + sBodyText1;
                // Pradyumna 11/11/2014 MITS 36930 - Modified - End
                // Pradyumna 11/11/2014 MITS 36930 - Added New - Start  
                aSubVariables = Regex.Matches(sSubject, @"<<(.*?)>>").Cast<Match>().Select(m => m.Value).Distinct().ToArray<string>();
                aBodyVariables = Regex.Matches(sBody, @"<<(.*?)>>").Cast<Match>().Select(m => m.Value).Distinct().ToArray<string>();
                foreach (string sSubVar in aSubVariables)
                {
                    sVariable = "";
                    svar = sSubVar.Replace("<<", "").Replace(">>", "").Trim();
                    if (!String.IsNullOrEmpty(svar))
                    {
                        sVariable = GetVariableValue(p_sTable, svar, objClaim, objEvent);

                        if (!String.IsNullOrEmpty(sVariable))
                        {
                            sSubject = sSubject.Replace(sSubVar, sVariable);
                        }
                        // rkapoor29 added gap 8 JIRA 7516 start
                        else
                        {
                            sSubject = sSubject.Replace(sSubVar, string.Empty);
                        }
                        // rkapoor29 added gap 8 JIRA 7516 end
                    }
                }
                foreach (string sBodyVar in aBodyVariables)
                {
                    sVariable = "";
                    svar = sBodyVar.Replace("<<", "").Replace(">>", "").Trim();

                    if (!String.IsNullOrEmpty(svar))
                    {
                        sVariable = GetVariableValue(p_sTable, svar, objClaim, objEvent);
                        if (!String.IsNullOrEmpty(sVariable))
                        {
                            sBody = sBody.Replace(sBodyVar, sVariable);
                        }
                        // rkapoor29 added gap 8 JIRA 7516 start
                        else
                        {
                            sBody = sBody.Replace(sBodyVar, string.Empty);
                        }
                        // rkapoor29 added gap 8 JIRA 7516 end
                    }
                }
                // Pradyumna 11/11/2014 MITS 36930 - Added Code - End


                #region Commented Code
                // Pradyumna 11/11/2014 MITS 36930 - Commented - Start
                //if (string.Compare(p_sTable, "claim", true) == 0)
                //{
                //    for (int i = 0; i < 4; i++)
                //    {
                //        svar = arrVar[i];
                //        sVariable = "";

                //        if (!string.IsNullOrEmpty(svar))
                //        {
                //            switch (svar)
                //            {
                //                case "CD":
                //                    sVariable = Common.Conversion.GetDBDateFormat(objClaim.DateOfClaim, "MM/dd/yyyy");
                //                    break;
                //                case "CN":
                //                    sVariable = objClaim.ClaimNumber;
                //                    break;
                //                case "CS":
                //                    sVariable = objCache.GetShortCode(objClaim.ClaimStatusCode) + " " + objCache.GetCodeDesc(objClaim.ClaimStatusCode);
                //                    break;
                //                case "CLN":
                //                    if (objClaim.ClaimantList.Count > 0)
                //                    {
                //                        foreach (Claimant objClaimant in objClaim.ClaimantList)
                //                        {
                //                            sVariable = objClaimant.ClaimantEntity.LastName + ", " + objClaimant.ClaimantEntity.FirstName;
                //                            break;
                //                        }
                //                    }
                //                    break;
                //                case "DPT":
                //                    sVariable = objCache.GetEntityLastName(objEvent.DeptEid);
                //                    break;
                //                case "EN":
                //                    sVariable = objClaim.EventNumber;
                //                    break;
                //                case "ED":
                //                    sVariable = Common.Conversion.GetDBDateFormat(objEvent.DateOfEvent, "MM/dd/yyyy");
                //                    break;
                //                case "ENO":
                //                    foreach (PersonInvolved objPerInv in objEvent.PiList)
                //                    {
                //                        if (string.Compare(objCache.GetShortCode(objPerInv.PiTypeCode), "E", true) == 0)
                //                        {
                //                            objEmp = (Employee)objDataModelFactory.GetDataModelObject("Employee", false);
                //                            objEmp.MoveTo(objPerInv.PiEid);
                //                            sVariable = objEmp.EmployeeNumber;
                //                            break;
                //                        }
                //                    }
                //                    break;
                //                case "EMPN":
                //                    foreach (PersonInvolved objPerInv in objEvent.PiList)
                //                    {
                //                        if (string.Compare(objCache.GetShortCode(objPerInv.PiTypeCode), "E", true) == 0)
                //                        {
                //                            sVariable = objPerInv.PiEntity.LastName + ", " + objPerInv.PiEntity.FirstName;
                //                            break;
                //                        }
                //                    }
                //                    break;
                //                case "JU":
                //                    string sStateCode = string.Empty; string sStateDesc = string.Empty;
                //                    objCache.GetStateInfo(objClaim.FilingStateId, ref sStateCode, ref sStateDesc);
                //                    sVariable = sStateCode + " " + sStateDesc;
                //                    break;
                //                case "PN":
                //                    if (objPolicy != null)
                //                    {
                //                        sVariable = objPolicy.PolicyName;
                //                    }
                //                    break;
                //                case "PNO":
                //                    if (objPolicy != null)
                //                    {
                //                        sVariable = objPolicy.PolicyNumber;
                //                    }
                //                    break;
                //                case "PS":
                //                    if (objPolicy != null)
                //                    {
                //                        sVariable = objPolicy.PolicySymbol;
                //                    }
                //                    break;
                //                case "ADJEMAIL":
                //                    if(objClaim.AdjusterList!= null && objClaim.AdjusterList.Count > 0)
                //                    {
                //                        if(!String.IsNullOrEmpty(objClaim.CurrentAdjuster.AdjusterEntity.EmailAddress))
                //                        {
                //                            sVariable = objClaim.CurrentAdjuster.AdjusterEntity.EmailAddress.Trim();
                //                        }
                //                    }
                //                    break;
                //            }
                //            arrVar[i] = sVariable;
                //        }
                //    }
                //}
                //else if (string.Compare(p_sTable, "event", true) == 0)
                //{
                //    for (int i = 0; i < 4; i++)
                //    {
                //        svar = arrVar[i];
                //        sVariable = "";
                //        if (svar != string.Empty)
                //        {
                //            switch (svar)
                //            {
                //                case "DPT":
                //                    sVariable = objCache.GetEntityLastName(objEvent.DeptEid);
                //                    break;
                //                case "EN":
                //                    sVariable = objEvent.EventNumber;
                //                    break;
                //                case "ED":
                //                    sVariable = Common.Conversion.GetDBDateFormat(objEvent.DateOfEvent, "MM/dd/yyyy");
                //                    break;
                //                case "ENO":
                //                    foreach (PersonInvolved objPerInv in objEvent.PiList)
                //                    {
                //                        if (string.Compare(objCache.GetShortCode(objPerInv.PiTypeCode), "E", true) == 0)
                //                        {
                //                            objEmp = (Employee)objDataModelFactory.GetDataModelObject("Employee", false);
                //                            objEmp.MoveTo(objPerInv.PiEid);
                //                            sVariable = objEmp.EmployeeNumber;
                //                            break;
                //                        }
                //                    }
                //                    break;
                //                case "EMPN":
                //                    foreach (PersonInvolved objPerInv in objEvent.PiList)
                //                    {
                //                        if (string.Compare(objCache.GetShortCode(objPerInv.PiTypeCode), "E", true) == 0)
                //                        {
                //                            sVariable = objPerInv.PiEntity.LastName + ", " + objPerInv.PiEntity.FirstName;
                //                            break;
                //                        }
                //                    }
                //                    break;
                //                default:
                //                    sVariable = string.Empty;
                //                    break;
                //            }
                //            arrVar[i] = sVariable;
                //        }
                //    }
                //}

                //if (!string.IsNullOrEmpty(arrVar[0]))
                //    sSubject = sSubject + " " + arrVar[0];
                //if (!string.IsNullOrEmpty(sSubText2))
                //    sSubject = sSubject + " " + sSubText2;
                //if (!string.IsNullOrEmpty(arrVar[1]))
                //    sSubject = sSubject + " " + arrVar[1];
                //if (!string.IsNullOrEmpty(arrVar[2]))
                //    sBody = sBody + " " + arrVar[2];
                //if (!string.IsNullOrEmpty(sBodyText2))
                //    sBody = sBody + " " + sBodyText2;
                //if (!string.IsNullOrEmpty(arrVar[3]))
                //    sBody = sBody + " " + arrVar[3];
                //if (!string.IsNullOrEmpty(sBodyText3))
                //    sBody = sBody + " " + sBodyText3;
                // Pradyumna 11/11/2014 MITS 36930 - Commented - End
                #endregion Commented Code
                if (string.IsNullOrEmpty(sFromEmail))
                {

                    if (!string.IsNullOrEmpty(m_userLogin.objUser.Email))
                    {
                        sFromEmail = m_userLogin.objUser.Email;
                    }
                    else
                    {
                        DataTable dtSMTPSettings = RMConfigurationSettings.GetSMTPServerSettings(m_iClientId);
                        sFromEmail = dtSMTPSettings.Rows[0]["ADMIN_EMAIL_ADDR"].ToString();
                    }
                }
                #endregion

                #region Get mail resipients emails
                //outer foreach added  by swati MITS # 36930
                sRecipient = p_sMailRecipient.Split(',');
                foreach (string s in sRecipient)
                {
                    foreach (PersonInvolved objPerInv in objEvent.PiList)
                    {
                        if (objPerInv.PiEntity.EntityTableId == Conversion.ConvertStrToInteger(p_sMailRecipient))
                        {
                            bEntityAttached = true;
                            if (!string.IsNullOrEmpty(objPerInv.PiEntity.EmailAddress))
                            {
                                atpos = objPerInv.PiEntity.EmailAddress.IndexOf("@");
                                dotpos = objPerInv.PiEntity.EmailAddress.LastIndexOf(".");
                                if (atpos > 1 || dotpos >= atpos + 2)
                                {
                                    if (!string.IsNullOrEmpty(sToEmail))
                                        sToEmail = sToEmail + "," + objPerInv.PiEntity.EmailAddress;
                                    else
                                        sToEmail = objPerInv.PiEntity.EmailAddress;
                                }
                            }
                            if (objPerInv.PiEntity.EntityXContactInfoList.Count > 0)
                            {
                                foreach (EntityXContactInfo objEntityContact in objPerInv.PiEntity.EntityXContactInfoList)
                                {
                                    if (!string.IsNullOrEmpty(objEntityContact.Email))
                                    {
                                        atpos = objEntityContact.Email.IndexOf("@");
                                        dotpos = objEntityContact.Email.LastIndexOf(".");
                                        if (atpos > 1 || dotpos >= atpos + 2)
                                        {
                                            if (!string.IsNullOrEmpty(sToEmail))
                                                sToEmail = sToEmail + "," + objEntityContact.Email;
                                            else
                                                sToEmail = objEntityContact.Email;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //added by swati for MITS # 36930 as the case now is of multiple recipient
                if (string.Compare(p_sTable, "claim", true) == 0)
                {
                    foreach (string s in sRecipient)
                    {
                        if (!s.Trim().EndsWith("*"))
                        {
                            if (objClaim.Context.LocalCache.GetTableId("CLAIMANT") == Conversion.ConvertStrToInteger(s))
                            {
                                foreach (Claimant objClaimant in objClaim.ClaimantList)
                                {
                                    bEntityAttached = true;
                                    //if (objClaim.PrimaryClaimant.PrimaryClmntFlag == true)
                                    //{
                                        if (!string.IsNullOrEmpty(objClaimant.ClaimantEntity.EmailAddress))
                                        {
                                            atpos = objClaimant.ClaimantEntity.EmailAddress.IndexOf("@");
                                            dotpos = objClaimant.ClaimantEntity.EmailAddress.LastIndexOf(".");
                                            if (atpos > 1 || dotpos >= atpos + 2)
                                            {
                                                if (!string.IsNullOrEmpty(sToEmail))
                                                    sToEmail = sToEmail + "," + objClaimant.ClaimantEntity.EmailAddress;
                                                else
                                                    sToEmail = objClaimant.ClaimantEntity.EmailAddress;
                                            }
                                        }
                                        if (objClaimant.ClaimantEntity.EntityXContactInfoList.Count > 0)
                                        {
                                            foreach (EntityXContactInfo objEntityContact in objClaimant.ClaimantEntity.EntityXContactInfoList)
                                            {
                                                if (!string.IsNullOrEmpty(objEntityContact.Email))
                                                {
                                                    atpos = objEntityContact.Email.IndexOf("@");
                                                    dotpos = objEntityContact.Email.LastIndexOf(".");
                                                    if (atpos > 1 || dotpos >= atpos + 2)
                                                    {
                                                        if (!string.IsNullOrEmpty(sToEmail))
                                                            sToEmail = sToEmail + "," + objEntityContact.Email;
                                                        else
                                                            sToEmail = objEntityContact.Email;
                                                    }
                                                }
                                            }
                                        }
                                    //}
                                }
                            }
                            else if (objClaim.Context.LocalCache.GetTableId("CLAIM_ADJUSTER") == Conversion.ConvertStrToInteger(s))
                            {
                                foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                                {
                                    bEntityAttached = true;
                                    //if (objClaim.CurrentAdjuster.CurrentAdjFlag == true)
                                    //{
                                        if (!string.IsNullOrEmpty(objAdjuster.AdjusterEntity.EmailAddress))
                                        {
                                            atpos = objAdjuster.AdjusterEntity.EmailAddress.IndexOf("@");
                                            dotpos = objAdjuster.AdjusterEntity.EmailAddress.LastIndexOf(".");
                                            if (atpos > 1 || dotpos >= atpos + 2)
                                            {
                                                if (!string.IsNullOrEmpty(sToEmail))
                                                    sToEmail = sToEmail + "," + objAdjuster.AdjusterEntity.EmailAddress;
                                                else
                                                    sToEmail = objAdjuster.AdjusterEntity.EmailAddress;
                                            }
                                        }
                                        if (objAdjuster.AdjusterEntity.EntityXContactInfoList.Count > 0)
                                        {
                                            foreach (EntityXContactInfo objEntityContact in objAdjuster.AdjusterEntity.EntityXContactInfoList)
                                            {
                                                if (!string.IsNullOrEmpty(objEntityContact.Email))
                                                {
                                                    atpos = objEntityContact.Email.IndexOf("@");
                                                    dotpos = objEntityContact.Email.LastIndexOf(".");
                                                    if (atpos > 1 || dotpos >= atpos + 2)
                                                    {
                                                        if (!string.IsNullOrEmpty(sToEmail))
                                                            sToEmail = sToEmail + "," + objEntityContact.Email;
                                                        else
                                                            sToEmail = objEntityContact.Email;
                                                    }
                                                }
                                            }
                                        }
                                    //}
                                }
                            }
                        }
                        else
                        {
                            if (objClaim.Context.LocalCache.GetTableId("CLAIMANT") == Conversion.ConvertStrToInteger(s.Replace("*", "")))
                            {
                                bEntityAttached = true;
                                if (objClaim.PrimaryClaimant.PrimaryClmntFlag == true)
                                {
                                    if (!string.IsNullOrEmpty(objClaim.PrimaryClaimant.ClaimantEntity.EmailAddress))
                                    {
                                        atpos = objClaim.PrimaryClaimant.ClaimantEntity.EmailAddress.IndexOf("@");
                                        dotpos = objClaim.PrimaryClaimant.ClaimantEntity.EmailAddress.LastIndexOf(".");
                                        if (atpos > 1 || dotpos >= atpos + 2)
                                        {
                                            if (!string.IsNullOrEmpty(sToEmail))
                                                sToEmail = sToEmail + "," + objClaim.PrimaryClaimant.ClaimantEntity.EmailAddress;
                                            else
                                                sToEmail = objClaim.PrimaryClaimant.ClaimantEntity.EmailAddress;
                                        }
                                    }
                                    if (objClaim.PrimaryClaimant.ClaimantEntity.EntityXContactInfoList.Count > 0)
                                    {
                                        foreach (EntityXContactInfo objEntityContact in objClaim.PrimaryClaimant.ClaimantEntity.EntityXContactInfoList)
                                        {
                                            if (!string.IsNullOrEmpty(objEntityContact.Email))
                                            {
                                                atpos = objEntityContact.Email.IndexOf("@");
                                                dotpos = objEntityContact.Email.LastIndexOf(".");
                                                if (atpos > 1 || dotpos >= atpos + 2)
                                                {
                                                    if (!string.IsNullOrEmpty(sToEmail))
                                                        sToEmail = sToEmail + "," + objEntityContact.Email;
                                                    else
                                                        sToEmail = objEntityContact.Email;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (objClaim.Context.LocalCache.GetTableId("CLAIM_ADJUSTER") == Conversion.ConvertStrToInteger(s.Replace("*", "")))
                            {
                                bEntityAttached = true;
                                if (objClaim.CurrentAdjuster.CurrentAdjFlag == true)
                                {
                                    if (!string.IsNullOrEmpty(objClaim.CurrentAdjuster.AdjusterEntity.EmailAddress))
                                    {
                                        atpos = objClaim.CurrentAdjuster.AdjusterEntity.EmailAddress.IndexOf("@");
                                        dotpos = objClaim.CurrentAdjuster.AdjusterEntity.EmailAddress.LastIndexOf(".");
                                        if (atpos > 1 || dotpos >= atpos + 2)
                                        {
                                            if (!string.IsNullOrEmpty(sToEmail))
                                                sToEmail = sToEmail + "," + objClaim.CurrentAdjuster.AdjusterEntity.EmailAddress;
                                            else
                                                sToEmail = objClaim.CurrentAdjuster.AdjusterEntity.EmailAddress;
                                        }
                                    }
                                    if (objClaim.CurrentAdjuster.AdjusterEntity.EntityXContactInfoList.Count > 0)
                                    {
                                        foreach (EntityXContactInfo objEntityContact in objClaim.CurrentAdjuster.AdjusterEntity.EntityXContactInfoList)
                                        {
                                            if (!string.IsNullOrEmpty(objEntityContact.Email))
                                            {
                                                atpos = objEntityContact.Email.IndexOf("@");
                                                dotpos = objEntityContact.Email.LastIndexOf(".");
                                                if (atpos > 1 || dotpos >= atpos + 2)
                                                {
                                                    if (!string.IsNullOrEmpty(sToEmail))
                                                        sToEmail = sToEmail + "," + objEntityContact.Email;
                                                    else
                                                        sToEmail = objEntityContact.Email;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //change end here by swati
                if (bEntityAttached == false)
                {
                    sMessage = Globalization.GetString("MergeManager.NoEntity.Message", m_iClientId);//dvatsa-cloud
                }
                else if (sFromEmail == string.Empty)
                {
                    sMessage = Globalization.GetString("MergeManager.NoSenderEmail.Message", m_iClientId);//dvatsa-cloud
                }
                else if (sToEmail == string.Empty)
                {
                    sMessage = Globalization.GetString("MergeManager.NoEntityEmail.Message", m_iClientId);//dvatsa-cloud
                }

                #endregion

                #region Send mail
                if (sFromEmail != string.Empty && sToEmail != string.Empty)
                {
                    objMailer = new Mailer(m_iClientId);
                    if (File.Exists(p_sFileName))
                    {
                        objMailer.AddAttachment(p_sFileName);
                        objMailer.To = sToEmail;
                        objMailer.From = sFromEmail;
                        objMailer.Subject = sSubject;
                        objMailer.Body = sBody;
                        objMailer.IsBodyHtml = false;
                        objMailer.SendMail();
                        sMessage = Globalization.GetString("MergeManager.MailSend.Message", m_iClientId);//dvatsa-cloud
                    }
                    else
                    {
                        objFileStream = new FileStream(p_sFileName, FileMode.Create);
                        objBinaryWriter = new BinaryWriter(objFileStream);
                        objBinaryWriter.Write(objMemory.ToArray());
                        objMemory.Close();
                        objBinaryWriter.Close();
                        objFileStream.Close();

                        objMailer.AddAttachment(p_sFileName);
                        objMailer.To = sToEmail;
                        objMailer.From = sFromEmail;
                        objMailer.Subject = sSubject.Replace("\r\n", ""); // Pradyumna 11/15/2014 MITS 36930
                        objMailer.Body = sBody;
                        objMailer.IsBodyHtml = false;
                        objMailer.SendMail();
                        sMessage = Globalization.GetString("MergeManager.MailSend.Message", m_iClientId);//dvatsa-cloud
                        File.Delete(p_sFileName);
                    }
                }
                #endregion
                return sMessage;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                if (p_objException.InnerException.ToString().Contains("specified string"))
                    throw new RMAppException(Globalization.GetString("MergeManager.MailFormat.Error", m_iClientId), p_objException);//dvatsa-cloud
                else
                    throw new RMAppException(Globalization.GetString("MergeManager.SendMailRTF.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                objMemory = null;
                objCache = null;
                objClaim = null;
                //objEmp = null;
                objEvent = null;
                sbSQL = null;
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
                if (objMailer != null)
                    objMailer.Dispose();
                if (objFileStream != null)
                    objFileStream.Dispose();
                if (objBinaryWriter != null)
                    objBinaryWriter.Dispose();
            }
        }
        /// <summary>
        /// Function to fetch value from rmA for a corresponding variable used while configuring merge email. 
        /// </summary>
        /// <param name="p_sTableName"></param>
        /// <param name="p_sVar"></param>
        /// <param name="p_objClaim"></param>
        /// <param name="p_objEvent"></param>
        /// <returns>string containing the value in rmA for a particular variable</returns>
        /// Added by Pradyumna 11/15/2014 MITS 36930
        private string GetVariableValue(string p_sTableName, string p_sVar, Claim p_objClaim, Event p_objEvent)
        {
            string sVariable = string.Empty;
            int iPolicyID = 0;
            bool bReturn = false;
            Employee objEmp = null;
            LocalCache objCache = null;
            DataModelFactory objDMF = null;
            Policy objPolicy = null;

            try
            {
                objCache = new LocalCache(m_sDSN, m_iClientId);

                if (string.Compare(p_sTableName, "claim", true) == 0)
                {
                    using (DbReader objRdr = DbFactory.GetDbReader(m_sDSN, "SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID =" + p_objClaim.ClaimId))
                    {
                        if (objRdr.Read())
                        {
                            iPolicyID = Conversion.CastToType<int>(Convert.ToString(objRdr.GetValue("POLICY_ID")), out bReturn);
                        }
                        if (iPolicyID != 0)
                        {
                            objDMF = new DataModelFactory(m_userLogin, m_iClientId);
                            objPolicy = (Policy)objDMF.GetDataModelObject("Policy", false);
                            objPolicy.MoveTo(iPolicyID);
                        }
                    }
                    switch (p_sVar.ToUpper())
                    {
                        case "CD":
                            sVariable = Common.Conversion.GetDBDateFormat(p_objClaim.DateOfClaim, "MM/dd/yyyy");
                            break;
                        case "CN":
                            sVariable = p_objClaim.ClaimNumber;
                            break;
                        case "CS":
                            sVariable = objCache.GetShortCode(p_objClaim.ClaimStatusCode) + " " + objCache.GetCodeDesc(p_objClaim.ClaimStatusCode);
                            break;
                        case "CLN":
                            if (p_objClaim.ClaimantList.Count > 0)
                            {
                                foreach (Claimant objClaimant in p_objClaim.ClaimantList)
                                {
                                    sVariable = objClaimant.ClaimantEntity.LastName + ", " + objClaimant.ClaimantEntity.FirstName;
                                    break;
                                }
                            }
                            break;
                        case "DPT":
                            sVariable = objCache.GetEntityLastName(p_objEvent.DeptEid);
                            break;
                        case "EN":
                            sVariable = p_objClaim.EventNumber;
                            break;
                        case "ED":
                            sVariable = Common.Conversion.GetDBDateFormat(p_objEvent.DateOfEvent, "MM/dd/yyyy");
                            break;
                        case "ENO":
                            foreach (PersonInvolved objPerInv in p_objEvent.PiList)
                            {
                                if (string.Compare(objCache.GetShortCode(objPerInv.PiTypeCode), "E", true) == 0)
                                {
                                    objEmp = (Employee)objDMF.GetDataModelObject("Employee", false);
                                    objEmp.MoveTo(objPerInv.PiEid);
                                    sVariable = objEmp.EmployeeNumber;
                                    break;
                                }
                            }
                            break;
                        case "EMPN":
                            foreach (PersonInvolved objPerInv in p_objEvent.PiList)
                            {
                                if (string.Compare(objCache.GetShortCode(objPerInv.PiTypeCode), "E", true) == 0)
                                {
                                    sVariable = objPerInv.PiEntity.LastName + ", " + objPerInv.PiEntity.FirstName;
                                    break;
                                }
                            }
                            break;
                        case "JU":
                            string sStateCode = string.Empty; string sStateDesc = string.Empty;
                            objCache.GetStateInfo(p_objClaim.FilingStateId, ref sStateCode, ref sStateDesc);
                            sVariable = sStateCode + " " + sStateDesc;
                            break;
                        case "PN":
                            if (objPolicy != null)
                            {
                                sVariable = objPolicy.PolicyName;
                            }
                            break;
                        case "PNO":
                            if (objPolicy != null)
                            {
                                sVariable = objPolicy.PolicyNumber;
                            }
                            break;
                        case "PS":
                            if (objPolicy != null)
                            {
                                sVariable = objPolicy.PolicySymbol;
                            }
                            break;
                        case "ADJEMAIL": //Added by Pradyumna 11/15/2014 for MITS 36930
                            if (p_objClaim.AdjusterList != null && p_objClaim.AdjusterList.Count > 0)
                            {
                                if (!String.IsNullOrEmpty(p_objClaim.CurrentAdjuster.AdjusterEntity.EmailAddress))
                                {
                                    sVariable = p_objClaim.CurrentAdjuster.AdjusterEntity.EmailAddress.Trim();
                                }
                            }
                            break;
                    }
                }
                else if (string.Compare(p_sTableName, "event", true) == 0)
                {
                    switch (p_sVar)
                    {
                        case "DPT":
                            sVariable = objCache.GetEntityLastName(p_objEvent.DeptEid);
                            break;
                        case "EN":
                            sVariable = p_objEvent.EventNumber;
                            break;
                        case "ED":
                            sVariable = Common.Conversion.GetDBDateFormat(p_objEvent.DateOfEvent, "MM/dd/yyyy");
                            break;
                        case "ENO":
                            foreach (PersonInvolved objPerInv in p_objEvent.PiList)
                            {
                                if (string.Compare(objCache.GetShortCode(objPerInv.PiTypeCode), "E", true) == 0)
                                {
                                    objEmp = (Employee)objDMF.GetDataModelObject("Employee", false);
                                    objEmp.MoveTo(objPerInv.PiEid);
                                    sVariable = objEmp.EmployeeNumber;
                                    break;
                                }
                            }
                            break;
                        case "EMPN":
                            foreach (PersonInvolved objPerInv in p_objEvent.PiList)
                            {
                                if (string.Compare(objCache.GetShortCode(objPerInv.PiTypeCode), "E", true) == 0)
                                {
                                    sVariable = objPerInv.PiEntity.LastName.Trim() + ", " + objPerInv.PiEntity.FirstName.Trim();
                                    break;
                                }
                            }
                            break;
                        default:
                            sVariable = string.Empty;
                            break;
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (objEmp != null)
                    objEmp.Dispose();
                if (objCache != null)
                    objCache.Dispose();
                if (objDMF != null)
                    objDMF.Dispose();
                if (objPolicy != null)
                    objPolicy.Dispose();
            }
            return sVariable.Trim();
        }

        #endregion

        //Starts : Nitika Added This Function here for AutoMailmerge Functionality
        public void AttachDocument(string p_sFileName, MemoryStream objMemory, string p_sCatid, string p_sSubject,
                                    string p_sName, string p_sNotes, string p_sKeywords, string p_sClass, string p_sCategory,
                                    string p_sType, string p_sRecordId, int iPsid, string p_sFormName)
        {
            XmlDocument objDOM = null;
            DocumentManager objDocumentManager = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            long lDocumentId = 0;
            SysSettings objSettings = null;
            Acrosoft objAcrosoft = null;
            BinaryReader binaryRdr = null;
            Byte[] fileBuffer = null;
            MediaView objMediaView = null;
            int iRetCd = 0;
            string sAppExcpXml = "";
            objAcrosoft = new Acrosoft(m_iClientId);//dvatsa-cloud
            Claim objClaim = null;
            Event objEvent = null;
            DataModelFactory objDataModelFactory = null;
            long lRecordId = 0;
            Policy objPolicy = null;
            // Start Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan
            PolicyEnh objPolicyEnh = null;
            // End Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan
            ////Mona:PaperVisionMerge : Animesh
            PaperVisionDocumentManager objPVDocManager;
            PVWrapper objPaperVision;
            // atavaragiri : MITS 25696 //
            string sTemp = String.Empty;
            XmlDocument objXmlDoc = new XmlDocument();
            XmlNode objNode = null;
            XmlNodeList objNodeList = null;
            bool bEquals = false;
            string ssubAppExcpXml = String.Empty;
            string sFolderId = String.Empty;
            int iTemp = 0;
            //end:MITS 25696

            //Animesh Insertion Ends
            try
            {
                objDocumentManager = new DocumentManager(m_userLogin, m_iClientId);
                objDocumentManager.ConnectionString = m_sDSN;
                objDocumentManager.SecurityConnectionString = m_sSecDSN; //Amandeep MITS 28800
                objDocumentManager.UserLoginName = m_userLogin.LoginName;
                //mgaba2:MITS 11704-04/07/2008-if doc path entered at user level-Start
                /*if (userLogin.objRiskmasterDatabase.DocPathType == 0)
                {
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else
                {
                    objDocumentManager.DocumentStorageType = StorageType.DatabaseStorage;
                }*/



                if (this.m_userLogin.DocumentPath.Length > 0)
                {
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else
                {
                    if (m_userLogin.objRiskmasterDatabase.DocPathType == 0)
                    {
                        objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                    }
                    else if (m_userLogin.objRiskmasterDatabase.DocPathType == 1)
                    {
                        objDocumentManager.DocumentStorageType = StorageType.DatabaseStorage;//rkaur27: RMACLOUD - 2381
                    }
                    else
                    {
                        objDocumentManager.DocumentStorageType = StorageType.S3BucketStorage;
                    }
                }
                //mgaba2:MITS 11704-04/07/2008-End
                //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //objDocumentManager.DestinationStoragePath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.m_userLogin.DocumentPath.Length > 0)
                {
                    objDocumentManager.DestinationStoragePath = m_userLogin.DocumentPath;
                }
                else
                {
                    objDocumentManager.DestinationStoragePath = m_userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("data");
                objDOM.AppendChild(objElemParent);

                objElemChild = objDOM.CreateElement("Document");

                objElemTemp = objDOM.CreateElement("DocumentId");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("FolderId");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("CreateDate");
                //Mona:PaperVisionMerge : Animesh
                //objElemTemp.InnerText = Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now);
                objElemTemp.InnerText = DateTime.Now.ToString();
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Category");
                objElemTemp.InnerText = p_sCategory;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Name");
                objElemTemp.InnerText = p_sName;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Class");
                objElemTemp.InnerText = p_sClass;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Subject");
                objElemTemp.InnerText = p_sSubject;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Type");
                objElemTemp.InnerText = p_sType;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Notes");
                objElemTemp.InnerText = p_sNotes;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("UserLoginName");
                objElemTemp.InnerText = m_userLogin.LoginName;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("FileName");
                objElemTemp.InnerText = p_sFileName;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("FilePath");
                objElemTemp.InnerText = "";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Keywords");
                objElemTemp.InnerText = p_sKeywords;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("AttachTable");
                //Shruti for 7918
                if (Conversion.ConvertStrToInteger(p_sCatid) == 20)
                {
                    objElemTemp.InnerText = p_sFormName.ToUpper();
                }
                else
                {
                    objElemTemp.InnerText = CatIdToRMTableName(Conversion.ConvertStrToLong(p_sCatid));
                }
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Subject");
                objElemTemp.InnerText = p_sSubject;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("AttachRecordId");
                objElemTemp.InnerText = p_sRecordId;
                objElemChild.AppendChild(objElemTemp);

                //Nikhil Garg		Defect No: 2397		Dated: 03/10/2006
                objElemTemp = objDOM.CreateElement("FormName");
                objElemTemp.InnerText = p_sFormName;
                objElemChild.AppendChild(objElemTemp);

                objDOM.FirstChild.AppendChild(objElemChild);

                //08/10/2006.. Raman Bhatia : If Acrosoft is enabled then legacy document management is not used
                //Instead store attachment using Acrosoft

                objSettings = new SysSettings(m_sDSN, m_iClientId);//dvatsa-cloud
                ////Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                if (objSettings.UsePaperVisionInterface == true)
                {
                    string sTableName = CatIdToRMTableName(Conversion.ConvertStrToLong(p_sCatid));
                    if (sTableName.ToLower() == "claim")
                    {
                        objPaperVision = new PVWrapper();
                        objPaperVision.PVEntityID = PaperVisionSection.EntityID;
                        objPaperVision.PVLoginUrl = PaperVisionSection.LoginUrl;
                        objPaperVision.PVProjID = PaperVisionSection.ProjectID;
                        objPaperVision.PVServerAddress = PaperVisionSection.PVServerAddress;
                        objPaperVision.PVShowDocLinkUrl = PaperVisionSection.DocumentLink;
                        //Animesh Inserted MITS 18345
                        objPaperVision.SharedNetworkLocationPath = PaperVisionSection.SharedNetworkLocationPath;
                        //objPaperVision.MappedDriveName = PaperVisionSection.MappedDriveName;
                        objPaperVision.DomainName = PaperVisionSection.DomainName;
                        objPaperVision.UserID = PaperVisionSection.UserID;
                        objPaperVision.Password = PaperVisionSection.Password;
                        //Animesh Insertion ends
                        objPVDocManager = new PaperVisionDocumentManager(m_userLogin, m_iClientId);
                        objPVDocManager.ConnectionString = this.m_sDSN;
                        objPVDocManager.UserLoginName = m_userLogin.LoginName;
                        objPVDocManager.AddDocument(objDOM.InnerXml, objMemory, iPsid, objPaperVision, out lDocumentId);
                    }
                    else
                    {
                        //commented & added by Swati Agarwal
                        // throw new RMAppException("PaperVision Document System is active. Attachments functionality will only be available for claims.");
                        throw new RMAppException(Globalization.GetString("MergeManager.SendAttachments.Error", m_iClientId));//dvatsa-cloud
                    }

                }
                //Animesh Insertion Ends
                else if (objSettings.UseAcrosoftInterface == false && objSettings.UseMediaViewInterface == false)
                {
                    objDocumentManager.AddDocument(objDOM.InnerXml, objMemory, iPsid, out lDocumentId);
                } // if
                else if (objSettings.UseAcrosoftInterface)
                {
                    binaryRdr = new BinaryReader(objMemory);
                    fileBuffer = binaryRdr.ReadBytes(Convert.ToInt32(objMemory.Length));
                    binaryRdr.Close();

                    string sTableName = CatIdToRMTableName(Conversion.ConvertStrToLong(p_sCatid));
                    string sPath = RMConfigurator.UserDataPath;
                    string sFullFileName = sPath + @"\MailMerge\" + p_sFileName;
                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = m_userLogin.LoginName;
                    string sTempAcrosoftPassword = m_userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }
                    switch (sTableName.ToLower())
                    {
                        case "claim": objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//dvatsa-cloud
                            objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
                            string sClaimNumber = objClaim.ClaimNumber;
                            string sEventNumber = objClaim.EventNumber;
                            //creating folder
                            objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                                AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);

                            // start: atavaragiri :MITS 25696:added condition to check for sub foldername from config file//
                            // if empty,the attachment will get created for root folder //

                            if (String.IsNullOrEmpty(AcrosoftSection.AcrosoftSubFolderName))
                            {

                                iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                    p_sFileName, fileBuffer, p_sName, p_sType, sEventNumber, sClaimNumber,
                                    AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", sTempAcrosoftUserId, out sAppExcpXml);
                            }

                            //creation of a sub folder at Acrosoft for mail merge attachments:MITS 25696//
                            else
                            {

                                objAcrosoft.GetAttachmentFolderDocumentList(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                     sEventNumber, sClaimNumber, AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml, out sTemp); //function used here to retrieve the folder id of the parent folder

                                objXmlDoc.LoadXml(sAppExcpXml);

                                objNode = objXmlDoc.SelectSingleNode(@"/FolderDocuments/Folder/FolderId");//extract folder id of parent
                                objNodeList = objXmlDoc.SelectNodes("//Title");
                                sFolderId = objNode.InnerText;

                                while (iTemp < objNodeList.Count)
                                {
                                    bEquals = String.Equals(objNodeList[iTemp].InnerText, AcrosoftSection.AcrosoftSubFolderName);
                                    if (bEquals == true)
                                        break;
                                    iTemp++;
                                }
                                if (bEquals == false)
                                    // sub folder creation//
                                    objAcrosoft.CreateSubFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, sFolderId, AcrosoftSection.AcrosoftSubFolderName, out ssubAppExcpXml);

                                // storing document in sub folder created //
                                iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                    p_sFileName, fileBuffer, p_sName, p_sType, AcrosoftSection.AcrosoftAttachmentsTypeKey, sFolderId, AcrosoftSection.AcrosoftSubFolderName, sTempAcrosoftUserId, out sAppExcpXml);


                            }

                            // end :MITS 25696 //

                            //storing document
                            //01/07/2007 Raman Bhatia: Document needs to be picked from memorystream
                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, sFullFileName, p_sSubject, p_sType, sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, p_sFileName, fileBuffer, p_sSubject, p_sType, sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
                            //gagnihotri MITS 17673 Document should be saved with the given title.




                            objClaim = null;
                            objAcrosoft = null;


                            break;
                        case "event": objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//dvatsa-cloud
                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                            objEvent.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
                            sEventNumber = objEvent.EventNumber;
                            //creating folder
                            objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, "0", AcrosoftSection.EventFolderFriendlyName,
                                AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                            //storing document		
                            //01/07/2007 Raman Bhatia: Document needs to be picked from memorystream
                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, sFullFileName, p_sSubject, p_sType, sEventNumber, "0", AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", userLogin.LoginName, out sAppExcpXml);
                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, p_sFileName, fileBuffer, p_sSubject, p_sType, sEventNumber, "0", AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
                            //gagnihotri MITS 17673 Document should be saved with the given title.
                            // atavaragiri :MITS 25696//
                            if (String.IsNullOrEmpty(AcrosoftSection.AcrosoftSubFolderName))
                            {

                                iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword, p_sFileName,
                               fileBuffer, p_sName, p_sType, sEventNumber, "0", AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "",
                               sTempAcrosoftUserId, out sAppExcpXml);
                            }

                                //creation of a sub folder at Acrosoft for mail merge attachments:MITS 25696//
                            else
                            {

                                objAcrosoft.GetAttachmentFolderDocumentList(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                     sEventNumber, "", AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml, out sTemp); //function used here to retrieve the folder id of the parent folder

                                objXmlDoc.LoadXml(sAppExcpXml);

                                objNode = objXmlDoc.SelectSingleNode(@"/FolderDocuments/Folder/FolderId");//extract folder id of parent
                                objNodeList = objXmlDoc.SelectNodes("//Title");
                                sFolderId = objNode.InnerText;

                                while (iTemp < objNodeList.Count)
                                {
                                    bEquals = String.Equals(objNodeList[iTemp].InnerText, AcrosoftSection.AcrosoftSubFolderName);
                                    if (bEquals == true)
                                        break;
                                    iTemp++;
                                }
                                if (bEquals == false)
                                    // sub folder creation//
                                    objAcrosoft.CreateSubFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, sFolderId, AcrosoftSection.AcrosoftSubFolderName, out ssubAppExcpXml);

                                // storing document in sub folder created //
                                iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                    p_sFileName, fileBuffer, p_sName, p_sType, AcrosoftSection.AcrosoftAttachmentsTypeKey, sFolderId, AcrosoftSection.AcrosoftSubFolderName, sTempAcrosoftUserId, out sAppExcpXml);


                            }

                            // end :MITS 25696 //
                            objEvent = null;
                            objAcrosoft = null;
                            objDataModelFactory = null;
                            break;
                        case "policy": objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//dvatsa-cloud
                            objPolicy = (Policy)objDataModelFactory.GetDataModelObject("Policy", false);
                            objPolicy.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
                            //creating folder
                            //gagnihotri R5 MCM changes
                            //objAcrosoft.CreatePolicyFolder(base.userLogin.LoginName, base.userLogin.Password, objConfig.GetValue("AcrosoftPolicyTypeKey"), objPolicy.PolicyName, objConfig.GetValue("PolicyFolderFriendlyName"), out sAppExcpXml);
                            objAcrosoft.CreatePolicyFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                AcrosoftSection.AcrosoftPolicyTypeKey, objPolicy.PolicyName, AcrosoftSection.PolicyFolderFriendlyName, out sAppExcpXml);
                            //storing document		   
                            //01/07/2007 Raman Bhatia: Document needs to be picked from memorystream
                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, sFullFileName, p_sSubject, p_sType, objPolicy.PolicyName, objConfig.GetValue("AcrosoftPolicyTypeKey"), "", "", userLogin.LoginName, out sAppExcpXml);
                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, p_sFileName, fileBuffer, p_sSubject, p_sType, objPolicy.PolicyName, AcrosoftSection.AcrosoftPolicyTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
                            //gagnihotri MITS 17673 Document should be saved with the given title.
                            iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword, p_sFileName,
                                fileBuffer, p_sName, p_sType, objPolicy.PolicyName, AcrosoftSection.AcrosoftPolicyTypeKey,
                                "", "", sTempAcrosoftUserId, out sAppExcpXml);
                            break;
                        // Start Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan
                        case "policyenh": objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//dvatsa-cloud
                            objPolicyEnh = (PolicyEnh)objDataModelFactory.GetDataModelObject("PolicyEnh", false);
                            objPolicyEnh.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
                            //creating folder
                            //gagnihotri R5 MCM changes
                            //objAcrosoft.CreatePolicyFolder(base.userLogin.LoginName, base.userLogin.Password, objConfig.GetValue("AcrosoftPolicyTypeKey"), objPolicy.PolicyName, objConfig.GetValue("PolicyFolderFriendlyName"), out sAppExcpXml);
                            objAcrosoft.CreatePolicyFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                AcrosoftSection.AcrosoftPolicyTypeKey, objPolicyEnh.PolicyName, AcrosoftSection.PolicyFolderFriendlyName, out sAppExcpXml);
                            //atavaragiri :MITS 28922 :incorrect variable was being passed as a parameter for policy name//

                            //storing document		   
                            //01/07/2007 Raman Bhatia: Document needs to be picked from memorystream
                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, sFullFileName, p_sSubject, p_sType, objPolicy.PolicyName, objConfig.GetValue("AcrosoftPolicyTypeKey"), "", "", userLogin.LoginName, out sAppExcpXml);
                            //gagnihotri R5 MCM changes
                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, p_sFileName, fileBuffer, p_sSubject, p_sType, objPolicy.PolicyName, objConfig.GetValue("AcrosoftPolicyTypeKey"), "", "", m_userLogin.LoginName, out sAppExcpXml);
                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, p_sFileName, fileBuffer, p_sSubject, p_sType, objPolicy.PolicyName, AcrosoftSection.AcrosoftPolicyTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
                            //gagnihotri MITS 17673 Document should be saved with the given title.
                            iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                p_sFileName, fileBuffer, p_sName, p_sType, objPolicyEnh.PolicyName,
                                AcrosoftSection.AcrosoftPolicyTypeKey, "", "", sTempAcrosoftUserId, out sAppExcpXml);
                            //atavaragiri :MITS 28922 :incorrect variable was being passed as a parameter for policy name//
                            break;
                        // End Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan  
                        default:

                            break;
                    } // switch
                    //rsolanki2 :  end updates for MCM mits 19200 
                    //objDataModelFactory.UnInitialize();
                } // else
                else if (objSettings.UseMediaViewInterface)
                {
                    objMediaView = new MediaView();
                    binaryRdr = new BinaryReader(objMemory);
                    fileBuffer = binaryRdr.ReadBytes(Convert.ToInt32(objMemory.Length));
                    binaryRdr.Close();
                    string sRecordNumber = "";
                    string sRecordType = "";
                    string sTableName = CatIdToRMTableName(Conversion.ConvertStrToLong(p_sCatid));
                    string sPath = RMConfigurator.UserDataPath;
                    string sFullFileName = sPath + @"\MailMerge\" + p_sFileName;

                    WriteBinaryFile(sFullFileName, fileBuffer);


                    switch (sTableName.ToLower())
                    {
                        case "claim": objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//dvatsa-cloud
                            objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
                            sRecordNumber = objClaim.ClaimNumber;
                            sRecordType = "Claim";
                            break;

                        case "event": objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//dvatsa-cloud
                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                            objEvent.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
                            sRecordNumber = objEvent.EventNumber;
                            sRecordType = "Event";
                            break;

                        case "policy": objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//dvatsa-cloud
                            objPolicy = (Policy)objDataModelFactory.GetDataModelObject("Policy", false);
                            objPolicy.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
                            sRecordNumber = objPolicy.PolicyName;
                            sRecordType = "Policy";
                            break;
                        // Start Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan
                        case "policyenh": objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//dvatsa-cloud
                            objPolicyEnh = (PolicyEnh)objDataModelFactory.GetDataModelObject("PolicyEnh", false);
                            objPolicyEnh.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
                            sRecordNumber = objPolicyEnh.PolicyName;
                            sRecordType = "Policy";
                            break;
                        // End Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan  
                        default:

                            break;
                    }

                    importList oList = new importList(sRecordNumber, sRecordType, p_sName);
                    oList.MMNotes = p_sNotes;
                    oList.MMDescription = p_sSubject;
                    objMediaView.MMUploadDocument(p_sFileName, sPath + @"\MailMerge", oList);

                } // else
            }

            finally
            {
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                }
                objDOM = null;
                objDocumentManager = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                objSettings = null;
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
                if (objEvent != null)
                {
                    objEvent.Dispose();
                }
                if (objPolicy != null)
                {
                    objPolicy.Dispose();
                }
                if (objPolicyEnh != null)
                {
                    objPolicyEnh.Dispose();
                }
                objPaperVision = null;//Mona:PaperVisionMerge : Animesh
                objPVDocManager = null;
            }
        }

        public void WriteBinaryFile(string fileName, byte[] fileBuffer)
        {
            FileStream fileStream;
            BinaryWriter binaryWrtr;

            try
            {
                // create new or overwrite existing
                fileStream = new FileStream(fileName, FileMode.Create);
                binaryWrtr = new BinaryWriter(fileStream);
                binaryWrtr.Write(fileBuffer);

                binaryWrtr.Close();
                fileStream.Close();
            }
            catch (Exception ex)
            {

            }
        }

        //Ends : Nitika Added This Function here for AutoMailmerge Functionality
    }
}
