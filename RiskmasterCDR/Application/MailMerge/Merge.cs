﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;

namespace Riskmaster.Application.MailMerge
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   29th November 2004
	///Purpose :   This class Creates, Edits, Fetches merge information.
	/// </summary>
	public class Merge : IDisposable
	{
		#region Constants Declaration
		/// <summary>
		/// Constant representing the integer value of claim merge
		/// </summary>
		private const int  CLAIM_MERGE = 1;
		/// <summary>
		/// Constant representing the integer value of event merge
		/// </summary>
		private const int  EVENT_MERGE = 2;
		/// <summary>
		/// Constant representing the integer value of employee merge
		/// </summary>
		private const int  EMPLOYEE_MERGE = 3;
		/// <summary>
		/// Constant representing the integer value of entity merge
		/// </summary>
		private const int  ENTITY_MERGE = 4;
		/// <summary>
		/// Constant representing the integer value of vehicle merge
		/// </summary>
		private const int  VEHICLE_MERGE = 5;
		/// <summary>
		/// Constant representing the integer value of policy merge
		/// </summary>
		private const int  POLICY_MERGE = 6;
		/// <summary>
		/// Constant representing the integer value of payment merge
		/// </summary>
		private const int  PAYMENT_MERGE = 7;
		/// <summary>
		/// Constant representing the integer value of patient merge
		/// </summary>
		private const int  PATIENT_MERGE = 8;
		/// <summary>
		/// Constant representing the integer value of physician merge
		/// </summary>
		private const int  PHYSICIAN_MERGE = 9;
		/// <summary>
		/// Constant representing the integer value of medical staff merge
		/// </summary>
		private const int  MED_STAFF_MERGE = 10;
		/// <summary>
		/// Constant representing the integer value of admin tracking merge
		/// </summary>
		private const int  AT_MERGE = 20;
		/// <summary>
		/// Constant representing the integer value of disablility plan merge
		/// </summary>
		private const int  DIS_PLAN_SEARCH_MERGE = 11;
        //Shruti
        /// <summary>
        /// Leave Plan merge
        /// </summary>
        private const int LEAVE_PLAN_MERGE = 23;
        //Shruti
        /// <summary>
        /// Policy Billing merge
        /// </summary>
        private const int POLICY_BILLING_MERGE = 22;

        // Start Naresh Enhanced Policy Merge
        /// <summary>
        /// Constant representing the integer value of Enhanced Policy merge
        /// </summary>
        private const int ENHANCED_POLICY_MERGE = 21;
        // End Naresh Enhanced Policy Merge
        //Anu Tennyson for MITS 18291 : STARTS 10/29/2009
        private const int PROPERTY_MERGE = 12;
        //Anu Tennyson for MITS 18291 : ENDS
        private const int DRIVER_MERGE = 24;  //Aman Driver Enh
		#endregion

		#region Variables Declaration
		/// <summary>
		/// Document storage path
		/// </summary>
		private string m_sDocStorageDSN=""; 
		/// <summary>
		/// Xml string containing the merged data
		/// </summary>
		private string m_sXMLData=""; 
		/// <summary>
		/// Template used in merge
		/// </summary>
		private Template m_objCurTemplate=null; 
		/// <summary>
		/// MergeManager class object
		/// </summary>
		private IManager m_objManager=null; 
		/// <summary>
		/// Connection string for database
		/// </summary>
		private string m_sConnectionString="";
		/// <summary>
		/// Type of database ORACLE,SQLSERVER etc.
		/// </summary>
		private long m_lDbMake=0;
		/// <summary>
		/// File name of newly created DOC/RTF file
		/// </summary>
		private string m_sResultFileName=""; 
		/// <summary>
		/// Record id to which to attach a doc
		/// </summary>
		private long m_RecordId=0; 
		/// <summary>
		/// Row id to use for merge
		/// </summary>
		private long m_lRowId=0; 
		/// <summary>
		/// Persist a newly created file or not
		/// </summary>
		private bool m_bPersistResultFile=false; 
		/// <summary>
		/// Whether to attach a doc or not
		/// </summary>
		private bool m_bAttachFlag=false;
		/// <summary>
		/// Whether to create ms doc file or not
		/// </summary>
		private bool m_bDocResultFlag=false; 
		/// <summary>
		/// Title of attached document
		/// </summary>
		private string m_sAttachTitle=""; 
		/// <summary>
		/// Notes of attached document
		/// </summary>
		private string m_sAttachNotes=""; 
		/// <summary>
		/// Keyword to attached document
		/// </summary>
		private string m_sAttachKeyWords="";
		/// <summary>
		/// Columen headers for resultant merge records
		/// </summary>
		private ArrayList m_arrlstColumnHeaders=null;
		/// <summary>
		/// Fields types of fields contained in record
		/// </summary>
		private ArrayList m_arrlstFieldTypes=null;
		/// <summary>
		/// Secure database connect string
		/// </summary>
		private string m_sSecureConnectString="";
		/// <summary>
		/// DBConnection object
		/// </summary>
		DbConnection m_objMergeConn=null;
		/// <summary>
		/// DBCommand object
		/// </summary>
		private DbCommand m_objMergeCmd=null;
		/// <summary>
		/// Dataset containing merge data
		/// </summary>
        //tkatsarski: 03/13/15 - RMA-1768: The property is no longer static, because it was causing problems when two users try to mail merge
        private DataSet m_objMergeds = null;
        /// <summary>
		/// Db storage or file storage
		/// </summary>
		private long m_iDBStorageType = 0;
		///<summary>
		/// Generic class object 
		/// </summary>
		private Generic m_objGeneric=null;

		private bool m_bIsPreFab = false;

		private UserLogin m_userLogin = null;
		
       
        // Start : Properties added By Nitika For AutoMailMerge Set Up.
        private int m_iClientId = 0;//dvatsa-cloud

        /// <summary>
        /// m_DefendantType
        /// </summary>
        
        public string m_DefendantType = string.Empty;
        /// <summary>
        /// m_ClaimantType
        /// </summary>
        public string m_ClaimantType = string.Empty;
        /// <summary>
        /// m_ExpertType
        /// </summary>
        public string m_ExpertType = string.Empty;
        /// <summary>
        /// m_PersonInvolvedType
        /// </summary>
        public string m_PersonInvolvedType = string.Empty;
        /// <summary>
        /// m_CurrentAdjuster
        /// </summary>
        public string m_CurrentAdjuster = string.Empty;
        /// <summary>
        /// m_PrimaryCasemanager
        /// </summary>
        public string m_PrimaryCasemanager = string.Empty;

        //tkatsarski: 03/13/15 - RMA-1768
        private static readonly Random m_objGetRandom = new Random();
        private static readonly object m_objSyncLock = new object();
        // End : Properties added By Nitika For AutoMailMerge Set Up.
		#endregion

		#region Properties Declaration
        // Start : Properties added By Nitika For AutoMailMerge Set Up.
        /// <summary>
        /// Defendant type
        /// </summary>
        public string sDefendantType
        {
            get
            {
                return m_DefendantType;
            }
            set
            {
                m_DefendantType = value;
            }
        }

        /// <summary>
        /// Defendant type
        /// </summary>
        public string sClaimantType
        {
            get
            {
                return m_ClaimantType;
            }
            set
            {
                m_ClaimantType = value;
            }
        }

        /// <summary>
        /// Defendant type
        /// </summary>
        public string sExpertType
        {
            get
            {
                return m_ExpertType;
            }
            set
            {
                m_ExpertType = value;
            }
        }

        /// <summary>
        /// PersonInvolved type
        /// </summary>
        public string sPersonInvolvedType
        {
            get
            {
                return m_PersonInvolvedType;
            }
            set
            {
                m_PersonInvolvedType = value;
            }
        }

         /// <summary>
        /// CurrentAdjuster
        /// </summary>
        public string sCurrentAdjuster
        {
            get
            {
                return m_CurrentAdjuster;
            }
            set
            {
                m_CurrentAdjuster = value;
            }
        }

        // akaushik5 Added for MITS 37272 Starts
        /// <summary>
        /// Gets a value indicating whether [is adjuster selected].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is adjuster selected]; otherwise, <c>false</c>.
        /// </value>
        public bool IsAdjusterSelected
        {
            // akaushik5 changed for RMA-5115
            //get
            //{
            //    // npadhy - If the user has never opened Mail Merge screen from UI, then USER_PREF_XML will never be saved for him
            //    // In that case default AdjusterSelected to false
            //    try
            //    {
            //        string userPrefXml = string.Empty;
            //        string sSQL = string.Format("SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = {0} ORDER BY USER_ID DESC", this.m_userLogin.UserId);
            //        using (DbReader objReader = DbFactory.GetDbReader(this.m_sConnectionString, sSQL))
            //        {
            //            if (!object.ReferenceEquals(objReader, null) && objReader.Read())
            //            {
            //                userPrefXml = objReader.GetString("PREF_XML");
            //            }
            //            objReader.Close();
            //        }

            //        XDocument xDocument = XDocument.Parse(userPrefXml);
            //        string node = (from mergemail in xDocument.Descendants("mergemail")
            //                       let recordselect = mergemail.Element("recordselect")
            //                       select recordselect.Attribute("selected").Value).FirstOrDefault();
            //        if (node != null)
            //            return !node.Equals("0");
            //        else
            //            return false;
            //    }

            //    catch (Exception ex)
            //    {
            //        return false;
            //    }
            //}
            get;
            set;
            // akaushik5 changed for RMA-5115
        }
        // akaushik5 Added for MITS 37272 Ends

        /// <summary>
        /// PrimaryCasemanager
        /// </summary>
        public string sPrimaryCasemanager
        {
            get
            {
                return m_PrimaryCasemanager;
            }
            set
            {
                m_PrimaryCasemanager = value;
            }
        }
        // End : Properties added By Nitika For AutoMailMerge Set Up.
        
        /// <summary>
		/// Document storage path
		/// </summary>
		public string DocStorageDSN
		{
			get
			{
				return m_sDocStorageDSN;
			}
			set
			{
				m_sDocStorageDSN=value;
			}
		}

		public bool PreFab
		{
			get
			{
				return m_bIsPreFab;
			}
			set
			{
				m_bIsPreFab = value;
			}
		}

		/// <summary>
		/// File or Db storage
		/// </summary>
		public long StorageType
		{
			get
			{
				return m_iDBStorageType;
			}
			set
			{
                m_iDBStorageType = value;
			}
		}
		/// <summary>
		/// Template to use for merge
		/// </summary>
		public Template Template
		{
			get
			{
				return  m_objCurTemplate;
			}
			set
			{
				m_objCurTemplate=value;
			}
		}
		/// <summary>
		/// Recordid to which to attach a mail merge document
		/// </summary>
		public long RecordId
		{
			get
			{
				return m_RecordId;
			}
			set
			{
				m_RecordId=value;
			}
		}
		/// <summary>
		/// MergeManager object reference
		/// </summary>
		public IManager Manager
		{
			get
			{
				return m_objManager;
			}
			set
			{
				m_objManager=value;
			}
		}
		/// <summary>
		/// File name of newly created doc/rtf file
		/// </summary>
		public string ResultFile
		{
			get
			{
				return m_sResultFileName;
			}
			set
			{
				m_sResultFileName=value;
			}
		}
		/// <summary>
		/// Dataset containing data to be merged
		/// </summary>
        //tkatsarski: 03/13/15 - RMA-1768
        public DataSet Data
        {
			
			set
			{
				m_objMergeds=value;
			}
		}
		/// <summary>
		/// DbCommand object
		/// </summary>
		public DbCommand Command
		{
			get
			{
				return m_objMergeCmd;
			}
			set
			{
				m_objMergeCmd=value;
			}
		}
		
		/// <summary>
		/// Connection string to Database
		/// </summary>
		public string  DSN
		{
			set
			{
				m_sConnectionString=value;
				m_objMergeConn=DbFactory.GetDbConnection(m_sConnectionString);
				m_objMergeConn.Open();
				m_lDbMake=(int)m_objMergeConn.DatabaseType;
				m_objMergeConn.Close();
				m_objGeneric=new Generic(m_iClientId);//rkaur27
				m_objGeneric.Setproperty(m_lDbMake,m_sConnectionString);
			}
		}
	
		/// <summary>
		/// Connection string to Security database
		/// </summary>
		public string  SecurityDSN
		{
			set
			{
				m_sSecureConnectString=value;
			}
		}
		/// <summary>
		/// XML string containing data to be merged
		/// </summary>
		public string XMLData
		{
			get
			{
				if (m_sXMLData!="")
				{
					return m_sXMLData;
				}
				else
				{
					return GenerateXML();
				}
			}
			set
			{
				m_sXMLData=value;
			}
		}
		/// <summary>
		/// Selected Row id/Record for merge 
		/// </summary>
		public long RowId
		{
			get
			{
				return m_lRowId;
			}
			set
			{
				TemplateField objField=null;
				long lValue=0;
				IDictionaryEnumerator objEnum=null;
				try
				{								
					objEnum=this.Template.Fields.GetEnumerator();
					m_lRowId=value;
					if (m_objMergeds.Tables[0].Rows.Count> 0)
					{
						if ((m_lRowId) <= m_objMergeds.Tables[0].Rows.Count)
						{
							while (objEnum.MoveNext())
							{
								objField=(TemplateField)objEnum.Value;
								lValue=objField.SeqNum - 1;
								objField.Value=Conversion.ConvertObjToStr(m_objMergeds.Tables[0].Rows[
									Convert.ToInt32(m_lRowId)]["FLD" + (lValue.ToString())]);
							}
						}
					}
				}
				catch (Exception p_objException)
				{
                    throw new RMAppException(Globalization.GetString("Merge.SetRowId.Error", m_iClientId), p_objException);//dvatsa-cloud
				}
				finally
				{
					objEnum=null;
					objField=null;
				}
			}
		}
		/// <summary>
		/// To decide whether to attach a record to a document or not  
		/// </summary>
		public bool  AttachFlag
		{
			get
			{
				return m_bAttachFlag;
			}
			set
			{
				m_bAttachFlag=value;
			}
		}
		/// <summary>
		/// Document file to be created /not
		/// </summary>
		public bool  DocResultFlag
		{
			get
			{
				return m_bDocResultFlag;
			}
			set
			{
				m_bDocResultFlag=value;
			}
		}
		/// <summary>
		/// File to be attached to document or not
		/// </summary>
		public string  AttachTitle
		{
			get
			{
				return m_sAttachTitle;
			}
			set
			{
				m_sAttachTitle=value;
			}
		}
		/// <summary>
		/// Notes to attach to document
		/// </summary>
		public string  AttachNotes
		{
			get
			{
				return m_sAttachNotes;
			}
			set
			{
				m_sAttachNotes=value;
			}
		}
		/// <summary>
		/// Keyword to attach to a document
		/// </summary>
		public string  AttachKeyWords
		{
			get
			{
				return m_sAttachKeyWords;
			}
			set
			{
				m_sAttachKeyWords=value;
			}
		}
		/// <summary>
		/// To decide if we need to persist the newly created rtf/doc file or not
		/// </summary>
		public bool  PersistResultFile
		{
			get
			{
				return m_bPersistResultFile;
			}
			set
			{
				m_bPersistResultFile=value;
			}
		}
		#endregion

		#region Merge Constructor
		/// <summary>
		/// Default constructor
		/// </summary>
		public Merge(int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_arrlstColumnHeaders=new ArrayList();
			m_arrlstFieldTypes=new ArrayList();
			if (m_objMergeCmd!=null)
			{
				m_lDbMake=(int)m_objMergeCmd.Connection.DatabaseType;
                m_objGeneric = new Generic(p_iClientId);
				m_objGeneric.Setproperty(m_lDbMake,m_objMergeCmd.Connection.ConnectionString);
			}
		}
        public Merge(UserLogin p_userLogin, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_arrlstColumnHeaders=new ArrayList();
			m_arrlstFieldTypes=new ArrayList();
			if (m_objMergeCmd!=null)
			{
				m_lDbMake=(int)m_objMergeCmd.Connection.DatabaseType;
                m_objGeneric = new Generic(p_iClientId);
				m_objGeneric.Setproperty(m_lDbMake,m_objMergeCmd.Connection.ConnectionString);
			}
			m_userLogin = p_userLogin;
		}
		#endregion

		#region Merge Destructor
		/// <summary>
		/// Destructor
		/// </summary>
		~Merge()
		{
            Dispose();
		}
		#endregion

        public void Dispose()
        {
            m_arrlstColumnHeaders = null;
            m_arrlstFieldTypes = null;
            m_sXMLData = null;
            m_objCurTemplate = null;
            m_objManager = null;
            m_sConnectionString = null;
            m_sResultFileName = null;
            m_sAttachTitle = null;
            m_sSecureConnectString = null;
            if (m_objMergeConn != null)
            {
                m_objMergeConn.Close();
                m_objMergeConn.Dispose();
            }
            m_objMergeCmd = null;
            m_objGeneric = null;
            //tkatsarski: 03/13/15 - RMA-1768
            if (m_objMergeds != null)
            {
                m_objMergeds.Dispose();
                m_objMergeds = null;
            }
        }

		#region Load Template Data
		/// Name		: Load
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Loads an existing template with data
		/// </summary>
		/// <param name="p_lFormID">ID of template to be loaded</param>
		internal void Load(long p_lFormID,long iDocType)
		{
			try
			{
				IManager objtmpManager=null;
                m_objCurTemplate = new Template(m_userLogin, iDocType, m_sDocStorageDSN, m_iClientId);//rkaur27
				objtmpManager=this.Manager;
				m_objCurTemplate.Manager=objtmpManager;
				m_objCurTemplate.DSN=m_sConnectionString;
				m_objCurTemplate.SecureDSN=m_sSecureConnectString;
				m_objCurTemplate.Load(p_lFormID);
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Merge.Load.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			
			
		}
		#endregion

		#region Function to generate RTF file
		/// Name		: GenerateRTF
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will generate rtf file 
		/// </summary>
		/// <param name="p_sValues">Comma seperated string containing record id's for which
		/// rtf will be generated</param>
		/// <returns>Generated rtf file content</returns>
		public int GenerateRTF(string p_sValues,string sNewFilePath,string sRtf)
		{
			string sOrigRTF=""; 
			string sNewRTF=""; 
			string sTemp="";
			string sHeaderRTF=""; 
			string sBodyRTF=""; 
			string sNewBodyRTF=""; 
			Regex objReg=null;
			string sValue=""; 
			long lRecordCount=0; 
			string sFullResultFile="";
			TemplateField objField=null;
			ArrayList arrlstCleanRTFMatches=null;
			ArrayList arrlstFieldsFoundInDocReplaces=null;
			ArrayList arrlstStyledReplacementBlock=null;
			ArrayList arrlstFieldsFoundInDoc=null;
			string []arrItems=null;
			Match objMatch=null;
			Group objGroup=null;
			StreamWriter objWriter=null;
			string sVal="";
			int iSuccess=0;
			try
			{
				arrlstCleanRTFMatches=new ArrayList();
				arrlstFieldsFoundInDocReplaces=new ArrayList();
				arrlstStyledReplacementBlock=new ArrayList();
				arrlstFieldsFoundInDoc=new ArrayList();
				arrItems=p_sValues.Split(",".ToCharArray()[0]);
				if (arrItems.Length !=0)
				{
					arrItems=p_sValues.Split(",".ToCharArray()[0]);
					lRecordCount=arrItems.Length;
				}
				else
				{
					arrItems=new string[0];
					arrItems[0]=this.RowId.ToString();
				}
				
				//string sNewFileName = "";
								
			
				//no need to change file name
				//Template.FormFileName = sNewFileName;
				string strOriginalPath = Manager.TemplatePath;
				//sNewFilePath = sNewFilePath.Sub
				int iIndex = sNewFilePath.LastIndexOf(@"\"); 
				string strFilePath;
				string strFileName;
				strFilePath = sNewFilePath.Substring(0,iIndex+1);
				strFileName = sNewFilePath.Remove(0,iIndex+1); 
			//	Manager.TemplatePath = strFilePath;
				//Template.FormFileName = strFileName;
				
				sOrigRTF=sRtf.Replace(Constants.VBCr,string.Empty);
				sOrigRTF=sOrigRTF.Replace(Constants.VBLf,string.Empty);
				sOrigRTF=sOrigRTF.Replace(@"\{", "<bb_opener>");
				sOrigRTF=sOrigRTF.Replace(@"\}", "<bb_closer>");
				if (sOrigRTF.Equals(""))
				{
					throw new RMAppException("Merge.GenerateRTF.TemplateNotFound");
				}
				objReg=new Regex("",RegexOptions.Multiline);
				sHeaderRTF=sOrigRTF.Substring(0,sOrigRTF.IndexOf(@"\sect")+1);
				sBodyRTF=sOrigRTF.Substring(sOrigRTF.Length-(sHeaderRTF.Length+5));
				sBodyRTF=sBodyRTF.Substring(0,sBodyRTF.Length-1);
				sNewBodyRTF=sBodyRTF;
				sOrigRTF=RemovePictureBlocks(sOrigRTF);
				arrlstFieldsFoundInDocReplaces=GetRTFFields(sOrigRTF);
				for (int i=0;i<arrlstFieldsFoundInDocReplaces.Count;i++)
				{
					arrlstCleanRTFMatches.Add(CleanMatch(arrlstFieldsFoundInDocReplaces[i].ToString()));
				}
				for (int i=0;i<arrlstFieldsFoundInDocReplaces.Count;i++)
				{
					arrlstStyledReplacementBlock.Add(MatchStyleBlock(arrlstFieldsFoundInDocReplaces[i].ToString()));
				}
                objReg = new Regex(@"MERGEFIELD\s*([A-Za-z0-9_]*)", RegexOptions.Multiline);
				for (int i=0;i<arrlstCleanRTFMatches.Count;i++)
				{
					objMatch=objReg.Matches(arrlstCleanRTFMatches[i].ToString())[0];
					objGroup=objMatch.Groups[1];
					sTemp=objGroup.Value;
					arrlstFieldsFoundInDoc.Add(sTemp);
                    if (sTemp.Length == 0)
                    {
                        Log.Write(String.Format(Globalization.GetString("Merge.UnexpectedFieldRTF.Warn", m_iClientId), arrlstCleanRTFMatches[i].ToString()), "CommonWebServiceLog", m_iClientId);//dvatsa-cloud
                    }
				}
				for (int i=0;i<arrItems.Length;i++)
				{
					this.RowId = Conversion.ConvertStrToLong(arrItems[i])-1;
					sNewBodyRTF=sBodyRTF;
					for (int j=0;j<arrlstFieldsFoundInDoc.Count;j++)
					{
						sValue=Conversion.ConvertObjToStr(arrlstFieldsFoundInDoc[j]).ToUpper();
						objField=(TemplateField)Template.Fields[sValue];
						if (objField==null)
						{
							//Mjain8 Added MITS 7517 for more detailed description
                            string sMError = Globalization.GetString("Merge.GenerateRTF.FieldNotFound", m_iClientId).Replace("%%1", sValue);//dvatsa-cloud
							throw new RMAppException(sMError);
						}
						sVal=arrlstStyledReplacementBlock[j].ToString().Replace(@"%VALUE%",@objField.Value);
						sNewBodyRTF=sNewBodyRTF.Replace(arrlstFieldsFoundInDocReplaces[j].ToString(),@sVal);
					}
					
					if (i==arrItems.Length-1)
					{
						sNewRTF=sNewRTF+sNewBodyRTF;
					}
					else
					{
						sNewRTF=sNewRTF+sNewBodyRTF+"\\page";
					}
				}
				sValue="";
				sValue=sHeaderRTF+sNewRTF+"}";
				sValue=sValue.Replace("<bb_opener>", "\\{");
				sValue=sValue.Replace("<bb_closer>", "\\}");
				sValue=RemoveNamedBlocks(sValue,@"\*\mailmerge");

				m_sResultFileName=Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".rtf");
                string sPath = RMConfigurator.UserDataPath;

				if ( !Directory.Exists(sPath+"\\MailMerge"))
					Directory.CreateDirectory( sPath+"\\MailMerge");
				string strWizardPath = sPath+"\\MailMerge"; 
				sFullResultFile= strWizardPath + "\\" + this.ResultFile;
				
				
				
				objWriter=new StreamWriter(sFullResultFile,false);
				objWriter.AutoFlush=true;	
				objWriter.Write(sValue);
				objWriter.Close();
				iSuccess=1;
				
			//	Manager.TemplatePath = strOriginalPath;
				
//				if (File.Exists(Manager.TemplatePath+strFileName))
//				{
//					File.Delete(Manager.TemplatePath+strFileName);
//				}
			

			}
			catch (FileNotFoundException p_objException)
			{
				throw new RMAppException("Merge.GenerateRTF.FileNotFoundError",p_objException);
			}
			catch (IOException p_objException)
			{
				throw new RMAppException("Merge.GenerateRTF.FileInputOutputError",p_objException);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Merge.GenerateRTF.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			finally
			{
				objReg=null;
				sOrigRTF=null; 
				sNewRTF=null; 
				sTemp=null;
				sHeaderRTF=null; 
				sBodyRTF=null; 
				sNewBodyRTF=null; 
				objReg=null;
				sValue=null; 
				sFullResultFile=null;
				objField=null;
				arrlstCleanRTFMatches=null;
				arrlstFieldsFoundInDocReplaces=null;
				arrlstStyledReplacementBlock=null;
				arrlstFieldsFoundInDoc=null;
				arrItems=null;
				objMatch=null;
				objGroup=null;
                if (objWriter != null)
                {
                    objWriter.Dispose();
                }
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
				sValue=null;
			}
			return iSuccess;
		}
		#endregion

        //tkatsarski: 03/13/15 Start changes for RMA-1768: The method generates random two-symbol string, which is used for creating unique temporary tables names
        //If two or more users try to run mail merge, temporary DB tables will be created, and if the tables' names are the same, an exception will be thrown.
        //The generated string is 2-symbols long because for Oracle there are table length-name restrictions.
        //Besides the generated random string, an UID is also used.
        private static string GetTablesNameUniquePart()
        {
            lock (m_objSyncLock)
            {
                return m_objGetRandom.Next(0, 256).ToString("X2");
            }
        }
        //tkatsarski: 03/13/15 End changes for RMA-1768

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The merge data source as a Base64 encoded string.</returns>
        public string GetMergeDataSource(string sRowIDs)
        {
            //get merge field names from .hdr file
            MemoryStream memoryStream = null;
            Dictionary<string, TemplateField> mergeValues = new Dictionary<string, TemplateField>();
            try
            {
                if (!Template.GetHeaderFile(out memoryStream))
                    throw new RMAppException("Merge.GetMergeDataSource.HeaderFileNotFound: " + Template.FormFileName);
                memoryStream.Position = 0;
                using (StreamReader rdr = new StreamReader(memoryStream))
                {
                    while (rdr.Peek() >= 0)
                    {
                        string line = rdr.ReadToEnd();
                        line = line.Replace("\"", string.Empty).Replace(Environment.NewLine,"\t");
                        string[] arr = line.Split(new char[1] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in arr)
                        {
                            //PSARIN2 7/19: MITS 28807  Missouri Highways - Mail Merge Issue 
                            //if (s.Trim().Length > 0 && !mergeValues.ContainsKey(s.Trim()))
                            //   mergeValues.Add(s.Trim(), null);
                            string smergekeyvalue = s.Trim().ToUpper();
                            if (s.Trim().Length > 0 && !mergeValues.ContainsKey(smergekeyvalue))
                                mergeValues.Add(smergekeyvalue, null);

                        }
                    }
                }
            }
            finally
            {
                if (memoryStream != null)
                    memoryStream.Dispose();
            }

            //get the matching values from Template.Fields collection
            List<string> keys = new List<string>(); //cannot change a dictionary value while iterating
            foreach (string key in mergeValues.Keys)
            {
                keys.Add(key);
            }
            foreach (string key in keys)
            {
                //kchoudhary2, MITS 9161
                TemplateField templateField = (TemplateField)Template.Fields[key];
                if (templateField == null)
                {
                    //kchoudhary2, MITS 9161
                    IDictionaryEnumerator objEnum = null;
                    TemplateField objFld = null;
                    objEnum = this.Template.Fields.GetEnumerator();
                    while (objEnum.MoveNext())
                    {
                        objFld = (TemplateField)objEnum.Value;
                        // if (objFld.WordFieldName.Length >= 39 && sValue == objFld.WordFieldName.Substring(0, 39))
                        // {
                        // objField = objFld;
                        //break; //the loop wasnt breaking added while doing 9157
                        //}
                        //9157 We need to apply the same logic with which we changed field names initially
                        ///if (objField == null)
                        //{
                        //tkr MITS 10647, RmWorld merge templates uses Field description
                        if (objFld.FieldDesc.ToUpper().Equals(key.ToUpper()))
                        {
                            templateField = objFld;
                            break;
                        }
                        else if (ReverseWordSafeLogic(templateField, key, objFld))
                        {
                            templateField = objFld;
                            break;
                        }
                        else if (objFld.FieldDesc.Replace("(Desc)","(Desc.)").ToUpper().Equals(key.ToUpper()))
                        {
                            //tkr MITS 10647 10/2007: ugly, ugly, ugly hack.
                            //see TemplateField.cs line 548, the "FixCodeFields" function.  RmWorld uses "(Desc.)" [see the period] instead of "(Desc)" [no period]
                            //again, RmWorld templates uses field descriptions in .hdr files, so any differences in logic in how RmWorld vs. RmNet create field descriptions will create problems  
                            //I'll bet dollars to donuts this is not the only difference!
                            templateField = objFld;
                            break;
                        }
                    }
                }
                if (templateField == null)
                {
                    //Mjain8 Added MITS 7517 for more detailed description
                    throw new RMAppException(Globalization.GetString("Merge.GenerateRTF.FieldNotFound", m_iClientId).Replace("%%1", key).Replace("%%2", Template.FormFileName));//dvatsa-cloud
                }
                mergeValues[key] = templateField;
            }

            // write the data source file (copy code from RMWorld)
            // Write a second blank record in (fixes a flaw in WFW6.0)
            // this avoids having the header record delimiter confirmation dialog appear to the user
            // only the 1st record is merged
            string dataSource1 = string.Empty;   //first line, the field values, tab-delimited and inside double-quotes
            string dataSource2 = string.Empty;  //second line, the blank field values, ditto 
            string tab = "\t"; //Convert.ToChar(9).ToString();
            string doubleQuotes = Convert.ToChar(34).ToString();
            string[] arrRowIDs = sRowIDs.Split(',');
            for (int iRow = 0; iRow < arrRowIDs.Length; iRow++)
            {
                //It should be zero-based, but the row index from UI is one-based
                RowId = int.Parse(arrRowIDs[iRow]) - 1;
                if ( iRow > 0 )
                {
                    dataSource1 += Environment.NewLine;
                }
                int iKey = 0;
                foreach (string key in mergeValues.Keys)
                {
                    if (iKey > 0)
                    {
                        dataSource1 += tab;
                    }
                    dataSource1 += doubleQuotes + mergeValues[key].Value + doubleQuotes;
                    //Just need one row for the blank field values
                    if (iRow == 0)
                    {
                        dataSource2 += (dataSource2.Length > 0 ? tab : string.Empty) + doubleQuotes + doubleQuotes;
                    }
                    iKey++;
                }
                //tkr if field count == 1 must append a tab to suppress MS word prompt asking "what is record delimiter?"
                if (mergeValues.Keys.Count == 1)
                {
                    dataSource1 += tab;
                    if (iRow == 0)
                    {
                        dataSource2 += tab;
                    }
                }
            }

            //this is just text, but must convert or lose tab delimiters
            return Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(dataSource1 + Environment.NewLine + dataSource2));
        }

		public int GenerateRTF(string p_sValues)
		{
			string sOrigRTF=""; 
			string sNewRTF=""; 
			string sTemp="";
			string sHeaderRTF=""; 
			string sBodyRTF=""; 
			string sNewBodyRTF=""; 
			Regex objReg=null;
			string sValue=""; 
			long lRecordCount=0; 
			string sFullResultFile="";
			TemplateField objField=null;
			ArrayList arrlstCleanRTFMatches=null;
			ArrayList arrlstFieldsFoundInDocReplaces=null;
			ArrayList arrlstStyledReplacementBlock=null;
			ArrayList arrlstFieldsFoundInDoc=null;
			string []arrItems=null;
			Match objMatch=null;
			Group objGroup=null;
			StreamWriter objWriter=null;
			string sVal="";
			SysSettings objSettings = null;
			int iSuccess=0;
			try
			{
				arrlstCleanRTFMatches=new ArrayList();
				arrlstFieldsFoundInDocReplaces=new ArrayList();
				arrlstStyledReplacementBlock=new ArrayList();
				arrlstFieldsFoundInDoc=new ArrayList();
				arrItems=p_sValues.Split(",".ToCharArray()[0]);
				if (arrItems.Length !=0)
				{
					arrItems=p_sValues.Split(",".ToCharArray()[0]);
					lRecordCount=arrItems.Length;
				}
				else
				{
					arrItems=new string[0];
					arrItems[0]=this.RowId.ToString();
				}
				
				sOrigRTF=Template.FormFileContent;

				if(!PreFab)
				{
                    sOrigRTF = sOrigRTF.Replace(Constants.VBCr, string.Empty);
                    sOrigRTF = sOrigRTF.Replace(Constants.VBLf, string.Empty);
					sOrigRTF=sOrigRTF.Replace(@"\{", "<bb_opener>");
					sOrigRTF=sOrigRTF.Replace(@"\}", "<bb_closer>");
					if (sOrigRTF.Equals(""))
					{
						throw new RMAppException("Merge.GenerateRTF.TemplateNotFound");
					}
					objReg=new Regex("",RegexOptions.Multiline);
					sHeaderRTF=sOrigRTF.Substring(0,sOrigRTF.IndexOf(@"\sect")+1);
					sBodyRTF=sOrigRTF.Substring(sOrigRTF.Length -(sHeaderRTF.Length+5));
					sBodyRTF=sBodyRTF.Substring(0,sBodyRTF.Length-1);
					sNewBodyRTF=sBodyRTF;
					sOrigRTF=RemovePictureBlocks(sOrigRTF);
					arrlstFieldsFoundInDocReplaces=GetRTFFields(sOrigRTF);
					for (int i=0;i<arrlstFieldsFoundInDocReplaces.Count;i++)
					{
						arrlstCleanRTFMatches.Add(CleanMatch(arrlstFieldsFoundInDocReplaces[i].ToString()));
					}
					for (int i=0;i<arrlstFieldsFoundInDocReplaces.Count;i++)
					{
						arrlstStyledReplacementBlock.Add(MatchStyleBlock(arrlstFieldsFoundInDocReplaces[i].ToString()));
					}
					objReg=new Regex("MERGEFIELD ([A-Za-z0-9_]*)",RegexOptions.Multiline);
					for (int i=0;i<arrlstCleanRTFMatches.Count;i++)
					{
						objMatch=objReg.Matches(arrlstCleanRTFMatches[i].ToString())[0];
						objGroup=objMatch.Groups[1];
						sTemp=objGroup.Value;
						arrlstFieldsFoundInDoc.Add(sTemp);
					}
					if ( p_sValues != "" )
					{
						for (int i=0;i<arrItems.Length;i++)
						{
							this.RowId = Conversion.ConvertStrToLong(arrItems[i])-1;
							sNewBodyRTF=sBodyRTF;
							for (int j=0;j<arrlstFieldsFoundInDoc.Count;j++)
							{
								sValue=Conversion.ConvertObjToStr(arrlstFieldsFoundInDoc[j]).ToUpper();
								objField=(TemplateField)Template.Fields[sValue];
								if (objField==null)
								{
                                    //kchoudhary2, MITS 9161
                                    IDictionaryEnumerator objEnum = null;
                                    TemplateField objFld=null;
                                    objEnum = this.Template.Fields.GetEnumerator();
                                    while (objEnum.MoveNext())
                                    {
										objFld = (TemplateField)objEnum.Value;
										// if (objFld.WordFieldName.Length >= 39 && sValue == objFld.WordFieldName.Substring(0, 39))
										// {
										// objField = objFld;
										//break; //the loop wasnt breaking added while doing 9157
										//}
										//9157 We need to apply the same logic with which we changed field names initially
										///if (objField == null)
										//{
										if(ReverseWordSafeLogic(objField,sValue,objFld))
										{	
											objField = objFld;
											break;
										}
											
										//}
									}

                                    //ends
                                    if (objField == null)
                                    {
                                        //Mjain8 Added MITS 7517 for more detailed description
                                        string sMError = Globalization.GetString("Merge.GenerateRTF.FieldNotFound", m_iClientId).Replace("%%1", sValue);//dvatsa-cloud
                                        throw new RMAppException(sMError);
                                    }
								}
								sVal=arrlstStyledReplacementBlock[j].ToString().Replace(@"%VALUE%",@objField.Value);
								sNewBodyRTF=sNewBodyRTF.Replace(arrlstFieldsFoundInDocReplaces[j].ToString(),@sVal);
							}
					
							if (i==arrItems.Length-1)
							{
								sNewRTF=sNewRTF+sNewBodyRTF;
							}
							else
							{
								sNewRTF=sNewRTF+sNewBodyRTF+"\\page";
							}
						}
					}
					sValue="";
					sValue=sHeaderRTF+sNewRTF+"}";
					sValue=sValue.Replace("<bb_opener>", "\\{");
					sValue=sValue.Replace("<bb_closer>", "\\}");
					sValue=RemoveNamedBlocks(sValue,@"\*\mailmerge");

				}
				m_sResultFileName=Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".rtf");
				
				//08/10/2006.. Raman Bhatia : If Acrosoft is enabled then legacy document management is not used
				//Instead store attachment using Acrosoft
				
				objSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud
                
				//07/21/2011 Raman Bhatia : Media View Interface for R8
                if(objSettings.UseAcrosoftInterface==false && objSettings.UseMediaViewInterface == false)
				{
					if ( m_iDBStorageType == 0 )
						sFullResultFile=Manager.TemplatePath+this.ResultFile;
					else
						sFullResultFile = Path.GetTempPath()+this.ResultFile;
					//need to check code
				}
				else
				{
					sFullResultFile = Path.GetTempPath()+this.ResultFile;
				} // else

				if (File.Exists(sFullResultFile))
				{
					File.Delete(sFullResultFile);
				}
				objWriter=new StreamWriter(sFullResultFile,false);
				objWriter.AutoFlush=true;	
				if(!PreFab)
					objWriter.Write(sValue);
				else
					objWriter.Write(sOrigRTF);
				objWriter.Close();

				iSuccess=1;
			}
			catch (FileNotFoundException p_objException)
			{
				throw new RMAppException("Merge.GenerateRTF.FileNotFoundError",p_objException);
			}
			catch (IOException p_objException)
			{
				throw new RMAppException("Merge.GenerateRTF.FileInputOutputError",p_objException);
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Merge.GenerateRTF.Error",m_iClientId),p_objException);//dvatsa-cloud
			}
			finally
			{
				objReg=null;
				objWriter=null;
				sOrigRTF=null; 
				sNewRTF=null; 
				sTemp=null;
				sHeaderRTF=null; 
				sBodyRTF=null; 
				sNewBodyRTF=null; 
				objReg=null;
				sValue=null; 
				sFullResultFile=null;
				objField=null;
				arrlstCleanRTFMatches=null;
				arrlstFieldsFoundInDocReplaces=null;
				arrlstStyledReplacementBlock=null;
				arrlstFieldsFoundInDoc=null;
				arrItems=null;
				objMatch=null;
				objGroup=null;
                if (objWriter != null)
                {
                    objWriter.Dispose();
                }
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
				sValue=null;
			}
			return iSuccess;


		}
		
		private bool ReverseWordSafeLogic(TemplateField  objField , string sValue,TemplateField objFld)
		{
			string exist = sValue;
			string converted = Generic.WordSafeFieldName(objFld.FieldDesc, m_iClientId);
			if (sValue.IndexOf("M_")==0)
			{
				exist =sValue.Substring(2);
			}											
			if (converted.IndexOf("M_") ==0)
			{
				converted = converted.Substring(2);
			}
			if (exist.Equals(converted))
			{				
				return true;
			}
			return false;
		}

















		#region Common Functions to be used while generation RTF file
		/// Name		: GetCurrentlyOpenCount
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will return the total number of { brackets in a rtf string
		/// </summary>
		/// <param name="p_sBlock">Rtf string</param>
		/// <param name="p_iPos">Length of rtf string to search</param>
		/// <returns>Total number of { brackets</returns>
		private long GetCurrentlyOpenCount(string p_sBlock,int p_iPos)
		{
			Regex objReg=null;
			long lOpen=0;
			long lClose=0;
			long lTotal=0;
			try
			{
				objReg=new Regex(@"{");
				lOpen=objReg.Matches(@p_sBlock.Substring(0,p_iPos)).Count;
				objReg=new Regex(@"}");
				lClose=objReg.Matches(@p_sBlock.Substring(0,p_iPos)).Count;
				lTotal=lOpen-lClose;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Merge.GetCurrentlyOpenCount.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			finally
			{
				objReg=null;
			}
			return lTotal;
		}
		/// Name		: MatchStyleBlock
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will return the matched string 
		/// </summary>
		/// <param name="p_sMatch">String to match</param>
		/// <returns>Matched string</returns>
		private string MatchStyleBlock(string p_sMatch)
		{
			Regex objReg=null; 
			string sStyleBlock=""; 
			string sValueMarker=""; 
			string sFieldName=""; 
			string sTmp=""; 
			string sMergeFieldToken=""; 
			int iValue=0; 
			int iTemp1=0;
			int iTemp2=0;
			string sReturnValue="";
			MatchCollection objSubMatches=null;
			sMergeFieldToken = "MERGEFIELD";
			string sFind="";
			string sReplace="";
			GroupCollection objGrpCollection=null;
			CaptureCollection objRTFSubBlockCaptures=null;
		
			try
			{
				//objReg=new Regex(@"\\\*\\fldinst ({[^}]*})");
				objReg=new Regex(@"\\\*\\fldinst ({[^}]*}\s*)*");
				//sStyleBlock = objReg.Matches(p_sMatch)[0].Groups[1].Value;
				objRTFSubBlockCaptures = objReg.Matches(p_sMatch)[0].Groups[1].Captures;
			
				//Guess the first one but do a best effort double check for case of "intervening blocks"
				sStyleBlock = objRTFSubBlockCaptures[0].Value;
				foreach(Capture cap in objRTFSubBlockCaptures)
					if (cap.Value.IndexOf("MERGEFIELD") !=-1)
						sStyleBlock = cap.Value;

			
				//Carry on with "Best Effort" RTF parsing.
				if (sStyleBlock.IndexOf("MERGEFIELD") ==-1)
				{
					iTemp1=sStyleBlock.IndexOf(" M")+1;
					iTemp2=sStyleBlock.IndexOf("D ");
					if (iTemp2==-1)
						iTemp2=sStyleBlock.IndexOf(@"D\");
				
					if (iTemp2!=-1)
					{
						sMergeFieldToken = sStyleBlock.Substring(iTemp1,(iTemp2-iTemp1)+1);
                        Log.Write(String.Format(Globalization.GetString("Merge.MergeFieldStyleLost.Warn", m_iClientId), p_sMatch), "CommonWebServiceLog", m_iClientId);//dvatsa-cloud
					}
				}
				objReg=new Regex(sMergeFieldToken.Replace(@"\",@"\\")+@"([ ]*\\[^ ]*)* ([^}]*)");
				objGrpCollection=objReg.Matches(@sStyleBlock)[0].Groups;
				sValueMarker = objGrpCollection[2].Value;
				sTmp = sValueMarker;
				objReg=new Regex(@"(\\[^ ]*)* ");
				sFieldName=objReg.Replace(@sValueMarker,"");
				objSubMatches=objReg.Matches(@sValueMarker);
				if (objSubMatches!=null)
				{
					iValue=0;
					while((sTmp.IndexOf(sFieldName)==-1) && (iValue<objSubMatches.Count-1))
					{
						sTmp=sTmp.Replace(objSubMatches[iValue].Value,"");
						iValue++;
					}
					if (iValue<objSubMatches.Count)
					{
						if (objSubMatches[iValue].Index > 1 )
						{
							sValueMarker=sValueMarker.Substring(0,objSubMatches[iValue].Index);
						}
					}
				}
				sStyleBlock = sStyleBlock.Replace(sMergeFieldToken, "");
				sFind=sValueMarker.Replace(" ", "");
				sReplace=" %VALUE%";
				sStyleBlock=sStyleBlock.Replace(sMergeFieldToken, "");
				sStyleBlock=sStyleBlock.Replace(" ", "");
				sReturnValue=sStyleBlock.Replace(sFind, sReplace);
				return sReturnValue;
			}
			catch (Exception p_objException)
			{
                Log.Write(String.Format(Globalization.GetString("Merge.MergeFieldStyleLost.Warn", m_iClientId), p_sMatch), "CommonWebServiceLog", m_iClientId);//dvatsa-cloud
			}
			finally
			{
				objReg=null; 
				sStyleBlock=null; 
				sValueMarker=null; 
				sFieldName=null; 
				sTmp=null; 
				sMergeFieldToken=null; 
				objSubMatches=null;
				objGrpCollection=null;
				sFind=null;
				sReplace=null;
			}
			return "%VALUE%";
		
		}
		/// Name		: RemovePictureBlocks
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function removes picture blocks from rtf string
		/// </summary>
		/// <param name="p_sOrigRTF">Rtf string</param>
		/// <returns>Rtf string without picture blocks</returns>
		private string RemovePictureBlocks(string p_sOrigRTF)
		{
			return RemoveNamedBlocks(p_sOrigRTF,@"\pict");
		}

		/// Name		: RemoveNamedBlocks
		/// Author		: Brian Battah
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function removes named blocks from rtf string
		/// Basically a general purpose version of RemovePicBlocks to remove 
		/// any named block
		///  Example: RemoveNamedBlocks(@"{mydoc{almost{\*\mailmerge}ends} here}",@"\*\mailmerge");
		///  would return "{mydoc{almostends} here}"
		/// </summary>
		/// <param name="p_sOrigRTF">Rtf string</param>
		/// <returns>Rtf string without any occurrence of the named rtf block</returns>
		private string RemoveNamedBlocks(string p_sOrigRTF, string sBlockName)
		{
			Regex objRegOpen=null;
			Regex objRegClose=null;
			int iOpenPos=0;
			int iPicOpenPos=0;
			int iPicClosePos=0;
			string sResult="";
			MatchCollection objOpens=null;
			MatchCollection objCloses=null;
			long lDepth=0;
			int iOpenIdx=0;
			int iCloseIdx=0;
			iOpenIdx = 0;
			iCloseIdx = 0;
			try
			{
				objRegOpen=new Regex(@"{");
				objOpens=objRegOpen.Matches(@p_sOrigRTF);
				objRegClose=new Regex(@"}");
				objCloses=objRegClose.Matches(@p_sOrigRTF);
				sResult=p_sOrigRTF;
				while(iOpenIdx<objOpens.Count)
				{
					iOpenPos = objOpens[iOpenIdx].Index + 1;
					if ( p_sOrigRTF.Length >= iOpenPos-1 + sBlockName.Length+1)
					{
						if (p_sOrigRTF.Substring(iOpenPos-1,sBlockName.Length+1).Equals("{" + sBlockName))
						{
							lDepth=GetCurrentlyOpenCount(p_sOrigRTF, iOpenPos - 1);
							iPicOpenPos=iOpenPos;
				
							while((iCloseIdx< objCloses.Count) && 
								((objCloses[iCloseIdx].Index + 1) < iOpenPos))
							{
								iCloseIdx = iCloseIdx + 1;
							}
							while((iCloseIdx< objCloses.Count) && 
								(lDepth!=GetCurrentlyOpenCount(p_sOrigRTF,objCloses[iCloseIdx].Index+1)))
							{
								iCloseIdx = iCloseIdx + 1;
							}
							iPicClosePos = objCloses[iCloseIdx].Index + 1;
							sResult=sResult.Replace(p_sOrigRTF.Substring(iPicOpenPos-1,iPicClosePos-(iPicOpenPos-1)),"");
						}	
					}
					iOpenIdx++;	
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Merge.RemoveNamedBlocks.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			finally
			{
				objRegOpen=null;
				objRegClose=null;
				objOpens=null;
				objCloses=null;
			}
			return sResult;
		}
		/// Name		: GetRTFFields
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns fields in rtf string
		/// </summary>
		/// <param name="p_sRTF">Rtf string</param>
		/// <returns>ArrayList containing the fields in rtf string</returns>
		private ArrayList GetRTFFields(string p_sRTF)
		{
			string sChunk="";
			ArrayList arrlstTemp=null;
			try
			{
				arrlstTemp=new ArrayList();
				sChunk=p_sRTF;
				ParseBlock(sChunk,arrlstTemp);
			}	
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Merge.GetRTFFields.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			finally
			{
				sChunk=null;
			}
			return arrlstTemp;
		}
		/// Name		: CleanMatch
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// The function looks for a match in a passed string
		/// </summary>
		/// <param name="p_sMatch">String to look for a match</param>
		/// <returns>Matched string</returns>
		private string CleanMatch(string p_sMatch)
		{
			Regex objReg=null;
			MatchCollection objMatches=null;
			try
			{
				objReg=new Regex(@"(\\[a-zA-Z0-9]+[ ]?)");
				objMatches=objReg.Matches(@p_sMatch);
				foreach (Match objMatch in objMatches)
				{
					if (objMatch.Value.Trim()!=@"\field" 
						&& objMatch.Value.Trim()!=@"\fldinst" 
						&& objMatch.Value.Trim()!=@"\fldrslt")
					{
						objReg=new Regex(objMatch.Value.Replace(@"\", @"\\"));
						p_sMatch=objReg.Replace(p_sMatch,"",1);
					}
				}
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Merge.CleanMatch.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			finally
			{
				objReg=null;
				objMatches=null;
			}
			return p_sMatch;
		}
		/// Name		: ParseBlock
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function parses a string for a match
		/// </summary>
		/// <param name="p_sChunk">String to parse</param>
		/// <param name="p_arrlstList">ArrayList containg the fields to look for in string</param>
		/// <returns>Parsed string</returns>
		private string ParseBlock(string p_sChunk, ArrayList p_arrlstList)
		{
			long lOpeners=0; 
			long lClosers=0; 
			long lDepth=0; 
			char c;
			char[] arrChunk;
			StringBuilder sTmp= new StringBuilder(p_sChunk.Length);
			StringBuilder sSubBlock=new StringBuilder(p_sChunk.Length);
			string s1="";
			Regex objReg=null;
			try
			{
				objReg=new Regex("{");
				lOpeners=objReg.Matches(p_sChunk).Count;
				objReg=new Regex("}");
				lClosers=objReg.Matches(p_sChunk).Count;
				
				if (lOpeners!=lClosers)
                    throw new RMAppException(Globalization.GetString("Merge.UnBalancedBlock.Error", m_iClientId));//dvatsa-cloud
				
				if (lOpeners==0)
					return "";

				p_sChunk=p_sChunk.Substring(p_sChunk.IndexOf("{"));
				p_sChunk=p_sChunk.Substring(0,p_sChunk.LastIndexOf("}")+1);
				
				arrChunk = p_sChunk.ToCharArray();
				
				lDepth = -1;
			
				for(int i=0;i <arrChunk.Length;i++)
				{
					c=arrChunk[i];
					sTmp.Append(c);
					if (lDepth >=1)
					{
						sSubBlock.Append(c);
					}
					if (c=='{')
					{
						lDepth = lDepth + 1;
						if (lDepth==1)
						{
						
							sSubBlock = new StringBuilder("{",p_sChunk.Length);
						}
					}
					if (c=='}')
					{
						if (lDepth==1)
						{
							ParseBlock(sSubBlock.ToString(),p_arrlstList);
							sSubBlock=new StringBuilder(p_sChunk.Length);
						}
						if (lDepth==0)
						{
							s1 = sTmp.ToString();
							if(s1.StartsWith(@"{\field"))
								if (CleanMatch(s1).IndexOf("MERGEFIELD") >= 0 )
									p_arrlstList.Add(s1);
						}
						lDepth--;
					}

				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Merge.ParseBlock.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			finally
			{
				objReg=null;
				sTmp=null; 
				sSubBlock=null;
			}
			return p_sChunk;
		}

		#endregion

		#region Function to generate XML of records that can be used for merging
		/// Name		: GenerateXML
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Generates XML string for a merged record
		/// Format of Xml is :
		/// <?xml-stylesheet type='text/xsl' href='records.xsl'?>
		/// <mergedata recordid='1' catid='2' docid='1157' formname='test24Nov1' docresultflag='False' >
		///       <header> 
		///       <column>Comp. Abbrev.</column> 
		///       <column>Comp. City</column>
		///       </header>
		/// </mergedata>
		/// </summary>
		/// <returns>XML string containing the records for merging</returns>
        public string GenerateXML()
        {
            TemplateField objField = null;
            StringBuilder sValue = new StringBuilder();
            long lValue = 0;
            long lTemp1 = 1;
            IDictionaryEnumerator objEnum = null;
            try
            {
                objEnum = Template.Fields.GetEnumerator();
                //Commented by jasbinder on April 30, 2005 because no processing is allowed within a SOAP action
                //	sValue = "<?xml-stylesheet type='text/xsl' href='records.xsl'?>"					
                //+Microsoft.VisualBasic.Constants.vbCrLf
                //+"<mergedata recordid='"+this.RecordId.ToString()+"' catid='"
                //Mjain8 8/25/06 MITS 7517 handled '&'
                sValue = sValue.Append("<mergedata recordid='" + this.RecordId.ToString() + "' catid='"
                    + this.Template.CatId.ToString() + "' docid='"
                    + this.Template.FormId.ToString() + "' formname='"
                    + this.Template.FormName.Replace("&", "&amp;") + "' docresultflag='"
                    + this.DocResultFlag.ToString() + "' >");
                if (AttachFlag)
                {
                    sValue = sValue.Append("<attachinfo><title>" + AttachTitle + "</title><notes>"
                        + AttachNotes + "</notes><keywords>" + AttachKeyWords + "</keywords></attachinfo>");
                }
                if (DocResultFlag)
                {
                    sValue = sValue.Append("<docresultflag />");
                }
                sValue = sValue.Append("<header>");
                while (objEnum.MoveNext())
                {
                    objField = (TemplateField)objEnum.Value;
                    sValue = sValue.Append(" <column>" + objField.FieldDesc.Replace("&", "&amp;")
                        + "</column>");

                }
                sValue = sValue.Append("</header>" + Constants.VBCrLf);
                //Create merge record xml
                //commented by jasbinder
                //for(int i=0;i<=m_objMergeds.Tables[0].Rows.Count;i++)
                for (int i = 0; i < m_objMergeds.Tables[0].Rows.Count; i++)
                {
                    sValue = sValue.Append("<record selected ='false' rowid='" + lTemp1.ToString() + "'>");
                    objEnum.Reset();
                    while (objEnum.MoveNext())
                    {
                        objField = (TemplateField)objEnum.Value;
                        lValue = objField.SeqNum - 1;
                        objField.Value = Conversion.ConvertObjToStr(m_objMergeds.Tables[0].Rows[i]["FLD" + (lValue.ToString())]);
                        sValue = sValue.Append("<field name='" + objField.WordFieldName + "'>"
                            + objField.Value.Replace("&", "&amp;") + "</field>"
                            + Constants.VBCrLf);
                    }
                    sValue = sValue.Append(" </record>" + Constants.VBCrLf);
                    lTemp1++;

                }
                sValue = sValue.Append("</mergedata>");

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Merge.GenerateXML.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //    m_objMergeds = null;
                //}
                objField = null;
                objEnum = null;
            }
            return sValue.ToString();
        }
		#endregion

       /// <summary>
       /// Return the Number of rows returned from Fetch Data
       /// </summary>
       /// <returns></returns>
        public int GetRecordCount()
        {            
            try
            {                
                return m_objMergeds.Tables[0].Rows.Count;                
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Merge.GetRecordCount.Error", m_iClientId), p_objException);//dvatsa-cloud
            }                  
        }
		#region Function to get supplemetal table key field name
		/// Name		: GetSuppTableKeyColName
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function gets supplemental table key column name.
		/// </summary>
		/// <param name="p_sSuppTableName">Supplemental table name</param>
		/// <returns>Supplemental table key column name</returns>
		private string GetSuppTableKeyColName(string p_sSuppTableName) 
		{
            //Shruti for MITS 9714, making query compatible for oracle
			return Generic.GetSingleString("SYS_FIELD_NAME", "SUPP_DICTIONARY", "SUPP_TABLE_NAME='" + p_sSuppTableName.ToUpper() + "' AND FIELD_TYPE=7",m_sConnectionString, m_iClientId);
		}
		#endregion

		#region Function to fetch merge Data
		/// Name		: FetchData
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function fetches the data for merging
		/// </summary>
		public bool FetchData()
		{
			long lCatId=0; 
			ArrayList arrlstDisplayTables=new ArrayList();
			ArrayList arrlstDisplayFields=new ArrayList();
			TemplateField objField=null;
			string sFieldName=""; 
            string sFieldTable = "";        //Added by csingh7 for R6 Claim Comment Enhancement
			int iIndex=0; 
			long lFormID=0; 
			string sJustFieldNames=""; 
			string sFrom=""; 
			string sWhere=""; 
			string sJustFieldNamesByNumber=""; 
			long lRecordID=0; 
			int iDispNum=0; 
			bool bMergeAll=false;
			string sATTable=""; 
			string sATKeyField=""; 
			string sFieldValue="";
			bMergeAll=true;
			long lTemp=0;
			MailMergeQuery objMailMergeQuery=null;
			IDictionaryEnumerator objDictionaryEnumerator=null;
            StringBuilder sbMEField = new StringBuilder(); //MITS 26255 hlv 8/1/12 added

			bool blnSuccess=false;
			try
			{
				lFormID = Template.FormId;
				lCatId = Template.CatId;
				lRecordID = RecordId;
				if (Template.Fields.Count >255)
				{
					throw new RMAppException("Merge.FetchData.TooManyFields");
				}
				//The following code assign temporary tables and fields based on category
				if (Template.CatId == CLAIM_MERGE) 
				{ 
					sJustFieldNames = "CLAIM.CLAIM_ID"; 
					sJustFieldNamesByNumber = "[CLAIM_TEMP].CLAIM_ID"; 
				} 
				else if (Template.CatId == EVENT_MERGE) 
				{ 
					sJustFieldNames = "EVENT.EVENT_ID"; 
					sJustFieldNamesByNumber = "[EVENT_TEMP].EVENT_ID"; 
				} 
				else if (Template.CatId == EMPLOYEE_MERGE) 
				{ 
					sJustFieldNames = "EMPLOYEE.EMPLOYEE_EID"; 
					sJustFieldNamesByNumber = "[EMP_TEMP].EMPLOYEE_EID"; 
				} 
				else if (Template.CatId == ENTITY_MERGE) 
				{ 
					sJustFieldNames = "ENTITY.ENTITY_ID"; 
					sJustFieldNamesByNumber = "[ENT_TEMP].ENTITY_ID"; 
				} 
				else if (Template.CatId == VEHICLE_MERGE) 
				{ 
					sJustFieldNames = "VEHICLE.UNIT_ID"; 
					sJustFieldNamesByNumber = "[VEH_TEMP].UNIT_ID"; 
				} 
				else if (Template.CatId == POLICY_MERGE) 
				{ 
					sJustFieldNames = "POLICY.POLICY_ID"; 
					sJustFieldNamesByNumber = "[POLICY_TEMP].POLICY_ID"; 
				} 
				else if (Template.CatId == PAYMENT_MERGE) 
				{ 
					sJustFieldNames = "FUNDS.TRANS_ID"; 
				} 
				else if (Template.CatId == AT_MERGE) 
				{ 
					sATTable = Template.TableQualify; 
					sATKeyField = GetSuppTableKeyColName(sATTable); 
					if (sATKeyField.IndexOf(".") == -1) 
					{ 
						sJustFieldNames = sATTable + "." + sATKeyField; 
					} 
					else 
					{ 
						sJustFieldNames = sATKeyField; 
					} 
				} 
				else if (Template.CatId == PATIENT_MERGE) 
				{ 
					sJustFieldNames = "PATIENT.PATIENT_ID"; 
					sJustFieldNamesByNumber = "[PAT_TEMP].PATIENT_ID"; 
				} 
				else if (Template.CatId == PHYSICIAN_MERGE) 
				{ 
					sJustFieldNames = "PHYSICIAN.PHYS_EID"; 
					sJustFieldNamesByNumber = "[QPHYS_TEMP].PHYS_EID"; 
				} 
				else if (Template.CatId == MED_STAFF_MERGE) 
				{ 
					sJustFieldNames = "MED_STAFF.STAFF_EID"; 
					sJustFieldNamesByNumber = "[MED_TEMP].STAFF_EID"; 
				}
				else if (Template.CatId == DIS_PLAN_SEARCH_MERGE) 
				{ 
					sJustFieldNames = "DISABILITY_PLAN.PLAN_ID"; 
					sJustFieldNamesByNumber = "[PLAN_TEMP].PLAN_ID"; 
				}
                //Shruti Policy Billing Merge
                else if (Template.CatId == POLICY_BILLING_MERGE)
                {
                    sJustFieldNames = "BILL_X_BILL_ITEM.POLICY_ID";
                    sJustFieldNamesByNumber = "[BILL_X_BILL_ITEM_TEMP].POLICY_ID";
                }
                //Shruti Leave Plan Merge
                else if (Template.CatId == LEAVE_PLAN_MERGE)
                {
                    sJustFieldNames = "LEAVE_PLAN.LP_ROW_ID";
                    sJustFieldNamesByNumber = "[LV_PLAN_TEMP].LP_ROW_ID";
                }
                // Start Naresh Enhanced Policy Merge
                else if (Template.CatId == ENHANCED_POLICY_MERGE)
                {
                    sJustFieldNames = "POLICY_ENH.POLICY_ID";
                    sJustFieldNamesByNumber = "[POLICY_ENH_TEMP].POLICY_ID";
                }
                // End Naresh Enhanced Policy Merge
                //Anu Tennyson for MITS 18291 : Begins 10/29/2009
                else if (Template.CatId == PROPERTY_MERGE)
                {
                    sJustFieldNames = "PROPERTY_UNIT.PROPERTY_ID";
                    sJustFieldNamesByNumber = "[PROPERTY_UNIT_TEMP].PROPERTY_ID";
                }
                //Anu Tennyson for MITS 18291 : ENDS
				
                //Aman Driver Enh
                else if (Template.CatId == DRIVER_MERGE)
                {
                    sJustFieldNames = "DRIVER.DRIVER_ROW_ID";
                    sJustFieldNamesByNumber = "[DRIVER_TEMP].DRIVER_ROW_ID";
                }
                //Aman Driver Enh

                //tkatsarski: 03/13/15 - RMA-1768
                m_objGeneric.TablesNameUniquePart = GetTablesNameUniquePart();
                //dvatsa-passed m_iClientid as parameter
                objMailMergeQuery = new MailMergeQuery(m_sConnectionString, m_lDbMake, Manager, m_iClientId, m_objGeneric.TablesNameUniquePart); //moved by hlv for MITS 26255 8/1/12

				objDictionaryEnumerator=Template.Fields.GetEnumerator();

				while (objDictionaryEnumerator.MoveNext())
				{
					objField=(TemplateField)objDictionaryEnumerator.Value;
					if (!objField.LongCodeFlag)
					{
						if (objField.FieldTable=="CLAIM_RES_TOT" ||objField.FieldTable == "CLAIM_PAY_TOT")
						{
							if (objField.FieldTable!="")
							{
								sFieldName = objField.FieldName + objField.Param + "_C";
							}
						}
						else
						{
							sFieldName = objField.FieldName;
						}
                        if (sFieldName == "COMMENTS")
                            sFieldTable = objField.FieldTable + objField.CodeTable;
                        else
                            sFieldTable = objField.FieldTable;

                        //MITS 26255 hlv 8/1/12 begin
                        if (objField.FieldType == 16) //Multi-Entity
                        {
                            sbMEField.Remove(0, sbMEField.Length);
                            sbMEField.Append(objMailMergeQuery.GetMultiEntityFieldName(objField.FieldId.ToString(), this.m_RecordId.ToString()));
                            Utilities.AddDelimited(ref sJustFieldNames, sbMEField.ToString(), ",", m_iClientId);
                        }
                        else
                        {
                            //Utilities.AddDelimited(ref sJustFieldNames,objField.FieldTable+"."+sFieldName,",");  Modified by csingh7 R6 Claim Comment Enhancement
                            Utilities.AddDelimited(ref sJustFieldNames, sFieldTable + "." + sFieldName, ",", m_iClientId);   
                        }
                        //MITS 26255 hlv 8/1/12 end

                        //Mjain8 8/25/06 MITS 7517. Idispnum works fine if all seq numbers are in order while in database 
						//there are many cases when seq number is not in order
						//Utilities.AddDelimited(ref sJustFieldNamesByNumber,"FLD"+iDispNum.ToString(),",");
                        Utilities.AddDelimited(ref sJustFieldNamesByNumber, "FLD" + (objField.SeqNum - 1).ToString(), ",", m_iClientId);
                        //Shruti for 9163
						//Generic.AddUnique(arrlstDisplayTables,objField.FieldTable.ToUpper());
                        Generic.AddUnique(arrlstDisplayTables, sFieldTable.ToUpper());  // Modified by csingh7 R6 Claim Comment Enhancement
                        if (arrlstDisplayTables.Count != arrlstDisplayFields.Count)
						{
							arrlstDisplayFields.Add("");
						}
                        //Shruti for 9163
                        //Generic.AddUnique(arrlstDisplayTables,objField.FieldTable.ToUpper());Modified by csingh7 R6 Claim Comment Enhancement
                        Generic.AddUnique(arrlstDisplayTables, sFieldTable.ToUpper());
						m_arrlstColumnHeaders.Add(objField.FieldDesc);
						lTemp=objField.FieldType*(objField.SuppFieldFlag!=0?-1:1);
						m_arrlstFieldTypes.Add(lTemp.ToString());
                        //Shruti for 9163
                        //iIndex = Generic.IndexOf(arrlstDisplayTables, objField.FieldTable.ToUpper());Modified by csingh7 R6 Claim Comment Enhancement
                        iIndex = Generic.IndexOf(arrlstDisplayTables, sFieldTable.ToUpper());
						sFieldValue=arrlstDisplayFields[iIndex].ToString();
                        //Utilities.AddDelimited(ref sFieldValue,objField.FieldTable   Modified by csingh7 R6 Claim Comment Enhancement

                        //MITS 26255 hlv 8/1/12 begin
                        if (objField.FieldType == 16)
                        {
                            Utilities.AddDelimited(ref sFieldValue, sbMEField.ToString() + " | FLD" + (objField.SeqNum - 1).ToString(), ",", m_iClientId);
                        }
                        else
                        {
                            Utilities.AddDelimited(ref sFieldValue, sFieldTable
                                + "." + sFieldName + " | FLD" + (objField.SeqNum - 1).ToString(), ",", m_iClientId);
                        }
                        //MITS 26255 hlv 8/1/12 end
						arrlstDisplayFields[iIndex]=sFieldValue;
						iDispNum++;
                        //skhare7 R8 enhancement Carrier Claim screen search
                        //if (objField.FieldDesc.Contains("UnitSalvage"))
                        //{
                        //    sFieldTable = "UNIT_X_CLAIM";
                        //    Generic.AddUnique(arrlstDisplayTables, sFieldTable.ToUpper());
                        //}//mits 29452 asingh263
                        if (objField.FieldDesc.Contains("PropSalvage"))
                        {
                            sFieldTable = "CLAIM_X_PROPERTYLOSS";
                            Generic.AddUnique(arrlstDisplayTables, sFieldTable.ToUpper());

                        }
                        //skhare7 R8 enhancement Carrier Claim screen search End
					}
				}
				//objMailMergeQuery=new MailMergeQuery(m_sConnectionString,m_lDbMake,Manager);
				//Call to different mail merge functions based on category
				switch(lCatId)
				{
					case CLAIM_MERGE:
                        // akaushik5 Changed for MITS 37272 Starts
                        //objMailMergeQuery.DoClaimMergeQuery(RecordId,arrlstDisplayTables,arrlstDisplayFields,sJustFieldNamesByNumber, bMergeAll,m_lDbMake,sDefendantType,sClaimantType,sExpertType,sPersonInvolvedType, sCurrentAdjuster, sPrimaryCasemanager); // Nitika Added 6 New parameter DefendantType, ClaimantType, ExpertType,PersonInvolvedType, CurrentAdjuster, PrimaryCasemanager for AutoMailmergeSetUp//
						objMailMergeQuery.DoClaimMergeQuery(RecordId,arrlstDisplayTables,arrlstDisplayFields,sJustFieldNamesByNumber, bMergeAll,m_lDbMake,sDefendantType,sClaimantType,sExpertType,sPersonInvolvedType, sCurrentAdjuster, sPrimaryCasemanager, this.IsAdjusterSelected);
                        // akaushik5 Changed for MITS 37272 Ends
                        break;
					case EVENT_MERGE:
						DoEventMergeQuery(RecordId,arrlstDisplayTables,arrlstDisplayFields,sJustFieldNamesByNumber, "[EVENT_TEMP]", ref sFrom,ref sWhere, bMergeAll,-1,sPersonInvolvedType,m_objGeneric.TablesNameUniquePart);// Starts : Nitika Added parameter PersonInvolvedType for AutoMailmergeSetUp End// //Parijat:chubb mail merge changes : needed lastparameter as claim id in case the same function gets used from DoClaimMerge   //tkatsarski: 03/13/15 - RMA-1768
						break;
					case EMPLOYEE_MERGE:
						objMailMergeQuery.DoEmployeeMergeQuery(RecordId,arrlstDisplayTables,arrlstDisplayFields,sJustFieldNamesByNumber,m_lDbMake);
						break;
					case ENTITY_MERGE:
						objMailMergeQuery.DoEntityMergeQuery(RecordId,arrlstDisplayTables,arrlstDisplayFields,sJustFieldNamesByNumber, m_lDbMake);
						break;
					case VEHICLE_MERGE:
						objMailMergeQuery.DoVehicleMergeQuery(RecordId,arrlstDisplayTables,arrlstDisplayFields,sJustFieldNamesByNumber, m_lDbMake);
						break;
					case POLICY_MERGE:
						objMailMergeQuery.DoPolicyMergeQuery(RecordId,arrlstDisplayTables,arrlstDisplayFields,sJustFieldNamesByNumber,m_lDbMake);
						break;
					case PAYMENT_MERGE:
						objMailMergeQuery.DoPaymentMergeQuery(RecordId,arrlstDisplayTables,arrlstDisplayFields,sJustFieldNamesByNumber, m_lDbMake);
						break;
					case AT_MERGE:
						objMailMergeQuery.DoAtMerge(RecordId,sATTable,sATKeyField,arrlstDisplayFields,iIndex);
						break;
					case PATIENT_MERGE:
						objMailMergeQuery.DoPatientMergeQuery(RecordId,arrlstDisplayTables,arrlstDisplayFields,sJustFieldNamesByNumber,ref sFrom,ref sWhere, bMergeAll,m_lDbMake);
						break;
					case PHYSICIAN_MERGE:
						objMailMergeQuery.DoPhysicianMergeQuery(RecordId,arrlstDisplayTables,arrlstDisplayFields,sJustFieldNamesByNumber, ref sFrom,ref sWhere, bMergeAll,m_lDbMake);
						break;
					case MED_STAFF_MERGE:
						objMailMergeQuery.DoMedStaffMergeQuery(RecordId,arrlstDisplayTables,arrlstDisplayFields,sJustFieldNamesByNumber, ref sFrom,ref sWhere, bMergeAll,m_lDbMake);
						break;
					case DIS_PLAN_SEARCH_MERGE:
						objMailMergeQuery.DoDisablityPlanMergeQuery(RecordId,arrlstDisplayTables,arrlstDisplayFields,sJustFieldNamesByNumber,m_lDbMake);
						break;
	                //Shruti Leave Plan Merge
	                case LEAVE_PLAN_MERGE:
	                    objMailMergeQuery.DoLeavePlanMergeQuery(RecordId, arrlstDisplayTables, arrlstDisplayFields, sJustFieldNamesByNumber, m_lDbMake);
	                    break;
	                // Start Naresh Enhanced policy Merge
	                case ENHANCED_POLICY_MERGE:
	                    objMailMergeQuery.DoEnhPolicyMergeQuery(RecordId, arrlstDisplayTables, arrlstDisplayFields, sJustFieldNamesByNumber, m_lDbMake);
	                    break;
	                // End Naresh Enhanced policy Merge
                    //Shruti Policy Billing Merge
                    case POLICY_BILLING_MERGE:
                        objMailMergeQuery.DoPolicyBillingMergeQuery(RecordId, arrlstDisplayTables, arrlstDisplayFields, sJustFieldNamesByNumber, m_lDbMake);
                        break;
                    //Anu Tennyson for MITS 18291 : BEGINS 10/29/2009
                    case PROPERTY_MERGE:
                        objMailMergeQuery.DoPropertyMergeQuery(RecordId, arrlstDisplayTables, arrlstDisplayFields, sJustFieldNamesByNumber, m_lDbMake);
                        break;
                    //Anu Tennyson for MITS 18291 : ENDS
                    //Aman Driver Enh
                    case DRIVER_MERGE:
                        objMailMergeQuery.DoDriverMergeQuery(RecordId, arrlstDisplayTables, arrlstDisplayFields, sJustFieldNamesByNumber, ref sFrom, ref sWhere, bMergeAll, m_lDbMake);
                        break;
                    //Aman Driver Enh
                    
				}
                if (objMailMergeQuery.Data != null) //RMA-11655 : bkuzhanthaim : objMailMergeQuery.Data is being null and m_objMergeds contains data for EVENT_MERGE
                    m_objMergeds = objMailMergeQuery.Data;  //tkatsarski: 03/13/15 - RMA-1768
				if (m_objMergeds!=null)
				{
					this.RowId=this.RowId;
				}
				//objMailMergeQuery=null;
				blnSuccess=true;
				
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Merge.FetchData.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			finally
			{
                if (objMailMergeQuery != null)
                {
                    objMailMergeQuery.Dispose();
                    objMailMergeQuery = null;
                }
				arrlstDisplayTables=null;
				arrlstDisplayFields=null;
				objField=null;
				sFieldName=null; 
				sJustFieldNames=null; 
				sFrom=null; 
				sWhere=null; 
				sJustFieldNamesByNumber=null; 
				sATTable=null;
				sATKeyField=null; 
				sFieldValue=null;
				objDictionaryEnumerator=null;
			}
			return blnSuccess;
		}
		#endregion

		#region Function to delete templorary tables
		/// Name		: CleanupEventMerge
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function deletes temporary tables related to event
		/// </summary>
		public void CleanupEventMerge()
		{
			MailMergeQuery objMailMergeQuery=new MailMergeQuery(m_objGeneric, m_iClientId);//rkaur27
			objMailMergeQuery.Command=Command;
			try
			{
				m_objGeneric.DropMTable (Command,"[EVENT_TEMP]");
				m_objGeneric.DropMTable (Command,"[PI_TEMP]");
				m_objGeneric.DropMTable (Command,"[EVENT_SUPP_TEMP]");
				m_objGeneric.DropMTable (Command,"[RPTD_TEMP]");
				m_objGeneric.DropMTable (Command,"[CAT_TEMP]");  
				m_objGeneric.DropMTable (Command,"[PISUPP_TEMP]");
 
				m_objGeneric.DropMTable (Command,"[BODY_PART_TEMP]");
				m_objGeneric.DropMTable (Command,"[INJURY_TEMP]");
				m_objGeneric.DropMTable (Command,"[DIAG_TEMP]");
				m_objGeneric.DropMTable (Command,"[PHYS_TEMP]");
				m_objGeneric.DropMTable (Command,"[HOSP_TEMP]");  

				m_objGeneric.DropMTable (Command,"[DEPT_TEMP]");
				m_objGeneric.DropMTable (Command,"[FAC_TEMP]");
				m_objGeneric.DropMTable (Command,"[LOC_TEMP]");
				m_objGeneric.DropMTable (Command,"[DIV_TEMP]");
				m_objGeneric.DropMTable (Command,"[REG_TEMP]");  
				m_objGeneric.DropMTable (Command,"[OP_TEMP]");
				m_objGeneric.DropMTable (Command,"[COMP_TEMP]");
				m_objGeneric.DropMTable (Command,"[CLIENT_TEMP]"); 
		   
				m_objGeneric.DropMTable (Command,"[DEPT_INV_TEMP]");
				m_objGeneric.DropMTable (Command,"[FAC_INV_TEMP]");
				m_objGeneric.DropMTable (Command,"[LOC_INV_TEMP]");
				m_objGeneric.DropMTable (Command,"[DIV_INV_TEMP]");
				m_objGeneric.DropMTable (Command,"[REG_INV_TEMP]");  
				m_objGeneric.DropMTable (Command,"[OP_INV_TEMP]");
				m_objGeneric.DropMTable (Command,"[COMP_INV_TEMP]");
				m_objGeneric.DropMTable (Command,"[CLIENT_INV_TEMP]"); 
          
				m_objGeneric.DropMTable (Command,"[DEPT_EMP_TEMP]");
				m_objGeneric.DropMTable (Command,"[FAC_EMP_TEMP]");
				m_objGeneric.DropMTable (Command,"[LOC_EMP_TEMP]");
				m_objGeneric.DropMTable (Command,"[DIV_EMP_TEMP]");
				m_objGeneric.DropMTable (Command,"[REG_EMP_TEMP]");  
				m_objGeneric.DropMTable (Command,"[OP_EMP_TEMP]");
				m_objGeneric.DropMTable (Command,"[COMP_EMP_TEMP]");
				m_objGeneric.DropMTable (Command,"[CLIENT_EMP_TEMP]"); 
				m_objGeneric.DropMTable (Command,"[EVENTQM_TEMP]"); 
                m_objGeneric.DropMTable(Command, "[GENERIC_ORG_TEMP]"); //Parijat : Chubb Mail merge generic fields changes 
				objMailMergeQuery.CleanupPatientMerge();
				objMailMergeQuery.CleanupPhysicianMerge();
				objMailMergeQuery.CleanupMedStaffMerge();
                objMailMergeQuery.CleanupDriverMerge();
				//Event Intervention temp table dropped
				m_objGeneric.DropMTable (Command,"[INT_TEMP]"); 
				m_objGeneric.DropMTable (Command,"[INT_EB_ENT_TEMP]"); 
				m_objGeneric.DropMTable (Command,"[INT_CODE_TEMP]"); 
				m_objGeneric.DropMTable (Command,"[INT_EMP_ENT_TEMP]"); 
				m_objGeneric.DropMTable (Command,"[INT_SUP_ENT_TEMP]"); 
				//objMailMergeQuery=null;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Merge.CleanupEventMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			finally
			{
                if (objMailMergeQuery != null)
                {
                    objMailMergeQuery.Dispose();
                    objMailMergeQuery = null;
                }
			}
          
		}
		#endregion

		#region Function to retrieve event merge data
		/// Name		: DoEventMergeQuery
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function retrieves data related to event
		/// </summary>
		/// <param name="p_lRecordID">Record id for which we need to get the data</param>
		/// <param name="p_arrlstDisplayTables">Table names from which to get the merge data</param>
		/// <param name="p_arrlstDisplayFields">Field names from which to get the merge data</param>
		/// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
		/// <param name="p_sTmpTable">Temporary table names</param>
		/// <param name="p_sFrom">From clause in sql query</param>
		/// <param name="p_sWhere">Where clause in sql query</param>
		/// <param name="bMergeAll">Merge all data or not</param>
        /// <param name="sPersonInvolvedType">Person Involved Type</param>
		internal void DoEventMergeQuery(long p_lRecordID,ArrayList p_arrlstDisplayTables,ArrayList p_arrlstDisplayFields,
			string p_sJustFieldNames,string p_sTmpTable,ref string p_sFrom,ref string p_sWhere
            , bool bMergeAll, long p_claimID, string sPersonInvolvedType, string p_sTablesNameUniquePart)// Nitika added PersonInvolvedType parameter For AutoMailMerge set up.//Parijat - Chubb Mail merge generic fields changes -claim ID added as parameter // Changed By Nitika for automailmerge Exe  //tkatsarski: 03/13/15 - RMA-1768
		{
			string sSQL=""; 
			int iIdx=0; 
			int iPos=0; 
			string sTmp=""; 
			string sTmpNext=""; 
			int iIdxNext=0; 
			string sPISel=""; 
			string sQPatSel="";
            string sQDriverSel = "";  //Aman
			string sQPhysSel=""; 
			string sQStaffSel=""; 
			string sPhysSel=""; 
			string sBitFields=""; 
			string sBitValues=""; 
			string sProcSel="";
			string sFname="";
			string sLname="";
            string sField = "";
            string sValue = "";
            string sKeyField = "";
			ArrayList arrlstTempValues=null;
			DbReader objReader=null;
			DbReader objReaderNext=null;
			DbConnection objConn=null;
			DataSet objTempds=null;
			DataSet objData=null;
			DbTransaction objTran=null;
			int iCreate=0;
			LocalCache objCache=null;
			string sName="";
            string sFieldNames = string.Empty;
            string sFieldValues = string.Empty;
			try
			{
				//We are checking this because this is also called by Claim merge
				//if null means it is not called by claim merge since when using claim
				//merge we use same command object used by claim to create temp tables
				//we are also using this iCreate==1 to check in finally block to clean up
				//the connection and command object only if these are created by this function i.e
				//event merge not claim merge
				if (m_objMergeCmd==null)
				{
					m_objMergeConn=DbFactory.GetDbConnection(m_sConnectionString);
					m_objMergeConn.Open();
					m_objMergeCmd=m_objMergeConn.CreateCommand();
					iCreate=1;
				}
				else
					m_objMergeConn = m_objMergeCmd.Connection;

				//-- ABhateja, 10.12.2006 MITS 7741 -START-
				//-- "Manager" object of "Generic.cs" was not set due to which error was coming in
				//-- "SafeSQL" function for Oracle database while trying to retrieve "Manager.UID"
				if (m_objGeneric.Manager == null)
					m_objGeneric.Manager = Manager;
                m_objGeneric.TablesNameUniquePart = p_sTablesNameUniquePart;    //tkatsarski: 03/13/15 - RMA-1768
				//-- ABhateja, 10.12.2006 MITS 7741 -END-

				if (m_lDbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
				{
					CleanupEventMerge();
				}
				//EVENT
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"EVENT"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"EVENT");
					if (p_sTmpTable=="[CLAIM_TEMP]")
					{
						Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpTable, "[EVENT_TEMP]", "EVENT_ID");
					}
					sSQL="SELECT EVENT_ID,"+p_arrlstDisplayFields[iIdx].ToString()+" [INTO EVENT_TEMP] "
						+" FROM EVENT WHERE EVENT_ID = "+p_lRecordID.ToString();
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
				else if (p_sTmpTable == "[EVENT_TEMP]")
				{
					sSQL = "SELECT EVENT_ID [INTO EVENT_TEMP] FROM EVENT "
						+" WHERE EVENT_ID = " + p_lRecordID.ToString();
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					
					
				}
				if (p_sTmpTable == "[EVENT_TEMP]")
				{
					p_sFrom = "[EVENT_TEMP]";
					p_sWhere = "";
				}
				objCache=new LocalCache(m_sConnectionString,m_iClientId);
				//Intervention & related 
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"EVENT_X_INTERVENT")||
					Utilities.ExistsInArray(p_arrlstDisplayTables,"INT_EB_ENTITY")||
					Utilities.ExistsInArray(p_arrlstDisplayTables,"INT_EMP_ENTITY")||
					Utilities.ExistsInArray(p_arrlstDisplayTables,"INT_SUP_ENTITY")||
					Utilities.ExistsInArray(p_arrlstDisplayTables,"EVENT_X_INT_CODES"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"EVENT_X_INTERVENT");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[EVENT_TEMP]", "[INT_TEMP]", "EVENT_ID");
                    //Shruti for MITS 9627
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_arrlstDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT EVENT_ID,INTERVENT_ROW_ID" + sTmp + 
						" [INTO INT_TEMP]"
						+" FROM EVENT_X_INTERVENT"
						+" WHERE EVENT_X_INTERVENT.EVENT_ID = "+p_lRecordID;
							
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[INT_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[INT_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [INT_TEMP](EVENT_ID,INTERVENT_ROW_ID" + sBitFields + ")"
                            //+ " SELECT EVENT_ID, 0" + sBitValues + " FROM [INT_TEMP]";//Shruti for MITS 9628
                            + " SELECT EVENT_ID, 0" + sBitValues + " FROM [EVENT_TEMP]";	//pmittal5 Mits 18599 11/30/09 - Event_Id should be picked from EVENT_TEMP, as INT_TEMP has no value			
				
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
				}
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"EVENT_X_INT_CODES"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"EVENT_X_INT_CODES");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[INT_TEMP]", "[INT_CODE_TEMP]", "INTERVENT_ROW_ID");
					sSQL = "SELECT EVENT_X_INTERVENT.INTERVENT_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + 
						" [INTO INT_CODE_TEMP]"
						+" FROM [INT_TEMP], EVENT_X_INTERVENT LEFT OUTER JOIN EVENT_X_INT_CODES ON"
						+" EVENT_X_INTERVENT.INTERVENT_ROW_ID = EVENT_X_INT_CODES.INTERVENT_ROW_ID"
						+" WHERE EVENT_X_INTERVENT.INTERVENT_ROW_ID = [INT_TEMP].INTERVENT_ROW_ID";
							
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[INT_CODE_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[INT_CODE_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [INT_CODE_TEMP](INTERVENT_ROW_ID" + sBitFields + ")" 
							+" SELECT INTERVENT_ROW_ID" + sBitValues + " FROM [INT_TEMP]";
				
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
				}
                //Event Comment     csingh7 R6 Claim Comment Enhancement
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "COMMENTS_TEXTEVENT"))
                {
                    iIdx = Generic.IndexOf(p_arrlstDisplayTables, "COMMENTS_TEXTEVENT");

                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[EVENT_TEMP]", "[COMMENTS_TEXTEVENT_TEMP]", "EVENT_ID", "ATTACH_RECORDID");
                    sSQL = "SELECT COMMENTS_TEXTEVENT.ATTACH_RECORDID, " +
                        p_arrlstDisplayFields[iIdx].ToString() + " [INTO COMMENTS_TEXTEVENT_TEMP]"
                        + " FROM [EVENT_TEMP],COMMENTS_TEXT COMMENTS_TEXTEVENT WHERE [EVENT_TEMP].EVENT_ID = COMMENTS_TEXTEVENT.ATTACH_RECORDID AND COMMENTS_TEXTEVENT.ATTACH_TABLE = 'EVENT'";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }


				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"INT_EB_ENTITY"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"INT_EB_ENTITY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[INT_TEMP]", "[INT_EB_ENT_TEMP]", "INTERVENT_ROW_ID");
					sSQL = "SELECT EVENT_X_INTERVENT.INTERVENT_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + 
						"  [INTO INT_EB_ENT_TEMP]"
						+" FROM [INT_TEMP], EVENT_X_INTERVENT,ENTITY INT_EB_ENTITY "
						+" WHERE EVENT_X_INTERVENT.INTERVENT_ROW_ID = [INT_TEMP].INTERVENT_ROW_ID"
						+" AND EVENT_X_INTERVENT.EVALUATOR_EID = INT_EB_ENTITY.ENTITY_ID";
							
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[INT_EB_ENT_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[INT_EB_ENT_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [INT_EB_ENT_TEMP](INTERVENT_ROW_ID" + sBitFields + ")" 
							+" SELECT INTERVENT_ROW_ID" + sBitValues + " FROM [INT_TEMP]";
				
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
				}
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"INT_EMP_ENTITY"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"INT_EMP_ENTITY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[INT_TEMP]", "[INT_EMP_ENT_TEMP]", "INTERVENT_ROW_ID");
					sSQL = "SELECT EVENT_X_INTERVENT.INTERVENT_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + 
						" [INTO INT_EMP_ENT_TEMP]"
						+" FROM [INT_TEMP], EVENT_X_INTERVENT,ENTITY INT_EMP_ENTITY "
						+" WHERE EVENT_X_INTERVENT.INTERVENT_ROW_ID = [INT_TEMP].INTERVENT_ROW_ID"
						+" AND EVENT_X_INTERVENT.EMPLOYEE_EID = INT_EMP_ENTITY.ENTITY_ID";
							
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[INT_EMP_ENT_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[INT_EMP_ENT_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [INT_EMP_ENT_TEMP](INTERVENT_ROW_ID" + sBitFields + ")" 
							+" SELECT INTERVENT_ROW_ID" + sBitValues + " FROM [INT_TEMP]";
				
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
				}
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"INT_SUP_ENTITY"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"INT_SUP_ENTITY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[INT_TEMP]", "[INT_SUP_ENT_TEMP]", "INTERVENT_ROW_ID");
					sSQL = "SELECT EVENT_X_INTERVENT.INTERVENT_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + 
						" [INTO INT_SUP_ENT_TEMP]"
						+" FROM [INT_TEMP], EVENT_X_INTERVENT,ENTITY INT_SUP_ENTITY "
						+" WHERE EVENT_X_INTERVENT.INTERVENT_ROW_ID = [INT_TEMP].INTERVENT_ROW_ID"
						+" AND EVENT_X_INTERVENT.SUPERVISOR_EID = INT_SUP_ENTITY.ENTITY_ID";
							
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[INT_SUP_ENT_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[INT_SUP_ENT_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [INT_SUP_ENT_TEMP](INTERVENT_ROW_ID" + sBitFields + ")" 
							+" SELECT INTERVENT_ROW_ID" + sBitValues + " FROM [INT_TEMP]";
				
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
				}
				//ENTITY
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"RPTD_ENTITY"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"RPTD_ENTITY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpTable, "[RPTD_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + 
						" [INTO RPTD_TEMP]"
						+" FROM EVENT,ENTITY RPTD_ENTITY WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
						+" AND EVENT.RPTD_BY_EID = RPTD_ENTITY.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[RPTD_TEMP]")==0)
					{
                        m_objGeneric.FindBitFields("[RPTD_TEMP]", ref sBitFields, ref sBitValues);
                        //dbisht6 jira RMA-8466
                        if (sBitFields.Contains("EVENT_ID"))
                        {
                            string sBitValuesSegment = null;
                            sBitValuesSegment = sBitValues.ToString().Remove(0, 1);
                            sSQL = "INSERT INTO [RPTD_TEMP](" + sBitFields.Remove(0, 1) + ")" +
                                    " SELECT EVENT_ID" + sBitValuesSegment.Remove(0, sBitValuesSegment.ToString().IndexOf(",")) + " FROM " + p_sTmpTable;

                        }
                        //dbisht6   end
                        else
                        {
                            sSQL = "INSERT INTO [RPTD_TEMP](EVENT_ID" + sBitFields + ")"
                                + " SELECT EVENT_ID" + sBitValues + " FROM " + p_sTmpTable;
                        }
				
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}

				}
				//EVENT_CAT
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"EVENT_CAT"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"EVENT_CAT");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpTable, "[CAT_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString()
						+ " [INTO CAT_TEMP]"+
						" FROM EVENT,CODES EVENT_CAT WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()+
						" AND EVENT.EVENT_IND_CODE = EVENT_CAT.CODE_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[CAT_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[CAT_TEMP]", ref sBitFields, ref sBitValues);
                        //dbisht6 jira RMA-8466
                        if (sBitFields.Contains("EVENT_ID"))
                        {
                            string sBitValuesSegment = null;
                            sBitValuesSegment = sBitValues.ToString().Remove(0, 1);
                            sSQL = "INSERT INTO [CAT_TEMP](" + sBitFields.Remove(0, 1) + ")" +
                                    " SELECT EVENT_ID" + sBitValuesSegment.Remove(0, sBitValuesSegment.ToString().IndexOf(",")) + " FROM " + p_sTmpTable;

                        }
                        //dbisht6   end
                        else
                        {
                            sSQL = "INSERT INTO [CAT_TEMP](EVENT_ID" + sBitFields + ")"
                                + " SELECT EVENT_ID" + sBitValues + " FROM " + p_sTmpTable;
                        } 
                        m_objMergeCmd.CommandType = CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
				}
				//EVENT_SUPP
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"EVENT_SUPP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"EVENT_SUPP");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpTable, "[EVENT_SUPP_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO EVENT_SUPP_TEMP]"+
						" FROM EVENT_SUPP WHERE EVENT_SUPP.EVENT_ID = " + p_lRecordID.ToString();
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[EVENT_SUPP_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[EVENT_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                      //dbisht6 jira RMA-8466
                        if (sBitFields.Contains("EVENT_ID"))
                        {
                            string sBitValuesSegment = null;
                            sBitValuesSegment = sBitValues.ToString().Remove(0, 1);
                            sSQL = "INSERT INTO [EVENT_SUPP_TEMP](" + sBitFields.Remove(0, 1) + ")" +
                                    " SELECT EVENT_ID" + sBitValuesSegment.Remove(0, sBitValuesSegment.ToString().IndexOf(",")) + " FROM " + p_sTmpTable;

                        }
                        //dbisht6   end
                        else
                        {
                            sSQL = "INSERT INTO [EVENT_SUPP_TEMP](EVENT_ID" + sBitFields + ")"
                                + " SELECT EVENT_ID" + sBitValues + " FROM " + p_sTmpTable;
                        }
					
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
				}
				//EVENT_QM
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"EVENT_QM"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"EVENT_QM");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpTable, "[EVENTQM_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO EVENTQM_TEMP]"+
						" FROM EVENT_QM WHERE EVENT_QM.EVENT_ID = " + p_lRecordID.ToString();
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[EVENTQM_TEMP]")==0)
					{
					     //dbisht6 rma-8466
						m_objGeneric.FindBitFields("[EVENTQM_TEMP]", ref sBitFields, ref sBitValues);
                        if (sBitFields.Contains("EVENT_ID"))
                        {
                            string sBitValuesSegment = null;
                            sBitValuesSegment = sBitValues.ToString().Remove(0, 1);


                            sSQL = "INSERT INTO [EVENTQM_TEMP](" + sBitFields.Remove(0, 1) + ")" +
                                    " SELECT EVENT_ID" + sBitValuesSegment.Remove(0, sBitValuesSegment.ToString().IndexOf(",")) + " FROM " + p_sTmpTable;
                            //sbSql.Append(sBitValuesSegment);
                            //sbSql.Append(sBitValuesSegment.Remove(0, sBitValuesSegment.ToString().IndexOf(",")));

                        }
                        else
                        {
                            sSQL = "INSERT INTO [EVENTQM_TEMP](EVENT_ID" + sBitFields + ")"
                                + " SELECT EVENT_ID" + sBitValues + " FROM " + p_sTmpTable;
                        }
					//dbisht6 end
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
				}
				//PERSON_INVOLVED
				if ((Utilities.ExistsInArray(p_arrlstDisplayTables,"PERSON_INVOLVED")) 
					|| (Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_ENTITY"))
					||(Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_SUPP")))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PERSON_INVOLVED");
					iIdxNext=Generic.IndexOf(p_arrlstDisplayTables,"PI_ENTITY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpTable, "[PI_TEMP]", "EVENT_ID");
					sPISel = "-1";
					sTmp = "";
					if (iIdx!=-1)
					{
						sTmp = Generic.Append(p_arrlstDisplayFields[iIdx].ToString());
					}
					if (iIdxNext!=-1)
					{
						sTmp += Generic.Append(p_arrlstDisplayFields[iIdxNext].ToString());
					}
					sSQL = "SELECT EVENT_ID,PI_ROW_ID" + sTmp + " [INTO PI_TEMP]";
					sSQL = sSQL + " FROM PERSON_INVOLVED,ENTITY PI_ENTITY" ;
					sSQL = sSQL + " WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString();
					if (sPISel!="-1")
					{
						sSQL = sSQL + " AND " + sPISel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PI_ENTITY.ENTITY_ID";
                    //Start : Added By Nitika for autoMailMerge Exe
                    if(!string.IsNullOrEmpty(sPersonInvolvedType))
                    {
                        sSQL = sSQL + " AND PI_ENTITY.ENTITY_TABLE_ID IN (" + sPersonInvolvedType.ToString() +")";
                    }
                    //End : Added By Nitika for autoMailMerge Exe
                    //Start MITS 15601 commented these lines
                    //sSQL = sSQL + " AND PI_TYPE_CODE NOT IN (" +Generic.GetCodeIDWithShort("P", objCache.GetTableId("PERSON_INV_TYPE"),m_sConnectionString)
                    //    +","+Generic.GetCodeIDWithShort("PHYS", objCache.GetTableId("PERSON_INV_TYPE"),m_sConnectionString)
                    //    +","+Generic.GetCodeIDWithShort("MED",objCache.GetTableId("PERSON_INV_TYPE"),m_sConnectionString)
                    //    +","+Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"),m_sConnectionString)+")";
                    //End MITS 15601 commented these lines
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[PI_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[PI_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [PI_TEMP](EVENT_ID,PI_ROW_ID" + sBitFields + ")" 
							+" SELECT EVENT_ID,0" + sBitValues + " FROM " + p_sTmpTable;
				
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
				}
					//PHYS_ENTITY
				else if ((Utilities.ExistsInArray(p_arrlstDisplayTables,"PHYS_ENTITY")) 
					|| (Utilities.ExistsInArray(p_arrlstDisplayTables,"HOSP_ENTITY"))
					||(Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_X_BODY_PART"))
					||(Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_X_INJURY"))
					||(Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_X_DIAGNOSIS")))
				
				{
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpTable, "[PI_TEMP]", "EVENT_ID");
					sPISel = "-1";
					sTmp = "";
					sSQL = "SELECT EVENT_ID,PI_ROW_ID" +sTmp + " [INTO PI_TEMP]";
					sSQL = sSQL + " FROM PERSON_INVOLVED,ENTITY PI_ENTITY" ;
					sSQL = sSQL + " WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString();
					if (sPISel!="-1")
					{
						sSQL = sSQL + " AND " + sPISel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PI_ENTITY.ENTITY_ID";
					sSQL = sSQL + " AND PI_TYPE_CODE NOT IN (" +Generic.GetCodeIDWithShort("P",objCache.GetTableId("PERSON_INV_TYPE"),m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("PHYS", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId) + ")";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[PI_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[PI_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [PI_TEMP](EVENT_ID,PI_ROW_ID" + sBitFields + ")" 
							+" SELECT EVENT_ID,0" + sBitValues + " FROM " + p_sTmpTable;
				
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
					
				}
				//PI_SUPP
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_SUPP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PI_SUPP");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[PISUPP_TEMP]", "PI_ROW_ID");
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() +
						" [INTO PISUPP_TEMP]"
						+" FROM PERSON_INVOLVED,PI_SUPP WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString();
					if (sPISel!="-1")
					{
						sSQL = sSQL + " AND " + sPISel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_ROW_ID = PI_SUPP.PI_ROW_ID";
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"PISUPP_TEMP");
					m_objGeneric.FindBitFields("[PISUPP_TEMP]", ref sBitFields, ref sBitValues);
					sSQL = "INSERT INTO [PISUPP_TEMP](PI_ROW_ID" + sBitFields + ")"
						+" SELECT PI_ROW_ID" + sBitValues + " FROM [PI_TEMP]"
						+ "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
				
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupCleanup(m_objMergeCmd);
				}
				//PHYS_ENTITY
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PHYS_ENTITY"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PHYS_ENTITY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[PHYS_TEMP]", "PI_ROW_ID");
					sPhysSel="-1";
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + 
						" [INTO PHYS_TEMP]"
						+ " FROM PERSON_INVOLVED,PI_X_PHYSICIAN,ENTITY PHYS_ENTITY "
						+" WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString();
					if (sPISel!="-1")
					{
						sSQL = sSQL + " AND " + sPISel;
					}
					if (sPhysSel!="-1")
					{
						sSQL = sSQL + " AND " + sPhysSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_ROW_ID = PI_X_PHYSICIAN.PI_ROW_ID"
						+" AND PI_X_PHYSICIAN.PHYSICIAN_EID = PHYS_ENTITY.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"PHYS_TEMP");
					m_objGeneric.FindBitFields("[PHYS_TEMP]", ref sBitFields, ref sBitValues);
					sSQL = "INSERT INTO [PHYS_TEMP](PI_ROW_ID" + sBitFields + ")"
						+" SELECT PI_ROW_ID" + sBitValues + " FROM [PI_TEMP]"
						+ "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
				
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupCleanup(m_objMergeCmd);
				}
				//HOSP_ENTITY
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"HOSP_ENTITY"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"HOSP_ENTITY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[HOSP_TEMP]", "PI_ROW_ID");
					sPhysSel="-1";
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO HOSP_TEMP]"
						+" FROM PERSON_INVOLVED,PI_X_HOSPITAL,ENTITY HOSP_ENTITY "
						+" WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString();
					if (sPISel!="-1")
					{
						sSQL = sSQL + " AND " + sPISel;
					}
					if (sPhysSel!="-1")
					{
						sSQL = sSQL + " AND " + sPhysSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_ROW_ID = PI_X_HOSPITAL.PI_ROW_ID"
						+" AND PI_X_HOSPITAL.HOSPITAL_EID = HOSP_ENTITY.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"HOSP_TEMP");
					m_objGeneric.FindBitFields("[HOSP_TEMP]", ref sBitFields, ref sBitValues);
					sSQL = "INSERT INTO [HOSP_TEMP](PI_ROW_ID" + sBitFields + ")"
						+" SELECT PI_ROW_ID" + sBitValues + " FROM [PI_TEMP]"
						+ "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
				
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupCleanup(m_objMergeCmd);
				}
				//multi-valued items which will be concatenated together
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_X_BODY_PART"))
				{
					arrlstTempValues=new ArrayList();
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PI_X_BODY_PART");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[BODY_PART_TEMP]", "PI_ROW_ID");
					iPos=p_arrlstDisplayFields[iIdx].ToString().IndexOf("FLD");
					sTmp=p_arrlstDisplayFields[iIdx].ToString().Substring(iPos);
					switch(m_lDbMake)
					{
						case (int) eDatabaseType.DBMS_IS_ACCESS:
							sSQL = "CREATE TABLE [BODY_PART_TEMP] (PI_ROW_ID LONG, " + sTmp + " TEXT(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SQLSRVR:
							sSQL = "CREATE TABLE [BODY_PART_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SYBASE:
							sSQL = "CREATE TABLE [BODY_PART_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_INFORMIX:
							sSQL = "CREATE TABLE [BODY_PART_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_DB2:
							sSQL = "CREATE TABLE [BODY_PART_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_ORACLE:
							
							m_objGeneric.DeleteOracleTables("DROP TABLE [BODY_PART_TEMP]","[BODY_PART_TEMP]");
							sSQL = "CREATE TABLE [BODY_PART_TEMP] (PI_ROW_ID NUMBER(10), " + sTmp + " VARCHAR(250))";
							break;
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					sSQL = m_objGeneric.SafeSQL("SELECT PI_ROW_ID FROM [PI_TEMP]");
					objData=null;
					m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objData);
					for (int iCnt=0;iCnt<objData.Tables[0].Rows.Count;iCnt++)
					{
						sTmpNext="";
						
						sSQL="SELECT BODY_PART_CODE FROM PI_X_BODY_PART WHERE PI_ROW_ID ="
							+Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0])
							+" AND BODY_PART_CODE <> 0";
						m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objTempds);
						for (int i=0;i<objTempds.Tables[0].Rows.Count;i++)
						{
							if (sTmpNext!="")
							{
								sTmpNext = sTmpNext + ", ";
							}
							sTmpNext = sTmpNext + objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])))
								+"_"+objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])));
						}
						objTempds=null;	
						if (sTmpNext.Length >250)
						{
							sTmp=sTmp.Substring(0,250);
						}
						sSQL = "INSERT INTO [BODY_PART_TEMP](PI_ROW_ID," + sTmp + ") VALUES (" 
							+ Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0]) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
						arrlstTempValues.Add(sSQL);
						
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					objTran=m_objMergeConn.BeginTransaction();
					m_objMergeCmd.Transaction=objTran;
					for (int i=0;i<arrlstTempValues.Count;i++)
					{
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(arrlstTempValues[i].ToString());
						m_objMergeCmd.ExecuteNonQuery();
					}
					objTran.Commit();
					arrlstTempValues=null;
				}
				//PI_X_INJURY
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_X_INJURY"))
				{
					arrlstTempValues=new ArrayList();
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PI_X_INJURY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[INJURY_TEMP]", "PI_ROW_ID");
					iPos=p_arrlstDisplayFields[iIdx].ToString().IndexOf("FLD");
					sTmp=p_arrlstDisplayFields[iIdx].ToString().Substring(iPos);
					switch(m_lDbMake)
					{
						case (int) eDatabaseType.DBMS_IS_ACCESS:
							sSQL = "CREATE TABLE [INJURY_TEMP] (PI_ROW_ID LONG, " + sTmp + " TEXT(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SQLSRVR:
							sSQL = "CREATE TABLE [INJURY_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SYBASE:
							sSQL = "CREATE TABLE [INJURY_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_INFORMIX:
							sSQL = "CREATE TABLE [INJURY_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_DB2:
							sSQL = "CREATE TABLE [INJURY_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_ORACLE:
							
							m_objGeneric.DeleteOracleTables("DROP TABLE [INJURY_TEMP]","[INJURY_TEMP]");
							sSQL = "CREATE TABLE [INJURY_TEMP] (PI_ROW_ID NUMBER(10), " + sTmp + " VARCHAR(250))";
							break;
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					sSQL = m_objGeneric.SafeSQL("SELECT PI_ROW_ID FROM [PI_TEMP]");
					objData=null;
					m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objData);
					for (int iCnt=0;iCnt<objData.Tables[0].Rows.Count;iCnt++)
					{
						sTmpNext="";
						sSQL="SELECT INJURY_CODE FROM PI_X_INJURY WHERE PI_ROW_ID ="
							+Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0])
							+" AND INJURY_CODE <> 0";
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						
						m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objTempds);
						for (int i=0;i<objTempds.Tables[0].Rows.Count;i++)
						{
							if (sTmpNext!="")
							{
								sTmpNext = sTmpNext + ", ";
							}
							sTmpNext = sTmpNext + objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])))
								+"_"+objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])));
						}
						objTempds=null;	
						if (sTmpNext.Length >250)
						{
							sTmp=sTmp.Substring(0,250);
						}
						sSQL = "INSERT INTO [INJURY_TEMP](PI_ROW_ID," + sTmp + ") VALUES (" 
							+ Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0]) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
						arrlstTempValues.Add(sSQL);
						
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					objTran=m_objMergeConn.BeginTransaction();
					m_objMergeCmd.Transaction=objTran;
					for (int i=0;i<arrlstTempValues.Count;i++)
					{
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(arrlstTempValues[i].ToString());
						m_objMergeCmd.ExecuteNonQuery();
					}
					objTran.Commit();
					arrlstTempValues=null;
				}
				//PI_X_DIAGNOSIS
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_X_DIAGNOSIS"))
				{
					arrlstTempValues=new ArrayList();
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PI_X_DIAGNOSIS");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[DIAG_TEMP]", "PI_ROW_ID");
					iPos=p_arrlstDisplayFields[iIdx].ToString().IndexOf("FLD");
					sTmp=p_arrlstDisplayFields[iIdx].ToString().Substring(iPos);
					switch(m_lDbMake)
					{
						case (int) eDatabaseType.DBMS_IS_ACCESS:
							sSQL = "CREATE TABLE [DIAG_TEMP] (PI_ROW_ID LONG, " + sTmp + " TEXT(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SQLSRVR:
							sSQL = "CREATE TABLE [DIAG_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SYBASE:
							sSQL = "CREATE TABLE [DIAG_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_INFORMIX:
							sSQL = "CREATE TABLE [DIAG_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_DB2:
							sSQL = "CREATE TABLE [DIAG_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_ORACLE:
							
							m_objGeneric.DeleteOracleTables("DROP TABLE [DIAG_TEMP]","[DIAG_TEMP]");
							sSQL = "CREATE TABLE [DIAG_TEMP] (PI_ROW_ID NUMBER(10), " + sTmp + " VARCHAR(250))";
							break;
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					sSQL = m_objGeneric.SafeSQL("SELECT PI_ROW_ID FROM [PI_TEMP]");
					objData=null;
					m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objData);
					for (int iCnt=0;iCnt<objData.Tables[0].Rows.Count;iCnt++)
					{
						sTmpNext="";
						sSQL="SELECT DIAGNOSIS_CODE FROM PI_X_DIAGNOSIS WHERE PI_ROW_ID ="
							+Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0])
							+" AND DIAGNOSIS_CODE <> 0";
						m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objTempds);
						for (int i=0;i<objTempds.Tables[0].Rows.Count;i++)
						{
							if (sTmpNext!="")
							{
								sTmpNext = sTmpNext + ", ";
							}
							sTmpNext = sTmpNext + objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])))
								+"_"+objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])));
						}
						objTempds=null;	
						
						if (sTmpNext.Length >250)
						{
							sTmp=sTmp.Substring(0,250);
						}
						sSQL = "INSERT INTO [DIAG_TEMP](PI_ROW_ID," + sTmp + ") VALUES (" 
							+ Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0]) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
						arrlstTempValues.Add(sSQL);
						
					}
					
					m_objMergeCmd.CommandType=CommandType.Text;
					objTran=m_objMergeConn.BeginTransaction();
					m_objMergeCmd.Transaction=objTran;
					for (int i=0;i<arrlstTempValues.Count;i++)
					{
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(arrlstTempValues[i].ToString());
						m_objMergeCmd.ExecuteNonQuery();
					}
					objTran.Commit();
					arrlstTempValues=null;
				}

				//QUEST/Win
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PATIENT") || 
					Utilities.ExistsInArray(p_arrlstDisplayTables,"PATIENT_ENTITY")||
					AnyQuestPatientTable(p_arrlstDisplayTables) ||
                    StartsWith(p_arrlstDisplayTables, "PAT_"))
				{
					arrlstTempValues=new ArrayList();
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PATIENT");
					iIdxNext=Generic.IndexOf(p_arrlstDisplayTables,"PATIENT_ENTITY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpTable, "[PAT_TEMP]", "EVENT_ID");
					sQPatSel="-1";
					sTmp = "";
					if (iIdx != -1)
					{
						sTmp= Generic.Append(p_arrlstDisplayFields[iIdx].ToString());
					}
					if (iIdxNext != -1)
					{
						sTmp= sTmp+Generic.Append(p_arrlstDisplayFields[iIdxNext].ToString());
					}

					sSQL = "SELECT PERSON_INVOLVED.EVENT_ID,PERSON_INVOLVED.PI_ROW_ID" + sTmp 
						+ " [INTO PAT_TEMP]"
						+ " FROM PERSON_INVOLVED,PATIENT,ENTITY PATIENT_ENTITY WHERE PERSON_INVOLVED.EVENT_ID = " 
						+ p_lRecordID.ToString();
					if (sQPatSel != "-1")
						sSQL = sSQL + " AND " + sQPatSel;
					sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT.PATIENT_ID"
						+ " AND PATIENT.PATIENT_EID = PATIENT_ENTITY.ENTITY_ID";
					sSQL = m_objGeneric.SafeSQL(sSQL);
					objReader=DbFactory.GetDbReader(m_sConnectionString,sSQL);
					if (m_objGeneric.GetMergeCount("[PAT_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[PAT_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [PAT_TEMP](EVENT_ID,PI_ROW_ID" + sBitFields + ")" 
							+" SELECT EVENT_ID,0" + sBitValues + " FROM " + p_sTmpTable;
						objConn = DbFactory.GetDbConnection(m_sConnectionString);
						objConn.Open();
						objConn.ExecuteNonQuery (m_objGeneric.SafeSQL(sSQL));
						objConn.Close();
					}
				}
				//PATIENT_SUPP
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PATIENT_SUPP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PATIENT_SUPP");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[PAT_SUPP_TEMP]", "PI_ROW_ID");
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO PAT_SUPP_TEMP]"
						+ " FROM PERSON_INVOLVED,PATIENT_SUPP WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString();
					if (sQPatSel!="-1")
					{
						sSQL = sSQL + " AND " + sQPatSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT_SUPP.PATIENT_ID";
					
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"PAT_SUPP_TEMP");
					m_objGeneric.FindBitFields("[PAT_SUPP_TEMP]", ref sBitFields, ref sBitValues);
					sSQL = "INSERT INTO [PAT_SUPP_TEMP](PI_ROW_ID" + sBitFields + ")"
						+" SELECT PI_ROW_ID" + sBitValues + " FROM [PAT_TEMP]"
						+ "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
				
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupCleanup(m_objMergeCmd);
				}
				//PATIENT_PRI_PHYS
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PATIENT_PRI_PHYS"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PATIENT_PRI_PHYS");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[PATPRIPHYS_TEMP]", "PI_ROW_ID");
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO PATPRIPHYS_TEMP]"
						+ " FROM PERSON_INVOLVED,PATIENT,ENTITY PATIENT_PRI_PHYS WHERE "+ 
						" PERSON_INVOLVED.EVENT_ID =" + p_lRecordID.ToString();
					if (sQPatSel!="-1")
					{
						sSQL = sSQL + " AND " + sQPatSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT.PATIENT_ID AND "+
						" PATIENT.PRI_PHYSICIAN_EID = PATIENT_PRI_PHYS.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"PATPRIPHYS_TEMP");
					m_objGeneric.FindBitFields("[PATPRIPHYS_TEMP]", ref sBitFields, ref sBitValues);
					sSQL = "INSERT INTO [PATPRIPHYS_TEMP](PI_ROW_ID" + sBitFields + ")"
						+" SELECT PI_ROW_ID" + sBitValues + " FROM [PAT_TEMP]"
						+ "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
				
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupCleanup(m_objMergeCmd);
				}
				//PATIENT_PROCEDURE
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PATIENT_PROCEDURE"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PATIENT_PROCEDURE");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[PATPROC_TEMP]", "PI_ROW_ID");
					sProcSel="-1";
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO PATPROC_TEMP]"
						+ " FROM PERSON_INVOLVED,PATIENT_PROCEDURE WHERE "+ 
						" PERSON_INVOLVED.EVENT_ID =" + p_lRecordID.ToString();
					if (sQPatSel!="-1")
					{
						sSQL = sSQL + " AND " + sQPatSel;
					}
					if (sProcSel!="-1")
					{
						sSQL = sSQL + " AND " + sProcSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT_PROCEDURE.PATIENT_ID ";
				
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"PATPROC_TEMP");
					m_objGeneric.FindBitFields("[PATPROC_TEMP]", ref sBitFields, ref sBitValues);
					sSQL = "INSERT INTO [PATPROC_TEMP](PI_ROW_ID" + sBitFields + ")"
						+" SELECT PI_ROW_ID" + sBitValues + " FROM [PAT_TEMP]"
						+ "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
				
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupCleanup(m_objMergeCmd);
				}
				//PATIENT_DIAGNOSIS
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PATIENT_DIAGNOSIS"))
				{
					arrlstTempValues=new ArrayList();
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PATIENT_DIAGNOSIS");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[PDIAG_TEMP]", "PI_ROW_ID");
					iPos=p_arrlstDisplayFields[iIdx].ToString().IndexOf("FLD");
					sTmp=p_arrlstDisplayFields[iIdx].ToString().Substring(iPos);
					switch(m_lDbMake)
					{
						case (int) eDatabaseType.DBMS_IS_ACCESS:
							sSQL = "CREATE TABLE [PDIAG_TEMP] (PI_ROW_ID LONG, " + sTmp + " TEXT(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SQLSRVR:
							sSQL = "CREATE TABLE [PDIAG_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SYBASE:
							sSQL = "CREATE TABLE [PDIAG_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_INFORMIX:
							sSQL = "CREATE TABLE [PDIAG_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_DB2:
							sSQL = "CREATE TABLE [PDIAG_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_ORACLE:
							
							m_objGeneric.DeleteOracleTables("DROP TABLE [PDIAG_TEMP]","[PDIAG_TEMP]");
							sSQL = "CREATE TABLE [PDIAG_TEMP] (PI_ROW_ID NUMBER(10), " + sTmp + " VARCHAR(250))";
							break;
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					sSQL = m_objGeneric.SafeSQL("SELECT PERSON_INVOLVED.PI_ROW_ID,PERSON_INVOLVED.PATIENT_ID "
						+" FROM [PAT_TEMP],PERSON_INVOLVED WHERE "
						+" [PAT_TEMP].PI_ROW_ID = PERSON_INVOLVED.PI_ROW_ID");
					objData=null;
					m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objData);
					for (int iCnt=0;iCnt<objData.Tables[0].Rows.Count;iCnt++)
					{
						sTmpNext="";
						sSQL="SELECT DIAGNOSIS_CODE FROM PATIENT_DIAGNOSIS WHERE PATIENT_ID ="
							+Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0])
							+" AND DIAGNOSIS_CODE <> 0";
						m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objTempds);
						for (int i=0;i<objTempds.Tables[0].Rows.Count;i++)
						{
							if (sTmpNext!="")
							{
								sTmpNext = sTmpNext + ", ";
							}
							sTmpNext = sTmpNext + objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])))
								+"_"+objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])));
						}
						objTempds=null;	
						
						if (sTmpNext.Length >250)
						{
							sTmp=sTmp.Substring(0,250);
						}
						sSQL = "INSERT INTO [PDIAG_TEMP](PI_ROW_ID," + sTmp + ") VALUES (" 
							+ Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0]) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
						arrlstTempValues.Add(sSQL);
						
					}
					
					m_objMergeCmd.CommandType=CommandType.Text;
					objTran=m_objMergeConn.BeginTransaction();
					m_objMergeCmd.Transaction=objTran;
					for (int i=0;i<arrlstTempValues.Count;i++)
					{					
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
					objTran.Commit();
					arrlstTempValues=null;
				}
				//PATIENT_DRG_CODES
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PATIENT_DRG_CODES"))
				{
					arrlstTempValues=new ArrayList();
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PATIENT_DRG_CODES");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[DRG_TEMP]", "PI_ROW_ID");
					iPos=p_arrlstDisplayFields[iIdx].ToString().IndexOf("FLD");
					sTmp=p_arrlstDisplayFields[iIdx].ToString().Substring(iPos);
					switch(m_lDbMake)
					{
						case (int) eDatabaseType.DBMS_IS_ACCESS:
							sSQL = "CREATE TABLE [DRG_TEMP] (PI_ROW_ID LONG, " + sTmp + " TEXT(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SQLSRVR:
							sSQL = "CREATE TABLE [DRG_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SYBASE:
							sSQL = "CREATE TABLE [DRG_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_INFORMIX:
							sSQL = "CREATE TABLE [DRG_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_DB2:
							sSQL = "CREATE TABLE [DRG_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_ORACLE:
							
							m_objGeneric.DeleteOracleTables("DROP TABLE [DRG_TEMP]","[DRG_TEMP]");
							sSQL = "CREATE TABLE [DRG_TEMP] (PI_ROW_ID NUMBER(10), " + sTmp + " VARCHAR(250))";
							break;
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					sSQL = m_objGeneric.SafeSQL("SELECT PERSON_INVOLVED.PI_ROW_ID,PERSON_INVOLVED.PATIENT_ID "
						+" FROM [PAT_TEMP],PERSON_INVOLVED WHERE "
						+" [PAT_TEMP].PI_ROW_ID = PERSON_INVOLVED.PI_ROW_ID");
					objData=null;
					m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objData);
					for (int iCnt=0;iCnt<objData.Tables[0].Rows.Count;iCnt++)
					{
						sTmpNext="";
						sSQL="SELECT DRG_CODE FROM PATIENT_DRG_CODES WHERE PATIENT_ID ="
							+Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0])
							+" AND DRG_CODE <> 0";
						m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objTempds);
						for (int i=0;i<objTempds.Tables[0].Rows.Count;i++)
						{
							if (sTmpNext!="")
							{
								sTmpNext = sTmpNext + ", ";
							}
							sTmpNext = sTmpNext + objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])))
								+"_"+objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])));
						}
						objTempds=null;	
						
						if (sTmpNext.Length >250)
						{
							sTmp=sTmp.Substring(0,250);
						}
						sSQL = "INSERT INTO [DRG_TEMP](PI_ROW_ID," + sTmp + ") VALUES (" 
							+ Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0]) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
						arrlstTempValues.Add(sSQL);
						
					}
					objTran=m_objMergeConn.BeginTransaction();
					m_objMergeCmd.Transaction=objTran;
					for (int i=0;i<arrlstTempValues.Count;i++)
					{
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
					objTran.Commit();
					arrlstTempValues=null;
				}

				//PATIENT_ACT_TAKEN
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PATIENT_ACT_TAKEN"))
				{
					arrlstTempValues=new ArrayList();
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PATIENT_ACT_TAKEN");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[ACTT_TEMP]", "PI_ROW_ID");
					iPos=p_arrlstDisplayFields[iIdx].ToString().IndexOf("FLD");
					sTmp=p_arrlstDisplayFields[iIdx].ToString().Substring(iPos);
					switch(m_lDbMake)
					{
						case (int) eDatabaseType.DBMS_IS_ACCESS:
							sSQL = "CREATE TABLE [ACTT_TEMP] (PI_ROW_ID LONG, " + sTmp + " TEXT(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SQLSRVR:
							sSQL = "CREATE TABLE [ACTT_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SYBASE:
							sSQL = "CREATE TABLE [ACTT_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_INFORMIX:
							sSQL = "CREATE TABLE [ACTT_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_DB2:
							sSQL = "CREATE TABLE [ACTT_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_ORACLE:
							
							m_objGeneric.DeleteOracleTables("DROP TABLE [ACTT_TEMP]","[ACTT_TEMP]");
							sSQL = "CREATE TABLE [ACTT_TEMP] (PI_ROW_ID NUMBER(10), " + sTmp + " VARCHAR(250))";
							break;
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					sSQL = m_objGeneric.SafeSQL("SELECT PERSON_INVOLVED.PI_ROW_ID,PERSON_INVOLVED.PATIENT_ID "
						+" FROM [PAT_TEMP],PERSON_INVOLVED WHERE "
						+" [PAT_TEMP].PI_ROW_ID = PERSON_INVOLVED.PI_ROW_ID");
					objData=null;
					m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objData);
					for (int iCnt=0;iCnt<objData.Tables[0].Rows.Count;iCnt++)
					{
						sTmpNext="";
						sSQL="SELECT ACTION_CODE FROM PATIENT_ACT_TAKEN WHERE PATIENT_ID ="
							+Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][1])
							+" AND ACTION_CODE <> 0";
						m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objTempds);
						for (int i=0;i<objTempds.Tables[0].Rows.Count;i++)
						{
							if (sTmpNext!="")
							{
								sTmpNext = sTmpNext + ", ";
							}
							sTmpNext = sTmpNext + objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])))
								+"_"+objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])));
						}
						objTempds=null;	
						
						if (sTmpNext.Length >250)
						{
							sTmp=sTmp.Substring(0,250);
						}
						sSQL = "INSERT INTO [ACTT_TEMP](PI_ROW_ID," + sTmp + ") VALUES (" 
							+ Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0]) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
						arrlstTempValues.Add(sSQL);
						
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					objTran=m_objMergeConn.BeginTransaction();
					m_objMergeCmd.Transaction=objTran;
					for (int i=0;i<arrlstTempValues.Count;i++)
					{
						
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
					objTran.Commit();
					arrlstTempValues=null;
				}
				//PATIENT_ATTN_PHYS
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PATIENT_ATTN_PHYS"))
				{
					arrlstTempValues=new ArrayList();
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PATIENT_ATTN_PHYS");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[ATPHYS_TEMP]", "PI_ROW_ID");
					iPos=p_arrlstDisplayFields[iIdx].ToString().IndexOf("FLD");
					sTmp=p_arrlstDisplayFields[iIdx].ToString().Substring(iPos);
					switch(m_lDbMake)
					{
						case (int) eDatabaseType.DBMS_IS_ACCESS:
							sSQL = "CREATE TABLE [ATPHYS_TEMP] (PI_ROW_ID LONG, " + sTmp + " TEXT(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SQLSRVR:
							sSQL = "CREATE TABLE [ATPHYS_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SYBASE:
							sSQL = "CREATE TABLE [ATPHYS_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_INFORMIX:
							sSQL = "CREATE TABLE [ATPHYS_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_DB2:
							sSQL = "CREATE TABLE [ATPHYS_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_ORACLE:
							
							m_objGeneric.DeleteOracleTables("DROP TABLE [ATPHYS_TEMP]","[ATPHYS_TEMP]");
							sSQL = "CREATE TABLE [ATPHYS_TEMP] (PI_ROW_ID NUMBER(10), " + sTmp + " VARCHAR(250))";
							break;
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					sSQL = m_objGeneric.SafeSQL("SELECT PERSON_INVOLVED.PI_ROW_ID,PERSON_INVOLVED.PATIENT_ID "
						+" FROM [PAT_TEMP],PERSON_INVOLVED WHERE "
						+" [PAT_TEMP].PI_ROW_ID = PERSON_INVOLVED.PI_ROW_ID");
					objData=null;
					m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objData);
					for (int iCnt=0;iCnt<objData.Tables[0].Rows.Count;iCnt++)
					{
						sTmpNext="";
						sSQL="SELECT PHYSICIAN_EID FROM PATIENT_ATTN_PHYS WHERE PATIENT_ID ="
							+Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][1])
							+" AND PHYSICIAN_EID <> 0";
						m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objTempds);
						
						for (int i=0;i<objTempds.Tables[0].Rows.Count;i++)
						{
							if (sTmpNext!="")
							{
								sTmpNext = sTmpNext + ", ";
							}
							objCache.GetEntityInfo(Conversion.ConvertStrToLong(
								Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0]))
								,ref sFname,ref sLname);
							if (sFname!="")
							{
								sName=sLname+" ,"+sFname;
							}
							else
							{
								sName="";
							}
							sTmpNext = sTmpNext +sName;
						}
						objTempds=null;	
						if (sTmpNext.Length >250)
						{
							sTmp=sTmp.Substring(0,250);
						}
						sSQL = "INSERT INTO [ATPHYS_TEMP](PI_ROW_ID," + sTmp + ") VALUES (" 
							+ Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0]) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
						arrlstTempValues.Add(sSQL);
					}
					objTran=m_objMergeConn.BeginTransaction();
					m_objMergeCmd.Transaction=objTran;
					for (int i=0;i<arrlstTempValues.Count;i++)
					{
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
					objTran.Commit();
					arrlstTempValues=null;
				}
				//QUEST/Win physicians
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PHYSICIAN") || 
					Utilities.ExistsInArray(p_arrlstDisplayTables,"PHYSICIAN_ENTITY")||
					AnyQuestPhysicianTable(p_arrlstDisplayTables) ||
                    StartsWith(p_arrlstDisplayTables, "PHYS_"))
				{
					arrlstTempValues=new ArrayList();
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PHYSICIAN");
					iIdxNext=Generic.IndexOf(p_arrlstDisplayTables,"PHYSICIAN_ENTITY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpTable, "[QPHYS_TEMP]", "EVENT_ID");
					sQPhysSel="-1";
					sTmp = "";
					if (iIdx != -1)
					{
						sTmp= Generic.Append(p_arrlstDisplayFields[iIdx].ToString());
					}
					if (iIdxNext != -1)
					{
						sTmp= sTmp+Generic.Append(p_arrlstDisplayFields[iIdxNext].ToString());
					}

					sSQL = "SELECT PERSON_INVOLVED.EVENT_ID,PERSON_INVOLVED.PI_ROW_ID" + sTmp 
						+ " [INTO QPHYS_TEMP]"
						+ " FROM PERSON_INVOLVED,PHYSICIAN,ENTITY PHYSICIAN_ENTITY WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString();
					if (sQPhysSel != "-1")
						sSQL = sSQL + " AND " + sQPatSel;
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYSICIAN.PHYS_EID"
						+ " AND PHYSICIAN.PHYS_EID = PHYSICIAN_ENTITY.ENTITY_ID";
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[QPHYS_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[QPHYS_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [QPHYS_TEMP](EVENT_ID,PI_ROW_ID" + sBitFields + ")" 
							+" SELECT EVENT_ID,0" + sBitValues + " FROM " + p_sTmpTable;
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
                }
               
				//PHYS_SUPP
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PHYS_SUPP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PHYS_SUPP");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[PHYSSUPP_TEMP]", "PI_ROW_ID");
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," 
						+ p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO PHYSSUPP_TEMP]"
						+" FROM PERSON_INVOLVED,PHYS_SUPP WHERE PERSON_INVOLVED.EVENT_ID = " 
						+ p_lRecordID.ToString();
					if (sQPhysSel!="-1")
					{
						sSQL = sSQL + " AND " + sQPhysSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_SUPP.PHYS_EID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"PHYSSUPP_TEMP");
					m_objGeneric.FindBitFields("[PHYSSUPP_TEMP]", ref sBitFields, ref sBitValues);
					sSQL = "INSERT INTO [PHYSSUPP_TEMP](PI_ROW_ID" + sBitFields + ")"
						+" SELECT PI_ROW_ID" + sBitValues + " FROM [QPHYS_TEMP]"
						+ "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
				
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupCleanup(m_objMergeCmd);
				}
				//PHYS_PRIVS
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PHYS_PRIVS"))
				{
                    sField = "";
                    sValue = "";
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PHYS_PRIVS");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[PRIV_TEMP]", "PI_ROW_ID");
                    //Shruti for MITS 9626
                    sKeyField = p_arrlstDisplayFields[iIdx].ToString();
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," 
						+ p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO PRIV_TEMP]"
						+" FROM PERSON_INVOLVED,PHYS_PRIVS WHERE PERSON_INVOLVED.EVENT_ID = " 
						+ p_lRecordID.ToString();
					if (sQPhysSel!="-1")
					{
						sSQL = sSQL + " AND " + sQPhysSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_PRIVS.PHYS_EID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"PRIV_TEMP");
                    //Shruti for MITS 9626
                    if (m_objGeneric.GetMergeCount("[PRIV_TEMP]") == 0)
                    {
                        if (sKeyField != "")
                        {
                            string[] arrFields = m_objGeneric.SafeSQL(sKeyField).Split(',');
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if (arrFields[i].Contains("PHYS_PRIVS.PREV_ID") || arrFields[i].Contains("PHYS_PRIVS.PHYS_EID") || arrFields[i].Contains("PHYS_PRIVS.STATUS_CODE")
                                    || arrFields[i].Contains("PHYS_PRIVS.TYPE_CODE") || arrFields[i].Contains("PHYS_PRIVS.CATEGORY_CODE"))
                                {
                                    string[] arrGetField = arrFields[i].Split(' ');
                                    if (arrGetField.Length == 2)
                                    {
                                        sField = sField + "," + arrGetField[1];
                                        sValue = sValue + "," + 0;
                                    }
                                }
                            }
                        }

                        m_objGeneric.FindBitFields("[PRIV_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [PRIV_TEMP](PI_ROW_ID";
                        sSQL = sField == "" ? sSQL + sBitFields + ")"
                            + " SELECT PI_ROW_ID" + sBitValues + " FROM [QPHYS_TEMP]"
                            + "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])" : sSQL + sBitFields + sField + ")"
                            + " SELECT PI_ROW_ID" + sBitValues + sValue + " FROM [QPHYS_TEMP]"
                            + "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
                        //Shruti for MITS 9626 ends
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        m_objGeneric.MergeDupCleanup(m_objMergeCmd);
                    }
				}
				//PHYS_CERTS
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PHYS_CERTS"))
				{
                    sField = "";
                    sValue = "";
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PHYS_CERTS");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[CERT_TEMP]", "PI_ROW_ID");
                    //Shruti for MITS 9626
                    sKeyField = p_arrlstDisplayFields[iIdx].ToString();
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," 
						+ p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO CERT_TEMP]"
						+" FROM PERSON_INVOLVED,PHYS_CERTS WHERE PERSON_INVOLVED.EVENT_ID = " 
						+ p_lRecordID.ToString();
					if (sQPhysSel!="-1")
					{
						sSQL = sSQL + " AND " + sQPhysSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_CERTS.PHYS_EID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"CERT_TEMP");
                    //Shruti for MITS 9626
                    if (m_objGeneric.GetMergeCount("[CERT_TEMP]") == 0)
                    {
                        if (sKeyField != "")
                        {
                            string[] arrFields = m_objGeneric.SafeSQL(sKeyField).Split(',');
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if (arrFields[i].Contains("PHYS_CERTS.CERT_ID") || arrFields[i].Contains("PHYS_CERTS.PHYS_EID") || arrFields[i].Contains("PHYS_CERTS.NAME_CODE")
                                    || arrFields[i].Contains("PHYS_CERTS.STATUS_CODE") || arrFields[i].Contains("PHYS_CERTS.BOARD_CODE"))
                                {
                                    string[] arrGetField = arrFields[i].Split(' ');
                                    if (arrGetField.Length == 2)
                                    {
                                        sField = sField + "," + arrGetField[1];
                                        sValue = sValue + "," + 0;
                                    }
                                }
                            }
                        }

                        m_objGeneric.FindBitFields("[CERT_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [CERT_TEMP](PI_ROW_ID";
                        sSQL = sField == "" ? sSQL + sBitFields + ") SELECT PI_ROW_ID" + sBitValues + " FROM [QPHYS_TEMP]" : sSQL +
                                sField + sBitFields + ") SELECT PI_ROW_ID" + sValue + sBitValues + " FROM [QPHYS_TEMP]";
                        //Shruti for MITS 9626 ends

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        m_objGeneric.MergeDupCleanup(m_objMergeCmd);
                    }
				}
				//PHYS_EDUCATION
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PHYS_EDUCATION"))
				{
                    sField = "";
                    sValue = "";
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PHYS_EDUCATION");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[EDUC_TEMP]", "PI_ROW_ID");
                    //Shruti for MITS 9626
                    sKeyField = p_arrlstDisplayFields[iIdx].ToString();

					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," 
						+ p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO EDUC_TEMP]"
						+" FROM PERSON_INVOLVED,PHYS_EDUCATION WHERE PERSON_INVOLVED.EVENT_ID = " 
						+ p_lRecordID.ToString();
					if (sQPhysSel!="-1")
					{
						sSQL = sSQL + " AND " + sQPhysSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_EDUCATION.PHYS_EID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"EDUC_TEMP");
                    //Shruti for MITS 9626
                    if (m_objGeneric.GetMergeCount("[CERT_TEMP]") == 0)
                    {
                        if (sKeyField != "")
                        {
                            string[] arrFields = m_objGeneric.SafeSQL(sKeyField).Split(',');
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if (arrFields[i].Contains("PHYS_EDUCATION.EDUC_ID") || arrFields[i].Contains("PHYS_EDUCATION.PHYS_EID") || arrFields[i].Contains("PHYS_EDUCATION.EDUC_TYPE_CODE")
                                    || arrFields[i].Contains("PHYS_EDUCATION.INSTITUTION_EID") || arrFields[i].Contains("PHYS_EDUCATION.DEGREE_TYPE"))
                                {
                                    string[] arrGetField = arrFields[i].Split(' ');
                                    if (arrGetField.Length == 2)
                                    {
                                        sField = sField + "," + arrGetField[1];
                                        sValue = sValue + "," + 0;
                                    }
                                }
                            }
                        }
                        m_objGeneric.FindBitFields("[EDUC_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [EDUC_TEMP](PI_ROW_ID";
                        sSQL = sField == "" ? sSQL + sBitFields + ")" + " SELECT PI_ROW_ID" + sBitValues + " FROM [QPHYS_TEMP]"
                            + "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])" : sBitFields + sField + ")" + " SELECT PI_ROW_ID" + sBitValues + sValue + " FROM [QPHYS_TEMP]"
                            + "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        m_objGeneric.MergeDupCleanup(m_objMergeCmd);
                    }
						
				}
				//PHYS_PREV_HOSP
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PHYS_PREV_HOSP"))
				{
                    sField = "";
                    sValue = "";
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PHYS_PREV_HOSP");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[PHOSP_TEMP]", "PI_ROW_ID");
                    //Shruti for MITS 9626
                    sKeyField = p_arrlstDisplayFields[iIdx].ToString();
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," 
						+ p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO PHOSP_TEMP]"
						+" FROM PERSON_INVOLVED,PHYS_PREV_HOSP WHERE PERSON_INVOLVED.EVENT_ID = " 
						+ p_lRecordID.ToString();
					if (sQPhysSel!="-1")
					{
						sSQL = sSQL + " AND " + sQPhysSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_PREV_HOSP.PHYS_EID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"PHOSP_TEMP");
                    //Shruti for MITS 9626
                    if (m_objGeneric.GetMergeCount("[PHOSP_TEMP]") == 0)
                    {
                        if (sKeyField != "")
                        {
                            string[] arrFields = m_objGeneric.SafeSQL(sKeyField).Split(',');
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if (arrFields[i].Contains("PHYS_PREV_HOSP.PREV_HOSP_ID") || arrFields[i].Contains("PHYS_PREV_HOSP.PHYS_EID") || arrFields[i].Contains("PHYS_PREV_HOSP.HOSPITAL_EID")
                                    || arrFields[i].Contains("PHYS_PREV_HOSP.STATUS_CODE") || arrFields[i].Contains("PHYS_PREV_HOSP.PRIV_CODE"))
                                {
                                    string[] arrGetField = arrFields[i].Split(' ');
                                    if (arrGetField.Length == 2)
                                    {
                                        sField = sField + "," + arrGetField[1];
                                        sValue = sValue + "," + 0;
                                    }
                                }
                            }
                        }

                        m_objGeneric.FindBitFields("[PHOSP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [PHOSP_TEMP](PI_ROW_ID";
                        sSQL = sField == "" ? sSQL + sBitFields + ")"
                            + " SELECT PI_ROW_ID" + sBitValues + " FROM [QPHYS_TEMP]"
                            + "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])" :
                            sSQL + sBitFields + sField + ")"
                            + " SELECT PI_ROW_ID" + sBitValues + sValue + " FROM [QPHYS_TEMP]"
                            + "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
                        //Shruti for MITS 9626
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        m_objGeneric.MergeDupCleanup(m_objMergeCmd);
                    }
				}
				//PHYS_SUB_SPECIALTY
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PHYS_SUB_SPECIALTY"))
				{
					arrlstTempValues=new ArrayList();
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"PHYS_SUB_SPECIALTY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[SUBSPEC_TEMP]", "PI_ROW_ID");
                    

					iPos=p_arrlstDisplayFields[iIdx].ToString().IndexOf("FLD");
					sTmp=p_arrlstDisplayFields[iIdx].ToString().Substring(iPos);
					switch(m_lDbMake)
					{
						case (int) eDatabaseType.DBMS_IS_ACCESS:
							sSQL = "CREATE TABLE [SUBSPEC_TEMP] (PI_ROW_ID LONG, " + sTmp + " TEXT(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SQLSRVR:
							sSQL = "CREATE TABLE [SUBSPEC_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_SYBASE:
							sSQL = "CREATE TABLE [SUBSPEC_TEMP] (PI_ROW_ID int, " + sTmp + " varchar(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_INFORMIX:
							sSQL = "CREATE TABLE [SUBSPEC_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_DB2:
							sSQL = "CREATE TABLE [SUBSPEC_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
							break;
						case (int) eDatabaseType.DBMS_IS_ORACLE:
							
							m_objGeneric.DeleteOracleTables("DROP TABLE [SUBSPEC_TEMP]","[SUBSPEC_TEMP]");
							sSQL = "CREATE TABLE [SUBSPEC_TEMP] (PI_ROW_ID NUMBER(10), " + sTmp + " VARCHAR(250))";
							break;
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					sSQL = m_objGeneric.SafeSQL("SELECT PERSON_INVOLVED.PI_ROW_ID,PERSON_INVOLVED.PI_EID FROM"
						+" [QPHYS_TEMP],PERSON_INVOLVED WHERE [QPHYS_TEMP].PI_ROW_ID ="
						+" PERSON_INVOLVED.PI_ROW_ID");
					objData=null;
					m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objData);
					for (int iCnt=0;iCnt<objData.Tables[0].Rows.Count;iCnt++)
					{
						sTmpNext="";
						sSQL="SELECT SPECIALTY_CODE FROM PHYS_SUB_SPECIALTY WHERE PHYS_EID ="
							+Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0])
							+" AND SPECIALTY_CODE <> 0";
						m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref objTempds);
						for (int i=0;i<objTempds.Tables[0].Rows.Count;i++)
						{
							if (sTmpNext!="")
							{
								sTmpNext = sTmpNext + ", ";
							}
							sTmpNext = sTmpNext + objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])))
								+"_"+objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])));
						}
						objTempds=null;	
						
						if (sTmpNext.Length >250)
						{
							sTmp=sTmp.Substring(0,250);
						}
						sSQL = "INSERT INTO [SUBSPEC_TEMP](PI_ROW_ID," + sTmp + ") VALUES (" 
							+ Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0]) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
						arrlstTempValues.Add(sSQL);
					
					}
					objTran=m_objMergeConn.BeginTransaction();
					m_objMergeCmd.Transaction=objTran;
					for (int i=0;i<arrlstTempValues.Count;i++)
					{
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
					objTran.Commit();
					arrlstTempValues=null;
				}
                //Aman Driver Enhancement MITS 29866
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "DRIVER") ||
                     Utilities.ExistsInArray(p_arrlstDisplayTables, "DRIVER_ENTITY") ||
                     AnyQuestDriverTable(p_arrlstDisplayTables))
                {
                    arrlstTempValues = new ArrayList();
                    iIdx = Generic.IndexOf(p_arrlstDisplayTables, "DRIVER");
                    iIdxNext = Generic.IndexOf(p_arrlstDisplayTables, "DRIVER_ENTITY");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpTable, "[DRIVER_TEMP]", "EVENT_ID");
                    sQDriverSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_arrlstDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sTmp = sTmp + Generic.Append(p_arrlstDisplayFields[iIdxNext].ToString());
                    }

                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID,PERSON_INVOLVED.PI_ROW_ID" + sTmp
                        + " [INTO DRIVER_TEMP]"
                        + " FROM PERSON_INVOLVED,DRIVER,ENTITY DRIVER_ENTITY WHERE PERSON_INVOLVED.EVENT_ID = "
                        + p_lRecordID.ToString();
                    if (sQDriverSel != "-1")
                        sSQL = sSQL + " AND " + sQDriverSel;
                    sSQL = sSQL + " AND PERSON_INVOLVED.DRIVER_ROW_ID = DRIVER.DRIVER_ROW_ID"
                        + " AND DRIVER.DRIVER_EID = DRIVER_ENTITY.ENTITY_ID";
                    sSQL = m_objGeneric.SafeSQL(sSQL);
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (m_objGeneric.GetMergeCount("[DRIVER_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[DRIVER_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [DRIVER_TEMP](EVENT_ID,PI_ROW_ID" + sBitFields + ")"
                            + " SELECT EVENT_ID,0" + sBitValues + " FROM " + p_sTmpTable;
                        objConn = DbFactory.GetDbConnection(m_sConnectionString);
                        objConn.Open();
                        objConn.ExecuteNonQuery(m_objGeneric.SafeSQL(sSQL));
                        objConn.Close();
                    }
                }
                //DRIVER_SUPP
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "DRIVER_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_arrlstDisplayTables, "DRIVER_SUPP");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[DRIVER_TEMP]", "[DRIVER_SUPP_TEMP]", "PI_ROW_ID");
                    sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString()
                        + " [INTO DRIVER_SUPP_TEMP]"
                        + " FROM PERSON_INVOLVED,DRIVER_SUPP WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString();
                    if (sQDriverSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sQDriverSel;
                    }
                    sSQL = sSQL + " AND PERSON_INVOLVED.DRIVER_ROW_ID = DRIVER_SUPP.DRIVER_ROW_ID";


                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    m_objGeneric.MergeDupTable(m_objMergeCmd, "DRIVER_SUPP_TEMP");
                    m_objGeneric.FindBitFields("[DRIVER_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                    sSQL = "INSERT INTO [DRIVER_SUPP_TEMP](PI_ROW_ID" + sBitFields + ")"
                        + " SELECT PI_ROW_ID" + sBitValues + " FROM [DRIVER_TEMP]"
                        + "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    m_objGeneric.MergeDupCleanup(m_objMergeCmd);
                }
                //Aman Driver Enhancement MITS 29866
				//QUEST/Win med. staff
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"MED_STAFF") || 
					Utilities.ExistsInArray(p_arrlstDisplayTables,"MED_ENTITY")||
					AnyQuestStaffTable(p_arrlstDisplayTables) ||
                    StartsWith(p_arrlstDisplayTables, "MED_"))
				{
					arrlstTempValues=new ArrayList();
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"MED_STAFF");
					iIdxNext=Generic.IndexOf(p_arrlstDisplayTables,"MED_ENTITY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpTable, "[MED_TEMP]", "EVENT_ID");
					sQStaffSel="-1";
					sTmp = "";
					if (iIdx != -1)
					{
						sTmp= Generic.Append(p_arrlstDisplayFields[iIdx].ToString());
					}
					if (iIdxNext != -1)
					{
						sTmp= sTmp+Generic.Append(p_arrlstDisplayFields[iIdxNext].ToString());
					}

					sSQL = "SELECT PERSON_INVOLVED.EVENT_ID,PERSON_INVOLVED.PI_ROW_ID" + sTmp 
						+" [INTO MED_TEMP] FROM PERSON_INVOLVED,MED_STAFF,ENTITY MED_ENTITY WHERE PERSON_INVOLVED.EVENT_ID = " 
						+ p_lRecordID.ToString();
					if (sQStaffSel != "-1")
						sSQL = sSQL + " AND " + sQPatSel;
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = MED_STAFF.STAFF_EID"
						+ " AND MED_STAFF.STAFF_EID = MED_ENTITY.ENTITY_ID";
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[MED_TEMP]")==0)
					{
                        //Mits 18595
                        //m_objGeneric.FindBitFields("[MED_TEMP]", ref sBitFields, ref sBitValues);
                        //sSQL = "INSERT INTO [MED_TEMP](EVENT_ID,PI_ROW_ID" + sBitFields + ")" 
                        //    +" SELECT EVENT_ID,0" + sBitValues + " FROM " + p_sTmpTable;
                        sFieldNames = "";
                        sFieldValues = "";
                        m_objGeneric.FindFields("[MED_TEMP]", ref sFieldNames, ref sFieldValues);
                        sSQL = "INSERT INTO [MED_TEMP](EVENT_ID" + sFieldNames + ")"
                            + " SELECT EVENT_ID" + sFieldValues + " FROM " + p_sTmpTable;
                        //End-pmittal5
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
				}
				//MED_STAFF_SUPP
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"MED_STAFF_SUPP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"MED_STAFF_SUPP");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[MED_TEMP]", "[MEDSUPP_TEMP]", "PI_ROW_ID");
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO MEDSUPP_TEMP]"
						+ " FROM PERSON_INVOLVED,MED_STAFF_SUPP WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString();
					if (sQStaffSel!="-1")
					{
						sSQL = sSQL + " AND " + sQStaffSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = MED_STAFF_SUPP.STAFF_EID";
				
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					
					m_objGeneric.MergeDupTable (m_objMergeCmd,"MEDSUPP_TEMP");
					m_objGeneric.FindBitFields("[MEDSUPP_TEMP]", ref sBitFields, ref sBitValues);
					sSQL = "INSERT INTO [MEDSUPP_TEMP](PI_ROW_ID" + sBitFields + ")"
						+" SELECT PI_ROW_ID" + sBitValues + " FROM [MED_TEMP]"
						+ "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
				
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupCleanup(m_objMergeCmd);
				}
				//STAFF_PRIVS
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"STAFF_PRIVS"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"STAFF_PRIVS");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[MED_TEMP]", "[SPRIV_TEMP]", "PI_ROW_ID");
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO SPRIV_TEMP]"
						+ " FROM PERSON_INVOLVED,STAFF_PRIVS WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString();
					if (sQStaffSel!="-1")
					{
						sSQL = sSQL + " AND " + sQStaffSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = STAFF_PRIVS.STAFF_EID";
				
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"SPRIV_TEMP");
                    //Mits 18595
                    //m_objGeneric.FindBitFields("[SPRIV_TEMP]", ref sBitFields, ref sBitValues);
                    //sSQL = "INSERT INTO [SPRIV_TEMP](PI_ROW_ID" + sBitFields + ")"
                    //    +" SELECT PI_ROW_ID" + sBitValues + " FROM [MED_TEMP]"
                    //    + "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
                    sFieldValues = "";
                    sFieldNames = ""; 
                    m_objGeneric.FindFields("[SPRIV_TEMP]", ref sFieldNames, ref sFieldValues);
                    sSQL = "INSERT INTO [SPRIV_TEMP](PI_ROW_ID" + sFieldNames + ")"
                        + " SELECT PI_ROW_ID" + sFieldValues + " FROM [MED_TEMP]"
                        + "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
				    //End - pmittal5
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupCleanup(m_objMergeCmd);
				}
				//STAFF_CERTS
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"STAFF_CERTS"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"STAFF_CERTS");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[MED_TEMP]", "[SCERT_TEMP]", "PI_ROW_ID");
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO SCERT_TEMP]"
						+ " FROM PERSON_INVOLVED,STAFF_CERTS WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString();
					if (sQStaffSel!="-1")
					{
						sSQL = sSQL + " AND " + sQStaffSel;
					}
					sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = STAFF_CERTS.STAFF_EID";
				
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupTable (m_objMergeCmd,"SCERT_TEMP");
                    //Mits 18595
                    //m_objGeneric.FindBitFields("[SCERT_TEMP]", ref sBitFields, ref sBitValues);
                    //sSQL = "INSERT INTO [SCERT_TEMP](PI_ROW_ID" + sBitFields + ")"
                    //    +" SELECT PI_ROW_ID" + sBitValues + " FROM [MED_TEMP]"
                    //    + "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
                    sFieldValues = "";
                    sFieldNames = ""; 
                    m_objGeneric.FindFields("[SCERT_TEMP]", ref sFieldNames, ref sFieldValues);
                    sSQL = "INSERT INTO [SCERT_TEMP](PI_ROW_ID" + sFieldNames + ")"
                        + " SELECT PI_ROW_ID" + sFieldValues + " FROM [MED_TEMP]"
                        + "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
                    //End - pmittal5
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					m_objGeneric.MergeDupCleanup(m_objMergeCmd);
				}
				
				//Mjain8 Added 8/22/06 MITS 7517
				//EventxOSHA table added
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"EVENT_X_OSHA"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"EVENT_X_OSHA");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere,p_sTmpTable, "[EVNTOSHA_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO EVNTOSHA_TEMP]"+
						" FROM EVENT_X_OSHA WHERE EVENT_X_OSHA.EVENT_ID = " + p_lRecordID.ToString();
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[EVNTOSHA_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[EVNTOSHA_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [EVNTOSHA_TEMP](EVENT_ID" + sBitFields + ")" 
							+" SELECT EVENT_ID" + sBitValues + " FROM " + p_sTmpTable;
				
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
				}
				//Event_x_osha_supp Added
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"EVENT_X_OSHA_SUPP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"EVENT_X_OSHA_SUPP");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere,p_sTmpTable, "[EVOSHASUP_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO EVOSHASUP_TEMP]"+
						" FROM EVENT_X_OSHA_SUPP WHERE EVENT_X_OSHA_SUPP.EVENT_ID = " + p_lRecordID.ToString();
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[EVOSHASUP_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[EVOSHASUP_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [EVOSHASUP_TEMP](EVENT_ID" + sBitFields + ")" 
							+" SELECT EVENT_ID" + sBitValues + " FROM " + p_sTmpTable;
				
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}
				}
				//Mjain8 MITS 7517 end

                ConnectOrgH(p_lRecordID, p_arrlstDisplayTables, p_arrlstDisplayFields, ref p_sFrom, ref p_sWhere, ref p_sTmpTable, p_claimID);//Parijat - Chubb Mail merge generic fields changes
				if (p_sTmpTable=="[EVENT_TEMP]")
				{
					sSQL = "SELECT " + p_sJustFieldNames
						+" FROM "+p_sFrom;
					if (p_sWhere!="")
					{
						sSQL = sSQL + " WHERE " + p_sWhere;
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=sSQL;
					m_objGeneric.GetDataset(m_objMergeConn,sSQL,ref m_objMergeds);
					CleanupEventMerge();
				
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Merge.DoEventMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			finally
			{
				sSQL=null; 
				sTmp=null; 
				sTmpNext=null; 
				sPISel=null;
				sQPatSel=null; 
				sQPhysSel=null; 
				sQStaffSel=null; 
				sPhysSel=null; 
				sBitFields=null; 
				sBitValues=null; 
				sProcSel=null;
				sFname=null;
				sLname=null;
				arrlstTempValues=null;
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                if (objTempds != null)
                {
                    objTempds.Dispose();
                }
                if (objData != null)
                {
                    objData.Dispose();
                }
				sName=null;
				if (objCache!=null)
				{
					objCache.Dispose();
					objCache=null;
				}
				if (iCreate==1)
				{
					m_objMergeCmd=null;	
				}
				if (iCreate==1)
				{
					if (m_objMergeConn!=null)
					{
						m_objMergeConn.Close();
						m_objMergeConn.Dispose();
						m_objMergeConn=null;	
					}
				}
                if (objTran != null)
                {
                    objTran.Dispose();
                }
				if (objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objReaderNext!=null)
				{
					objReaderNext.Close();
					objReaderNext.Dispose();
				}
			}
		}
		#endregion

		#region Common functions used for event merge
		/// Name		: ConnectOrgH
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function gets data related to organizational hierarchy 
		/// </summary>
		/// <param name="p_lRecordID">Record id for which we need to get the data</param>
		/// <param name="p_arrlstDisplayTables">Table names from which to get the merge data</param>
		/// <param name="p_arrlstDisplayFields">Field names from which to get the merge data</param>
		/// <param name="p_sFrom">From clause in sql query</param>
		/// <param name="p_sWhere">Where clause in sql query</param>
		/// <param name="p_sTmpTable">Temporary table names</param>
		private void ConnectOrgH(long p_lRecordID,ArrayList p_arrlstDisplayTables,ArrayList p_arrlstDisplayFields,
			ref string p_sFrom, ref string p_sWhere,ref string p_sTmpParent,long claimID) //Parijat - Chubb Mail merge generic fields changes :added parameter
		{
            bool flagORG = false;//Parijat : Chubb Mail merge generic fields changes 
			string sSQL=""; 
			int iIdx=0; 
			string sBitFields=""; 
			string sBitValues="";
			DbConnection objConn=null;
			DbReader objReader=null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;
			LocalCache objCache=null;
			try
			{
				
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				objCommand=objConn.CreateCommand();
				objTran=objConn.BeginTransaction();
				objCommand.Transaction=objTran;
				sSQL = "UPDATE ENTITY SET PARENT_EID = 0 WHERE ENTITY_ID = 0";
				objCommand.CommandText=sSQL;
				objCommand.ExecuteNonQuery();
				sSQL = "UPDATE EVENT SET DEPT_EID = 0 WHERE EVENT_ID = " + p_lRecordID.ToString() + " AND DEPT_EID IS NULL";
				objCommand.CommandText=sSQL;
				objCommand.ExecuteNonQuery();
				sSQL = "UPDATE EVENT SET DEPT_INVOLVED_EID = 0 WHERE EVENT_ID = " + p_lRecordID.ToString() + " AND DEPT_INVOLVED_EID IS NULL";
				objCommand.CommandText=sSQL;
				objCommand.ExecuteNonQuery();
				objTran.Commit();
				objConn.Close();
				objCommand=null;
				objCache=new LocalCache(m_sConnectionString,m_iClientId);
				//DEPARTMENT
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"DEPT"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"DEPT");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[DEPT_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO DEPT_TEMP]"
						+" FROM EVENT,ENTITY DEPT WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
						+ " AND EVENT.DEPT_EID = DEPT.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
                }//Parijat : Chubb Mail merge generic fields changes ***********GENERIC DEPARTMENT*********
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "GENERIC_ORG") && claimID !=-1)
                {
                    
                        iIdx = Generic.IndexOf(p_arrlstDisplayTables, "GENERIC_ORG");
                        Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[GENERIC_ORG_TEMP]", "EVENT_ID");
                        sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID [INTO GENERIC_ORG_TEMP]"
                            + " FROM EVENT,ENTITY GENERIC_ORG WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
                            + " AND EVENT.DEPT_EID = GENERIC_ORG.ENTITY_ID";
                        //if (!AckCheckForOrgLevel(p_lRecordID, claimID, "DEPARTMENT"))
                        //{
                        //    sSQL += " AND 1=2";//so that any ohow atleast a blank table is created so as to map the FLD fields
                        //}

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        flagORG = true;
                    
                }//Parijat : Chubb Mail merge generic fields changes -end
				//FACILITY
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"FACILITY"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"FACILITY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[FAC_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO FAC_TEMP]"
						+" FROM EVENT,ENTITY DEPT,ENTITY FACILITY WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
						+" AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
						+" AND DEPT.PARENT_EID = FACILITY.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
                }//Parijat : Chubb Mail merge generic fields changes ***********GENERIC FACILITY*********
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "GENERIC_ORG") && claimID != -1)
                {
                    if (AckCheckForOrgLevel(p_lRecordID, claimID, "FACILITY"))
                    {
                        iIdx = Generic.IndexOf(p_arrlstDisplayTables, "GENERIC_ORG");

                        if (flagORG)
                            //sSQL = "INSERT [INTO GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString();
                            sSQL = "INSERT INTO [GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID";
                        else
                        {
                            Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[GENERIC_ORG_TEMP]", "EVENT_ID");
                            sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID [INTO GENERIC_ORG_TEMP]";
                        }
                        sSQL += " FROM EVENT,ENTITY DEPT,ENTITY GENERIC_ORG WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
                          + " AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
                          + " AND DEPT.PARENT_EID = GENERIC_ORG.ENTITY_ID";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        flagORG = true;
                    }
                }//Parijat : Chubb Mail merge generic fields changes -end
				//LOCATION
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"LOCATION"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"LOCATION");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[LOC_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO LOC_TEMP]"
						+" FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION WHERE EVENT.EVENT_ID= " + p_lRecordID.ToString()
						+" AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
						+" AND DEPT.PARENT_EID = FACILITY.ENTITY_ID"
						+" AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
                //Parijat : Chubb Mail merge generic fields changes***********GENERIC LOCATION*********
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "GENERIC_ORG") && claimID != -1)
                {
                    if (AckCheckForOrgLevel(p_lRecordID, claimID, "LOCATION"))
                    {
                        iIdx = Generic.IndexOf(p_arrlstDisplayTables, "GENERIC_ORG");
                        if (flagORG)
                            //sSQL = "INSERT [INTO GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString();
                            sSQL = "INSERT INTO [GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID";
                        else
                        {
                            Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[GENERIC_ORG_TEMP]", "EVENT_ID");
                            sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID [INTO GENERIC_ORG_TEMP]";
                        }

                        sSQL += " FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY GENERIC_ORG WHERE EVENT.EVENT_ID= " + p_lRecordID.ToString()
                            + " AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
                            + " AND DEPT.PARENT_EID = FACILITY.ENTITY_ID"
                            + " AND FACILITY.PARENT_EID = GENERIC_ORG.ENTITY_ID";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Parijat : Chubb Mail merge generic fields changes -end
				//DIVISION
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"DIVISION"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"DIVISION");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[DIV_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO DIV_TEMP]"
						+" FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION, "
						+" ENTITY DIVISION "
						+" WHERE EVENT.EVENT_ID= " + p_lRecordID.ToString()
						+" AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
						+" AND DEPT.PARENT_EID = FACILITY.ENTITY_ID"
						+" AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID"
						+" AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
                //Parijat : Chubb Mail merge generic fields changes***********GENERIC DIVISION*********
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "GENERIC_ORG") && claimID != -1)
                {
                    if (AckCheckForOrgLevel(p_lRecordID, claimID, "DIVISION"))
                    {
                        iIdx = Generic.IndexOf(p_arrlstDisplayTables, "GENERIC_ORG");
                        if (flagORG)
                            //sSQL = "INSERT [INTO GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString();
                            sSQL = "INSERT INTO [GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID";
                        else
                        {
                            Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[GENERIC_ORG_TEMP]", "EVENT_ID");
                            sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID [INTO GENERIC_ORG_TEMP]";
                        }

                        sSQL += " FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION, "
                            + " ENTITY GENERIC_ORG "
                            + " WHERE EVENT.EVENT_ID= " + p_lRecordID.ToString()
                            + " AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
                            + " AND DEPT.PARENT_EID = FACILITY.ENTITY_ID"
                            + " AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID"
                            + " AND LOCATION.PARENT_EID = GENERIC_ORG.ENTITY_ID";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Parijat : Chubb Mail merge generic fields changes -end
				//REGION
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"REGION"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"REGION");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[REG_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO REG_TEMP]"
						+" FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION, "
						+" ENTITY DIVISION,ENTITY REGION "
						+" WHERE EVENT.EVENT_ID= " + p_lRecordID.ToString()
						+" AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
						+" AND DEPT.PARENT_EID = FACILITY.ENTITY_ID"
						+" AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID"
						+" AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID"
						+" AND DIVISION.PARENT_EID = REGION.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
                //Parijat : Chubb Mail merge generic fields changes***********GENERIC REGION*********
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "GENERIC_ORG") && claimID != -1)
                {
                    if (AckCheckForOrgLevel(p_lRecordID, claimID, "REGION"))
                    {
                        iIdx = Generic.IndexOf(p_arrlstDisplayTables, "GENERIC_ORG");

                        if (flagORG)
                            //sSQL = "INSERT [INTO GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString();
                            sSQL = "INSERT INTO [GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID";
                        else
                        {
                            Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[GENERIC_ORG_TEMP]", "EVENT_ID");
                            sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID [INTO GENERIC_ORG_TEMP]";
                        }
                        sSQL += " FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION, "
                        + " ENTITY DIVISION,ENTITY GENERIC_ORG "
                        + " WHERE EVENT.EVENT_ID= " + p_lRecordID.ToString()
                        + " AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
                        + " AND DEPT.PARENT_EID = FACILITY.ENTITY_ID"
                        + " AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID"
                        + " AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID"
                        + " AND DIVISION.PARENT_EID = GENERIC_ORG.ENTITY_ID";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        flagORG = true;
                    }
                }
                //Parijat : Chubb Mail merge generic fields changes -end
				//OPERATION
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"OPERATION"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"OPERATION");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[OP_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO OP_TEMP]"
						+" FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION, "
						+" ENTITY DIVISION,ENTITY REGION,ENTITY OPERATION"
						+" WHERE EVENT.EVENT_ID= " + p_lRecordID.ToString()
						+" AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
						+" AND DEPT.PARENT_EID = FACILITY.ENTITY_ID"
						+" AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID"
						+" AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID"
						+" AND DIVISION.PARENT_EID = REGION.ENTITY_ID"
						+" AND REGION.PARENT_EID = OPERATION.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
                //Parijat : Chubb Mail merge generic fields changes***********GENERIC OPERATION*********
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "GENERIC_ORG") && claimID != -1)
                {
                    if (AckCheckForOrgLevel(p_lRecordID, claimID, "OPERATION"))
                    {
                        iIdx = Generic.IndexOf(p_arrlstDisplayTables, "GENERIC_ORG");

                        if (flagORG)
                            //sSQL = "INSERT [INTO GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString();
                            sSQL = "INSERT INTO [GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID";
                        else
                        {
                            Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[GENERIC_ORG_TEMP]", "EVENT_ID");
                            sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID [INTO GENERIC_ORG_TEMP]";
                        }
                        sSQL += " FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION, "
                        + " ENTITY DIVISION,ENTITY REGION,ENTITY GENERIC_ORG"
                        + " WHERE EVENT.EVENT_ID= " + p_lRecordID.ToString()
                        + " AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
                        + " AND DEPT.PARENT_EID = FACILITY.ENTITY_ID"
                        + " AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID"
                        + " AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID"
                        + " AND DIVISION.PARENT_EID = REGION.ENTITY_ID"
                        + " AND REGION.PARENT_EID = GENERIC_ORG.ENTITY_ID";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        flagORG = true;
                    }
                }
                //Parijat : Chubb Mail merge generic fields changes -end
				//COMPANY
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"COMPANY"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"COMPANY");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[COMP_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO COMP_TEMP]"
						+" FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION, "
						+" ENTITY DIVISION,ENTITY REGION,ENTITY OPERATION,"
						+" ENTITY COMPANY"
						+ " WHERE EVENT.EVENT_ID= " + p_lRecordID.ToString()
						+" AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
						+" AND DEPT.PARENT_EID = FACILITY.ENTITY_ID"
						+" AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID"
						+" AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID"
						+" AND DIVISION.PARENT_EID = REGION.ENTITY_ID"
						+" AND REGION.PARENT_EID = OPERATION.ENTITY_ID"
						+" AND OPERATION.PARENT_EID = COMPANY.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					
				}
                //Parijat : Chubb Mail merge generic fields changes***********GENERIC COMPANY*********
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "GENERIC_ORG") && claimID != -1)
                {
                    if (AckCheckForOrgLevel(p_lRecordID, claimID, "COMPANY"))
                    {
                        iIdx = Generic.IndexOf(p_arrlstDisplayTables, "GENERIC_ORG");

                        if (flagORG)
                            //sSQL = "INSERT [INTO GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString();
                            sSQL = "INSERT INTO [GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID";
                        else
                        {
                            Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[GENERIC_ORG_TEMP]", "EVENT_ID");
                            sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID [INTO GENERIC_ORG_TEMP]";
                        }
                        sSQL += " FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION, "
                        + " ENTITY DIVISION,ENTITY REGION,ENTITY OPERATION,"
                        + " ENTITY GENERIC_ORG"
                        + " WHERE EVENT.EVENT_ID= " + p_lRecordID.ToString()
                        + " AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
                        + " AND DEPT.PARENT_EID = FACILITY.ENTITY_ID"
                        + " AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID"
                        + " AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID"
                        + " AND DIVISION.PARENT_EID = REGION.ENTITY_ID"
                        + " AND REGION.PARENT_EID = OPERATION.ENTITY_ID"
                        + " AND OPERATION.PARENT_EID = GENERIC_ORG.ENTITY_ID";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        flagORG = true;
                    }
                }
                //Parijat : Chubb Mail merge generic fields changes -end
				//CLIENT
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"CLIENTENT"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"CLIENTENT");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[CLIENT_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CLIENT_TEMP]"
						+" FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION,ENTITY DIVISION,"
						+" ENTITY REGION,ENTITY OPERATION,ENTITY COMPANY,ENTITY CLIENTENT "
						+" WHERE EVENT.EVENT_ID= " + p_lRecordID.ToString()
						+" AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
						+" AND DEPT.PARENT_EID = FACILITY.ENTITY_ID"
						+" AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID"
						+" AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID"
						+" AND DIVISION.PARENT_EID = REGION.ENTITY_ID"
						+" AND REGION.PARENT_EID = OPERATION.ENTITY_ID"
						+" AND OPERATION.PARENT_EID = COMPANY.ENTITY_ID"
						+" AND COMPANY.PARENT_EID = CLIENTENT.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
                //Parijat : Chubb Mail merge generic fields changes***********GENERIC CLIENT*********
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "GENERIC_ORG") && claimID != -1)
                {
                    if (AckCheckForOrgLevel(p_lRecordID, claimID, "CLIENT"))
                    {
                        iIdx = Generic.IndexOf(p_arrlstDisplayTables, "GENERIC_ORG");

                        if (flagORG)
                            //sSQL = "INSERT [INTO GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString();
                            sSQL = "INSERT INTO [GENERIC_ORG_TEMP] SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID";
                        else
                        {
                            Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[GENERIC_ORG_TEMP]", "EVENT_ID");
                            sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + ", GENERIC_ORG.ENTITY_TABLE_ID [INTO GENERIC_ORG_TEMP]";
                        }
                        sSQL += " FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION,ENTITY DIVISION,"
                        + " ENTITY REGION,ENTITY OPERATION,ENTITY COMPANY,ENTITY GENERIC_ORG "
                        + " WHERE EVENT.EVENT_ID= " + p_lRecordID.ToString()
                        + " AND EVENT.DEPT_EID = DEPT.ENTITY_ID"
                        + " AND DEPT.PARENT_EID = FACILITY.ENTITY_ID"
                        + " AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID"
                        + " AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID"
                        + " AND DIVISION.PARENT_EID = REGION.ENTITY_ID"
                        + " AND REGION.PARENT_EID = OPERATION.ENTITY_ID"
                        + " AND OPERATION.PARENT_EID = COMPANY.ENTITY_ID"
                        + " AND COMPANY.PARENT_EID = GENERIC_ORG.ENTITY_ID";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        flagORG = true;
                    }
                }
                //Parijat : Chubb Mail merge generic fields changes -end
				//DEPT_INV
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"DEPT_INV"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"DEPT_INV");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[DEPT_INV_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID,"  +p_arrlstDisplayFields[iIdx].ToString() + " [INTO DEPT_INV_TEMP]"
						+" FROM EVENT,ENTITY DEPT_INV WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
						+ " AND EVENT.DEPT_INVOLVED_EID = DEPT_INV.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
				//FACILITY_INV
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"FACILITY_INV"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"FACILITY_INV");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[FAC_INV_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID,"  +p_arrlstDisplayFields[iIdx].ToString() + " [INTO FAC_INV_TEMP]"
						+" FROM EVENT,ENTITY DEPT_INV,ENTITY FACILITY_INV WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
						+ " AND EVENT.DEPT_INVOLVED_EID = DEPT_INV.ENTITY_ID"
						+" AND DEPT_INV.PARENT_EID = FACILITY_INV.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
				//LOCATION_INV
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"LOCATION_INV"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"LOCATION_INV");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[LOC_INV_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID,"  +p_arrlstDisplayFields[iIdx].ToString() + " [INTO LOC_INV_TEMP]"
						+" FROM EVENT,ENTITY DEPT_INV,ENTITY FACILITY_INV "
						+",ENTITY LOCATION_INV "
						+" WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
						+ " AND EVENT.DEPT_INVOLVED_EID = DEPT_INV.ENTITY_ID"
						+" AND DEPT_INV.PARENT_EID = FACILITY_INV.ENTITY_ID"
						+" AND FACILITY_INV.PARENT_EID = LOCATION_INV.ENTITY_ID";
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
				//DIVISION_INV
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"DIVISION_INV"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"DIVISION_INV");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[DIV_INV_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID,"  +p_arrlstDisplayFields[iIdx].ToString() + " [INTO DIV_INV_TEMP]"
						+" FROM EVENT,ENTITY DEPT_INV,ENTITY FACILITY_INV "
						+",ENTITY LOCATION_INV,ENTITY DIVISION_INV "
						+" WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
						+ " AND EVENT.DEPT_INVOLVED_EID = DEPT_INV.ENTITY_ID"
						+" AND DEPT_INV.PARENT_EID = FACILITY_INV.ENTITY_ID"
						+" AND FACILITY_INV.PARENT_EID = LOCATION_INV.ENTITY_ID"
						+" AND LOCATION_INV.PARENT_EID = DIVISION_INV.ENTITY_ID";

					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
				//REGION_INV
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"REGION_INV"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"REGION_INV");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[REG_INV_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID,"  +p_arrlstDisplayFields[iIdx].ToString() + " [INTO REG_INV_TEMP]"
						+" FROM EVENT,ENTITY DEPT_INV,ENTITY FACILITY_INV "
						+",ENTITY LOCATION_INV,ENTITY DIVISION_INV,ENTITY REGION_INV "
						+" WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
						+ " AND EVENT.DEPT_INVOLVED_EID = DEPT_INV.ENTITY_ID"
						+" AND DEPT_INV.PARENT_EID = FACILITY_INV.ENTITY_ID"
						+" AND FACILITY_INV.PARENT_EID = LOCATION_INV.ENTITY_ID"
						+" AND LOCATION_INV.PARENT_EID = DIVISION_INV.ENTITY_ID"
						+" AND DIVISION_INV.PARENT_EID = REGION_INV.ENTITY_ID";

					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
				//OPERATION_INV
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"OPERATION_INV"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"OPERATION_INV");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[OP_INV_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID,"  +p_arrlstDisplayFields[iIdx].ToString() + " [INTO OP_INV_TEMP]"
						+" FROM EVENT,ENTITY DEPT_INV,ENTITY FACILITY_INV "
						+",ENTITY LOCATION_INV,ENTITY DIVISION_INV,ENTITY REGION_INV "
						+",ENTITY OPERATION_INV"
						+" WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
						+ " AND EVENT.DEPT_INVOLVED_EID = DEPT_INV.ENTITY_ID"
						+" AND DEPT_INV.PARENT_EID = FACILITY_INV.ENTITY_ID"
						+" AND FACILITY_INV.PARENT_EID = LOCATION_INV.ENTITY_ID"
						+" AND LOCATION_INV.PARENT_EID = DIVISION_INV.ENTITY_ID"
						+" AND DIVISION_INV.PARENT_EID = REGION_INV.ENTITY_ID"
						+" AND REGION_INV.PARENT_EID = OPERATION_INV.ENTITY_ID";

					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
				//COMPANY_INV
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"COMPANY_INV"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"COMPANY_INV");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[COMP_INV_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID,"  +p_arrlstDisplayFields[iIdx].ToString() + " [INTO COMP_INV_TEMP]"
						+" FROM EVENT,ENTITY DEPT_INV,ENTITY FACILITY_INV "
						+",ENTITY LOCATION_INV,ENTITY DIVISION_INV,ENTITY REGION_INV "
						+",ENTITY OPERATION_INV,ENTITY COMPANY_INV "
						+" WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
						+ " AND EVENT.DEPT_INVOLVED_EID = DEPT_INV.ENTITY_ID"
						+" AND DEPT_INV.PARENT_EID = FACILITY_INV.ENTITY_ID"
						+" AND FACILITY_INV.PARENT_EID = LOCATION_INV.ENTITY_ID"
						+" AND LOCATION_INV.PARENT_EID = DIVISION_INV.ENTITY_ID"
						+" AND DIVISION_INV.PARENT_EID = REGION_INV.ENTITY_ID"
						+" AND REGION_INV.PARENT_EID = OPERATION_INV.ENTITY_ID"
						+" AND OPERATION_INV.PARENT_EID = COMPANY_INV.ENTITY_ID";

					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
				//CLIENT_INV
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"CLIENT_INV"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"CLIENT_INV");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[CLIENT_INV_TEMP]", "EVENT_ID");
					sSQL = "SELECT EVENT.EVENT_ID,"  +p_arrlstDisplayFields[iIdx].ToString() + " [INTO CLIENT_INV_TEMP]"
						+" FROM EVENT,ENTITY DEPT_INV,ENTITY FACILITY_INV "
						+",ENTITY LOCATION_INV,ENTITY DIVISION_INV,ENTITY REGION_INV "
						+",ENTITY OPERATION_INV,ENTITY COMPANY_INV,ENTITY CLIENT_INV "
						+" WHERE EVENT.EVENT_ID = " + p_lRecordID.ToString()
						+ " AND EVENT.DEPT_INVOLVED_EID = DEPT_INV.ENTITY_ID"
						+" AND DEPT_INV.PARENT_EID = FACILITY_INV.ENTITY_ID"
						+" AND FACILITY_INV.PARENT_EID = LOCATION_INV.ENTITY_ID"
						+" AND LOCATION_INV.PARENT_EID = DIVISION_INV.ENTITY_ID"
						+" AND DIVISION_INV.PARENT_EID = REGION_INV.ENTITY_ID"
						+" AND REGION_INV.PARENT_EID = OPERATION_INV.ENTITY_ID"
						+" AND OPERATION_INV.PARENT_EID = COMPANY_INV.ENTITY_ID"
						+" AND COMPANY_INV.PARENT_EID = CLIENT_INV.ENTITY_ID";

					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
				}
				//DEPT EMPLOYEE
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"DEPT_EMP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"DEPT_EMP");
					//Mjain8 corrected 8/22/06 MITS 7517:p_sTmpParent replaced by [PI_TEMP]
                    if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_TEMP"))
                    {
                        Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[DEPT_EMP_TEMP]", "PI_ROW_ID");
                    }
                    else
                    {
                        p_sFrom = p_sFrom + "," + "[DEPT_EMP_TEMP]";
                    }
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," +p_arrlstDisplayFields[iIdx].ToString() + " [INTO DEPT_EMP_TEMP]"
						+" FROM PERSON_INVOLVED,ENTITY DEPT_EMP WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString()
						+" AND PERSON_INVOLVED.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
                        + " AND PI_TYPE_CODE NOT IN (" + Generic.GetCodeIDWithShort("P", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("PHYS", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId) + ")";
						

					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[DEPT_EMP_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[DEPT_EMP_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [DEPT_EMP_TEMP](PI_ROW_ID" + sBitFields + ")" 
							+" SELECT PI_ROW_ID" + sBitValues + " FROM [PI_TEMP]" ;
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}

				}
				//FACILITY EMPLOYEE
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"FACILITY_EMP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"FACILITY_EMP");
					if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_TEMP"))
					{
						Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[FAC_EMP_TEMP]", "PI_ROW_ID");
					}
					else
					{
					 p_sFrom=p_sFrom+","+"[FAC_EMP_TEMP]";
					}
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," +p_arrlstDisplayFields[iIdx].ToString() + " [INTO FAC_EMP_TEMP]"
						+" FROM PERSON_INVOLVED,ENTITY DEPT_EMP,ENTITY FACILITY_EMP WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString()
						+" AND PERSON_INVOLVED.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
						+" AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
                        + " AND PI_TYPE_CODE NOT IN (" + Generic.GetCodeIDWithShort("P", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("PHYS", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId) + ")";
						

					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[FAC_EMP_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[FAC_EMP_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [FAC_EMP_TEMP](PI_ROW_ID" + sBitFields + ")" 
							+" SELECT PI_ROW_ID" + sBitValues + " FROM [PI_TEMP]";
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}

				}
				//LOCATION EMPLOYEE
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"LOCATION_EMP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"LOCATION_EMP");
					if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_TEMP"))
					{
						Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[LOC_EMP_TEMP]", "PI_ROW_ID");
					}
					else
					{
						p_sFrom=p_sFrom+","+"[LOC_EMP_TEMP]";
					}
					
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," +p_arrlstDisplayFields[iIdx].ToString() + " [INTO LOC_EMP_TEMP]"
						+" FROM PERSON_INVOLVED,ENTITY DEPT_EMP,ENTITY FACILITY_EMP "
						+",ENTITY LOCATION_EMP "
						+" WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString()
						+" AND PERSON_INVOLVED.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
						+" AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
						+" AND FACILITY_EMP.PARENT_EID = LOCATION_EMP.ENTITY_ID"
                        + " AND PI_TYPE_CODE NOT IN (" + Generic.GetCodeIDWithShort("P", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("PHYS", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId) + ")";
						
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[LOC_EMP_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[LOC_EMP_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [LOC_EMP_TEMP](PI_ROW_ID" + sBitFields + ")" 
							+" SELECT PI_ROW_ID" + sBitValues + " FROM [PI_TEMP]" ;

						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}

				}
				//DIVISION EMPLOYEE
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"DIVISION_EMP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"DIVISION_EMP");
					if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_TEMP"))
					{
						Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[DIV_EMP_TEMP]", "PI_ROW_ID");
					}
					else
					{
						p_sFrom=p_sFrom+","+"[DIV_EMP_TEMP]";
					}
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," +p_arrlstDisplayFields[iIdx].ToString() + " [INTO DIV_EMP_TEMP]"
						+" FROM PERSON_INVOLVED,ENTITY DEPT_EMP,ENTITY FACILITY_EMP "
						+",ENTITY LOCATION_EMP,ENTITY DIVISION_EMP "
						+" WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString()
						+" AND PERSON_INVOLVED.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
						+" AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
						+" AND FACILITY_EMP.PARENT_EID = LOCATION_EMP.ENTITY_ID"
						+" AND LOCATION_EMP.PARENT_EID = DIVISION_EMP.ENTITY_ID"
                        + " AND PI_TYPE_CODE NOT IN (" + Generic.GetCodeIDWithShort("P", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("PHYS", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId) + ")";
						

					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[DIV_EMP_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[DIV_EMP_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [DIV_EMP_TEMP](PI_ROW_ID" + sBitFields + ")" 
							+" SELECT PI_ROW_ID" + sBitValues + " FROM [PI_TEMP]" ;
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}

				}
				//REGION EMPLOYEE
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"REGION_EMP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"REGION_EMP");
					if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_TEMP"))
					{
						Generic.ChainIt(ref p_sFrom, ref p_sWhere,"[PI_TEMP]", "[REG_EMP_TEMP]", "PI_ROW_ID");
					}
					else
					{
						p_sFrom=p_sFrom+","+"[REG_EMP_TEMP]";
					}
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," +p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO REG_EMP_TEMP]"
						+" FROM PERSON_INVOLVED,ENTITY DEPT_EMP,ENTITY FACILITY_EMP "
						+",ENTITY LOCATION_EMP,ENTITY DIVISION_EMP,ENTITY REGION_EMP "
						+" WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString()
						+" AND PERSON_INVOLVED.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
						+" AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
						+" AND FACILITY_EMP.PARENT_EID = LOCATION_EMP.ENTITY_ID"
						+" AND LOCATION_EMP.PARENT_EID = DIVISION_EMP.ENTITY_ID"
						+" AND DIVISION_EMP.PARENT_EID = REGION_EMP.ENTITY_ID"
                        + " AND PI_TYPE_CODE NOT IN (" + Generic.GetCodeIDWithShort("P", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("PHYS", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId) + ")";
						

					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[REG_EMP_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[REG_EMP_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [REG_EMP_TEMP](PI_ROW_ID" + sBitFields + ")" 
							+" SELECT PI_ROW_ID" + sBitValues + " FROM [PI_TEMP]";
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}

				}
				//OPERATION_EMP
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"OPERATION_EMP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"OPERATION_EMP");
					if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_TEMP"))
					{
						Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[OP_EMP_TEMP]", "PI_ROW_ID");
					}
					else
					{
						p_sFrom=p_sFrom+","+"[OP_EMP_TEMP]";
					}
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," +p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO OP_EMP_TEMP]"
						+" FROM PERSON_INVOLVED,ENTITY DEPT_EMP,ENTITY FACILITY_EMP "
						+",ENTITY LOCATION_EMP,ENTITY DIVISION_EMP,ENTITY REGION_EMP "
						+",ENTITY OPERATION_EMP "
						+" WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString()
						+" AND PERSON_INVOLVED.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
						+" AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
						+" AND FACILITY_EMP.PARENT_EID = LOCATION_EMP.ENTITY_ID"
						+" AND LOCATION_EMP.PARENT_EID = DIVISION_EMP.ENTITY_ID"
						+" AND DIVISION_EMP.PARENT_EID = REGION_EMP.ENTITY_ID"
						+" AND REGION_EMP.PARENT_EID = OPERATION_EMP.ENTITY_ID"
                        + " AND PI_TYPE_CODE NOT IN (" + Generic.GetCodeIDWithShort("P", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("PHYS", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId) + ")";
						

					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[OP_EMP_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[OP_EMP_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [OP_EMP_TEMP](PI_ROW_ID" + sBitFields + ")" 
							+" SELECT PI_ROW_ID" + sBitValues + " FROM [PI_TEMP]";
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}

				}
				//COMPANY_EMP
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"COMPANY_EMP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"COMPANY_EMP");
					if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_TEMP"))
					{
						Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[COMP_EMP_TEMP]", "PI_ROW_ID");
					}
					else
					{
						p_sFrom=p_sFrom+","+"[COMP_EMP_TEMP]";
					}
					
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," +p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO COMP_EMP_TEMP]"
						+" FROM PERSON_INVOLVED,ENTITY DEPT_EMP,ENTITY FACILITY_EMP "
						+",ENTITY LOCATION_EMP,ENTITY DIVISION_EMP,ENTITY REGION_EMP "
						+",ENTITY OPERATION_EMP,ENTITY COMPANY_EMP "
						+" WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString()
						+" AND PERSON_INVOLVED.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
						+" AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
						+" AND FACILITY_EMP.PARENT_EID = LOCATION_EMP.ENTITY_ID"
						+" AND LOCATION_EMP.PARENT_EID = DIVISION_EMP.ENTITY_ID"
						+" AND DIVISION_EMP.PARENT_EID = REGION_EMP.ENTITY_ID"
						+" AND REGION_EMP.PARENT_EID = OPERATION_EMP.ENTITY_ID"
						+" AND OPERATION_EMP.PARENT_EID = COMPANY_EMP.ENTITY_ID"
                        + " AND PI_TYPE_CODE NOT IN (" + Generic.GetCodeIDWithShort("P", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("PHYS", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId) + ")";
						

					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[COMP_EMP_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[COMP_EMP_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [COMP_EMP_TEMP](PI_ROW_ID" + sBitFields + ")" 
							+" SELECT PI_ROW_ID" + sBitValues + " FROM [PI_TEMP]" ;
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}

				}
				//CLIENT_EMP
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,"CLIENT_EMP"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"CLIENT_EMP");
					if (Utilities.ExistsInArray(p_arrlstDisplayTables,"PI_TEMP"))
					{
						Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PI_TEMP]", "[CLIENT_EMP_TEMP]", "PI_ROW_ID");
					}
					else
					{
						p_sFrom=p_sFrom+","+"[CLIENT_EMP_TEMP]";
					}
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," +p_arrlstDisplayFields[iIdx].ToString() 
						+ " [INTO CLIENT_EMP_TEMP]"
						+" FROM PERSON_INVOLVED,ENTITY DEPT_EMP,ENTITY FACILITY_EMP "
						+",ENTITY LOCATION_EMP,ENTITY DIVISION_EMP,ENTITY REGION_EMP "
						+",ENTITY OPERATION_EMP,ENTITY COMPANY_EMP,ENTITY CLIENT_EMP "
						+" WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString()
						+" AND PERSON_INVOLVED.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
						+" AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
						+" AND FACILITY_EMP.PARENT_EID = LOCATION_EMP.ENTITY_ID"
						+" AND LOCATION_EMP.PARENT_EID = DIVISION_EMP.ENTITY_ID"
						+" AND DIVISION_EMP.PARENT_EID = REGION_EMP.ENTITY_ID"
						+" AND REGION_EMP.PARENT_EID = OPERATION_EMP.ENTITY_ID"
						+" AND OPERATION_EMP.PARENT_EID = COMPANY_EMP.ENTITY_ID"
						+" AND COMPANY_EMP.PARENT_EID = CLIENT_EMP.ENTITY_ID"
                        + " AND PI_TYPE_CODE NOT IN (" + Generic.GetCodeIDWithShort("P", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("PHYS", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId)
                        + "," + Generic.GetCodeIDWithShort("MED", objCache.GetTableId("PERSON_INV_TYPE"), m_sConnectionString, m_iClientId) + ")";
						
					
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
					if (m_objGeneric.GetMergeCount("[CLIENT_EMP_TEMP]")==0)
					{
						m_objGeneric.FindBitFields("[CLIENT_EMP_TEMP]", ref sBitFields, ref sBitValues);
						sSQL = "INSERT INTO [CLIENT_EMP_TEMP](PI_ROW_ID" + sBitFields + ")" 
							+" SELECT PI_ROW_ID" + sBitValues + " FROM [PI_TEMP]" ;
						m_objMergeCmd.CommandType=CommandType.Text;
						m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
						m_objMergeCmd.ExecuteNonQuery();
					}

				}
				//QUEST/Win Patient
				PIOrgHQuery("PAT_", 8, "PATIENT", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PAT_", 7, "PATIENT", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PAT_", 6, "PATIENT", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PAT_", 5, "PATIENT", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PAT_", 4, "PATIENT", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PAT_", 3, "PATIENT", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PAT_", 2, "PATIENT", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PAT_", 1, "PATIENT", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				//QUEST/Win Physician
				PIOrgHQuery("PHYS_", 8, "PHYSICIAN", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PHYS_", 7, "PHYSICIAN", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PHYS_", 6, "PHYSICIAN", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PHYS_", 5, "PHYSICIAN", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PHYS_", 4, "PHYSICIAN", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PHYS_", 3, "PHYSICIAN", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PHYS_", 2, "PHYSICIAN", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("PHYS_", 1, "PHYSICIAN", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				//QUEST/Win Medical
				PIOrgHQuery("MED_", 8, "MED_STAFF", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("MED_", 7, "MED_STAFF", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("MED_", 6, "MED_STAFF", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("MED_", 5, "MED_STAFF", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("MED_", 4, "MED_STAFF", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("MED_", 3, "MED_STAFF", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("MED_", 2, "MED_STAFF", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				PIOrgHQuery("MED_", 1, "MED_STAFF", p_arrlstDisplayTables, p_arrlstDisplayFields
					, ref p_sFrom, ref p_sWhere,  p_lRecordID);
				
                //MGaba2- 07/04/2008 :Added Contact information of department in Claim Merge
                //MITS 12749:Start
                if (Utilities.ExistsInArray(p_arrlstDisplayTables,"ENT_X_CONTACTINFO"))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,"ENT_X_CONTACTINFO");
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, p_sTmpParent, "[DEPT_CONTACT_TEMP]", "EVENT_ID");
                    sSQL = "SELECT EVENT.EVENT_ID, " + p_arrlstDisplayFields[iIdx].ToString()
                        + " [INTO DEPT_CONTACT_TEMP]"
                    + " FROM ENT_X_CONTACTINFO,EVENT WHERE ENT_X_CONTACTINFO.ENTITY_ID = EVENT.DEPT_EID AND EVENT.EVENT_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[DEPT_CONTACT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[DEPT_CONTACT_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [DEPT_CONTACT_TEMP](EVENT_ID" + sBitFields + ")"
                            + "SELECT EVENT_ID" + sBitValues + " FROM " + p_sTmpParent;
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //MITS 12749:End

			}
			catch (RMAppException p_objException)
			{
				if (objTran!=null)
				{
					//objTran.Rollback();
				}
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				if (objTran!=null)
				{
					objTran.Rollback();
				}
                throw new RMAppException(Globalization.GetString("Merge.ConnectOrgH.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			finally
			{
				if (objCache!=null)
				{
					objCache.Dispose();
					objCache=null;
				}
				sSQL=null; 
				sBitFields=null; 
				sBitValues=null;
				if (objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn!=null)
				{
					objConn.Close();
					objConn.Dispose();
				}
				objCommand=null;
                if (objTran != null)
                {
                    objTran.Dispose();
                }
			}


		}
		/// Name		: PIOrgHQuery
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function gets data related to organizational hierarchy 
		/// </summary>
		/// <param name="p_sPrefix">Prefix to be added to temp table</param>
		/// <param name="p_iLevel">Level id</param>
		/// <param name="p_sChild">Child Table name</param>
		/// <param name="p_arrlstDisplayTables">Table names from which to get the merge data</param>
		/// <param name="p_arrlstDisplayFields">Field names from which to get the merge data</param>
		/// <param name="p_sFrom">From clause in sql query</param>
		/// <param name="p_sWhere">Where clause in sql query</param>
		/// <param name="p_lRecordID">Record id for which we need to get the data</param>
		private void PIOrgHQuery(string p_sPrefix,int p_iLevel,string p_sChild,ArrayList p_arrlstDisplayTables,ArrayList p_arrlstDisplayFields,
			ref string p_sFrom,ref string p_sWhere,long p_lRecordID)
		{
			string sTableName=""; 
			string sPICrit1=""; 
			string sPICrit2=""; 
			string sDeptField=""; 
			string sTempTable=""; 
			int iIdx=0; 
			string sSQL=""; 
			string sBitFields=""; 
			string sBitValues="";
			DbConnection objConn=null;
			DbReader objReader=null;
			try
			{
				//DYNAMIC QUERY WILL BE CREATED TO CREATE TEMP TABLES
				switch(p_sChild)
				{
					case "PATIENT":
						sPICrit1 = "PATIENT_ID";
						sPICrit2 = "PATIENT_ID";
						sDeptField = "FACILITY_DEPT_EID";
						sTempTable = "[PAT_TEMP]";
						break;
					case "PHYSICIAN":
						sPICrit1 = "PI_EID";
						sPICrit2 = "PHYS_EID";
						sDeptField = "DEPT_ASSIGNED_EID";
						sTempTable = "[QPHYS_TEMP]";
						break;
					case "MED_STAFF":
						sPICrit1 = "PI_EID";
						sPICrit2 = "STAFF_EID";
						sDeptField = "DEPT_ASSIGNED_EID";
						sTempTable = "[MED_TEMP]";
						break;
				}
				if (p_iLevel == 1) 
				{ 
					sTableName = p_sPrefix + "CLNT"; 
				} 
				else if (p_iLevel == 2) 
				{ 
					sTableName = p_sPrefix + "COMP"; 
				} 
				else if (p_iLevel == 3) 
				{ 
					sTableName = p_sPrefix + "OP"; 
				} 
				else if (p_iLevel == 4) 
				{ 
					sTableName = p_sPrefix + "REG"; 
				} 
				else if (p_iLevel == 5) 
				{ 
					sTableName = p_sPrefix + "DIV"; 
				} 
				else if (p_iLevel == 6) 
				{ 
					sTableName = p_sPrefix + "LOC"; 
				} 
				else if (p_iLevel == 7) 
				{ 
					sTableName = p_sPrefix + "FAC"; 
				} 
				else if (p_iLevel == 8) 
				{ 
					sTableName = p_sPrefix + "DEPT"; 
				}
				if (Utilities.ExistsInArray(p_arrlstDisplayTables,sTableName))
				{
					iIdx=Generic.IndexOf(p_arrlstDisplayTables,sTableName);
					Generic.ChainIt(ref p_sFrom, ref p_sWhere, sTempTable, "["+ sTableName + "_TEMP]", "PI_ROW_ID");
					sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID," +p_arrlstDisplayFields[iIdx].ToString() 
						+" [INTO "+ sTableName + "_TEMP]"
						+" FROM PERSON_INVOLVED," + p_sChild + ",ENTITY " + p_sPrefix + "DEPT";
					if (p_iLevel < 8) 
					{ 
						sSQL = sSQL + ",ENTITY " + p_sPrefix + "FAC"; 
					} 
					if (p_iLevel < 7) 
					{ 
						sSQL = sSQL + ",ENTITY " + p_sPrefix + "LOC"; 
					} 
					if (p_iLevel < 6) 
					{ 
						sSQL = sSQL + ",ENTITY " + p_sPrefix + "DIV"; 
					} 
					if (p_iLevel < 5) 
					{ 
						sSQL = sSQL + ",ENTITY " + p_sPrefix + "REG"; 
					} 
					if (p_iLevel < 4) 
					{ 
						sSQL = sSQL + ",ENTITY " + p_sPrefix + "OP"; 
					} 
					if (p_iLevel < 3) 
					{ 
						sSQL = sSQL + ",ENTITY " + p_sPrefix + "COMP"; 
					} 
					if (p_iLevel < 2) 
					{ 
						sSQL = sSQL + ",ENTITY " + p_sPrefix + "CLNT"; 
					}		
					sSQL = sSQL + " WHERE PERSON_INVOLVED.EVENT_ID = " + p_lRecordID.ToString()
						+" AND PERSON_INVOLVED." + sPICrit1 + " = " + p_sChild + "." + sPICrit2 + 
						" AND " + p_sChild + "." + sDeptField + " = " + p_sPrefix + "DEPT.ENTITY_ID";
					if (p_iLevel < 8) 
					{ 
						sSQL = sSQL + " AND " + p_sPrefix + "DEPT.PARENT_EID = " + p_sPrefix + "FAC.ENTITY_ID"; 
					} 
					if (p_iLevel < 7) 
					{ 
						sSQL = sSQL + " AND " + p_sPrefix + "FAC.PARENT_EID = " + p_sPrefix + "LOC.ENTITY_ID"; 
					} 
					if (p_iLevel < 6) 
					{ 
						sSQL = sSQL + " AND " + p_sPrefix + "LOC.PARENT_EID = " + p_sPrefix + "DIV.ENTITY_ID"; 
					} 
					if (p_iLevel < 5) 
					{ 
						sSQL = sSQL + " AND " + p_sPrefix + "DIV.PARENT_EID = " + p_sPrefix + "REG.ENTITY_ID"; 
					} 
					if (p_iLevel < 4) 
					{ 
						sSQL = sSQL + " AND " + p_sPrefix + "REG.PARENT_EID = " + p_sPrefix + "OP.ENTITY_ID"; 
					} 
					if (p_iLevel < 3) 
					{ 
						sSQL = sSQL + " AND " + p_sPrefix + "OP.PARENT_EID = " + p_sPrefix + "COMP.ENTITY_ID"; 
					} 
					if (p_iLevel < 2) 
					{ 
						sSQL = sSQL + " AND " + p_sPrefix + "COMP.PARENT_EID = " + p_sPrefix + "CLNT.ENTITY_ID"; 
					}
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
                    //Shruti for MITS 9625
                    if (m_objGeneric.GetMergeCount("[" + sTableName + "_TEMP]") == 0)
                    {
					m_objGeneric.FindBitFields("[" + sTableName + "_TEMP]", ref sBitFields, ref sBitValues);
					m_objGeneric.MergeDupTable (m_objMergeCmd, sTableName + "_TEMP");
					sSQL = "INSERT INTO [" + sTableName + "_TEMP](PI_ROW_ID" + sBitFields + ")"
						+" SELECT PI_ROW_ID" + sBitValues + " FROM "+sTempTable
						+ "   WHERE PI_ROW_ID NOT IN (SELECT PI_ROW_ID FROM [MRG_TMP])";
					m_objMergeCmd.CommandType=CommandType.Text;
					m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
					m_objMergeCmd.ExecuteNonQuery();
                    }
					m_objGeneric.MergeDupCleanup(m_objMergeCmd);
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Merge.PIOrgHQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			finally
			{
				sTableName=null; 
				sPICrit1=null; 
				sPICrit2=null; 
				sDeptField=null; 
				sTempTable=null; 
				sSQL=null;
				sBitFields=null;
				sBitValues=null;
				if (objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn!=null)
				{
					objConn.Close();
					objConn.Dispose();
				}
			}
		}
        /// <summary>
        /// This is used in case of OrgHierarchy mail merge Tables.Returns if the table starts with for ex. "MED_"
        /// </summary>
        /// <param name="p_arrlstList">List of tables to be displayed</param>
        /// <param name="p_sTableName">Name of the table</param>
        /// <returns></returns>
        private bool StartsWith(ArrayList p_arrlstList, string p_sTableName)
        {
            int iCnt = 0;
            for (iCnt = 0; iCnt < p_arrlstList.Count; iCnt++)
            {
                if (Convert.ToString(p_arrlstList[iCnt]).StartsWith(p_sTableName))
                {
                    return true;
                }
            }
            return false;
        }
        /// Name		: AnyQuestDriverTable
        /// Author		: Amandeep Kaur
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function returns true if any value in passed array exists in
        /// a staff table string
        /// </summary>
        /// <param name="p_arrlstList">Array</param>
        /// <returns>Returns true if any value in passed array exists in
        /// a staff table string else false</returns>
        private bool AnyQuestDriverTable(ArrayList p_arrlstList)
        {
            string sValues = ",DRIVER,DRIVER_ENTITY,DRIVER_SUPP,";               
            for (int i = 0; i < p_arrlstList.Count; i++)
            {
                if (sValues.IndexOf("," + p_arrlstList[i].ToString() + ",") != -1)
                {
                    return true;
                }
            }
            return false;
        }
		/// Name		: AnyQuestStaffTable
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns true if any value in passed array exists in
		/// a staff table string
		/// </summary>
		/// <param name="p_arrlstList">Array</param>
		/// <returns>Returns true if any value in passed array exists in
		/// a staff table string else false</returns>
		private bool AnyQuestStaffTable(ArrayList p_arrlstList)
		{
			string sValues=",MED_STAFF,MED_STAFF_ENTITY,MED_STAFF_SUPP," 
				+"STAFF_PRIVS,STAFF_CERTS,";
			for (int i=0;i<p_arrlstList.Count;i++)
			{
				if (sValues.IndexOf(","+p_arrlstList[i].ToString()+",")!=-1)
				{
					return true;
				}
			}
			return false;
		}
		/// Name		: AnyQuestPatientTable
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns true if any value in passed array exists in
		/// a patient table string
		/// </summary>
		/// <param name="p_arrlstList">Array</param>
		/// <returns>Returns true if any value in passed array exists in
		/// a patient table string else false</returns>
		private bool AnyQuestPatientTable(ArrayList p_arrlstList)
		{
			string sValues=",PATIENT,PATIENT_ENTITY,PATIENT_SUPP"
				+",PATIENT_PRI_PHYS,PATIENT_PROCEDURE,PATIENT_DIAGNOSIS," 
				+"PATIENT_DRG_CODES,PATIENT_ACT_TAKEN,PATIENT_ATTN_PHYS,";
			for (int i=0;i<p_arrlstList.Count;i++)
			{
				if (sValues.IndexOf(","+p_arrlstList[i].ToString()+",")!=-1)
				{
					return true;
				}
			}
			return false;
		}
		/// Name		: AnyQuestPhysicianTable
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns true if any value in passed array exists in
		/// a physician table string
		/// </summary>
		/// <param name="p_arrlstList">Array</param>
		/// <returns>Returns true if any value in passed array exists in
		/// a physician table string else false</returns>
		private bool AnyQuestPhysicianTable(ArrayList p_arrlstList)
		{
			string sValues=",PHYSICIAN,PHYSICIAN_ENTITY,PHYS_SUPP,PHYS_PRIVS," +
				"PHYS_CERTS,PHYS_EDUCATION,PHYS_PREV_HOSP,PHYS_SUB_SPECIALTY,";
			for (int i=0;i<p_arrlstList.Count;i++)
			{
				if (sValues.IndexOf(","+p_arrlstList[i].ToString()+",")!=-1)
				{
					return true;
				}
			}
			return false;
		}
		#endregion
        /// Author		: Parijat Sharma
        ///************************************************************
        /// Chubb Mail merge generic fields changes
        ///************************************************************
        /// <summary>
        /// This Function Checks if the acknowledgement is checked for a particular organization level
        /// </summary>
        public bool AckCheckForOrgLevel(long event_ID, long claim_ID,string level)
        {
            DbConnection objConn = null;
            DbReader objReader = null;
            DbTransaction objTran = null;
            DbCommand objCommand = null;
            bool ackFlag = false;
            string chkLob = "";
            int ackFlagInteger =0;
            string sSQL = null;
            int iLOB = 0 ;
            bool returnFlag = false;

            m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
            m_objMergeConn.Open();
            m_objMergeCmd = m_objMergeConn.CreateCommand();

            sSQL = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + claim_ID.ToString();
            objReader = DbFactory.GetDbReader(m_sConnectionString, m_objGeneric.SafeSQL(sSQL));
            if (objReader.Read())
            {
                iLOB = Conversion.ConvertObjToInt(objReader.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
            }
            objReader.Close();
            sSQL = string.Empty;

            //Geeta : Mits 18718 for Acord Comma Seperated List Issue
            switch (level.ToUpper())
            {
                case "DEPARTMENT":
                    sSQL = @"SELECT EMAIL_ACK FROM ENT_X_CONTACTINFO,CONTACT_LOB WHERE ENTITY_ID IN(SELECT DEPT.ENTITY_ID FROM EVENT,ENTITY DEPT 
                    WHERE EVENT.EVENT_ID= " + event_ID.ToString() + @" AND EVENT.DEPT_EID = DEPT.ENTITY_ID)  AND EMAIL_ACK = -1
                                AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + iLOB.ToString();
                     
                    break;
                case "FACILITY":
                    sSQL = @"SELECT EMAIL_ACK FROM ENT_X_CONTACTINFO,CONTACT_LOB WHERE ENTITY_ID IN(SELECT FACILITY.ENTITY_ID FROM EVENT,ENTITY DEPT,ENTITY FACILITY 
                            WHERE EVENT.EVENT_ID= " +event_ID.ToString()+ @" AND EVENT.DEPT_EID = DEPT.ENTITY_ID AND 
                            DEPT.PARENT_EID = FACILITY.ENTITY_ID) AND EMAIL_ACK = -1
                                AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + iLOB.ToString();
                    break;
                case "LOCATION":
                    sSQL = @"SELECT EMAIL_ACK FROM ENT_X_CONTACTINFO,CONTACT_LOB WHERE ENTITY_ID IN(SELECT LOCATION.ENTITY_ID FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION 
                             WHERE EVENT.EVENT_ID= " + event_ID.ToString() + @" AND EVENT.DEPT_EID = DEPT.ENTITY_ID AND 
                            DEPT.PARENT_EID = FACILITY.ENTITY_ID AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID) AND EMAIL_ACK = -1
                                 AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + iLOB.ToString();
                    break;
                case "DIVISION":
                    sSQL = @"SELECT EMAIL_ACK FROM ENT_X_CONTACTINFO,CONTACT_LOB WHERE ENTITY_ID IN(SELECT DIVISION.ENTITY_ID FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION,
                            ENTITY DIVISION WHERE EVENT.EVENT_ID= " + event_ID.ToString() + @" AND EVENT.DEPT_EID = DEPT.ENTITY_ID 
                            AND DEPT.PARENT_EID = FACILITY.ENTITY_ID AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID 
                            AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID) AND EMAIL_ACK = -1
                                 AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + iLOB.ToString();
                    break;
                case "REGION":
                    sSQL = @"SELECT EMAIL_ACK FROM ENT_X_CONTACTINFO,CONTACT_LOB WHERE ENTITY_ID IN(SELECT REGION.ENTITY_ID FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION
                            ,ENTITY DIVISION, ENTITY REGION WHERE EVENT.EVENT_ID= " + event_ID.ToString() + @" AND 
                            EVENT.DEPT_EID = DEPT.ENTITY_ID AND DEPT.PARENT_EID = FACILITY.ENTITY_ID AND 
                            FACILITY.PARENT_EID = LOCATION.ENTITY_ID AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID
                            AND DIVISION.PARENT_EID = REGION.ENTITY_ID) AND EMAIL_ACK = -1
                                 AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + iLOB.ToString();
                        break;
                case "OPERATION":
                        sSQL = @"SELECT EMAIL_ACK FROM ENT_X_CONTACTINFO,CONTACT_LOB WHERE ENTITY_ID IN(SELECT OPERATION.ENTITY_ID FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION,
                                ENTITY DIVISION,ENTITY REGION,ENTITY OPERATION WHERE EVENT.EVENT_ID= " + event_ID.ToString() + @" AND 
                                EVENT.DEPT_EID = DEPT.ENTITY_ID AND DEPT.PARENT_EID = FACILITY.ENTITY_ID AND 
                                FACILITY.PARENT_EID = LOCATION.ENTITY_ID AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID AND 
                                DIVISION.PARENT_EID = REGION.ENTITY_ID AND REGION.PARENT_EID = OPERATION.ENTITY_ID) AND EMAIL_ACK = -1
                                 AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + iLOB.ToString();
                        break;
                case "COMPANY":
                        sSQL = @"SELECT EMAIL_ACK FROM ENT_X_CONTACTINFO,CONTACT_LOB WHERE ENTITY_ID IN(SELECT COMPANY.ENTITY_ID FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION,
                                ENTITY DIVISION,ENTITY REGION,ENTITY OPERATION,ENTITY COMPANY 
                                WHERE EVENT.EVENT_ID= " + event_ID.ToString() + @" AND EVENT.DEPT_EID = DEPT.ENTITY_ID AND DEPT.PARENT_EID = FACILITY.ENTITY_ID
                                AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID
                                AND DIVISION.PARENT_EID = REGION.ENTITY_ID AND REGION.PARENT_EID = OPERATION.ENTITY_ID
                                AND OPERATION.PARENT_EID = COMPANY.ENTITY_ID) AND EMAIL_ACK = -1
                                 AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + iLOB.ToString();
                        break;
                default :
                        sSQL = @"SELECT EMAIL_ACK FROM ENT_X_CONTACTINFO,CONTACT_LOB WHERE ENTITY_ID IN(SELECT CLIENTENT.ENTITY_ID FROM EVENT,ENTITY DEPT,ENTITY FACILITY,ENTITY LOCATION,
                                ENTITY DIVISION, ENTITY REGION,ENTITY OPERATION,ENTITY COMPANY,ENTITY CLIENTENT 
                                WHERE EVENT.EVENT_ID= " + event_ID.ToString() + @" AND EVENT.DEPT_EID = DEPT.ENTITY_ID AND DEPT.PARENT_EID = FACILITY.ENTITY_ID AND 
                                FACILITY.PARENT_EID = LOCATION.ENTITY_ID AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID AND 
                                DIVISION.PARENT_EID = REGION.ENTITY_ID AND REGION.PARENT_EID = OPERATION.ENTITY_ID AND 
                                OPERATION.PARENT_EID = COMPANY.ENTITY_ID AND COMPANY.PARENT_EID = CLIENTENT.ENTITY_ID) AND EMAIL_ACK = -1
                                AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + iLOB.ToString();
                        break;
            }
            objReader = DbFactory.GetDbReader(m_sConnectionString, m_objGeneric.SafeSQL(sSQL));
            if (objReader.Read())
            {
                //ackFlagInteger = Conversion.ConvertObjToInt(objReader.GetValue("EMAIL_ACK"), m_iClientId);
                //chkLob = Conversion.ConvertObjToStr(objReader.GetValue("LINE_OF_BUS_CODE"));
                
                //if(ackFlagInteger ==-1)
                //    ackFlag = true;
                returnFlag = true;
            }
            objReader.Close();

           
            //if (chkLob.Contains(iLOB.ToString()) && ackFlag == true)
            //    returnFlag = true;

            return returnFlag;
        }
	}
}
