﻿
using System;

namespace Riskmaster.Application.MailMerge
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   29th November 2004
	///Purpose :   This class Creates, Edits, Fetches permission information.
	/// </summary>
	internal class TemplatePerm
	{
		#region Variable Declaration
		/// <summary>
		/// Form id
		/// </summary>
		private	long m_lFormId=0; 
		/// <summary>
		/// User id
		/// </summary>
		private	long m_lUserId=0; 
		/// <summary>
		/// Group Id
		/// </summary>
		private	long m_lGroupId=0; 
		/// <summary>
		/// Display name
		/// </summary>
		private string m_sDisplayName="";
		/// <summary>
		/// Group or not
		/// </summary>
		private bool m_bIsGroup=false;
        private int m_iClientId = 0;
		#endregion

		#region Property Declaration
		/// <summary>
		/// Group or not
		/// </summary>
		public bool IsGroup
		{
			get
			{
				return m_bIsGroup;
			}
			set
			{
				m_bIsGroup=value;
			}
		}
		/// <summary>
		/// Display name
		/// </summary>
		public string DisplayName
		{
			get
			{
				return m_sDisplayName;
			}
			set
			{
				m_sDisplayName=value;
			}
		}
		/// <summary>
		/// Form id
		/// </summary>
		public long FormId
		{
			get
			{
				return m_lFormId;
			}
			set
			{
				m_lFormId=value;
			}
		}
		/// <summary>
		/// User id
		/// </summary>
		public long UserId
		{
			get
			{
				return m_lUserId;
			}
			set
			{
				m_lUserId=value;
			}
		}
		/// <summary>
		/// Group id
		/// </summary>
		public long GroupId
		{
			get
			{
				return m_lGroupId;
			}
			set
			{
				m_lGroupId=value;
			}
		}
		#endregion

		#region  TemplatePerm class constructor
		/// <summary>
		/// TemplatePerm class constructor
		/// </summary>
        public TemplatePerm(int p_iClientId) { m_iClientId = p_iClientId; }
		#endregion
	}
}
