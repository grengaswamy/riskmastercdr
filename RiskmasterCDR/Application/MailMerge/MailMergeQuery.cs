﻿
/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/11/2014 | 34276  | achouhan3   | Add Entity ID Type and Entity ID Number in mail Merge  
 **********************************************************************************************/
using System;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.Text;
using System.IO;
using System.Xml;
using System.Threading;
using Riskmaster.Settings;
namespace Riskmaster.Application.MailMerge
{
    /////<summary>
    ///Author  :   Parag Sarin
    ///Dated   :   29th November 2004
    ///Purpose :   This class Fetches merge information.
    /// </summary>
    internal class MailMergeQuery : IDisposable
    {
        #region Variable Declaration
        private static string m_sDBType = "";
        private static string m_lUserId = "32";
        ///<summary>
        /// Connection string to the database
        /// </summary>
        private string m_sConnectionString = "";
        ///<summary>
        /// DbConnection object
        /// </summary>
        private DbConnection m_objMergeConn = null;
        ///<summary>
        /// DbCommand object 
        /// </summary>
        private DbCommand m_objMergeCmd = null;
        ///<summary>
        /// Database type ORACLE,SQL SERVER, etc.
        /// </summary>
        private long m_lDbMake = 0;
        ///<summary>
        /// Merged data
        /// </summary>
        private DataSet m_objMergeds = null;
        ///<summary>
        /// Generic class object 
        /// </summary>
        private Generic m_objGeneric = null;
        SysSettings objSettings = null;
        private string sConcatOperator = string.Empty;
        private StringBuilder sbSql = null;
        //dvatsa-cloud
        private int m_iClientId = 0;
        #endregion

        #region Constructor

        ///<summary>
        /// Overloaded Constructor of MailMergeQuery class 
        /// </summary>
        /// <param name="p_sConnectionString">Connection string</param>
        /// <param name="p_lDbMake">Database type</param>
        /// <param name="p_objManager">Manager object</param>
        internal MailMergeQuery(string p_sConnectionString, long p_lDbMake, IManager p_objManager, int p_iClientId, string p_sTablesNameUniquePart)//dvatsa-cloud   //tkatsarski: 03/13/15 - RMA-1768
        {
            m_iClientId = p_iClientId;//dvatsa-cloud
            m_sConnectionString = p_sConnectionString;
            m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
            m_objMergeConn.Open();
            m_lDbMake = (int)m_objMergeConn.DatabaseType;
            m_objMergeConn.Close();
            m_objGeneric = new Generic(m_iClientId);//rkaur27
            m_objGeneric.Setproperty(p_lDbMake, p_sConnectionString);
            m_objGeneric.Manager = p_objManager;
            m_objGeneric.TablesNameUniquePart = p_sTablesNameUniquePart;//tkatsarski: 03/13/15 - RMA-1768
            if (m_lDbMake == (int)eDatabaseType.DBMS_IS_ORACLE)
            {
                sConcatOperator = "||";
            }
            else
            {
                sConcatOperator = "+";
            }
            sbSql = new StringBuilder();
        }
        ///<summary>
        /// Overloaded Constructor of MailMergeQuery class 
        /// </summary>
        /// <param name="p_sConnectionString">Connection string</param>
        /// <param name="p_lDbMake">Database type</param>
        /// <param name="p_objManager">Manager object</param>
        internal MailMergeQuery(Generic p_objGeneric, int p_iClientId)//rkaur27
        {
            m_objGeneric = p_objGeneric;
            m_iClientId = p_iClientId;//rkaur27        
        }
        internal MailMergeQuery() { }
        #endregion

        #region Destructor
        /// <summary>
        /// Destructor
        /// </summary>
        ~MailMergeQuery()
        {
            Dispose();
        }
        #endregion

        public void Dispose()
        {
            if (m_objMergeConn != null)
            {
                m_objMergeConn.Close();
                m_objMergeConn.Dispose();
            }
            if (m_objMergeds != null)
            {
                m_objMergeds.Dispose();
            }
            m_objMergeCmd = null;
            m_sConnectionString = null;
            if (sbSql != null)
            {
                sbSql = null;
            }
        }

        #region Property Declaration
        /// <summary>
        /// DBCommand object
        /// </summary>
        public DbCommand Command
        {
            set
            {
                m_objMergeCmd = value;
            }
            get
            {
                return m_objMergeCmd;
            }
        }

        //tkatsarski: 03/13/15 - RMA-1768: The property is added, so we can stop using the static Merge.Data
        public DataSet Data
        {
            set
            {
                m_objMergeds = value;
            }
            get
            {
                return m_objMergeds;
            }
        }
        #endregion

        #region Functions to delete temporary tables
        //Shruti Leave Plan Merge starts
        /// Name		: CleanupLeavePlanMerge
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function deletes temporary tables related to leave plan
        /// </summary>
        internal void CleanupLeavePlanMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[LV_PLAN_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[LV_INSURED_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[LVPLANSUPP_TEMP]");
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupLeavePlanMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        //Shruti Leave Plan Merge starts

        //Shruti Policy Billing Merge starts
        /// Name		: CleanupPolicyBillMerge
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function deletes temporary tables related to policy billing
        /// </summary>
        internal void CleanupPolicyBillMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[POLICY_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[TERM_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[TRANS_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[BILL_X_BILL_ITEM_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[BILL_X_RECEIPT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[BILL_X_DSBRSMNT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[BILL_X_ADJSTMNT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[BILL_X_PREM_ITEM_TEMP]");
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupPolicyBillMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        //Shruti Leave Plan Merge starts

        /// Name		: CleanupDisPlanMerge
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function deletes temporary tables related to claim
        /// </summary>
        internal void CleanupDisPlanMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[PLAN_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLASS_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[TD_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[INSURED_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PLANSUPP_TEMP]");
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupDisPlanMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }

        /// Name		: CleanupClaimMerge
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function deletes temporary tables related to claim
        /// </summary>
        internal void CleanupClaimMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLAIM_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLAIMANT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLMNT_ATT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[UNIT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[VEH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[VEH_DEPT_TEMP]");
                //spahariya MITS 29464
                m_objGeneric.DropMTable(m_objMergeCmd, "[VEH_OWN_TEMP]");
                //spahariya MITS 29465
                m_objGeneric.DropMTable(m_objMergeCmd, "[POL_AGENCY_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[LIT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[LIT_ATT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[LIT_FIRM_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[EXPERT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[ADJ_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[ADJ_TEXT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[DEF_ATT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[FUNDS_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAYEE_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLAIM_SUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MCO_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[LIT_SUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[DEFENDANT_SUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLAIM_ADJ_SUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[LITIGATION_ENTITY_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLAIMANT_SUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLMRES_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLMPAY_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CM_CASE_MGT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[TPLN_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MEDMGT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[ACCOMM_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[VOCREHAB_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CMGRHIST_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[TPLN_ENT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[TPLN_AB_ENT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[TPLN_RB_ENT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MEDMGT_PG_ENT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MEDMGT_CM_ENT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[VR_PG_ENT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CMH_CM_ENT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CM_CMGR_RT_ENTITY]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CMGR_NOTES_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[UNITSUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLAIM_LV_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLAIM_LVD_TEMP]");
                //gagnihotri 10/07/2009 MITS 17813 Enhanced Policy related temp tables
                m_objGeneric.DropMTable(m_objMergeCmd, "[POLICY_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[TRANS_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[TERM_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[INS_ENT_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CVG_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[RTNG_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[DCNT_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[DTIER_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[EXP_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[INSURED_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MCO_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[POLSUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[POLSUPPENH_TEMP]");
                //MGaba2- 07/04/2008 : MITS 12749 Added Contact information of department in Claim Merge-Start
                m_objGeneric.DropMTable(m_objMergeCmd, "[DEPT_CONTACT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CUR_ADJ_TEMP]");//Parijat : Chubb Mail merge generic fields changes 
                m_objGeneric.DropMTable(m_objMergeCmd, "[GENERIC_ORG_TEMP]");//Parijat : Chubb Mail merge generic fields changes 
                //MGaba2:MITS 12749 :End
                // Start: Ayush : Property Claim merge 12/16/2009, MITS 18291
                m_objGeneric.DropMTable(m_objMergeCmd, "[PROPERTY_TEMP]");
                //asharma326 MITS 29423
                m_objGeneric.DropMTable(m_objMergeCmd, "[PROPERTY_TEMP_LOSS]");
                //asharma326
                m_objGeneric.DropMTable(m_objMergeCmd, "[PSCHED_TEMP]");
                // End: Ayush : Property Claim merge 12/16/2009, MITS 18291.
                //skhare7 subrogation R8 enhancement
                m_objGeneric.DropMTable(m_objMergeCmd, "[SUB_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[SUB_SPE_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[SUB_ADP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[SUB_COMP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[SUB_ADJ_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[SUB_SUPP_TEMP]");
                //Liability Info
                m_objGeneric.DropMTable(m_objMergeCmd, "[LIAB_SUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[LIAB_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[LAIB_OWN_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[LIAB_PARTY_TEMP]");
                //Property Loss Info
                m_objGeneric.DropMTable(m_objMergeCmd, "[PROPLOSS_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PROPLOSS_SUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PROP_OWNR_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PROP_PHOLD_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[SALVAGEP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[SALVAGEU_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[UNIT_SALB_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PROP_SALB_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLAIM_LV_SUPP_TEMP]"); //For Mits 30698 
                //mits 29452 asingh263
                m_objGeneric.DropMTable(m_objMergeCmd, "[SALVAGEIP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[SALVAGEY_TEMP]");
                //mits 29452 asingh263


                //skhare7 end
                CleanupEventMerge();
                // npadhy MITS 18935 Drop all the Temporary table for Policy Merge
                CleanupPolicyMerge();
                //BOB Enhancement Unit Stat
                m_objGeneric.DropMTable(m_objMergeCmd, "[UNIT_STAT_TEMP]");
                //mdhamija MITS 27692
                m_objGeneric.DropMTable(m_objMergeCmd, "[DISABILITY_PLAN_TEMP]");

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupClaimMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        /// Name		: CleanupEventMerge
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function deletes temporary tables related to event
        /// </summary>
        private void CleanupEventMerge()
        {
            try
            {
                m_objGeneric.DropMTable(Command, "[EVENT_TEMP]");
                m_objGeneric.DropMTable(Command, "[PI_TEMP]");
                m_objGeneric.DropMTable(Command, "[EVENT_SUPP_TEMP]");
                m_objGeneric.DropMTable(Command, "[RPTD_TEMP]");
                m_objGeneric.DropMTable(Command, "[CAT_TEMP]");
                m_objGeneric.DropMTable(Command, "[PISUPP_TEMP]");

                m_objGeneric.DropMTable(Command, "[BODY_PART_TEMP]");
                m_objGeneric.DropMTable(Command, "[INJURY_TEMP]");
                m_objGeneric.DropMTable(Command, "[DIAG_TEMP]");
                m_objGeneric.DropMTable(Command, "[PHYS_TEMP]");
                m_objGeneric.DropMTable(Command, "[HOSP_TEMP]");

                m_objGeneric.DropMTable(Command, "[DEPT_TEMP]");
                m_objGeneric.DropMTable(Command, "[FAC_TEMP]");
                m_objGeneric.DropMTable(Command, "[LOC_TEMP]");
                m_objGeneric.DropMTable(Command, "[DIV_TEMP]");
                m_objGeneric.DropMTable(Command, "[REG_TEMP]");
                m_objGeneric.DropMTable(Command, "[OP_TEMP]");
                m_objGeneric.DropMTable(Command, "[COMP_TEMP]");
                m_objGeneric.DropMTable(Command, "[CLIENT_TEMP]");

                m_objGeneric.DropMTable(Command, "[DEPT_INV_TEMP]");
                m_objGeneric.DropMTable(Command, "[FAC_INV_TEMP]");
                m_objGeneric.DropMTable(Command, "[LOC_INV_TEMP]");
                m_objGeneric.DropMTable(Command, "[DIV_INV_TEMP]");
                m_objGeneric.DropMTable(Command, "[REG_INV_TEMP]");
                m_objGeneric.DropMTable(Command, "[OP_INV_TEMP]");
                m_objGeneric.DropMTable(Command, "[COMP_INV_TEMP]");
                m_objGeneric.DropMTable(Command, "[CLIENT_INV_TEMP]");

                m_objGeneric.DropMTable(Command, "[DEPT_EMP_TEMP]");
                m_objGeneric.DropMTable(Command, "[FAC_EMP_TEMP]");
                m_objGeneric.DropMTable(Command, "[LOC_EMP_TEMP]");
                m_objGeneric.DropMTable(Command, "[DIV_EMP_TEMP]");
                m_objGeneric.DropMTable(Command, "[REG_EMP_TEMP]");
                m_objGeneric.DropMTable(Command, "[OP_EMP_TEMP]");
                m_objGeneric.DropMTable(Command, "[COMP_EMP_TEMP]");
                m_objGeneric.DropMTable(Command, "[CLIENT_EMP_TEMP]");
                m_objGeneric.DropMTable(Command, "[EVENTQM_TEMP]");
                m_objGeneric.DropMTable(Command, "[GENERIC_ORG_TEMP]");//Parijat : Chubb Mail merge generic fields changes 
                CleanupPatientMerge();
                CleanupPhysicianMerge();
                CleanupMedStaffMerge();
                CleanupDriverMerge();//Aman Driver Enh

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupEventMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }


        }
        /// Name		: CleanupPolicyMerge
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function deletes temporary tables related to policy
        /// </summary>
        internal void CleanupPolicyMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[POLICY_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[INS_ENT_TEMP]");
                // npadhy Start MITS 18935 Added the case for Reinsurer
                m_objGeneric.DropMTable(m_objMergeCmd, "[REINS_ENT_TEMP]");
                // npadhy Start MITS 18935 Added the case for Reinsurer
                m_objGeneric.DropMTable(m_objMergeCmd, "[CVG_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[INSURED_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MCO_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[POLSUPP_TEMP]");
                //Gagan Safeway Retrofit Policy Jursidiction : START
                m_objGeneric.DropMTable(m_objMergeCmd, "[STATE_TEMP]");
                //Gagan Safeway Retrofit Policy Jursidiction : END
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupPolicyMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        /// Name		: CleanupVehicleMerge
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function deletes temporary tables related to vehicle
        /// </summary>
        internal void CleanupVehicleMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[VEH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[VEHSUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[DEPT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[FAC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[LOC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[DIV_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[REG_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[OP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[COMP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLIENT_TEMP]");
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupVehicleMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        /// Name		: CleanupEmployeeMerge
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function deletes temporary tables related to employee
        /// </summary>
        internal void CleanupEmployeeMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[EMP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[EMPSUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[DEPT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[FAC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[LOC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[DIV_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[REG_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[OP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[COMP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CLIENT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHONE_TEMP]"); //Added Rakhi for R7:Add Emp Data Elements
                m_objGeneric.DropMTable(m_objMergeCmd, "[ADD_TEMP]");//Added Rakhi for R7:Add Emp Data Elements
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupEmployeeMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        /// Name		: CleanupPhysicianMerge
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function deletes temporary tables related to physician
        /// </summary>
        internal void CleanupPhysicianMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[QPHYS_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHYSSUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PRIV_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CERT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[EDUC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHOSP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[SUBSPEC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHYS_DEPT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAT_DEPT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHYS_FAC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHYS_LOC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHYS_DIV_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAT_REG_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHYS_REG_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHYS_OP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHYS_COMP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHYS_CLNT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHONE_TEMP]"); //Added Rakhi for R7:Add Emp Data Elements
                m_objGeneric.DropMTable(m_objMergeCmd, "[ADD_TEMP]");//Added Rakhi for R7:Add Emp Data Elements
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupPhysicianMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        /// Name		: CleanupPatientMerge
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function deletes temporary tables related to patient
        /// </summary>
        internal void CleanupPatientMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAT_SUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PATPRIPHYS_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PATPROC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PDIAG_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[DRG_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[ACTT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[ATPHYS_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAT_DEPT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAT_FAC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAT_LOC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAT_DIV_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAT_REG_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAT_OP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAT_COMP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PAT_CLNT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHONE_TEMP]"); //Added Rakhi for R7:Add Emp Data Elements
                m_objGeneric.DropMTable(m_objMergeCmd, "[ADD_TEMP]");//Added Rakhi for R7:Add Emp Data Elements
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupPatientMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        /// Name		: CleanupDriverMerge
        /// Author		: Amandeep Kaur
        /// Date Created: 10/08/2012		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function deletes temporary tables related to driver
        /// </summary>
        internal void CleanupDriverMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[DRIVER_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[DRIVER_SUPP_TEMP]");
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupDriverMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        /// Name		: CleanupMedStaffMerge
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function deletes temporary tables related to medical staff
        /// </summary>
        internal void CleanupMedStaffMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[MED_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MEDSUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[SPRIV_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[SCERT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MED_DEPT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MED_FAC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MED_LOC_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MED_DIV_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MED_REG_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MED_OP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MED_COMP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MED_CLNT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHONE_TEMP]"); //Added Rakhi for R7:Add Emp Data Elements
                m_objGeneric.DropMTable(m_objMergeCmd, "[ADD_TEMP]");//Added Rakhi for R7:Add Emp Data Elements
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupMedStaffMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        /// Name		: CleanupEntityMerge
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function deletes temporary tables related to people/entity
        /// </summary>
        internal void CleanupEntityMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[ENT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[ENTSUPP_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PHONE_TEMP]"); //Added Rakhi for R7:Add Emp Data Elements
                m_objGeneric.DropMTable(m_objMergeCmd, "[ADD_TEMP]");//Added Rakhi for R7:Add Emp Data Elements
                m_objGeneric.DropMTable(m_objMergeCmd, "[ENTITYIDTYPE_TEMP]"); //MITS:34276-- Entity ID Type search 
                m_objGeneric.DropMTable(m_objMergeCmd, "[CONTACT_TEMP]");// sranjan9   added JIRA 7526
                m_objGeneric.DropMTable(m_objMergeCmd, "[CODELICEN_TEMP]"); // sranjan9  added JIRA 7526
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupEntityMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        // Start Naresh Enhanced Policy Merge
        /// Name		: CleanupEnhPolicyMerge
        /// Author		: Naresh Chandra Padhy
        /// Date Created: 01/22/2007		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function deletes temporary tables related to people/entity
        /// </summary>
        internal void CleanupEnhPolicyMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[POLICY_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[INS_ENT_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[CVG_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[EXP_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[INSURED_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[MCO_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[POLSUPPENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[TERM_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[TRANS_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[RTNG_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[DCNT_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[DTIER_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[BRO_ENH_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[BROKER_FIRM_TEMP]");

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupEnhPolicyMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        // End Naresh Enhanced Policy Merge

        //Anu Tennyson   for MITS 18291 : STARTS
        /// Name		: CleanupPropertyMerge
        /// Author		: Anu Tennyson
        /// Date Created: 10/29/2009		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function deletes temporary tables related to Property
        /// </summary>
        internal void CleanupPropertyMerge()
        {
            try
            {
                m_objGeneric.DropMTable(m_objMergeCmd, "[PROPERTY_UNIT_TEMP]");
                m_objGeneric.DropMTable(m_objMergeCmd, "[PROPSUPP_TEMP]");
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.CleanupPropertyMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
        }
        //Anu Tennyson for MITS 18291 : END
        #endregion

        #region Vehicle Merge
        /// Name		: VehicleSubQuery
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function creates dynamic query related to vehicle merge
        /// </summary>
        /// <param name="p_objTableInfo">Vehicle related information</param>
        internal void VehicleSubQuery(ref VehicleTempTable p_objTableInfo)
        {
            string sSQL = string.Empty;
            string sBitFields = string.Empty;
            string sBitValues = string.Empty;
            try
            {
                //CREATES DYNAMIC VEHICLE RELATED QUERY TO FETCH DATA
                sSQL = "SELECT UNIT_ID, " + p_objTableInfo.DisplayFields + " [INTO " + p_objTableInfo.TableName + "] "
                    + " FROM " + p_objTableInfo.FromTables + " WHERE " + p_objTableInfo.QueryWhere;
                Generic.ChainIt(ref p_objTableInfo.ChainFrom, ref p_objTableInfo.ChainWhere, "[VEH_TEMP]", "[" + p_objTableInfo.TableName + "]", "UNIT_ID");
                m_objMergeCmd.CommandType = CommandType.Text;
                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                m_objMergeCmd.ExecuteNonQuery();
                if (m_objGeneric.GetMergeCount("[" + p_objTableInfo.TableName + "]") == 0)
                {
                    m_objGeneric.FindBitFields("[" + p_objTableInfo.TableName + "]", ref sBitFields, ref sBitValues);
                    sSQL = "INSERT INTO " + "[" + p_objTableInfo.TableName + "]" + "(UNIT_ID" + sBitFields + ")"
                        + " SELECT UNIT_ID" + sBitValues + " FROM [VEH_TEMP]";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.VehicleSubQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                sSQL = null;
                sBitFields = null;
                sBitValues = null;
            }
        }
        /// Name		: DoVehicleMergeQuery
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to vehicle
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        internal void DoVehicleMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, long p_ldbMake)
        {
            string sSQL = string.Empty;
            int iIdx = 0;
            Merge objMerge = null;
            VehicleTempTable objTempTable = new VehicleTempTable();
            DbReader objReader = null;
            try
            {
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupVehicleMerge();
                }
                //VEHICLE
                if (Utilities.ExistsInArray(p_objDisplayTables, "VEHICLE"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "VEHICLE");
                    sSQL = "SELECT UNIT_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO VEH_TEMP] " + " FROM VEHICLE WHERE UNIT_ID = " + p_lRecordID.ToString();
                }
                else
                {
                    sSQL = "SELECT UNIT_ID [INTO VEH_TEMP] FROM VEHICLE WHERE UNIT_ID = "
                        + p_lRecordID.ToString();
                }
                m_objMergeCmd.CommandType = CommandType.Text;
                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                m_objMergeCmd.ExecuteNonQuery();
                objTempTable.ChainFrom = "[VEH_TEMP]";
                objTempTable.ChainWhere = string.Empty;
                //VEHICLE SUPPLEMENTAL
                if (Utilities.ExistsInArray(p_objDisplayTables, "VEHICLE_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "VEHICLE_SUPP");
                    objTempTable.DisplayFields = p_objDisplayFields[iIdx].ToString();
                    objTempTable.TableName = "VEHSUPP_TEMP";
                    objTempTable.FromTables = "VEHICLE_SUPP";
                    objTempTable.QueryWhere = "UNIT_ID = " + p_lRecordID.ToString();
                    VehicleSubQuery(ref objTempTable);
                }
                //VEHICLE DEPT
                objTempTable.FromTables = "VEHICLE,ENTITY DEPT";
                objTempTable.QueryWhere = "UNIT_ID = " + p_lRecordID.ToString() + " AND VEHICLE.HOME_DEPT_EID = DEPT.ENTITY_ID";
                if (Utilities.ExistsInArray(p_objDisplayTables, "DEPT"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DEPT");
                    objTempTable.DisplayFields = p_objDisplayFields[iIdx].ToString();
                    objTempTable.TableName = "DEPT_TEMP";
                    VehicleSubQuery(ref objTempTable);
                }
                //VEHICLE FACILITY
                objTempTable.FromTables = objTempTable.FromTables + ",ENTITY FACILITY ";
                objTempTable.QueryWhere = objTempTable.QueryWhere + " AND DEPT.PARENT_EID = FACILITY.ENTITY_ID";
                if (Utilities.ExistsInArray(p_objDisplayTables, "FACILITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "FACILITY");
                    objTempTable.DisplayFields = p_objDisplayFields[iIdx].ToString();
                    objTempTable.TableName = "FAC_TEMP";
                    VehicleSubQuery(ref objTempTable);
                }
                //VEHICLE LOCATION
                objTempTable.FromTables = objTempTable.FromTables + ",ENTITY LOCATION ";
                objTempTable.QueryWhere = objTempTable.QueryWhere + " AND FACILITY.PARENT_EID = LOCATION.ENTITY_ID";
                if (Utilities.ExistsInArray(p_objDisplayTables, "LOCATION"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "LOCATION");
                    objTempTable.DisplayFields = p_objDisplayFields[iIdx].ToString();
                    objTempTable.TableName = "LOC_TEMP";
                    VehicleSubQuery(ref objTempTable);
                }
                //VEHICLE DIVISION
                objTempTable.FromTables = objTempTable.FromTables + ",ENTITY DIVISION ";
                objTempTable.QueryWhere = objTempTable.QueryWhere + " AND LOCATION.PARENT_EID = DIVISION.ENTITY_ID";
                if (Utilities.ExistsInArray(p_objDisplayTables, "DIVISION"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DIVISION");
                    objTempTable.DisplayFields = p_objDisplayFields[iIdx].ToString();
                    objTempTable.TableName = "DIV_TEMP";
                    VehicleSubQuery(ref objTempTable);
                }
                //VEHICLE REGION
                objTempTable.FromTables = objTempTable.FromTables + ",ENTITY REGION ";
                objTempTable.QueryWhere = objTempTable.QueryWhere + " AND DIVISION.PARENT_EID = REGION.ENTITY_ID";
                if (Utilities.ExistsInArray(p_objDisplayTables, "REGION"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "REGION");
                    objTempTable.DisplayFields = p_objDisplayFields[iIdx].ToString();
                    objTempTable.TableName = "REG_TEMP";
                    VehicleSubQuery(ref objTempTable);
                }
                //VEHICLE OPERATION
                objTempTable.FromTables = objTempTable.FromTables + ",ENTITY OPERATION ";
                objTempTable.QueryWhere = objTempTable.QueryWhere + " AND REGION.PARENT_EID = OPERATION.ENTITY_ID";
                if (Utilities.ExistsInArray(p_objDisplayTables, "OPERATION"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "OPERATION");
                    objTempTable.DisplayFields = p_objDisplayFields[iIdx].ToString();
                    objTempTable.TableName = "OP_TEMP";
                    VehicleSubQuery(ref objTempTable);
                }
                //VEHICLE COMPANY
                objTempTable.FromTables = objTempTable.FromTables + ",ENTITY COMPANY ";
                objTempTable.QueryWhere = objTempTable.QueryWhere + " AND OPERATION.PARENT_EID = COMPANY.ENTITY_ID";
                if (Utilities.ExistsInArray(p_objDisplayTables, "COMPANY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "COMPANY");
                    objTempTable.DisplayFields = p_objDisplayFields[iIdx].ToString();
                    objTempTable.TableName = "COMP_TEMP";
                    VehicleSubQuery(ref objTempTable);
                }
                //VEHICLE CLIENT
                objTempTable.FromTables = objTempTable.FromTables + ",ENTITY CLIENTENT ";
                objTempTable.QueryWhere = objTempTable.QueryWhere + " AND COMPANY.PARENT_EID = CLIENTENT.ENTITY_ID";
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLIENTENT"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLIENTENT");
                    objTempTable.DisplayFields = p_objDisplayFields[iIdx].ToString();
                    objTempTable.TableName = "CLIENT_TEMP";
                    VehicleSubQuery(ref objTempTable);
                }
                sSQL = "SELECT " + p_sJustFieldNames
                    + " FROM " + objTempTable.ChainFrom;
                if (objTempTable.ChainWhere != "")
                {
                    sSQL = sSQL + " WHERE " + objTempTable.ChainWhere;
                }
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);//rkaur27
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupVehicleMerge();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoVehicleMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}

            }
        }
        #endregion

        //Anu Tennyson for MITS 18291 : BEGINS 10/29/2009
        #region Property Merge
        ///<summary>
        /// This function creates dynamic query related to property merge
        /// </summary>
        /// <param name="p_objTableInfo">Property related information</param>
        internal void PropertySubQuery(ref PropertyTempTable p_objTableInfo)
        {
            StringBuilder sbSql = new StringBuilder();
            string sBitFields = string.Empty;
            string sBitValues = string.Empty;

            try
            {
                //CREATES DYNAMIC PROPERTY RELATED QUERY TO FETCH DATA
                sbSql.Append("SELECT PROPERTY_ID, ");
                sbSql.Append(p_objTableInfo.DisplayFields);
                sbSql.Append(" [INTO ");
                sbSql.Append(p_objTableInfo.TableName);
                sbSql.Append("] FROM ");
                sbSql.Append(p_objTableInfo.FromTables);
                sbSql.Append(" WHERE ");
                sbSql.Append(p_objTableInfo.QueryWhere);

                Generic.ChainIt(ref p_objTableInfo.ChainFrom, ref p_objTableInfo.ChainWhere, "[PROPERTY_UNIT_TEMP]", "[" + p_objTableInfo.TableName + "]", "PROPERTY_ID");
                m_objMergeCmd.CommandType = CommandType.Text;
                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sbSql.ToString());
                m_objMergeCmd.ExecuteNonQuery();
                if (m_objGeneric.GetMergeCount("[" + p_objTableInfo.TableName + "]") == 0)
                {
                    m_objGeneric.FindBitFields("[" + p_objTableInfo.TableName + "]", ref sBitFields, ref sBitValues);
                    sbSql = new StringBuilder();
                    sbSql.Append("INSERT INTO " + "[");
                    sbSql.Append(p_objTableInfo.TableName);
                    sbSql.Append("] (PROPERTY_ID");
                    sbSql.Append(sBitFields);
                    sbSql.Append(") SELECT PROPERTY_ID");
                    sbSql.Append(sBitValues);
                    sbSql.Append(" FROM [PROPERTY_UNIT_TEMP]");
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sbSql.ToString());
                    m_objMergeCmd.ExecuteNonQuery();
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.PropertySubQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                sbSql = null;
            }
        }
        //Anu Tennyson for MITS 18291 : END

        //Anu Tennyson for MITS 18291 :BEGINS 10/29/2009
        ///<summary>
        /// This function retrieves data related to property
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_ldbMake">Database type</param>
        internal void DoPropertyMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, long p_ldbMake)
        {
            int iIdx = 0;
            StringBuilder sbSQL = new StringBuilder();
            Merge objMerge = null;
            PropertyTempTable objTempTable = new PropertyTempTable();

            try
            {
                using (m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    m_objMergeConn.Open();
                    using (m_objMergeCmd = m_objMergeConn.CreateCommand())
                    {
                        if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                        {
                            CleanupPropertyMerge();
                        }

                        //PROPERTY
                        if (Utilities.ExistsInArray(p_objDisplayTables, "PROPERTY_UNIT"))
                        {
                            iIdx = Generic.IndexOf(p_objDisplayTables, "PROPERTY_UNIT");
                            sbSQL.Append("SELECT PROPERTY_ID,");
                            sbSQL.Append(p_objDisplayFields[iIdx].ToString());
                            sbSQL.Append(" [INTO PROPERTY_UNIT_TEMP] " + " FROM PROPERTY_UNIT WHERE PROPERTY_ID = ");
                            sbSQL.Append(p_lRecordID.ToString());
                        }
                        else
                        {
                            sbSQL.Append("SELECT PROPERTY_ID [INTO PROPERTY_UNIT_TEMP] FROM PROPERTY_UNIT WHERE PROPERTY_ID = ");
                            sbSQL.Append(p_lRecordID.ToString());
                        }
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sbSQL.ToString());
                        m_objMergeCmd.ExecuteNonQuery();
                        objTempTable.ChainFrom = "[PROPERTY_UNIT_TEMP]";
                        objTempTable.ChainWhere = string.Empty;

                        //PROPERTY SUPPLEMENTAL
                        if (Utilities.ExistsInArray(p_objDisplayTables, "PROPERTY_UNIT_SUPP"))
                        {
                            iIdx = Generic.IndexOf(p_objDisplayTables, "PROPERTY_UNIT_SUPP");
                            objTempTable.DisplayFields = p_objDisplayFields[iIdx].ToString();
                            objTempTable.TableName = "PROPSUPP_TEMP";
                            objTempTable.FromTables = "PROPERTY_UNIT_SUPP";
                            objTempTable.QueryWhere = "PROPERTY_ID = " + p_lRecordID.ToString();
                            PropertySubQuery(ref objTempTable);
                        }

                        sbSQL = new StringBuilder();
                        sbSQL.Append("SELECT ");
                        sbSQL.Append(p_sJustFieldNames);
                        sbSQL.Append(" FROM ");
                        sbSQL.Append(objTempTable.ChainFrom);
                        if (objTempTable.ChainWhere != "")
                        {
                            sbSQL.Append(" WHERE ");
                            sbSQL.Append(objTempTable.ChainWhere);
                        }
                        m_objGeneric.GetDataset(m_objMergeConn, sbSQL.ToString(), ref m_objMergeds);

                        using (objMerge = new Merge(m_iClientId))//rkaur27
                        {
                            objMerge.Command = m_objMergeCmd;
                            //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                        }//using objMerge
                    }//using m_objMergeCmd
                }//using m_objMergeConn
                CleanupPropertyMerge();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoPropertyMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
                sbSQL = null;
            }
        }
        //Anu Tennyson for MITS 18291 : END

        #endregion
        #region Admin Tracking Merge
        /// Name		: DoAtMerge
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to admin tracking
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_sATTable">Table name</param>
        /// <param name="p_sATKeyField">Field name</param>
        /// <param name="p_objDisplayFields">Table names from which to get the merge data</param>
        /// <param name="p_iIndex">Index/Position of field in fields array</param>
        internal void DoAtMerge(long p_lRecordID, string p_sATTable, string p_sATKeyField
            , ArrayList p_objDisplayFields, int p_iIndex)
        {
            DbReader objReader = null;
            string sSQL = string.Empty;
            Merge objMerge = null;
            try
            {
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                //ADMIN TRACKING FIELDS RELATED DATA
                sSQL = "SELECT " + p_objDisplayFields[p_iIndex].ToString() + " FROM " +
                    p_sATTable
                + " WHERE " + p_sATKeyField + " = " + p_lRecordID.ToString();
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);//rkaur27
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoAtMerge.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
            }
        }
        #endregion

        #region Patient Merge
        /// Name		: DoPatientMergeQuery
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to patient
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        internal void DoPatientMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, ref string p_sFrom, ref string p_sWhere
            , bool bMergeAll, long p_ldbMake)
        {
            string sSQL = string.Empty;
            int iIdx = 0;
            int iPos = 0;
            string sTmp = string.Empty;
            string sTmpNext = string.Empty;
            int iIdxNext = 0;
            int iIdxAddr = 0; //RMA-8753 nshah28 
            string sBitFields = string.Empty;
            string sBitValues = string.Empty;
            string sProcSel = string.Empty;
            Merge objMerge = null;
            DbReader objReader = null;
            DbReader objReaderNext = null;
            ArrayList objTempValues = null;
            LocalCache objCache = null;
            string sFieldNames = string.Empty;
            string sFieldValues = string.Empty;
            string sFieldName = string.Empty;
            string sFieldPos = string.Empty;
            try
            {
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupPatientMerge();
                }
                //Patient
                if (Utilities.ExistsInArray(p_objDisplayTables, "PATIENT")
                    || Utilities.ExistsInArray(p_objDisplayTables, "PATIENT_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PATIENT");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "PATIENT_ENTITY");
                    sSQL = "SELECT PATIENT.PATIENT_ID ";
                    if (iIdx != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    sSQL = sSQL + " [INTO PAT_TEMP]"
                        + " FROM PATIENT,ENTITY PATIENT_ENTITY WHERE PATIENT.PATIENT_ID = " + p_lRecordID.ToString()
                        + " AND PATIENT.PATIENT_EID = PATIENT_ENTITY.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                else
                {
                    sSQL = "SELECT PATIENT_ID [INTO PAT_TEMP] FROM PATIENT WHERE PATIENT_ID = "
                        + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                p_sFrom = "[PAT_TEMP]";
                p_sWhere = "";
                //Patient Supplemental
                if (Utilities.ExistsInArray(p_objDisplayTables, "PATIENT_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PATIENT_SUPP");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[PAT_SUPP_TEMP]", "PATIENT_ID");
                    sSQL = "SELECT PATIENT_SUPP.PATIENT_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO PAT_SUPP_TEMP]"
                    + " FROM PATIENT_SUPP WHERE PATIENT_SUPP.PATIENT_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PAT_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PAT_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [PAT_SUPP_TEMP](PATIENT_ID" + sBitFields + ") VALUES" //Mits 18595
                            + " (" + p_lRecordID.ToString() + sBitValues + ")";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Previous physician
                if (Utilities.ExistsInArray(p_objDisplayTables, "PATIENT_PRI_PHYS"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PATIENT_PRI_PHYS");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[PATPRIPHYS_TEMP]", "PATIENT_ID");
                    sSQL = "SELECT PATIENT.PATIENT_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO PATPRIPHYS_TEMP]"
                    + " FROM PATIENT,ENTITY PATIENT_PRI_PHYS WHERE PATIENT.PATIENT_ID = " + p_lRecordID.ToString()
                    + " AND PATIENT.PRI_PHYSICIAN_EID = PATIENT_PRI_PHYS.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PATPRIPHYS_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PATPRIPHYS_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [PATPRIPHYS_TEMP](PATIENT_ID" + sBitFields + ") VALUES" //Mits 18595
                            + " (" + p_lRecordID.ToString() + sBitValues + ")";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_objDisplayTables, "ADDRESS_X_PHONEINFO"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "ADDRESS_X_PHONEINFO");
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("|");
                    sFieldName = p_objDisplayFields[iIdx].ToString().Substring(0, iPos - 1);
                    sFieldPos = p_objDisplayFields[iIdx].ToString().Substring(iPos);

                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[PHONE_TEMP]", "PATIENT_ID");

                    sbSql.Length = 0;
                    sbSql.AppendFormat("SELECT PATIENT_ID,{0} {1}'('{1}CODE_DESC{1}')' {2}", sFieldName, sConcatOperator, sFieldPos);
                    sbSql.Append(" [INTO PHONE_TEMP]");
                    sbSql.AppendFormat(" FROM PATIENT,ADDRESS_X_PHONEINFO,CODES_TEXT  WHERE PATIENT_ID ={0} ", p_lRecordID.ToString());
                    sbSql.Append(" AND PATIENT.PATIENT_EID=ADDRESS_X_PHONEINFO.ENTITY_ID");
                    sbSql.Append(" AND CODES_TEXT.CODE_ID=ADDRESS_X_PHONEINFO.PHONE_CODE");
                    sSQL = sbSql.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    //If nothing went in, fill with empty record(s) so that inner join doesn't fail
                    if (m_objGeneric.GetMergeCount("[PHONE_TEMP]") == 0)
                    {
                        sFieldValues = "";
                        sFieldNames = "";
                        sbSql.Length = 0;

                        m_objGeneric.FindFields("[PHONE_TEMP]", ref sFieldNames, ref sFieldValues);

                        sbSql.AppendFormat("INSERT INTO [PHONE_TEMP](PATIENT_ID{0}) VALUES", sFieldNames);
                        sbSql.AppendFormat(" ({0}{1})", p_lRecordID.ToString(), sFieldValues);
                        sSQL = sbSql.ToString();

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                AddOrgPhoneQuery("PAT_PHN_", 8, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PAT_PHN_", 7, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PAT_PHN_", 6, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PAT_PHN_", 5, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PAT_PHN_", 4, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PAT_PHN_", 3, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PAT_PHN_", 2, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PAT_PHN_", 1, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);

                if (Utilities.ExistsInArray(p_objDisplayTables, "PAT_PRI_PHYS_PHN"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PAT_PRI_PHYS_PHN");
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("|");
                    sFieldName = p_objDisplayFields[iIdx].ToString().Substring(0, iPos - 1);
                    sFieldPos = p_objDisplayFields[iIdx].ToString().Substring(iPos);
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[PATPRIPHYSPHN_TEMP]", "PATIENT_ID");

                    sSQL = "SELECT PATIENT_ID," + sFieldName + " + '(' + CODE_DESC + ')'" + sFieldPos
                            + " [INTO PATPRIPHYSPHN_TEMP]"
                            + " FROM PATIENT,CODES_TEXT,ADDRESS_X_PHONEINFO PAT_PRI_PHYS_PHN  WHERE PATIENT_ID = " + p_lRecordID.ToString()
                            + " AND PATIENT.PATIENT_EID=ADDRESS_X_PHONEINFO.ENTITY_ID"
                            + " AND CODES_TEXT.CODE_ID=ADDRESS_X_PHONEINFO.PHONE_CODE"
                            + " AND PATIENT.PRI_PHYSICIAN_EID = PAT_PRI_PHYS_PHN.ENTITY_ID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PATPRIPHYSPHN_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PATPRIPHYSPHN_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [PATPRIPHYSPHN_TEMP](PATIENT_ID" + sBitFields + ") VALUES"
                            + " (" + p_lRecordID.ToString() + sBitValues + ")";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_objDisplayTables, "ADDRESS"))//RMA-8753 nshah28
                {
                    if (objSettings.UseMultipleAddresses)
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "ENTITY_X_ADDRESSES");
                        Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[ADD_TEMP]", "PATIENT_ID");

                        sbSql.Length = 0;
                        //RMA-8753 nshah28 start
                        //sbSql.AppendFormat("SELECT PATIENT_ID,{0}", p_objDisplayFields[iIdx].ToString());
                        iIdxAddr = Generic.IndexOf(p_objDisplayTables, "ADDRESS");
                        sbSql.AppendFormat("SELECT PATIENT_ID");
                        if (iIdx > -1)
                            sbSql.AppendFormat(" ,{0}", p_objDisplayFields[iIdx].ToString());
                        if (iIdxAddr > -1)
                            sbSql.AppendFormat(" ,{0}", p_objDisplayFields[iIdxAddr].ToString());
                        //RMA-8753 nshah28 end
                        sbSql.Append(" [INTO ADD_TEMP]");
                        //sbSql.AppendFormat(" FROM PATIENT,ENTITY_X_ADDRESSES WHERE PATIENT_ID = {0}", p_lRecordID.ToString());
                        sbSql.AppendFormat(" FROM PATIENT,ENTITY_X_ADDRESSES, ADDRESS WHERE ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID  AND PATIENT_ID = {0}", p_lRecordID.ToString());
                        //RMA-8753 nshah28 END
                        sbSql.Append(" AND PATIENT.PATIENT_EID=ENTITY_X_ADDRESSES.ENTITY_ID");
                        sSQL = sbSql.ToString();

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        //If nothing went in, fill with empty record(s) so that inner join doesn't fail
                        if (m_objGeneric.GetMergeCount("[ADD_TEMP]") == 0)
                        {
                            sFieldValues = "";
                            sFieldNames = "";
                            sbSql.Length = 0;

                            m_objGeneric.FindFields("[ADD_TEMP]", ref sFieldNames, ref sFieldValues);
                            sbSql.AppendFormat("INSERT INTO [ADD_TEMP](PATIENT_ID{0}) VALUES", sFieldNames);
                            sbSql.AppendFormat(" ({0}{1})", p_lRecordID.ToString(), sFieldValues);
                            sSQL = sbSql.ToString();

                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                }
                //Added Rakhi for R7:Add Emp Data Elements
                //Procedures
                if (Utilities.ExistsInArray(p_objDisplayTables, "PATIENT_PROCEDURE"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PATIENT_PROCEDURE");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[PATPROC_TEMP]", "PATIENT_ID");
                    sProcSel = "-1";

                    sSQL = "SELECT PATIENT_PROCEDURE.PATIENT_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO PATPROC_TEMP]"
                    + " FROM PATIENT_PROCEDURE WHERE PATIENT_PROCEDURE.PATIENT_ID = "
                        + p_lRecordID.ToString();
                    if (sProcSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sProcSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PATPROC_TEMP]") == 0)
                    {
                        //Mits 18595
                        //m_objGeneric.FindBitFields("[PATPROC_TEMP]", ref sBitFields, ref sBitValues);
                        //sSQL = "INSERT INTO [PATPROC_TEMP](PATIENT_ID" + sBitFields + ") VALUES" 
                        //    +" (" + p_lRecordID.ToString() + sBitValues + ")";
                        sFieldNames = "";
                        sFieldValues = "";
                        m_objGeneric.FindFields("[PATPROC_TEMP]", ref sFieldNames, ref sFieldValues);
                        sSQL = "INSERT INTO [PATPROC_TEMP](PATIENT_ID" + sFieldNames + ") VALUES"
                            + " (" + p_lRecordID.ToString() + sFieldValues + ")";
                        //End - pmittal5
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Diagnosis
                if (Utilities.ExistsInArray(p_objDisplayTables, "PATIENT_DIAGNOSIS"))
                {
                    objTempValues = new ArrayList();
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PATIENT_DIAGNOSIS");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[PDIAG_TEMP]", "PATIENT_ID");
                    //pmittal5 Mits 18595 12/03/09 
                    //iPos=p_objDisplayTables[iIdx].ToString().IndexOf("FLD");
                    //sTmp = p_objDisplayTables[iIdx].ToString().Substring(iPos);
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("FLD");
                    sTmp = p_objDisplayFields[iIdx].ToString().Substring(iPos);
                    //End - pmittal5
                    switch (p_ldbMake)
                    {
                        case (int)eDatabaseType.DBMS_IS_ACCESS:
                            sSQL = "CREATE TABLE [PDIAG_TEMP] (PATIENT_ID LONG, " + sTmp + " TEXT(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_SQLSRVR:
                            sSQL = "CREATE TABLE [PDIAG_TEMP] (PATIENT_ID int, " + sTmp + " varchar(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_SYBASE:
                            sSQL = "CREATE TABLE [PDIAG_TEMP] (PATIENT_ID int, " + sTmp + " varchar(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_INFORMIX:
                            sSQL = "CREATE TABLE [PDIAG_TEMP] (PATIENT_ID INTEGER, " + sTmp + " VARCHAR(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_DB2:
                            sSQL = "CREATE TABLE [PDIAG_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_ORACLE:
                            m_objGeneric.DeleteOracleTables("DROP TABLE [PDIAG_TEMP]", "[PDIAG_TEMP]");
                            sSQL = "CREATE TABLE [PDIAG_TEMP] (PATIENT_ID NUMBER(10), " + sTmp + " VARCHAR(250))";
                            break;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    sSQL = m_objGeneric.SafeSQL("SELECT PATIENT_ID FROM [PAT_TEMP]");
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        sTmpNext = "";
                        sSQL = "SELECT DIAGNOSIS_CODE FROM PATIENT_DIAGNOSIS WHERE PATIENT_ID = "
                            + Conversion.ConvertObjToStr(objReader.GetValue(0))
                            + " AND DIAGNOSIS_CODE <> 0";
                        objReaderNext = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        while (objReaderNext.Read())
                        {
                            if (sTmpNext != "")
                            {
                                sTmpNext = sTmpNext + ", ";
                            }
                            sTmpNext = sTmpNext + objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReaderNext.GetValue(0))))
                                + "_" + objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReaderNext.GetValue(0))));
                        }
                        if (sTmpNext.Length > 250)
                        {
                            sTmp = sTmp.Substring(0, 250);
                        }
                        sSQL = "INSERT INTO [PDIAG_TEMP](PATIENT_ID," + sTmp + ") VALUES ("
                            //pmittal5 Mits 18595 - Wrong Reader object was used
                            //+ Conversion.ConvertObjToStr(objReaderNext.GetValue(0)) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
                            + Conversion.ConvertObjToStr(objReader.GetValue(0)) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
                        objTempValues.Add(sSQL);
                    }
                    for (int i = 0; i < objTempValues.Count; i++)
                    {
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(objTempValues[i].ToString());
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                    objTempValues = null;
                }
                //Drug codes
                if (Utilities.ExistsInArray(p_objDisplayTables, "PATIENT_DRG_CODES"))
                {
                    objTempValues = new ArrayList();
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PATIENT_DRG_CODES");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[DRG_TEMP]", "PATIENT_ID");
                    //pmittal5 Mits 18595 12/03/09 
                    //iPos=p_objDisplayTables[iIdx].ToString().IndexOf("FLD");
                    //sTmp=p_objDisplayTables[iIdx].ToString().Substring(iPos);
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("FLD");
                    sTmp = p_objDisplayFields[iIdx].ToString().Substring(iPos);
                    //End - pmittal5 
                    switch (p_ldbMake)
                    {
                        case (int)eDatabaseType.DBMS_IS_ACCESS:
                            sSQL = "CREATE TABLE [DRG_TEMP] (PATIENT_ID LONG, " + sTmp + " TEXT(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_SQLSRVR:
                            sSQL = "CREATE TABLE [DRG_TEMP] (PATIENT_ID int, " + sTmp + " varchar(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_SYBASE:
                            sSQL = "CREATE TABLE [DRG_TEMP] (PATIENT_ID int, " + sTmp + " varchar(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_INFORMIX:
                            sSQL = "CREATE TABLE [DRG_TEMP] (PATIENT_ID INTEGER, " + sTmp + " VARCHAR(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_DB2:
                            sSQL = "CREATE TABLE [DRG_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_ORACLE:
                            m_objGeneric.DeleteOracleTables("DROP TABLE [DRG_TEMP]", "[DRG_TEMP]");
                            sSQL = "CREATE TABLE [DRG_TEMP] (PATIENT_ID NUMBER(10), " + sTmp + " VARCHAR(250))";
                            break;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    sSQL = m_objGeneric.SafeSQL("SELECT PATIENT_ID FROM [PAT_TEMP]");
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        sTmpNext = "";
                        sSQL = "SELECT DRG_CODE FROM PATIENT_DRG_CODES WHERE PATIENT_ID = "
                            + Conversion.ConvertObjToStr(objReader.GetValue(0))
                            + " AND DRG_CODE <> 0";
                        objReaderNext = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        while (objReaderNext.Read())
                        {
                            if (sTmpNext != "")
                            {
                                sTmpNext = sTmpNext + ", ";
                            }
                            sTmpNext = sTmpNext + objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReaderNext.GetValue(0))))
                                + "_" + objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReaderNext.GetValue(0))));
                        }
                        if (sTmpNext.Length > 250)
                        {
                            sTmp = sTmp.Substring(0, 250);
                        }
                        sSQL = "INSERT INTO [DRG_TEMP](PATIENT_ID," + sTmp + ") VALUES ("
                            //pmittal5 Mits 18595 - Wrong Reader object was used
                            //+ Conversion.ConvertObjToStr(objReaderNext.GetValue(0)) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
                            + Conversion.ConvertObjToStr(objReader.GetValue(0)) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
                        objTempValues.Add(sSQL);
                    }
                    for (int i = 0; i < objTempValues.Count; i++)
                    {
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(objTempValues[i].ToString());
                        m_objMergeCmd.ExecuteNonQuery();
                    }

                    objTempValues = null;
                }
                //Action taken
                if (Utilities.ExistsInArray(p_objDisplayTables, "PATIENT_ACT_TAKEN"))
                {
                    objTempValues = new ArrayList();
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PATIENT_ACT_TAKEN");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[ACTT_TEMP]", "PATIENT_ID");
                    //pmittal5 Mits 18595
                    //iPos=p_objDisplayTables[iIdx].ToString().IndexOf("FLD");
                    //sTmp=p_objDisplayTables[iIdx].ToString().Substring(iPos);
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("FLD");
                    sTmp = p_objDisplayFields[iIdx].ToString().Substring(iPos);
                    //End - pmittal5
                    switch (p_ldbMake)
                    {
                        case (int)eDatabaseType.DBMS_IS_ACCESS:
                            sSQL = "CREATE TABLE [ACTT_TEMP] (PATIENT_ID LONG, " + sTmp + " TEXT(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_SQLSRVR:
                            sSQL = "CREATE TABLE [ACTT_TEMP] (PATIENT_ID int, " + sTmp + " varchar(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_SYBASE:
                            sSQL = "CREATE TABLE [ACTT_TEMP] (PATIENT_ID int, " + sTmp + " varchar(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_INFORMIX:
                            sSQL = "CREATE TABLE [ACTT_TEMP] (PATIENT_ID INTEGER, " + sTmp + " VARCHAR(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_DB2:
                            sSQL = "CREATE TABLE [ACTT_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_ORACLE:
                            m_objGeneric.DeleteOracleTables("DROP TABLE [ACTT_TEMP]", "[ACTT_TEMP]");
                            sSQL = "CREATE TABLE [ACTT_TEMP] (PATIENT_ID NUMBER(10), " + sTmp + " VARCHAR(250))";
                            break;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    sSQL = m_objGeneric.SafeSQL("SELECT PATIENT_ID FROM [PAT_TEMP]");
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        sTmpNext = "";
                        sSQL = "SELECT ACTION_CODE FROM PATIENT_ACT_TAKEN WHERE PATIENT_ID = "
                            + Conversion.ConvertObjToStr(objReader.GetValue(0))
                            + " AND ACTION_CODE <> 0";
                        objReaderNext = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        while (objReaderNext.Read())
                        {
                            if (sTmpNext != "")
                            {
                                sTmpNext = sTmpNext + ", ";
                            }
                            sTmpNext = sTmpNext + objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReaderNext.GetValue(0))))
                                + "_" + objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReaderNext.GetValue(0))));
                        }
                        if (sTmpNext.Length > 250)
                        {
                            sTmp = sTmp.Substring(0, 250);
                        }
                        sSQL = "INSERT INTO [ACTT_TEMP](PATIENT_ID," + sTmp + ") VALUES ("
                            //pmittal5 Mits 18595 - Wrong Reader object was used
                            //+ Conversion.ConvertObjToStr(objReaderNext.GetValue(0)) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
                            + Conversion.ConvertObjToStr(objReader.GetValue(0)) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
                        objTempValues.Add(sSQL);
                    }

                    for (int i = 0; i < objTempValues.Count; i++)
                    {
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(objTempValues[i].ToString());
                        m_objMergeCmd.ExecuteNonQuery();
                    }

                    objTempValues = null;
                }
                //Attending physician
                if (Utilities.ExistsInArray(p_objDisplayTables, "PATIENT_ATTN_PHYS"))
                {
                    objTempValues = new ArrayList();
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PATIENT_ATTN_PHYS");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[PAT_TEMP]", "[ATPHYS_TEMP]", "PATIENT_ID");
                    //pmittal5 Mits 18595
                    //iPos=p_objDisplayTables[iIdx].ToString().IndexOf("FLD");
                    //sTmp=p_objDisplayTables[iIdx].ToString().Substring(iPos);
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("FLD");
                    sTmp = p_objDisplayFields[iIdx].ToString().Substring(iPos);
                    //End - pmittal5
                    switch (p_ldbMake)
                    {
                        case (int)eDatabaseType.DBMS_IS_ACCESS:
                            sSQL = "CREATE TABLE [ATPHYS_TEMP] (PATIENT_ID LONG, " + sTmp + " TEXT(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_SQLSRVR:
                            sSQL = "CREATE TABLE [ATPHYS_TEMP] (PATIENT_ID int, " + sTmp + " varchar(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_SYBASE:
                            sSQL = "CREATE TABLE [ATPHYS_TEMP] (PATIENT_ID int, " + sTmp + " varchar(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_INFORMIX:
                            sSQL = "CREATE TABLE [ATPHYS_TEMP] (PATIENT_ID INTEGER, " + sTmp + " VARCHAR(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_DB2:
                            sSQL = "CREATE TABLE [ATPHYS_TEMP] (PI_ROW_ID INTEGER, " + sTmp + " VARCHAR(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_ORACLE:
                            m_objGeneric.DeleteOracleTables("DROP TABLE [ATPHYS_TEMP]", "[ATPHYS_TEMP]");
                            sSQL = "CREATE TABLE [ATPHYS_TEMP] (PATIENT_ID NUMBER(10), " + sTmp + " VARCHAR(250))";
                            break;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    sSQL = m_objGeneric.SafeSQL("SELECT PATIENT_ID FROM [PAT_TEMP]");
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        sTmpNext = "";
                        sSQL = "SELECT PHYSICIAN_EID FROM PATIENT_ATTN_PHYS WHERE PATIENT_ID = "
                            + Conversion.ConvertObjToStr(objReader.GetValue(0))
                            + " AND PHYSICIAN_EID <> 0";
                        objReaderNext = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        while (objReaderNext.Read())
                        {
                            if (sTmpNext != "")
                            {
                                sTmpNext = sTmpNext + ", ";
                            }
                            sTmpNext = sTmpNext + objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReaderNext.GetValue(0))))
                                + "_" + objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReaderNext.GetValue(0))));
                        }
                        if (sTmpNext.Length > 250)
                        {
                            sTmp = sTmp.Substring(0, 250);
                        }
                        sSQL = "INSERT INTO [ATPHYS_TEMP](PATIENT_ID," + sTmp + ") VALUES ("
                            //pmittal5 Mits 18595 - Wrong Reader object was used
                            //+ Conversion.ConvertObjToStr(objReaderNext.GetValue(0)) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
                            + Conversion.ConvertObjToStr(objReader.GetValue(0)) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
                        objTempValues.Add(sSQL);
                    }

                    for (int i = 0; i < objTempValues.Count; i++)
                    {
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(objTempValues[i].ToString());
                        m_objMergeCmd.ExecuteNonQuery();
                    }

                }
                //Following function calls create dynamic query related to dept,company etc.
                QOrgHQuery("PAT_", 8, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PAT_", 7, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PAT_", 6, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PAT_", 5, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PAT_", 4, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PAT_", 3, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PAT_", 2, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PAT_", 1, "PATIENT", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                sSQL = "Select " + p_sJustFieldNames
                    + " FROM " + p_sFrom;
                if (p_sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + p_sWhere;
                }
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);//rkaur27
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupPatientMerge();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoPatientMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objReaderNext != null)
                {
                    objReaderNext.Close();
                    objReaderNext.Dispose();
                }
                sSQL = null;
                sBitFields = null;
                sBitValues = null;
                sFieldNames = null;
                sFieldValues = null;
                sTmp = null;
                sTmpNext = null;
                objTempValues = null;
                sProcSel = null;
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
            }
        }
        #endregion

        #region Driver Merge
        /// Name		: DoDriverMergeQuery
        /// Author		: Amandep Kaur
        /// Date Created: 10/08/2012		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to driver
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        internal void DoDriverMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, ref string p_sFrom, ref string p_sWhere
            , bool bMergeAll, long p_ldbMake)
        {
            string sSQL = string.Empty;
            int iIdx = 0;
            int iPos = 0;
            string sTmp = string.Empty;
            string sTmpNext = string.Empty;
            int iIdxNext = 0;
            string sBitFields = string.Empty;
            string sBitValues = string.Empty;
            string sProcSel = string.Empty;
            Merge objMerge = null;
            DbReader objReader = null;
            DbReader objReaderNext = null;
            ArrayList objTempValues = null;
            LocalCache objCache = null;
            string sFieldNames = string.Empty;
            string sFieldValues = string.Empty;
            string sFieldName = string.Empty;
            string sFieldPos = string.Empty;
            try
            {
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud
                objCache = new LocalCache(m_sConnectionString,m_iClientId);
                if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupPatientMerge();
                }
                //Driver
                if (Utilities.ExistsInArray(p_objDisplayTables, "DRIVER")
                    || Utilities.ExistsInArray(p_objDisplayTables, "DRIVER_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DRIVER");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "DRIVER_ENTITY");
                    sSQL = "SELECT DRIVER.DRIVER_ROW_ID ";
                    if (iIdx != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    sSQL = sSQL + " [INTO DRIVER_TEMP]"
                        + " FROM DRIVER,ENTITY DRIVER_ENTITY WHERE DRIVER.DRIVER_ROW_ID = " + p_lRecordID.ToString()
                        + " AND DRIVER.DRIVER_EID = DRIVER_ENTITY.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                else
                {
                    sSQL = "SELECT DRIVER_ROW_ID [INTO DRIVER_TEMP] FROM DRIVER WHERE DRIVER_ROW_ID = "
                        + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                p_sFrom = "[DRIVER_TEMP]";
                p_sWhere = "";
                //Driver Supplemental
                if (Utilities.ExistsInArray(p_objDisplayTables, "DRIVER_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DRIVER_SUPP");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[DRIVER_TEMP]", "[DRIVER_SUPP_TEMP]", "DRIVER_ROW_ID");
                    sSQL = "SELECT DRIVER_SUPP.DRIVER_ROW_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO DRIVER_SUPP_TEMP]"
                    + " FROM DRIVER_SUPP WHERE DRIVER_SUPP.DRIVER_ROW_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[DRIVER_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[DRIVER_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [DRIVER_SUPP_TEMP](DRIVER_ROW_ID" + sBitFields + ") VALUES" //Mits 18595
                            + " (" + p_lRecordID.ToString() + sBitValues + ")";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                sSQL = "Select " + p_sJustFieldNames
                    + " FROM " + p_sFrom;
                if (p_sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + p_sWhere;
                }
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);//rkaur27
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupDriverMerge();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoDriverMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objReaderNext != null)
                {
                    objReaderNext.Close();
                    objReaderNext.Dispose();
                }
                sSQL = null;
                sBitFields = null;
                sBitValues = null;
                sFieldNames = null;
                sFieldValues = null;
                sTmp = null;
                sTmpNext = null;
                objTempValues = null;
                sProcSel = null;
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
            }
        }
        #endregion
        #region Physician Merge
        /// Name		: DoPhysicianMergeQuery
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to physician
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        internal void DoPhysicianMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, ref string p_sFrom, ref string p_sWhere
            , bool bMergeAll, long p_ldbMake)
        {
            string sSQL = string.Empty;
            int iIdx = 0;
            int iIdxAddr = 0; //RMA-8753 nshah28 
            int iPos = 0;
            string sTmp = string.Empty;
            string sTmpNext = "";
            int iIdxNext = 0;
            //Shruti for 11832
            int iId = 0;
            string sBitFields = "";
            string sBitValues = "";
            DbReader objReader = null;
            DbReader objReaderNext = null;
            ArrayList objTempValues = null;
            LocalCache objCache = null;
            //Shruti, 9168
            string sKeyField = "";
            string sField = "";
            string sValue = "";
            //Shruti, 9168 ends
            string sFieldNames = string.Empty;
            string sFieldValues = string.Empty;
            try
            {
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud
                if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupPhysicianMerge();
                }
                //PHYSICIAN
                if (Utilities.ExistsInArray(p_objDisplayTables, "PHYSICIAN")
                    || Utilities.ExistsInArray(p_objDisplayTables, "PHYSICIAN_ENTITY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "MED_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PHYSICIAN");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "PHYSICIAN_ENTITY");
                    //Shruti for 11832
                    iId = Generic.IndexOf(p_objDisplayTables, "MED_ENTITY");
                    sSQL = "SELECT PHYSICIAN.PHYS_EID ";
                    if (iIdx != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    if (iId != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iId].ToString());
                    }
                    sSQL = sSQL + " [INTO QPHYS_TEMP]"
                        + " FROM PHYSICIAN,ENTITY PHYSICIAN_ENTITY,ENTITY MED_ENTITY WHERE PHYSICIAN.PHYS_EID = " + p_lRecordID.ToString()
                        + " AND PHYSICIAN.PHYS_EID = PHYSICIAN_ENTITY.ENTITY_ID"
                        //Fixed issue 14745
                    + " AND PHYSICIAN.PHYS_EID = MED_ENTITY.ENTITY_ID";
                    //Shruti for 11832 ends
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                else
                {
                    sSQL = "SELECT PHYS_EID [INTO QPHYS_TEMP] FROM PHYSICIAN WHERE PHYS_EID = "
                        + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                p_sFrom = "[QPHYS_TEMP]";
                p_sWhere = "";
                //PHYSICIAN SUPPLEMENTAL
                if (Utilities.ExistsInArray(p_objDisplayTables, "PHYS_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PHYS_SUPP");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[PHYSSUPP_TEMP]", "PHYS_EID");
                    sSQL = "SELECT PHYS_SUPP.PHYS_EID," + p_objDisplayFields[iIdx].ToString() + " [INTO PHYSSUPP_TEMP]"
                        + " FROM PHYS_SUPP WHERE PHYS_EID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PHYSSUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PHYSSUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [PHYSSUPP_TEMP](PHYS_EID" + sBitFields + ")"
                            + " VALUES(" + p_lRecordID.ToString() + sBitValues + ")";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_objDisplayTables, "ADDRESS_X_PHONEINFO"))
                {
                    string sFieldName = string.Empty;
                    string sFieldPos = string.Empty;

                    iIdx = Generic.IndexOf(p_objDisplayTables, "ADDRESS_X_PHONEINFO");
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("|");
                    sFieldName = p_objDisplayFields[iIdx].ToString().Substring(0, iPos - 1);
                    sFieldPos = p_objDisplayFields[iIdx].ToString().Substring(iPos);
                    sbSql.Length = 0;

                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[PHONE_TEMP]", "PHYS_EID");
                    //sSQL = "SELECT [QPHYS_TEMP].PHYS_EID," + p_objDisplayFields[iIdx].ToString()
                    //        + " [INTO PHONE_TEMP]"
                    //        + " FROM [QPHYS_TEMP],ADDRESS_X_PHONEINFO 
                    //        + " WHERE [QPHYS_TEMP].PHYS_EID=ADDRESS_X_PHONEINFO.ENTITY_ID";

                    sbSql.AppendFormat("SELECT ENTITY_ID PHYS_EID,{0} {1}'('{1}CODE_DESC{1}')' {2}", sFieldName, sConcatOperator, sFieldPos);
                    sbSql.Append(" [INTO PHONE_TEMP]");
                    sbSql.AppendFormat(" FROM ADDRESS_X_PHONEINFO,CODES_TEXT WHERE ENTITY_ID = {0}", p_lRecordID.ToString());
                    sbSql.Append(" AND CODES_TEXT.CODE_ID=ADDRESS_X_PHONEINFO.PHONE_CODE");
                    sSQL = sbSql.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    //If nothing went in, fill with empty record(s) so that inner join doesn't fail
                    if (m_objGeneric.GetMergeCount("[PHONE_TEMP]") == 0)
                    {
                        sFieldValues = "";
                        sFieldNames = "";
                        sbSql.Length = 0;

                        m_objGeneric.FindFields("[PHONE_TEMP]", ref sFieldNames, ref sFieldValues);

                        sbSql.AppendFormat("INSERT INTO [PHONE_TEMP](PHYS_EID{0}) VALUES", sFieldNames);
                        sbSql.AppendFormat(" ({0}{1})", p_lRecordID.ToString(), sFieldValues);
                        sSQL = sbSql.ToString();

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                AddOrgPhoneQuery("PHYS_PHN_", 8, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PHYS_PHN_", 7, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PHYS_PHN_", 6, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PHYS_PHN_", 5, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PHYS_PHN_", 4, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PHYS_PHN_", 3, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PHYS_PHN_", 2, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("PHYS_PHN_", 1, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                if (Utilities.ExistsInArray(p_objDisplayTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_objDisplayTables, "ADDRESS"))//RMA-8753 nshah28
                {
                    if (objSettings.UseMultipleAddresses)
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "ENTITY_X_ADDRESSES");
                        Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[ADD_TEMP]", "PHYS_EID");
                        //sSQL = "SELECT [QPHYS_TEMP].PHYS_EID," + p_objDisplayFields[iIdx].ToString()
                        //        + " [INTO ADD_TEMP]"
                        //        + " FROM [QPHYS_TEMP],ENTITY_X_ADDRESSES 
                        //        + " WHERE [QPHYS_TEMP].PHYS_EID=ENTITY_X_ADDRESSES.ENTITY_ID";

                        sbSql.Length = 0;
                       // sbSql.AppendFormat("SELECT ENTITY_ID PHYS_EID,{0}", p_objDisplayFields[iIdx].ToString());
                        //RMA-8753 nshah28 start
                        iIdxAddr = Generic.IndexOf(p_objDisplayTables, "ADDRESS");
                        sbSql.AppendFormat("SELECT ENTITY_ID PHYS_EID");
                        if (iIdx > -1)
                            sbSql.AppendFormat(" ,{0}", p_objDisplayFields[iIdx].ToString());
                        if (iIdxAddr > -1)
                            sbSql.AppendFormat(" ,{0}", p_objDisplayFields[iIdxAddr].ToString());
                        //RMA-8753 nshah28 end
                        sbSql.Append(" [INTO ADD_TEMP]");
                        //sbSql.AppendFormat(" FROM ENTITY_X_ADDRESSES WHERE ENTITY_ID = {0}", p_lRecordID.ToString());
                        sbSql.AppendFormat(" FROM ENTITY_X_ADDRESSES, ADDRESS WHERE ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID AND ENTITY_ID = {0}", p_lRecordID.ToString());
                        //RMA-8753 nshah28 
                        sSQL = sbSql.ToString();

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        //If nothing went in, fill with empty record(s) so that inner join doesn't fail
                        if (m_objGeneric.GetMergeCount("[ADD_TEMP]") == 0)
                        {
                            sFieldValues = "";
                            sFieldNames = "";
                            sbSql.Length = 0;

                            m_objGeneric.FindFields("[ADD_TEMP]", ref sFieldNames, ref sFieldValues);

                            sbSql.AppendFormat("INSERT INTO [ADD_TEMP](PHYS_EID{0}) VALUES", sFieldNames);
                            sbSql.AppendFormat(" ({0}{1})", p_lRecordID.ToString(), sFieldValues);
                            sSQL = sbSql.ToString();

                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                }
                //Added Rakhi for R7:Add Emp Data Elements
                //PHYSICIAN Privileges
                if (Utilities.ExistsInArray(p_objDisplayTables, "PHYS_PRIVS"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PHYS_PRIVS");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[PRIV_TEMP]", "PHYS_EID");
                    sSQL = "SELECT PHYS_EID," + p_objDisplayFields[iIdx].ToString() + " [INTO PRIV_TEMP]"
                        + " FROM PHYS_PRIVS WHERE PHYS_EID = " + p_lRecordID.ToString();
                    //Shruti, 9168
                    sKeyField = sKeyField + Generic.Append(p_objDisplayFields[iIdx].ToString());
                    //Shruti, 9168 ends

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PRIV_TEMP]") == 0)
                    {
                        //Shruti, 9168
                        if (sKeyField != "")
                        {
                            string[] arrFields = m_objGeneric.SafeSQL(sKeyField).Split(',');
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if (arrFields[i].Contains("PHYS_PRIVS.PRIV_ID") || arrFields[i].Contains("PHYS_PRIVS.CATEGORY_CODE")
                                    || arrFields[i].Contains("PHYS_PRIVS.TYPE_CODE") || arrFields[i].Contains("PHYS_PRIVS.STATUS_CODE"))
                                {
                                    string[] arrGetField = arrFields[i].Split(' ');
                                    if (arrGetField.Length == 2)
                                    {
                                        sField += "," + arrGetField[1];
                                        sValue += "," + 0;
                                    }
                                }
                            }
                        }
                        m_objGeneric.FindBitFields("[PRIV_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [PRIV_TEMP](PHYS_EID";
                        sSQL = sField == "" ? sSQL + sBitFields + ") SELECT PHYS_EID" + sBitValues + " FROM [QPHYS_TEMP]" : sSQL +
                            sField + sBitFields + ") SELECT PHYS_EID" + sValue + sBitValues + " FROM [QPHYS_TEMP]";
                        //sSQL = "INSERT INTO [PRIV_TEMP](PHYS_EID" + sBitFields + ")" 
                        //    +" Values (" + p_lRecordID.ToString() + sBitValues + ")";
                        //Shruti, 9168 ends
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Certifications
                sKeyField = "";
                sField = "";
                sValue = "";
                if (Utilities.ExistsInArray(p_objDisplayTables, "PHYS_CERTS"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PHYS_CERTS");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[CERT_TEMP]", "PHYS_EID");
                    //Shruti, 9168
                    sKeyField = sKeyField + Generic.Append(p_objDisplayFields[iIdx].ToString());
                    //Shruti, 9168 ends

                    sSQL = "SELECT PHYS_EID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO CERT_TEMP]"
                        + " FROM PHYS_CERTS WHERE PHYS_EID = "
                        + p_lRecordID.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CERT_TEMP]") == 0)
                    {
                        //Shruti, 9168
                        if (sKeyField != "")
                        {
                            string[] arrFields = m_objGeneric.SafeSQL(sKeyField).Split(',');
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if (arrFields[i].Contains("PHYS_CERTS.CERT_ID") || arrFields[i].Contains("PHYS_CERTS.NAME_CODE")
                                    || arrFields[i].Contains("PHYS_CERTS.STATUS_CODE") || arrFields[i].Contains("PHYS_CERTS.BOARD_CODE"))
                                {
                                    string[] arrGetField = arrFields[i].Split(' ');
                                    if (arrGetField.Length == 2)
                                    {
                                        sField += "," + arrGetField[1];
                                        sValue += "," + 0;
                                    }
                                }
                            }
                        }
                        m_objGeneric.FindBitFields("[CERT_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [CERT_TEMP](PHYS_EID";
                        sSQL = sField == "" ? sSQL + sBitFields + ") SELECT PHYS_EID" + sBitValues + " FROM [QPHYS_TEMP]" : sSQL +
                            sField + sBitFields + ") SELECT PHYS_EID" + sValue + sBitValues + " FROM [QPHYS_TEMP]";
                        //sSQL = "INSERT INTO [CERT_TEMP](PHYS_EID" + sBitFields + ")" 
                        //    +" Values (" + p_lRecordID.ToString() + sBitValues + ")";
                        //Shruti, 9168 ends
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Education

                sKeyField = "";
                sField = "";
                sValue = "";
                if (Utilities.ExistsInArray(p_objDisplayTables, "PHYS_EDUCATION"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PHYS_EDUCATION");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[EDUC_TEMP]", "PHYS_EID");
                    //Shruti, 9168
                    sKeyField = sKeyField + Generic.Append(p_objDisplayFields[iIdx].ToString());
                    //Shruti, 9168 ends
                    sSQL = "SELECT PHYS_EID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO EDUC_TEMP]"
                        + " FROM PHYS_EDUCATION WHERE PHYS_EID = "
                        + p_lRecordID.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[EDUC_TEMP]") == 0)
                    {
                        //Shruti, 9168
                        if (sKeyField != "")
                        {
                            string[] arrFields = m_objGeneric.SafeSQL(sKeyField).Split(',');
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                //if (arrFields[i].Contains("PHYS_EDUCATION.EDUC_ID") || arrFields[i].Contains("EDUC_TYPE_CODE.NAME_CODE") //Mits PHYS_EDUCATION.EDUC_TYPE_CODE and PHYS_EID cannot be set to null
                                if (arrFields[i].Contains("PHYS_EDUCATION.EDUC_ID") || arrFields[i].Contains("PHYS_EDUCATION.EDUC_TYPE_CODE") || arrFields[i].Contains("PHYS_EDUCATION.PHYS_EID")
                                    || arrFields[i].Contains("PHYS_EDUCATION.INSTITUTION_EID") || arrFields[i].Contains("PHYS_EDUCATION.DEGREE_TYPE"))
                                {
                                    string[] arrGetField = arrFields[i].Split(' ');
                                    if (arrGetField.Length == 2)
                                    {
                                        sField += "," + arrGetField[1];
                                        sValue += "," + 0;
                                    }
                                }
                            }
                        }

                        m_objGeneric.FindBitFields("[EDUC_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [EDUC_TEMP](PHYS_EID";
                        sSQL = sField == "" ? sSQL + sBitFields + ") SELECT PHYS_EID" + sBitValues + " FROM [QPHYS_TEMP]" : sSQL +
                            sField + sBitFields + ") SELECT PHYS_EID" + sValue + sBitValues + " FROM [QPHYS_TEMP]";
                        //sSQL = "INSERT INTO [EDUC_TEMP](PHYS_EID" + sBitFields + ")" 
                        //    +" Values (" + p_lRecordID.ToString() + sBitValues + ")";
                        //Shruti, 9168 ends
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Previous hospitals
                if (Utilities.ExistsInArray(p_objDisplayTables, "PHYS_PREV_HOSP")
                    || Utilities.ExistsInArray(p_objDisplayTables, "PHYS_PREV_HOSP_ENT"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PHYS_PREV_HOSP");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "PHYS_PREV_HOSP_ENT");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[PHOSP_TEMP]", "PHYS_EID");
                    sSQL = "SELECT PHYS_EID,";
                    if (iIdx != -1)
                    {
                        sSQL = sSQL + p_objDisplayFields[iIdx].ToString();
                    }

                    if (iIdxNext != -1)
                    {
                        if (iIdx != -1)
                        {
                            if (p_objDisplayFields[iIdx].ToString() != "")
                            {
                                sSQL = sSQL + ",";
                            }
                        }
                        sSQL = sSQL + p_objDisplayFields[iIdxNext].ToString();
                    }
                    sSQL = sSQL + " [INTO PHOSP_TEMP]"
                    + " FROM PHYS_PREV_HOSP";
                    if (iIdxNext != -1)
                    {
                        sSQL = sSQL + ", ENTITY PHYS_PREV_HOSP_ENT " +
                            " WHERE PHYS_PREV_HOSP.PHYS_EID = " + p_lRecordID.ToString();
                    }
                    if (iIdxNext != -1)
                    {
                        sSQL = sSQL + " AND PHYS_PREV_HOSP_ENT.ENTITY_ID = PHYS_PREV_HOSP.HOSPITAL_EID";
                    }

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PHOSP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PHOSP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [PHOSP_TEMP](PHYS_EID" + sBitFields + ")"
                            + " Values (" + p_lRecordID.ToString() + sBitValues + ")";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //SUB SPECIALTY
                if (Utilities.ExistsInArray(p_objDisplayTables, "PHYS_SUB_SPECIALTY"))
                {
                    objTempValues = new ArrayList();
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PHYS_SUB_SPECIALTY");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[QPHYS_TEMP]", "[SUBSPEC_TEMP]", "PHYS_EID");
                    //Shruti, 9168
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("FLD");
                    sTmp = p_objDisplayFields[iIdx].ToString().Substring(iPos);
                    //Shruti, 9168 ends
                    switch (p_ldbMake)
                    {
                        case (int)eDatabaseType.DBMS_IS_ACCESS:
                            sSQL = "CREATE TABLE [SUBSPEC_TEMP] (PHYS_EID LONG, " + sTmp + " TEXT(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_SQLSRVR:
                            sSQL = "CREATE TABLE [SUBSPEC_TEMP] (PHYS_EID int, " + sTmp + " varchar(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_SYBASE:
                            sSQL = "CREATE TABLE [SUBSPEC_TEMP] (PHYS_EID int, " + sTmp + " varchar(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_INFORMIX:
                            sSQL = "CREATE TABLE [SUBSPEC_TEMP] (PHYS_EID INTEGER, " + sTmp + " VARCHAR(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_DB2:
                            sSQL = "CREATE TABLE [SUBSPEC_TEMP] (PHYS_EID INTEGER, " + sTmp + " VARCHAR(250))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_ORACLE:
                            m_objGeneric.DeleteOracleTables("DROP TABLE [SUBSPEC_TEMP]", "[SUBSPEC_TEMP]");
                            sSQL = "CREATE TABLE [SUBSPEC_TEMP] (PHYS_EID NUMBER(10), " + sTmp + " VARCHAR(250))";
                            break;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    //Shruti, 9168
                    sSQL = m_objGeneric.SafeSQL("SELECT PHYS_EID FROM [QPHYS_TEMP]");
                    //Shruti, 9168 ends
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        sTmpNext = "";
                        sSQL = "SELECT SPECIALTY_CODE FROM PHYS_SUB_SPECIALTY WHERE PHYS_EID =  "
                            + Conversion.ConvertObjToStr(objReader.GetValue(0))
                            + " AND SPECIALTY_CODE <> 0";
                        objReaderNext = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        while (objReaderNext.Read())
                        {
                            if (sTmpNext != "")
                            {
                                sTmpNext = sTmpNext + ", ";
                            }
                            objCache = new LocalCache(m_sConnectionString, m_iClientId);
                            sTmpNext = sTmpNext + objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReaderNext.GetValue(0))))
                                + "_" + objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReaderNext.GetValue(0))));
                            //MITS 14621 : Umesh 
                            //This was issue of connection pooling. 
                            objCache.Dispose();
                            //Shruti, 9168
                            if (sTmpNext.Length > 250)
                            {
                                sTmp = sTmp.Substring(0, 250);
                            }
                            sSQL = "INSERT INTO [SUBSPEC_TEMP](PHYS_EID," + sTmp + ") VALUES ("
                                + Conversion.ConvertObjToStr(objReader.GetValue(0)) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
                            objTempValues.Add(sSQL);
                            sTmpNext = "";//Added by sharishkumar for Mits 35353
                        }
                        //MITS 14621 : Umesh 
                        //This was issue of connection pooling. 
                        objReaderNext.Close();
                        //Shruti, 9168 ends
                    }
                    for (int i = 0; i < objTempValues.Count; i++)
                    {
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(objTempValues[i].ToString());
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                    objTempValues = null;

                    if (m_objGeneric.GetMergeCount("[SUBSPEC_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[SUBSPEC_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [SUBSPEC_TEMP](PHYS_EID" + sBitFields + ")"
                            + " Values (" + p_lRecordID.ToString() + sBitValues + ")";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Following function calls create dynamic query related to dept,company etc.
                QOrgHQuery("PHYS_", 8, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PHYS_", 7, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PHYS_", 6, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PHYS_", 5, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PHYS_", 4, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PHYS_", 3, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PHYS_", 2, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("PHYS_", 1, "PHYSICIAN", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                sSQL = "Select " + p_sJustFieldNames
                    + " FROM " + p_sFrom;
                if (p_sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + p_sWhere;
                }
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);

                using (Merge objMerge = new Merge(m_iClientId))
                {
                    objMerge.Command = m_objMergeCmd;
                }
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupPhysicianMerge();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoPhysicianMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objReaderNext != null)
                {
                    objReaderNext.Close();
                    objReaderNext.Dispose();
                }
                sSQL = null;
                sBitFields = null;
                sBitValues = null;
                sFieldNames = null;
                sFieldValues = null;
                sTmp = null;
                sTmpNext = null;
                objTempValues = null;
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
            }
        }
        #endregion

        #region Medical Staff Merge
        /// Name		: DoMedStaffMergeQuery
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to medical staff
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        internal void DoMedStaffMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, ref string p_sFrom, ref string p_sWhere
            , bool bMergeAll, long p_ldbMake)
        {
            string sSQL = "";
            Merge objMerge = null;
            int iIdx = 0;
            int iIdxAddr = 0; //RMA-8753 nshah28 
            int iIdxNext = 0;
            string sBitFields = "";
            string sBitValues = "";
            DbReader objReader = null;
            string sFieldNames = string.Empty;
            string sFieldValues = string.Empty;
            try
            {
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud
                if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupMedStaffMerge();
                }
                //MEDICAL STAFF
                if (Utilities.ExistsInArray(p_objDisplayTables, "MED_STAFF")
                    || Utilities.ExistsInArray(p_objDisplayTables, "MED_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "MED_STAFF");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "MED_ENTITY");
                    sSQL = "SELECT MED_STAFF.STAFF_EID ";
                    if (iIdx != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    sSQL = sSQL + " [INTO MED_TEMP]"
                        + " FROM MED_STAFF,ENTITY MED_ENTITY WHERE MED_STAFF.STAFF_EID = " + p_lRecordID.ToString()
                        + " AND MED_STAFF.STAFF_EID = MED_ENTITY.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                else
                {
                    sSQL = "SELECT STAFF_EID [INTO MED_TEMP] FROM MED_STAFF WHERE STAFF_EID = "
                        + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                p_sFrom = "[MED_TEMP]";
                p_sWhere = "";
                //MEDICAL STAFF SUPPLEMENTAL
                if (Utilities.ExistsInArray(p_objDisplayTables, "MED_STAFF_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "MED_STAFF_SUPP");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[MED_TEMP]", "[MEDSUPP_TEMP]", "STAFF_EID");
                    sSQL = "SELECT MED_STAFF_SUPP.STAFF_EID," + p_objDisplayFields[iIdx].ToString() + " [INTO MEDSUPP_TEMP]"
                        + " FROM MED_STAFF_SUPP WHERE STAFF_EID =  " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[MEDSUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[MEDSUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [MEDSUPP_TEMP](STAFF_EID" + sBitFields + ")"
                            + " VALUES(" + p_lRecordID.ToString() + sBitValues + ")";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //MEDICAL STAFF Privileges
                if (Utilities.ExistsInArray(p_objDisplayTables, "STAFF_PRIVS"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "STAFF_PRIVS");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[MED_TEMP]", "[SPRIV_TEMP]", "STAFF_EID");
                    sSQL = "SELECT STAFF_EID," + p_objDisplayFields[iIdx].ToString() + " [INTO SPRIV_TEMP]"
                        + " FROM STAFF_PRIVS WHERE STAFF_EID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[SPRIV_TEMP]") == 0)
                    {
                        //pmittal5 Mits 18595
                        //m_objGeneric.FindBitFields("[SPRIV_TEMP]", ref sBitFields, ref sBitValues);
                        //sSQL = "INSERT INTO [SPRIV_TEMP](STAFF_EID" + sBitFields + ")" 
                        //    +" (" + p_lRecordID.ToString() + sBitValues + ")";
                        sFieldValues = "";
                        sFieldNames = "";
                        m_objGeneric.FindFields("[SPRIV_TEMP]", ref sFieldNames, ref sFieldValues);
                        sSQL = "INSERT INTO [SPRIV_TEMP](STAFF_EID" + sFieldNames + ") VALUES"
                            + " (" + p_lRecordID.ToString() + sFieldValues + ")";
                        //End - pmittal5
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Certifications
                if (Utilities.ExistsInArray(p_objDisplayTables, "STAFF_CERTS"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "STAFF_CERTS");
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[MED_TEMP]", "[SCERT_TEMP]", "STAFF_EID");
                    sSQL = "SELECT STAFF_EID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO SCERT_TEMP]"
                        + " FROM STAFF_CERTS WHERE STAFF_EID = "
                        + p_lRecordID.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[SCERT_TEMP]") == 0)
                    {
                        //pmittal5 Mits 18595
                        //m_objGeneric.FindBitFields("[SCERT_TEMP]", ref sBitFields, ref sBitValues);
                        //sSQL = "INSERT INTO [SCERT_TEMP](STAFF_EID" + sBitFields + ")" 
                        //    +" (" + p_lRecordID.ToString() + sBitValues + ")";
                        sFieldValues = "";
                        sFieldNames = "";
                        m_objGeneric.FindFields("[SCERT_TEMP]", ref sFieldNames, ref sFieldValues);
                        sSQL = "INSERT INTO [SCERT_TEMP](STAFF_EID" + sFieldNames + ") VALUES"
                            + " (" + p_lRecordID.ToString() + sFieldValues + ")";
                        //End - pmittal5
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_objDisplayTables, "ADDRESS_X_PHONEINFO"))
                {
                    int iPos = 0;
                    string sFieldName = string.Empty;
                    string sFieldPos = string.Empty;
                    sbSql.Length = 0;

                    iIdx = Generic.IndexOf(p_objDisplayTables, "ADDRESS_X_PHONEINFO");
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("|");
                    sFieldName = p_objDisplayFields[iIdx].ToString().Substring(0, iPos - 1);
                    sFieldPos = p_objDisplayFields[iIdx].ToString().Substring(iPos);

                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[MED_TEMP]", "[PHONE_TEMP]", "STAFF_EID");

                    sbSql.AppendFormat("SELECT ENTITY_ID STAFF_EID,{0} {1}'('{1}CODE_DESC{1}')' {2}", sFieldName, sConcatOperator, sFieldPos);
                    sbSql.Append(" [INTO PHONE_TEMP]");
                    sbSql.AppendFormat(" FROM ADDRESS_X_PHONEINFO,CODES_TEXT WHERE ENTITY_ID = {0}", p_lRecordID.ToString());
                    sbSql.Append(" AND CODES_TEXT.CODE_ID=ADDRESS_X_PHONEINFO.PHONE_CODE");
                    sSQL = sbSql.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    //If nothing went in, fill with empty record(s) so that inner join doesn't fail
                    if (m_objGeneric.GetMergeCount("[PHONE_TEMP]") == 0)
                    {
                        sFieldValues = "";
                        sFieldNames = "";
                        sbSql.Length = 0;
                        m_objGeneric.FindFields("[PHONE_TEMP]", ref sFieldNames, ref sFieldValues);

                        sbSql.AppendFormat("INSERT INTO [PHONE_TEMP](STAFF_EID{0}) VALUES", sFieldNames);
                        sbSql.AppendFormat(" ({0}{1})", p_lRecordID.ToString(), sFieldValues);
                        sSQL = sbSql.ToString();
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                AddOrgPhoneQuery("MED_PHN_", 8, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("MED_PHN_", 7, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("MED_PHN_", 6, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("MED_PHN_", 5, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("MED_PHN_", 4, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("MED_PHN_", 3, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("MED_PHN_", 2, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                AddOrgPhoneQuery("MED_PHN_", 1, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                if (Utilities.ExistsInArray(p_objDisplayTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_objDisplayTables, "ADDRESS"))//RMA-8753 nshah28 
                {
                    if (objSettings.UseMultipleAddresses)
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "ENTITY_X_ADDRESSES");
                        Generic.ChainIt(ref p_sFrom, ref p_sWhere, "[MED_TEMP]", "[ADD_TEMP]", "STAFF_EID");
                        sbSql.Length = 0;
                        //sbSql.AppendFormat("SELECT ENTITY_ID STAFF_EID,{0}", p_objDisplayFields[iIdx].ToString());
                        //RMA-8753 nshah28 start
                        iIdxAddr = Generic.IndexOf(p_objDisplayTables, "ADDRESS");
                        sbSql.AppendFormat("SELECT ENTITY_ID STAFF_EID");
                        if (iIdx > -1)
                            sbSql.AppendFormat(" ,{0}", p_objDisplayFields[iIdx].ToString());
                        if (iIdxAddr > -1)
                            sbSql.AppendFormat(" ,{0}", p_objDisplayFields[iIdxAddr].ToString());
                        //RMA-8753 nshah28 end

                        sbSql.Append(" [INTO ADD_TEMP]");
                        //sbSql.AppendFormat(" FROM ENTITY_X_ADDRESSES WHERE ENTITY_ID = {0}", p_lRecordID.ToString());
                        //RMA-8753 nshah28
                        sbSql.AppendFormat(" FROM ENTITY_X_ADDRESSES, ADDRESS WHERE ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID   AND  ENTITY_ID = {0}", p_lRecordID.ToString());
                        sSQL = sbSql.ToString();

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        //If nothing went in, fill with empty record(s) so that inner join doesn't fail
                        if (m_objGeneric.GetMergeCount("[ADD_TEMP]") == 0)
                        {
                            sFieldValues = "";
                            sFieldNames = "";
                            sbSql.Length = 0;
                            m_objGeneric.FindFields("[ADD_TEMP]", ref sFieldNames, ref sFieldValues);

                            sbSql.AppendFormat("INSERT INTO [ADD_TEMP](STAFF_EID{0}) VALUES", sFieldNames);
                            sbSql.AppendFormat(" ({0}{1})", p_lRecordID.ToString(), sFieldValues);
                            sSQL = sbSql.ToString();
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                }
                //Added Rakhi for R7:Add Emp Data Elements
                //Following functions calls create dynamic query related to dept,company etc.
                QOrgHQuery("MED_", 8, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("MED_", 7, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("MED_", 6, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("MED_", 5, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("MED_", 4, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("MED_", 3, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("MED_", 2, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                QOrgHQuery("MED_", 1, "MED_STAFF", p_objDisplayTables, p_objDisplayFields, ref p_sFrom, ref p_sWhere, p_lRecordID);
                sSQL = "Select " + p_sJustFieldNames
                    + " FROM " + p_sFrom;
                if (p_sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + p_sWhere;
                }
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupMedStaffMerge();

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoMedStaffMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;
                sBitFields = null;
                sBitValues = null;
                sFieldNames = null;
                sFieldValues = null;
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
            }
        }
        #endregion

        # region Common Function used in Patient,Physician,Medical Staff Merge
        /// <summary>
        /// Gets data related to PATIENT,PHYSICIAN,MEDICAL STAFF
        /// </summary>
        /// <param name="p_sPrefix">Prefix like PAT_ etc.</param>
        /// <param name="p_iLevel">Level</param>
        /// <param name="p_sChild">Table name like PATIENT,PHYSICIAN</param>
        /// <param name="p_objDisplayTables">Tables from which to get the desired data</param>
        /// <param name="p_objDisplayFields">Fields from which to get the desired data</param>
        /// <param name="p_sFrom">From clause</param>
        /// <param name="p_sWhere">Where clause</param>
        /// <param name="p_lRecordID">Record id</param>
        internal void QOrgHQuery(string p_sPrefix, int p_iLevel, string p_sChild, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            ref string p_sFrom, ref  string p_sWhere, long p_lRecordID)
        {
            string sTableName = "";
            string sDeptField = "";
            string sTempTable = "";
            int iIdx = 0;
            string sSQL = "";
            string sBitFields = "";
            string sBitValues = "";
            string sPKField = "";
            DbConnection objConn = null;
            DbReader objReader = null;
            try
            {
                switch (p_sChild)
                {
                    case "PATIENT":
                        sDeptField = "FACILITY_DEPT_EID";
                        sTempTable = "[PAT_TEMP]";
                        sPKField = "PATIENT_ID";
                        break;
                    case "PHYSICIAN":
                        sDeptField = "DEPT_ASSIGNED_EID";
                        sTempTable = "[QPHYS_TEMP]";
                        sPKField = "PHYS_EID";
                        break;
                    case "MED_STAFF":
                        sDeptField = "DEPT_ASSIGNED_EID";
                        sPKField = "STAFF_EID";
                        sTempTable = "[MED_TEMP]";
                        break;
                }
                if (p_iLevel == 1)
                {
                    sTableName = p_sPrefix + "CLNT";
                }
                else if (p_iLevel == 2)
                {
                    sTableName = p_sPrefix + "COMP";
                }
                else if (p_iLevel == 3)
                {
                    sTableName = p_sPrefix + "OP";
                }
                else if (p_iLevel == 4)
                {
                    sTableName = p_sPrefix + "REG";
                }
                else if (p_iLevel == 5)
                {
                    sTableName = p_sPrefix + "DIV";
                }
                else if (p_iLevel == 6)
                {
                    sTableName = p_sPrefix + "LOC";
                }
                else if (p_iLevel == 7)
                {
                    sTableName = p_sPrefix + "FAC";
                }
                else if (p_iLevel == 8)
                {
                    sTableName = p_sPrefix + "DEPT";
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, sTableName))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, sTableName);
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, sTempTable, "[" + sTableName + "_TEMP]", sPKField);
                    sSQL = "SELECT " + p_sChild + "." + sPKField + "," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO " + sTableName + "_TEMP]"
                        + " FROM " + p_sChild + ",ENTITY " + p_sPrefix + "DEPT";
                    if (p_iLevel < 8)
                    {
                        sSQL = sSQL + ",ENTITY " + p_sPrefix + "FAC";
                    }
                    if (p_iLevel < 7)
                    {
                        sSQL = sSQL + ",ENTITY " + p_sPrefix + "LOC";
                    }
                    if (p_iLevel < 6)
                    {
                        sSQL = sSQL + ",ENTITY " + p_sPrefix + "DIV";
                    }
                    if (p_iLevel < 5)
                    {
                        sSQL = sSQL + ",ENTITY " + p_sPrefix + "REG";
                    }
                    if (p_iLevel < 4)
                    {
                        sSQL = sSQL + ",ENTITY " + p_sPrefix + "OP";
                    }
                    if (p_iLevel < 3)
                    {
                        sSQL = sSQL + ",ENTITY " + p_sPrefix + "COMP";
                    }
                    if (p_iLevel < 2)
                    {
                        sSQL = sSQL + ",ENTITY " + p_sPrefix + "CLNT";
                    }
                    sSQL = sSQL + " WHERE " + p_sChild + "." + sPKField + " = " + p_lRecordID.ToString()
                        + " AND " + p_sChild + "." + sDeptField + " = " + p_sPrefix + "DEPT.ENTITY_ID";
                    if (p_iLevel < 8)
                    {
                        sSQL = sSQL + " AND " + p_sPrefix + "DEPT.PARENT_EID = " + p_sPrefix + "FAC.ENTITY_ID";
                    }
                    if (p_iLevel < 7)
                    {
                        sSQL = sSQL + " AND " + p_sPrefix + "FAC.PARENT_EID = " + p_sPrefix + "LOC.ENTITY_ID";
                    }
                    if (p_iLevel < 6)
                    {
                        sSQL = sSQL + " AND " + p_sPrefix + "LOC.PARENT_EID = " + p_sPrefix + "DIV.ENTITY_ID";
                    }
                    if (p_iLevel < 5)
                    {
                        sSQL = sSQL + " AND " + p_sPrefix + "DIV.PARENT_EID = " + p_sPrefix + "REG.ENTITY_ID";
                    }
                    if (p_iLevel < 4)
                    {
                        sSQL = sSQL + " AND " + p_sPrefix + "REG.PARENT_EID = " + p_sPrefix + "OP.ENTITY_ID";
                    }
                    if (p_iLevel < 3)
                    {
                        sSQL = sSQL + " AND " + p_sPrefix + "OP.PARENT_EID = " + p_sPrefix + "COMP.ENTITY_ID";
                    }
                    if (p_iLevel < 2)
                    {
                        sSQL = sSQL + " AND " + p_sPrefix + "COMP.PARENT_EID = " + p_sPrefix + "CLNT.ENTITY_ID";
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[" + sTableName + "_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[" + sTableName + "_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [" + sTableName + "_TEMP](" + sPKField + sBitFields + ")"
                            + " SELECT " + sPKField + sBitValues + " FROM " + sTempTable;
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.QOrgHQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                sTableName = null;
                sDeptField = null;
                sTempTable = null;
                sSQL = null;
                sBitFields = null;
                sBitValues = null;
                sPKField = null;
            }
        }
        #endregion

        #region Payment Merge
        /// Name		: DoPaymentMergeQuery
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to payment
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        internal void DoPaymentMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, long p_ldbMake)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            int iIdx = 0;
            Merge objMerge = null;
            DbReader objReader = null;
            try
            {
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud

                //MGaba2-MITS 12198-commenting following line as 
                //Table FUNDS is added in "sFrom" just before adding FUNDS_SUPP- for outer join
                //sFrom = "FUNDS";
                sWhere = "FUNDS.TRANS_ID = " + p_lRecordID.ToString();
                //FUNDS
                //Shruti for adding Funds Supplemenal fields in Funds Merge
                if (Utilities.ExistsInArray(p_objDisplayTables, "FUNDS"))
                {
                    sSelect = "SELECT " + p_objDisplayFields[Generic.IndexOf(p_objDisplayTables, "FUNDS")].ToString();

                    //**************************************************************************************************************************************************
                    //**************************************************************************************************************
                    if (objSettings.UseMultiCurrency != 0 && (p_objDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT") > 0 || p_objDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT") > 0))
                    {
                        if (p_objDisplayFields[iIdx].ToString().Contains("CLAIM_CURRENCY_AMOUNT"))
                        {
                            sSelect = sSelect.Replace("Funds.CLAIM_CURRENCY_AMOUNT", "CONVERT(varchar(30),FUNDS.CLAIM_CURRENCY_AMOUNT )" + "+'_'+" + "CONVERT(varchar(30),FUNDS.CLAIM_CURRENCY_CODE )");
                        }

                        if (p_objDisplayFields[iIdx].ToString().Contains("PMT_CURRENCY_AMOUNT"))
                        {
                            sSelect = sSelect.Replace("Funds.PMT_CURRENCY_AMOUNT", "CONVERT(varchar(30),FUNDS.PMT_CURRENCY_AMOUNT )" + "+'_'+" + "CONVERT(varchar(30),FUNDS.PMT_CURRENCY_CODE )");
                        }
                    }


                    //
                }

                if (Utilities.ExistsInArray(p_objDisplayTables, "PAYEE_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PAYEE_ENTITY");
                    sSelect = sSelect + "," + p_objDisplayFields[iIdx].ToString();
                    //MGaba2-MITS 12198-added if condition-as funds table is yet not added to sFrom-Start
                    if (sFrom != "")
                    {
                        sFrom = sFrom + ", ";
                    }
                    //sFrom = sFrom + ", ENTITY PAYEE_ENTITY";
                    sFrom = sFrom + "ENTITY PAYEE_ENTITY";
                    //MGaba2-MITS 12198-End
                    if (sWhere != "")
                    {
                        sWhere = sWhere + " AND ";
                    }
                    sWhere = sWhere + "FUNDS.PAYEE_EID = PAYEE_ENTITY.ENTITY_ID";
                }
                //FUNDS TRANSACTION SPLIT
                if (Utilities.ExistsInArray(p_objDisplayTables, "FUNDS_TRANS_SPLIT"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "FUNDS_TRANS_SPLIT");
                    sSelect = sSelect + "," + p_objDisplayFields[iIdx].ToString();
                    //MGaba2-MITS 12198-as funds table is yet not added to sFrom-added if condition-Start
                    if (sFrom != "")
                    {
                        sFrom = sFrom + ", ";
                    }
                    // sFrom = sFrom + ", FUNDS_TRANS_SPLIT";
                    sFrom = sFrom + "FUNDS_TRANS_SPLIT";
                    //MGaba2-MITS 12198-End
                    if (sWhere != "")
                    {
                        sWhere = sWhere + " AND ";
                    }
                    sWhere = sWhere + "FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID";
                }
                //Shruti
                //Funds Supplemental
                //MGaba2-MITS 12198-Adding table FUNDS here in "sFrom",just bfore FUNDS_SUPP-to have outer join with FUNDS_SUPP
                if (sFrom != "")
                {
                    sFrom = sFrom + ", ";
                }
                sFrom = sFrom + "FUNDS";

                //MGaba2-MITS 12198-End
                if (Utilities.ExistsInArray(p_objDisplayTables, "FUNDS_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "FUNDS_SUPP");
                    sSelect = sSelect == "" ? "SELECT " + p_objDisplayFields[iIdx].ToString() : sSelect + "," + p_objDisplayFields[iIdx].ToString();
                    //MGaba2-MITS 12198-Payment Merge was returning blank record
                    //Commenting following code to convert inner to outer join

                    /* sFrom = sFrom == "" ? "FUNDS_SUPP" : sFrom + ", FUNDS_SUPP";
                    
                     if (sWhere != "")
                    {
                        sWhere = sWhere + " AND ";
                    }
                    sWhere = sWhere + "FUNDS_SUPP.TRANS_ID = " + p_lRecordID.ToString();
                     */
                    sFrom = sFrom + " LEFT OUTER JOIN FUNDS_SUPP ON FUNDS.TRANS_ID = FUNDS_SUPP.TRANS_ID";
                    //MGaba2-MITS 12198-End
                }


                sSQL = sSelect + " FROM " + sFrom + " ";
                if (sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + sWhere;
                }


                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoPaymentMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;
                sFrom = null;
                sWhere = null;
                sSelect = null;
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
            }
        }
        #endregion

        #region Policy Merge
        /// Name		: DoPolicyMergeQuery
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to policy
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        internal void DoPolicyMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, long p_ldbMake)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            int iIdx = 0;

            // npadhy Start MITS 18935 Added the case for Reinsurer and modified the logic for Insurer
            int iIdxInsurer = 0;
            int iIdxReinsurer = 0;
            // npadhy End MITS 18935 Added the case for Reinsurer and modified the logic for Insurer

            int iIdxNext = 0;
            string sBitFields = "";
            string sBitValues = "";
            //VehicleTempTable objTempTable;
            Merge objMerge = null;
            DbReader objReader = null;
            try
            {
                //objTempTable=new VehicleTempTable();
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupPolicyMerge();
                }


                //POLICY
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() +
                        " [INTO POLICY_TEMP] FROM POLICY WHERE POLICY_ID = " + p_lRecordID.ToString();
                }
                else
                {
                    sSQL = "SELECT POLICY_ID [INTO POLICY_TEMP] FROM POLICY WHERE POLICY_ID = "
                        + p_lRecordID.ToString();
                }
                m_objMergeCmd.CommandType = CommandType.Text;
                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                m_objMergeCmd.ExecuteNonQuery();
                sFrom = "[POLICY_TEMP]";
                sWhere = "";

                // npadhy Start MITS 18935 Added the case for Reinsurer and modified the logic for Insurer
                //INSURER
                if (Utilities.ExistsInArray(p_objDisplayTables, "INSURER_ENTITY") || Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_INSURER"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "INSURER_ENTITY");
                    iIdxInsurer = Utilities.IndexInArray(p_objDisplayTables, "POLICY_X_INSURER");

                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[INS_ENT_TEMP]", "POLICY_ID");

                    sSQL = "SELECT POLICY.POLICY_ID";

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_objDisplayFields[iIdx].ToString());
                    if (iIdxInsurer > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_objDisplayFields[iIdxInsurer].ToString());

                    sSQL = sSQL + " [INTO INS_ENT_TEMP]"
                    + " FROM POLICY,ENTITY INSURER_ENTITY, POLICY_X_INSURER"
                    + " WHERE POLICY.POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID"
                    + " AND POLICY_X_INSURER.INSURER_CODE = INSURER_ENTITY.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }

                // npadhy MITS Handling the case for Reinsurer
                if (Utilities.ExistsInArray(p_objDisplayTables, "REINSURER_ENTITY") || Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_INS_REINS"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "REINSURER_ENTITY");
                    iIdxReinsurer = Utilities.IndexInArray(p_objDisplayTables, "POLICY_X_INS_REINS");

                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[REINS_ENT_TEMP]", "POLICY_ID");

                    sSQL = "SELECT POLICY.POLICY_ID";

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_objDisplayFields[iIdx].ToString());
                    if (iIdxReinsurer > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_objDisplayFields[iIdxReinsurer].ToString());

                    sSQL = sSQL + " [INTO REINS_ENT_TEMP]"
                    + " FROM POLICY,ENTITY REINSURER_ENTITY, POLICY_X_INS_REINS, POLICY_X_INSURER"
                    + " WHERE POLICY.POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID"
                    + " AND POLICY_X_INSURER.IN_ROW_ID = POLICY_X_INS_REINS.POL_X_INS_ROW_ID"
                    + " AND POLICY_X_INS_REINS.REINSURER_EID = REINSURER_ENTITY.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                // npadhy End MITS 18935 Added the case for Reinsurer and modified the logic for Insurer

                //COVERAGE TYPE
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_CVG_TYPE"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_CVG_TYPE");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[CVG_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO CVG_TEMP]"
                    + " FROM POLICY_X_CVG_TYPE"
                    + " WHERE POLICY_X_CVG_TYPE.POLICY_ID = " + p_lRecordID.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CVG_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CVG_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [CVG_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //INSURED
                if (Utilities.ExistsInArray(p_objDisplayTables, "INSURED"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "INSURED");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[INSURED_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO INSURED_TEMP]"
                    + " FROM POLICY_X_INSURED,ENTITY INSURED WHERE POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_X_INSURED.INSURED_EID = INSURED.ENTITY_ID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[INSURED_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[INSURED_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [INSURED_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //POLICY MCO
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_MCO")
                    || Utilities.ExistsInArray(p_objDisplayTables, "MCO_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_MCO");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "MCO_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[MCO_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID";
                    if (iIdx != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    sSQL = sSQL + " [INTO MCO_TEMP]"
                    + " FROM POLICY_X_MCO,ENTITY MCO_ENTITY WHERE POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_X_MCO.MCO_EID = MCO_ENTITY.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[MCO_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[MCO_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [MCO_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }


                //Gagan Safeway Retrofit Policy Jursidiction : START
                //if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_STATE"))
                //{
                //    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_STATE");
                //    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[STATE_TEMP]", "POLICY_ID");
                //    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO STATE_TEMP]"
                //    + " FROM POLICY_X_STATE"
                //    + " WHERE POLICY_X_STATE.POLICY_ID = " + p_lRecordID.ToString();

                //    m_objMergeCmd.CommandType = CommandType.Text;
                //    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                //    m_objMergeCmd.ExecuteNonQuery();
                //    if (m_objGeneric.GetMergeCount("[STATE_TEMP]") == 0)
                //    {
                //        m_objGeneric.FindBitFields("[STATE_TEMP]", ref sBitFields, ref sBitValues);

                //        try
                //        {
                //            sSQL = "INSERT INTO [STATE_TEMP](POLICY_ID" + sBitFields + ")"
                //            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_TEMP]";
                //            m_objMergeCmd.CommandType = CommandType.Text;
                //            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                //            m_objMergeCmd.ExecuteNonQuery();
                //        }
                //        catch
                //        {
                //        }

                //    }
                //}


                //multi-valued items which will be concatenated together
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_STATE"))
                {
                    int iPos = 0;
                    string sTmp = "";
                    ArrayList arrlstTempValues = null;
                    DataSet objData = null;
                    DataSet objTempds = null;
                    string sTmpNext = "";
                    string sStateInfo = "";
                    LocalCache objCache = null;
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);
                    string sAbbreviation = "";
                    string sName = "";
                    string sState = "";
                    DbTransaction objTran = null;

                    arrlstTempValues = new ArrayList();
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_STATE");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[STATE_TEMP]", "POLICY_ID");

                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("FLD");
                    sTmp = p_objDisplayFields[iIdx].ToString().Substring(iPos);
                    switch (m_lDbMake)
                    {
                        case (int)eDatabaseType.DBMS_IS_ACCESS:
                            sSQL = "CREATE TABLE [STATE_TEMP] (POLICY_ID LONG, " + sTmp + " TEXT(2000))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_SQLSRVR:
                            sSQL = "CREATE TABLE [STATE_TEMP] (POLICY_ID int, " + sTmp + " varchar(2000))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_SYBASE:
                            sSQL = "CREATE TABLE [STATE_TEMP] (POLICY_ID int, " + sTmp + " varchar(2000))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_INFORMIX:
                            sSQL = "CREATE TABLE [STATE_TEMP] (POLICY_ID INTEGER, " + sTmp + " VARCHAR(2000))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_DB2:
                            sSQL = "CREATE TABLE [STATE_TEMP] (POLICY_ID INTEGER, " + sTmp + " VARCHAR(2000))";
                            break;
                        case (int)eDatabaseType.DBMS_IS_ORACLE:

                            m_objGeneric.DeleteOracleTables("DROP TABLE [STATE_TEMP]", "[STATE_TEMP]");
                            sSQL = "CREATE TABLE [STATE_TEMP] (POLICY_ID NUMBER(10), " + sTmp + " VARCHAR2(2000))";
                            break;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    sSQL = m_objGeneric.SafeSQL("SELECT POLICY_ID FROM [POLICY_TEMP]");
                    objData = null;
                    m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref objData);
                    for (int iCnt = 0; iCnt < objData.Tables[0].Rows.Count; iCnt++)
                    {
                        sTmpNext = "";

                        sSQL = "SELECT STATE_ID FROM POLICY_X_STATE WHERE POLICY_ID ="
                            + Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0])
                            + " AND STATE_ID <> 0";
                        m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref objTempds);
                        for (int i = 0; i < objTempds.Tables[0].Rows.Count; i++)
                        {
                            if (sTmpNext != "")
                            {
                                sTmpNext = sTmpNext + ", ";
                            }


                            //MITS MITS 27337    objCache.GetStateInfo((Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objTempds.Tables[0].Rows[i][0])))
                            //  ,ref sAbbreviation, ref sName);

                            //sState = /*sAbbreviation + "_" + */ sName;
                            sState = objTempds.Tables[0].Rows[i][0].ToString();

                            sTmpNext = sTmpNext + sState;
                        }

                        objTempds = null;

                        if (sTmpNext.Length > 2000)
                        {
                            sTmpNext = sTmpNext.Substring(0, 2000);
                        }


                        sSQL = "INSERT INTO [STATE_TEMP](POLICY_ID," + sTmp + ") VALUES ("
                            + Conversion.ConvertObjToStr(objData.Tables[0].Rows[iCnt][0]) + "," + Utilities.FormatSqlFieldValue(sTmpNext) + ")";
                        arrlstTempValues.Add(sSQL);

                    }

                    m_objMergeCmd.CommandType = CommandType.Text;

                    objTran = m_objMergeConn.BeginTransaction();
                    m_objMergeCmd.Transaction = objTran;
                    for (int i = 0; i < arrlstTempValues.Count; i++)
                    {
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(arrlstTempValues[i].ToString());
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                    objTran.Commit();
                    arrlstTempValues = null;
                }


                //Gagan Safeway Retrofit Policy Jursidiction : END



                //POLICY SUPPLEMENTAL
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[POLSUPP_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO POLSUPP_TEMP]"
                    + " FROM POLICY_SUPP"
                    + " WHERE POLICY_SUPP.POLICY_ID = " + p_lRecordID.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[POLSUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[POLSUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [POLSUPP_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                sSQL = "SELECT " + p_sJustFieldNames
                    + " FROM " + sFrom;
                if (sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + sWhere;
                }
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupPolicyMerge();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoPolicyMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;
                sFrom = null;
                sWhere = null;
                sBitFields = null;
                sBitValues = null;
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
            }

        }
        #endregion

        #region Entity Merge
        /// Name		: DoEntityMergeQuery
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to entity
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        internal void DoEntityMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, long p_ldbMake)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            int iIdx = 0;
            int iIdxAddr = 0; //RMA-8753 nshah28 
            string sFields = "";
            string sBitFields = "";
            string sBitValues = "";
            Merge objMerge = null;
            DbReader objReader = null;
            try
            {
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                objSettings = new SysSettings(m_sConnectionString, m_iClientId);//dvatsa-cloud
                if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupEntityMerge();
                }
                //ENTITY
                if (Utilities.ExistsInArray(p_objDisplayTables, "ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "ENTITY");
                    if (iIdx != -1)
                    {
                        sFields = p_objDisplayFields[iIdx].ToString();
                    }
                    sSQL = "SELECT ENTITY_ID," + sFields + " [INTO ENT_TEMP] FROM ENTITY "
                        + " WHERE ENTITY_ID = " + p_lRecordID.ToString();

                }
                else
                {
                    sSQL = "SELECT ENTITY_ID [INTO ENT_TEMP] FROM ENTITY WHERE ENTITY_ID = "
                        + p_lRecordID.ToString();
                }
                m_objMergeCmd.CommandType = CommandType.Text;
                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                m_objMergeCmd.ExecuteNonQuery();
                sFrom = "[ENT_TEMP]";
                sWhere = "";
                //Entity Contact Info - Shruti for MITS 11832
                if (Utilities.ExistsInArray(p_objDisplayTables, "ENT_X_CONTACTINFO"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "ENT_X_CONTACTINFO");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[ENT_TEMP]", "[ENT_CONTACT_TEMP]", "ENTITY_ID");
                    sSQL = "SELECT ENTITY_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO ENT_CONTACT_TEMP]"
                    + " FROM ENT_X_CONTACTINFO WHERE ENTITY_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[ENT_CONTACT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[ENT_CONTACT_TEMP]", ref sBitFields, ref sBitValues);
                        //dbisht6 jira RMA-8466
                        if (sBitFields.Contains("ENTITY_ID"))
                        {
                            string sBitValuesSegment = null;
                            sBitValuesSegment = sBitValues.ToString().Remove(0, 1);
                            sSQL = "INSERT INTO [ENT_CONTACT_TEMP](" + sBitFields.Remove(0, 1) + ")" +
                                    " SELECT ENTITY_ID" + sBitValuesSegment.Remove(0, sBitValuesSegment.ToString().IndexOf(",")) + " FROM [ENT_TEMP]";

                        }
                        //dbisht6 end
                        else
                        {
                            sSQL = "INSERT INTO [ENT_CONTACT_TEMP](ENTITY_ID" + sBitFields + ")"
                                + " SELECT ENTITY_ID" + sBitValues + " FROM [ENT_TEMP]";
                        }
                        //sSQL = "INSERT INTO [ENT_CONTACT_TEMP](ENTITY_ID" + sBitFields + ")"
                        //    + " SELECT ENTITY_ID" + sBitValues + " FROM [ENT_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Entity Operating As
                if (Utilities.ExistsInArray(p_objDisplayTables, "ENT_X_OPERATINGAS"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "ENT_X_OPERATINGAS");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[ENT_TEMP]", "[ENT_OPERATINGAS_TEMP]", "ENTITY_ID");
                    sSQL = "SELECT ENTITY_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO ENT_OPERATINGAS_TEMP]"
                    + " FROM ENT_X_OPERATINGAS WHERE ENTITY_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[ENT_OPERATINGAS_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[ENT_OPERATINGAS_TEMP]", ref sBitFields, ref sBitValues);
                        //dbisht6 jira RMA-8466
                        if (sBitFields.Contains("ENTITY_ID"))
                        {
                            string sBitValuesSegment = null;
                            sBitValuesSegment = sBitValues.ToString().Remove(0, 1);
                            sSQL = "INSERT INTO [ENT_OPERATINGAS_TEMP](" + sBitFields.Remove(0, 1) + ")" +
                                    " SELECT ENTITY_ID" + sBitValuesSegment.Remove(0, sBitValuesSegment.ToString().IndexOf(",")) + " FROM [ENT_TEMP]";

                        }
                        //dbisht6 end
                        else
                        {
                            sSQL = "INSERT INTO [ENT_OPERATINGAS_TEMP](ENTITY_ID" + sBitFields + ")"
                                + " SELECT ENTITY_ID" + sBitValues + " FROM [ENT_TEMP]";
                        }
                        //sSQL = "INSERT INTO [ENT_OPERATINGAS_TEMP](ENTITY_ID" + sBitFields + ")"
                        //    + " SELECT ENTITY_ID" + sBitValues + " FROM [ENT_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Shruti for MITS 11832 - ends
                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_objDisplayTables, "ADDRESS_X_PHONEINFO"))
                {
                    int iPos = 0;
                    string sFieldName = string.Empty;
                    string sFieldPos = string.Empty;
                    sbSql.Length = 0;

                    iIdx = Generic.IndexOf(p_objDisplayTables, "ADDRESS_X_PHONEINFO");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[ENT_TEMP]", "[PHONE_TEMP]", "ENTITY_ID");
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("|");
                    sFieldName = p_objDisplayFields[iIdx].ToString().Substring(0, iPos - 1);
                    sFieldPos = p_objDisplayFields[iIdx].ToString().Substring(iPos);

                    sbSql.AppendFormat("SELECT ENTITY_ID,{0} {1}'('{1}CODE_DESC{1}')' {2}", sFieldName, sConcatOperator, sFieldPos);
                    sbSql.Append(" [INTO PHONE_TEMP]");
                    sbSql.AppendFormat(" FROM ADDRESS_X_PHONEINFO,CODES_TEXT WHERE ENTITY_ID = {0}", p_lRecordID.ToString());
                    sbSql.Append(" AND CODES_TEXT.CODE_ID=ADDRESS_X_PHONEINFO.PHONE_CODE");
                    sSQL = sbSql.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }

                if (Utilities.ExistsInArray(p_objDisplayTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_objDisplayTables, "ADDRESS"))//RMA-8753 nshah28 
                {

                    if (objSettings.UseMultipleAddresses)
                    {

                        iIdx = Generic.IndexOf(p_objDisplayTables, "ENTITY_X_ADDRESSES");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[ENT_TEMP]", "[ADD_TEMP]", "ENTITY_ID");

                        sbSql.Length = 0;
                        //sbSql.AppendFormat("SELECT ENTITY_ID,{0}", p_objDisplayFields[iIdx].ToString());
                        //RMA-8753 nshah28 start
                        iIdxAddr = Generic.IndexOf(p_objDisplayTables, "ADDRESS");
                        sbSql.AppendFormat("SELECT ENTITY_ID");
                        if (iIdx > -1)
                            sbSql.AppendFormat(" ,{0}", p_objDisplayFields[iIdx].ToString());
                        if (iIdxAddr > -1)
                            sbSql.AppendFormat(" ,{0}", p_objDisplayFields[iIdxAddr].ToString());
                        //RMA-8753 nshah28 end
                        sbSql.Append(" [INTO ADD_TEMP]");
                        //sbSql.AppendFormat(" FROM ENTITY_X_ADDRESSES  AND  ENTITY_ID = {0}", p_lRecordID.ToString());
                        //RMA-8753 nshah28 
                        sbSql.AppendFormat(" FROM ENTITY_X_ADDRESSES, ADDRESS WHERE ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID   AND  ENTITY_ID = {0}", p_lRecordID.ToString());
                        sSQL = sbSql.ToString();

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Added Rakhi for R7:Add Emp Data Elements
                //MITS:34276-- Entity ID Type Search Starts
                if (Utilities.ExistsInArray(p_objDisplayTables, "ENT_ID_TYPE"))
                {

                    iIdx = Generic.IndexOf(p_objDisplayTables, "ENT_ID_TYPE");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[ENT_TEMP]", "[ENT_ID_TYPE_TEMP]", "ENTITY_ID");
                    sSQL = "SELECT ENTITY_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO ENT_ID_TYPE_TEMP]"
                    + " FROM ENT_ID_TYPE WHERE ENTITY_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[ENT_ID_TYPE_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[ENT_ID_TYPE_TEMP]", ref sBitFields, ref sBitValues);
                        //dbisht6 jira RMA-8466
                        if (sBitFields.Contains("ENTITY_ID"))
                        {
                            string sBitValuesSegment = null;
                            sBitValuesSegment = sBitValues.ToString().Remove(0, 1);
                            sSQL = "INSERT INTO [ENT_ID_TYPE_TEMP](" + sBitFields.Remove(0, 1) + ")" +
                                    " SELECT ENTITY_ID" + sBitValuesSegment.Remove(0, sBitValuesSegment.ToString().IndexOf(",")) + " FROM [ENT_TEMP]";

                        }
                        //dbisht6 end
                        else
                        {
                            sSQL = "INSERT INTO [ENT_ID_TYPE_TEMP](ENTITY_ID" + sBitFields + ")"
                                + " SELECT ENTITY_ID" + sBitValues + " FROM [ENT_TEMP]";
                        }
                        //sSQL = "INSERT INTO [ENT_ID_TYPE_TEMP](ENTITY_ID" + sBitFields + ")"
                        //    + " SELECT ENTITY_ID" + sBitValues + " FROM [ENT_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }

                }
                //MITS:34276-- Entity ID Type Search Ends
                //ENTITY SUPPLEMENTAL
                if (Utilities.ExistsInArray(p_objDisplayTables, "ENTITY_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "ENTITY_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[ENT_TEMP]", "[ENTSUPP_TEMP]", "ENTITY_ID");
                    sSQL = "SELECT ENTITY_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO ENTSUPP_TEMP]"
                    + " FROM ENTITY_SUPP WHERE ENTITY_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[ENTSUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[ENTSUPP_TEMP]", ref sBitFields, ref sBitValues);
                        //dbisht6 jira RMA-8466
                        if (sBitFields.Contains("ENTITY_ID"))
                        {
                            string sBitValuesSegment = null;
                            sBitValuesSegment = sBitValues.ToString().Remove(0, 1);
                            sSQL = "INSERT INTO [ENTSUPP_TEMP](" + sBitFields.Remove(0, 1) + ")" +
                                    " SELECT ENTITY_ID" + sBitValuesSegment.Remove(0, sBitValuesSegment.ToString().IndexOf(",")) + " FROM [ENT_TEMP]";

                        }
                        //dbisht6 end
                        else
                        {
                            sSQL = "INSERT INTO [ENTSUPP_TEMP](ENTITY_ID" + sBitFields + ")"
                                + " SELECT ENTITY_ID" + sBitValues + " FROM [ENT_TEMP]";
                        }
                        //sSQL = "INSERT INTO [ENTSUPP_TEMP](ENTITY_ID" + sBitFields + ")"
                        //    + " SELECT ENTITY_ID" + sBitValues + " FROM [ENT_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Shruti - Reverting as code for 'operating as' and 'contact info' has already been included for 11832
                ////gagnihotri Added Code for OPERATING AS and CONTACT INFO tabs.
                ////ENTITY OPERATING AS
                //if (Utilities.ExistsInArray(p_objDisplayTables,"ENT_X_OPERATINGAS")) 
                //{
                //    iIdx=Generic.IndexOf(p_objDisplayTables,"ENT_X_OPERATINGAS");

                //    //Add temp table to final outer joined query
                //    Generic.ChainIt(ref sFrom, ref sWhere, "[ENT_TEMP]", "[OA_TEMP]", "ENTITY_ID");

                //    //Dump results into temp table
                //     sSQL = "SELECT ENTITY_ID," + p_objDisplayFields[iIdx].ToString() 
                //         + " [INTO OA_TEMP]"
                //     +" FROM ENT_X_OPERATINGAS WHERE ENTITY_ID = " + p_lRecordID.ToString();
                //    m_objMergeCmd.CommandType=CommandType.Text;
                //    m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
                //    m_objMergeCmd.ExecuteNonQuery();

                //    //If nothing went in, fill with empty record(s) so that inner join doesn't fail
                //    if (m_objGeneric.GetMergeCount("[OA_TEMP]")==0)
                //    {
                //        m_objGeneric.FindBitFields("[OA_TEMP]", ref sBitFields, ref sBitValues);
                //        sSQL = "INSERT INTO [OA_TEMP](ENTITY_ID" + sBitFields + ")"
                //            + " VALUES (" + p_lRecordID.ToString() + sBitValues + ")";
                //        m_objMergeCmd.CommandType=CommandType.Text;
                //        m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
                //        m_objMergeCmd.ExecuteNonQuery();
                //    }
                //}

                ////ENTITY CONTACT INFO
                //if (Utilities.ExistsInArray(p_objDisplayTables,"ENT_X_CONTACTINFO")) 
                //{
                //    iIdx=Generic.IndexOf(p_objDisplayTables,"ENT_X_CONTACTINFO");

                //    //Add temp table to final outer joined query
                //    Generic.ChainIt(ref sFrom, ref sWhere, "[ENT_TEMP]", "[CONTACT_TEMP]", "ENTITY_ID");

                //    //Dump results into temp table
                //     sSQL = "SELECT ENTITY_ID," + p_objDisplayFields[iIdx].ToString() 
                //         + " [INTO CONTACT_TEMP]"
                //     +" FROM ENT_X_CONTACTINFO WHERE ENTITY_ID = " + p_lRecordID.ToString();
                //    m_objMergeCmd.CommandType=CommandType.Text;
                //    m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
                //    m_objMergeCmd.ExecuteNonQuery();

                //    //If nothing went in, fill with empty record(s) so that inner join doesn't fail
                //    if (m_objGeneric.GetMergeCount("[CONTACT_TEMP]")==0)
                //    {
                //        m_objGeneric.FindBitFields("[CONTACT_TEMP]", ref sBitFields, ref sBitValues);
                //        sSQL = "INSERT INTO [CONTACT_TEMP](ENTITY_ID" + sBitFields + ")"
                //            + " VALUES (" + p_lRecordID.ToString() + sBitValues + ")";
                //        m_objMergeCmd.CommandType=CommandType.Text;
                //        m_objMergeCmd.CommandText=m_objGeneric.SafeSQL(sSQL);
                //        m_objMergeCmd.ExecuteNonQuery();
                //    }
                //}

                //Added By Nitika For AIC Gap 9
                //Entity Operating As
                if (Utilities.ExistsInArray(p_objDisplayTables, "ENTITY_X_CODELICEN"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "ENTITY_X_CODELICEN");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[ENT_TEMP]", "[ENT_CODELICEN_TEMP]", "ENTITY_ID");
                    sSQL = "SELECT ENTITY_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO ENT_CODELICEN_TEMP]"
                    + " FROM ENTITY_X_CODELICEN WHERE ENTITY_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    //if (m_objGeneric.GetMergeCount("[ENT_CODELICEN_TEMP]") == 0)
                    //{
                    //    m_objGeneric.FindBitFields("[ENT_CODELICEN_TEMP]", ref sBitFields, ref sBitValues);
                    //    sSQL = "INSERT INTO [ENT_CODELICEN_TEMP](ENTITY_ID" + sBitFields + ")"
                    //        + " SELECT ENTITY_ID" + sBitValues + " FROM [ENT_TEMP]";
                    //    m_objMergeCmd.CommandType = CommandType.Text;
                    //    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    //    m_objMergeCmd.ExecuteNonQuery();
                    //}
                }
                //Added By Nitika For AIC Gap 9
                sSQL = "SELECT " + p_sJustFieldNames
                    + " FROM " + sFrom;
                if (sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + sWhere;
                }
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupEntityMerge();

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoEntityMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;
                sFrom = null;
                sWhere = null;
                sFields = null;
                sBitFields = null;
                sBitValues = null;
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
            }

        }
        #endregion

        #region Employee Merge
        /// Name		: DoEmployeeMergeQuery
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to employee
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        internal void DoEmployeeMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, long p_ldbMake)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            int iIdx = 0;
            int iIdxAddr = 0; //RMA-8753 nshah28 
            string sFields = "";
            string sBitFields = "";
            string sBitValues = "";
            Merge objMerge = null;
            DbReader objReader = null;
            try
            {
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupEmployeeMerge();
                }
                //EMPLOYEE
                if (Utilities.ExistsInArray(p_objDisplayTables, "EMPLOYEE")
                    || Utilities.ExistsInArray(p_objDisplayTables, "ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "EMPLOYEE");
                    if (iIdx != -1)
                    {
                        sFields = p_objDisplayFields[iIdx].ToString();
                    }
                    iIdx = Generic.IndexOf(p_objDisplayTables, "ENTITY");

                    if (iIdx != -1)
                    {
                        if (sFields != "")
                        {
                            sFields = sFields + ",";
                        }
                        sFields = sFields + p_objDisplayFields[iIdx].ToString();
                    }
                    sSQL = "SELECT EMPLOYEE.EMPLOYEE_EID," + sFields
                        + " [INTO EMP_TEMP] FROM EMPLOYEE,ENTITY WHERE EMPLOYEE_EID = "
                        + p_lRecordID.ToString() + " AND ENTITY.ENTITY_ID = EMPLOYEE.EMPLOYEE_EID";
                }
                else
                {
                    sSQL = "SELECT EMPLOYEE_EID [INTO EMP_TEMP] FROM EMPLOYEE WHERE EMPLOYEE_EID = "
                        + p_lRecordID.ToString();
                }
                m_objMergeCmd.CommandType = CommandType.Text;
                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                m_objMergeCmd.ExecuteNonQuery();
                sFrom = "[EMP_TEMP]";
                sWhere = "";
                //EMPLOYEE SUPPLEMENTAL
                if (Utilities.ExistsInArray(p_objDisplayTables, "EMP_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "EMP_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[EMP_TEMP]", "[EMPSUPP_TEMP]", "EMPLOYEE_EID");
                    sSQL = "SELECT EMPLOYEE_EID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO EMPSUPP_TEMP]"
                        + " FROM EMP_SUPP WHERE EMPLOYEE_EID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[EMPSUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[EMPSUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [EMPSUPP_TEMP](EMPLOYEE_EID" + sBitFields + ")"
                            + " SELECT EMPLOYEE_EID" + sBitValues + " FROM [EMP_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_objDisplayTables, "ADDRESS_X_PHONEINFO"))
                {
                    int iPos = 0;
                    string sFieldName = string.Empty;
                    string sFieldPos = string.Empty;
                    sbSql.Length = 0;

                    iIdx = Generic.IndexOf(p_objDisplayTables, "ADDRESS_X_PHONEINFO");
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("|");
                    sFieldName = p_objDisplayFields[iIdx].ToString().Substring(0, iPos - 1);
                    sFieldPos = p_objDisplayFields[iIdx].ToString().Substring(iPos);

                    Generic.ChainIt(ref sFrom, ref sWhere, "[EMP_TEMP]", "[PHONE_TEMP]", "EMPLOYEE_EID");

                    sbSql.AppendFormat("SELECT ENTITY_ID EMPLOYEE_EID,{0} {1}'('{1}CODE_DESC{1}')' {2}", sFieldName, sConcatOperator, sFieldPos);
                    sbSql.Append(" [INTO PHONE_TEMP]");
                    sbSql.AppendFormat(" FROM ADDRESS_X_PHONEINFO,CODES_TEXT WHERE ENTITY_ID = {0}", p_lRecordID.ToString());
                    sbSql.Append(" AND CODES_TEXT.CODE_ID=ADDRESS_X_PHONEINFO.PHONE_CODE");
                    sSQL = sbSql.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                AddOrgPhoneQuery("EMP_PHN_", 8, "EMPLOYEE", p_objDisplayTables, p_objDisplayFields, ref sFrom, ref sWhere, p_lRecordID);
                AddOrgPhoneQuery("EMP_PHN_", 7, "EMPLOYEE", p_objDisplayTables, p_objDisplayFields, ref sFrom, ref sWhere, p_lRecordID);
                AddOrgPhoneQuery("EMP_PHN_", 6, "EMPLOYEE", p_objDisplayTables, p_objDisplayFields, ref sFrom, ref sWhere, p_lRecordID);
                AddOrgPhoneQuery("EMP_PHN_", 5, "EMPLOYEE", p_objDisplayTables, p_objDisplayFields, ref sFrom, ref sWhere, p_lRecordID);
                AddOrgPhoneQuery("EMP_PHN_", 4, "EMPLOYEE", p_objDisplayTables, p_objDisplayFields, ref sFrom, ref sWhere, p_lRecordID);
                AddOrgPhoneQuery("EMP_PHN_", 3, "EMPLOYEE", p_objDisplayTables, p_objDisplayFields, ref sFrom, ref sWhere, p_lRecordID);
                AddOrgPhoneQuery("EMP_PHN_", 2, "EMPLOYEE", p_objDisplayTables, p_objDisplayFields, ref sFrom, ref sWhere, p_lRecordID);
                AddOrgPhoneQuery("EMP_PHN_", 1, "EMPLOYEE", p_objDisplayTables, p_objDisplayFields, ref sFrom, ref sWhere, p_lRecordID);
               
                if (Utilities.ExistsInArray(p_objDisplayTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_objDisplayTables, "ADDRESS"))//RMA-8753 nshah28 
                {

                    if (objSettings.UseMultipleAddresses)
                    {

                        iIdx = Generic.IndexOf(p_objDisplayTables, "ENTITY_X_ADDRESSES");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[EMP_TEMP]", "[ADD_TEMP]", "EMPLOYEE_EID");

                        sbSql.Length = 0;
                       // sbSql.AppendFormat("SELECT ENTITY_ID EMPLOYEE_EID,{0}", p_objDisplayFields[iIdx].ToString());
                        //RMA-8753 nshah28 start
                        iIdxAddr = Generic.IndexOf(p_objDisplayTables, "ADDRESS"); 
                        sbSql.AppendFormat("SELECT ENTITY_ID EMPLOYEE_EID");
                        if (iIdx > -1)
                            sbSql.AppendFormat(" ,{0}", p_objDisplayFields[iIdx].ToString()); 
                        if (iIdxAddr > -1)
                            sbSql.AppendFormat(" ,{0}", p_objDisplayFields[iIdxAddr].ToString());
                        //RMA-8753 nshah28 end
                        sbSql.Append(" [INTO ADD_TEMP]");
                       // sbSql.AppendFormat(" FROM ENTITY_X_ADDRESSES WHERE ENTITY_ID = {0}", p_lRecordID.ToString());
                        //RMA-8753 nshah28(adding join with Address)
                        sbSql.AppendFormat(" FROM ENTITY_X_ADDRESSES, ADDRESS WHERE ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID   AND  ENTITY_ID = {0}", p_lRecordID.ToString());
                        sSQL = sbSql.ToString();

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Added Rakhi for R7:Add Emp Data Elements

                // dept assigned
                if (Utilities.ExistsInArray(p_objDisplayTables, "DEPT_EMP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DEPT_EMP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[EMP_TEMP]", "[DEPT_TEMP]", "EMPLOYEE_EID");
                    sSQL = "SELECT EMPLOYEE_EID," + p_objDisplayFields[iIdx].ToString() + " [INTO DEPT_TEMP]"
                        + " FROM EMPLOYEE,ENTITY DEPT_EMP WHERE EMPLOYEE_EID = " + p_lRecordID.ToString()
                        + " AND EMPLOYEE.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }

                // FACILITY_EMP
                if (Utilities.ExistsInArray(p_objDisplayTables, "FACILITY_EMP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "FACILITY_EMP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[EMP_TEMP]", "[FAC_TEMP]", "EMPLOYEE_EID");
                    sSQL = "SELECT EMPLOYEE_EID," + p_objDisplayFields[iIdx].ToString() + " [INTO FAC_TEMP]"
                        + " FROM EMPLOYEE,ENTITY DEPT_EMP,ENTITY FACILITY_EMP WHERE EMPLOYEE_EID = " + p_lRecordID.ToString()
                        + " AND EMPLOYEE.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
                        + " AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                // FACILITY_EMP
                if (Utilities.ExistsInArray(p_objDisplayTables, "LOCATION_EMP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "LOCATION_EMP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[EMP_TEMP]", "[LOC_TEMP]", "EMPLOYEE_EID");
                    sSQL = "SELECT EMPLOYEE_EID," + p_objDisplayFields[iIdx].ToString() +
                        " [INTO LOC_TEMP]"
                        + " FROM EMPLOYEE,ENTITY DEPT_EMP,ENTITY FACILITY_EMP,ENTITY LOCATION_EMP WHERE EMPLOYEE_EID = " + p_lRecordID.ToString()
                        + " AND EMPLOYEE.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
                        + " AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
                        + " AND FACILITY_EMP.PARENT_EID = LOCATION_EMP.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                // DIVISION_EMP
                if (Utilities.ExistsInArray(p_objDisplayTables, "DIVISION_EMP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DIVISION_EMP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[EMP_TEMP]", "[DIV_TEMP]", "EMPLOYEE_EID");
                    sSQL = "SELECT EMPLOYEE_EID," + p_objDisplayFields[iIdx].ToString() +
                        " [INTO DIV_TEMP]"
                        + " FROM EMPLOYEE,ENTITY DEPT_EMP,ENTITY FACILITY_EMP,ENTITY LOCATION_EMP,ENTITY DIVISION_EMP WHERE EMPLOYEE_EID = " + p_lRecordID.ToString()
                        + " AND EMPLOYEE.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
                        + " AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
                        + " AND FACILITY_EMP.PARENT_EID = LOCATION_EMP.ENTITY_ID"
                        + " AND LOCATION_EMP.PARENT_EID = DIVISION_EMP.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                // REGION_EMP
                if (Utilities.ExistsInArray(p_objDisplayTables, "REGION_EMP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "REGION_EMP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[EMP_TEMP]", "[REG_TEMP]", "EMPLOYEE_EID");
                    sSQL = "SELECT EMPLOYEE_EID," + p_objDisplayFields[iIdx].ToString() + " [INTO REG_TEMP]"
                        + " FROM EMPLOYEE,ENTITY DEPT_EMP,ENTITY FACILITY_EMP,ENTITY LOCATION_EMP,ENTITY DIVISION_EMP,ENTITY REGION_EMP "
                        + " WHERE EMPLOYEE_EID = " + p_lRecordID.ToString()
                        + " AND EMPLOYEE.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
                        + " AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
                        + " AND FACILITY_EMP.PARENT_EID = LOCATION_EMP.ENTITY_ID"
                        + " AND LOCATION_EMP.PARENT_EID = DIVISION_EMP.ENTITY_ID"
                        + " AND DIVISION_EMP.PARENT_EID = REGION_EMP.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                // OPERATION_EMP
                if (Utilities.ExistsInArray(p_objDisplayTables, "OPERATION_EMP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "OPERATION_EMP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[EMP_TEMP]", "[OP_TEMP]", "EMPLOYEE_EID");
                    sSQL = "SELECT EMPLOYEE_EID," + p_objDisplayFields[iIdx].ToString() + " [INTO OP_TEMP]"
                        + " FROM EMPLOYEE,ENTITY DEPT_EMP,ENTITY FACILITY_EMP,ENTITY LOCATION_EMP,"
                        + " ENTITY DIVISION_EMP,ENTITY REGION_EMP,ENTITY OPERATION_EMP WHERE "
                        + " EMPLOYEE_EID = " + p_lRecordID.ToString()
                        + " AND EMPLOYEE.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
                        + " AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
                        + " AND FACILITY_EMP.PARENT_EID = LOCATION_EMP.ENTITY_ID"
                        + " AND LOCATION_EMP.PARENT_EID = DIVISION_EMP.ENTITY_ID"
                        + " AND DIVISION_EMP.PARENT_EID = REGION_EMP.ENTITY_ID"
                        + " AND REGION_EMP.PARENT_EID = OPERATION_EMP.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }

                // OPERATION_EMP
                if (Utilities.ExistsInArray(p_objDisplayTables, "COMPANY_EMP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "COMPANY_EMP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[EMP_TEMP]", "[COMP_TEMP]", "EMPLOYEE_EID");
                    sSQL = "SELECT EMPLOYEE_EID," + p_objDisplayFields[iIdx].ToString() +
                        " [INTO COMP_TEMP]"
                        + " FROM EMPLOYEE,ENTITY DEPT_EMP,ENTITY FACILITY_EMP,ENTITY LOCATION_EMP,ENTITY"
                        + " DIVISION_EMP,ENTITY REGION_EMP,ENTITY OPERATION_EMP,ENTITY COMPANY_EMP "
                        + " WHERE EMPLOYEE_EID = " + p_lRecordID.ToString()
                        + " AND EMPLOYEE.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
                        + " AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
                        + " AND FACILITY_EMP.PARENT_EID = LOCATION_EMP.ENTITY_ID"
                        + " AND LOCATION_EMP.PARENT_EID = DIVISION_EMP.ENTITY_ID"
                        + " AND DIVISION_EMP.PARENT_EID = REGION_EMP.ENTITY_ID"
                        + " AND REGION_EMP.PARENT_EID = OPERATION_EMP.ENTITY_ID"
                        + " AND OPERATION_EMP.PARENT_EID = COMPANY_EMP.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                // CLIENT_EMP
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLIENT_EMP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLIENT_EMP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[EMP_TEMP]", "[CLIENT_TEMP]", "EMPLOYEE_EID");
                    sSQL = "SELECT EMPLOYEE_EID," + p_objDisplayFields[iIdx].ToString() +
                        " [INTO CLIENT_TEMP]"
                        + " FROM EMPLOYEE,ENTITY DEPT_EMP,ENTITY FACILITY_EMP,ENTITY LOCATION_EMP,ENTITY"
                        + " DIVISION_EMP,ENTITY REGION_EMP,ENTITY OPERATION_EMP,ENTITY COMPANY_EMP "
                        + " ,ENTITY CLIENT_EMP "
                        + " WHERE EMPLOYEE_EID = " + p_lRecordID.ToString()
                        + " AND EMPLOYEE.DEPT_ASSIGNED_EID = DEPT_EMP.ENTITY_ID"
                        + " AND DEPT_EMP.PARENT_EID = FACILITY_EMP.ENTITY_ID"
                        + " AND FACILITY_EMP.PARENT_EID = LOCATION_EMP.ENTITY_ID"
                        + " AND LOCATION_EMP.PARENT_EID = DIVISION_EMP.ENTITY_ID"
                        + " AND DIVISION_EMP.PARENT_EID = REGION_EMP.ENTITY_ID"
                        + " AND REGION_EMP.PARENT_EID = OPERATION_EMP.ENTITY_ID"
                        + " AND OPERATION_EMP.PARENT_EID = COMPANY_EMP.ENTITY_ID"
                        + " AND COMPANY_EMP.PARENT_EID = CLIENT_EMP.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                sSQL = "SELECT " + p_sJustFieldNames
                    + " FROM " + sFrom;
                if (sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + sWhere;
                }
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupEmployeeMerge();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoEmployeeMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;
                sFrom = null;
                sWhere = null;
                sFields = null;
                sBitFields = null;
                sBitValues = null;
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
            }
        }
        #endregion

        #region GetMultiEntityFieldName
        /// Name		: GetMultiEntityFieldName
        /// Author		: Hong Hao Lv(Scott)
        /// Date Created: 8/1/2012		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// this function for the Multi-Entity, get the column name when field type is 16(Multi-Entity)
        /// added for MITS 26255 hlv
        /// </summary>
        /// <param name="fieldID">field id of Multi-Entity, you can find it in table [SUPP_DICTIONARY]</param>
        /// <param name="recordID">record id that includ this Multi-Entity field, you can find it in table [SUPP_MULTI_VALUE]</param>
        /// <returns></returns>
        internal string GetMultiEntityFieldName(string fieldID, string recordID)
        {
            StringBuilder sb = new StringBuilder();

            string sSQL = " select CODE_ID from SUPP_MULTI_VALUE where FIELD_ID = " + fieldID + " and RECORD_ID = " + recordID;

            DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

            sb.Append("'");
            while (objReader.Read())
            {
                sb.Append(objReader.GetValue(0).ToString() + ",");
            }

            if (sb.Length > 1) { sb.Remove(sb.Length - 1, 1); }
            sb.Append("'");

            objReader.Close();
            if (objReader != null)
            {
                objReader.Dispose();
                objReader = null;
            }

            return sb.ToString();
        }

        #endregion

        #region Claim Merge
        /// Name		: DoClaimMergeQuery
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function retrieves data related to claim
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        /// <param name="sDefendantType">Defendant Type</param>
        /// <param name="sClaimantType">Claimant Type</param>
        /// <param name="sExpertType">Expert Type</param>
        /// <param name="sPersonInvolvedType">Person Involved Type</param>
        /// <param name="sCurrentAdjuster">Current Adjuster</param>
        /// <param name="sPrimaryCasemanager">Primary Casemanager</param>
        // Nitika Added 6 New parameter DefendantType, ClaimantType, ExpertType,PersonInvolvedType, CurrentAdjuster, PrimaryCasemanager for AutoMailmergeSetUp//
        // akaushik5 Changed for MITS 37272 Starts
        //internal void DoClaimMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
        //    string p_sJustFieldNames, bool bMergeAll, long p_lDbMake, string sDefendantType, string sClaimantType, string sExpertType, string sPersonInvolvedType, string sCurrentAdjuster, string sPrimaryCasemanager)
        internal void DoClaimMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, bool bMergeAll, long p_lDbMake, string sDefendantType, string sClaimantType, string sExpertType, string sPersonInvolvedType, string sCurrentAdjuster, string sPrimaryCasemanager, bool isAdjusterSelected)
        // akaushik5 Changed for MITS 37272 Ends
        {
            string sSQL = string.Empty;
            string sFrom = "";
            string sWhere = "";
            int iIdx = 0;

            // npadhy Start MITS 18935 Added the case for Reinsurer and modified the logic for Insurer
            int iIdxInsurer = 0;
            int iIdxReinsurer = 0;
            // npadhy End MITS 18935 Added the case for Reinsurer and modified the logic for Insurer

            int iIdxNext = 0;
            long lEventID = 0;
            string sTmp = "";
            int iPos = 0;
            string sTmpNext = "";
            string sClaimantSel = "";
            string sArbSel = "";  //asingh263 MITS 34597
            string sLitSel = "";
            string sExpertSel = "";
            string sAdjSel = "";
            string sAdjTextSel = "";
            string sLeaveSel = "";
            string sLeaveDetailSel = "";
            string sDefSel = "";
            string sFundsSel = "";
            string sUnitSel = "";
            int iPosNext = 0;
            string sBitFields = "";
            string sBitValues = "";
            string sSQLStart = "";
            string sSQLMid = "";
            string sFinalSQL = "";
            string sCols = "";
            string sValues = "";
            string sTemp = "";
            StringBuilder sbSql = new StringBuilder();
            DbReader objReader = null;
            //Shruti, 12185
            DataSet objDataSet = null;
            Merge objMerge = null;
            string sFieldNames = "";
            string sFieldValues = "";
            //mdhamija MITS 27692
            string sFields = "";
            string sOrderBy = ""; //MITS 25322 hlv 8/9/12 
            try
            {
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                if (p_lDbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupClaimMerge();
                }
                //CLAIM
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM");
                    sSQL = "SELECT CLAIM_ID,CATASTROPHE_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                        + ",EVENT_ID, LINE_OF_BUS_CODE, PLAN_ID [INTO CLAIM_TEMP] FROM CLAIM WHERE CLAIM_ID = " //Shruti, 12185
                        + p_lRecordID.ToString();//modified by mdhamija MITS 27692
                }
                else
                {
                    sSQL = "SELECT CLAIM_ID,EVENT_ID,LINE_OF_BUS_CODE, PLAN_ID [INTO CLAIM_TEMP] FROM CLAIM " //Shruti, 12185
                        + " WHERE CLAIM_ID = " + p_lRecordID.ToString();//modified by mdhamija MITS 27692
                }
                m_objMergeCmd.CommandType = CommandType.Text;
                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                m_objMergeCmd.ExecuteNonQuery();
                sSQL = "SELECT EVENT_ID FROM CLAIM WHERE CLAIM_ID = " + p_lRecordID.ToString();
                objReader = DbFactory.GetDbReader(m_sConnectionString, m_objGeneric.SafeSQL(sSQL));
                if (objReader.Read())
                {
                    lEventID = Conversion.ConvertStrToLong(
                        Conversion.ConvertObjToStr(objReader.GetValue(0)));
                }
                objReader.Close();
                sFrom = "[CLAIM_TEMP]";
                sWhere = "";
                //CLAIMANT
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIMANT")
                    || Utilities.ExistsInArray(p_objDisplayTables, "CLAIMANT_ENTITY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "CLAIMANT_ATTORNEY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "CLAIMANT_ATT_FIRM"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIMANT");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "CLAIMANT_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIMANT_TEMP]", "CLAIM_ID");
                    sClaimantSel = "-1";
                    sTmp = "";
                    sSQL = "SELECT CLAIMANT.CLAIM_ID,CLAIMANT.CLAIMANT_ROW_ID";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sTmp += Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                        sOrderBy = " ORDER BY CLAIMANT_ENTITY.DTTM_RCD_LAST_UPD";  //MITS 25322 hlv 8/9/12 
                    }
                    sSQL = sSQL + sTmp + " [INTO CLAIMANT_TEMP] "
                        + " FROM CLAIMANT,ENTITY CLAIMANT_ENTITY WHERE CLAIMANT.CLAIM_ID = " + p_lRecordID.ToString();
                    if (sClaimantSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sClaimantSel;
                    }
                    //Start : Added By Nitika for autoMailMerge Exe
                    if (!string.IsNullOrEmpty(sClaimantType))
                    {
                        sSQL = sSQL + " AND CLAIMANT.CLAIMANT_TYPE_CODE IN (" + sClaimantType.ToString() + ")";
                    }
                    //End : Added By Nitika for autoMailMerge Exe
                    sSQL = sSQL + " AND CLAIMANT.CLAIMANT_EID = CLAIMANT_ENTITY.ENTITY_ID" + sOrderBy; //MITS 25322 hlv 8/9/12 
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CLAIMANT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CLAIMANT_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [CLAIMANT_TEMP](CLAIM_ID,CLAIMANT_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //Claim Comment     csingh7 R6 Claim Comment Enhancement
                if (Utilities.ExistsInArray(p_objDisplayTables, "COMMENTS_TEXTCLAIM"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "COMMENTS_TEXTCLAIM");

                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[COMMENTS_TEXTCLAIM_TEMP]", "CLAIM_ID", "ATTACH_RECORDID");
                    //sSQL = "SELECT COMMENTS_TEXTCLAIM.ATTACH_RECORDID" +  //pmittal5 Mits 18599
                    sSQL = "SELECT COMMENTS_TEXTCLAIM.ATTACH_RECORDID, COMMENTS_TEXTCLAIM.COMMENT_ID, " +
                        p_objDisplayFields[iIdx].ToString() + " [INTO COMMENTS_TEXTCLAIM_TEMP]"
                        + " FROM [CLAIM_TEMP],COMMENTS_TEXT COMMENTS_TEXTCLAIM WHERE [CLAIM_TEMP].CLAIM_ID = COMMENTS_TEXTCLAIM.ATTACH_RECORDID AND COMMENTS_TEXTCLAIM.ATTACH_TABLE = 'CLAIM'";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    //pmittal5 Mits 18599 11/30/09 
                    if (m_objGeneric.GetMergeCount("[COMMENTS_TEXTCLAIM_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[COMMENTS_TEXTCLAIM_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [COMMENTS_TEXTCLAIM_TEMP](ATTACH_RECORDID,COMMENT_ID " + sBitFields + ")"
                            + " SELECT CLAIM_ID, 0 " + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                    //End - pmittal5
                }

                //CLAIMANT_ATTORNEY
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIMANT_ATTORNEY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "CLAIMANT_ATT_FIRM"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIMANT_ATTORNEY");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "CLAIMANT_ATT_FIRM");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIMANT_TEMP]", "[CLMNT_ATT_TEMP]", "CLAIMANT_ROW_ID");
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sTmp += Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    sSQL = "SELECT CLAIMANT.CLAIMANT_ROW_ID" + sTmp + " [INTO CLMNT_ATT_TEMP]"
                        + " FROM CLAIMANT,ENTITY CLAIMANT_ATTORNEY,ENTITY CLAIMANT_ATT_FIRM "
                        + " WHERE CLAIMANT.CLAIM_ID = " + p_lRecordID.ToString();
                    if (sClaimantSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sClaimantSel;
                    }
                    sSQL = sSQL + " AND CLAIMANT.ATTORNEY_EID = CLAIMANT_ATTORNEY.ENTITY_ID"
                        + " AND CLAIMANT_ATTORNEY.PARENT_EID = CLAIMANT_ATT_FIRM.ENTITY_ID ";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    m_objGeneric.MergeDupTable(m_objMergeCmd, "CLMNT_ATT_TEMP");
                    m_objGeneric.FindBitFields("[CLMNT_ATT_TEMP]", ref sBitFields, ref sBitValues);
                    sSQL = "INSERT INTO [CLMNT_ATT_TEMP](CLAIMANT_ROW_ID" + sBitFields + ")"
                        + " SELECT CLAIMANT_ROW_ID" + sBitValues + " FROM [CLAIMANT_TEMP]"
                        + "   WHERE CLAIMANT_ROW_ID NOT IN (SELECT CLAIMANT_ROW_ID FROM [MRG_TMP])";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    m_objGeneric.MergeDupCleanup(m_objMergeCmd);
                }
                //UNIT CLAIM
                if (Utilities.ExistsInArray(p_objDisplayTables, "UNIT_X_CLAIM")
                    || Utilities.ExistsInArray(p_objDisplayTables, "VEHICLE")
                    || Utilities.ExistsInArray(p_objDisplayTables, "VEH_DEPT"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "UNIT_X_CLAIM");

                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[UNIT_TEMP]", "CLAIM_ID");
                    sUnitSel = "-1";
                    sSQL = "SELECT CLAIM_ID,UNIT_ID ";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sSQL += Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = sSQL + " [INTO UNIT_TEMP]"
                        + " FROM UNIT_X_CLAIM WHERE UNIT_X_CLAIM.CLAIM_ID = " + p_lRecordID.ToString();
                    if (sUnitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sUnitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[UNIT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[UNIT_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [UNIT_TEMP](CLAIM_ID,UNIT_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //VEHICLE
                if (Utilities.ExistsInArray(p_objDisplayTables, "VEHICLE"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "VEHICLE");

                    Generic.ChainIt(ref sFrom, ref sWhere, "[UNIT_TEMP]", "[VEH_TEMP]", "UNIT_ID");
                    sSQL = "SELECT UNIT_X_CLAIM.CLAIM_ID,VEHICLE.UNIT_ID, " +
                        p_objDisplayFields[iIdx].ToString() + " [INTO VEH_TEMP]"
                        + " FROM UNIT_X_CLAIM,VEHICLE WHERE UNIT_X_CLAIM.CLAIM_ID = "
                        + p_lRecordID.ToString();
                    if (sUnitSel != "-1")
                    {
                        sSQL += " AND " + sUnitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[VEH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[VEH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [VEH_TEMP](CLAIM_ID,UNIT_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,UNIT_ID" + sBitValues + " FROM [UNIT_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //VEHICLE DEPT
                if (Utilities.ExistsInArray(p_objDisplayTables, "VEH_DEPT"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "VEH_DEPT");

                    Generic.ChainIt(ref sFrom, ref sWhere, "[UNIT_TEMP]", "[VEH_DEPT_TEMP]", "UNIT_ID");
                    sSQL = "SELECT UNIT_ID, " + p_objDisplayFields[iIdx].ToString() + " [INTO VEH_DEPT_TEMP]"
                        + " FROM UNIT_X_CLAIM,ENTITY VEH_DEPT WHERE CLAIM_ID = " + p_lRecordID.ToString()
                        + " AND UNIT_X_CLAIM.HOME_DEPT_EID = VEH_DEPT.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[VEH_DEPT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[VEH_DEPT_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [VEH_DEPT_TEMP](UNIT_ID" + sBitFields + ")"
                            + " SELECT UNIT_ID" + sBitValues + " FROM [UNIT_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //spahariya 08/28/2012 MITS 29464- start
                //VEHICLE_OWNER_NAME
                if (Utilities.ExistsInArray(p_objDisplayTables, "VEHICLE_OWNER_NAME"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "VEHICLE_OWNER_NAME");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[UNIT_TEMP]", "[VEH_OWN_TEMP]", "UNIT_ID");

                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT UNIT_ID, UNIT_X_CLAIM.OWNER_EID  " + sTmp
                        + " [INTO VEH_OWN_TEMP]"
                        + " FROM UNIT_X_CLAIM, ENTITY VEHICLE_OWNER_NAME "
                        + " WHERE VEHICLE_OWNER_NAME.ENTITY_ID = UNIT_X_CLAIM.OWNER_EID "
                        + " AND CLAIM_ID = " + p_lRecordID.ToString();

                    if (sUnitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sUnitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[VEH_OWN_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[VEH_OWN_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [VEH_OWN_TEMP](UNIT_ID,OWNER_EID" + sBitFields + ")"
                            + " SELECT UNIT_ID,0" + sBitValues + " FROM [UNIT_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //spahariya - End
                //spahariya 08/28/2012 MITS 29465- start
                //POL_AGENCY_NAME
                if (Utilities.ExistsInArray(p_objDisplayTables, "POL_AGENCY_NAME"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POL_AGENCY_NAME");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POL_AGENCY_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM.POLICE_AGENCY_EID " + sTmp
                        + " [INTO POL_AGENCY_TEMP]"
                        + " FROM CLAIM, ENTITY POL_AGENCY_NAME "
                        + " WHERE POL_AGENCY_NAME.ENTITY_ID = CLAIM.POLICE_AGENCY_EID"
                        + " AND CLAIM.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[POL_AGENCY_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[POL_AGENCY_TEMP]", ref sBitFields, ref sBitValues);
                        //sSQL = "INSERT INTO [LITIGATION_ENTITY_TEMP](CLAIM_ID,ENTITY_ID" + sBitFields + ")"
                        sSQL = "INSERT INTO [POL_AGENCY_TEMP](CLAIM_ID,POLICE_AGENCY_EID" + sBitFields + ")"  //pmittal5 Mits 18595 12/02/09
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //spahariya - End
                //CLAIM LITIGATION
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_LITIGATION") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "LIT_ATTORNEY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "LIT_ATTORNEY_FIRM")
                    || Utilities.ExistsInArray(p_objDisplayTables, "EXPERT")
                    || Utilities.ExistsInArray(p_objDisplayTables, "EXPERT_ENTITY")
                    )
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_X_LITIGATION");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LIT_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM_ID,LITIGATION_ROW_ID " + sTmp + " [INTO LIT_TEMP]"
                        + " FROM CLAIM_X_LITIGATION "
                        + " WHERE CLAIM_X_LITIGATION.CLAIM_ID = " + p_lRecordID.ToString();
                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[LIT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[LIT_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [LIT_TEMP](CLAIM_ID,LITIGATION_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //LITIGATION SUPPLEMENTAL
                if (Utilities.ExistsInArray(p_objDisplayTables, "LITIGATION_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "LITIGATION_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LIT_SUPP_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_LITIGATION.LITIGATION_ROW_ID " + sTmp
                        + " [INTO LIT_SUPP_TEMP]"
                        + " FROM CLAIM, CLAIM_X_LITIGATION, LITIGATION_SUPP "
                        + " WHERE CLAIM_X_LITIGATION.CLAIM_ID = " + p_lRecordID.ToString()
                        + " AND CLAIM.CLAIM_ID = CLAIM_X_LITIGATION.CLAIM_ID"
                        + " AND LITIGATION_SUPP.LITIGATION_ROW_ID = CLAIM_X_LITIGATION.LITIGATION_ROW_ID";

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[LIT_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[LIT_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [LIT_SUPP_TEMP](CLAIM_ID,LITIGATION_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Litigation Entity
                if (Utilities.ExistsInArray(p_objDisplayTables, "LITIGATION_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "LITIGATION_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LITIGATION_ENTITY_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_LITIGATION.JUDGE_EID " + sTmp
                        + " [INTO LITIGATION_ENTITY_TEMP]"
                        + " FROM CLAIM, CLAIM_X_LITIGATION, ENTITY LITIGATION_ENTITY "
                        + " WHERE CLAIM.CLAIM_ID = CLAIM_X_LITIGATION.CLAIM_ID"
                        + " AND LITIGATION_ENTITY.ENTITY_ID = CLAIM_X_LITIGATION.JUDGE_EID"
                        + " AND CLAIM_X_LITIGATION.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[LITIGATION_ENTITY_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[LITIGATION_ENTITY_TEMP]", ref sBitFields, ref sBitValues);
                        //sSQL = "INSERT INTO [LITIGATION_ENTITY_TEMP](CLAIM_ID,ENTITY_ID" + sBitFields + ")"
                        sSQL = "INSERT INTO [LITIGATION_ENTITY_TEMP](CLAIM_ID,JUDGE_EID" + sBitFields + ")"  //pmittal5 Mits 18595 12/02/09
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //CLAIM ADJUSTER SUPP
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_ADJ_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_ADJ_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIM_ADJ_SUPP_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_ADJUSTER.ADJ_ROW_ID " + sTmp
                        + " [INTO CLAIM_ADJ_SUPP_TEMP]"
                        + " FROM CLAIM, CLAIM_ADJUSTER, CLAIM_ADJ_SUPP "
                        + " WHERE CLAIM_ADJUSTER.CLAIM_ID = " + p_lRecordID.ToString()
                        + " AND CLAIM.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID"
                        + " AND CLAIM_ADJ_SUPP.ADJ_ROW_ID = CLAIM_ADJUSTER.ADJ_ROW_ID";

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CLAIM_ADJ_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CLAIM_ADJ_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [CLAIM_ADJ_SUPP_TEMP](CLAIM_ID,ADJ_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //DEFENDANT ATTORNEY
                if (Utilities.ExistsInArray(p_objDisplayTables, "DEFENDANT_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DEFENDANT_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[DEFENDANT_SUPP_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, DEFENDANT.DEFENDANT_ROW_ID " + sTmp
                        + " [INTO DEFENDANT_SUPP_TEMP]"
                        + " FROM CLAIM, DEFENDANT, DEFENDANT_SUPP "
                        + " WHERE DEFENDANT.CLAIM_ID = " + p_lRecordID.ToString()
                        + " AND CLAIM.CLAIM_ID = DEFENDANT.CLAIM_ID"
                        + " AND DEFENDANT.DEFENDANT_ROW_ID = DEFENDANT_SUPP.DEFENDANT_ROW_ID";

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[DEFENDANT_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[DEFENDANT_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [DEFENDANT_SUPP_TEMP](CLAIM_ID,DEFENDANT_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //CLAIMANT SUPPLEMENTAL
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIMANT_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIMANT_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIMANT_SUPP_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIMANT.CLAIMANT_ROW_ID " + sTmp
                        + " [INTO CLAIMANT_SUPP_TEMP]"
                        + " FROM CLAIM, CLAIMANT, CLAIMANT_SUPP "
                        + " WHERE CLAIMANT.CLAIM_ID =  " + p_lRecordID.ToString()
                        + " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID"
                        + " AND CLAIMANT.CLAIMANT_ROW_ID = CLAIMANT_SUPP.CLAIMANT_ROW_ID";

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CLAIMANT_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CLAIMANT_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [CLAIMANT_SUPP_TEMP](CLAIM_ID,CLAIMANT_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //LITIGATION ATTORNEY
                if (Utilities.ExistsInArray(p_objDisplayTables, "LIT_ATTORNEY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "LIT_ATTORNEY_FIRM"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "LIT_ATTORNEY");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "LIT_ATTORNEY_FIRM");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[LIT_TEMP]", "[LIT_ATT_TEMP]", "LITIGATION_ROW_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sTmp += Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    sSQL = "SELECT CLAIM_X_LITIGATION.LITIGATION_ROW_ID " + sTmp
                        + " [INTO LIT_ATT_TEMP]"
                        + " FROM CLAIM_X_LITIGATION,ENTITY LIT_ATTORNEY,ENTITY LIT_ATTORNEY_FIRM "
                        + " WHERE CLAIM_X_LITIGATION.CLAIM_ID =" + p_lRecordID.ToString();
                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    sSQL = sSQL + " AND CLAIM_X_LITIGATION.CO_ATTORNEY_EID = LIT_ATTORNEY.ENTITY_ID"
                        + " AND LIT_ATTORNEY.PARENT_EID = LIT_ATTORNEY_FIRM.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[LIT_ATT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[LIT_ATT_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [LIT_ATT_TEMP](LITIGATION_ROW_ID" + sBitFields + ")"
                            + " SELECT LITIGATION_ROW_ID " + sBitValues + " FROM [LIT_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //EXPERT
                if (Utilities.ExistsInArray(p_objDisplayTables, "EXPERT")
                    || Utilities.ExistsInArray(p_objDisplayTables, "EXPERT_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "EXPERT");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "EXPERT_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[LIT_TEMP]", "[EXPERT_TEMP]", "LITIGATION_ROW_ID");
                    sExpertSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sTmp += Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    sSQL = "SELECT CLAIM_X_LITIGATION.LITIGATION_ROW_ID " + sTmp
                        + " [INTO EXPERT_TEMP]"
                        + " FROM CLAIM_X_LITIGATION,EXPERT,ENTITY EXPERT_ENTITY "
                        + " WHERE CLAIM_X_LITIGATION.CLAIM_ID =" + p_lRecordID.ToString();
                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    if (sExpertSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sExpertSel;
                    }
                    //Start : Added By Nitika for autoMailMerge Exe
                    if (!string.IsNullOrEmpty(sExpertType))
                    {
                        sSQL = sSQL + " AND EXPERT.EXPERT_TYPE IN (" + sExpertType.ToString() + ")";
                    }
                    //End : Added By Nitika for autoMailMerge Exe
                    sSQL = sSQL + "  AND CLAIM_X_LITIGATION.LITIGATION_ROW_ID = EXPERT.LITIGATION_ROW_ID"
                        + " AND EXPERT.EXPERT_EID = EXPERT_ENTITY.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    m_objGeneric.MergeDupTable(m_objMergeCmd, "EXPERT_TEMP");
                    m_objGeneric.FindBitFields("[EXPERT_TEMP]", ref sBitFields, ref sBitValues);
                    sSQL = "INSERT INTO [EXPERT_TEMP](LITIGATION_ROW_ID" + sBitFields + ")"
                        + " SELECT LITIGATION_ROW_ID " + sBitValues + " FROM [LIT_TEMP]"
                        + " WHERE LITIGATION_ROW_ID NOT IN (SELECT LITIGATION_ROW_ID FROM [MRG_TMP])";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    m_objGeneric.MergeDupCleanup(m_objMergeCmd);
                }

                //asingh263 mits 34597
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_ARB"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_X_ARB");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[ARBITRATION_TEMP]", "CLAIM_ID");
                    sArbSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM_ID,ARBITRATION_ROW_ID " + sTmp + " [INTO ARBITRATION_TEMP]"
                        + " FROM CLAIM_X_ARB "
                        + " WHERE CLAIM_X_ARB.CLAIM_ID = " + p_lRecordID.ToString();
                    if (sArbSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sArbSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[ARBITRATION_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[ARBITRATION_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [ARBITRATION_TEMP](CLAIM_ID,ARBITRATION_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                if (Utilities.ExistsInArray(p_objDisplayTables, "ARBITRATION_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "ARBITRATION_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[ARBITRATION_SUPP_TEMP]", "CLAIM_ID");
                    sArbSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_ARB.ARBITRATION_ROW_ID " + sTmp
                        + " [INTO ARBITRATION_SUPP_TEMP]"
                        + " FROM CLAIM, CLAIM_X_ARB, ARBITRATION_SUPP "
                        + " WHERE CLAIM_X_ARB.CLAIM_ID = " + p_lRecordID.ToString()
                        + " AND CLAIM.CLAIM_ID = CLAIM_X_ARB.CLAIM_ID"
                        + " AND ARBITRATION_SUPP.ARBITRATION_ROW_ID = CLAIM_X_ARB.ARBITRATION_ROW_ID";

                    if (sArbSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sArbSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[ARBITRATION_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[ARBITRATION_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [ARBITRATION_SUPP_TEMP](CLAIM_ID,ARBITRATION_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //asingh263 mits 34597
                //ADJUSTER_
                if (Utilities.ExistsInArray(p_objDisplayTables, "ADJUSTER_ENTITY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "ADJUST_DATED_TEXT")
                    || Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_ADJUSTER"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "ADJUSTER_ENTITY");
                    //MITS 16335
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[ADJ_TEMP]", "CLAIM_ID");
                    sAdjSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {   //MITS 16335
                        //Generic.ChainIt(ref sFrom,ref sWhere, "[CLAIM_TEMP]", "[ADJ_TEMP]", "CLAIM_ID");
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_ADJUSTER");
                    sAdjSel = "-1";
                    string sClaimAdjuterFields = "";
                    if (iIdx != -1)
                    {   //MITS 16335
                        //if( sTmp == "" )
                        //{
                        //    Generic.ChainIt(ref sFrom,ref sWhere, "[CLAIM_TEMP]", "[ADJ_TEMP]", "CLAIM_ID");
                        //}
                        sClaimAdjuterFields = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM_ADJUSTER.CLAIM_ID,CLAIM_ADJUSTER.ADJ_ROW_ID ";
                    if (sTmp != "")
                    {
                        sSQL += sTmp;
                    }
                    if (sClaimAdjuterFields != "")
                    {
                        sSQL += sClaimAdjuterFields;
                    }
                    sSQL += " [INTO ADJ_TEMP]"
                        + " FROM CLAIM_ADJUSTER,ENTITY ADJUSTER_ENTITY  "
                        + " WHERE CLAIM_ADJUSTER.CLAIM_ID =" + p_lRecordID.ToString();
                    if (sAdjSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sAdjSel;
                    }
                    sSQL = sSQL + " AND CLAIM_ADJUSTER.ADJUSTER_EID = ADJUSTER_ENTITY.ENTITY_ID";
                    //Start : Added By Nitika for autoMailMerge Exe
                    // akaushik5 Changed for MITS 37272 Starts
                    //if (!string.IsNullOrEmpty(sCurrentAdjuster))
                    if (!string.IsNullOrEmpty(sCurrentAdjuster) ||(!isAdjusterSelected && this.IsCurrentAdjusterAttached(p_lRecordID)))
                    // akaushik5 Changed for MITS 37272 Ends
                    {
                        sSQL = sSQL + " AND CLAIM_ADJUSTER.CURRENT_ADJ_FLAG = -1";
                    }
                    //End : Added By Nitika for autoMailMerge Exe
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[ADJ_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[ADJ_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [ADJ_TEMP](CLAIM_ID,ADJ_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //CURRENT ADJUSTER : Parijat : Chubb Mail merge generic fields changes 
                if (Utilities.ExistsInArray(p_objDisplayTables, "CURRENT_ADJUSTER_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CURRENT_ADJUSTER_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CUR_ADJ_TEMP]", "CLAIM_ID");
                    sAdjSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM_ADJUSTER.CLAIM_ID,CLAIM_ADJUSTER.ADJ_ROW_ID ";
                    if (sTmp != "")
                    {
                        sSQL += sTmp;
                    }

                    sSQL += " [INTO CUR_ADJ_TEMP]"
                        + " FROM CLAIM_ADJUSTER,ENTITY CURRENT_ADJUSTER_ENTITY  "
                        + " WHERE CLAIM_ADJUSTER.CLAIM_ID =" + p_lRecordID.ToString();
                    sSQL += "AND CLAIM_ADJUSTER.CURRENT_ADJ_FLAG = -1 ";

                    sSQL = sSQL + " AND CLAIM_ADJUSTER.ADJUSTER_EID = CURRENT_ADJUSTER_ENTITY.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CUR_ADJ_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CUR_ADJ_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [CUR_ADJ_TEMP](CLAIM_ID,ADJ_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Parijat :End Chubb Mail merge generic fields changes 
                if (Utilities.ExistsInArray(p_objDisplayTables, "ADJUST_DATED_TEXT"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "ADJUST_DATED_TEXT");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "EXPERT_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[ADJ_TEMP]", "[ADJ_TEXT_TEMP]", "ADJ_ROW_ID");
                    sAdjTextSel = "-1";
                    sSQL = "SELECT CLAIM_ADJUSTER.ADJ_ROW_ID, " + p_objDisplayFields[iIdx].ToString()
                        + " [INTO ADJ_TEXT_TEMP]"
                        + " FROM CLAIM_ADJUSTER,ADJUST_DATED_TEXT  "
                        + " WHERE CLAIM_ADJUSTER.CLAIM_ID =" + p_lRecordID.ToString();
                    if (sAdjSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sAdjSel;
                    }
                    if (sAdjTextSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sAdjTextSel;
                    }
                    sSQL = sSQL + "  AND CLAIM_ADJUSTER.ADJ_ROW_ID = ADJUST_DATED_TEXT.ADJ_ROW_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    m_objGeneric.MergeDupTable(m_objMergeCmd, "ADJ_TEXT_TEMP");
                    m_objGeneric.FindBitFields("[ADJ_TEXT_TEMP]", ref sBitFields, ref sBitValues);
                    sSQL = "INSERT INTO [ADJ_TEXT_TEMP](ADJ_ROW_ID" + sBitFields + ")"
                        + " SELECT ADJ_ROW_ID " + sBitValues + " FROM [ADJ_TEMP]"
                        + " WHERE ADJ_ROW_ID NOT IN (SELECT ADJ_ROW_ID FROM [MRG_TMP])";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    m_objGeneric.MergeDupCleanup(m_objMergeCmd);
                }

                //Leave Management Merge                
                if (Utilities.ExistsInArray(p_objDisplayTables, "LEAVE_RB_ENTITY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_LEAVE")
                    || Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_LV_X_DETAIL"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "LEAVE_RB_ENTITY");
                    //MITS 16335
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIM_LV_TEMP]", "CLAIM_ID");
                    sLeaveSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {   //MITS 16335
                        //Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIM_LV_TEMP]", "CLAIM_ID");
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_LEAVE");
                    sLeaveSel = "-1";
                    string sClaimLeaveFields = "";
                    if (iIdx != -1)
                    {   //MITS 16335
                        //if (sTmp == "")
                        //{
                        //    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIM_LV_TEMP]", "CLAIM_ID");
                        //}
                        sClaimLeaveFields = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM_LEAVE.CLAIM_ID,CLAIM_LEAVE.LEAVE_ROW_ID ";
                    if (sTmp != "")
                    {
                        sSQL += sTmp;
                    }
                    if (sClaimLeaveFields != "")
                    {
                        sSQL += sClaimLeaveFields;
                    }
                    sSQL += " [INTO CLAIM_LV_TEMP]"
                        + " FROM CLAIM_LEAVE,ENTITY LEAVE_RB_ENTITY  "
                        + " WHERE CLAIM_LEAVE.CLAIM_ID =" + p_lRecordID.ToString();
                    if (sLeaveSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLeaveSel;
                    }
                    sSQL = sSQL + " AND CLAIM_LEAVE.REVIEWER_EID = LEAVE_RB_ENTITY.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CLAIM_LV_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CLAIM_LV_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [CLAIM_LV_TEMP](CLAIM_ID,LEAVE_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_LV_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_LV_X_DETAIL"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_LV_X_DETAIL");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_LV_TEMP]", "[CLAIM_LVD_TEMP]", "LEAVE_ROW_ID");
                    sLeaveDetailSel = "-1";
                    sSQL = "SELECT CLAIM_LEAVE.LEAVE_ROW_ID, " + p_objDisplayFields[iIdx].ToString()
                        + " [INTO CLAIM_LVD_TEMP]"
                        + " FROM CLAIM_LEAVE,CLAIM_LV_X_DETAIL  "
                        + " WHERE CLAIM_LEAVE.CLAIM_ID =" + p_lRecordID.ToString();
                    if (sLeaveSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLeaveSel;
                    }
                    if (sLeaveDetailSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLeaveDetailSel;
                    }
                    sSQL = sSQL + "  AND CLAIM_LEAVE.LEAVE_ROW_ID = CLAIM_LV_X_DETAIL.LEAVE_ROW_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    m_objGeneric.MergeDupTable(m_objMergeCmd, "CLAIM_LVD_TEMP");
                    m_objGeneric.FindBitFields("[CLAIM_LVD_TEMP]", ref sBitFields, ref sBitValues);
                    sSQL = "INSERT INTO [CLAIM_LVD_TEMP](LEAVE_ROW_ID" + sBitFields + ")"
                        + " SELECT LEAVE_ROW_ID " + sBitValues + " FROM [CLAIM_LV_TEMP]" +
                         " WHERE LEAVE_ROW_ID NOT IN (SELECT LEAVE_ROW_ID FROM [MRG_TMP])";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    m_objGeneric.MergeDupCleanup(m_objMergeCmd);
                }
                // Ayush : Property Claim Merge, 12/16/2009,  MITS 18291 Start:
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_PROPERTY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_X_PROPERTY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROPERTY_TEMP]", "CLAIM_ID");
                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("SELECT C1.CLAIM_ID,");
                    sbSql.Append(p_objDisplayFields[iIdx].ToString());
                    sbSql.Append(" [INTO PROPERTY_TEMP]");
                    sbSql.Append(" FROM CLAIM C1,CLAIM_X_PROPERTY WHERE C1.PROP_ROW_ID = CLAIM_X_PROPERTY.PROP_ROW_ID ");
                    sbSql.Append(" AND C1.LINE_OF_BUS_CODE =");
                    sbSql.Append(845);
                    sbSql.Append(" AND C1.CLAIM_ID =");
                    sbSql.Append(p_lRecordID.ToString());
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sbSql.ToString());
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PROPERTY_TEMP]") == 0)
                    {
                        //Asharma326 Change for MITS:29423 Starts
                        m_objGeneric.FindBitFields("[PROPERTY_TEMP]", ref sBitFields, ref sBitValues, true);
                        sbSql.Remove(0, sbSql.Length);
                        if (sBitFields.Contains("CLAIM_ID"))
                        {
                            sbSql.Append("INSERT INTO [PROPERTY_TEMP] (");
                            sbSql.Append(sBitFields.ToString().Remove(0, 1));
                            sbSql.Append(") VALUES ((SELECT CLAIM_ID FROM [CLAIM_TEMP])");
                            string sBitValuesSegment = null;
                            sBitValuesSegment = sBitValues.ToString().Remove(0, 1);
                            //sbSql.Append(sBitValuesSegment);
                            sbSql.Append(sBitValuesSegment.Remove(0, sBitValuesSegment.ToString().IndexOf(",")));
                            //sbSql.Append(sBitValues.ToString().Remove(0, 3));
                            sbSql.Append(" )");
                        }
                        else
                        {
                            sbSql.Append("INSERT INTO [PROPERTY_TEMP](CLAIM_ID");
                            sbSql.Append(sBitFields.ToString());
                            sbSql.Append(") SELECT CLAIM_ID");
                            sbSql.Append(sBitValues.ToString());
                            sbSql.Append(" FROM [CLAIM_TEMP]");
                        }
                        #region "Code Before MITS 29423"
                        //sbSql.Append("INSERT INTO [PROPERTY_TEMP](CLAIM_ID");
                        //sbSql.Append(sBitFields.ToString());
                        //sbSql.Append(") SELECT CLAIM_ID");
                        //sbSql.Append(sBitValues.ToString());
                        //sbSql.Append(" FROM [CLAIM_TEMP]");
                        #endregion
                        //Asharma326 Change for MITS:29423 Starts
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sbSql.ToString());
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                // Ayush : Property Claim Merge, 12/16/2009,  MITS 18291 End.

                //asharma326 MITS 29423 Starts
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_PROPERTY_LOSS"))
                {
                    //var to check whether Property id is in select query or not
                    bool localvar = false;

                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_X_PROPERTY_LOSS");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROPERTY_TEMP_LOSS]", "CLAIM_ID");
                    if (p_objDisplayFields[iIdx].ToString().Contains("CLAIM_X_PROPERTY_LOSS.PROPERTY_ID"))
                    {
                        localvar = true;
                    }
                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("SELECT C1.CLAIM_ID,");
                    if (localvar)
                    {
                        //p_objDisplayFields[iIdx].ToString().Replace("CLAIM_X_PROPERTYLOSS.PROPERTY_ID", "PROPERTY_UNIT.PIN");
                        sbSql.Append(p_objDisplayFields[iIdx].ToString().Replace("CLAIM_X_PROPERTY_LOSS", "CLAIM_X_PROPERTYLOSS").ToString().Replace("CLAIM_X_PROPERTYLOSS.PROPERTY_ID", "PROPERTY_UNIT.PIN"));
                    }
                    else
                    {
                        sbSql.Append(p_objDisplayFields[iIdx].ToString().Replace("CLAIM_X_PROPERTY_LOSS", "CLAIM_X_PROPERTYLOSS").ToString());
                    }
                    sbSql.Append(" [INTO PROPERTY_TEMP_LOSS]");
                    //sbSql.Append(" FROM CLAIM C1,CLAIM_X_PROPERTY WHERE C1.PROP_ROW_ID = CLAIM_X_PROPERTY.PROP_ROW_ID ");

                    if (localvar)
                    {
                        sbSql.Append(" FROM CLAIM C1,CLAIM_X_PROPERTYLOSS,PROPERTY_UNIT  WHERE C1.CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID AND CLAIM_X_PROPERTYLOSS.PROPERTY_ID=PROPERTY_UNIT.PROPERTY_ID");
                    }
                    else
                    {
                        sbSql.Append(" FROM CLAIM C1,CLAIM_X_PROPERTYLOSS WHERE C1.CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID ");
                    }
                    //sbSql.Append(" AND C1.LINE_OF_BUS_CODE =");
                    //sbSql.Append(845);
                    sbSql.Append(" AND C1.CLAIM_ID =");
                    sbSql.Append(p_lRecordID.ToString());

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sbSql.ToString());
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PROPERTY_TEMP_LOSS]") == 0)
                    {
                        //Asharma326 Change for MITS:29423
                        m_objGeneric.FindBitFields("[PROPERTY_TEMP_LOSS]", ref sBitFields, ref sBitValues, true);
                        sbSql.Remove(0, sbSql.Length);
                        if (sBitFields.Contains("CLAIM_ID"))
                        {
                            sbSql.Append("INSERT INTO [PROPERTY_TEMP_LOSS] (");
                            sbSql.Append(sBitFields.ToString().Remove(0, 1));
                            sbSql.Append(") VALUES ((SELECT CLAIM_ID FROM [CLAIM_TEMP])");
                            string sBitValuesSegment = null;
                            sBitValuesSegment = sBitValues.ToString().Remove(0, 1);
                            //sbSql.Append(sBitValuesSegment);
                            sbSql.Append(sBitValuesSegment.Remove(0, sBitValuesSegment.ToString().IndexOf(",")));

                            //sbSql.Append(sBitValues.ToString().Remove(0, 3));
                            sbSql.Append(" )");
                        }
                        else
                        {
                            sbSql.Append("INSERT INTO [PROPERTY_TEMP_LOSS](CLAIM_ID");
                            sbSql.Append(sBitFields.ToString());
                            sbSql.Append(") SELECT CLAIM_ID");
                            sbSql.Append(sBitValues.ToString());
                            sbSql.Append(" FROM [CLAIM_TEMP]");
                        }
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sbSql.ToString());
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Asharma326 MITS 29423 Change Ends

                // Ayush : Property Claim Schedule Merge, 12/16/2009,  MITS 18291 Start:
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_PSCHED_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_PSCHED_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PSCHED_TEMP]", "CLAIM_ID");
                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("SELECT C1.CLAIM_ID, ");
                    sbSql.Append(p_objDisplayFields[iIdx].ToString());
                    //MITS 22655 : Introduced table POLICY_X_UAR to correct the join
                    sbSql.Append(" [INTO PSCHED_TEMP] FROM CLAIM C1, CLAIM_X_PROPERTY CXP, POLICY_X_UAR PXU,POLICY_X_PSCHED_ENH WHERE C1.PROP_ROW_ID = CXP.PROP_ROW_ID");
                    sbSql.Append(" AND POLICY_X_PSCHED_ENH.UAR_ROW_ID =  PXU.UAR_ROW_ID AND PXU.UAR_ID = CXP.PROPERTY_ID AND POLICY_X_PSCHED_ENH.POLICY_ID = C1.PRIMARY_POLICY_ID");
                    sbSql.Append(" AND C1.LINE_OF_BUS_CODE = 845 AND C1.CLAIM_ID =");
                    sbSql.Append(p_lRecordID.ToString());

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sbSql.ToString());
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PSCHED_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PSCHED_TEMP]", ref sBitFields, ref sBitValues);
                        sbSql.Remove(0, sbSql.Length);
                        sbSql.Append("INSERT INTO [PSCHED_TEMP](CLAIM_ID");
                        sbSql.Append(sBitFields.ToString());
                        sbSql.Append(") SELECT CLAIM_ID");
                        sbSql.Append(sBitValues.ToString());
                        sbSql.Append(" FROM [CLAIM_TEMP]");
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sbSql.ToString());
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                // Ayush : Property Claim Schedule Merge, 12/16/2009,  MITS 18291 End.
                //DEFENDENT
                if (Utilities.ExistsInArray(p_objDisplayTables, "DEFENDANT")
                    || Utilities.ExistsInArray(p_objDisplayTables, "DEFEND_ENTITY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "DEFEND_ATTORNEY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "DEFEND_ATT_FIRM"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DEFENDANT");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "DEFEND_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[DEF_TEMP]", "CLAIM_ID");
                    sDefSel = "-1";
                    sTmp = string.Empty;
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sTmp += Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    sSQL = "SELECT CLAIM_ID,DEFENDANT_ROW_ID " + sTmp
                        + " [INTO DEF_TEMP]"
                        + " FROM DEFENDANT,ENTITY DEFEND_ENTITY "
                        + " WHERE DEFENDANT.CLAIM_ID = " + p_lRecordID.ToString();

                    if (sDefSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sDefSel;
                    }
                    sSQL = sSQL + " AND DEFENDANT.DEFENDANT_EID = DEFEND_ENTITY.ENTITY_ID";
                    //Start : Added By Nitika for autoMailMerge Exe
                    if (!string.IsNullOrEmpty(sDefendantType))
                    {
                        sSQL = sSQL + " AND DEFENDANT.DFNDNT_TYPE_CODE IN (" + sDefendantType.ToString() + ")";
                    }
                    //End : Added By Nitika for autoMailMerge Exe
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[DEF_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[DEF_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [DEF_TEMP](CLAIM_ID,DEFENDANT_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //DEFENDENT ATTORNEY
                if (Utilities.ExistsInArray(p_objDisplayTables, "DEFEND_ATTORNEY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "DEFEND_ATT_FIRM"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DEFEND_ATTORNEY");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "DEFEND_ATT_FIRM");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[DEF_TEMP]", "[DEF_ATT_TEMP]", "DEFENDANT_ROW_ID");
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sTmp += Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    sSQL = "SELECT DEFENDANT.DEFENDANT_ROW_ID " + sTmp
                        + " [INTO DEF_ATT_TEMP]"
                        + " FROM DEFENDANT,ENTITY DEFEND_ATTORNEY,ENTITY DEFEND_ATT_FIRM "
                        + " WHERE DEFENDANT.CLAIM_ID = " + p_lRecordID.ToString();

                    if (sDefSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sDefSel;
                    }
                    sSQL = sSQL + " AND DEFENDANT.ATTORNEY_EID = DEFEND_ATTORNEY.ENTITY_ID"
                        + " AND DEFEND_ATTORNEY.PARENT_EID = DEFEND_ATT_FIRM.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[DEF_ATT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[DEF_ATT_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [DEF_ATT_TEMP](DEFENDANT_ROW_ID" + sBitFields + ")"
                            + " SELECT DEFENDANT_ROW_ID" + sBitValues + " FROM [DEF_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                if (Utilities.ExistsInArray(p_objDisplayTables, "FUNDS")
                    || Utilities.ExistsInArray(p_objDisplayTables, "PAYEE_ENTITY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "FUNDS_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "FUNDS");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[FUNDS_TEMP]", "CLAIM_ID");
                    sFundsSel = "-1";
                    //if (iIdx > -1)
                    //{
                    //    sSQL = "SELECT CLAIM_ID,TRANS_ID, " + p_objDisplayFields[iIdx].ToString() + " ,PMT_CURRENCY_CODE,CLAIM_CURRENCY_CODE"
                    //        + " [INTO FUNDS_TEMP]"
                    //        + " FROM FUNDS "
                    //        + " WHERE FUNDS.CLAIM_ID =  " + p_lRecordID.ToString();
                    //}
                    //else
                    //{
                    //    sSQL = "SELECT CLAIM_ID,TRANS_ID " 
                    //        + " [INTO FUNDS_TEMP]"
                    //        + " FROM FUNDS "
                    //        + " WHERE FUNDS.CLAIM_ID =  " + p_lRecordID.ToString();
                    //}

                    //if (sFundsSel!="-1")
                    //{
                    //    sSQL = sSQL + " AND " + sFundsSel;
                    //}

                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    m_sDBType = m_objMergeConn.DatabaseType.ToString().ToUpper();


                    // akaushk5 Changed for MITS 37232 Starts
                    //if (objSettings.UseMultiCurrency != 0 && (p_objDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT") > 0 || p_objDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT") > 0))
                    if (objSettings.UseMultiCurrency != 0 && iIdx > -1 && (p_objDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT") > 0 || p_objDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT") > 0))
                    // akaushk5 Changed for MITS 37232 Ends
                    {
                        string[] arPC = null;
                        string[] arCA = null;
                        string[] arFA = null;
                        ArrayList ar = new ArrayList();



                        //********************************************************************************************
                        if (iIdx > -1)
                        {
                            sSQL = "SELECT CLAIM_ID,TRANS_ID, " + p_objDisplayFields[iIdx].ToString() + " ,PMT_CURRENCY_CODE,CLAIM_CURRENCY_CODE"
                                + " [INTO FUNDS_TEMP]"
                                + " FROM FUNDS "
                                + " WHERE FUNDS.CLAIM_ID =  " + p_lRecordID.ToString();
                        }
                        else
                        {
                            sSQL = "SELECT CLAIM_ID,TRANS_ID "
                                + " [INTO FUNDS_TEMP]"
                                + " FROM FUNDS "
                                + " WHERE FUNDS.CLAIM_ID =  " + p_lRecordID.ToString();
                        }

                        if (sFundsSel != "-1")
                        {
                            sSQL = sSQL + " AND " + sFundsSel;
                        }



                        //**********************************************************************************************

                        //m_objMergeConn.ExecuteNonQuery ExecSearchSQL(sSQL, ref p_objConn)
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (p_objDisplayFields[iIdx].ToString().Contains("PMT_CURRENCY_AMOUNT"))
                        {
                            int iDex = p_objDisplayFields[iIdx].ToString().Substring(p_objDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT")).IndexOf(",");
                            if (iDex > -1)
                                arPC = p_objDisplayFields[iIdx].ToString().Substring(p_objDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT")).Substring(0, p_objDisplayFields[iIdx].ToString().Substring(p_objDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT")).IndexOf(",")).Split('|');
                            else
                                arPC = p_objDisplayFields[iIdx].ToString().Substring(p_objDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT")).Split('|');
                        }
                        if (p_objDisplayFields[iIdx].ToString().Contains("CLAIM_CURRENCY_AMOUNT"))
                        {
                            int inDex = p_objDisplayFields[iIdx].ToString().Substring(p_objDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT")).IndexOf(",");
                            if (inDex > -1)
                                arCA = p_objDisplayFields[iIdx].ToString().Substring(p_objDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT")).Substring(0, p_objDisplayFields[iIdx].ToString().Substring(p_objDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT")).IndexOf(",")).Split('|');
                            else
                                arCA = p_objDisplayFields[iIdx].ToString().Substring(p_objDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT")).Split('|');
                        }
                        if (p_objDisplayFields[iIdx].ToString().Contains("FUNDS.AMOUNT"))
                        {
                            int inDex = p_objDisplayFields[iIdx].ToString().Substring(p_objDisplayFields[iIdx].ToString().IndexOf("FUNDS.AMOUNT")).IndexOf(",");
                            if (inDex > -1)
                                arFA = p_objDisplayFields[iIdx].ToString().Substring(p_objDisplayFields[iIdx].ToString().IndexOf("FUNDS.AMOUNT")).Substring(0, p_objDisplayFields[iIdx].ToString().Substring(p_objDisplayFields[iIdx].ToString().IndexOf("FUNDS.AMOUNT")).IndexOf(",")).Split('|');
                            else
                                arFA = p_objDisplayFields[iIdx].ToString().Substring(p_objDisplayFields[iIdx].ToString().IndexOf("FUNDS.AMOUNT")).Split('|');
                        }
                        string sSQLTempNew = "SELECT " + p_objDisplayFields[iIdx].ToString().Replace("FUNDS.AMOUNT", "").Replace("FUNDS.CLAIM_CURRENCY_AMOUNT", "").Replace("FUNDS.PMT_CURRENCY_AMOUNT", "")
                                     + " ,[FUNDS_TEMP].PMT_CURRENCY_CODE,[FUNDS_TEMP].CLAIM_CURRENCY_CODE FROM [FUNDS_TEMP]";
                        if (arCA != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arCA[1].Trim(), arCA[1].Trim() + " " + arCA[0].Trim());

                            m_objMergeCmd.CommandType = CommandType.Text;
                            //Start averma62 - MITS 31571
                            if (m_sDBType == Constants.DB_SQLSRVR)
                            {
                                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL("ALTER TABLE [FUNDS_TEMP] ALTER COLUMN "
                                   + arCA[1].Trim() + " VARCHAR(30) ");
                                m_objMergeCmd.ExecuteNonQuery();
                            }

                            if (m_sDBType == Constants.DB_ORACLE)
                            {
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("SELECT  * [INTO FUNDS_TEMP_NEW] FROM [FUNDS_TEMP]"));
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("TRUNCATE TABLE [FUNDS_TEMP]"));
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("ALTER TABLE [FUNDS_TEMP] MODIFY " + arCA[1].Trim() + " VARCHAR2(30) "));
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("INSERT INTO [FUNDS_TEMP] SELECT * FROM [FUNDS_TEMP_NEW]"));
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("DROP TABLE [FUNDS_TEMP_NEW]"));
                            }
                        }
                        if (arPC != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arPC[1].Trim(), arPC[1].Trim() + " " + arPC[0].Trim());

                            m_objMergeCmd.CommandType = CommandType.Text;
                            if (m_sDBType == Constants.DB_SQLSRVR)
                            {
                                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL("ALTER TABLE [FUNDS_TEMP] ALTER COLUMN "
                                        + arPC[1].Trim() + " VARCHAR(30) ");
                                m_objMergeCmd.ExecuteNonQuery();
                            }
                            if (m_sDBType == Constants.DB_ORACLE)
                            {
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("SELECT  * [INTO FUNDS_TEMP_NEW] FROM [FUNDS_TEMP]"));
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("TRUNCATE TABLE [FUNDS_TEMP]"));
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("ALTER TABLE [FUNDS_TEMP] MODIFY " + arPC[1].Trim() + " VARCHAR2(30) "));
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("INSERT INTO [FUNDS_TEMP] SELECT * FROM [FUNDS_TEMP_NEW]"));
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("DROP TABLE [FUNDS_TEMP_NEW]"));
                            }
                        }
                        if (arFA != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arFA[1].Trim(), arFA[1].Trim() + " " + "AMOUNT");

                            m_objMergeCmd.CommandType = CommandType.Text;
                            if (m_sDBType == Constants.DB_SQLSRVR)
                            {
                                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL("ALTER TABLE [FUNDS_TEMP] ALTER COLUMN "
                                        + arFA[1].Trim() + " VARCHAR(30) ");
                                m_objMergeCmd.ExecuteNonQuery();
                            }
                            if (m_sDBType == Constants.DB_ORACLE)
                            {
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("SELECT  * [INTO FUNDS_TEMP_NEW] FROM [FUNDS_TEMP]"));
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("TRUNCATE TABLE [FUNDS_TEMP]"));
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("ALTER TABLE [FUNDS_TEMP] MODIFY " + arFA[1].Trim() + " VARCHAR2(30) "));
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("INSERT INTO [FUNDS_TEMP] SELECT * FROM [FUNDS_TEMP_NEW]"));
                                m_objMergeConn.ExecuteNonQuery(m_objGeneric.SafeSQL("DROP TABLE [FUNDS_TEMP_NEW]"));
                            }
                            //End averma62 - MITS 31571
                        }
                        DbConnection objNewConn = m_objMergeCmd.Connection;
                        using (DbReader rdr = objNewConn.ExecuteReader(m_objGeneric.SafeSQL(sSQLTempNew)))
                        {
                            while (rdr.Read())
                            {
                                if (arCA != null)
                                {
                                    string sClaimAmt = Conversion.ConvertObjToStr(rdr.GetValue("CLAIM_CURRENCY_AMOUNT"));
                                    string sCurrCode = Conversion.ConvertObjToStr(rdr.GetValue("CLAIM_CURRENCY_CODE"));
                                    ar.Add(m_objGeneric.SafeSQL("UPDATE [FUNDS_TEMP] SET " + arCA[1].Trim() + "='" + sClaimAmt + "_" + sCurrCode +
                                        "' WHERE " + arCA[1].Trim() + "= '" + sClaimAmt + "' AND [FUNDS_TEMP].CLAIM_CURRENCY_CODE=" + sCurrCode));
                                }
                                if (arPC != null)
                                {
                                    string sPmtAmt = Conversion.ConvertObjToStr(rdr.GetValue("PMT_CURRENCY_AMOUNT"));
                                    string sCurrCode = Conversion.ConvertObjToStr(rdr.GetValue("PMT_CURRENCY_CODE"));
                                    ar.Add(m_objGeneric.SafeSQL("UPDATE [FUNDS_TEMP] SET " + arPC[1].Trim() + "='" + sPmtAmt + "_" + sCurrCode +
                                        "' WHERE " + arPC[1].Trim() + "= '" + sPmtAmt + "' AND [FUNDS_TEMP].PMT_CURRENCY_CODE=" + sCurrCode));
                                }
                            }
                        }
                        foreach (string sStr in ar)
                        {
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sStr);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                        objNewConn = null;
                    }
                    else
                    {
                        if (iIdx > -1)
                        {
                            sSQL = "SELECT CLAIM_ID,TRANS_ID, " + p_objDisplayFields[iIdx].ToString() + " ,PMT_CURRENCY_CODE,CLAIM_CURRENCY_CODE"
                                + " [INTO FUNDS_TEMP]"
                                + " FROM FUNDS "
                                + " WHERE FUNDS.CLAIM_ID =  " + p_lRecordID.ToString();
                        }
                        else
                        {
                            sSQL = "SELECT CLAIM_ID,TRANS_ID "
                                + " [INTO FUNDS_TEMP]"
                                + " FROM FUNDS "
                                + " WHERE FUNDS.CLAIM_ID =  " + p_lRecordID.ToString();
                        }

                        if (sFundsSel != "-1")
                        {
                            sSQL = sSQL + " AND " + sFundsSel;
                        }

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();



                    }




                    //


                    if (m_objGeneric.GetMergeCount("[FUNDS_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[FUNDS_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        if (p_objDisplayFields[iIdx].ToString().IndexOf("CTL_NUMBER") != -1)
                        {
                            sTmp = p_objDisplayFields[iIdx].ToString();
                            iPos = p_objDisplayFields[iIdx].ToString().IndexOf("CTL_NUMBER |");
                            iPosNext = p_objDisplayFields[iIdx].ToString().IndexOf(",", iPos);

                            if (iPosNext == -1)
                            {
                                sTmp = sTmp.Substring(iPos + 12).Trim();
                            }
                            else
                            {
                                sTmp = sTmp.Substring(iPos + 12, (iPosNext - (iPos + 12))).Trim();
                            }
                        }
                        // rrachev MITS# 36712 BEGIN
                        //{
                        string sCurrencyFields = String.Empty;
                        string sCurrencyValues = String.Empty;

                        if (iIdx > -1)
                        {
                            sCurrencyFields = ",PMT_CURRENCY_CODE,CLAIM_CURRENCY_CODE";
                            sCurrencyValues = ",0,0";
                        }

                        if (sTmp != "")
                        {
                            sSQL = "INSERT INTO [FUNDS_TEMP](CLAIM_ID,TRANS_ID," + sTmp + sBitFields + sCurrencyFields + ")"
                                + " SELECT CLAIM_ID, 0, ' '" + sBitValues + sCurrencyValues + " FROM [CLAIM_TEMP]";
                        }
                        else
                        {
                            sSQL = "INSERT INTO [FUNDS_TEMP](CLAIM_ID,TRANS_ID" + sBitFields + sCurrencyFields + ")"
                                + " SELECT CLAIM_ID, 0 " + sBitValues + sCurrencyValues + " FROM [CLAIM_TEMP]";
                        }

                        //if (sTmp != "")
                        //{
                        //    sSQL = "INSERT INTO [FUNDS_TEMP](CLAIM_ID,TRANS_ID," + sTmp + sBitFields + ")"
                        //        + " SELECT CLAIM_ID, 0, ' '" + sBitValues + " FROM [CLAIM_TEMP]";
                        //}
                        //else
                        //{
                        //    sSQL = "INSERT INTO [FUNDS_TEMP](CLAIM_ID,TRANS_ID" + sBitFields + ")"
                        //        + " SELECT CLAIM_ID, 0 " + sBitValues + " FROM [CLAIM_TEMP]";
                        //}
                        // } rrachev MITS# 36712 END
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Funds Supplemental
                if (Utilities.ExistsInArray(p_objDisplayTables, "FUNDS_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "FUNDS_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[FUNDS_TEMP]", "[FUNDS_SUPP_TEMP]", "TRANS_ID");
                    sSQL = "SELECT [FUNDS_TEMP].TRANS_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO FUNDS_SUPP_TEMP]"
                        + " FROM [FUNDS_TEMP],FUNDS_SUPP"
                        + " WHERE FUNDS_SUPP.TRANS_ID = [FUNDS_TEMP].TRANS_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    if (m_objGeneric.GetMergeCount("[FUNDS_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[FUNDS_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [FUNDS_SUPP_TEMP](TRANS_ID" + sBitFields + ")"
                            + " SELECT TRANS_ID " + sBitValues + " FROM [FUNDS_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //skhare7 R8 Enhancements Subrogation
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_SUBRO") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "SUBRO_SPECIALIST_ENTITY") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "SUBRO_ADVPARTY_ENTITY") ||
                       Utilities.ExistsInArray(p_objDisplayTables, "SUBRO_COMPANY_ENTITY") ||
                         Utilities.ExistsInArray(p_objDisplayTables, "SUBROGATION_SUPP") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "SUBRO_ADJUSTER_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_X_SUBRO");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SUB_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM_ID,SUBROGATION_ROW_ID " + sTmp + " [INTO SUB_TEMP]"
                        + " FROM CLAIM_X_SUBRO "
                        + " WHERE CLAIM_X_SUBRO.CLAIM_ID = " + p_lRecordID.ToString();
                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[SUB_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[SUB_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [SUB_TEMP](CLAIM_ID,SUBROGATION_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Sub SUPPLEMENTAL
                if (Utilities.ExistsInArray(p_objDisplayTables, "SUBROGATION_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "SUBROGATION_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SUB_SUPP_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_SUBRO.SUBROGATION_ROW_ID " + sTmp
                        + " [INTO SUB_SUPP_TEMP]"
                        + " FROM CLAIM, CLAIM_X_SUBRO, SUBROGATION_SUPP "
                        + " WHERE CLAIM_X_SUBRO.CLAIM_ID = " + p_lRecordID.ToString()
                        + " AND CLAIM.CLAIM_ID = CLAIM_X_SUBRO.CLAIM_ID"
                        + " AND SUBROGATION_SUPP.SUBROGATION_ROW_ID = CLAIM_X_SUBRO.SUBROGATION_ROW_ID";

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[SUB_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[SUB_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [SUB_SUPP_TEMP](CLAIM_ID,SUBROGATION_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //subrogation Entity
                if (Utilities.ExistsInArray(p_objDisplayTables, "SUBRO_SPECIALIST_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "SUBRO_SPECIALIST_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SUB_SPE_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_SUBRO.SUB_SPECIALIST_EID " + sTmp
                        + " [INTO SUB_SPE_TEMP]"
                        + " FROM CLAIM, CLAIM_X_SUBRO, ENTITY SUBRO_SPECIALIST_ENTITY "
                        + " WHERE CLAIM.CLAIM_ID = CLAIM_X_SUBRO.CLAIM_ID"
                        + " AND SUBRO_SPECIALIST_ENTITY.ENTITY_ID = CLAIM_X_SUBRO.SUB_SPECIALIST_EID"
                        + " AND CLAIM_X_SUBRO.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[SUB_SPE_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[SUB_SPE_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [SUB_SPE_TEMP](CLAIM_ID,SUB_SPECIALIST_EID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //subrogation Entity
                if (Utilities.ExistsInArray(p_objDisplayTables, "SUBRO_ADVPARTY_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "SUBRO_ADVPARTY_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SUB_ADP_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_SUBRO.SUB_ADVERSE_PARTY_EID " + sTmp
                        + " [INTO SUB_ADP_TEMP]"
                        + " FROM CLAIM, CLAIM_X_SUBRO, ENTITY SUBRO_ADVPARTY_ENTITY "
                        + " WHERE CLAIM.CLAIM_ID = CLAIM_X_SUBRO.CLAIM_ID"
                        + " AND SUBRO_ADVPARTY_ENTITY.ENTITY_ID = CLAIM_X_SUBRO.SUB_ADVERSE_PARTY_EID"
                        + " AND CLAIM_X_SUBRO.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[SUB_ADP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[SUB_ADP_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [SUB_ADP_TEMP](CLAIM_ID,SUB_ADVERSE_PARTY_EID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //subrogation Entity
                if (Utilities.ExistsInArray(p_objDisplayTables, "SUBRO_COMPANY_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "SUBRO_COMPANY_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SUB_COMP_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_SUBRO.SUB_ADVERSE_INS_CO_EID  " + sTmp
                        + " [INTO SUB_COMP_TEMP]"
                        + " FROM CLAIM, CLAIM_X_SUBRO, ENTITY SUBRO_COMPANY_ENTITY "
                        + " WHERE CLAIM.CLAIM_ID = CLAIM_X_SUBRO.CLAIM_ID"
                        + " AND SUBRO_COMPANY_ENTITY.ENTITY_ID = CLAIM_X_SUBRO.SUB_ADVERSE_INS_CO_EID "
                        + " AND CLAIM_X_SUBRO.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[SUB_COMP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[SUB_COMP_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [SUB_COMP_TEMP](CLAIM_ID,SUB_ADVERSE_INS_CO_EID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "SUBRO_ADJUSTER_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "SUBRO_ADJUSTER_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SUB_ADJ_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_SUBRO.SUB_ADV_ADJUSTER_EID  " + sTmp
                        + " [INTO SUB_ADJ_TEMP]"
                        + " FROM CLAIM, CLAIM_X_SUBRO, ENTITY SUBRO_ADJUSTER_ENTITY "
                        + " WHERE CLAIM.CLAIM_ID = CLAIM_X_SUBRO.CLAIM_ID"
                        + " AND SUBRO_ADJUSTER_ENTITY.ENTITY_ID = CLAIM_X_SUBRO.SUB_ADV_ADJUSTER_EID "
                        + " AND CLAIM_X_SUBRO.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[SUB_ADJ_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[SUB_ADJ_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [SUB_ADJ_TEMP](CLAIM_ID,SUB_ADV_ADJUSTER_EID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //liability

                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_LIABILITYLOSS") ||
                  Utilities.ExistsInArray(p_objDisplayTables, "LIABILTY_PARTY_NAME") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_LIABILITYLOSS_SUPP") ||
                  Utilities.ExistsInArray(p_objDisplayTables, "LIABILTY_OWNER_NAME"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_X_LIABILITYLOSS");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LIAB_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM_ID,ROW_ID " + sTmp + " [INTO LIAB_TEMP]"
                        + " FROM CLAIM_X_LIABILITYLOSS "
                        + " WHERE CLAIM_X_LIABILITYLOSS.CLAIM_ID = " + p_lRecordID.ToString();
                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[LIAB_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[LIAB_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [LIAB_TEMP](CLAIM_ID,ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "LIABILTY_PARTY_NAME"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "LIABILTY_PARTY_NAME");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LIAB_PARTY_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_LIABILITYLOSS.PARTY_AFFECTED  " + sTmp
                        + " [INTO LIAB_PARTY_TEMP]"
                        + " FROM CLAIM, CLAIM_X_LIABILITYLOSS, ENTITY LIABILTY_PARTY_NAME "
                        + " WHERE CLAIM.CLAIM_ID = CLAIM_X_LIABILITYLOSS.CLAIM_ID"
                        + " AND LIABILTY_PARTY_NAME.ENTITY_ID = CLAIM_X_LIABILITYLOSS.PARTY_AFFECTED "
                        + " AND CLAIM_X_LIABILITYLOSS.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[LIAB_PARTY_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[LIAB_PARTY_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [LIAB_PARTY_TEMP](CLAIM_ID,PARTY_AFFECTED" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "LIABILTY_OWNER_NAME"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "LIABILTY_OWNER_NAME");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LAIB_OWN_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_LIABILITYLOSS.POLICYHOLDER  " + sTmp
                        + " [INTO LAIB_OWN_TEMP]"
                        + " FROM CLAIM, CLAIM_X_LIABILITYLOSS, ENTITY LIABILTY_OWNER_NAME "
                        + " WHERE CLAIM.CLAIM_ID = CLAIM_X_LIABILITYLOSS.CLAIM_ID"
                        + " AND LIABILTY_OWNER_NAME.ENTITY_ID = CLAIM_X_LIABILITYLOSS.POLICYHOLDER "
                        + " AND CLAIM_X_LIABILITYLOSS.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[LAIB_OWN_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[LAIB_OWN_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [LAIB_OWN_TEMP](CLAIM_ID,POLICYHOLDER" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_LIABILITYLOSS_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_X_LIABILITYLOSS_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LIAB_SUPP_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_LIABILITYLOSS.ROW_ID " + sTmp
                        + " [INTO LIAB_SUPP_TEMP]"
                        + " FROM CLAIM, CLAIM_X_LIABILITYLOSS, CLAIM_X_LIABILITYLOSS_SUPP "
                        + " WHERE CLAIM_X_LIABILITYLOSS.CLAIM_ID = " + p_lRecordID.ToString()
                        + " AND CLAIM.CLAIM_ID = CLAIM_X_LIABILITYLOSS.CLAIM_ID"
                        + " AND CLAIM_X_LIABILITYLOSS_SUPP.ROW_ID = CLAIM_X_LIABILITYLOSS.ROW_ID";

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[LIAB_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[LIAB_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [LIAB_SUPP_TEMP](CLAIM_ID,ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Property Loss
                //mgaba2: mits 29259:adding for CLAIM_X_PROPERTYINVOLVED
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_PROPERTYLOSS") ||
                Utilities.ExistsInArray(p_objDisplayTables, "PROPERTY_OWNER_NAME") ||
                Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_PROPERTYINVOLVED") ||
                     Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_PROPERTYLOSS_SUPP") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "PROPERTY_PHOLDER_NAME"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_X_PROPERTYLOSS");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROPLOSS_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM_ID,ROW_ID " + sTmp + " [INTO PROPLOSS_TEMP]"
                        + " FROM CLAIM_X_PROPERTYLOSS "
                        + " WHERE CLAIM_X_PROPERTYLOSS.CLAIM_ID = " + p_lRecordID.ToString();
                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PROPLOSS_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PROPLOSS_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [PROPLOSS_TEMP](CLAIM_ID,ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                if (Utilities.ExistsInArray(p_objDisplayTables, "PROPERTY_OWNER_NAME"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PROPERTY_OWNER_NAME");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROP_OWNR_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_PROPERTYLOSS.OWNER   " + sTmp
                        + " [INTO PROP_OWNR_TEMP]"
                        + " FROM CLAIM, CLAIM_X_PROPERTYLOSS, ENTITY PROPERTY_OWNER_NAME "
                        + " WHERE CLAIM.CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID"
                        + " AND PROPERTY_OWNER_NAME.ENTITY_ID = CLAIM_X_PROPERTYLOSS.OWNER "
                        + " AND CLAIM_X_PROPERTYLOSS.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PROP_OWNR_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PROP_OWNR_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [PROP_OWNR_TEMP](CLAIM_ID,OWNER" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "PROPERTY_PHOLDER_NAME"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PROPERTY_PHOLDER_NAME");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROP_PHOLD_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_PROPERTYLOSS.POLICYHOLDER   " + sTmp
                        + " [INTO PROP_PHOLD_TEMP]"
                        + " FROM CLAIM, CLAIM_X_PROPERTYLOSS, ENTITY PROPERTY_PHOLDER_NAME "
                        + " WHERE CLAIM.CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID"
                        + " AND PROPERTY_PHOLDER_NAME.ENTITY_ID = CLAIM_X_PROPERTYLOSS.POLICYHOLDER "
                        + " AND CLAIM_X_PROPERTYLOSS.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PROP_PHOLD_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PROP_PHOLD_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [PROP_PHOLD_TEMP](CLAIM_ID,POLICYHOLDER" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_PROPERTYLOSS_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_X_PROPERTYLOSS_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROPLOSS_SUPP_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_PROPERTYLOSS.ROW_ID " + sTmp
                        + " [INTO PROPLOSS_SUPP_TEMP]"
                        + " FROM CLAIM, CLAIM_X_PROPERTYLOSS, CLAIM_X_PROPERTYLOSS_SUPP "
                        + " WHERE CLAIM_X_PROPERTYLOSS.CLAIM_ID = " + p_lRecordID.ToString()
                        + " AND CLAIM.CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID"
                        + " AND CLAIM_X_PROPERTYLOSS_SUPP.ROW_ID = CLAIM_X_PROPERTYLOSS.ROW_ID";

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PROPLOSS_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PROPLOSS_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [PROPLOSS_SUPP_TEMP](CLAIM_ID,ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //mgaba2: mits 29259:adding for CLAIM_X_PROPERTYINVOLVED
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_PROPERTYINVOLVED"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_X_PROPERTYINVOLVED");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROPINV_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, CLAIM_X_PROPERTYINVOLVED.ROW_ID " + sTmp
                        + " [INTO PROPINV_TEMP]"
                        + " FROM CLAIM, CLAIM_X_PROPERTYLOSS, CLAIM_X_PROPERTYINVOLVED "
                        + " WHERE CLAIM_X_PROPERTYLOSS.CLAIM_ID = " + p_lRecordID.ToString()
                        + " AND CLAIM.CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID"
                        + " AND CLAIM_X_PROPERTYINVOLVED.ROW_ID = CLAIM_X_PROPERTYLOSS.ROW_ID";

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PROPINV_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PROPINV_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [PROPINV_TEMP](CLAIM_ID,ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Salvage
                if (Utilities.ExistsInArray(p_objDisplayTables, "SALVAGE") ||
                  (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_X_PROPERTYLOSS")))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "SALVAGE");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SALVAGEP_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }


                    sSQL = "SELECT CLAIM_ID,SALVAGE_ROW_ID " + sTmp + " [INTO SALVAGEP_TEMP]"
                    + " FROM CLAIM_X_PROPERTYLOSS ,SALVAGE"
                    + " WHERE SALVAGE.PARENT_NAME='ClaimXPropertyLoss'"
                   + " AND CLAIM_X_PROPERTYLOSS.ROW_ID=SALVAGE.PARENT_ID AND "
                    + " CLAIM_X_PROPERTYLOSS.CLAIM_ID = " + p_lRecordID.ToString();
                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }


                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[SALVAGEP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[SALVAGEP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [SALVAGEP_TEMP](CLAIM_ID,SALVAGE_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "SALVAGE_UNIT") ||
                (Utilities.ExistsInArray(p_objDisplayTables, "UNIT_X_CLAIM")))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "SALVAGE_UNIT");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SALVAGEU_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }


                    sSQL = "SELECT CLAIM_ID,SALVAGE_ROW_ID " + sTmp + " [INTO SALVAGEU_TEMP]"
                        + " FROM UNIT_X_CLAIM ,SALVAGE SALVAGE_UNIT"
                        + " WHERE UNIT_X_CLAIM.UNIT_ROW_ID=SALVAGE_UNIT.PARENT_ID AND "
                        + " SALVAGE_UNIT.PARENT_NAME='UnitXClaim' AND "
                        + " UNIT_X_CLAIM.CLAIM_ID = " + p_lRecordID.ToString();
                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }


                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[SALVAGEU_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[SALVAGEU_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [SALVAGEU_TEMP](CLAIM_ID,SALVAGE_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "UNTSALVAGE_OWNER_NAME"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "UNTSALVAGE_OWNER_NAME");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[UNIT_SALB_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, SALVAGE.BUYER_EID   " + sTmp
                        + " [INTO UNIT_SALB_TEMP]"
                        + " FROM CLAIM, UNIT_X_CLAIM,SALVAGE, ENTITY UNTSALVAGE_OWNER_NAME "
                        + " WHERE CLAIM.CLAIM_ID = UNIT_X_CLAIM.CLAIM_ID"
                        + " AND UNTSALVAGE_OWNER_NAME.ENTITY_ID = SALVAGE.BUYER_EID "
                         + " AND SALVAGE.PARENT_ID = UNIT_X_CLAIM.UNIT_ROW_ID AND "
                          + " SALVAGE.PARENT_NAME='UnitXClaim'"
                        + " AND UNIT_X_CLAIM.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[UNIT_SALB_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[UNIT_SALB_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [UNIT_SALB_TEMP](CLAIM_ID,BUYER_EID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //mits 29452 asingh263 starts
                if (Utilities.ExistsInArray(p_objDisplayTables, "UNITSALVAGE_INPOSS_NAME"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "UNITSALVAGE_INPOSS_NAME");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SALVAGEIP_TEMP]", "CLAIM_ID");
                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID,  SALVAGE.POSSESSION_OF   " + sTmp
                                        + " [INTO SALVAGEIP_TEMP]"
                                        + " FROM CLAIM_X_PROPERTYLOSS, CLAIM, SALVAGE,ENTITY UNITSALVAGE_INPOSS_NAME"
                                        + " WHERE CLAIM_X_PROPERTYLOSS.CLAIM_ID=CLAIM.CLAIM_ID "
                                        + " AND SALVAGE.PARENT_ID=CLAIM_X_PROPERTYLOSS.ROW_ID "
                                        + "AND UNITSALVAGE_INPOSS_NAME.ENTITY_ID=SALVAGE.POSSESSION_OF"
                                    + " AND CLAIM.CLAIM_ID= " + p_lRecordID.ToString();
                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[SALVAGEIP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[SALVAGEIP_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [SALVAGEIP_TEMP](CLAIM_ID,SALVAGE.POSSESSION_OF" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                if (Utilities.ExistsInArray(p_objDisplayTables, "UNITSALVAGE_YARD_ADD"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "UNITSALVAGE_YARD_ADD");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SALVAGEY_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    sSQL = "SELECT CLAIM.CLAIM_ID, SALVAGE.SALVAGE_YARD_ADDR_ID  " + sTmp
                        + " [INTO SALVAGEY_TEMP]"
                                        + " from CLAIM_X_PROPERTYLOSS, CLAIM, SALVAGE,ENTITY_X_ADDRESSES UNITSALVAGE_YARD_ADD"
                                        + " where CLAIM_X_PROPERTYLOSS.CLAIM_ID=CLAIM.CLAIM_ID"
                                        + " AND SALVAGE.PARENT_ID=CLAIM_X_PROPERTYLOSS.ROW_ID"
                                        + " AND UNITSALVAGE_YARD_ADD.ADDRESS_ID=SALVAGE.SALVAGE_YARD_ADDR_ID"
                                        + " AND CLAIM.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[SALVAGEY_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[SALVAGEY_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [SALVAGEY_TEMP](CLAIM_ID,SALVAGE_YARD_ADDR_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //mits 29452 asingh263 ends
                if (Utilities.ExistsInArray(p_objDisplayTables, "PROPSALVAGE_OWNER_NAME"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PROPSALVAGE_OWNER_NAME");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROP_SALB_TEMP]", "CLAIM_ID");

                    sLitSel = "-1";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL = "SELECT CLAIM.CLAIM_ID, SALVAGE.BUYER_EID   " + sTmp
                        + " [INTO PROP_SALB_TEMP]"
                        + " FROM CLAIM, CLAIM_X_PROPERTYLOSS,SALVAGE, ENTITY PROPSALVAGE_OWNER_NAME "
                        + " WHERE CLAIM.CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID"
                        + " AND PROPSALVAGE_OWNER_NAME.ENTITY_ID = SALVAGE.BUYER_EID "
                         + " AND SALVAGE.PARENT_ID = CLAIM_X_PROPERTYLOSS.ROW_ID "
                           + " AND  SALVAGE.PARENT_NAME='ClaimXPropertyLoss'"
                        + " AND CLAIM_X_PROPERTYLOSS.CLAIM_ID = " + p_lRecordID;

                    if (sLitSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sLitSel;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PROP_SALB_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PROP_SALB_TEMP]", ref sBitFields, ref sBitValues);

                        sSQL = "INSERT INTO [PROP_SALB_TEMP](CLAIM_ID,BUYER_EID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }



                //skhare7 End
                //claim reserves 
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_RES_TOT"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_RES_TOT");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLMRES_TEMP]", "CLAIM_ID");
                    sFundsSel = "-1";
                    ArrayList objTemp = new ArrayList();
                    sSQL = "";
                    //Shruti, 12185 starts
                    int iLOB = 0;
                    int iCType = 0;
                    bool bResByClaimType = false;
                    bool bAll = false;
                    bool bBal = false;
                    bool bIncur = false;
                    bool bAllDone = false;
                    bool bCollInIncurredBal = false;
                    bool bCollInRsvBal = false;
                    double dRAmount = 0;
                    double dRPaid = 0;
                    double dRCollect = 0;
                    double dRBalance = 0;
                    double dRIncurred = 0;

                    double dBalTotal = 0;
                    double dPaidTotal = 0;
                    double dCollTotal = 0;
                    double dIncTotal = 0;
                    double dResTotal = 0;
                    string sResult = "";



                    sSQL = "SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + p_lRecordID.ToString();

                    objReader = DbFactory.GetDbReader(m_sConnectionString, m_objGeneric.SafeSQL(sSQL));
                    if (objReader.Read())
                    {
                        iLOB = Conversion.ConvertObjToInt(objReader.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
                        iCType = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_TYPE_CODE"), m_iClientId);
                    }

                    sSQL = "SELECT RES_BY_CLM_TYPE, COLL_IN_RSV_BAL, COLL_IN_INCUR_BAL FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + iLOB;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, m_objGeneric.SafeSQL(sSQL));
                    if (objReader.Read())
                    {
                        bResByClaimType = Conversion.ConvertObjToBool(objReader.GetValue("RES_BY_CLM_TYPE"), m_iClientId);
                        bCollInIncurredBal = Conversion.ConvertObjToBool(objReader.GetValue("COLL_IN_INCUR_BAL"), m_iClientId);
                        bCollInRsvBal = Conversion.ConvertObjToBool(objReader.GetValue("COLL_IN_RSV_BAL"), m_iClientId);
                    }

                    sSQL = "";
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf(".");
                    iPosNext = p_objDisplayFields[iIdx].ToString().IndexOf("|");
                    while (iPos != -1)
                    {
                        sTmp = p_objDisplayFields[iIdx].ToString().Substring(iPos + 1, iPosNext - iPos - 1).Trim();
                        sTmp = sTmp.Substring(0, sTmp.Length - 2);
                        iPos = p_objDisplayFields[iIdx].ToString().IndexOf(",", iPosNext);
                        if (iPos != -1)
                        {
                            sTmpNext = p_objDisplayFields[iIdx].ToString().Substring(iPosNext + 1, iPos - iPosNext - 1).Trim();
                        }
                        else
                        {
                            sTmpNext = p_objDisplayFields[iIdx].ToString().Substring(iPosNext + 1);
                        }
                        objTemp.Add(sTmp + "|" + sTmpNext);
                        if (sSQL.Length > 0)
                        {
                            sSQL = sSQL + ",";
                        }
                        if (m_lDbMake == (int)eDatabaseType.DBMS_IS_ACCESS)
                        {
                            sSQL = sSQL + sTmpNext + " DOUBLE";
                        }
                        else if (m_lDbMake == (int)eDatabaseType.DBMS_IS_SYBASE || m_lDbMake == (int)eDatabaseType.DBMS_IS_SQLSRVR)
                        {
                            sSQL = sSQL + sTmpNext + " float";
                        }
                        else if (m_lDbMake == (int)eDatabaseType.DBMS_IS_INFORMIX || m_lDbMake == (int)eDatabaseType.DBMS_IS_DB2)
                        {
                            sSQL = sSQL + sTmpNext + " FLOAT";
                        }
                        else if (m_lDbMake == (int)eDatabaseType.DBMS_IS_ORACLE)
                        {
                            sSQL = sSQL + sTmpNext + " NUMBER";
                        }
                        iPos = p_objDisplayFields[iIdx].ToString().IndexOf(".", iPosNext + 1);
                        iPosNext = p_objDisplayFields[iIdx].ToString().IndexOf("|", iPosNext + 1);
                    }
                    if (m_lDbMake == (int)eDatabaseType.DBMS_IS_ACCESS)
                    {
                        sSQL = "CREATE TABLE [CLMRES_TEMP](CLAIM_ID LONG," + sSQL + ")";
                    }
                    else if (m_lDbMake == (int)eDatabaseType.DBMS_IS_SYBASE || m_lDbMake == (int)eDatabaseType.DBMS_IS_SQLSRVR)
                    {
                        sSQL = "CREATE TABLE [CLMRES_TEMP](CLAIM_ID int," + sSQL + ")";
                    }
                    else if (m_lDbMake == (int)eDatabaseType.DBMS_IS_INFORMIX || m_lDbMake == (int)eDatabaseType.DBMS_IS_DB2)
                    {
                        sSQL = "CREATE TABLE [CLMRES_TEMP](CLAIM_ID INTEGER," + sSQL + ")";
                    }
                    else if (m_lDbMake == (int)eDatabaseType.DBMS_IS_ORACLE)
                    {
                        sSQL = "CREATE TABLE [CLMRES_TEMP](CLAIM_ID NUMBER(10)," + sSQL + ")";
                    }

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    sSQLStart = "INSERT INTO [CLMRES_TEMP](CLAIM_ID";
                    sSQLMid = ") values(" + p_lRecordID.ToString();
                    sCols = "";
                    sValues = "";
                    sTemp = "";

                    for (int i = 0; i < objTemp.Count; i++)
                    {
                        sTmp = objTemp[i].ToString();
                        iPos = sTmp.IndexOf("|");
                        sTemp = sTmp.Substring(0, iPos);

                        sSQL = "SELECT RESERVE_TYPE_CODE, SUM(RESERVE_AMOUNT)  RES_AMOUNT," //removed RES_STATUS_CODE MITS 32665 asharma326
                            + " SUM(PAID_TOTAL)  PAID_TOTAL, SUM(COLLECTION_TOTAL)  COLL_TOTAL "
                            + " FROM RESERVE_CURRENT "
                            + " WHERE CLAIM_ID = " + p_lRecordID.ToString();


                        if (sTemp.StartsWith("PAID"))
                        {
                            bBal = false;
                            bIncur = false;
                            sTemp = sTemp.Replace("PAID", "");
                            sResult = "PAID_TOTAL";
                            //sSQL  = String.Format(sSQL,"PAID_TOTAL");
                        }
                        else if (sTmp.StartsWith("COLLECTED"))
                        {
                            bBal = false;
                            bIncur = false;
                            sTemp = sTemp.Replace("COLLECTED", "");
                            sResult = "COLL_TOTAL";
                            //sSQL  = String.Format(sSQL,"COLLECTION_TOTAL");
                        }
                        else if (sTmp.StartsWith("INCURRED"))
                        {
                            bBal = false;
                            bIncur = true;
                            sTemp = sTemp.Replace("INCURRED", "");
                            //sSQL  = String.Format(sSQL,"INCURRED_AMOUNT");
                            sResult = "";
                        }
                        else if (sTmp.StartsWith("BALANCE"))
                        {
                            bBal = true;
                            bIncur = false;
                            sTemp = sTemp.Replace("BALANCE", "");
                            sResult = "";
                            //sSQL  = String.Format(sSQL,"BALANCE_AMOUNT");
                        }
                        else if (sTmp.StartsWith("RESERVE"))
                        {
                            bBal = false;
                            bIncur = false;
                            sTemp = sTemp.Replace("RESERVE", "");
                            //sSQL  = String.Format(sSQL,"RESERVE_AMOUNT");
                            sResult = "RES_AMOUNT";
                        }

                        if (!sTemp.EndsWith("<ALL>"))
                        {
                            bAll = false;
                            if (Common.Conversion.ConvertStrToInteger(sTemp) != 0)
                                sSQL += " AND RESERVE_TYPE_CODE = " + sTemp;
                            else//Throw Error - invalid reserve type recorded in merge definition.
                                throw new ExceptionTypes.RMAppException(Globalization.GetString("MailMergeQuery.InvalidMergeReserveTotalType.Error",m_iClientId));//dvatsa-cloud
                        }
                        else
                        {
                            bAll = true;
                        }

                        sSQL += " AND RESERVE_TYPE_CODE IN (";
                        sSQL += " SELECT RESERVE_TYPE_CODE";
                        if (bResByClaimType)
                        {
                            sSQL += " FROM SYS_CLM_TYPE_RES, CODES_TEXT ";
                            sSQL += " WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ";
                            sSQL += " LINE_OF_BUS_CODE = " + iLOB;
                            sSQL += " AND CLAIM_TYPE_CODE = " + iCType + " )";
                        }
                        else // Get all reserve buckets that are available
                        {
                            sSQL += " FROM SYS_LOB_RESERVES, CODES_TEXT ";
                            sSQL += " WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ";
                            sSQL += " LINE_OF_BUS_CODE = " + iLOB + " )";
                        }
                        // sSQL += " GROUP BY RESERVE_TYPE_CODE, RES_STATUS_CODE ";
                        sSQL += " GROUP BY RESERVE_TYPE_CODE ";//removed RES_STATUS_CODE MITS 32665 asharma326
                        if ((bAll && !bAllDone) || (!bAll && bBal) || (!bAll && bIncur))
                        {

                            objDataSet = DbFactory.GetDataSet(m_sConnectionString, m_objGeneric.SafeSQL(sSQL),m_iClientId);//dvatsa-cloud
                            if (objDataSet.Tables[0] != null)
                            {
                                if (objDataSet.Tables[0].Rows.Count != 0)
                                {
                                    for (int j = 0; j < objDataSet.Tables[0].Rows.Count; j++)
                                    {

                                        dRAmount = Conversion.ConvertStrToDouble(
                                            objDataSet.Tables[0].Rows[j]["RES_AMOUNT"].ToString());
                                        dRPaid = Conversion.ConvertStrToDouble(
                                            objDataSet.Tables[0].Rows[j]["PAID_TOTAL"].ToString());
                                        dRCollect = Conversion.ConvertStrToDouble(
                                            objDataSet.Tables[0].Rows[j]["COLL_TOTAL"].ToString());

                                        dRBalance = CalculateReserveBalance(dRAmount, dRPaid, dRCollect, bCollInRsvBal, 0);
                                        dRIncurred = CalculateIncurred(dRBalance, dRPaid, dRCollect, bCollInRsvBal, bCollInIncurredBal);

                                        if (bAll)
                                        {
                                            dBalTotal += dRBalance;
                                            dPaidTotal += dRPaid;
                                            dCollTotal += dRCollect;
                                            dIncTotal += dRIncurred;
                                            dResTotal += dRAmount;
                                        }
                                    }
                                }
                                else
                                {
                                    dRAmount = 0;
                                    dRPaid = 0;
                                    dRCollect = 0;
                                    dRBalance = 0;
                                    dRIncurred = 0;
                                }

                                if (!bAll && bBal)
                                {
                                    sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                    sValues += "," + dRBalance;
                                }
                                else if (!bAll && bIncur)
                                {
                                    sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                    sValues += "," + dRIncurred;
                                }
                            }
                            if (bAll)
                            {
                                bAllDone = true;
                                switch (sResult)
                                {
                                    case "":
                                        if (bBal)
                                        {
                                            sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                            sValues += "," + dBalTotal;
                                        }
                                        else
                                        {
                                            sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                            sValues += "," + dIncTotal;
                                        }
                                        break;
                                    case "PAID_TOTAL":
                                        sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                        sValues += "," + dPaidTotal;
                                        break;
                                    case "COLL_TOTAL":
                                        sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                        sValues += "," + dCollTotal;
                                        break;
                                    case "RES_AMOUNT":
                                        sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                        sValues += "," + dResTotal;
                                        break;
                                }
                            }
                        }
                        else if (bAll && bAllDone)
                        {
                            switch (sResult)
                            {
                                case "":
                                    if (bBal)
                                    {
                                        sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                        sValues += "," + dBalTotal;
                                    }
                                    else
                                    {
                                        sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                        sValues += "," + dIncTotal;
                                    }
                                    break;
                                case "PAID_TOTAL":
                                    sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                    sValues += "," + dPaidTotal;
                                    break;
                                case "COLL_TOTAL":
                                    sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                    sValues += "," + dCollTotal;
                                    break;
                                case "RES_AMOUNT":
                                    sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                    sValues += "," + dResTotal;
                                    break;
                            }
                        }
                        else
                        {
                            objReader = DbFactory.GetDbReader(m_sConnectionString, m_objGeneric.SafeSQL(sSQL));

                            if (objReader.Read())
                            {
                                sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                sValues += "," + Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(sResult)));
                            }
                            else
                            {
                                sCols = sCols + "," + sTmp.Substring(iPos + 1);
                                sValues += ",0";
                            }
                        }
                        objReader.Close();
                        //Shruti, 12185 ends
                    }
                    sFinalSQL = sSQLStart + sCols + sSQLMid + sValues + ")";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sFinalSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }

                //Start averma62

                if (Utilities.ExistsInArray(p_objDisplayTables, "CATASTROPHE"))
                {
                    iIdx = Utilities.IndexInArray(p_objDisplayTables, "CATASTROPHE");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CATASTROPHE_TEMP]", "CATASTROPHE_ROW_ID");
                    sSQL = "SELECT CLAIM.CATASTROPHE_ROW_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO CATASTROPHE_TEMP]"
                        + " FROM CLAIM, CATASTROPHE"
                       + " WHERE CLAIM.CLAIM_ID = " + p_lRecordID.ToString() + " AND CLAIM.CATASTROPHE_ROW_ID = CATASTROPHE.CATASTROPHE_ROW_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CATASTROPHE_TEMP]") == 0)
                    {
                        sFieldNames = "";
                        sFieldValues = "";
                        m_objGeneric.FindFields("[CATASTROPHE_TEMP]", ref sFieldNames, ref sFieldValues);
                        sSQL = "INSERT INTO [CATASTROPHE_TEMP](CATASTROPHE_ROW_ID" + sFieldNames + ")"
                              + " SELECT CATASTROPHE_ROW_ID" + sFieldValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //End averma62
                //payments by trans type 
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_PAY_TOT"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_PAY_TOT");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLMPAY_TEMP]", "CLAIM_ID");
                    sFundsSel = "-1";
                    ArrayList objTemp = new ArrayList();
                    sSQL = "";
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf(".");
                    iPosNext = p_objDisplayFields[iIdx].ToString().IndexOf("|");
                    while (iPos != -1 && iPosNext != -1)
                    {
                        sTmp = p_objDisplayFields[iIdx].ToString().Substring(iPos + 1, iPosNext - iPos - 1).Trim();
                        sTmp = sTmp.Substring(0, sTmp.Length - 2);
                        iPos = p_objDisplayFields[iIdx].ToString().IndexOf(",", iPosNext);
                        if (iPos != -1)
                        {
                            sTmpNext = p_objDisplayFields[iIdx].ToString().Substring(iPosNext + 1, iPos - iPosNext - 1).Trim();
                        }
                        else
                        {
                            sTmpNext = p_objDisplayFields[iIdx].ToString().Substring(iPosNext + 1);
                        }
                        objTemp.Add(sTmp + "|" + sTmpNext);
                        if (sSQL.Length > 0)
                        {
                            sSQL = sSQL + ",";
                        }
                        if (m_lDbMake == (int)eDatabaseType.DBMS_IS_ACCESS)
                        {
                            sSQL = sSQL + sTmpNext + " DOUBLE";
                        }
                        else if (m_lDbMake == (int)eDatabaseType.DBMS_IS_SYBASE || m_lDbMake == (int)eDatabaseType.DBMS_IS_SQLSRVR)
                        {
                            sSQL = sSQL + sTmpNext + " float";
                        }
                        else if (m_lDbMake == (int)eDatabaseType.DBMS_IS_INFORMIX || m_lDbMake == (int)eDatabaseType.DBMS_IS_DB2)
                        {
                            sSQL = sSQL + sTmpNext + " FLOAT";
                        }
                        else if (m_lDbMake == (int)eDatabaseType.DBMS_IS_ORACLE)
                        {
                            sSQL = sSQL + sTmpNext + " NUMBER";
                        }
                        iPos = p_objDisplayFields[iIdx].ToString().IndexOf(".", iPosNext + 1);
                        iPosNext = p_objDisplayFields[iIdx].ToString().IndexOf("|", iPosNext + 1);
                    }
                    if (m_lDbMake == (int)eDatabaseType.DBMS_IS_ACCESS)
                    {
                        sSQL = "CREATE TABLE [CLMPAY_TEMP](CLAIM_ID LONG," + sSQL + ")";
                    }
                    else if (m_lDbMake == (int)eDatabaseType.DBMS_IS_SYBASE || m_lDbMake == (int)eDatabaseType.DBMS_IS_SQLSRVR)
                    {
                        sSQL = "CREATE TABLE [CLMPAY_TEMP](CLAIM_ID int," + sSQL + ")";
                    }
                    else if (m_lDbMake == (int)eDatabaseType.DBMS_IS_INFORMIX || m_lDbMake == (int)eDatabaseType.DBMS_IS_DB2)
                    {
                        sSQL = "CREATE TABLE [CLMPAY_TEMP](CLAIM_ID INTEGER," + sSQL + ")";
                    }
                    else if (m_lDbMake == (int)eDatabaseType.DBMS_IS_ORACLE)
                    {
                        sSQL = "CREATE TABLE [CLMPAY_TEMP](CLAIM_ID NUMBER(10)," + sSQL + ")";
                    }

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    sSQLStart = "INSERT INTO [CLMPAY_TEMP](CLAIM_ID";
                    sSQLMid = ") values(" + p_lRecordID.ToString();
                    sCols = "";
                    sValues = "";
                    sTemp = "";
                    for (int i = 0; i < objTemp.Count; i++)
                    {
                        sTmp = objTemp[i].ToString();
                        iPos = sTmp.IndexOf("|");
                        if (sTmp.Substring(0, 4) == "PAID")
                        {
                            sSQL = "SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) FROM FUNDS,"
                                + " FUNDS_TRANS_SPLIT WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
                                + " AND FUNDS.CLAIM_ID = " + p_lRecordID.ToString()
                                + " AND FUNDS.PAYMENT_FLAG <> 0 AND FUNDS.VOID_FLAG = 0";
                            sTemp = "";
                            sTemp = sTmp.Substring(0, iPos);
                            sTemp = sTemp.Substring(4);
                            if (sTemp != "<ALL>")
                            {
                                sSQL = sSQL + " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = ";
                                sSQL = sSQL + sTemp;
                            }
                        }
                        else if (sTmp.Substring(0, 9) == "COLLECTED")
                        {
                            sSQL = "SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) FROM FUNDS,"
                                + " FUNDS_TRANS_SPLIT WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
                                + " AND FUNDS.CLAIM_ID = " + p_lRecordID.ToString()
                                + " AND FUNDS.PAYMENT_FLAG <> 0 AND FUNDS.VOID_FLAG = 0";
                            sTemp = "";
                            sTemp = sTmp.Substring(0, iPos);
                            sTemp = sTemp.Substring(9);
                            if (sTemp != "<ALL>")
                            {
                                sSQL = sSQL + " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = ";
                                sSQL = sSQL + sTemp;
                            }
                        }
                        objReader = DbFactory.GetDbReader(m_sConnectionString, m_objGeneric.SafeSQL(sSQL));
                        if (objReader.Read())
                        {
                            sCols = sCols + "," + sTmp.Substring(iPos + 1);
                            sValues += "," + Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }
                        else
                        {
                            sCols = sCols + sTmp.Substring(iPos + 1);
                            sValues += ",0";
                        }
                        objReader.Close();
                    }
                    sFinalSQL = sSQLStart + sCols + sSQLMid + sValues + ")";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sFinalSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                //PAYEE_ENTITY
                if (Utilities.ExistsInArray(p_objDisplayTables, "PAYEE_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "PAYEE_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[FUNDS_TEMP]", "[PAYEE_TEMP]", "TRANS_ID");
                    sSQL = "SELECT FUNDS.TRANS_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO PAYEE_TEMP]"
                        + " FROM FUNDS,ENTITY PAYEE_ENTITY WHERE FUNDS.CLAIM_ID = " + p_lRecordID.ToString();
                    if (sFundsSel != "-1")
                    {
                        sSQL = sSQL + " AND " + sFundsSel;
                    }
                    sSQL = sSQL + " AND FUNDS.PAYEE_EID = PAYEE_ENTITY.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    if (m_objGeneric.GetMergeCount("[PAYEE_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PAYEE_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [PAYEE_TEMP](TRANS_ID" + sBitFields + ")"
                            + " SELECT TRANS_ID " + sBitValues + " FROM [FUNDS_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //claim supplemental
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIM_SUPP_TEMP]", "CLAIM_ID");
                    sSQL = "SELECT CLAIM_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO CLAIM_SUPP_TEMP]"
                        + " FROM CLAIM_SUPP"
                        + " WHERE CLAIM_SUPP.CLAIM_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    if (m_objGeneric.GetMergeCount("[CLAIM_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CLAIM_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [CLAIM_SUPP_TEMP](CLAIM_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID " + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }


                //claim leave supplemental for mits 30698
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_LV_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CLAIM_LV_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIM_LV_SUPP_TEMP]", "CLAIM_ID");
                    sSQL = "SELECT CLAIM_LEAVE.CLAIM_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO CLAIM_LV_SUPP_TEMP]"
                        + " FROM CLAIM_LV_SUPP  INNER JOIN CLAIM_LEAVE  ON CLAIM_LV_SUPP.LEAVE_ROW_ID = CLAIM_LEAVE.LEAVE_ROW_ID"
                        + " WHERE CLAIM_LEAVE.CLAIM_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    if (m_objGeneric.GetMergeCount("[CLAIM_LV_SUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CLAIM_LV_SUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [CLAIM_LV_SUPP_TEMP](CLAIM_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID " + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //POLICY
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "INSURER_ENTITY")
                    // npadhy Start MITS 18935 Added the case for Reinsurer and modified the logic for Insurer
                    || Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_INSURER")
                    || Utilities.ExistsInArray(p_objDisplayTables, "REINSURER_ENTITY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_INS_REINS")
                    // npadhy End MITS 18935 Added the case for Reinsurer and modified the logic for Insurer
                    || Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_CVG_TYPE")
                    || Utilities.ExistsInArray(p_objDisplayTables, "INSURED")
                    || Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_MCO")
                    || Utilities.ExistsInArray(p_objDisplayTables, "MCO_ENTITY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "POLICY_SUPP")
                    )
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY");
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    else
                    {
                        sTmp = "";
                    }
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POLICY_TEMP]", "CLAIM_ID");
                    if (objSettings.MultiCovgPerClm != -1)
                    {
                        sSQL = "SELECT CLAIM_ID,POLICY_ID" + sTmp + " [INTO POLICY_TEMP]"
                            + " FROM CLAIM,POLICY"
                            + " WHERE CLAIM.CLAIM_ID = " + p_lRecordID.ToString()
                            + " AND CLAIM.PRIMARY_POLICY_ID = POLICY.POLICY_ID";
                    }
                    else
                    {
                        sSQL = "SELECT CLAIM.CLAIM_ID,POLICY.POLICY_ID" + sTmp + " [INTO POLICY_TEMP]"
                            + " FROM CLAIM,POLICY,CLAIM_X_POLICY"
                            + " WHERE CLAIM_X_POLICY.CLAIM_ID = " + p_lRecordID.ToString()
                            + " AND CLAIM_X_POLICY.CLAIM_ID = CLAIM.CLAIM_ID"
                            + " AND CLAIM_X_POLICY.POLICY_ID = POLICY.POLICY_ID";
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    if (m_objGeneric.GetMergeCount("[POLICY_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[POLICY_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        // akaushik5 Changed for MITS 32465 Starts
                        //sSQL = "INSERT INTO [POLICY_TEMP](CLAIM_ID" + sBitFields + ")"
                        //    + " SELECT CLAIM_ID " + sBitValues + " FROM [CLAIM_TEMP]";
                        sSQL = "INSERT INTO [POLICY_TEMP](CLAIM_ID,POLICY_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0 " + sBitValues + " FROM [CLAIM_TEMP]";
                        // akaushik5 Changed for MITS 32465 Ends
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                // npadhy Start MITS 18935 Added the case for Reinsurer and modified the logic for Insurer
                //INSURER
                if (Utilities.ExistsInArray(p_objDisplayTables, "INSURER_ENTITY") || Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_INSURER"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "INSURER_ENTITY");
                    iIdxInsurer = Utilities.IndexInArray(p_objDisplayTables, "POLICY_X_INSURER");

                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[INS_ENT_TEMP]", "POLICY_ID");

                    sSQL = "SELECT POLICY.POLICY_ID";

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_objDisplayFields[iIdx].ToString());
                    if (iIdxInsurer > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_objDisplayFields[iIdxInsurer].ToString());

                    sSQL = sSQL + " [INTO INS_ENT_TEMP]"
                    + " FROM POLICY,ENTITY INSURER_ENTITY, POLICY_X_INSURER"
                    + " WHERE POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID"
                    + " AND POLICY_X_INSURER.INSURER_CODE = INSURER_ENTITY.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    if (m_objGeneric.GetMergeCount("[INS_ENT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[INS_ENT_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [INS_ENT_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID " + sBitValues + " FROM [POLICY_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                // npadhy MITS Handling the case for Reinsurer
                if (Utilities.ExistsInArray(p_objDisplayTables, "REINSURER_ENTITY") || Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_INS_REINS"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "REINSURER_ENTITY");
                    iIdxReinsurer = Utilities.IndexInArray(p_objDisplayTables, "POLICY_X_INS_REINS");

                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[REINS_ENT_TEMP]", "POLICY_ID");

                    sSQL = "SELECT POLICY.POLICY_ID";

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_objDisplayFields[iIdx].ToString());
                    if (iIdxReinsurer > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_objDisplayFields[iIdxReinsurer].ToString());

                    sSQL = sSQL + " [INTO REINS_ENT_TEMP]"
                    + " FROM POLICY,ENTITY REINSURER_ENTITY, POLICY_X_INS_REINS, POLICY_X_INSURER"
                    + " WHERE POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID"
                    + " AND POLICY_X_INSURER.IN_ROW_ID = POLICY_X_INS_REINS.POL_X_INS_ROW_ID"
                    + " AND POLICY_X_INS_REINS.REINSURER_EID = REINSURER_ENTITY.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[REINS_ENT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[REINS_ENT_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [REINS_ENT_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID " + sBitValues + " FROM [POLICY_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                // npadhy End MITS 18935 Added the case for Reinsurer and modified the logic for Insurer
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_CVG_TYPE"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_CVG_TYPE");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[CVG_TEMP]", "POLICY_ID");
                    sTmp = "-1";
                    sSQL = "SELECT [POLICY_TEMP].POLICY_ID," + p_objDisplayFields[iIdx].ToString() +
                        " [INTO CVG_TEMP]"
                        + " FROM [POLICY_TEMP],POLICY_X_CVG_TYPE"
                        + " WHERE POLICY_X_CVG_TYPE.POLICY_ID = [POLICY_TEMP].POLICY_ID";
                    if (sTmp != "-1")
                    {
                        sSQL = sSQL + " AND " + sTmp;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    if (m_objGeneric.GetMergeCount("[CVG_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CVG_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        //dbisht6 jira RMA-8466
                        if (sBitFields.Contains("POLICY_ID"))
                        {
                            string sBitValuesSegment = null;
                            sBitValuesSegment = sBitValues.ToString().Remove(0, 1);
                            sSQL = "INSERT INTO [CVG_TEMP](" + sBitFields.Remove(0, 1) + ")"
                                + " SELECT POLICY_ID " + sBitValuesSegment.Remove(0, sBitValuesSegment.ToString().IndexOf(",")) + " FROM [POLICY_TEMP]";
                        }
                        //dbisht6 end
                        else
                        {

                            sSQL = "INSERT INTO [CVG_TEMP](POLICY_ID" + sBitFields + ")"
                                + " SELECT POLICY_ID " + sBitValues + " FROM [POLICY_TEMP]";
                        }
                        //sSQL = "INSERT INTO [CVG_TEMP](POLICY_ID" + sBitFields + ")"
                        //    + " SELECT POLICY_ID " + sBitValues + " FROM [POLICY_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //INSURED
                if (Utilities.ExistsInArray(p_objDisplayTables, "INSURED"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "INSURED");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[INSURED_TEMP]", "POLICY_ID");
                    sTmp = "-1";
                    sSQL = "SELECT [POLICY_TEMP].POLICY_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO INSURED_TEMP]"
                        + " FROM [POLICY_TEMP],POLICY_X_INSURED,ENTITY INSURED WHERE [POLICY_TEMP].POLICY_ID = POLICY_X_INSURED.POLICY_ID"
                        + " AND POLICY_X_INSURED.INSURED_EID = INSURED.ENTITY_ID";
                    if (sTmp != "-1")
                    {
                        sSQL = sSQL + " AND " + sTmp;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    if (m_objGeneric.GetMergeCount("[INSURED_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[INSURED_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [INSURED_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID " + sBitValues + " FROM [POLICY_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //MCO support
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_MCO")
                    || Utilities.ExistsInArray(p_objDisplayTables, "MCO_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_MCO");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "MCO_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[MCO_TEMP]", "POLICY_ID");
                    sTmp = "-1";
                    sSQL = "SELECT [POLICY_TEMP].POLICY_ID";
                    if (iIdx != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    sSQL = sSQL + " [INTO MCO_TEMP]"
                        + " FROM [POLICY_TEMP],POLICY_X_MCO,ENTITY MCO_ENTITY "
                        + " WHERE [POLICY_TEMP].POLICY_ID = POLICY_X_MCO.POLICY_ID"
                        + " AND POLICY_X_MCO.MCO_EID = MCO_ENTITY.ENTITY_ID";
                    if (sTmp != "-1")
                    {
                        sSQL = sSQL + " AND " + sTmp;
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    if (m_objGeneric.GetMergeCount("[MCO_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[MCO_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [MCO_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID " + sBitValues + " FROM [POLICY_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //POLICY_SUPP
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_SUPP"))
                {
                    long p_lPolicyId = 0;
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[POLSUPP_TEMP]", "POLICY_ID");
                    sSQL = "SELECT [POLICY_TEMP].POLICY_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO POLSUPP_TEMP]"
                        + " FROM [POLICY_TEMP],POLICY_SUPP"
                        + " WHERE POLICY_SUPP.POLICY_ID = [POLICY_TEMP].POLICY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    if (m_objGeneric.GetMergeCount("[POLSUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[POLSUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [POLSUPP_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID " + sBitValues + " FROM [POLICY_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //gagnihotri 10/07/2009 MITS 17813
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_ENH") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "INSURER_ENTITY_ENH") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_INSRD_ENH") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_CVG_ENH") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_EXP_ENH") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_RTNG_ENH") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_MCO_ENH") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "MCO_ENTITY_ENH") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_DCNT_ENH") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_DTIER_ENH") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_TRANS_ENH") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_TERM_ENH") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "POLICY_ENH_SUPP"))
                {
                    string sPolicyStatus = "";
                    int iPolicyInd = 0;
                    string sPolicyStatusDesc = "";
                    long p_lPolicyId = 0;
                    sSQL = "SELECT CLAIM_ID,POLICY_ID "
                        + " FROM CLAIM,POLICY_ENH"
                        + " WHERE CLAIM.CLAIM_ID = " + p_lRecordID.ToString()
                        + " AND CLAIM.PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null && objReader.Read())
                    {
                        p_lPolicyId = Conversion.ConvertObjToInt64(objReader.GetValue("POLICY_ID"), m_iClientId);
                        objReader.Dispose();
                    }
                    if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_ENH"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_ENH");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POLICY_ENH_TEMP]", "CLAIM_ID");
                        sSQL = "SELECT POLICY_INDICATOR FROM POLICY_ENH WHERE POLICY_ID = " + p_lPolicyId.ToString();
                        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        if (objReader != null && objReader.Read())
                        {
                            iPolicyInd = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_INDICATOR"), m_iClientId);
                            using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
                            {
                                objCache.GetCodeInfo(iPolicyInd, ref sPolicyStatus, ref sPolicyStatusDesc);
                            }
                            if (sPolicyStatus.ToUpper() == "Q")
                            {
                                sSQL = "SELECT POLICY_ID,CLAIM_ID," + p_objDisplayFields[iIdx].ToString() +
                           " [INTO POLICY_ENH_TEMP] FROM POLICY_ENH,CLAIM WHERE POLICY_ID = " + p_lPolicyId.ToString()
                                + " AND CLAIM.CLAIM_ID = " + p_lRecordID.ToString()
                                + " AND CLAIM.PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID";
                            }
                            else
                            {
                                p_objDisplayFields[iIdx] = p_objDisplayFields[iIdx].ToString().Replace("POLICY_ENH.POLICY_STATUS_CODE", "POLICY_X_TRANS_ENH.POLICY_STATUS");
                                sSQL = "SELECT POLICY_ENH.POLICY_ID,CLAIM_ID," + p_objDisplayFields[iIdx] + " [INTO POLICY_ENH_TEMP] FROM POLICY_ENH, CLAIM,POLICY_X_TRANS_ENH WHERE POLICY_ENH.POLICY_ID = " + p_lPolicyId;
                                sSQL = sSQL + " AND POLICY_X_TRANS_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_TRANS_ENH A WHERE A.POLICY_ID = " + p_lPolicyId + ")";
                                sSQL = sSQL + " AND  POLICY_ENH.POLICY_ID = POLICY_X_TRANS_ENH.POLICY_ID"
                                + " AND CLAIM.CLAIM_ID = " + p_lRecordID.ToString()
                                + " AND CLAIM.PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID";
                            }
                        }
                        else //pmittal5 Mits 18595 - In case no policy is attached with the claim
                        {
                            sSQL = "SELECT CLAIM_ID, POLICY_ID," + p_objDisplayFields[iIdx].ToString() +
                                   " [INTO POLICY_ENH_TEMP] FROM POLICY_ENH,CLAIM WHERE POLICY_ID = " + p_lPolicyId.ToString()
                                   + " AND CLAIM.CLAIM_ID = " + p_lRecordID.ToString()
                                   + " AND CLAIM.PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID";
                        }
                        //End - pmittal5
                    }
                    else
                    {
                        sSQL = "SELECT POLICY_ID [INTO POLICY_ENH_TEMP] FROM POLICY_ENH WHERE POLICY_ID = "
                            + p_lPolicyId.ToString();
                        //gagnihotri 10/07/2009 MITS 17813
                        sFrom = sFrom + ", [POLICY_ENH_TEMP]";
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    //pmittal5 Mits 18595 12/04/09 - In case no policy is attached with the claim
                    if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_ENH"))
                    {
                        if (m_objGeneric.GetMergeCount("[POLICY_ENH_TEMP]") == 0)
                        {
                            sFieldNames = "";
                            sFieldValues = "";
                            m_objGeneric.FindFields("[POLICY_ENH_TEMP]", ref sFieldNames, ref sFieldValues);
                            sTmp = "";
                            sSQL = "INSERT INTO [POLICY_ENH_TEMP](CLAIM_ID " + sFieldNames + ")"
                                + " SELECT CLAIM_ID " + sFieldValues + " FROM [CLAIM_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //End - pmittal5

                    //TRANSACTIONS Table for policy status
                    if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_TRANS_ENH"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_TRANS_ENH");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[TRANS_ENH_TEMP]", "POLICY_ID");
                        sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO TRANS_ENH_TEMP]"
                        + " FROM POLICY_X_TRANS_ENH"
                        + " WHERE POLICY_X_TRANS_ENH.POLICY_ID = " + p_lPolicyId.ToString()
                        + " AND POLICY_X_TRANS_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_TRANS_ENH A WHERE A.POLICY_ID = " + p_lPolicyId + ")";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[TRANS_ENH_TEMP]") == 0)
                        {
                            //pmittal5 Mits 18595 12/02/09 - Some fields in the table have their Nullable value set to "No" in database. 
                            //So, all the fields in the table are added in Insert Query 
                            //m_objGeneric.FindBitFields("[TRANS_ENH_TEMP]", ref sBitFields, ref sBitValues); 
                            //sSQL = "INSERT INTO [TRANS_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            //    + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                            sFieldNames = "";
                            sFieldValues = "";
                            m_objGeneric.FindFields("[TRANS_ENH_TEMP]", ref sFieldNames, ref sFieldValues);
                            sSQL = "INSERT INTO [TRANS_ENH_TEMP](POLICY_ID" + sFieldNames + ")"
                                  + " SELECT POLICY_ID" + sFieldValues + " FROM [POLICY_ENH_TEMP]";
                            //End - pmittal5
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //Policy Terms
                    if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_TERM_ENH"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_TERM_ENH");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[TERM_ENH_TEMP]", "POLICY_ID");
                        sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO TERM_ENH_TEMP]"
                        + " FROM POLICY_X_TERM_ENH"
                        + " WHERE POLICY_X_TERM_ENH.POLICY_ID = " + p_lPolicyId.ToString()
                        + " AND POLICY_X_TERM_ENH.TERM_ID = (SELECT MAX(TERM_ID) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_lPolicyId + ")";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[TERM_ENH_TEMP]") == 0)
                        {
                            //pmittal5 Mits 18595 12/02/09 - Some fields in the table have their Nullable value set to "No" in database.
                            //m_objGeneric.FindBitFields("[TERM_ENH_TEMP]", ref sBitFields, ref sBitValues);
                            //sSQL = "INSERT INTO [TERM_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            //    + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                            sFieldNames = "";
                            sFieldValues = "";
                            m_objGeneric.FindFields("[TERM_ENH_TEMP]", ref sFieldNames, ref sFieldValues);
                            sSQL = "INSERT INTO [TERM_ENH_TEMP](POLICY_ID" + sFieldNames + ")"
                                  + " SELECT POLICY_ID" + sFieldValues + " FROM [POLICY_ENH_TEMP]";
                            //End - pmittal5
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //INSURER
                    if (Utilities.ExistsInArray(p_objDisplayTables, "INSURER_ENTITY_ENH"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "INSURER_ENTITY_ENH");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[INS_ENT_ENH_TEMP]", "POLICY_ID");
                        sSQL = "SELECT POLICY_ENH.POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO INS_ENT_ENH_TEMP]"
                        + " FROM POLICY_ENH,ENTITY INSURER_ENTITY_ENH"
                        + " WHERE POLICY_ENH.POLICY_ID = " + p_lPolicyId.ToString()
                        + " AND POLICY_ENH.INSURER_EID = INSURER_ENTITY_ENH.ENTITY_ID";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        //pmittal5 Mits 18595 12/02/09 
                        if (m_objGeneric.GetMergeCount("[INS_ENT_ENH_TEMP]") == 0)
                        {
                            sFieldNames = "";
                            sFieldValues = "";
                            m_objGeneric.FindFields("[INS_ENT_ENH_TEMP]", ref sFieldNames, ref sFieldValues);
                            sSQL = "INSERT INTO [INS_ENT_ENH_TEMP](POLICY_ID" + sFieldNames + ")"
                                  + " SELECT POLICY_ID" + sFieldValues + " FROM [POLICY_ENH_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                        //End - pmittal5
                    }
                    //COVERAGE TYPE
                    if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_CVG_ENH"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_CVG_ENH");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[CVG_ENH_TEMP]", "POLICY_ID");
                        sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO CVG_ENH_TEMP]"
                        + " FROM POLICY_X_CVG_ENH"
                        + " WHERE POLICY_X_CVG_ENH.POLICY_ID = " + p_lPolicyId.ToString()
                        + " AND POLICY_X_CVG_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_CVG_ENH A WHERE A.POLICY_ID = " + p_lPolicyId.ToString() + ")";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[CVG_ENH_TEMP]") == 0)
                        {
                            //pmittal5 Mits 18595 12/02/09
                            //m_objGeneric.FindBitFields("[CVG_ENH_TEMP]", ref sBitFields, ref sBitValues);
                            //sSQL = "INSERT INTO [CVG_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            //    + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                            sFieldNames = "";
                            sFieldValues = "";
                            m_objGeneric.FindFields("[CVG_ENH_TEMP]", ref sFieldNames, ref sFieldValues);
                            sSQL = "INSERT INTO [CVG_ENH_TEMP](POLICY_ID" + sFieldNames + ")"
                            + " SELECT POLICY_ID" + sFieldValues + " FROM [POLICY_ENH_TEMP]";
                            //End - pmittal5
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //Rating
                    if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_RTNG_ENH"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_RTNG_ENH");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[RTNG_ENH_TEMP]", "POLICY_ID");
                        sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO RTNG_ENH_TEMP]"
                        + " FROM POLICY_X_RTNG_ENH"
                        + " WHERE POLICY_X_RTNG_ENH.POLICY_ID = " + p_lPolicyId.ToString()
                        + " AND POLICY_X_RTNG_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_RTNG_ENH A WHERE A.POLICY_ID = " + p_lPolicyId.ToString() + ")";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[RTNG_ENH_TEMP]") == 0)
                        {
                            //pmittal5 Mits 18595 12/02/09
                            //m_objGeneric.FindBitFields("[RTNG_ENH_TEMP]", ref sBitFields, ref sBitValues);
                            //sSQL = "INSERT INTO [RTNG_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            //    + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                            sFieldNames = "";
                            sFieldValues = "";
                            m_objGeneric.FindFields("[RTNG_ENH_TEMP]", ref sFieldNames, ref sFieldValues);
                            sSQL = "INSERT INTO [RTNG_ENH_TEMP](POLICY_ID" + sFieldNames + ")"
                            + " SELECT POLICY_ID" + sFieldValues + " FROM [POLICY_ENH_TEMP]";
                            //End - pmittal5
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //Discount
                    if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_DCNT_ENH"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_DCNT_ENH");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[DCNT_ENH_TEMP]", "POLICY_ID");
                        sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO DCNT_ENH_TEMP]"
                        + " FROM POLICY_X_DCNT_ENH"
                        + " WHERE POLICY_X_DCNT_ENH.POLICY_ID = " + p_lPolicyId.ToString()
                        + " AND POLICY_X_DCNT_ENH.TRANSACTION_ID =    (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_DCNT_ENH A WHERE A.POLICY_ID = " + p_lPolicyId + ")";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[DCNT_ENH_TEMP]") == 0)
                        {
                            //pmittal5 Mits 18595 12/02/09
                            //m_objGeneric.FindBitFields("[DCNT_ENH_TEMP]", ref sBitFields, ref sBitValues);
                            //sSQL = "INSERT INTO [DCNT_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            //    + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                            sFieldNames = "";
                            sFieldValues = "";
                            m_objGeneric.FindFields("[DCNT_ENH_TEMP]", ref sFieldNames, ref sFieldValues);
                            sSQL = "INSERT INTO [DCNT_ENH_TEMP](POLICY_ID" + sFieldNames + ")"
                            + " SELECT POLICY_ID" + sFieldValues + " FROM [POLICY_ENH_TEMP]";
                            //End -pmittal5
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //Discount Tier
                    if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_DTIER_ENH"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_DTIER_ENH");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[DTIER_ENH_TEMP]", "POLICY_ID");
                        sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO DTIER_ENH_TEMP]"
                        + " FROM POLICY_X_DTIER_ENH"
                        + " WHERE POLICY_X_DTIER_ENH.POLICY_ID = " + p_lPolicyId.ToString()
                        + " AND POLICY_X_DTIER_ENH.TRANSACTION_ID =    (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_DTIER_ENH A WHERE A.POLICY_ID = " + p_lPolicyId.ToString() + ")";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[DTIER_ENH_TEMP]") == 0)
                        {
                            //pmittal5 Mits 18595 12/02/09
                            //m_objGeneric.FindBitFields("[DTIER_ENH_TEMP]", ref sBitFields, ref sBitValues);
                            //sSQL = "INSERT INTO [DTIER_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            //    + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                            sFieldNames = "";
                            sFieldValues = "";
                            m_objGeneric.FindFields("[DTIER_ENH_TEMP]", ref sFieldNames, ref sFieldValues);
                            sSQL = "INSERT INTO [DTIER_ENH_TEMP](POLICY_ID" + sFieldNames + ")"
                            + " SELECT POLICY_ID" + sFieldValues + " FROM [POLICY_ENH_TEMP]";
                            //End -pmittal5
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //Exposures
                    if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_EXP_ENH"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_EXP_ENH");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[EXP_ENH_TEMP]", "POLICY_ID");
                        sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO EXP_ENH_TEMP]"
                        + " FROM POLICY_X_EXP_ENH"
                        + " WHERE POLICY_X_EXP_ENH.POLICY_ID = " + p_lPolicyId.ToString()
                        + " AND POLICY_X_EXP_ENH.TRANSACTION_ID =    (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_EXP_ENH A WHERE A.POLICY_ID = " + p_lPolicyId.ToString() + ")";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[EXP_ENH_TEMP]") == 0)
                        {
                            //pmittal5 Mits 18595 12/02/09
                            //m_objGeneric.FindBitFields("[EXP_ENH_TEMP]", ref sBitFields, ref sBitValues);
                            //sSQL = "INSERT INTO [EXP_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            //    + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                            sFieldNames = "";
                            sFieldValues = "";
                            m_objGeneric.FindFields("[EXP_ENH_TEMP]", ref sFieldNames, ref sFieldValues);
                            sSQL = "INSERT INTO [EXP_ENH_TEMP](POLICY_ID" + sFieldNames + ")"
                            + " SELECT POLICY_ID" + sFieldValues + " FROM [POLICY_ENH_TEMP]";
                            //End -pmittal5
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //INSURED
                    if (Utilities.ExistsInArray(p_objDisplayTables, "INSURED_ENTITY_ENH"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "INSURED_ENTITY_ENH");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[INSURED_ENH_TEMP]", "POLICY_ID");
                        sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO INSURED_ENH_TEMP]"
                        + " FROM POLICY_X_INSRD_ENH,ENTITY INSURED_ENTITY_ENH WHERE POLICY_ID = " + p_lPolicyId.ToString()
                        + " AND POLICY_X_INSRD_ENH.INSURED_EID = INSURED_ENTITY_ENH.ENTITY_ID";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[INSURED_ENH_TEMP]") == 0)
                        {
                            m_objGeneric.FindBitFields("[INSURED_ENH_TEMP]", ref sBitFields, ref sBitValues);
                            sSQL = "INSERT INTO [INSURED_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                                + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //POLICY MCO
                    if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_MCO_ENH")
                        || Utilities.ExistsInArray(p_objDisplayTables, "MCO_ENTITY_ENH"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_MCO_ENH");
                        iIdxNext = Generic.IndexOf(p_objDisplayTables, "MCO_ENTITY_ENH");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[MCO_ENH_TEMP]", "POLICY_ID");
                        sSQL = "SELECT POLICY_ID";
                        if (iIdx != -1)
                        {
                            sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                        }
                        if (iIdxNext != -1)
                        {
                            sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                        }
                        sSQL = sSQL + " [INTO MCO_ENH_TEMP]"
                        + " FROM POLICY_X_MCO_ENH,ENTITY MCO_ENTITY_ENH WHERE POLICY_ID = " + p_lRecordID.ToString()
                        + " AND POLICY_X_MCO_ENH.MCO_EID = MCO_ENTITY_ENH.ENTITY_ID";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[MCO_ENH_TEMP]") == 0)
                        {
                            m_objGeneric.FindBitFields("[MCO_ENH_TEMP]", ref sBitFields, ref sBitValues);
                            sSQL = "INSERT INTO [MCO_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                                + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //POLICY SUPPLEMENTAL
                    if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_SUPP"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_SUPP");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[POLSUPP_TEMP]", "POLICY_ID");
                        sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO POLSUPP_TEMP]"
                        + " FROM POLICY_SUPP"
                        + " WHERE POLICY_SUPP.POLICY_ID = " + p_lPolicyId.ToString();

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[POLSUPP_TEMP]") == 0)
                        {
                            m_objGeneric.FindBitFields("[POLSUPP_TEMP]", ref sBitFields, ref sBitValues);
                            sSQL = "INSERT INTO [POLSUPP_TEMP](POLICY_ID" + sBitFields + ")"
                                + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //gagnihotri 10/05/2009 MITS 17813
                    //ENHANCED POLICY SUPPLEMENTAL
                    if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_ENH_SUPP"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_ENH_SUPP");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[POLSUPPENH_TEMP]", "POLICY_ID");
                        sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO POLSUPPENH_TEMP]"
                        + " FROM POLICY_ENH_SUPP"
                        + " WHERE POLICY_ENH_SUPP.POLICY_ID = " + p_lPolicyId.ToString();

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[POLSUPPENH_TEMP]") == 0)
                        {
                            m_objGeneric.FindBitFields("[POLSUPPENH_TEMP]", ref sBitFields, ref sBitValues);
                            sSQL = "INSERT INTO [POLSUPPENH_TEMP](POLICY_ID" + sBitFields + ")"
                                + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }

                }
                if (m_objGeneric.IsCaseMgtInstalled() && (Utilities.ExistsInArray(p_objDisplayTables, "CM_CASE_MGT") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_X_CMGR_HIST") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_CMGR_RT_ENTITY") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_CMGR_CM_ENTITY") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_CASE_MGR_NOTES") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_TPLAN_PG_ENTITY") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_TPLAN_AB_ENTITY") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_X_TREATMENT_PLN") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_MEDMGT_CM_ENTITY") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_MEDMGT_PG_ENTITY") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_X_MEDMGTSAVINGS") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_X_ACCOMMODATION") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_VR_PG_ENTITY") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "CM_X_VOCREHAB")))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CM_CASE_MGT");

                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    else
                    {
                        sTmp = "";
                    }
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CM_CASE_MGT_TEMP]", "CLAIM_ID");
                    sSQL = "SELECT CLAIM_ID, CASEMGT_ROW_ID" + sTmp
                        + " [INTO CM_CASE_MGT_TEMP]"
                        + " FROM CASE_MANAGEMENT"
                        + " WHERE CLAIM_ID = " + p_lRecordID;

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    //pmittal5 Mits 18599 11/30/09 
                    if (m_objGeneric.GetMergeCount("[CM_CASE_MGT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CM_CASE_MGT_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [CM_CASE_MGT_TEMP](CLAIM_ID, CASEMGT_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0 " + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                    //End - pmittal5



                    if (Utilities.ExistsInArray(p_objDisplayTables, "CM_X_TREATMENT_PLN") ||
                        Utilities.ExistsInArray(p_objDisplayTables, "CM_TPLAN_AB_ENTITY"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "CM_X_TREATMENT_PLN");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[CM_CASE_MGT_TEMP]", "[TPLN_TEMP]", "CASEMGT_ROW_ID");
                        if (iIdx != -1)
                        {
                            sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                        }

                        sSQL = "SELECT [CM_CASE_MGT_TEMP].CASEMGT_ROW_ID,CMTP_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                            + " [INTO TPLN_TEMP]"
                            + " FROM [CM_CASE_MGT_TEMP],CM_X_TREATMENT_PLN"
                            + " WHERE CM_X_TREATMENT_PLN.CASEMGT_ROW_ID = [CM_CASE_MGT_TEMP].CASEMGT_ROW_ID";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[TPLN_TEMP]") == 0)
                        {
                            m_objGeneric.FindBitFields("[TPLN_TEMP]", ref sBitFields, ref sBitValues);
                            sTmp = "";
                            sSQL = "INSERT INTO [TPLN_TEMP](CASEMGT_ROW_ID,CMTP_ROW_ID" + sBitFields + ")"
                                + " SELECT CASEMGT_ROW_ID,0 " + sBitValues + " FROM [CM_CASE_MGT_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }

                    }
                    if (Utilities.ExistsInArray(p_objDisplayTables, "CM_TPLAN_PG_ENTITY"))
                    {

                        iIdx = Generic.IndexOf(p_objDisplayTables, "CM_TPLAN_PG_ENTITY");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[TPLN_TEMP]", "[TPLN_ENT_TEMP]", "CMTP_ROW_ID");


                        sSQL = "SELECT CM_X_TREATMENT_PLN.CMTP_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                            + " [INTO TPLN_ENT_TEMP]"
                            + " FROM [TPLN_TEMP], CM_X_TREATMENT_PLN,ENTITY CM_TPLAN_PG_ENTITY WHERE CM_X_TREATMENT_PLN.CMTP_ROW_ID = [TPLN_TEMP].CMTP_ROW_ID"
                            + " AND CM_X_TREATMENT_PLN.PROVIDER_EID = CM_TPLAN_PG_ENTITY.ENTITY_ID";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[TPLN_ENT_TEMP]") == 0)
                        {
                            m_objGeneric.FindBitFields("[TPLN_ENT_TEMP]", ref sBitFields, ref sBitValues);
                            sTmp = "";
                            sSQL = "INSERT INTO [TPLN_ENT_TEMP](CMTP_ROW_ID" + sBitFields + ")"
                                + " SELECT CMTP_ROW_ID " + sBitValues + " FROM [TPLN_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    if (Utilities.ExistsInArray(p_objDisplayTables, "CM_TPLAN_RB_ENTITY"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "CM_TPLAN_RB_ENTITY");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[TPLN_TEMP]", "[TPLN_RB_ENT_TEMP]", "CMTP_ROW_ID");


                        sSQL = "SELECT CM_X_TREATMENT_PLN.CMTP_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                            + " [INTO TPLN_RB_ENT_TEMP]"
                            + " FROM [TPLN_TEMP], CM_X_TREATMENT_PLN,ENTITY CM_TPLAN_RB_ENTITY WHERE CM_X_TREATMENT_PLN.CMTP_ROW_ID = [TPLN_TEMP].CMTP_ROW_ID"
                            + " AND CM_X_TREATMENT_PLN.REVIEWER_EID = CM_TPLAN_RB_ENTITY.ENTITY_ID";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[TPLN_RB_ENT_TEMP]") == 0)
                        {
                            m_objGeneric.FindBitFields("[TPLN_RB_ENT_TEMP]", ref sBitFields, ref sBitValues);
                            sTmp = "";
                            sSQL = "INSERT INTO [TPLN_RB_ENT_TEMP](CMTP_ROW_ID" + sBitFields + ")"
                                + " SELECT CMTP_ROW_ID " + sBitValues + " FROM [TPLN_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    if (Utilities.ExistsInArray(p_objDisplayTables, "CM_TPLAN_AB_ENTITY"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "CM_TPLAN_AB_ENTITY");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[TPLN_TEMP]", "[TPLN_AB_ENT_TEMP]", "CMTP_ROW_ID");


                        sSQL = "SELECT CM_X_TREATMENT_PLN.CMTP_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                            + " [INTO TPLN_AB_ENT_TEMP]"
                            + " FROM [TPLN_TEMP], CM_X_TREATMENT_PLN,ENTITY CM_TPLAN_AB_ENTITY WHERE CM_X_TREATMENT_PLN.CMTP_ROW_ID = [TPLN_TEMP].CMTP_ROW_ID"
                            + " AND CM_X_TREATMENT_PLN.APPROVED_BY_EID = CM_TPLAN_AB_ENTITY.ENTITY_ID";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[TPLN_AB_ENT_TEMP]") == 0)
                        {
                            m_objGeneric.FindBitFields("[TPLN_AB_ENT_TEMP]", ref sBitFields, ref sBitValues);
                            sTmp = "";
                            sSQL = "INSERT INTO [TPLN_AB_ENT_TEMP](CMTP_ROW_ID" + sBitFields + ")"
                                + " SELECT CMTP_ROW_ID " + sBitValues + " FROM [TPLN_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //Medical Management Savings
                    if (Utilities.ExistsInArray(p_objDisplayTables, "CM_X_MEDMGTSAVINGS")
                        || Utilities.ExistsInArray(p_objDisplayTables, "CM_MEDMGT_PG_ENTITY")
                        || Utilities.ExistsInArray(p_objDisplayTables, "CM_MEDMGT_CM_ENTITY"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "CM_X_MEDMGTSAVINGS");
                        if (iIdx != -1)
                        {
                            sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                        }
                        else
                            sTmp = string.Empty;
                        Generic.ChainIt(ref sFrom, ref sWhere, "[CM_CASE_MGT_TEMP]", "[MEDMGT_TEMP]", "CASEMGT_ROW_ID");


                        sSQL = "SELECT [CM_CASE_MGT_TEMP].CASEMGT_ROW_ID,CMMS_ROW_ID" + sTmp
                            + "  [INTO MEDMGT_TEMP] FROM [CM_CASE_MGT_TEMP],CM_X_MEDMGTSAVINGS"
                            + " WHERE CM_X_MEDMGTSAVINGS.CASEMGT_ROW_ID = [CM_CASE_MGT_TEMP].CASEMGT_ROW_ID";


                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[MEDMGT_TEMP]") == 0)
                        {
                            m_objGeneric.FindBitFields("[MEDMGT_TEMP]", ref sBitFields, ref sBitValues);
                            sTmp = "";
                            sSQL = "INSERT INTO [MEDMGT_TEMP](CASEMGT_ROW_ID,CMMS_ROW_ID" + sBitFields + ")"
                                + " SELECT CASEMGT_ROW_ID,0 " + sBitValues + " FROM [CM_CASE_MGT_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //Medical Management Savings Case Manager Entity
                    if (Utilities.ExistsInArray(p_objDisplayTables, "CM_MEDMGT_CM_ENTITY"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "CM_MEDMGT_CM_ENTITY");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[MEDMGT_TEMP]", "[MEDMGT_CM_ENT_TEMP]", "CMMS_ROW_ID");


                        sSQL = "SELECT CM_X_MEDMGTSAVINGS.CMMS_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                            + "  [INTO MEDMGT_CM_ENT_TEMP] FROM [MEDMGT_TEMP], CM_X_MEDMGTSAVINGS,ENTITY CM_MEDMGT_CM_ENTITY WHERE CM_X_MEDMGTSAVINGS.CMMS_ROW_ID = [MEDMGT_TEMP].CMMS_ROW_ID "
                            + "  AND CM_X_MEDMGTSAVINGS.CASE_MGR_EID = CM_MEDMGT_CM_ENTITY.ENTITY_ID";


                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[MEDMGT_CM_ENT_TEMP]") == 0)
                        {
                            m_objGeneric.FindBitFields("[MEDMGT_CM_ENT_TEMP]", ref sBitFields, ref sBitValues);
                            sTmp = "";
                            sSQL = "INSERT INTO [MEDMGT_CM_ENT_TEMP](CMMS_ROW_ID" + sBitFields + ")"
                                + " SELECT CMMS_ROW_ID " + sBitValues + " FROM [MEDMGT_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //Medical Management Savings Provider Group Entity
                    if (Utilities.ExistsInArray(p_objDisplayTables, "CM_MEDMGT_PG_ENTITY"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "CM_MEDMGT_PG_ENTITY");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[MEDMGT_TEMP]", "[MEDMGT_PG_ENT_TEMP]", "CMMS_ROW_ID");


                        sSQL = "SELECT CM_X_MEDMGTSAVINGS.CMMS_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                            + "  [INTO MEDMGT_PG_ENT_TEMP] FROM [MEDMGT_TEMP], CM_X_MEDMGTSAVINGS,ENTITY CM_MEDMGT_CM_ENTITY WHERE CM_X_MEDMGTSAVINGS.CMMS_ROW_ID = [MEDMGT_TEMP].CMMS_ROW_ID "
                            + "  AND CM_X_MEDMGTSAVINGS.PROVIDER_EID = CM_MEDMGT_PG_ENTITY.ENTITY_ID";


                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[MEDMGT_PG_ENT_TEMP]") == 0)
                        {
                            m_objGeneric.FindBitFields("[MEDMGT_PG_ENT_TEMP]", ref sBitFields, ref sBitValues);
                            sTmp = "";
                            sSQL = "INSERT INTO [MEDMGT_PG_ENT_TEMP](CMMS_ROW_ID" + sBitFields + ")"
                                + " SELECT CMMS_ROW_ID " + sBitValues + " FROM [MEDMGT_TEMP]";
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                    //ACCOMMODATION
                    if (Utilities.ExistsInArray(p_objDisplayTables, "CM_X_ACCOMMODATION"))
                    {
                        iIdx = Generic.IndexOf(p_objDisplayTables, "CM_X_ACCOMMODATION");
                        Generic.ChainIt(ref sFrom, ref sWhere, "[CM_CASE_MGT_TEMP]", "[ACCOMM_TEMP]", "CASEMGT_ROW_ID");
                        if (iIdx != -1)
                        {
                            sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                        }

                        sSQL = "SELECT [CM_CASE_MGT_TEMP].CASEMGT_ROW_ID,CM_ACCOMM_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                            + " [INTO ACCOMM_TEMP]"
                            + " FROM [CM_CASE_MGT_TEMP],CM_X_ACCOMMODATION"
                            + " WHERE CM_X_ACCOMMODATION.CM_ROW_ID = [CM_CASE_MGT_TEMP].CASEMGT_ROW_ID";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                        if (m_objGeneric.GetMergeCount("[ACCOMM_TEMP]") == 0)
                        {
                            //pmittal5 Mits 18599 12/01/09 - All fields in the table are added in Insert query, since some fields cannot be set to Null
                            //m_objGeneric.FindBitFields("[ACCOMM_TEMP]", ref sBitFields, ref sBitValues);
                            sFieldNames = "";
                            sFieldValues = "";
                            m_objGeneric.FindFields("[ACCOMM_TEMP]", ref sFieldNames, ref sFieldValues);
                            sTmp = "";
                            //sSQL = "INSERT INTO [ACCOMM_TEMP](CASEMGT_ROW_ID,CM_ACCOMM_ROW_ID" + sBitFields + ")"
                            //    + " SELECT CASEMGT_ROW_ID,0 " + sBitValues + " FROM [CM_CASE_MGT_TEMP]";
                            sSQL = "INSERT INTO [ACCOMM_TEMP](CASEMGT_ROW_ID" + sFieldNames + ")"
                                + " SELECT CASEMGT_ROW_ID" + sFieldValues + " FROM [CM_CASE_MGT_TEMP]";
                            //End - pmittal5
                            m_objMergeCmd.CommandType = CommandType.Text;
                            m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                            m_objMergeCmd.ExecuteNonQuery();
                        }
                    }
                }

                //BOB Enhancement Unit Stat : Start
                if (m_objGeneric.IsUnitStatInstalled() && (Utilities.ExistsInArray(p_objDisplayTables, "UNIT_STAT")))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "UNIT_STAT");

                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    else
                    {
                        sTmp = "";
                    }
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[UNIT_STAT_TEMP]", "CLAIM_ID");
                    sSQL = "SELECT CLAIM_ID, UNIT_STAT_ROW_ID" + sTmp
                        + " [INTO UNIT_STAT_TEMP]"
                        + " FROM UNIT_STAT"
                        + " WHERE CLAIM_ID = " + p_lRecordID;

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[UNIT_STAT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[UNIT_STAT_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [UNIT_STAT_TEMP](CLAIM_ID, UNIT_STAT_ROW_ID" + sBitFields + ")"
                            + " SELECT CLAIM_ID,0 " + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //BOB Enhancement Unit Stat : End      


                //Vocational Rehabilitation Case Manager Entity
                if (Utilities.ExistsInArray(p_objDisplayTables, "CM_VR_PG_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CM_VR_PG_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[VOCREHAB_TEMP]", "[VR_PG_ENT_TEMP]", "CMVR_ROW_ID");


                    sSQL = "SELECT CM_X_VOCREHAB.CMVR_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO VR_PG_ENT_TEMP]"
                        + " FROM [VOCREHAB_TEMP], CM_X_VOCREHAB,ENTITY CM_VR_PG_ENTITY WHERE CM_X_VOCREHAB.CMVR_ROW_ID = [VOCREHAB_TEMP].CMVR_ROW_ID"
                        + " AND CM_X_VOCREHAB.PROVIDER_EID = CM_VR_PG_ENTITY.ENTITY_ID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[VR_PG_ENT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[VR_PG_ENT_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [VR_PG_ENT_TEMP](CMVR_ROW_ID" + sBitFields + ")"
                            + " SELECT CMVR_ROW_ID " + sBitValues + " FROM [VOCREHAB_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Case Manager History
                if (Utilities.ExistsInArray(p_objDisplayTables, "CM_X_CMGR_HIST")
                    || Utilities.ExistsInArray(p_objDisplayTables, "CM_CMGR_CM_ENTITY")
                    || Utilities.ExistsInArray(p_objDisplayTables, "CM_CASE_MGR_NOTES")
                    || Utilities.ExistsInArray(p_objDisplayTables, "CM_CMGR_RT_ENTITY")
                    )
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CM_X_CMGR_HIST");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CM_CASE_MGT_TEMP]", "[CMGRHIST_TEMP]", "CASEMGT_ROW_ID");
                    if (iIdx != -1)
                    {
                        sTmp = Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    else { sTmp = string.Empty; }

                    sSQL = "SELECT [CM_CASE_MGT_TEMP].CASEMGT_ROW_ID,CMCMH_ROW_ID" + sTmp
                        + " [INTO CMGRHIST_TEMP]"
                        + "  FROM [CM_CASE_MGT_TEMP],CM_X_CMGR_HIST  WHERE CM_X_CMGR_HIST.CASEMGT_ROW_ID = [CM_CASE_MGT_TEMP].CASEMGT_ROW_ID";

                    //Start : Added By Nitika for autoMailMerge Exe
                    if (!string.IsNullOrEmpty(sPrimaryCasemanager))
                    {
                        sSQL = sSQL + " AND CM_X_CMGR_HIST.PRIMARY_CMGR_FLAG = -1";
                    }
                    //End : Added By Nitika for autoMailMerge Exe
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CMGRHIST_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CMGRHIST_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [CMGRHIST_TEMP](CASEMGT_ROW_ID,CMCMH_ROW_ID" + sBitFields + ")"
                            + " SELECT CASEMGT_ROW_ID,0 " + sBitValues + " FROM [CM_CASE_MGT_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "CM_CMGR_CM_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CM_CMGR_CM_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CMGRHIST_TEMP]", "[CMH_CM_ENT_TEMP]", "CMCMH_ROW_ID");


                    sSQL = "SELECT CM_X_CMGR_HIST.CMCMH_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO CMH_CM_ENT_TEMP]" //incorect earlier discovered during mits 9162
                        + "  FROM [CMGRHIST_TEMP], CM_X_CMGR_HIST,ENTITY CM_CMGR_CM_ENTITY WHERE CM_X_CMGR_HIST.CMCMH_ROW_ID = [CMGRHIST_TEMP].CMCMH_ROW_ID"
                        + " AND CM_X_CMGR_HIST.CASE_MGR_EID = CM_CMGR_CM_ENTITY.ENTITY_ID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CMH_CM_ENT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CMH_CM_ENT_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [CMH_CM_ENT_TEMP](CMCMH_ROW_ID " + sBitFields + ")"
                            + " SELECT CMCMH_ROW_ID  " + sBitValues + " FROM [CMGRHIST_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "CM_CMGR_RT_ENTITY"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CM_CMGR_RT_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CMGRHIST_TEMP]", "[CMH_RT_ENT_TEMP]", "CMCMH_ROW_ID");


                    sSQL = "SELECT CM_X_CMGR_HIST.CMCMH_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO CMH_RT_ENT_TEMP]"
                        + "  FROM [CMGRHIST_TEMP], CM_X_CMGR_HIST,ENTITY CM_CMGR_RT_ENTITY WHERE CM_X_CMGR_HIST.CMCMH_ROW_ID = [CMGRHIST_TEMP].CMCMH_ROW_ID"
                        + " AND CM_X_CMGR_HIST.REF_TO_EID = CM_CMGR_RT_ENTITY.ENTITY_ID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CMH_RT_ENT_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CMH_RT_ENT_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [CMH_RT_ENT_TEMP](CMCMH_ROW_ID " + sBitFields + ")"
                            + " SELECT CMCMH_ROW_ID  " + sBitValues + " FROM [CMGRHIST_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, "CM_CASE_MGR_NOTES"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "CM_CASE_MGR_NOTES");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CMGRHIST_TEMP]", "[CMGR_NOTES_TEMP]", "CMCMH_ROW_ID");


                    sSQL = "SELECT [CMGRHIST_TEMP].CMCMH_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO CMGR_NOTES_TEMP]"
                        + "  FROM [CMGRHIST_TEMP], CASE_MGR_NOTES CM_CASE_MGR_NOTES WHERE CM_CASE_MGR_NOTES.CASEMGR_ROW_ID = [CMGRHIST_TEMP].CMCMH_ROW_ID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CMGR_NOTES_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CMGR_NOTES_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [CMGR_NOTES_TEMP](CMCMH_ROW_ID " + sBitFields + ")"
                            + " SELECT CMCMH_ROW_ID  " + sBitValues + " FROM [CMGRHIST_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //MITS 8791 Unit Supplemental
                if (Utilities.ExistsInArray(p_objDisplayTables, "UNIT_X_CLAIM_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "UNIT_X_CLAIM_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[UNITSUPP_TEMP]", "CLAIM_ID");

                    sUnitSel = "-1";
                    sSQL = "SELECT CLAIM.CLAIM_ID, UNIT_X_CLAIM.UNIT_ROW_ID ";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sSQL += Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }

                    sSQL += " [INTO UNITSUPP_TEMP] FROM CLAIM, UNIT_X_CLAIM, UNIT_X_CLAIM_SUPP" +
                        " WHERE CLAIM.CLAIM_ID = UNIT_X_CLAIM.CLAIM_ID " +
                        "AND UNIT_X_CLAIM_SUPP.UNIT_ROW_ID = UNIT_X_CLAIM.UNIT_ROW_ID " +
                        "AND UNIT_X_CLAIM.CLAIM_ID = " + p_lRecordID.ToString();

                    if (sUnitSel != "-1")
                    {
                        sSQL += " AND " + sUnitSel;
                    }

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[UNITSUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[UNITSUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sTmp = "";
                        sSQL = "INSERT INTO [UNITSUPP_TEMP](CLAIM_ID, UNIT_ROW_ID " + sBitFields + ")"
                            + " SELECT CLAIM_ID, 0" + sBitValues + " FROM [CLAIM_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //mdhamija MITS 27692
                if (Utilities.ExistsInArray(p_objDisplayTables, "CLAIM_DIS_PLAN")
                    || Utilities.ExistsInArray(p_objDisplayTables, "DISABILITY_PLAN"))
                {
                    Generic.ChainIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[DISABILITY_PLAN_TEMP]", "PLAN_ID");
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DISABILITY_PLAN");
                    sSQL = "SELECT CLAIM.CLAIM_ID,CLAIM.PLAN_ID ";
                    sTmp = "";
                    if (iIdx != -1)
                    {
                        sSQL += Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    sSQL += " [INTO DISABILITY_PLAN_TEMP] FROM CLAIM, DISABILITY_PLAN" +
                        " WHERE CLAIM.PLAN_ID = DISABILITY_PLAN.PLAN_ID " +
                        "AND CLAIM.CLAIM_ID =" + p_lRecordID.ToString();
                    if (iIdx != -1)
                    {
                        sFields = p_objDisplayFields[iIdx].ToString();
                    }
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                //mdhamija MITS 27692 End
                objMerge = new Merge(m_iClientId);
                objMerge.Command = m_objMergeCmd;
                objMerge.DSN = m_sConnectionString;
                //-- ABhateja, 10.12.2006 MITS 7741
                //-- Set the "Manager" property of the merge class.
                objMerge.Manager = m_objGeneric.Manager;
                objMerge.DoEventMergeQuery(lEventID, p_objDisplayTables, p_objDisplayFields, p_sJustFieldNames, "[CLAIM_TEMP]", ref sFrom, ref sWhere, bMergeAll, p_lRecordID, sPersonInvolvedType, m_objGeneric.TablesNameUniquePart); // Nitika Added PersonInvolvedType Parameter for autoMailmerge Set up//tkatsarski: 03/13/15 - RMA-1768
                sSQL = "SELECT " + p_sJustFieldNames
                    + " FROM " + sFrom;
                if (sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + sWhere;
                }

                if (p_objDisplayTables.Contains("GENERIC_ORG"))
                {
                    sSQL += " ORDER BY [GENERIC_ORG_TEMP].ENTITY_TABLE_ID DESC";
                }

                //


                //
                m_objMergeCmd.CommandType = CommandType.Text;
                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupClaimMerge();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoClaimMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeConn = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;
                sFrom = null;
                sWhere = null;
                sTmp = null;
                sTmpNext = null;
                sClaimantSel = null;
                sLitSel = null;
                sExpertSel = null;
                sAdjSel = null;
                sLeaveSel = null;
                sAdjTextSel = null;
                sLeaveDetailSel = null;
                sDefSel = null;
                sFundsSel = null;
                sUnitSel = null;
                sBitFields = null;
                sBitValues = null;
                sSQLStart = null;
                sSQLMid = null;
                sFinalSQL = null;
                sCols = null;
                sValues = null;
                sTemp = null;
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
                //Start-Mridul Bansal. 01/22/10. MITS#18230
                sbSql = null;
                //End-Mridul Bansal. 01/22/10. MITS#18230
            }
        }


        //Shruti, 12185


        private double CalculateReserveBalance(double p_dReserve, double p_dPaid, double p_dCollect,
            bool p_bCollInRsvBal, int p_iCStat)
        {
            double dReturnValue = 0;
            double dTemp = 0;

            if (p_bCollInRsvBal)
            {
                dTemp = p_dPaid - p_dCollect;
                if (dTemp < 0)
                    dTemp = 0;
                dReturnValue = p_dReserve - dTemp;
            }
            else
                dReturnValue = p_dReserve - p_dPaid;

            return dReturnValue;
        }
        private double CalculateIncurred(double p_dBalance, double p_dPaid, double p_dCollect,
            bool p_bCollInRsvBal, bool p_bCollInIncurredBal)
        {
            double dReturnValue = 0;
            double dTemp = 0;

            if (p_bCollInRsvBal)
            {
                dTemp = p_dPaid - p_dCollect;
                if (dTemp < 0)
                    dTemp = 0;
                if (p_dBalance < 0)
                    dReturnValue = dTemp;
                else
                    dReturnValue = p_dBalance + dTemp;
            }
            else
            {
                if (p_dBalance < 0)
                    dReturnValue = p_dPaid;
                else
                    dReturnValue = p_dBalance + p_dPaid;
            }


            if (p_bCollInIncurredBal)
            {
                dReturnValue = dReturnValue - p_dCollect;
            }

            if (dReturnValue < 0)
                dReturnValue = 0;

            return dReturnValue;
        }
        //Shruti, 12185 ends
        #endregion

        #region Disablity Plan Merge
        /// Name		: DoDisablityPlanMergeQuery
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to entity
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        internal void DoDisablityPlanMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, long p_ldbMake)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            int iIdx = 0;
            int iIdxNext = 0;
            string sFields = "";
            string sBitFields = "";
            string sBitValues = "";
            Merge objMerge = null;
            DbReader objReader = null;
            string sKeyField = "";
            string sField = "";
            string sValue = "";
            try
            {
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupDisPlanMerge();
                }
                //DISABILITY_PLAN
                if (Utilities.ExistsInArray(p_objDisplayTables, "DISABILITY_PLAN"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DISABILITY_PLAN");
                    if (iIdx != -1)
                    {
                        sFields = p_objDisplayFields[iIdx].ToString();
                    }
                    sSQL = "SELECT PLAN_ID," + sFields + " [INTO PLAN_TEMP] FROM DISABILITY_PLAN "
                        + " WHERE PLAN_ID = " + p_lRecordID.ToString();

                }
                else
                {
                    sSQL = "SELECT PLAN_ID [INTO PLAN_TEMP] FROM DISABILITY_PLAN WHERE PLAN_ID = "
                        + p_lRecordID.ToString();
                }
                m_objMergeCmd.CommandType = CommandType.Text;
                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                m_objMergeCmd.ExecuteNonQuery();
                sFrom = "[PLAN_TEMP]";
                sWhere = "";
                //DIS_PLAN_X_INSURED
                if (Utilities.ExistsInArray(p_objDisplayTables, "DIS_PLAN_X_INSURED") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "INSURED_ENTITY")
                    )
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DIS_PLAN_X_INSURED");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "INSURED_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[PLAN_TEMP]", "[INSURED_TEMP]", "PLAN_ID");
                    sSQL = "SELECT DIS_PLAN_X_INSURED.PLAN_ID";
                    if (iIdx != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                        //Shruti, 4/23/2007, MITS 9161 starts
                        if (p_objDisplayFields[iIdx].ToString().Contains("DIS_PLAN_X_INSURED.INSURED_EID"))
                        {
                            sKeyField = sKeyField + Generic.Append(p_objDisplayFields[iIdx].ToString());
                        }
                        //Shruti, 4/23/2007, MITS 9161 ends
                    }
                    if (iIdxNext != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }

                    sSQL += " [INTO INSURED_TEMP]"
                        + " FROM DIS_PLAN_X_INSURED, ENTITY INSURED_ENTITY WHERE PLAN_ID = " + p_lRecordID.ToString()
                        + " AND DIS_PLAN_X_INSURED.INSURED_EID = INSURED_ENTITY.ENTITY_ID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[INSURED_TEMP]") == 0)
                    {
                        //Shruti, 4/23/2007, MITS 9161 starts
                        if (sKeyField != "")
                        {
                            string[] arrFields = m_objGeneric.SafeSQL(sKeyField).Split(',');
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if (arrFields[i].Contains("DIS_PLAN_X_INSURED.INSURED_EID"))
                                {
                                    string[] arrGetField = arrFields[i].Split(' ');
                                    if (arrGetField.Length == 2)
                                    {
                                        sField = "," + arrGetField[1];
                                        sValue = "," + 0;
                                    }
                                }
                            }
                        }
                        //Shruti, 4/23/2007, MITS 9161 ends

                        m_objGeneric.FindBitFields("[INSURED_TEMP]", ref sBitFields, ref sBitValues);
                        //Shruti, 4/23/2007, MITS 9161 starts
                        sSQL = "INSERT INTO [INSURED_TEMP](PLAN_ID";
                        sSQL = sField == "" ? sSQL + sBitFields + ") SELECT PLAN_ID" + sBitValues + " FROM [PLAN_TEMP]" : sSQL +
                            sField + sBitFields + ") SELECT PLAN_ID" + sValue + sBitValues + " FROM [PLAN_TEMP]";
                        //sSQL = sSQL + sBitFields + ") SELECT PLAN_ID" + sBitValues + " FROM [PLAN_TEMP]";
                        //Shruti, 4/23/2007, MITS 9161 ends
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //9157 DISBALITY_CLASS is missing
                if (Utilities.ExistsInArray(p_objDisplayTables, "DISABILITY_CLASS"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DISABILITY_CLASS");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[PLAN_TEMP]", "[DIS_PLAN_CLASS_TEMP]", "PLAN_ID");
                    sSQL = "SELECT DISABILITY_CLASS.PLAN_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO DIS_PLAN_CLASS_TEMP]"
                        + " FROM DISABILITY_PLAN, DISABILITY_CLASS WHERE DISABILITY_PLAN.PLAN_ID = DISABILITY_CLASS.PLAN_ID"
                        + " AND DISABILITY_PLAN.PLAN_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[DIS_PLAN_CLASS_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[DIS_PLAN_CLASS_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [DIS_PLAN_CLASS_TEMP](PLAN_ID" + sBitFields + ")"
                            + " SELECT PLAN_ID" + sBitValues + " FROM [PLAN_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //Class Table Driven
                if (Utilities.ExistsInArray(p_objDisplayTables, "DIS_CLASS_TD"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DIS_CLASS_TD");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[PLAN_TEMP]", "[TD_TEMP]", "PLAN_ID");
                    sSQL = "SELECT DISABILITY_CLASS.PLAN_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO TD_TEMP]"
                        + " FROM DISABILITY_PLAN, DISABILITY_CLASS,DIS_CLASS_TD WHERE DISABILITY_PLAN.PLAN_ID = DISABILITY_CLASS.PLAN_ID"
                        //+" AND DIS_PLAN_X_INSURED.INSURED_EID = INSURED_ENTITY.ENTITY_ID" //9157 doesnt look right
                        + " AND DISABILITY_CLASS.CLASS_ROW_ID = DIS_CLASS_TD.CLASS_ID"
                        + " AND DISABILITY_PLAN.PLAN_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[TD_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[TD_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [TD_TEMP](PLAN_ID" + sBitFields + ")"
                            + " SELECT PLAN_ID" + sBitValues + " FROM [PLAN_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Supplemental
                if (Utilities.ExistsInArray(p_objDisplayTables, "DIS_PLAN_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "DIS_PLAN_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[PLAN_TEMP]", "[PLANSUPP_TEMP]", "PLAN_ID");
                    sSQL = "SELECT DIS_PLAN_SUPP.PLAN_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO PLANSUPP_TEMP]"
                        + " FROM DIS_PLAN_SUPP WHERE DIS_PLAN_SUPP.PLAN_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[PLANSUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[PLANSUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [PLANSUPP_TEMP](PLAN_ID" + sBitFields + ")"
                            + " SELECT PLAN_ID" + sBitValues + " FROM [PLAN_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                sSQL = "SELECT " + p_sJustFieldNames
                    + " FROM " + sFrom;
                if (sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + sWhere;
                }
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupDisPlanMerge();

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoDisablityPlanMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;
                sFrom = null;
                sWhere = null;
                sFields = null;
                sBitFields = null;
                sBitValues = null;
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
                sKeyField = null;
                sField = null;
                sValue = null;

            }

        }
        #endregion
        //shruti Leave Plan Merge starts
        #region Leave Plan Merge
        /// Name		: DoLeavePlanMergeQuery
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to entity
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        internal void DoLeavePlanMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, long p_ldbMake)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            int iIdx = 0;
            int iIdxNext = 0;
            string sFields = "";
            string sBitFields = "";
            string sBitValues = "";
            Merge objMerge = null;
            DbReader objReader = null;
            try
            {
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupDisPlanMerge();
                }
                //DISABILITY_PLAN
                if (Utilities.ExistsInArray(p_objDisplayTables, "LEAVE_PLAN"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "LEAVE_PLAN");
                    if (iIdx != -1)
                    {
                        sFields = p_objDisplayFields[iIdx].ToString();
                    }
                    sSQL = "SELECT LP_ROW_ID," + sFields + " [INTO LV_PLAN_TEMP] FROM LEAVE_PLAN "
                        + " WHERE LP_ROW_ID = " + p_lRecordID.ToString();

                }
                else
                {
                    sSQL = "SELECT LP_ROW_ID [INTO LV_PLAN_TEMP] FROM LEAVE_PLAN WHERE LP_ROW_ID = "
                        + p_lRecordID.ToString();
                }
                m_objMergeCmd.CommandType = CommandType.Text;
                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                m_objMergeCmd.ExecuteNonQuery();
                sFrom = "[LV_PLAN_TEMP]";
                sWhere = "";
                //Insured
                if (Utilities.ExistsInArray(p_objDisplayTables, "LP_X_ENTITY") ||
                    Utilities.ExistsInArray(p_objDisplayTables, "LV_INSURED_ENTITY")
                    )
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "LP_X_ENTITY");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "LV_INSURED_ENTITY");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[LV_PLAN_TEMP]", "[LV_INSURED_TEMP]", "LP_ROW_ID");
                    sSQL = "SELECT LP_X_ENTITY.LP_ROW_ID";
                    if (iIdx != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    sSQL += " [INTO LV_INSURED_TEMP]"
                        + " FROM LP_X_ENTITY, ENTITY LV_INSURED_ENTITY WHERE LP_ROW_ID = " + p_lRecordID.ToString()
                        + " AND LP_X_ENTITY.ENTITY_EID = LV_INSURED_ENTITY.ENTITY_ID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[LV_INSURED_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[LV_INSURED_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [LV_INSURED_TEMP](LP_ROW_ID" + sBitFields + ")"
                            + " SELECT LP_ROW_ID" + sBitValues + " FROM [LV_PLAN_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //Supplemental
                if (Utilities.ExistsInArray(p_objDisplayTables, "LEAVE_PLAN_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "LEAVE_PLAN_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[LV_PLAN_TEMP]", "[LVPLANSUPP_TEMP]", "LP_ROW_ID");
                    sSQL = "SELECT LEAVE_PLAN_SUPP.LP_ROW_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO LVPLANSUPP_TEMP]"
                        + " FROM LEAVE_PLAN_SUPP WHERE LEAVE_PLAN_SUPP.LP_ROW_ID = " + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[LVPLANSUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[LVPLANSUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [LVPLANSUPP_TEMP](LP_ROW_ID" + sBitFields + ")"
                            + " SELECT LP_ROW_ID" + sBitValues + " FROM [LV_PLAN_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                sSQL = "SELECT " + p_sJustFieldNames
                    + " FROM " + sFrom;
                if (sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + sWhere;
                }
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupLeavePlanMerge();

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoLeavePlanMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;
                sFrom = null;
                sWhere = null;
                sFields = null;
                sBitFields = null;
                sBitValues = null;
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
            }

        }
        #endregion
        //shruti Leave Plan Merge ends

        #region Policy Billing Merge
        /// Name		: DoPolicyBillingMergeQuery
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to entity
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_sTmpTable">Temporary table names</param>
        /// <param name="p_sFrom">From clause in sql query</param>
        /// <param name="p_sWhere">Where clause in sql query</param>
        /// <param name="bMergeAll">Merge all data or not</param>
        internal void DoPolicyBillingMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, long p_ldbMake)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            string sFields = "";
            string sBitFields = "";
            string sBitValues = "";
            int iIdx = 0;
            int iIdxNext = 0;
            long lPolicyId = 0;
            Merge objMerge = null;
            DbReader objReader = null;
            LocalCache objCache = null;
            string sFieldNames = string.Empty;
            string sFieldValues = string.Empty;
            try
            {
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupPolicyBillMerge();
                }
                //Billing Item
                if (Utilities.ExistsInArray(p_objDisplayTables, "BILL_X_BILL_ITEM"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "BILL_X_BILL_ITEM");
                    if (iIdx != -1)
                    {
                        sFields = p_objDisplayFields[iIdx].ToString();
                    }
                    sSQL = "SELECT BILLING_ITEM_ROWID, POLICY_ID, " + sFields + " [INTO BILL_X_BILL_ITEM_TEMP] FROM BILL_X_BILL_ITEM "
                        + " WHERE BILL_X_BILL_ITEM.POLICY_ID = " + p_lRecordID.ToString();

                }
                else
                {
                    sSQL = "SELECT BILLING_ITEM_ROWID, POLICY_ID [INTO BILL_X_BILL_ITEM_TEMP] FROM BILL_X_BILL_ITEM WHERE BILL_X_BILL_ITEM.POLICY_ID = "
                        + p_lRecordID.ToString();
                }
                m_objMergeCmd.CommandType = CommandType.Text;
                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                m_objMergeCmd.ExecuteNonQuery();
                if (m_objGeneric.GetMergeCount("[BILL_X_BILL_ITEM_TEMP]") == 0)
                {
                    m_objGeneric.FindBitFields("[BILL_X_BILL_ITEM_TEMP]", ref sBitFields, ref sBitValues);
                    sSQL = "INSERT INTO [BILL_X_BILL_ITEM_TEMP](BILLING_ITEM_ROWID,POLICY_ID" + sBitFields + ")"
                        + " SELECT 0," + p_lRecordID.ToString() + sBitValues + " FROM POLICY_ENH WHERE POLICY_ID=" + p_lRecordID.ToString();
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                sFrom = "[BILL_X_BILL_ITEM_TEMP]";
                sWhere = "";

                objCache = new LocalCache(m_sConnectionString, m_iClientId);

                lPolicyId = objCache.GetCodeId("P", "POLICY_INDICATOR");


                //Build criteria query
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[BILL_X_BILL_ITEM_TEMP]", "[POLICY_ENH_TEMP]", "POLICY_ID");

                    sSQL = "SELECT POLICY_ENH.POLICY_ID ";
                    if (iIdx != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    sSQL += " [INTO POLICY_ENH_TEMP]"
                        + " FROM POLICY_ENH, POLICY_X_TRANS_ENH WHERE POLICY_ENH.POLICY_ID = " + p_lRecordID.ToString()
                        + " AND POLICY_X_TRANS_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_TRANS_ENH A WHERE A.POLICY_ID = " + p_lRecordID.ToString() + ")"
                        + " AND  POLICY_ENH.POLICY_ID = POLICY_X_TRANS_ENH.POLICY_ID AND POLICY_ENH.POLICY_INDICATOR = " + lPolicyId.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                }

                //TRANSACTIONS Table for policy status
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_TRANS_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_TRANS_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[TRANS_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO TRANS_ENH_TEMP]"
                        + " FROM POLICY_X_TRANS_ENH WHERE POLICY_X_TRANS_ENH.POLICY_ID = " + p_lRecordID.ToString()
                        + " AND POLICY_X_TRANS_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_TRANS_ENH A WHERE A.POLICY_ID = " + p_lRecordID.ToString() + ")";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    if (m_objGeneric.GetMergeCount("[TRANS_ENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[TRANS_ENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [TRANS_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //Policy Terms - This is new. It is found only in Enhanced Policy Merge and not in regular policy merge
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_TERM_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_TERM_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[TERM_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO TERM_ENH_TEMP]"
                        + " FROM POLICY_X_TERM_ENH WHERE POLICY_X_TERM_ENH.POLICY_ID = " + p_lRecordID.ToString()
                        + " AND POLICY_X_TERM_ENH.TERM_ID = (SELECT MAX(TERM_ID) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_lRecordID.ToString() + ")";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();

                    if (m_objGeneric.GetMergeCount("[TERM_ENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[TERM_ENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [TERM_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }

                //Receipts
                if (Utilities.ExistsInArray(p_objDisplayTables, "BILL_X_RECEIPT"))
                {
                    int iIn = 0;
                    iIdx = Generic.IndexOf(p_objDisplayTables, "BILL_X_RECEIPT");


                    sSQL = "SELECT POLICY_ID,BILL_X_RECEIPT.BILLING_ITEM_ROWID," + p_objDisplayFields[iIdx].ToString() //Mits 18595
                        + " [INTO BILL_X_RECEIPT_TEMP]"
                        + " FROM BILL_X_RECEIPT,BILL_X_BILL_ITEM WHERE BILL_X_BILL_ITEM.POLICY_ID = " + p_lRecordID.ToString()
                        + " AND BILL_X_BILL_ITEM.BILLING_ITEM_ROWID = BILL_X_RECEIPT.BILLING_ITEM_ROWID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[BILL_X_RECEIPT_TEMP]") == 0)
                    {
                        iIn = 1;
                        //pmittal5 Mits 18595
                        //m_objGeneric.FindBitFields("[BILL_X_RECEIPT_TEMP]", ref sBitFields, ref sBitValues);
                        //sSQL = "INSERT INTO [BILL_X_RECEIPT_TEMP](BILLING_ITEM_ROWID,POLICY_ID" + sBitFields + ")"
                        //    + " SELECT 0,POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        sFieldValues = "";
                        sFieldNames = "";
                        m_objGeneric.FindFields("[BILL_X_RECEIPT_TEMP]", ref sFieldNames, ref sFieldValues);
                        sSQL = "INSERT INTO [BILL_X_RECEIPT_TEMP](POLICY_ID" + sFieldNames + ")"
                            + " SELECT POLICY_ID" + sFieldValues + " FROM [POLICY_ENH_TEMP]";
                        //End - pmittal5
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                    if (iIn == 0)
                        Generic.ChainIt(ref sFrom, ref sWhere, "[BILL_X_BILL_ITEM_TEMP]", "[BILL_X_RECEIPT_TEMP]", "BILLING_ITEM_ROWID");
                    else
                        Generic.ChainItWithOr(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[BILL_X_RECEIPT_TEMP]", "POLICY_ID");
                }

                //Adjustment
                if (Utilities.ExistsInArray(p_objDisplayTables, "BILL_X_ADJSTMNT"))
                {
                    int iIn = 0;
                    iIdx = Generic.IndexOf(p_objDisplayTables, "BILL_X_ADJSTMNT");


                    sSQL = "SELECT BILL_X_ADJSTMNT.BILLING_ITEM_ROWID ,POLICY_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO BILL_X_ADJSTMNT_TEMP]"
                        + " FROM BILL_X_ADJSTMNT,BILL_X_BILL_ITEM WHERE BILL_X_BILL_ITEM.POLICY_ID = " + p_lRecordID.ToString()
                        + " AND BILL_X_BILL_ITEM.BILLING_ITEM_ROWID = BILL_X_ADJSTMNT.BILLING_ITEM_ROWID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[BILL_X_ADJSTMNT_TEMP]") == 0)
                    {
                        iIn = 1;
                        m_objGeneric.FindBitFields("[BILL_X_ADJSTMNT_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [BILL_X_ADJSTMNT_TEMP](BILLING_ITEM_ROWID,POLICY_ID" + sBitFields + ")"
                            + " SELECT 0,POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                    if (iIn == 0)
                        Generic.ChainIt(ref sFrom, ref sWhere, "[BILL_X_BILL_ITEM_TEMP]", "[BILL_X_ADJSTMNT_TEMP]", "BILLING_ITEM_ROWID");
                    else
                        Generic.ChainItWithOr(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[BILL_X_ADJSTMNT_TEMP]", "POLICY_ID");
                }

                //Disbursement
                if (Utilities.ExistsInArray(p_objDisplayTables, "BILL_X_DSBRSMNT"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "BILL_X_DSBRSMNT");
                    int iIn = 0;

                    sSQL = "SELECT POLICY_ID, BILL_X_DSBRSMNT.BILLING_ITEM_ROWID," + p_objDisplayFields[iIdx].ToString() //Mits 18595
                        + " [INTO BILL_X_DSBRSMNT_TEMP]"
                        + " FROM BILL_X_DSBRSMNT,BILL_X_BILL_ITEM WHERE BILL_X_BILL_ITEM.POLICY_ID = " + p_lRecordID.ToString()
                        + " AND BILL_X_BILL_ITEM.BILLING_ITEM_ROWID = BILL_X_DSBRSMNT.BILLING_ITEM_ROWID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[BILL_X_DSBRSMNT_TEMP]") == 0)
                    {
                        iIn = 1;
                        //pmittal5 Mits 18595
                        //m_objGeneric.FindBitFields("[BILL_X_DSBRSMNT_TEMP]", ref sBitFields, ref sBitValues);
                        //sSQL = "INSERT INTO [BILL_X_DSBRSMNT_TEMP](BILLING_ITEM_ROWID,POLICY_ID" + sBitFields + ")"
                        //    + " SELECT 0,POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        sFieldValues = "";
                        sFieldNames = "";
                        m_objGeneric.FindFields("[BILL_X_DSBRSMNT_TEMP]", ref sFieldNames, ref sFieldValues);
                        sSQL = "INSERT INTO [BILL_X_DSBRSMNT_TEMP](POLICY_ID" + sFieldNames + ")"
                            + " SELECT POLICY_ID" + sFieldValues + " FROM [POLICY_ENH_TEMP]";
                        //End - pmittal5
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                    if (iIn == 0)
                        Generic.ChainIt(ref sFrom, ref sWhere, "[BILL_X_BILL_ITEM_TEMP]", "[BILL_X_DSBRSMNT_TEMP]", "BILLING_ITEM_ROWID");
                    else
                        Generic.ChainItWithOr(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[BILL_X_DSBRSMNT_TEMP]", "POLICY_ID");
                }

                //Premium Item
                if (Utilities.ExistsInArray(p_objDisplayTables, "BILL_X_PREM_ITEM"))
                {
                    int iIn = 0;
                    iIdx = Generic.IndexOf(p_objDisplayTables, "BILL_X_PREM_ITEM");

                    sSQL = "SELECT BILL_X_PREM_ITEM.BILLING_ITEM_ROWID ,POLICY_ID," + p_objDisplayFields[iIdx].ToString()
                        + " [INTO BILL_X_PREM_ITEM_TEMP]"
                        + " FROM BILL_X_PREM_ITEM,BILL_X_BILL_ITEM WHERE BILL_X_BILL_ITEM.POLICY_ID = " + p_lRecordID.ToString()
                        + " AND BILL_X_BILL_ITEM.BILLING_ITEM_ROWID = BILL_X_PREM_ITEM.BILLING_ITEM_ROWID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[BILL_X_PREM_ITEM_TEMP]") == 0)
                    {
                        iIn = 1;
                        m_objGeneric.FindBitFields("[BILL_X_PREM_ITEM_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [BILL_X_PREM_ITEM_TEMP](BILLING_ITEM_ROWID,POLICY_ID" + sBitFields + ")"
                            + " SELECT 0,POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                    if (iIn == 0)
                        Generic.ChainIt(ref sFrom, ref sWhere, "[BILL_X_BILL_ITEM_TEMP]", "[BILL_X_PREM_ITEM_TEMP]", "BILLING_ITEM_ROWID");
                    else
                        Generic.ChainItWithOr(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[BILL_X_PREM_ITEM_TEMP]", "POLICY_ID");
                }
                sSQL = "SELECT " + p_sJustFieldNames
                    + " FROM " + sFrom;
                if (sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + sWhere;
                }
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupPolicyBillMerge();

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoPolicyBillingMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                sSQL = null;
                sFrom = null;
                sWhere = null;
                sFields = null;
                sBitFields = null;
                sBitValues = null;
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
            }

        }
        #endregion
        //shruti Policy Billing Merge ends

        #region Enhanced Policy Merge
        // Start Naresh Enhanced Policy Merge
        /// Name		: DoEnhPolicyMergeQuery
        /// Author		: Naresh Chandra Padhy
        /// Date Created: 01/22/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///<summary>
        /// This function retrieves data related to policy
        /// </summary>
        /// <param name="p_lRecordID">Record id for which we need to get the data</param>
        /// <param name="p_objDisplayTables">Table names from which to get the merge data</param>
        /// <param name="p_objDisplayFields">Field names from which to get the merge data</param>
        /// <param name="p_sJustFieldNames">Field names from which to get the merge data</param>
        /// <param name="p_ldbMake">Database Type</param>
        internal void DoEnhPolicyMergeQuery(long p_lRecordID, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            string p_sJustFieldNames, long p_ldbMake)
        {
            int iPolicyInd = 0;
            string sPolicyStatus = "";
            string sPolicyStatusDesc = "";
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            int iIdx = 0;
            int iIdxNext = 0;
            string sBitFields = "";
            string sBitValues = "";
            //VehicleTempTable objTempTable;
            Merge objMerge = null;
            DbReader objReader = null;
            try
            {
                //objTempTable = new VehicleTempTable();
                m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
                m_objMergeConn.Open();
                m_objMergeCmd = m_objMergeConn.CreateCommand();
                if (p_ldbMake != (int)eDatabaseType.DBMS_IS_ORACLE)
                {
                    CleanupEnhPolicyMerge();
                }
                //POLICY
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_ENH");
                    sSQL = "SELECT POLICY_INDICATOR FROM POLICY_ENH WHERE POLICY_ID = " + p_lRecordID.ToString();
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null && objReader.Read())
                    {
                        iPolicyInd = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_INDICATOR"), m_iClientId);
                        using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
                        {
                            objCache.GetCodeInfo(iPolicyInd, ref sPolicyStatus, ref sPolicyStatusDesc);
                        }
                        if (sPolicyStatus.ToUpper() == "Q")
                        {
                            sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() +
                       " [INTO POLICY_ENH_TEMP] FROM POLICY_ENH WHERE POLICY_ID = " + p_lRecordID.ToString();
                        }
                        else
                        {
                            p_objDisplayFields[iIdx] = p_objDisplayFields[iIdx].ToString().Replace("POLICY_ENH.POLICY_STATUS_CODE", "POLICY_X_TRANS_ENH.POLICY_STATUS");
                            sSQL = "SELECT POLICY_ENH.POLICY_ID," + p_objDisplayFields[iIdx] + " [INTO POLICY_ENH_TEMP] FROM POLICY_ENH, POLICY_X_TRANS_ENH WHERE POLICY_ENH.POLICY_ID = " + p_lRecordID;
                            sSQL = sSQL + " AND POLICY_X_TRANS_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_TRANS_ENH A WHERE A.POLICY_ID = " + p_lRecordID + ")";
                            sSQL = sSQL + " AND  POLICY_ENH.POLICY_ID = POLICY_X_TRANS_ENH.POLICY_ID";
                        }
                    }
                }
                else
                {
                    sSQL = "SELECT POLICY_ID [INTO POLICY_ENH_TEMP] FROM POLICY_ENH WHERE POLICY_ID = "
                        + p_lRecordID.ToString();
                }
                m_objMergeCmd.CommandType = CommandType.Text;
                m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                m_objMergeCmd.ExecuteNonQuery();
                sFrom = "[POLICY_ENH_TEMP]";
                sWhere = "";
                //TRANSACTIONS Table for policy status
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_TRANS_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_TRANS_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[TRANS_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO TRANS_ENH_TEMP]"
                    + " FROM POLICY_X_TRANS_ENH"
                    + " WHERE POLICY_X_TRANS_ENH.POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_X_TRANS_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_TRANS_ENH A WHERE A.POLICY_ID = " + p_lRecordID + ")";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[TRANS_ENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[TRANS_ENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [TRANS_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Policy Terms
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_TERM_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_TERM_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[TERM_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO TERM_ENH_TEMP]"
                    + " FROM POLICY_X_TERM_ENH"
                    + " WHERE POLICY_X_TERM_ENH.POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_X_TERM_ENH.TERM_ID = (SELECT MAX(TERM_ID) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_lRecordID + ")";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[TERM_ENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[TERM_ENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [TERM_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //INSURER
                if (Utilities.ExistsInArray(p_objDisplayTables, "INSURER_ENTITY_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "INSURER_ENTITY_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[INS_ENT_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ENH.POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO INS_ENT_ENH_TEMP]"
                    + " FROM POLICY_ENH,ENTITY INSURER_ENTITY_ENH"
                    + " WHERE POLICY_ENH.POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_ENH.INSURER_EID = INSURER_ENTITY_ENH.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }
                //COVERAGE TYPE
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_CVG_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_CVG_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[CVG_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO CVG_ENH_TEMP]"
                    + " FROM POLICY_X_CVG_ENH"
                    + " WHERE POLICY_X_CVG_ENH.POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_X_CVG_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_CVG_ENH A WHERE A.POLICY_ID = " + p_lRecordID.ToString() + ")";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[CVG_ENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[CVG_ENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [CVG_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Rating
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_RTNG_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_RTNG_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[RTNG_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO RTNG_ENH_TEMP]"
                    + " FROM POLICY_X_RTNG_ENH"
                    + " WHERE POLICY_X_RTNG_ENH.POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_X_RTNG_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_RTNG_ENH A WHERE A.POLICY_ID = " + p_lRecordID.ToString() + ")";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[RTNG_ENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[RTNG_ENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [RTNG_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Discount
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_DCNT_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_DCNT_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[DCNT_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO DCNT_ENH_TEMP]"
                    + " FROM POLICY_X_DCNT_ENH"
                    + " WHERE POLICY_X_DCNT_ENH.POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_X_DCNT_ENH.TRANSACTION_ID =    (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_DCNT_ENH A WHERE A.POLICY_ID = " + p_lRecordID + ")";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[DCNT_ENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[DCNT_ENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [DCNT_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Discount Tier
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_DTIER_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_DTIER_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[DTIER_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO DTIER_ENH_TEMP]"
                    + " FROM POLICY_X_DTIER_ENH"
                    + " WHERE POLICY_X_DTIER_ENH.POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_X_DTIER_ENH.TRANSACTION_ID =    (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_DTIER_ENH A WHERE A.POLICY_ID = " + p_lRecordID.ToString() + ")";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[DTIER_ENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[DTIER_ENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [DTIER_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //Exposures
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_EXP_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_EXP_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[EXP_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO EXP_ENH_TEMP]"
                    + " FROM POLICY_X_EXP_ENH"
                    + " WHERE POLICY_X_EXP_ENH.POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_X_EXP_ENH.TRANSACTION_ID =    (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_EXP_ENH A WHERE A.POLICY_ID = " + p_lRecordID.ToString() + ")";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[EXP_ENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[EXP_ENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [EXP_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //INSURED
                if (Utilities.ExistsInArray(p_objDisplayTables, "INSURED_ENTITY_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "INSURED_ENTITY_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[INSURED_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO INSURED_ENH_TEMP]"
                    + " FROM POLICY_X_INSRD_ENH,ENTITY INSURED_ENTITY_ENH WHERE POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_X_INSRD_ENH.INSURED_EID = INSURED_ENTITY_ENH.ENTITY_ID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[INSURED_ENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[INSURED_ENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [INSURED_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }




                //For mits 27889
                //POLICY BROKER 
                if (Utilities.ExistsInArray(p_objDisplayTables, "BRO_ENTITY_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "BRO_ENTITY_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[BRO_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO BRO_ENH_TEMP]"
                    + " FROM POLICY_ENH,ENTITY BRO_ENTITY_ENH WHERE POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_ENH.BROKER_EID = BRO_ENTITY_ENH.ENTITY_ID";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[BRO_ENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[BRO_ENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [BRO_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [BRO_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }



                //POLICY BROKER  Firm
                if (Utilities.ExistsInArray(p_objDisplayTables, "BROKER_ENTITY_FIRM"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "BROKER_ENTITY_FIRM");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[BROKER_FIRM_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO BROKER_FIRM_TEMP]"
                     + " FROM POLICY_ENH,Entity Broker,ENTITY BROKER_ENTITY_FIRM WHERE POLICY_ID = " + p_lRecordID.ToString()
                     + " AND POLICY_ENH.BROKER_EID = Broker.ENTITY_ID"
                    + " AND Broker.PARENT_EID = BROKER_ENTITY_FIRM.ENTITY_ID  ";

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                }




                //POLICY MCO
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_X_MCO_ENH")
                    || Utilities.ExistsInArray(p_objDisplayTables, "MCO_ENTITY_ENH"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_X_MCO_ENH");
                    iIdxNext = Generic.IndexOf(p_objDisplayTables, "MCO_ENTITY_ENH");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[MCO_ENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID";
                    if (iIdx != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdx].ToString());
                    }
                    if (iIdxNext != -1)
                    {
                        sSQL = sSQL + Generic.Append(p_objDisplayFields[iIdxNext].ToString());
                    }
                    sSQL = sSQL + " [INTO MCO_ENH_TEMP]"
                    + " FROM POLICY_X_MCO_ENH,ENTITY MCO_ENTITY_ENH WHERE POLICY_ID = " + p_lRecordID.ToString()
                    + " AND POLICY_X_MCO_ENH.MCO_EID = MCO_ENTITY_ENH.ENTITY_ID";
                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[MCO_ENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[MCO_ENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [MCO_ENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //POLICY SUPPLEMENTAL
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[POLSUPP_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO POLSUPP_TEMP]"
                    + " FROM POLICY_SUPP"
                    + " WHERE POLICY_SUPP.POLICY_ID = " + p_lRecordID.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[POLSUPP_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[POLSUPP_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [POLSUPP_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                //gagnihotri 10/05/2009
                //ENHANCED POLICY SUPPLEMENTAL
                if (Utilities.ExistsInArray(p_objDisplayTables, "POLICY_ENH_SUPP"))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, "POLICY_ENH_SUPP");
                    Generic.ChainIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[POLSUPPENH_TEMP]", "POLICY_ID");
                    sSQL = "SELECT POLICY_ID," + p_objDisplayFields[iIdx].ToString() + " [INTO POLSUPPENH_TEMP]"
                    + " FROM POLICY_ENH_SUPP"
                    + " WHERE POLICY_ENH_SUPP.POLICY_ID = " + p_lRecordID.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[POLSUPPENH_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[POLSUPPENH_TEMP]", ref sBitFields, ref sBitValues);
                        sSQL = "INSERT INTO [POLSUPPENH_TEMP](POLICY_ID" + sBitFields + ")"
                            + " SELECT POLICY_ID" + sBitValues + " FROM [POLICY_ENH_TEMP]";
                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
                sSQL = "SELECT " + p_sJustFieldNames
                    + " FROM " + sFrom;
                if (sWhere != "")
                {
                    sSQL = sSQL + " WHERE " + sWhere;
                }
                m_objGeneric.GetDataset(m_objMergeConn, sSQL, ref m_objMergeds);
                objMerge = new Merge(m_iClientId);
                objMerge.Command = m_objMergeCmd;
                //Merge.Data = m_objMergeds;    //tkatsarski: 03/13/15 - RMA-1768
                CleanupPolicyMerge();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.DoEnhPolicyMergeQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objMerge != null)
                {
                    objMerge.Dispose();
                }
                if (m_objMergeCmd != null)
                {
                    m_objMergeCmd = null;
                }
                if (m_objMergeConn != null)
                {
                    m_objMergeConn.Close();
                    m_objMergeConn.Dispose();
                    m_objMergeCmd = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;
                sFrom = null;
                sWhere = null;
                sBitFields = null;
                sBitValues = null;
                //tkatsarski: 03/13/15 - RMA-1768
                //if (m_objMergeds != null)
                //{
                //    m_objMergeds.Dispose();
                //}
                sPolicyStatus = null;
                sPolicyStatusDesc = null;
            }

        }
        // End Naresh Enhanced Policy Merge
        #endregion
        #region PhoneNumbers Merge
        private void AddOrgPhoneQuery(string p_sPrefix, int p_iLevel, string p_sChild, ArrayList p_objDisplayTables, ArrayList p_objDisplayFields,
            ref string p_sFrom, ref  string p_sWhere, long p_lRecordID)
        {
            string sTableName = "";
            string sDeptField = "";
            string sTempTable = "";
            int iIdx = 0;
            string sSQL = "";
            string sBitFields = "";
            string sBitValues = "";
            string sPKField = "";
            DbConnection objConn = null;
            DbReader objReader = null;
            string sFieldName = string.Empty;
            string sFieldPos = string.Empty;
            int iPos = 0;
            try
            {
                switch (p_sChild)
                {
                    case "PATIENT":
                        sDeptField = "FACILITY_DEPT_EID";
                        sTempTable = "[PAT_TEMP]";
                        sPKField = "PATIENT_ID";
                        break;
                    case "PHYSICIAN":
                        sDeptField = "DEPT_ASSIGNED_EID";
                        sTempTable = "[QPHYS_TEMP]";
                        sPKField = "PHYS_EID";
                        break;
                    case "MED_STAFF":
                        sDeptField = "DEPT_ASSIGNED_EID";
                        sPKField = "STAFF_EID";
                        sTempTable = "[MED_TEMP]";
                        break;
                    case "EMPLOYEE":
                        sDeptField = "DEPT_ASSIGNED_EID";
                        sPKField = "EMPLOYEE_EID";
                        sTempTable = "[EMP_TEMP]";
                        break;
                }
                if (p_iLevel == 1)
                {
                    sTableName = p_sPrefix + "CLNT";
                }
                else if (p_iLevel == 2)
                {
                    sTableName = p_sPrefix + "COMP";
                }
                else if (p_iLevel == 3)
                {
                    sTableName = p_sPrefix + "OP";
                }
                else if (p_iLevel == 4)
                {
                    sTableName = p_sPrefix + "REG";
                }
                else if (p_iLevel == 5)
                {
                    sTableName = p_sPrefix + "DIV";
                }
                else if (p_iLevel == 6)
                {
                    sTableName = p_sPrefix + "LOC";
                }
                else if (p_iLevel == 7)
                {
                    sTableName = p_sPrefix + "FAC";
                }
                else if (p_iLevel == 8)
                {
                    sTableName = p_sPrefix + "DEPT";
                }
                if (Utilities.ExistsInArray(p_objDisplayTables, sTableName))
                {
                    iIdx = Generic.IndexOf(p_objDisplayTables, sTableName);
                    Generic.ChainIt(ref p_sFrom, ref p_sWhere, sTempTable, "[" + sTableName + "_TEMP]", sPKField);
                    iPos = p_objDisplayFields[iIdx].ToString().IndexOf("|");
                    sFieldName = p_objDisplayFields[iIdx].ToString().Substring(0, iPos - 1);
                    sFieldPos = p_objDisplayFields[iIdx].ToString().Substring(iPos);
                    sbSql.Length = 0;
                    sbSql.AppendFormat("SELECT {0}.{1},{2}{3}'('{3}CODE_DESC{3}')'{4}", p_sChild, sPKField, sFieldName, sConcatOperator, sFieldPos);
                    sbSql.AppendFormat(" [INTO {0}_TEMP]", sTableName);
                    sbSql.AppendFormat(" FROM CODES_TEXT,{0},ADDRESS_X_PHONEINFO {1},ENTITY {2}DEPT_ENT", p_sChild, sTableName, p_sPrefix);

                    if (p_iLevel < 8)
                    {
                        sbSql.AppendFormat(",ENTITY {0}FAC_ENT", p_sPrefix);
                    }
                    if (p_iLevel < 7)
                    {
                        sbSql.AppendFormat(",ENTITY {0}LOC_ENT", p_sPrefix);
                    }
                    if (p_iLevel < 6)
                    {
                        sbSql.AppendFormat(",ENTITY {0}DIV_ENT", p_sPrefix);
                    }
                    if (p_iLevel < 5)
                    {
                        sbSql.AppendFormat(",ENTITY {0}REG_ENT", p_sPrefix);
                    }
                    if (p_iLevel < 4)
                    {
                        sbSql.AppendFormat(",ENTITY {0}OP_ENT", p_sPrefix);
                    }
                    if (p_iLevel < 3)
                    {
                        sbSql.AppendFormat(",ENTITY {0}COMP_ENT", p_sPrefix);
                    }
                    if (p_iLevel < 2)
                    {
                        sbSql.AppendFormat(",ENTITY {0}CLNT_ENT", p_sPrefix);
                    }
                    sbSql.AppendFormat(" WHERE {0}.{1} = {2}  AND CODES_TEXT.CODE_ID = {3}.PHONE_CODE", p_sChild, sPKField, p_lRecordID.ToString(), sTableName);
                    sbSql.AppendFormat("  AND  {0}.{1} = {2}DEPT_ENT.ENTITY_ID", p_sChild, sDeptField, p_sPrefix);
                    sbSql.AppendFormat("  AND  {0}_ENT.ENTITY_ID = {0}.ENTITY_ID", sTableName);
                    if (p_iLevel < 8)
                    {
                        sbSql.AppendFormat(" AND {0}DEPT_ENT.PARENT_EID = {0}FAC_ENT.ENTITY_ID", p_sPrefix);
                    }
                    if (p_iLevel < 7)
                    {
                        sbSql.AppendFormat(" AND {0}FAC_ENT.PARENT_EID = {0}LOC_ENT.ENTITY_ID", p_sPrefix);
                    }
                    if (p_iLevel < 6)
                    {
                        sbSql.AppendFormat(" AND {0}LOC_ENT.PARENT_EID = {0}DIV_ENT.ENTITY_ID", p_sPrefix);
                    }
                    if (p_iLevel < 5)
                    {
                        sbSql.AppendFormat(" AND {0}DIV_ENT.PARENT_EID = {0}REG_ENT.ENTITY_ID", p_sPrefix);
                    }
                    if (p_iLevel < 4)
                    {
                        sbSql.AppendFormat(" AND {0}REG_ENT.PARENT_EID = {0}OP_ENT.ENTITY_ID", p_sPrefix);
                    }
                    if (p_iLevel < 3)
                    {
                        sbSql.AppendFormat(" AND {0}OP_ENT.PARENT_EID = {0}COMP_ENT.ENTITY_ID", p_sPrefix);
                    }
                    if (p_iLevel < 2)
                    {
                        sbSql.AppendFormat(" AND {0}COMP_ENT.PARENT_EID = {0}CLNT_ENT.ENTITY_ID", p_sPrefix);
                    }
                    sSQL = sbSql.ToString();

                    m_objMergeCmd.CommandType = CommandType.Text;
                    m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                    m_objMergeCmd.ExecuteNonQuery();
                    if (m_objGeneric.GetMergeCount("[" + sTableName + "_TEMP]") == 0)
                    {
                        m_objGeneric.FindBitFields("[" + sTableName + "_TEMP]", ref sBitFields, ref sBitValues);
                        sbSql.Length = 0;
                        sbSql.AppendFormat("INSERT INTO [{0}_TEMP]({1}{2})", sTableName, sPKField, sBitFields);
                        sbSql.AppendFormat(" SELECT {0}{1} FROM {2}", sPKField, sBitValues, sTempTable);
                        sSQL = sbSql.ToString();

                        m_objMergeCmd.CommandType = CommandType.Text;
                        m_objMergeCmd.CommandText = m_objGeneric.SafeSQL(sSQL);
                        m_objMergeCmd.ExecuteNonQuery();
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MailMergeQuery.QOrgHQuery.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                sTableName = null;
                sDeptField = null;
                sTempTable = null;
                sSQL = null;
                sBitFields = null;
                sBitValues = null;
                sPKField = null;
            }
        }
        #endregion

        // akaushk5 Added for MITS 37272 Starts
        /// <summary>
        /// Determines whether [is current adjuster attached] [the specified claim identifier].
        /// </summary>
        /// <param name="claimId">The claim identifier.</param>
        /// <returns>Is current Adjuster present on Claim.</returns>
        private bool IsCurrentAdjusterAttached(long claimId)
        {
            string sql = string.Format("SELECT COUNT(CURRENT_ADJ_FLAG) AS CNT FROM CLAIM_ADJUSTER WHERE CLAIM_ID = {0} AND CURRENT_ADJ_FLAG = -1" , claimId);
            int currentAdjusterCount = default(int);
            using (DbReader objDbReader = DbFactory.GetDbReader(this.m_sConnectionString, sql))
            {
                if (!object.ReferenceEquals(objDbReader, null) && objDbReader.Read())
                {
                    currentAdjusterCount = objDbReader.GetInt("CNT");
                }
            }
            return !currentAdjusterCount.Equals(default(int));
        }
        // akaushk5 Added for MITS 37272 Ends
    }
}
