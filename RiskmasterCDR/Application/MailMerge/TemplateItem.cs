﻿
using System;

namespace Riskmaster.Application.MailMerge
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   29th November 2004
	///Purpose :   This class Creates, Edits, Fetches template information.
	/// </summary>
	internal class TemplateItem
	{
		#region Variable Declaration
		/// <summary>
		/// Template id 
		/// </summary>
		public long m_lTemplateId=0; 
		/// <summary>
		///  Template description
		/// </summary>
		public string m_sTemplateDesc=""; 
		/// <summary>
		/// Category
		/// </summary>
		public long m_lCategory=0; 
		/// <summary>
		/// Category description
		/// </summary>
		public string m_sCategoryDesc="";
        private int m_iClientId = 0;
		#endregion

		#region Property Declaration
		/// <summary>
		/// Template id
		/// </summary>
		public long TemplateId
		{
			get
			{
				return m_lTemplateId;
			}
			set
			{
				m_lTemplateId=value;
			}
		}
		/// <summary>
		/// Category
		/// </summary>
		public long Category
		{
			get
			{
				return m_lCategory;
			}
			set
			{
				m_lCategory=value;
			}
		}
		/// <summary>
		/// Category Description
		/// </summary>
		public string CategoryDesc
		{
			get
			{
				return m_sCategoryDesc;
			}
			set
			{
				m_sCategoryDesc=value;
			}
		}
		/// <summary>
		/// Template description
		/// </summary>
		public string TemplateDesc
		{
			get
			{
				return m_sTemplateDesc;
			}
			set
			{
				m_sTemplateDesc=value;
			}
		}
		#endregion

		#region TemplateItem constructor
		/// <summary>
		/// TemplateItem constructor
		/// </summary>
        public TemplateItem(int p_iClientId) { m_iClientId = p_iClientId; }
		#endregion
	}
}
