﻿using System;
using Riskmaster.DataModel;
using System.Collections;
using System.IO;
using System.Text;
using System.Data;
using System.Xml;
using System.Threading;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.Application.FileStorage;
using Riskmaster.Application.MediaViewWrapper;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;
namespace Riskmaster.Application.MailMerge
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   29th November 2004
	///Purpose :   This class Creates, Edits, Fetches template information.
	/// </summary>
	public class Template
	{
		#region Constant Declaration
		/// <summary>
		/// Default form id constant
		/// </summary>
		private const int DEFAULT_FORM_ID=-5000;
		#endregion

		#region Variable Declaration
		/// <summary>
		/// Documnet storage path
		/// </summary>
		private string m_sDocStorageDSN=""; 
		/// <summary>
		/// Db or File or S3 storage
		/// </summary>
        private long m_iDBStorageType = 0;
        private int m_iClientId = 0;
		/// <summary>
		/// Form id
		/// </summary>
		private long m_lFormId=0; 
		/// <summary>
		/// Category id
		/// </summary>
		private long m_lCatId=0; 
		/// <summary>
		/// Form name
		/// </summary>
		private string m_sFormName=""; 
		/// <summary>
		/// Form Description
		/// </summary>
		private string m_sFormDesc=""; 
		/// <summary>
		/// Table name
		/// </summary>
		private string m_sTableQualify=""; 
		/// <summary>
		/// Template file name
		/// </summary>
		private string m_sFormFileName=""; 
		/// <summary>
		/// Line of Business Code
		/// </summary>
		private long m_lLOB=0; 
		/// <summary>
		/// State Code
		/// </summary>
		private long m_lStateId=0; 
		/// <summary>
		/// Merge Format Type Code
		/// </summary>
		private long m_lMergeFormatTypeCode=0; 
		/// <summary>
		/// Merge Document Type Code
		/// </summary>
		private long m_lMergeDocTypeCode=0; 
		/// <summary>
		/// File contents
		/// </summary>
		private string m_sFormFileContent="";
		/// <summary>
		/// Fields list
		/// </summary>
		private SortedList m_objColFields=null;
		/// <summary>
		/// Permissions list
		/// </summary>
		private ArrayList m_arrlstColPerms=null;
		/// <summary>
		/// Admin table list
		/// </summary>
		private ArrayList m_arrlstAvailAdminTables=null;
		/// <summary>
		/// Existing Field list
		/// </summary>
		private ArrayList m_arrlstAvailFields=null;
		/// <summary>
		/// Template name for already available template in the system
		/// </summary>
		private string m_sPrefab="";
		/// <summary>
		/// Category name
		/// </summary>
		private string m_sCatName="";
		/// <summary>
		/// Manager object
		/// </summary>
		private IManager m_objManager=null;
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sConnectionString="";
		/// <summary>
		/// Secure connection string
		/// </summary>
		private string m_sSecureConnectionString="";
        //spahariya MITS 28867
        /// <summary>
        /// Email Check
        /// </summary>
        private long m_lEmailCheck = 0;
        /// <summary>
        /// Mail default recipient
        /// </summary>
        private long m_lMailRecipient = 0; 
        //spahariya - end
        //added by swati MITS # 36930
        /// <summary>
        /// Mail default recipient list
        /// </summary>
        private string m_sMailUserRecipient = string.Empty;
        //end by swati
		private UserLogin m_userLogin = null;

        private string m_UserDataPath = RMConfigurator.UserDataPath;

		#endregion

		#region Property Declaration

		/// <summary>
		/// Category Id
		/// </summary>
		public long CatId 
		{
			get
			{
				return m_lCatId;
			}
			set
			{
				m_lCatId=value;
			}
		}
		/// <summary>
		/// Connection string
		/// </summary>
		public string DSN 
		{
			set
			{
				m_sConnectionString=value;
			}
		}
		/// <summary>
		/// Security database connection string
		/// </summary>
		public string SecureDSN 
		{
			set
			{
				m_sSecureConnectionString=value;
			}
		}
		/// <summary>
		/// Template name
		/// </summary>
		public string FormName 
		{
			get
			{
				return m_sFormName;
			}
			set
			{
				m_sFormName=value;
			}
		}
		/// <summary>
		/// Template Description
		/// </summary>
		public string FormDesc 
		{
			get
			{
				return m_sFormDesc;
			}
			set
			{
				m_sFormDesc=value;
			}
		}
		/// <summary>
		/// Table name for admin tracking merge
		/// </summary>
		public string TableQualify 
		{
			get
			{
				return m_sTableQualify;
			}
			set
			{
				m_sTableQualify=value;
			}
		}
		/// <summary>
		/// Gets/Sets the Line of Business Code
		/// </summary>
		public long LOB 
		{
			get
			{
				return m_lLOB;
			}
			set
			{
				m_lLOB=value;
			}
		}
		/// <summary>
		/// Gets/Sets the State Code
		/// </summary>
		public long StateId
		{
			get
			{
				return m_lStateId;
			}
			set
			{
				m_lStateId=value;
			}
		}
		/// <summary>
		/// Gets/Sets the Merge Document Type Code
		/// </summary>
		public long MergeDocTypeCode
		{
			get
			{
				return m_lMergeDocTypeCode;
			}
			set
			{
				m_lMergeDocTypeCode=value;
			}
		}
		/// <summary>
		/// Gets/Sets the Merge Format Type Code
		/// </summary>
		public long MergeFormatTypeCode
		{
			get
			{
				return m_lMergeFormatTypeCode;
			}
			set
			{
				m_lMergeFormatTypeCode=value;
			}
		}

		/// <summary>
		/// To test for a new template
		/// </summary>
		public bool IsNew 
		{
			get
			{
				if (FormId <= 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			set
			{
				IsNew=value;
			}
		}
		/// <summary>
		/// Template file name
		/// </summary>
		public string FormFileName 
		{
			get
			{
				return m_sFormFileName;
			}
			set
			{
				m_sFormFileName=value;
			}
		}
		/// <summary>
		/// Category name
		/// </summary>
		public string CategoryName 
		{
			get
			{
				return m_sCatName;
			}
			
		}
        //spahariya 28867
        /// <summary>
        /// Gets/Sets Mail default recipient
        /// </summary>
        public long MailRecipient
        {
            get
            {
                return m_lMailRecipient;
            }
            set
            {
                m_lMailRecipient = value;
            }
        }
        /// <summary>
        /// Gets/Sets Email Check
        /// </summary>
        public long EmailCheck
        {
            get
            {
                return m_lEmailCheck;
            }
            set
            {
                m_lEmailCheck = value;
            }
        }

        //spahariya
        //added by swati MITS # 36930
        /// <summary>
        /// Gets/Sets Mail default recipient list
        /// </summary>
        public string MailUserRecipientList
        {
            get
            {
                return m_sMailUserRecipient;
            }
            set
            {
                m_sMailUserRecipient = value;
            }
        }
        //end by swati
		/// <summary>
		/// Template id
		/// </summary>
		public long FormId 
		{
			
			get
			{
				return m_lFormId;
			}
			set
			{
				TemplateField objTemplateField=null;
				TemplatePerm objTemplatePerm=null;
				IDictionaryEnumerator objEnum=null;
				try
				{
					if (m_lFormId!=value)
					{
						m_lFormId=value;
						objEnum=m_objColFields.GetEnumerator();
						while (objEnum.MoveNext())
						{
							objTemplateField=(TemplateField)objEnum.Value;
							objTemplateField.FormId=m_lFormId;
						}
						for (int i=0;i<m_arrlstColPerms.Count;i++)
						{
							objTemplatePerm=(TemplatePerm)m_arrlstColPerms[i];
							objTemplatePerm.FormId=m_lFormId;
						}
					}
				}	
				catch (Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("Template.FormId.SetError",m_iClientId),p_objException);
				}
				finally
				{
					objTemplateField=null;
					objTemplatePerm=null;
					objEnum=null;

				}
			}		
		}
		/// <summary>
		/// Returns the File contents as MemoryStream
		/// BSB 02.24.2006 Modified to be a Pass-Thru to FormFileContent.
		/// Crazy Maintenance head-ache to keep the two versions in sync.
		/// </summary>
		public MemoryStream FormFileContentinMemory 
		{
			get
			{
				//BSB 02.24.2006 Bad Idea to Maintain two copies of FormFileContent Property implementation.
				// Just consume a single one and reformat as "MemoryStream" here.
				string sContent=this.FormFileContent;
				byte[] bytes = Encoding.Default.GetBytes(sContent);
				MemoryStream ret = new MemoryStream();
				ret.Write(bytes,0,bytes.Length);
				return ret;
			}
		}
        private StorageType GetStorageType(long p_iStorageType)
        {
            switch (p_iStorageType)
            {
                case 0:
                    return StorageType.FileSystemStorage;
                case 1:
                    return StorageType.DatabaseStorage;
                case 2:
                    return StorageType.S3BucketStorage;
                default:
                    return StorageType.FileSystemStorage;
            }
        }
        /// <summary>
        /// Returns the file as a base64 string.
        /// </summary>
		public string FormFileContent 
		{
			get
			{
				string sFileName="";
				string sAcrosoftPath = "";
                string sMediaViewPath = "";
                StorageType eStorageType = GetStorageType(m_iDBStorageType);
                FileStorageManager objFileStorageManager = new FileStorageManager(eStorageType, m_sDocStorageDSN,this.m_iClientId);
				DbConnection objConn=null;
				bool blnExists=false;
				RMConfigurator objRMConfig=new RMConfigurator();
				SysSettings objSettings = null;
				Acrosoft objAcrosoft = null;
                MediaView objMediaView = null;
				string sAppExcpXml = "";
				try
				{
					//08/10/2006.. Raman Bhatia : If Acrosoft is enabled then legacy document management is not used
					//Instead fetch file from Acrosoft and use its contents
					
					objSettings = new SysSettings(m_sConnectionString,m_iClientId);
                    if (objSettings.UseAcrosoftInterface == false && objSettings.UseMediaViewInterface == false)
					{
                            
						if (m_iDBStorageType == 1)
						{
							sFileName="";
							objFileStorageManager.FileExists(this.FormFileName,out blnExists);
							if (blnExists)
							{
								//objFileStorageManager.RetrieveFile(this.FormFileName,Manager.TemplatePath +this.FormFileName,out sFileName);
                                //Change by kuladeep for mits:29824 Start
                                //objFileStorageManager.RetrieveFile(this.FormFileName,"",out sFileName);
                                objFileStorageManager.RetrieveFile(this.FormFileName, "", m_userLogin.LoginName, out sFileName);
                                //Change by kuladeep for mits:29824 End
							}
						}
					}
					else if(objSettings.UseAcrosoftInterface)
					{
                        sAcrosoftPath = String.Format("{0}\\MailMerge\\", m_UserDataPath);
						int iRetCd = 0;
						sAppExcpXml = "";

						//Fetching rtf and/or doc from Acrosoft and putting it in the temperory path
						objAcrosoft = new Acrosoft(m_iClientId);//dvatsa-cloud
                        //rsolanki2 :  start updates for MCM mits 19200 
                        Boolean bSuccess = false;
                        string sTempAcrosoftUserId = m_userLogin.LoginName;
                        string sTempAcrosoftPassword = m_userLogin.Password;

                        if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                        {
                            if (AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName] != null
                                && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].RmxUser))
                            {
                                sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftUserId;
                                sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftPassword;
                            }
                            else
                            {
                                sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                                sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                            }
                        }


                        iRetCd = objAcrosoft.FetchAndWriteGeneralFolderDocument(sTempAcrosoftUserId, sTempAcrosoftPassword,
                            this.FormFileName, "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, AcrosoftSection.GeneralFolderFriendlyName,
                            sAcrosoftPath + this.FormFileName, out sAppExcpXml);

                        //gagnihotri R5 MCM changes
                        //objAcrosoft.FetchAndWriteGeneralFolderDocument(m_userLogin.LoginName , m_userLogin.Password , this.FormFileName.Replace(".rtf", ".doc") , "Templates" , objRMConfig.GetValue("AcrosoftGeneralStorageTypeKey") , objRMConfig.GetValue("GeneralFolderFriendlyName") , sAcrosoftPath + this.FormFileName.Replace(".rtf", ".doc") , out sAppExcpXml);
                        objAcrosoft.FetchAndWriteGeneralFolderDocument(sTempAcrosoftUserId, sTempAcrosoftPassword,
                            this.FormFileName.Replace(".rtf", ".doc"), "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey,
                            AcrosoftSection.GeneralFolderFriendlyName, sAcrosoftPath + this.FormFileName.Replace(".rtf", ".doc"), out sAppExcpXml);

                        //rsolanki2 :  end updates for MCM mits 19200 



                        ////gagnihotri R5 MCM changes
                        

                        ////iRetCd = objAcrosoft.FetchAndWriteGeneralFolderDocument(m_userLogin.LoginName , m_userLogin.Password , this.FormFileName , "Templates" , objRMConfig.GetValue("AcrosoftGeneralStorageTypeKey") , objRMConfig.GetValue("GeneralFolderFriendlyName") , sAcrosoftPath + this.FormFileName , out sAppExcpXml);
                        //iRetCd = objAcrosoft.FetchAndWriteGeneralFolderDocument(m_userLogin.LoginName, m_userLogin.Password,
                        //    this.FormFileName, "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, AcrosoftSection.GeneralFolderFriendlyName, 
                        //    sAcrosoftPath + this.FormFileName, out sAppExcpXml);
						
                        ////gagnihotri R5 MCM changes
                        ////objAcrosoft.FetchAndWriteGeneralFolderDocument(m_userLogin.LoginName , m_userLogin.Password , this.FormFileName.Replace(".rtf", ".doc") , "Templates" , objRMConfig.GetValue("AcrosoftGeneralStorageTypeKey") , objRMConfig.GetValue("GeneralFolderFriendlyName") , sAcrosoftPath + this.FormFileName.Replace(".rtf", ".doc") , out sAppExcpXml);
                        //objAcrosoft.FetchAndWriteGeneralFolderDocument(m_userLogin.LoginName, m_userLogin.Password,
                        //    this.FormFileName.Replace(".rtf", ".doc"), "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, 
                        //    AcrosoftSection.GeneralFolderFriendlyName, sAcrosoftPath + this.FormFileName.Replace(".rtf", ".doc"), out sAppExcpXml);
							
					} // else
                    else if (objSettings.UseMediaViewInterface)
                    {
                        sMediaViewPath = String.Format("{0}\\MailMerge\\", m_UserDataPath);
                        sAppExcpXml = "";
                        objMediaView = new MediaView();

                        string sFormFileName = this.FormFileName;
                        objMediaView.MMGetDocumentByName("MailMergeTemplates", "Misc", ref sFormFileName, ref sMediaViewPath);
                        string sNewName = sFormFileName.Replace(".rtf", ".doc");
                        bool bResponse = objMediaView.MMGetDocumentByName("MailMergeTemplates", "Misc", ref sNewName, ref sMediaViewPath);
                        

                        
                    }

					//TODO : Understand the following code and see is any changes are needed in this code for Acrosoft	
					if (!this.Prefab.Trim().Equals(""))
						{
                            string sPath = m_UserDataPath;
							if (File.Exists(sPath +"\\MailMerge\\template\\"+ this.Prefab))
							{
                                using (FileStream fs = new FileStream(sPath + "\\MailMerge\\template\\" + this.Prefab, FileMode.Open, FileAccess.Read))
                                {
                                    using (BinaryReader br = new BinaryReader(fs))
                                    {
                                        m_sFormFileContent = Convert.ToBase64String(br.ReadBytes((int)fs.Length));
                                    }
                                }
							}
							else 
							{
								bool bExists=false;
								objFileStorageManager.FileExists(this.Prefab,out bExists);
								if (bExists)
								{
									sFileName="";
                                    //Change by kuladeep for mits:29824 Start
                                    //objFileStorageManager.RetrieveFile(this.Prefab,Manager.TemplatePath+this.Prefab,out sFileName);
                                    objFileStorageManager.RetrieveFile(this.Prefab, Manager.TemplatePath + this.Prefab, m_userLogin.LoginName, out sFileName);
                                    //Change by kuladeep for mits:29824 End
                               
                                    using (FileStream fs = new FileStream(sFileName, FileMode.Open, FileAccess.Read))
                                    {
                                        using (BinaryReader br = new BinaryReader(fs))
                                        {
                                            m_sFormFileContent = Convert.ToBase64String(br.ReadBytes((int)fs.Length));
                                        }
                                    }
								}
							}
						}
						else
						{
							string sTempPath = "";
							if(objSettings.UseAcrosoftInterface==false && objSettings.UseMediaViewInterface == false)
							{
								sTempPath = Manager.TemplatePath;
							} // if
							else if(objSettings.UseAcrosoftInterface)
							{
								sTempPath = sAcrosoftPath;
							} // else
                            else if (objSettings.UseMediaViewInterface)
                            {
                                sTempPath = sMediaViewPath;
                            }

							if (File.Exists(sTempPath + this.FormFileName))
							{
                                using (FileStream fs = new FileStream(sTempPath + this.FormFileName, FileMode.Open, FileAccess.Read))
                                {
                                    using (BinaryReader br = new BinaryReader(fs))
                                    {
                                        m_sFormFileContent = Convert.ToBase64String(br.ReadBytes((int)fs.Length));
                                    }
                                }
							}
							else
							{
								bool bExists=false;
								objFileStorageManager.FileExists(this.FormFileName,out bExists);
								if (bExists)
								{
									sFileName="";
                                    //Change by kuladeep for mits:29824 Start
                                    //objFileStorageManager.RetrieveFile(this.FormFileName,"",out sFileName);
                                    objFileStorageManager.RetrieveFile(this.FormFileName, "", m_userLogin.LoginName, out sFileName);
                                    //Change by kuladeep for mits:29824 End
                                
                                    using (FileStream fs = new FileStream(sFileName, FileMode.Open, FileAccess.Read))
                                    {
                                        using (BinaryReader br = new BinaryReader(fs))
                                        {
                                            m_sFormFileContent = Convert.ToBase64String(br.ReadBytes((int)fs.Length));
                                        }
                                    }
								}
							}
						}
					
					return m_sFormFileContent;
				}
				catch (FileNotFoundException p_objException)
				{
					throw new RMAppException("Template.FormFileContent.FileNotFoundError",p_objException);
				}
				catch (IOException p_objException)
				{
					throw new RMAppException("Template.FormFileContent.FileInputOutputError",p_objException);
				}
				catch (Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("Template.FormFileContent.Error",m_iClientId),p_objException);
				}
				finally
				{
					if (objConn!=null)
					{
                        objConn.Dispose();
					}
				}
			}
			set
			{
				m_sFormFileContent=value;
			}
		}

        /// <summary>
        /// Returns the file as a base64 string.
        /// </summary>
        public string HeaderFileContent
        {
            get
            {
                string sFileName = "";
                string sAcrosoftPath = "";
                string sMediaViewPath = "";
                string headerFileName = GetHeaderFileName(this.FormFileName);//33465 - nkaranam2
                StorageType eStorageType = GetStorageType(m_iDBStorageType);
                FileStorageManager objFileStorageManager = new FileStorageManager(eStorageType, m_sDocStorageDSN,m_iClientId);
                DbConnection objConn = null;
                bool blnExists = false;
                RMConfigurator objRMConfig = new RMConfigurator();
                SysSettings objSettings = null;
                Acrosoft objAcrosoft = null;
                string sAppExcpXml = "";
                MediaView objMediaView = null;
                try
                {
                    //08/10/2006.. Raman Bhatia : If Acrosoft is enabled then legacy document management is not used
                    //Instead fetch file from Acrosoft and use its contents

                    objSettings = new SysSettings(m_sConnectionString,m_iClientId);
                    if (objSettings.UseAcrosoftInterface == false && objSettings.UseMediaViewInterface == false)
                    {

                        if (m_iDBStorageType == 1)
                        {
                            sFileName = "";
                            objFileStorageManager.FileExists(headerFileName, out blnExists);
                            if (blnExists)
                            {
                                //Change by kuladeep for mits:29824 Start
                                //objFileStorageManager.RetrieveFile(headerFileName, "", out sFileName);
                                objFileStorageManager.RetrieveFile(headerFileName, "", m_userLogin.LoginName, out sFileName);
                                //Change by kuladeep for mits:29824 End
                            }
                        }
                    }
                    else if(objSettings.UseAcrosoftInterface)
                    {
                        sAcrosoftPath = String.Format("{0}\\MailMerge\\", m_UserDataPath);
                        int iRetCd = 0;
                        sAppExcpXml = "";

                        //Fetching rtf and/or doc from Acrosoft and putting it in the temperory path
                        objAcrosoft = new Acrosoft(m_iClientId);//dvatsa-cloud

                        //rsolanki2 :  start updates for MCM mits 19200 
                        Boolean bSuccess = false;
                        string sTempAcrosoftUserId = m_userLogin.LoginName;
                        string sTempAcrosoftPassword = m_userLogin.Password;

                        if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                        {
                            if (AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName] != null
                                && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].RmxUser))
                            {
                                sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftUserId;
                                sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftPassword;
                            }
                            else
                            {
                                sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                                sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                            }
                        }

                        //gagnihotri R5 MCM changes
                        //iRetCd = objAcrosoft.FetchAndWriteGeneralFolderDocument(m_userLogin.LoginName, m_userLogin.Password, headerFileName, "Templates", objRMConfig.GetValue("AcrosoftGeneralStorageTypeKey"), objRMConfig.GetValue("GeneralFolderFriendlyName"), sAcrosoftPath + headerFileName, out sAppExcpXml);
                        iRetCd = objAcrosoft.FetchAndWriteGeneralFolderDocument(sTempAcrosoftUserId, sTempAcrosoftPassword,
                            headerFileName, "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, AcrosoftSection.GeneralFolderFriendlyName,
                            sAcrosoftPath + headerFileName, out sAppExcpXml);

                        //gagnihotri R5 MCM changes
                        //objAcrosoft.FetchAndWriteGeneralFolderDocument(m_userLogin.LoginName, m_userLogin.Password, headerFileName, "Templates", objRMConfig.GetValue("AcrosoftGeneralStorageTypeKey"), objRMConfig.GetValue("GeneralFolderFriendlyName"), sAcrosoftPath + headerFileName, out sAppExcpXml);
                        objAcrosoft.FetchAndWriteGeneralFolderDocument(sTempAcrosoftUserId, sTempAcrosoftPassword, 
                            headerFileName, "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, 
                            AcrosoftSection.GeneralFolderFriendlyName, sAcrosoftPath + headerFileName, out sAppExcpXml);

                        //rsolanki2 :  end updates for MCM mits 19200 
                    } // else
                    else if (objSettings.UseMediaViewInterface)
                    {
                        sMediaViewPath = String.Format("{0}\\MailMerge\\", m_UserDataPath);
                        sAppExcpXml = "";
                        objMediaView = new MediaView();

                        string sFormFileName = headerFileName;
                        bool bResponse = objMediaView.MMGetDocumentByName("MailMergeTemplates", "Misc", ref headerFileName, ref sMediaViewPath);
                        
                    }

                    //TODO : Understand the following code and see is any changes are needed in this code for Acrosoft	
                    if (!this.Prefab.Trim().Equals(""))
                    {
                        string sPath = m_UserDataPath;
                        if (File.Exists(sPath + "\\MailMerge\\template\\" + this.Prefab))
                        {
                            using (FileStream fs = new FileStream(sPath + "\\MailMerge\\template\\" + this.Prefab, FileMode.Open, FileAccess.Read))
                            {
                                using (BinaryReader br = new BinaryReader(fs))
                                {
                                    m_sFormFileContent = Convert.ToBase64String(br.ReadBytes((int)fs.Length));
                                }
                            }
                        }
                        else
                        {
                            bool bExists = false;
                            objFileStorageManager.FileExists(this.Prefab, out bExists);
                            if (bExists)
                            {
                                sFileName = "";
                                //Change by kuladeep for mits:29824 Start
                                //objFileStorageManager.RetrieveFile(this.Prefab, Manager.TemplatePath + this.Prefab, out sFileName);
                                objFileStorageManager.RetrieveFile(this.Prefab, Manager.TemplatePath + this.Prefab, m_userLogin.LoginName, out sFileName);
                                //Change by kuladeep for mits:29824 End
                            
                                using (FileStream fs = new FileStream(sFileName, FileMode.Open, FileAccess.Read))
                                {
                                    using (BinaryReader br = new BinaryReader(fs))
                                    {
                                        m_sFormFileContent = Convert.ToBase64String(br.ReadBytes((int)fs.Length));
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        string sTempPath = "";
                        if (objSettings.UseAcrosoftInterface == false && objSettings.UseMediaViewInterface == false)
                        {
                            sTempPath = Manager.TemplatePath;
                        } // if
                        else if(objSettings.UseAcrosoftInterface)
                        {
                            sTempPath = sAcrosoftPath;
                        } // else
                        else if (objSettings.UseMediaViewInterface)
                        {
                            sTempPath = sMediaViewPath;
                        }


                        if (File.Exists(sTempPath + headerFileName))
                        {
                            using (FileStream fs = new FileStream(sTempPath + headerFileName, FileMode.Open, FileAccess.Read))
                            {
                                using (BinaryReader br = new BinaryReader(fs))
                                {
                                    m_sFormFileContent = Convert.ToBase64String(br.ReadBytes((int)fs.Length));
                                }
                            }
                        }
                        else
                        {
                            bool bExists = false;
                            objFileStorageManager.FileExists(headerFileName, out bExists);
                            if (bExists)
                            {
                                sFileName = "";
                                //Change by kuladeep for mits:29824 Start
                                //objFileStorageManager.RetrieveFile(headerFileName, "", out sFileName);
                                objFileStorageManager.RetrieveFile(headerFileName, "", m_userLogin.LoginName, out sFileName);
                                //Change by kuladeep for mits:29824 End
                     
                                using (FileStream fs = new FileStream(sFileName, FileMode.Open, FileAccess.Read))
                                {
                                    using (BinaryReader br = new BinaryReader(fs))
                                    {
                                        m_sFormFileContent = Convert.ToBase64String(br.ReadBytes((int)fs.Length));
                                    }
                                }
                            }
                        }
                    }

                    return m_sFormFileContent;
                }
                catch (FileNotFoundException p_objException)
                {
                    throw new RMAppException("Template.FormFileContent.FileNotFoundError", p_objException);
                }
                catch (IOException p_objException)
                {
                    throw new RMAppException("Template.FormFileContent.FileInputOutputError", p_objException);
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("Template.FormFileContent.Error",m_iClientId), p_objException);
                }
                finally
                {
                    if (objConn != null)
                    {
                        objConn.Dispose();
                    }
                }
            }
            set
            {
                m_sFormFileContent = value;
            }
        }



		/// <summary>
		/// String containing available template name
		/// </summary>
		public string Prefab 
		{
			get
			{
				return m_sPrefab;
			}
			set
			{
				m_sPrefab=value;
			}
		}
		/// <summary>
		/// Manager reference
		/// </summary>
		public IManager Manager
		{
			get
			{
				return m_objManager;
			}
			set
			{
				m_objManager=value;
			}
		}
		/// <summary>
		/// Fields in a template
		/// </summary>
		public SortedList Fields
		{
			get
			{
				return m_objColFields;
			}
		}
		#endregion

		#region Template class constructor
		/// <summary>
		/// Template class default constructor
		/// </summary>
        public Template(int p_iClientId) { m_iClientId = p_iClientId; }
		/// <summary>
		/// Template class Overloaded constructor
		/// </summary>
		/// <param name="p_sConnectString">Connection string to the database</param>
		/*
		public Template(string p_sConnectString)
		{
			m_objColFields=new SortedList();
			m_arrlstColPerms=new ArrayList();
			m_sConnectionString=p_sConnectString;
			m_arrlstAvailAdminTables=new ArrayList();
		}
		public Template(string p_sConnectString,bool p_bDbStorage,string p_sDocStorageDSN)
		{
			m_bDBStorage=p_bDbStorage;
			m_sDocStorageDSN=p_sDocStorageDSN;
			m_objColFields=new SortedList();
			m_arrlstColPerms=new ArrayList();
			m_sConnectionString=p_sConnectString;
			m_arrlstAvailAdminTables=new ArrayList();
		}
		*/
        public Template(UserLogin p_userLogin, int p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_objColFields = new SortedList();
            m_arrlstColPerms = new ArrayList();
            m_userLogin = p_userLogin;
            m_sConnectionString = p_userLogin.objRiskmasterDatabase.ConnectionString;
            m_arrlstAvailAdminTables = new ArrayList();
        }
		public Template(UserLogin p_userLogin,long p_iDbStorageType,string p_sDocStorageDSN,int p_iClientId)
		{
            m_iClientId = p_iClientId;
            m_iDBStorageType = p_iDbStorageType;
			m_sDocStorageDSN=p_sDocStorageDSN;
			m_objColFields=new SortedList();
			m_arrlstColPerms=new ArrayList();
			m_userLogin = p_userLogin;
			m_sConnectionString=m_userLogin.objRiskmasterDatabase.ConnectionString;
			m_arrlstAvailAdminTables=new ArrayList();
		}
		#endregion

		#region Template class destructor
		/// <summary>
		/// Template class destructor
		/// </summary>
		~Template()
		{
			m_sFormName=null;
			m_sFormDesc=null;
			m_sTableQualify=null; 
			m_sFormFileName=null; 
			m_sFormFileContent=null;
			m_objColFields=null;
			m_arrlstColPerms=null;
			m_arrlstAvailAdminTables=null;
			m_arrlstAvailFields=null;
			m_sPrefab=null;
            m_sCatName=null;
            m_objManager=null;
			m_sConnectionString=null;
			m_sSecureConnectionString=null;
		}
		#endregion

		#region Functions to load Template information
		/// Name		: Load
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function loads the Template data
		/// </summary>
		/// <param name="p_lFormID">Template id for which to load data</param>
		public void Load(long p_lFormID)
		{
			try
			{
				if (!LoadMergeFormTableData(p_lFormID))
				{
					return;
				}
				LoadMergeFormDefTableData(p_lFormID); 
				LoadMergeFormPermTableData(p_lFormID);
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.Load.Error",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This function loads merge form data
		/// </summary>
		/// <param name="p_lFormID">Merge form id</param>
		/// <returns>Loading data successful or not </returns>
		private bool LoadMergeFormTableData(long p_lFormID)
		{
			string sSql="";
			DbReader objReader=null;
			try
			{
                sSql = "SELECT FORM_ID,FORM_NAME,FORM_DESC,FORM_FILE_NAME, MERGE_FORM.CAT_ID, MERGE_CAT.CAT_NAME, TABLE_QUALIFY, LINE_OF_BUS_CODE, STATE_ID, MERGE_DOC_TYPE_CODE, MERGE_FORMAT_TYPE_CODE, SEND_MAIL_FLAG, DESIGNATED_RECIPIENT FROM MERGE_FORM,MERGE_CAT WHERE MERGE_FORM.FORM_ID=" + p_lFormID.ToString() + " AND MERGE_FORM.CAT_ID = MERGE_CAT.CAT_ID";
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
				if (objReader.Read())
				{
					m_lFormId = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue( "FORM_ID"))); 
					m_sFormName = Conversion.ConvertObjToStr(objReader.GetValue("FORM_NAME")); 
					m_sFormDesc = Conversion.ConvertObjToStr(objReader.GetValue("FORM_DESC")); 
					m_sFormFileName = Conversion.ConvertObjToStr(objReader.GetValue("FORM_FILE_NAME")); 
					m_lCatId = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue( "CAT_ID"))); 
					m_sCatName=Conversion.ConvertObjToStr(objReader.GetValue("CAT_NAME")).ToLower(); 
					m_sTableQualify=Conversion.ConvertObjToStr(objReader.GetValue("TABLE_QUALIFY")).ToLower();
                    m_lLOB = Conversion.ConvertObjToInt64(objReader.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
                    m_lStateId = Conversion.ConvertObjToInt64(objReader.GetValue("STATE_ID"), m_iClientId);
                    m_lMergeDocTypeCode = Conversion.ConvertObjToInt64(objReader.GetValue("MERGE_DOC_TYPE_CODE"), m_iClientId);
                    m_lMergeFormatTypeCode = Conversion.ConvertObjToInt64(objReader.GetValue("MERGE_FORMAT_TYPE_CODE"), m_iClientId);
                    m_lMailRecipient = Conversion.ConvertObjToInt64(objReader.GetValue("DESIGNATED_RECIPIENT"), m_iClientId);
                    m_lEmailCheck = Conversion.ConvertObjToInt64(objReader.GetValue("SEND_MAIL_FLAG"), m_iClientId);
				}
				else
				{
					return false;
				}
				return true;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.LoadMergeFormTableData.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}
		/// <summary>
		/// This function loads merge form field data
		/// </summary>
		/// <param name="p_lFormID">Merge form id</param>
		/// <returns>Loading data successful or not </returns>
		private void LoadMergeFormDefTableData(long p_lFormID )
		{
			string sSql="";
			DbReader objReader=null;
			long lData=0;
			TemplateField objTemplateField=null;
			bool bCanRead=false;
			bool bLongCode=false;
			int iCount=1;
			try
			{
                //Shruti for 11580
                for (int i = m_objColFields.Count-1; i >= 0; i--)
                {
                    m_objColFields.RemoveAt(i);
                }
                //Shruti for 11580 ends
				sSql ="SELECT FORM_ID,FIELD_ID, ITEM_TYPE, SEQ_NUM, SUPP_FIELD_FLAG, MERGE_PARAM FROM MERGE_FORM_DEF WHERE FORM_ID=" + p_lFormID.ToString()  +" ORDER BY SEQ_NUM";
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
				bCanRead=objReader.Read();
				while (bCanRead)
				{
					objTemplateField=new TemplateField(m_iClientId);//rkaur27
					objTemplateField.m_sConnectionString=m_sConnectionString;	
					objTemplateField.FormId=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue( "FORM_ID"))); 
					objTemplateField.ItemType=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue( "ITEM_TYPE"))); 
					objTemplateField.SeqNum=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue( "SEQ_NUM"))); 
					objTemplateField.SuppFieldFlag=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue( "SUPP_FIELD_FLAG"))); 
					objTemplateField.Param=Conversion.ConvertObjToStr(objReader.GetValue("MERGE_PARAM")); 
					objTemplateField.LongCodeFlag=bLongCode;
					bLongCode=false;
					//to see
					lData=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue( "FIELD_ID")));
                    if (Conversion.ConvertLongToBool(objTemplateField.SuppFieldFlag, m_iClientId))
					{
						objTemplateField.FieldId=(1001 * 65536)+lData;
					}
					else
					{
						objTemplateField.FieldId=lData;
					}
					//to see
                    objTemplateField.WordFieldName = Generic.WordSafeFieldName(objTemplateField.FieldDesc, m_iClientId);
					if (m_objColFields.ContainsKey(objTemplateField.WordFieldName))
					{
                        //iCount=1;
                        //objTemplateField.WordFieldName=objTemplateField.WordFieldName+iCount.ToString();
                        //while (m_objColFields.ContainsKey(objTemplateField.WordFieldName))
                        //{
                        //    objTemplateField.WordFieldName=objTemplateField.WordFieldName+iCount.ToString();
                        //    iCount++;
                        //}
                        //m_objColFields.Add(objTemplateField.WordFieldName,objTemplateField);
						
					}
					else
					{
                        //PSARIN2 7/19: MITS 28807  Missouri Highways - Mail Merge Issue 
                        //m_objColFields.Add(objTemplateField.WordFieldName, objTemplateField);
                        m_objColFields.Add(objTemplateField.WordFieldName.ToUpper(), objTemplateField);
					}
					if (!objTemplateField.LongCodeFlag)
					{
                        if (!Conversion.ConvertLongToBool(objTemplateField.SuppFieldFlag, m_iClientId))
						{
							if ((objTemplateField.FieldType==3) || (objTemplateField.FieldType==6))
							{
								bLongCode=true;
							}
							else
							{
								bLongCode=false;
							}
						}
						else
						{
							if ((objTemplateField.FieldType==6) || (objTemplateField.FieldType==9))
							{
								bLongCode=true;
							}
							else
							{
								bLongCode=false;
							}
						}
					}
					else
					{
						bLongCode=false;
					}
					if (!bLongCode)
					{
						bCanRead=objReader.Read();
					}
				}
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.LoadMergeFormDefTableData.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}
		/// <summary>
		/// This function loads merge form permission data
		/// </summary>
		/// <param name="p_lFormID">Merge form id</param>
		/// <returns>Loading data successful or not </returns>
		private void LoadMergeFormPermTableData(long p_lFormID)
		{
			string sSql="";
			DbReader objReader=null;
			TemplatePerm objTemplatePerm=null;
			int iTemp=0;
			string sInClause="";
			try
			{
				sSql = "SELECT FORM_ID, MERGE_FORM_PERM.GROUP_ID, GROUP_NAME FROM MERGE_FORM_PERM, USER_GROUPS WHERE FORM_ID=" + p_lFormID.ToString() + " AND MERGE_FORM_PERM.GROUP_ID=USER_GROUPS.GROUP_ID";
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
				while (objReader.Read())
				{
					objTemplatePerm=new TemplatePerm(m_iClientId);//rkaur27
					objTemplatePerm.FormId=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue( "FORM_ID"))); 
					objTemplatePerm.GroupId=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue( "GROUP_ID"))); 
					objTemplatePerm.DisplayName=Conversion.ConvertObjToStr(objReader.GetValue("GROUP_NAME")).ToLower(); 
					objTemplatePerm.UserId=0;
					objTemplatePerm.IsGroup=true;
					m_arrlstColPerms.Add(objTemplatePerm);
				}
				objReader.Close();
				sSql = "SELECT FORM_ID, USER_ID FROM MERGE_FORM_PERM WHERE FORM_ID=";
				sSql =sSql+ p_lFormID.ToString();
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
			
				while (objReader.Read())
				{
					objTemplatePerm=new TemplatePerm(m_iClientId);//rkaur27
					objTemplatePerm.FormId=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue( "FORM_ID"))); 
					objTemplatePerm.GroupId=0;
					objTemplatePerm.DisplayName="";
					objTemplatePerm.UserId=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue( "USER_ID")));
					objTemplatePerm.IsGroup=false;
					if (objTemplatePerm.UserId!=0)
					{
						m_arrlstColPerms.Add(objTemplatePerm);
						if (iTemp==0)
						{
							sInClause=sInClause+objTemplatePerm.UserId;
						}
						else
						{
							sInClause=sInClause+","+objTemplatePerm.UserId;
						}
						iTemp=1;	
					}
				}
				objReader.Close();
				if (sInClause.Length >0)
				{
					sSql = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME ";
					sSql += " FROM USER_DETAILS_TABLE,USER_TABLE";
					sSql += " WHERE USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID " ;
//					if (Conversion.ConvertLongToBool(m_lDSNID))
//					{
//						sSQL += "AND DSNID="+m_lDSNID;
//					}
				//	sSql = "SELECT USER_ID, LOGIN_NAME FROM USER_DETAILS_TABLE";
				//	sSql += " WHERE USER_ID IN (" + sInClause + ")";
					sSql += " AND USER_DETAILS_TABLE.USER_ID IN (" + sInClause + ")";
					objReader = DbFactory.GetDbReader(m_sSecureConnectionString,sSql);
					while (objReader.Read())
					{
						for (int i=0;i<m_arrlstColPerms.Count;i++)
						{
							objTemplatePerm=(TemplatePerm)m_arrlstColPerms[i];
                            if (objTemplatePerm.UserId == Conversion.ConvertObjToInt64(objReader.GetValue("USER_ID"), m_iClientId))
							{
								string sLName=Conversion.ConvertObjToStr(objReader.GetValue( "LAST_NAME"));
								string sFName=Conversion.ConvertObjToStr(objReader.GetValue( "FIRST_NAME"));
								string sName="";
								if (sFName=="")
								{
									sName=sLName;
								}
								else
								{
									sName=sLName+", "+sFName;
								}
//								objTemplatePerm.DisplayName=Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME"));
								objTemplatePerm.DisplayName=sName;
								break;
							}
						}
						
					}
				}
				objReader.Close();

			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.LoadMergeFormPermTableData.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}
		#endregion

		#region Functions to save Template information
		/// Name		: StoreMergeFormTableData
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function stores form data
		/// </summary>
		/// <param name="p_sFormDefXml">Xml string containing records to be stored</param>
		/// <param name="objCommand">Command object</param>
		/// <returns>Data saved successfully/not</returns>
		/// extra parameter p_sFormFileName added by jasbinder on 17th june 2005
		private bool StoreMergeFormTableData(string p_sFormXml,DbCommand objCommand, string p_sFormFileName)
		{
			string sSql="";
			int iExists=0;
            int iNextUID = 0;   //added by swati
			DbReader objReader=null;
			bool bRetValue=false;
			XmlDocument objDoc=null;
			XmlNode objRoot=null;
			XmlNode objNode=null;
			MergeManager objMergeManager=null;
            string[] sUser = null;  //added by swati
            int iFlag = 0;
			try
			{
				objDoc=new XmlDocument();
				objDoc.LoadXml(p_sFormXml);
				objRoot=objDoc.DocumentElement;
				objNode= objRoot.SelectSingleNode("//form");
				sSql="select count(FORM_ID) from MERGE_FORM where FORM_ID="+Utilities.FormatSqlFieldValue(this.FormId.ToString());
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
				if (objReader.Read())
				{
					iExists= Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReader.GetValue(0))); 
					objReader.Close();
				}
				this.CatId=Conversion.ConvertStrToLong(
					objNode.Attributes["catid"].Value);
				this.FormName=objNode.Attributes["formname"].Value;
				this.FormDesc=objNode.Attributes["formDesc"].Value;
				this.TableQualify=objNode.Attributes["tablequalify"].Value;
				//mail merge crawford enhancements..modified by Raman Bhatia
				
				objMergeManager = new MergeManager(m_iClientId);//rkaur27
				//setting LOB
                //MITS 9159
				if(this.CatId != objMergeManager.RMTableNameToCatId("CLAIM"))
					this.LOB = -1;
				else
					this.LOB = Conversion.ConvertStrToLong(objNode.Attributes["LOB"].Value);
				//setting StateId
				//Shruti for 9319
				if(objNode.Attributes["AllStatesSelected"].Value.ToLower() == "true")
					this.StateId = -1;
				else
					this.StateId = Conversion.ConvertStrToLong(objNode.Attributes["StateId"].Value);
                //spahariya MITS 28867 - start 06/26/2012
                if (objNode.Attributes["emailCheck"].Value.ToLower() == "true")
                    this.EmailCheck = -1;
                else
                    this.EmailCheck = 0;
                this.MailRecipient = Conversion.ConvertStrToLong(objNode.Attributes["mailRecipient"].Value);
                //spahariya - end
                //added by swati MTS # 36930
                this.MailUserRecipientList = objNode.Attributes["usertypeid"].Value;
                sUser = this.MailUserRecipientList.Split(',');
                //change end here by swati
				//setting MergeDocumentType
				this.MergeDocTypeCode = Conversion.ConvertStrToLong(objNode.Attributes["MergeDocumentType"].Value);
				//setting MergeDocumentFormat
				this.MergeFormatTypeCode = Conversion.ConvertStrToLong(objNode.Attributes["MergeDocumentFormat"].Value);

                //added by swati MITS # 36930
                if (!string.IsNullOrEmpty(this.MailUserRecipientList.ToString()))
                {
                sSql = "DELETE FROM FORM_RECIPIENT WHERE FORM_ID = " + Utilities.FormatSqlFieldValue(Conversion.ConvertObjToStr(this.FormId.ToString()));
                objCommand.CommandText = sSql;
                objCommand.ExecuteNonQuery();
                foreach (string s in sUser)
                {
                    if (s.Contains("*"))                    
                        iFlag = -1;                        
                    else
                        iFlag = 0;

                    sSql = string.Empty;
                    if (DbFactory.GetDatabaseType(m_sConnectionString) == eDatabaseType.DBMS_IS_ORACLE)
                    {
                        iNextUID = Utilities.GetNextUID(m_sConnectionString, "FORM_RECIPIENT", m_iClientId);
                        sSql = "INSERT INTO FORM_RECIPIENT(RECIPIENT_ROW_ID, FORM_ID, DESIGNATED_RECIPIENT_ID, IS_PRIMARY_CURRENT)";
                        sSql += " VALUES(" + iNextUID + ", " + Utilities.FormatSqlFieldValue(Conversion.ConvertObjToStr(this.FormId.ToString())) + ", " + s.Replace("*", "") + ", " + iFlag + ")";

                    }
                    else
                    {
                        sSql = "INSERT INTO FORM_RECIPIENT(FORM_ID, DESIGNATED_RECIPIENT_ID, IS_PRIMARY_CURRENT)";
                        sSql += " VALUES(" + Utilities.FormatSqlFieldValue(Conversion.ConvertObjToStr(this.FormId.ToString())) + ", " + s.Replace("*", "") + ", " + iFlag + ")";
                    } 
                    objCommand.CommandText = sSql;
                    objCommand.ExecuteNonQuery();
                    sSql = string.Empty;
                }
                }
                //change end here
				if (iExists==0)
				{
                    //pmittal5 Mits 13646 12/08/08
                    //sSql="INSERT INTO MERGE_FORM(FORM_ID, CAT_ID, FORM_NAME, FORM_DESC, TABLE_QUALIFY, FORM_FILE_NAME, LINE_OF_BUS_CODE, STATE_ID, MERGE_DOC_TYPE_CODE, MERGE_FORMAT_TYPE_CODE)";
                    //spahariya MITS 28867 added Email chek and default recipient
                    sSql = "INSERT INTO MERGE_FORM(FORM_ID, CAT_ID, FORM_NAME, FORM_DESC, TABLE_QUALIFY, FORM_FILE_NAME, LINE_OF_BUS_CODE, STATE_ID, MERGE_DOC_TYPE_CODE, MERGE_FORMAT_TYPE_CODE, MCM_TRANSFER_STATUS, SEND_MAIL_FLAG, DESIGNATED_RECIPIENT)";
					
                    //sSql+=" VALUES("+Utilities.FormatSqlFieldValue(Conversion.ConvertObjToStr(this.FormId.ToString()))+","+Utilities.FormatSqlFieldValue(objNode.Attributes["catid".Value)+","+Utilities.FormatSqlFieldValue(objNode.Attributes["formname"].Value)+","+Utilities.FormatSqlFieldValue(objNode.Attributes["formDesc"].Value)+","+Utilities.FormatSqlFieldValue(objNode.Attributes["tablequalify"].Value)+","+Utilities.FormatSqlFieldValue(this.FormFileName)+")";
					//changed by jasbinder on 17th june 2005
                    
                    //pmittal5 Mits 13646 12/08/08
					//sSql+=" VALUES("+Utilities.FormatSqlFieldValue(Conversion.ConvertObjToStr(this.FormId.ToString()))+","+Utilities.FormatSqlFieldValue(objNode.Attributes["catid"].Value)+","+Utilities.FormatSqlFieldValue(objNode.Attributes["formname"].Value)+","+Utilities.FormatSqlFieldValue(objNode.Attributes["formDesc"].Value)+","+Utilities.FormatSqlFieldValue(objNode.Attributes["tablequalify"].Value)+","+Utilities.FormatSqlFieldValue(p_sFormFileName)+","+Utilities.FormatSqlFieldValue(this.LOB.ToString())+","+Utilities.FormatSqlFieldValue(this.StateId.ToString())+","+Utilities.FormatSqlFieldValue(this.MergeDocTypeCode.ToString())+","+Utilities.FormatSqlFieldValue(this.MergeFormatTypeCode.ToString())+")";
                    sSql += " VALUES(" + Utilities.FormatSqlFieldValue(Conversion.ConvertObjToStr(this.FormId.ToString())) + "," + Utilities.FormatSqlFieldValue(objNode.Attributes["catid"].Value) + "," + Utilities.FormatSqlFieldValue(objNode.Attributes["formname"].Value) + "," + Utilities.FormatSqlFieldValue(objNode.Attributes["formDesc"].Value) + "," + Utilities.FormatSqlFieldValue(objNode.Attributes["tablequalify"].Value) + "," + Utilities.FormatSqlFieldValue(p_sFormFileName) + "," + Utilities.FormatSqlFieldValue(this.LOB.ToString()) + "," + Utilities.FormatSqlFieldValue(this.StateId.ToString()) + "," + Utilities.FormatSqlFieldValue(this.MergeDocTypeCode.ToString()) + "," + Utilities.FormatSqlFieldValue(this.MergeFormatTypeCode.ToString()) + ", 0 ," + Utilities.FormatSqlFieldValue(this.EmailCheck.ToString()) + "," + Utilities.FormatSqlFieldValue(this.MailRecipient.ToString()) + ")";
				}
				else
				{
					this.FormId=Conversion.ConvertStrToLong(
						objNode.Attributes["id"].Value);

                    //pmittal5 Mits 13646 12/08/08
                    //sSql="UPDATE MERGE_FORM set FORM_ID="+Utilities.FormatSqlFieldValue(objNode.Attributes["id"].Value)+",CAT_ID="+Utilities.FormatSqlFieldValue(objNode.Attributes["catid"].Value)+",FORM_NAME="+Utilities.FormatSqlFieldValue(objNode.Attributes["formname"].Value)+",FORM_DESC="+Utilities.FormatSqlFieldValue(objNode.Attributes["formDesc"].Value)+",TABLE_QUALIFY="+Utilities.FormatSqlFieldValue(objNode.Attributes["tablequalify"].Value)+",FORM_FILE_NAME="+Utilities.FormatSqlFieldValue(this.FormFileName)+",LINE_OF_BUS_CODE="+Utilities.FormatSqlFieldValue(this.LOB.ToString())+",STATE_ID="+Utilities.FormatSqlFieldValue(this.StateId.ToString())+",MERGE_DOC_TYPE_CODE="+Utilities.FormatSqlFieldValue(this.MergeDocTypeCode.ToString())+",MERGE_FORMAT_TYPE_CODE="+Utilities.FormatSqlFieldValue(this.MergeFormatTypeCode.ToString());
                    //spahariya MITS 28867 added Email chek and default recipient
                    //sSql = "UPDATE MERGE_FORM set FORM_ID=" + Utilities.FormatSqlFieldValue(objNode.Attributes["id"].Value) + ",CAT_ID=" + Utilities.FormatSqlFieldValue(objNode.Attributes["catid"].Value) + ",FORM_NAME=" + Utilities.FormatSqlFieldValue(objNode.Attributes["formname"].Value) + ",FORM_DESC=" + Utilities.FormatSqlFieldValue(objNode.Attributes["formDesc"].Value) + ",TABLE_QUALIFY=" + Utilities.FormatSqlFieldValue(objNode.Attributes["tablequalify"].Value) + ",FORM_FILE_NAME=" + Utilities.FormatSqlFieldValue(this.FormFileName) + ",LINE_OF_BUS_CODE=" + Utilities.FormatSqlFieldValue(this.LOB.ToString()) + ",STATE_ID=" + Utilities.FormatSqlFieldValue(this.StateId.ToString()) + ",MERGE_DOC_TYPE_CODE=" + Utilities.FormatSqlFieldValue(this.MergeDocTypeCode.ToString()) + ",MERGE_FORMAT_TYPE_CODE=" + Utilities.FormatSqlFieldValue(this.MergeFormatTypeCode.ToString()) + ",MCM_TRANSFER_STATUS = 0,SEND_MAIL_FLAG=" + Utilities.FormatSqlFieldValue(this.EmailCheck.ToString()) + ",DESIGNATED_RECIPIENT=" + Utilities.FormatSqlFieldValue(this.MailRecipient.ToString());
                    //33465 - nkaranam2
                    sSql = "UPDATE MERGE_FORM set FORM_ID=" + Utilities.FormatSqlFieldValue(objNode.Attributes["id"].Value) + ",CAT_ID=" + Utilities.FormatSqlFieldValue(objNode.Attributes["catid"].Value) + ",FORM_NAME=" + Utilities.FormatSqlFieldValue(objNode.Attributes["formname"].Value) + ",FORM_DESC=" + Utilities.FormatSqlFieldValue(objNode.Attributes["formDesc"].Value) + ",TABLE_QUALIFY=" + Utilities.FormatSqlFieldValue(objNode.Attributes["tablequalify"].Value) + ",FORM_FILE_NAME=" + Utilities.FormatSqlFieldValue(p_sFormFileName) + ",LINE_OF_BUS_CODE=" + Utilities.FormatSqlFieldValue(this.LOB.ToString()) + ",STATE_ID=" + Utilities.FormatSqlFieldValue(this.StateId.ToString()) + ",MERGE_DOC_TYPE_CODE=" + Utilities.FormatSqlFieldValue(this.MergeDocTypeCode.ToString()) + ",MERGE_FORMAT_TYPE_CODE=" + Utilities.FormatSqlFieldValue(this.MergeFormatTypeCode.ToString()) + ",MCM_TRANSFER_STATUS = 0,SEND_MAIL_FLAG=" + Utilities.FormatSqlFieldValue(this.EmailCheck.ToString()) + ",DESIGNATED_RECIPIENT=" + Utilities.FormatSqlFieldValue(this.MailRecipient.ToString());
					
                    sSql+=" where FORM_ID="+Utilities.FormatSqlFieldValue(Conversion.ConvertObjToStr(this.FormId.ToString()));
				}
                objCommand.CommandText=sSql;
				objCommand.ExecuteNonQuery();
				bRetValue=true;
			}
			catch (XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.StoreMergeFormTableData.XmlError",m_iClientId),p_objException);
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.StoreMergeFormTableData.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDoc=null;
				objNode=null;
				objRoot=null;
				objMergeManager=null;
				sSql=null;
			}
			return bRetValue;
		}
//overloaded method added by jasbinder
		private bool StoreMergeFormTableData(string p_sFormXml,DbCommand objCommand)
		{
			string sSql="";
			int iExists=0;
			DbReader objReader=null;
			bool bRetValue=false;
			XmlDocument objDoc=null;
			XmlNode objRoot=null;
			XmlNode objNode=null;
			MergeManager objMergeManager = null;
			try
			{
				objDoc=new XmlDocument();
				objDoc.LoadXml(p_sFormXml);
				objRoot=objDoc.DocumentElement;
				objNode= objRoot.SelectSingleNode("//form");
				sSql="select count(FORM_ID) from MERGE_FORM where FORM_ID="+Utilities.FormatSqlFieldValue(this.FormId.ToString());
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
				if (objReader.Read())
				{
					iExists= Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReader.GetValue(0))); 
					objReader.Close();
				}
				this.CatId=Conversion.ConvertStrToLong(
					objNode.Attributes["catid"].Value);
				this.FormName=objNode.Attributes["formname"].Value;
				this.FormDesc=objNode.Attributes["formDesc"].Value;
				this.TableQualify=objNode.Attributes["tablequalify"].Value;
				//mail merge crawford enhancements..modified by Raman Bhatia
				
				objMergeManager = new MergeManager(m_iClientId);//rkaur27
				//setting LOB
				if(this.CatId != objMergeManager.RMTableNameToCatId("CLAIM") && this.CatId != objMergeManager.RMTableNameToCatId("EVENT"))
					this.LOB = -1;
				else
					this.LOB = Conversion.ConvertStrToLong(objNode.Attributes["LOB"].Value);
				//setting StateId
				if(objNode.Attributes["AllStatesSelected"].Value == "true")
					this.StateId = -1;
				else
					this.StateId = Conversion.ConvertStrToLong(objNode.Attributes["StateId"].Value);
				//setting MergeDocumentType
				this.MergeDocTypeCode = Conversion.ConvertStrToLong(objNode.Attributes["MergeDocumentType"].Value);
				//setting MergeDocumentFormat
				this.MergeFormatTypeCode = Conversion.ConvertStrToLong(objNode.Attributes["MergeDocumentFormat"].Value);
                //spahariya MITS 28867 - start 06/26/2012
                if (objNode.Attributes["emailCheck"].Value.ToLower() == "true")
                    this.EmailCheck = -1;
                else
                    this.EmailCheck = 0;
                this.MailRecipient = Conversion.ConvertStrToLong(objNode.Attributes["mailRecipient"].Value);
                //spahariya - end
				if (iExists==0)
				{
					//sSql="INSERT INTO [MERGE_FORM]([FORM_ID], [CAT_ID], [FORM_NAME], [FORM_DESC], [TABLE_QUALIFY], [FORM_FILE_NAME])";
					//sSql+=" VALUES("+Utilities.FormatSqlFieldValue(Conversion.ConvertObjToStr(this.FormId.ToString()))+","+Utilities.FormatSqlFieldValue(objNode.Attributes["catid"].Value)+","+Utilities.FormatSqlFieldValue(objNode.Attributes["formname"].Value)+","+Utilities.FormatSqlFieldValue(objNode.Attributes["formDesc"].Value)+","+Utilities.FormatSqlFieldValue(objNode.Attributes["tablequalify"].Value)+","+Utilities.FormatSqlFieldValue(this.FormFileName)+")";

                    //pmittal5 Mits 13646 12/08/08
                    //sSql="INSERT INTO MERGE_FORM(FORM_ID, CAT_ID, FORM_NAME, FORM_DESC, TABLE_QUALIFY, FORM_FILE_NAME, LINE_OF_BUS_CODE, STATE_ID, MERGE_DOC_TYPE_CODE, MERGE_FORMAT_TYPE_CODE)";
                    //sSql+=" VALUES("+Utilities.FormatSqlFieldValue(Conversion.ConvertObjToStr(this.FormId.ToString()))+","+Utilities.FormatSqlFieldValue(objNode.Attributes["catid"].Value)+","+Utilities.FormatSqlFieldValue(objNode.Attributes["formname"].Value)+","+Utilities.FormatSqlFieldValue(objNode.Attributes["formDesc"].Value)+","+Utilities.FormatSqlFieldValue(objNode.Attributes["tablequalify"].Value)+","+Utilities.FormatSqlFieldValue(this.FormFileName)+","+Utilities.FormatSqlFieldValue(this.LOB.ToString())+","+Utilities.FormatSqlFieldValue(this.StateId.ToString())+","+Utilities.FormatSqlFieldValue(this.MergeDocTypeCode.ToString())+","+Utilities.FormatSqlFieldValue(this.MergeFormatTypeCode.ToString())+")";
                    sSql = "INSERT INTO MERGE_FORM(FORM_ID, CAT_ID, FORM_NAME, FORM_DESC, TABLE_QUALIFY, FORM_FILE_NAME, LINE_OF_BUS_CODE, STATE_ID, MERGE_DOC_TYPE_CODE, MERGE_FORMAT_TYPE_CODE, MCM_TRANSFER_STATUS, SEND_MAIL_FLAG, DESIGNATED_RECIPIENT)";
                    sSql += " VALUES(" + Utilities.FormatSqlFieldValue(Conversion.ConvertObjToStr(this.FormId.ToString())) + "," + Utilities.FormatSqlFieldValue(objNode.Attributes["catid"].Value) + "," + Utilities.FormatSqlFieldValue(objNode.Attributes["formname"].Value) + "," + Utilities.FormatSqlFieldValue(objNode.Attributes["formDesc"].Value) + "," + Utilities.FormatSqlFieldValue(objNode.Attributes["tablequalify"].Value) + "," + Utilities.FormatSqlFieldValue(this.FormFileName) + "," + Utilities.FormatSqlFieldValue(this.LOB.ToString()) + "," + Utilities.FormatSqlFieldValue(this.StateId.ToString()) + "," + Utilities.FormatSqlFieldValue(this.MergeDocTypeCode.ToString()) + "," + Utilities.FormatSqlFieldValue(this.MergeFormatTypeCode.ToString()) + ", 0 ," + Utilities.FormatSqlFieldValue(this.EmailCheck.ToString()) + "," + Utilities.FormatSqlFieldValue(this.MailRecipient.ToString()) + ")";				
				}
				else
				{
					this.FormId=Conversion.ConvertStrToLong(
						objNode.Attributes["id"].Value);
					//sSql="UPDATE MERGE_FORM set FORM_ID="+Utilities.FormatSqlFieldValue(objNode.Attributes["id"].Value)+",CAT_ID="+Utilities.FormatSqlFieldValue(objNode.Attributes["catid"].Value)+",FORM_NAME="+Utilities.FormatSqlFieldValue(objNode.Attributes["formname"].Value)+",FORM_DESC="+Utilities.FormatSqlFieldValue(objNode.Attributes["formDesc"].Value)+",TABLE_QUALIFY="+Utilities.FormatSqlFieldValue(objNode.Attributes["tablequalify"].Value)+",FORM_FILE_NAME="+Utilities.FormatSqlFieldValue(this.FormFileName);
					//sSql+=" where FORM_ID="+Utilities.FormatSqlFieldValue(Conversion.ConvertObjToStr(this.FormId.ToString()));
                    
                    //pmittal5 Mits 13646 12/08/08
                    //sSql="UPDATE MERGE_FORM set FORM_ID="+Utilities.FormatSqlFieldValue(objNode.Attributes["id"].Value)+",CAT_ID="+Utilities.FormatSqlFieldValue(objNode.Attributes["catid"].Value)+",FORM_NAME="+Utilities.FormatSqlFieldValue(objNode.Attributes["formname"].Value)+",FORM_DESC="+Utilities.FormatSqlFieldValue(objNode.Attributes["formDesc"].Value)+",TABLE_QUALIFY="+Utilities.FormatSqlFieldValue(objNode.Attributes["tablequalify"].Value)+",FORM_FILE_NAME="+Utilities.FormatSqlFieldValue(this.FormFileName)+",LINE_OF_BUS_CODE="+Utilities.FormatSqlFieldValue(this.LOB.ToString())+",STATE_ID="+Utilities.FormatSqlFieldValue(this.StateId.ToString())+",MERGE_DOC_TYPE_CODE="+Utilities.FormatSqlFieldValue(this.MergeDocTypeCode.ToString())+",MERGE_FORMAT_TYPE_CODE="+Utilities.FormatSqlFieldValue(this.MergeFormatTypeCode.ToString());
                    //spahariya MITS 28867 added Email chek and default recipient
                    sSql = "UPDATE MERGE_FORM set FORM_ID=" + Utilities.FormatSqlFieldValue(objNode.Attributes["id"].Value) + ",CAT_ID=" + Utilities.FormatSqlFieldValue(objNode.Attributes["catid"].Value) + ",FORM_NAME=" + Utilities.FormatSqlFieldValue(objNode.Attributes["formname"].Value) + ",FORM_DESC=" + Utilities.FormatSqlFieldValue(objNode.Attributes["formDesc"].Value) + ",TABLE_QUALIFY=" + Utilities.FormatSqlFieldValue(objNode.Attributes["tablequalify"].Value) + ",FORM_FILE_NAME=" + Utilities.FormatSqlFieldValue(this.FormFileName) + ",LINE_OF_BUS_CODE=" + Utilities.FormatSqlFieldValue(this.LOB.ToString()) + ",STATE_ID=" + Utilities.FormatSqlFieldValue(this.StateId.ToString()) + ",MERGE_DOC_TYPE_CODE=" + Utilities.FormatSqlFieldValue(this.MergeDocTypeCode.ToString()) + ",MERGE_FORMAT_TYPE_CODE=" + Utilities.FormatSqlFieldValue(this.MergeFormatTypeCode.ToString()) + ",MCM_TRANSFER_STATUS = 0,SEND_MAIL_FLAG=" + Utilities.FormatSqlFieldValue(this.EmailCheck.ToString()) + ",DESIGNATED_RECIPIENT=" + Utilities.FormatSqlFieldValue(this.MailRecipient.ToString());
					
                    sSql+=" where FORM_ID="+Utilities.FormatSqlFieldValue(Conversion.ConvertObjToStr(this.FormId.ToString()));
				}
				objCommand.CommandText=sSql;
				objCommand.ExecuteNonQuery();
				bRetValue=true;
			}
			catch (XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.StoreMergeFormTableData.XmlError",m_iClientId),p_objException);
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.StoreMergeFormTableData.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDoc=null;
				objNode=null;
				objRoot=null;
				objMergeManager=null;
				sSql=null;
			}
			return bRetValue;
		}
		/// Name		: StoreMergeFormDefTableData
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function stores template fields data
		/// </summary>
		/// <param name="p_sFormDefXml">Xml string containing records to be stored</param>
		/// <param name="objCommand">Command object</param>
		/// <returns>Returns true/false saving data successful/not</returns>
		public bool StoreMergeFormDefTableData(string p_sFormDefXml,DbCommand objCommand)
		{
			string sSql="";
			string sWhere="";
			DbReader objReader=null;
			//int iTemp=1; commented by jasbinder on june 20th 2005
			bool bRetValue=false;
			XmlDocument objDoc=null;
			XmlNode objRoot=null;
			XmlNodeList objNodeList=null;
			string sFieldID="";   // JP 09.08.2006
			string sParam="";     // JP 09.08.2006
            ArrayList arraySelectedMergeFields = null;
			try
			{
				sWhere = " WHERE FORM_ID =" +this.FormId.ToString();
				objDoc=new XmlDocument();
				objDoc.LoadXml(p_sFormDefXml);
				objRoot=objDoc.DocumentElement;
				objNodeList= objRoot.SelectNodes("//form");
				sSql = "DELETE FROM MERGE_FORM_DEF " +sWhere;
				objCommand.CommandText=sSql;
				objCommand.ExecuteNonQuery();
				foreach (XmlNode objTempNode in objNodeList)
				{

					string [] strArraySelectedMergeFields;										
					
					strArraySelectedMergeFields	=	objTempNode.Attributes["fieldid"].Value.ToString().Split(',');
					arraySelectedMergeFields = new ArrayList();
					for (int i=1;i<strArraySelectedMergeFields.Length;i++)
					{
						if ( !arraySelectedMergeFields.Contains(strArraySelectedMergeFields[i]))
						{
							arraySelectedMergeFields.Add(strArraySelectedMergeFields[i]);
							sSql="INSERT INTO MERGE_FORM_DEF(FORM_ID,FIELD_ID,ITEM_TYPE,SEQ_NUM,SUPP_FIELD_FLAG,MERGE_PARAM) ";
							//sSql+=" VALUES("+Utilities.FormatSqlFieldValue(this.FormId.ToString())+","+Utilities.FormatSqlFieldValue(objTempNode.Attributes["fieldid"].Value)+","+Utilities.FormatSqlFieldValue(objTempNode.Attributes["itemtype"].Value)+","+Utilities.FormatSqlFieldValue(iTemp.ToString())+","+Utilities.FormatSqlFieldValue(objTempNode.Attributes["supplierflag"].Value)+","+Utilities.FormatSqlFieldValue(objTempNode.Attributes["mergeparam"].Value)+")";  commented by jasbinder on june 20th 2005
							sParam = "";
							sFieldID = strArraySelectedMergeFields[i].ToString();   // JP 09.08.2006
							if (sFieldID.IndexOf("|") >= 0)
							{
								sParam = sFieldID.Substring(sFieldID.IndexOf("|") + 1);
								sFieldID = sFieldID.Substring(0, sFieldID.IndexOf("|"));
							}

							// sSql+=" VALUES("+Utilities.FormatSqlFieldValue(this.FormId.ToString())+","+Utilities.FormatSqlFieldValue(strArraySelectedMergeFields[i].ToString())+",1,"+Utilities.FormatSqlFieldValue(i.ToString())+","+Utilities.FormatSqlFieldValue(objTempNode.Attributes["supplierflag"].Value)+","+Utilities.FormatSqlFieldValue(objTempNode.Attributes["mergeparam"].Value)+")";
							sSql+=" VALUES("+Utilities.FormatSqlFieldValue(this.FormId.ToString())+","+Utilities.FormatSqlFieldValue(sFieldID)+",1,"+Utilities.FormatSqlFieldValue(i.ToString())+","+Utilities.FormatSqlFieldValue(objTempNode.Attributes["supplierflag"].Value)+","+Utilities.FormatSqlFieldValue(sParam)+")";
							//iTemp = iTemp + 1;  commented by jasbinder on june 20th 2005
							objCommand.CommandText=sSql;
							objCommand.ExecuteNonQuery();
						}
					}
				}
				
				bRetValue=true;
			}
			catch (XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.StoreMergeFormDefTableData.XmlError",m_iClientId),p_objException);
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.StoreMergeFormDefTableData.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDoc=null;
				objRoot=null;
				objNodeList=null;
				sSql=null;
				sWhere=null;
                arraySelectedMergeFields = null;
			}
			return bRetValue;
		}
		/// Name		: StoreMergeFormPermTableData
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function stores form permission data
		/// </summary>
		/// <param name="p_sFormDefXml">Xml string containing records to be stored</param>
		/// <param name="objCommand">Command object</param>
		/// <returns>Saving data successful/not</returns>
		public bool StoreMergeFormPermTableData(string p_sFormPermXml,DbCommand objCommand)
		{
			string sSql="";
			string sWhere="";
			DbReader objReader=null;
			bool bRetValue=false;
			XmlDocument objDoc=null;
			XmlNode objRoot=null;
			XmlNodeList objNodeList=null;
			int iTemp=0;
			try
			{
				objDoc=new XmlDocument();
				objDoc.LoadXml(p_sFormPermXml);
				objRoot=objDoc.DocumentElement;
				objNodeList= objRoot.SelectNodes("//form");

				sWhere = " WHERE FORM_ID =" +this.FormId.ToString();
				sSql = "DELETE FROM MERGE_FORM_PERM " +sWhere;
				objCommand.CommandText=sSql;
				objCommand.ExecuteNonQuery();
				foreach (XmlNode objTempNode in objNodeList)
				{
					//added by jasbinder on 16th june 2005
					
					string [] strArrayGroups;										
					string [] strArrayUsers;										
					
					
					strArrayGroups	=	objTempNode.Attributes["groupid"].Value.ToString().Split(',');
					strArrayUsers	=	objTempNode.Attributes["userid"].Value.ToString().Split(',');
					
					
					
					for (int i=1;i<strArrayGroups.Length;i++)
					{	
						//sSql+=" VALUES("+Utilities.FormatSqlFieldValue(this.FormId.ToString())+","+Utilities.FormatSqlFieldValue(objTempNode.Attributes["userid"].Value)+","+Utilities.FormatSqlFieldValue(objTempNode.Attributes["groupid"].Value)+")";	
						if (strArrayGroups[i].ToString() != "0")
						{
							sSql="INSERT INTO MERGE_FORM_PERM";
							sSql+=" VALUES("+Utilities.FormatSqlFieldValue(this.FormId.ToString())+","+0+","+Utilities.FormatSqlFieldValue(strArrayGroups[i].ToString())+")";	
							objCommand.CommandText=sSql;
							objCommand.ExecuteNonQuery();	
						}
					}
					sSql = "";
					for (int i=1;i<strArrayUsers.Length;i++)
					{	
						//sSql+=" VALUES("+Utilities.FormatSqlFieldValue(this.FormId.ToString())+","+Utilities.FormatSqlFieldValue(objTempNode.Attributes["userid"].Value)+","+Utilities.FormatSqlFieldValue(objTempNode.Attributes["groupid"].Value)+")";	
						if (strArrayUsers[i].ToString() != "0")
						{
							sSql="INSERT INTO MERGE_FORM_PERM";
							sSql+=" VALUES("+Utilities.FormatSqlFieldValue(this.FormId.ToString())+","+Utilities.FormatSqlFieldValue(strArrayUsers[i].ToString())+","+0+")";	
							objCommand.CommandText=sSql;
							objCommand.ExecuteNonQuery();	
						}
					}

				
					if (strArrayGroups[1].ToString()=="0" && strArrayUsers[1].ToString()=="0")
					{
						sSql="INSERT INTO MERGE_FORM_PERM";
						sSql+=" VALUES("+Utilities.FormatSqlFieldValue(this.FormId.ToString())+","+0+","+0+")";	
						objCommand.CommandText=sSql;
						objCommand.ExecuteNonQuery();
					}
				
					//till here
					
					iTemp=1;
				}
				if (iTemp==0)
				{
					sSql="INSERT INTO MERGE_FORM_PERM ";
					sSql+=" VALUES("+Utilities.FormatSqlFieldValue(this.FormId.ToString())+",0,0)";
					objCommand.CommandText=sSql;
					objCommand.ExecuteNonQuery();
				}
				bRetValue=true;
			}
			catch (XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.StoreMergeFormPermTableData.XmlError",m_iClientId),p_objException);
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.StoreMergeFormPermTableData.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDoc=null;
				objRoot=null;
				objNodeList=null;
				sSql=null;
				sWhere=null;
			}
			return bRetValue;
		}
		/// Name		: Store
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///**********************************************************
		/// <summary>
		/// This function saves the Template data 
		/// </summary>
		/// <param name="p_sFormXml">Xml string containing the Template data</param>
		/// <param name="p_sFormDefXml">Xml string containing the Template field data</param>
		/// <param name="p_sFormPerXml">Xml string containing the Template Permission data</param>
		/// <param name="p_sFormFileContent">String containing the File contents</param>
		public bool Store(string p_sFormXml,string p_sFormDefXml,string p_sFormPerXml,string p_sFormFileContent, string p_sFileName)
		{
			DbCommand objCommand=null;
			DbConnection objConn=null;
			DbTransaction objTran=null;
			FileStorageManager objFileStorageManager=null;
			bool bSuccess=false;
			SysSettings objSettings = null;
			Acrosoft objAcrosoft = null;
            MediaView objMediaView = null;
			RMConfigurator objConfig = null;
			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				objCommand=objConn.CreateCommand();
				objTran=objConn.BeginTransaction();
				objCommand.Transaction=objTran;
				if (this.FormId==DEFAULT_FORM_ID)
				{
                    this.FormId = Utilities.GetNextUID(m_sConnectionString, "MERGE_FORM", m_iClientId);
				}
				//StoreMergeFormTableData(p_sFormXml,objCommand);
				//commented & changed by jasbinder on 17th july 2005
				StoreMergeFormTableData(p_sFormXml,objCommand,p_sFileName);
				StoreMergeFormDefTableData(p_sFormDefXml,objCommand);
				StoreMergeFormPermTableData(p_sFormPerXml,objCommand);
				this.FormFileName = p_sFileName;

				//Raman Bhatia.. 08/08/2006
				//If Acrosoft is enabled then we do not want to use the legacy document management solution
				//Document would be posted to Acrosoft

				objSettings = new SysSettings(m_sConnectionString,m_iClientId);
                byte[] bytes = bytes = Convert.FromBase64String(p_sFormFileContent);
				if(objSettings.UseAcrosoftInterface==false && objSettings.UseMediaViewInterface == false)
				{
                    if (m_iDBStorageType == 0)
                    {
                        File.Delete(Manager.TemplatePath + this.FormFileName);
                        using (FileStream fs = new FileStream(Manager.TemplatePath + this.FormFileName, FileMode.Create, FileAccess.Write))
                        {
                            using (BinaryWriter bw = new BinaryWriter(fs))
                            {
                                fs.Write(bytes, 0, bytes.Length);
                            }
                        }
                    }
                    else
                    {
                        using (FileStream fs = new FileStream(Path.GetTempPath() + this.FormFileName, FileMode.Create, FileAccess.Write))
                        {
                            using (BinaryWriter bw = new BinaryWriter(fs))
                            {
                                fs.Write(bytes, 0, bytes.Length);
                            }
                        }
						objFileStorageManager=new FileStorageManager(GetStorageType(m_iDBStorageType),m_sDocStorageDSN,m_iClientId);
						objFileStorageManager.StoreFile(Path.GetTempPath() +this.FormFileName,this.FormFileName);
						File.Delete(Path.GetTempPath() + this.FormFileName);
						objFileStorageManager=null;
					}
				} // if
				else if(objSettings.UseAcrosoftInterface)
				{
					//Posting Template to Acrosoft..Raman Bhatia
                    using (FileStream fs = new FileStream(Path.GetTempPath() + this.FormFileName, FileMode.Create, FileAccess.Write))
                    {
                        using (BinaryWriter bw = new BinaryWriter(fs))
                        {
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
					//objFileStorageManager.StoreFile(Path.GetTempPath() +this.FormFileName,this.FormFileName);
					string sDocTitle = this.FormFileName;
					string sAppExcpXml = "";
					objConfig = new RMConfigurator();
					objAcrosoft = new Acrosoft(m_iClientId);//dvatsa-cloud

                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bTempSuccess = false;
                    string sTempAcrosoftUserId = m_userLogin.LoginName;
                    string sTempAcrosoftPassword = m_userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bTempSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }
                    
					//deleting old template
                    //gagnihotri R5 MCM changes
					//objAcrosoft.DeleteFile(m_userLogin.LoginName , m_userLogin.Password ,this.FormFileName , "Templates" , objConfig.GetValue("AcrosoftGeneralStorageTypeKey") , objConfig.GetValue("GeneralFolderFriendlyName"), out sAppExcpXml);
					//objAcrosoft.StoreObjectBuffer(m_userLogin.LoginName , m_userLogin.Password , Path.GetTempPath() + this.FormFileName , sDocTitle , "" , "Templates" , objConfig.GetValue("AcrosoftGeneralStorageTypeKey") , "" , "" , m_userLogin.LoginName , out sAppExcpXml);

                    objAcrosoft.DeleteFile(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        this.FormFileName , "Templates" , 
                        AcrosoftSection.AcrosoftGeneralStorageTypeKey , AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                    //gagnihotri Creating the Templates folder, if not there already
                    
                    objAcrosoft.CreateGeneralFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        AcrosoftSection.AcrosoftGeneralStorageTypeKey, "Templates", AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);

                    objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        Path.GetTempPath() + this.FormFileName, sDocTitle, "", "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, "", "",
                        sTempAcrosoftUserId, out sAppExcpXml);
                    File.Delete(Path.GetTempPath() + this.FormFileName);

                    //rsolanki2 :  end updates for MCM mits 19200 
					objFileStorageManager=null;
				} // else
                else if (objSettings.UseMediaViewInterface)
                {
                    using (FileStream fs = new FileStream(Path.GetTempPath() + this.FormFileName, FileMode.Create, FileAccess.Write))
                    {
                        using (BinaryWriter bw = new BinaryWriter(fs))
                        {
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                    
                    string sMediaViewPath = Path.GetTempPath();
                    objMediaView = new MediaView();

                    string sFormFileName = this.FormFileName;
                    objMediaView.MMDeleteDocumentByName("MailMergeTemplates", "Misc", sFormFileName);

                    importList oList = new importList("MailMergeTemplates", "Misc", sFormFileName);
                    objMediaView.MMUploadDocument(sFormFileName, sMediaViewPath, oList);
                    File.Delete(Path.GetTempPath() + this.FormFileName);
                }
				
				objTran.Commit();
				objConn.Close();
				LoadMergeFormDefTableData(this.FormId);
				CreateHeaderFile();
				bSuccess=true;
			}
			catch (FileNotFoundException p_objException)
			{
				throw new RMAppException("Template.Store.FileNotFoundError",p_objException);
			}
			catch (IOException p_objException)
			{
				throw new RMAppException("Template.Store.FileInputOutputError",p_objException);
			}
			catch (RMAppException p_objException)
			{
				if (objTran!=null)
				{
					objTran.Rollback();
				}
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				if (objTran!=null)
				{
					objTran.Rollback();
				}
				throw new RMAppException(Globalization.GetString("Template.Store.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objConn != null)
				{
					objConn.Close();
					objConn.Dispose();
				}
				objCommand=null;
				objTran=null;
				objFileStorageManager=null;
				objSettings = null;
			}
			return bSuccess;
		}
		/// Name		: Store
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///**********************************************************
		/// <summary>
		/// This function saves the Template data 
		/// </summary>
		/// <param name="p_sFormXml">Xml string containing the Template data</param>
		/// <param name="p_sFormDefXml">Xml string containing the Template field data</param>
		/// <param name="p_sFormPerXml">Xml string containing the Template Permission data</param>
		/// <param name="p_sFormFileContent">String containing the File contents</param>
		public bool Store(string p_sFormXml,string p_sFormDefXml,string p_sFormPerXml,MemoryStream p_objFileContent,out MemoryStream p_objHeaderFile)
		{
			FileStream objWriter=null;
			DbCommand objCommand=null;
			DbConnection objConn=null;
			DbTransaction objTran=null;
			FileStorageManager objFileStorageManager=null;
			bool bSuccess=false;
			p_objHeaderFile=null;
			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				objCommand=objConn.CreateCommand();
				objTran=objConn.BeginTransaction();
				objCommand.Transaction=objTran;
				if (this.FormId==DEFAULT_FORM_ID)
				{
                    this.FormId = Utilities.GetNextUID(m_sConnectionString, "MERGE_FORM", m_iClientId);
				}
				StoreMergeFormTableData(p_sFormXml,objCommand);
				StoreMergeFormDefTableData(p_sFormDefXml,objCommand);
				StoreMergeFormPermTableData(p_sFormPerXml,objCommand);
				File.Delete(Manager.TemplatePath+this.FormFileName);
				
				objWriter=new FileStream(Manager.TemplatePath+this.FormFileName,FileMode.Create,FileAccess.Write);
				objWriter.Write(p_objFileContent.ToArray(),0,(int)p_objFileContent.Length);
				objWriter.Flush();
				objWriter.Close();
				p_objFileContent=null;
                if (m_iDBStorageType == 1 || m_iDBStorageType == 2)
				{
					objFileStorageManager=new FileStorageManager(GetStorageType(m_iDBStorageType),m_sDocStorageDSN,m_iClientId);
					objFileStorageManager.StoreFile(Manager.TemplatePath +this.FormFileName,this.FormFileName);
					File.Delete(Manager.TemplatePath + this.FormFileName);
					objFileStorageManager=null;
				}
				objTran.Commit();
				objConn.Close();
				LoadMergeFormDefTableData(this.FormId);
				CreateHeaderFile(out p_objHeaderFile);
				bSuccess=true;
			}
			catch (FileNotFoundException p_objException)
			{
				throw new RMAppException("Template.Store.FileNotFoundError",p_objException);
			}
			catch (IOException p_objException)
			{
				throw new RMAppException("Template.Store.FileInputOutputError",p_objException);
			}
			catch (RMAppException p_objException)
			{
				if (objTran!=null)
				{
					objTran.Rollback();
				}
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				if (objTran!=null)
				{
					objTran.Rollback();
				}
				throw new RMAppException(Globalization.GetString("Template.Store.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objConn != null)
				{
					objConn.Close();
					objConn.Dispose();
				}
				if (objWriter!=null)
				{
					objWriter.Close();
					objWriter=null;
				}
				objCommand=null;
                if (objTran != null)
                {
                    objTran.Dispose();
                }
				p_objFileContent=null;
				objFileStorageManager=null;
			}
			return bSuccess;
		}
		/// <summary>
		/// Retrieve the template Header file
		/// </summary>
		/// <param name="p_objMemory">Memorystream object</param>
		/// <returns>Success-True Failure- False</returns>
		public bool GetHeaderFile(out MemoryStream p_objMemory)
		{
			string sFileName="";
			p_objMemory=null;
            string headerFileName = GetHeaderFileName(this.FormFileName);//33465 - nkaranam2
            SysSettings objSettings = null;
            Acrosoft objAcrosoft = null;
            RMConfigurator objRMConfig = new RMConfigurator();
            string sAppExcpXml = "";
            string sAcrosoftPath = "";
            string sMediaViewPath = "";
            MediaView objMediaView = null;

            try
            {
                //01/04/2008.. Raman Bhatia : If Acrosoft is enabled then legacy document management is not used
                //Instead fetch file from Acrosoft and use its contents

                objSettings = new SysSettings(m_sConnectionString,m_iClientId);
                if (objSettings.UseAcrosoftInterface == true)
                {
                    sAcrosoftPath = String.Format("{0}\\MailMerge\\", m_UserDataPath);
                    int iRetCd = 0;
                    sAppExcpXml = "";

                    //Fetching rtf and/or doc from Acrosoft and putting it in the temperory path
                    objAcrosoft = new Acrosoft(m_iClientId);//dvatsa-cloud
                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = m_userLogin.LoginName;
                    string sTempAcrosoftPassword = m_userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }

                    //gagnihotri R5 MCM changes
                    //iRetCd = objAcrosoft.FetchAndWriteGeneralFolderDocument(m_userLogin.LoginName, m_userLogin.Password, headerFileName, "Templates", objRMConfig.GetValue("AcrosoftGeneralStorageTypeKey"), objRMConfig.GetValue("GeneralFolderFriendlyName"), sAcrosoftPath + headerFileName, out sAppExcpXml);
                    iRetCd = objAcrosoft.FetchAndWriteGeneralFolderDocument(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        headerFileName, "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, AcrosoftSection.GeneralFolderFriendlyName, 
                        sAcrosoftPath + headerFileName, out sAppExcpXml);

                    //rsolanki2 :  end updates for MCM mits 19200 
                    if (iRetCd == 0)
                    {
                        p_objMemory = Utilities.ConvertFilestreamToMemorystream(sAcrosoftPath + headerFileName, m_iClientId);
                        return true;
                    }


                }
                else if (objSettings.UseMediaViewInterface)
                {
                    objMediaView = new MediaView();
                    sMediaViewPath = String.Format("{0}\\MailMerge\\", m_UserDataPath);
                    bool bResponse = objMediaView.MMGetDocumentByName("MailMergeTemplates", "Misc", ref headerFileName, ref sMediaViewPath);
                    if (bResponse)
                    {
                        p_objMemory = Utilities.ConvertFilestreamToMemorystream(sMediaViewPath + headerFileName, m_iClientId);
                        return true;
                    }
                }

                if (m_iDBStorageType == 1 || m_iDBStorageType == 2)
                {
                    bool bExists = false;
                    FileStorageManager objFileStorageManager = new FileStorageManager(GetStorageType(m_iDBStorageType), m_sDocStorageDSN,m_iClientId);
                    objFileStorageManager.FileExists(headerFileName, out bExists);
                    if (bExists)
                    {
                        //Change by kuladeep for mits:29824 Start
                        //objFileStorageManager.RetrieveFile(headerFileName,string.Empty, out sFileName);
                        objFileStorageManager.RetrieveFile(headerFileName, string.Empty, m_userLogin.LoginName, out sFileName);
                        //Change by kuladeep for mits:29824 End

                        p_objMemory = Utilities.ConvertFilestreamToMemorystream(sFileName, m_iClientId);
                        return true;
                    }
                }
                else if (File.Exists(Manager.TemplatePath + headerFileName))
                {
                    p_objMemory = Utilities.ConvertFilestreamToMemorystream(Manager.TemplatePath + headerFileName, m_iClientId);
                    return true;
                }
                return false;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Template.GetHeaderFile.Error",m_iClientId), p_objException);
            }
            finally
            {
                objSettings = null;
                objAcrosoft = null;
                objRMConfig = null;
                objMediaView = null;
            }
		}

		/// Name		: CreateHeaderFile
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
	
		/// <summary>
		/// Creates a Header file for ms word to show merge fields data
		/// </summary>
		/// <param name="p_objMemory">Memorystream object</param>
		private void CreateHeaderFile(out MemoryStream p_objMemory)
		{
			TemplateField objField=null;
			TemplateField objExtraField=null;
			string sLine="";
			string sHFilename="";
			IDictionaryEnumerator objEnum=null;
			StreamWriter objWriter=null;
			FileStorageManager objFileStorageManager=null;
			SysSettings objSettings = null;
			Acrosoft objAcrosoft = null;
            MediaView objMediaView = null;
			RMConfigurator objConfig = null;
            bool bMultipleFields = false;    //MGaba2:MITS 12422
			try
			{
				objEnum=Fields.GetEnumerator();
				while (objEnum.MoveNext())
				{
					objField=(TemplateField)objEnum.Value;
					if (sLine!="")
					{
						sLine=sLine+Convert.ToChar(9).ToString();
                        //MGaba2:MITS 12422:Start:-setting variable as true,in case of more than one field
                        bMultipleFields = true;
                        //MGaba2:MITS 12422 :End
					}
					sLine=sLine+Convert.ToChar(34).ToString()+objField.WordFieldName+Convert.ToChar(34).ToString();
                    if (((Conversion.ConvertLongToBool(objField.SuppFieldFlag, m_iClientId)) && (objField.FieldType == 6) || (objField.FieldType == 9)) || ((!Conversion.ConvertLongToBool(objField.SuppFieldFlag, m_iClientId)) && (objField.FieldType == 6) || (objField.FieldType == 3)))
					{
						objExtraField= new TemplateField(m_iClientId);//rkaur27
						objExtraField.FormId=objField.FormId;
						objExtraField.SeqNum=objField.SeqNum;
						objExtraField.Param=objField.Param;
						objExtraField.LongCodeFlag=false;
						objExtraField.m_sConnectionString= m_sConnectionString;
                        if ((Conversion.ConvertLongToBool(objField.SuppFieldFlag, m_iClientId)))
						{
							objExtraField.FieldId=(1001 * 65536)+objField.FieldId;
						}
						else
						{
							objExtraField.FieldId=objField.FieldId;
						}
                        objExtraField.WordFieldName = Generic.WordSafeFieldName(objField.FieldDesc, m_iClientId);
						if (sLine!="")
						{
							sLine=sLine+Convert.ToChar(9).ToString();
                            //MGaba2:MITS 12422:Start:-setting variable as true,in case of more than one field
                            bMultipleFields = true;
                            //MGaba2:MITS 12422 :End
						}
						sLine=sLine+Convert.ToChar(34).ToString()+objExtraField.WordFieldName+Convert.ToChar(34).ToString();
						objExtraField=null;
					}
				}
                //MGaba2:MITS 12422:07/28/2008:Error while using mail merge with single field-Start 
                //Appending a tab in case template has single field
                if (bMultipleFields == false)
                {
                    sLine = sLine + Convert.ToChar(9).ToString();
                }
                //MGaba2:MITS 12422:End
				//08/10/2006 Raman Bhatia
				//If Acrosoft is enabled then legacy document management is not used
				
				objSettings = new SysSettings(m_sConnectionString,m_iClientId);
				if(objSettings.UseAcrosoftInterface==false && objSettings.UseMediaViewInterface == false)
				{
				
					sHFilename=Manager.TemplatePath +this.FormFileName.Replace(this.FormFileName.Substring(this.FormFileName.Length-4,4),".HDR");
					objWriter=new StreamWriter(sHFilename,false);
					objWriter.Write(sLine+"\r\n"+sLine+"\r\n");
					objWriter.Flush();
					objWriter.Close();
                    p_objMemory = Utilities.ConvertFilestreamToMemorystream(sHFilename, m_iClientId);
                    if (m_iDBStorageType == 1 || m_iDBStorageType == 2)
					{
						objFileStorageManager=new FileStorageManager(GetStorageType(m_iDBStorageType),m_sDocStorageDSN,m_iClientId);
						objFileStorageManager.StoreFile(sHFilename,this.FormFileName.Replace(this.FormFileName.Substring(this.FormFileName.Length-4,4),".HDR"));
						File.Delete(sHFilename);
						objFileStorageManager=null;
					}
				}
				else if(objSettings.UseAcrosoftInterface)
				{
					sHFilename=Path.GetTempPath() +this.FormFileName.Replace(this.FormFileName.Substring(this.FormFileName.Length-4,4),".HDR");
					objWriter=new StreamWriter(sHFilename,false);
					objWriter.Write(sLine+"\r\n"+sLine+"\r\n");
					objWriter.Flush();
					objWriter.Close();

                    p_objMemory = Utilities.ConvertFilestreamToMemorystream(sHFilename, m_iClientId);
					string sDocTitle = this.FormFileName.Replace(this.FormFileName.Substring(this.FormFileName.Length-4,4),".HDR");
					string sAppExcpXml = "";
					objConfig = new RMConfigurator();
					objAcrosoft = new Acrosoft(m_iClientId);//dvatsa-cloud
                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = m_userLogin.LoginName;
                    string sTempAcrosoftPassword = m_userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }

                    //gagnihotri R5 MCM changes
					//objAcrosoft.DeleteFile(m_userLogin.LoginName , m_userLogin.Password , sDocTitle , "Templates" , objConfig.GetValue("AcrosoftGeneralStorageTypeKey") , objConfig.GetValue("GeneralFolderFriendlyName") , out sAppExcpXml);
					//objAcrosoft.StoreObjectBuffer(m_userLogin.LoginName , m_userLogin.Password , sHFilename , sDocTitle , "" , "Templates" , objConfig.GetValue("AcrosoftGeneralStorageTypeKey") , "" , "" , m_userLogin.LoginName , out sAppExcpXml);
                    objAcrosoft.DeleteFile(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        sDocTitle, "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                    //gagnihotri Creating the Templates folder, if not there already
                    objAcrosoft.CreateGeneralFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, 
                        AcrosoftSection.AcrosoftGeneralStorageTypeKey, "Templates", AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                    objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword, sHFilename, sDocTitle, "",
                        "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, "", "", sTempAcrosoftUserId, out sAppExcpXml);
					File.Delete(sHFilename);
                    //rsolanki2 :  end updates for MCM mits 19200 
 
				} // else
                else if (objSettings.UseMediaViewInterface)
                {
                    sHFilename = Path.GetTempPath() + this.FormFileName.Replace(this.FormFileName.Substring(this.FormFileName.Length - 4, 4), ".HDR");
                    objWriter = new StreamWriter(sHFilename, false);
                    objWriter.Write(sLine + "\r\n" + sLine + "\r\n");
                    objWriter.Flush();
                    objWriter.Close();

                    p_objMemory = Utilities.ConvertFilestreamToMemorystream(sHFilename, m_iClientId);
                    string sDocTitle = this.FormFileName.Replace(this.FormFileName.Substring(this.FormFileName.Length - 4, 4), ".HDR");
                    objConfig = new RMConfigurator();
                    string sFileName = sDocTitle + ".HDR";

                    objMediaView = new MediaView();
                    objMediaView.MMDeleteDocumentByName("MailMergeTemplates", "Misc", sFileName);

                    importList oList = new importList("MailMergeTemplates", "Misc", sFileName);
                    objMediaView.MMUploadDocument(sFileName, Path.GetTempPath(), oList);
                    File.Delete(sHFilename);
                }
                else
                {
                    sHFilename = Path.GetTempPath() + this.FormFileName.Replace(this.FormFileName.Substring(this.FormFileName.Length - 4, 4), ".HDR");
                    objWriter = new StreamWriter(sHFilename, false);
                    objWriter.Write(sLine + "\r\n" + sLine + "\r\n");
                    objWriter.Flush();
                    objWriter.Close();

                    p_objMemory = Utilities.ConvertFilestreamToMemorystream(sHFilename, m_iClientId);
                }
				
			}
			catch (FileNotFoundException p_objException)
			{
				throw new RMAppException("Template.CreateHeaderFile.FileNotFoundError",p_objException);
			}
			catch (IOException p_objException)
			{
				throw new RMAppException("Template.CreateHeaderFile.FileInputOutputError",p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.CreateHeaderFile.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objWriter!=null)
				{
					objWriter.Close();
					objWriter.Dispose();
				}
				sLine=null;
				sHFilename=null;
				objEnum=null;
				objField=null;
				objExtraField=null;
				objFileStorageManager=null;
                objConfig = null;
                objSettings = null;
			}
		}
        //33465 - nkaranam2 - Support to DOCX format
        //Returns Header File Name based on FormFileName
        private string GetHeaderFileName(string FormFileName)
        {
            if(FormFileName.EndsWith(".docx"))
                 return FormFileName.Replace(FormFileName.Substring(FormFileName.Length - 5, 5), ".HDR");
            else
                 return FormFileName.Replace(FormFileName.Substring(FormFileName.Length - 4, 4), ".HDR");
        }
		/// Name		: CreateHeaderFile
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates a Header file for ms word to show merge fields data
		/// </summary>
		private void CreateHeaderFile()
		{
			TemplateField objField=null;
			TemplateField objExtraField=null;
			string sLine="";
			string sHFilename="";
			IDictionaryEnumerator objEnum=null;
			StreamWriter objWriter=null;
			FileStorageManager objFileStorageManager=null;
			SysSettings objSettings = null;
			Acrosoft objAcrosoft = null;
            MediaView objMediaView = null;
			RMConfigurator objConfig = null;
            bool bMultipleFields = false;    //MGaba2:MITS 12422
			try
			{
				objEnum=Fields.GetEnumerator();
				while (objEnum.MoveNext())
				{
					objField=(TemplateField)objEnum.Value;
					if (sLine!="")
					{
						sLine=sLine+Convert.ToChar(9).ToString();
                        //MGaba2:MITS 12422:Start:-setting variable as true,in case of more than one field
                        bMultipleFields = true;
                        //MGaba2:MITS 12422:07/28/2008 : End
					}
					sLine=sLine+Convert.ToChar(34).ToString()+objField.WordFieldName+Convert.ToChar(34).ToString();
				}
                //MGaba2:MITS 12422:07/28/2008:Error while using mail merge with single field
                //Appending a tab in case template has single field-Start
                if (bMultipleFields == false)
                {
                    sLine = sLine + Convert.ToChar(9).ToString();
                }
                //MGaba2:MITS 12422 : End
				
                //08/10/2006 Raman Bhatia
                //If Acrosoft is enabled then legacy document management is not used
				objSettings = new SysSettings(m_sConnectionString,m_iClientId);
				if(objSettings.UseAcrosoftInterface==false && objSettings.UseMediaViewInterface == false)
				{
					if (m_iDBStorageType == 0)
					{
                        sHFilename = Manager.TemplatePath + GetHeaderFileName(this.FormFileName);//33465 - nkaranam2
						objWriter=new StreamWriter(sHFilename,false);
						objWriter.Write(sLine+"\r\n"+sLine+"\r\n");
						objWriter.Flush();
						objWriter.Close();
					}
					else
					{
                        sHFilename = Path.GetTempPath() + GetHeaderFileName(this.FormFileName);//33465 - nkaranam2
						objWriter=new StreamWriter(sHFilename,false);
						objWriter.Write(sLine+"\r\n"+sLine+"\r\n");
						objWriter.Flush();
						objWriter.Close();
					}
                    if (m_iDBStorageType == 1 || m_iDBStorageType == 2)
					{
                        objFileStorageManager = new FileStorageManager(GetStorageType(m_iDBStorageType), m_sDocStorageDSN, m_iClientId);
                        objFileStorageManager.StoreFile(sHFilename, GetHeaderFileName(this.FormFileName));//33465 - nkaranam2
						File.Delete(sHFilename);
						objFileStorageManager=null;
					}    
				} // if
				else if(objSettings.UseAcrosoftInterface)
				{
                    sHFilename = Path.GetTempPath() + GetHeaderFileName(this.FormFileName);//33465 - nkaranam2
					objWriter=new StreamWriter(sHFilename,false);
					objWriter.Write(sLine+"\r\n"+sLine+"\r\n");
					objWriter.Flush();
					objWriter.Close();

                    string sDocTitle = GetHeaderFileName(this.FormFileName);//33465 - nkaranam2
					string sAppExcpXml = "";
					objConfig = new RMConfigurator();
					objAcrosoft = new Acrosoft(m_iClientId);//dvatsa-cloud
                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = m_userLogin.LoginName;
                    string sTempAcrosoftPassword = m_userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }
                    //gagnihotri R5 MCM changes
					//objAcrosoft.DeleteFile(m_userLogin.LoginName , m_userLogin.Password , sDocTitle , "Templates" , objConfig.GetValue("AcrosoftGeneralStorageTypeKey") , objConfig.GetValue("GeneralFolderFriendlyName") , out sAppExcpXml);
					//objAcrosoft.StoreObjectBuffer(m_userLogin.LoginName , m_userLogin.Password , sHFilename , sDocTitle , "" , "Templates" , objConfig.GetValue("AcrosoftGeneralStorageTypeKey") , "" , "" ,m_userLogin.LoginName , out sAppExcpXml);
                    objAcrosoft.DeleteFile(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        sDocTitle, "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);

                    //gagnihotri Creating the Templates folder, if not there already
                    objAcrosoft.CreateGeneralFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        AcrosoftSection.AcrosoftGeneralStorageTypeKey, "Templates", AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);

                    objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        sHFilename, sDocTitle, "", "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, "", "", sTempAcrosoftUserId, out sAppExcpXml);

					File.Delete(sHFilename);
                    //rsolanki2 :  end updates for MCM mits 19200 
 
				} // else
                else if (objSettings.UseMediaViewInterface)
                {
                    sHFilename = Path.GetTempPath() + GetHeaderFileName(this.FormFileName);//33465 - nkaranam2
                    objWriter = new StreamWriter(sHFilename, false);
                    objWriter.Write(sLine + "\r\n" + sLine + "\r\n");
                    objWriter.Flush();
                    objWriter.Close();

                    
                    string sDocTitle = GetHeaderFileName(this.FormFileName);//33465 - nkaranam2
                    objConfig = new RMConfigurator();
                    string sFileName = sDocTitle; 

                    objMediaView = new MediaView();
                    objMediaView.MMDeleteDocumentByName("MailMergeTemplates", "Misc", sFileName);

                    importList oList = new importList("MailMergeTemplates", "Misc", sFileName);
                    objMediaView.MMUploadDocument(sFileName, Path.GetTempPath(), oList);
                    File.Delete(sHFilename);
                }
                else
                {
                    sHFilename = Path.GetTempPath() + GetHeaderFileName(this.FormFileName);//33465 - nkaranam2
                    objWriter = new StreamWriter(sHFilename, false);
                    objWriter.Write(sLine + "\r\n" + sLine + "\r\n");
                    objWriter.Flush();
                    objWriter.Close();

                    
                }
				
				
			}
			catch (FileNotFoundException p_objException)
			{
				throw new RMAppException("Template.CreateHeaderFile.FileNotFoundError",p_objException);
			}
			catch (IOException p_objException)
			{
				throw new RMAppException("Template.CreateHeaderFile.FileInputOutputError",p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.CreateHeaderFile.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objWriter!=null)
				{
					objWriter.Close();
                    objWriter.Dispose();
				}
				sLine=null;
				sHFilename=null;
				objEnum=null;
				objField=null;
				objExtraField=null;
				objFileStorageManager=null;
				objAcrosoft = null;
				objConfig = null;
                objSettings = null;
			}
		}
		#endregion

		#region Function to delete a template
		/// Name		: Delete
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function deletes a template
		/// </summary>
		public void Delete()
		{
			string sSql="";
			DbConnection objConn=null;
			DbCommand objCommand=null;
			DbTransaction objTran=null;
			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				objCommand=objConn.CreateCommand();
				objTran=objConn.BeginTransaction();
				objCommand.Transaction=objTran;
				sSql = "DELETE FROM MERGE_FORM_DEF WHERE FORM_ID= " +FormId;
				objCommand.CommandText=sSql;
				objCommand.ExecuteNonQuery();
				sSql = "DELETE FROM MERGE_FORM_PERM WHERE FORM_ID= " +FormId;
				objCommand.CommandText=sSql;
				objCommand.ExecuteNonQuery ();
				sSql = "DELETE FROM MERGE_FORM WHERE FORM_ID= " +FormId;
				objCommand.CommandText=sSql;
				objCommand.ExecuteNonQuery ();
				objTran.Commit();
				objConn.Close();
			}
			catch (Exception p_objException)
			{
				objTran.Rollback();
				throw new RMAppException(Globalization.GetString("Template.Delete.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objConn != null)
				{
					objConn.Close();
					objConn.Dispose();
				}
				objCommand=null;
                if (objTran != null)
                {
                    objTran.Dispose();
                }
				sSql=null;
			}
		}
		#endregion

		#region Function to reload a data
		/// Name		: Refresh
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function reloads the data
		/// </summary>
		private void Refresh()
		{
			long lFormID=0;
			lFormID=FormId;
			if (IsNew) 
			{
				return;
			}
			else
			{
				Clear();
				Load(lFormID);
			}
		}
		#endregion

		#region Function to clear the data
		/// Name		: Clear
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function clears the data
		/// </summary>
		private void Clear()
		{
			m_lFormId=0;
			m_lCatId=0;
			m_sFormName="";
			m_sFormDesc="";
			m_sTableQualify="0";
			m_sFormFileName="";
			m_objColFields=new SortedList();
			m_arrlstColPerms=new ArrayList();
			m_sFormFileContent="";
		}
		#endregion

		#region Function to retrieve fields used in an existing template
		/// Name		: GetUsedFieldList
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the existing fields related to a template
		/// </summary>
		/// <returns>Xml containing the existing fields data</returns>
		public XmlDocument GetUsedFieldList()
		{
			TemplateField objField=null;
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			IDictionaryEnumerator objEnum=null;
			long lTempValue=0;
            SysSettings objSysSettings=new SysSettings(m_sConnectionString,m_iClientId);
            //Added Rakhi for R7:Add Emp Data Elements
            bool bOldPhoneFieldExists = false;
            string sFieldDesc = string.Empty;
            //Added Rakhi for R7:Add Emp Data Elements
			try
			{
				objEnum=m_objColFields.GetEnumerator();
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("UsedFieldList");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("Fields");
				objDOM.FirstChild.AppendChild(objElemChild);
				while (objEnum.MoveNext())
				{
					objField=(TemplateField)objEnum.Value;
                    //Added Rakhi for R7:Add Emp Data Elements:Prevent Fields from coming in the Display list of an existing Search if the setting is off
                    if (String.Compare(objField.FieldTable,"entity_x_addresses",true)==0 && !objSysSettings.UseMultipleAddresses)
                    {
                        continue;
                    }
                    //Added Rakhi for R7:Add Emp Data Elements:Prevent Fields from coming in the Display list of an existing Search if the setting is off
                    //Rakhi-Added Case to prevent Old Phone Fields(Office and Home) from coming in new Merge Template
                    switch (objField.CatId)
                    {
                        case 3:
                        case 4:
                        case 8:
                        case 9:
                        case 10:
                            sFieldDesc = objField.FieldDesc;
                            //There are a number of Display Categories for each org hierarchy level where these phone fields exists for each cat_id.
                            //For Instance:-Employee Client, Employee Company,Employee Operation,Employee Region...........
                            //To avoid putting cases for each one of them,have taken .contains criteria as every field to be considered is satisfing this condition as of now.
                            if ((sFieldDesc.ToLower().Contains("office phone")) || sFieldDesc.ToLower().Contains("home phone"))
                            {
                                bOldPhoneFieldExists = true;
                            }
                            break;
                    }
                    //Added Case to prevent Old Phone Fields(Office and Home) from coming in new Merge Template
					objElemTemp=objDOM.CreateElement("Field");
					objElemTemp.SetAttribute("CatId",objField.CatId.ToString());
					objElemTemp.SetAttribute("DisplayCat",objField.DisplayCat.ToString());
					objElemTemp.SetAttribute("FieldDesc",objField.FieldDesc.ToString());
					objElemTemp.SetAttribute("FieldName",objField.FieldName.ToString());
                    if (Conversion.ConvertLongToBool(objField.SuppFieldFlag, m_iClientId))
					{
						lTempValue=(8+ 1000) * 65536 +objField.FieldId;
					
					}
					else
					{
						lTempValue=objField.FieldId;
					}
					objElemTemp.SetAttribute("FieldId",lTempValue.ToString());
					objElemTemp.SetAttribute("MergeParam",objField.Param.ToString());  // Stash param on for res total or pay total type fields (will be empty string for everyone else)   JP 09.08.2006
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
                //Added Rakhi for R7:Add Emp Data Elements
                if (bOldPhoneFieldExists)
                {
                    objElemTemp=objDOM.CreateElement("OldPhoneFieldExists");
                    objElemTemp.InnerText="true";
                    objDOM.DocumentElement.AppendChild(objElemTemp);
                }
                //Added Rakhi for R7:Add Emp Data Elements
				return objDOM;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.GetUsedFieldList.XmlError",m_iClientId),p_objException);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.GetUsedFieldList.Error",m_iClientId),p_objException);
			}
			finally
			{ 			
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				objField=null;
                objSysSettings = null;
			}
		}
		#endregion

		#region Functions to retreive all fields that can be used to create template 
		/// Name		: ConstructAggregateFields
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function processes the claim reserve related passed data
		/// </summary>
		/// <param name="p_objOrig">Template field item</param>
		/// <param name="p_lTemp">Index id</param>
		/// <param name="p_sTable">Table name i.e CLAIM_PAY_TOT or CLAIM_RES_TOT</param>
		private void ConstructAggregateFields(TemplateFieldItem p_objOrig,ref long p_lTemp,string p_sTable) 
		{
			DbReader objReader=null;
			string  sSQL="";
			string sCodeTable="";
			string sFieldName="";
			TemplateFieldItem objTemplateFieldItem=null;
			//ONLY FOR CLAIM_RES_TOT AND CLAIM_PAY_TOT
			if (p_sTable!="CLAIM_PAY_TOT" && p_sTable!="CLAIM_RES_TOT")
			{
				return;
			}
			if (p_sTable=="CLAIM_RES_TOT")
			{
				sCodeTable="RESERVE_TYPE";
			}
			if (p_sTable=="CLAIM_PAY_TOT")
			{
				sCodeTable="TRANS_TYPES";
			}
			try
			{
				sFieldName=p_objOrig.FieldName;
				sSQL = " SELECT CODES.CODE_ID,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY";
				sSQL=sSQL+" WHERE GLOSSARY.SYSTEM_TABLE_NAME = '"+sCodeTable+"'";
				sSQL=sSQL+" AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ";
				sSQL=sSQL+" AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG = 0 ORDER BY CODES_TEXT.CODE_DESC ";
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
				while (objReader.Read())
				{
					objTemplateFieldItem=new TemplateFieldItem(m_iClientId);//rkaur27
					objTemplateFieldItem.FieldName=sFieldName+" ("+Conversion.ConvertObjToStr(objReader.GetValue( "CODE_DESC"))+")";
					objTemplateFieldItem.DisplayCat=p_objOrig.DisplayCat;
					objTemplateFieldItem.FieldId=p_objOrig.FieldId;
					objTemplateFieldItem.Param=Conversion.ConvertObjToStr(objReader.GetValue("CODE_ID"));  // Store off code id as param     JP 09.08.2006
					m_arrlstAvailFields.Add(objTemplateFieldItem);
					p_lTemp++;
				}
				p_objOrig.FieldName=sFieldName+" (ALL)";
				//BSB 10.13.2006 Need to add <ALL> as Merge Param for the (ALL) case.
				p_objOrig.Param="<ALL>";  // Store off code id as param     JP 09.08.2006

			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.ConstructAggregateFields.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
                objTemplateFieldItem = null;
				sSQL=null;
				sCodeTable=null;
				sFieldName=null;
			}
		}
		/// Name		: GetAvailableFieldList
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the field list based on Category id
		/// </summary>
		/// <param name="p_lParmCatId">Category id</param>
		private void GetAvailableFieldList(long p_lParmCatId)
		{
			TemplateFieldItem objTemplateFieldItem=null;
			TemplateFieldItem objTmpNested=null;
			DbReader objReader=null;
			DbReader objTempReader=null;
			long lCatId=p_lParmCatId;
			string  sSQL="";
			bool bLongCodeDoneFlag=false;
			string sFieldTable="";
			bool bLongCodeFlag=false;
			bool bSkipDuplicate=false;
			long lData=0;
			long lTmp=0;
			bool bRead=false;
			string sTemp="";
			TemplateCategory objCategory=null;
			long lConstruct=0;
            SysSettings objSettings = null;
            bool bShowFields = true; //pmittal5 Mits 19142
            //Start-Mridul Bansal. 01/13/10. MITS#18229
            ColLobSettings objLobSettings = null;
            //End-Mridul Bansal. 01/13/10. MITS#18229
            bool bAddressField=false;//Added Rakhi for R7:Add Emp Data Elements
            bool bOldPhoneField = false;//Added Rakhi for R7:Add Emp Data Elements

            bool bIsEnhPolicyEnabled = false;
            bool bIsPolicyTrackingEnabled = false;
			try
			{
                
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);
                // akaushik5 Added for MITS 37272 Starts
                objLobSettings = new ColLobSettings(m_sConnectionString, this.m_iClientId);
                // akaushik5 Added for MITS 37272 Ends
    			CatId=p_lParmCatId;

				m_arrlstAvailFields =new ArrayList();
				if (lCatId != 20)
				{
                    sSQL = "SELECT DISPLAY_CAT,FIELD_ID,FIELD_DESC,FIELD_TYPE,FIELD_TABLE FROM MERGE_DICTIONARY";
                    sSQL = sSQL + " WHERE CAT_ID = " + lCatId.ToString() + " ORDER BY DISPLAY_CAT,FIELD_DESC";

					objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
					bRead=objReader.Read();
					while (bRead)
					{
                        //gagnihotri 09/15/2009 MITS 17813, Added condition to restrict addition of PolicyMgmt Supplemental fields when enh. policy is disabled.
                        //Shruti for MITS 9761. The query above fetches the fields for Policy and EnhPolicy both. The fields in both case have same names 
                        // and seem to get duplicated. Check is put to get the fields either for policy or policyEnh based on the utility settings. 
                        sFieldTable = Conversion.ConvertObjToStr(objReader.GetValue("FIELD_TABLE"));
                        //Added Rakhi for R7:Add Emp Data Elements:Prevent Fields from coming in the Display list of an existing Merge if the setting is off
                        bAddressField = false;
                        if (String.Compare(sFieldTable,"entity_x_addresses",true)==0 && !objSettings.UseMultipleAddresses)
                        {
                            bAddressField = true;
                        }
                        //Added Rakhi for R7:Add Emp Data Elements:Prevent Fields from coming in the Display list of an existing Merge if the setting is off
                        //Rakhi-Added Case to prevent Old Phone Fields(Office and Home) from coming in new Merge Template
                        switch (lCatId)
                        {
                            case 3:
                            case 4:
                            case 8:
                            case 9:
                            case 10:
                                string sFieldDesc = Conversion.ConvertObjToStr(objReader.GetValue("FIELD_DESC"));
                                bOldPhoneField = false;
                                //There are a number of Display Categories for each org hierarchy level where these phone fields exists for each cat_id.
                                //For Instance:-Employee Client, Employee Company,Employee Operation,Employee Region...........
                                //To avoid putting cases for each one of them,have taken .contains criteria as every field to be considered is satisfing this condition as of now.
                                if ((sFieldDesc.ToLower().Contains("office phone")) || sFieldDesc.ToLower().Contains("home phone"))
                                {
                                    bOldPhoneField = true;
                                }
                                break;
                        }
                        //Added Case to prevent Old Phone Fields(Office and Home) from coming in new Merge Template
                        
                        //pmittal5 Mits 19142 12/15/09 - Dont show Insurer and Reinsurer fields if corresponding settings are not enabled for Policy Tracking and Claim Merge
                        if (lCatId == 6 || lCatId == 1) //Policy Tracking / Claim
                        {
                            if (objSettings.MultipleInsurer == false && (sFieldTable == "INSURER_ENTITY" || sFieldTable == "POLICY_X_INSURER"))
                                bShowFields = false;
                            else if (objSettings.MultipleReInsurer == false && (sFieldTable == "REINSURER_ENTITY" || sFieldTable == "POLICY_X_INS_REINS"))
                                bShowFields = false;
                            else
                                bShowFields = true;
                        }
                        if (bShowFields && !bAddressField && !bOldPhoneField) //Added && condition for R7:Prevent Fields from coming in the Display list of an existing Search if the setting is off 
                        {//End - pmittal5
                            //Start-Mridul Bansal. 01/13/10. MITS#18229
                            // akaushik5 Commneted for MITS 37272 Starts
                            //objLobSettings = new ColLobSettings(m_sConnectionString);
                            // akaushik5 Commneted for MITS 37272 Ends
                            bIsEnhPolicyEnabled = objLobSettings[241].UseEnhPolFlag == -1 && objLobSettings[242].UseEnhPolFlag == -1 && objLobSettings[243].UseEnhPolFlag == -1 && objLobSettings[845].UseEnhPolFlag == -1;
                            bIsPolicyTrackingEnabled = objLobSettings[241].UseEnhPolFlag != -1 && objLobSettings[242].UseEnhPolFlag != -1 && objLobSettings[243].UseEnhPolFlag != -1 && objLobSettings[845].UseEnhPolFlag != -1;
                            //if (((objSettings.UseEnhPolFlag == -1 && sFieldTable != "POLICY" &&
                            //     sFieldTable != "POLICY_X_CVG_TYPE" && sFieldTable != "INSURED" &&
                            //     sFieldTable != "INSURER_ENTITY" && sFieldTable != "POLICY_SUPP" &&
                            //     sFieldTable != "POLICY_X_INSURER" && sFieldTable != "REINSURER_ENTITY" && sFieldTable != "POLICY_X_INS_REINS") ||  //pmittal5 Mits 19142
                            //     (objSettings.UseEnhPolFlag != -1 && !sFieldTable.EndsWith("_ENH") && sFieldTable != "POLICY_ENH_SUPP")) ||
                            //     lCatId != 1)//Shruti for 11864

                            if (lCatId == 1 && bIsEnhPolicyEnabled && (sFieldTable == "POLICY" || sFieldTable == "POLICY_X_CVG_TYPE" || sFieldTable == "INSURED" ||
                                 sFieldTable == "INSURER_ENTITY" || sFieldTable == "POLICY_SUPP" || sFieldTable == "POLICY_X_INSURER" && sFieldTable == "REINSURER_ENTITY" && sFieldTable == "POLICY_X_INS_REINS"))
                            {
                                bRead = objReader.Read();
                                continue;
                            }
                            if (lCatId == 1 && bIsPolicyTrackingEnabled && (sFieldTable.EndsWith("_ENH") || sFieldTable == "POLICY_ENH_SUPP"))
                            {
                                bRead = objReader.Read();
                                continue;
                            }
                                 
                            //End-Mridul Bansal. 01/13/10. MITS#18229
                                lData = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("FIELD_TYPE")));
                                if (lData != 8)
                                {
                                    objTemplateFieldItem = new TemplateFieldItem(m_iClientId);//rkaur27
                                    bLongCodeDoneFlag = false;
                                    objTemplateFieldItem.DisplayCat = Conversion.ConvertObjToStr(objReader.GetValue("DISPLAY_CAT"));
                                    objTemplateFieldItem.FieldName = Conversion.ConvertObjToStr(objReader.GetValue("FIELD_DESC"));
                                    objTemplateFieldItem.FieldId = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("FIELD_ID")));
                                    objTemplateFieldItem.FieldType = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("FIELD_TYPE")));
                                    objTemplateFieldItem.FieldTable = sFieldTable;
                                    objTemplateFieldItem.IsSupp = false;
                                    objTemplateFieldItem.LongCodeFlag = bLongCodeFlag;
                                    objTemplateFieldItem.ExtendFieldName();
                                    if (bLongCodeFlag)
                                    {
                                        bLongCodeDoneFlag = true;
                                    }
                                    if (objTemplateFieldItem.FieldName == Conversion.ConvertObjToStr(objReader.GetValue("FIELD_DESC")) || bLongCodeDoneFlag || objTemplateFieldItem.IsSupp)
                                    {
                                        bLongCodeFlag = false;
                                    }
                                    else
                                    {
                                        bLongCodeFlag = true;
                                    }

                                    ConstructAggregateFields(objTemplateFieldItem, ref lConstruct, sFieldTable);

                                    m_arrlstAvailFields.Add(objTemplateFieldItem);
                                }
                                else
                                {
                                    bSkipDuplicate = false;
                                    int i = 0;
                                    for (i = 0; i < m_arrlstAvailFields.Count; i++)
                                    {
                                        objTmpNested = (TemplateFieldItem)m_arrlstAvailFields[i];
                                        if (objTmpNested.FieldTable == sFieldTable)
                                        {
                                            bSkipDuplicate = true;
                                        }
                                    }
                                    if (!bSkipDuplicate)
                                    {
                                        sSQL = "SELECT FIELD_ID,USER_PROMPT,FIELD_TYPE FROM" +
                                                " SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + sFieldTable + "'" +
                                                //pmittal5 Mits 20680 - Grid Type field (Field_Type:17) should not be visible for selection in Mail Merge
                                                //" AND FIELD_TYPE NOT IN (7,20,14,15,16) AND " +
                                                // npadhy JIRA 6415 For Type = 22, User Lookup Supplementals - we can not support Mail merge right now.
                                                // This will involve lots of change as we do not support the mail merge for SUPP_MULTI_VALUE correctly
                                                " AND FIELD_TYPE NOT IN (7,20,14,15,17, 22) AND " +  //MITS 26255 hlv 8/6/12
                                                 " (DELETE_FLAG=0 OR DELETE_FLAG IS NULL) ORDER BY USER_PROMPT";
                                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                        bRead = objTempReader.Read();
                                        while (bRead)
                                        {
                                            objTemplateFieldItem = new TemplateFieldItem(m_iClientId);//rkaur27
                                            objTemplateFieldItem.DisplayCat = Conversion.ConvertObjToStr(objTempReader.GetValue("FIELD_ID"));
                                            bLongCodeDoneFlag = false;
                                            objTemplateFieldItem.FieldName = Conversion.ConvertObjToStr(objTempReader.GetValue("USER_PROMPT"));
                                            lTmp = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objTempReader.GetValue("FIELD_ID")));
                                            objTemplateFieldItem.FieldId = (Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objTempReader.GetValue("FIELD_TYPE"))) + 1000) * 65536 + lTmp;
                                            objTemplateFieldItem.FieldType = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objTempReader.GetValue("FIELD_TYPE")));
                                            objTemplateFieldItem.FieldTable = sFieldTable;
                                            objTemplateFieldItem.IsSupp = true;
                                            objTemplateFieldItem.LongCodeFlag = bLongCodeFlag;
                                            objTemplateFieldItem.ExtendFieldName();
                                            objTemplateFieldItem.FieldDesc = Conversion.ConvertObjToStr(objReader.GetValue("FIELD_DESC"));
                                            if (bLongCodeFlag)
                                            {
                                                bLongCodeDoneFlag = true;
                                            }
                                            m_arrlstAvailFields.Add(objTemplateFieldItem);
                                            if (bLongCodeDoneFlag || objTemplateFieldItem.FieldName == Conversion.ConvertObjToStr(objTempReader.GetValue("USER_PROMPT")))
                                            {
                                                bRead = objTempReader.Read();
                                                bLongCodeFlag = false;
                                            }
                                            else
                                            {
                                                bLongCodeFlag = true;
                                            }

                                        }
                                        //to see
                                        objTempReader.Close();
                                    }
                                }//end supp
                        }
                        if (!bLongCodeFlag)
                        {
                            bRead = objReader.Read();
                        }
					}// end while
					objReader.Close();
				}	
				else//CatId = 20 "Admin Tracking"
				{
					m_arrlstAvailFields =new ArrayList();
					GetAvailableAdminTableList();
					for (int i=0;i<m_arrlstAvailAdminTables.Count;i++)
					{
						objCategory=(TemplateCategory)m_arrlstAvailAdminTables[i];
						if (!string.IsNullOrEmpty(sTemp))
						{
							sTemp+=",'"+objCategory.CategoryDesc+"'";
						}
						else
						{
							sTemp+="'"+objCategory.CategoryDesc+"'";
						}
					}
					if (string.IsNullOrEmpty(sTemp))
					{
						return;
					}
					sSQL = "SELECT FIELD_ID,USER_PROMPT,FIELD_TYPE, GLOSSARY_TEXT.TABLE_NAME, SUPP_TABLE_NAME FROM SUPP_DICTIONARY, GLOSSARY, GLOSSARY_TEXT WHERE SUPP_TABLE_NAME IN (" + sTemp + ")";
                    //sSQL += " AND FIELD_TYPE NOT IN (4,5,7,11,14,15,16,20) AND (DELETE_FLAG=0 OR DELETE_FLAG IS NULL)";
                    //MGaba2:MITS 12398 : FreeText/Memo/Time fields were not available for mail merge
                    //pmittal5 Mits 20680 - Grid Type field (Field_Type:17) should not be visible for selection in Mail Merge
                    //sSQL += " AND FIELD_TYPE NOT IN (7,14,15,16,20) AND (DELETE_FLAG=0 OR DELETE_FLAG IS NULL)";
                    sSQL += " AND FIELD_TYPE NOT IN (7,14,15,16,20,17) AND (DELETE_FLAG=0 OR DELETE_FLAG IS NULL)";
					sSQL  += " AND GLOSSARY.SYSTEM_TABLE_NAME = SUPP_DICTIONARY.SUPP_TABLE_NAME";
					sSQL  += " AND GLOSSARY_TEXT.TABLE_ID = GLOSSARY.TABLE_ID ORDER BY TABLE_NAME,USER_PROMPT";
					objTempReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
					bRead=objTempReader.Read();
					while (bRead)
					{
						bLongCodeDoneFlag=false;
						objTemplateFieldItem=new TemplateFieldItem(m_iClientId);//rkaur27
						objTemplateFieldItem.DisplayCat=Conversion.ConvertObjToStr(objTempReader.GetValue("TABLE_NAME"));
						objTemplateFieldItem.FieldName=Conversion.ConvertObjToStr(objTempReader.GetValue("USER_PROMPT"));
						lTmp=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objTempReader.GetValue("FIELD_ID")));
						objTemplateFieldItem.FieldType=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objTempReader.GetValue("FIELD_TYPE")));
						objTemplateFieldItem.FieldId=(8+ 1000)*65536 + lTmp;
						objTemplateFieldItem.IsSupp=true;
						if (bLongCodeFlag)
						{
							objTemplateFieldItem.LongCodeFlag=true;
							bLongCodeFlag=false;
							bLongCodeDoneFlag=true;
						}
						objTemplateFieldItem.ExtendFieldName();
						m_arrlstAvailFields.Add(objTemplateFieldItem);
						if (bLongCodeDoneFlag  || objTemplateFieldItem.FieldName==Conversion.ConvertObjToStr(objTempReader.GetValue( "USER_PROMPT")))
						{
							bRead=objTempReader.Read();
						}
						else
						{
							bLongCodeFlag = true;
						}
					}
					objTempReader.Close();
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.GetAvailableFieldList.Error",m_iClientId),p_objException);
			}
			finally
			{ 			
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objTempReader != null)
				{
					objTempReader.Close();
					objTempReader.Dispose();
				}
				objTemplateFieldItem=null;
				objTmpNested=null;
				objCategory=null;
                objSettings = null;
                //Start-Mridul Bansal. 01/13/10. MITS#18229.
                objLobSettings = null;
                //Start-Mridul Bansal. 01/13/10. MITS#18229.
			}
	
		}
		/// Name		: CreateAvailableFieldsXml
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns field list as Xml string
		/// </summary>
		/// <returns>Xml string containing field list</returns>
		public XmlDocument CreateAvailableFieldsXml(long p_lCatId)
		{
			string sPrevious="";
			string sTemp="";
			TemplateFieldItem objField=null;
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			XmlElement objNewChild=null;
            SysSettings objSettings = null;//Deb MITS 27988 
			try
			{
				GetAvailableFieldList(p_lCatId);
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("AvailableFieldsList");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("Fields");
				objDOM.FirstChild.AppendChild(objElemChild);
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);
				for (int i=0;i<m_arrlstAvailFields.Count;i++)
				{
					objField= (TemplateFieldItem)m_arrlstAvailFields[i];
                    //Deb MITS 27988 
                    if (objSettings.UseMultiCurrency != -1 && (objField.FieldName == "Claim Currency Amount" || objField.FieldName == "Payment Currency Amount"))
                    {
                       continue;
                    }
                    //Deb MITS 27988 
					if (objField.IsSupp==true)
					{
						sTemp=objField.FieldDesc;
					}
					else
					{
						sTemp=objField.DisplayCat;
					}
					if (CatId==20)
					{
						sTemp=objField.DisplayCat;
					}
					if (!sPrevious.Equals(sTemp))
					{
						if (i!=0)
						{
							objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
						}
						objElemTemp=objDOM.CreateElement("Field");	
						objElemTemp.SetAttribute("DisplayCat",sTemp);
					}
					sPrevious=sTemp;
					objNewChild=objDOM.CreateElement("Value");
					objNewChild.SetAttribute("FieldName",objField.FieldName);
                   
					objNewChild.SetAttribute("FieldId",Conversion.ConvertObjToStr(objField.FieldId));
					objNewChild.SetAttribute("FieldType",Conversion.ConvertObjToStr(objField.FieldType));
					objNewChild.SetAttribute("FieldTable",objField.FieldTable);
					objNewChild.SetAttribute("LongCodeFlag",objField.LongCodeFlag.ToString());
					objNewChild.SetAttribute("IsSupp",objField.IsSupp.ToString());
					objNewChild.SetAttribute("MergeParam",objField.Param.ToString());   // Include param (code id) for res total and pay total fields   JP 09.08.2006
					objElemTemp.AppendChild((XmlNode)objNewChild);
				}
				if (m_arrlstAvailFields.Count >=1)
				{
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				return objDOM;	
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.CreateAvailableFieldsXml.XmlError",m_iClientId),p_objException);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.CreateAvailableFieldsXml.Error",m_iClientId),p_objException);
			}
			finally
			{ 			
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				objNewChild=null;
                objField = null;
                objSettings = null;//Deb MITS 27988 
			}
		}
		#endregion

		#region Function to get admin tracking tables
		/// Name		: GetAvailableAdminTableList
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the Admin tracking tables list
		/// </summary>
		/// <returns>Xml containing Admin Table List</returns>
		public XmlDocument GetAvailableAdminTableList()
		{
			long lGlossaryType=0;
			string sWhere="";
			string sSql="";
			lGlossaryType=468;
			DbReader objReader=null;
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			TemplateCategory objCat=null;
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("AvailableAdminTableList");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("TableNames");
				objDOM.FirstChild.AppendChild(objElemChild);
				sSql = "SELECT SYSTEM_TABLE_NAME, GLOSSARY_TEXT.TABLE_NAME FROM GLOSSARY, GLOSSARY_TEXT WHERE GLOSSARY_TYPE_CODE=" + lGlossaryType.ToString() + " AND GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID " + "ORDER BY GLOSSARY_TEXT.TABLE_NAME"; 
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
				
				while (objReader.Read())
				{
					objCat=new TemplateCategory(m_iClientId);//rkaur27
					objCat.CategoryDesc = Conversion.ConvertObjToStr(objReader.GetValue( "SYSTEM_TABLE_NAME"));
					objCat.CategoryName = Conversion.ConvertObjToStr(objReader.GetValue( "TABLE_NAME"));
					m_arrlstAvailAdminTables.Add(objCat);
					objElemTemp=objDOM.CreateElement("Table");
					objElemTemp.SetAttribute("CategoryDesc",Conversion.ConvertObjToStr(objReader.GetValue( "SYSTEM_TABLE_NAME")));
					objElemTemp.SetAttribute("CategoryName",Conversion.ConvertObjToStr(objReader.GetValue( "TABLE_NAME")));
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				return objDOM;
			}
			catch (XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.GetAvailableAdminTableList.XmlError",m_iClientId),p_objException);
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.GetAvailableAdminTableList.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
                objCat = null;
				sWhere=null;
				sSql=null;
			}
		}
		#endregion

		#region Function to retrieve the permission(s) on existing template
		/// Name		: GetUsedPermList
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the existing permission list on a template
		/// </summary>
		/// <returns>Xml containing Permission List</returns>
		public XmlDocument GetUsedPermList()
		{
			TemplatePerm objTemplatePerm=null;
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			bool bRead=false;
			DbReader objReader=null;
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("UsedTemplatePerm");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("TemplatePermItems");
				objDOM.FirstChild.AppendChild(objElemChild);
				sSQL = "SELECT * FROM MERGE_FORM_PERM WHERE FORM_ID = " + this.FormId.ToString() + " AND USER_ID = 0 AND GROUP_ID = 0";
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
				bRead=objReader.Read();
				if (bRead)
				{
					objElemTemp=objDOM.CreateElement("PermItem");
					objElemTemp.InnerText="All";
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
					return objDOM;
				}
				else
				{
					for (int i=0;i<m_arrlstColPerms.Count;i++)
					{
						objTemplatePerm=(TemplatePerm)m_arrlstColPerms[i];
						objElemTemp=objDOM.CreateElement("PermItem");
						objElemTemp.SetAttribute("DisplayName",objTemplatePerm.DisplayName.ToString());
						objElemTemp.SetAttribute("GroupId",objTemplatePerm.GroupId.ToString());
						objElemTemp.SetAttribute("IsGroup",objTemplatePerm.IsGroup.ToString());
						objElemTemp.SetAttribute("UserId",objTemplatePerm.UserId.ToString());
						objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
					}
					return objDOM;
				}
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.GetUsedFieldList.XmlError",m_iClientId),p_objException);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Template.GetUsedPermList.Error",m_iClientId),p_objException);
			}
			finally
			{ 			
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				sSQL=null;
			}
		}
		#endregion
	}
	
}

