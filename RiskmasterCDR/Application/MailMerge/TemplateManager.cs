﻿using System;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.Text;
using System.IO;
using System.Xml;
using Riskmaster.Application.FileStorage;
using Riskmaster.Security;
using Riskmaster.Settings;

namespace Riskmaster.Application.MailMerge
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   29th November 2004
	///Purpose :   This class Creates, Edits, Fetches template information.
	///**************************************************************
	///* Amendment  -->
	///  1.Date		: 7 Feb 06 
	///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
	///				  violation exception so it can be correctly called. 
	///    Author	: Sumit
	///**************************************************************
	/// </summary>
	public class TemplateManager:IManager
	{
		#region Constants

		/// <summary>
		/// Function IDs for User Document related security permission
		/// </summary>
		private const int MAILMG_SID = 10;
		private const int MAILMG_CREATE_SID = 11;
		private const int MAILMG_EDIT_SID = 12;
		private const int MAILMG_DELETE_SID = 13;
        /// <summary>
        /// Policy merge
        ///abansal23 : MITS 14951 : 3/25/2009
        ///6 is category id for "Policy Merge"
        /// </summary>
        private const int POLICY_MERGE = 6;
        /// <summary>
        /// Enhanced Policy merge
        ///abansal23 : MITS 14951 : 3/25/2009
        ///21 is category id for "Policy Mgmt Merge" 
        /// </summary>
        private const int ENHANCED_POLICY_MERGE = 21;
		#endregion

		#region Variable Declaration
		/// <summary>
		/// Template path
		/// </summary>
		private string m_sTemplatePath=""; 
		/// <summary>
		/// User id
		/// </summary>
		private string m_sUID=""; 
		/// <summary>
		/// Password
		/// </summary>
		private string m_sPwd=""; 
		/// <summary>
		/// Secure DB connection string
		/// </summary>
		private string m_sSecDSN=""; 
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// DSN Id
		/// </summary>
		private long m_lDSNID=0;
        private int m_iClientId = 0;
		/// <summary>
		/// Document path type
		/// </summary>
		private long m_lDocPathType=0; 
		/// <summary>
		/// 		/// Constant default FORM ID
		/// </summary>
		private const int DEFAULT_FORM_ID=-5000;
		/// <summary>
		/// Document storage DSN
		/// </summary>
		private string m_sDocStorageDSN="";
		/// <summary>
		/// Template object
		/// </summary>
		private Template m_objTemplate=null;
		/// <summary>		
		/// m_userLogin: Passed from Adapter and used to check permission 
		/// </summary>
		protected UserLogin m_userLogin;
		#endregion

		#region Property Declaration
		/// <summary>
		/// Secure DB DSN
		/// </summary>
		public string SecurityDSN
		{
			get
			{
				return m_sSecDSN;
			}
			set
			{	
				m_sSecDSN=value;
			}
		}
		/// <summary>
		/// Password
		/// </summary>
		public override string Pwd
		{
			get
			{
				return m_sPwd;
			}
			set
			{	
				m_sPwd=value;
			}
		}
		/// <summary>
		/// User ID
		/// </summary>
		public override string UID
		{
			get
			{
				return m_sUID;
			}
			set
			{	
				m_sUID=value;
			}
		}
		/// <summary>
		/// Connection string to database
		/// </summary>
		public override string DSN
		{
			get
			{
				return m_sDSN;
			}
			set
			{	
				m_sDSN=value;
			}
		}
		/// <summary>
		/// Template storage path
		/// </summary>
		public override string TemplatePath
		{
			get
			{
				return m_sTemplatePath;
			}
			set
			{	
				m_sTemplatePath=value;
				if (m_sTemplatePath != "" && !m_sTemplatePath.Substring(m_sTemplatePath.Length-1,1).Equals(@"\"))
				{
					m_sTemplatePath=m_sTemplatePath+@"\";
				}
			}
		}
		/// <summary>
		/// Template class object
		/// </summary>
		public Template CurTemplate
		{
			get
			{
				return m_objTemplate;
			}
		}
		/// <summary>
		/// Document path storage DSN
		/// </summary>
		public override string DocStorageDSN
		{
			get
			{
				return m_sDocStorageDSN;
			}
			set
			{	
				m_sDocStorageDSN=value;
			}
		}
		/// <summary>
		/// Documnet path type
		/// </summary>
		public override long DocPathType
		{
			get
			{
				return m_lDocPathType;
			}
			set
			{	
				m_lDocPathType=value;
				
			}
		}
		#endregion

		#region TemplateManager constructor
		/// <summary>
		/// TemplateManager class Overloaded constructor
		/// </summary>
        public TemplateManager(string p_sConnectString, UserLogin oLogin, int p_iClientId)
		{
			//
			// TODO: Add constructor logic here
			//
            m_iClientId = p_iClientId;
            m_objTemplate = new Template(oLogin, p_iClientId);
			m_objTemplate.Manager=this;
			m_userLogin = oLogin;
			m_sDSN = oLogin.objRiskmasterDatabase.ConnectionString;
		}

		/// <summary>
		/// TemplateManager class Overloaded constructor
		/// </summary>
        public TemplateManager(UserLogin oLogin, int p_iClientId)
		{
			//
			// TODO: Add constructor logic here
			//
            m_iClientId = p_iClientId;
            m_objTemplate = new Template(p_iClientId);
			m_objTemplate.Manager=this;
			m_userLogin = oLogin;
			m_sDSN = oLogin.objRiskmasterDatabase.ConnectionString;
		}
		#endregion

		#region TemplateManager destructor
		/// <summary>
		/// TemplateManager class destructor
		/// </summary>
		~TemplateManager()
		{
			m_objTemplate.Manager=null;
			m_objTemplate=null;
			m_sUID=null; 
			m_sPwd=null; 
			m_sSecDSN=null; 
			m_sDSN=null;
			m_sDocStorageDSN=null;
		}
		#endregion

		#region Function to retrieve document categories
		/// Name		: GetDocumentTypesClassesCategories
		/// Author		: Jasbinder Singh Bali
		/// Date Created: 06/30/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the Types, Classes and Categories of document
		/// </summary>
		/// <returns>Xml containing document categories</returns>
		public XmlDocument GetDocumentTypesClassesCategories()
		{
			
			XmlDocument objDOM		=	null;
			XmlElement objElemParent=	null;			

			XmlDocument objXmlDomTypes		=	GetDocumentTypes();
			XmlDocument objXmlDomClasses	=	GetDocumentClasses();
			XmlDocument objXmlDomCategories =	GetDocumentCategories();


			try
			{
				objDOM = new XmlDocument();

				objElemParent = objDOM.CreateElement("TypesClassesCategories");
				objDOM.AppendChild(objElemParent);
	
				XmlNode objNodeTypes = objDOM.ImportNode(objXmlDomTypes.DocumentElement,true);
				objDOM.FirstChild.AppendChild(objNodeTypes);

				XmlNode objNodeClasses = objDOM.ImportNode(objXmlDomClasses.DocumentElement,true);
				objDOM.FirstChild.AppendChild(objNodeClasses);

				XmlNode objNodeCategories = objDOM.ImportNode(objXmlDomCategories.DocumentElement,true);
				objDOM.FirstChild.AppendChild(objNodeCategories);
		
				return objDOM;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetDocumentCategories.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetDocumentCategories.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				objDOM=null;								
				objElemParent=null;
			}

		}
		#endregion

		#region Function to retrieve document categories
		/// Name		: GetCategoryListPrefabTemplateListServerUniqueFileName
		/// Author		: Jasbinder Singh Bali
		/// Date Created: 07/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the CategoryList, PrefabTemplateList and ServerUniqueFileName of document
		/// </summary>
		/// <returns>Xml containing document categories</returns>
		public XmlDocument GetCategoryListPrefabTemplateListServerUniqueFileName()
		{
			
			XmlDocument objDOM		=	null;
			XmlElement objElemParent=	null;			
			XmlDocument objXmlDomPrefabTemplateList		=	GetPrefabTemplateList();
			XmlDocument objXmlDomCategoryList			=	GetCategoryList();			
			XmlDocument objXmlDomServerUniqueFileName	=	GetServerUniqueFileName();
			XmlDocument ObjXmlDomLOBList				=   GetLOBList();
            XmlDocument ObjXmlDomMailRecipientList      =   GetMailRecipientList(); //spahariya 28867
            XmlNode objNodeCategoryList = null;
            XmlNode objNodePrefabTemplateList = null;
            XmlNode objNodeServerUniqueFileName = null;
            XmlNode objNodeLOBList = null;
            XmlNode objNodeMailRecipientList = null; //spahariya 28867
            //Shruti for 11412
            XmlElement objTemplateNames = null;
            string sSQL = "";
            DbReader objReader = null;
            int iPrevCatId = 0;
            int iCurCatId = 0;
			try
			{
				//Check if the user has Mail Merge create permission
				if(!m_userLogin.IsAllowedEx(MAILMG_CREATE_SID))
				{
					throw new PermissionViolationException(RMPermissions.RMO_CREATE, MAILMG_CREATE_SID);
				}

				objDOM = new XmlDocument();

				objElemParent = objDOM.CreateElement("CategoryListPrefabTemplateListServerUniqueFileName");
				objDOM.AppendChild(objElemParent);

                objTemplateNames = objDOM.CreateElement("TemplateNames");

				objNodeCategoryList = objDOM.ImportNode(objXmlDomCategoryList.DocumentElement,true);
				objDOM.FirstChild.AppendChild(objNodeCategoryList);

				objNodePrefabTemplateList = objDOM.ImportNode(objXmlDomPrefabTemplateList.DocumentElement,true);
				objDOM.FirstChild.AppendChild(objNodePrefabTemplateList);

				objNodeServerUniqueFileName = objDOM.ImportNode(objXmlDomServerUniqueFileName.DocumentElement,true);
				objDOM.FirstChild.AppendChild(objNodeServerUniqueFileName);

				objNodeLOBList = objDOM.ImportNode(ObjXmlDomLOBList.DocumentElement,true);
				objDOM.FirstChild.AppendChild(objNodeLOBList);
                //spahariya MITS 28867
                objNodeMailRecipientList = objDOM.ImportNode(ObjXmlDomMailRecipientList.DocumentElement, true);
                objDOM.FirstChild.AppendChild(objNodeMailRecipientList);

                //Shruti for 11412
                sSQL = " SELECT CAT_ID,FORM_NAME FROM MERGE_FORM ORDER BY CAT_ID";
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);
                while (objReader.Read())
                {
                    iCurCatId = objReader.GetInt32("CAT_ID");
                    if (iCurCatId != iPrevCatId)
                    {
                        if (objTemplateNames.InnerText == string.Empty)
                        {
                            objTemplateNames.InnerText = objReader.GetValue("CAT_ID").ToString() + "^" +
                                                       objReader.GetValue("FORM_NAME").ToString();
                        }
                        else
                        {
                            objTemplateNames.InnerText = objTemplateNames.InnerText + ";" + objReader.GetValue("CAT_ID").ToString() + "^" +
                                                       objReader.GetValue("FORM_NAME").ToString();
                        }
                    }
                    else
                    {
                        objTemplateNames.InnerText = objTemplateNames.InnerText + "," + objReader.GetValue("FORM_NAME").ToString();
                    }
                    iPrevCatId = iCurCatId;
                }
                objDOM.DocumentElement.AppendChild(objTemplateNames);
                //Shruti for 11412 ends

				return objDOM;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCategoryListPrefabTemplateListServerUniqueFileName.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCategoryListPrefabTemplateListServerUniqueFileName.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				objDOM=null;								
				objElemParent=null;
                objXmlDomPrefabTemplateList = null;
                objXmlDomCategoryList = null;
                objXmlDomServerUniqueFileName = null;
                ObjXmlDomLOBList = null;
                objNodeCategoryList = null;
                objNodePrefabTemplateList = null;
                objNodeServerUniqueFileName = null;
                objNodeLOBList = null;

			}

		}
		#endregion


		#region Function to retrieve document categories
		/// Name		: GetDocumentCategories
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the Categories of document
		/// </summary>
		/// <returns>Xml containing document categories</returns>
		public XmlDocument GetDocumentCategories()
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			long lID=0;
			DbReader objReader=null;
			string sCode="";
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("DocumentCategory");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("Categories");
				objDOM.FirstChild.AppendChild(objElemChild);
				sSQL=" select table_id from GLOSSARY "
					+" where SYSTEM_TABLE_NAME ='DOCUMENT_CATEGORY'";
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				if (objReader.Read())
				{
					lID=Conversion.ConvertStrToLong(
						Conversion.ConvertObjToStr(objReader.GetValue(0)));
				}
				objReader.Close();
				/*sSQL=" select CODE_ID,SHORT_CODE,CODE_DESC from CODES_TEXT "
					+" where  "
					+" code_id in (select code_id from codes "
					+ " where "
                    + " TABLE_ID=" + lID.ToString() + ")  ORDER BY SHORT_CODE, CODE_DESC";*/
                //abisht MITS 10632 commented the above code and written a changed query so that the deleted code are not selected.

                //Asharma326 MITS 33772 Starts
                //eDatabaseType edt = DbFactory.GetDatabaseType(m_sDSN);
                //string DateforDB = "";
                //if (edt == eDatabaseType.DBMS_IS_SQLSRVR)
                //{
                //    DateforDB = "GETDATE()";
                //}
                //else { DateforDB = " TO_CHAR(sysdate,'YYYYMMDD')"; }

                //Asharma326 MITS 33772 Ends
                sSQL = " SELECT CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC "
                     + " FROM "
                        + " CODES,CODES_TEXT "
                        + " WHERE "
                        + " CODES.CODE_ID = CODES_TEXT.CODE_ID "
                        + " AND CODES_TEXT.CODE_ID IN (SELECT CODES.CODE_ID FROM CODES WHERE TABLE_ID =" + lID.ToString() + " )"
                    //+ " AND ( CODES.EFF_END_DATE > " + DateforDB //nnithiyanand 34072 - Used NULLIF to check NULL strings in EFF_END_DATE
                        + "AND ( NULLIF(CODES.EFF_END_DATE,'NULL')  > " + DateTime.Now.ToString("yyyyMMdd")
                        + " OR CODES.EFF_END_DATE IS NULL )"// //asingh263 mits 32934
                        + " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ORDER BY CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC";
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);
				while (objReader.Read())
				{
					objElemTemp=objDOM.CreateElement("Category");
					objElemTemp.SetAttribute("codeid",Conversion.ConvertObjToStr(objReader.GetValue( "CODE_ID")));
					sCode=Conversion.ConvertObjToStr(objReader.GetValue( "SHORT_CODE"))+
						" -"+Conversion.ConvertObjToStr(objReader.GetValue( "CODE_DESC"));
					objElemTemp.SetAttribute("description",sCode);
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				objReader.Close();
				return objDOM;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetDocumentCategories.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetDocumentCategories.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				sSQL=null;
				sCode=null;
			}
		}
		#endregion


		#region Function to retrieve document categories
		/// Name		: GetDocumentCategories
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the Categories of document
		/// </summary>
		/// <returns>Xml containing document categories</returns>
		public XmlDocument GetServerUniqueFileName()
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("FileName");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("UniqueFileName");
				objDOM.FirstChild.AppendChild(objElemChild);
				
				objElemTemp=objDOM.CreateElement("ServerUniqueFileName");
                objElemTemp.SetAttribute("FileName", Generic.GetServerUniqueFileName("mmt", "doc", m_iClientId));
				objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				return objDOM;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetDocumentCategories.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetDocumentCategories.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				
			}
		}
		#endregion

		#region Function to retrieve document classes
		/// Name		: GetDocumentClasses
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the class of document
		/// </summary>
		/// <returns>Xml containing document classes</returns>
		public XmlDocument GetDocumentClasses()
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			long lID=0;
			DbReader objReader=null;
			string sCode="";
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("DocumentClass");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("Classes");
				objDOM.FirstChild.AppendChild(objElemChild);
				sSQL=" select table_id from GLOSSARY "
					+" where SYSTEM_TABLE_NAME ='DOCUMENT_CLASS'";
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				if (objReader.Read())
				{
					lID=Conversion.ConvertStrToLong(
						Conversion.ConvertObjToStr(objReader.GetValue(0)));
				}
				objReader.Close();
				/*sSQL=" select CODE_ID,SHORT_CODE,CODE_DESC from CODES_TEXT "
					+" where  "
					+" code_id in (select code_id from codes "
					+ " where "
					+" TABLE_ID="+lID.ToString()+") ORDER BY SHORT_CODE, CODE_DESC";*/
                //abisht MITS 10632 commented the above code and written a changed query so that the deleted code are not selected.

                //Asharma326 MITS 33772 Starts
                //eDatabaseType edt = DbFactory.GetDatabaseType(m_sDSN);
                //string DateForDB = "";
                //if (edt == eDatabaseType.DBMS_IS_SQLSRVR)
                //{
                //    DateForDB = "GETDATE()";
                //}
                //else { DateForDB = " TO_CHAR(sysdate,'YYYYMMDD')"; } 
                //Asharma326 MITS 33772 Ends
                sSQL = " SELECT CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC "
                   + " FROM "
                        + " CODES,CODES_TEXT "
                        + " WHERE "
                        + " CODES.CODE_ID = CODES_TEXT.CODE_ID "
                        + " AND CODES_TEXT.CODE_ID IN (SELECT CODES.CODE_ID FROM CODES WHERE TABLE_ID =" + lID.ToString() + " )"
                        + "AND ( NULLIF(CODES.EFF_END_DATE,'NULL')  > " + DateTime.Now.ToString("yyyyMMdd")
                        + " OR CODES.EFF_END_DATE IS NULL )" // //asingh263 mits 32934,
                        + " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ORDER BY CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC";
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);

				while (objReader.Read())
				{
					objElemTemp=objDOM.CreateElement("Class");
					objElemTemp.SetAttribute("codeid",Conversion.ConvertObjToStr(objReader.GetValue( "CODE_ID")));
					sCode=Conversion.ConvertObjToStr(objReader.GetValue( "SHORT_CODE"))+
						" -"+Conversion.ConvertObjToStr(objReader.GetValue( "CODE_DESC"));
					objElemTemp.SetAttribute("description",sCode);
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				objReader.Close();
				return objDOM;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetDocumentClasses.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetDocumentClasses.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				sSQL=null;
				sCode=null;
			}
		}
		#endregion

		#region Function to retrieve document types
		/// Name		: GetDocumentTypes
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the type of document
		/// </summary>
		/// <returns>Xml containing document types</returns>
		public XmlDocument GetDocumentTypes()
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			long lID=0;
			DbReader objReader=null;
			string sCode="";
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("DocumentType");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("Types");
				objDOM.FirstChild.AppendChild(objElemChild);
				sSQL=" select table_id from GLOSSARY "
					+" where SYSTEM_TABLE_NAME ='DOCUMENT_TYPE'";
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				if (objReader.Read())
				{
					lID=Conversion.ConvertStrToLong(
						Conversion.ConvertObjToStr(objReader.GetValue(0)));
				}
				objReader.Close();
				/*sSQL=" select CODE_ID,SHORT_CODE,CODE_DESC from CODES_TEXT "
					+" where  "
					+" code_id in (select code_id from codes "
					+ " where "
					+" TABLE_ID="+lID.ToString()+") ORDER BY SHORT_CODE, CODE_DESC";*/
                //abisht MITS 10632 commented the above code and written a changed query so that the deleted code are not selected.
                //Asharma326 MITS 33772 Starts
                //eDatabaseType edt = DbFactory.GetDatabaseType(m_sDSN);

                //string DateForDB = "";
                //if (edt == eDatabaseType.DBMS_IS_SQLSRVR)
                //{
                //    DateForDB = "GETDATE()";
                //}
                //else { DateForDB = " TO_CHAR(sysdate,'YYYYMMDD')"; } 
                //Asharma326 MITS 33772 Ends
                sSQL = " SELECT CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC "
                    + " FROM "
                        + " CODES,CODES_TEXT "
                        + " WHERE "
                        + " CODES.CODE_ID = CODES_TEXT.CODE_ID "
                        + " AND CODES_TEXT.CODE_ID IN (SELECT CODES.CODE_ID FROM CODES WHERE TABLE_ID =" + lID.ToString() + " )"
                        + "AND ( NULLIF(CODES.EFF_END_DATE,'NULL')  > " + DateTime.Now.ToString("yyyyMMdd")
                        + " OR CODES.EFF_END_DATE IS NULL )"// //asingh263 mits 32934
                        + " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ORDER BY CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC";
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);
				while (objReader.Read())
				{
					objElemTemp=objDOM.CreateElement("Type");
					objElemTemp.SetAttribute("codeid",Conversion.ConvertObjToStr(objReader.GetValue( "CODE_ID")));
					sCode=Conversion.ConvertObjToStr(objReader.GetValue( "SHORT_CODE"))+
						" -"+Conversion.ConvertObjToStr(objReader.GetValue( "CODE_DESC"));
					objElemTemp.SetAttribute("description",sCode);
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				objReader.Close();
				return objDOM;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetDocumentTypes.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetDocumentTypes.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				sSQL=null;
				sCode=null;
			}
		}
		#endregion

        private StorageType GetStorageType(long p_iStorageType)
        {
            switch (p_iStorageType)
            {
                case 0:
                    return StorageType.FileSystemStorage;
                case 1:
                    return StorageType.DatabaseStorage;
                case 2:
                    return StorageType.S3BucketStorage;
                default:
                    return StorageType.FileSystemStorage;
            }
        }
		#region Function to retrieve a File
		/// <summary>
		/// Saves a file
		/// </summary>
		/// <param name="p_sFilename">File to save</param>
		/// <param name="p_objMemory">MemoryStream containing the file</param>
		/// <returns>Success-True Failure- False</returns>
		public bool SaveFile(string p_sFilename,MemoryStream p_objMemory)
			
		{
			FileStorageManager objFileStorageManager=null;
			bool bExists=false;
			try
			{
                if (m_lDocPathType == 1 || m_lDocPathType == 2)
				{
					objFileStorageManager=new FileStorageManager(GetStorageType(m_lDocPathType),m_sDocStorageDSN,m_iClientId);
					objFileStorageManager.FileExists(p_sFilename,out bExists);
					if (bExists)
					{
						objFileStorageManager.DeleteFile(p_sFilename);
						objFileStorageManager.StoreFile(p_objMemory,p_sFilename);
					}
				}
				else 
				{
					objFileStorageManager=new FileStorageManager(StorageType.FileSystemStorage,m_sDocStorageDSN,m_iClientId);
					objFileStorageManager.FileExists(p_sFilename,out bExists);
					if (bExists)
					{
						objFileStorageManager.DeleteFile(p_sFilename);
						objFileStorageManager.StoreFile(p_objMemory,p_sFilename);
						return true;
					}
				}
				return false;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.SaveFile.Error",m_iClientId),p_objException);
			}
			finally
			{
				objFileStorageManager=null;
			}
		}
		#endregion
		#region Function to retrieve a File
		/// <summary>
		/// Retrieve a file
		/// </summary>
		/// <param name="p_objMemory">Memorystream object</param>
		/// <returns>Success-True Failure- False</returns>
		public bool GetFile(string p_sFilename,out MemoryStream p_objMemory)
			
		{
			FileStorageManager objFileStorageManager=null;
			string sFileName="";
			p_objMemory=null;
            string sPath = RMConfigurator.UserDataPath;

			try
			{
                if (m_lDocPathType == 1 || m_lDocPathType == 2)
                {
                    bool bExists = false;
                    objFileStorageManager = new FileStorageManager(GetStorageType(m_lDocPathType), m_sDocStorageDSN, m_iClientId);
					objFileStorageManager.FileExists(p_sFilename,out bExists);
					if (bExists)
					{
                        //Change by kuladeep for mits:29824 Start
                        //objFileStorageManager.RetrieveFile(p_sFilename,TemplatePath+p_sFilename,out sFileName);
                        objFileStorageManager.RetrieveFile(p_sFilename, TemplatePath + p_sFilename, m_userLogin.LoginName, out sFileName);
                        //Change by kuladeep for mits:29824 End

                        p_objMemory = Utilities.ConvertFilestreamToMemorystream(TemplatePath + sFileName, m_iClientId);
						return true;
					}
					//MITS 8411	
					// other option are tried anyways so moved the code down as 
					// they should be tried for regular stoarage or or acrosoft as well
				}
				else if (File.Exists(TemplatePath + p_sFilename))
				{
                    p_objMemory = Utilities.ConvertFilestreamToMemorystream(TemplatePath + p_sFilename, m_iClientId);
					return true;
				} 
				
				//MITS 8411	
				// other option are tried anyways
				if ( File.Exists(sPath+"//MailMerge//"+p_sFilename))
				{
                    p_objMemory = Utilities.ConvertFilestreamToMemorystream(sPath + "//MailMerge//" + p_sFilename, m_iClientId);
					return true;
				}
				else if ( File.Exists(Path.GetTempPath()+p_sFilename))
				{
                    p_objMemory = Utilities.ConvertFilestreamToMemorystream(Path.GetTempPath() + p_sFilename, m_iClientId);
					return true;
				}

				return false;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetFile.Error",m_iClientId),p_objException);
			}
			finally
			{
				objFileStorageManager=null;
			}
		}
		#endregion
		
		#region Function to retrieve a RTF File
		/// <summary>
		/// Retrieve a file
		/// </summary>
		/// <param name="p_objMemory">string object</param>
		/// <returns>Success-True Failure- False</returns>
		public bool GetFileRTF(string p_sFilename,out string p_objMemory)
			
		{
			FileStorageManager objFileStorageManager=null;
			string sFileName="";
			p_objMemory=null;
			SysSettings objSettings = null;
			try
			{
				//08/10/2006.. Raman Bhatia : If Acrosoft is enabled then legacy document management is not used
				//Instead store attachment using Acrosoft
				
				objSettings = new SysSettings(m_sDSN,m_iClientId);
                
				if(objSettings.UseAcrosoftInterface==false && objSettings.UseMediaViewInterface == false)
				{
                    if (m_lDocPathType == 1 || m_lDocPathType == 2)
				    {
						bool bExists=false;
                        objFileStorageManager = new FileStorageManager(GetStorageType(m_lDocPathType), m_sDocStorageDSN, m_iClientId);
						objFileStorageManager.FileExists(p_sFilename,out bExists);
						if (bExists)
						{
                            //Change by kuladeep for mits:29824 Start
                            //objFileStorageManager.RetrieveFile(p_sFilename,"",out sFileName);
                            objFileStorageManager.RetrieveFile(p_sFilename, "", m_userLogin.LoginName, out sFileName);
                            //Change by kuladeep for mits:29824 End
					
							//p_objMemory=Utilities.ConvertFilestreamToMemorystream(TemplatePath+sFileName);
                            p_objMemory = Utilities.ReadFileStream(sFileName, m_iClientId);
							return true;
						}
						else
						{
							if ( File.Exists(Path.GetTempPath()+p_sFilename))
							{
                                p_objMemory = Utilities.ReadFileStream(Path.GetTempPath() + p_sFilename, m_iClientId);
								return true;
							}
						}
					}
					else if (File.Exists(TemplatePath + p_sFilename))
					{
						//p_objMemory=Utilities.ConvertFilestreamToMemorystream(TemplatePath+p_sFilename);
                        p_objMemory = Utilities.ReadFileStream(TemplatePath + p_sFilename, m_iClientId);
						return true;
					}
				}
				else
				{
					if ( File.Exists(Path.GetTempPath()+p_sFilename))
					{
                        p_objMemory = Utilities.ReadFileStream(Path.GetTempPath() + p_sFilename, m_iClientId);
						return true;
					}
				} // else

				return false;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetFile.Error",m_iClientId),p_objException);
			}
			finally
			{
				objFileStorageManager=null;
			}
		}
		#endregion
		#region Function to retrieve templates
		/// Name		: GetCustomTemplateList
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns custom templates list
		/// </summary>
		/// <returns>Xml containing the templates list</returns>
		public XmlDocument GetCustomTemplateList()
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			XmlElement objNewChild=null;
			string sPrevious="";
			string sSQL="";
			DbReader objReader=null;
			long lTemp=0;
			string sCatDesc="";
			try
			{
				//Check if the user has Mail Merge permission
				if(!m_userLogin.IsAllowedEx(MAILMG_SID))
				{
					throw new PermissionViolationException(RMPermissions.RMO_ACCESS, MAILMG_SID);
				}

				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("CustomTemplateList");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("Templates");
				objDOM.FirstChild.AppendChild(objElemChild);
                //aaggarwal29:start Merge for asharma326 MITS 36623
                //MITS 35336 starts
                //sSQL=" SELECT FORM_ID,FORM_NAME,FORM_DESC,CAT_NAME,MERGE_FORM.CAT_ID, MERGE_FORM.TABLE_QUALIFY FROM MERGE_FORM,MERGE_CAT WHERE MERGE_FORM.CAT_ID = MERGE_CAT.CAT_ID ORDER BY MERGE_FORM.CAT_ID,MERGE_FORM.FORM_NAME";
                sSQL = "SELECT DISTINCT MERGE_FORM.FORM_ID,MERGE_FORM.FORM_NAME,MERGE_FORM.FORM_DESC,MERGE_CAT.CAT_NAME,MERGE_FORM.CAT_ID,";
                sSQL = sSQL + "MERGE_FORM.TABLE_QUALIFY FROM MERGE_FORM inner join MERGE_CAT on MERGE_FORM.CAT_ID = MERGE_CAT.CAT_ID  inner join MERGE_FORM_PERM ";
                //sSQL = sSQL + " on MERGE_FORM.FORM_ID= MERGE_FORM_PERM.FORM_ID and MERGE_FORM_PERM.USER_ID in (0," + m_userLogin.UserId+ ")   ORDER BY  MERGE_FORM.CAT_ID,MERGE_FORM.FORM_NAME";
                sSQL = sSQL + " on MERGE_FORM.FORM_ID= MERGE_FORM_PERM.FORM_ID AND (MERGE_FORM_PERM.USER_ID = " + m_userLogin.UserId + " OR MERGE_FORM_PERM.GROUP_ID= " + m_userLogin.GroupId + " OR (MERGE_FORM_PERM.USER_ID = 0 AND MERGE_FORM_PERM.GROUP_ID=0) )   ORDER BY  MERGE_FORM.CAT_ID,MERGE_FORM.FORM_NAME";
                ////MITS 35336 ends
                //aaggarwal29: end Merge for asharma326 MITS 36623
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				while (objReader.Read())
				{
					if (Conversion.ConvertObjToStr(objReader.GetValue( "CAT_ID")).Equals("20"))
					{
						sCatDesc=Generic.GetSingleString("TABLE_NAME", "GLOSSARY, GLOSSARY_TEXT", "GLOSSARY.SYSTEM_TABLE_NAME = '"+Conversion.ConvertObjToStr(objReader.GetValue( "TABLE_QUALIFY"))+"' AND GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID ",m_sDSN, m_iClientId)+ " Merge";
						
					}
					else
					{
						sCatDesc=Conversion.ConvertObjToStr(objReader.GetValue( "CAT_NAME"));
						
					}
					if (!sPrevious.Equals(sCatDesc))
					{
						if (lTemp!=0)
						{
							objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
						}
						objElemTemp=objDOM.CreateElement("Category");
						objElemTemp.SetAttribute("Catid",Conversion.ConvertObjToStr(objReader.GetValue( "CAT_ID")));
						objElemTemp.SetAttribute("CategoryDesc",sCatDesc);
					}
					sPrevious=sCatDesc;
					objNewChild=objDOM.CreateElement("Template");
					objNewChild.SetAttribute("Category",Conversion.ConvertObjToStr(objReader.GetValue( "CAT_ID")));
					objNewChild.SetAttribute("TemplateDesc",Conversion.ConvertObjToStr(objReader.GetValue( "FORM_NAME")));
					objNewChild.SetAttribute("TemplateId",Conversion.ConvertObjToStr(objReader.GetValue( "FORM_ID")));
					objElemTemp.AppendChild((XmlNode)objNewChild);
					lTemp++;
				}
				if (lTemp > 0)
				{
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				objReader.Close();

				return objDOM;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCustomTemplateList.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCustomTemplateList.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDOM=null;
				objNewChild=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				sPrevious=null;
				sSQL=null;
				sCatDesc=null;
			}
			
		}
		#endregion

		#region Function to retrieve distinct tables related to passed category
		/// Name		: GetFieldDisplayCategoryList
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns distinct tables related to passed category
		/// </summary>
		/// <returns>Xml containing distinct tables related to passed category</returns>
		public XmlDocument GetFieldDisplayCategoryList(long p_lCatId)
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			DbReader objReader=null;
            string sWhere = "";
            SysSettings sysSettings ;
            ColLobSettings objLobSettings = null;

            bool bIsEnhPolicyEnabled = false;
            bool bIsPolicyTrackingEnabled = false;
			try
			{
                //gagnihotri 09/15/2009 MITS 17813
                //To restrict merge fields either from Poilcy Tracking supplementals or from 
                //Policy Management supplementals in claim merge templates.
                sWhere = " WHERE CAT_ID=" + p_lCatId.ToString();
                sysSettings = new SysSettings(m_sDSN,m_iClientId);//Parijat : Chubb Mail merge generic fields changes 
                objLobSettings = new ColLobSettings(m_sDSN, m_iClientId);
                bIsEnhPolicyEnabled = objLobSettings[241].UseEnhPolFlag == -1 && objLobSettings[242].UseEnhPolFlag == -1 && objLobSettings[243].UseEnhPolFlag == -1 && objLobSettings[845].UseEnhPolFlag == -1;
                bIsPolicyTrackingEnabled = objLobSettings[241].UseEnhPolFlag != -1 && objLobSettings[242].UseEnhPolFlag != -1 && objLobSettings[243].UseEnhPolFlag != -1 && objLobSettings[845].UseEnhPolFlag != -1;
                if (p_lCatId == 1 && bIsEnhPolicyEnabled )
                {
                    //sWhere = sWhere + " AND FIELD_TABLE != 'POLICY_SUPP'";
                    sWhere = sWhere + " AND FIELD_TABLE != 'POLICY_SUPP' AND FIELD_TABLE != 'INSURER_ENTITY'  AND FIELD_TABLE != 'POLICY_X_INSURER'  AND FIELD_TABLE != 'REINSURER_ENTITY'  AND FIELD_TABLE != 'POLICY_X_INS_REINS'";//pmittal5 Mits 19142
                }
                else if (p_lCatId == 1 && bIsPolicyTrackingEnabled)
                {
                    sWhere = sWhere + " AND FIELD_TABLE != 'POLICY_ENH_SUPP'";
                    sWhere = sWhere + " AND FIELD_TABLE != 'INSURER_ENTITY_ENH'"; //pmittal5 Mits 19142
                }
                //pmittal5 Mits 19142 12/15/09 - Dont show Insurer and Reinsurer fields if corresponding settings are not enabled for Policy Tracking and Claim Merge
                if (bIsPolicyTrackingEnabled)
                {
                    if ((p_lCatId == 6 || p_lCatId == 1) && sysSettings.MultipleInsurer == false)
                    {
                        sWhere = sWhere + " AND FIELD_TABLE != 'INSURER_ENTITY' AND FIELD_TABLE != 'POLICY_X_INSURER'";
                    }
                    if ((p_lCatId == 6 || p_lCatId == 1) && sysSettings.MultipleReInsurer == false)
                    {
                        sWhere = sWhere + " AND FIELD_TABLE != 'REINSURER_ENTITY' AND FIELD_TABLE != 'POLICY_X_INS_REINS'";
                    }
                }
                //End - pmittal5
                //Added Rakhi For R7:Add Emp Data Elements:For Preventing Addresses to be loaded in Category if setting is off
                if (!sysSettings.UseMultipleAddresses)
                {
                    switch (p_lCatId)
                    {
                        case 3:
                        case 4:
                        case 8:
                        case 9:
                        case 10:
                          sWhere = sWhere + " AND FIELD_TABLE != 'ENTITY_X_ADDRESSES'";
                          break;
                    }
                }
                //Added Rakhi For R7:Add Emp Data Elements:For Preventing Addresses to be loaded in Category if setting is off
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("GetFieldDisplayCategoryList");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("Categories");
				objDOM.FirstChild.AppendChild(objElemChild);
				//sSQL="SELECT DISTINCT DISPLAY_CAT FROM MERGE_DICTIONARY WHERE CAT_ID=" + p_lCatId.ToString() + " ORDER BY DISPLAY_CAT";
                sSQL = "SELECT DISTINCT DISPLAY_CAT FROM MERGE_DICTIONARY " + sWhere + " ORDER BY DISPLAY_CAT";
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				while (objReader.Read())
				{
					objElemTemp=objDOM.CreateElement("Category");
					objElemTemp.SetAttribute("CategoryName",Conversion.ConvertObjToStr(objReader.GetValue( "DISPLAY_CAT")));
                    //Parijat : Chubb Mail merge generic fields changes 
                    if (!(!(sysSettings.ClaimLetterFlag) && (Conversion.ConvertObjToStr(objReader.GetValue("DISPLAY_CAT")) == "Acknowledgement Fields" || Conversion.ConvertObjToStr(objReader.GetValue("DISPLAY_CAT")) == "Current Adjuster")))
					    objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
                   // Parijat : End Chubb Mail merge generic fields changes 
				}
				objReader.Close();
				return objDOM;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetFieldDisplayCategoryList.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetFieldDisplayCategoryList.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
                sSQL = null;
                sysSettings = null;
                sWhere = null;
			}
		}
		#endregion

		#region Function to add a new template
		/// Name		: AddNew
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function deletes a template
		/// </summary>
		/// <param name="p_lFormID">Template id</param>
		/// <summary>
		/// This function adds a new template
		/// </summary>
		public void AddNew()
		{
			try
			{
				//Check if the user has Mail Merge create permission
				if(!m_userLogin.IsAllowedEx(MAILMG_CREATE_SID))
				{
					throw new PermissionViolationException(RMPermissions.RMO_CREATE, MAILMG_CREATE_SID);
				}

				m_objTemplate.Manager=null;
				m_objTemplate=new Template(m_userLogin,m_lDocPathType,m_sDocStorageDSN,m_iClientId);
				m_objTemplate.Manager=this;
				m_objTemplate.FormId=DEFAULT_FORM_ID;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.AddNew.Error",m_iClientId),p_objException);
			}
		}
		#endregion

		#region Function to delete a template
		/// Name		: DeleteTemplate
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function deletes a template
		/// </summary>
		/// <param name="p_lFormID">Template id</param>
		public bool DeleteTemplate(long p_lFormID)
		{
			string sSql="";
			DbConnection objConn=null;
			DbCommand objCommand=null;
			DbTransaction objTran=null;
			bool blnSuccess=false;
			try
			{
				//Check if the user has Mail Merge delete permission
				if(!m_userLogin.IsAllowedEx(MAILMG_DELETE_SID))
				{
					throw new PermissionViolationException(RMPermissions.RMO_DELETE, MAILMG_DELETE_SID);
				}

				objConn = DbFactory.GetDbConnection(m_sDSN);
				objConn.Open();
				objCommand=objConn.CreateCommand();
				objTran=objConn.BeginTransaction();
				objCommand.Transaction=objTran;
				sSql = "DELETE FROM MERGE_FORM_DEF WHERE FORM_ID= " +p_lFormID;
				objCommand.CommandText=sSql;
				objCommand.ExecuteNonQuery();
				sSql = "DELETE FROM MERGE_FORM_PERM WHERE FORM_ID= " +p_lFormID;
				objCommand.CommandText=sSql;
				objCommand.ExecuteNonQuery ();
				sSql = "DELETE FROM MERGE_FORM WHERE FORM_ID= " +p_lFormID;
				objCommand.CommandText=sSql;
				objCommand.ExecuteNonQuery ();
				objTran.Commit();
				objConn.Close();
				blnSuccess=true;
			}
			catch(RMAppException p_objRMAppException)
			{
				if( objTran != null )
					objTran.Rollback();
				throw p_objRMAppException;
			}
			catch (Exception p_objException)
			{
				if( objTran != null )
					objTran.Rollback();
				throw new RMAppException(Globalization.GetString("TemplateManager.DeleteTemplate.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objConn != null)
				{
					objConn.Close();
					objConn.Dispose();
				}
                if (objTran != null)
                {
                    objTran.Dispose();
                }
				objCommand=null;
				sSql=null;
			}
			return blnSuccess;
		}
		#endregion

		#region Function to list available templates list
		/// Name		: GetPrefabTemplateList
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the list of all available templates list
		/// </summary>
		/// <returns>Xml containing Template List</returns>
		public XmlDocument GetPrefabTemplateList()
		{
			XmlDocument	objDOM			=	null;
			XmlElement	objElemParent	=	null;
			XmlElement	objElemChild	=	null;
			XmlElement	objElemTemp		=	null;
			string		sTmp			=	"";
			string		sLine			=	"";
			StreamReader	objReader	=	null;
			int			iIndex			=	0;
			string		sFileName		=	"";
			bool		blnExists		=	false;

			FileStorageManager objFileStorageManager=null;
			try
			{
//				if (Conversion.ConvertLongToBool(m_lDocPathType))
//				{
//					objFileStorageManager=new FileStorageManager(StorageType.DatabaseStorage,m_sDocStorageDSN);
//					sFileName="";
//					objFileStorageManager.FileExists("mergemaster.def",out blnExists);
//					if (blnExists)
//					{
//						objFileStorageManager.RetrieveFile("mergemaster.def",TemplatePath +"mergemaster.def",out sFileName);
//					}
//					objFileStorageManager=null;
//				}
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("GetPrefabTemplateList");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("PrefabTemplates");
				objDOM.FirstChild.AppendChild(objElemChild);

                string sPath = RMConfigurator.UserDataPath;
                sTmp = String.Format("{0}\\MailMerge\\template\\mergemaster.def", sPath);
				if (Directory.Exists(sPath + "\\MailMerge\\template") && File.Exists(sTmp))
				{
					objReader=File.OpenText(sTmp);
					sLine=objReader.ReadLine();
					while(sLine!=null)
					{
						iIndex=sLine.IndexOf(Convert.ToChar("\t"));
						objElemTemp=objDOM.CreateElement("Template");
						objElemTemp.SetAttribute("PrefabTemplateDesc",sLine.Substring(0,iIndex));
						objElemTemp.SetAttribute("PrefabTemplateFile",sLine.Substring(iIndex+1));
						sLine=objReader.ReadLine();
						objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
					}
					objReader.Close();
					
				}
				return objDOM;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetPrefabTemplateList.XmlError",m_iClientId),p_objException);
			}
			catch (FileNotFoundException p_objException)
			{
				throw new RMAppException("TemplateManager.GetPrefabTemplateList.FileNotFoundError",p_objException);
			}
			catch (IOException p_objException)
			{
				throw new RMAppException("TemplateManager.GetPrefabTemplateList.FileInputOutputError",p_objException);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetPrefabTemplateList.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				if (objReader != null)
				{
					objReader.Close();
                    objReader.Dispose();
				}
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				sTmp=null;
				sLine=null;
			}
		}
		#endregion

		#region Function to retrieve category list
		/// Name		: GetCategoryList
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the categories list
		/// </summary>
		/// <returns>Xml containing CategoryList</returns>
		public XmlDocument GetCategoryList()
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			DbReader objReader=null;
            //Start-Mridul Bansal. 01/12/10. MITS#18229.
            bool bEnhPolActiveForGL = false;
            bool bEnhPolActiveForAL = false;
            bool bEnhPolActiveForPC = false;
            bool bEnhPolActiveForWC = false;
            ColLobSettings objLobSettings = null;
            int iCatId = 0;
            //End-Mridul Bansal. 01/12/10. MITS#18229.
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("GetCategoryList");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("Categories");
                //Start-Mridul Bansal. 01/12/10. MITS#18229.
                //abansal23 : MITS 14951 : 3/25/2009 START
                //Issues In Mail Merge
                //bool bUseEnhPolicy = false;
                //SysSettings sysSettings = new SysSettings(m_sDSN);
                //bUseEnhPolicy = Conversion.ConvertObjToBool(sysSettings.UseEnhPolFlag);
                //abansal23 : MITS 14951 : 3/25/2009 END
                objLobSettings = new ColLobSettings(m_sDSN, m_iClientId);
                bEnhPolActiveForAL = objLobSettings[242].UseEnhPolFlag == -1 ? true : false;
                bEnhPolActiveForGL = objLobSettings[241].UseEnhPolFlag == -1 ? true : false;
                bEnhPolActiveForPC = objLobSettings[845].UseEnhPolFlag == -1 ? true : false;
                bEnhPolActiveForWC = objLobSettings[243].UseEnhPolFlag == -1 ? true : false;
                //End-Mridul Bansal. 01/12/10. MITS#18229.

				objDOM.FirstChild.AppendChild(objElemChild);
				sSQL=" SELECT CAT_DESC, CAT_NAME,CAT_ID FROM MERGE_CAT  ORDER BY CAT_ID";
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				while (objReader.Read())
				{
                    //Start-Mridul Bansal. 01/12/10. MITS#18229.
                    //abansal23 : MITS 14951 : 3/25/2009 -Added if condition
                    //21 is categoer id for "Policy Mgmt Merge" and 6 is categoer id for "Policy Merge"
                    //if ((bUseEnhPolicy == true && Conversion.ConvertObjToInt(objReader.GetValue("CAT_ID"), m_iClientId) != POLICY_MERGE) || (bUseEnhPolicy == false && Conversion.ConvertObjToInt(objReader.GetValue("CAT_ID"), m_iClientId) != ENHANCED_POLICY_MERGE))
                    iCatId = Conversion.ConvertObjToInt(objReader.GetValue("CAT_ID"), m_iClientId);
                    if (!((iCatId == POLICY_MERGE && bEnhPolActiveForAL && bEnhPolActiveForGL && bEnhPolActiveForPC && bEnhPolActiveForWC) || (iCatId == ENHANCED_POLICY_MERGE && !bEnhPolActiveForAL && !bEnhPolActiveForGL && !bEnhPolActiveForPC && !bEnhPolActiveForWC)))
                    {
                        objElemTemp = objDOM.CreateElement("Category");
                        objElemTemp.SetAttribute("Category", Conversion.ConvertObjToStr(objReader.GetValue("CAT_NAME")));
                        //Start-Mridul Bansal. 01/12/10. MITS#18229.
                        //objElemTemp.SetAttribute("CategoryId", Conversion.ConvertObjToStr(objReader.GetValue("CAT_ID")));
                        objElemTemp.SetAttribute("CategoryId", iCatId.ToString());
                        //End-Mridul Bansal. 01/12/10. MITS#18229.
                        objElemTemp.SetAttribute("CategoryDesc", Conversion.ConvertObjToStr(objReader.GetValue("CAT_DESC")));
                        objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
                    }
				}
				objReader.Close();
				return objDOM; 
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCategoryList.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCategoryList.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				sSQL=null;
			}
		}
		#endregion

		#region Function to retrieve LOB
		/// Name		: GetLOBList
		/// Author		: Raman Bhatia
		/// Date Created: 09/26/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the LOB list
		/// </summary>
		/// <returns>Xml containing LOBList</returns>
		public XmlDocument GetLOBList()
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			string sTableId="";
			DbReader objReader=null;
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("GetLOBList");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("LOBs");
				objDOM.FirstChild.AppendChild(objElemChild);
				
				sTableId = GetTableId("LINE_OF_BUSINESS").ToString();
				sSQL=@"SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC,CODES.LINE_OF_BUS_CODE FROM CODES,CODES_TEXT
					 WHERE CODES.TABLE_ID = " + sTableId + " AND CODES.CODE_ID = CODES_TEXT.CODE_ID";
				sSQL=sSQL + " AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0 ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC";
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				while (objReader.Read())
				{
					objElemTemp=objDOM.CreateElement("LOB");
					objElemTemp.SetAttribute("codeid",Conversion.ConvertObjToStr(objReader.GetValue( "CODE_ID")));
					objElemTemp.SetAttribute("shortcode",Conversion.ConvertObjToStr(objReader.GetValue( "SHORT_CODE")));
					objElemTemp.SetAttribute("codedesc",Conversion.ConvertObjToStr(objReader.GetValue( "CODE_DESC")));
					if(objReader.GetValue( "CODE_ID").ToString() == this.CurTemplate.LOB.ToString() )
						objElemTemp.SetAttribute("Selected" , "true");
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				objReader.Close();
				return objDOM; 
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCategoryList.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetLOBList.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				sSQL=null;
			}

		}
		/// Name		: GetLOB
		/// Author		: Raman Bhatia
		/// Date Created: 09/29/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the LOB for a particular short code
		/// </summary>
		/// <returns>Xml containing LOBList</returns>
		public long GetLOB(string p_sShortCode)
		{
			string sSQL="";
			string sTableId="";
			long lCodeId=0;
			DbReader objReader=null;
			try
			{
				sTableId = GetTableId("LINE_OF_BUSINESS").ToString();
				sSQL="SELECT CODES.CODE_ID FROM CODES,CODES_TEXT WHERE CODES.TABLE_ID = " + sTableId + " AND CODES.CODE_ID = CODES_TEXT.CODE_ID";
				sSQL=sSQL + " AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0 AND CODES.SHORT_CODE = '" + p_sShortCode + "' ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC";
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				while (objReader.Read())
				{
                    lCodeId = Conversion.ConvertObjToInt64(objReader.GetValue("CODE_ID"), m_iClientId);
				}
				objReader.Close();
				return lCodeId; 
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCategoryList.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetLOBList.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				sSQL=null;
			}

		}
		/// Name		: GetEventLOB
		/// Author		: Raman Bhatia
		/// Date Created: 09/29/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the LOB for a particular Event ID
		/// </summary>
		/// <returns>variable containing LOB Code</returns>
		public long GetEventLOB(int p_iEventID)
		{
			string sSQL="";
			long lLOB=0;
			DbReader objReader=null;
			try
			{
				sSQL=@"SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE EVENT_ID=" + p_iEventID.ToString();
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				while (objReader.Read())
				{
                    lLOB = Conversion.ConvertObjToInt64(objReader.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
				}
				objReader.Close();
				return lLOB;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCategoryList.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetLOBList.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				sSQL=null;
			}

		}
		/// <summary>
		/// GetTableId returns the TABLE_ID for a particular SYSTEM_TABLE_NAME in the GLOSSARY table.
		/// </summary>
		/// <param name="p_sTableName">SYSTEM_TABLE_NAME for which the TABLE_ID is required</param>
		/// <returns>Table Id</returns>
		public int GetTableId(string p_sTableName )
		{
			DbReader objReader = null ;

			int iTableId = 0 ;
			string sSQL = "" ;
			
			try
			{
				if( p_sTableName == "" )
					return(0) ;

				sSQL = " SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + p_sTableName + "'" ;
				objReader = DbFactory.GetDbReader( m_sDSN , sSQL );
				if ( objReader != null )
					if( objReader.Read() )				
						iTableId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "TABLE_ID" ), m_iClientId );					
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetTableId.ErrorTableID",m_iClientId) , p_objEx );				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( iTableId );
				
		} 
		#endregion

		#region Function to retrieve default Format Code and Document Type Code
		/// Name		: GetDefaultFormatDocumentTypeCode
		/// Author		: Raman Bhatia
		/// Date Created: 09/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the default format code and document type code
		/// </summary>
		/// <returns>Xml containing CategoryList</returns>
		public XmlDocument GetDefaultFormatDocumentTypeCode()
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			string sTableId="";
			DbReader objReader=null;
			try
			{
				sTableId = GetTableId("MERGE_FORMAT_TYPE").ToString();
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("GetDefaultFormatDocumentTypeCode");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("FormatTypeCode");
				objDOM.FirstChild.AppendChild(objElemChild);
				sSQL=@"SELECT TOP 1 CODES.CODE_ID , CODES.SHORT_CODE , CODES_TEXT.CODE_DESC FROM CODES , CODES_TEXT WHERE TABLE_ID = " + sTableId + " AND CODES.CODE_ID = CODES_TEXT.CODE_ID ORDER BY CODES.SHORT_CODE ASC";
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				while (objReader.Read())
				{
					objElemTemp=objDOM.CreateElement("FormatCode");
					objElemTemp.SetAttribute("codeid",Conversion.ConvertObjToStr(objReader.GetValue( "CODE_ID")));
					objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue( "SHORT_CODE")) + " " + Conversion.ConvertObjToStr(objReader.GetValue( "CODE_DESC"));
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				objReader.Close();
				
				sTableId = GetTableId("MERGE_DOC_TYPE").ToString();
				objElemChild=objDOM.CreateElement("DocumentTypeCode");
				objDOM.FirstChild.AppendChild(objElemChild);
				sSQL=@"SELECT TOP 1 CODES.CODE_ID , CODES.SHORT_CODE , CODES_TEXT.CODE_DESC FROM CODES , CODES_TEXT WHERE TABLE_ID = " + sTableId + " AND CODES.CODE_ID = CODES_TEXT.CODE_ID ORDER BY CODES.SHORT_CODE ASC";
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				while (objReader.Read())
				{
					objElemTemp=objDOM.CreateElement("DocumentCode");
					objElemTemp.SetAttribute("codeid",Conversion.ConvertObjToStr(objReader.GetValue( "CODE_ID")));
					objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue( "SHORT_CODE")) + " " + Conversion.ConvertObjToStr(objReader.GetValue( "CODE_DESC"));
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				objReader.Close();

				return objDOM; 
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCategoryList.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCategoryList.Error",m_iClientId),p_objException);
			}
			finally
			{ 	
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				sSQL=null;
			}
		}
		#endregion
		#region Functions to retrieve permission list
		/// Name		: GetAvailablePermList
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function lists all permissions
		/// </summary>
		/// <returns>Xml containing list of all permissions in the system</returns>
		public XmlDocument GetAvailablePermList()
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			DbReader objReader=null;
			string sName="";
			string sFName="";
			string sLName="";
			long lUID=0;
			long lPrev=0;
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("AvailableTemplatePerm");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("TemplatePermItems");
				objDOM.FirstChild.AppendChild(objElemChild);
				sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME ";
				sSQL += " FROM USER_DETAILS_TABLE,USER_TABLE";
				sSQL += " WHERE USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID " ;
                if (Conversion.ConvertLongToBool(m_lDSNID, m_iClientId))
				{
					sSQL += "AND DSNID="+m_lDSNID;
				}
				objReader = DbFactory.GetDbReader(m_sSecDSN,sSQL);
				while (objReader.Read())
				{
					lUID= Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue( "USER_ID")));
					sLName=Conversion.ConvertObjToStr(objReader.GetValue( "LAST_NAME"));
					sFName=Conversion.ConvertObjToStr(objReader.GetValue( "FIRST_NAME"));
					if (sFName=="")
					{
						sName=sLName;
					}
					else
					{
						sName=sLName+", "+sFName;
					}
					if (!sName.Trim().Equals("") && lUID!=lPrev)
					{
						objElemTemp=objDOM.CreateElement("PermItem");
						objElemTemp.SetAttribute("DisplayName",sName);
						objElemTemp.SetAttribute("GroupId","0");
						objElemTemp.SetAttribute("IsGroup","0");
						objElemTemp.SetAttribute("UserId",lUID.ToString());
					}
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				
				objReader.Close();
				
				sSQL = "SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS ";
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				while (objReader.Read())
				{
					objElemTemp=objDOM.CreateElement("PermItem");
					objElemTemp.SetAttribute("DisplayName",Conversion.ConvertObjToStr(objReader.GetValue( "GROUP_NAME")));
					objElemTemp.SetAttribute("GroupId",Conversion.ConvertObjToStr(objReader.GetValue( "GROUP_ID")));
					objElemTemp.SetAttribute("IsGroup","1");
					objElemTemp.SetAttribute("UserId","0");
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				objReader.Close();
				return objDOM;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetAvailablePermList.XmlError",m_iClientId),p_objException);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetAvailablePermList.Error",m_iClientId),p_objException);
			}
			finally
			{ 		
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				sSQL=null;
				sName=null;
				sFName=null;
				sLName=null;
			}
		}
		/// <summary>
		/// This function returns the all permissions 
		/// </summary>
		/// <param name="p_sDSNID"></param>
		public XmlDocument GetAvailablePermListEx(string p_sDSNID)
		{
			XmlDocument objReturnDoc=null;
			try
			{
				m_lDSNID=Conversion.ConvertStrToLong(p_sDSNID);
				objReturnDoc=GetAvailablePermList();
				m_lDSNID=0;
				return objReturnDoc;
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				objReturnDoc=null;
			}
		}
		#endregion

		#region Function to retrieve information related to currently loaded template
		/// Name		: GetCurrentTemplateInfo
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the information related to currently loaded template
		/// </summary>
		/// <returns>Xml string containing the currently loaded template information</returns>
		public XmlDocument GetCurrentTemplateInfo()
		{
			XmlElement objElemChild=null;
			XmlElement objElemParent=null;
			XmlDocument objDOM=null;
			XmlDocument ObjXmlDomLOBList = null;
            XmlDocument ObjXmlDomMAilRecipientList = null;
            XmlDocument ObjXmlDomUserRecipientList = null;  //added by swati
			LocalCache objLocalCache = null;
            XmlNode objNodeLOBList = null;
            XmlNode objNodeMailRecipientList = null; //spahariya MITS 28867
            XmlNode objNodeUserRecipientList = null;    //added by swati
			string sStateShortCode = "";
			string sStateDesc = "";
			string sMergeDocumentTypeShortCode = "";
			string sMergeDocumentTypeDesc = "";
			string sMergeFormatShortCode = "";
			string sMergeFormatDesc = "";
            //Shruti for 11412
            XmlElement objTemplateNames = null;
            string sSQL = "";
            DbReader objReader = null;
			try
			{
				objDOM =new XmlDocument();
				objLocalCache = new LocalCache(m_sDSN,m_iClientId); 
				objElemParent = objDOM.CreateElement("GetCurrentTemplateInfo");
                objTemplateNames = objDOM.CreateElement("TemplateNames");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("Template");
				objElemChild.SetAttribute("CatId",this.CurTemplate.CatId.ToString());
				objElemChild.SetAttribute("FormFileName",this.CurTemplate.FormFileName.ToString());
				objElemChild.SetAttribute("FormId",this.CurTemplate.FormId.ToString());
				objElemChild.SetAttribute("FormName",this.CurTemplate.FormName.ToString());
				objElemChild.SetAttribute("Prefab",this.CurTemplate.Prefab.ToString());
				objElemChild.SetAttribute("IsNew",this.CurTemplate.IsNew.ToString());
				objElemChild.SetAttribute("FormDesc",this.CurTemplate.FormDesc.ToString());
				//Crawford Enhancement..modified by Raman Bhatia
				objElemChild.SetAttribute("LOB",this.CurTemplate.LOB.ToString());
                //spahariya 28867 start
				objElemChild.SetAttribute("emailCheck",this.CurTemplate.EmailCheck.ToString());
                objElemChild.SetAttribute("mailRecipient", this.CurTemplate.MailRecipient.ToString());
                //spahariya end
                objElemChild.SetAttribute("FormDesc", this.CurTemplate.FormDesc.ToString());
				objElemChild.SetAttribute("StateId",this.CurTemplate.StateId.ToString());
				objLocalCache.GetStateInfo(Convert.ToInt32(this.CurTemplate.StateId) , ref sStateShortCode , ref sStateDesc);
				objElemChild.SetAttribute("StateShortCode",sStateShortCode);
				objElemChild.SetAttribute("StateDesc",sStateDesc);
				
				objElemChild.SetAttribute("MergeDocumentType",this.CurTemplate.MergeDocTypeCode.ToString());
				objLocalCache.GetCodeInfo(Convert.ToInt32(this.CurTemplate.MergeDocTypeCode) , ref sMergeDocumentTypeShortCode , ref sMergeDocumentTypeDesc);
				objElemChild.SetAttribute("MergeDocumentTypeShortCode",sMergeDocumentTypeShortCode);
				objElemChild.SetAttribute("MergeDocumentTypeDesc",sMergeDocumentTypeDesc);
				
				objElemChild.SetAttribute("MergeDocumentFormat",this.CurTemplate.MergeFormatTypeCode.ToString());
				objElemChild.SetAttribute("MergeDocumentFormat",this.CurTemplate.MergeFormatTypeCode.ToString());
				objLocalCache.GetCodeInfo(Convert.ToInt32(this.CurTemplate.MergeFormatTypeCode) , ref sMergeFormatShortCode , ref sMergeFormatDesc);
				objElemChild.SetAttribute("MergeFormatShortCode",sMergeFormatShortCode);
				objElemChild.SetAttribute("MergeFormatDesc",sMergeFormatDesc);
				
                //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //TemplatePath = m_userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.m_userLogin.DocumentPath.Length>0)                    
                {
                    TemplatePath = m_userLogin.DocumentPath;
                }
                else
                {
                    TemplatePath = m_userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
				objElemChild.SetAttribute("FormFileContent",this.CurTemplate.FormFileContent);


				
				objDOM.FirstChild.AppendChild(objElemChild);

				//LOB information
				ObjXmlDomLOBList = GetLOBList();
				objNodeLOBList = objDOM.ImportNode(ObjXmlDomLOBList.DocumentElement,true);
				objDOM.FirstChild.AppendChild(objNodeLOBList);
                //Shruti for 11412
                //spahariya MITS 28867-start
                ObjXmlDomMAilRecipientList = GetMailRecipientList();
                objNodeMailRecipientList = objDOM.ImportNode(ObjXmlDomMAilRecipientList.DocumentElement, true);
                objDOM.FirstChild.AppendChild(objNodeMailRecipientList);
                //spahariya - end
                //added by swai for AIC Gap 7 MITS # 36930
                ObjXmlDomUserRecipientList = GetUserRecipientList(Convert.ToInt32(this.CurTemplate.FormId));
                objNodeUserRecipientList = objDOM.ImportNode(ObjXmlDomUserRecipientList.DocumentElement, true);
                objDOM.FirstChild.AppendChild(objNodeUserRecipientList);
                //change end here by swati
                sSQL = " SELECT FORM_NAME FROM MERGE_FORM WHERE CAT_ID = " + this.CurTemplate.CatId.ToString();
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);
                while (objReader.Read())
                {
                    objTemplateNames.InnerText = objTemplateNames.InnerText + "," + Conversion.ConvertObjToStr(objReader.GetValue("FORM_NAME"));
                }
                objDOM.DocumentElement.AppendChild(objTemplateNames);
                //Shruti for 11412 ends
				return objDOM;
			}
			catch(XmlException p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCurrentTemplateInfo.XmlError",m_iClientId),p_objException);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.GetCurrentTemplateInfo.Error",m_iClientId),p_objException);
			}
			finally
			{ 		
				objDOM=null;
				objElemChild=null;
				objElemParent=null;
                ObjXmlDomLOBList = null;
                objNodeLOBList = null;
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
			}
		}
		#endregion

		#region Function to load a template
		/// Name		: MoveTo
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function loads the template information
		/// </summary>
		/// <param name="p_sDocValue">Template id</param>
		public void MoveTo(string p_sDocValue)
		{
			long lFormID=0;
			try
			{
				if(!m_userLogin.IsAllowedEx( MAILMG_EDIT_SID))
				{
					throw new PermissionViolationException(RMPermissions.RMO_UPDATE, MAILMG_EDIT_SID);
				}

				lFormID=Conversion.ConvertStrToLong(p_sDocValue);
				m_objTemplate.Manager=null;
                m_objTemplate = new Template(m_userLogin, m_lDocPathType, m_sDocStorageDSN, m_iClientId);
				m_objTemplate.SecureDSN=this.m_sSecDSN;
				m_objTemplate.Manager=(TemplateManager)this;
				m_objTemplate.Load(lFormID);

			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateManager.MoveTo.Error",m_iClientId),p_objException);
			}
		}
		#endregion
        /// Name		: GetRecipientList
        /// Author		: Sonam Pahariya 
        /// Date Created: 06/26/2012 MITS 28867		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function returns the Mail Recipient list
        /// </summary>
        /// <returns>Xml containing Mail Recipient list</returns>
        public XmlDocument GetMailRecipientList()
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            string sSQL = "";
            DbReader objReader = null;
            try
            {
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("GetMailRecipient");
                objDOM.AppendChild(objElemParent);
                objElemChild = objDOM.CreateElement("Recipients");
                objDOM.FirstChild.AppendChild(objElemChild);

                sSQL = string.Empty;
                //MODIFIED BY SWATI FOR AIc Gap 8 MITS # 36930
                //sSQL = @"SELECT GT.TABLE_NAME, G.TABLE_ID FROM GLOSSARY_TEXT GT, GLOSSARY G WHERE G.TABLE_ID = GT.TABLE_ID AND G.GLOSSARY_TYPE_CODE IN (4, 7) ORDER BY GT.TABLE_NAME";
                sSQL = @"SELECT GT.TABLE_NAME, G.TABLE_ID FROM GLOSSARY_TEXT GT, GLOSSARY G WHERE G.TABLE_ID = GT.TABLE_ID AND (G.GLOSSARY_TYPE_CODE IN (4, 7) "
                + " OR G.SYSTEM_TABLE_NAME IN ('CLAIMANT','CLAIM_ADJUSTER')) ORDER BY GT.TABLE_NAME";
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);
                while (objReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("Recipient");
                    objElemTemp.SetAttribute("codeid", Conversion.ConvertObjToStr(objReader.GetValue("TABLE_ID")));
                    objElemTemp.SetAttribute("codedesc", Conversion.ConvertObjToStr(objReader.GetValue("TABLE_NAME")));
                    if (objReader.GetValue("TABLE_ID").ToString() == this.CurTemplate.MailRecipient.ToString())
                        objElemTemp.SetAttribute("Selected", "true");
                    objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
                }
                objReader.Close();
                
                return objDOM;
            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("TemplateManager.GetMailRecipientList.XmlError",m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("TemplateManager.GetMailRecipientList.Error",m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objDOM = null;
                objElemTemp = null;
                objElemChild = null;
                objElemParent = null;
                sSQL = null;
            }

        }
        //spahariya End
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function returns the Mail Recipient selected user list
        /// </summary>
        /// <returns>Xml containing Mail Recipient list</returns>
        public XmlDocument GetUserRecipientList(int p_iFormId)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            string sSQL = "";
            DbReader objReader = null;
            LocalCache objLocalCache = null;
            string sRecipientType = string.Empty;
            try
            {
                objDOM = new XmlDocument();
                objLocalCache = new LocalCache(m_sDSN,m_iClientId); 
                objElemParent = objDOM.CreateElement("GetUserRecipientList");
                objDOM.AppendChild(objElemParent);
                objElemChild = objDOM.CreateElement("Recipients");
                objDOM.FirstChild.AppendChild(objElemChild);

                sSQL = string.Empty;
                sSQL = @"SELECT DESIGNATED_RECIPIENT_ID, IS_PRIMARY_CURRENT FROM FORM_RECIPIENT WHERE FORM_ID = " + p_iFormId;
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);
                while (objReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("Recipient");
                    if (Conversion.ConvertObjToInt(objReader.GetValue("IS_PRIMARY_CURRENT"),m_iClientId) == -1)
                    {
                        objElemTemp.SetAttribute("codeid", Conversion.ConvertObjToStr(objReader.GetValue("DESIGNATED_RECIPIENT_ID")) + "*");
                    }
                    else
                    {
                        objElemTemp.SetAttribute("codeid", Conversion.ConvertObjToStr(objReader.GetValue("DESIGNATED_RECIPIENT_ID")));
                    }

                    sSQL = string.Empty;
                    sSQL = "SELECT TABLE_NAME FROM GLOSSARY_TEXT WHERE TABLE_ID = " + Conversion.ConvertObjToInt(objReader.GetValue("DESIGNATED_RECIPIENT_ID"),m_iClientId);
                    sRecipientType = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_userLogin.objRiskmasterDatabase.ConnectionString, sSQL));
                    if (Conversion.ConvertObjToInt(objReader.GetValue("IS_PRIMARY_CURRENT"),m_iClientId) == -1)
                    {
                        objElemTemp.SetAttribute("codedesc", sRecipientType + "*");
                    }
                    else
                    {
                        objElemTemp.SetAttribute("codedesc", sRecipientType);
                    }                    
                    
                    objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
                }
                objReader.Close();

                return objDOM;
            }
            catch (XmlException p_objException)
            {
                Log.Write(p_objException.ToString(),m_iClientId);
                throw new RMAppException(Globalization.GetString("TemplateManager.GetUserRecipientList.XMLError",m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                Log.Write(p_objException.ToString(),m_iClientId);
                throw new RMAppException(Globalization.GetString("TemplateManager.GetUserRecipientList.RMError",m_iClientId), p_objException);
            }
            catch (Exception p_objException)
            {
                Log.Write(p_objException.ToString(),m_iClientId);
                throw new RMAppException(Globalization.GetString("TemplateManager.GetUserRecipientList.Error",m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objDOM = null;
                objElemTemp = null;
                objElemChild = null;
                objElemParent = null;
                sSQL = null;
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
            }

        }
        //change end here by swati
	}
}
