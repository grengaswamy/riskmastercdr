using System;

namespace Riskmaster.Application.MailMerge
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   29th November 2004
	///Purpose :   This class Creates, Edits, Fetches template category information.
	/// </summary>
	internal class TemplateCategory
	{
		#region Variable Declaration
		/// <summary>
		/// Category Id
		/// </summary>
		private long m_lCategoryId=0; 
		/// <summary>
		/// Category name
		/// </summary>
		private string m_sCategoryName=""; 
		/// <summary>
		/// Category description
		/// </summary>
		private string m_sCategoryDesc="";
        private int m_iClientId = 0;
		#endregion

		#region Property Declaration
		/// <summary>
		/// Category Id
		/// </summary>
		public long CategoryId
		{
			get
			{
				return m_lCategoryId;
			}
			set
			{
				m_lCategoryId=value;
			}
		}
		/// <summary>
		/// Category name
		/// </summary>
		public string CategoryName
		{
			get
			{
				return m_sCategoryName;
			}
			set
			{
				m_sCategoryName=value;
			}
		}
		/// <summary>
		/// Category description
		/// </summary>
		public string CategoryDesc
		{
			get
			{
				return m_sCategoryDesc;
			}
			set
			{
				m_sCategoryDesc=value;
			}
		}
		#endregion

		#region TemplateCategory constructor
		/// <summary>
		/// TemplateCategory class constructor
		/// </summary>
        public TemplateCategory(int p_iClientId) { m_iClientId = p_iClientId; }
        #endregion
	}
}
