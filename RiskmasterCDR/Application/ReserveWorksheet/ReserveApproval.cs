﻿
using System;
using System.Xml;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using System.Text;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.Reserves;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Linq;
using System.Linq.Expressions;

namespace Riskmaster.Application.ReserveWorksheet
{

    /// <summary>
    /// ReserveApproval class has functions to List/Approve/Void/Deny funds transactions
    /// </summary>
    public class ReserveApproval : ReserveWorksheetBase
    {
        #region Variables Declaration
               
        /// <summary>
        /// Connection string for security database
        /// </summary>
        private string m_sSecureDSNConnectionString = "";
        /// <summary>
        /// Connection string for database
        /// </summary>
        private string m_sDSN = "";
        /// <summary>
        /// User name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// </summary>

        private long m_lDbMake = 0;
        /// <summary>
        /// Manager Id
        /// </summary>
        private long m_lManagerId = 0;
        /// <summary>
        /// User Id
        /// </summary>
        private long m_lUserId = 0;

        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";

        /// <summary>
        /// Private Variable to store the current DSN ID
        /// </summary>
        private int m_iDSNID = 0;
        /// <summary>
        /// Private static variable to store Subordinate User List
        /// </summary>
        private static string m_sSubordinateUsersList = string.Empty;

        private DataModelFactory m_objDataModelFactory;

        private int m_iClientId = 0;

        #endregion

        #region Properties Declaration
        /// <summary>
        /// User Id
        /// </summary>
        public long UserId
        {
            get
            {
                return m_lUserId;
            }
            set
            {
                m_lUserId = value;
            }
        }
        /// <summary>
        /// Manager Id
        /// </summary>
        public long ManagerId
        {
            set
            {
                m_lManagerId = value;
            }
        }
        /// <summary>
        /// Connection string for security database
        /// </summary>
        public string SecureDSN
        {
            get
            {
                return m_sSecureDSNConnectionString;
            }
            set
            {
                m_sSecureDSNConnectionString = value;
            }
        }
        /// <summary>
        /// User name
        /// </summary>
        public string UserName
        {
            get
            {
                return m_sUserName;
            }
            set
            {
                m_sUserName = value;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// ReserveApproval Constructor
        /// </summary>
        /// <param name="p_sDSN">DSN</param>
        /// <param name="p_sDsnName">Name of DSN</param>
        /// <param name="p_sUserName">Name of User</param>
        /// <param name="p_sPassword">password</param>
        /// <param name="p_sSecurityConnectionString">Security DB Connection string</param>
        public ReserveApproval(ReserveWorksheetCommon p_CommonInfo, int p_iClientId): base(p_CommonInfo, p_iClientId)
        {
            DbConnection objTempConn = null;
            try
            {                          
                
                // Get ReserveApproval approval options
                objTempConn = DbFactory.GetDbConnection(p_CommonInfo.Connectionstring);
                m_sDSN = p_CommonInfo.Connectionstring;
                m_sSecureDSNConnectionString = p_CommonInfo.SecurityConnectionstring;

                objTempConn.Open();
                m_lDbMake = (long)objTempConn.DatabaseType;
                objTempConn.Close();
                objTempConn = null;

                m_iDSNID = p_CommonInfo.DSNID;

                // Initialize data model factory
                m_sUserName = p_CommonInfo.UserName;
                m_sPassword = p_CommonInfo.UserPassword;
                m_sDsnName = p_CommonInfo.DSN;
                m_iClientId = p_iClientId;
                m_objDataModelFactory = new DataModel.DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveApproval.Constructor.Error",p_iClientId), p_objException);
            }
            finally
            {
                if (objTempConn != null)
                {
                    objTempConn.Close();
                    objTempConn = null;
                }
            }

        }

        public void Dispose()
        {
            try
            {
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.UnInitialize();
                    m_objDataModelFactory = null;
                }
            }
            catch
            {
            }
        }

        #endregion






        #region Common Functions
        /// Name		: CloseReader
        /// Author		: Vaibhav Mathur
        /// Date Created: 09/08/2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Closes a passed DbReader
        /// </summary>
        /// <param name="p_objReader">DbReader</param>
        private void CloseReader(ref DbReader p_objReader)
        {
            if (p_objReader != null)
            {
                if (!p_objReader.IsClosed)
                {
                    p_objReader.Close();
                }
                p_objReader = null;
            }
        }
        #endregion

        #region List Funds Transactions

        /// Name		: ListFundsTransactions
        /// Author		: Vaibhav Mathur
        /// Date Created: 09/08/2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 12/05/2009      Made it Generic     Gaurav
        ///************************************************************
        /// <summary>
        /// Lists funds transactions
        /// </summary>
        /// <param name="p_objOutAllTran">All transactions XML</param>
        /// <returns>xmlDocument</returns>
        public XmlDocument ListFundsTransactions(out XmlDocument p_objOutAllTran,string p_sShowAllItem)
        {
            int iPendingApprovalCode = 0;
            StringBuilder sbSQL = null;
            StringBuilder sbReservesList = null;
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;

            DbReader objRead = null;
            DataSet objDS = null;
            LocalCache objCache = null;
            Dictionary<int, double> dictReserve = null;
            p_objOutAllTran = null;
            bool bOracleDB = false;
            bool blnSuccess = false;
            string sHidden = "false";
            string sSupChain = "";
            try
            {
                //shobhana For customization of Transations
                AssignReserveWorksheet("","");
                if (p_sShowAllItem == "True")
                {
                    sSupChain = ShowItemsToAllUser(m_lUserId);
                }
                else
                {
                    //RMA-8253 pgupta93 START
                    //sSupChain = DownSupChain(m_lUserId, p_sShowAllItem);
                    sSupChain = DownSupChain(m_lUserId, p_sShowAllItem, "False");
                    //RMA-8253 pgupta93 END
                }
                if (sSupChain.Trim().Equals(""))
                    sSupChain = "0";
                objCache = new LocalCache(m_sDSN,ClientId);
                sbReservesList = new StringBuilder();
                sbSQL = new StringBuilder();
                dictReserve = new Dictionary<int, double>();
                sbSQL.Append("SELECT CODE_ID FROM CODES WHERE TABLE_ID = ");
                sbSQL.Append(objCache.GetTableId("MASTER_RESERVE"));
                sbSQL.Append(" AND (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL)");
                objRead = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                if(objRead.ConnectionType == eConnectionType.ManagedOracleServer)
                    bOracleDB = true;
                sbSQL.Remove(0, sbSQL.Length);
                while (objRead.Read())
                {
                    dictReserve.Add(objRead.GetInt32(0), 0);
                    sbReservesList.Append("NULL " + objCache.GetCodeDesc(objRead.GetInt32(0)) + "Reserve, ");
                }
                CloseReader(ref objRead);
                iPendingApprovalCode = objCache.GetCodeId("PA", "RSW_STATUS");

                if (bOracleDB)
                {
                    sbSQL.Append("SELECT R.RSW_ROW_ID RswRowId, R.RSW_STATUS_CODE RswStatus, C.CLAIM_NUMBER ClaimNumber, C.CLAIM_ID ClaimID, C.CLAIM_STATUS_CODE ClaimStatusCode, C.DATE_OF_CLAIM DateOfClaim, R.DTTM_RCD_LAST_UPD DateSubmitted, ");
                    sbSQL.Append("E.DATE_OF_EVENT DateOfEvent, R.SUBMITTED_BY SubmittedBy, ");
                    sbSQL.Append(sbReservesList);
                    sbSQL.Append("NULL TotalPaid, ");
                    sbSQL.Append("(SELECT ET.FIRST_NAME || ' ' || ET.LAST_NAME FROM ENTITY ET, CLAIMANT CM WHERE CM.CLAIM_ID=C.CLAIM_ID AND CM.CLAIMANT_EID=ET.ENTITY_ID AND CM.PRIMARY_CLMNT_FLAG = -1) PrimaryClaimant, ");
                    sbSQL.Append("(SELECT ET.FIRST_NAME || ' ' || ET.LAST_NAME FROM ENTITY ET, CLAIM_ADJUSTER CA WHERE CA.CLAIM_ID=C.CLAIM_ID AND ADJUSTER_EID= ET.ENTITY_ID AND CURRENT_ADJ_FLAG = -1) CurrentAdjuster ");
                    //JIRA start  7810 
                    //sbSQL.Append(",HOLD_REASON as 'HOLDREASON'");//Snehal
                    //sbSQL.Append("FROM RSW_WORKSHEETS R, CLAIM C, EVENT E "); 
                    //6385
                    //sbSQL.Append(" ,nvl(HOLD_REASON.HOLD_REASON_CODE,0) HOLDREASON");  //JIRA   7810 
                    sbSQL.Append(" ,nvl( (SELECT LISTAGG(CD.SHORT_CODE, ',') ");
                    sbSQL.Append(" WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID) AS SHORT_CODE");
                    sbSQL.Append(" FROM HOLD_REASON INNER JOIN CODES ON HOLD_REASON.HOLD_REASON_CODE = CODES.CODE_ID");
                    sbSQL.Append(" INNER JOIN  CODES CD ON CODES.RELATED_CODE_ID = CD.CODE_ID");
                    sbSQL.Append(" WHERE HOLD_STATUS_CODE = "  + iPendingApprovalCode.ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("RSW_WORKSHEETS"));
                    sbSQL.Append(" AND ON_HOLD_ROW_ID = R.RSW_ROW_ID  GROUP BY ON_HOLD_ROW_ID),0)  AS HOLD_REASON_CODE");


                    sbSQL.Append(" ,nvl( (SELECT LISTAGG(CT.CODE_DESC, ',') WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID)");
                    sbSQL.Append(" FROM HOLD_REASON INNER JOIN CODES  C ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID");
                    sbSQL.Append(" INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID  WHERE HOLD_STATUS_CODE =" + iPendingApprovalCode.ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("RSW_WORKSHEETS"));
                    sbSQL.Append(" AND ON_HOLD_ROW_ID = R.RSW_ROW_ID  GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLDREASON");

                    //6385


                    sbSQL.Append(" FROM RSW_WORKSHEETS R");
                   // sbSQL.Append(" LEFT OUTER JOIN  HOLD_REASON ");
                   // sbSQL.Append(" ON  R.RSW_ROW_ID = HOLD_REASON.ON_HOLD_ROW_ID AND HOLD_REASON.ON_HOLD_TABLE_ID =" + objCache.GetTableId("RSW_WORKSHEETS"));
                    //JIRA  End 7810 
                    sbSQL.Append(", CLAIM C, EVENT E ");
                    sbSQL.Append(" WHERE C.CLAIM_ID=R.CLAIM_ID AND C.EVENT_ID = E.EVENT_ID ");
                    sbSQL.Append(" AND R.RSW_STATUS_CODE = " + iPendingApprovalCode.ToString());
                    //sbSQL.Append("AND R.SUBMITTED_TO =" + UserId.ToString() + " ORDER BY DateOfEvent");
                    sbSQL.Append(" AND R.RSW_ROW_ID IN (" + sSupChain + ") ORDER BY DateOfEvent");
                }
                else
                {
                    sbSQL.Append("SELECT R.RSW_ROW_ID RswRowId, R.RSW_STATUS_CODE RswStatus, C.CLAIM_NUMBER ClaimNumber, C.CLAIM_ID ClaimID, C.CLAIM_STATUS_CODE ClaimStatusCode, C.DATE_OF_CLAIM DateOfClaim, R.DTTM_RCD_LAST_UPD DateSubmitted,");
                    sbSQL.Append("E.DATE_OF_EVENT DateOfEvent, R.SUBMITTED_BY SubmittedBy, ");
                    sbSQL.Append(sbReservesList);
                    sbSQL.Append("NULL AS 'TotalPaid', ");
                    sbSQL.Append("(SELECT COALESCE(ET.FIRST_NAME,' ') + ' ' + COALESCE(ET.LAST_NAME,' ') FROM ENTITY ET, CLAIMANT CM WHERE CM.CLAIM_ID=C.CLAIM_ID AND CM.CLAIMANT_EID=ET.ENTITY_ID AND CM.PRIMARY_CLMNT_FLAG = -1) AS 'PrimaryClaimant', ");
                    sbSQL.Append("(SELECT COALESCE(ET.FIRST_NAME,' ') + ' ' + COALESCE(ET.LAST_NAME,' ') FROM ENTITY ET, CLAIM_ADJUSTER CA WHERE CA.CLAIM_ID=C.CLAIM_ID AND ADJUSTER_EID= ET.ENTITY_ID AND CURRENT_ADJ_FLAG = -1) AS 'CurrentAdjuster' ");
                    //JIRA start  7810 
                    //sbSQL.Append(",HOLD_REASON as 'HOLDREASON'");//Snehal
                    //sbSQL.Append("FROM RSW_WORKSHEETS R, CLAIM C, EVENT E ");
                  
                    //6385
                    //sbSQL.Append(" ,ISNULL(HOLD_REASON.HOLD_REASON_CODE,0) HOLDREASON ");//7810 
                    sbSQL.Append(" ,ISNULL(  (SELECT STUFF((SELECT ', ' + CD.SHORT_CODE   FROM HOLD_REASON  INNER JOIN CODES");
                    sbSQL.Append(" ON HOLD_REASON.HOLD_REASON_CODE = CODE_ID");
                    sbSQL.Append(" INNER JOIN  CODES CD ON CODES.RELATED_CODE_ID = CD.CODE_ID");
                    sbSQL.Append(" WHERE ON_HOLD_ROW_ID = HR.ON_HOLD_ROW_ID");
                    sbSQL.Append(" AND HOLD_STATUS_CODE = " + iPendingApprovalCode.ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("RSW_WORKSHEETS"));
                    sbSQL.Append(" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE");
                    sbSQL.Append(" FROM HOLD_REASON  HR WHERE HOLD_STATUS_CODE =" + iPendingApprovalCode.ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("RSW_WORKSHEETS"));
                    sbSQL.Append(" AND ON_HOLD_ROW_ID = R.RSW_ROW_ID GROUP BY ON_HOLD_ROW_ID ) ,'') AS HOLD_REASON_CODE");


                    sbSQL.Append(" ,ISNULL(  (SELECT STUFF((SELECT ', ' + CT.CODE_DESC   FROM HOLD_REASON  INNER JOIN CODES  C");
                    sbSQL.Append(" ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID INNER JOIN CODES_TEXT CT");
                    sbSQL.Append(" ON C.CODE_ID = CT.CODE_ID  WHERE ON_HOLD_ROW_ID = HR.ON_HOLD_ROW_ID");
                    sbSQL.Append(" AND HOLD_STATUS_CODE =" + iPendingApprovalCode.ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("RSW_WORKSHEETS"));
                    sbSQL.Append(" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE_DESC");
                    sbSQL.Append(" FROM HOLD_REASON  HR WHERE HOLD_STATUS_CODE =" + iPendingApprovalCode.ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("RSW_WORKSHEETS"));
                    sbSQL.Append(" AND ON_HOLD_ROW_ID =R.RSW_ROW_ID GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLDREASON");
                    //6385
                    sbSQL.Append(" FROM RSW_WORKSHEETS R");
                   // sbSQL.Append(" LEFT OUTER JOIN  HOLD_REASON ");
                    //sbSQL.Append(" ON  R.RSW_ROW_ID = HOLD_REASON.ON_HOLD_ROW_ID AND HOLD_REASON.ON_HOLD_TABLE_ID ="+  objCache.GetTableId("RSW_WORKSHEETS"));
                    //JIRA  End 7810 
                    sbSQL.Append(", CLAIM C, EVENT E ");
                    sbSQL.Append(" WHERE C.CLAIM_ID=R.CLAIM_ID AND C.EVENT_ID = E.EVENT_ID ");
                    sbSQL.Append(" AND R.RSW_STATUS_CODE = " + iPendingApprovalCode.ToString());
                    //sbSQL.Append("AND R.SUBMITTED_TO =" + UserId.ToString() + " ORDER BY DateOfEvent");
                    sbSQL.Append(" AND R.RSW_ROW_ID IN (" + sSupChain + ") ORDER BY DateOfEvent");
                }
                objDS = DbFactory.GetDataSet(m_sDSN, sbSQL.ToString(), m_iClientId);
                sbSQL.Remove(0, sbSQL.Length);
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("ReserveApproval");
                objDOM.AppendChild(objElemParent);

                foreach (DataRow dr in objDS.Tables[0].Rows)
                {
                    objElemChild = objDOM.CreateElement("Approvals");
                    GetRSWReserves(Conversion.CastToType<int>(dr["ClaimID"].ToString(), out blnSuccess), Conversion.CastToType<int>(dr["RswRowId"].ToString(), out blnSuccess), dictReserve);
                    blnSuccess = false;
                    foreach (DataColumn dc in objDS.Tables[0].Columns)
                    {                        
                        objElemTemp = objDOM.CreateElement(dc.ColumnName);
                        if (dc.ColumnName.ToUpper().Contains("DATE"))
                        {
                            objElemTemp.InnerText = Conversion.ToDate(dr[dc.ColumnName].ToString()).ToShortDateString();
                            if(dc.ColumnName.ToUpper().Equals("DATESUBMITTED"))
                                sHidden = "true";
                        }
                        else if (dc.ColumnName.ToUpper().Contains("SUBMITTEDBY"))
                        {
                            objElemTemp.InnerText = GetSubmittedToUser(dr[dc.ColumnName].ToString());
                            sHidden = "true";
                        }
                        else if (dc.ColumnName.ToUpper().Contains("RESERVE"))
                        {
                            foreach (int iCodeId in dictReserve.Keys)
                            {
                                if (dc.ColumnName.ToUpper().Replace("RESERVE", "").Equals(objCache.GetCodeDesc(iCodeId).ToUpper()))
                                {
                                    objElemTemp.InnerText = string.Format("{0:C}", dictReserve[iCodeId]);
                                    dictReserve[iCodeId] = 0;
                                    break;
                                }
                            }
                        }
                        //gagnihotri MITS 19097
                        else if( dc.ColumnName.ToUpper().Contains("TOTALPAID"))
                        {
                            if(base.dictClaimPaidToDate.ContainsKey(Conversion.CastToType<int>(dr["ClaimID"].ToString(), out blnSuccess)))
                                objElemTemp.InnerText = string.Format("{0:C}", base.dictClaimPaidToDate[Conversion.CastToType<int>(dr["ClaimID"].ToString(), out blnSuccess)]);
                        }
                        //JIRA 6385 Start Snehal
                        else if (dc.ColumnName.ToUpper().Contains("HOLDREASON"))
                        {
                            objElemTemp.InnerText = dr[dc.ColumnName].ToString();
                        }
                        ////JIRA 6385 End Snehal
                        else
                        {
                            objElemTemp.InnerText = dr[dc.ColumnName].ToString();
                            if (dc.ColumnName.ToUpper().Contains("RSWROWID") || dc.ColumnName.ToUpper().Contains("CLAIMSTATUSCODE") ||
                                dc.ColumnName.ToUpper().Contains("CLAIMID") || dc.ColumnName.ToUpper().Contains("RSWSTATUS") ||dc.ColumnName.ToUpper().Contains("HOLD_REASON_CODE") )
                            {
                                sHidden = "true";
                            }
                        }
                        objElemTemp.SetAttribute("hidden", sHidden);
                        objElemChild.AppendChild(objElemTemp);
                        sHidden = "false";
                    }
                    objDOM.FirstChild.AppendChild(objElemChild);
                }

                p_objOutAllTran = objDOM;


            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveApproval.ListFundsTransactions.Error",m_iClientId), p_objException);
            }
            finally
            {
                CloseReader(ref objRead);
                sbSQL = null;
                sbReservesList = null;
                objDOM = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objDS != null)
                {
                    objDS.Dispose();
                    objDS = null;
                }
            }
            return p_objOutAllTran;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_sRSWID">Reserve Row ID</param>
        private string GetSubmittedToUser(string p_sRSWID)
        {
            string sSQL = string.Empty;
            string sSubmittedBy = string.Empty;

            if (DbFactory.IsOracleDatabase(m_sSecureDSNConnectionString))
            {
                sSQL = String.Format("SELECT FIRST_NAME || ' ' || LAST_NAME FROM USER_TABLE WHERE USER_ID ={0}", p_sRSWID);
            }//if
            else
            {
                sSQL = String.Format("SELECT COALESCE(FIRST_NAME,' ') + ' ' + COALESCE(LAST_NAME,' ') FROM USER_TABLE WHERE USER_ID ={0}", p_sRSWID);
            }//else
            sSubmittedBy = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sSecureDSNConnectionString, sSQL));
            

            return sSubmittedBy;
        }

        /// <summary>
        /// This function will get the reserve categories for the LOB and Claim Type (if required).
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <param name="p_iRSWID">Reserve Row ID</param>
        private void GetRSWReserves(int p_iClaimID, int p_iRSWID, Dictionary<int, double> pdictReserve)
        {
            XmlDocument objXMLOut = new XmlDocument();
            DbReader objReader = null;
            LocalCache objCache = null;
            StringBuilder sbSQL;
            ReserveFunds.structReserves[] arrReserves = null;
            Dictionary<int, double> dictReserveAmount = null;
            int iLOBCode = 0;
            int iClmTypeCode = 0;
            int iRelatedCodeID = 0;
            double dAmount = 0.0;
            int iReserveTypeCode = 0;
            string sAmt = "";
            bool blnSuccess = false;
            //skhare7 

            int iClaimCurrCode = 0;
            try
            {
                sbSQL = new StringBuilder();
                objCache = new LocalCache(m_sDSN,ClientId);
                dictReserveAmount = pdictReserve;

                #region Get the Line of Business for the claim
                sbSQL.Append("SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE,CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimID);
                objReader = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                if (objReader.Read())
                {
                    iLOBCode = objReader.GetInt32("LINE_OF_BUS_CODE");
                    iClmTypeCode = objReader.GetInt32("CLAIM_TYPE_CODE");
                    iClaimCurrCode = objReader.GetInt32("CLAIM_CURR_CODE");//skhare7
                }
                objReader.Dispose();
                sbSQL.Remove(0, sbSQL.Length);

                #endregion

                if (p_iRSWID != 0)
                {
                    sbSQL.Append("SELECT RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + p_iRSWID);
                    objReader = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                    sbSQL.Remove(0, sbSQL.Length);
                    if (objReader.Read())
                        objXMLOut.LoadXml(objReader.GetString("RSW_XML"));
                    else
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.LoadMainData.RSWNotFound",m_iClientId));

                    objReader.Dispose();
                    base.m_baseUserlogin = m_objDataModelFactory.Context.RMUser;// pgupta93 :RMA-10597 
                    arrReserves = GetResCategories(iLOBCode, iClmTypeCode);

                    string[,] sResFinal = null;
                    sResFinal = new string[arrReserves.GetLength(0), 4];

                    sResFinal = GetNewReserveAmts(iLOBCode, arrReserves, objXMLOut, iClaimCurrCode);

                    for (int iCount = 0; iCount < sResFinal.GetLength(0); iCount++)
                    {
                        # region Getting Reserves
                        iReserveTypeCode = Conversion.CastToType<int>(sResFinal[iCount, 0], out blnSuccess);
                        sAmt = sResFinal[iCount, 3];
                        dAmount = Conversion.CastToType<double>(sAmt, out blnSuccess);
                        iRelatedCodeID = objCache.GetRelatedCodeId(iReserveTypeCode);
                        if(pdictReserve.ContainsKey(iRelatedCodeID))
                        {
                            if (dAmount > 0.0)
                            {
                                pdictReserve[iRelatedCodeID] += dAmount;
                            }
                        }
                        # endregion
                    }
                }
            }
            finally
            {
                objXMLOut = null;
                sbSQL = null;
                arrReserves = null;
                if (objCache != null)
                    objCache.Dispose();
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                base.m_baseUserlogin = null;// pgupta93 :RMA-10597 
            }
        }

        //sachin 6385 starts
        private bool GetRSWReservesInc(int p_iClaimID, int p_iRSWID , long m_lUserId, int iGroupID)
        {
            XmlDocument objXMLOut = new XmlDocument();
            DbReader objReader = null;
            LocalCache objCache = null;
            StringBuilder sbSQL;
            ReserveFunds.structReserves[] arrReserves = null;          
            int iLOBCode = 0;
            int iClmTypeCode = 0;
                   
            int iCnt = 0;
            double claimIncTotal = 0.0;
            bool bUnderClaimIncUserLimit = false;

            int iClaimCurrCode = 0;
            try
            {
                sbSQL = new StringBuilder();
                objCache = new LocalCache(m_sDSN, ClientId);            

                #region Get the Line of Business for the claim
                sbSQL.Append("SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE,CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimID);
                objReader = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                if (objReader.Read())
                {
                    iLOBCode = objReader.GetInt32("LINE_OF_BUS_CODE");
                    iClmTypeCode = objReader.GetInt32("CLAIM_TYPE_CODE");
                    iClaimCurrCode = objReader.GetInt32("CLAIM_CURR_CODE");
                }
                objReader.Dispose();
                sbSQL.Remove(0, sbSQL.Length);

                #endregion

                if (p_iRSWID != 0)
                {
                    sbSQL.Append("SELECT RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + p_iRSWID);
                    objReader = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                    sbSQL.Remove(0, sbSQL.Length);
                    if (objReader.Read())
                        objXMLOut.LoadXml(objReader.GetString("RSW_XML"));
                    else
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.LoadMainData.RSWNotFound", m_iClientId));

                    objReader.Dispose();
                    base.m_baseUserlogin = m_objDataModelFactory.Context.RMUser;
                    arrReserves = GetResCategories(iLOBCode, iClmTypeCode);
                   
                    string[,] sResFinal = null;
                    //sResFinal = new string[arrReserves.GetLength(0), 4];

                    sResFinal = GetNewReserveAmts(iLOBCode, arrReserves, objXMLOut, iClaimCurrCode);                                      
                    
                        for (iCnt = 0; iCnt < arrReserves.GetLength(0); iCnt++)
                        {
                            # region Get Claim Incurred Limits and Validate If Incurred amount fall within that limit

                            for (int iCount = 0; iCount < sResFinal.GetLength(0); iCount++)
                            {
                                # region Query to find reserve limit for all Reserves
                                if (Riskmaster.Common.Conversion.ConvertStrToInteger(sResFinal[iCount, 0]) == arrReserves[iCnt].iReserveTypeCode)
                                {
                                    if (Riskmaster.Common.Conversion.ConvertStrToDouble(sResFinal[iCount, 4]) != 0.0)
                                    {
                                        if (Riskmaster.Common.Conversion.ConvertStrToDouble(sResFinal[iCount, 3]) > 0.0)
                                        {
                                            claimIncTotal = claimIncTotal + Riskmaster.Common.Conversion.ConvertStrToDouble(sResFinal[iCount, 3]);
                                        }
                                    }
                                }
                                # endregion
                            }
                            # endregion
                        }
                        bUnderClaimIncUserLimit = CheckClaimIncLimits(p_iClaimID, claimIncTotal, Convert.ToInt32(m_lUserId), iGroupID, iClaimCurrCode);
                        if (bUnderClaimIncUserLimit)
                            return false;
                        else
                            return true;                 
                }
            }
            finally
            {
                objXMLOut = null;
                sbSQL = null;
                arrReserves = null;
                if (objCache != null)
                    objCache.Dispose();
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                base.m_baseUserlogin = null;// pgupta93 :RMA-10597 
            }
            return false;
        }      
        //sachin 6385 ends

        /// Name		: DownSupChain
        /// Author		: Parag Sarin
        /// Date Created: 02/02/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Retrieves the supervisor chain from user id (passed in) down in other words it gets all the users who have the user id (passed in) as a supervisor somewhere above them.
        /// </summary>
        /// <param name="p_lUserId">User id</param>
        /// <param name="p_sShowAllItem"></param>
        /// <param name="p_sShowMyPendingTrans"
        /// <returns>Supervisor chain</returns>
        private string DownSupChain(long p_lUserId, string p_sShowAllItem, string p_sShowMyPendingTrans)//Added one new parameter for show my pending transaction
        {
            //loops through user table and retrieves the supervisor chain from user id (passed in) down in other words it gets all the users who have the user id (passed in) as a supervisor somewhere above them.
            string sUserSQL = "";
            StringBuilder sbSQL = null;
            string sUsers = "";
            string sReserveIDList = "";
            //int iApprovalDays = 0;
            long lTotalDays = 0;
            string sTransDate = "";
            int iCount = 0;
            bool bLOBOrTopUser = false;
            long lTop = 0;
            long lWC = 0;
            long lGC = 0;
            long lVA = 0;
            long lDI = 0;
            StringBuilder sbLOBUSers = null;
            StringBuilder sbTransIDSQL = null;
            string sLOBUSers = "";
            string sStartList = "";
            string sWhichLOB = string.Empty;
            DbReader objRead = null;
            DbReader objTempRead = null;
            string sSQL = "";
            DateTime datTemp;
            CCacheFunctions objCacheFunctions = null;
            //smahajan6: Safeway: Payment Supervisory Approval : Start
            string sAssignedDateTime = string.Empty;
            DateTime dStartDateTime;
            DateTime dEndDateTime;
            DateTime dCurrentDateTime = DateTime.Now;
            //int iApprovalHours = 0;
            int iManagerId = 0;
            int iDaysCount = 0;
            int iHoursCount = 0;
            //Start: rsushilaggar 11/23/2011 MITS-26332
            bool bTempValue = false;
            string sQuery = string.Empty;
            String sSqlReserve = string.Empty;
            LocalCache objCache = null;
            //bool bRecordAdded = false;
            //smahajan6: Safeway: Payment Supervisory Approval : End
            try
            {
                objCache = new LocalCache(m_sDSN,ClientId);
                //Check to see who the top level users are
                bLOBOrTopUser = GetTopApprovers(ref lTop, ref lWC, ref lGC, ref lVA, ref lDI);
                
                //Get Supervisory Approval Settings
                ReadRSWCheckOptions();

                //CloseReader(ref objRead);
                sbSQL = new StringBuilder();
                objCacheFunctions = new CCacheFunctions(m_sDSN,m_iClientId);
                if (m_lUserId == 0)
                {
                    return "";
                }
                else
                {
                    sbSQL.Append("SELECT RSW_ROW_ID,DTTM_RCD_ADDED ");
                    sbSQL.Append("FROM RSW_WORKSHEETS ");
                    sbSQL.Append("WHERE RSW_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("PA", "RSW_STATUS").ToString());
                    //RMA-8253 pgupta START
                    if (p_sShowMyPendingTrans == "True")
                        sbSQL.Append(" AND SUBMITTED_BY =" + UserId.ToString() + " AND ADDED_BY_USER <> " + Utilities.FormatSqlFieldValue(m_sUserName));
                    else
                     sbSQL.Append(" AND SUBMITTED_TO =" + UserId.ToString() + " AND ADDED_BY_USER <> " + Utilities.FormatSqlFieldValue(m_sUserName));
                    //RMA-8253 pgupta END
                    objRead = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());

                    while (objRead.Read())
                    {
                        //smahajan6: Safeway: Payment Supervisory Approval : Start
                        sAssignedDateTime = "";
                        sAssignedDateTime = objRead.GetValue("DTTM_RCD_ADDED").ToString();
                        datTemp = ToDateTime(sAssignedDateTime);
                        dStartDateTime = datTemp;
                        dEndDateTime = dStartDateTime.AddDays(m_iDaysAppReserves).AddHours(m_iHoursAppReserves);
                        
                        //Start rsusilaggar MITS 26332 11/23/2011
                        //TO DO change this with reserve settings
                        if (m_bUseSupAppReserves)
                        {
                            bTempValue = true;
                        }
                        else
                        {
                            bTempValue = (dEndDateTime >= dCurrentDateTime || dEndDateTime == dStartDateTime || (lTop == m_lUserId));
                        }
                        //if (dEndDateTime >= dCurrentDateTime || dEndDateTime == dStartDateTime || (lTop == m_lUserId))
                        //{
                        if (bTempValue)
                        {
                            if ((!sReserveIDList.Trim().Equals("")))
                            {
                                if ((!sReserveIDList.Substring(sReserveIDList.Length - 1).Equals(",")))
                                {
                                    sReserveIDList = sReserveIDList + ",";
                                }
                            }
                            sReserveIDList = sReserveIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("RSW_ROW_ID")) + ",";
                        }
                        //End rsushilaggar

                    }
                    CloseReader(ref objRead);

                    //}	Commented by Mohit
                    //The top level user is logged on we will get the checks that are assigned to the lob users and have been waiting for three days
                    if (bLOBOrTopUser && lTop == m_lUserId)
                    {
                        sLOBUSers = "";
                        sbLOBUSers = new StringBuilder();
                        if (lWC != 0)
                        {
                            sbLOBUSers.Append(lWC + ",");
                        }
                        if (lDI != 0)
                        {
                            sbLOBUSers.Append(lDI + ",");
                        }
                        if (lGC != 0)
                        {
                            sbLOBUSers.Append(lGC + ",");
                        }
                        if (lVA != 0)
                        {
                            //sbLOBUSers.Append(lVA+",");	Commented by Mohit
                            sbLOBUSers.Append(lVA);
                        }
                        sLOBUSers = sbLOBUSers.ToString();
                        if (sLOBUSers.Trim().Length > 0)
                        {
                            if (sLOBUSers.Substring(sLOBUSers.Length - 1).Equals(","))
                            {
                                sLOBUSers = sLOBUSers.Substring(0, sLOBUSers.Length - 1);
                            }
                        }
                        sbSQL.Remove(0, sbSQL.Length);
                        /*************Start: Mohit Yadav: Safeway: Getting Error if specified TOP level supervisor 
                         **but none of LOB level supervisor. Added 'if' condition to check this sql error***********/
                        if (sLOBUSers != "")
                        {
                            sbSQL.Append("SELECT RSW_ROW_ID,DTTM_RCD_ADDED ");
                            sbSQL.Append("FROM RSW_WORKSHEETS ");
                            sbSQL.Append("WHERE RSW_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("PA", "RSW_STATUS").ToString());
                            //RMA-8253 pgupta START
                            if (p_sShowMyPendingTrans == "True")
                                sbSQL.Append(" AND SUBMITTED_BY =" + UserId.ToString() + " AND ADDED_BY_USER <> " + Utilities.FormatSqlFieldValue(m_sUserName));
                            else
                                sbSQL.Append(" AND SUBMITTED_TO IN (" + sLOBUSers + ") AND ADDED_BY_USER <> " + Utilities.FormatSqlFieldValue(m_sUserName));
                            //RMA-8253 pgupta END

                            objRead = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                            while (objRead.Read())
                            {
                                sTransDate = Conversion.ConvertObjToStr(objRead.GetValue("DTTM_RCD_ADDED"));
                                datTemp = Conversion.ToDate(Conversion.ConvertObjToStr(sTransDate));
                                if (m_iDaysAppReserves != 0)
                                {
                                    datTemp = datTemp.AddDays(m_iDaysAppReserves);
                                }
                                else
                                {
                                    datTemp = DateTime.Now;
                                }
                                if (datTemp <= DateTime.Now)
                                {
                                    if ((!sReserveIDList.Trim().Equals("")))
                                    {
                                        if ((!sReserveIDList.Substring(sReserveIDList.Length - 1).Equals(",")))
                                        {
                                            sReserveIDList = sReserveIDList + ",";
                                        }
                                    }
                                    //smahajan6: Safeway: Payment Supervisory Approval - commented
                                    //sReserveIDList = sReserveIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID"))+ ",";
                                }
                            }//end while
                            CloseReader(ref objRead);
                        }
                        /*************End: Mohit Yadav: Safeway: Ending 'if' condition specified above**/
                        sLOBUSers = "";
                        sbLOBUSers = new StringBuilder();
                        if (lWC != 0)
                        {
                            sbLOBUSers.Append(lWC + ",");
                        }
                        if (lDI != 0)
                        {
                            sbLOBUSers.Append(lDI + ",");
                        }
                        if (lGC != 0)
                        {
                            sbLOBUSers.Append(lGC + ",");
                        }
                        if (lVA != 0)
                        {
                            sbLOBUSers.Append(lVA + ",");
                        }
                        if (lTop != 0)
                        {
                            //sbLOBUSers.Append(lTop + ",");	commented by Mohit
                            sbLOBUSers.Append(lTop);
                        }
                        sLOBUSers = sbLOBUSers.ToString();
                        if (sLOBUSers.Trim().Length > 0)
                        {
                            if (sLOBUSers.Substring(sLOBUSers.Length - 1).Equals(","))
                            {
                                sLOBUSers = sLOBUSers.Substring(0, sLOBUSers.Length - 1);
                            }
                        }
                        sStartList = GetStartingUsers(sLOBUSers);
                        //smahajan6: Safeway: Payment Supervisory Approval : Sart
                        if (sStartList.Trim().Length > 0)
                        {
                            sStartList = sStartList + "," + sLOBUSers;
                        }
                        else
                        {
                            sStartList = sLOBUSers;
                        }
                        //smahajan6: Safeway: Payment Supervisory Approval : End
                    }
                    //An LOB user is logged on We will need to screen the transactions for LOB
                    else if (bLOBOrTopUser && lTop != m_lUserId)
                    {
                        sLOBUSers = "";
                        sbLOBUSers = new StringBuilder();
                        if (lWC != 0)
                        {
                            sbLOBUSers.Append(lWC + ",");
                            if (lWC == m_lUserId)
                            {
                                sWhichLOB = "243";
                            }
                        }
                        if (lDI != 0)
                        {
                            sbLOBUSers.Append(lDI + ",");
                            if (lDI == m_lUserId)
                            {
                                sWhichLOB = sWhichLOB + ",844";
                            }
                        }
                        if (lGC != 0)
                        {
                            sbLOBUSers.Append(lGC + ",");
                            if (lGC == m_lUserId)
                            {
                                sWhichLOB = sWhichLOB + ",241";
                            }
                        }
                        if (lVA != 0)
                        {
                            sbLOBUSers.Append(lVA + ",");
                            if (lVA == m_lUserId)
                            {
                                sWhichLOB = sWhichLOB + ",242";
                            }
                        }
                        if (lTop != 0)
                        {
                            //sbLOBUSers.Append(lTop + ",");	commented by Mohit
                            sbLOBUSers.Append(lTop);
                            if (lTop == m_lUserId)
                            {
                                sWhichLOB = "-1";
                            }
                        } 
                        sLOBUSers = sbLOBUSers.ToString();
                        if (sLOBUSers.Trim().Length > 0)
                        {
                            if (sLOBUSers.Substring(sLOBUSers.Length - 1).Equals(","))
                            {
                                sLOBUSers = sLOBUSers.Substring(0, sLOBUSers.Length - 1);
                            }
                        }
                        sStartList = GetStartingUsers(sLOBUSers);
                    }//end if (bLOBOrTopUser && lTop != m_lUserId)
                    //We start down the superviory chain
                    if (sStartList.Trim().Length > 0)
                    {
                        if (sStartList.Trim().Substring(0, sStartList.Trim().Length - 1).Equals(","))
                        {
                            sStartList = sStartList.Substring(0, sLOBUSers.Length - 1);
                        }
                    }
                    //smahajan6: Safeway: Payment Supervisory Approval : Start
                    if (!bLOBOrTopUser)
                    {
                        sStartList = Conversion.ConvertObjToStr(m_lUserId);
                    }

                    sStartList = GetSubordinateUsers(sStartList);
                    //smahajan6: Safeway: Payment Supervisory Approval : End
                    if (sStartList.Trim().Equals(""))
                    {
                        //Get all users who have the currrent user as their manager
                        sUserSQL = "SELECT USER_ID, MANAGER_ID FROM USER_TABLE WHERE MANAGER_ID = " + p_lUserId.ToString();
                    }
                    else
                    {
                        //Get all users who have the users in the starting list as their manager
                        sUserSQL = "SELECT USER_ID, MANAGER_ID FROM USER_TABLE WHERE MANAGER_ID IN (" + sStartList + ")";
                    }

                    objRead = DbFactory.GetDbReader(m_sSecureDSNConnectionString, sUserSQL);
                    bool bIsEOF = objRead.Read();
                    iCount = 1; //added by Mohit
                    do
                    {
                        lTotalDays = m_iDaysAppReserves * iCount;
                        //list of users who have lUserID as their manager
                        while (bIsEOF)
                        {
                            if (!sUsers.Trim().Equals(""))
                            {
                                if (!sUsers.Substring(sUsers.Length - 1).Equals(","))
                                {
                                    sUsers += ",";
                                }
                            }
                            sUsers += Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")) + ",";
                            sbTransIDSQL = new StringBuilder();
                            
                            sbTransIDSQL.Append("SELECT RSW_ROW_ID,DTTM_RCD_ADDED,CLAIM_ID ");
                            sbTransIDSQL.Append("FROM RSW_WORKSHEETS ");
                            sbTransIDSQL.Append("WHERE RSW_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("PA", "RSW_STATUS").ToString());
                            //RMA-8253 pgupta START
                            if (p_sShowMyPendingTrans == "True")   sbTransIDSQL.Append(" AND SUBMITTED_BY =" + UserId.ToString() + " AND ADDED_BY_USER <> " + Utilities.FormatSqlFieldValue(m_sUserName));
                            else
                            sbTransIDSQL.Append(" AND SUBMITTED_TO = " + Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")) + " AND ADDED_BY_USER <> " + Utilities.FormatSqlFieldValue(m_sUserName));
                            //RMA-8253 pgupta END
                            objTempRead = DbFactory.GetDbReader(m_sDSN, sbTransIDSQL.ToString());

                            while (objTempRead.Read())
                            {
                                sTransDate = Conversion.ConvertObjToStr(objTempRead.GetValue("DTTM_RCD_ADDED"));
                                datTemp = Conversion.ToDate(Conversion.ConvertObjToStr(sTransDate));

                                if (m_iDaysAppReserves != 0)
                                {
                                    //Added by Mohit
                                    //MWC this will add in the step missed when going between the supervisory chain and the LOB users
                                    if (lTop == m_lUserId)
                                        lTotalDays = lTotalDays + m_iDaysAppReserves;
                                    //datTemp=datTemp.AddDays(iApprovalDays);
                                    //commented by Mohit
                                    datTemp = datTemp.AddDays(lTotalDays);
                                }
                                else
                                {
                                    datTemp = DateTime.Now;
                                }
                                //smahajan6: Safeway: Payment Supervisory Approval : Start
                                sAssignedDateTime = sTransDate;
                                iDaysCount = m_iDaysAppReserves;
                                iHoursCount = m_iHoursAppReserves;
                                iManagerId = GetManagerID(Conversion.ConvertObjToInt(objRead.GetValue("USER_ID"), base.ClientId));

                                datTemp = ToDateTime(sAssignedDateTime);

                                while (iManagerId != 0 && iManagerId != Conversion.ConvertObjToInt(m_lUserId, base.ClientId))
                                {
                                    iDaysCount = iDaysCount + m_iDaysAppReserves;
                                    iHoursCount = iHoursCount + m_iHoursAppReserves;
                                    iManagerId = GetManagerID(iManagerId);
                                }

                                if (iManagerId == 0 && bLOBOrTopUser && lTop == m_lUserId)
                                {
                                    iDaysCount = iDaysCount + m_iDaysAppReserves;
                                    iHoursCount = iHoursCount + m_iHoursAppReserves;
                                }

                                dStartDateTime = datTemp.AddDays(iDaysCount).AddHours(iHoursCount);
                                dEndDateTime = dStartDateTime.AddDays(m_iDaysAppReserves).AddHours(m_iHoursAppReserves);
                                //if (dStartDateTime <= dCurrentDateTime && ((dEndDateTime >= dCurrentDateTime) || (lTop == m_lUserId)))

                                //Start rsusilaggar MITS 26332 11/23/2011
                                //TO DO change this with reserve settings
                                if (m_bUseSupAppReserves)
                                {
                                    bTempValue = (dStartDateTime <= dCurrentDateTime);
                                }
                                else
                                {
                                    bTempValue = (dStartDateTime <= dCurrentDateTime && ((dEndDateTime >= dCurrentDateTime) || (lTop == m_lUserId) || (dStartDateTime == dEndDateTime)));
                                }

                                //if (m_bUseSupAppReserves && m_bAllowAccessGrpSup && p_sShowAllItem == "True")
                                //{
                                //    bTempValue = true;
                                //}
                                // if (dStartDateTime <= dCurrentDateTime && ((dEndDateTime >= dCurrentDateTime) || (lTop == m_lUserId) || (dStartDateTime == dEndDateTime))) //Added for Mits 19417:User approval not sent to second level manager(when approval days are set to 0)
                                //if (datTemp <= DateTime.Now) - modified Time Validation Check
                                //smahajan6: Safeway: Payment Supervisory Approval : End
                                if (bTempValue)
                                {
                                    if (!string.IsNullOrEmpty(sWhichLOB))
                                    {
                                        if (sWhichLOB.IndexOf(GetLOBForClaim(Conversion.ConvertObjToInt64(
                                        objTempRead.GetValue("CLAIM_ID"), base.ClientId)).ToString()) > -1)
                                        {
                                            if ((!sReserveIDList.Trim().Equals("")))
                                            {
                                                if ((!sReserveIDList.Substring(sReserveIDList.Length - 1).Equals(",")))
                                                {
                                                    sReserveIDList = sReserveIDList + ",";
                                                }
                                            }
                                            sReserveIDList = sReserveIDList + Common.Conversion.ConvertObjToStr(objTempRead.GetValue("RSW_ROW_ID")) + ",";
                                        }
                                    }
                                    else
                                    {
                                        if ((!sReserveIDList.Trim().Equals("")))
                                        {
                                            if ((!sReserveIDList.Substring(sReserveIDList.Length - 1).Equals(",")))
                                            {
                                                sReserveIDList = sReserveIDList + ",";
                                            }
                                        }
                                        sReserveIDList = sReserveIDList + Common.Conversion.ConvertObjToStr(objTempRead.GetValue("RSW_ROW_ID")) + ",";
                                    }
                                }
                                //End rsushilaggar
                            }//end while objTempRead
                            CloseReader(ref objTempRead);
                            bIsEOF = objRead.Read();
                        }//end while
                        CloseReader(ref objRead);
                        if (sUsers.Trim().Length > 0)
                        {
                            if (sUsers.Substring(sUsers.Length - 1).Equals(","))
                            {
                                sUsers = sUsers.Substring(0, sUsers.Length - 1);
                            }
                        }
                        if (!sUsers.Trim().Equals(""))
                        {
                            sUserSQL = "SELECT USER_ID, MANAGER_ID FROM USER_TABLE WHERE MANAGER_ID IN (" + sUsers + ")";
                        }
                        else
                        {
                            sUserSQL = "SELECT USER_ID, MANAGER_ID FROM USER_TABLE WHERE MANAGER_ID = -1";
                        }
                        objRead = DbFactory.GetDbReader(m_sSecureDSNConnectionString, sUserSQL);
                        sUsers = "";
                        iCount = iCount + 1;
                    } while (bIsEOF);
                    CloseReader(ref objRead);
                }//added by Mohit

                if (sReserveIDList.Trim().Length > 0)
                {
                    if (sReserveIDList.Substring(sReserveIDList.Length - 1).Equals(","))
                    {
                        sReserveIDList = sReserveIDList.Substring(0, sReserveIDList.Length - 1);
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveApproval.DownSupChain.Error", m_iClientId), p_objException);
            }
            finally
            {
                CloseReader(ref objRead);
                CloseReader(ref objTempRead);
                objCacheFunctions = null;
                sbSQL = null;
                sbLOBUSers = null;
            }
            return sReserveIDList;
        }

        /// Name		: GetTopApprovers
        /// Author		: Parag Sarin
        /// Date Created: 02/02/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Gets Top Approvers for LOB's
        /// </summary>
        /// <param name="p_lTop">LOB</param>
        /// <param name="p_lWC">LOB WC</param>
        /// <param name="p_lGC">LOB GC</param>
        /// <param name="p_lVA">LOB VA</param>
        /// <param name="p_lDI">LOB DI</param>
        /// <returns>Success - True or Failure- False</returns>
        private bool GetTopApprovers(ref long p_lTop, ref long p_lWC, ref long p_lGC, ref long p_lVA, ref long p_lDI)
        {
            string sSQL = "";
            long lLobCode = 0;
            DbReader objRead = null;
            bool bRetValue = false;
            try
            {
                sSQL = "SELECT LOB_CODE,USER_ID FROM LOB_SUP_APPROVAL";
                objRead = DbFactory.GetDbReader(m_sDSN, sSQL);

                while (objRead.Read())
                {
                    lLobCode = Conversion.ConvertObjToInt64(objRead.GetValue("LOB_CODE"), base.ClientId);
                    switch (lLobCode)
                    {
                        case 242:
                            p_lVA = Conversion.ConvertObjToInt64(objRead.GetValue("USER_ID"), base.ClientId);
                            if (p_lVA == m_lUserId)
                            {
                                bRetValue = true;
                            }
                            break;
                        case 241:
                            p_lGC = Conversion.ConvertObjToInt64(objRead.GetValue("USER_ID"), base.ClientId);
                            if (p_lGC == m_lUserId)
                            {
                                bRetValue = true;
                            }
                            break;
                        case 243:
                            p_lWC = Conversion.ConvertObjToInt64(objRead.GetValue("USER_ID"), base.ClientId);
                            if (p_lWC == m_lUserId)
                            {
                                bRetValue = true;
                            }
                            break;
                        case 844:
                            p_lDI = Conversion.ConvertObjToInt64(objRead.GetValue("USER_ID"), base.ClientId);
                            if (p_lDI == m_lUserId)
                            {
                                bRetValue = true;
                            }
                            break;
                        default:
                            p_lTop = Conversion.ConvertObjToInt64(objRead.GetValue("USER_ID"), base.ClientId);
                            if (p_lTop == m_lUserId)
                            {
                                bRetValue = true;
                            }
                            break;
                    }//end switch
                }//end while
                CloseReader(ref objRead);

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveApproval.GetTopApprovers.Error", m_iClientId), p_objException);
            }
            finally
            {
                CloseReader(ref objRead);
            }
            return bRetValue;

        }

       

        /// Name		: GetStartingUsers
        /// Author		: Parag Sarin
        /// Date Created: 02/02/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************

        /// <summary>
        /// Get User ids
        /// </summary>
        /// <param name="p_sUsersToInclude">Manager ids</param>
        /// <returns>String containing userids separated by comma</returns>
        private string GetStartingUsers(string p_sUsersToInclude)
        {
            StringBuilder sbSQL = null;
            StringBuilder sbTemp = null;
            DbReader objRead = null;
            string sReturnValue = "";
            try
            {
                sbSQL = new StringBuilder();
                if (!p_sUsersToInclude.Trim().Equals(""))
                {
                    sbSQL.Append("SELECT USER_ID FROM USER_TABLE WHERE (MANAGER_ID IS NULL OR MANAGER_ID = 0 OR MANAGER_ID IN (" + p_sUsersToInclude + ")) AND USER_ID NOT IN (" + p_sUsersToInclude + ")");
                }
                else
                {
                    sbSQL.Append("SELECT USER_ID FROM USER_TABLE WHERE MANAGER_ID IS NULL OR MANAGER_ID = 0");
                }

                objRead = DbFactory.GetDbReader(m_sSecureDSNConnectionString, sbSQL.ToString());
                sbTemp = new StringBuilder();
                while (objRead.Read())
                {
                    if (!sbTemp.ToString().Trim().Equals(""))
                    {
                        if (!sbTemp.ToString().Substring(sbTemp.Length - 1).Equals(",")) //smahajan6: Safeway: Payment Supervisory Approval
                        {
                            sbTemp.Append(",");
                        }
                        sbTemp.Append(Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")));
                    }
                    //smahajan6: Safeway: Payment Supervisory Approval : Start - fixed issue in base
                    else
                    {
                        sbTemp.Append(Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")));
                    }
                    //smahajan6: Safeway: Payment Supervisory Approval : End
                }
                sReturnValue = sbTemp.ToString();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveApproval.GetStartingUsers.Error", m_iClientId), p_objException);
            }
            finally
            {
                CloseReader(ref objRead);
                sbSQL = null;
                sbTemp = null;
            }
            return sReturnValue;
        }

        /// Name		: GetSubordinateUsers
        /// Author		: Saurav Mahajan
        /// Date Created: 11/12/2008
        /// <summary>
        /// Get Subordinate User ids with Parent Id
        /// </summary>
        /// <param name="p_sUsersToInclude">Parent User Ids</param>
        /// <returns>String containing userids with Paret Id separated by comma</returns>
        private string GetSubordinateUsers(string p_sUsersToInclude)
        {
            string sUserList = string.Empty;
            try
            {
                m_sSubordinateUsersList = "";
                sUserList = GetSubordinateUsersList(p_sUsersToInclude);

                if (sUserList != "")
                {
                    sUserList = sUserList + "," + p_sUsersToInclude;
                }
                else
                {
                    sUserList = p_sUsersToInclude;
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveApproval.GetSubordinateUsers.Error", m_iClientId), p_objException);
            }
            return sUserList;
        }

        /// Name		: GetSubordinateUsersList
        /// Author		: Saurav Mahajan
        /// Date Created: 11/12/2008
        /// <summary>
        /// Get Subordinate User ids
        /// </summary>
        /// <param name="p_sUsersToInclude">User Ids</param>
        /// <returns>String containing userids separated by comma</returns>
        private string GetSubordinateUsersList(string p_sUsersToInclude)
        {
            StringBuilder sbSQL = null;
            StringBuilder sbTemp = null;
            DbReader objRead = null;
            try
            {
                sbSQL = new StringBuilder();
                if (p_sUsersToInclude.Trim().Equals(""))
                {
                    return m_sSubordinateUsersList;
                }
                else
                {
                    sbSQL.Append("SELECT USER_ID FROM USER_TABLE WHERE MANAGER_ID IN (" + p_sUsersToInclude + ") AND USER_ID NOT IN (" + p_sUsersToInclude + ")");

                    objRead = DbFactory.GetDbReader(m_sSecureDSNConnectionString, sbSQL.ToString());
                    sbTemp = new StringBuilder();
                    while (objRead.Read())
                    {
                        if (!sbTemp.ToString().Trim().Equals(""))
                        {
                            if (!sbTemp.ToString().Substring(sbTemp.Length - 1).Equals(","))
                            {
                                sbTemp.Append(",");
                            }
                            sbTemp.Append(Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")));
                        }
                        else
                        {
                            sbTemp.Append(Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")));
                        }
                    }
                    if (sbTemp.ToString() != "")
                    {
                        if (m_sSubordinateUsersList != "")
                        {
                            m_sSubordinateUsersList = m_sSubordinateUsersList + "," + sbTemp.ToString();
                        }
                        else
                        {
                            m_sSubordinateUsersList = sbTemp.ToString();
                        }
                    }
                    GetSubordinateUsersList(sbTemp.ToString());
                    CloseReader(ref objRead);
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveApproval.GetSubordinateUsersList.Error", m_iClientId), p_objException);
            }
            finally
            {
                CloseReader(ref objRead);
                sbSQL = null;
                sbTemp = null;
            }
            return m_sSubordinateUsersList;
        }

        /// Name		: GetLOBForClaim
        /// Author		: Parag Sarin
        /// Date Created: 02/02/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Gets LOB for claim
        /// </summary>
        /// <param name="p_lClaimID">ClaimID</param>
        /// <returns>LOB</returns>
        private long GetLOBForClaim(long p_lClaimID)
        {
            long lLOBForClaim = 0;
            DbReader objRead = null;
            StringBuilder sbSQL = null;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_lClaimID.ToString());
                objRead = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                if (objRead.Read())
                {
                    lLOBForClaim = Conversion.ConvertObjToInt64(objRead.GetValue(0), base.ClientId);
                }
                CloseReader(ref objRead);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveApproval.GetLOBForClaim.Error", m_iClientId), p_objException);
            }
            finally
            {
                CloseReader(ref objRead);
                sbSQL = null;
            }
            return lLOBForClaim;
        }



        private string ShowItemsToAllUser(long p_lUser_Id)
        {
            string sTransIDList = string.Empty;
            StringBuilder sSql = null;
            CCacheFunctions objCCacheFunctions = null;
            DbReader objRead = null;
            bool bLOBOrTopUser = false;
            long lTop = 0;
            long lWC = 0;
            long lGC = 0;
            long lVA = 0;
            long lDI = 0;
            double dMaxAmount = 0.0;
            Dictionary<int, double> dictReserve = null;
            String sSqlReserve = string.Empty;
            List<ReserveLimit> lReserveLimits = null;
            StringBuilder sbSQL = null;
            LocalCache objCache = null;
            bool blnSuccess = false;
            string sQuery = string.Empty;
            DbReader objTempRead = null;
            //Start: rsushilaggar 11/23/2011 MITS-26332
            bool bTempValue = false;
            int iSubmittedToUser = 0;
            int iGroupID = 0;
            int iManagerGroupID = 0;
            StringBuilder sbSQLTemp = null;
            bool bUnderClaimIncLimit = false;//sachin 6385
            try
            {
                objCCacheFunctions = new CCacheFunctions(m_sDSN, m_iClientId);
                objCache = new LocalCache(m_sDSN,ClientId);
                if (m_bUseSupAppReserves)
                {
                    //RMA-8253 pgupta93 START
                    //sTransIDList = DownSupChain(m_lUserId, "True");
                    sTransIDList = DownSupChain(m_lUserId, "True","False");
                    //RMA-8253 pgupta93 END
                    if (!string.IsNullOrEmpty(sTransIDList))
                    {
                        sTransIDList = sTransIDList + ",";
                    }

                    if (m_bAllowAccessGrpSup)
                    {
                        //rsushilaggar Display the reserve worksheet to the user who is not in the supervisory chain 
                        //but in the same supervisor's SMS group 
                        //TO DO handle group iid
                        sQuery = "Select GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " + m_lUserId;
                        objTempRead = DbFactory.GetDbReader(m_sDSN, sQuery);
                        if (objTempRead.Read())
                        {
                            iGroupID = Common.Conversion.ConvertObjToInt(objTempRead.GetValue("GROUP_ID"), base.ClientId);
                        }
                        CloseReader(ref objTempRead);

                        if (iGroupID != 0)
                        {
                            sSqlReserve = "SELECT RL.LINE_OF_BUS_CODE,RELATED_CODE_ID,MAX_AMOUNT FROM RESERVE_LIMITS RL INNER JOIN CODES C ON C.CODE_ID = RL.RESERVE_TYPE_CODE WHERE USER_ID = " + m_lUserId + " OR GROUP_ID =" + iGroupID;
                        }
                        else
                        {
                            sSqlReserve = "SELECT RL.LINE_OF_BUS_CODE,RELATED_CODE_ID,MAX_AMOUNT FROM RESERVE_LIMITS RL INNER JOIN CODES C ON C.CODE_ID = RL.RESERVE_TYPE_CODE WHERE USER_ID = " + m_lUserId;
                        }
                        objRead = DbFactory.GetDbReader(m_sDSN, sSqlReserve);
                        //dictReserveAmount = new Dictionary<int, double>();
                        lReserveLimits = new List<ReserveLimit>();
                        while (objRead.Read())
                        {
                            ReserveLimit limit;
                            limit.iLOBID = Common.Conversion.ConvertObjToInt(objRead.GetValue("LINE_OF_BUS_CODE"), base.ClientId);
                            limit.iResType = Common.Conversion.ConvertObjToInt(objRead.GetValue("RELATED_CODE_ID"), base.ClientId);
                            limit.dMaxAmount = Common.Conversion.ConvertObjToDouble(objRead.GetValue("MAX_AMOUNT"), base.ClientId);
                            lReserveLimits.Add(limit);
                        }
                        CloseReader(ref objRead);



                        sbSQL = new StringBuilder();
                        if (iGroupID > 0)
                        {
                            //sbSQL.Append("SELECT RSW_ROW_ID,SUBMITTED_TO ");
                            //sbSQL.Append("FROM RSW_WORKSHEETS ");
                            //sbSQL.Append("WHERE RSW_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("PA", "RSW_STATUS").ToString());

                            sbSQL.Append("SELECT RSW_ROW_ID,RSW.CLAIM_ID,C.LINE_OF_BUS_CODE,RSW.SUBMITTED_TO FROM RSW_WORKSHEETS RSW ");
                            sbSQL.Append(" INNER JOIN CLAIM C ON C.CLAIM_ID = RSW.CLAIM_ID ");
                            sbSQL.Append(" WHERE RSW.RSW_STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("PA", "RSW_STATUS").ToString());

                            objRead = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());

                            while (objRead.Read())
                            {
                                sbSQLTemp = new StringBuilder();
                                dictReserve = new Dictionary<int, double>();
                                sbSQLTemp.Append("SELECT CODE_ID FROM CODES WHERE TABLE_ID = ");
                                sbSQLTemp.Append(objCache.GetTableId("MASTER_RESERVE"));
                                sbSQLTemp.Append(" AND (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL)");
                                objTempRead = DbFactory.GetDbReader(m_sDSN, sbSQLTemp.ToString());
                                while (objTempRead.Read())
                                {
                                    dictReserve.Add(objTempRead.GetInt32(0), 0);
                                }
                                CloseReader(ref objTempRead);
                                bool bRecordAdded = false;
                                bool bTemp = false;
                                GetRSWReserves(Conversion.CastToType<int>(objRead["CLAIM_ID"].ToString(), out blnSuccess), Conversion.CastToType<int>(objRead["RSW_ROW_ID"].ToString(), out blnSuccess), dictReserve);

                                bUnderClaimIncLimit = GetRSWReservesInc(Conversion.CastToType<int>(objRead["CLAIM_ID"].ToString(), out blnSuccess), Conversion.CastToType<int>(objRead["RSW_ROW_ID"].ToString(), out blnSuccess), m_lUserId, iGroupID); //sachin 6385

                                iSubmittedToUser = Common.Conversion.ConvertObjToInt(objRead.GetValue("SUBMITTED_TO"), base.ClientId);
                                if (iSubmittedToUser > 0)
                                {
                                    sQuery = "Select GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " + iSubmittedToUser;
                                    objTempRead = DbFactory.GetDbReader(m_sDSN, sQuery);
                                    if (objTempRead.Read())
                                    {
                                        iManagerGroupID = Common.Conversion.ConvertObjToInt(objTempRead.GetValue("GROUP_ID"), base.ClientId);
                                    }
                                    CloseReader(ref objTempRead);
                                    if (iManagerGroupID == iGroupID)
                                    {
                                        foreach (int iCodeId in dictReserve.Keys)
                                        {
                                            ReserveLimit temp = lReserveLimits.Find(c => c.iLOBID == Conversion.CastToType<int>(objRead["LINE_OF_BUS_CODE"].ToString(), out blnSuccess) && (c.iResType == iCodeId || c.iResType == 0));

                                            if (temp.dMaxAmount > 0 || bUnderClaimIncLimit)//sachin 6385
                                            {
                                                if ((temp.dMaxAmount >= dictReserve[iCodeId]) || bUnderClaimIncLimit)//sachin 6385
                                                {
                                                    bRecordAdded = true;
                                                }
                                                else
                                                {
                                                    bRecordAdded = false;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                bRecordAdded = true;
                                            }
                                        }
                                        //Add a logic to check the limit
                                        if (bRecordAdded)
                                        {
                                            sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("RSW_ROW_ID")) + ",";
                                        }
                                    }
                                }


                            }
                            CloseReader(ref objRead);
                        }
                        //End rsushilaggar                   
                    }
                }
                else
                {

                    sQuery = "Select GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " + p_lUser_Id;
                    objTempRead = DbFactory.GetDbReader(m_sDSN, sQuery);
                    if (objTempRead.Read())
                    {
                        iGroupID = Common.Conversion.ConvertObjToInt(objTempRead.GetValue("GROUP_ID"), base.ClientId);
                    }
                    CloseReader(ref objTempRead);
                    //TO DO handle group iid
                    if (iGroupID != 0)
                    {
                        sSqlReserve = "SELECT RL.LINE_OF_BUS_CODE,RELATED_CODE_ID,MAX_AMOUNT FROM RESERVE_LIMITS RL INNER JOIN CODES C ON C.CODE_ID = RL.RESERVE_TYPE_CODE WHERE USER_ID = " + p_lUser_Id + " OR GROUP_ID =" + iGroupID;
                    }
                    else
                    {
                        sSqlReserve = "SELECT RL.LINE_OF_BUS_CODE,RELATED_CODE_ID,MAX_AMOUNT FROM RESERVE_LIMITS RL INNER JOIN CODES C ON C.CODE_ID = RL.RESERVE_TYPE_CODE WHERE USER_ID = " + p_lUser_Id;
                    }
                    objRead = DbFactory.GetDbReader(m_sDSN, sSqlReserve);
                    //dictReserveAmount = new Dictionary<int, double>();
                    lReserveLimits = new List<ReserveLimit>();
                    while (objRead.Read())
                    {
                        ReserveLimit limit;
                        limit.iLOBID = Common.Conversion.ConvertObjToInt(objRead.GetValue("LINE_OF_BUS_CODE"), base.ClientId);
                        limit.iResType = Common.Conversion.ConvertObjToInt(objRead.GetValue("RELATED_CODE_ID"), base.ClientId);
                        limit.dMaxAmount = Common.Conversion.ConvertObjToDouble(objRead.GetValue("MAX_AMOUNT"), base.ClientId);
                        lReserveLimits.Add(limit);
                    }
                    CloseReader(ref objRead);



                    //Check to see who the top level users are
                    bLOBOrTopUser = GetTopApprovers(ref lTop, ref lWC, ref lGC, ref lVA, ref lDI);

                    sSql = new StringBuilder();
                    sSql.Append("SELECT RSW_ROW_ID,RSW.CLAIM_ID,C.LINE_OF_BUS_CODE FROM RSW_WORKSHEETS RSW ");
                    sSql.Append(" INNER JOIN CLAIM C ON C.CLAIM_ID = RSW.CLAIM_ID ");
                    sSql.Append(" WHERE RSW.RSW_STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("PA", "RSW_STATUS").ToString());
                    objRead = DbFactory.GetDbReader(m_sDSN, sSql.ToString());
                    while (objRead.Read())
                    {
                        sbSQL = new StringBuilder();
                        dictReserve = new Dictionary<int, double>();
                        sbSQL.Append("SELECT CODE_ID FROM CODES WHERE TABLE_ID = ");
                        sbSQL.Append(objCache.GetTableId("MASTER_RESERVE"));
                        sbSQL.Append(" AND (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL)");
                        objTempRead = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                        while (objTempRead.Read())
                        {
                            dictReserve.Add(objTempRead.GetInt32(0), 0);
                        }
                        CloseReader(ref objTempRead);
                        bool bRecordAdded = false;
                        bool bTemp = false;
                        GetRSWReserves(Conversion.CastToType<int>(objRead["CLAIM_ID"].ToString(), out blnSuccess), Conversion.CastToType<int>(objRead["RSW_ROW_ID"].ToString(), out blnSuccess), dictReserve);
                        if (bLOBOrTopUser)
                        {
                            if (lWC == m_lUserId && objCCacheFunctions.GetCodeIDWithShort("WC", "LINE_OF_BUSINESS") == Common.Conversion.ConvertObjToInt(objRead.GetValue("LINE_OF_BUS_CODE"), base.ClientId))
                            {
                                dMaxAmount = GetTopApproverMaxAmount(p_lUser_Id, objCCacheFunctions.GetCodeIDWithShort("WC", "LINE_OF_BUSINESS"));
                                bTemp = true;
                            }
                            if (lGC == m_lUserId && objCCacheFunctions.GetCodeIDWithShort("GC", "LINE_OF_BUSINESS") == Common.Conversion.ConvertObjToInt(objRead.GetValue("LINE_OF_BUS_CODE"), base.ClientId))
                            {
                                dMaxAmount = GetTopApproverMaxAmount(p_lUser_Id, objCCacheFunctions.GetCodeIDWithShort("GC", "LINE_OF_BUSINESS"));
                                bTemp = true;
                            }
                            if (lVA == m_lUserId && objCCacheFunctions.GetCodeIDWithShort("VA", "LINE_OF_BUSINESS") == Common.Conversion.ConvertObjToInt(objRead.GetValue("LINE_OF_BUS_CODE"), base.ClientId))
                            {
                                dMaxAmount = GetTopApproverMaxAmount(p_lUser_Id, objCCacheFunctions.GetCodeIDWithShort("VA", "LINE_OF_BUSINESS"));
                                bTemp = true;
                            }
                            if (lDI == m_lUserId && objCCacheFunctions.GetCodeIDWithShort("DI", "LINE_OF_BUSINESS") == Common.Conversion.ConvertObjToInt(objRead.GetValue("LINE_OF_BUS_CODE"), base.ClientId))
                            {
                                dMaxAmount = GetTopApproverMaxAmount(p_lUser_Id, objCCacheFunctions.GetCodeIDWithShort("DI", "LINE_OF_BUSINESS"));
                                bTemp = true;
                            }

                            if (lTop == m_lUserId)
                            {
                                bRecordAdded = true;
                            }
                            else
                            {
                                if (bTemp)
                                {
                                    double amount = 0;
                                    foreach (int iCodeId in dictReserve.Keys)
                                    {
                                        amount = amount + dictReserve[iCodeId];
                                    }

                                    if (dMaxAmount > 0)
                                    {
                                        if (amount <= dMaxAmount)
                                            bRecordAdded = true;
                                        else
                                        {
                                            bRecordAdded = false;
                                            break;
                                        }
                                    }

                                }

                            }
                        }
                        else
                        {
                            foreach (int iCodeId in dictReserve.Keys)
                            {
                                ReserveLimit temp = lReserveLimits.Find(c => c.iLOBID == Conversion.CastToType<int>(objRead["LINE_OF_BUS_CODE"].ToString(), out blnSuccess) && (c.iResType == iCodeId || c.iResType == 0));
                                if (temp.dMaxAmount > 0)
                                {
                                    if (temp.dMaxAmount >= dictReserve[iCodeId])
                                    {
                                        bRecordAdded = true;
                                    }
                                    else
                                    {
                                        bRecordAdded = false;
                                        break;
                                    }
                                }
                                else
                                {
                                    bRecordAdded = true;
                                }
                            }
                        }

                        if (bRecordAdded)
                        {
                            sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("RSW_ROW_ID")) + ",";
                        }
                    }


                }

                if (sTransIDList.Trim().Length > 0)
                {
                    if (sTransIDList.Substring(sTransIDList.Length - 1).Equals(","))
                    {
                        sTransIDList = sTransIDList.Substring(0, sTransIDList.Length - 1);
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SupervisorApproval.DownSupChain.Error", m_iClientId), p_objException);
            }
            finally
            {
                CloseReader(ref objRead);
                objCCacheFunctions = null;
                sSql = null;
            }

            return sTransIDList;
        }

        private double GetTopApproverMaxAmount(long p_lUserId, int p_iLineOfBus)
        {
            double dMaxAmount = 0;
            DbReader objRead = null;
            StringBuilder sbSQL = null;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT RESERVE_MAX FROM LOB_SUP_APPROVAL WHERE USER_ID = " + p_lUserId.ToString() + " AND LOB_CODE = " + p_iLineOfBus.ToString());
                objRead = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                if (objRead.Read())
                {
                    dMaxAmount = Conversion.ConvertObjToDouble(objRead.GetValue("RESERVE_MAX"), base.ClientId);
                }
                else
                {
                    dMaxAmount = -1;
                }
                CloseReader(ref objRead);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SupervisorApproval.GetMaxAmount.Error", m_iClientId), p_objException);
            }
            finally
            {
                CloseReader(ref objRead);
                sbSQL = null;
            }
            return dMaxAmount;
        }

        //RMA-344       achouhan3       Enables Sorting for Reserve Worksheet Starts
        #region List Funds Tranasction

        /// Name		: ListFundsTransactions
        /// Author		: achouhan3
        /// Date Created: 01/22/2015		
        /// <summary>
        /// Lists funds transactions
        /// </summary>
        /// <param name="p_objOutAllTran">All transactions XML</param>
        /// /// <param name="p_objInAllTran">request XML with all fields</param>
        /// <returns>xmlDocument</returns>
        public XmlDocument ListFundsTransactions(out XmlDocument p_objOutAllTran, XmlDocument p_objInAllTran)
        {
            int iPendingApprovalCode = 0;
            StringBuilder sbSQL = null;
            StringBuilder sbReservesList = null;
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;

            DbReader objRead = null;
            DataSet objDS = null;
            LocalCache objCache = null;
            Dictionary<int, double> dictReserve = null;
            p_objOutAllTran = null;
            bool bOracleDB = false;
            bool blnSuccess = false;
            string sHidden = "false";
            string sSupChain = "";
            string p_sShowAllItem = String.Empty;
            string p_sShowMyPendingTrans = String.Empty;
            string strSortOrder = String.Empty;
            string strSortColumn= String.Empty;
            DateTime dtReserve;
            try
            {
                //shobhana For customization of Transations
                AssignReserveWorksheet("", "");
                if (p_objInAllTran.SelectSingleNode("//ShowAllItems") != null)
                    p_sShowAllItem = p_objInAllTran.SelectSingleNode("//ShowAllItems").InnerText;
                else
                    p_sShowAllItem = "False";
                if (p_objInAllTran.SelectSingleNode("//SortOrder") != null)
                    strSortOrder = p_objInAllTran.SelectSingleNode("//SortOrder").InnerText;
                if (p_objInAllTran.SelectSingleNode("//ShowAllItems") != null)
                    strSortColumn = p_objInAllTran.SelectSingleNode("//SortColumn").InnerText;
                //RMA-8253 pgupta93 START
                if (p_objInAllTran.SelectSingleNode("//ShowMyTrans") != null)
                {
                    p_sShowMyPendingTrans = p_objInAllTran.SelectSingleNode("//ShowMyTrans").InnerText;
                }
                //RMA-8253 pgupta93 END
                if (p_sShowAllItem == "True")
                {
                    sSupChain = ShowItemsToAllUser(m_lUserId);
                }
                else
                {
                    //RMA-8253 pgupta93 START
                    //sSupChain = DownSupChain(m_lUserId, p_sShowAllItem);
                    sSupChain = DownSupChain(m_lUserId, p_sShowAllItem,p_sShowMyPendingTrans);
                    //RMA-8253 pgupta93 END
                }
                if (sSupChain.Trim().Equals(""))
                    sSupChain = "0";
                objCache = new LocalCache(m_sDSN, ClientId);
                sbReservesList = new StringBuilder();
                sbSQL = new StringBuilder();
                dictReserve = new Dictionary<int, double>();
                sbSQL.Append("SELECT CODE_ID FROM CODES WHERE TABLE_ID = ");
                sbSQL.Append(objCache.GetTableId("MASTER_RESERVE"));
                sbSQL.Append(" AND (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL)");
                objRead = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                if (objRead.ConnectionType == eConnectionType.ManagedOracleServer)
                    bOracleDB = true;
                sbSQL.Remove(0, sbSQL.Length);
                while (objRead.Read())
                {
                    dictReserve.Add(objRead.GetInt32(0), 0);
                    sbReservesList.Append("0 " + objCache.GetCodeDesc(objRead.GetInt32(0)) + "Reserve, ");
                }
                CloseReader(ref objRead);
                iPendingApprovalCode = objCache.GetCodeId("PA", "RSW_STATUS");

                if (bOracleDB)
                {
                    sbSQL.Append("SELECT R.RSW_ROW_ID RswRowId, R.RSW_STATUS_CODE RswStatus, C.CLAIM_NUMBER ClaimNumber, C.CLAIM_ID ClaimID, C.CLAIM_STATUS_CODE ClaimStatusCode, C.DATE_OF_CLAIM DateOfClaim, R.DTTM_RCD_LAST_UPD DateSubmitted, ");
                    sbSQL.Append("E.DATE_OF_EVENT DateOfEvent, R.SUBMITTED_BY SubmittedBy, ");
                    sbSQL.Append(sbReservesList);
                    sbSQL.Append("NULL TotalPaid, ");
                    sbSQL.Append("(SELECT ET.FIRST_NAME || ' ' || ET.LAST_NAME FROM ENTITY ET, CLAIMANT CM WHERE CM.CLAIM_ID=C.CLAIM_ID AND CM.CLAIMANT_EID=ET.ENTITY_ID AND CM.PRIMARY_CLMNT_FLAG = -1) PrimaryClaimant, ");
                    sbSQL.Append("(SELECT ET.FIRST_NAME || ' ' || ET.LAST_NAME FROM ENTITY ET, CLAIM_ADJUSTER CA WHERE CA.CLAIM_ID=C.CLAIM_ID AND ADJUSTER_EID= ET.ENTITY_ID AND CURRENT_ADJ_FLAG = -1) CurrentAdjuster ");

                    //6385
                    //sbSQL.Append(" ,nvl(HOLD_REASON.HOLD_REASON_CODE,0) HOLDREASON");  //JIRA   7810 
                    sbSQL.Append(" ,nvl( (SELECT LISTAGG(CD.SHORT_CODE, ',') ");
                    sbSQL.Append(" WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID) AS SHORT_CODE");
                    sbSQL.Append(" FROM HOLD_REASON INNER JOIN CODES ON HOLD_REASON.HOLD_REASON_CODE = CODES.CODE_ID");
                    sbSQL.Append(" INNER JOIN  CODES CD ON CODES.RELATED_CODE_ID = CD.CODE_ID");
                    sbSQL.Append(" WHERE HOLD_STATUS_CODE = " + iPendingApprovalCode.ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("RSW_WORKSHEETS"));
                    sbSQL.Append(" AND ON_HOLD_ROW_ID = R.RSW_ROW_ID  GROUP BY ON_HOLD_ROW_ID),0)  AS HOLD_REASON_CODE");


                    sbSQL.Append(" ,nvl( (SELECT LISTAGG(CT.CODE_DESC, ',') WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID)");
                    sbSQL.Append(" FROM HOLD_REASON INNER JOIN CODES  C ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID");
                    sbSQL.Append(" INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID  WHERE HOLD_STATUS_CODE =" + iPendingApprovalCode.ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("RSW_WORKSHEETS"));
                    sbSQL.Append(" AND ON_HOLD_ROW_ID = R.RSW_ROW_ID  GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLDREASON");

                    //6385
                    //sbSQL.Append("FROM RSW_WORKSHEETS R, CLAIM C, EVENT E ");
                    sbSQL.Append(" FROM RSW_WORKSHEETS R");
                    //sbSQL.Append(" LEFT OUTER JOIN  HOLD_REASON ON  R.RSW_ROW_ID = HOLD_REASON.ON_HOLD_ROW_ID AND HOLD_REASON.ON_HOLD_TABLE_ID = " + objCache.GetTableId("RSW_WORKSHEETS"));//7810
                    sbSQL.Append(" , CLAIM C, EVENT E ");

                    sbSQL.Append(" WHERE C.CLAIM_ID=R.CLAIM_ID AND C.EVENT_ID = E.EVENT_ID ");
                    sbSQL.Append(" AND R.RSW_STATUS_CODE = " + iPendingApprovalCode.ToString());
                    //sbSQL.Append("AND R.SUBMITTED_TO =" + UserId.ToString() + " ORDER BY DateOfEvent");
                    sbSQL.Append(" AND R.RSW_ROW_ID IN (" + sSupChain + ") ORDER BY ");


                }
                else
                {
                    sbSQL.Append("SELECT R.RSW_ROW_ID RswRowId, R.RSW_STATUS_CODE RswStatus, C.CLAIM_NUMBER ClaimNumber, C.CLAIM_ID ClaimID, C.CLAIM_STATUS_CODE ClaimStatusCode, C.DATE_OF_CLAIM DateOfClaim, R.DTTM_RCD_LAST_UPD DateSubmitted,");
                    sbSQL.Append("E.DATE_OF_EVENT DateOfEvent, R.SUBMITTED_BY SubmittedBy, ");
                    sbSQL.Append(sbReservesList);
                    sbSQL.Append("NULL AS 'TotalPaid', ");
                    sbSQL.Append("(SELECT COALESCE(ET.FIRST_NAME,' ') + ' ' + COALESCE(ET.LAST_NAME,' ') FROM ENTITY ET, CLAIMANT CM WHERE CM.CLAIM_ID=C.CLAIM_ID AND CM.CLAIMANT_EID=ET.ENTITY_ID AND CM.PRIMARY_CLMNT_FLAG = -1) AS 'PrimaryClaimant', ");
                    sbSQL.Append("(SELECT COALESCE(ET.FIRST_NAME,' ') + ' ' + COALESCE(ET.LAST_NAME,' ') FROM ENTITY ET, CLAIM_ADJUSTER CA WHERE CA.CLAIM_ID=C.CLAIM_ID AND ADJUSTER_EID= ET.ENTITY_ID AND CURRENT_ADJ_FLAG = -1) AS 'CurrentAdjuster' ");

                    //sbSQL.Append("FROM RSW_WORKSHEETS R, CLAIM C, EVENT E ");


                    //6385
                    //sbSQL.Append(" ,ISNULL(HOLD_REASON.HOLD_REASON_CODE,0) HOLDREASON ");//7810 
                    sbSQL.Append(" ,ISNULL(  (SELECT STUFF((SELECT ', ' + CD.SHORT_CODE   FROM HOLD_REASON  INNER JOIN CODES");
                    sbSQL.Append(" ON HOLD_REASON.HOLD_REASON_CODE = CODE_ID");
                    sbSQL.Append(" INNER JOIN  CODES CD ON CODES.RELATED_CODE_ID = CD.CODE_ID");
                    sbSQL.Append(" WHERE ON_HOLD_ROW_ID = HR.ON_HOLD_ROW_ID");
                    sbSQL.Append(" AND HOLD_STATUS_CODE = " + iPendingApprovalCode.ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("RSW_WORKSHEETS"));
                    sbSQL.Append(" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE");
                    sbSQL.Append(" FROM HOLD_REASON  HR WHERE HOLD_STATUS_CODE ="+ iPendingApprovalCode.ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("RSW_WORKSHEETS"));
                    sbSQL.Append(" AND ON_HOLD_ROW_ID = R.RSW_ROW_ID GROUP BY ON_HOLD_ROW_ID ) ,'') AS HOLD_REASON_CODE");

                    
                    sbSQL.Append(" ,ISNULL(  (SELECT STUFF((SELECT ', ' + CT.CODE_DESC   FROM HOLD_REASON  INNER JOIN CODES  C");
                    sbSQL.Append(" ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID INNER JOIN CODES_TEXT CT");
                    sbSQL.Append(" ON C.CODE_ID = CT.CODE_ID  WHERE ON_HOLD_ROW_ID = HR.ON_HOLD_ROW_ID");
                    sbSQL.Append(" AND HOLD_STATUS_CODE =" + iPendingApprovalCode.ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("RSW_WORKSHEETS"));
                    sbSQL.Append(" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE_DESC");
                    sbSQL.Append(" FROM HOLD_REASON  HR WHERE HOLD_STATUS_CODE =" + iPendingApprovalCode.ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("RSW_WORKSHEETS"));
                    sbSQL.Append(" AND ON_HOLD_ROW_ID =R.RSW_ROW_ID GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLDREASON");
                    //6385
                    sbSQL.Append(" FROM RSW_WORKSHEETS R");
                    //sbSQL.Append(" LEFT OUTER JOIN  HOLD_REASON ON  R.RSW_ROW_ID = HOLD_REASON.ON_HOLD_ROW_ID AND HOLD_REASON.ON_HOLD_TABLE_ID = " + objCache.GetTableId("RSW_WORKSHEETS"));//7810
                    sbSQL.Append(" , CLAIM C, EVENT E ");

                    sbSQL.Append(" WHERE C.CLAIM_ID=R.CLAIM_ID AND C.EVENT_ID = E.EVENT_ID ");
                    sbSQL.Append(" AND R.RSW_STATUS_CODE = " + iPendingApprovalCode.ToString());
                    //sbSQL.Append("AND R.SUBMITTED_TO =" + UserId.ToString() + " ORDER BY DateOfEvent");
                    sbSQL.Append(" AND R.RSW_ROW_ID IN (" + sSupChain + ") ORDER BY ");
                }
                
                if (!String.IsNullOrEmpty(strSortColumn))
                {
                    sbSQL.Append(strSortColumn);
                    sbSQL.Append(" ");
                    sbSQL.Append(strSortOrder);
                    sbSQL.Append(",");
                }
                if (!strSortColumn.Contains("DateOfEvent"))
                    sbSQL.Append(" DateOfEvent");
                
                
                objDS = DbFactory.GetDataSet(m_sDSN, sbSQL.ToString().TrimEnd(','), m_iClientId);
                sbSQL.Remove(0, sbSQL.Length);
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("ReserveApproval");
                objDOM.AppendChild(objElemParent);

                foreach (DataRow dr in objDS.Tables[0].Rows)
                {
                    GetRSWReserves(Conversion.CastToType<int>(dr["ClaimID"].ToString(), out blnSuccess), Conversion.CastToType<int>(dr["RswRowId"].ToString(), out blnSuccess), dictReserve);
                    blnSuccess = false;
                    foreach (DataColumn dc in objDS.Tables[0].Columns)
                    {
                        if (dc.ColumnName.ToUpper().Contains("DATE"))
                        {
                            bool IsDate = DateTime.TryParse(dr[dc.ColumnName].ToString(), out dtReserve);
                            if (IsDate)
                                dr[dc.ColumnName] = dtReserve.ToString("mm/dd/yyyy");// Conversion.ToDate(dr[dc.ColumnName].ToString()).ToShortDateString();  
                            else
                                dr[dc.ColumnName] = Conversion.ToDate(dr[dc.ColumnName].ToString()).ToShortDateString();
                        }
                        else if (dc.ColumnName.ToUpper().Contains("SUBMITTEDBY"))
                        {
                            dr[dc.ColumnName] = GetSubmittedToUser(dr[dc.ColumnName].ToString());
                            sHidden = "true";
                        }
                        else if (dc.ColumnName.ToUpper().Contains("RESERVE"))
                        {
                            foreach (int iCodeId in dictReserve.Keys)
                            {
                                if (dc.ColumnName.ToUpper().Replace("RESERVE", "").Equals(objCache.GetCodeDesc(iCodeId).ToUpper()))
                                {
                                    dr[dc.ColumnName] = dictReserve[iCodeId];
                                    dictReserve[iCodeId] = 0;
                                    break;
                                }
                            }
                        }
                        //gagnihotri MITS 19097
                        else if (dc.ColumnName.ToUpper().Contains("TOTALPAID"))
                        {
                            if (base.dictClaimPaidToDate.ContainsKey(Conversion.CastToType<int>(dr["ClaimID"].ToString(), out blnSuccess)))
                            {
                                dr[dc.ColumnName] = base.dictClaimPaidToDate[Conversion.CastToType<int>(dr["ClaimID"].ToString(), out blnSuccess)];
                            }
                        }
                    }
                }
                DataTable dtTemp = new DataTable();
                if (objDS.Tables.Count > 0 && !String.IsNullOrEmpty(strSortColumn) && !strSortColumn.ToUpper().Contains("DATE"))
                    objDS.Tables[0].DefaultView.Sort = strSortColumn + " " + strSortOrder;
                dtTemp = objDS.Tables[0].DefaultView.ToTable();
                foreach (DataRow dr in dtTemp.Rows)
                {

                    objElemChild = objDOM.CreateElement("Approvals");
                    foreach (DataColumn dc in dtTemp.Columns)
                    {
                        objElemTemp = objDOM.CreateElement(dc.ColumnName);
                        if (dc.ColumnName.ToUpper().Contains("DATE") && dc.ColumnName.ToUpper().Equals("DATESUBMITTED"))
                        {
                            sHidden = "true";
                            objElemTemp.InnerText =Convert.ToString(dr[dc.ColumnName]);
                        }
                        else if (dc.ColumnName.ToUpper().Contains("SUBMITTEDBY"))
                        {
                            sHidden = "true";
                            objElemTemp.InnerText = Convert.ToString(dr[dc.ColumnName]);
                        }
                        else if (dc.ColumnName.ToUpper().Contains("RESERVE"))
                        {
                            objElemTemp.InnerText = string.Format("{0:C}", dr[dc.ColumnName]);
                        }
                        else if (dc.ColumnName.ToUpper().Contains("TOTALPAID"))
                        {
                            if (base.dictClaimPaidToDate.ContainsKey(Conversion.CastToType<int>(dr["ClaimID"].ToString(), out blnSuccess)))
                            {
                                objElemTemp.InnerText = string.Format("{0:C}", dr[dc.ColumnName]);
                            }
                        }
                        //JIRA 7810 Start Snehal
                        else if (dc.ColumnName.ToUpper().Contains("HOLDREASON"))
                        {
                            objElemTemp.InnerText = dr[dc.ColumnName].ToString();
                        }
                        ////JIRA 7810 End Snehal 
                        else
                            objElemTemp.InnerText = Convert.ToString(dr[dc.ColumnName]);
                        if (dc.ColumnName.ToUpper().Contains("RSWROWID") || dc.ColumnName.ToUpper().Contains("CLAIMSTATUSCODE") ||
                                dc.ColumnName.ToUpper().Contains("CLAIMID") || dc.ColumnName.ToUpper().Contains("RSWSTATUS") || dc.ColumnName.ToUpper().Contains("HOLD_REASON_CODE"))
                        {
                            sHidden = "true";
                        }
                        objElemTemp.SetAttribute("hidden", sHidden);
                        objElemChild.AppendChild(objElemTemp);
                        sHidden = "false";
                    }
                    objDOM.FirstChild.AppendChild(objElemChild);
                }


                p_objOutAllTran = objDOM;
                dtTemp = null;
               

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveApproval.ListFundsTransactions.Error", m_iClientId), p_objException);
            }
            finally
            {
                CloseReader(ref objRead);
                sbSQL = null;
                sbReservesList = null;
                objDOM = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objDS != null)
                {
                    objDS.Dispose();
                    objDS = null;
                }
            }
            return p_objOutAllTran;
        }
        
        #endregion
        //RMA-344       achouhan3       Enables Sorting for Reserve Worksheet Ends
    }
}
