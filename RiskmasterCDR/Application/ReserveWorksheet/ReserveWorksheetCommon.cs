﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.ReserveWorksheet
{
    public class ReserveWorksheetCommon
    {

        public string Connectionstring { get; set; }

        public string UserName { get; set; }

        public string UserPassword { get; set; }

        public int UserId { get; set; }

        public string DSN { get; set; }

        public string SecurityConnectionstring { get; set; }

        public int DSNID { get; set; }

        public int ManagerId { get; set; }

    }
}
