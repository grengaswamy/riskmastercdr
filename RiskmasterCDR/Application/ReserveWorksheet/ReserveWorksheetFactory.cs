﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.ReserveWorksheet
{
    public class ReserveWorksheetFactory<T>
    {
        public ReserveWorksheetCommon m_CommonInfo { get; set; }

        /// <summary>
        /// ClientID for cloud
        /// </summary>
        private int m_iClientId = 0;

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_sConnStr"></param>
        public ReserveWorksheetFactory(string p_sConnStr, int p_iClientId)
        {
            m_CommonInfo = new ReserveWorksheetCommon();
            m_CommonInfo.Connectionstring = p_sConnStr;
            m_iClientId = p_iClientId;
        }

        public ReserveWorksheetFactory(string p_sConStr, string p_sSecurityConnectionString, int p_iUserID, int p_iDSNID,int p_iClientId)
        {
            m_CommonInfo = new ReserveWorksheetCommon();
            m_CommonInfo.Connectionstring = p_sConStr;
            m_CommonInfo.SecurityConnectionstring = p_sSecurityConnectionString;
            m_CommonInfo.UserId = p_iUserID;
            m_CommonInfo.DSNID = p_iDSNID;
            m_iClientId = p_iClientId;
        }

        public ReserveWorksheetFactory(string p_sUserName, string p_sPassword, string p_sDSN, string p_sConStr, int p_iClientId)
        {
            m_CommonInfo = new ReserveWorksheetCommon();
            m_CommonInfo.Connectionstring = p_sConStr;
            m_CommonInfo.UserName = p_sUserName;
            m_CommonInfo.UserPassword = p_sPassword;
            m_CommonInfo.DSN = p_sDSN;
            m_iClientId = p_iClientId;
        }

        public ReserveWorksheetFactory(string p_sUserName, string p_sPassword, int p_iUserID, string p_sDSN, string p_sConStr, string p_sSecurityConnectionString, int p_iDSNID, int p_iManagerID, int p_iClientId)
        {
            m_CommonInfo = new ReserveWorksheetCommon();
            m_CommonInfo.Connectionstring = p_sConStr;
            m_CommonInfo.SecurityConnectionstring = p_sSecurityConnectionString;
            m_CommonInfo.UserId = p_iUserID;
            m_CommonInfo.DSNID = p_iDSNID;
            m_CommonInfo.UserName = p_sUserName;
            m_CommonInfo.UserPassword = p_sPassword;
            m_CommonInfo.DSN = p_sDSN;
            m_CommonInfo.ManagerId = p_iManagerID;
            m_iClientId = p_iClientId;
        }

        #endregion


        public T GetReserveWorksheet(string p_ClassName)
        {
            switch (p_ClassName)
            {
                case "Customize":
                    {
                        return (T)Convert.ChangeType(new ReserveWorksheetCustomize(m_CommonInfo, m_iClientId), typeof(T));
                    }
                case "Generic":
                case "":        //tmalhotra2 -Mits 27272
                    {
                        return (T)Convert.ChangeType(new ReserveWorksheetGeneric(m_CommonInfo, m_iClientId), typeof(T));
                    }
            }
            return default(T);
        }
    }
}
