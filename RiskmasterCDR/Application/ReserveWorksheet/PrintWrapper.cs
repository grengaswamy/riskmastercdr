﻿using System;
using C1.C1PrintDocument;
using System.Drawing;
using System.Drawing.Imaging;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.IO;

namespace Riskmaster.Application.ReserveWorksheet
{
    /**************************************************************
     * $File		: PrintWrapper.cs
     * $Revision	: 1.0.0.0
     * $Date		: 09/17/2008
     * $Author		: Mohit Yadav
     * $Comment		: PrintWrapper class has basic functions to create Print Document.
     * $Source		:  	
    **************************************************************/
    public class PrintWrapper
    {//MGaba2:R6:Change the Protection typr to public
        #region Variable Declarations
        /// <summary>
        /// Private variable to store C1PrintDocumnet object instance.
        /// </summary>
        C1PrintDocument m_objPrintDoc = null;
        /// <summary>
        /// Private variable to store CurrentX
        /// </summary>
        private double m_dblCurrentX = 0;
        /// <summary>
        /// Private variable to store CurrentY
        /// </summary>
        private double m_dblCurrentY = 0;
        /// <summary>
        /// Private variable to store Font object.
        /// </summary>
        private Font m_objFont = null;
        /// <summary>
        /// Private variable to store Text object.
        /// </summary>
        private RenderText m_objText = null;
        /// <summary>
        /// Private variable to store PageWidth
        /// </summary>
        private double m_dblPageWidth = 0.0;
        /// <summary>
        /// Private variable to store PageHeight
        /// </summary>
        private double m_dblPageHeight = 0.0;
        /// <summary>
        /// Private variable to store CurrentX
        /// </summary>
        private double m_dblPageLimit = 0;

        private int m_iClientId = 0;

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor, initializes the variables to the default value
        /// </summary>		
        internal PrintWrapper(int p_iClientId)
        {
            m_objPrintDoc = new C1PrintDocument();
            m_objPrintDoc.DefaultUnit = UnitTypeEnum.Twip;
            m_objPrintDoc.PageSettings.Margins.Top = 10;
            m_objPrintDoc.PageSettings.Margins.Left = 10;
            m_objPrintDoc.PageSettings.Margins.Bottom = 10;
            m_objPrintDoc.PageSettings.Margins.Right = 10;
            m_iClientId = p_iClientId;
        }
        #endregion

        /// <summary>
        /// Initialize the document
        /// </summary>
        /// <param name="p_bIsLandscape">IsLandscape</param>
        internal void StartDoc(bool p_bIsLandscape)
        {
            try
            {
                m_objPrintDoc.PageSettings.Landscape = p_bIsLandscape;

                if (p_bIsLandscape)
                {
                    m_dblPageHeight = 8.5 * 1440;
                    m_dblPageWidth = 11 * 1440;
                }
                else
                {
                    m_dblPageHeight = 11 * 1440;
                    m_dblPageWidth = 8.5 * 1440;
                }
                m_objPrintDoc.StartDoc();

                m_objFont = new Font("Verdana", 10);
                m_objText = new RenderText(m_objPrintDoc);
                m_objText.Style.Font = m_objFont;
                m_objText.Style.WordWrap = false;
                m_objText.Width = m_dblPageWidth;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.StartDoc.Error",m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Read property for PageHeight.
        /// </summary>
        internal double PageHeight
        {
            get
            {
                return (m_dblPageHeight);
            }
        }
        /// <summary>
        /// Read property for PageWidth.
        /// </summary>
        internal double PageWidth
        {
            get
            {
                return (m_dblPageWidth);
            }
        }
        /// <summary>
        /// Read/Write property for CurrentX.
        /// </summary>
        internal double CurrentX
        {
            get
            {
                return (m_dblCurrentX);
            }
            set
            {
                m_dblCurrentX = value;
            }
        }
        /// <summary>
        /// Read/Write property for CurrentY.
        /// </summary>
        internal double CurrentY
        {
            get
            {
                return (m_dblCurrentY);
            }
            set
            {
                m_dblCurrentY = value;
            }
        }

        /// <summary>
        /// Read property for FontSize.
        /// </summary>
        internal double FontSize
        {
            get
            {
                return (m_objFont.Size);
            }
        }

        /// <summary>
        /// Read property for FontSize.
        /// </summary>
        internal string FontName
        {
            get
            {
                return (m_objFont.Name);
            }
        }

        /// <summary>
        /// Read property for PageHeight.
        /// </summary>
        internal double PageLimit
        {
            set
            {
                m_dblPageLimit = value;
            }
            get
            {
                return m_dblPageLimit;
            }
        }
        /// <summary>
        /// Render Text on the document.
        /// </summary>
        /// <param name="p_sPrintText">String text to print</param>
        internal void PrintText(string p_sPrintText)
        {
            try
            {
                m_objText.Text = p_sPrintText + "\r";
                m_objText.AutoWidth = true;
                m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintText.Error",m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Render Text on the document with no "\r" on the end.
        /// </summary>
        /// <param name="p_sPrintText">String text to print</param>
        internal void PrintTextNoCr(string p_sPrintText)
        {
            try
            {
                m_objText.Text = p_sPrintText + "";
                m_objText.AutoWidth = true;
                m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintTextNoCr.Error",m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// MGaba2:MITS 18534: Created to Print Text which takes more space than available like Claimant Name
        /// </summary>
        /// <param name="p_sPrintText"></param>
        /// <param name="p_colLength"></param>
        /// <returns></returns>
        /// Added:Yukti, Dt:01/08/2014, MITS 30252, Added flag for to wrap Comments properly
        /// //internal int PrintMultiLineText(string p_sPrintText, double p_colLength)
        internal int PrintMultiLineText(string p_sPrintText, double p_colLength, bool bCommentsMultiLine)
        {
            
            int iCount = 0;
            double dWidthAlreadPrinted = 0;
            double p_charHeight = GetTextHeight("W");
            try
            {
                    if (p_sPrintText != null)
                    {
                        for (int i = 0; i < p_sPrintText.Length; i++)
                        {
                            //Added:Yukti, Dt:01/08/2014, MITS 30252
                            //if ((dWidthAlreadPrinted + GetTextWidth(p_sPrintText.Substring(i, 1))) > (p_colLength - GetTextWidth("M")))
                                if (((dWidthAlreadPrinted + GetTextWidth(p_sPrintText.Substring(i, 1))) > (p_colLength - GetTextWidth("M"))) && !bCommentsMultiLine)
                                {
                                    iCount += 1;
                                    dWidthAlreadPrinted = 0;
                                    if ((p_sPrintText.Substring(i, 1) != " ") && (p_sPrintText.Substring(i - 1, 1) != " "))
                                    {

                                        m_objText.Text = "-";
                                        m_objText.AutoWidth = true;
                                        m_objPrintDoc.RenderDirect(m_dblCurrentX + dWidthAlreadPrinted, m_dblCurrentY + (iCount * p_charHeight), m_objText);
                                        dWidthAlreadPrinted += GetTextWidth("-");
                                    }
                                }
                            //added:Yukti,DT:01/08/2014, MITS 30252
                                else if (((dWidthAlreadPrinted + GetTextWidth(p_sPrintText.Substring(i, 1))) > (p_colLength * 3) - 100) && bCommentsMultiLine)
                                {
                                    iCount += 1;
                                    dWidthAlreadPrinted = 0;
                                    if ((p_sPrintText.Substring(i, 1) != " ") && (p_sPrintText.Substring(i - 1, 1) != " "))
                                    {

                                        m_objText.Text = "-";
                                        m_objText.AutoWidth = true;
                                        m_objPrintDoc.RenderDirect(m_dblCurrentX + dWidthAlreadPrinted, m_dblCurrentY + (iCount * p_charHeight), m_objText);
                                        dWidthAlreadPrinted += GetTextWidth("-");
                                    }

                                }
                            //Ended:Yukti,Dt:01/08/2014, MITS 30252
                            m_objText.Text = p_sPrintText.Substring(i, 1) + "";
                            m_objText.AutoWidth = true;
                            m_objPrintDoc.RenderDirect(m_dblCurrentX + dWidthAlreadPrinted, m_dblCurrentY + (iCount * p_charHeight), m_objText);
                            dWidthAlreadPrinted += GetTextWidth(p_sPrintText.Substring(i, 1));
                        }
                    }
                return iCount + 1;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintMultiLineText.Error", m_iClientId), p_objEx);
            }
        }
        /// <summary>
        /// Print text such that it ends at given X Co-ordinate.
        /// </summary>
        /// <param name="p_sPrintText">String text to print</param>
        internal void PrintTextEndAt(string p_sPrintText)
        {
            try
            {
                this.CurrentX = m_dblCurrentX - this.GetTextWidth(p_sPrintText);
                this.PrintText(p_sPrintText);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintTextEndAt.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Set Font 
        /// </summary>
        /// <param name="p_sFontName">Font Name</param>
        /// <param name="p_dblFontSize">Font Size</param>
        internal void SetFont(string p_sFontName, double p_dblFontSize)
        {
            try
            {
                m_objFont = new Font(p_sFontName, (float)p_dblFontSize);
                m_objText.Style.Font = m_objFont;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFont.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Set Font 
        /// </summary>
        /// <param name="p_dblFontSize">Font Size</param>
        internal void SetFont(double p_dblFontSize)
        {
            try
            {
                m_objFont = new Font(m_objFont.Name, (float)p_dblFontSize);
                m_objText.Style.Font = m_objFont;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFont.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Set Font 
        /// </summary>
        /// <param name="p_sFontName">Font Name</param>
        internal void SetFont(string p_sFontName)
        {
            try
            {
                m_objFont = new Font(p_sFontName, m_objFont.Size);
                m_objText.Style.Font = m_objFont;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFont.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Set Font Bold
        /// </summary>
        /// <param name="p_bBold">Bool flag</param>
        internal void SetFontBold(bool p_bBold)
        {
            try
            {
                if (p_bBold)
                    m_objFont = new Font(m_objFont.Name, m_objFont.Size, FontStyle.Bold);
                else
                    m_objFont = new Font(m_objFont.Name, m_objFont.Size);

                m_objText.Style.Font = m_objFont;

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFontBold.Error", m_iClientId), p_objEx);
            }

        }
        /// <summary>
        /// Get text width.
        /// </summary>
        /// <param name="p_sText">String Text</param>
        /// <returns>Width</returns>
        internal double GetTextWidth(string p_sText)
        {
            try
            {
                m_objText.Text = p_sText + "";
                return (m_objText.BoundWidth);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.GetTextWidth.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Get Text Height
        /// </summary>
        /// <param name="p_sText">String Text</param>
        /// <returns>Height</returns>
        internal double GetTextHeight(string p_sText)
        {
            try
            {
                m_objText.Text = p_sText + "";
                return (m_objText.BoundHeight);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.GetTextHeight.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Save the C1PrintDocument in PDF format.
        /// </summary>
        /// <param name="p_sPath">Path</param>
        internal void Save(string p_sPath)
        {
            try
            {
                m_objPrintDoc.ExportToPDF(p_sPath, false);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.Save.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Save the C1PrintDocument in PDF format.
        /// </summary>
        /// <param name="p_sPath">Path</param>
        internal void GetPDFStream(MemoryStream p_objMemoryStream)
        {
            try
            {
                m_objPrintDoc.Save(p_objMemoryStream);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.Save.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Save the C1PrintDocument in PDF format.
        /// </summary>
        /// <param name="p_sPath">Path</param>
        internal void GetPDFStream(string p_sPath, ref MemoryStream p_objMemoryStream)
        {
            FileStream objFileStream = null;
            BinaryReader objBReader = null;
            Byte[] arrRet = null;
            try
            {
                this.Save(p_sPath);
                objFileStream = new FileStream(p_sPath, FileMode.Open);
                objBReader = new BinaryReader(objFileStream);
                arrRet = objBReader.ReadBytes((int)objFileStream.Length);
                p_objMemoryStream = new MemoryStream(arrRet);
                objFileStream.Close();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.Save.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Call the end event.
        /// </summary>
        internal void EndDoc()
        {
            try
            {
                m_objPrintDoc.EndDoc();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.EndDoc.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Print Line
        /// </summary>
        /// <param name="p_dblFromX">From X</param>
        /// <param name="p_dblFromY">From Y</param>
        /// <param name="p_dblToX">To X</param>
        /// <param name="p_dblToY">To Y</param>
        internal void PrintLine(double p_dblFromX, double p_dblFromY, double p_dblToX, double p_dblToY)
        {
            try
            {
                m_objPrintDoc.RenderDirectLine(p_dblFromX, p_dblFromY, p_dblToX, p_dblToY);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintLine.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Start a new page.
        /// </summary>
        internal void NewPage()
        {
            try
            {
                m_objPrintDoc.NewPage();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.NewPage.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Constructs a rectangle with the appropriate co-ordinates
        /// </summary>
        /// <param name="p_dX1"></param>
        /// <param name="p_dY1"></param>
        /// <param name="p_dX2"></param>
        /// <param name="p_dY2"></param>
        /// <param name="p_FillColor"></param>
        internal void FillRect(double p_dX1, double p_dY1, double p_dX2, double p_dY2, Color p_FillColor)
        {
            try
            {
                C1DocStyle objStyle = new C1DocStyle(m_objPrintDoc);
                objStyle.BackColor = p_FillColor;
                m_objPrintDoc.RenderDirectRectangle(p_dX1, p_dY1, p_dX2, p_dY2, objStyle);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintTextNoCr.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Prints an image on the PDF
        /// </summary>
        /// <param name="p_sPath">ShortName of the user logged in</param>
        internal void PrintImage(string p_sShortName)
        {
            Image objImage = null;
            ImageAlignDef objAlignDef = null;

            double dblWidth = 0.0;
            double dblHeight = 0.0;

            string sPath = string.Empty;

            try
            {
                sPath = RMConfigurator.UserDataPath + "\\Signatures\\" + p_sShortName + ".jpg";
                if (File.Exists(sPath))
                {
                    objImage = Image.FromFile(sPath);
                    objAlignDef = new ImageAlignDef(ImageAlignHorzEnum.Left, ImageAlignVertEnum.Top, false, false, true, false, false);

                    // Image height is in pixel. By dividing it from resolution, Get the height in Inch.
                    // covert inch to twips by multiplying 1440. 
                    dblWidth = (objImage.Width / objImage.HorizontalResolution) * 1440;
                    dblHeight = (objImage.Height / objImage.VerticalResolution) * 1440;

                    m_objPrintDoc.RenderDirectImage(CurrentX, CurrentY, objImage, dblWidth, dblHeight, objAlignDef);
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintImage.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objImage = null;
                objAlignDef = null;
            }
        }
    }
}
