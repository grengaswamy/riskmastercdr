﻿using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Data;
using System.Collections;
using System.Drawing;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Settings;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.Reserves;
using Riskmaster.Models;
using Riskmaster.Application.FileStorage;
using Riskmaster.Application.RMUtilities;

namespace Riskmaster.Application.ReserveWorksheet
{
    public class ReserveWorksheetCustomize : ReserveWorksheetBase
    {
        ReserveWorksheetCommon m_CommonInfo = null;
        //Mits 19027 shobhana
        SysSettings objSysSetting = null;
        private bool bPrevModValtToZero = false;
        private int m_iClientId = 0;

        #region Constructor

        public ReserveWorksheetCustomize(ReserveWorksheetCommon p_CommonInfo, int p_iClientId): base(p_CommonInfo, p_iClientId)
        {
            m_CommonInfo = p_CommonInfo;
            m_iClientId = p_iClientId;
        }

        #endregion

        public override void GetReserveWorksheet(ReserveWorksheetRequest worksheetRequest, ref ReserveWorksheetResponse worksheetResponse, UserLogin p_objLogin)
        {
            int iClaimantRowId = 0;
            int iUnitRowId = 0;
            XmlDocument objXMLOut = new XmlDocument();
         //   objXMLOut.Load("instance.xml");
            int iClaimId = 0;
            string sClaimStatus = string.Empty;
            string sFromApproved = string.Empty;
            string sRSWID = string.Empty;
            string sSQL = string.Empty;
            DbReader objReader = null;
            LocalCache objCache = null;
            ReserveWorksheetGeneric objRSWGen;
            string sRswTypeToLaunch = string.Empty;
            string sRSWType = string.Empty;
            int iSubmittedTo = 0;
            int iSubmittedBy = 0;

            try
            {
                if (worksheetRequest.RSWId != 0)
                {
                    sRSWID = worksheetRequest.RSWId.ToString();
                }
                else
                {
                    sRSWID = "";
                }
                //Mits 19027 shobhana
                objSysSetting = new SysSettings(m_CommonInfo.Connectionstring,m_iClientId);
                bPrevModValtToZero = objSysSetting.PrevResModifyzero;

                // sRSWID = objXMLOut.SelectSingleNode("//RSWId").InnerText.Trim();
                //If 'RSWId' is empty or null, then a 'New' Reserve Worksheet is being made
                if (sRSWID != "")
                {
                    sSQL = "SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + sRSWID;
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
                    if (objReader.Read())
                    {
                        objXMLOut.LoadXml(objReader.GetString("RSW_XML"));
                        iSubmittedTo = Riskmaster.Common.Conversion.ConvertStrToInteger(objReader.GetString("SUBMITTED_TO"));
                        iSubmittedBy = Riskmaster.Common.Conversion.ConvertStrToInteger(objReader.GetString("SUBMITTED_BY"));
                        //sRswTypeToLaunch = objReader.GetString("RSWXMLTYPE");
                        //tmalhotra2 mits 27272 start
                        if (!string.IsNullOrEmpty(objReader.GetString("RSWXMLTYPE")))
                        {
                            sRswTypeToLaunch = objReader.GetString("RSWXMLTYPE");
                        }
                        else
                        {
                            sRswTypeToLaunch = "Generic";
                        }
                         if (!(sRswTypeToLaunch == "Customize" && (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Approved" || objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "History")))
                        { 
                            if (!IsRSWAllowed(worksheetRequest.RSWId, sRswTypeToLaunch, Conversion.ConvertStrToInteger(objXMLOut.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                             {
                                 if (sRswTypeToLaunch == "Generic" && (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Approved" || objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "History"))

                               {
                              objRSWGen = new ReserveWorksheetGeneric(m_CommonInfo,m_iClientId);
                              objRSWGen.GetReserveWorksheet(worksheetRequest, ref worksheetResponse, p_objLogin);
                                return;

                            
                            
                               }
                            else

                            throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                             }
                       }
                    
                        //if (!IsRSWAllowed(worksheetRequest.RSWId, sRswTypeToLaunch, Conversion.ConvertStrToInteger(objXMLOut.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                        //{
                        //    throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                        //}
                    }
                    else
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.LoadMainData.RSWNotFound",m_iClientId));
                 
                        if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Rejected")
                        {
                            objXMLOut = MakeRWReadOnly(objXMLOut);
                            #region Checking Authorized approver
                            if (iSubmittedBy != 0)
                            {
                                if (m_CommonInfo.UserId != iSubmittedBy) //If a user other than the one submitted by tries to edit the sheet, then perform the following check
                                {
                                    int iLevel = 1;
                                    iLevel = GetSupervisoryLevel(iSubmittedBy, m_CommonInfo.UserId, iClaimId);
                                    if (iLevel == 0)
                                    {
                                        if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                                            objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText = "NoAuthEdit";
                                        //  return objXMLOut;
                                        worksheetResponse.OutputXmlString = objXMLOut.OuterXml;
                                        return;
                                    }
                                }
                            }
                            #endregion
                        }

                        if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Rejected")
                        {
                            objXMLOut = MakeRWEditable(objXMLOut);
                            //objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText = "Rejected (Edit Mode)";
                            objXMLOut.SelectSingleNode("//control[@name='hdnMode']").InnerText = "Edit";
                            //By Vaibhav for Safeway MITS 13937
                            if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                                objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText = "";

                        }

                        //If the worksheet has to be opened in 'ReadOnly' mode, appropriate function is called and 
                        //controls are rendered as read-only.
                        if (objXMLOut.SelectSingleNode("//control[@name='hdnMode']").InnerText == "ReadOnly")
                        {
                            objXMLOut = MakeRWReadOnly(objXMLOut);
                            //By Vaibhav for Safeway MITS 13939
                            if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                                objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText = "";

                        }

                        if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Pending Approval")
                        {
                            if (m_CommonInfo.UserId != iSubmittedBy)
                            {
                                if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                                    objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText = "";
                            }
                        }
                        else if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Rejected")
                        {
                            if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                                objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText = "";
                        }
                  
                }
                else

                {
                    // Create a new Worksheet.
                    iClaimId = worksheetRequest.ClaimId;

                    // iClaimId = Conversion.ConvertStrToInteger(objXMLOut.SelectSingleNode("//control[@name='hdnClaimId']").InnerText.Trim());
                    if (iClaimId == 0)
                    {
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.LoadMainData.ClaimNotFound",m_iClientId));
                    }
                    else
                    {
                        string appPath = string.Empty;
                        appPath = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
                        objXMLOut.Load(appPath + "App_Data\\RswCustomize.xml");
                       // objXMLOut.Load("instance.xml");
                       sRSWType = "CRW";
                        //  sRSWType = objXMLOut.SelectSingleNode("//RSWType").InnerText.Trim();

                        sFromApproved = objXMLOut.SelectSingleNode("//FromApproved").InnerText.Trim();
                        // Fetch the information from the database for the new worksheet
                        sClaimStatus = GetClaimStatus(iClaimId);
                        objCache = new LocalCache(m_CommonInfo.Connectionstring,base.ClientId);
                        //making SMS Security permission check, whether the current user is 
                        //            // having the permission to update Reserve amount or not
                        GetSecurityIDS(iClaimId, iClaimantRowId, iUnitRowId, 0);

                        ////checking To Create New Worksheet SMS Permission
                        if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_CREATE))
                            throw new RMAppException("You don't have permission to complete requested operation.");

                        switch (sRSWType)
                        {
                          case "CRW":
                            //    //check,wheather the Claim is closed 
                              if (IsClosedClaim(iClaimId))
                                  throw new RMAppException("ReserveWorksheet can not be created for Closed Claim");

                                    objXMLOut.LoadXml(objXMLOut.SelectSingleNode("//CRWXML/form").OuterXml);
                                    // Set the Reserve Worksheet type to CRW
                                    objXMLOut.SelectSingleNode("//control[@name='hdnRSWType']").InnerText = objCache.GetCodeId("CRW", "RSW_TYPE").ToString();
                                    FillNewCRW(ref objXMLOut, iClaimId, worksheetRequest.FromApproved, sRSWType);
                                break;
                           default:
                                    throw new RMAppException(String.Format(Globalization.GetString("ReserveWorksheet.LoadMainData.InvalidStatusRSW",m_iClientId), sClaimStatus));
                        }
                    }
                   
           
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
            ///return objXMLOut;
            worksheetResponse.OutputXmlString = objXMLOut.OuterXml;
        }
    

        public override void SubmitWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
        {
            int iClaimId = 0;
            int iAdjId = 0;
            int iApproverId = 0;
            XmlNode objNode = null;
            DbReader objReaderLOB = null;
            int iLOBCode = 0;
            string p_sCallFor = string.Empty;

            int iClmTypeCode = 0;
            XmlDocument p_objXmlDoc = null;
            bool bUnderLimit = false;
            LocalCache objCache = null;
            string sSubmittedTo = string.Empty;
            string sSupName = string.Empty;
            string sRSWId = string.Empty;
            int iClaimantRowId = 0;
                   int iUnitRowId = 0;
            StringBuilder sbSQL;
            DbConnection objCon = null;
            DbReader objReader = null;
            string sRswTypeToLaunch = string.Empty;
            int iNewStatusCode;
            bool bDaysApprovalLapsed = false;

            ReserveFunds.structReserves[] arrReserves = null;
            int iRSWID = 0;
            int iSubmToID = 0;
            int iSubmByID = 0;
            string sRSWDateTime = "";
            //skhare7 
            ReserveCurrent objReserveCurrent = null;
            DataModelFactory objDataModelFactory = null;
            int iClaimCurrCode = 0;
            try
            {
            p_objXmlDoc = new XmlDocument();
             p_objXmlDoc.LoadXml(p_WorksheetRequest.InputXmlString);
                sbSQL = new StringBuilder();
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);

                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnRSWid']");
                if (objNode != null)
                    iRSWID = Conversion.ConvertStrToInteger(objNode.InnerText);

                string sReason = "";

                objNode = p_objXmlDoc.SelectSingleNode("//AppRejReqCom");
                if (objNode != null)
                    sReason = p_objXmlDoc.SelectSingleNode("//AppRejReqCom").InnerText;

                if (iRSWID <= 0)
                {
                    objNode = p_objXmlDoc.SelectSingleNode("//RswId");
                    if (objNode != null)
                        iRSWID = Conversion.ConvertStrToInteger(objNode.InnerText.Trim());

                    sbSQL.Append("SELECT RSWXMLTYPE,RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRSWID);
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    sbSQL.Remove(0, sbSQL.Length);
                    if (objReader.Read())
                    {
                        p_objXmlDoc.LoadXml(objReader.GetString("RSW_XML"));
                        sRswTypeToLaunch = objReader.GetString("RSWXMLTYPE");

                        //Added by Shobhana
                        if (!IsRSWAllowed(iRSWID, sRswTypeToLaunch,Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                        {
                            throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                        }
                    }
                 

                    else
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.LoadMainData.RSWNotFound",m_iClientId));

                    objReader.Close();
                }

                if (p_sCallFor == "")
                    if (p_objXmlDoc.SelectSingleNode("//control[@name='hdnRSWStatus']") != null)
                        p_sCallFor = p_objXmlDoc.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText;

                //Get Claim ID
                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnClaimId']");
                if (objNode != null)
                    iClaimId = Conversion.ConvertStrToInteger(objNode.InnerText);

                #region Checking Authorized approver
                objCon.Open();
                if (p_sCallFor == "Pending Approval")
                {
                    sbSQL.Append("SELECT SUBMITTED_TO FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRSWID.ToString());
                    sSubmittedTo = Conversion.ConvertObjToStr(objCon.ExecuteScalar(sbSQL.ToString()));
                    sbSQL.Remove(0, sbSQL.Length);
                    iSubmToID = Conversion.ConvertStrToInteger(sSubmittedTo);
                    if (iSubmToID != 0)
                    {
                        if (m_CommonInfo.UserId != iSubmToID) //If a user other than the one submitted to tries to approve the sheet, then perform the following check
                        {
                            int iLevel = 1;
                         //   iLevel = GetSupervisoryLevel(iSubmToID, m_CommonInfo.UserId);
                            iLevel = GetSupervisoryLevel(iSubmToID, m_CommonInfo.UserId, iClaimId);
                            if (iLevel == 0)
                            {
                                if (p_objXmlDoc.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                                    p_objXmlDoc.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText = "NoAuthSubmit";
                              p_WorksheetResponse.OutputXmlString = p_objXmlDoc.OuterXml;
                                                         return ;
                            }
                        }
                    }
                }
                #endregion

                sbSQL.Append("SELECT SUBMITTED_BY FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRSWID.ToString());
                iSubmByID = Conversion.ConvertObjToInt(objCon.ExecuteScalar(sbSQL.ToString()), base.ClientId);
                sbSQL.Remove(0, sbSQL.Length);

                #region Get the Line of Business for the claim

                sbSQL.Append("SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE ,CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimId);
                objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReaderLOB.Read())
                {
                    iLOBCode = objReaderLOB.GetInt32("LINE_OF_BUS_CODE");
                    iClmTypeCode = objReaderLOB.GetInt32("CLAIM_TYPE_CODE");
                    iClaimCurrCode = objReaderLOB.GetInt32("CLAIM_CURR_CODE");
                }
                objReaderLOB.Close();
                sbSQL.Remove(0, sbSQL.Length);

                #endregion

                //Check Reserve Limits Enabled/Disabled
                GetLOBReserveOptions(iClaimId);

                //Get Supervisory Approval Settings
                ReadRSWCheckOptions();

                arrReserves = GetResCategories(iLOBCode, iClmTypeCode);

                string[,] sResFinal = null;
                sResFinal = new string[arrReserves.GetLength(0), 4];

                sResFinal = GetNewReserveAmts(iLOBCode, arrReserves, p_objXmlDoc, iClaimCurrCode);
               
                //aaggarwal29 MITS 27763 -- start 
                // Previously Security check was happening only if m_bREsetLimitFlag is true, which was not correct.
                //If user does not have "UPDATE" permissions, he should not be allowed to perform any updates under any condition.
                GetSecurityIDS(iClaimId, iClaimantRowId, iUnitRowId, 0);
                if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
                {
                    throw new RMAppException("You don't have permission to complete requested operation.");
                }
                //aaggarwal29 MITS 27763 --end

                if (m_bResLimitFlag) // If the Flag is 'true' then check the reserve limits
                {//commented by shobhana
                    //bUnderLimit = UnderUserLimit(p_objXmlDoc, m_CommonInfo.UserId, p_iGroupId, iClaimId, sResFinal, arrReserves);
                    //end shobhana
                    bUnderLimit = UnderUserLimit( m_CommonInfo.UserId, p_iGroupId, iClaimId, sResFinal, arrReserves,iClaimCurrCode);
                    //By Vaibhav on 11/14/08
                    if (bUnderLimit && m_iDaysAppReserves > 0)
                    {
                        sbSQL.Append("SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1");
                        int iTopID = Conversion.ConvertObjToInt(objCon.ExecuteScalar(sbSQL.ToString()), base.ClientId);
                        sbSQL.Remove(0, sbSQL.Length);

                        if (iTopID != m_CommonInfo.UserId)
                        {
                            //Check if Days for Approval has lapsed and set the bUnderLimit flag to false
                            //so that the reserve worksheet goes to the next level supervisor
                            sbSQL.Append("SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRSWID);
                            objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                            sbSQL.Remove(0, sbSQL.Length);
                            if (objReader.Read())
                            {
                                sRSWDateTime = objReader.GetString("DTTM_RCD_LAST_UPD");
                                //By Vaibhav on 11/14/08 : modified  CheckDaysForApprovalLapsed function
                                // By Vaibhav on 12/03/2008 : MITS 13937
                                //commented by shobhana//

                           //     bDaysApprovalLapsed = CheckDaysForApprovalLapsed(sRSWDateTime, m_CommonInfo.UserId, m_CommonInfo.UserId);
                                //end shohbhana
                                bDaysApprovalLapsed = CheckDaysForApprovalLapsed(sRSWDateTime, m_CommonInfo.UserId, m_CommonInfo.UserId, iClaimId);
                                // bDaysApprovalLapsed = CheckDaysForApprovalLapsed(sRSWDateTime, Conversion.ConvertObjToInt(objReader.GetString("SUBMITTED_TO")), p_iUserId);
                            }
                            objReader.Close();
                            if (bDaysApprovalLapsed)
                                bUnderLimit = false;
                        }
                    }
                    //vaibhav code end

                    //aaggarwal29 code commenting start for MITS 27763 
                    //Moved this code out of if clause as security permissions were getting checked only in some scenarios
                   // GetSecurityIDS(iClaimId, iClaimantRowId, iUnitRowId, 0);
                    //if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
                    //{
                    //    //No Security permission is there 
                    //    bUnderLimit = false;
                    //    objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnSMSCheckToUpdate']");
                    //    if (objNode != null)
                    //        objNode.InnerText = "No";
                    //}
                    //else
                    //{
                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnSMSCheckToUpdate']");
                        if (objNode != null)
                            objNode.InnerText = "Yes";
                    //}
                    //aaggarwal29 code commenting end for MITS 27763 
                }
                else
                    bUnderLimit = true;

                if (bUnderLimit)
                {
                    # region Check Reserve Limits Enabled/Disabled: Supervisory Approval not required

                    //Change the worksheet status and save it as Pending Approval. No supervisory approval is required
                    //A Pending worksheet is sent directly for approval as the reserve limit is not set, approve the worksheet
                    if (p_sCallFor == "Pending" || p_sCallFor == "Pending Approval")
                    {
                        // Change the 'Last Modified By' and 'Last Modified Time'
                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdndtLastMod']");
                        if (objNode != null)
                        {
                            objNode.InnerText = DateTime.Now.ToString();
                            objNode.Attributes["dbformat"].InnerText = Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString());
                        }
                       
                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='LastModBy']");
                        if (objNode != null)
                            objNode.InnerText = m_CommonInfo.UserId.ToString();

                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnUnderLimit']");
                        if (objNode != null)
                            objNode.InnerText = "Yes";

                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnSubmittedToID']");
                        if (objNode != null)
                        {
                            objNode.InnerText = m_CommonInfo.UserId.ToString();
                            sSubmittedTo = m_CommonInfo.UserId.ToString();
                        }

                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnRSWStatus']");
                        if (objNode != null)
                        {
                            //If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted
                            #region If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted

                            // Update record in database table 'RSW_WORKSHEETS'
                            iNewStatusCode = objCache.GetCodeId("PA", "RSW_STATUS");
                            objNode.InnerText = "Pending Approval";
                            objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();

                            // Change the date rejected and rejected by fields
                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdndtAppRej']");
                            objNode.InnerText = "";
                            objNode.Attributes["dbformat"].InnerText = "";

                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnAppRejBy']");
                            objNode.InnerText = "";

                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnMode']");
                            if (objNode != null)
                                objNode.InnerText = "ReadOnly";

                            // Update record in database table 'RSW_WORKSHEETS'
                            p_objXmlDoc = SaveReserveWorksheet(p_objXmlDoc);
                            SaveReserveWorksheetHistory(p_objXmlDoc);
                            #endregion

                            p_objXmlDoc = MakeRWReadOnly(p_objXmlDoc);
                            p_WorksheetResponse.OutputXmlString = p_objXmlDoc.OuterXml;
                                            return;
                        }
                    }

                    #endregion
                }
                else
                {
                    # region Supervisory Approval is required
                    //Supervisory approval is required. Change the worksheet status and save it as Pending Approval.

                    //Check the setting whether to use the Supervisor Approval
                    //If "no" Supervisor Approval then user will be dispalyed a message and the reserve worksheet will not save.
                    //If "yes" Supervisor Approval then check other settings to get the supervisor and update it accordingly

                    if (!m_bUseSupAppReserves)
                    {
                        //User will be shown the message:
                        //You do not have permission to set the reserve of this amount.
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.Reserves.PermissionErr",m_iClientId));
                    }
                    else
                    {
                        //Supervisory Approval will be followed as per settings.
                        //Use Current Adjusters supervisory chain
                        if (m_bUseCurrAdjReserves)
                        {
                            #region Use Current Adjusters supervisory chain
//MGaba2:R8:SuperVisory Approval
                            iAdjId = CommonFunctions.GetAdjusterEID(iClaimId, m_CommonInfo.Connectionstring, base.ClientId);
                            //We have a current adjuster who has a RM user id assigned.  
                            //We go after this users limits and work our way up the supervisory chain
                            if (iAdjId > 0)
                            {
                                //To notify immediate adjuster
                                if (m_bNotifySupReserves)
                                {
                                    if (iAdjId != m_CommonInfo.UserId)
                                        if (m_CommonInfo.UserId == iSubmByID)
                                            iApproverId = iAdjId;
                                        else
//MGaba2:R8:SuperVisory Approval
                                            iApproverId = CommonFunctions.GetManagerID(m_CommonInfo.UserId, m_CommonInfo.DSNID, base.ClientId);
                                    else
//MGaba2:R8:SuperVisory Approval
                                        iApproverId = CommonFunctions.GetManagerID(iAdjId, m_CommonInfo.DSNID, base.ClientId);//We might end-up having iApproverId as 0
                                }
                                else //Use Current Adjusters supervisory chain
                                    //commented by shobhana
                                    //iApproverId = GetApprovalID(p_objXmlDoc, iAdjId, iClaimId, m_CommonInfo.UserId, p_iGroupId, sResFinal, arrReserves);
                                    //commented by shobhana
                                    iApproverId = GetApprovalID( iAdjId, iClaimId, m_CommonInfo.UserId, p_iGroupId, sResFinal, arrReserves,iClaimCurrCode);
                            }
                            else //We do not have a current adjuster with a RM user Id Assigned.
                            {
                                //To notify immediate manager when there is no current adjuster
                                if (m_bNotifySupReserves)
//MGaba2:R8:SuperVisory Approval
                                    iApproverId = CommonFunctions.GetManagerID(m_CommonInfo.UserId, m_CommonInfo.DSNID, base.ClientId);//We might end-up having iApproverId as 0
                                else //Check the limits for the user who is logged on and work our way up supervisory chain
//MGaba2:R8:SuperVisory Approval
                                    iApproverId = GetApprovalID(CommonFunctions.GetManagerID(m_CommonInfo.UserId, m_CommonInfo.DSNID, base.ClientId), iClaimId, m_CommonInfo.UserId, p_iGroupId, sResFinal, arrReserves, iClaimCurrCode);
                            }

                            #endregion
                        }
                        else
                        {
                            #region Use Current Users supervisory chain

                            //To notify immediate manager
                            if (m_bNotifySupReserves)
//MGaba2:R8:SuperVisory Approval
                                iApproverId = CommonFunctions.GetManagerID(m_CommonInfo.UserId, m_CommonInfo.DSNID, base.ClientId);//We might end-up having iApproverId as 0
                            else //Check the limits for the user who is logged on and work our way up there supervisory chain 
//MGaba2:R8:SuperVisory Approval
                                iApproverId = GetApprovalID(CommonFunctions.GetManagerID(m_CommonInfo.UserId, m_CommonInfo.DSNID, base.ClientId), iClaimId, m_CommonInfo.UserId, p_iGroupId, sResFinal, arrReserves, iClaimCurrCode);

                            #endregion
                        }
                    }

                    #endregion

                    if (iApproverId == 0)
                    {
                        iApproverId = GetLOBManagerID(iLOBCode, sResFinal);
                    }

                    #region If valid approver then submit the worksheet to that approver

                    if (iApproverId > 0)
                    {
                        if (iApproverId != m_CommonInfo.UserId)
                        {

                            #region Get Approver information

                            sbSQL.Append("SELECT MANAGER_ID, LOGIN_NAME, USER_TABLE.First_Name as FName, USER_TABLE.Last_Name as LName");
                            sbSQL.Append(" FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " + iApproverId);
                            sbSQL.Append(" AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = " + m_CommonInfo.DSNID);
                            objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sbSQL.ToString());
                            sbSQL.Remove(0, sbSQL.Length);

                            if (objReader.Read())
                            {
                                sSupName = objReader.GetString("FName") + ", " + objReader.GetString("LName");
                                sSubmittedTo = objReader.GetString("LOGIN_NAME");
                            }
                            else
                            {
                                sSupName = "";
                                sSubmittedTo = "";
                            }
                            objReader.Close();

                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnSubmittedTo']");
                            if (objNode != null)
                            {
                                objNode.InnerText = sSubmittedTo;
                            }

                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnSubmittedToID']");
                            if (objNode != null)
                            {
                                objNode.InnerText = iApproverId.ToString();
                                sSubmittedTo = iApproverId.ToString();
                            }

                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='SupName']");
                            if (objNode != null)
                            {
                                objNode.InnerText = sSupName;
                            }

                            #endregion

                            // Change the 'Last Modified By' and 'Last Modified Time'
                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdndtLastMod']");
                            if (objNode != null)
                            {
                                objNode.InnerText = DateTime.Now.ToString();
                                objNode.Attributes["dbformat"].InnerText = Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString());
                            }
                        
                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnLastModBy']");
                            if (objNode != null)
                                objNode.InnerText = m_CommonInfo.UserName;

                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnUnderLimit']");

                            if (objNode != null)
                                objNode.InnerText = "No";

                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnRSWStatus']");
                            if (objNode != null)
                            {
                                //If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted
                                #region If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted

                                // Update record in database table 'RSW_WORKSHEETS'
                                iNewStatusCode = objCache.GetCodeId("PA", "RSW_STATUS");
                                objNode.InnerText = "Pending Approval";
                                objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();

                                // Change the date rejected and rejected by fields
                                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdndtAppRej']");
                                objNode.InnerText = "";
                                objNode.Attributes["dbformat"].InnerText = "";

                                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnAppRejBy']");
                                objNode.InnerText = "";

                                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnMode']");
                                if (objNode != null)
                                    objNode.InnerText = "ReadOnly";

                                //rejection comments
                                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnAppRejComm']");
                                if (objNode != null)
                                    objNode.InnerText = sReason;

                                // Update record in database table 'RSW_WORKSHEETS'
                                p_objXmlDoc = SaveReserveWorksheet(p_objXmlDoc);
                                SaveReserveWorksheetHistory(p_objXmlDoc);
                                #endregion

                                p_objXmlDoc = MakeRWReadOnly(p_objXmlDoc);
                            }
                        }
                        else
                        {
                            //Current User has the Authority as LOB Manager or Top Level Manager
                            //Change the 'Last Modified By' and 'Last Modified Time'
                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdndtLastMod']");
                            if (objNode != null)
                            {
                                objNode.InnerText = DateTime.Now.ToString();
                                objNode.Attributes["dbformat"].InnerText = Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString());
                            }

                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnLastModBy']");
                            if (objNode != null)
                                objNode.InnerText = m_CommonInfo.UserName;

                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnUnderLimit']");
                            if (objNode != null)
                                objNode.InnerText = "Yes";

                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnRSWStatus']");
                            if (objNode != null)
                            {
                                //If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted
                                #region If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted

                                // Update record in database table 'RSW_WORKSHEETS'
                                iNewStatusCode = objCache.GetCodeId("PA", "RSW_STATUS");
                                objNode.InnerText = "Pending Approval";
                                objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();

                                // Change the date rejected and rejected by fields
                                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdndtAppRej']");
                                objNode.InnerText = "";
                                objNode.Attributes["dbformat"].InnerText = "";

                                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnAppRejBy']");
                                objNode.InnerText = "";

                                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnMode']");
                                if (objNode != null)
                                    objNode.InnerText = "ReadOnly";

                                //rejection comments
                                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnAppRejComm']");
                                if (objNode != null)
                                    objNode.InnerText = sReason;

                                // Update record in database table 'RSW_WORKSHEETS'
                                p_objXmlDoc = SaveReserveWorksheet(p_objXmlDoc);
                                SaveReserveWorksheetHistory(p_objXmlDoc);
                                #endregion
                                //for the time being shobhana
                             p_objXmlDoc = MakeRWReadOnly(p_objXmlDoc);
                                p_WorksheetResponse.OutputXmlString = p_objXmlDoc.OuterXml;
                                                               return;
                                //                             return p_objXmlDoc;
                            }
                        }
                    }
                    else
                    {
                        #region Get Approver information

                        sbSQL.Append("SELECT MANAGER_ID, LOGIN_NAME, USER_TABLE.First_Name as FName, USER_TABLE.Last_Name as LName");
                        sbSQL.Append(" FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " + iApproverId);
                        sbSQL.Append(" AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = " + m_CommonInfo.DSNID);
                        objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sbSQL.ToString());
                        sbSQL.Remove(0, sbSQL.Length);

                        if (objReader.Read())
                        {
                            sSupName = objReader.GetString("FName") + ", " + objReader.GetString("LName");
                            sSubmittedTo = objReader.GetString("LOGIN_NAME");
                        }
                        else
                        {
                            sSupName = "";
                            sSubmittedTo = "";
                        }
                        objReader.Close();

                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnSubmittedTo']");
                        if (objNode != null)
                        {
                            objNode.InnerText = sSubmittedTo;
                        }

                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnSubmittedToID']");
                        if (objNode != null)
                        {
                            objNode.InnerText = m_CommonInfo.UserId.ToString();
                            sSubmittedTo = m_CommonInfo.UserId.ToString();
                        }

                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='SupName']");
                        if (objNode != null)
                        {
                            objNode.InnerText = sSupName;
                        }

                        #endregion

                        // Change the 'Last Modified By' and 'Last Modified Time'
                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdndtLastMod']");
                        if (objNode != null)
                        {
                            objNode.InnerText = DateTime.Now.ToString();
                            objNode.Attributes["dbformat"].InnerText = Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString());
                        }

                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnLastModBy']");
                        if (objNode != null)
                            objNode.InnerText = m_CommonInfo.UserName;

                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnUnderLimit']");
                        if (objNode != null)
                            objNode.InnerText = "NoSupervisor";

                        objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnRSWStatus']");
                        if (objNode != null)
                        {
                            //If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted
                            #region If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted

                            // Update record in database table 'RSW_WORKSHEETS'
                            iNewStatusCode = objCache.GetCodeId("PA", "RSW_STATUS");
                            objNode.InnerText = "Pending Approval";
                            objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();

                            // Change the date rejected and rejected by fields
                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdndtAppRej']");
                            objNode.InnerText = "";
                            objNode.Attributes["dbformat"].InnerText = "";

                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnAppRejBy']");
                            objNode.InnerText = "";

                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnMode']");
                            if (objNode != null)
                                objNode.InnerText = "ReadOnly";

                            //rejection comments
                            objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnAppRejComm']");
                            if (objNode != null)
                                objNode.InnerText = sReason;

                            // Update record in database table 'RSW_WORKSHEETS'
                            p_objXmlDoc = SaveReserveWorksheet(p_objXmlDoc);
                            SaveReserveWorksheetHistory(p_objXmlDoc);
                            #endregion

                      p_objXmlDoc = MakeRWReadOnly(p_objXmlDoc);
                        }
                    }

                    #endregion
                }
                #region send email/diary
                if (iApproverId > 0 && (!m_bDisableDiaryNotify || !m_bDisableEmailNotify))//rsushilaggar 22-Mar-2010 Mits : 19624
                {
                    // Send an email to the person to whom the worksheet is submitted.
                    string sToEmail = string.Empty;
                    string sFromEmail = string.Empty;
                    string sSubjectEmail = string.Empty;
                  
                    string sToUser = string.Empty;
                    string sFromUser = string.Empty;
                   
                    string sRSWType = string.Empty;
                    string sClaimNumber = string.Empty;
                    string sPCName = string.Empty;
                    string sSubmittedBy = string.Empty;
                    string strReason = string.Empty;

                    string sCurAdjEmail = string.Empty;
                    string sCurAdjUser = string.Empty;
                    string sCurAdjLogin = string.Empty;
                    int iAdjID = 0;
                    DbConnection objSecDBConn = null;

                    objSecDBConn = DbFactory.GetDbConnection(m_CommonInfo.SecurityConnectionstring);
                    objSecDBConn.Open();

                    sbSQL.Append("SELECT LOGIN_NAME");
                    sbSQL.Append(" FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " + Conversion.ConvertStrToInteger(iApproverId.ToString()));
                    sbSQL.Append(" AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = " + m_CommonInfo.DSNID);
                    sSubmittedTo = Conversion.ConvertObjToStr(objSecDBConn.ExecuteScalar(sbSQL.ToString()));
                    sbSQL.Remove(0, sbSQL.Length);

                    objNode = p_objXmlDoc.SelectSingleNode("//control[@name='WorksheetType']");
                    if (objNode != null)
                        sRSWType = objNode.Attributes["title"].InnerText;

                    objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnClaimNum']");
                    sClaimNumber = objNode.InnerText;

                    objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnpcName']");
                    sPCName = objNode.InnerText;

                    objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnLastModBy']");
                    sSubmittedBy = objNode.InnerText;

                    objNode = p_objXmlDoc.SelectSingleNode("//control[@name='hdnAppRejComm']");
                    if (objNode != null)
                    {  if (objNode.InnerText != "")
                           strReason=objNode.InnerText;
                          
                    }
                    // Send a diary to the person to whom the worksheet is submitted
                    string sRegarding = sClaimNumber + " * " + sPCName;
                   //skhare7 Fn Transfferd to common fns  R8 supervisory approval
                    CommonFunctions.GetUserDetails(sSubmittedTo, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sToEmail, ref sToUser,m_iClientId);
                    CommonFunctions.GetUserDetails(m_CommonInfo.UserName, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sFromEmail, ref sFromUser,m_iClientId);
                    //skhare7 Fn Transfferd to common fns  R8 supervisory approval End
                    string sBodyEmail = string.Empty;
                  //skhare7 changed for R8
                  //  EmailFormat objEMail = null;
                  //  objEMail = new EmailFormat(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword);
                    objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);
                    objReserveCurrent = (ReserveCurrent)objDataModelFactory.GetDataModelObject("ReserveCurrent", true);
                    sBodyEmail = objReserveCurrent.GetEmailFormat("RSWSheet_SUB", iClaimId);
               
                    if (sPCName != "" && sPCName != null)
                    {
                        sSubjectEmail = sRSWType + " Approval Request for Claim " + sClaimNumber + " - " + sPCName;
                   
                    }
                    else
                    {

                        sSubjectEmail = sRSWType + " Approval Request for Claim " + sClaimNumber;
                   
                    }
                    sBodyEmail = sBodyEmail.Replace("{RSW_TYPE}", sRSWType)
                                           .Replace("{REASON}", strReason)
                                           .Replace("{USER_NAME}", sPCName)
                                           .Replace("{SUBMITTED_BY}", sFromUser)
                                           .Replace("{SUBMITTED_TO}", sToUser);
                       

                    if (sSubmittedTo != m_CommonInfo.UserName)
                    {
                        //The appropriate user should be getting an email and a diary
                        GenerateDiary(sSubjectEmail, sBodyEmail, 1, sSubmittedTo, m_CommonInfo.UserName, sRegarding, iClaimId);
                        if (sToEmail != "" && sFromEmail != "")
                            CommonFunctions.SendEmail(sToEmail, sFromEmail, sSubjectEmail, sBodyEmail, base.ClientId);
                        //skhare7 Fn Transfferd to common fns  R8 supervisory approval
                    }

//MGaba2:R8:SuperVisory Approval

                    iAdjID = CommonFunctions.GetAdjusterEID(iClaimId, m_CommonInfo.Connectionstring, base.ClientId);

                    if ((iAdjID > 0) && (iApproverId != iAdjID))
                    {
                        sbSQL.Append("SELECT LOGIN_NAME");
                        sbSQL.Append(" FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " + Conversion.ConvertStrToInteger(iAdjID.ToString()));
                        sbSQL.Append(" AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = " + m_CommonInfo.DSNID);
                        sCurAdjLogin = Conversion.ConvertObjToStr(objSecDBConn.ExecuteScalar(sbSQL.ToString()));
                        //skhare7 Fn Transfferd to common fns  R8 supervisory approval
                        CommonFunctions.GetUserDetails(sCurAdjLogin, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sCurAdjEmail, ref sCurAdjUser,m_iClientId);
                        //skhare7 Fn Transfferd to common fns  R8 supervisory approval end
                        if (sPCName != "" && sPCName != null)
                        {
                            sSubjectEmail = sRSWType + " Approval Request for Claim " + sClaimNumber + " - " + sPCName;
                           
                        }
                        else
                        {

                            sSubjectEmail = sRSWType + " Approval Request for Claim " + sClaimNumber;
                            
                        }

                        sBodyEmail = sBodyEmail.Replace("{RSW_TYPE}", sRSWType)
                                               .Replace("{USER_NAME}", sPCName)
                                               .Replace("{SUBMITTED_BY}", sFromUser)
                                               .Replace("{SUBMITTED_TO}", sToUser);
       
                        //shobhana for MITS 
//                        

                        GenerateDiary(sSubjectEmail, sBodyEmail, 2, sCurAdjLogin, m_CommonInfo.UserName, sRegarding, iClaimId);
                        if (sToEmail != "" && sFromEmail != "")
                            //skhare7 Fn Transfferd to common fns  R8 supervisory approval
                            CommonFunctions.SendEmail(sCurAdjEmail, sFromEmail, sSubjectEmail, sBodyEmail, base.ClientId);
                        //skhare7 Fn Transfferd to common fns  R8 supervisory approval End
                    }
                }
                #endregion
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException
                    (Globalization.GetString("ReserveWorksheet.SubmitWorksheet.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                objNode = null;
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                if (objReaderLOB != null)
                {
                    objReaderLOB.Dispose();
                    objReaderLOB = null;
                }
                if (objReserveCurrent != null)
                {
                    objReserveCurrent.Dispose();
                   
                }
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                   
                }
                
                
            }
           // return p_objXmlDoc;
        
         p_WorksheetResponse.OutputXmlString = p_objXmlDoc.OuterXml;
            return;
        }

       
        ///<summary>
        /// This function saves the Reserve Worksheet: a New as well as an existing worksheet
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        /// <returns>XML to be rendered on screen</returns>
        /// //commneted by shobhana 
        public override void SaveData(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse)
        {
           XmlNode objNode = null;
           int iRSWId = 0;
           int iStatusCode = 0;
           string sSQL = string.Empty;
           LocalCache objCache = null;
           string sShortCode = string.Empty;
           string sDesc = string.Empty;
           DbConnection objCon = null;
           DbReader objReader = null;

           long lUserMId = 0;
           string sSubmittedTo = string.Empty;
           string sSupName = string.Empty;
                
           string sEditGrid = string.Empty; // Grid Names which can only be edited
           string sAddGrid = string.Empty; // Grid Names in which new nodes can be added.
           XmlDocument p_objXMLIn = new XmlDocument();
           p_objXMLIn.LoadXml(p_WorksheetRequest.InputXmlString);
           try
           {
             
               objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
               objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);

               // Change the 'Last Modified By' and 'Last Modified Time'
               objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdndtLastMod']");
               if (objNode != null)
               {
                   objNode.InnerText = DateTime.Now.ToString();
                   objNode.Attributes["dbformat"].InnerText = Conversion.GetDateTime(DateTime.Now.ToString());
               }
               //added by shobhana 19027
               objSysSetting = new SysSettings(m_CommonInfo.Connectionstring,m_iClientId);
               bPrevModValtToZero = objSysSetting.PrevResModifyzero;
               objNode = (XmlElement)p_objXMLIn.SelectSingleNode("//control[@name='hdnPrevModValtoZero']");
               if (objNode != null)
                   objNode.InnerText = bPrevModValtToZero.ToString();
               objNode = (XmlElement)p_objXMLIn.SelectSingleNode("//control[@name='hdnLastModBy']");
               if (!IsRSWAllowed(p_WorksheetRequest.RSWId, p_WorksheetRequest.RSWType, Conversion.ConvertStrToInteger(p_objXMLIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
               {
                   throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
               }
               if (objNode != null)
                   objNode.InnerText = m_CommonInfo.UserName;

               objNode = (XmlElement)p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWXmlType']");
               if (objNode != null)
                   objNode.InnerText = p_WorksheetRequest.RSWType;

               #region To Get The Reserve Worksheet Supervisor

               sSQL = "SELECT MANAGER_ID, LOGIN_NAME FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                       + m_CommonInfo.UserId
                       + " AND USER_TABLE.MANAGER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                       + m_CommonInfo.DSNID;
               objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
               if (objReader.Read())
               {
                   lUserMId = objReader.GetInt32("MANAGER_ID");
                   sSubmittedTo = objReader.GetString("LOGIN_NAME");
               }
               else
               {
                   lUserMId = m_CommonInfo.UserId;
                   sSubmittedTo = "";
               }
               objReader.Close();

               objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSubmittedTo']");
               if (objNode != null)
               {
                   objNode.InnerText = sSubmittedTo;
               }

               objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSubmittedToID']");
               if (objNode != null)
               {
                   objNode.InnerText = lUserMId.ToString();
               }

               if (lUserMId != 0)
               {
                   sSQL = "SELECT MANAGER_ID, LOGIN_NAME, USER_TABLE.First_Name as FName, USER_TABLE.Last_Name as LName FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                           + lUserMId
                           + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                           + m_CommonInfo.DSNID;
                   objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                   if (objReader.Read())
                   {
                       sSupName = objReader.GetString("FName") + ", " + objReader.GetString("LName");
                   }
                   else
                   {
                       sSupName = "";
                   }
                   objReader.Close();
               }

               objNode = p_objXMLIn.SelectSingleNode("//control[@name='SupName']");
               if (objNode != null)
               {
                   objNode.InnerText = sSupName;
               }

               #endregion

               iRSWId = Conversion.ConvertStrToInteger(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

               if (iRSWId == -1) // New worksheet
               {
                   #region New Reserve Worksheet

                   iStatusCode = objCache.GetCodeId("PE", "RSW_STATUS");
                   // Change 'Request status' from New to Pending.
                   objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                   if (objNode != null)
                   {
                       objNode.InnerText = "Pending";
                       objNode.Attributes["codeid"].InnerText = iStatusCode.ToString();
                   }

                   objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnMode']");
                   if (objNode != null)
                       objNode.InnerText = "Editable";
                   //  lClaimId = Riskmaster.Common.Conversion.ConvertStrToLong(objXmlIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText);
                       // lClaimantRowId = Riskmaster.Common.Conversion.ConvertStrToLong(objXmlIn.SelectSingleNode("//control[@name='hdnClaimantRowId']").InnerText);
                      //s   lUnitRowId = Riskmaster.Common.Conversion.ConvertStrToLong(objXmlIn.SelectSingleNode("//control[@name='hdnUnitRowId']").InnerText);


                   //check if there is any existing reserve worksheet with status as Pending for this claim, then don't save
                   if (CheckPendingRSW(Riskmaster.Common.Conversion.ConvertStrToLong(p_objXMLIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText), "PE"))
                       throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveData.PendingRSWFound",m_iClientId));
                   else
                       p_objXMLIn = SaveReserveWorksheet(p_objXMLIn);

                   #endregion
               }
               else // old worksheet
               {
                   objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                   if (objNode != null)
                   {
                       if (objNode.InnerText.IndexOf("Rejected") != -1) // If a 'rejected worksheet is being saved.
                       {
                           #region If a rejected worksheet is being saved

                           int iNewStatusCode = objCache.GetCodeId("PE", "RSW_STATUS");
                           iStatusCode = Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].InnerText);

                           objNode.InnerText = "Pending";
                           objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();

                           // Change the date rejected and rejected by fields
                           objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdndtAppRej']");
                           objNode.InnerText = "";
                           objNode.Attributes["dbformat"].InnerText = "";

                           objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejBy']");
                           objNode.InnerText = "";

                           objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnMode']");
                           if (objNode != null)
                               objNode.InnerText = "Editable";

                           // Update record in database table 'RSW_WORKSHEETS'
                           p_objXMLIn = SaveReserveWorksheet(p_objXMLIn);
                           SaveReserveWorksheetHistory(p_objXMLIn);

                           #endregion
                       }
                       else // If an existing (non-rejected) worksheet is being saved
                       {
                           #region If an existing (non-rejected) worksheet is being saved

                           objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnMode']");
                           if (objNode != null)
                               objNode.InnerText = "Editable";

                           // Update record in database table 'RSW_WORKSHEETS'
                           p_objXMLIn = SaveReserveWorksheet(p_objXMLIn);
                           SaveReserveWorksheetHistory(p_objXMLIn);

                           #endregion
                       }
                   }
               }
           }
           catch (Exception ex)
           {
               throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveData.Err", m_iClientId), ex);
           }
           finally
           {
               if (objCache != null)
               {
                   objCache.Dispose();
                   objCache = null;
               }
               if (objCon != null)
               {
                   objCon.Close();
                   objCon = null;
               }
               if (objReader != null)
               {
                   objReader.Dispose();
                   objReader = null;
               }
           }

           p_WorksheetResponse.OutputXmlString = p_objXMLIn.OuterXml;
           return;
        }


        ///<summary>
        /// This function saves the Reserve Worksheet: a New as well as an existing worksheet
        /// It is similar to SaveData in impelmentation except that it performs additional security check 
        /// for UPDATE permissions.
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        /// <returns>XML to be rendered on screen</returns>
        /// added by aaggarwal29 -- for MITS 27763
        public override void SaveDataWithUserLogin(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, UserLogin pUserLogin)
        {
            XmlNode objNode = null;
            int iRSWId = 0;
            int iStatusCode = 0;
            int iClaimId = 0;
            int iClaimantRowId = 0;
            int iUnitRowId = 0;
            string sSQL = string.Empty;
            LocalCache objCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            DbConnection objCon = null;
            DbReader objReader = null;

            long lUserMId = 0;
            string sSubmittedTo = string.Empty;
            string sSupName = string.Empty;

            string sEditGrid = string.Empty; // Grid Names which can only be edited
            string sAddGrid = string.Empty; // Grid Names in which new nodes can be added.
            XmlDocument p_objXMLIn = new XmlDocument();
         
            p_objXMLIn.LoadXml(p_WorksheetRequest.InputXmlString);
            try
            {

                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);

                // Change the 'Last Modified By' and 'Last Modified Time'
                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdndtLastMod']");
                if (objNode != null)
                {
                    objNode.InnerText = DateTime.Now.ToString();
                    objNode.Attributes["dbformat"].InnerText = Conversion.GetDateTime(DateTime.Now.ToString());
                }
                
                //added by shobhana 19027
                objSysSetting = new SysSettings(m_CommonInfo.Connectionstring,m_iClientId);
                bPrevModValtToZero = objSysSetting.PrevResModifyzero;
                objNode = (XmlElement)p_objXMLIn.SelectSingleNode("//control[@name='hdnPrevModValtoZero']");
                if (objNode != null)
                    objNode.InnerText = bPrevModValtToZero.ToString();
                objNode = (XmlElement)p_objXMLIn.SelectSingleNode("//control[@name='hdnLastModBy']");
                if (!IsRSWAllowed(p_WorksheetRequest.RSWId, p_WorksheetRequest.RSWType, Conversion.ConvertStrToInteger(p_objXMLIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                {
                    throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                }
                if (objNode != null)
                    objNode.InnerText = m_CommonInfo.UserName;

                objNode = (XmlElement)p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWXmlType']");
                if (objNode != null)
                    objNode.InnerText = p_WorksheetRequest.RSWType;

                #region To Get The Reserve Worksheet Supervisor

                sSQL = "SELECT MANAGER_ID, LOGIN_NAME FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                        + m_CommonInfo.UserId
                        + " AND USER_TABLE.MANAGER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                        + m_CommonInfo.DSNID;
                objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                if (objReader.Read())
                {
                    lUserMId = objReader.GetInt32("MANAGER_ID");
                    sSubmittedTo = objReader.GetString("LOGIN_NAME");
                }
                else
                {
                    lUserMId = m_CommonInfo.UserId;
                    sSubmittedTo = "";
                }
                objReader.Close();

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSubmittedTo']");
                if (objNode != null)
                {
                    objNode.InnerText = sSubmittedTo;
                }

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSubmittedToID']");
                if (objNode != null)
                {
                    objNode.InnerText = lUserMId.ToString();
                }

                if (lUserMId != 0)
                {
                    sSQL = "SELECT MANAGER_ID, LOGIN_NAME, USER_TABLE.First_Name as FName, USER_TABLE.Last_Name as LName FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                            + lUserMId
                            + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                            + m_CommonInfo.DSNID;
                    objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                    if (objReader.Read())
                    {
                        sSupName = objReader.GetString("FName") + ", " + objReader.GetString("LName");
                    }
                    else
                    {
                        sSupName = "";
                    }
                    objReader.Close();
                }

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='SupName']");
                if (objNode != null)
                {
                    objNode.InnerText = sSupName;
                }

                #endregion

                iRSWId = Conversion.ConvertStrToInteger(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);
                //Get Claim ID
                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnClaimId']");
                if (objNode != null)
                    iClaimId = Conversion.ConvertStrToInteger(objNode.InnerText);

                //Get ClaimantRowId
                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnClaimantRowId']");
                if (objNode != null)
                    iClaimantRowId = Conversion.ConvertStrToInteger(objNode.InnerText);

                //Get UnitRowId
                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnUnitRowId']");
                if (objNode != null)
                    iUnitRowId = Conversion.ConvertStrToInteger(objNode.InnerText);
                if (iRSWId == -1) // New worksheet
                {
                    #region New Reserve Worksheet

                    iStatusCode = objCache.GetCodeId("PE", "RSW_STATUS");
                    // Change 'Request status' from New to Pending.
                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    if (objNode != null)
                    {
                        objNode.InnerText = "Pending";
                        objNode.Attributes["codeid"].InnerText = iStatusCode.ToString();
                    }

                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnMode']");
                    if (objNode != null)
                        objNode.InnerText = "Editable";
                    //  lClaimId = Riskmaster.Common.Conversion.ConvertStrToLong(objXmlIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText);
                    // lClaimantRowId = Riskmaster.Common.Conversion.ConvertStrToLong(objXmlIn.SelectSingleNode("//control[@name='hdnClaimantRowId']").InnerText);
                    //s   lUnitRowId = Riskmaster.Common.Conversion.ConvertStrToLong(objXmlIn.SelectSingleNode("//control[@name='hdnUnitRowId']").InnerText);


                    //check if there is any existing reserve worksheet with status as Pending for this claim, then don't save
                    if (CheckPendingRSW(Riskmaster.Common.Conversion.ConvertStrToLong(p_objXMLIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText), "PE"))
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveData.PendingRSWFound",m_iClientId));
                    else
                        p_objXMLIn = SaveReserveWorksheet(p_objXMLIn);

                    #endregion
                }
                else // old worksheet
                {
                     GetSecurityIDS(iClaimId, iClaimantRowId, iUnitRowId, 0);
                    if (!pUserLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
                    {
                        throw new RMAppException("You don't have permission to complete requested operation.");
                    }
                    
                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    if (objNode != null)
                    {
                        if (objNode.InnerText.IndexOf("Rejected") != -1) // If a 'rejected worksheet is being saved.
                        {
                            #region If a rejected worksheet is being saved

                            int iNewStatusCode = objCache.GetCodeId("PE", "RSW_STATUS");
                            iStatusCode = Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].InnerText);

                            objNode.InnerText = "Pending";
                            objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();

                            // Change the date rejected and rejected by fields
                            objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdndtAppRej']");
                            objNode.InnerText = "";
                            objNode.Attributes["dbformat"].InnerText = "";

                            objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejBy']");
                            objNode.InnerText = "";

                            objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnMode']");
                            if (objNode != null)
                                objNode.InnerText = "Editable";

                            // Update record in database table 'RSW_WORKSHEETS'
                            p_objXMLIn = SaveReserveWorksheet(p_objXMLIn);
                            SaveReserveWorksheetHistory(p_objXMLIn);

                            #endregion
                        }
                        else // If an existing (non-rejected) worksheet is being saved
                        {
                            #region If an existing (non-rejected) worksheet is being saved

                            objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnMode']");
                            if (objNode != null)
                                objNode.InnerText = "Editable";

                            // Update record in database table 'RSW_WORKSHEETS'
                            p_objXMLIn = SaveReserveWorksheet(p_objXMLIn);
                            SaveReserveWorksheetHistory(p_objXMLIn);

                            #endregion
                        }
                    }
                }
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveData.Err", m_iClientId), ex);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }

            p_WorksheetResponse.OutputXmlString = p_objXMLIn.OuterXml;
            return;
        }

        //commented by shobhana 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        /// <param name="p_objXMLOut"></param>
        public override void DeleteWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse)
        {
            LocalCache objCache = null;
            StringBuilder sbSQL = null;
            int iStatusCode = 0;
            DbConnection objConn = null;
            string sReason = "";
            int iRSWHistId = 0;
            int iApproverID = 0;
            int iRSWID = 0;
            try
            {
                iRSWID = p_WorksheetRequest.RSWId;
                if (iRSWID == 0)
                {
                    throw new RMAppException("ReserveWorksheetId to be deleted is not found");
                }

                sbSQL = new StringBuilder();
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                iStatusCode = objCache.GetCodeId("DE", "RSW_STATUS");
                objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objConn.Open();

                // Update the db table RSW_WORKSHEETS to set the status equal to deleted
                sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_STATUS_CODE = " + iStatusCode.ToString());
                sbSQL.Append(", UPDATED_BY_USER = " + Utilities.FormatSqlFieldValue(m_CommonInfo.UserName));
                sbSQL.Append(", DTTM_RCD_LAST_UPD = '" + Conversion.GetDateTime(DateTime.Now.ToString()));
                sbSQL.Append("' WHERE RSW_ROW_ID = " + iRSWID.ToString());
                objConn.ExecuteNonQuery(sbSQL.ToString());
                sbSQL.Remove(0, sbSQL.Length);

                sbSQL.Append("SELECT APPROVER_ID FROM RSV_APPROVAL_HIST WHERE RSW_ROW_ID = " + iRSWID);
                sbSQL.Append(" AND RSW_HIST_ROW_ID IN (SELECT MAX(RSW_HIST_ROW_ID) FROM RSV_APPROVAL_HIST");
                sbSQL.Append(" WHERE RSW_ROW_ID = " + iRSWID + ")");
                iApproverID = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sbSQL.ToString()), base.ClientId);
                sbSQL.Remove(0, sbSQL.Length);


                #region Reserve Approval History

                iRSWHistId = Utilities.GetNextUID(m_CommonInfo.Connectionstring, "RSV_APPROVAL_HIST", base.ClientId);
                // Insert into database table 'RSV_APPROVAL_HIST' new record
                sbSQL.Append("INSERT INTO RSV_APPROVAL_HIST (RSW_HIST_ROW_ID, RSW_ROW_ID, RSW_STATUS_CODE, REASON, APPROVER_ID, ");
                sbSQL.Append("CHANGED_BY_USER, DTTM_APPROVAL_CHGD) VALUES (");
                sbSQL.Append(iRSWHistId.ToString() + ", ");
                sbSQL.Append(iRSWID.ToString() + ", ");
                sbSQL.Append(iStatusCode.ToString() + ", ");
                sbSQL.Append(Utilities.FormatSqlFieldValue(sReason) + ", ");
                sbSQL.Append(iApproverID.ToString() + ", ");
                sbSQL.Append(Utilities.FormatSqlFieldValue(m_CommonInfo.UserName) + ", '");
                sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "')");

                objConn.ExecuteNonQuery(sbSQL.ToString());
                sbSQL.Remove(0, sbSQL.Length);
                #endregion
            
          
                 p_WorksheetResponse.Status = true;
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.DeleteWorksheet.Err", m_iClientId), ex);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn = null;
                }
            }
        }

        /// <summary>
        /// MGaba2:r6:Functioning of Approve Button:Start
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        /// <param name="p_objXMLOut"></param>
        public override void ApproveReserveWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
        {
            XmlDocument objXMLOut = new XmlDocument();
            int iRswId = 0;
            int iClaimId = 0;
            int iClaimantEID = 0;
            int iUnitID = 0;
            int iReserveTypeCode = 0;
            XmlDocument objXmlInForPrint = null;
            XmlDocument objXmlOutForPrint = null;
            XmlElement objTempElement = null;
            string sReservesWithinLimit = "No";
            int iAttachRSW = 0;
            int iReserveTrackingLevel = 0;
            string sTableNameForAttach = "CLAIM";
            int iRecordIdForAttach = 0;
            int iClaimantRowId = 0;
            int iUnitRowId = 0;
            string sRswTypeToLaunch = string.Empty;
            string sRSWStatus = string.Empty;
    
            string sDateEntered = "";
            string sAmt = "";
            string sClaimNum = "";
            double dAmount = 0;
            int iStatusCodeID = 0;
            int iLOBCode = 0;
            int iClmTypeCode = 0;
            string sReason = "";
            string sSQL = "";
            string sRswStatus = "";

            ReserveCurrent objFunds = null; // mcapps2 Changed from ReserveFunds to ReserveCurrent for Reserve Current data model object changes.
            LocalCache objCache = null;
            DbConnection objCon = null;
            XmlNode objNod = null;
            DbReader objRdr = null;

            string sSubmittedTo = string.Empty;
            int iSubmToID = 0;
            ReserveWorksheetRequest objRSWReqForPrint = null;
             ReserveWorksheetResponse objRSWResForPrint = null;

            string sLastModOn = string.Empty;
            string sSubmittedBy = string.Empty;
            DataModelFactory objDataModelFactory = null;
            Claim objClaim = null;

            ReserveFunds.structReserves[] arrReserves = null;
            int iCnt;
            StringBuilder sbSQL;
            string sRSWID = "";

            bool bResAdjusted = false;
            bool bUnderLimit = false;
       
            //Priya Aggarwal: Document Imaging for Reserve Worksheet - start
            DbTransaction objDbTransaction = null;
            DbCommand cmd = null;
            string fileName = string.Empty;
            int documentId = 0;
            //Priya Aggarwal: Document Imaging for Reserve Worksheet - end

            long[,] larrayResCurrent = null;
            long[,] larrayResHistory = null;
            int iClaimCurrCode = 0;
            //Aman MITS 27915
            double dExhClaimRateToBase = 0.0;
            SysSettings objSysSetting = null;
            try
            {
                sbSQL = new StringBuilder();
             //   objCache = new LocalCache(m_sConStr);
                objXMLOut = new XmlDocument();
                        objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);

                        objXMLOut = new XmlDocument();
                        objXMLOut.LoadXml(p_WorksheetRequest.InputXmlString);


                        objNod = objXMLOut.SelectSingleNode("//RswId");
                if (objNod != null)
                    sRSWID = objNod.InnerText.Trim();


                if (sRSWID == "")
                {
                    objNod = objXMLOut.SelectSingleNode("//control[@name='hdnRSWid']");
                    if (objNod != null)
                        sRSWID = objNod.InnerText;
                }
                objNod = objXMLOut.SelectSingleNode("//control[@name='hdnClaimId']");
                if (objNod != null)
                    iClaimId = Conversion.ConvertObjToInt(objNod.InnerText, base.ClientId);

                GetSecurityIDS(iClaimId, iClaimantEID, iUnitID, 0);

                                //checking To Update/Approve Worksheet SMS Permission
                                if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
                                    throw new RMAppException("You don't have permission to complete requested operation.");
                                

                                //Check if Claim is closed,then Worksheed cann't be Saved
                                if (IsClosedClaim(iClaimId))
                                    throw new RMAppException("ReserveWorksheet can not be Approved for Closed Claim.");
                                #region Checking for Status and then accordingly submitting it
                               
                                //In case status is other than PA,worksheet will be submitted first
                                objNod = objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']");
                                if (objNod != null)
                                {
                                    sRSWStatus = objNod.InnerText.Trim();
                                }
                                //It will go for Submit first in case status of reserve worksheet is Pending or Rejected
                                //And after submit,if reserves set are in limits then only it will go for approve

                                if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText != "Pending Approval")
                                {
                                    SubmitWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                                    objXMLOut.LoadXml(p_WorksheetResponse.OutputXmlString);

                                    if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                                    {
                                        sReservesWithinLimit = objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText;
                                    }

                                    //added by Nitin for Mits 18882
                                    objNod = objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']");
                                    if (objNod != null)
                                    {
                                        sRSWStatus = objNod.InnerText.Trim();
                                    }

                                    if (sRSWStatus == "Pending Approval" && sReservesWithinLimit == "No")
                                    {
                                        p_WorksheetResponse.OutputXmlString = objXMLOut.OuterXml;
                                        return;
                                    }


                                    objXMLOut = null;
                                    objXMLOut = new XmlDocument();
                                }

                                //If reserve worksheet is in pending approval status
                                //or if it is in pending/rejected and reserve changes satisfies the limits
                                //then it will go for approval

                                if (sRSWStatus != "Pending Approval" && sReservesWithinLimit == "No")
                                {
                                    //added by Nitin for Mits 18882
                                    p_WorksheetResponse.OutputXmlString = objXMLOut.OuterXml;
                                    return;
                                }
                                #endregion
                if (sRSWID != "")
                {
                    sbSQL.Append("SELECT RSWXMLTYPE, RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + sRSWID);
                    objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    sbSQL.Remove(0, sbSQL.Length);
                    if (objRdr.Read())
                    {
                        objXMLOut.LoadXml(objRdr.GetString("RSW_XML"));
                        sRswTypeToLaunch = objRdr.GetString("RSWXMLTYPE");
                        if (!IsRSWAllowed(Convert.ToInt32(sRSWID), sRswTypeToLaunch, Conversion.ConvertStrToInteger(objXMLOut.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                        {
                            throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                        }
                      
                    }
                    else
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.LoadMainData.RSWNotFound",m_iClientId));

                    objRdr.Close();
                }
                else
                    throw new RMAppException(Globalization.GetString("ReserveWorksheet.LoadMainData.RSWNotFound",m_iClientId));


                # region Get the Line of Business for the claim

                //Get the Line of Business for the claim

                objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);
                objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(iClaimId);

                iClaimantEID = objClaim.PrimaryClaimant.ClaimantEid;
                iClmTypeCode = objClaim.ClaimTypeCode;
                iLOBCode = objClaim.LineOfBusCode;
                iClaimCurrCode = objClaim.CurrencyType;

                #endregion

                #region Submiting for checking limits
                //Check Reserve Limits Enabled/Disabled
                GetLOBReserveOptions(iClaimId);

                //Get Supervisory Approval Settings
                ReadRSWCheckOptions();

                string[,] sResFinal = null;

                //According to shobhana here array of reserves should be taken out from XML not database
                arrReserves = GetResCategories(iLOBCode, iClmTypeCode);
                //end
                foreach (XmlNode objGpNode in objXMLOut.SelectNodes("//group"))
                {
                    string sResTab = "";
                    string sGroup = "";
                    int iCount = 0;
                    bool bFindNode = false;
                    string sLOB = "";
                    //string sCtrlMName = "";
                    //string sGPName = "";

                    sLOB = objCache.GetShortCode(objClaim.LineOfBusCode);

                    sGroup = objGpNode.Attributes["name"].InnerText;
                    for (iCount = 0; iCount < arrReserves.GetLength(0); iCount++)
                    {
                        string tempstr = arrReserves[iCount].sReserveDesc.Trim();
                        if (tempstr.IndexOf(" ") > 0)
                            tempstr = tempstr.Replace(" ", "");

                        sResTab = tempstr + sLOB;

                        if (sGroup == sResTab)
                            bFindNode = true;

                     
                    }
                    //if (!bFindNode && objGpNode.Attributes["name"].InnerText.EndsWith("ClaimInfo") == false)
                    //{
                    //    XmlNode objdelNode;
                    //    objdelNode = p_objXML.SelectSingleNode("//form");
                    //    objdelNode.RemoveChild(objGpNode);
                    //}
                }

                sResFinal = new string[arrReserves.GetLength(0), 4];



                sResFinal = GetNewReserveAmts(iLOBCode, arrReserves, objXMLOut, iClaimCurrCode);

                if (m_bResLimitFlag) //If the Flag is 'true' then check the reserve limits
                {
                    bUnderLimit = UnderUserLimit(m_CommonInfo.UserId, p_iGroupId, iClaimId, sResFinal, arrReserves,iClaimCurrCode);
                        //UnderUserLimit(objXMLOut, p_iUserId, p_iGroupId, iClaimId, sResFinal, arrReserves);

                    if (bUnderLimit && m_iDaysAppReserves > 0)
                    {
                        int iTopID = 0;
                        sbSQL.Append("SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1");
                        objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                        sbSQL.Remove(0, sbSQL.Length);
                        if (objRdr.Read())
                            iTopID = objRdr.GetInt("USER_ID");
                        objRdr.Close();

                        if (iTopID != m_CommonInfo.UserId)
                        {
                            //Check if Days for Approval has lapsed and set the bUnderLimit flag to false
                            //so that the reserve worksheet goes to the next level supervisor
                            bool bDaysApprovalLapsed = false;
                            string sRSWDateTime = "";

                            sbSQL.Append("SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + sRSWID);
                            objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                            sbSQL.Remove(0, sbSQL.Length);
                            if (objRdr.Read())
                            {
                                sRSWDateTime = objRdr.GetString("DTTM_RCD_LAST_UPD");
                                bDaysApprovalLapsed = CheckDaysForApprovalLapsed(sRSWDateTime, m_CommonInfo.UserId, m_CommonInfo.UserId, iClaimId);
                                    //CheckDaysForApprovalLapsed(sRSWDateTime, p_iUserId, p_iUserId);
                            }
                            objRdr.Close();
                            if (bDaysApprovalLapsed)
                                bUnderLimit = false;
                        }
                    }
                }
                else
                    bUnderLimit = true;

                //He cannot approve worksheet submitted by sub ordinate
                if (!bUnderLimit) // If the limit is above the user's limit
                    throw new RMAppException(Globalization.GetString("ReserveWorksheet.ApproveWorksheet.NoAuthApprove",m_iClientId));

                #endregion

                if (bUnderLimit)
                {
                    // When a 'Pending Approval' worksheet is sent for approval.
                      if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Pending Approval")
                    {
                        // Check whether the logged-in user is part of the managerial chain.
                        // Check 'submitted to' field for the worksheet and go on ascending till you find the user.
                        // If not found, return that the user can not approve.

                        objNod = objXMLOut.SelectSingleNode("//control[@name='hdnRSWid']");
                        if (objNod != null)
                            iRswId = Conversion.ConvertObjToInt(objNod.InnerText, base.ClientId);

                        sSQL = "SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRswId;
                        objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
                        if (objRdr.Read())
                        {
                            sSubmittedTo = objRdr.GetString("SUBMITTED_TO");
                            sRswStatus = objRdr.GetInt32("RSW_STATUS_CODE").ToString();
                        }
                        objRdr.Close();

                        objNod = objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']");
                        if (objNod != null)
                            if (objNod.Attributes["codeid"].InnerText != "")
                                if (sRswStatus != objNod.Attributes["codeid"].Value)
                                {
                                    objNod.InnerText = "";
                                    throw new RMAppException(Globalization.GetString("ReserveWorksheet.DataChanged",m_iClientId));
                                }

                        #region Checking Authorized approver
                        iSubmToID = Riskmaster.Common.Conversion.ConvertStrToInteger(sSubmittedTo);
                        if (iSubmToID != 0)
                        {
                            if (m_CommonInfo.UserId != iSubmToID) //If a user other than the one submitted to tries to approve the sheet, then perform the following check
                            {
                                int iLevel = 1;
                                iLevel = GetSupervisoryLevel(iSubmToID, m_CommonInfo.UserId, iClaimId);//GetSupervisoryLevel(iSubmToID, m_CommonInfo.UserId);
                                    //GetSupervisoryLevel(iSubmToID, m_CommonInfo.UserId);
                                if (iLevel == 0)
                                    throw new RMAppException(Globalization.GetString("ReserveWorksheet.ApproveWorksheet.NoAuthApprove",m_iClientId));
                            }
                        }

                        #endregion

                        //If reached here, then the user has the authority to approve the reserves entered.
                        //Modify the reserves. Call the ModifyReserves() function of Funds Adaptor.
                        objNod = objXMLOut.SelectSingleNode("//control[@name='hdnClaimNum']");
                        if (objNod != null)
                            sClaimNum = objNod.InnerText;

                        //objFunds = new ReserveFunds(m_CommonInfo.Connectionstring, sClaimNum, 0, 0, p_objLogin);

                        objDataModelFactory = new DataModelFactory(p_objLogin, m_iClientId);

                        sDateEntered = DateTime.Now.ToShortDateString();

                        #region Getting New Reserves

                        int iReserveType = 0;
                        string sReserveDesc = "";
                        string sGPName = "";
                        string sLOB = "";
                        string sCtrlMName = "";

                        sResFinal = new string[arrReserves.GetLength(0), 4];
                        sLOB = objCache.GetShortCode(iLOBCode);

                        for (iCnt = 0; iCnt < arrReserves.GetLength(0); iCnt++)
                        {
                            iReserveType = arrReserves[iCnt].iReserveTypeCode;
                            sReserveDesc = arrReserves[iCnt].sReserveDesc.Trim().Replace(" ", "");
                            sGPName = "//group[@name='" + sReserveDesc + sLOB + "']";

                            if (objXMLOut.SelectSingleNode(sGPName) != null)
                            {
                                sCtrlMName = sReserveDesc.Substring(0, 3) + sLOB;

                                sResFinal[iCnt, 0] = iReserveType.ToString();

                                objNod = objXMLOut.SelectSingleNode("//control[@name='ResReason" + sCtrlMName + "']");
                                if (objNod != null)
                                    sResFinal[iCnt, 1] = objNod.InnerText.ToString();

                                objNod = objXMLOut.SelectSingleNode("//control[@name='hdnNewRes" + sCtrlMName + "']");
                                if (objNod != null)
                                    sResFinal[iCnt, 2] = Riskmaster.Common.Conversion.ConvertStrToDouble(objNod.InnerText).ToString();
                            }
                        }
                        #endregion

                        #region Entering reserves for claims

                        larrayResCurrent = new long[arrReserves.GetLength(0), 2];
                        larrayResHistory = new long[arrReserves.GetLength(0), 2];

                        //Aman MITS 27915 --start                       
                        objSysSetting = new SysSettings(m_CommonInfo.Connectionstring,m_iClientId);
                        dExhClaimRateToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objSysSetting.BaseCurrencyType, m_CommonInfo.Connectionstring, base.ClientId);
                        //Aman MITS 27915 --end

                        for (iCnt = 0; iCnt < arrReserves.GetLength(0); iCnt++)
                        {
                            larrayResCurrent[iCnt, 0] = arrReserves[iCnt].iReserveTypeCode;
                            larrayResHistory[iCnt, 0] = arrReserves[iCnt].iReserveTypeCode;

                            for (int iCount = 0; iCount < sResFinal.GetLength(0); iCount++)
                            {
                                # region Updating Reserves
                                iReserveTypeCode = Riskmaster.Common.Conversion.ConvertStrToInteger(sResFinal[iCount, 0]);
                                sReason = sResFinal[iCount, 1];
                                sAmt = sResFinal[iCount, 2];
                                //dAmount = Conversion.ConvertStrToDouble(sAmt);
                                dAmount = Conversion.ConvertStrToDouble(sAmt) * dExhClaimRateToBase;  //Aman MITS 27915
                                if (iReserveTypeCode == arrReserves[iCnt].iReserveTypeCode)
                                {
                                        //this part of code is commented by Nitin for Mits 18873
                                    //if (dAmount > 0.0)
                                    //{
                                        // mcapps2 Changed from ReserveFunds to ReserveCurrent for Reserve Current data model object changes. - Begin
                                        //objFunds.ModifyReserves(iClaimId, iClaimantEID, iUnitID, iReserveTypeCode, sDateEntered,
                                        //dAmount, iStatusCodeID, m_CommonInfo.UserName , sReason, m_CommonInfo.UserId , p_iGroupId);

                                        // mcapps2 Changed from ReserveFunds to ReserveCurrent for Reserve Current data model object changes. - Begin
                                        //objFunds = new ReserveFunds(m_CommonInfo.Connectionstring, sClaimNum, 0, 0, p_objLogin);
                                        objFunds = (ReserveCurrent)objDataModelFactory.GetDataModelObject("ReserveCurrent", true);
                                        // mcapps2 Changed from ReserveFunds to ReserveCurrent for Reserve Current data model object changes. - End

                                        objFunds.ClaimId = iClaimId;
                                        objFunds.ClaimantEid = iClaimantEID;
                                        objFunds.UnitId = iUnitID;
                                        objFunds.ReserveTypeCode = iReserveTypeCode;
                                        objFunds.DateEntered = sDateEntered;
                                        objFunds.ReserveAmount = dAmount;
                                        objFunds.ResStatusCode = iStatusCodeID;
                                        objFunds.EnteredByUser = m_CommonInfo.UserName;
                                        objFunds.Reason = sReason;
                                        objFunds.iUserId = m_CommonInfo.UserId;
                                        objFunds.sUser = m_CommonInfo.UserName;
                                        objFunds.iGroupId = p_iGroupId;
                                        objFunds.sUpdateType = "Reserves";
                                        objFunds.Save();
                                        
                                        objFunds = null;
                                        // mcapps2 Changed from ReserveFunds to ReserveCurrent for Reserve Current data model object changes. - End

                                        bResAdjusted = true;
                                        //We have to get the Reserve Current and Reserve History ID here and put it in a array
                                        larrayResCurrent[iCnt, 1] = GetResCurrentID(iClaimId, iClaimantEID, iReserveTypeCode);
                                        larrayResHistory[iCnt, 1] = GetResHistoryID(iClaimId, iClaimantEID, iReserveTypeCode);
                                    //}
                                    //else
                                    //{
                                    //    //We have to get the Reserve Current and Reserve History ID here and put it in a array
                                    //    larrayResCurrent[iCnt, 1] = 0;
                                    //    larrayResHistory[iCnt, 1] = 0;
                                    //}
                                }
                                
                                # endregion
                            }
                        }
                        #endregion

                        if (bResAdjusted)
                        {
                            //Change the worksheet to change the following info:
                            int iAprCodeId = 0;
                            iAprCodeId = objCache.GetCodeId("AP", "RSW_STATUS");

                            //Worksheet approved on and approved by
                            objNod = objXMLOut.SelectSingleNode("//control[@name='hdndtAppRej']");
                            if (objNod != null)
                            {
                                objNod.InnerText = DateTime.Now.ToString();
                                objNod.Attributes["dbformat"].InnerText = Conversion.GetDateTime(DateTime.Now.ToString());
                            }

                            objNod = objXMLOut.SelectSingleNode("//control[@name='hdnAppRejBy']");
                            if (objNod != null)
                                objNod.InnerText = m_CommonInfo.UserName;

                            //Worksheet - status
                            objNod = objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']");
                            if (objNod != null)
                            {
                                objNod.InnerText = "Approved";
                                objNod.Attributes["codeid"].InnerText = iAprCodeId.ToString();
                            }

                            objNod = objXMLOut.SelectSingleNode("//control[@name='hdnLastModBy']");
                            if (objNod != null)
                                sSubmittedBy = objNod.InnerText;

                            objNod = objXMLOut.SelectSingleNode("//control[@name='hdndtLastMod']");
                            if (objNod != null)
                                sLastModOn = objNod.InnerText;

                            //Worksheet - status
                            objNod = objXMLOut.SelectSingleNode("//control[@name='hdnReqStatus']");
                            if (objNod != null)
                            {
                                objNod.InnerText = "Approved";
                                objNod.Attributes["codeid"].InnerText = iAprCodeId.ToString();
                            }

                            //Set the hidden node to a value, in order to refresh the Reserve Listing screen.
                            objNod = objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']");
                            if (objNod != null)
                                objNod.InnerText = "Approve";

                            //Worksheet - approval comments if any.

                            #region If any other approved worksheet is present for that claim, move it to History status

                            objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                            objCon.Open();

                            XmlDocument objPrevAppDoc = new XmlDocument();
                            sSQL = string.Empty;
                            sSQL = "SELECT RSW_ROW_ID, RSW_XML FROM RSW_WORKSHEETS WHERE CLAIM_ID = " + iClaimId.ToString()
                                + " AND RSW_STATUS_CODE = " + iAprCodeId;

                            objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
                            if (objRdr.Read())
                            {
                                int iHisCode = objCache.GetCodeId("HI", "RSW_STATUS");
                                int iHisRSWId = objRdr.GetInt32("RSW_ROW_ID");
                                objPrevAppDoc.LoadXml(objRdr.GetString("RSW_XML"));

                                //Change the XML to update Reserve Worksheet status
                                objNod = objPrevAppDoc.SelectSingleNode("//control[@name='hdnReqStatus']");
                                if (objNod != null)
                                {
                                    objNod.InnerText = "History";
                                    objNod.Attributes["codeid"].InnerText = iHisCode.ToString();
                                }

                                objNod = objPrevAppDoc.SelectSingleNode("//control[@name='hdnRSWStatus']");
                                if (objNod != null)
                                {
                                    objNod.InnerText = "History";
                                    objNod.Attributes["codeid"].InnerText = iHisCode.ToString();
                                }
                                UpdateRSWTransAmountSafeWay(objXMLOut, arrReserves,iClaimCurrCode);

                                objPrevAppDoc = SaveReserveWorksheet(objPrevAppDoc);
                                SaveReserveWorksheetHistory(objPrevAppDoc);
                                objPrevAppDoc = null;

                                //************Mohit Yadav: For Future Use: Can Insert new records into the RSWTables*****

                            }
                            objRdr.Close();

                            #endregion

                            //Update record in database table 'RSW_WORKSHEETS', to change the following:
                            //RSW_STATUS_CODE, UPDATED_BY_USER, DTTM_RCD_LAST_UPD, APPROVED_BY, DTTM_APPROVED

                            //rejection comments
                            objNod = objXMLOut.SelectSingleNode("//control[@name='hdnAppRejComm']");
                            if (objNod != null)
                                if (objXMLOut.SelectSingleNode("//AppRejReqCom") != null)
                                    objNod.InnerText = objXMLOut.SelectSingleNode("//AppRejReqCom").InnerText;
                                else if (objXMLOut.SelectSingleNode("//control[@name='hdnAppRejComm']") != null)
                                    objNod.InnerText = objXMLOut.SelectSingleNode("//control[@name='hdnAppRejComm']").InnerText;

                          

                            objXMLOut = SaveReserveWorksheet(objXMLOut);
                            SaveReserveWorksheetHistory(objXMLOut);
                            //*****Mohit Yadav: Reserve Worksheet record link to Reserve_Current and Reserve_History*****
                            SaveReserveWorksheetLinks(objXMLOut, larrayResCurrent, larrayResHistory);
                            UpdateRSWTransAmountSafeWay(objXMLOut, arrReserves,iClaimCurrCode);
                            objXMLOut = MakeRWReadOnly(objXMLOut);

                            //*****Mohit Yadav: For Future Use: Insert new record into the RSWTables*****
                            //*****Mohit Yadav: Creating and saving PDF file*****
                            //*****Mohit Yadav: Attaching the PDF file to the claim*****

                            //Priya Aggarwal: Document Imaging for Reserve Worksheet - start
                            #region Attach document to Claim

                            //  commented
                         //   MemoryStream memoryStream = new MemoryStream();
                            //MITS 14622

                       //     string dateTimeNow = string.Format("{0:yyyyMMddhhmmss}", System.DateTime.Now);
                       //     fileName = objClaim.ClaimNumber + "_" + dateTimeNow + ".pdf";
                       ////     PrintWorksheet(sRSWID, out memoryStream, true, p_objLogin.objRiskmasterDatabase.GlobalDocPath.ToString(), fileName);
                       //     PrintWorksheet(objRSWReqForPrint, ref objRSWResForPrint);
                       //     if (objCon == null)
                       //     {
                       //         objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                       //         objCon.Open();
                       //     }
                       //     cmd = objCon.CreateCommand();
                           //try
                           // {
                           //     //fileName = objClaim.ClaimNumber + "_" + string.Format("{0:yyyyMMddhhmmss}", System.DateTime.Now) + ".pdf";
                           //     documentId = Utilities.GetNextUID(objCon, "DOCUMENT");
                           //     objDbTransaction = objCon.BeginTransaction();
                           //     cmd.Transaction = objDbTransaction;
                           //     sSQL = "INSERT INTO DOCUMENT (DOCUMENT_ID, DOCUMENT_NAME, "
                           //         + " DOCUMENT_FILENAME,DOCUMENT_TYPE, DOC_INTERNAL_TYPE, "
                           //         + " FOLDER_ID, DOCUMENT_CLASS, "
                           //         + " USER_ID, DOCUMENT_CATEGORY, CREATE_DATE) VALUES("
                           //         + documentId + " , 'RESERVE_WORKSHEET', '" + fileName + "', " +
                           //         objCache.GetCodeId("COR", "DOCUMENT_TYPE").ToString() + ", 2, 0, 0, '"
                           //         + p_objLogin.LoginName + "', 0, " /*,'RESERVE_WORKSHEET', "*/
                           //         + dateTimeNow + ")";
                           //     cmd.CommandText = sSQL;
                           //     cmd.ExecuteNonQuery();

                           //     sSQL = "INSERT INTO DOCUMENT_ATTACH VALUES('CLAIM', " + iClaimId.ToString()
                           //         + ", " + documentId.ToString() + ")";
                           //     cmd.CommandText = sSQL;
                           //     cmd.ExecuteNonQuery();
                           //     objDbTransaction.Commit();
                           // }
                           // catch (Exception ex)
                           // {
                           //     string test = ex.Message;
                           //     objDbTransaction.Rollback();
                           // }
                            sbSQL.Append("SELECT ATTACH_RSV_WK FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + iLOBCode.ToString());
                                                    objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());

                                                    sbSQL.Remove(0, sbSQL.Length);

                                                    if (objRdr.Read())
                                                    {
                                                        iAttachRSW = objRdr.GetInt32("ATTACH_RSV_WK");
                                                    }
                                                    if (iAttachRSW == -1)
                                                    {
                                                        MemoryStream memoryStream = new MemoryStream();
                                                     //   MITS 14622
                                                        string dateTimeNow = string.Format("{0:yyyyMMddhhmmss}", System.DateTime.Now);
                                                        fileName = objClaim.ClaimNumber + "_" + dateTimeNow + ".pdf";

                                                      //  MGaba2:R6:Changed the signatures of PrintWorksheet function.So need to make these changes:Start

                                                        objXmlInForPrint = new XmlDocument();
                                                        objXmlOutForPrint = new XmlDocument();
                                                        objTempElement = objXmlInForPrint.CreateElement("form");
                                                        objXmlInForPrint.AppendChild(objTempElement);
                                                        objTempElement = objXmlInForPrint.CreateElement("control");
                                                        objTempElement.SetAttribute("name", "RSWId");
                                                        objTempElement.InnerText = sRSWID;
                                                        objXmlInForPrint.FirstChild.AppendChild(objTempElement);
                                                        objTempElement = objXmlInForPrint.CreateElement("control");
                                                        objTempElement.SetAttribute("name", "DocPath");
                                                        objTempElement.InnerText = p_objLogin.objRiskmasterDatabase.GlobalDocPath.ToString();                            
                                                        if (p_objLogin.DocumentPath.Length > 0)
                                                        {
                                                             //use StoragePath set for the user
                                                            objTempElement.InnerText = p_objLogin.DocumentPath;
                                                        }
                                                        else
                                                        {
                                                           //  use StoragePath set for the DSN
                                                            objTempElement.InnerText = p_objLogin.objRiskmasterDatabase.GlobalDocPath;
                                                        }
                                                        objXmlInForPrint.FirstChild.AppendChild(objTempElement);
                                                        objTempElement = objXmlInForPrint.CreateElement("control");
                                                        objTempElement.SetAttribute("name", "DocPathType");
                                                        objTempElement.InnerText = p_objLogin.objRiskmasterDatabase.DocPathType.ToString();
                                                        objXmlInForPrint.FirstChild.AppendChild(objTempElement);
                                                        objTempElement = objXmlInForPrint.CreateElement("control");
                                                        objTempElement.SetAttribute("name", "FileName");
                                                        objTempElement.InnerText = fileName;
                                                        objXmlInForPrint.FirstChild.AppendChild(objTempElement);
                                                        objRSWReqForPrint = new ReserveWorksheetRequest();
                                                        objRSWResForPrint = new ReserveWorksheetResponse();
                                                        objRSWReqForPrint.InputXmlString = objXmlInForPrint.InnerXml.ToString();
                                                        PrintWorksheet(objRSWReqForPrint, ref objRSWResForPrint);
                                                      //  PrintWorksheet(sRSWID, out memoryStream, true, p_objLogin.objRiskmasterDatabase.GlobalDocPath.ToString(), fileName);
                                                   //     MGaba2:R6:End
                                                        if (objCon == null)
                                                        {
                                                            objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                                                            objCon.Open();
                                                        }
                                                        cmd = objCon.CreateCommand();
                                                        try
                                                        {
                                                            fileName = objClaim.ClaimNumber + "_" + string.Format("{0:yyyyMMddhhmmss}", System.DateTime.Now) + ".pdf";
                                                            documentId = Utilities.GetNextUID(objCon, "DOCUMENT", base.ClientId);
                                                            objDbTransaction = objCon.BeginTransaction();
                                                            cmd.Transaction = objDbTransaction;
                                                            sbSQL.Append("INSERT INTO DOCUMENT (DOCUMENT_ID, DOCUMENT_NAME, DOCUMENT_TYPE,"
                                                                + " DOCUMENT_FILENAME,DOC_INTERNAL_TYPE, "
                                                                + " FOLDER_ID, DOCUMENT_CLASS, "
                                                                + " USER_ID, DOCUMENT_CATEGORY, CREATE_DATE) VALUES("
                                                                + documentId + " , 'RESERVE_WORKSHEET',0, '" + fileName + "', 2, 0, 0, '"
                                                                + p_objLogin.LoginName + "', 0, "
                                                                + dateTimeNow + ")");
                                                            cmd.CommandText = sbSQL.ToString();
                                                            cmd.ExecuteNonQuery();
                                                            sbSQL.Remove(0, sbSQL.Length);

                                                          //  iReserveTrackingLevel = GetReserveTracking(iClaimId, ref iLOBCode);
                                                        //    MGaba2:To Do Left for iReserveTrackingLevel=2
                                                            sTableNameForAttach = "CLAIM";
                                                            iRecordIdForAttach = iClaimId;

                                                            //if (iReserveTrackingLevel != 0)
                                                            //{
                                                            //    if (iClaimantRowId != 0)
                                                            //    {
                                                            //        sTableNameForAttach = "CLAIMANT";
                                                            //        iRecordIdForAttach = iClaimantRowId;
                                                            //    }
                                                            //    else if (iUnitRowId != 0)
                                                            //    {
                                                            //        sTableNameForAttach = "UNIT";
                                                            //        iRecordIdForAttach = iUnitRowId;
                                                            //    }
                                                            //}


                                                            sbSQL.Append("INSERT INTO DOCUMENT_ATTACH VALUES(" + Utilities.FormatSqlFieldValue(sTableNameForAttach) + ", " + iRecordIdForAttach.ToString()
                                                                + ", " + documentId.ToString() + ")");
                                                            cmd.CommandText = sbSQL.ToString();
                                                            cmd.ExecuteNonQuery();
                                                            sbSQL.Remove(0, sbSQL.Length);

                                                            objDbTransaction.Commit();
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            string test = ex.Message;
                                                            objDbTransaction.Rollback();
                                                        }
                                                    }
                                                  
                         //   end shobhana 
                           #endregion

                            //Priya Aggarwal: Document Imaging for Reserve Worksheet - end
                                                   
                            #region Send an email to the person who submitted the worksheet and also create the Diary

                            string sToEmail = string.Empty;
                            string sFromEmail = string.Empty;
                            string sSubjectEmail = string.Empty;
                       //     string sBodyEmail = string.Empty;
                            string sToUser = string.Empty;
                            string sFromUser = string.Empty;
                           
                            string sRSWType = string.Empty;
                            string sPCName = string.Empty;
                            string strReason = string.Empty;
                            int iAdjID = 0;

//MGaba2:R8:SuperVisory Approval
                            iAdjID = CommonFunctions.GetAdjusterEID(iClaimId, m_CommonInfo.Connectionstring, base.ClientId);


                              if (iAdjID > 0 && (!m_bDisableEmailNotify || !m_bDisableDiaryNotify))//Start : rsushilaggar - 22-Mar-2010 Mits : 19624
                            {
                                objFunds = (ReserveCurrent)objDataModelFactory.GetDataModelObject("ReserveCurrent", true);

                                sbSQL.Append("SELECT LOGIN_NAME");
                                sbSQL.Append(" FROM USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID = " + Conversion.ConvertStrToInteger(iAdjID.ToString()));
                                sbSQL.Append(" AND DSNID = " + m_CommonInfo.DSNID);
                                objRdr = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sbSQL.ToString());
                                sbSQL.Remove(0, sbSQL.Length);

                                if (objRdr.Read())
                                    sSubmittedTo = objRdr.GetString("LOGIN_NAME");
                                else
                                    sSubmittedTo = "";

                                objRdr.Close();

                                objNod = objXMLOut.SelectSingleNode("//control[@name='WorksheetType']");
                                if (objNod != null)
                                    sRSWType = objNod.Attributes["title"].InnerText;

                                objNod = objXMLOut.SelectSingleNode("//control[@name='hdnpcName']");
                                sPCName = objNod.InnerText;

                                objNod = objXMLOut.SelectSingleNode("//control[@name='hdnAppRejComm']");
                                if (objNod != null)
                                {  if (objNod.InnerText != "")
                                      strReason = objNod.InnerText;
                                }
                                //skhare7 Fn Transfferd to common fns  R8 supervisory approval
                                CommonFunctions.GetUserDetails(sSubmittedTo, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sToEmail, ref sToUser,m_iClientId);
                                CommonFunctions.GetUserDetails(m_CommonInfo.UserName, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sFromEmail, ref sFromUser,m_iClientId);
                                //skhare7 Fn Transfferd to common fns  R8 supervisory approval End
                               // sSubjectEmail = sRSWType + " for " + sClaimNum + " – " + sPCName + " Approved";
                                string sBodyEmail = string.Empty;
                              //  EmailFormat objEMail = null;
                              //  objEMail = new EmailFormat(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword);
                                objFunds = (ReserveCurrent)objDataModelFactory.GetDataModelObject("ReserveCurrent", true);
                                sBodyEmail = objFunds.GetEmailFormat("RSWSheet_APP", iClaimId);
               
                                if (sPCName != "" && sPCName != null)

                                //changed by shobhana MITS 22442

                              
                                {

                                    sSubjectEmail = "The Reserve Worksheet for " + sClaimNum + " – " + sPCName + " Approved";
                                   
                      
                                }
                                else
                                {
                                    sSubjectEmail = "The Reserve Worksheet for " + sClaimNum +" Approved";
                                  
                                }
                                if (sLastModOn != null && sLastModOn != "")
                                {
                                    DateTime dtLastModOn = Convert.ToDateTime(sLastModOn);
                                    sLastModOn = dtLastModOn.ToShortDateString();
                                
                                }
               
                                    sBodyEmail = sBodyEmail.Replace("{RSW_TYPE}", sRSWType)
                                                     .Replace("{APPROVED_BY}", sFromUser)
                                                     .Replace("{LAST_MODIFIED_DATE}",sLastModOn)
                                                     .Replace("{USER_NAME}", sPCName)
                                                     .Replace("{APPROVE_REASON}", strReason);
                                 // MITS 22442
                                //Send a diary to the person who submitted the worksheet
                                string sRegarding = sClaimNum + " * " + sPCName;
                                if (sSubmittedTo != m_CommonInfo.UserName)
                                {
                                    //The appropriate user should be getting an email and a diary
                                    GenerateDiary(sSubjectEmail, sBodyEmail, 1, sSubmittedTo, m_CommonInfo.UserName, sRegarding, iClaimId);
                                    if (sToEmail != "" && sFromEmail != "")
                                        CommonFunctions.SendEmail(sToEmail, sFromEmail, sSubjectEmail, sBodyEmail, base.ClientId);
                                    //skhare7 Fn Transfferd to common fns  R8 supervisory approval
                                }
                            }

                            #endregion

                        }
                    }
                }
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException
                    (Globalization.GetString("ReserveWorksheet.ApproveWorksheet.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();

                if (objClaim != null)
                    objClaim.Dispose();

                if (objFunds != null)
                    objFunds = null;

                if (objCache != null)
                    objCache.Dispose();

                if (objCon != null)
                    objCon.Dispose();

                if (objRdr != null)
                    objRdr.Dispose();

                objNod = null;
            }


            p_WorksheetResponse.OutputXmlString = objXMLOut.OuterXml;
        }

        /// <summary>
        /// This function is called when the worksheet is sent for rejection.
        /// </summary>
        /// <param name="p_objXmlDoc">in XML</param>
        /// <param name="p_iDSNId">DSN id</param>
        /// <param name="p_iUserId">Logged in user id</param>
        /// <param name="p_iGroupId">Logged in user's group id</param>
        /// <returns>Returns the XML to be rendered on the screen.</returns>
        public override void RejectWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId)
        {
            int iRswId = 0;
            StringBuilder sbSQL;
            string sRswStatus = "";
            XmlNode objNod = null;
            DbConnection objCon = null;
            LocalCache objCache = null;
            DbReader objRdr = null;
            DbReader objReaderLOB = null;            
            XmlDocument objRswXml = null;

            string sSubmittedTo = string.Empty;
            int iSubmToID = 0;
            int iSubmbyID = 0;
            int iClaimId = 0;

            string sSubmittedBy = string.Empty;
            string sLastModOn = string.Empty;

            int iLOBCode = 0;
            int iClmTypeCode = 0;
            bool bUnderLimit = false;
            string sRswTypeToLaunch = string.Empty;
            ReserveFunds.structReserves[] arrReserves = null;
            //skhare7 R8
            ReserveCurrent objReserveCurrent = null;
            DataModelFactory objDataModelFactory = null;
            //skhare7 
            int iClaimCurrCode = 0;

            try
            {
                sbSQL = new StringBuilder();

                /*Check whether the logging user is part of the managerial chain. 
                 *Check 'SUBMITTED_TO' field for the worksheet and go on ascending till you find the user. 
                 *If not found, return that the user can not reject.*/
                objRswXml = new XmlDocument();
                objRswXml.LoadXml(p_WorksheetRequest.InputXmlString);


                objNod = objRswXml.SelectSingleNode("//RswId");
                if (objNod != null)
                    iRswId = Conversion.ConvertObjToInt(objNod.InnerText, base.ClientId);

                if (iRswId <= 0)
                {
                    objNod = objRswXml.SelectSingleNode("//control[@name='hdnRSWid']");
                    if (objNod != null)
                        iRswId = Conversion.ConvertStrToInteger(objNod.InnerText);
                }

                sbSQL.Append("SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRswId);
                objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                sbSQL.Remove(0, sbSQL.Length);
                if (objRdr.Read())
                {
                    sSubmittedTo = objRdr.GetString("SUBMITTED_TO");
                    iSubmbyID = Riskmaster.Common.Conversion.ConvertStrToInteger(objRdr.GetString("SUBMITTED_BY"));
                    iSubmToID = Riskmaster.Common.Conversion.ConvertStrToInteger(objRdr.GetString("SUBMITTED_TO"));
                    sRswStatus = objRdr.GetInt32("RSW_STATUS_CODE").ToString();
                    sRswTypeToLaunch = objRdr.GetString("RSWXMLTYPE");

                    //Added by Shobhana
                    if (!IsRSWAllowed(iRswId, sRswTypeToLaunch, Conversion.ConvertStrToInteger(objRswXml.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                    {
                        throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                    }
                }
                objRdr.Close();

                //If a user other than the one submitted to tries to approve the sheet, then perform the following check
                #region Check to see if a user other than the one submitted to tries to reject the reserve worksheet
                if (iSubmToID != 0)
                {
                    if (m_CommonInfo.UserId != iSubmToID)
                    {
                        //Stop looping if the user is found in the managerial chain
                        int iLevel = 1;
                        iLevel = GetSupervisoryLevel(iSubmToID, m_CommonInfo.UserId, iClaimId);//GetSupervisoryLevel(iSubmToID, m_CommonInfo.UserId);
                        if (iLevel == 0)
                            throw new RMAppException(Globalization.GetString("ReserveWorksheet.RejectWorksheet.NoAuthRej",m_iClientId));
                    }
                }

                #endregion

                /*Check whether the reserves submitted are under the user's limits.
                 *If not, return that the user cannot reject.*/

                objRswXml = new XmlDocument();
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);

                sbSQL.Append("SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRswId);
                sbSQL.Append(" AND RSW_STATUS_CODE = " + objCache.GetCodeId("PA", "RSW_STATUS"));
                objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                sbSQL.Remove(0, sbSQL.Length);
                if (objRdr.Read())
                    objRswXml.LoadXml(objRdr.GetString("RSW_XML"));
                objRdr.Close();

                //Check If the status has changed
                objNod = objRswXml.SelectSingleNode("//RswStatus");
                if (objNod != null)
                {
                    if (objNod.InnerText != "")
                    {
                        if (sRswStatus != objNod.InnerText)
                        {
                            objNod.InnerText = "";
                            throw new RMAppException(Globalization.GetString("ReserveWorksheet.DataChanged",m_iClientId));
                        }
                    }
                }

                objNod = objRswXml.SelectSingleNode("//control[@name='hdnClaimId']");
                iClaimId = Conversion.ConvertStrToInteger(objNod.InnerText);

                #region Get the Line of Business for the claim

                sbSQL.Append("SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE,CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimId);
                objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReaderLOB.Read())
                {
                    iLOBCode = objReaderLOB.GetInt32("LINE_OF_BUS_CODE");
                    iClmTypeCode = objReaderLOB.GetInt32("CLAIM_TYPE_CODE");
                    iClaimCurrCode = objReaderLOB.GetInt32("CLAIM_CURR_CODE");
                }
                objReaderLOB.Close();
                sbSQL.Remove(0, sbSQL.Length);

                #endregion

                //Check Reserve Limits Enabled/Disabled
                GetLOBReserveOptions(iClaimId);

                //Get Supervisory Approval Settings
                ReadRSWCheckOptions();

                string[,] sResFinal = null;

                arrReserves = GetResCategories(iLOBCode, iClmTypeCode);
                sResFinal = new string[arrReserves.GetLength(0), 4];

                sResFinal = GetNewReserveAmts(iLOBCode, arrReserves, objRswXml, iClaimCurrCode);

                if (m_bResLimitFlag) //If the Flag is 'true' then check the reserve limits
                {
                    bUnderLimit = UnderUserLimit(m_CommonInfo.UserId, p_iGroupId, iClaimId, sResFinal, arrReserves,iClaimCurrCode);

                    //By Vaibhav on 11/14/08
                    if (bUnderLimit && m_iDaysAppReserves > 0)
                    {
                        int iTopID = 0;
                        sbSQL.Append("SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1");
                        objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                        sbSQL.Remove(0, sbSQL.Length);
                        if (objRdr.Read())
                            iTopID = objRdr.GetInt("USER_ID");

                        if (iTopID != m_CommonInfo.UserId)
                        {
                            //Check if Days for Approval has lapsed and set the bUnderLimit flag to false
                            //so that the reserve worksheet goes to the next level supervisor
                            bool bDaysApprovalLapsed = false;
                            string sRSWDateTime = "";

                            sbSQL.Append("SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRswId);
                            objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                            sbSQL.Remove(0, sbSQL.Length);
                            if (objRdr.Read())
                            {
                                sRSWDateTime = objRdr.GetString("DTTM_RCD_LAST_UPD");
                                //By Vaibhav on 11/14/08 : modified  CheckDaysForApprovalLapsed function
                                bDaysApprovalLapsed = CheckDaysForApprovalLapsed(sRSWDateTime, Conversion.ConvertObjToInt(objRdr.GetString("SUBMITTED_TO"), base.ClientId), m_CommonInfo.UserId, iClaimId);
                                    //CheckDaysForApprovalLapsed(sRSWDateTime, Conversion.ConvertObjToInt(objRdr.GetString("SUBMITTED_TO")), m_CommonInfo.UserId);
                            }
                            objRdr.Close();
                            if (bDaysApprovalLapsed)
                                bUnderLimit = false;
                        }
                    }
                    //vaibhav code end
                }
                else
                    bUnderLimit = true;

                //He can reject worksheet submitted by sub ordinate, even if its not in limit
                if (!bUnderLimit) // If the limit is above the user's limit
                    throw new RMAppException(Globalization.GetString("ReserveWorksheet.RejectWorksheet.NoAuthRej",m_iClientId));

                //If reached here, then the user has the authority to reject the reserves entered.
                //Change the worksheet XML to change the following info:
                //last modified on and last modified by
                if (bUnderLimit)
                {
                    objNod = objRswXml.SelectSingleNode("//control[@name='hdndtLastMod']");

                    if (objNod != null)
                    {
                        sLastModOn = objNod.InnerText;
                        objNod.InnerText = DateTime.Now.ToString();
                        objNod.Attributes["dbformat"].InnerText = Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString());
                    }


                    objNod = objRswXml.SelectSingleNode("//control[@name='hdnLastModBy']");
                    if (objNod != null)
                    {
                        sSubmittedBy = objNod.InnerText;
                        objNod.InnerText = m_CommonInfo.UserName;
                    }

                    objNod = objRswXml.SelectSingleNode("//control[@name='hdnUnderLimit']");
                    if (objNod != null)
                        objNod.InnerText = "Reject";

                    //status
                    int iRejCode = objCache.GetCodeId("RJ", "RSW_STATUS");
                    objNod = objRswXml.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    objNod.InnerText = "Rejected";
                    objNod.Attributes["codeid"].InnerText = iRejCode.ToString();

                    //Change the date rejected and rejected by fields
                    objNod = objRswXml.SelectSingleNode("//control[@name='hdndtAppRej']");
                    objNod.InnerText = DateTime.Now.ToString();
                    objNod.Attributes["dbformat"].InnerText = Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString());

                    objNod = objRswXml.SelectSingleNode("//control[@name='hdnAppRejBy']");
                    objNod.InnerText = m_CommonInfo.UserName;

                    objNod = objRswXml.SelectSingleNode("//control[@name='hdnMode']");
                    if (objNod != null)
                        objNod.InnerText = "ReadOnly";

                    //rejection comments
                    objNod = objRswXml.SelectSingleNode("//control[@name='hdnAppRejComm']");
                    if (objNod != null)
                        if (objRswXml.SelectSingleNode("//AppRejReqCom") != null)
                            objNod.InnerText = objRswXml.SelectSingleNode("//AppRejReqCom").InnerText;
                        else if (objRswXml.SelectSingleNode("//control[@name='hdnAppRejComm']") != null)
                            objNod.InnerText = objRswXml.SelectSingleNode("//control[@name='hdnAppRejComm']").InnerText;

                    // Update record in database table 'RSW_WORKSHEETS'
                    objRswXml = SaveReserveWorksheet(objRswXml);
                    SaveReserveWorksheetHistory(objRswXml);
                    objRswXml = MakeRWEditable(objRswXml);

                    //Insert new record into the RSWTables*****                  
                    //For future use: RSWtoTables(objRswXml, objCon, iNewRswId);

                    //Send email to the user who submitted the worksheet

                    string sToEmail = string.Empty;
                    string sFromEmail = string.Empty;
                    string sSubjectEmail = string.Empty;
                //    string sBodyEmail = string.Empty;
                    string sToUser = string.Empty;
                    string sFromUser = string.Empty;

                    string sCurAdjEmail = string.Empty;
                    string sCurAdjUser = string.Empty;
                    string sCurAdjLogin = string.Empty;
                  
                    string sRSWType = string.Empty;
                    string sClaimNumber = string.Empty;
                    string sPCName = string.Empty;
                    string sReason = string.Empty;

                    //smahajan6 - For MITS 13750 - Start
//MGaba2:R8:SuperVisory Approval
                    int iAdjID = CommonFunctions.GetAdjusterEID(iClaimId, m_CommonInfo.Connectionstring, base.ClientId);
                  //  string sBodyEmail = string.Empty;
                   
              
                    sSubmittedTo = "";
                    if (iAdjID > 0 && (!m_bDisableEmailNotify || !m_bDisableDiaryNotify))//rsushilaggar : 22-Mar-2010 Mits : 19624
                    {
                        sbSQL.Append("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE");
                        sbSQL.Append(" WHERE USER_ID = " + iAdjID);
                        sbSQL.Append(" AND DSNID = " + m_CommonInfo.DSNID);
                        objRdr = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sbSQL.ToString());
                        sbSQL.Remove(0, sbSQL.Length);

                        if (objRdr.Read())
                            sSubmittedTo = objRdr.GetString("LOGIN_NAME");
                        else
                            sSubmittedTo = "";

                        objRdr.Close();

                        objNod = objRswXml.SelectSingleNode("//control[@name='WorksheetType']");
                        if (objNod != null)
                            sRSWType = objNod.Attributes["title"].InnerText;

                        objNod = objRswXml.SelectSingleNode("//control[@name='hdnClaimNum']");
                        if (objNod != null)
                            sClaimNumber = objNod.InnerText;

                        objNod = objRswXml.SelectSingleNode("//control[@name='hdnpcName']");
                        if (objNod != null)
                            sPCName = objNod.InnerText;

                        objNod = objRswXml.SelectSingleNode("//control[@name='hdnAppRejComm']");
                        if (objNod != null)
                        {  if (objNod.InnerText != "")
                              sReason = objNod.InnerText;
                        }
                        //skhare7 Fn Transfferd to common fns  R8 supervisory approval
                        CommonFunctions.GetUserDetails(sSubmittedTo, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sToEmail, ref sToUser,m_iClientId);
                        CommonFunctions.GetUserDetails(m_CommonInfo.UserName, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sFromEmail, ref sFromUser,m_iClientId);
                        //skhare7 Fn Transfferd to common fns  R8 supervisory approval End
                        sSubjectEmail = sRSWType + " for Claim " + sClaimNumber + " - " + sPCName + " Rejected";
                        //shobhana 
                        string sBodyEmail = string.Empty;
                        //skahre7 changed for R8
                     //   EmailFormat objEMail = null;
                       // objEMail = new EmailFormat(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword);
                        objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);
                        objReserveCurrent = (ReserveCurrent)objDataModelFactory.GetDataModelObject("ReserveCurrent", true);
                        sBodyEmail = objReserveCurrent.GetEmailFormat("RSWSheet_REJ", iClaimId);
                       // MITS 22442
                        if (sPCName != "" && sPCName != null)
                        {

                            sSubjectEmail = "The Reserve Worksheet for Claim " + sClaimNumber + " - " + sPCName + " Rejected";
                        
                        }
                        else
                        {
                             sSubjectEmail = "The Reserve Worksheet for Claim " + sClaimNumber + " Rejected";
                          
                        }
                        //MITS 20181 Defects removed
                        if (sLastModOn != null && sLastModOn != "")
                        {
                            DateTime dtLastModOn = Convert.ToDateTime(sLastModOn);
                            sLastModOn = dtLastModOn.ToShortDateString();

                        }
                        sBodyEmail = sBodyEmail.Replace("{RSW_TYPE}", sRSWType)
                                              .Replace("{REJECTED_BY}", sFromUser)
                                              .Replace("{SUBMITTED_DATE}", sLastModOn)
                                              .Replace("{USER_NAME}", sPCName)
                                              .Replace("{REASON}", sReason);
                        //MITS 20181 Defects removed
                            //RISKMASTER X"; //Vaibhav for Safeway MITS 13918
                      //  MITS 22442
                        //Send a diary to the person who submitted the worksheet
                        string sRegarding = sClaimNumber + " * " + sPCName; // TODO: regarding
                        if (sSubmittedTo != m_CommonInfo.UserName)//smahajan6 - For MITS 13750 
                        {
                            //The appropriate user should be getting an email, not a diary. No diary should exist
                            GenerateDiary(sSubjectEmail, sBodyEmail, 2, sSubmittedTo, m_CommonInfo.UserName, sRegarding, iClaimId);
                            if (sToEmail != "" && sFromEmail != "")
                                CommonFunctions.SendEmail(sToEmail, sFromEmail, sSubjectEmail, sBodyEmail, base.ClientId);
                            //skhare7 Fn Transfferd to common fns  R8 supervisory approval End
                        }
                    }
                    //smahajan6 - For MITS 13750 - End
                }
           
        
        
             p_WorksheetResponse.OutputXmlString = objRswXml.OuterXml;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objCon != null)
                    objCon.Dispose();
                if (objCache != null)
                    objCache.Dispose();
                if (objRdr != null)
                    objRdr.Dispose();
                if (objReaderLOB != null)
                    objReaderLOB.Dispose();
                objRswXml = null;
                objNod = null;
                if (objReserveCurrent != null)
                {
                    objReserveCurrent.Dispose();
                    objReserveCurrent = null;
                }
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                    objDataModelFactory = null;
                }
            }
        }


        /// <summary>
        /// MGaba2:R6 :This function Prints a Worksheet.Changing the signature of this function to make it void
        /// </summary>
        /// <param name="p_sRSWID">RSW ID of the worksheet to be printed</param>
        /// <param name="p_objMemory">out parameter containing the PDF in byte format</param>
        /// <param name="p_IsApproval">Denotes whether being called when approving worksheet</param>
        /// <param name="p_spath">Document Path</param>
        /// <param name="p_sfileName">File Name</param>
        public override void PrintWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse)
        {
            XmlDocument objRSWDoc = null;
            XmlNode objNode = null;

            DbReader objReader = null;
            StringBuilder sbSQL;

            string sFile = string.Empty;
            PrintWrapper objPrintWrapper = null;

            string sTitle = string.Empty;
            double dLineHeight = 0.0;

            string sText = string.Empty;
            string sClaimNumber = string.Empty;

            //Vaibhav for Safeway on 03/30/2009
            DirectoryInfo objDirectory = null;

            //Vaibhav: Safeway : 03/10/2209: Start
            //Populating the claim related information
            long lClaimID = 0;

            int iLOBCode = 0;
            int iReserveType = 0;
            int iClmTypeCode = 0;

            Claim objClaim = null;
            DataModelFactory objDataModelFactory = null;
            LocalCache objCache = null;

            ReserveFunds.structReserves[] arrReserves = null;

            string sLOB = string.Empty;
            string sReserveType = string.Empty;
            string[,] sResFinal = null;
            //Vaibhav: End

            //MGaba2:R6
            //Transferred code from business layer to application layer in case of click of Print button:Start
            string sRSWID = "0";
            MemoryStream objMemory = null;
            XmlElement objTempElement = null;
            string sPathForAttach = string.Empty;
            string sFileNameForAttach = string.Empty;
            XmlDocument objXmlIn = null;
            XmlDocument objXmlOut = null;
            bool bDocPathType = false;
            FileStorageManager objFileStorageManager = null;
            //MGaba2:R6:End
            string sRswTypeToLaunch = string.Empty;  //MGaba2:MITS 22225
            try
            {

                objXmlIn = new XmlDocument();
                objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);

                //string sGpName = string.Empty;
                sbSQL = new StringBuilder();
                objRSWDoc = new XmlDocument();
                objNode = objXmlIn.SelectSingleNode("//control[@name='RSWId']");
                if (objNode != null)
                {
                    sRSWID = objNode.InnerText.Trim();
                }
                //MGaba2:MITS 22225:Start
                //In case a WC worksheet is created as Generic but now user prints it with Customize settings
                //or worksheet for any lob other than wc is created as customize but now user prints it with Generic settings
                //Error should come
                //sbSQL.Append("SELECT RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + sRSWID);
                sbSQL.Append("SELECT RSWXMLTYPE,RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + sRSWID);
                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                sbSQL.Remove(0, sbSQL.Length);
                if (objReader.Read())
                {
                    objRSWDoc.LoadXml(objReader.GetString("RSW_XML"));
                    sRswTypeToLaunch = objReader.GetString("RSWXMLTYPE");
                    if ((objRSWDoc.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText != "Approved" && objRSWDoc.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText != "History"))
                    {  if (!IsRSWAllowed(Convert.ToInt32(sRSWID), sRswTypeToLaunch, Convert.ToInt32(objRSWDoc.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                    {
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.ConfigurationChanged.Error",m_iClientId));
                    }
                    }
                }//MGaba2:MITS 22225:End
                else
                    throw new RMAppException(Globalization.GetString("ReserveWorksheet.PrintWorksheet.RSWNotFound",m_iClientId));
                objReader.Close();
                objReader.Dispose();

                //Title of the worksheet
                objNode = objRSWDoc.SelectSingleNode("//control[@title='Claim Reserve Worksheet']");
                if (objNode != null)
                    sTitle = "Claim Reserve Worksheet";

                objNode = objRSWDoc.SelectSingleNode("//control[@name='hdnClaimNum']");
                if (objNode != null)
                    sTitle += " for " + objNode.InnerText;

                //Vaibhav: Safeway: Start
                objNode = objRSWDoc.SelectSingleNode("//control[@name='hdnClaimId']");
                if (objNode != null)
                    lClaimID = Conversion.ConvertStrToLong(objNode.InnerText);

                objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);

                objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(Conversion.ConvertObjToInt(lClaimID, base.ClientId));
                iLOBCode = objClaim.LineOfBusCode;
                iClmTypeCode = objClaim.ClaimTypeCode;
                sLOB = objCache.GetShortCode(iLOBCode);
                //Vaibhav: End

                objPrintWrapper = new PrintWrapper(m_iClientId);
                objPrintWrapper.StartDoc(false);
                dLineHeight = objPrintWrapper.GetTextHeight("W");

                CreateNewPage(true, sTitle, objPrintWrapper);
                objPrintWrapper.PageLimit = 14700;

                objPrintWrapper.CurrentX = 100;
                objPrintWrapper.CurrentY = 600;

                string sGpName = string.Empty;

          
                ////objPrintWrapper.EndDoc();

                foreach (XmlNode objGroup in objRSWDoc.SelectNodes("//group"))
                {
                    WriteText("", false, objPrintWrapper, sTitle, false, 9);
                    WriteText(objGroup.Attributes["title"].InnerText.ToUpper(), false, objPrintWrapper, sTitle, true, 9);

                    sGpName = objGroup.Attributes["name"].InnerText;
                    switch (sGpName)
                    {
                        case "ClaimInfo":
                            PrintAllWorksheetsSafeWay(objGroup, objRSWDoc, objPrintWrapper, sLOB, sGpName);
                            break;
                        case "RESERVESUMMARYWC":
                            PrintAllWorksheetsSafeWay(objGroup, objRSWDoc, objPrintWrapper, sLOB, sGpName);
                            break;
                        default:
                            {
                                //commented by shobhana for the time being for test
                                arrReserves = GetResCategories(iLOBCode, iClmTypeCode);

                                for (int iCnt = 0; iCnt < arrReserves.GetLength(0); iCnt++)
                                {                                   
                                    iReserveType = arrReserves[iCnt].iReserveTypeCode;
                                    //Aman Multi Currency--Start
                                    sReserveType = objCache.GetCodeDesc(iReserveType);
                                    if (sReserveType.Length < 3)
                                    {
                                        continue;
                                    }

                                    sReserveType = sReserveType.Substring(0, 3);   //Aman Multi Currency--End

                                    if (sGpName == arrReserves[iCnt].sReserveDesc.Trim().Replace(" ", "") + sLOB)
                                        //PrintAllWorksheets(objGroup, objRSWDoc, objPrintWrapper, sLOB, sReserveType);
                                PrintAllWorksheetsSafeWay(objGroup, objRSWDoc, objPrintWrapper, sLOB, sGpName.Substring(0, 3));
                                }
                            }
                            break;
                    }
                }

                objPrintWrapper.EndDoc();


                sFile = TempFile;



                objMemory = new MemoryStream();
                objPrintWrapper.GetPDFStream(sFile, ref objMemory);
                File.Delete(sFile);

                //MGaba2:R6:Changed the signatures of this function...
                //So need to fetch path and file name from xml in
                //objNode = objXmlIn.SelectSingleNode("//control[@name='DocPath']");
                //if (objNode != null)
                //{
                //    sPathForAttach = objNode.InnerText.Trim();
                //}
                //objNode = objXmlIn.SelectSingleNode("//control[@name='FileName']");
                //if (objNode != null)
                //{
                //    sFileNameForAttach = objNode.InnerText.Trim();
                //}

                ////Data Stotage Type: File or database
                //objNode = objXmlIn.SelectSingleNode("//control[@name='DocPathType']");
                //if (objNode != null)
                //{
                //    bDocPathType = Convert.ToBoolean(Convert.ToInt32(objNode.InnerText.Trim()));
                //}
                //MGaba2:R6:End

                //Priya Aggarwal: Document Imaging for Reserve Worksheet - start  
                //MGaba2:R6:Replaced variable names:p_spath with sPathForAttach and p_sfileName with sFileNameForAttach
              
                //Priya Aggarwal: Document Imaging for Reserve Worksheet - start          
                if (sPathForAttach != string.Empty)
                {
                    //Vaibhav for Safeway on 03/30/2009: RSW approval UAT - checking the existance of directory in order to save
                    objDirectory = new DirectoryInfo(sPathForAttach);
                    if (objDirectory.Exists)
                    {
                        objPrintWrapper.Save(sPathForAttach + "\\" + sFileNameForAttach);
                    }
                }
                if (sPathForAttach != string.Empty)
                {
                    objFileStorageManager = new FileStorageManager(bDocPathType ? StorageType.DatabaseStorage : StorageType.FileSystemStorage, sPathForAttach, m_iClientId);
                    objFileStorageManager.StoreFile(objMemory, sFileNameForAttach);
                }
                //Priya Aggarwal: Document Imaging for Reserve Worksheet - end
                else
                { //MGaba2:R6:Transferred code from business layer to application layer in case of click of Print button
                    objXmlOut = new XmlDocument();
                    objTempElement = objXmlOut.CreateElement("RSW");
                    objXmlOut.AppendChild(objTempElement);
                    objTempElement = objXmlOut.CreateElement("File");
                    objTempElement.SetAttribute("Filename", Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));
                    objTempElement.InnerText = Convert.ToBase64String(objMemory.ToArray());
                    objXmlOut.FirstChild.AppendChild(objTempElement);
                    p_WorksheetResponse.OutputXmlString = objXmlOut.OuterXml;
                }
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                objPrintWrapper = null;
                objRSWDoc = null;
                objNode = null;
                objCache = null;
                objClaim = null;
                objDataModelFactory = null;
                //Vaibhav on 03/30/2009
                objDirectory = null;
                objFileStorageManager = null;
                objMemory = null;

            }
            //MGaba2:Changed the return type of function from string to void
            //return sFile;
        }

        //gagnihotri Reserve Approval
        public override void ListFundsTransactions(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse)
        {
            //List of Funds Transactions

            ReserveApproval objResApprove = null;
            XmlDocument objAllTrans = null;
            XmlDocument objXmlIn = null;

            try
            {
                //objResApprove = new ReserveApproval(connectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, m_securityConnectionString, base.DSNID);
                objResApprove = new ReserveApproval(m_CommonInfo,m_iClientId);
                objResApprove.UserId = m_CommonInfo.UserId;
                objResApprove.UserName = m_CommonInfo.UserName;
                objResApprove.ListFundsTransactions(out objAllTrans,"");
                objXmlIn = new XmlDocument();
                objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);

                if (objAllTrans != null)
                {
                    if (objAllTrans.SelectSingleNode("//ReserveApproval/Approvals") != null)
                    {
                        if (objAllTrans.SelectSingleNode("//ReserveApproval/Approvals").InnerXml.Length > 0)
                        {
                            objXmlIn.SelectSingleNode("//ReserveApprovals").InnerXml = objAllTrans.SelectSingleNode("//ReserveApproval").InnerXml;
                        }
                    }
                }
                p_WorksheetResponse.OutputXmlString = objXmlIn.SelectSingleNode("//ReserveApprovals").OuterXml.ToString();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
                //return false;
            }
            catch (Exception p_objException)
            {
                throw new Exception(Globalization.GetString("ReserveApprovalAdaptor.ListFundsTransactions.Error", m_iClientId), p_objException);
                //p_objErrOut.Add(p_objException, Globalization.GetString("ReserveApprovalAdaptor.ListFundsTransactions.Error"), BusinessAdaptorErrorType.Error);
                //return false;
            }
            finally
            {
                objResApprove.Dispose(); // important to kill DataModelFactory
                objResApprove = null;
                objAllTrans = null;
                objXmlIn = null;
            }
            //return true;
        }

        public override void ApproveReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
        {
            //ReserveWorksheet objResWS = null;
            ReserveApproval objResApprove = null;
            string sCallFor = "";
            StringBuilder sbXmlBuilder = null;
            XmlNode objNod = null;
            string sReason = "";
            XmlDocument p_objXmlIn = null;
            XmlDocument p_objXmlOut = null;
            try
            {
                //objResWS = new ReserveWorksheet(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objUser.UserId, base.userLogin.objRiskmasterDatabase.DataSourceName, base.connectionString, base.securityConnectionString, base.DSNID, base.userLogin.objUser.ManagerId);
                p_objXmlIn = new XmlDocument();
                p_objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);
                //rejection comments
                objNod = p_objXmlIn.SelectSingleNode("//AppRejReqCom");
                if (objNod != null)
                    sReason = p_objXmlIn.SelectSingleNode("//AppRejReqCom").InnerText;

                //p_objXmlOut = SubmitWorksheet(p_objXmlIn, base.userID, base.groupID, base.userLogin.objRiskmasterDatabase.DataSourceId, sCallFor, base.userLogin);
                SubmitWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                p_objXmlOut = new XmlDocument();
                p_objXmlOut.LoadXml(p_WorksheetResponse.OutputXmlString);

                //rejection comments
                objNod = p_objXmlOut.SelectSingleNode("//control[@name='hdnAppRejComm']");
                if (objNod != null)
                    objNod.InnerText = sReason;

                p_WorksheetRequest.InputXmlString = p_objXmlOut.InnerXml;

                if (p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                {
                    sCallFor = p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText;
                    if (sCallFor == "Yes")
                        //Changes reqd. PLEASE NOTE
                        //p_objXmlOut = objResWS.ApproveWorksheet(p_objXmlOut, base.userID, base.groupID, base.userLogin, base.userLogin.objRiskmasterDatabase.DataSourceId, "Pending Approval");
                        ApproveReserveWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                }
                p_objXmlOut = null;

                //objResApprove = new ReserveApproval(base.connectionString, base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.m_securityConnectionString, base.DSNID);
                objResApprove = new ReserveApproval(m_CommonInfo, m_iClientId);
                objResApprove.UserId = m_CommonInfo.UserId;
                objResApprove.UserName = m_CommonInfo.UserName;
                p_objXmlOut = objResApprove.ListFundsTransactions(out p_objXmlOut,"");

                sbXmlBuilder = new StringBuilder();
                sbXmlBuilder.Append("<ReserveApproval>");

                sbXmlBuilder.Append("<CallFor>");
                sbXmlBuilder.Append(sCallFor);
                sbXmlBuilder.Append("</CallFor>");

                sbXmlBuilder.Append("<ReserveApprovals>");
                if (p_objXmlOut != null)
                {
                    if (p_objXmlOut.SelectSingleNode("//ReserveApproval/Approvals") != null)
                    {
                        if (p_objXmlOut.SelectSingleNode("//ReserveApproval/Approvals").InnerXml.Length > 0)
                        {
                            sbXmlBuilder.Append(p_objXmlOut.SelectSingleNode("//ReserveApproval").InnerXml);
                        }
                    }
                }
                sbXmlBuilder.Append("</ReserveApprovals>");
                sbXmlBuilder.Append("</ReserveApproval>");

                //p_objXmlOut = null;
                //p_objXmlOut = new XmlDocument();
                //p_objXmlOut.LoadXml(sbXmlBuilder.ToString());
                p_WorksheetResponse.OutputXmlString = sbXmlBuilder.ToString();

                //return true;
            }
            catch (RMAppException p_objException)
            {
                //By Vaibhav for Safeway RSW MITS 13865
                if (p_objException.Message == "Reserves exceed your authority limits. The worksheet is being sent to your supervisor for approval.")
                {
                    //p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Warning);
                    throw new Exception("Reserves exceed your authority limits. The worksheet is being sent to your supervisor for approval.", p_objException);
                    //return true;
                }
                else
                    //p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                    throw p_objException;
                //Vaibhav code end
                //return false;
            }
            catch (Exception p_objException)
            {
                //p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.ApproveReserveWorksheet.Error"), BusinessAdaptorErrorType.Error);
                throw new Exception(Globalization.GetString("ReservesAdaptor.ApproveReserveWorksheet.Error", m_iClientId), p_objException);
                //return false;
            }
            finally
            {
            }
        }

        /// <summary>
        ///  This function is a wrapper over Riskmaster.Application.ReservWorksheet.RejectWorksheet() function.
        ///  This function rejects the Reserve Worksheet.
        ///  It can be only called when a worksheet is pending for approval from Supervisor
        ///  SAFEWAY: 11/07/2008: Vaibhav Mathur
        /// </summary>
        public override void RejectReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
        {
            //ReserveWorksheet objResWS = null;
            ReserveApproval objResApprove = null;
            string sCallFor = "";
            StringBuilder sbXmlBuilder = null;
            XmlNode objNod = null;
            string sReason = "";
            XmlDocument p_objXmlIn = null;
            XmlDocument p_objXmlOut = null;
            try
            {
                //objResWS = new ReserveWorksheet(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objUser.UserId, base.userLogin.objRiskmasterDatabase.DataSourceName, base.connectionString, base.securityConnectionString, base.DSNID, base.userLogin.objUser.ManagerId);
                p_objXmlIn = new XmlDocument();
                p_objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);
                //Mohit Yadav
                //rejection comments
                objNod = p_objXmlIn.SelectSingleNode("//AppRejReqCom");
                if (objNod != null)
                    sReason = p_objXmlIn.SelectSingleNode("//AppRejReqCom").InnerText;

                //p_objXmlOut = objResWS.SubmitWorksheet(p_objXmlIn, base.userID, base.groupID, base.userLogin.objRiskmasterDatabase.DataSourceId, sCallFor, base.userLogin);
                SubmitWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                p_objXmlOut = new XmlDocument();
                p_objXmlOut.LoadXml(p_WorksheetResponse.OutputXmlString);

                //rejection comments
                objNod = p_objXmlOut.SelectSingleNode("//control[@name='hdnAppRejComm']");
                if (objNod != null)
                    objNod.InnerText = sReason;

                p_WorksheetRequest.InputXmlString = p_objXmlOut.InnerXml;

                if (p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                {
                    sCallFor = p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText;
                    if (sCallFor == "Yes")
                        //p_objXmlOut = objResWS.RejectWorksheet(p_objXmlIn, base.userLogin.objRiskmasterDatabase.DataSourceId, base.userID, base.groupID);
                        RejectWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId);
                }
                p_objXmlOut = null;

                //objResApprove = new ReserveApproval(base.connectionString, base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.m_securityConnectionString, base.DSNID);
                objResApprove = new ReserveApproval(m_CommonInfo, m_iClientId);
                objResApprove.UserId = m_CommonInfo.UserId;
                objResApprove.UserName = m_CommonInfo.UserName;
                p_objXmlOut = objResApprove.ListFundsTransactions(out p_objXmlOut,"");

                sbXmlBuilder = new StringBuilder();
                sbXmlBuilder.Append("<ReserveApproval>");

                sbXmlBuilder.Append("<CallFor>");
                sbXmlBuilder.Append(sCallFor);
                sbXmlBuilder.Append("</CallFor>");

                sbXmlBuilder.Append("<ReserveApprovals>");
                if (p_objXmlOut != null)
                {
                    if (p_objXmlOut.SelectSingleNode("//ReserveApproval/Approvals") != null)
                    {
                        if (p_objXmlOut.SelectSingleNode("//ReserveApproval/Approvals").InnerXml.Length > 0)
                        {
                            sbXmlBuilder.Append(p_objXmlOut.SelectSingleNode("//ReserveApproval").InnerXml);
                        }
                    }
                }
                sbXmlBuilder.Append("</ReserveApprovals>");
                sbXmlBuilder.Append("</ReserveApproval>");

                //p_objXmlOut = null;
                //p_objXmlOut = new XmlDocument();
                //p_objXmlOut.LoadXml(sbXmlBuilder.ToString());
                p_WorksheetResponse.OutputXmlString = sbXmlBuilder.ToString();

            }
            catch (RMAppException p_objException)
            {
                //By Vaibhav for Safeway RSW MITS 13865
                if (p_objException.Message == "You do not have the authority to reject this worksheet.")
                {
                    //p_objErrOut.Add(p_objException, "You do not have the authority to reject this worksheet. Please use \"Open Worksheet \" then submit it from the reserve worksheet screen.", BusinessAdaptorErrorType.PopupMessage);
                    throw new Exception("You do not have the authority to reject this worksheet. Please use \"Open Worksheet \" then submit it from the reserve worksheet screen.", p_objException);
                }
                else
                {
                    //p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                    throw p_objException;

                }
                //Vaibhav code end
                //return false;
            }
            catch (Exception p_objException)
            {
                //p_objErrOut.Add(p_objException, Globalization.GetString("ReservesWorksheetAdaptor.RejectReserveWorksheet.Error"), BusinessAdaptorErrorType.Error);
                throw new Exception(Globalization.GetString("ReservesWorksheetAdaptor.RejectReserveWorksheet.Error", m_iClientId), p_objException);
                //return false;
            }
            finally
            {

            }
            //return true;
        }
      
        #region PrivateFunctions

        private void FillNewWorkSheet(ref XmlDocument p_objXML, int p_iClaimId, int p_iClaimantRowId, int p_iUnitRowId, string p_sFromApproved)
        {
            LocalCache objCache = null;
            XmlDocument objApprovedXML = null;
            DbReader objReader = null;
            XmlNode objNode = null;
            string[] arrFields = null;
            XmlNode oCurrentNode = null;
            string sTransType = string.Empty;
            string curReserveBalText = string.Empty;
            double dPaidToDateAmount = 0.0;
            double dReserveAmount = 0.0;
            double dReserveBalance = 0.0;

            try
            {

                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);

                // Get the approved xml in objApprovedXML if p_sFromApproved <> empty
                if (!String.IsNullOrEmpty(p_sFromApproved))
                {
                    string sSQL = "SELECT RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " +
                        p_sFromApproved + " AND RSW_STATUS_CODE = "
                        + objCache.GetCodeId("AP", "RSW_STATUS");
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
                    objApprovedXML = new XmlDocument();

                    if (objReader.Read())
                        objApprovedXML.LoadXml(objReader.GetValue("RSW_XML").ToString());
                    else // In case no xml found, create from scratch
                        p_sFromApproved = "";

                    objReader.Close();
                }

                //Creating new fresh Xml
                p_objXML = CreateNewXml(p_iClaimId, p_iClaimantRowId, p_iUnitRowId);


                // Populating values from an approved reserve worksheet in to fresh xml
                if (!String.IsNullOrEmpty(p_sFromApproved))
                {
                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnCtrlToClear']");
                    arrFields = objNode.InnerText.Split(',');

                    for (int count = 0; count < arrFields.Length; count++)
                    {
                        objNode = p_objXML.SelectSingleNode("//control[@name='" + arrFields[count] + "']");
                        if (objNode != null)
                        {
                            //Raman 10/12/2009 Adding a null check
                            oCurrentNode = objApprovedXML.SelectSingleNode("//control[@name='" + arrFields[count] + "']");
                            if (oCurrentNode != null)
                            {
                                //changes for added New ReserveBalance textbox in transtypes
                                curReserveBalText = oCurrentNode.Attributes["name"].Value;

                                //cloning only Incurred amount data
                                if (curReserveBalText.Contains("_TransType"))
                                {
                                    if (curReserveBalText.Contains("_TransTypeNewReserveBal_"))
                                    {
                                        if (curReserveBalText.Contains("txt_ReserveType_"))
                                        {
                                            string sReserveType = curReserveBalText.Substring("txt_ReservType_".Length); //,curReserveBalText.Substring(curReserveBalText.IndexOf("_TransTypeNewReserveBal_")).Length);

                                            sReserveType = sReserveType.Substring(1, sReserveType.Substring(0, (sReserveType.Length - sReserveType.Substring(sReserveType.IndexOf("_TransTypeNewReserveBal_")).Length - 1)).Length);

                                            sTransType = curReserveBalText.Substring(curReserveBalText.LastIndexOf('_') + 1);

                                            dPaidToDateAmount = Riskmaster.Common.Conversion.ConvertStrToDouble(p_objXML.SelectSingleNode("//control[@name='txt_ReserveType_" + sReserveType + "_Paid_" + sTransType + "']").InnerText);

                                            dReserveAmount = Riskmaster.Common.Conversion.ConvertStrToDouble(objApprovedXML.SelectSingleNode("//control[@name='txt_ReserveType_" + sReserveType + "_TransTypeIncurredAmount_" + sTransType + "']").InnerText);

                                            dReserveBalance = dReserveAmount - dPaidToDateAmount;

                                            objNode.InnerText = ConvertDoubleToCurrencyStr(dReserveBalance);  //string.Format("{0:C}", dReserveBalance);

                                            //also populate hdnField corresponding to NewReserveBal

                                            p_objXML.SelectSingleNode("//control[@name='hdn_ReserveType_" + sReserveType + "_TransTypeNewReserveBal_" + sTransType + "']").InnerText = oCurrentNode.InnerText;
                                        }

                                    }
                                    else
                                    {
                                        objNode.InnerText = oCurrentNode.InnerText;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.FillNewCRW.Err", m_iClientId), ex);
            }
            finally
            {
                objCache = null;
                oCurrentNode = null;
            }

        }

        ///<summary>
        /// This function saves the Reserve Worksheet: a New as well as an existing worksheet
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        /// <returns>XML to be rendered on screen</returns>
        private XmlDocument SaveReserveWorksheet(XmlDocument p_objXMLIn)
        {
            XmlNode objNode = null;
            int iRSWId = 0;
            int iRSWHistId = 0;
            int iStatusCode = 0;
            StringBuilder sbSQL;
            LocalCache objCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            DbConnection objCon = null;
            DbCommand objCmd = null;
            DbParameter objParameter = null;
            long lClaimantRowId = 0;
            long lUnitRowId = 0;
            long lUserMId = 0;
            string sSubmittedTo = string.Empty;
            string sSupName = string.Empty;

            try
            {
                sbSQL = new StringBuilder();
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSubmittedTo']");
                if (objNode != null)
                {
                    sSubmittedTo = objNode.InnerText;
                }

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSubmittedToID']");
                if (objNode != null)
                {
                    lUserMId = Riskmaster.Common.Conversion.ConvertStrToLong(objNode.InnerText);
                }

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='SupName']");
                if (objNode != null)
                {
                    sSupName = objNode.InnerText;
                }

                iRSWId = Conversion.ConvertStrToInteger(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);
                //Mits 19027 shobhana
                objSysSetting = new SysSettings(m_CommonInfo.Connectionstring, m_iClientId);
                bPrevModValtToZero = objSysSetting.PrevResModifyzero;
                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnPrevModValtoZero']");
                if (objNode != null)
                    objNode.InnerText = bPrevModValtToZero.ToString();

                if (iRSWId == -1) // New worksheet
                {
                    #region New Reserve Worksheet

                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    if (objNode != null)
                    {
                        iStatusCode = Riskmaster.Common.Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].InnerText);
                    }

                    iRSWId = Utilities.GetNextUID(m_CommonInfo.Connectionstring, "RSW_WORKSHEETS", base.ClientId);

                    // Set the control 'hdnRSWid' with the new RSW id
                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']");
                    if (objNode != null)
                        objNode.InnerText = iRSWId.ToString();

                    // Insert into database table 'RSW_WORKSHEETS' new record
                    sbSQL.Append("INSERT INTO RSW_WORKSHEETS (RSW_ROW_ID, RSWXMLTYPE,CLAIM_ID, CLAIMANT_ROW_ID,UNIT_ROW_ID,RSW_TYPE_CODE, RSW_STATUS_CODE, SUBMITTED_BY, SUBMITTED_TO, ");
                    sbSQL.Append("ADDED_BY_USER, UPDATED_BY_USER, DTTM_RCD_ADDED, DTTM_RCD_LAST_UPD, APPROVED_BY, DTTM_APPROVED, RSW_XML) VALUES (");
                    sbSQL.Append(iRSWId.ToString() + ", '");
                    sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWXmlType']").InnerText + "', ");
                    sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText + ", ");
                    sbSQL.Append(lClaimantRowId.ToString() + ", ");
                    sbSQL.Append(lUnitRowId.ToString() + ", ");
                    sbSQL.Append(0 + ", ");
                    sbSQL.Append(iStatusCode.ToString() + ", '");
                    sbSQL.Append(m_CommonInfo.UserId.ToString() + "', '");
                    sbSQL.Append(lUserMId.ToString() + "', ");
                    sbSQL.Append(Utilities.FormatSqlFieldValue(p_objXMLIn.SelectSingleNode("//control[@name='hdnCreatedBy']").InnerText) + ", ");
                    sbSQL.Append(Utilities.FormatSqlFieldValue(p_objXMLIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText) + ", '");
                    sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='dtCreated']/@dbformat").InnerText + "', '");
                    sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "', ");
                    sbSQL.Append(Utilities.FormatSqlFieldValue(p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejBy']").InnerText) + ", '");
                    sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdndtAppRej']/@dbformat").InnerText + "', ~XML~)");

                    objCon.Open();
                    objCmd = objCon.CreateCommand();

                    objParameter = objCmd.CreateParameter();
                    objParameter.Value = p_objXMLIn.OuterXml;
                    objParameter.ParameterName = "XML";
                    objCmd.Parameters.Add(objParameter);

                    objCmd.CommandText = sbSQL.ToString();
                    sbSQL.Remove(0, sbSQL.Length);
                    objCmd.ExecuteNonQuery();

                    //For Future Safeway: Adding data into Tables RSWtoTables(p_objXMLIn, objCon, iHisId);

                    #region Reserve Approval History
                    string sReason = "";
                    iRSWHistId = Utilities.GetNextUID(m_CommonInfo.Connectionstring, "RSV_APPROVAL_HIST", base.ClientId);

                    // Insert into database table 'RSV_APPROVAL_HIST' new record
                    sbSQL.Append("INSERT INTO RSV_APPROVAL_HIST (RSW_HIST_ROW_ID, RSW_ROW_ID, RSW_STATUS_CODE, REASON, APPROVER_ID, ");
                    sbSQL.Append("CHANGED_BY_USER, DTTM_APPROVAL_CHGD) VALUES (");
                    sbSQL.Append(iRSWHistId.ToString() + ", ");
                    sbSQL.Append(iRSWId.ToString() + ", ");
                    sbSQL.Append(iStatusCode.ToString() + ", ");
                    sbSQL.Append(Utilities.FormatSqlFieldValue(sReason) + ", ");
                    sbSQL.Append(lUserMId.ToString() + ", ");
                    sbSQL.Append(Utilities.FormatSqlFieldValue(p_objXMLIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText) + ", '");
                    sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "')");

                    objCmd.CommandText = sbSQL.ToString();
                    sbSQL.Remove(0, sbSQL.Length);
                    objCmd.ExecuteNonQuery();
                    #endregion

                    #endregion
                }
                else // existing worksheet
                {
                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    if (objNode != null)
                    {
                        if (objNode.InnerText.IndexOf("Rejected") != -1) // If a 'rejected worksheet is being saved.
                        {
                            #region If a rejected worksheet is being saved

                            iStatusCode = Riskmaster.Common.Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].InnerText);

                            // Update record in database table 'RSW_WORKSHEETS'
                            sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_STATUS_CODE = ");
                            sbSQL.Append(iStatusCode.ToString());
                            sbSQL.Append(", UPDATED_BY_USER = '");
                            sbSQL.Append(m_CommonInfo.UserName);
                            sbSQL.Append("', DTTM_RCD_LAST_UPD = '");
                            sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                            sbSQL.Append("' WHERE RSW_ROW_ID = ");
                            sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

                            objCon.Open();
                            objCmd = objCon.CreateCommand();

                            objCmd.CommandText = sbSQL.ToString();
                            sbSQL.Remove(0, sbSQL.Length);
                            objCmd.ExecuteNonQuery();

                            // Update xml in database table 'RSW_WORKSHEETS'
                            sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_XML = ~XML~ WHERE RSW_ROW_ID = ");
                            sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

                            objParameter = objCmd.CreateParameter();
                            objParameter.Value = p_objXMLIn.OuterXml;
                            objParameter.ParameterName = "XML";
                            objCmd.Parameters.Add(objParameter);

                            objCmd.CommandText = sbSQL.ToString();
                            sbSQL.Remove(0, sbSQL.Length);
                            objCmd.ExecuteNonQuery();

                            //For Future Safeway: Insert a new record in RSW Tables RSWtoTables(p_objXMLIn, objCon, iHisId);

                            #endregion
                        }
                        else // If an existing (non-rejected) worksheet is being saved
                        {
                            #region If an existing (non-rejected) worksheet is being saved
                            iStatusCode = Riskmaster.Common.Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].InnerText);

                            // Update record in database table 'RSW_WORKSHEETS'
                            if (objNode.InnerText != "History")
                            {
                                sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_STATUS_CODE = " + iStatusCode.ToString());
                                sbSQL.Append(", SUBMITTED_TO = '" + lUserMId.ToString() + "'");
                                sbSQL.Append(", UPDATED_BY_USER = '" + m_CommonInfo.UserName);
                                sbSQL.Append("', DTTM_RCD_LAST_UPD = '");
                                sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                sbSQL.Append("', APPROVED_BY = '");
                                sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejBy']").InnerText);
                                sbSQL.Append("', DTTM_APPROVED = '");
                                sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdndtAppRej']/@dbformat").InnerText);
                                sbSQL.Append("' WHERE RSW_ROW_ID = ");
                                sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);
                            }
                            else if (objNode.InnerText == "Approved")
                            {
                                sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_STATUS_CODE = " + iStatusCode.ToString());
                                sbSQL.Append(", UPDATED_BY_USER = '" + m_CommonInfo.UserName);
                                sbSQL.Append("', DTTM_RCD_LAST_UPD = '");
                                sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                sbSQL.Append("', APPROVED_BY = '");
                                sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejBy']").InnerText);
                                sbSQL.Append("', DTTM_APPROVED = '");
                                sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdndtAppRej']/@dbformat").InnerText);
                                sbSQL.Append("' WHERE RSW_ROW_ID = ");
                                sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);
                            }
                            else if (objNode.InnerText == "History")
                            {
                                sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_STATUS_CODE = " + iStatusCode.ToString());
                                sbSQL.Append(", DTTM_RCD_LAST_UPD = '");
                                sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                sbSQL.Append("' WHERE RSW_ROW_ID = ");
                                sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);
                            }

                            objCon.Open();
                            objCmd = objCon.CreateCommand();
                            objCmd.CommandText = sbSQL.ToString();
                            objCmd.ExecuteNonQuery();
                            sbSQL.Remove(0, sbSQL.Length);

                            // Update xml in database table 'RSW_WORKSHEETS'
                            sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_XML = ~XML~ WHERE RSW_ROW_ID = ");
                            sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

                            objParameter = objCmd.CreateParameter();
                            objParameter.Value = p_objXMLIn.OuterXml;
                            objParameter.ParameterName = "XML";
                            objCmd.Parameters.Add(objParameter);

                            objCmd.CommandText = sbSQL.ToString();
                            objCmd.ExecuteNonQuery();
                            sbSQL.Remove(0, sbSQL.Length);

                            //For Future Safeway: Update record in database table RSWtoTables

                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveReserveWorksheet.Err", m_iClientId), ex);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
            }
            return p_objXMLIn;
        }
    
        ///<summary>
        /// This function saves the Reserve Worksheet History of Approvals: a New as well as an existing worksheet
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        private void SaveReserveWorksheetHistory(XmlDocument p_objXMLIn)
        {
            XmlNode objNode = null;
            int iRSWId = 0;
            int iRSWHistId = 0;
            int iStatusCode = 0;
            int iLastStatusCode = 0;
            StringBuilder sbSQL;
            LocalCache objCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            DbConnection objCon = null;
            DbCommand objCmd = null;

            long lUserMId = 0;
            int iApproverID = 0;
            string sSubmittedTo = string.Empty;
            string sSupName = string.Empty;
            string sReason = "";

            try
            {
                sbSQL = new StringBuilder();
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSubmittedTo']");
                if (objNode != null)
                {
                    sSubmittedTo = objNode.InnerText;
                }

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSubmittedToID']");
                if (objNode != null)
                {
                    lUserMId = Riskmaster.Common.Conversion.ConvertStrToLong(objNode.InnerText);
                }

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSupName']");
                if (objNode != null)
                {
                    sSupName = objNode.InnerText;
                }

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejComm']");
                if (objNode != null)
                {
                    sReason = objNode.InnerText;
                }

                iRSWId = Conversion.ConvertStrToInteger(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

                if (iRSWId > 0)
                {
                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    objCon.Open();
                    iStatusCode = Riskmaster.Common.Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].InnerText);

                    sbSQL.Append("SELECT APPROVER_ID FROM RSV_APPROVAL_HIST WHERE RSW_ROW_ID = " + iRSWId);
                    sbSQL.Append(" AND RSW_HIST_ROW_ID IN (SELECT MAX(RSW_HIST_ROW_ID) FROM RSV_APPROVAL_HIST");
                    sbSQL.Append(" WHERE RSW_ROW_ID = " + iRSWId + ")");
                    iApproverID = Conversion.ConvertObjToInt(objCon.ExecuteScalar(sbSQL.ToString()), base.ClientId);
                    sbSQL.Remove(0, sbSQL.Length);

                    sbSQL.Append("SELECT RSW_STATUS_CODE FROM RSV_APPROVAL_HIST WHERE RSW_ROW_ID = " + iRSWId);
                    sbSQL.Append(" AND RSW_HIST_ROW_ID IN (SELECT MAX(RSW_HIST_ROW_ID) FROM RSV_APPROVAL_HIST");
                    sbSQL.Append(" WHERE RSW_ROW_ID = " + iRSWId + ")");
                    iLastStatusCode = Conversion.ConvertObjToInt(objCon.ExecuteScalar(sbSQL.ToString()), base.ClientId);
                    sbSQL.Remove(0, sbSQL.Length);

                    if ((iApproverID != lUserMId) || (iLastStatusCode != iStatusCode))
                    {
                        objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                        if (objNode != null)
                        {
                            // Update record in database table 'RSW_WORKSHEETS'
                            if (objNode.InnerText != "Pending")
                            {
                                iRSWHistId = Utilities.GetNextUID(m_CommonInfo.Connectionstring, "RSV_APPROVAL_HIST", base.ClientId);

                                // Insert into database table 'RSV_APPROVAL_HIST' new record
                                sbSQL.Append("INSERT INTO RSV_APPROVAL_HIST (RSW_HIST_ROW_ID, RSW_ROW_ID, RSW_STATUS_CODE, REASON, APPROVER_ID, ");
                                sbSQL.Append("CHANGED_BY_USER, DTTM_APPROVAL_CHGD) VALUES (");
                                sbSQL.Append(iRSWHistId.ToString() + ", ");
                                sbSQL.Append(iRSWId.ToString() + ", ");
                                sbSQL.Append(iStatusCode.ToString() + ", ");
                                sbSQL.Append(Utilities.FormatSqlFieldValue(sReason) + ", ");
                                sbSQL.Append(lUserMId.ToString() + ", ");
                                sbSQL.Append(Utilities.FormatSqlFieldValue(m_CommonInfo.UserName) + ", '");
                                sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "')");

                                objCmd = objCon.CreateCommand();
                                objCmd.CommandText = sbSQL.ToString();
                                sbSQL.Remove(0, sbSQL.Length);
                                objCmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveReserveWorksheetHistory.Err", m_iClientId), ex);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
            }
        }

        #region Functions to Create new dynamic Xml

        private XmlDocument CreateNewXml(int p_iClaimdId, int p_iClaimantRowId, int p_iUnitRowId)
        {
            ReserveFunds.structReserves[] arrReserves = null;
            XmlDocument workSheetDoc = null;
            XmlElement objClaimInfoNode = null;
            XmlElement objFormElement = null;
            XmlElement objTempElement = null;
            XmlElement objReserveSummaryNode = null;
            StringBuilder strControlToEditList = null;
            StringBuilder strControlToClearList = null;
            StringBuilder reserveTypesList = null;
            bool reasonCommentsRequired = true;
            StringBuilder strSql = null;
            DbReader objReader = null;

            try
            {
                workSheetDoc = new XmlDocument();

                objFormElement = workSheetDoc.CreateElement("form");

                //Adding claim info node to form node
                objClaimInfoNode = GetClaimInfoNode(workSheetDoc, p_iClaimdId);

                XmlElement objClaimInfoElement = workSheetDoc.CreateElement("commonsection");
                objClaimInfoElement.SetAttribute("name", "ClaimInfo");
                objClaimInfoElement.SetAttribute("title", "CLAIM INFORMATION");
                CreatingClaimInfoControls(workSheetDoc, objClaimInfoNode, ref objClaimInfoElement, p_iClaimdId);

                objFormElement.AppendChild(objClaimInfoElement);

                //Gettting Reserve Types
                arrReserves = GetRequiredReserveTypeArray(p_iClaimdId);

                strControlToEditList = new StringBuilder();
                strControlToClearList = new StringBuilder();
                reserveTypesList = new StringBuilder();

                //Reading Utility Settings for LOB Reserve Options
                GetLOBReserveOptions(p_iClaimdId);

                //nnorouzi; MITS 18989; See if Reason and Comments are required
                strSql = new StringBuilder();
                strSql.Append(string.Format("SELECT {0} FROM {1} WHERE {2}", "REASON_COMMENTS_RSV_WK", "SYS_PARMS_LOB, CLAIM", "SYS_PARMS_LOB.LINE_OF_BUS_CODE = CLAIM.LINE_OF_BUS_CODE AND CLAIM.CLAIM_ID = " + p_iClaimdId));
                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, strSql.ToString());
                if (objReader != null && objReader.Read())
                {
                    reasonCommentsRequired = Conversion.ConvertObjToBool(objReader[0], base.ClientId);
                }
                else
                {
                    reasonCommentsRequired = true;
                }


                for (int iCnt = 0; iCnt < arrReserves.GetLength(0); iCnt++)
                {
                    //GetReserveType Node and append the same with formNode
                    objTempElement = GetReserveTypeNode(workSheetDoc, objClaimInfoNode, arrReserves[iCnt].iReserveTypeCode, arrReserves[iCnt].sReserveDesc, p_iClaimdId, p_iClaimantRowId, p_iUnitRowId, ref strControlToEditList, ref strControlToClearList, reasonCommentsRequired);

                    if (objTempElement != null)
                    {
                        if (iCnt == 0)
                        {
                            objTempElement.SetAttribute("selected", "1");
                        }

                        reserveTypesList.Append(objTempElement.GetAttribute("name").ToString() + ",");
                        objFormElement.AppendChild(objTempElement);
                    }

                    objTempElement = null;
                }

                objReserveSummaryNode = GetReserveSummaryNode(workSheetDoc, objClaimInfoNode, p_iClaimdId, p_iClaimantRowId, p_iUnitRowId, ref strControlToClearList, ref strControlToEditList, reasonCommentsRequired);

                //By Nitin,if No ReserveType has been created for a Claim then ReserveSummary should be come as selected
                if (reserveTypesList.ToString().Length == 0)
                {
                    objReserveSummaryNode.SetAttribute("selected", "1");
                }
                objFormElement.AppendChild(objReserveSummaryNode);

                //populating the hidden field values
                objFormElement.SelectSingleNode("//control[@name='hdnCtrlToClear']").InnerText = strControlToClearList.ToString();
                objFormElement.SelectSingleNode("//control[@name='hdnCtrlToEdit']").InnerText = strControlToEditList.ToString();
                objFormElement.SelectSingleNode("//control[@name='hdnReserveTypeTabs']").InnerText = reserveTypesList.ToString();
                objFormElement.SelectSingleNode("//control[@name='SupName']").InnerText = objClaimInfoNode.SelectSingleNode("//control[@name='txt_ClaimInfo_SupervisorName']").Attributes["title"].Value;
                objFormElement.SelectSingleNode("//control[@name='hdnPCName']").InnerText = objClaimInfoNode.SelectSingleNode("//control[@name='txt_ClaimInfo_PCName']").Attributes["title"].Value;
                objFormElement.SelectSingleNode("//control[@name='hdnClaimNumber']").InnerText = objClaimInfoNode.SelectSingleNode("//control[@name='txt_ClaimInfo_ClaimNumber']").Attributes["title"].Value;

                workSheetDoc.AppendChild(objFormElement);
                return workSheetDoc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private XmlElement GetClaimInfoNode(XmlDocument workSheetDoc, int p_iClaimId)
        {
            Claim objClaim = null;
            Claimant objPrimaryClaimant = null;
            ClaimAdjuster objCurAdj = null;
            Event objEvent = null;
            DataModelFactory objDataModelFactory = null;
            XmlElement objClaimInfoElement = null;
            XmlElement objDisplayColElement = null;
            XmlElement objControlElement = null;
            string lblCssClass = "half123";
            string txtCssClass = "half";
            string strTitle = string.Empty;
            string strRef = string.Empty;
            string strName = string.Empty;
            string strType = string.Empty;
            string strReadOnly = string.Empty;
            string strPaidAmnt = string.Empty;
            string strOutStanding = string.Empty;
            string strTotalIncured = string.Empty;
            string sSQL = string.Empty;

            try
            {
                objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);
                objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimId);
                objPrimaryClaimant = objClaim.PrimaryClaimant;
                objCurAdj = objClaim.CurrentAdjuster;
                objEvent = (objClaim.Parent as Event);


                objClaimInfoElement = GetNewGroupElementForWorkSheet(workSheetDoc, "ClaimInfo", 0, "CLAIM INFORMATION");

                //claim information data
                //line break
                objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, "ClaimInfo_linebreak1");
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                //for claim number
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                strTitle = "Claim Number :";
                strName = "lbl_ClaimInfo_ClaimNumber";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                strTitle = objClaim.ClaimNumber;
                strName = "txt_ClaimInfo_ClaimNumber";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                strTitle = "Claimant :";
                strName = "lbl_ClaimInfo_ClaimantName";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                if (objPrimaryClaimant.ClaimantEntity != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        strTitle = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        strTitle = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }
                else
                {
                    strTitle = "";
                }

                strName = "txt_ClaimInfo_ClaimantName";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                //date of Event
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                strTitle = "Date of Event :";
                strName = "lbl_ClaimInfo_DateOfEvent";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                strTitle = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d").ToString();
                strName = "txt_ClaimInfo_DateOfEvent";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                //date of Claim
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                strTitle = "Date of Claim :";
                strName = "lbl_ClaimInfo_DateOfClaim";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;


                strTitle = Conversion.GetDBDateFormat(objClaim.DateOfClaim, "d").ToString();
                strName = "txt_ClaimInfo_DateOfClaim";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;


                //new display column for Supervisor Name
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

                strTitle = "Supervisor :";
                strName = "lbl_ClaimInfo_SupervisorName";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                strTitle = GetSupervisorName();
                strName = "txt_ClaimInfo_SupervisorName";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;


                //new display column for Primary Claimant
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

                strTitle = "Primary Claimant :";
                strName = "lbl_ClaimInfo_PCName";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;


                //Primary Claimant textbox
                if (objPrimaryClaimant.ClaimantEntity != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        strTitle = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        strTitle = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }
                else
                {
                    strTitle = "";
                }

                strName = "txt_ClaimInfo_PCName";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                //Adjuster
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                strTitle = "Adjuster :";
                strName = "lbl_ClaimInfo_Adjuster";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;


                //Adjuster Name
                if (objCurAdj.AdjusterEntity != null)
                {
                    if (objCurAdj.AdjusterEntity.LastName != "" && objCurAdj.AdjusterEntity.FirstName != "")
                        strTitle = objCurAdj.AdjusterEntity.LastName + ", " + objCurAdj.AdjusterEntity.FirstName;
                    else
                        strTitle = objCurAdj.AdjusterEntity.LastName;
                }
                else
                {
                    strTitle = "";
                }

                strName = "txt_ClaimInfo_Adjuster";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, "ClaimInfo_linebreak2");
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                return objClaimInfoElement;
            }
            catch (Exception ex)
            {
                throw new RMAppException("Error occured while getting claim information", ex.InnerException);
            }
            finally
            {
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
                if (objClaim != null)
                    objClaim.Dispose();
                if (objPrimaryClaimant != null)
                    objPrimaryClaimant.Dispose();
                if (objEvent != null)
                    objEvent.Dispose();
            }
        }

        private XmlElement GetReserveSummaryNode(XmlDocument workSheetDoc, XmlElement claimInfoElement, int p_iClaimId, int p_iClaimantRowId, int p_iUnitRowId, ref StringBuilder strControlToClear, ref StringBuilder strControlToEdit, bool reasonCommentsRequired)
        {
            XmlElement objReserveSummaryNode = null;
            XmlElement objDisplayColElement = null;
            XmlElement objControlElement = null;
            string lblCssClass = "half123";
            string strTitle = string.Empty;
            string strRef = string.Empty;
            string strName = string.Empty;
            string strType = string.Empty;
            string strReadOnly = string.Empty;
            string strPaidAmnt = string.Empty;
            string strOutStanding = string.Empty;
            string strTotalIncured = string.Empty;
            string strSQL = string.Empty;
            DbReader objReader = null;
            StringBuilder objSql = null;


            objReserveSummaryNode = GetNewGroupElementForWorkSheet(workSheetDoc, "RESERVESUMMARY", 0, "RESERVE SUMMARY");
            objReserveSummaryNode.SetAttribute("onclick", "CreateReserveSummary()");

            //getting all hiddend fileds information and appending the same to Reserve Summary Node
            objDisplayColElement = GetHiddenFieldsNode(workSheetDoc, p_iClaimId, p_iClaimantRowId, p_iUnitRowId);
            objReserveSummaryNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //Creating a blank line
            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, objReserveSummaryNode.GetAttribute("name").ToString() + "_linebreak1");
            objReserveSummaryNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //Creating Static Claim Information Fields and appending the same to ReserveSummary node
            //CreatingClaimInfoControls(workSheetDoc, claimInfoElement, ref objReserveSummaryNode,p_iClaimId);

            //Creating a blank line
            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, objReserveSummaryNode.GetAttribute("name").ToString() + "_linebreak2");
            objReserveSummaryNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //Creating blank div , that will store dynamically created Table for Summary
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
            strTitle = "";
            strName = "divReserveSummary";
            strRef = "//control[@name='" + strName + "']";
            strType = "divcontainer";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            objReserveSummaryNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //creating Comments and Reason controls

            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, objReserveSummaryNode.GetAttribute("name").ToString() + "_linebreak3");
            objReserveSummaryNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //new display column for Comments
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "Comments   ";
            strName = "lbl_" + objReserveSummaryNode.GetAttribute("name").ToString() + "_Comments";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            lblCssClass = "ReserveSummaryComments";
            if (reasonCommentsRequired) //nnorouzi; MITS 18989
            {
                lblCssClass += " required";
            }
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + objReserveSummaryNode.GetAttribute("name").ToString() + "_Comments";
            strControlToClear.Append(strName + ",");
            strControlToEdit.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "memo";
            strReadOnly = "false";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("cols", "84");
            objControlElement.SetAttribute("rows", "6");
            objControlElement.SetAttribute("required", "yes");
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "hdn_" + objReserveSummaryNode.GetAttribute("name").ToString() + "_Comments";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = "";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            //nnorouzi; MITS 18989 start
            strTitle = "";
            strName = "hdn_ReasonCommentsRequired";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = reasonCommentsRequired.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            //nnorouzi; MITS 18989 end

            objReserveSummaryNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            return objReserveSummaryNode;
        }

        private XmlElement GetHiddenFieldsNode(XmlDocument workSheetDoc, int p_iClaimId, int p_iClaimantRowId, int p_iUnitRowId)
        {
            XmlElement objDisplayColElement = null;
            XmlElement objControlElement = null;
            string lblCssClass = "half123";
            string strTitle = string.Empty;
            string strRef = string.Empty;
            string strName = string.Empty;
            string strType = string.Empty;
            string strReadOnly = string.Empty;
            string strPaidAmnt = string.Empty;
            string strOutStanding = string.Empty;
            string strTotalIncured = string.Empty;
            string strSQL = string.Empty;


            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "";
            strName = "hdnCtrlToClear";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //control to edit hidden field
            strTitle = "";
            strName = "hdnCtrlToEdit";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control having list of group name
            strTitle = "";
            strName = "hdnReserveTypeTabs";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control RSWid
            strTitle = "";
            strName = "hdnRSWid";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = "-1";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnSubmittedToID
            strTitle = "";
            strName = "hdnSubmittedToID";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnSubmittedTo
            strTitle = "";
            strName = "hdnSubmittedTo";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnRSWStatus
            strTitle = "";
            strName = "hdnRSWStatus";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("codeid", "0");
            objControlElement.InnerText = "New";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnAppRejBy
            strTitle = "";
            strName = "hdnAppRejBy";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdndtAppRej
            strTitle = "";
            strName = "hdndtAppRej";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("dbformat", "");
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdncreatedBy
            strTitle = "";
            strName = "hdnCreatedBy";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = m_CommonInfo.UserId.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdndtLastMod
            strTitle = "";
            strName = "hdndtLastMod";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = DateTime.Now.ToString();
            objControlElement.SetAttribute("dbformat", Conversion.GetDateTime(DateTime.Now.ToString()));
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnLastModBy
            strTitle = "";
            strName = "hdnLastModBy";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = m_CommonInfo.UserName;
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control dtCreated
            strTitle = "";
            strName = "dtCreated";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = DateTime.Now.ToString();
            objControlElement.SetAttribute("dbformat", Conversion.GetDateTime(DateTime.Now.ToString()));
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control CLAIMiD
            strTitle = "";
            strName = "hdnClaimId";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = p_iClaimId.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control Claim Number
            strTitle = "";
            strName = "hdnClaimNumber";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control UnitID
            strTitle = "";
            //MGaba2:Changing the names from hdnUnitId/hdnClaimantEId to hdnUnitRowId/hdnClaimantRowId
            strName = "hdnUnitRowId";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = p_iUnitRowId.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control ClaimantEId
            strTitle = "";
            strName = "hdnClaimantRowId";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = p_iClaimantRowId.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control PrimaryClaimant
            strTitle = "";
            strName = "hdnPCName";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;


            //hidden control for hdnRSWType
            strTitle = "";
            strName = "hdnRSWType";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = "0";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control for hdnMode
            strTitle = "";
            strName = "hdnMode";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control for hdnEditRejected
            strTitle = "";
            strName = "hdnEditRejected";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control for hdnUnderLimit
            strTitle = "";
            strName = "hdnUnderLimit";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnSMSCheckToUpdate
            strTitle = "";
            strName = "hdnSMSCheckToUpdate";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control for SupervisorName
            strTitle = "";
            strName = "hdnSupName";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnAppRejComm for reason to be populated from UI
            strTitle = "";
            strName = "hdnAppRejComm";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnAppRejComm for reason to be populated from UI
            strTitle = "";
            strName = "hdnReqStatus";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("codeid", "0");
            objControlElement.InnerText = "New";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control SupervisorName
            strTitle = "";
            strName = "SupName";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control CurrentUser
            strTitle = "";
            strName = "hdnCurrentUser";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = m_CommonInfo.UserId.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;


            //hidden control hdnCollInRsvBal
            strTitle = "";
            strName = "hdnCollInRsvBal";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = m_bCollInResBal.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnCollInIncurredBal
            strTitle = "";
            strName = "hdnCollInIncurredBal";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = m_bCollInIncBal.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnPrevResBelowPaid
            strTitle = "";
            strName = "hdnPrevResBelowPaid";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = m_bPrevResBelowPaid.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            return objDisplayColElement;
        }

        private ReserveFunds.structReserves[] GetRequiredReserveTypeArray(int p_iClaimdId)
        {
            StringBuilder sbSQL = null;
            DbReader objReaderLOB = null;
            int iLOBCode = 0;
            int iClmTypeCode = 0;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimdId);
                objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReaderLOB.Read())
                {
                    iLOBCode = objReaderLOB.GetInt32("LINE_OF_BUS_CODE");
                    iClmTypeCode = objReaderLOB.GetInt32("CLAIM_TYPE_CODE");
                }
                objReaderLOB.Close();
                sbSQL.Remove(0, sbSQL.Length);

                return GetResCategories(iLOBCode, iClmTypeCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CreatingClaimInfoControls(XmlDocument workSheetDoc, XmlElement claimInfoElement, ref XmlElement claimInfoParentNode, int p_iClaimId)
        {
            XmlElement objDisplayColElement = null;
            XmlElement objControlElement = null;
            string lblCssClass = "oneThirdLabel";
            string txtCssClass = "oneThirdLabel";
            string strTitle = string.Empty;
            string strRef = string.Empty;
            string strName = string.Empty;
            string strType = string.Empty;
            string strReadOnly = string.Empty;
            string strPaidAmnt = string.Empty;
            string strOutStanding = string.Empty;
            string strTotalIncured = string.Empty;
            string strSQL = string.Empty;


            //new display column
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='lbl_ClaimInfo_ClaimNumber']").Attributes["title"].Value;
            strName = "lbl_" + claimInfoParentNode.GetAttribute("name").ToString() + "_ClaimNumber";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='txt_ClaimInfo_ClaimNumber']").Attributes["title"].Value;
            strName = "txt_" + claimInfoParentNode.GetAttribute("name").ToString() + "_ClaimNumber";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            //claimInfoParentNode.AppendChild(objDisplayColElement);
            //objDisplayColElement = null;



            //new display column
            //objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='lbl_ClaimInfo_ClaimantName']").Attributes["title"].Value;
            strName = "lbl_" + claimInfoParentNode.GetAttribute("name").ToString() + "_ClaimantName";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='txt_ClaimInfo_ClaimantName']").Attributes["title"].Value;
            strName = "txt_" + claimInfoParentNode.GetAttribute("name").ToString() + "_ClaimantName";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            claimInfoParentNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;


            //new display column for claim date
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='lbl_ClaimInfo_DateOfClaim']").Attributes["title"].Value;
            strName = "lbl_" + claimInfoParentNode.GetAttribute("name").ToString() + "_DateOfClaim";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='txt_ClaimInfo_DateOfClaim']").Attributes["title"].Value;
            strName = "txt_" + claimInfoParentNode.GetAttribute("name").ToString() + "_DateOfClaim";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='lbl_ClaimInfo_DateOfEvent']").Attributes["title"].Value;
            strName = "lbl_" + claimInfoParentNode.GetAttribute("name").ToString() + "_DateOfEvent";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='txt_ClaimInfo_DateOfEvent']").Attributes["title"].Value;
            strName = "txt_" + claimInfoParentNode.GetAttribute("name").ToString() + "_DateOfEvent";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            claimInfoParentNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;


            //new display column for Adjuster Name
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='lbl_ClaimInfo_Adjuster']").Attributes["title"].Value;
            strName = "lbl_" + claimInfoParentNode.GetAttribute("name").ToString() + "_Adjuster";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='txt_ClaimInfo_Adjuster']").Attributes["title"].Value;
            strName = "txt_" + claimInfoParentNode.GetAttribute("name").ToString() + "_Adjuster";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "Supervisor :";
            strName = "lbl_" + claimInfoParentNode.GetAttribute("name").ToString() + "_SupervisorName";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='txt_ClaimInfo_SupervisorName']").Attributes["title"].Value;
            strName = "txt_" + claimInfoParentNode.GetAttribute("name").ToString() + "_SupervisorName";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            claimInfoParentNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;
        }

        private XmlElement GetReserveTypeNode(XmlDocument workSheetDoc, XmlElement claimInfoElement, int p_iReserveTypeCode, string p_sReserveTypeText, int p_iClaimId, int p_iClaimantRowId, int p_iUnitRowId, ref StringBuilder strControlToEditList, ref StringBuilder strControlToClear, bool reasonCommentsRequired)
        {
            DbReader objReaderLOB = null;
            DbReader objReader = null;
            XmlElement reserveTypeElement = null;
            XmlElement objDisplayColElement = null;
            XmlElement objControlElement = null;
            string lblCssClass = string.Empty;
            string threeCntrlClass = string.Empty;
            string strTitle = string.Empty;
            string strRef = string.Empty;
            string strName = string.Empty;
            string strType = string.Empty;
            string strReadOnly = string.Empty;
            string strPaidAmnt = string.Empty;
            string strOutStanding = string.Empty;
            string strTotalIncured = string.Empty;
            string strSQL = string.Empty;
            double dRes, dIncurred, dColl, dPaid, dBalAmt, dTransPaidAmt, dTransIncurredAmt, dTransNewReserveBal;

            //Creating a new node as ReserveType
            reserveTypeElement = GetNewGroupElementForWorkSheet(workSheetDoc, "ReserveType_" + p_iReserveTypeCode, 0, p_sReserveTypeText);


            //Creating and fetching Claim information for this node
            //CreatingClaimInfoControls(workSheetDoc, claimInfoElement, ref reserveTypeElement, p_iClaimId);

            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, reserveTypeElement.GetAttribute("name").ToString() + "_linebreak1");
            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //new display column for paid
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            if (m_bCollInResBal)
                strTitle = "Current Paid to Date (-collections)  ";
            else
                strTitle = "Current Paid to Date  ";

            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_Paid";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "twoThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //calculating paid , incurred and outstanding amount 
            dRes = 0.0;
            dIncurred = 0.0;
            dColl = 0.0;
            dPaid = 0.0;
            dBalAmt = 0.0;
            strSQL = string.Empty;

            strSQL = GetQueryToReserveAmounts(p_iReserveTypeCode, p_iClaimId, p_iClaimantRowId, p_iUnitRowId);

            objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, strSQL);
            if (objReader.Read())
            {
                dRes = objReader.GetDouble("RESERVE_AMOUNT");
                dBalAmt = objReader.GetDouble("BALANCE_AMOUNT");
                dPaid = objReader.GetDouble("PAID_TOTAL");
                //dIncurred = (objReader.GetDouble("PAID_BALANCE") > dRes)? objReader.GetDouble("PAID_BALANCE") : dRes;
                dIncurred = objReader.GetDouble("INCURRED_AMOUNT");
                dColl = objReader.GetDouble("COLLECTION_TOTAL");
            }
            objReader.Close();

            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
            //dBalAmt = CalculateReserveBalance(dRes, dPaid, dColl, m_bCollInResBal);
            dBalAmt = CalculateReserveBalance(dRes, dPaid, dColl, m_bCollInResBal , p_iReserveTypeCode);
           
            //getting paid after calculating from collections
            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
            if (!IsRecoveryReserve(p_iReserveTypeCode))
            {
                dPaid = CalculatePaidAmount(dPaid, dColl, m_bCollInResBal);
                dIncurred = CalculateIncurred(dBalAmt, dPaid, dColl, m_bCollInResBal, m_bCollInIncBal, p_iReserveTypeCode);
            }
            else
            {
                dIncurred = CalculateIncurred(dBalAmt, dPaid, dColl, false, false, p_iReserveTypeCode);
            }

          

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_Paid";
            strRef = "//control[@name='" + strName + "']";
            strType = "currency";
            strReadOnly = "true";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = ConvertDoubleToCurrencyStr(dPaid);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //new display column for Outstanding
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "Current Reserve Balance ";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_Outstanding";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "twoThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_Outstanding";
            strRef = "//control[@name='" + strName + "']";
            strType = "currency";
            strReadOnly = "true";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = ConvertDoubleToCurrencyStr(dBalAmt);  //string.Format("{0:C}", dBalAmt);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //new display column for TotalIncurred
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "Current Total Reserve ";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TotalIncurred";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "twoThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_TotalIncurred";
            strRef = "//control[@name='" + strName + "']";
            strType = "currency";
            strReadOnly = "true";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = ConvertDoubleToCurrencyStr(dRes);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //adding hidden field hdnColl_ReserveType for storing collection for this reservetype ,to be used by javascript at UI level
            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_Collection";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = dColl.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //adding hidden field for ReserveAmt
            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_ReserveAmt";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //adding hidden field hdnReserveChange_ReserveType for storing Reserve amount change for this reservetype ,to be used by javascript at UI level
            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_ReserveChange";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, reserveTypeElement.GetAttribute("name").ToString() + "_linebreak2");
            reserveTypeElement.AppendChild(objDisplayColElement);

            //adding display column for Transaction Type label

            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "Transaction Type";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_ReserveCatagory";
            strRef = "//control[@name='" + strName + "']";
            //MGaba2:R6:Changed temporarily
            //strType = "labelonly";
            strType = "message";
            strReadOnly = "true";
            threeCntrlClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            if (m_bCollInResBal)
                strTitle = "Paid to Date(-collections)";
            else
                strTitle = "Paid to Date";

            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypePaid";
            strRef = "//control[@name='" + strName + "']";
            threeCntrlClass = "oneThird";
            //MGaba2:R6:Changed temporarily
            //strType = "labelonly";
            strType = "message";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "New Reserve Balance";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeNewReserveBal";
            strRef = "//control[@name='" + strName + "']";
            //MGaba2:R6:Changed temporarily
            //strType = "labelonly";
            threeCntrlClass = "oneThird";
            strType = "message";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "New Reserve Amount";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeTotalIncurrend";
            strRef = "";// "//control[@name='" + strName + "']";
            //MGaba2:R6:Changed temporarily
            // strType = "labelonly";
            strType = "message";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //Addding column for Each Transction Type under current Reserve Type
            //now fetching transaction type for each request type code  and creating xml nodes for this
            strSQL = "SELECT CODE_DESC TransDesc,CODES.CODE_ID TransId from CODES,CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.RELATED_CODE_ID =" + p_iReserveTypeCode;
            objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, strSQL);

            while (objReaderLOB.Read())
            {
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                strTitle = objReaderLOB.GetValue("TransDesc").ToString();
                strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransType_" + objReaderLOB.GetInt32("TransId").ToString();
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                //objControlElement.SetAttribute("width", "56px");
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                //setting paid amount in text box
                strTitle = "";
                strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_Paid_" + objReaderLOB.GetInt32("TransId").ToString();
                strRef = "//control[@name='" + strName + "']";
                strType = "currency";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                //objControlElement.SetAttribute("width", "56px");
                //claculate paid for trans type
                dTransPaidAmt = 0.0;
                dTransPaidAmt = GetPaidAmountForTransType(objReaderLOB.GetInt32("TransId"), p_iReserveTypeCode, p_iClaimId, p_iClaimantRowId, p_iUnitRowId, m_bCollInResBal);
                objControlElement.InnerText = ConvertDoubleToCurrencyStr(dTransPaidAmt);  //string.Format("{0:C}", dTransPaidAmt); 
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                //New Reserve Balance textbox
                strTitle = "";
                strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeNewReserveBal_" + objReaderLOB.GetInt32("TransId").ToString();
                strControlToClear.Append(strName + ",");
                strControlToEditList.Append(strName + ",");
                strRef = "//control[@name='" + strName + "']";
                strType = "currency";
                strReadOnly = "false";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                objControlElement.SetAttribute("onblur", "TransTypeNewReserveBalance_OnBlur('" + reserveTypeElement.GetAttribute("name").ToString() + "','" + objReaderLOB.GetInt32("TransId").ToString() + "')");
                objControlElement.InnerText = ConvertDoubleToCurrencyStr(0.0); // string.Format("{0:C}", 0.0); 
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                //adding hidden field for storing new ReserveBal amount
                strTitle = "";
                strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeNewReserveBal_" + objReaderLOB.GetInt32("TransId").ToString();
                strControlToClear.Append(strName + ",");
                strRef = "//control[@name='" + strName + "']";
                strType = "hidden";
                strReadOnly = "";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objControlElement.InnerText = ConvertDoubleToCurrencyStr(0.0); //string.Format("{0:C}", 0.0); 
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                //creating TotalInccured Text
                strTitle = "";
                strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeIncurredAmount_" + objReaderLOB.GetInt32("TransId").ToString();
                strControlToEditList.Append(strName + ",");
                strControlToClear.Append(strName + ",");

                strRef = "//control[@name='" + strName + "']";
                strType = "currency";
                strReadOnly = "false";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                objControlElement.SetAttribute("onblur", "CalculateReserveAmount_OnBlur('" + reserveTypeElement.GetAttribute("name").ToString() + "','" + objReaderLOB.GetInt32("TransId").ToString() + "')");

                dTransIncurredAmt = 0.0;
                dTransIncurredAmt = dTransPaidAmt;
                objControlElement.InnerText = ConvertDoubleToCurrencyStr(dTransPaidAmt); //string.Format("{0:C}", dTransPaidAmt); 
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                //adding hidden field for storing TotalInccured Text
                strTitle = "";
                strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeIncurredAmount_" + objReaderLOB.GetInt32("TransId").ToString();
                strControlToClear.Append(strName + ",");
                strRef = "//control[@name='" + strName + "']";
                strType = "hidden";
                strReadOnly = "";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objControlElement.InnerText = ConvertDoubleToCurrencyStr(dIncurred); //string.Format("{0:C}", dIncurred);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                reserveTypeElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;
            }

            objReaderLOB.Close();

            //Adding New Other Trans field in order to round off the data starts
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
            strTitle = "Other Adjustments";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransType_OtherAdjustment";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            lblCssClass = "twoThird";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //creating TotalInccured for Other trans type
            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeIncurredAmount_OtherAdjustment";
            strControlToEditList.Append(strName + ",");
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "currency";
            strReadOnly = "false";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("onblur", "CalculateReserveAmount_OnBlur('" + reserveTypeElement.GetAttribute("name").ToString() + "')");
            objControlElement.InnerText = ConvertDoubleToCurrencyStr(dBalAmt);  //string.Format("{0:C}", dBalAmt);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //Added by Nitin for Mits 18989
            //adding hidden field for storing Other Field 
            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeIncurredAmount_OtherAdjustment";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = ConvertDoubleToCurrencyStr(dBalAmt);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //Adding of new Misc field Ends ...........

            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, reserveTypeElement.GetAttribute("name").ToString() + "_linebreak3");
            reserveTypeElement.AppendChild(objDisplayColElement);

            //new display column for New Total Inccured
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            //adding hidden field for storing new inccured amount
            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWTOTALINCURRED";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = ConvertDoubleToCurrencyStr(dRes); //string.Format("{0:C}", dIncurred);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;


            strTitle = "New Total Reserve  ";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWTOTALINCURRED";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "twoThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWTOTALINCURRED";
            strControlToClear.Append(strName + ",");
            //to populate value in this textbox from respective hidden field
            //strRef = "//control[@name='" + "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWTOTALINCURRED" + "']";
            strRef = "//control[@name='" + strName + "']";
            strType = "currency";
            strReadOnly = "true";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = ConvertDoubleToCurrencyStr(dRes); //string.Format("{0:C}", dIncurred); 
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;


            //new display column for New Outstanding
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            //adding hidden field for storing new outstanding amount
            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWOutstanding";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = ConvertDoubleToCurrencyStr(dBalAmt); //string.Format("{0:C}", dBalAmt);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //adding textbox for new outstanding
            strTitle = "New Reserve Balance   ";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWOutstanding";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "twoThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWOutstanding";
            strControlToClear.Append(strName + ",");
            //to populate value in this textbox from respective hidden field
            //strRef = "//control[@name='" + "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWOutstanding" + "']";
            strRef = "//control[@name='" + strName + "']";
            strType = "currency";
            strReadOnly = "true";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = ConvertDoubleToCurrencyStr(dBalAmt); //string.Format("{0:C}", dBalAmt); 
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, reserveTypeElement.GetAttribute("name").ToString() + "_linebreak4");
            reserveTypeElement.AppendChild(objDisplayColElement);

            //new display column for Reason
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "Reason   ";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_Reason";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "ReserveTypeReason";
            if (reasonCommentsRequired) //nnorouzi; MITS 18989
            {
                lblCssClass += " required";
            }
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_ReserveWorksheetReason";
            strControlToEditList.Append(strName + ",");
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "code";  //type="code" codetable="REASON" 
            strReadOnly = "";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("codetable", "REASON");
            objControlElement.SetAttribute("reasoncode", "");
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //new display column for Comments
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "Comments   ";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_Comments";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "ReserveTypeComments";
            if (reasonCommentsRequired) //nnorouzi; MITS 18989
            {
                lblCssClass += " required";
            }
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_Comments";
            strControlToEditList.Append(strName + ",");
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "memo";
            strReadOnly = "";
            lblCssClass = "twoThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("cols", "82");
            objControlElement.SetAttribute("rows", "4");
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWTOTALINCURRED";
            //strControlToClear.Append(strName + ",");

            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_Comments";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = "";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            return reserveTypeElement;
        }

        private XmlElement GetControlElementForWorkSheet(XmlDocument workSheetDoc, string strTitle, string strClass, string strName, string strRef, string strType, string strReadonly)
        {
            XmlElement objControlElement = null;

            objControlElement = workSheetDoc.CreateElement("control");
            objControlElement.SetAttribute("name", strName);
            objControlElement.SetAttribute("ref", strRef);
            objControlElement.SetAttribute("class", strClass);
            objControlElement.SetAttribute("readonly", strReadonly);
            objControlElement.SetAttribute("title", strTitle);
            objControlElement.SetAttribute("type", strType);

            return objControlElement;
        }

        private XmlElement CreateSectionForWorksheet(XmlDocument workSheetDoc, string strName)
        {
            XmlElement objSectionElement = null;

            objSectionElement = workSheetDoc.CreateElement("control");
            objSectionElement.SetAttribute("name", strName);

            return objSectionElement;

        }

        private XmlElement GetNewGroupElementForWorkSheet(XmlDocument workSheetDoc, string strName, int iSelected, string strTitle)
        {
            XmlElement objGrouptElement = null;

            objGrouptElement = workSheetDoc.CreateElement("group");
            objGrouptElement.SetAttribute("name", strName);
            if (iSelected == 1)
            {
                objGrouptElement.SetAttribute("selected", "1");
            }
            objGrouptElement.SetAttribute("title", strTitle.ToUpper());

            return objGrouptElement;
        }


        private double GetPaidAmountForTransType(int p_iTransId, int p_iReserveTypeCode, int p_iClaimId, int p_iClaimantRowId, int p_iUnitRowId, bool p_bCollInRsvBal)
        {
            StringBuilder sbSQL = null;
            DbReader objReader;
            double dPaidAmount = 0;
            double dCollectionAmount = 0;
            int iClaimantEid = 0;
            int iUnitId = 0;
            int iReserveTrackingLevel = 0;
            int iLOBCode = 0;

            try
            {
                sbSQL = new StringBuilder();

                //FUNDS.COLLECTION_FLAG = -1 

                sbSQL.Append("SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) PaidAmount");
                sbSQL.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT");
                sbSQL.Append(" WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND FUNDS.PAYMENT_FLAG = -1 AND FUNDS.VOID_FLAG = 0 ");
                sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
                sbSQL.Append(" AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = " + p_iTransId + " AND FUNDS.CLAIM_ID = " + p_iClaimId);

                iReserveTrackingLevel = GetReserveTracking(p_iClaimId, ref iLOBCode);

                if (p_iClaimantRowId > 0)
                {
                    GetClaimantId(p_iClaimantRowId, ref iClaimantEid);

                    if (iLOBCode == 241)   //GC
                    {
                        if (iReserveTrackingLevel == 1)  //detail level tracking is ON
                        {
                            if (iClaimantEid > 0)
                            {
                                sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + iClaimantEid);
                            }
                        }
                        else if (iReserveTrackingLevel == 0)  //claim level tracking is ON
                        {
                            if (iClaimantEid != 0)
                            {
                                throw new Exception("Detail level tracking is OFF in utiltiy settings");
                            }
                        }
                    }
                }
                else if (p_iUnitRowId > 0)
                {
                    GetUnitId(p_iUnitRowId, ref iUnitId);

                    if (iLOBCode == 242) //VA
                    {
                        if (iReserveTrackingLevel == 1) //detail level tracking is ON
                        {
                            if (iUnitId > 0)
                            {
                                sbSQL.Append("AND FUNDS.UNIT_ID = " + iUnitId);
                            }
                        }
                        else if (iReserveTrackingLevel == 0)  //Claim level tracking is ON
                        {
                            if (iUnitId != 0)
                            {
                                throw new Exception("Detail level tracking is OFF in utiltiy settings");
                            }
                        }
                    }
                }

                sbSQL.Append(" GROUP BY FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE");

                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReader.Read())
                {
                    dPaidAmount = objReader.GetDouble("PaidAmount");
                }
                sbSQL.Remove(0, sbSQL.Length);
                objReader.Close();


                if (p_bCollInRsvBal)
                {
                    sbSQL.Append("SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) CollectionAmount");
                    sbSQL.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT");
                    sbSQL.Append(" WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND FUNDS.COLLECTION_FLAG = -1 AND FUNDS.VOID_FLAG = 0 ");
                    sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
                    sbSQL.Append(" AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = " + p_iTransId + " AND FUNDS.CLAIM_ID = " + p_iClaimId);

                    if (p_iClaimantRowId > 0)
                    {
                        GetClaimantId(p_iClaimantRowId, ref iClaimantEid);
                        sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + iClaimantEid);
                    }
                    else if (p_iUnitRowId > 0)
                    {
                        GetUnitId(p_iUnitRowId, ref iUnitId);
                        sbSQL.Append("AND FUNDS.UNIT_ID = " + iUnitId);
                    }

                    sbSQL.Append(" GROUP BY FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE");

                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    if (objReader.Read())
                    {
                        dCollectionAmount = objReader.GetDouble("CollectionAmount");
                    }
                    sbSQL.Remove(0, sbSQL.Length);
                    objReader.Close();

                    dPaidAmount = dPaidAmount - dCollectionAmount;

                    if (dPaidAmount < 0)
                    {
                        dPaidAmount = 0;
                    }
                }

                return dPaidAmount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private XmlElement GetLineBreakDisplayElement(XmlDocument workSheetDoc, string cntrlName)
        {
            XmlElement objDisplayColElement = null;
            XmlElement objControlElement = null;

            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            objControlElement = workSheetDoc.CreateElement("control");
            objControlElement.SetAttribute("type", "linebreak");
            objControlElement.SetAttribute("name", cntrlName);

            objDisplayColElement.AppendChild(objControlElement);

            return objDisplayColElement;
        }

        private string GetQueryToReserveAmounts(int p_iReserveTypeCode, int p_iClaimId, int p_iClaimantRowId, int p_iUnitRowId)
        {
            StringBuilder sbSql = null;
            int iReserveTrackingLevel = 0;
            int iLOBCode = 0;
            int iClaimantEid = 0;
            int iUnitId = 0;
            try
            {
                iReserveTrackingLevel = GetReserveTracking(p_iClaimId, ref iLOBCode);
                if (p_iClaimantRowId != 0)
                {
                    GetClaimantId(p_iClaimantRowId, ref   iClaimantEid);
                }
                else if (p_iUnitRowId != 0)
                {
                    GetUnitId(p_iUnitRowId, ref iUnitId);
                }
                sbSql = new StringBuilder();
                sbSql.Append("SELECT RC_ROW_ID, PAID_TOTAL - COLLECTION_TOTAL PAID_BALANCE, PAID_TOTAL, RESERVE_AMOUNT,");
                sbSql.Append(" BALANCE_AMOUNT, INCURRED_AMOUNT, COLLECTION_TOTAL FROM RESERVE_CURRENT");
                sbSql.Append(" WHERE CLAIM_ID = " + p_iClaimId + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode);

                if (iLOBCode == 241)   //GC
                {
                    if (iReserveTrackingLevel == 1)  //detail level tracking is ON
                    {
                        if (iClaimantEid > 0)
                        {
                            //create desired query
                            sbSql.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                        }
                    }
                    else if (iReserveTrackingLevel == 0)  //claim level tracking is ON
                    {
                        if (iClaimantEid != 0)
                        {
                            throw new Exception("Detail level tracking is OFF in utiltiy settings");
                        }
                    }
                }
                else if (iLOBCode == 242) //VA
                {
                    if (iReserveTrackingLevel == 1)
                    {
                        if (iUnitId > 0)
                        {
                            //create desired query
                            sbSql.Append(" AND UNIT_ID = " + iUnitId);
                        }
                    }
                    else if (iReserveTrackingLevel == 0)  //Claim level tracking is ON
                    {
                        if (iUnitId != 0)
                        {
                            throw new Exception("Detail level tracking is OFF in utiltiy settings");
                        }
                    }
                }

                return sbSql.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        private void FillNewCRW(ref XmlDocument p_objXML, int p_iClaimId, string p_sFromApproved, string p_sRSWType)
        {
            Claim objClaim = null;
            Claimant objPrimaryClaimant = null;
            ClaimAdjuster objCurAdj = null;
            Event objEvent = null;
            DataModelFactory objDataModelFactory = null;
            LocalCache objCache = null;
            //shobhana
         //   p_sFromApproved = string.Empty;
            //
            XmlNode objNode = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sName = string.Empty;
            XmlDocument objApprovedXML = null;
            DbReader objReader = null;

            long lUserMId = 0;
            string sSupName = string.Empty;

            StringBuilder sbSQL = null;
            bool bResByClaimType = false;
            LobSettings objLobSetting = null;
            DataSet objDataSet = null;
            ReserveFunds.structReserves[] arrReserves = null;
            int iCnt;
            DataRow objDataRow = null;
            DbConnection objCon = null;
            
            //XmlAttribute attrMCurrency = null;  //Aman Multi Currency
            try
            {
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                sbSQL = new StringBuilder();

                // Get the approved xml in objApprovedXML if p_sFromApproved <> empty
                if (p_sFromApproved != "" &&  p_sFromApproved !=null)
                {
                    string sSQL = "SELECT RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " +
                        p_sFromApproved
                        + " AND RSW_STATUS_CODE = "
                        + objCache.GetCodeId("AP", "RSW_STATUS");
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
                    objApprovedXML = new XmlDocument();

                    if (objReader.Read())
                        objApprovedXML.LoadXml(objReader.GetValue("RSW_XML").ToString());
                    else // In case no xml found, create from scratch
                        p_sFromApproved = "";

                    objReader.Dispose();
                }

                #region Utility Setting Information


//shobhana
                bool reasonCommentsRequired = true;
                StringBuilder strSqlSetting = null;
                DbReader objReaderSetting = null;

                strSqlSetting = new StringBuilder();
                strSqlSetting.Append(string.Format("SELECT {0} FROM {1} WHERE {2}", "REASON_COMMENTS_RSV_WK", "SYS_PARMS_LOB, CLAIM", "SYS_PARMS_LOB.LINE_OF_BUS_CODE = CLAIM.LINE_OF_BUS_CODE AND CLAIM.CLAIM_ID = " + p_iClaimId));
                objReaderSetting = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, strSqlSetting.ToString());
                if (objReaderSetting != null && objReaderSetting.Read())
                {
                    reasonCommentsRequired = Conversion.ConvertObjToBool(objReaderSetting[0], base.ClientId);

                }
                else
                {
                    reasonCommentsRequired = true;
                }
                objNode = p_objXML.SelectSingleNode("//control[@name='hdn_ReasonCommentsRequired']");
                if (objNode != null)
                    objNode.InnerText = reasonCommentsRequired.ToString();
                GetLOBReserveOptions(p_iClaimId);

                objNode = p_objXML.SelectSingleNode("//control[@name='hdnCollInRsvBal']");
                if (objNode != null)
                    objNode.InnerText = m_bCollInResBal.ToString();

                objNode = p_objXML.SelectSingleNode("//control[@name='hdnCollInIncurredBal']");
                if (objNode != null)
                    objNode.InnerText = m_bCollInIncBal.ToString();

                objNode = p_objXML.SelectSingleNode("//control[@name='hdnPrevResBelowPaid']");
                if (objNode != null)
                    objNode.InnerText = m_bPrevResBelowPaid.ToString();

                #endregion

                // Fields common in the Worksheets.
                // Populating the date and the user for the creation and last modified.
                objNode = p_objXML.SelectSingleNode("//control[@name='dtCreated']");
                if (objNode != null)
                {
                    objNode.InnerText = DateTime.Now.ToString();
                    objNode.Attributes["dbformat"].InnerText = Conversion.GetDateTime(DateTime.Now.ToString());
                }

                #region Populating the hidden fields

                objNode = p_objXML.SelectSingleNode("//control[@name='hdnReqStatus']");
                if (objNode != null)
                    objNode.InnerText = "New";

                objNode = p_objXML.SelectSingleNode("//control[@name='hdnCreatedBy']");
                if (objNode != null)
                    objNode.InnerText = m_CommonInfo.UserId.ToString();
                objNode = p_objXML.SelectSingleNode("//control[@name='hdnCurrentUser']");
                if (objNode != null)
                    objNode.InnerText = m_CommonInfo.UserId.ToString();
                
                objNode = p_objXML.SelectSingleNode("//control[@name='hdndtLastMod']");
                if (objNode != null)
                {
                    objNode.InnerText = DateTime.Now.ToString();
                    objNode.Attributes["dbformat"].InnerText = Conversion.GetDateTime(DateTime.Now.ToString());
                }
              
                objNode = p_objXML.SelectSingleNode("//control[@name='hdnLastModBy']");

                if (objNode != null)
                    objNode.InnerText = m_CommonInfo.UserName;

                objNode = p_objXML.SelectSingleNode("//control[@name='hdnClaimId']");

                if (objNode != null)
                    objNode.InnerText = p_iClaimId.ToString();


                objNode = p_objXML.SelectSingleNode("//control[@name='hdnRSWStatus']");
                if (objNode != null)
                {
                    objNode.InnerText = "New";
                    objNode.Attributes["codeid"].InnerText = "0";
                }

                // Populating the claim related information
                objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);
                objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimId);
                objPrimaryClaimant = objClaim.PrimaryClaimant;
                objCurAdj = objClaim.CurrentAdjuster;
                objEvent = (objClaim.Parent as Event);

                objNode = p_objXML.SelectSingleNode("//control[@name='hdnClaimNum']");
                if (objNode != null)
                    objNode.InnerText = objClaim.ClaimNumber;

                objNode = p_objXML.SelectSingleNode("//control[@name='hdnCurAdjName']");
                if (objNode != null)
                {
                    if (objCurAdj.AdjusterEntity.LastName != "" && objCurAdj.AdjusterEntity.FirstName != "")
                        objNode.InnerText = objCurAdj.AdjusterEntity.LastName + ", " + objCurAdj.AdjusterEntity.FirstName;
                    else
                        objNode.InnerText = objCurAdj.AdjusterEntity.LastName;
                }

                objNode = p_objXML.SelectSingleNode("//control[@name='CurAdjName']");
                if (objNode != null)
                {
                    if (objCurAdj.AdjusterEntity.LastName != "" && objCurAdj.AdjusterEntity.FirstName != "")
                        objNode.InnerText = objCurAdj.AdjusterEntity.LastName.Trim() + ", " + objCurAdj.AdjusterEntity.FirstName.Trim();
                    else
                        objNode.InnerText = objCurAdj.AdjusterEntity.LastName.Trim();
                }

                objNode = p_objXML.SelectSingleNode("//control[@name='hdnCurAdjId']");
                if (objNode != null)
                    objNode.InnerText = Conversion.ConvertObjToInt(objCurAdj.AdjusterEntity.EntityId, base.ClientId).ToString();

                objNode = p_objXML.SelectSingleNode("//control[@name='hdnpcName']");
                if (objNode != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        objNode.InnerText = objNode.InnerText.Trim() + objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        objNode.InnerText = objNode.InnerText.Trim() + objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }

                #region General: Populating Claim Number, Primary Claimant, Date Of Loss related information

                objNode = p_objXML.SelectSingleNode("//control[@name='claimnum']");
                if (objNode != null)
                {
                    objNode.InnerText = objClaim.ClaimNumber;
                }
                objNode = p_objXML.SelectSingleNode("//control[@name='claimnumMedWC']");

                if (objNode != null)
                {
                    objNode.InnerText = objClaim.ClaimNumber;
                }

                objNode = p_objXML.SelectSingleNode("//control[@name='claimnumTemWC']");

                if (objNode != null)
                {
                    objNode.InnerText = objClaim.ClaimNumber;
                }

                objNode = p_objXML.SelectSingleNode("//control[@name='claimnumPerWC']");

                if (objNode != null)
                {
                    objNode.InnerText = objClaim.ClaimNumber;
                }

                objNode = p_objXML.SelectSingleNode("//control[@name='claimnumVocWC']");

                if (objNode != null)
                {
                    objNode.InnerText = objClaim.ClaimNumber;
                }

                objNode = p_objXML.SelectSingleNode("//control[@name='claimnumLegWC']");

                if (objNode != null)
                {
                    objNode.InnerText = objClaim.ClaimNumber;
                }

                objNode = p_objXML.SelectSingleNode("//control[@name='claimnumAllWC']");
                if (objNode != null)
                {
                    objNode.InnerText = objClaim.ClaimNumber;
                }

                objNode = p_objXML.SelectSingleNode("//control[@name='claimnumResSummWCtest']");


                if (objNode != null)
                {
                    objNode.InnerText = objClaim.ClaimNumber;
                }
                objNode = p_objXML.SelectSingleNode("//control[@name='pcName']");
                if (objNode != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }
                objNode = p_objXML.SelectSingleNode("//control[@name='pcNameLegWC']");
                if (objNode != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }

                objNode = p_objXML.SelectSingleNode("//control[@name='pcNameAllWC']");
                if (objNode != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }
               
                objNode = p_objXML.SelectSingleNode("//control[@name='pcNamePerWC']");
                if (objNode != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }
                objNode = p_objXML.SelectSingleNode("//control[@name='pcNameVocWC']");
                if (objNode != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }

             
                objNode = p_objXML.SelectSingleNode("//control[@name='pcNameTemWC']");
                if (objNode != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }

                objNode = p_objXML.SelectSingleNode("//control[@name='pcNameMedWC']");
                if (objNode != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }
                objNode = p_objXML.SelectSingleNode("//control[@name='pcNameResSummWCtest']");
                if (objNode != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }
                //Mits 19027 shobhana
                objSysSetting = new SysSettings(m_CommonInfo.Connectionstring, m_iClientId);
                bPrevModValtToZero = objSysSetting.PrevResModifyzero;
                objNode = p_objXML.SelectSingleNode("//control[@name='hdnPrevModValtoZero']");
                if (objNode != null)
                    objNode.InnerText = bPrevModValtToZero.ToString();

                // pcNameMedWC,dtLossMedWC
                //pcNameTemWC,claimnumTemWC,dtLossTemWC
                //pcNamePerWC,claimnumPerWC,dtLossPerWC
                //pcNameVocWC,claimnumVocWC,dtLossVocWC
                //pcNameLegWC,claimnumLegWC,dtLossLegWC
                //pcNameAllWC,claimnumAllWC,dtLossAllWC

                objNode = p_objXML.SelectSingleNode("//control[@name='dtLossResSummWCtest']");
                if (objNode != null)
                    objNode.InnerText = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d");
                objNode = p_objXML.SelectSingleNode("//control[@name='dtLoss']");
                if (objNode != null)
                    objNode.InnerText = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d");
                objNode = p_objXML.SelectSingleNode("//control[@name='dtLossMedWC']");
                if (objNode != null)
                    objNode.InnerText = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d");
                objNode = p_objXML.SelectSingleNode("//control[@name='dtLossTemWC']");
              
                if (objNode != null)
                    objNode.InnerText = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d");
                objNode = p_objXML.SelectSingleNode("//control[@name='dtLossPerWC']");
                if (objNode != null)
                    objNode.InnerText = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d");
                objNode = p_objXML.SelectSingleNode("//control[@name='dtLossVocWC']");
                if (objNode != null)
                    objNode.InnerText = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d");
                objNode = p_objXML.SelectSingleNode("//control[@name='dtLossMedWC']");
                if (objNode != null)
                    objNode.InnerText = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d");
                objNode = p_objXML.SelectSingleNode("//control[@name='dtLossLegWC']");
                if (objNode != null)
                    objNode.InnerText = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d");
                objNode = p_objXML.SelectSingleNode("//control[@name='dtLossAllWC']");
                if (objNode != null)
                    objNode.InnerText = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d");

                #endregion

                #endregion

                #region CRW

                // In case when the worksheet is Claim Reserve Worksheet
                if (p_sRSWType == "CRW")
                {
                    // Populate Event Description
                    XmlCDataSection objCdata = null;
                    objNode = p_objXML.SelectSingleNode("//control[@name='eventdesc']");
                    if (objNode != null && objEvent.EventDescription != null)
                    {
                        objCdata = p_objXML.CreateCDataSection(objEvent.EventDescription.ToString());
                        objNode.AppendChild(objCdata);
                    }

                    if (objClaim.PrimaryPiEmployee != null)
                    {
                        objNode = p_objXML.SelectSingleNode("//control[@name='dtHire']");
                        if (objNode != null)
                            objNode.InnerText = Conversion.GetDBDateFormat(objClaim.PrimaryPiEmployee.DateHired, "d");

                        string[] arrBparts = null;
                        String sBP = string.Empty;
                        objNode = p_objXML.SelectSingleNode("//control[@name='bodyparts']");
                        if (objNode != null && objClaim.PrimaryPiEmployee.BodyParts.ToString() != null)
                        {
                            arrBparts = objClaim.PrimaryPiEmployee.BodyParts.ToString().Split(' ');
                            for (int count = 0; count < arrBparts.Length; count++)
                            {
                                objCache.GetCodeInfo(Riskmaster.Common.Conversion.ConvertStrToInteger(arrBparts[count]), ref sShortCode, ref sDesc);
                                sBP = sBP + sShortCode + " " + sDesc + "\r\n";
                            }
                            objNode.InnerText = sBP;
                        }
                    }

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnClaimTypeDesc']");
                    objCache.GetCodeInfo(objClaim.ClaimTypeCode, ref sShortCode, ref sDesc);
                    if (objNode != null)
                        objNode.InnerText = sDesc;

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnClaimTypeId']");
                    if (objNode != null)
                        objNode.InnerText = Conversion.ConvertObjToStr(objClaim.ClaimTypeCode);

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnClaimTypeCode']");
                    objCache.GetCodeInfo(objClaim.ClaimTypeCode, ref sShortCode, ref sDesc);
                    if (objNode != null)
                        objNode.InnerText = sShortCode;

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnclaimType']");
                    objCache.GetCodeInfo(objClaim.ClaimTypeCode, ref sShortCode, ref sDesc);
                    if (objNode != null)
                        objNode.InnerText = sShortCode;

                    objNode = p_objXML.SelectSingleNode("//control[@name='dtRpt']");
                    if (objNode != null)
                        objNode.InnerText = Conversion.GetDBDateFormat(objEvent.DateReported, "d");

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnClaimStatusId']");
                    objCache.GetCodeInfo(objClaim.ClaimStatusCode, ref sShortCode, ref sDesc);
                    if (objNode != null)
                        objNode.InnerText = Conversion.ConvertObjToStr(objClaim.ClaimStatusCode);

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnClaimStatus']");
                    objCache.GetCodeInfo(objClaim.ClaimStatusCode, ref sShortCode, ref sDesc);
                    if (objNode != null)
                        objNode.InnerText = sShortCode + " " + sDesc;

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnClaimStatusDesc']");
                    objCache.GetCodeInfo(objClaim.ClaimStatusCode, ref sShortCode, ref sDesc);
                    if (objNode != null)
                        objNode.InnerText = sDesc;

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnClaimStatusCode']");
                    objCache.GetCodeInfo(objClaim.ClaimStatusCode, ref sShortCode, ref sDesc);
                    if (objNode != null)
                        objNode.InnerText = sShortCode;

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdndtClaim']");
                    if (objNode != null)
                        objNode.InnerText = Conversion.GetDBDateFormat(objClaim.DateOfClaim, "d");

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnClaimCreateDate']");
                    if (objNode != null)
                        if (objClaim.DttmRcdAdded != string.Empty)
                            objNode.InnerText = objClaim.DttmRcdAdded.Substring(0, 8);

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdndept']");
                    objCache.GetOrgInfo(objEvent.DeptEid, ref sShortCode, ref sDesc);
                    if (objNode != null)
                        objNode.InnerText = sShortCode + " - " + sDesc;

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnDeptDesc']");
                    objCache.GetOrgInfo(objEvent.DeptEid, ref sShortCode, ref sDesc);
                    if (objNode != null)
                        objNode.InnerText = sDesc;

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnEventDeptABBR']");
                    objCache.GetOrgInfo(objEvent.DeptEid, ref sShortCode, ref sDesc);
                    if (objNode != null)
                        objNode.InnerText = sShortCode;

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnEventDeptEID']");
                    if (objNode != null)
                        objNode.InnerText = Conversion.ConvertObjToStr(objEvent.DeptEid);

                    objNode = p_objXML.SelectSingleNode("//control[@name='dtDOB']");
                    if (objNode != null)
                        objNode.InnerText = Conversion.GetDBDateFormat(objPrimaryClaimant.ClaimantEntity.BirthDate, "d");

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnpcClaimantEid']");
                    if (objNode != null)
                        objNode.InnerText = Conversion.ConvertObjToStr(objPrimaryClaimant.ClaimantEid);

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnpcGenderCode']");
                    if (objNode != null)
                        objNode.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(objPrimaryClaimant.ClaimantEntity.SexCode, base.ClientId));

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnpcGenderId']");
                    if (objNode != null)
                        objNode.InnerText = Conversion.ConvertObjToStr(objPrimaryClaimant.ClaimantEntity.SexCode);

                    // Code specific for Claim Reserve Worksheet.
                    //Check LOB
                    //Get Reserve Types for that LOB
                    //Iterate through these Reserve Types and fill values
                    //Use Control Names like MedBillAmt, MedNewBillAmt i.e 1st 3 Character from Reserve Types
                    //So for WC and Medical: MedWC; Indemnity: IndWC; Rehabilitation: RehWC; Legal: LegWC; Other: OthWC;

                    arrReserves = GetResCategories(objClaim.LineOfBusCode, objClaim.ClaimTypeCode);

                    double dRes, dIncurred, dColl, dPaid, dBalAmt;
                    string sSQL = string.Empty;
                    int iReserveTypeCode = 0;
                    string sReserveDesc = "";
                    string sLOB = "";
                    string sCtrlMName = "";
                    string sGPName = "";

                    sLOB = objCache.GetShortCode(objClaim.LineOfBusCode);

                    //Removing other LOB tabs
                    foreach (XmlNode objGpNode in p_objXML.SelectNodes("//group"))
                    {

                        if (objGpNode.Attributes["name"].InnerText.EndsWith(sLOB) == false && objGpNode.Attributes["name"].InnerText.EndsWith("ClaimInfo") == false)
                        {
                            //objGpNode.RemoveAll();
                            XmlNode objdelNode;
                            objdelNode = p_objXML.SelectSingleNode("//form");
                            objdelNode.RemoveChild(objGpNode);
                        }
                    }

                    //shobhana
                    foreach (XmlNode objGpNode in p_objXML.SelectNodes("//group"))
                    {
                        string sResTab = "";
                        string sGroup = "";
                        int iCount = 0;
                        bool bFindNode = false;

                        sGroup = objGpNode.Attributes["name"].InnerText;
                        for (iCount = 0; iCount < arrReserves.GetLength(0); iCount++)
                        {
                            string tempstr = arrReserves[iCount].sReserveDesc.Trim();
                            if (tempstr.IndexOf(" ") > 0)
                                tempstr = tempstr.Replace(" ", "");
                            
                            sResTab = tempstr + sLOB;

                            if (sGroup == sResTab)
                                bFindNode = true;

                            if (sGroup == "RESERVESUMMARY" + sLOB)
                                bFindNode = true;
                        }
                        if (!bFindNode && objGpNode.Attributes["name"].InnerText.EndsWith("ClaimInfo") == false)
                        {
                            XmlNode objdelNode;
                            objdelNode = p_objXML.SelectSingleNode("//form");
                            objdelNode.RemoveChild(objGpNode);
                        }
                    }
                    StringBuilder reserveTypesList = null;
                    reserveTypesList = new StringBuilder();

                    foreach (XmlNode objGpNode in p_objXML.SelectNodes("//group"))
                    {
                        string sGroup1 = "";
                        sGroup1 = objGpNode.Attributes["name"].InnerText;
                        reserveTypesList.Append(sGroup1 + ",");
                    }
                    // objFormElement.SelectSingleNode("//control[@name='hdnReserveTypeTabs']").InnerText = reserveTypesList.ToString();

                    objNode = p_objXML.SelectSingleNode("//control[@name='hdnReserveTypeTabs']");
                    if (objNode != null)
                    {
                        objNode.InnerText = reserveTypesList.ToString();
                    }

                

                    for (iCnt = 0; iCnt < arrReserves.GetLength(0); iCnt++)
                    {
                        iReserveTypeCode = arrReserves[iCnt].iReserveTypeCode;
                        sReserveDesc = arrReserves[iCnt].sReserveDesc.Trim().Replace(" ", "");

                        sGPName = "//group[@name='" + sReserveDesc + sLOB + "']";

                        if (p_objXML.SelectSingleNode(sGPName) != null)
                        {
                            dRes = 0.0;
                            dIncurred = 0.0;
                            dColl = 0.0;
                            dPaid = 0.0;
                            dBalAmt = 0.0;
                            sSQL = string.Empty;

                            //Aman Multi Currency made changes in the query --Start
                            sSQL = "SELECT RC_ROW_ID, PAID_TOTAL - COLLECTION_TOTAL PAID_BALANCE, CLAIM_CURRENCY_PAID_TOTAL - CLAIM_CURR_COLLECTION_TOTAL CLAIM_CURRENCY_PAID_BALANCE, PAID_TOTAL, CLAIM_CURRENCY_PAID_TOTAL, RESERVE_AMOUNT,"
                                   + " CLAIM_CURRENCY_RESERVE_AMOUNT, BALANCE_AMOUNT, CLAIM_CURRENCY_BALANCE_AMOUNT, INCURRED_AMOUNT, CLAIM_CURRENCY_INCURRED_AMOUNT, COLLECTION_TOTAL, CLAIM_CURR_COLLECTION_TOTAL FROM RESERVE_CURRENT"
                                   + " WHERE CLAIM_ID = " + p_iClaimId + " AND RESERVE_TYPE_CODE = " + iReserveTypeCode;
                            objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);

                            if (objReader.Read())
                            {
                                //dRes = objReader.GetDouble("RESERVE_AMOUNT");
                                dRes = objReader.GetDouble("CLAIM_CURRENCY_RESERVE_AMOUNT");
                                //dBalAmt = objReader.GetDouble("BALANCE_AMOUNT");
                                dBalAmt = objReader.GetDouble("CLAIM_CURRENCY_BALANCE_AMOUNT");
                                //dPaid = objReader.GetDouble("PAID_TOTAL");
                                dPaid = objReader.GetDouble("CLAIM_CURRENCY_PAID_TOTAL");
                                //dIncurred = (objReader.GetDouble("PAID_BALANCE") > dRes)? objReader.GetDouble("PAID_BALANCE") : dRes;
                                //dIncurred = objReader.GetDouble("INCURRED_AMOUNT");
                                dIncurred = objReader.GetDouble("CLAIM_CURRENCY_INCURRED_AMOUNT");
                                //dColl = objReader.GetDouble("COLLECTION_TOTAL");
                                dColl = objReader.GetDouble("CLAIM_CURR_COLLECTION_TOTAL");
                            }
                            //Aman Multi Currency made changes in the query --End
                            objReader.Dispose();
                            
                            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
                            //dBalAmt = CalculateReserveBalance(dRes, dPaid, dColl, m_bCollInResBal);
                            dBalAmt = CalculateReserveBalance(dRes, dPaid, dColl, m_bCollInResBal , iReserveTypeCode);
                            if (!IsRecoveryReserve(iReserveTypeCode)) 
                            {
                                dIncurred = CalculateIncurred(dBalAmt, dPaid, dColl, m_bCollInResBal, m_bCollInIncBal, iReserveTypeCode);
                            }
                            else
                            {
                                dIncurred = CalculateIncurred(dBalAmt, dPaid, dColl, false, false, iReserveTypeCode);
                            }
                            sCtrlMName = sReserveDesc.Substring(0, 3) + sLOB;

                            //Balance
                            objNode = p_objXML.SelectSingleNode("//control[@name='BalAmt" + sCtrlMName + "']");
                            //attrMCurrency = p_objXML.CreateAttribute("CurrencyValue");
                            if (objNode != null)
                            {
                                // objNode.InnerText = ConvertDoubleToCurrencyStr(dBalAmt);                               
                                //attrMCurrency.Value = dBalAmt.ToString();
                                //objNode.Attributes.Append(attrMCurrency);
                                objNode.Attributes["CurrencyValue"].Value = dBalAmt.ToString();
                                objNode.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dBalAmt, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
                            }

                            objNode = p_objXML.SelectSingleNode("//control[@name='hdn_NewBalAmt" + sCtrlMName + "']");
                            if (objNode != null)
                            {
                                //attrMCurrency.Value = ConvertDoubleToCurrencyStr(dBalAmt);
                                //objNode.Attributes.Append(attrMCurrency);
                                //objNode.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dBalAmt, m_eNavType, m_CommonInfo.Connectionstring); //Aman Multi Currency
                                //objNode.InnerText = ConvertDoubleToCurrencyStr(dBalAmt);
                                objNode.InnerText = dBalAmt.ToString();
                            }

                            //Incurred and Paid
                            objNode = p_objXML.SelectSingleNode("//control[@name='hdn_NewIncAmt" + sCtrlMName + "']");
                            if (objNode != null)
                            {
                                //objNode.InnerText = ConvertDoubleToCurrencyStr(dIncurred);
                                objNode.InnerText = dIncurred.ToString();
                                //attrMCurrency.Value = ConvertDoubleToCurrencyStr(dIncurred);
                                //objNode.Attributes.Append(attrMCurrency);
                                //objNode.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dIncurred, m_eNavType, m_CommonInfo.Connectionstring); //Aman Multi Currency
                            }

                            objNode = p_objXML.SelectSingleNode("//control[@name='IncurredAmt" + sCtrlMName + "']");
                            if (objNode != null)
                            {
                                //objNode.InnerText = ConvertDoubleToCurrencyStr(dIncurred);
                                //attrMCurrency.Value = dIncurred.ToString();
                                //objNode.Attributes.Append(attrMCurrency);
                                objNode.Attributes["CurrencyValue"].Value = dIncurred.ToString();
                                objNode.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dIncurred, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
                            }
                           

                            objNode = p_objXML.SelectSingleNode("//control[@name='PaidAmt" + sCtrlMName + "']");
                            if (objNode != null)
                            {
                                //objNode.InnerText = ConvertDoubleToCurrencyStr(dPaid);
                                //attrMCurrency.Value = dPaid.ToString();
                                //objNode.Attributes.Append(attrMCurrency);
                                objNode.Attributes["CurrencyValue"].Value = dPaid.ToString();
                                objNode.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dPaid, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
                            }

                            //Reserve and Reserve Balance
                            objNode = p_objXML.SelectSingleNode("//control[@name='hdnRes" + sCtrlMName + "']");
                            if (objNode != null) 
                            {
                                //objNode.InnerText = ConvertDoubleToCurrencyStr(dRes);
                                objNode.InnerText = dRes.ToString();
                            }

                            objNode = p_objXML.SelectSingleNode("//control[@name='hdnBalAmt" + sCtrlMName + "']");
                            if (objNode != null)
                            {
                                objNode.InnerText = dBalAmt.ToString();
                                //objNode.InnerText = ConvertDoubleToCurrencyStr(dBalAmt);
                            }

                            //Incurred
                            objNode = p_objXML.SelectSingleNode("//control[@name='hdnIncurred" + sCtrlMName + "']");
                            if (objNode != null)
                            {
                                //objNode.InnerText = ConvertDoubleToCurrencyStr(dIncurred);
                                objNode.InnerText = dIncurred.ToString();
                            }

                            //Collection
                            objNode = p_objXML.SelectSingleNode("//control[@name='hdnColl" + sCtrlMName + "']");
                            if (objNode != null)
                            {
                                //objNode.InnerText = ConvertDoubleToCurrencyStr(dColl);
                                objNode.InnerText = dColl.ToString();
                            }

                            #region Populating Claim Number, Primary Claimant, Date Of Loss related information

                            objNode = p_objXML.SelectSingleNode("//control[@name='pcName" + sCtrlMName + "']");
                            if (objNode != null)
                            {
                                if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                                    objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                                else
                                    objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                            }

                            objNode = p_objXML.SelectSingleNode("//control[@name='dtLoss" + sCtrlMName + "']");
                            if (objNode != null)
                                objNode.InnerText = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d");

                            objNode = p_objXML.SelectSingleNode("//control[@name='claimnum" + sCtrlMName + "']");
                            if (objNode != null)
                            {
                                objNode.InnerText = objClaim.ClaimNumber;
                            }
                            #endregion

                        }
                    }

                    #region ResSummary: Populating Claim Number, Primary Claimant, Date Of Loss related information

                    objNode = p_objXML.SelectSingleNode("//control[@name='pcNameResSummWCtest" + sLOB + "']");
                    if (objNode != null)
                    {
                        if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                            objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                        else
                            objNode.InnerText = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                    }

                    objNode = p_objXML.SelectSingleNode("//control[@name='dtLossResSummWCtest" + sLOB + "']");
                    if (objNode != null)
                        objNode.InnerText = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d");

                    objNode = p_objXML.SelectSingleNode("//control[@name='claimnumResSummWCtest" + sLOB + "']");
                    if (objNode != null)
                    {
                        objNode.InnerText = objClaim.ClaimNumber;
                    }

                    #endregion
                    //commented by shobhana
                    sSQL = "SELECT MANAGER_ID, LOGIN_NAME FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                            + m_CommonInfo.UserId
                            + " AND USER_TABLE.MANAGER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                            + m_CommonInfo.DSNID;
                    objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                    if (objReader.Read())
                        lUserMId = objReader.GetInt32("MANAGER_ID");
                    else
                        lUserMId = 0;

                    if (lUserMId != 0)
                    {
                        sSQL = "SELECT MANAGER_ID, LOGIN_NAME, USER_TABLE.First_Name as FName, USER_TABLE.Last_Name as LName FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                                + lUserMId
                                + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                                + m_CommonInfo.DSNID;
                        objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                        if (objReader.Read())
                        {
                            sSupName = objReader.GetString("FName") + ", " + objReader.GetString("LName");
                        }
                        else
                        {
                            sSupName = "";
                        }
                    }

                    objNode = p_objXML.SelectSingleNode("//control[@name='SupName']");
                    if (objNode != null)
                    {
                        objNode.InnerText = sSupName;
                    }

                    if (p_sFromApproved != "" && p_sFromApproved !=null) // Making from an approved reserve worksheet
                    {
                        #region CRW from Approved
                        XmlNode objNodeApp = null;
                        string[] arrFields = null;

                        //shobhana for cloning
                        objNode = p_objXML.SelectSingleNode("//control[@name='hdnCtrlToClear']");
                        objNodeApp = objApprovedXML.SelectSingleNode("//control[@name='hdnCtrlToClear']");
                        arrFields = objNode.InnerText.Split(',');
                     
                        if (objNode.InnerText == objNodeApp.InnerText)
                        {
                            for (int count = 0; count < arrFields.Length; count++)
                            {
                                objNode = p_objXML.SelectSingleNode("//control[@name='" + arrFields[count] + "']");
                                if (objNode != null)
                                {
                                    //Aman Multi Currency --Start
                                    if (objNode.Attributes["type"].Value == "currency")
                                    {
                                        objNode.InnerText = objApprovedXML.SelectSingleNode("//control[@name='" + arrFields[count] + "']").InnerText;
                                        objNode.Attributes["CurrencyValue"].Value = objApprovedXML.SelectSingleNode("//control[@name='" + arrFields[count] + "']").Attributes["CurrencyValue"].Value;

                                    }
                                    else
                                    {                                        
                                        objNode.InnerText = objApprovedXML.SelectSingleNode("//control[@name='" + arrFields[count] + "']").InnerText;
                                    }
                                    //Aman Multi Currency --End
                                }                                                                   
                            }
                        }

                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.FillNewCRW.Err", m_iClientId), ex);
            }
            finally
            {
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
                if (objClaim != null)
                    objClaim.Dispose();
                if (objPrimaryClaimant != null)
                    objPrimaryClaimant.Dispose();
                if (objEvent != null)
                    objEvent.Dispose();
                if (objCache != null)
                    objCache.Dispose();
                if (objLobSetting != null)
                    objLobSetting = null;
                if (objDataSet != null)
                    objDataSet.Dispose();
            }
        }
        
        private string GetClaimStatus(int p_iClaimId)
        {
            LocalCache objCache = null;
            DbReader objRdr = null;
            try
            {
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring,
                    "SELECT CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_ID=" + p_iClaimId);
                if (objRdr.Read())
                    return objCache.GetShortCode(objRdr.GetInt32(0));
                else
                    return "";
            }
            finally { objCache.Dispose(); }
        }
        
        private bool CheckPendingRSW(long p_lClaimId, string p_sStatus)
        {
            StringBuilder sbSQL = new StringBuilder();
            DbReader objReader = null;
            bool bReturnVal = false;

            try
            {
                sbSQL.Append("SELECT RSW_ROW_ID FROM RSW_WORKSHEETS, CODES WHERE RSW_STATUS_CODE = CODES.CODE_ID AND ");
                sbSQL.Append(" CODES.SHORT_CODE LIKE '" + p_sStatus + "' AND CLAIM_ID = " + p_lClaimId);
                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReader.Read())
                    bReturnVal = true;
                objReader.Close();
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return bReturnVal;
        }

        private void UpdateRSWTransAmountSafeWay(XmlDocument p_objXML, ReserveFunds.structReserves[] arrReserves,int p_iClaimCurrCode)
        {//MGaba2:r6:Added to make an entry in table used for reporting purpose
            StringBuilder sbSql = new StringBuilder();

            XmlNode objNode = null;
            DbReader objDBRdr = null;
            string sRSWID = string.Empty;
            string strCtrlName = string.Empty;
            //shobhana
             string sGPName = "";
            string strCtrlCommName = string.Empty;
            string sReserveDesc=string.Empty;
            DbConnection objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
            DbCommand objDBCom = null;
            DbTransaction objDbTrans = null;
            int iReason = 0;
            SysParms objSysSetting = null;
            try
            {
                //Added by Nitin to implement transasction 
                objSysSetting = new SysParms(m_CommonInfo.Connectionstring, m_iClientId);
                double dblClaimToBaseExcRate = CommonFunctions.ExchangeRateSrc2Dest(p_iClaimCurrCode, objSysSetting.SysSettings.BaseCurrencyType, m_CommonInfo.Connectionstring, base.ClientId);
                
                objNode = p_objXML.SelectSingleNode("//control[@name='hdnRSWid']");
                if (objNode != null)
                {
                    sRSWID = objNode.InnerText.Trim();
                }

                objConn.Open();

                objDbTrans = objConn.BeginTransaction();
                objDBCom = objConn.CreateCommand();
                objDBCom.Transaction = objDbTrans;

                //Before inserting records for this RSWId ,first deleting TRANS AMOUNT ,to avoid multiple entries and primary key voilation
                sbSql.Append("DELETE FROM RSW_TRANS_AMOUNT WHERE RSW_ROW_ID = " + sRSWID);
                objDBCom.CommandText = sbSql.ToString();
                objDBCom.ExecuteNonQuery();
                sbSql.Remove(0, sbSql.Length);

                //Before inserting records for this RSWId ,first deleting COMMENTS ,to avoid multiple entries and primary key voilation
                sbSql.Append("DELETE FROM RSW_COMMENTS WHERE RSW_ROW_ID = " + sRSWID);
                objDBCom.CommandText = sbSql.ToString();
                objDBCom.ExecuteNonQuery();
                sbSql.Remove(0, sbSql.Length);

                //Now inserting data in RSW_TRANS_AMOUNT  and COMMENTS table

                for (int iCnt = 0; iCnt < arrReserves.GetLength(0); iCnt++)
                {
                    sReserveDesc = arrReserves[iCnt].sReserveDesc.Trim().Replace(" ", "");
                    sGPName = "//group[@name='" + sReserveDesc + "WC" + "']";

                    if (p_objXML.SelectSingleNode(sGPName) != null)
                    {
                        sbSql.Append("SELECT CODE_DESC TransDesc,CODES.CODE_ID TransId from CODES,CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.CODE_ID =" + arrReserves[iCnt].iReserveTypeCode);
                        objDBRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSql.ToString());
                        sbSql.Remove(0, sbSql.Length);

                        while (objDBRdr.Read())
                        {

                            sbSql.Append("INSERT INTO RSW_TRANS_AMOUNT(RSW_ROW_ID,RESERVE_TYPE_CODE,TRANS_TYPE,RES_BALANCE,AMOUNT,PAID_TOTAL) VALUES(");
                            sbSql.Append(sRSWID + ",");
                            sbSql.Append(arrReserves[iCnt].iReserveTypeCode + ",");
                            sbSql.Append(0 + ",");
                            //sbSql.Append( ",");

                            //    strCtrlName = "txt_ReserveType_" + arrReserves[iCnt].iReserveTypeCode + "_TransTypeNewReserveBal_" + objDBRdr.GetInt32("TransId").ToString();
                            strCtrlName = "NewBalAmt" + arrReserves[iCnt].sReserveDesc.Substring(0, 3) + "WC";
                            objNode = p_objXML.SelectSingleNode("//control[@name='" + strCtrlName + "']");
                            if (objNode != null)//skhare7 R8
                               // sbSql.Append(Conversion.ConvertStrToDouble(objNode.InnerText) + ",");
                                 sbSql.Append((Conversion.ConvertStrToDouble(objNode.InnerText) * dblClaimToBaseExcRate) + ",");
                            else
                                sbSql.Append(Conversion.ConvertStrToDouble("0.0") + ",");
                            strCtrlName = "NewIncAmt" + arrReserves[iCnt].sReserveDesc.Substring(0, 3) + "WC";
                            //  strCtrlName = "txt_ReserveType_" + arrReserves[iCnt].iReserveTypeCode + "_TransTypeIncurredAmount_" + objDBRdr.GetInt32("TransId").ToString();
                            objNode = p_objXML.SelectSingleNode("//control[@name='" + strCtrlName + "']");
                            if (objNode != null)
                               // sbSql.Append(Conversion.ConvertStrToDouble(objNode.InnerText) + ",");
                                sbSql.Append((Conversion.ConvertStrToDouble(objNode.InnerText) * dblClaimToBaseExcRate) + ",");
                            else
                                sbSql.Append(Conversion.ConvertStrToDouble("0.0") + ",");

                            strCtrlName = "PaidAmt" + arrReserves[iCnt].sReserveDesc.Substring(0, 3) + "WC".ToString();
                            //  strCtrlName = "txt_ReserveType_" + arrReserves[iCnt].iReserveTypeCode + "_Paid_" + objDBRdr.GetInt32("TransId").ToString();
                            objNode = p_objXML.SelectSingleNode("//control[@name='" + strCtrlName + "']");
                            if (objNode != null)
                              //  sbSql.Append(Conversion.ConvertStrToDouble(objNode.InnerText));
                                sbSql.Append((Conversion.ConvertStrToDouble(objNode.InnerText) * dblClaimToBaseExcRate) );
                            else
                                sbSql.Append(Conversion.ConvertStrToDouble("0.0"));

                            sbSql.Append(")");

                            objDBCom.CommandText = sbSql.ToString();
                            objDBCom.ExecuteNonQuery();
                            sbSql.Remove(0, sbSql.Length);

                        }

                        //Added by Nitin One more row for Other TransType
                        //sbSql.Append("INSERT INTO RSW_TRANS_AMOUNT(RSW_ROW_ID,RESERVE_TYPE_CODE,TRANS_TYPE,TRANS_DESC,RES_BALANCE,AMOUNT,PAID_TOTAL) VALUES(");
                        //sbSql.Append(sRSWID + ",");
                        //sbSql.Append(arrReserves[iCnt].iReserveTypeCode + ",");
                        //sbSql.Append("-1,'Other Adjustments',");
                        //strCtrlName = "txt_ReserveType_" + arrReserves[iCnt].iReserveTypeCode + "_TransTypeIncurredAmount_OtherAdjustment";
                        //objNode = p_objXML.SelectSingleNode("//control[@name='" + strCtrlName + "']");
                        //if (objNode != null)
                        //{
                        //    sbSql.Append(Conversion.ConvertStrToDouble(objNode.InnerText));
                        //    sbSql.Append(",");
                        //    sbSql.Append(Conversion.ConvertStrToDouble(objNode.InnerText));
                        //}
                        //else
                        //{
                        //    sbSql.Append("0, 0");
                        //}

                        //sbSql.Append(",0)");
                        //objDBCom.CommandText = sbSql.ToString();
                        //objDBCom.ExecuteNonQuery();
                        //sbSql.Remove(0, sbSql.Length);

                        //now inserting the comments
                        strCtrlName = "ResReason" + arrReserves[iCnt].sReserveDesc.Substring(0, 3) + "WC";
                        strCtrlCommName = "Comm" + arrReserves[iCnt].sReserveDesc.Substring(0, 3) + "WC";
                        objNode = p_objXML.SelectSingleNode("//control[@name='" + strCtrlName + "']");
                        if (objNode != null)
                        {
                            if (objNode.Attributes["reasoncode"] != null)
                                if (objNode.Attributes["reasoncode"].Value != "")
                                    iReason = Convert.ToInt32(objNode.Attributes["reasoncode"].Value);
                        }
                        sbSql.Append("INSERT INTO RSW_COMMENTS(RSW_ROW_ID,RESERVE_TYPE_CODE,COMMENTS,REASON_CODE) VALUES(");
                        sbSql.Append(sRSWID + ",");
                        sbSql.Append(arrReserves[iCnt].iReserveTypeCode + ",");
                        if (objNode != null)
                            sbSql.Append("'" + p_objXML.SelectSingleNode("//control[@name='" + strCtrlCommName + "']").InnerText + "',");
                        else
                            sbSql.Append("'" + "" + "',");
                        sbSql.Append(iReason + ")");

                        objDBCom.CommandText = sbSql.ToString();
                        objDBCom.ExecuteNonQuery();
                        sbSql.Remove(0, sbSql.Length);
                    }
                }
                objDbTrans.Commit();
            }
            catch (RMAppException p_objExp)
            {
                objDbTrans.Rollback();
                throw p_objExp;
            }
            catch (Exception p_objException)
            {
                objDbTrans.Rollback();
                throw new RMAppException
                    (Globalization.GetString("ReserveWorksheet.UpdateRSWTransAmount.Error",m_iClientId), p_objException);
            }
            finally
            {
                objConn.Close();
                objConn = null;
                objDBCom = null;
                objDBRdr = null;
            }
        }

        /// <summary>
        /// MGaba2:Updating Summary comments for reporting purpose in Rsw_worksheets
        /// </summary>
        /// <param name="p_objXML"></param>
        private void UpdateSummaryComments(XmlDocument p_objXML)
        {

            StringBuilder sbSql = new StringBuilder();
            XmlNode objNode = null;
            string sRSWID = string.Empty;
            DbConnection objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
            DbCommand objDBCom = null;
            DbParameter objDBParam = null;

            try
            {
                //MGaba2:Inserting Reserve Summary Comments for Reporting Purposes
                objNode = p_objXML.SelectSingleNode("//control[@name='hdnRSWid']");
                if (objNode != null)
                {
                    sRSWID = objNode.InnerText.Trim();
                }

                objConn.Open();
                objDBCom = objConn.CreateCommand();
                objDBParam = objDBCom.CreateParameter();

                objDBParam.ParameterName = "SummaryComments";
                objDBParam.SourceColumn = "SUMMARY_COMMENTS";
                objDBParam.Direction = ParameterDirection.Input;
                objDBParam.Value = "";
                objNode = p_objXML.SelectSingleNode("//control[@name='txt_RESERVESUMMARY_Comments']");
                if (objNode != null)
                {
                    objDBParam.Value = objNode.InnerText;
                }

                objDBCom.Parameters.Add(objDBParam);

                sbSql.Append("UPDATE RSW_WORKSHEETS SET SUMMARY_COMMENTS=~SummaryComments~ WHERE RSW_ROW_ID=");
                sbSql.Append(sRSWID);

                objDBCom.CommandText = sbSql.ToString();
                objDBCom.ExecuteNonQuery();
                sbSql.Remove(0, sbSql.Length);
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            finally
            {
                objConn = null;
                objDBCom = null;
                objDBParam = null;
            }
        }

        #endregion

    }
}
