using System;

namespace Riskmaster.Application.EventExplorer
{
	/// <summary>
	/// Author  :   Vaibhav Kaushik
	/// Dated   :   1 Oct 2004 
	/// Purpose :   Represents Payment Data.  
	/// </summary>	
	internal class Payment
	{
		#region Variable Declaration
		/// <summary>
		/// Private varaible for Transacrion Type Code
		/// </summary>
		private int m_iTransTypeCode = 0 ;
		/// <summary>
		/// Private variable for Amount
		/// </summary>
		private double m_dblAmount = 0.0 ;
		/// <summary>
		/// Private variable for FromDate
		/// </summary>
		private string m_sFromDate = "" ;
		/// <summary>
		/// Private variable for ToDate
		/// </summary>
		private string m_sToDate = "" ;
		/// <summary>
		/// Private variable for Code Description 
		/// </summary>
		private string m_sCodeDesc = "" ;
		/// <summary>
		/// Private variable for Amount Paid
		/// </summary>
		private double m_dblPaid = 0.0 ;
		/// <summary>
		/// Private variable for Avg. Rate
		/// </summary>
		private double m_dblAvgRate = 0.0 ;
		/// <summary>
		/// Private variable for Lump Sum flag.
		/// </summary>
		private bool m_bLumpSum = false ;
		#endregion

		#region Constructors
		/// <summary>
		/// Default Constructor
		/// </summary>
		internal Payment()
		{			
		}
		#endregion

		#region Properties
		/// <summary>
		/// Read/Write property for TransTypeCode.
		/// </summary>
		internal int TransTypeCode 
		{
			get
			{
				return m_iTransTypeCode;
			}
			set
			{
				m_iTransTypeCode = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Amount.
		/// </summary>
		internal double Amount 
		{
			get
			{
				return m_dblAmount;
			}
			set
			{
				m_dblAmount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Paid.
		/// </summary>
		internal double Paid 
		{
			get
			{
				return m_dblPaid;
			}
			set
			{
				m_dblPaid = value ;
			}
		}
		/// <summary>
		/// Read/Write property for AvgRate.
		/// </summary>
		internal double AvgRate 
		{
			get
			{
				return m_dblAvgRate;
			}
			set
			{
				m_dblAvgRate = value ;
			}
		}
		/// <summary>
		/// Read/Write property for FromDate.
		/// </summary>
		internal string FromDate 
		{
			get
			{
				return m_sFromDate.Trim();
			}
			set
			{
				m_sFromDate = value ;
			}
		}
		/// <summary>
		/// Read/Write property for ToDate.
		/// </summary>
		internal string ToDate 
		{
			get
			{
				return m_sToDate.Trim();
			}
			set
			{
				m_sToDate = value ;
			}
		}
		/// <summary>
		/// Read/Write property for CodeDesc.
		/// </summary>
		internal string CodeDesc 
		{
			get
			{
				return m_sCodeDesc;
			}
			set
			{
				m_sCodeDesc = value ;
			}
		}

		/// <summary>
		/// Read/Write property for LumpSum.
		/// </summary>
		internal bool LumSum
		{
			get
			{
				return m_bLumpSum ;
			}
			set
			{
				m_bLumpSum = value ;
			}
		}
		#endregion
	}
}
