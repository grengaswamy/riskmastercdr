using System;
using System.Collections;
using System.Linq;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.EventExplorer
{
	/// <summary>
	/// Author  :   Vaibhav Kaushik
	/// Dated   :   1 Oct 2004 
	/// Purpose :   For Getting Current Financial Information.
	/// </summary>	
	internal class FinancialSummary
	{
		#region Variable Declaration
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;		
		/// <summary>
		/// Private variable to store reference of Financial Summary Item List
		/// </summary>
		private FinancialSummaryItemList m_objFinancialSummaryItemList  = null ;
		/// <summary>
		/// Private variable to store reference of Financial Summary Item
		/// </summary>
		private FinancialSummaryItem m_objFinancialSummaryItem = null ;
        private int m_iClientId = 0;
		#endregion 

		#region Constructors
		/// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sConnectionString">Connection String</param>
        internal FinancialSummary(string p_sConnectionString, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_sConnectionString = p_sConnectionString ;						
		}
		#endregion 

		/// <summary>
		/// Get the Financial Summary List for a claim.
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>FinancialSummaryItemList</returns>
		internal FinancialSummaryItemList GetListForClaim( int p_iClaimID )
		{
			try
			{
				m_objFinancialSummaryItemList = new FinancialSummaryItemList();
				BuildList( p_iClaimID , "" ) ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("FinancialSummary.GetListForClaim.DataError", m_iClientId), p_objEx);
			}
			return( m_objFinancialSummaryItemList );
		}
		/// <summary>
		/// Get the Financial Summary List for a claimant
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <param name="p_iClaimantID">Claimant ID</param>
		/// <returns>FinancialSummaryItemList</returns>
		internal FinancialSummaryItemList GetListForClaimant( int p_iClaimID , int p_iClaimantID)
		{
			try
			{
				m_objFinancialSummaryItemList = new FinancialSummaryItemList();
				BuildList( p_iClaimID , " AND CLAIMANT_EID=" + p_iClaimantID.ToString() ) ;
				return( m_objFinancialSummaryItemList );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("FinancialSummary.GetListForClaimant.DataError", m_iClientId), p_objEx);
			}
		}
		/// <summary>
		/// Build the Financial Summary Item List
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <param name="p_sWhereClause">Where Clause string </param>
		private void BuildList( int p_iClaimID , string p_sWhereClause )
		{
			
			DbReader objReader = null ;		
	
			ArrayList arrlstTemp = null ;
			FinancialSummaryData objFinancialSummaryData = null ;
						
			string sSQL = "" ;
			int iIndex = 0 ;
			int iCount = 0 ;
			bool bIsInitialized = false ;
            //Changed by Gagan for MITS 16243 : Start
            int iLOB = 0;
            bool bCollInRsvBal = false;
            bool bCollInIncurredBal = false;
            double dRIncurred = 0;
            // akaushik5 added for MITS 31530 Starts
            int p_iClaimantEID = default(int);
            bool bSuccess = false;
            // akaushik5 added for MITS 31530 Ends

			try           
            {

                sSQL = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimID.ToString();
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iLOB = Conversion.ConvertStrToInteger(objReader.GetValue("LINE_OF_BUS_CODE").ToString());
                    }
                }               

                sSQL =  "SELECT COLL_IN_RSV_BAL, COLL_IN_INCUR_BAL ";
                sSQL += "FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + iLOB.ToString();                                         

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                    
                        bCollInRsvBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("COLL_IN_RSV_BAL").ToString()), m_iClientId);
                    
                        bCollInIncurredBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("COLL_IN_INCUR_BAL").ToString()), m_iClientId);

                    
                    }
                    objReader.Close();

                }

                // akaushik5 added for MITS 31530 Starts
                if (iLOB.Equals(243) || iLOB.Equals(844))
                {
                    sSQL = string.Format("SELECT CLAIMANT_EID from CLAIMANT where CLAIM_ID = {0}", p_iClaimID);
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            p_iClaimantEID = Conversion.CastToType<int>(objReader.GetValue("CLAIMANT_EID").ToString(), out bSuccess);
                        }

                        objReader.Close();
                    }
                }

                if (bSuccess && !p_iClaimantEID.Equals(default(int)))
                {
                    p_sWhereClause += string.Format(" AND CLAIMANT_EID = {0}", p_iClaimantEID);
                }
                // akaushik5 added for MITS 31530 Ends

             //Changed by Gagan for MITS 16243 : End

				arrlstTemp = new ArrayList();

                //Ashish Ahuja : Mits 32210 Start

                //sSQL = "SELECT  CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC" 
                //    +	" FROM CODES, CODES_TEXT, GLOSSARY" 
                //    +	" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID" 
                //    +	" AND CODES.TABLE_ID = GLOSSARY.TABLE_ID" 
                //    +	" AND GLOSSARY.SYSTEM_TABLE_NAME='RESERVE_TYPE'" 
                //    +	" AND CODES.DELETED_FLAG=0" 
                //    +	" ORDER BY CODES_TEXT.CODE_DESC" ;
                sSQL = "SELECT  CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC "
                    + " FROM CODES, CODES_TEXT, GLOSSARY,RESERVE_CURRENT"
                    + " WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID"
                    + " AND RESERVE_CURRENT.RESERVE_TYPE_CODE=CODES.CODE_ID "
                    + " AND RESERVE_CURRENT.CLAIM_ID=" + p_iClaimID.ToString()
                    + " AND CODES.TABLE_ID = GLOSSARY.TABLE_ID"
                    + " AND GLOSSARY.SYSTEM_TABLE_NAME='RESERVE_TYPE'"
                    + " AND CODES.DELETED_FLAG=0"
                    + " GROUP BY CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC "
                    + " ORDER BY CODES_TEXT.CODE_DESC";
                //Ashish Ahuja : Mits 32210 End
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );

				if( objReader != null )
				{
					while( objReader.Read() )
					{	
						objFinancialSummaryData = new FinancialSummaryData();
						objFinancialSummaryData.CodeID = objReader.GetInt( "CODE_ID" );
						objFinancialSummaryData.ShortCode = objReader.GetString( "SHORT_CODE" );
						objFinancialSummaryData.CodeDesc = objReader.GetString( "CODE_DESC" );
                        
                        // akaushik5 Added for MITS 31530 Starts
                        // Below condition is added to check if codeId is already present in Temp array.
                        if (!object.ReferenceEquals(arrlstTemp, null) 
                            && object.ReferenceEquals(arrlstTemp.Cast<FinancialSummaryData>().FirstOrDefault(x => x.CodeID.Equals(objFinancialSummaryData.CodeID)),null))
                        // akaushik5 Added for MITS 31530 Ends
                        {
                            arrlstTemp.Add(objFinancialSummaryData);
                        }

						objFinancialSummaryData = null ;
					}
				}
				if( !objReader.IsClosed )
					objReader.Close();
                //Ashish ahuja : Mits 33210 Code Commented
/*
				for( iIndex = 0 ; iIndex < arrlstTemp.Count ; iIndex++ )
				{
					objFinancialSummaryData = ( FinancialSummaryData )arrlstTemp[iIndex] ;

					sSQL = " SELECT COUNT(*) FROM RESERVE_CURRENT WHERE CLAIM_ID=" + p_iClaimID 
						+	" AND RESERVE_TYPE_CODE=" + objFinancialSummaryData.CodeID.ToString() + " " + p_sWhereClause ;

					objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
					if( objReader != null )
						if( objReader.Read() )
							iCount = objReader.GetInt( 0 );

					if( !objReader.IsClosed )
						objReader.Close();

					if( iCount > 0 )
					{
						bIsInitialized = false ;						
						foreach( FinancialSummaryItem objTempFinancialSummaryItem in m_objFinancialSummaryItemList )
						{										
							if( m_objFinancialSummaryItem.FinancialTypeId == objFinancialSummaryData.CodeID )
							{
								bIsInitialized = true ;
								m_objFinancialSummaryItem = objTempFinancialSummaryItem ;
								break;
							}
						}

						if( ! bIsInitialized )
						{
							m_objFinancialSummaryItem = new FinancialSummaryItem();

							m_objFinancialSummaryItem.FinancialTypeId = objFinancialSummaryData.CodeID ;
							m_objFinancialSummaryItem.FinancialTypeShortCode = objFinancialSummaryData.ShortCode ;
							m_objFinancialSummaryItem.FinancialTypeDesc = objFinancialSummaryData.CodeDesc ;
							m_objFinancialSummaryItem.ReserveExists = true ;
							m_objFinancialSummaryItem.OutstandingTotal = 0.0 ;
							m_objFinancialSummaryItem.PaymentsTotal = 0.0 ;
							m_objFinancialSummaryItem.CollectionsTotal = 0.0 ;
							m_objFinancialSummaryItem.IncurredTotal = 0.0 ;
							m_objFinancialSummaryItemList.Add( m_objFinancialSummaryItem );														
						}

						sSQL = "SELECT * FROM RESERVE_CURRENT WHERE CLAIM_ID=" + p_iClaimID 
							+	" AND RESERVE_TYPE_CODE=" + objFinancialSummaryData.CodeID.ToString() + " " + p_sWhereClause ;

						objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
						if( objReader != null )
							while( objReader.Read() )
							{
								m_objFinancialSummaryItem.OutstandingTotal += objReader.GetDouble( "BALANCE_AMOUNT" );
								m_objFinancialSummaryItem.PaymentsTotal += objReader.GetDouble( "PAID_TOTAL" );
                                m_objFinancialSummaryItem.CollectionsTotal += objReader.GetDouble( "COLLECTION_TOTAL" );
                                //Changed by Gagan for MITS 16243 : Start
                                dRIncurred = CalculateIncurred(objReader.GetDouble("BALANCE_AMOUNT"),
                                                               objReader.GetDouble("PAID_TOTAL"),
                                                               objReader.GetDouble("COLLECTION_TOTAL"), bCollInRsvBal, bCollInIncurredBal);
								//m_objFinancialSummaryItem.IncurredTotal += objReader.GetDouble( "INCURRED_AMOUNT" );								
                                m_objFinancialSummaryItem.IncurredTotal += dRIncurred;
                                //Changed by Gagan for MITS 16243 : End
							}

						if( !objReader.IsClosed )
							objReader.Close();					
					}// End if icount
					objFinancialSummaryData = null ;
				}// End if for loop
			}*/
                //Ashish Ahuja : Mits 33210 : Start
                for (iIndex = 0; iIndex < arrlstTemp.Count; iIndex++)
                {
                    objFinancialSummaryData = (FinancialSummaryData)arrlstTemp[iIndex];
                    sSQL = "SELECT RESERVE_TYPE_CODE,SUM (BALANCE_AMOUNT) as BALANCE_AMOUNT,SUM (PAID_TOTAL) as PAID_TOTAL,SUM (COLLECTION_TOTAL) as COLLECTION_TOTAL "
                   + " FROM RESERVE_CURRENT WHERE CLAIM_ID=" + p_iClaimID
                   + " AND RESERVE_TYPE_CODE=" + objFinancialSummaryData.CodeID.ToString() + " " + p_sWhereClause
                   + " GROUP BY RESERVE_TYPE_CODE";

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                   
                    if (objReader != null)
                        if (objReader.Read())
                        {
                            bIsInitialized = false;
                            foreach (FinancialSummaryItem objTempFinancialSummaryItem in m_objFinancialSummaryItemList)
                            {
                                if (m_objFinancialSummaryItem.FinancialTypeId == objFinancialSummaryData.CodeID)
                                {
                                    bIsInitialized = true;
                                    m_objFinancialSummaryItem = objTempFinancialSummaryItem;
                                    break;
                                }
                            }

                            if (!bIsInitialized)
                            {
                                m_objFinancialSummaryItem = new FinancialSummaryItem();

                                m_objFinancialSummaryItem.FinancialTypeId = objFinancialSummaryData.CodeID;
                                m_objFinancialSummaryItem.FinancialTypeShortCode = objFinancialSummaryData.ShortCode;
                                m_objFinancialSummaryItem.FinancialTypeDesc = objFinancialSummaryData.CodeDesc;
                                m_objFinancialSummaryItem.ReserveExists = true;
                                m_objFinancialSummaryItem.OutstandingTotal = 0.0;
                                m_objFinancialSummaryItem.PaymentsTotal = 0.0;
                                m_objFinancialSummaryItem.CollectionsTotal = 0.0;
                                m_objFinancialSummaryItem.IncurredTotal = 0.0;
                                m_objFinancialSummaryItemList.Add(m_objFinancialSummaryItem);
                            }

                            m_objFinancialSummaryItem.OutstandingTotal += objReader.GetDouble("BALANCE_AMOUNT");
                            m_objFinancialSummaryItem.PaymentsTotal += objReader.GetDouble("PAID_TOTAL");
                            m_objFinancialSummaryItem.CollectionsTotal += objReader.GetDouble("COLLECTION_TOTAL");
                            //Changed by Gagan for MITS 16243 : Start
                            dRIncurred = CalculateIncurred(objReader.GetDouble("BALANCE_AMOUNT"),
                                                           objReader.GetDouble("PAID_TOTAL"),
                                                           objReader.GetDouble("COLLECTION_TOTAL"), bCollInRsvBal, bCollInIncurredBal,objFinancialSummaryData.CodeID);
                            //m_objFinancialSummaryItem.IncurredTotal += objReader.GetDouble( "INCURRED_AMOUNT" );								
                            m_objFinancialSummaryItem.IncurredTotal += dRIncurred;
                            //Changed by Gagan for MITS 16243 : End
                            //}

                        }// End if icount

                    if (!objReader.IsClosed)
                        objReader.Close();
                    objFinancialSummaryData = null;
                }// End if for loop
            }
            //Ashish Ahuja : Mits 33210 : End
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("FinancialSummary.BuildList.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				arrlstTemp = null ;
				objFinancialSummaryData = null ;
				
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}     
        
        }// End If BuilList


        //Changed by Gagan for MITS 16243 : Start

        #region private CalculateIncurred (p_dBalance, p_dPaid, p_dCollect, p_bCollInRsvBal)

        /// Name		: CalculateIncurred        
        /// Date Created: 5/11/2009
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used calculating the Incurred amount on the basis of 
        /// total amount collected and paid.
        /// </summary>
        /// <remarks>		
        /// </remarks>
        /// <param name="p_dBalance">The remaining balance</param>
        /// <param name="p_dPaid">The amount already paid to the Claimant</param>
        /// <param name="p_dCollect">The amount already collected.</param>
        /// <param name="p_bCollInRsvBal"></param>
        private double CalculateIncurred(double p_dBalance, double p_dPaid, double p_dCollect,
            bool p_bCollInRsvBal, bool p_bCollInIncurredBal, int p_iReserveTypeCode)//Shruti for MITS 8551
        {
            double dReturnValue = 0;
            double dTemp = 0;

            bool bIsRecoveryReserve = false;
            LocalCache objCache = null;
            objCache = new LocalCache(m_sConnectionString, m_iClientId);
            //zmohammad : JIRA 5352(Mits 37388) - Fix for Recovery Reserve type - Start
            if (p_iReserveTypeCode != 0 && (objCache.GetRelatedShortCode(p_iReserveTypeCode) == "R"))
            {
                bIsRecoveryReserve = true;
            }
            objCache.Dispose();

            if (bIsRecoveryReserve)
            {
                if (p_dBalance < 0)
                {
                    dReturnValue = p_dCollect;
                }
                else
                {
                    dReturnValue = p_dBalance + p_dCollect;
                }
            }
            else
            {
                if (p_bCollInRsvBal)
                {
                    dTemp = p_dPaid - p_dCollect;
                    if (dTemp < 0)
                        dTemp = 0;
                    if (p_dBalance < 0)
                        dReturnValue = dTemp;
                    else
                        dReturnValue = p_dBalance + dTemp;
                }
                else
                {
                    if (p_dBalance < 0)
                        dReturnValue = p_dPaid;
                    else
                        dReturnValue = p_dBalance + p_dPaid;
                }
            }
            //zmohammad : JIRA 5352(Mits 37388) - Fix for Recovery Reserve type - End
            //Shruti for MITS 8551
            if ((p_bCollInIncurredBal) && (!bIsRecoveryReserve))
            {
                dReturnValue = dReturnValue - p_dCollect;
            }

            if (dReturnValue < 0)
                dReturnValue = 0;

            return dReturnValue;
        }
        #endregion



        //Changed by Gagan for MITS 16243 : End





	}
}
