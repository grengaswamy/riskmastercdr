using System;

namespace Riskmaster.Application.EventExplorer
{
	/// <summary>
	/// Author  :   Vaibhav Kaushik
	/// Dated   :   1 Oct 2004 
	/// Purpose :   Represents Claim Notes Data.  
	/// </summary>
	internal class ClaimNotes
	{
		#region Variable Declaration
		/// <summary>
		/// Private variable for Header Note
		/// </summary>
		private string m_sHeaderNote = "" ;
		/// <summary>
		/// Private variable for Date Entered
		/// </summary>
		private string m_sDateEntered = "" ;
		/// <summary>
		/// Private variable for Entered By User.
		/// </summary>
		private string m_sEnteredByUser = "" ;
		/// <summary>
		/// Private variable for Last First Name
		/// </summary>
		private string m_sLastFirstName = "" ;
        /* Arnab: MITS-10226 - Added new variable */
        /// <summary>
        /// Private variable for DateTime Record of the Last Update
        /// </summary>                
        private string m_sDttmRcdLastUpd = "";
        //MITS-10226 End
		#endregion

		#region Constructors
		/// <summary>
		/// Default Constructor
		/// </summary>
		internal ClaimNotes()
		{			
		}
		#endregion

		#region Properties
		/// <summary>
		/// Read/Write property for HeaderNote
		/// </summary>
		internal string HeaderNote 
		{
			get
			{
				return m_sHeaderNote;
			}
			set
			{
				m_sHeaderNote = value ;
			}
		}
		/// <summary>
		/// Read/Write property for DateEntered
		/// </summary>
		internal string DateEntered 
		{
			get
			{
				return m_sDateEntered ;
			}
			set
			{
				m_sDateEntered = value ;
			}
		}
		/// <summary>
		/// Read/Write property for EnteredByUser
		/// </summary>
		internal string EnteredByUser 
		{
			get
			{
				return m_sEnteredByUser;
			}
			set
			{
				m_sEnteredByUser = value ;
			}
		}
		/// <summary>
		/// Read/Write property for LastFirstName
		/// </summary>
		internal string LastFirstName 
		{
			get
			{
				return m_sLastFirstName ;
			}
			set
			{
				m_sLastFirstName = value ;
			}
		}
        /* Arnab: MITS-10226 - Added new Get/Set Property for the variable "m_sDttmRcdAdded" */
        /// <summary DttmLastUpdated>
        /// Read/Write property for Last Update Date
        /// </summary>        
        internal string DttmLastUpdated
        {
            get
            {                
                return m_sDttmRcdLastUpd;
            }
            set
            {
                m_sDttmRcdLastUpd = value;
            }
        }
        //MITS:10226 End
		#endregion
	}
}
