using System;

namespace Riskmaster.Application.EventExplorer
{
	/// <summary>
	/// Author  :   Vaibhav Kaushik
	/// Dated   :   1 Oct 2004 
	/// Purpose :   Represents a Financial Summary Item.  
	/// </summary>
	internal class FinancialSummaryItem
	{
		#region Variable Declaration
		/// <summary>
		/// Private variable for Outstanding Total
		/// </summary>
		private double m_dblOutstandingTotal = 0.0 ;
		/// <summary>
		/// Private variable for Payments Total
		/// </summary>
		private double m_dblPaymentsTotal = 0.0 ;
		/// <summary>
		/// Private variable for Collections Total
		/// </summary>
		private double m_dblCollectionsTotal = 0.0 ;
		/// <summary>
		/// Private variable for Incurred Total
		/// </summary>
		private double m_dblIncurredTotal = 0.0 ;
		/// <summary>
		/// Private variable for Reserve Exists
		/// </summary>
		private bool m_bReserveExists = false ;
		/// <summary>
		/// Private variable for Financial TypeId
		/// </summary>
		private int m_iFinancialTypeId = 0 ;
		/// <summary>
		/// Private variable for Financial Type Short Code
		/// </summary>
		private string m_sFinancialTypeShortCode = "" ;
		/// <summary>
		/// Private variable for Financial Type Description
		/// </summary>
		private string m_sFinancialTypeDesc = "" ;
		#endregion

		#region Constructors
		/// <summary>
		/// Default Constructor
		/// </summary>
		internal FinancialSummaryItem()
		{				
		}
		#endregion

		#region Properties
		/// <summary>
		/// Read/Write property for OutstandingTotal.
		/// </summary>
		internal double OutstandingTotal 
		{
			get
			{
				return m_dblOutstandingTotal;
			}
			set
			{
				m_dblOutstandingTotal = value ;
			}
		}	
		/// <summary>
		/// Read/Write property for PaymentsTotal.
		/// </summary>
		internal double PaymentsTotal 
		{
			get
			{
				return m_dblPaymentsTotal;
			}
			set
			{
				m_dblPaymentsTotal = value ;
			}
		}	
		/// <summary>
		/// Read/Write property for CollectionsTotal.
		/// </summary>
		internal double CollectionsTotal 
		{
			get
			{
				return m_dblCollectionsTotal;
			}
			set
			{
				m_dblCollectionsTotal = value ;
			}
		}	
		/// <summary>
		/// Read/Write property for IncurredTotal.
		/// </summary>
		internal double IncurredTotal 
		{
			get
			{
				return m_dblIncurredTotal;
			}
			set
			{
				m_dblIncurredTotal = value ;
			}
		}		
		/// <summary>
		/// Read/Write property for ReserveExists.
		/// </summary>
		internal bool ReserveExists 
		{
			get
			{
				return m_bReserveExists;
			}
			set
			{
				m_bReserveExists = value ;
			}
		}		
		/// <summary>
		/// Read/Write property for FinancialTypeId.
		/// </summary>
		internal int FinancialTypeId 
		{
			get
			{
				return m_iFinancialTypeId;
			}
			set
			{
				m_iFinancialTypeId = value ;
			}
		}		
		/// <summary>
		/// Read/Write property for FinancialTypeShortCode.
		/// </summary>
		internal string FinancialTypeShortCode 
		{
			get
			{
				return m_sFinancialTypeShortCode;
			}
			set
			{
				m_sFinancialTypeShortCode = value ;
			}
		}		
		/// <summary>
		/// Read/Write property for FinancialTypeDesc.
		/// </summary>
		internal string FinancialTypeDesc 
		{
			get
			{
				return m_sFinancialTypeDesc;
			}
			set
			{
				m_sFinancialTypeDesc = value ;
			}
		}
		#endregion
		
	}
}
