using System;
using System.Collections ;

namespace Riskmaster.Application.EventExplorer
{
	/// <summary>
	/// Author  :   Vaibhav Kaushik
	/// Dated   :   1 Oct 2004 
	/// Purpose :   Represents Financial Summary Item List.  
	/// </summary>
	internal class FinancialSummaryItemList : CollectionBase
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		internal FinancialSummaryItemList()
		{			
		}
		/// <summary>
		/// Add an item in the list.
		/// </summary>
		/// <param name="p_objFinancialSummaryItem">Financial Summary Item</param>
		/// <returns>Position on which the item is inserted.</returns>
		internal int Add(FinancialSummaryItem p_objFinancialSummaryItem)
		{
			return List.Add( p_objFinancialSummaryItem );			
		}		
		/// <summary>
		/// Remove the item form the list.
		/// </summary>
		/// <param name="p_objFinancialSummaryItem">Financial Summary Item</param>
		internal void Remove( FinancialSummaryItem p_objFinancialSummaryItem )
		{
			List.Remove( p_objFinancialSummaryItem );
		} 
		/// <summary>
		/// Check the existence of the item in the list.
		/// </summary>
		/// <param name="p_objFinancialSummaryItem">Financial Summary Item</param>
		/// <returns>Return the status of the item. "True" if item is in the list.</returns>
		internal bool Contains( FinancialSummaryItem p_objFinancialSummaryItem )
		{
			return List.Contains( p_objFinancialSummaryItem );
		}
		/// <summary>
		/// Get the index of the item in the list.
		/// </summary>
		/// <param name="p_objFinancialSummaryItem">Financial Summary Item</param>
		/// <returns>Return the integer index.</returns>
		internal int IndexOf( FinancialSummaryItem p_objFinancialSummaryItem )
		{
			return List.IndexOf( p_objFinancialSummaryItem );
		}
		/// <summary>
		/// Copy the item form one list to another list.
		/// </summary>
		/// <param name="p_arrFinancialSummaryItem">Financial Summary Item</param>
		/// <param name="iIndex">The index of the item.</param>
		internal void CopyTo( FinancialSummaryItem[] p_arrFinancialSummaryItem , int p_iIndex)
		{
			List.CopyTo( p_arrFinancialSummaryItem , p_iIndex );
		}
		/// <summary>
		/// Indexing for FinancialSummaryItem
		/// </summary>
		internal FinancialSummaryItem this[int p_iIndex]
		{
			get { return ( FinancialSummaryItem )List[p_iIndex]; }
			set { List[p_iIndex] = value; }
		}
	}
}
