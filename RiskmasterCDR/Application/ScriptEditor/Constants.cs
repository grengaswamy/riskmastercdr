using System;

namespace Riskmaster.Application.ScriptEditor
{
	/**************************************************************
	 * $File		: Constants.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 12/20/2005
	 * $Author		: Vaibhav Kaushik
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	public class Constants
	{
		internal const string DOCUMENT_ROOT = "ScriptEditor" ;
		internal const string TREE_ROOT = "ScriptEditorTreeRoot" ;
		internal const string LOAD_SCRIPT_ROOT = "LoadScriptRoot" ;

		internal const string DATA_MODEL_OBJECTS = "DataModelObjects" ;
		internal const string DATA_MODEL_OBJECT = "DataModelObject" ;
		internal const string DATA_MODEL_OBJECT_NAME = "Name" ;
		internal const string DATA_MODEL_OBJECT_FRIENDLY_NAME = "FriendlyName" ;
		
		internal const string TREE_NODE = "TreeNode" ;	
		internal const string NODE_TYPE_SCRIPT = "NodeTypeScript" ;

		internal const string SCRIPT_TYPE = "ScriptType" ;
		internal const string SCRIPT_ROW_ID = "RowId" ;
		internal const string SCRIPT_OBJECT_NAME = "ObjectName" ;
		internal const string SCRIPT_TEXT = "ScriptText" ;
		internal const string SCRIPT_DESC = "Description" ;
		internal const string EXPANDED = "Expanded" ;
		internal const string HIGHLIGHT = "Highlight" ;

		internal const string AUTHOR = "Author" ;
		internal const string CREATED_ON = "CreatedOn" ;
		internal const string MODIFIED_BY = "ModifiedBy";
		internal const string MODIFIED_ON = "ModifiedOn" ;

		internal const string SELECTED_OBJECTS_LIST = "SelectedObjectsList" ;
		internal const string SEPARATOR = "|" ;

		// Event is the same as VB Key word, so need to use the Namespace.
		internal const string DEFAULT_SUB_CODE = @"Sub {0}{1}( obj{1} As Riskmaster.Datamodel.{1} )

	' Write your code here.

End Sub";

		internal const string DEFAULT_FUNCTION_CODE = @"Sub {0}{1}( obj{1} As Riskmaster.Datamodel.{1} )

	' Write your code here.
	
	' Good idea is to reset error collection
	g_ValErrors.Clear()

	' Sample code to add the error msg into the collection. 
	' g_ValErrors.add( ""UniqueErrorDescription"" , ""Put your error message here."" ) 

End Sub";
	
	}
}
