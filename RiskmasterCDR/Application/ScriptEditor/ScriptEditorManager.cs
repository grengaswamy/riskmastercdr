﻿
using System;
using System.Xml ;
using System.Collections ;
using Riskmaster.Common ;
using Riskmaster.Db ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;
using System.Data ;
using System.Reflection ; 
using Riskmaster.Scripting ;
using Riskmaster.Security ;
using System.Web;


namespace Riskmaster.Application.ScriptEditor
{
	/**************************************************************
	 * $File		: ScriptEditorManager.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 12/20/2005
	 * $Author		: Vaibhav Kaushik
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	public class ScriptEditorManager
	{
		#region Variable Declaration 
		private XmlDocument m_objXmlDocument = null ;
		private XmlElement m_objRootNode = null ;
		private ArrayList m_arrlstAllScripts = null ;
        private string m_sUserName = "";            //gagnihotri MITS 11995 Changes made for Audit table
        private int m_iClientId = 0;//psharma206
		

		private ArrayList m_arrlstScriptObjects = null ;

		public ArrayList ScriptObjectsList 
		{
			get
			{
				return( m_arrlstScriptObjects ) ;
			}
			set
			{
				m_arrlstScriptObjects = value ;
			}
		}

		#endregion 

		#region Constructors
        public ScriptEditorManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)//psharma206 jira 106
		{
            m_iClientId = p_iClientId;//psharma206 jira106
            m_sUserName = p_sUserName;  //gagnihotri MITS 11995 Changes made for Audit table
			if( !Common.IsInitialized )
                Common.Initialized(p_sDsnName, p_sUserName, p_sPassword, m_iClientId); //psharma206 jira 106
			
			Common.StartDocument( ref m_objXmlDocument , ref m_objRootNode , Constants.DOCUMENT_ROOT );
			

		}

		#endregion 

		#region Dispose.
		public void Dispose()
		{
			try
			{
				if( Common.DataModelFactory != null )
				{
					Common.DataModelFactory.UnInitialize();
					Common.DataModelFactory = null ;
					Common.IsInitialized = false ;
					Common.ConnectionString = "" ;
					Common.DsnName = "" ;
					Common.UserName = "" ;
					Common.Password = "" ;
				}
				m_objXmlDocument = null ;									
			}
			catch
			{
				// Avoid raising any exception here.				
			}
		}

		#endregion

		#region Load Data Model Object List, Using Reflection
		public XmlDocument LoadDataModelObjects()
		{
			XmlElement objDataModelObjectsNode = null ;
			XmlElement objTempNode = null ;
			Assembly objDataModelAssembly = null ;
			Type[] objTypes = null ;
			Type objType = null ;
			SortedList objSortedList = null ;
			
			try
			{
				objDataModelAssembly = Assembly.GetAssembly( typeof( Riskmaster.DataModel.Event) );
				objTypes = objDataModelAssembly.GetTypes();
				Common.CreateElement( m_objRootNode , Constants.DATA_MODEL_OBJECTS , ref objDataModelObjectsNode );
							
				objSortedList = new SortedList();
				foreach( Type objTypeTemp in objTypes )
				{					
					if(!objTypeTemp.IsClass)
						continue;				
					if( objTypeTemp.BaseType.Name == "DataCollection" )
						continue;
                    if (objTypeTemp.BaseType.Name != "DataObject" && objTypeTemp.BaseType.Name != "Entity")
                        if (objTypeTemp.Name != "ADMTable")
                            continue;
                    if (objTypeTemp.Name == "ReserveHistory")
                    {
                        continue;
                    }
					objSortedList.Add( objTypeTemp.Name , objTypeTemp );					
				}								
				
				for( int iIndex = 0 ; iIndex < objSortedList.Count ; iIndex++ )
				{
					objType = ( Type ) objSortedList.GetByIndex( iIndex );
					Common.CreateElement( objDataModelObjectsNode , Constants.DATA_MODEL_OBJECT , ref objTempNode );
					Common.CreateAndSetElement( objTempNode , Constants.DATA_MODEL_OBJECT_NAME , objType.Name );
					Common.CreateAndSetElement( objTempNode , Constants.DATA_MODEL_OBJECT_FRIENDLY_NAME , objType.Name );
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("ScriptEditorManager.LoadDataModelObjects.Error", m_iClientId) , p_objEx );				
			}
			finally
			{
				objDataModelObjectsNode = null ;
				objTempNode = null ;				
			}
			return( m_objXmlDocument );
		}
		
		#endregion 

		#region Load Script
		public XmlDocument LoadScript( int p_iRowId )
		{
			DbReader objReader = null ;
			XmlElement objScriptRootNode = null ;			
				
			string sSQL = string.Empty ;	
			string sDiscription = string.Empty ;
			string sAuthor = string.Empty ;
			string sCreatedOn = string.Empty ;
			string sModifiedBy = string.Empty ;
			string sModifiedOn = string.Empty ;

			string[] arrProperties = null ;
			int iScriptType = 0 ;

			try
			{
				Common.CreateElement( m_objRootNode , Constants.LOAD_SCRIPT_ROOT , ref objScriptRootNode );				
				
				sSQL = " SELECT * FROM VBS_SCRIPTS WHERE ROW_ID = " + p_iRowId ;
				objReader = DbFactory.GetDbReader( Common.ConnectionString , sSQL );
				
				if( objReader.Read() )
				{
					iScriptType	= Conversion.ConvertObjToInt( objReader.GetValue( "TYPE" ), m_iClientId )  ;
					
					Common.CreateAndSetElement( objScriptRootNode , Constants.SCRIPT_ROW_ID , Conversion.ConvertObjToInt( objReader.GetValue( "ROW_ID" ), m_iClientId ).ToString() );
					Common.CreateAndSetElement( objScriptRootNode , Constants.SCRIPT_TYPE , iScriptType.ToString() );
					Common.CreateAndSetElement( objScriptRootNode , Constants.SCRIPT_OBJECT_NAME , objReader.GetString( "OBJECT_NAME" ) );
					Common.CreateAndSetElement( objScriptRootNode , Constants.SCRIPT_TEXT , objReader.GetString( "SCRIPT" ) );

					sDiscription = objReader.GetString( "DESCRIPTION" );
					arrProperties = sDiscription.Split( Constants.SEPARATOR.ToCharArray() );	
					Common.CreateAndSetElement( objScriptRootNode , Constants.AUTHOR , arrProperties[0] );
					Common.CreateAndSetElement( objScriptRootNode , Constants.CREATED_ON , arrProperties[1] );
					Common.CreateAndSetElement( objScriptRootNode , Constants.MODIFIED_BY , arrProperties[2] );
					Common.CreateAndSetElement( objScriptRootNode , Constants.MODIFIED_ON , arrProperties[3] );
				}
				else
				{
					throw new RMAppException(Globalization.GetString("ScriptEditorManager.LoadScript.ScriptNotFound", m_iClientId) );	
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("ScriptEditorManager.LoadScript.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
				objScriptRootNode = null ;				
			}
			return( m_objXmlDocument );
		}

		#endregion 

		#region Return New Script Xml
		public XmlDocument NewScript( int p_iScriptType , string p_sObjectName )
		{
			XmlElement objScriptRootNode = null ;
			string sDefaultCode = string.Empty ;
			string sInitial = string.Empty ;
				
			try
			{
				switch( p_iScriptType )
				{
					case 1:
						sInitial = "ValC" ;
						break ;
					case 2:
						sInitial = "CalcC" ;
						break ;
					case 3:
						sInitial = "InitC" ;
						break ;
					case 4:
						sInitial = "PSC" ;
						break ;
					case 5:
						sInitial = "ASC" ;
						break ;
                    case 6:
                        sInitial = "PDC";
                        break;
				}
				if( p_iScriptType != 1 )
					sDefaultCode = string.Format( Constants.DEFAULT_SUB_CODE , sInitial , p_sObjectName );
				else
					sDefaultCode = string.Format( Constants.DEFAULT_FUNCTION_CODE , sInitial , p_sObjectName );

				Common.CreateElement( m_objRootNode , Constants.LOAD_SCRIPT_ROOT , ref objScriptRootNode );													
				Common.CreateAndSetElement( objScriptRootNode , Constants.SCRIPT_ROW_ID , "0" );
				Common.CreateAndSetElement( objScriptRootNode , Constants.SCRIPT_TYPE , p_iScriptType.ToString() );
				Common.CreateAndSetElement( objScriptRootNode , Constants.SCRIPT_OBJECT_NAME , p_sObjectName );
				Common.CreateAndSetElement( objScriptRootNode , Constants.SCRIPT_TEXT , sDefaultCode );
				Common.CreateAndSetElement( objScriptRootNode , Constants.AUTHOR , Common.DataModelFactory.Context.RMUser.LoginName );
				Common.CreateAndSetElement( objScriptRootNode , Constants.CREATED_ON , System.DateTime.Now.ToString("d") );
				Common.CreateAndSetElement( objScriptRootNode , Constants.MODIFIED_BY , "" );
				Common.CreateAndSetElement( objScriptRootNode , Constants.MODIFIED_ON , "" );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("ScriptEditorManager.NewScript.Error", m_iClientId), p_objEx);		//psharma206 jira 106		
			}
			finally
			{
				objScriptRootNode = null ;				
			}
			return( m_objXmlDocument );
		}

		#endregion 

		#region Return Tree Xml
		public XmlDocument LoadTreeXml( XmlDocument p_objXmlIn )
		{
			DbReader objReader = null ;
			XmlElement objTreeRootNode = null ;
			XmlElement objScripts = null ;
			XmlElement objScript = null ;
			XmlElement[] arrScripts = new XmlElement[6] ;
			
			string sSQL = string.Empty ;
			string sObjectName = string.Empty ;
			int iScriptId = 0 ;
			int iTempScriptId = 0 ;
			int iScriptType = 0 ;
			try
			{
				iScriptId = Conversion.ConvertStrToInteger( p_objXmlIn.SelectSingleNode( "//FocusScriptId" ).InnerText );
				iScriptType = Conversion.ConvertStrToInteger( p_objXmlIn.SelectSingleNode( "//FocusScriptType" ).InnerText );
				
				Common.CreateElement( m_objRootNode , Constants.TREE_ROOT , ref objTreeRootNode );				
				objTreeRootNode.SetAttribute( Constants.SCRIPT_TYPE , "0" ); // Script Type Zero for uniquely identifying as ROOT.
				objTreeRootNode.SetAttribute( Constants.EXPANDED , "" );
				
				// Create DEFAULT parent nodes for Validation, Calculation, Initialization, Before Save, After Save.
				for( int iIndex = 1 ; iIndex <= 6 ; iIndex++ )
				{
					Common.CreateElement( objTreeRootNode , Constants.TREE_NODE , ref objScripts );	
					objScripts.SetAttribute( Constants.SCRIPT_TYPE , iIndex.ToString() );
					objScripts.SetAttribute( Constants.SELECTED_OBJECTS_LIST , "" );
					if( iScriptType == iIndex )
						objScripts.SetAttribute( Constants.EXPANDED , "" );
					arrScripts[iIndex-1] = objScripts ;
				}

				sSQL = " SELECT * FROM VBS_SCRIPTS WHERE ROW_ID != 0 ORDER BY TYPE , OBJECT_NAME " ;
				objReader = DbFactory.GetDbReader( Common.ConnectionString , sSQL );
				
				while( objReader.Read() )
				{
					iScriptType	= Conversion.ConvertObjToInt( objReader.GetValue( "TYPE" ), m_iClientId )  ;
					sObjectName = objReader.GetString( "OBJECT_NAME" );

					objScripts = arrScripts[iScriptType-1];
					objScripts.Attributes[Constants.SELECTED_OBJECTS_LIST].InnerText += sObjectName + "," ;	

					Common.CreateElement( objScripts , Constants.TREE_NODE , ref objScript );	
					
					iTempScriptId = Conversion.ConvertObjToInt( objReader.GetValue( "ROW_ID" ), m_iClientId ) ;					
					if( iScriptId == iTempScriptId )
						objScript.SetAttribute( Constants.HIGHLIGHT , "" );	

					objScript.SetAttribute( Constants.NODE_TYPE_SCRIPT , "" );
					objScript.SetAttribute( Constants.SCRIPT_ROW_ID , iTempScriptId.ToString() );
					objScript.SetAttribute( Constants.SCRIPT_TYPE , iScriptType.ToString() );
					objScript.SetAttribute( Constants.SCRIPT_OBJECT_NAME , sObjectName );					
				}				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("ScriptEditorManager.LoadTreeXml.Error", m_iClientId), p_objEx);		//psharma206 jira 106		
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
				objTreeRootNode = null ;
				objScripts = null ;
				objScript = null ;
			}
			return( m_objXmlDocument );
		}

		#endregion 

		#region Save Script
		public XmlDocument SaveScript( XmlDocument p_objXmlIn )
		{
			ScriptEngine objScriptEngine = null ;
			string sScriptText = string.Empty ;
			string sObjectName = string.Empty ;			

			int iRowId = 0 ;
			
			try
			{
				sObjectName = p_objXmlIn.SelectSingleNode( "//ObjectName" ).InnerText;
				//Raman 6/27 Script Editor is crashing due to htmlencode at UI layer
                //sScriptText = p_objXmlIn.SelectSingleNode( "//ScriptText" ).InnerText ;

                sScriptText = HttpUtility.HtmlDecode(p_objXmlIn.SelectSingleNode("//ScriptText").InnerText);
                
				objScriptEngine = Common.DataModelFactory.Context.ScriptEngine ;
				
				if( !objScriptEngine.IsReady )
					objScriptEngine.ForceLoad() ;

				objScriptEngine.AddScript(sScriptText );				
				
				objScriptEngine.ResetEngine();

				objScriptEngine.StoreBinary();

				objScriptEngine.StartEngine();

				SaveScriptInDB( p_objXmlIn , ref iRowId );

				this.LoadScript( iRowId );
								
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("ScriptEditorManager.SaveScript.Error", m_iClientId), p_objEx);		//psharma206 jira 106		
			}
			finally
			{
								
			}
			return( m_objXmlDocument );
		}


		private void SaveScriptInDB( XmlDocument p_objXmlIn , ref int p_iRowId )
		{
			DbConnection objConn = null ;
			DbWriter objWriter = null ;
			byte[] arrByte = null ;
			
			int iRowId = 0 ;
			int iScriptType = 0 ;
			string sScriptText = string.Empty ;
			string sObjectName = string.Empty ;
			string sAuthor = string.Empty ;
			string sCreatedOn = string.Empty ;
			string sModifiedBy = string.Empty ;
			string sModifiedOn = string.Empty ;
			string sDescription = string.Empty ;

			try
			{
				iRowId = Conversion.ConvertStrToInteger( p_objXmlIn.SelectSingleNode( "//RowId" ).InnerText );
				iScriptType = Conversion.ConvertStrToInteger( p_objXmlIn.SelectSingleNode( "//ScriptType" ).InnerText );
				sAuthor = p_objXmlIn.SelectSingleNode( "//Author" ).InnerText;
				sCreatedOn = p_objXmlIn.SelectSingleNode( "//CreatedOn" ).InnerText;
				sModifiedBy = p_objXmlIn.SelectSingleNode( "//ModifiedBy" ).InnerText;
				sModifiedOn = p_objXmlIn.SelectSingleNode( "//ModifiedOn" ).InnerText;				
				sObjectName = p_objXmlIn.SelectSingleNode( "//ObjectName" ).InnerText;
				sScriptText = p_objXmlIn.SelectSingleNode( "//ScriptText" ).InnerText ;				

				sDescription = sAuthor + Constants.SEPARATOR + sCreatedOn + Constants.SEPARATOR + sModifiedBy + Constants.SEPARATOR + sModifiedOn ;
				arrByte = new byte[1] ;
				objConn = DbFactory.GetDbConnection( Common.ConnectionString );
				objConn.Open();
				
				objWriter = DbFactory.GetDbWriter( objConn );
				objWriter.Tables.Add( "VBS_SCRIPTS");
				objWriter.Fields.Add( "TYPE" , iScriptType );
				objWriter.Fields.Add( "OBJECT_NAME" , sObjectName );
				objWriter.Fields.Add( "SCRIPT" , sScriptText );
				objWriter.Fields.Add( "BINARY", arrByte ); //Still uses writer to accomplish binary field storing.
				objWriter.Fields.Add( "DESCRIPTION", sDescription );
                //gagnihotri MITS 11995 Changes made for Audit table
                objWriter.Fields.Add("UPDATED_BY_USER", m_sUserName);
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
				if( iRowId <= 0 )
				{
                    iRowId = Utilities.GetNextUID(Common.ConnectionString, "VBS_SCRIPTS", m_iClientId);
					p_iRowId = iRowId ;
					objWriter.Fields.Add( "ROW_ID" , iRowId );					
				}
				else
				{
					p_iRowId = iRowId ;
					objWriter.Where.Add( "ROW_ID='" + iRowId + "'" );
				}

				objWriter.Execute();
			
				objConn.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("ScriptEditorManager.SaveScriptInDB.Error", m_iClientId), p_objEx);		//psharma206 jira 106		
			}
			finally
			{
				objConn.Close();
				objWriter = null ;
			}

		}

		#endregion 

		#region Delete Script
		public void DeleteScript( int p_iRowId )
		{
			ScriptEngine objScriptEngine = null ;
			string sSQL = string.Empty ;
			DbConnection objConn = null ;

			try
			{
				sSQL = "DELETE FROM VBS_SCRIPTS WHERE ROW_ID = " + p_iRowId ;
				objConn = DbFactory.GetDbConnection( Common.ConnectionString );
				objConn.Open();				
				objConn.ExecuteNonQuery( sSQL );
				objConn.Close();
				

				// Vaibhav : 10/11/2006 : Delete script would require complete compilation again.
				objScriptEngine = Common.DataModelFactory.Context.ScriptEngine ;
				
				if( !objScriptEngine.IsReady )
					objScriptEngine.ForceLoad() ;

                //Start by Shivendu for MITS 12044
                objScriptEngine.DeleteScript();
                //End by Shivendu for MITS 12044
				objScriptEngine.ResetEngine();

				objScriptEngine.StoreBinary();

				objScriptEngine.StartEngine();

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("ScriptEditorManager.DeleteScript.Error", m_iClientId), p_objEx);		//psharma206 jira 106		
			}
			finally
			{
				if( objConn != null )
				{
					objConn.Close();
					objConn = null ;
				}
			}
		}
		#endregion 
	}
}
