using System;
using System.Collections.Generic;
using System.IO;
using Xceed.Compression;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using C1.C1Zip;
using C1.C1Zip.ZLib;



namespace Riskmaster.Application.ZipUtil
{

	/// <summary>	
	/// This class is used for compression and decompression
	/// of images. 			
	/// </summary>
	public class ZipUtility:IDisposable
	{
		
		#region Member Variables
		/// <summary>
		/// Contains the binary data for the image that has been converted to memory stream. 
		/// </summary>
		private byte[] m_arrByte;
        private int m_iClientId = 0;
		#endregion

		#region Constructor
		/// <summary>
		/// Riskmaster.Application.ZipUtility is the default constructor.
		/// </summary>
        public ZipUtility(int p_iClientId)
		{
            //License key should not be configurable through Riskmaster.config
            const string ZIP_LICENSE_KEY = "SCN10-5A4G2-CBUWD-W45A";

			try
			{
                m_iClientId = p_iClientId;
                Xceed.Compression.Licenser.LicenseKey = ZIP_LICENSE_KEY;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ZipUtil.ZipUtility.LicenseKeyError",m_iClientId),p_objException);
			}
		}
		#endregion

		#region Methods

		/// <summary>
		/// This function will decode the binary string encrypted through EncodeBinaryString() of this library.
		/// </summary>
		/// <param name="p_sSrc">String to be decoded.</param>
		/// <param name="p_arrImage">This array will contain the decoded bytes.</param>
		internal  void DecodeBinaryString(ref string p_sSrc,ref byte[] p_arrImage)
		{
			short sHex9 =0;
            short sHexA =0;
            short sHex0 =0;
			int iITmp=0;
			int iJTmp=0;
			string sSTmp="";
			short sC1=0;
            short sC2=0;
			int iLen=0; 
			try
			{
				if(p_sSrc!=null && p_sSrc!="")
				{
					sHex9=Conversion.GetAscii("9");
					sHexA=Conversion.GetAscii("A");
					sHex0=Conversion.GetAscii("0");
					iLen=p_sSrc.Length;
					sSTmp=sSTmp.PadRight((iLen/2),' ');
					while(iITmp<iLen)
					{
						sC2=Conversion.GetAscii(p_sSrc.Substring(iITmp,1));
					
						if(sC2 > sHex9)
						{
							sC2 = (short)(sC2 - sHexA + 10);
						}
						else
						{
							sC2 = (short)(sC2 - sHex0);
						}
						++iITmp;


						sC1=Conversion.GetAscii(p_sSrc.Substring(iITmp,1));
					
						if(sC1 > sHex9)
						{
							sC1 = (short)(sC1 - sHexA + 10);
						}
						else
						{
							sC1 = (short)(sC1 - sHex0);
						}
						++iITmp;

                       p_arrImage[iJTmp]=(byte)((sC2 * 16) + sC1);
    
                         ++iJTmp;
					}
				}				
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("ZipUtil.DecodeBinaryString.Error", m_iClientId), p_objException);
			}
		}
		/// <summary>
		/// This function will encode the byte array into a string.
		/// </summary>
		/// <param name="p_arrImage">Array of bytes to be encoded</param>		
		/// <returns>String containing the encoded binary data</returns>
		internal string EncodeBinaryString(ref byte[] p_arrImage)
		{
			int iTemp=0;
			int kTemp=0;
			int lTemp=0;
			string sLookUp="";
			System.Text.StringBuilder sbTemp=new System.Text.StringBuilder();
			
			try
			{
				sLookUp = "000102030405060708090A0B0C0D0E0F"
					+ "101112131415161718191A1B1C1D1E1F"
					+ "202122232425262728292A2B2C2D2E2F"
					+ "303132333435363738393A3B3C3D3E3F"
					+ "404142434445464748494A4B4C4D4E4F"
					+ "505152535455565758595A5B5C5D5E5F"
					+ "606162636465666768696A6B6C6D6E6F"
					+ "707172737475767778797A7B7C7D7E7F"
					+ "808182838485868788898A8B8C8D8E8F"
					+ "909192939495969798999A9B9C9D9E9F"
					+ "A0A1A2A3A4A5A6A7A8A9AAABACADAEAF"
					+ "B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF"
					+ "C0C1C2C3C4C5C6C7C8C9CACBCCCDCECF"
					+ "D0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF"
					+ "E0E1E2E3E4E5E6E7E8E9EAEBECEDEEEF"
					+ "F0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF";
			  
				iTemp=p_arrImage.GetLowerBound(0);
				kTemp=p_arrImage.GetUpperBound(0);
				while(iTemp<=kTemp)
				{
					lTemp=2*p_arrImage[iTemp];
					sbTemp.Append(sLookUp.Substring(lTemp,2));
					iTemp=iTemp+1;
				}
				return sbTemp.ToString();
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("ZipUtil.EncodeBinaryString.Error", m_iClientId), p_objException);
			}
			finally
			{
              sbTemp=null;
			}
		}
		

		#region Vaibhav Has commented the Code, Component One is replaced by XCeed
//		/// <summary>
//		/// This function will decompress the string into memory stream.
//		/// </summary>
//		/// <param name="p_sStr">String to be decompressed.</param>
//		/// <returns>Memory Stream after decompression</returns>
//		public MemoryStream UnZIPBufferToMemoryStream1(ref string p_sStr)
//		{
//			MemoryStream objMsSrc=null;
//			C1ZStreamReader objSr=null;
//			BinaryWriter objBw=null;
//			BinaryReader objBr=null;
//			MemoryStream objMsDest=null;
//			byte[] arrImage=null;
//			byte[] arrData=null;
//			try
//			{
//				if(p_sStr!=null  && p_sStr.Length>0)
//				{
//					arrImage=new byte[p_sStr.Length/2];
//					DecodeBinaryString(ref p_sStr,ref arrImage);	
//					objMsDest=new MemoryStream();
//					objMsSrc=new MemoryStream(arrImage);
//					objSr = new C1ZStreamReader(objMsSrc);
//					objBw =new BinaryWriter(objMsDest);
//					objBr=new BinaryReader(objSr);
//					int iCount =0;
//					arrData=new byte[1024];
//					while(true)
//					{
//						iCount = objBr.Read(arrData, 0, 1024);
//						if(iCount <= 0)
//						{
//							break;
//						}
//						objBw.Write(arrData,0,iCount);
//					}
//					objBw.Flush();
//					objSr.Close();
//					
//				}
//			}
//			catch(RMAppException p_objException)
//			{
//				throw p_objException;
//			}
//			catch(Exception p_objException)
//			{
//				throw new RMAppException(Globalization.GetString("ZipUtil.UnZIPBufferToMemoryStream.Error"),p_objException);
//			}
//			finally
//			{
//				if(objMsSrc!=null)
//				{
//					objMsSrc.Close();
//					objMsSrc=null;
//				}
//				if(objBr!=null)
//				{
//					objBr.Close();
//					objBr=null;
//				}
//				if(objSr!=null)
//				{
//					objSr.Close();
//					objSr=null;
//				}		
//				if(arrImage!=null)
//				{
//                 arrImage=null;
//				}
//				p_sStr=null;
//				arrData=null;			
//			}
//			return objMsDest;
//		}

		#endregion		
		/// <summary>
		/// This function will decompress the string into memory stream.
		/// </summary>
		/// <param name="p_sStr">String to be decompressed.</param>
		/// <returns>Memory Stream after decompression</returns>
		public MemoryStream UnZIPBufferToMemoryStream(ref string p_sStr)
		{
			CompressedStream compStream = null ;

			MemoryStream objMsSrc=null;
			BinaryWriter objBw=null;
			MemoryStream objMsDest=null;
			byte[] buffer = new byte[ 32768 ];
			int bytesRead = 0;
			byte[] arrImage=null;

			try
			{
				if(p_sStr!=null  && p_sStr.Length>0)
				{
					arrImage=new byte[p_sStr.Length/2];

					DecodeBinaryString(ref p_sStr,ref arrImage);
					objMsSrc=new MemoryStream(arrImage);	
				
					using( compStream =  new CompressedStream( objMsSrc ) )
					{
						objMsDest=new MemoryStream();
						objBw =new BinaryWriter(objMsDest);

						while( ( bytesRead = compStream.Read( buffer, 0, buffer.Length ) ) > 0 )
						{
							objBw.Write( buffer, 0, bytesRead );
						}
						objBw.Flush();

						objMsDest.Seek( 0 , SeekOrigin.Begin );
					}										
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("ZipUtil.UnZIPBufferToMemoryStream.Error", m_iClientId), p_objException);
			}
			finally
			{
				if(objMsSrc!=null)
				{
					objMsSrc.Close();
					objMsSrc=null;
				}
				if(compStream!=null)
				{
					compStream.Close();
					compStream=null;
				}							
				p_sStr=null;				
			}
			return objMsDest;
		}

		
		#region Vaibhav Has commented the Code, Component One is replaced by XCeed
//		/// <summary>
//		/// This function will firstly compress the file and then encode the file using EncodeBinaryString() function.
//		/// </summary>
//		/// <param name="p_sFilePath">Path of the file to be compressed and encoded</param>
//		/// <returns>Encoded Binary String</returns>
//		public string ZipFile1(string p_sFilePath)
//		{
//			
//		    FileStream objFsSrc=null;
//		    MemoryStream objMsDest=new MemoryStream();
//			string sRetVal="";
//			BinaryReader objBr=null;
//			BinaryWriter objBw =null;
//			C1ZStreamWriter objSw=null;
//			byte[] arrPhoto=null;
//			
//			try
//			{ 
//				if(p_sFilePath!=null && p_sFilePath.Length>0)
//				{
//					objFsSrc=new FileStream(p_sFilePath,FileMode.Open,FileAccess.Read);
//					objBr = new BinaryReader(objFsSrc);
//					arrPhoto=objBr.ReadBytes((int)objFsSrc.Length);
//					objSw = new C1ZStreamWriter(objMsDest);
//					objBw = new BinaryWriter(objSw);
//					objBw.Write(arrPhoto);
//					objBw.Flush();
//					m_arrByte=objMsDest.ToArray();
//					sRetVal=EncodeBinaryString(ref m_arrByte);
//					
//				}
//			}
//			catch(RMAppException p_objException)
//			{
//				throw p_objException;
//			}
//			catch(Exception p_objException)
//			{
//				throw new RMAppException(Globalization.GetString("ZipUtil.ZipFile.Error"),p_objException);
//			}
//			finally
//			{
//				if(objMsDest!=null)
//				{
//					objMsDest.Close();
//					objMsDest=null;
//				}
//				if(objFsSrc!=null)
//				{
//					objFsSrc.Close();
//					objFsSrc=null;
//				}
//				if(objBr!=null)
//				{
//					objBr.Close();
//					objBr=null;
//				}
//				arrPhoto=null;
//			}
//			return sRetVal;
//		}

		#endregion

        /// <summary>
        /// Zips a file into a memory stream
        /// which is subsequently compressed and encoded into a string
        /// </summary>
        /// <param name="strFilePath">string containing the path to be compressed and encoded</param>
        /// <returns>compressed and encoded string</returns>
        public string ZipStream(string strFilePath)
        {
            return ZipFile(strFilePath);
        } // method: ZipStream

        /// <summary>
        /// Unzips a file into a Memory Stream
        /// </summary>
        /// <param name="strUnzippedContent">string containing the originally unzipped content</param>
        /// <returns>uncompressed and decoded Memory Stream</returns>
        public MemoryStream UnzipStream(string strUnzippedContent)
        {
            return UnZIPBufferToMemoryStream(ref strUnzippedContent);
        } // method: UnzipStream

        /// <summary>
        /// Compresses a specified file into a .zip file
        /// </summary>
        /// <param name="strFile">string containing a file path</param>
        /// <param name="strZipFile">string containing the name of the .zip file to create</param>
        public void CompressFile(string strFile, string strZipFile)
        {
            C1ZipFile objZip = new C1ZipFile();

            try
            {
                //Verify that the file exists at the specified path
                if (VerifyFilePath(strFile))
                {
                    //Add the specified file to the .zip file
                    objZip.Entries.Add(strFile);

                    //Create the specified .zip file
                    objZip.Create(strZipFile);
                } // if
            }
            catch (ZStreamException ex)
            {

                throw;
            }
            catch (IOException ex)
            {
                throw;
            } // catch

        } // method: CompressFile

        /// <summary>
        /// Compresses a Generic List string collection of file paths
        /// into a single .zip file
        /// </summary>
        /// <param name="arrFiles">Generic List string collection containing
        /// all of the required file paths</param>
        /// <param name="strZipFile">the name of the .zip file to be created</param>
        public void CompressFile(List<string> arrFiles, string strZipFile)
        {
            //Loop through all of the specified files and add them to the .zip file
            foreach (string  strFile in arrFiles)
            {
                CompressFile(strFile, strZipFile);
            }//foreach
        }//method: CompressFile()



		
		/// <summary>
		/// This function will firstly compress the file and then encode the file using EncodeBinaryString() function.
		/// </summary>
		/// <param name="p_sFilePath">Path of the file to be compressed and encoded</param>
		/// <returns>Encoded Binary String</returns>
		public string ZipFile(string p_sFilePath)
		{
			
			FileStream objFsSrc = null;
			MemoryStream objMsDest = null ;
			
			string sRetVal="";
			byte[] buffer = new byte[ 32768 ];
			int bytesRead = 0;


			try
			{ 
				if(p_sFilePath!=null && p_sFilePath.Length>0)
				{
					using( objFsSrc = new FileStream(p_sFilePath,FileMode.Open,FileAccess.Read) )
					{
						objMsDest = new MemoryStream();
                        
						using(CompressedStream compStream = new CompressedStream( objMsDest ) )
						{					
							while( ( bytesRead = objFsSrc.Read( buffer, 0, buffer.Length ) ) > 0 )
							{
								compStream.Write( buffer, 0, bytesRead );
							}			
						}
					}

					m_arrByte = objMsDest.ToArray();
					sRetVal = EncodeBinaryString( ref m_arrByte );
					
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("ZipUtil.ZipFile.Error", m_iClientId), p_objException);
			}
			finally
			{
				if(objMsDest!=null)
				{
					objMsDest.Close();
					objMsDest=null;
				}
				if(objFsSrc!=null)
				{
					objFsSrc.Close();
					objFsSrc=null;
				}
			}
			return sRetVal;
		}

        /// <summary>
        /// Determines whether or not a specified file exists at
        /// the specified file path
        /// </summary>
        /// <param name="strFilePath">string containing the path to the file to be verified</param>
        /// <returns>boolean indicating whether or not the file exists at the correct file path/file location</returns>
        internal bool VerifyFilePath(string strFilePath)
        {
            if (File.Exists(strFilePath))
            {
                return true;
            } // if

            return false;
        } // method: VerifyFilePath


		#region Vaibhav Has commented the Code, Component One is replaced by XCeed
//		/// Name			: CreateZipArchive
//		/// Author			: Aditya Babbar
//		/// Date Created	: 22-Dec-2004
//		/// ************************************************************
//		/// Amendment History
//		/// ************************************************************
//		/// Date Amended	*   Amendment   *    Author
//		///					*				*
//		///					*				*	
//		/// ************************************************************		
//		/// <summary>
//		/// Creates specified zip archive file.
//		/// </summary>
//		/// <param name="p_sZipArchiveFile">
//		///		Fully qualified zip archive file name.
//		///	</param>
//		/// <returns>0-Success</returns>
//		public int CreateZipArchive(string p_sZipArchiveFile)
//		{
//			int iReturnValue = 0;
//			C1ZipFile objC1ZipFile = null;			
//			
//			try
//			{ 
//				objC1ZipFile = new C1ZipFile();
//				objC1ZipFile.Create(p_sZipArchiveFile);
//				
//			}
//			catch(Exception p_objException)
//			{
//				throw new RMAppException(Globalization.GetString("ZipUtil.CreateZipArchive.Error"),p_objException);
//			}
//			finally
//			{
//				if(objC1ZipFile!=null)
//				{
//					objC1ZipFile.Close();
//					objC1ZipFile = null;
//				}
//			}
//			return iReturnValue;
//		}
//
//		/// Name			: AddFileToZipArchive
//		/// Author			: Aditya Babbar
//		/// Date Created	: 22-Dec-2004
//		/// ************************************************************
//		/// Amendment History
//		/// ************************************************************
//		/// Date Amended	*   Amendment   *    Author
//		///					*				*
//		///					*				*	
//		/// ************************************************************		
//		/// <summary>
//		/// Adds specified file to the zip archive. If the
//		/// archive does not exist, a new one is created.
//		/// </summary>
//		/// <param name="p_sFileToBeAdded">
//		///		Fully qualified name of the file to be added.
//		///	</param>
//		/// <param name="p_sZipArchiveFile">
//		///		Fully qualified zip archive file name.
//		///	</param>
//		/// <returns>0-Success</returns>
//		public int AddFileToZipArchive(string p_sFileToBeAdded,
//										string p_sZipArchiveFile)
//		{
//			int iReturnValue = 0;
//			C1ZipFile objC1ZipFile = null;			
//			
//			try
//			{ 
//				objC1ZipFile = new C1ZipFile();
//				if(File.Exists(p_sZipArchiveFile))
//				{
//					objC1ZipFile.Open(p_sZipArchiveFile);
//				}
//				else
//				{
//					objC1ZipFile.Create(p_sZipArchiveFile);
//				}
//
//				objC1ZipFile.Entries.Add(p_sFileToBeAdded);  
//			}			
//			catch(Exception p_objException)
//			{
//				throw new RMAppException(Globalization.GetString("ZipUtil.AddFileToZipArchive.Error"),p_objException);
//			}
//			finally
//			{
//				if(objC1ZipFile!=null)
//				{
//					objC1ZipFile.Close();
//					objC1ZipFile = null;
//				}
//			}
//			return iReturnValue;
//		}

		#endregion
        /// <summary>
		/// This function returns the path of unzipped file.
		/// </summary>
		/// <param name="p_EncodedString">Encoded and compressed file string</param>
		/// <returns>Path of the unzipped file</returns>
		public string UnZIPBufferToTempFile(ref string p_sEncodedString)
		{
			string sOutName="";
			MemoryStream objMsStream=null;
			FileStream objFileStream=null;
			try
			{
				sOutName=Path.GetTempFileName();
				objMsStream=UnZIPBufferToMemoryStream(ref p_sEncodedString);
				objFileStream=new FileStream(sOutName,FileMode.OpenOrCreate,FileAccess.ReadWrite);
				objMsStream.WriteTo(objFileStream);
				objMsStream.Close();
				objFileStream.Close();

			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("ZipUtil.UnZIPBufferToTempFile.Error", m_iClientId), p_objException);
			}
			finally
			{
				if(objMsStream!=null)
				{
                objMsStream.Close();
				objMsStream=null;
				}
				if(objFileStream!=null)
				{
				objFileStream.Close();
				objFileStream=null;
				}
				p_sEncodedString=null;
			}
     return sOutName;
		}
		/// <summary>
		/// This function will decompress the encoded string to a supplied file name. 
		/// </summary>
		/// <param name="p_sEncodedString">string to be decompressed</param>
		/// <param name="p_sOutFileName">Path of the file where encoded string will be decompressed</param>
		public void UnZIPBufferToFile(ref string p_sEncodedString,string p_sOutFileName)
		{
			MemoryStream objMsStream=null;
			FileStream objFileStream=null;
			try
			{
				objMsStream=UnZIPBufferToMemoryStream(ref p_sEncodedString);
				objFileStream=new FileStream(p_sOutFileName,FileMode.OpenOrCreate,FileAccess.Write);
				objMsStream.WriteTo(objFileStream);
				objMsStream.Close();
				objFileStream.Close();

			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("ZipUtil.UnZIPBufferToFile.Error", m_iClientId), p_objException);
			}
			finally
			{
				if(objMsStream!=null)
				{
					objMsStream.Close();
					objMsStream=null;
				}
				if(objFileStream!=null)
				{
					objFileStream.Close();
					objFileStream=null;
				}
				p_sEncodedString=null;
			}

		}		
		
		#endregion

		#region IDisposable Members
        /// <summary>
        /// This method will be called by the caller of this library to nullify module level variables.
       /// </summary>
		public void Dispose()
		{
			if(this.m_arrByte!=null)
			{
				this.m_arrByte=null;
			}
		}

		#endregion
	}
}
