using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
namespace Riskmaster.Application.SecurityManagement
{
	/// Name		: PwdPolicyParms
	/// Author		: Anurag Agarwal
	/// Date Created: 07/10/2006		
	///************************************************************
	/// Amendment History
	///************************************************************
	/// Date Amended    *  Amendment   *    Author
	///************************************************************
	/// <summary>
	/// This class handles the Password Parms table. Also looks for Account Locked/Unlocked features
	/// </summary>
	public class PwdPolicyParms
	{
        private int m_iClientId = 0;
		public PwdPolicyParms(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}

		
		/// Name		: LoadParmsData
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 Jul 2006		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Function will load the data from Password Parms Table
		/// </summary>
		/// <param name="p_objXMLDocument">Instance Document</param>
		/// <returns>Document filled with database values</returns>
		public XmlDocument LoadParmsData(XmlDocument p_objXMLDocument)
		{
			string sConnStr = "";
			string sQuery = "";
			DbConnection objConn = null;
			DbReader objReader = null;

            try
            {
                sConnStr = Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId);
                objConn = DbFactory.GetDbConnection(sConnStr);
                objConn.Open();

                sQuery = "SELECT * FROM PASSWORD_POLICY_PARMS";
                objReader = objConn.ExecuteReader(sQuery);
                if (objReader.Read())
                {
                    p_objXMLDocument.SelectSingleNode("//control[@name='PwdMinLength']").InnerText = objReader["PWD_LENGTH_MIN"].ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='PwdMaxLength']").InnerText = objReader["PWD_LENGTH_MAX"].ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='PwdMinAge']").InnerText = objReader["PWD_CHANGE_MIN_AGE"].ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='PwdExpireDays']").InnerText = objReader["PWD_EXPR_DAYS"].ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='PwdExpireWarningDays']").InnerText = objReader["PWD_EXPR_WARNING_DAYS"].ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='PreviousPwds']").InnerText = objReader["PREVIOUS_PWDS_TO_KEEP"].ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='PwdAttempts']").InnerText = objReader["ALLOWED_LOGIN_ATTEMPTS"].ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='PwdLockDuration']").InnerText = objReader["PWD_LOCKOUT_DUR"].ToString();
                    //p_objXMLDocument.SelectSingleNode("//control[@name='optMain']").InnerText = objReader["PWD_VALIDATE_OPTION"].ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='optMain']").Attributes["value"].Value = objReader["PWD_VALIDATE_OPTION"].ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='chkUseName']").InnerText = Conversion.ConvertStrToBool(objReader["PWD_CHECK_NAME"].ToString()).ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='chkLitUpperCase']").InnerText = Conversion.ConvertStrToBool(objReader["PWD_CHECK_UPPERCASE"].ToString()).ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='chkLitLowerCase']").InnerText = Conversion.ConvertStrToBool(objReader["PWD_CHECK_LOWERCASE"].ToString()).ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='chkLitNumerals']").InnerText = Conversion.ConvertStrToBool(objReader["PWD_CHECK_NUMERALS"].ToString()).ToString();
                    p_objXMLDocument.SelectSingleNode("//control[@name='chkLitSpecialChar']").InnerText = Conversion.ConvertStrToBool(objReader["PWD_CHECK_SPECIALCHAR"].ToString()).ToString();

                }
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (objConn != null)
                {                 
                    objConn.Dispose();                 
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }

			return p_objXMLDocument;
		}

		/// Name		: SaveParmsData
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 Jul 2006		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Function will save the data from Password Parms Table
		/// </summary>
		/// <param name="p_objXMLDocument">Instance Document</param>
		/// <returns>Document filled with database values</returns>
		public XmlDocument SaveParmsData(XmlDocument p_objXMLDocument)
		{
			string strPwdMinLength = "";
			string strPwdMaxLength = "";
			string strPwdMinAge = "";
			string strPwdExpireDays = "";
			string strPwdExpireWarningDays = "";
			string strPreviousPwds = "";
			string strPwdAttempts = "";
			string strPwdLockDuration = "";
			string strchkUseName = "";
			string strchkLitUpperCase = "";
			string strchkLitLowerCase = "";
			string strchkLitNumerals = "";
			string strchkLitSpecialChar = "";
			string strOptMain = "";
			string sConnStr = "";
			string sQuery = "";
			DbConnection objConn = null;
			DbReader objReader = null;

            try
            {
                strPwdMinLength = p_objXMLDocument.SelectSingleNode("//control[@name='PwdMinLength']").InnerText;
                strPwdMaxLength = p_objXMLDocument.SelectSingleNode("//control[@name='PwdMaxLength']").InnerText;
                strPwdMinAge = p_objXMLDocument.SelectSingleNode("//control[@name='PwdMinAge']").InnerText;
                strPwdExpireDays = p_objXMLDocument.SelectSingleNode("//control[@name='PwdExpireDays']").InnerText;
                strPwdExpireWarningDays = p_objXMLDocument.SelectSingleNode("//control[@name='PwdExpireWarningDays']").InnerText;
                strPreviousPwds = p_objXMLDocument.SelectSingleNode("//control[@name='PreviousPwds']").InnerText;
                strPwdAttempts = p_objXMLDocument.SelectSingleNode("//control[@name='PwdAttempts']").InnerText;
                strPwdLockDuration = p_objXMLDocument.SelectSingleNode("//control[@name='PwdLockDuration']").InnerText;
                strchkUseName = Conversion.ConvertBoolToInt(Conversion.ConvertStrToBool(p_objXMLDocument.SelectSingleNode("//control[@name='chkUseName']").InnerText), m_iClientId).ToString();
                //strOptMain = p_objXMLDocument.SelectSingleNode("//control[@name='optMain']").InnerText;
                strOptMain = p_objXMLDocument.SelectSingleNode("//control[@name='optMain']").Attributes["value"].Value;
                if (strOptMain == "4")
                {
                    strchkLitUpperCase = Conversion.ConvertBoolToInt(Conversion.ConvertStrToBool(p_objXMLDocument.SelectSingleNode("//control[@name='chkLitUpperCase']").InnerText), m_iClientId).ToString();
                    strchkLitLowerCase = Conversion.ConvertBoolToInt(Conversion.ConvertStrToBool(p_objXMLDocument.SelectSingleNode("//control[@name='chkLitLowerCase']").InnerText), m_iClientId).ToString();
                    strchkLitNumerals = Conversion.ConvertBoolToInt(Conversion.ConvertStrToBool(p_objXMLDocument.SelectSingleNode("//control[@name='chkLitNumerals']").InnerText), m_iClientId).ToString();
                    strchkLitSpecialChar = Conversion.ConvertBoolToInt(Conversion.ConvertStrToBool(p_objXMLDocument.SelectSingleNode("//control[@name='chkLitSpecialChar']").InnerText), m_iClientId).ToString();
                }
                else
                {
                    strchkLitUpperCase = "0";
                    strchkLitLowerCase = "0";
                    strchkLitNumerals = "0";
                    strchkLitSpecialChar = "0";
                }
                sConnStr = Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId);
                objConn = DbFactory.GetDbConnection(sConnStr);
                objConn.Open();

                sQuery = "SELECT * FROM PASSWORD_POLICY_PARMS";
                objReader = objConn.ExecuteReader(sQuery);
                if (objReader.Read())
                {
                    if (objReader != null)
                        objReader.Close();

                    sQuery = String.Format("UPDATE PASSWORD_POLICY_PARMS SET PWD_CHANGE_MIN_AGE = {0}, PWD_EXPR_DAYS = {2} ,PWD_EXPR_WARNING_DAYS = {3} ,PREVIOUS_PWDS_TO_KEEP = {4} ,ALLOWED_LOGIN_ATTEMPTS = {5}" +
                        " ,PWD_LENGTH_MIN = {6} ,PWD_LENGTH_MAX = {7} ,PWD_LOCKOUT_DUR = {8}, PWD_VALIDATE_OPTION = {9}, " +
                        " PWD_CHECK_NAME = {10}, PWD_CHECK_UPPERCASE = {11}, PWD_CHECK_LOWERCASE = {12}, PWD_CHECK_NUMERALS = {13}, " +
                        " PWD_CHECK_SPECIALCHAR = {14}", strPwdMinAge, "", strPwdExpireDays, strPwdExpireWarningDays,
                        strPreviousPwds, strPwdAttempts, strPwdMinLength, strPwdMaxLength, strPwdLockDuration, strOptMain,
                        strchkUseName, strchkLitUpperCase, strchkLitLowerCase, strchkLitNumerals, strchkLitSpecialChar);

                    objConn.ExecuteNonQuery(sQuery);
                }
                else
                {
                    if (objReader != null)
                        objReader.Close();

                    sQuery = String.Format("INSERT INTO PASSWORD_POLICY_PARMS(PWD_CHANGE_MIN_AGE,PWD_EXPR_DAYS," +
                        "PWD_EXPR_WARNING_DAYS,PREVIOUS_PWDS_TO_KEEP,ALLOWED_LOGIN_ATTEMPTS,PWD_LENGTH_MIN,PWD_LENGTH_MAX," +
                        "PWD_LOCKOUT_DUR,PWD_VALIDATE_OPTION,PWD_CHECK_NAME,PWD_CHECK_UPPERCASE,PWD_CHECK_LOWERCASE," +
                        "PWD_CHECK_NUMERALS,PWD_CHECK_SPECIALCHAR) VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}," +
                        "{12},{13})", strPwdMinAge, strPwdExpireDays, strPwdExpireWarningDays, strPreviousPwds,
                        strPwdAttempts, strPwdMinLength, strPwdMaxLength, strPwdLockDuration, strOptMain, strchkUseName,
                        strchkLitUpperCase, strchkLitLowerCase, strchkLitNumerals, strchkLitSpecialChar);

                    objConn.ExecuteNonQuery(sQuery);
                }

                if (objConn.State == System.Data.ConnectionState.Open)
                    objConn.Close();
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (objConn != null)
                {                 
                    objConn.Dispose();                 
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }

            }

			return p_objXMLDocument;
		}

		/// Name		: GetUsersList
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 Jul 2006		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Function will Load the list of Users with there status of locked/Unlocked
		/// </summary>
		/// <param name="p_objXMLDocument">Instance Document</param>
		/// <returns>Document filled with User list</returns>
		public void GetUsersList(ref XmlDocument p_objXMLDocument)
		{
			DbConnection objConn = null;
			DbReader objRow = null;
			string sSql="";
			try
			{
              
				sSql =  "Select UDTFirst.user_id , UDTFirst.dsnid, UDTFirst.login_name,UT.FIRST_NAME ,UT.LAST_NAME, UT.ACCOUNT_LOCK_STATUS from User_Details_Table UDTFirst, User_Table UT where dsnid = (select max(dsnid) from User_Details_Table UDTSecond where UDTFirst.user_id = UDTSecond.user_id) and UDTFirst.user_id = UT.user_id order by UT.FIRST_NAME ,UT.LAST_NAME" ;
				objConn=DbFactory.GetDbConnection(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId));
				objConn.Open();

				objRow = objConn.ExecuteReader(sSql);
				if(objRow != null)
				{
					while(objRow.Read())
					{  
						XmlElement objUserElement =  p_objXMLDocument.CreateElement("User");
						objUserElement.SetAttribute("loginname",Conversion.ConvertObjToStr(objRow["login_name"]));
    
						string sUserFullName = Conversion.ConvertObjToStr(objRow["LAST_NAME"]) + " " + Conversion.ConvertObjToStr(objRow["FIRST_NAME"]);
				       
						objUserElement.SetAttribute("userid",Conversion.ConvertObjToStr(objRow["user_id"]));
						objUserElement.SetAttribute("UserFullName",sUserFullName);
                        if (Conversion.ConvertObjToInt(objRow["ACCOUNT_LOCK_STATUS"], m_iClientId) == 1)
							objUserElement.SetAttribute("status","Y");
						else
							objUserElement.SetAttribute("status","N");
						p_objXMLDocument.SelectSingleNode("//Users").AppendChild(objUserElement);
					}
                  
				}

				if(objConn != null)
					objConn.Close();
               
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
				if(objRow!=null)
				{
					objRow.Close();
					objRow = null;
				}
				if(objConn != null)
					objConn = null;
			}
		}

		/// Name		: UnlockUserAccount
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 Jul 2006		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Function will Unlock the selected list of Users 
		/// </summary>
		/// <param name="p_objXMLDocument">Instance Document</param>
		/// <returns>returns success or failure</returns>
		public bool UnlockUserAccount(ref XmlDocument p_objXMLDocument)
		{
			string[] arrUserIds = null;
			string sUserIds  = "";
			string sTempUserIds  = "";
			DbConnection objConn = null;

			try
			{

				objConn=DbFactory.GetDbConnection(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId));
				objConn.Open();

				sTempUserIds = p_objXMLDocument.SelectSingleNode("//SelectedUserIds").InnerText.Trim();
				arrUserIds = sTempUserIds.Split(',');
				
				foreach( string sUserId in arrUserIds )
				{
					if( sUserId.Trim()!="" )
					{
						if( sUserIds == "" )
						{
							sUserIds = sUserId ;
						}
						else
						{
							sUserIds = sUserIds + ","  +sUserId ;
						}
					}
				}

                //Raman Bhatia MITS 18594 : Lockout Duration does not work
                //Added DTTM_ACCOUNT_LOCK to maintain datetime user was locked

                string sSql = "Update User_Table SET ACCOUNT_LOCK_STATUS = 0 , DTTM_ACCOUNT_LOCK = '' WHERE user_id in ( " +  sUserIds + " )" ;
				objConn.ExecuteNonQuery(sSql); 
				
				arrUserIds = null;
				arrUserIds = sUserIds.Split(',');
				foreach( string sUserId in arrUserIds )
				{
					if( sUserId.Trim()!="" )
					{
						objConn.ExecuteNonQuery("INSERT INTO LOGIN_HISTORY (USER_ID, DTTM_LOGIN, STATUS)" + 
							" VALUES(" + sUserId + ",'" + Conversion.ToDbDateTime(DateTime.Now) + "',1)");
					}
				}

				if(objConn != null)
					objConn.Close();
			}
		
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
                if (objConn != null)
                {
                    objConn.Dispose();
                }
					objConn = null;
			}
			return true;
		}

		/// Name		: LockUserAccount
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 Jul 2006		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Function will Lock the selected list of Users 
		/// </summary>
		/// <param name="p_objXMLDocument">Instance Document</param>
		/// <returns>returns success or failure</returns>
		public bool LockUserAccount(ref XmlDocument p_objXMLDocument)
		{
			string[] arrUserIds = null;
			string sTempUserIds  = "";
			string sUserIds  = "";
			DbConnection objConn = null;

			try
			{

                objConn = DbFactory.GetDbConnection(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId));
				objConn.Open();

				sTempUserIds = p_objXMLDocument.SelectSingleNode("//SelectedUserIds").InnerText.Trim();
				arrUserIds = sTempUserIds.Split(',');
				
				foreach( string sUserId in arrUserIds )
				{
					if( sUserId.Trim()!="" )
					{
						if( sUserIds == "" )
						{
							sUserIds = sUserId ;
						}
						else
						{
							sUserIds = sUserIds + ","  +sUserId ;
						}
					}
				}

                //Raman Bhatia MITS 18594 : Lockout Duration does not work
                //Added DTTM_ACCOUNT_LOCK to maintain datetime user was locked

                string sCurrentDateTime = Conversion.ToDbDateTime(DateTime.Now);

				string sSql = "Update User_Table SET ACCOUNT_LOCK_STATUS = 1 , DTTM_ACCOUNT_LOCK = " + sCurrentDateTime + " WHERE user_id in ( " +  sUserIds + " )" ;
				objConn.ExecuteNonQuery(sSql); 

				if(objConn != null)
					objConn.Close();
			}
		
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
                if (objConn != null)
                {
                    objConn.Dispose();
                }
				objConn = null;
			}
			return true;
		}
	}
}
