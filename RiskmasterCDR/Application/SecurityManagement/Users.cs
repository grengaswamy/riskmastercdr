using System;
using Riskmaster;
using Riskmaster.Db;
using System.Data;
using Riskmaster.Common;
using System.Collections;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using System.Xml;
using System.Collections.Generic;
using System.Text;


namespace Riskmaster.Application.SecurityManagement
{
	/// <summary>
	/// Riskmaster.Application.SecurityManagement.Users class classifies User�s details on context of Riskmaster, 
	/// it contain all the information about the user like Name,City,Country Phone etc
	/// </summary>
	public class Users : User
	{
		/// <summary>
		/// This is code access public key.
		/// </summary>
		private const string CODE_ACCESS_PUBLIC_KEY = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";

        /// <summary>
        /// Private Variable for Client Id
        /// </summary>
        private int m_iClientId = 0;

        
		/// <summary>
		/// Riskmaster.Application.SecurityManagement.Users is the default constructor.
		/// </summary>
		public Users(int p_iClientId) : base(p_iClientId)
        {// ClientID added as required argument to avoid conflict with UserId
            m_iClientId = p_iClientId;
			base.EnforceCheckSum = false;
		}

		/// <summary>
		/// Riskmaster.Application.SecurityManagement.User constructor with parameter. 
		/// This constructor calls the function Load() and serves as a wrapper.
		/// </summary>
		/// <param name="p_iUserId">Value of the userid corresponding to which object will be populated.</param>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
        public Users(int p_iUserId, int p_iClientId)
            : base(p_iUserId, p_iClientId)
        {// ClientID added as required argument to avoid conflict with UserId
			try
			{
				base.EnforceCheckSum = false;
                m_iClientId = p_iClientId;
                this.Load(p_iUserId);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
		}

        //gagnihotri MITS 11995 Changes made for Audit table
        public Users(string p_sUserName, int p_iClientId)
            : base(p_sUserName, p_iClientId)
        {
            m_iClientId = p_iClientId;
            base.EnforceCheckSum = false;
        }

		/// <summary>
		/// This function call loads the user collection(m_arrlstUserCol) defined in PublicFunctions.cs. 
		/// First Code Access Security Usage.
		/// This attribute requires that any caller to have a strongname proving that it's CSC Riskmaster Code. 
		/// To ensure that your assembly has an appropriate strongname, place the following line in the 
		/// AssemblyInfo.cs file:
		/// [assembly: AssemblyKeyFile("..\\..\\..\\Riskmaster.snk")].Here the path of the .snk file has to be specified relative to your project.
		/// </summary>
		/// <returns>True if successful else throws error.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool Load(ref ArrayList p_arrlstUserCol)
		{
			bool bRetVal=false;
			Users objUser = null;
			
			using (DbReader objReader = DbFactory.ExecuteReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT * FROM USER_TABLE ORDER BY FIRST_NAME,LAST_NAME"))
            {
                //This loop will load all the persisted Users object to a collection in memory.
                while (objReader.Read())
                {
                    objUser = new Users(m_iClientId);
                    objUser.UserId = objReader.GetInt("USER_ID");
                    objUser.Address1 = objReader.GetString("ADDRESS1");
                    objUser.Address2 = objReader.GetString("ADDRESS2");
                    objUser.City = objReader.GetString("CITY");
                    objUser.Country = objReader.GetString("COUNTRY");
                    objUser.Email = objReader.GetString("EMAIL_ADDR");
                    objUser.FirstName = objReader.GetString("FIRST_NAME");
                    objUser.NlsCode = objReader.GetInt("NLS_CODE");
                    objUser.LastName = objReader.GetString("LAST_NAME");
                    objUser.HomePhone = objReader.GetString("HOME_PHONE");
                    objUser.OfficePhone = objReader.GetString("OFFICE_PHONE");
                    objUser.State = objReader.GetString("STATE_ABBR");
                    objUser.Title = objReader.GetString("TITLE");
                    objUser.ZipCode = objReader.GetString("ZIP_CODE");
                    objUser.ManagerId = objReader.GetInt("MANAGER_ID");
                    objUser.CompanyName = objReader.GetString("COMPANY_NAME");
                    //for R5.. fill SMS access flag too
                    objUser.SMSAccess = objReader.GetBoolean("IS_SMS_USER");
                    //skhare7 MITS:22374
                    objUser.UPSAccess = objReader.GetBoolean("IS_USRPVG_ACCESS");
                    //skhare7 MITS:22374
                    p_arrlstUserCol.Add(objUser);
                }//while

                bRetVal = true;
            }//using
				
			return bRetVal;
		}

        //srajindersin MITS 30263 dt 11/27/2012
        /// <summary>
        /// This function call returns User dataset
        /// First Code Access Security Usage.
        /// This attribute requires that any caller to have a strongname proving that it's CSC Riskmaster Code. 
        /// To ensure that your assembly has an appropriate strongname, place the following line in the 
        /// AssemblyInfo.cs file:
        /// [assembly: AssemblyKeyFile("..\\..\\..\\Riskmaster.snk")].Here the path of the .snk file has to be specified relative to your project.
        /// </summary>
        /// <returns>True if successful else throws error.</returns>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = CODE_ACCESS_PUBLIC_KEY)]
        public DataSet Load()
        {
            DataSet dsUser = new DataSet();
            dsUser = DbFactory.ExecuteDataSet(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT USER_ID,FIRST_NAME,LAST_NAME,COMPANY_NAME FROM USER_TABLE ORDER BY FIRST_NAME,LAST_NAME", new Dictionary<string, string>(),m_iClientId);

            return dsUser;
        }

		/// <summary>
		/// This function call actually removes the user from database.One can directly call this function by passing in the User_Id to be deleted 
		/// or load this class with User_Id and call its Remove() function to do the same job.
		/// </summary>
		/// <param name="p_iUserId"></param>
		/// <returns>True if successful else throws error.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool Remove(int p_iUserId)
		{  
			bool bRetVal=false;
			string sSQL="";
			DbConnection objConn = null; 
			DbTransaction objTrans=null;
			DataSet objUsersDS = null;
			int iNoOfRowsAffected=0;
			try
			{ 
				objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
				objConn.Open();
				DbCommand objCommand=objConn.CreateCommand();
				objTrans=objConn.BeginTransaction();
				objCommand.Transaction=objTrans;

				// Loop through all users that had this manager and clear their manager value.
				//  This must be done with objects instead of direct SQL because of CRC.
				sSQL = "SELECT USER_ID FROM USER_TABLE WHERE MANAGER_ID = " + p_iUserId;
				objUsersDS = DbFactory.GetDataSet( SecurityDatabase.GetSecurityDsn(m_iClientId), sSQL,m_iClientId);
				if(objUsersDS!=null && objUsersDS.Tables.Count>0)
				{
					foreach(DataRow objRow in objUsersDS.Tables[0].Rows)
					{  
						Riskmaster.Security.User objUser=new Riskmaster.Security.User(m_iClientId);
                        objUser.Load(Conversion.ConvertObjToInt(objRow["USER_ID"], m_iClientId));
						objUser.ManagerId = 0;
						objUser.Save();
					}
                  
				}
				objUsersDS = null;

				// Remove the user's record from the database
				sSQL = "DELETE FROM USER_TABLE WHERE USER_ID=" + p_iUserId;
				objCommand.CommandText=sSQL;
				iNoOfRowsAffected=objCommand.ExecuteNonQuery();
				sSQL = "DELETE FROM USER_DETAILS_TABLE WHERE USER_ID=" + p_iUserId;
				objCommand.CommandText=sSQL;
				iNoOfRowsAffected+=objCommand.ExecuteNonQuery();

				// JP 5/16/2007  Bad code. Invalidates CRC and gives error.     sSQL = "UPDATE USER_TABLE SET MANAGER_ID=0 WHERE MANAGER_ID=" + p_iUserId;
				// JP 5/16/2007 objCommand.CommandText=sSQL;
				// JP 5/16/2007 iNoOfRowsAffected+=objCommand.ExecuteNonQuery();
				
				objTrans.Commit();
				bRetVal=true;
				
			}
			catch(Exception p_objException)
			{
				if(objTrans!=null)
					objTrans.Rollback();
                throw new DataModelException(Globalization.GetString("Users.Remove.RemoveError", m_iClientId), p_objException);
			}
			finally
			{
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
				if(objTrans!=null)
				{
					objTrans.Dispose();
				}
                if (objUsersDS != null)
                {
                    objUsersDS.Dispose();
                }
			}
			return bRetVal;
		}

		/// <summary>
		/// This function call saves the user default screen layout in the database.
		/// </summary>
		/// <param name="p_iUserId"></param>
		/// <param name="p_iDsnId"></param>
		/// <returns>Nothing</returns>
		public static void SaveUserLayout(int p_iUserId, int p_iDsnId, int p_iClientId)
		{
            string sConnStr = string.Empty;
            StringBuilder sSQL = new StringBuilder();
			string sUserID=p_iUserId.ToString();
			int iUserCount=0;			
			DbConnection objConn = null; 	
			DataSources objDataSource = null;
            bool blnParsed = false;
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            
			try
			{				
				objDataSource=new DataSources(p_iDsnId,false, p_iClientId);							
				sConnStr = objDataSource.ConnectionString;

                sSQL.AppendFormat(string.Format("SELECT COUNT(Member_ID) FROM NET_LAYOUTS_MEMBERS WHERE MEMBER_ID={0}", p_iUserId));


                iUserCount = Conversion.CastToType<int>(DbFactory.ExecuteScalar(sConnStr, sSQL.ToString()).ToString(), out blnParsed);
                
				if(iUserCount == 0)
				{
                    sSQL.Length = 0; //Clear out the previous string contents

					sSQL.Append("INSERT INTO NET_LAYOUTS_MEMBERS(LAYOUT_ID, MEMBER_ID, ISGROUP) ");
                    sSQL.AppendFormat(string.Format("VALUES({0},{1},{2})", "~LAYOUT_ID~", "~MEMBER_ID~", "~ISGROUP~"));

                    dictParams.Add("LAYOUT_ID", "1");
                    dictParams.Add("MEMBER_ID", sUserID);
                    dictParams.Add("ISGROUP", "0");

					DbFactory.ExecuteNonQuery(sConnStr, sSQL.ToString(), dictParams);
				}				
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Users.SaveUserLayout.SaveError",p_iClientId), p_objException);
			}
			finally
			{
				if(objConn!=null)
				{
					objConn.Dispose();
				}
				objDataSource = null;
			}
			   
		}
	}//class Users

    /// <summary>
    /// Generic Users collection which adds Users required for Security Management System
    /// </summary>
    /// <typeparam name="T">generic class type parameter</typeparam>
    public class Users<T> : ICollection<T> where T: User
    {
        List<T> m_arrUsers = new List<T>();

        #region ICollection<T> Members

        public void Add(T item)
        {
            m_arrUsers.Add(item);
        }

        public void Clear()
        {
            m_arrUsers.Clear();
        }

        public bool Contains(T item)
        {
            return m_arrUsers.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            m_arrUsers.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get 
            {
                return m_arrUsers.Count;
            }
        }

        public bool IsReadOnly
        {
            get 
            {
                return false;
            }
        }

        public bool Remove(T item)
        {
            return m_arrUsers.Remove(item);
        }

        #endregion

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return m_arrUsers.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_arrUsers.GetEnumerator();
        }

        #endregion
    }//class Users
}
