using System;
using System.Collections.Generic;
using Riskmaster.Db;
using System.Xml;
using System.Text;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Collections;
using System.Data;
using Riskmaster.Settings;


namespace Riskmaster.Application.SecurityManagement
{
    public class GroupModulePermission
    {
        /// <summary>
        /// delimiter for separating function id
        /// </summary>
        private const string m_sFuncIdDelimiter = "|";

        private string m_sUserName = "";        //gagnihotri MITS 11995 Changes made for Audit table
        // npadhy Changed the Constant names and Key as per new FDM Constants and keys
        //tanwar2 - mits 30910 - start
        private const int GC_DED = 190;
        private const int WC_DED = 3040;
        private const int GC_AGG_EDIT = 300;
        private const int WC_AGG_EDIT = 5800;
        //tanwar2 - mits 30910 - end

        private int m_iClientId = 0; //Ash - cloud

        /// <summary>
        /// gagnihotri MITS 11995 Changes made for Audit table
        /// Default Constructor
        /// </summary>
        public GroupModulePermission(int p_iClientId)
        {
            m_iClientId = p_iClientId;
        }

        /// <summary>
        /// gagnihotri MITS 11995 Changes made for Audit table
        /// Overloaded Constructor
        /// </summary>
        public GroupModulePermission(string p_sUserName, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_iClientId = p_iClientId;
        }

        /// <summary>
        /// get the permissions for child function nodes.
        /// </summary>
        /// <param name="p_objDocIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <returns></returns>
        public bool GetChildPermissions(XmlDocument p_objDocIn, ref XmlDocument p_objXmlOut)
        {
            string sSQL = string.Empty;
            int iFunctionID = 0;
            string sTempConnString = "";
            StringBuilder sbReturnValue = new StringBuilder();
            StringBuilder sbFunctionIDs = new StringBuilder();

            Dictionary<int, int> dicFunctionChildCount = new Dictionary<int, int>();
            Dictionary<int, string> dicFunctionName = new Dictionary<int, string>();
            Dictionary<int, int> dicFunctionAllowed = new Dictionary<int, int>();
            int iParentID = 0;
            //tanwar2 - mits 30910 - start
            SysSettings oSettings = null;
            ColLobSettings oLobSettings = null;
            int iLobCode = 0;
            //tanwar2 - mits 30910 - end

            try
            {
                //Get DSN ID and Group ID
                int iDsnID = 0;
                string sGroupID = string.Empty;
                GetDsnGroupIDs(p_objDocIn, out iDsnID, out sGroupID);

                DataSources objDataSource = new DataSources(iDsnID, false, m_iClientId);

                //Raman -- for R5
                XmlNode oParentId = null;
                bool bIsModulePermissionEnabled = objDataSource.Status;
                if (bIsModulePermissionEnabled == false)
                {
                    oParentId = p_objDocIn.SelectSingleNode("//parentFuncId");
                    if (oParentId != null)
                    {
                        ((XmlElement)oParentId).SetAttribute("ModulePermissionDisabled", "true");
                        p_objXmlOut = p_objDocIn;
                        return true;
                    }
                }
                string sRMConnectionString = objDataSource.ConnectionString;
                //vsharma205 starts- Jira 7767
                oSettings = new SysSettings(sRMConnectionString, m_iClientId);
                //vsharma205 ends- Jira 7767
                string sSecConnectionString = Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId);

                oParentId = p_objDocIn.SelectSingleNode("//parentFuncId");
                if (oParentId != null)
                {
                    string sParentID = oParentId.InnerText;
                    if (string.IsNullOrEmpty(sParentID))
                        iParentID = 0;
                    else
                        iParentID = int.Parse(sParentID);
                }

                //For Admin Tracking tables. *20 leaves space for all the individual
                //Admin Tracking table permissions
                if (iParentID == 5000000)
                {
                    sSQL = "SELECT 5000000 + GLOSSARY.TABLE_ID*20, GLOSSARY_TEXT.TABLE_NAME, 1 FROM GLOSSARY " +
                            " INNER JOIN GLOSSARY_TEXT ON GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID " +
                            "WHERE GLOSSARY_TYPE_CODE = 468 ORDER BY GLOSSARY_TEXT.TABLE_NAME";
                    sSecConnectionString = sRMConnectionString;
                }
                //get individule permissions for each Admin Tracking table, and assume
                // there is no child permissions for these individule permissions.
                else if ((iParentID > 5000000) && (iParentID < 6000000))
                {
                    //zmohammad MITs 34033 Start : Checking for table names in all DSNs so they dont show up.
                    DataTable dsnList = Riskmaster.Security.RiskmasterDatabase.GetRiskmasterDatabases(m_iClientId);
                    DbReader oDbReader = null;
                    StringBuilder sbtemp = new StringBuilder();
                    foreach (DataRow dr in dsnList.Rows)
                    {
                        sTempConnString = dr[2].ToString();
                       
                        sSQL = "SELECT 5000000 + GLOSSARY.TABLE_ID*20 AS FUNC_ID FROM GLOSSARY " +
                                " INNER JOIN GLOSSARY_TEXT ON GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID " +
                                "WHERE GLOSSARY_TYPE_CODE = 468 ORDER BY GLOSSARY_TEXT.TABLE_NAME";
                        try
                        {
                            oDbReader = DbFactory.GetDbReader(sTempConnString, sSQL);
                        }
                        catch
                        {
                            continue;
                        }
                        {
                            while (oDbReader.Read())
                            {
                                if (sbtemp.Length > 0)
                                    sbtemp.Append(",");
                                sbtemp.Append(oDbReader.GetInt(0).ToString());
                            }
                        }
                    }
                    //zmohammad MITs 34033 End : Checking for table names in all DSNs so they dont show up.
                    int iTableId = iParentID - 5000000;
                    if (sbtemp.Length>0)
                        sSQL = "SELECT FUNC_ID+" + iTableId + ", FUNCTION_NAME, 0 FROM FUNCTION_LIST " +
                           "WHERE PARENT_ID=5000000 AND FUNC_ID NOT IN ("+sbtemp.ToString()+") ORDER BY FUNCTION_NAME";
                    else
                        sSQL = "SELECT FUNC_ID+" + iTableId + ", FUNCTION_NAME, 0 FROM FUNCTION_LIST " +
                            "WHERE PARENT_ID=5000000 ORDER BY FUNCTION_NAME";
                }
                else
                {
                    /*Vsharma205 starts Jira-7767  */

                    /* Obsolete
                      sSQL = "SELECT f.FUNC_ID, f.FUNCTION_NAME, COUNT(fc.FUNC_ID) FROM FUNCTION_LIST f " +
                        "LEFT OUTER JOIN FUNCTION_LIST fc ON f.FUNC_ID = fc.PARENT_ID " +
                        "WHERE f.PARENT_ID=" + iParentID.ToString() + " GROUP BY f.FUNC_ID, f.FUNCTION_NAME ";
                     */
                    
                    /*
                    Description:-  "and f.FUNC_ID not in" check has been used to filter/hide some functions in SMS serach and permissions tree view , based on  Entity role ON/OFF
                    funcID for Search tree: 8000025,	8000003,	8000010,	8000008,	8000009,
                    funcID for Employee Maintenanace: 14508,	14506,	14503,	14509,	17513,	14530,	14505,	14507,	15570,	14510,
                    funcID for Phisical maintenanace: 21208,	21206,	21203,	21209,	21580,	21211,	21207,	21370,
                    funcID fro Driver Maintenanace: 25208,	25206,	25203,	25209,	25580,	25211,	25207,	25370,
                    func ID for Medical maintenanace: 21308,	21306,	21303,	21309,	22360,	21311,	21307,	21380,
                    func ID for Patient Tracking Maintenance: 21008,	21006,	21003,	21009,	21513,	21011,	21007,	21512,*/
                    sSQL = "SELECT f.FUNC_ID, f.FUNCTION_NAME, COUNT(fc.FUNC_ID) FROM FUNCTION_LIST f " +
                        "LEFT OUTER JOIN FUNCTION_LIST fc ON f.FUNC_ID = fc.PARENT_ID " +
                        "WHERE f.PARENT_ID=" + iParentID.ToString();
                    //Rkatyal4 : JIRA 13647 start
                    if (oSettings.UseEntityRole)
                    {
                        sSQL = sSQL +
                        "and f.FUNC_ID not in (14000, 14508,	14506,	14503,	14509,	17513,	14530,	14505,	14507,	15570,	14510,	21208,	21206,	21203,	21209,	21580,	21211,	21207,	21370,	25208,	25206,	25203,	25209,	25580,	25211,	25207,	25370,	21308,	21306,	21303,	21309,	22360,	21311,	21307,	21380,	21008,	21006,	21003,	21009,	21513,	21011,	21007,	21512)";
                    }
                    sSQL = sSQL +
                        " GROUP BY f.FUNC_ID, f.FUNCTION_NAME ";

                    /*Vsharma205 ends Jira-7767  */
                    //Rkatyal4 : JIRA 13647 end
                    if (iParentID == 0)
                        sSQL += " ORDER BY f.FUNC_ID";
                    else
                        sSQL += " ORDER BY f.FUNCTION_NAME";
                }
                using (DbReader oDbReader = DbFactory.GetDbReader(sSecConnectionString, sSQL))
                {
                    while (oDbReader.Read())
                    {
                        iFunctionID = oDbReader.GetInt(0);
                        iLobCode = 0;
                        //tanwar2 - mits 30910 - start
                        switch (iFunctionID)
                        {
                            case GC_DED:
                            case GC_AGG_EDIT:
                                iLobCode = 241;
                                break;
                            case WC_DED:
                            case WC_AGG_EDIT:
                                iLobCode = 243;
                                break;
                        }
                        if (iLobCode>0)
                        {
                            oSettings = new SysSettings(sRMConnectionString, m_iClientId); //ddhiman added: Cloud changes, 10/10/2014
                            oLobSettings = new ColLobSettings(sRMConnectionString, m_iClientId);
                            if (!(oSettings.MultiCovgPerClm == -1 && oSettings.UsePolicyInterface && oLobSettings[iLobCode].ApplyDedToPaymentsFlag == -1))
                            {
                                continue;
                            }
                            //ddhiman 10/16/2014
                            if (oSettings.UsePolicyInterface && oLobSettings[iLobCode].ApplyDedToPaymentsFlag == -1 && oSettings.UsePolicyInterface && oLobSettings[iLobCode].SharedAggDedFlag != -1)
                            {
                                if (iFunctionID == GC_AGG_EDIT || iFunctionID == WC_AGG_EDIT)
                                {
                                    continue;
                                }
                            }
                            //End ddhiman
                        }
                        //tanwar2 - mits 30910 - end

                        if (sbFunctionIDs.Length > 0)
                            sbFunctionIDs.Append(",");
                        sbFunctionIDs.Append(iFunctionID.ToString());

                        string sFunctionName = oDbReader.GetString(1);
                        int iCount = oDbReader.GetInt(2);
                        dicFunctionName.Add(iFunctionID, sFunctionName);
                        dicFunctionChildCount.Add(iFunctionID, iCount);
                        oSettings = new SysSettings(sRMConnectionString, m_iClientId);
                    }
                }

                //check if the child function permission is set
                sSQL = "SELECT FUNC_ID FROM GROUP_PERMISSIONS WHERE GROUP_ID=" + sGroupID +
                    " AND FUNC_ID IN (" + sbFunctionIDs.ToString() + ")";
                using (DbReader oDbReader = DbFactory.GetDbReader(sRMConnectionString, sSQL))
                {
                    while (oDbReader.Read())
                    {
                        iFunctionID = oDbReader.GetInt("FUNC_ID");
                        //jramkumar for MITS 34843
                        if(!dicFunctionAllowed.ContainsKey(iFunctionID))
                            dicFunctionAllowed.Add(iFunctionID, 1);
                    }
                }

                //get the return value string
                sbReturnValue.Append("<functions>");
                foreach (KeyValuePair<int, string> kvp in dicFunctionName)
                {
                    sbReturnValue.Append("<function id='").Append(kvp.Key.ToString()).Append("'");
                    sbReturnValue.Append(" name='").Append(kvp.Value.Replace("'", "&apos;")).Append("'");
                    sbReturnValue.Append(" childCount='").Append(dicFunctionChildCount[kvp.Key].ToString()).Append("'");
                    if (dicFunctionAllowed.ContainsKey(kvp.Key))
                    {
                        sbReturnValue.Append(" allowed='1'");
                    }
                    else
                    {
                        sbReturnValue.Append(" allowed='0'");
                    }
                    sbReturnValue.Append("/>");
                }
                sbReturnValue.Append("</functions>");

                XmlNode oRetNode = p_objDocIn.SelectSingleNode("//RetVal");
                if (oRetNode != null)
                {
                    oRetNode.InnerText = sbReturnValue.ToString();
                }
                p_objXmlOut = p_objDocIn;
            }
            catch (Exception e)
            {
                throw new RMAppException
                    (Globalization.GetString("GroupModulePermissions.GetChildPermissions", m_iClientId), e);
            }
            
            return true;
        }

        
        
        //abansal23: Clone for module security groups Starts
        /// <summary>
        /// Copy all the permissions from existing module group to clone one
        /// </summary>
        /// <param name="p_objDocIn">Input xml</param>
        /// <param name="p_iGroupId">parent group Id</param>
        /// <param name="c_iGroupId">Clone Group Id</param>
        /// <returns></returns>
        public bool CopyClonePermissions(XmlDocument p_objDocIn,int p_iGroupId, int c_iGroupId)
        {
            string sSQL = string.Empty;
            int iCount = 0;
            int iCount2 = 0;

            try
            {
                string DsnId = ((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdDBId']")).InnerText;
                DataSources objDataSource = new DataSources(Convert.ToInt16(DsnId), false,m_iClientId);
                string sRMConnectionString = objDataSource.ConnectionString;


                //check if the child function permission is set
                sSQL = "SELECT FUNC_ID FROM GROUP_PERMISSIONS WHERE GROUP_ID=" + p_iGroupId;
                
                //Fetch permissions assigned to parent group
                ArrayList arGrantedFuncIDs = new ArrayList();
                using (DbReader oDbReader = DbFactory.GetDbReader(sRMConnectionString, sSQL))
                {
                    while (oDbReader.Read())
                    {
                        arGrantedFuncIDs.Add(oDbReader.GetInt("FUNC_ID").ToString());
                    }
                }

                if (arGrantedFuncIDs.Count> 0)
                {
                    using (DbConnection oConnection = DbFactory.GetDbConnection(sRMConnectionString))
                    {
                        oConnection.Open();
                        sSQL = "INSERT INTO GROUP_PERMISSIONS(GROUP_ID, FUNC_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES(";
                        for(iCount=0;iCount<arGrantedFuncIDs.Count;iCount++)
                        {
                            string sInsertSQL = sSQL + c_iGroupId + ", " + arGrantedFuncIDs[iCount].ToString() + ",'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
                            iCount2 = oConnection.ExecuteNonQuery(sInsertSQL);
                        }
                    }
                }
            }
            catch (Exception e)
            {   
                throw new RMAppException
                    (Globalization.GetString("GroupModulePermissions.CopyClonePermissions", m_iClientId), e);
            }
            return true;
        }
        //abansal23: Clone for module security groups Ends





        /// <summary>
        /// Update permissions for the module.
        /// </summary>
        /// <param name="p_objDocIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <returns></returns>
        public bool UpdateModulePermissions(XmlDocument p_objDocIn, ref XmlDocument p_objXmlOut)
        {
            try
            {
                //Get DSN ID and Group ID
                int iDsnID = 0;
                string sGroupID = string.Empty;
                GetDsnGroupIDs(p_objDocIn, out iDsnID, out sGroupID);

                DataSources objDataSource = new DataSources(iDsnID, false,m_iClientId);
                string sRMConnectionString = objDataSource.ConnectionString;
                string sSecConnectionString = Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId);

                //Update the permissions which are granted/revoked individually.
                GrantIndividualPermissions(p_objDocIn, sGroupID, sSecConnectionString, sRMConnectionString);
                RevokeIndividualPermissions(p_objDocIn, sGroupID, sSecConnectionString, sRMConnectionString);

                //Update the permissions which are granted/revoked for all child nodes.
                GrantBatchPermissions(p_objDocIn, sGroupID, sSecConnectionString, sRMConnectionString);
                RevokeBatchPermissions(p_objDocIn, sGroupID, sSecConnectionString, sRMConnectionString);
            }
            catch (Exception e)
            {
                throw new RMAppException
                    (Globalization.GetString("GroupModulePermissions.UpdateModulePermissions", m_iClientId), e);
            }
            
            return true;
        }


        /// <summary>
        /// Save permission changes caused by granting each individual
        /// permissions by the user. A user could revoke and then grant the same
        /// permissions, so we may need to delete the permission entry before inserting
        /// it.
        /// </summary>
        /// <param name="p_objDocIn">input message</param>
        /// <param name="sGroupId">The module group permissions</param>
        /// <param name="sSecConnectionString">security databasea connection string</param>
        /// <param name="sRMConnectionString">Riskmaster database connection string</param>
        private void GrantIndividualPermissions(XmlDocument p_objDocIn, string sGroupId, string sSecConnectionString, string sRMConnectionString)
        {
            string sSQL = string.Empty;
            int iCount = 0;
            string sUserTableName = string.Empty;   //mbahl3 mits25625
            Dictionary<string, object> dictParams = new Dictionary<string, object>();   //mbahl3 mits25625
           
            try
            {
                //Add newly granted permissions to the database
                string sGrantedFuncIDs = GetGrantRevokeFunctionIDs(p_objDocIn, "grantedFuncIDs");
                if (sGrantedFuncIDs.Length > 0)
                {
                    string[] arGrantedFuncIDs = sGrantedFuncIDs.Split(m_sFuncIdDelimiter.ToCharArray());
                    using (DbConnection oConnection = DbFactory.GetDbConnection(sRMConnectionString))
                    {
                        oConnection.Open();

                        //try to delete the record first in case the user uncheck/check the same permision
                        sGrantedFuncIDs = sGrantedFuncIDs.Replace(m_sFuncIdDelimiter, ",");

                        string sDeleteSQL = "DELETE FROM GROUP_PERMISSIONS WHERE FUNC_ID IN (" + sGrantedFuncIDs + ")" +
                                            " AND GROUP_ID=" + sGroupId;
                        iCount = oConnection.ExecuteNonQuery(sDeleteSQL);
                        //gagnihotri MITS 11995 Changes made for Audit table
                        sSQL = "INSERT INTO GROUP_PERMISSIONS(GROUP_ID, FUNC_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES(";
                        foreach (string sFuncID in arGrantedFuncIDs)
                        {
                            //gagnihotri MITS 11995 Changes made for Audit table
                            string sInsertSQL = sSQL + sGroupId + ", " + sFuncID + ",'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
                            iCount = oConnection.ExecuteNonQuery(sInsertSQL);
                        }
                        //mbahl3 mits25625 start

                        foreach (string sFuncID in arGrantedFuncIDs)
                        {
                            object objExecuteQuery = null;
                            sSQL = string.Format("SELECT GLOSSARY_TEXT.TABLE_NAME FROM GLOSSARY  INNER JOIN GLOSSARY_TEXT ON GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID WHERE GLOSSARY_TYPE_CODE = 468 and (5000000 + GLOSSARY.TABLE_ID*20)={0}", "~sFuncID~");
                            dictParams.Clear();
                            dictParams.Add("sFuncID", sFuncID);
                            objExecuteQuery = DbFactory.ExecuteScalar(sRMConnectionString, sSQL, dictParams);
                            if (objExecuteQuery != null)
                            {
                                string str = objExecuteQuery.ToString();
                                InsertintoSecurityDB(sFuncID, str, sSecConnectionString);
                            }
                        }
                            
                        //mbahl3 mits25625 end
                    }

                    ResetParentFuncId(0, p_objDocIn);
                }
            }
            catch(Exception e)
            {
                throw new RMAppException
                    (Globalization.GetString("GroupModulePermissions.GrantIndividualPermissions", m_iClientId), e);
            }
        }
        //mbahl3 mits25625 start
        public void InsertintoSecurityDB(string sFuncId, string sFunctionName, string sSecConnectionString)
        {
            DbWriter objWriter = null;
            Dictionary<string, object> objDictParams = new Dictionary<string, object>();               
            int iCount = 0;
            using (DbConnection oConnection = DbFactory.GetDbConnection(sSecConnectionString))
            {
               
                  int id = 0;
                string sSQL = string.Format("SELECT FUNC_ID FROM FUNCTION_LIST WHERE FUNC_ID = {0}", "~sFuncId~");
                  objDictParams.Add("sFuncId", sFuncId);
               using (DbReader oDbReader = DbFactory.ExecuteReader(sSecConnectionString, sSQL, objDictParams))
                {
                    if (oDbReader.Read())
                    {
                        id = oDbReader.GetInt32(0);
                    }
                    if (id != int.Parse(sFuncId))
                    {
                        oConnection.Open();
                        objWriter = DbFactory.GetDbWriter(oConnection);
                        objWriter.Tables.Add("FUNCTION_LIST");
                        objWriter.Fields.Add("FUNC_ID", sFuncId);
                        objWriter.Fields.Add("FUNCTION_NAME", sFunctionName);
                        objWriter.Fields.Add("PARENT_ID", 5000000);
                        objWriter.Fields.Add("ENTRY_TYPE", 1);
                        objWriter.Fields.Add("CHECKSUM", null);
                       iCount = objWriter.Execute();

                        
                    }
                }
            }
        }

        //mbahl3 mits25625 end
        /// <summary>
        /// Save permission changes caused by revoking each individual
        /// permissions by the user. A user could revoke and then grant the same
        /// permissions, so we may need to delete the permission entry before inserting
        /// it.
        /// </summary>
        /// <param name="p_objDocIn">input message</param>
        /// <param name="sGroupId">The module group permissions</param>
        /// <param name="sSecConnectionString">security databasea connection string</param>
        /// <param name="sRMConnectionString">Riskmaster database connection string</param>
        private void RevokeIndividualPermissions(XmlDocument p_objDocIn, string sGroupId, string sSecConnectionString, string sRMConnectionString)
        {
            //remove revoked permissions from the database
            try
            {
                string sRevokedFuncIDs = GetGrantRevokeFunctionIDs(p_objDocIn, "revokedFuncIDs");
                string[] arRevokedFuncIDs = sRevokedFuncIDs.Split(m_sFuncIdDelimiter.ToCharArray());
                if (sRevokedFuncIDs.Length > 0)
                {
                    foreach (string sFuncID in arRevokedFuncIDs)
                    {
                        RevokeAllChildPermissions(sFuncID, sGroupId, sRMConnectionString, sSecConnectionString);
                    }

                    ResetParentFuncId(0, p_objDocIn);
                }
            }
            catch (Exception e)
            {
                throw new RMAppException
                    (Globalization.GetString("GroupModulePermissions.RevokeIndividualPermissions", m_iClientId), e);
            }
        }


        /// <summary>
        /// Save permission changes caused by granting all child
        /// permissions of a node by the user.
        /// </summary>
        /// <param name="p_objDocIn">input message</param>
        /// <param name="sGroupId">The module group permissions</param>
        /// <param name="sSecConnectionString">security databasea connection string</param>
        /// <param name="sRMConnectionString">Riskmaster database connection string</param>
        private void GrantBatchPermissions(XmlDocument p_objDocIn, string sGroupId, string sSecConnectionString, string sRMConnectionString)
        {
            string sGrantFuncId = string.Empty;
            XmlNode oFuncId = null;

            //remove revoked permissions from the database
            try
            {
                oFuncId = p_objDocIn.SelectSingleNode("//grantedFromFuncId");
                if (oFuncId != null)
                {
                    sGrantFuncId = oFuncId.InnerText;
                    oFuncId.InnerText = string.Empty;
                }

                if (string.IsNullOrEmpty(sGrantFuncId))
                {
                    return;
                }

                GrantAllChildPermissions(sGrantFuncId, sGroupId, sRMConnectionString, sSecConnectionString);
                ResetParentFuncId(0, p_objDocIn);
            }
            catch (Exception e)
            {
                throw new RMAppException
                    (Globalization.GetString("GroupModulePermissions.GrantBatchPermissions", m_iClientId), e);
            }
        }

        /// <summary>
        /// Save permission changes caused by revoking all child
        /// permissions of a node by the user.
        /// </summary>
        /// <param name="p_objDocIn">input message</param>
        /// <param name="sGroupId">The module group permissions</param>
        /// <param name="sSecConnectionString">security databasea connection string</param>
        /// <param name="sRMConnectionString">Riskmaster database connection string</param>
        private void RevokeBatchPermissions(XmlDocument p_objDocIn, string sGroupId, string sSecConnectionString, string sRMConnectionString)
        {
            string sRevokeFuncId = string.Empty;
            XmlNode oFuncId = null;

            //remove revoked permissions from the database
            try
            {
                oFuncId = p_objDocIn.SelectSingleNode("//revokedFromFuncId");
                if (oFuncId != null)
                {
                    sRevokeFuncId = oFuncId.InnerText;
                    oFuncId.InnerText = string.Empty;
                }

                if (string.IsNullOrEmpty(sRevokeFuncId))
                {
                    return;
                }

                RevokeAllChildPermissions(sRevokeFuncId, sGroupId, sRMConnectionString, sSecConnectionString);
                ResetParentFuncId(0, p_objDocIn);
            }
            catch (Exception e)
            {
                throw new RMAppException
                    (Globalization.GetString("GroupModulePermissions.RevokeBatchPermissions", m_iClientId), e);
            }
        }


        /// <summary>
        /// Grant permissions for this function and all child functions
        /// </summary>
        /// <param name="sFunctionId">Current function id</param>
        private void GrantAllChildPermissions(string sFunctionId, string sGroupId, string sRMConnectionString, string sSecConnectionString)
        {
            string sSQL = string.Empty;
            string sFunctionIDs = string.Empty;
            int iFunctionId = int.Parse(sFunctionId);
            int iCount = 0;
            List<int> lstFuncIDs = new List<int>();
            bool bAdminTable = false; //mbahl3 mits25625
            int iTableId = 0; //mbahl3 mits25625
            Dictionary<string, object> dictParams = new Dictionary<string, object>(); //mbahl3  mits25625
            string sTableName = string.Empty;
            string sTableId = string.Empty;
            StringBuilder sbSQL = new StringBuilder(); //mbahl3 mits25625
            try
            {
                //Retrieve all direct child functions
                GetDirectChildren(iFunctionId, sRMConnectionString, sSecConnectionString, ref sFunctionIDs, ref lstFuncIDs, ref bAdminTable); //mbahl3 mits25625

                if (sFunctionIDs.Length > 0)
                    sFunctionIDs += ",";
                sFunctionIDs += sFunctionId;

                //Revoke permissions for this and all direct child functions
                if (sFunctionIDs.Length > 0)
                {
                    using (DbConnection oConnection = DbFactory.GetDbConnection(sRMConnectionString))
                    {
                        oConnection.Open();
                        string sDeleteSQL = "DELETE FROM GROUP_PERMISSIONS WHERE FUNC_ID IN (" + sFunctionIDs + ")" +
                                           " AND GROUP_ID=" + sGroupId;
                        iCount = oConnection.ExecuteNonQuery(sDeleteSQL);

                        //insert the permissions to the database
                        string[] arGrantedFuncIDs = sFunctionIDs.Split(',');
                        //gagnihotri MITS 11995 Changes made for Audit table                            
                        sSQL = "INSERT INTO GROUP_PERMISSIONS(GROUP_ID, FUNC_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES(";
                        foreach (string sFuncID in arGrantedFuncIDs)
                        {
                            //gagnihotri MITS 11995 Changes made for Audit table
                            string sInertSQL = sSQL + sGroupId + ", " + sFuncID + ",'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
                            iCount = oConnection.ExecuteNonQuery(sInertSQL);
                        }
                        //mbahl3 mits25625
                        foreach (string sFuncID in arGrantedFuncIDs)
                        {
                            object objExecuteQuery = null;
                            sSQL = string.Format("SELECT GLOSSARY_TEXT.TABLE_NAME FROM GLOSSARY  INNER JOIN GLOSSARY_TEXT ON GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID WHERE GLOSSARY_TYPE_CODE = 468 and (5000000 + GLOSSARY.TABLE_ID*20)={0}", "~sFunctionId~");
                            dictParams.Clear();
                            dictParams.Add("sFunctionId", sFunctionId);

                            objExecuteQuery = DbFactory.ExecuteScalar(sRMConnectionString, sSQL, dictParams);
                            if (objExecuteQuery != null)
                            {
                                sTableName = objExecuteQuery.ToString();
                            }
                        }

                       //mbahl3 mits25625
                         }
                    //mbahl3 mits25625
                    if (bAdminTable)
                    {
                        using (DbConnection oConnection = DbFactory.GetDbConnection(sSecConnectionString))
                        {
                            DbWriter objWriter = null;

                            oConnection.Open();
                            string sDeleteSQL = "DELETE FROM FUNCTION_LIST WHERE FUNC_ID IN (" + sFunctionIDs + ")";
                            iCount = oConnection.ExecuteNonQuery(sDeleteSQL);
                            iTableId = iFunctionId - 5000000;
                            sSQL = string.Format("SELECT FUNC_ID + {0} FUNC_ID, FUNCTION_NAME FROM FUNCTION_LIST WHERE PARENT_ID=5000000",iTableId);
                            int iFuncId = 0;
                            string sFuncName = string.Empty;
                            object objExecuteQuery = null;
                            objExecuteQuery = DbFactory.ExecuteScalar(sSecConnectionString, sSQL);
                            sTableId = objExecuteQuery.ToString();
                            sbSQL.Append("INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES(");
                            sbSQL.Append(string.Format("{0},{1},{2},0)", "~iFuncId~", "~sFuncName~", "~iFunctionId~"));
                         using (DbReader oDbReader = DbFactory.GetDbReader(sSecConnectionString, sSQL))
                         {

                             while (oDbReader.Read())
                             {
                                 iFuncId = Conversion.ConvertObjToInt(oDbReader.GetValue(0), m_iClientId);
                                 sFuncName = Conversion.ConvertObjToStr(oDbReader.GetValue(1));
                                 dictParams.Clear();
                                 dictParams.Add("iFuncId", iFuncId);
                                 dictParams.Add("sFuncName", sFuncName);
                                 dictParams.Add("iFunctionId", iFunctionId);
                                 iCount = DbFactory.ExecuteNonQuery(sSecConnectionString, sbSQL.ToString(), dictParams);
                             }
                             string sSQLinsert = string.Format("SELECT FUNC_ID FROM FUNCTION_LIST WHERE FUNC_ID = {0}", "~sFunctionId~");
                             dictParams.Add("sFunctionId", sFunctionId);
                             objWriter = DbFactory.GetDbWriter(oConnection);
                             objWriter.Tables.Add("FUNCTION_LIST");
                             objWriter.Fields.Add("FUNC_ID", sFunctionId);
                             objWriter.Fields.Add("FUNCTION_NAME", sTableName);
                             objWriter.Fields.Add("PARENT_ID", 5000000);
                             objWriter.Fields.Add("ENTRY_TYPE", 1);
                             objWriter.Fields.Add("CHECKSUM", null);
                             iCount = objWriter.Execute();
                          }
  
                        }
                    }
                    //mbahl3 mits25625
                }

                //Revoke permissions for the grand-child permissions
                foreach (int iFuncId in lstFuncIDs)
                {
                    GrantAllChildPermissions(iFuncId.ToString(), sGroupId, sRMConnectionString, sSecConnectionString);
                }
            }
            catch (Exception e)
            {
                throw new RMAppException
                    (Globalization.GetString("GroupModulePermissions.GrantAllChildPermissions", m_iClientId), e);
            }
        }


        /// <summary>
        /// Revoke permissions for this function and all child functions
        /// </summary>
        /// <param name="sFunctionId">Current function id</param>
        private void RevokeAllChildPermissions(string sFunctionId, string sGroupId, string sRMConnectionString, string sSecConnectionString)
        {
            string sSQL = string.Empty;
            string sFunctionIDs = string.Empty;
            int iFunctionId = int.Parse(sFunctionId);
            int iCount = 0;
            List<int> lstFuncIDs = new List<int>();
            bool bAdminTable = false; //mbahl3 mits25625
            try
            {
                //if revoke all permissions related to this database, fucntion id =0
                if (sFunctionId == "0")
                {
                    using (DbConnection oConnection = DbFactory.GetDbConnection(sRMConnectionString))
                    {
                        oConnection.Open();
                        string sDeleteSQL = "DELETE FROM GROUP_PERMISSIONS WHERE GROUP_ID=" + sGroupId;
                        iCount = oConnection.ExecuteNonQuery(sDeleteSQL);
                    }

                    return;
                }

                //Retrieve all direct child functions
                GetDirectChildren(iFunctionId, sRMConnectionString, sSecConnectionString, ref sFunctionIDs, ref lstFuncIDs, ref bAdminTable); //mbahl3 mits25625

                if (sFunctionIDs.Length > 0)
                    sFunctionIDs += ",";
                sFunctionIDs += sFunctionId;

                //Revoke permissions for this and all direct child functions
                if (sFunctionIDs.Length > 0)
                {
                    using (DbConnection oConnection = DbFactory.GetDbConnection(sRMConnectionString))
                    {
                        oConnection.Open();
                        string sDeleteSQL = "DELETE FROM GROUP_PERMISSIONS WHERE FUNC_ID IN (" + sFunctionIDs + ")" +
                                           " AND GROUP_ID=" + sGroupId;
                        iCount = oConnection.ExecuteNonQuery(sDeleteSQL);
                    }
                }

                //Revoke permissions for the grand-child permissions
                foreach (int iFuncId in lstFuncIDs)
                {
                    RevokeAllChildPermissions(iFuncId.ToString(), sGroupId, sRMConnectionString, sSecConnectionString);
                }
            }
            catch (Exception e)
            {
                throw new RMAppException
                    (Globalization.GetString("GroupModulePermissions.RevokeAllChildPermissions", m_iClientId), e);
            }
        }

        /// <summary>
        /// get the granted/revoked function ids from the input document
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        private string GetGrantRevokeFunctionIDs(XmlDocument p_objDocIn, string sPath)
        {
            string sFunctionIDs = string.Empty;
            XmlNode oFunctionIDs = p_objDocIn.SelectSingleNode("//" + sPath);
            if (oFunctionIDs != null)
            {
                sFunctionIDs = oFunctionIDs.InnerText;
                if (sFunctionIDs.Length > 2)
                {
                    sFunctionIDs = sFunctionIDs.Replace(m_sFuncIdDelimiter + m_sFuncIdDelimiter, m_sFuncIdDelimiter);

                    if (sFunctionIDs.Substring(0, 1) == m_sFuncIdDelimiter)
                        sFunctionIDs = sFunctionIDs.Substring(1);

                    if (sFunctionIDs.Substring(sFunctionIDs.Length - 1) == m_sFuncIdDelimiter)
                        sFunctionIDs = sFunctionIDs.Substring(0, sFunctionIDs.Length - 1);
                }

                oFunctionIDs.InnerText = string.Empty;
            }

            return sFunctionIDs;
        }


        /// <summary>
        /// get direct children of the parent node
        /// </summary>
        /// <param name="iParentID"></param>
        /// <param name="sSecConnectionString"></param>
        /// <param name="?"></param>
        /// <param name="lstFunctionIDs"></param>
        private void GetDirectChildren(int iParentID, string sRMConnectionString, string sSecConnectionString, ref string sFunctionIDs, ref List<int> lstFunctionIDs, ref bool bAdminTable) //mbahl3 mits25625
        {
            string sConnectionString = string.Empty;
            string sSQL = string.Empty;
            sFunctionIDs = string.Empty;
            int iFunctionID = 0;

            //For Admin Tracking tables. *20 leaves space for all the individual
            //Admin Tracking table permissions
            if (iParentID == 5000000)
            {
                sSQL = "SELECT 5000000 + GLOSSARY.TABLE_ID*20, 1 FROM GLOSSARY " +
                        " INNER JOIN GLOSSARY_TEXT ON GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID " +
                        "WHERE GLOSSARY_TYPE_CODE = 468";
                sSecConnectionString = sRMConnectionString;
            }
            //get individule permissions for each Admin Tracking table, and assume
            // there is no child permissions for these individule permissions.

            else if ((iParentID > 5000000) && (iParentID < 6000000) && IsAdminTrackingTable(iParentID - 5000000, sRMConnectionString))
            {
                int iTableId = iParentID - 5000000;
                
                sSQL = "SELECT FUNC_ID+" + iTableId + ", 0 FROM FUNCTION_LIST " +
                        "WHERE PARENT_ID=5000000";
                bAdminTable = true; //mbahl3 mits25625
            }
            else
            {
                sSQL = "SELECT f.FUNC_ID, COUNT(fc.FUNC_ID) FROM FUNCTION_LIST f " +
                    "LEFT OUTER JOIN FUNCTION_LIST fc ON f.FUNC_ID = fc.PARENT_ID " +
                    "WHERE f.PARENT_ID=" + iParentID.ToString() + " GROUP BY f.FUNC_ID";
            }
            using (DbReader oDbReader = DbFactory.GetDbReader(sSecConnectionString, sSQL))
            {
                while (oDbReader.Read())
                {
                    if (sFunctionIDs.Length > 0)
                        sFunctionIDs += ",";
                    iFunctionID = oDbReader.GetInt(0);

                    sFunctionIDs += iFunctionID.ToString();
                    int iCount = oDbReader.GetInt(1);
                    if (iCount > 0)
                        lstFunctionIDs.Add(iFunctionID);
                }
            }

        }

        /// <summary>
        /// Reset parent functoin id to top level after permission update
        /// </summary>
        /// <param name="iParantId"></param>
        /// <param name="oDocIn"></param>
        private void ResetParentFuncId(int iParantId, XmlDocument oDocIn)
        {
            XmlNode oParentNode = oDocIn.SelectSingleNode("//parentFuncId");
            if (oParentNode != null)
                oParentNode.InnerText = iParantId.ToString();
        }

        /// <summary>
        /// get the DSN ID and Group ID from the input document
        /// </summary>
        /// <param name="p_objDocIn">The input document</param>
        /// <param name="iDsnID">DSN ID</param>
        /// <param name="sGroupID">Module group ID</param>
        private void GetDsnGroupIDs(XmlDocument p_objDocIn, out int iDsnID, out string sGroupID)
        {
            //get child function nodes
            XmlNode oDsnIDNode = p_objDocIn.SelectSingleNode("//DsnId");
            iDsnID = 0;
            if (oDsnIDNode != null)
                iDsnID = int.Parse(oDsnIDNode.InnerText);

            XmlNode oGroupIDNode = p_objDocIn.SelectSingleNode("//GroupId");
            sGroupID = "0";
            if (oGroupIDNode != null)
                sGroupID = oGroupIDNode.InnerText;
        }
        /// <summary>
        /// This function will check if iTable id is
        /// Admin Tracking Table i.e. iTableId = AdminTrackingTableId *20)
        /// </summary>
        /// <param name="iTableId">Table Id</param>
        /// <param name="sRMConnectionString">RM connection string</param>
        /// <returns>true/false</returns>
        private bool IsAdminTrackingTable(int iTableId, string sRMConnectionString)
        {
            string sSql = string.Empty;
            bool bAdminTrackingTable = false;
            sSql = "select GLOSSARY.TABLE_ID*20 FROM GLOSSARY " +
                       " INNER JOIN GLOSSARY_TEXT ON GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID WHERE GLOSSARY_TYPE_CODE = 468 ";
            using(DbReader objRdr = DbFactory.GetDbReader(sRMConnectionString, sSql))
            {
                while(objRdr.Read())
                {
                    if(objRdr.GetInt32(0) == iTableId)
                    {
                        bAdminTrackingTable = true;
                        break;
                    }
                }

            }


            return bAdminTrackingTable;
        }
    }
}
