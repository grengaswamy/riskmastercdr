﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security.Encryption;
using Riskmaster.Common;
using Riskmaster.Security;
using System.Data;
using System.Collections.Generic;

namespace Riskmaster.Application.SecurityManagement
{
	/// <summary> 
	///Author  :   Tanuj Narula
	///Dated   :   3rd,July 2004
	///Purpose :   Contains the functions related to XML processing.
	/// </summary>
	public class Security
	{
		/// <summary>
		/// This is code access key control.
		/// </summary>
		private const string CODE_ACCESS_PUBLIC_KEY = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";

        /// <summary>
        /// Private Variable for Client Id
        /// </summary>
        private int m_iClientId = 0;

		/// <summary>
		/// This is the default constructor.
		/// </summary>
		public Security(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}

		/// <summary>
		/// Contains UserId.
		/// </summary>
		private int m_iID ;  
		
		/// <summary>
		/// Contains DSN ID.
		/// </summary>
		private int m_iDSNID;
	   
		/// <summary>
		/// This represents the UserId. 
		/// </summary>
		public int  ID
		{
			get
			{
				return m_iID;
			}
			set
			{
				if (value != m_iID)
				{
					m_iID=value;
				}
			}
		}
		/// <summary>
		/// DSNID, Key in database
		/// </summary>
		public int  DSNID
		{
			get
			{
				return m_iDSNID;
			}
			set
			{
				if (value != m_iDSNID)
				{
					m_iDSNID=value;
				}
			}
		}
		
		/// <summary>
		/// This function acts as a wrapper over actual functions for filling various XmlDocuments.
		/// If some error occurs while processing Xml,it throws that error back to client application with appropriate message. 
		/// </summary>
		/// <param name="p_sFormName">Name of the form for which Xml to be filled in.</param>
		/// <param name="p_objXMLForm">Xml document to be filled in.</param>
		/// <returns>True if successful else throws error back to client application.</returns>
		public bool GetXMLData(string p_sFormName, ref XmlDocument p_objXMLForm)
		{ 
			bool bRetVal=false;

			//try
			//{
				p_sFormName=p_sFormName.ToLower().Trim();
				
				switch(p_sFormName)
				{
					case "userinfo" :
					{

                        if (GetUserInfoData(ref p_objXMLForm))
						{
							bRetVal=true;
						}
							
						break;
					}
					case "datasourceinfo" :
					{
                        if (GetDataSourceInfoData(ref p_objXMLForm))
						{
							bRetVal=true;
						}
						
						break;
					}
					case "useraccessperm" :
					{
                        if (GetUserAccessPermData(ref p_objXMLForm))
						{
							bRetVal=true;
						}
					
						break;
					}
					case "setemailoptions" :
					{
                        if (GetEmailOptionData(ref p_objXMLForm))
						{
							bRetVal=true;
						}
						break;
					}
					case "odbcdrivers" :
					{
                        if (GetODBCDrivers(ref p_objXMLForm))
						{
							bRetVal=true;
						}
						break;
					}
					case "predefineddsn" :
					{
                        if (GetPredefinedDSNXml(ref p_objXMLForm))
						{
							bRetVal=true;
						}
						break;
					}
					case "dsnwizardprocesslogin" :
					{
                        if (GetDBValidateXml(ref p_objXMLForm))
						{
							bRetVal=true;
						}
						break;
					}
						case "dsnwizardprocesslicense" :
					{
                        if (GetValidateLicenseXml(ref p_objXMLForm))
						{
							bRetVal=true;
						}
						break;
					}
						case "domainauthentication":
					{
                        if (GetDomainAuthenticationData(ref p_objXMLForm))
						{
							bRetVal=true;
						}
						break;
					}
						
					default :
					{
						break;
					}

				}
				//rsolanki2:Domain authentication updates
			XmlNode objTempXml;
			objTempXml = p_objXMLForm.SelectSingleNode("//control[@name='hdIsDomainAuthenticationEnabled']");
			if (objTempXml!=null)
			{
				p_objXMLForm.SelectSingleNode("//control[@name='hdIsDomainAuthenticationEnabled']").InnerText = Convert.ToString(Security.bIsDomainAuthenticationEnabled());
			}            
			
			//}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			//finally
			//{
			//	
			//}
			return bRetVal;
		}
		/// <summary>
		/// This function acts as wrapper over actual function call of FillUserInfoDOM() function which fills XmlDocument corresponding to a user.
		/// </summary>
		/// <param name="p_objXMLForm">XmlDocument to be filled.</param>
		/// <returns>True if successful else throw custom exception.</returns>
        public bool GetUserInfoData(ref XmlDocument p_objXMLForm)
		{   
			bool bRetVal=false;
			//try
			//{
				
				if(FillUserInfoDOM(ref p_objXMLForm))
				{
					bRetVal=true;
				 
				}               
			//}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			//finally
			//{
			//}
			return bRetVal;
		}
		/// <summary>
		/// This function acts as wrapper over actual function call of FillDataSourceInfoDOM() function which actually fills XmlDocument corresponding to a DataSource.
		/// </summary>
		/// <param name="p_objXMLForm">XmlDocument to be filled.</param>
		/// <returns>True if successful else throws custom exception.</returns>
        public bool GetDataSourceInfoData(ref XmlDocument p_objXMLForm)
		{
			bool bRetVal =false;
			//try
			//{
				if(FillDataSourceInfoDOM(ref p_objXMLForm))
				{
					bRetVal=true;
				}
			//}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			//finally
			//{
			//}
			return bRetVal;
		}
		/// <summary>
		///  This function acts as wrapper over actual function call of 
		///  FillUserAccessPermDOM() function which actually fills XmlDocument corresponding to a DataSource.
		/// </summary>
		/// <param name="p_objXMLForm">XmlDocument to be filled.</param>
		/// <returns>True if successful else throws custom exception.</returns>
        public bool GetUserAccessPermData(ref XmlDocument p_objXMLForm)
		{
			bool bRetVal =false;
			//try
			//{
				if(FillUserAccessPermDOM(ref p_objXMLForm))
				{
					bRetVal=true;
				}
			//}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			//finally
			//{
			//}
			return bRetVal;
		}

				
		/// <summary>
		///  This function actually fills XmlDocument corresponding to a User.
		/// </summary>
		/// <param name="p_objXMLForm">XmlDocument to be filled in.</param>
		/// <returns>True if successful else throws custom exception.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool FillUserInfoDOM(ref XmlDocument p_objXMLForm)
		{
			bool bRetVal=false,bUserIdFound=false;
			XmlNodeList objXMLNodeList=null;
			XmlElement objXmlElem=null;
			string sFieldName="";
			string[] arrTemp=new string[2];
			int iIndex,iID=0;
			Users objUsers=null;
			ArrayList arrlstUserCol=null;
			ArrayList arrlstLanguages=null;
			RMWinSecurity objCrmWin=null;
			try
			{
				objUsers=new Users(m_iClientId);
				arrlstUserCol=new ArrayList();
				if(!objUsers.Load(ref arrlstUserCol))//Load all the users in the system.
				{
					return false;
				}
				arrlstLanguages=new ArrayList();
                objCrmWin = new RMWinSecurity(m_iClientId);
				if(!objCrmWin.LoadLanguages(ref arrlstLanguages))
				{
					return false;
				}
			   
				if(ID==0)
					#region Processing for new user
				 
				{
					objXMLNodeList = p_objXMLForm.GetElementsByTagName("control");
					foreach(XmlElement objTempXmlElem in objXMLNodeList)
					{
						sFieldName=objTempXmlElem.GetAttribute("name");
						sFieldName=sFieldName.ToLower().Trim();
						switch(sFieldName)
						{
							case "language" :
							{
							for (iIndex=0;iIndex<(arrlstLanguages.Count+1);iIndex++)
							 {
								 if(iIndex==0)
								 {
									
									 objXmlElem =  p_objXMLForm.CreateElement("option");
									 objXmlElem.SetAttribute("value","");
									 objXmlElem.InnerText = "";
									 objTempXmlElem.AppendChild(objXmlElem);
									 objXmlElem = null;
								 }
								 else
								 {
									 arrTemp = ((string)arrlstLanguages[iIndex - 1]).Split('|');
									 objXmlElem =  p_objXMLForm.CreateElement("option");
									 objXmlElem.SetAttribute("value",arrTemp[1]);
									 objXmlElem.InnerText = arrTemp[0];
									 objTempXmlElem.AppendChild(objXmlElem);
									 
									 //pmittal5 MITS 11700 - Setting default value to "English(United States)"
									 if (arrTemp[1] == "1033")
									 {
										 objXmlElem.SetAttribute("selected", "");
										 objTempXmlElem.SetAttribute("value", arrTemp[1]);
									 }

									 objXmlElem = null;

								 }
										
							 }
								break;
							}
							case "manager" :
							{
								objXmlElem =  p_objXMLForm.CreateElement("option");
								objXmlElem.SetAttribute("value","");
								objXmlElem.InnerText = "";
								objXmlElem.SetAttribute("selected","");
								objTempXmlElem.AppendChild(objXmlElem);
								for(iIndex=0;iIndex<(arrlstUserCol.Count);iIndex++)
								{
									objXmlElem = p_objXMLForm.CreateElement("option");
									objXmlElem.SetAttribute("id",(((Users)arrlstUserCol[iIndex]).UserId).ToString());
									objXmlElem.InnerText = ((Users)arrlstUserCol[iIndex]).FirstName + " " + ((Users)arrlstUserCol[iIndex]).LastName;
									objXmlElem.SetAttribute("value",(((Users)arrlstUserCol[iIndex]).UserId).ToString());
									objTempXmlElem.AppendChild(objXmlElem);
									 
								}
								break;
							}
							case "lstdatasources" :
							{
								CreateOptionList(ref p_objXMLForm,objTempXmlElem,CreateOptionsFor.AllDsns);
								break;
							}

						}

					}
					objXMLNodeList=null;
				}
					#endregion
				else
					#region Populating XML for existing user	
				{
					for(iIndex=0 ;iIndex<arrlstUserCol.Count;iIndex++)//loop for getting the desired User. 
					{
						if (((Users)arrlstUserCol[iIndex]).UserId==ID)
						{
							iID = iIndex;
							bUserIdFound=true;
							break;
						}
					}
					if(!bUserIdFound) //If User does not exist in the system,the exception will be thrown to calling function.
					{
                        throw new RecordNotFoundException(Globalization.GetString("Security.FillUserInfoDOM.InvalidUId", m_iClientId));
					}
					objXMLNodeList = p_objXMLForm.GetElementsByTagName("control");

					//This loop fills the blank Xml Document with User details.
					foreach(XmlElement objTempXmlElem in objXMLNodeList)
					{
						sFieldName = objTempXmlElem.GetAttribute("name").ToLower().Trim();
						#region
						switch(sFieldName)
						{
							case "userid" :
							{
								objTempXmlElem.InnerText=(((Users)arrlstUserCol[iID]).UserId).ToString();
								break;
							}
							case "lastname" :
							{
							
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).LastName;
								
								break;
							}
							case "firstname" :
							{   
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).FirstName;
								
								break;
							}
							case "title" :
							{
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).Title;
								break;
							}
							case  "company" :
							{
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).CompanyName;
								break;
							}
							case  "country" :
							{
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).Country;
								break;
							}
							case  "city" :
							{
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).City;
					
								break;
							}
							case  "state" :
							{
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).State;
								break;
							}
							case  "zip" :
							{
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).ZipCode;
								
								break;
							}
							case  "officephone" :
							{
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).OfficePhone;
								
								break;
							}
							case  "homephone" :
							{
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).HomePhone;
								
								break;
							}
							case  "address1" :
							{
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).Address1;
								
								break;
							}
							case  "address2" :
							{
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).Address2;
								
								break;
							}
							case  "email" :
							{
									
								objTempXmlElem.InnerText=((Users)arrlstUserCol[iID]).Email;
								
								break;
							}
							case   "language" : //Fills language related details.
							{
								for (iIndex=0;iIndex<(arrlstLanguages.Count+1);iIndex++)
								{
									if(iIndex==0)
									{
										
										objXmlElem =  p_objXMLForm.CreateElement("option");
										objXmlElem.InnerText = "";
										objTempXmlElem.AppendChild(objXmlElem);
										objXmlElem = null;
									}
									else 
									{
										arrTemp = ((string)arrlstLanguages[iIndex - 1]).Split('|');
										objXmlElem =  p_objXMLForm.CreateElement("option");
										objXmlElem.SetAttribute("value",arrTemp[1]);
										objXmlElem.InnerText = arrTemp[0];
										if(((Users)arrlstUserCol[iID]).NlsCode==Convert.ToInt32(arrTemp[1]))
										{
											objXmlElem.SetAttribute("selected","");
											objTempXmlElem.SetAttribute("value",((Users)arrlstUserCol[iID]).NlsCode.ToString());
										}
										objTempXmlElem.AppendChild(objXmlElem);
										objXmlElem = null;

									}
								}
								break;
							}
							case   "manager" :
							{
								//MITS 11875 Abhishek - Start
								//if((((Users)arrlstUserCol[iID]).ManagerId==((Users)arrlstUserCol[iID]).UserId) || (((Users)arrlstUserCol[iID]).ManagerId==0))
								//{

								objXmlElem = p_objXMLForm.CreateElement("option");
								objXmlElem.SetAttribute("value", "0");
								 objXmlElem.InnerText = "";
								objTempXmlElem.AppendChild(objXmlElem);
								//}
								//MITS 11875 Abhishek - end
								for(iIndex=0;iIndex<(arrlstUserCol.Count);iIndex++)
								{
									objXmlElem = p_objXMLForm.CreateElement("option");
									objXmlElem.SetAttribute("value",(((Users)arrlstUserCol[iIndex]).UserId).ToString());
									objXmlElem.InnerText = ((Users)arrlstUserCol[iIndex]).FirstName + " " + ((Users)arrlstUserCol[iIndex]).LastName;
									if(((Users)arrlstUserCol[iID]).ManagerId==((Users)arrlstUserCol[iIndex]).UserId && ((Users)arrlstUserCol[iID]).ManagerId!=((Users)arrlstUserCol[iID]).UserId )
									{
										objTempXmlElem.SetAttribute("value",((Users)arrlstUserCol[iID]).ManagerId.ToString());
									}
									objTempXmlElem.AppendChild(objXmlElem);
									objXmlElem=null;

								}
								break;
							}
							case "lstdatasources" :
							{
								
							  CreateOptionList(ref p_objXMLForm,objTempXmlElem,CreateOptionsFor.AllDsns);
							  break;
							}
								case "lstshowdatasources" :
							{
								
							  CreateOptionList(ref p_objXMLForm,objTempXmlElem,CreateOptionsFor.UserSpecificDsns);
							  break;
							}
							//Arnab:MITS-10662 - Added case for SMSAccess
							case "smsaccess":
							{
								XmlElement objSMSAdminFlag = (XmlElement)p_objXMLForm.SelectSingleNode("//control[@name='hdIsSMSAdminUser']");
								//R5: ..Raman Bhatia March 16 2009 We now store SMS_Access flag in Users object
								//this.PopulateSMSAccessValue(objTempXmlElem, ID, objSMSAdminFlag);
								objTempXmlElem.InnerText = ((Users)arrlstUserCol[iID]).SMSAccess.ToString();
								break;						                             
							}
                            //skhare7 MITS:22374
                            case "userpraccess":
                            {
                                objTempXmlElem.InnerText = ((Users)arrlstUserCol[iID]).UPSAccess.ToString();
                                break;
                            }
                            //End skhare7 MITS:22374
							//MITS-10662 end
								#endregion

						}
					}

				}
				#endregion
				p_objXMLForm.SelectSingleNode("//control[@name='hdEnhSecurity']").InnerText = PublicFunctions.IsPwdPolEnabled().ToString() ;
				bRetVal=true;
			}

			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			finally
			{
				//Nullifies all the used object so as to be garbage collected by CLR.
				if(objXMLNodeList!=null)
					objXMLNodeList=null;
				if(objXmlElem!=null)
					objXmlElem=null;
				if(arrTemp!=null)
					arrTemp=null;
				if(arrlstLanguages!=null)
					arrlstLanguages=null;
				if(objUsers!=null)
					objUsers=null;
				if(objCrmWin!=null)
					objCrmWin=null;
			}
			return bRetVal;
		}

		/// <summary>
		///  This function actually fills XmlDocument corresponding to a DataSource.
		/// </summary>
		/// <param name="p_objXMLForm">XmlDocument to be filled in.</param>
		/// <returns>True if successful else throws custom exception.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool FillDataSourceInfoDOM(ref XmlDocument p_objXMLForm)  
		{
			bool bRetVal =false,bDsnIdFound=false;
			string sFieldName="";
			string sFieldValue="";
			XmlNodeList objXMLNodeList=null;
			PublicFunctions.m_Security=null;
			string sDBName="",sDriver="",sServerName="";
			DataSources objDataSources=null;
			string sAddParams="";
			try
			{
                PublicFunctions.m_Security = new RMWinSecurity(m_iClientId);
				objDataSources= new DataSources(DSNID,false,m_iClientId);
				objXMLNodeList = p_objXMLForm.GetElementsByTagName("control");
				
				foreach(XmlElement objTempXmlElem in objXMLNodeList)//This loop fills the XmlDocument with DataSource details.
				{
					sFieldName = objTempXmlElem.GetAttribute("name").ToLower().Trim();
					sFieldValue=objTempXmlElem.GetAttribute("value").ToLower().Trim();
					#region
					switch(sFieldName)
					{
						case "dsnid" :
						{
							
							objTempXmlElem.InnerText=(objDataSources.DataSourceId).ToString();
							break;
						}
						case "dsname" :
						{
							if(!ContainsNull(objDataSources.DataSourceName))
							{
								objTempXmlElem.InnerText=objDataSources.DataSourceName;
							}
							break;
						}
						case "connectionstring" :
						{
							if(!ContainsNull(objDataSources.ConnectionString))
							{
								if(objDataSources.ConnectionString.IndexOf("UID")!=-1)
								{
									if(objDataSources.ConnectionString.IndexOf("Server")==-1)
									{
										objTempXmlElem.InnerText="";
										break;
									}
									else
									{
										  string sConnStr = objDataSources.ConnectionString.Substring(0,objDataSources.ConnectionString.IndexOf("UID"));
                                          objTempXmlElem.InnerText = RMCryptography.EncryptString(sConnStr);//Deb:Pen Testing
										  sConnStr=null;
									}
									
								}
								
							}
							break;
						}
						case "systemloginname" :
						{   
							if(!ContainsNull(objDataSources.RMUserId))
							{
								objTempXmlElem.InnerText= objDataSources.RMUserId;
							}
							break;
						}
						case "systempassword" :
						{
							if(!ContainsNull(objDataSources.RMPassword))
							{
                                //rsushilaggar changes done for Pan Testing
								objTempXmlElem.InnerText= RMCryptography.EncryptString(objDataSources.RMPassword);
							}
							break;
						}
						
						
						case  "docpath" :
						{     
								if(objDataSources.GlobalDocPath!=null)
								
								if(objDataSources.DocPathType == 1)
								{
                                    //objTempXmlElem.InnerText = this.EncryptDocPathCredentials(objDataSources.GlobalDocPath);
                                    //Deb:Pen Testing
                                    objTempXmlElem.InnerText= RMCryptography.EncryptString(this.EncryptDocPathCredentials(objDataSources.GlobalDocPath));
                                    objTempXmlElem.Attributes["disabled"].Value="false";
								}
								else
								{
									objTempXmlElem.InnerText=(objDataSources.GlobalDocPath);
									objTempXmlElem.Attributes["disabled"].Value="true";
								}
							
							break;
						}
						case  "workstations" :
						{
							
							objTempXmlElem.InnerText =Convert.ToString(objDataSources.NumLicenses);
							break;
						}
						case  "chkmodulesec" :
						{
							if(objDataSources.Status == true)
							{
								objTempXmlElem.InnerText="True";
							}
							break;
						}
						case  "optservers" :
						{
							if(objDataSources.DocPathType == 0)
							{
								objTempXmlElem.Attributes["value"].Value="0";
							}
                            else if (objDataSources.DocPathType == 2)
                            {
                                objTempXmlElem.Attributes["value"].Value = "2";
                            }
                            else
                            {
                                objTempXmlElem.Attributes["value"].Value = "1";
                            }
							break;
						}
							case  "licupdatedate" :
						{
								if(!ContainsNull(objDataSources.LicUpdDate.ToString()))
								{
								objTempXmlElem.InnerText=Conversion.ToDbDate(objDataSources.LicUpdDate);
		
								}
								break;
						}
						case  "orgsecflag" :
						{
							if(!ContainsNull(objDataSources.OrgSecFlag.ToString()))
							{
								objTempXmlElem.InnerText=(objDataSources.OrgSecFlag.ToString());
							}
							break;
						}												
					}
					#endregion
				}
				if(objDataSources.ConnectionString.IndexOf("Server")!=-1)
				{
					if(objDataSources.ConnectionString != "")
					{
					   
						//p_objXMLForm.SelectSingleNode("//buttonscript[@name='btnEdit']").Attributes["style"].Value="";
						//p_objXMLForm.SelectSingleNode("//buttonscript[@name='btnEdit']").Attributes["functionname"].Value="OpenDBWizardForEdit("+objDataSources.DataSourceId+")";
						DataSources objDs;
						if(!PublicFunctions.m_Security.SetupData(objDataSources.DataSourceId,ref sServerName,ref sDBName,ref sDriver,ref sAddParams,out objDs))
							return false;
						foreach(XmlElement objTempXmlElem in objXMLNodeList)
						{
							sFieldName = objTempXmlElem.GetAttribute("name").ToLower().Trim();
							#region
							switch(sFieldName)
							{
								case "dsname" :
								{
									if(!ContainsNull(objDataSources.DataSourceName))
									{
										objTempXmlElem.InnerText= objDataSources.DataSourceName;
									}
									break;
								
								}
								case "servername" :
								{
									if(!ContainsNull(sServerName))
									{
										objTempXmlElem.InnerText= sServerName;
									}
									break;
								}
								case "loginname" :
								{   
									if(!ContainsNull(objDataSources.RMUserId))
									{
										objTempXmlElem.InnerText= objDataSources.RMUserId;
									}
									break;
								}
								case "password" :
								{
									if(!ContainsNull(objDataSources.RMPassword))
									{
                                        objTempXmlElem.InnerText = RMCryptography.EncryptString(objDataSources.RMPassword);//Deb Pen testing
									}
									break;
								}
								case  "dbname":
								{  
									if(!ContainsNull(sDBName))
									{
										objTempXmlElem.InnerText= sDBName;
									}
									break;
								}
								case  "drivername":
								{
									if(!ContainsNull(sDriver))
									{
										objTempXmlElem.InnerText= sDriver;
									}
									break;
								}
						
							}
							#endregion
						}
					}
				}

				bRetVal=true;
			}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			finally
			{
				if(objXMLNodeList!=null)
					objXMLNodeList=null;
				if(PublicFunctions.m_Security!=null)
					PublicFunctions.m_Security=null;
				if(objDataSources!=null)
					objDataSources=null;
			}
			return bRetVal;
		}
		
		/// <summary>
		/// Checks to see if Domain Authentication is Enabled
		/// </summary>
		/// <returns>false indicating that "RMWorld-style" domain authentication settings
		/// are not used in RMX R5 and above</returns>
		public static bool bIsDomainAuthenticationEnabled()
		{
			return false;
		}
		/// <summary>
		///  This function fills XmlDocument corresponding to DomainOptions.
		/// </summary>
		/// <param name="p_objXMLForm">XmlDocument to be filled in.</param>
		/// <returns>True if successful else throws custom exception.</returns>
		/// 

		//rsolanki2: Domain authentication updates.
		//returns the domain names which are enabled
		public static ArrayList getDomainNames(int p_iClientId)//rkaur27 : Cloud Change
		{            
			string sSql = "";
			DbConnection objConn = null;
			DbReader objReader = null;
			ArrayList sNames=new ArrayList();
			try
			{
				objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(p_iClientId));
				objConn.Open();
				sSql = "SELECT NAME FROM DOMAIN WHERE ENABLED=1 ORDER BY DOMAINID";
				objReader = objConn.ExecuteReader(sSql);
				if (objReader != null)
				{
					while (objReader.Read())
					{
						sNames.Add(objReader.GetString("NAME"));
					}
				}
			}
			catch (Exception ex)
			{
				//throw new Exception(Globalization.GetString("SecurityManagement.Security.getDomainNames"), exp);
				throw new Exception("SecurityManagement.Security.getDomainNames", ex);
			}
			finally
			{
				if (objReader!=null)
				{
					objReader.Close();                    
				}
				if (objConn != null)
				{
					objConn.Close();
				}            
			}
			return sNames;
		}


		public bool FillDomainOptionsDOM(ref XmlDocument p_objXMLForm)
		{
			string sDomainName = "";
			string sDomainStatus = ""; //rsolanki2: domain authentication updates
			string sFieldName = "";
			bool bDomainLogin = false;
			bool bRetVal = false;
			XmlNode objXMLNode = null;
			XmlNode objXMLtempNode = null;
			DbConnection objConn = null;
			DbReader objReader = null;
			string sTempNode = null;
			bool bDomainStatus = false;

			string sSql = null;
			try
			{
				//rsolanki2:  Domain authentication updates
				bDomainLogin = bIsDomainAuthenticationEnabled();

				objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
				objConn.Open();
				sSql = "SELECT NAME,ENABLED FROM DOMAIN";
				objReader = objConn.ExecuteReader(sSql);
				if (objReader != null)
				{
					objXMLNode = p_objXMLForm.GetElementsByTagName("DomainTableList")[0];
					while (objReader.Read())
					{
						sDomainName = objReader.GetString("NAME");
						bDomainStatus = Convert.ToBoolean(objReader.GetValue("ENABLED"));
						sDomainStatus = (bDomainStatus) ? "Enabled" : "Disabled";

						sTempNode = string.Format("<listrow><DomainName>{0}</DomainName><Enabled>{1}</Enabled></listrow>", sDomainName, sDomainStatus);
						objXMLNode.InnerXml = objXMLNode.InnerXml + sTempNode; 
					}
			   }

			   //updating checkbox value               
			   objXMLNode = p_objXMLForm.DocumentElement;
			   objXMLtempNode = objXMLNode.SelectNodes(@"//control[@name='chkDomain']")[0];
			   if (bDomainLogin)
			   {
				   objXMLtempNode.InnerText = "True";
			   }               

			   bRetVal = true;
			}
			catch (Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					//objConn.Close();
					objConn.Dispose();
				}
				
			}
			return bRetVal;
		}
		/// <summary>
		///  This function fills XmlDocument corresponding to EmailOptions.
		/// </summary>
		/// <param name="p_objXMLForm">XmlDocument to be filled in.</param>
		/// <returns>True if successful else throws custom exception.</returns>
		public bool FillEmailOptionsDOM(ref XmlDocument p_objXMLForm)
		{
			bool bRetVal =true;
			XmlNodeList objXMLNodeList=null;
			
				
			string sFieldName=string.Empty,sSMTP=string.Empty,sEmailAdd=string.Empty;

            //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
            string sAtlernateDomain = string.Empty;
            string sReplyToDomain = string.Empty;
			bool bSecureDSN = false; //Secure DSN is not being used to distinguish DSNs
		   
			DataTable dtSMTPSettings = RMConfigurationSettings.GetSMTPServerSettings(m_iClientId);//rkaur27

            // psharma206  smtp cloud start
            if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true")
            {
                sSMTP = RMConfigurationManager.GetAppSetting("SMTPServer");
            }
            // psharma206  smtp cloud end
            else
            {
                sSMTP = dtSMTPSettings.Rows[0]["SMTP_SERVER"].ToString();
            }



			sEmailAdd = dtSMTPSettings.Rows[0]["ADMIN_EMAIL_ADDR"].ToString(); 
            //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
            sAtlernateDomain = dtSMTPSettings.Rows[0]["ALTERNATE_DOMAIN"].ToString();
            sReplyToDomain = dtSMTPSettings.Rows[0]["REPLYTO_DOMAIN"].ToString(); 

			objXMLNodeList = p_objXMLForm.GetElementsByTagName("control");
			foreach(XmlElement objTempXmlElem in objXMLNodeList)
			{
				sFieldName = objTempXmlElem.GetAttribute("name").ToLower().Trim();
				#region
				switch(sFieldName)
				{
					case "smtpserver" :
					{
						if(!ContainsNull(sSMTP))
						{
                            

							objTempXmlElem.InnerText=sSMTP;
						}
						break;
					}
					case "adminemailaddr" :
					{
						if(!ContainsNull(sEmailAdd))
						{
							objTempXmlElem.InnerText=sEmailAdd;
						}
						break;
					}
					case "chksecure" :
					{
						if(bSecureDSN)
						{
							objTempXmlElem.InnerText="True";
						}
						break;
					}
                    //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
                    case "senderalternatedomain":
                    {
                        if (!ContainsNull(sAtlernateDomain))
                        {
                            objTempXmlElem.InnerText = sAtlernateDomain;
                        }
                        break;
                    }
                    case "senderreplytodomain":
                    {
                        if (!ContainsNull(sReplyToDomain))
                        {
                            objTempXmlElem.InnerText = sReplyToDomain;
                        }
                        break;
                    }
                    //End rsushilaggar
					
				}
				#endregion
				
				
			}//foreach
			
			
			objXMLNodeList=null;
			
			return bRetVal;
		}
		/// <summary>
		/// This function gets the xml for ODBC drivers.
		/// </summary>
		/// <param name="p_objXMLForm">Xml document contains the xml.</param>
		/// <returns>True or false.</returns>
        public bool GetODBCDrivers(ref XmlDocument p_objXMLForm)
		{
			bool bRetVal =false;
			RMWinSecurity objRmWin=null;
			List<string> arrlstODBCDrivers = new List<string>();
			XmlElement objTemp=null;
			XmlElement objDriver=null;
			string[] arrStr=new string[4];
			int iCount=1;
			try
			{

                objRmWin = new RMWinSecurity(m_iClientId);
				objTemp = (XmlElement)p_objXMLForm.SelectSingleNode("//control[@name='optservers']");
                if (objTemp.Attributes["page"] != null && objTemp.Attributes["page"].Value == "PolicySystemAddDSN")
                    {
                       RMWinSecurity.IsPolicyDrivers = true;
                    }
                else
                    {
                       RMWinSecurity.IsPolicyDrivers = false;
                    }

				arrlstODBCDrivers = objRmWin.LoadODBCDrivers();
				foreach(string sDriver in arrlstODBCDrivers)
				{   
					arrStr=sDriver.Split('|');
					//new check introduced for filtering out File option as mentioned by Tanuj mail Dated  10/20/2005 09:54AM;  Subject: File option to be deleted.
					if (arrStr!=null)
					{
						if (arrStr.Length >=4)
						{
							if (arrStr[3]!=null)
							{
								if(arrStr[3]!="")
								{
									continue;
								}
							}
						}
					}
					objDriver= p_objXMLForm.CreateElement("optionDB");
					objDriver.SetAttribute("name",arrStr[0]);
					objDriver.InnerText=arrStr[0];
					objDriver.SetAttribute("version",arrStr[1]);
					objDriver.SetAttribute("company",arrStr[2]);
					objDriver.SetAttribute("optbuttonid",iCount.ToString()); 
					objDriver.SetAttribute("serverid", iCount.ToString());
					objDriver.SetAttribute("value", iCount.ToString());
					objDriver.SetAttribute("type", "driver");
					//Code commented for new check introduced for filtering out File option as mentioned by Tanuj mail Dated  10/20/2005 09:54AM;  Subject: File option to be deleted.
					/*if(arrStr[3]!="")
					{
						objDriver.SetAttribute("onclick", "OnSelectDB(this,'file')");
						objDriver.SetAttribute("view","file"); 
					}
					else
					{*/
						objDriver.SetAttribute("onclick", "OnSelectDB(this,'server')");
						objDriver.SetAttribute("view","server");
						objTemp.AppendChild(objDriver);
					/*}*/
					iCount++;
				}//foreach
				if(p_objXMLForm.SelectSingleNode("//control[@name='hdEditDsnId']").InnerText.Trim()!="")
				{
					PublicFunctions.ConstructAllParametersXmlForExistingDsn(ref p_objXMLForm, m_iClientId);
				}
				bRetVal=true;
			}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;

			//}
			finally
			{
				objRmWin=null;
				arrlstODBCDrivers=null;
				objTemp=null;
				objDriver=null;
				arrStr=null;
			}
			return bRetVal;
		}
		/// <summary>
		/// This function gets the xml for Predefined dsns.
		/// </summary>
		/// <param name="p_objXMLForm">Contains the xml for Prdefined dsns.</param>
		/// <returns>True or false.</returns>
        public bool GetPredefinedDSNXml(ref XmlDocument p_objXMLForm)
		{
			bool bRetVal =false;
			RMWinSecurity objRmWin=null;
			ArrayList arrlstPredefinedDSNs=null;
			XmlElement objTemp=null;
			XmlElement objPredefinedDSN=null;
			string[] arrStr=new string[2];
			int iCount=1;
			try
			{
                objRmWin = new RMWinSecurity(m_iClientId);
				arrlstPredefinedDSNs=new ArrayList();
				objTemp = (XmlElement)p_objXMLForm.SelectSingleNode("//control[@name='optDSNs']");
				objRmWin.LoadPreDefinedDataSources(ref arrlstPredefinedDSNs);
				foreach(string sDriver in arrlstPredefinedDSNs)
				{      
					objPredefinedDSN= p_objXMLForm.CreateElement("optionDB");
					arrStr=sDriver.Split('|');
					objPredefinedDSN.InnerText=arrStr[0];
					objPredefinedDSN.SetAttribute("name",arrStr[0]);
					objPredefinedDSN.SetAttribute("company",arrStr[1]);
					objPredefinedDSN.SetAttribute("type","dsnwizard");//"optbuttonid", iCount
					objPredefinedDSN.SetAttribute("select","datasource");
					objPredefinedDSN.SetAttribute("optbuttonid", iCount.ToString());
					objPredefinedDSN.SetAttribute("serverid", iCount.ToString());
					objPredefinedDSN.SetAttribute("value", iCount.ToString());
					objPredefinedDSN.SetAttribute("onclick", "OnSelectDB(this,'')");
					iCount++;
					objTemp.AppendChild(objPredefinedDSN);
				}
		
				
				bRetVal=true;
			}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;

			//}
			finally
			{
				objRmWin=null;
				arrlstPredefinedDSNs=null;
				objTemp=null;
				objPredefinedDSN=null;
				arrStr=null;
			}
			return bRetVal;
		}
		/// <summary>
		/// This function validates the database connection and returns the related data.
		/// </summary>
		/// <param name="p_objXMLForm">Contains the connection related data.</param>
		/// <returns>True or false.</returns>
        public bool GetDBValidateXml(ref XmlDocument p_objXMLForm)
		{  
			bool bRetVal=false;
			RMWinSecurity objRmWin;
			string sMsg="";
			XmlElement objElement=null;
			XmlElement objUserLogin;
			XmlElement objPasswordDsn;
			XmlElement objDBName=null;
			XmlElement objOptSelected=null;
			XmlElement objServerName=null;
			string sDsn="";
			string sConnStr="";
			string[] arrStr=new string[2];
			string sError="No";
			string sDsnid="";
			string sView="";
			string sAdditionalParamsChecked;
			string sAdditionalParams;
			Hashtable htAttributes=null;
			string sCallingMethod = "" ;
			try
			{
				if(p_objXMLForm.SelectSingleNode("//control[@name='hdInvokedFrom']").InnerText=="SetDocPath")
				{
				  sCallingMethod = "SET_DOC_PATH" ;
				}
                objRmWin = new RMWinSecurity(m_iClientId);
				objElement=(XmlElement)p_objXMLForm.SelectSingleNode("//loginscreenkeys/IsDsn");
				
					sDsnid=p_objXMLForm.SelectSingleNode("//DBkey//control[@name='optservers']").Attributes["value"].Value;
					objUserLogin=(XmlElement)p_objXMLForm.SelectSingleNode("//loginscreenkeys/inputlogin");
					objPasswordDsn=(XmlElement)p_objXMLForm.SelectSingleNode("//loginscreenkeys/inputpassword");
					sView=((XmlElement)p_objXMLForm.SelectSingleNode("//DBkey//optionDB[@value='" + sDsnid + "']")).Attributes["view"].Value;
					objOptSelected=(XmlElement)p_objXMLForm.SelectSingleNode("//DBkey//optionDB[@value='" + sDsnid + "']");
					sDsn=objOptSelected.InnerText;
					sAdditionalParamsChecked=p_objXMLForm.SelectSingleNode("//loginscreenkeys/additionalparamschecked").InnerText;
					sAdditionalParams=p_objXMLForm.SelectSingleNode("//loginscreenkeys/additionalparams").InnerText;
					if(sView=="server")
					{
						objDBName=(XmlElement)p_objXMLForm.SelectSingleNode("//loginscreenkeys/inputdatabasename");
						objServerName=(XmlElement)p_objXMLForm.SelectSingleNode("//loginscreenkeys/inputservername");
						if (!objRmWin.IsConnectionValid(sDsn, objUserLogin.InnerText, objPasswordDsn.InnerText, objServerName.InnerText, objDBName.InnerText, sView, sCallingMethod, sAdditionalParams, sAdditionalParamsChecked, ref sMsg, ref sConnStr))
						{
							sError="Yes";
						}
						
					}
					else
					{
						objDBName=(XmlElement)p_objXMLForm.SelectSingleNode("//loginscreenkeys/inputfileselect");
						if (!objRmWin.IsConnectionValid(sDsn, objUserLogin.InnerText, objPasswordDsn.InnerText, "", objDBName.InnerText, sView, sCallingMethod, sAdditionalParams, sAdditionalParamsChecked, ref sMsg, ref sConnStr))
						{
							sError="Yes";
						}

					}
					

				((XmlElement)p_objXMLForm.SelectSingleNode("//dsnmessage")).Attributes["message"].Value=sMsg;
				((XmlElement)p_objXMLForm.SelectSingleNode("//dsnmessage")).Attributes["iserror"].Value=sError;
				((XmlElement)p_objXMLForm.SelectSingleNode("//dsnmessage")).Attributes["mode"].Value="connectionmessage";
				 
				htAttributes =new Hashtable();
				if(sError=="Yes")
				{
				   //((XmlElement)p_objXMLForm.SelectSingleNode("//buttonscriptDSNWizard[@name='btnNext']")).SetAttribute("disabled","true");
				}
				if(p_objXMLForm.SelectSingleNode("//control[@name='hdInvokedFrom']").InnerText!="SetDocPath" )
				{
                    htAttributes.Add("functionname", "SetWizardAction(this,'ValidateDB',event)");
					
					//PublicFunctions.ProcessButtons(ref p_objXMLForm, ref htAttributes,false);

				}
				else
				{
                    htAttributes.Add("functionname", "SetWizardAction(this,'ExistingDsn',event)");
				 //PublicFunctions.ProcessButtons(ref p_objXMLForm, ref htAttributes,true);

				}
				
				p_objXMLForm.SelectSingleNode("//DBkey").InnerXml="";
				 p_objXMLForm.SelectSingleNode("//loginkey").InnerXml="";
				p_objXMLForm.SelectSingleNode("//loginscreenkeys/inputservername").InnerText="";
				p_objXMLForm.SelectSingleNode("//loginscreenkeys/inputdatabasename").InnerText="";
				p_objXMLForm.SelectSingleNode("//loginscreenkeys/inputlogin").InnerText="";
				p_objXMLForm.SelectSingleNode("//loginscreenkeys/inputpassword").InnerText="";
				p_objXMLForm.SelectSingleNode("//loginscreenkeys/IsDsn").InnerText="";
				
				p_objXMLForm.SelectSingleNode("//connkey").InnerText=RMCryptography.EncryptString(sConnStr);//Deb:Pen Testing
				
				
				
			}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			finally
			{
				 objElement=null;
				 objUserLogin=null;
				 objPasswordDsn=null;
				 objDBName=null;
				 objOptSelected=null;
				 objServerName=null;
				 arrStr=null;
				 htAttributes=null;

			}
			return bRetVal;
		}
		/// <summary>
		/// This function checks for licence information  
		/// </summary>
		/// <param name="p_objXMLForm">Contains licence related information.</param>
		/// <returns>True or false.</returns>
		public bool GetValidateLicenseXml(ref XmlDocument p_objXMLForm)
		{  
			bool bRetVal=false;
			RMWinSecurity objRmWin=null;
			string sMsg="";
			int iIndexForNonPreDsn=0;
			XmlElement objLicenseCode=null;
			XmlElement objDSName=null;
			int iNumberLicense=0;
			int iDsnId;
			string sLicUpdateDate="";
			bool bErr=false;
			DataSources objDatasource=null;
			DbConnection objConn=null;
			//gagnihotri MITS 11995 Changes made for Audit table
			string sUserName = "";
			XmlNode objXMLNode = null;
			try
			{

                objRmWin = new RMWinSecurity(m_iClientId);
				objLicenseCode=(XmlElement)p_objXMLForm.SelectSingleNode("//licenseKey//control[@name='inputLicensecode']");
				objDSName=(XmlElement)p_objXMLForm.SelectSingleNode("//licenseKey//control[@name='inputDatasourcename']");
				//gagnihotri MITS 11995 Changes made for Audit table
				objXMLNode = p_objXMLForm.SelectSingleNode("//licenseKey//control[@name='hdUserLoginName']");
				if (objXMLNode != null)
					sUserName = objXMLNode.InnerText;
				
					objRmWin.ValidateLicenseInfo(objLicenseCode.InnerText,objDSName.InnerText,
						ref iNumberLicense,ref sLicUpdateDate,ref sMsg);
				
				
				if(sMsg!="")
				{
					((XmlElement)p_objXMLForm.SelectSingleNode("//dsnmessage")).Attributes["message"].Value=sMsg;
					((XmlElement)p_objXMLForm.SelectSingleNode("//dsnmessage")).Attributes["iserror"].Value="Yes";
					((XmlElement)p_objXMLForm.SelectSingleNode("//dsnmessage")).Attributes["mode"].Value="licensevalidationfailed";
					bErr=true;

				}
				
				
			if(!bErr)
				{

                    iDsnId = Conversion.ConvertObjToInt(((XmlElement)p_objXMLForm.SelectSingleNode("//loginscreenkeys/EditDsnId")).InnerText, m_iClientId);
				
				 if(iDsnId!=0)
					 //gagnihotri MITS 11995 Changes made for Audit table
					 //objDatasource=new DataSources(iDsnId,false);
                     objDatasource = new DataSources(iDsnId, false, sUserName, m_iClientId);
				  else
				  {
					 //gagnihotri MITS 11995 Changes made for Audit table
					 //objDatasource=new DataSources();
                      objDatasource = new DataSources(sUserName, m_iClientId);
					  objDatasource.DocPathType=0;
					  objDatasource.GlobalDocPath="";
					  objDatasource.Status=false;
				
				  }
					objDatasource.DataSourceName=((XmlElement)p_objXMLForm.SelectSingleNode("//licenseKey//control[@name='inputDatasourcename']")).InnerText;
					objDatasource.RMUserId=((XmlElement)p_objXMLForm.SelectSingleNode("//loginscreenkeys/inputlogin")).InnerText;
					objDatasource.RMPassword=((XmlElement)p_objXMLForm.SelectSingleNode("//loginscreenkeys/inputpassword")).InnerText;
					string sEncyptedConnStr=((XmlElement)p_objXMLForm.SelectSingleNode("//connkey")).InnerText;
                    sEncyptedConnStr = RMCryptography.DecryptString(sEncyptedConnStr);//Deb:Pen Testing
					iIndexForNonPreDsn=sEncyptedConnStr.IndexOf("Server");
					int iUidIndex=sEncyptedConnStr.IndexOf("UID");
					
				if(iUidIndex!=-1)
				{
				   sEncyptedConnStr=sEncyptedConnStr.Substring(0,iUidIndex);
					if(iIndexForNonPreDsn!=-1)
					{
						objDatasource.ConnectionString=sEncyptedConnStr;
						if(p_objXMLForm.SelectSingleNode("//loginscreenkeys/additionalparamschecked").InnerText.Trim()=="True")
						{
							if(p_objXMLForm.SelectSingleNode("//loginscreenkeys/additionalparams").InnerText.Trim()!="")
							objDatasource.ConnectionString=objDatasource.ConnectionString+p_objXMLForm.SelectSingleNode("//loginscreenkeys/additionalparams").InnerText.Trim();
						}
					}
					else
					{
						objDatasource.ConnectionString="";
					}
					sEncyptedConnStr=sEncyptedConnStr+"UID="+objDatasource.RMUserId+";"+"PWD="+objDatasource.RMPassword+";";
				}
						 
					objDatasource.NumLicenses=iNumberLicense;
					objDatasource.LicUpdDate= Conversion.ToDate( sLicUpdateDate ) ; //System.DateTime.Parse(sLicUpdateDate);
					try
					{  	
						objConn= DbFactory.GetDbConnection(sEncyptedConnStr);	
						objConn.Open();
						objDatasource.DbType = objConn.DatabaseType;
					 }
					finally{objConn.Close();}
				  
				  objDatasource.Save();
				  p_objXMLForm.SelectSingleNode("//control[@name='hdNewDBId']").InnerText=objDatasource.DataSourceId.ToString();
				  p_objXMLForm.SelectSingleNode("//control[@name='hdEntityType']").InnerText="Datasource";
				}
				
				p_objXMLForm.SelectSingleNode("//DBkey").InnerXml="";
				p_objXMLForm.SelectSingleNode("//loginkey").InnerXml="";
				p_objXMLForm.SelectSingleNode("//licenseKey").InnerXml="";
				p_objXMLForm.SelectSingleNode("//resultkeys").InnerXml="";
			}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			finally
			{
				 objRmWin=null;
				 objLicenseCode=null;
				 objDatasource=null;
				 objXMLNode=null;
				 if(objConn!=null)
					 objConn.Dispose();
			}
			return bRetVal;
		}
		
		/// <summary>
		///  This function fills XmlDocument corresponding to DSNWizardServerLogin.
		/// </summary>
		/// <param name="p_objXMLForm">Xmldocument to be filled.</param>
		/// <param name="p_sServerName">Server Name,pass either "-1" or some appropriate value.</param>
		/// <param name="p_sDBName">Database name,pass either "-1" or some appropriate value.</param>
		/// <param name="p_sLoginName">Login name,pass either "-1" or some appropriate value.</param>
		/// <param name="p_sLoginPassword">Password,pass either "-1" or some appropriate value.</param>
		/// <returns>True if successful else throws custom exception.</returns>
		public bool FillDSNWizardServerLoginDOM(ref XmlDocument p_objXMLForm,string p_sServerName,string p_sDBName,string p_sLoginName,string p_sLoginPassword )
		{
			bool bRetVal=false;
			XmlNodeList objXMLNodeList=null;
			try
			{
				objXMLNodeList=p_objXMLForm.GetElementsByTagName("dsnloginscreen");
				foreach(XmlElement objTempXmlElem in objXMLNodeList)
				{
					if(p_sServerName=="-1")
						p_sServerName="";
					objTempXmlElem.SetAttribute("servername",p_sServerName);
					if(p_sDBName=="-1")
						p_sDBName="";
					objTempXmlElem.SetAttribute("dbname",p_sDBName);
					if(p_sLoginName=="-1")
						p_sLoginName="";
					objTempXmlElem.SetAttribute("loginname",p_sLoginName);
					if(p_sLoginPassword=="-1")
						p_sLoginPassword="";
					objTempXmlElem.SetAttribute("loginpassword",p_sLoginPassword);
					
				}
				
				objXMLNodeList=null;
				bRetVal=true;
	
			}
			catch(Exception p_objException)
			{
                throw new XmlOperationException(Globalization.GetString("Security.FillDSNWizardServerLoginDOM.FillXmlDocumentError", m_iClientId), p_objException);
			}
			finally
			{
				if(objXMLNodeList!=null)
					objXMLNodeList=null;
			}
			return bRetVal;
		}
		/// <summary>
		/// This function fills XmlDocument corresponding to DSNWizardDSNLogin.
		/// </summary>
		/// <param name="p_objXMLForm">Xmldocument to be filled</param>
		/// <param name="p_sLoginName">Login name,pass either "-1" or some appropriate value.</param>
		/// <param name="p_sLoginPassword">Password,pass either "-1" or some appropriate value.</param>
		/// <returns>True if successful else throws custom exception.</returns>
		public bool FillDSNWizardDSNLoginDOM(ref XmlDocument p_objXMLForm, string p_sLoginName, string p_sLoginPassword)
		{ 
			bool bRetVal=false;
			XmlNodeList objXMLNodeList=null;
			try
			{
				objXMLNodeList=p_objXMLForm.GetElementsByTagName("dsnloginscreen");
				foreach(XmlElement objTempXmlElem in objXMLNodeList)
				{
					if(p_sLoginName=="-1")
						p_sLoginName="";
					objTempXmlElem.SetAttribute("loginname",p_sLoginName);
					if(p_sLoginPassword=="-1")
						p_sLoginPassword="";
					objTempXmlElem.SetAttribute("loginpassword",p_sLoginPassword);
					
				}
				objXMLNodeList=null;
				bRetVal=true;
			}
			catch(Exception p_objException)
			{
                throw new XmlOperationException(Globalization.GetString("Security.FillDSNWizardDSNLoginDOM.FillXmlDocumentError", m_iClientId), p_objException);
			}
			finally
			{
				if(objXMLNodeList!=null)
					objXMLNodeList=null;
			}
			return bRetVal;
		}
		/// <summary>
		/// This function fills XmlDocument corresponding to DSNWizardFileLogin.
		/// </summary>
		/// <param name="p_objXMLForm">Xmldocument to be filled.</param>
		/// <param name="p_sDbFileName">>DbFileName,pass either "-1" or some appropriate value.</param>
		/// <param name="p_sLoginName">>LoginName,pass either "-1" or some appropriate value.</param>
		/// <param name="p_sLoginPassword">>Password,pass either "-1" or some appropriate value.</param>
		/// <returns>True if successful else throws custom exception.</returns>
		public bool FillDSNWizardFileLoginDOM(ref XmlDocument p_objXMLForm, string p_sDbFileName, string p_sLoginName, string p_sLoginPassword)
		{ 
			bool bRetVal=false;
			XmlNodeList objXMLNodeList=null;
			try
			{
				objXMLNodeList=p_objXMLForm.GetElementsByTagName("dsnloginscreen");
				foreach(XmlElement objTempXmlElem in objXMLNodeList)
				{  
					if(p_sDbFileName=="-1")
						p_sDbFileName="";
					objTempXmlElem.SetAttribute("dbfilename",p_sDbFileName);
					if(p_sLoginName=="-1")
						p_sLoginName="";
					objTempXmlElem.SetAttribute("loginname",p_sLoginName);
					if(p_sLoginPassword=="-1")
						p_sLoginPassword="";
					objTempXmlElem.SetAttribute("loginpassword",p_sLoginPassword);
					
				}
				objXMLNodeList=null;
				bRetVal=true;
			}
			catch(Exception p_objException)
			{
                throw new XmlOperationException(Globalization.GetString("Security.FillDSNWizardFileLoginDOM.FillXmlDocumentError", m_iClientId), p_objException);
			}
			finally
			{
				if(objXMLNodeList!=null)
					objXMLNodeList=null;
			}
			return bRetVal;
		}
		private string EncryptDocPathCredentials(string p_sDocPath)
		{
			string[] sPairs;
			try
			{
				sPairs= p_sDocPath.Split(';');
				string [] sPair = null;
				for(int i=0;i< sPairs.Length;i++)
				{
					sPair = sPairs[i].Split('=');
					if(sPair[0].Trim().ToUpper() == "UID" || sPair[0].Trim().ToUpper() == "PWD")
						sPairs[i] = String.Format("{0}={1}",sPair[0], RMCryptography.EncryptString(sPair[1]));
				}
			}
			finally
			{
			  
			}
			return String.Join(";",sPairs);
		}
		//-- This function has not been tested as it depends upon the xml generated from front end.
		/// <summary>
		/// This function fills the selected datasource xmldocument.
		/// </summary>
		/// <param name="p_objXMLForm">XmlDocument to be filled.</param>
		/// <param name="p_iId">Item id to be selected from XmlDocument.</param>
		/// <returns>>True if successful else throws custom exception.</returns>
		public bool GetSelectedDataSource(ref XmlDocument p_objXMLForm,int p_iId)
		{
			XmlElement objXmlElem=null;
			XmlNodeList objXmlNodeList=null;
			bool bRetVal=false;
			try
			{
				objXmlNodeList=p_objXMLForm.GetElementsByTagName("dsn");
				objXmlElem=((XmlElement)objXmlNodeList.Item(p_iId));
				objXmlElem.SetAttribute("optselected","true");
				bRetVal=true;
			}
			catch(Exception p_objException)
			{
                throw new XmlOperationException(Globalization.GetString("Security.GetSelectedDataSource.GetSelectedDataSourceError", m_iClientId), p_objException);
			}
			finally
			{
				if(objXmlElem!=null)
					objXmlElem=null;
				if(objXmlNodeList!=null)
					objXmlNodeList=null;
			}
			return bRetVal;
		}
		/// <summary>
		/// This function fills XmlDocument corresponding to UserAccessPermissions.
		/// </summary>
		/// <param name="p_objXMLForm">Xmldocument to be filled</param>
		/// <returns>True if successful else throws custom exception.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool FillUserAccessPermDOM(ref XmlDocument p_objXMLForm)  
		{
			bool bRetVal =false,bIdFound=false;
			string sFieldName="";
			UserLogins objUserLogin=null;
			XmlNodeList objXMLNodeList=null;
			ArrayList arrlstLoginCol=null;

			int iIndex,iID=0;
			try
			{
				objUserLogin= new UserLogins(m_iClientId);
				arrlstLoginCol=new ArrayList();
				if(!objUserLogin.Load(ref arrlstLoginCol))//Load all the UserLogin in the system.
				{
					bRetVal=false;
					return bRetVal;
				}
				/*Check whether the desired UserId and DataSourceId
				were ever persisted in permanent data storage.
				*/
				for(iIndex=0;iIndex<arrlstLoginCol.Count;iIndex++)
				{
					if (((UserLogins)arrlstLoginCol[iIndex]).DatabaseId==DSNID && ((UserLogins)arrlstLoginCol[iIndex]).UserId==ID)
					{
						iID = iIndex;
						bIdFound=true;
						break;
					}
				}
				/*If not persisted ,following "if" loop will throw the "RecordNotFoundException" 
				  exception.
				 */
				if(!bIdFound)
				{
                    throw new RecordNotFoundException(Globalization.GetString("Security.FillUserAccessPermDOM.InvalidDsnIdOrUserId", m_iClientId));
				}
				objXMLNodeList = p_objXMLForm.GetElementsByTagName("control");
				
				foreach(XmlElement objTempXmlElem in objXMLNodeList)//This loop will fill all the details related to UserLogin.
				{
					sFieldName = objTempXmlElem.GetAttribute("name").ToLower().Trim();
					#region
					switch(sFieldName)
					{
						case "userpermid" :
						{
							objTempXmlElem.SetAttribute("value",(((UserLogins)arrlstLoginCol[iID]).UserId).ToString());
							break;
						}
						case "userid" :
						{
							objTempXmlElem.SetAttribute("value",(((UserLogins)arrlstLoginCol[iID]).UserId).ToString());
							break;
						}
						case "dsnid" :
						{
							
							objTempXmlElem.SetAttribute("value",((UserLogins)arrlstLoginCol[iID]).DatabaseId.ToString());
							
							break;
						}
						case "loginname" :
						{   
							if(!ContainsNull(((UserLogins)arrlstLoginCol[iID]).LoginName))
							{
								objTempXmlElem.InnerText=((UserLogins)arrlstLoginCol[iID]).LoginName;
							}
							break;
						}
						case "password" :
						{
							if(!ContainsNull(((UserLogins)arrlstLoginCol[iID]).Password))
							{
								objTempXmlElem.InnerText=((UserLogins)arrlstLoginCol[iID]).Password;
							}
							break;
						}
						case "forcepwdchange" :
						{
							if(!ContainsNull(((UserLogins)arrlstLoginCol[iID]).ForceChangePwd.ToString()))
							{
								if(((UserLogins)arrlstLoginCol[iID]).ForceChangePwd == 0)
									objTempXmlElem.InnerText="False";
								else
									objTempXmlElem.InnerText="True";

							}
							break;
						}
						case  "expdate":
						{  
							if(((UserLogins)arrlstLoginCol[iID]).PasswordExpire!=System.DateTime.MinValue)
							{
								//Ratheen--#1761 changes made for permission expires check box
								objTempXmlElem.InnerText=((UserLogins)arrlstLoginCol[iID]).PasswordExpire.ToShortDateString();	
								objTempXmlElem.Attributes["disabled"].Value="false";
								((UserLogins)arrlstLoginCol[iID]).PermissionExpires = true;
								//if(((UserLogins)arrlstLoginCol[iID]).PermissionExpires)
								objTempXmlElem.PreviousSibling.InnerText = ((UserLogins)arrlstLoginCol[iID]).PermissionExpires.ToString();
								
							}
							break;
						}
						case  "filepath":
						{
							if(!ContainsNull(objTempXmlElem.InnerText=((UserLogins)arrlstLoginCol[iID]).DocumentPath))
							{
								objTempXmlElem.InnerText=((UserLogins)arrlstLoginCol[iID]).DocumentPath;
							}
							break;
						}
						//parijat : Mits 11690 
						case "hdissmsadminuser":
						{
							XmlElement objSMSAdminFlag = (XmlElement)p_objXMLForm.SelectSingleNode("//control[@name='hdIsSMSAdminUser']");
							this.PopulateSMSAccessValue(objTempXmlElem, ID, objSMSAdminFlag);
							break;
						}
						//Arnab: MITS-10662 - Comment this section as it is moved to FillUserInfoDOM()
						/*
						case  "smsaccess":
						{  
							UserLogins objLogin = (UserLogins)arrlstLoginCol[iID] ;
							XmlElement objSMSAdminFlag = (XmlElement)p_objXMLForm.SelectSingleNode("//control[@name='hdIsSMSAdminUser']") ;
							this.PopulateSMSAccessValue(  objTempXmlElem, ref objLogin, objSMSAdminFlag ) ;
							break;
						}*/
						//MITS-10662 - End
						case "lstmodulegroups" :
						{   
							XmlElement objTemp=p_objXMLForm.CreateElement("option");
							objTemp.SetAttribute("value","");
							objTemp.InnerText = "Please select the Module Group.";
							objTempXmlElem.AppendChild(objTemp);
							//Raman for R5
							//This condition does not seem to be required at all..

							//if(p_objXMLForm.SelectSingleNode("//control[@name='dsnid']").InnerText.Trim()==p_objXMLForm.SelectSingleNode("//control[@name='hdDsnIdForCurrentlyLoadedMGrps']").InnerText.Trim())
							//{   
								try
								{
									CreateOptionList(ref p_objXMLForm,objTempXmlElem,CreateOptionsFor.DSNSpecificModuleGrps);
									//objTempXmlElem.Attributes["disabled"].Value="false";
								}
								catch(Exception p_objException)
								{
									//if(p_objException is LoadModuleGrpException)
										//objTempXmlElem.Attributes["disabled"].Value="true";
								}
								
								
							//}
							break;
						}
						case "ispasswordpolicyenable":
						objTempXmlElem.InnerText = PublicFunctions.IsPwdPolEnabled().ToString();
						break;
					}
					#endregion
				}	
				objXMLNodeList=null;
				sFieldName="";
				objXMLNodeList= p_objXMLForm.SelectNodes("//control[@controltype='timeline']");
				foreach(XmlElement objTempXmlElem in objXMLNodeList)
				{
					sFieldName = objTempXmlElem.GetAttribute("name").ToLower().Trim();
					#region
					switch(sFieldName)
					{
							
						case "mon" :
							objTempXmlElem.SetAttribute("value1",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).MonStart));
							objTempXmlElem.SetAttribute("value2",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).MonEnd));
							if(((UserLogins)arrlstLoginCol[iID]).MonAccess == true)
							{
								objTempXmlElem.InnerText="True";
							}
							else
							{
								objTempXmlElem.InnerText="";
							}
							break;
						case "tue" :
							objTempXmlElem.SetAttribute("value1",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).TueStart));
							objTempXmlElem.SetAttribute("value2",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).TueEnd));
							if(((UserLogins)arrlstLoginCol[iID]).TueAccess == true)
							{
								objTempXmlElem.InnerText="True";
							}
							else
							{
								objTempXmlElem.InnerText="";
							}
							break;
						case "wed" :
							objTempXmlElem.SetAttribute("value1",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).WedStart));
							objTempXmlElem.SetAttribute("value2",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).WedEnd));
							if(((UserLogins)arrlstLoginCol[iID]).WedAccess == true)
							{
								objTempXmlElem.InnerText="True";
							}
							else
							{
								objTempXmlElem.InnerText="";
							}
							break;
						case "thu" :
							objTempXmlElem.SetAttribute("value1",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).ThuStart));
							objTempXmlElem.SetAttribute("value2",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).ThuEnd));
							if(((UserLogins)arrlstLoginCol[iID]).ThuAccess == true)
							{
								objTempXmlElem.InnerText="True";
							}
							else
							{
								objTempXmlElem.InnerText="";
							}
							break;
						case "fri" :
							objTempXmlElem.SetAttribute("value1",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).FriStart));
							objTempXmlElem.SetAttribute("value2",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).FriEnd));
							if(((UserLogins)arrlstLoginCol[iID]).FriAccess == true)
							{
								objTempXmlElem.InnerText="True";
							}
							else
							{
								objTempXmlElem.InnerText="";
							}
							break;
						case "sat" :
							objTempXmlElem.SetAttribute("value1",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).SatStart));
							objTempXmlElem.SetAttribute("value2",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).SatEnd));
							if(((UserLogins)arrlstLoginCol[iID]).SatAccess == true)
							{
								objTempXmlElem.InnerText="True";
							}
							else
							{
								objTempXmlElem.InnerText="";
							}
							break;
						case "sun" :
							objTempXmlElem.SetAttribute("value1",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).SunStart));
							objTempXmlElem.SetAttribute("value2",PublicFunctions.GetPmAmTimeFromDbTimeFormat(((UserLogins)arrlstLoginCol[iID]).SunEnd));
							if(((UserLogins)arrlstLoginCol[iID]).SunAccess == true)
							{
								objTempXmlElem.InnerText="True";
							}
							else
							{
								objTempXmlElem.InnerText="";
							}
							break;
					}
					#endregion

				}
				objXMLNodeList=null;

				//rsolanki2 : Domain authentication updates
				if (Security.bIsDomainAuthenticationEnabled())
				{
					this.UpdateUserPermUIElementsForDomainAuthentication(ref p_objXMLForm);
				}
				else
				{
					this.UpdateUserPermUIElementsForPwdPolicy(ref p_objXMLForm);
				}

				bRetVal=true;
			
			}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			finally
			{
				
				if(objUserLogin!=null)
					objUserLogin=null;
				if(objXMLNodeList!=null)
					objXMLNodeList=null;
			}
			return bRetVal;
		}
		//rsolanki2 : Domain authentication updates 
		private void UpdateUserPermUIElementsForDomainAuthentication(ref XmlDocument p_objDoc)
		{
			p_objDoc.SelectSingleNode("//form/group[@name='useraccessperm']/displaycolumn").RemoveChild(p_objDoc.SelectSingleNode("//control[@name='password']"));
			p_objDoc.SelectSingleNode("//form/group[@name='useraccessperm']/displaycolumn").RemoveChild(p_objDoc.SelectSingleNode("//control[@name='btnUpdate']"));
			p_objDoc.SelectSingleNode("//form/group[@name='useraccessperm']/displaycolumn").RemoveChild(p_objDoc.SelectSingleNode("//control[@name='forcepwdchange']"));
			//p_objDoc.SelectSingleNode("//form/group[@name='useraccessperm']/displaycolumn").RemoveChild(p_objDoc.SelectSingleNode("//control[@name='expdate']"));            
		   
		}
		private void UpdateUserPermUIElementsForPwdPolicy(ref XmlDocument p_objDoc)
		{
			bool bIsEnhEnabled = PublicFunctions.IsPwdPolEnabled() ;

			if(!bIsEnhEnabled)
			{
				p_objDoc.SelectSingleNode("//control[@name='password']").Attributes["readonly"].Value = "false" ;
				//UPDATE UI LOGIC
				//p_objDoc.SelectSingleNode("//form/group[@name='useraccessperm']/displaycolumn").RemoveChild(p_objDoc.SelectSingleNode("//control[@name='btnUpdate']"));
				//p_objDoc.SelectSingleNode("//form/group[@name='useraccessperm']/displaycolumn").RemoveChild(p_objDoc.SelectSingleNode("//control[@name='forcepwdchange']"));
			}
		}
		#region Arnab:MITS-10662 - Commented method and modified
		/*
		private void PopulateSMSAccessValue( XmlElement p_objElement, ref UserLogins p_objUserLogin ,XmlElement p_objSMSSdminFlag )
		{
			SMSUser objSMSUser = null;

			try
			{
					
					objSMSUser =new SMSUser(p_objUserLogin.UserId) ;
					//objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(0),"Select * from SMSUsers where RmUserId = " +p_objUserLogin.UserId.ToString()) ;
					//firstly check by Userid
					if(objSMSUser.RecordId != 0)
					{
						//objReader.Close() ;
						p_objElement.InnerText = "True" ;
						p_objSMSSdminFlag.InnerText = "False" ;

					}
					else
					{
						p_objElement.InnerText = "False";
						//objReader.Close() ;
						objSMSUser= new SMSUser(p_objUserLogin.LoginName) ;
						//objReader = DbFactory.GetDbReader( SecurityDatabase.GetSecurityDsn(0) ,"Select * from SMSUsers where RMLoginName = '"+ p_objUserLogin.LoginName +"'") ;
						if( objSMSUser.RecordId!=0 )
						{
							string sIsAdminUser = objSMSUser.UserId.ToString() ;
							if( sIsAdminUser.IndexOf("-")!=-1 )
							{
							   p_objSMSSdminFlag.InnerText = "True" ;
							}
						  // objReader.Close() ;
						   p_objElement.InnerText = "True" ;
						   

						}
						else
						{
							//objReader.Close() ;
							p_objElement.InnerText = "False" ;
							p_objSMSSdminFlag.InnerText = "False" ;
						}                      
					}                
				//secondly check by loginName
			}
			finally
			{
			  objSMSUser= null ;
			}
		}
		*/
#endregion

		//Arnab: MITS-10662 - Modified method
		/// <summary>PopulateSMSAccessValue()
		/// This method searches for records from the SMS_USERS table depending upon the record_id, user_id
		/// and login_name. If the record is found then it sets the SMSAccess checkbox's value as true else
		/// false. It also searches the conditions for a user to be an admin user
		/// </summary>
		/// <param name="p_objElement">It represents the SMSAccess checkbox value from instance</param>
		/// <param name="iUserID">User ID from USER_TABLE</param>
		/// <param name="p_objSMSSdminFlag">Admin flag</param>
		private void PopulateSMSAccessValue(XmlElement p_objElement, int iUserID, XmlElement p_objSMSSdminFlag)
		{
			SMSUser objSMSUser = null;
			try
			{
				objSMSUser = new SMSUser(iUserID,m_iClientId);                
				//Select all data from SMS_USERS WHERE RmUserId = iUserID
				//firstly check by Userid
				if (objSMSUser != null)
				{
					if (objSMSUser.RecordId != 0 && objSMSUser.RmLoginName != "") //for normal user
					{
						p_objElement.InnerText = "True";
						p_objSMSSdminFlag.InnerText = "False";
					}
					else
					{
						p_objElement.InnerText = "False";
						objSMSUser = new SMSUser(-iUserID, m_iClientId); //creating SMSObject for the admin user (-ve userid)
						//"Select ALL from SMS_Users where RMLoginName = SMS_USERS.LoginName;
						if (objSMSUser.RecordId != 0) //for admin user
						{
							string sIsAdminUser = objSMSUser.UserId.ToString();
							if (sIsAdminUser.IndexOf("-") != -1)
							{
								p_objSMSSdminFlag.InnerText = "True";
							}
							p_objElement.InnerText = "True";
						}
						else
						{
							p_objElement.InnerText = "False";
							p_objSMSSdminFlag.InnerText = "False";
						}
					}                    
				}
			}
			finally
			{
				objSMSUser = null;
			}
		}

		/// <summary>
		/// This function acts as wrapper over actual function call of FillEmailOptionsDOM() function which actually fills XmlDocument corresponding to EmailOptions.
		/// </summary>
		/// <param name="p_objXMLForm">Xmldocument to be filled.</param>
		/// <returns>True if successful else throws custom exception.</returns>
        public bool GetEmailOptionData(ref XmlDocument p_objXMLForm)
		{  
			bool bRetVal=false;
			try
			{
				if(FillEmailOptionsDOM(ref p_objXMLForm))
				{
					bRetVal=true;
					
				}
			}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			finally
			{
			}
			return bRetVal;
		}
		/// <summary>
		/// This function acts as wrapper over actual function call of FillDomainAuthenticationDOM() function which actually fills XmlDocument corresponding to DomainAuthentication.
		/// </summary>
		/// <param name="p_objXMLForm">Xmldocument to be filled.</param>
		/// <returns>True if successful else throws custom exception.</returns>
        public bool GetDomainAuthenticationData(ref XmlDocument p_objXMLForm)
		{
			bool bRetVal = false;
			try
			{
				if (FillDomainOptionsDOM(ref p_objXMLForm))
				{
					bRetVal = true;
				}
			}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			finally
			{
			}
			return bRetVal;
		}
		/// <summary>
		/// It determines whether the input string is empty/null string or not
		/// </summary>
		/// <param name="p_sStrVal">Input string to this function either empty/null or with some value. </param>
		/// <returns>Returns true,if the input string is empty/null or returns false,if input string has some value contained in it.</returns>
		private bool ContainsNull(string p_sStrVal)
		{
			if(p_sStrVal=="" || p_sStrVal==null)
				return true;
			else
				return false;
		}
		/// <summary>
		/// This function creates the option list for various drop down boxes.
		/// </summary>
		/// <param name="p_objXMLForm">Xml doc.</param>
		/// <param name="p_objParentNode">Option list will be appended to this node.</param>
		/// <param name="enumOption">Option for creating option lists.</param>
		private void CreateOptionList(ref XmlDocument p_objXMLForm ,XmlElement p_objParentNode, CreateOptionsFor enumOption)
		{
			DbConnection objConn = null; 
			DbReader objReader=null;
			string sQuery="";
			XmlElement objXmlElem=null;
			bool bGetOptionsForDsnModuleGrps=false;
			string sConnStr="";
			try
			{
				
				switch (enumOption)
				{
					case CreateOptionsFor.AllDsns :
					{
						sQuery = GetQueryForAllDsns();
						objXmlElem =  p_objXMLForm.CreateElement("option");
						objXmlElem.SetAttribute("value","");
						objXmlElem.InnerText = "Please select the Datasource...";
						p_objParentNode.AppendChild(objXmlElem);
						objXmlElem = null;
						break;
					}
					case CreateOptionsFor.UserSpecificDsns :
					{
						sQuery=GetQueryForUserSpecificDsns();
						break;
					}
					case CreateOptionsFor.DSNSpecificModuleGrps :
					{
						bGetOptionsForDsnModuleGrps=true;
                        sQuery = GetQueryForDSNSpecificModuleGrps(Conversion.ConvertObjToInt(p_objXMLForm.SelectSingleNode("//control[@name='dsnid']").InnerText, m_iClientId), ref sConnStr);
						break;
					}
				}
				if(!bGetOptionsForDsnModuleGrps)
				objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
				else
				objConn = DbFactory.GetDbConnection(sConnStr);
				try
				{
					objConn.Open();
				}
				catch
				{
					throw new LoadModuleGrpException();
				}
				objReader=objConn.ExecuteReader(sQuery);
				
				
				while(objReader.Read())
				{
					objXmlElem =  p_objXMLForm.CreateElement("option");
					objXmlElem.SetAttribute("value",Conversion.ConvertObjToStr(objReader.GetInt32(0)));
					objXmlElem.InnerText = objReader.GetString(1);
					p_objParentNode.AppendChild(objXmlElem);
					objXmlElem = null;
				}
				
			}
			
			//catch(Exception p_objException)
			//{
			//	 throw p_objException;
			//}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objXmlElem=null;
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
			
		}
		/// <summary>
		/// This function returns the qury for getting all dsns.
		/// </summary>
		/// <returns>Query string.</returns>
		public string GetQueryForAllDsns()
		{
			//Gagan 12/07/2006 - Changes made for MITS 8480: Start
			//Sorting by DSN for 'Allow user to login  to Datasources' dropdown

			return "Select DSNID, DSN " +
				"From DATA_SOURCE_TABLE ORDER BY DSN" ;
 
			//Gagan 12/07/2006 - Changes made for MITS 8480: End 
		}

		/// <summary>
		/// This function returns query string for dsn specific module grps.
		/// </summary>
		/// <param name="p_iDsnId">Dsn id</param>
		/// <param name="p_sConnStr">Conn str.</param>
		/// <returns>Qury string.</returns>
		public string GetQueryForDSNSpecificModuleGrps(int p_iDsnId, ref string p_sConnStr)
		{
			DataSources objDataSource;
			try
			{
                objDataSource = new DataSources(p_iDsnId, false, m_iClientId);
				if(objDataSource.ConnectionString == "")
					p_sConnStr = "DSN=" + objDataSource.DataSourceName + ";UID=" + objDataSource.RMUserId + ";PWD=" + objDataSource.RMPassword;
				else
					p_sConnStr = objDataSource.ConnectionString;
		 
				return "SELECT * " + 
					"FROM USER_GROUPS ORDER BY GROUP_NAME";
			}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			finally
			{
			  objDataSource=null;
			}
 
		}
		/// <summary>
		/// This function returns the query string for User specific dsns.
		/// </summary>
		/// <returns>Query string.</returns>
		public string GetQueryForUserSpecificDsns()
		{
			   return "Select USER_DETAILS_TABLE.DSNID, DATA_SOURCE_TABLE.DSN "+
					  "From USER_DETAILS_TABLE , DATA_SOURCE_TABLE "+
					  "Where USER_DETAILS_TABLE.USER_ID =" + m_iID + "AND USER_DETAILS_TABLE.DSNID= DATA_SOURCE_TABLE.DSNID";
		}

	}
	/// <summary>
	///  Enum containing options for creating option lists. 
	/// </summary>
	public enum CreateOptionsFor:byte
	{
		AllDsns = 1,
		UserSpecificDsns=2,
		DSNSpecificModuleGrps=3
	};
}
