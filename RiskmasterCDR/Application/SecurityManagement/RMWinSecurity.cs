using System;
using System.Collections;
using System.Diagnostics;
using Microsoft.Win32;
using Riskmaster.Common;
using Riskmaster.Common.Win32;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Security.Encryption;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;




namespace Riskmaster.Application.SecurityManagement
{
	/// <summary>
	///Author  :   Aashish Bhateja
	///Dated   :   7th,July 2004
	///Purpose :   Contains general functions related to Security.
	/// </summary>
	public class RMWinSecurity
	{
        private int m_iClientId = 0;

		/// <summary>
		/// Used to get the Driver file name in LoadODBCDrivers function.
		/// </summary>
		private const string REG_ODBC_SYSTEM_ROOT = "Software\\ODBC\\ODBCINST.INI";

		/// <summary>
		/// Used to get the Driver file name in LoadODBCDrivers function.
		/// </summary>
		private const string REG_ODBC_DRIVERFILENAME_KEY = "Driver";

		/// <summary>
		/// Used to get the file extension in LoadODBCDrivers function.
		/// </summary>
		private const string REG_ODBC_DBFILEEXTENSION_KEY = "FileExtns";

		/// <summary>
		/// Used to get the file usage in LoadODBCDrivers function.
		/// </summary>
		private const string REG_ODBC_DBFILEUSAGE_KEY = "FileUsage";

		private const string CODE_ACCESS_PUBLIC_KEY = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";

		/// <summary>
		/// This constant is used while establishing connection through a pre-defined 
		/// DSN instead of ODBC Driver.
		/// </summary>
		public const string USE_DSN = "Yes";

        /// <summary>
        /// The flag is true when the driver is for Policy system connection string.
        /// </summary>
        public static bool IsPolicyDrivers 
        { 
            get; 
            set; 
        }
		/// <summary>
		/// Default constructor.
		/// </summary>
		public RMWinSecurity(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}

		//gagnihotri MITS 11995 Changes made for Audit table
		private string m_sUserName = "";

		/// <summary>
		/// OVERLOADED: Class constructor
		/// </summary>
		/// <param name="p_sUserName">string containing the user's name</param>
		public RMWinSecurity(string p_sUserName, int p_iClientId)
		{
			m_sUserName = p_sUserName;
            m_iClientId = p_iClientId;
		}

		/// <summary>
		/// Used in LoadModuleGroups() function to check whether Users exist in 
		/// USER_MEMBERSHIP table in underlying RM DB.
		/// </summary>
		/// <param name="p_sConnectStr">Connection String to connect to RM DB.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		private bool CheckUsers(string p_sConnectStr)
		{
			bool bReturn = false;
			string sSQL = "";
			DbConnection objConn = DbFactory.GetDbConnection(p_sConnectStr);
			DbReader objReader = null;
			try
			{
				objConn.Open();
				sSQL = "SELECT * FROM USER_MEMBERSHIP";
				objReader = objConn.ExecuteReader(sSQL);
				
				if(objReader.Read())
					bReturn = true;
				else
					bReturn = false;
			}
			catch(Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("RMWinSecurity.CheckUsers.CheckUsersError", m_iClientId),p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
			return bReturn;
		}
		
		//-- Not Yet Tested -- RemoveTransparencyChars() throwing some error.
		/// <summary>
		/// Clears the License History.
		/// </summary>
		/// <param name="p_sCode">Code required to clear the License History.</param>
		/// <param name="p_sMessage">Message, represents whether the License history
		/// has been cleared or not.
		/// </param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool ClearLicenseHistory(string p_sCode,ref string p_sMessage)
		{
			bool bReturn = false;
			int iIndex = 0;
			DbConnection objConn = null;
			string sSQL = "";
			string sDate = "", sTmp = "";
			
			try
			{
				sTmp = p_sCode.Trim();
				try
				{
					sTmp = RMCryptography.DecryptString(sTmp);
				}
				catch
				{
					//	Silently move on
				}
				iIndex = sTmp.IndexOf(':');
				if(iIndex == -1)
				{
					bReturn = false;
					p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
					return bReturn;
				}
				sDate = sTmp.Substring(0,iIndex);
				sTmp = sTmp.Substring(iIndex+1);
			
				if(sDate != Conversion.ToDbDate(DateTime.Now))
				{
					bReturn = false;
                    p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
					return bReturn;
				}
				if(sTmp != "Mynd")
				{
					bReturn = false;
                    p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
					return bReturn;
				}
				objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
				objConn.Open();
				sSQL = "DELETE FROM MACHINE_LOGIN";
				objConn.ExecuteNonQuery(sSQL);

                p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistorySuccess", m_iClientId);
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryError", m_iClientId), p_objException);
			}
			finally
			{
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
			return bReturn;
		}

		/// <summary>
		/// Loads the ODBC Drivers for DB Connection Wizard.
		/// </summary>
		/// <returns>generic string List collection containing all supported ODBC Drivers</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public List<string> LoadODBCDrivers()
		{
			const string REG_ODBC_SYSTEM_ROOT = "Software\\ODBC\\ODBCINST.INI";
			const string REG_ODBC_DRIVERFILENAME_KEY = "Driver";

			List<string> arrODBCDrivers = new List<string>();

			//Get the list of supported as well as available ODBC Drivers
			var arrSupportedODBCDrivers = GetODBCDrivers().ToList<string>();
			var arrInstalledODBCDrivers = RegistryManager.GetRegistrySubKeys(REG_ODBC_SYSTEM_ROOT).ToList<string>();

			//Find the intersection of all supported drivers with the currently installed drivers
			IEnumerable<string> arrAvailableODBCDrivers = arrSupportedODBCDrivers.Intersect(arrInstalledODBCDrivers);

			//Iterate through the intersection results to obtain additional driver information/details
			foreach (var supportedODBCDriver in arrAvailableODBCDrivers)
			{
				string strDriverFileName = RegistryManager.ReadRegistryKey(string.Format("{0}\\{1}", REG_ODBC_SYSTEM_ROOT, supportedODBCDriver), REG_ODBC_DRIVERFILENAME_KEY);

				if (!string.IsNullOrEmpty(strDriverFileName))
				{
					FileVersionInfo objFileVersionInfo = FileVersionInfo.GetVersionInfo(strDriverFileName);
					string strDriverVersion = objFileVersionInfo.FileVersion;
					string strCompanyName = objFileVersionInfo.CompanyName;
					arrODBCDrivers.Add(string.Format("{0}|{1}|{2}", supportedODBCDriver, strDriverVersion, strCompanyName));
				}//if
			}//foreach

			return arrODBCDrivers;
		}//method: LoadODBCDrivers()

		/// <summary>
		/// Gets a list of the ODBC Drivers on the system
		/// for SQL Server and Oracle
		/// </summary>
		/// <returns>string array containing all of the currently installed SQL Server and Oracle drivers</returns>
		private string[] GetODBCDrivers()
		{

			List<string> arrFilteredDrivers = new List<string>();
			const string ODBC_DRIVERS = @"SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers";
			const string SQL_SERVER_FILTER = "SQL Server";
			const string SQL_NATIVE_CLIENT_FILTER = "SQL Native Client";
			const string ORACLE_FILTER = "Oracle in";
            const string DB2_ODBC_FILTER = "iSeries Access ODBC Driver";

			string[] arrODBCDrivers = RegistryManager.GetRegistryValues(ODBC_DRIVERS);

			foreach (string strODBCDriver in arrODBCDrivers)
			{
                if (strODBCDriver.Contains(SQL_SERVER_FILTER) || strODBCDriver.Contains(SQL_NATIVE_CLIENT_FILTER) || strODBCDriver.Contains(ORACLE_FILTER) || (strODBCDriver.Contains(DB2_ODBC_FILTER) && RMWinSecurity.IsPolicyDrivers))
				{
					arrFilteredDrivers.Add(strODBCDriver);
				} // if

			}//foreach

			return arrFilteredDrivers.ToArray();
		}//method: GetODBCDrivers()

		/// <summary>
		/// Loads the pre-defined datasources for DB Connection Wizard. 
		/// </summary>
		/// <param name="p_arrlstSQLDataSources">Pre-defined datasources.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool LoadPreDefinedDataSources(ref ArrayList p_arrlstSQLDataSources)
		{
			bool bReturn = false;  
			const int BUFFER_LENGTH = 255;
			int hEnv = 0, iResult = 0, iServLen = 0, iDescLen = 0;
			string sServerName = "";
			sServerName = sServerName.PadRight(BUFFER_LENGTH,' ');
			string sDescription = "";
			sDescription = sDescription.PadRight(BUFFER_LENGTH,' ');

			System.Text.StringBuilder sbServerName = new System.Text.StringBuilder();
			sbServerName.Append(sServerName);

			System.Text.StringBuilder sbDescription = new System.Text.StringBuilder();
			sbDescription.Append(sDescription);

			try
			{
				iResult = Functions.SQLAllocEnv(ref hEnv);
				
				do
				{
					iResult = Functions.SQLDataSources(hEnv,Const.SQL_FETCH_NEXT,sbServerName,BUFFER_LENGTH,ref iServLen,sbDescription,BUFFER_LENGTH,ref iDescLen);
					
					if(iResult == Const.SQL_SUCCESS)
						p_arrlstSQLDataSources.Add(sbServerName.ToString().Substring(0,iServLen) + "|" + sbDescription.ToString().Substring(0,iDescLen));
				} while(iResult == Const.SQL_SUCCESS);
				Functions.SQLFreeEnv(hEnv);
				
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.LoadPreDefinedDataSources.LoadPreDefinedDataSourcesError", m_iClientId), p_objException);
			}
			finally
			{
			}
			return bReturn;
		}
		
		/// <summary>
		/// Used to save the datasource related information once the user creates the
		/// datasource.
		/// </summary>
		/// <param name="p_sUseDSN">Represents whether the ODBC Driver/Predefined
		/// datasource is being used.
		/// </param>
		/// <param name="p_sDSName">DS Name.</param>
		/// <param name="p_sODBCDriverName">Driver Name.</param>
		/// <param name="p_sUserName">User Name.</param>
		/// <param name="p_sPassword">Password.</param>
		/// <param name="p_sServerName">ServerName.</param>
		/// <param name="p_sDBFileName">DB FileName.</param>
		/// <param name="p_sView">Whether server/file view is being used.</param>
		/// <param name="p_sAdditionalParams">Additional Parameters.</param>
		/// <param name="p_sAdditionalParamsChecked">Whether Additional Parameters
		/// checked or not.
		/// </param>
		/// <param name="p_iDSNID">DSN ID.</param>
		/// <param name="p_iNumLicenses">Number of Licenses.</param>
		/// <param name="p_datLicenseUpdtDate">License update date.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool Finish(string p_sUseDSN,string p_sDSName,string p_sODBCDriverName,string p_sUserName,string p_sPassword,string p_sServerName,string p_sDBFileName,string p_sView,string p_sAdditionalParams,string p_sAdditionalParamsChecked,ref int p_iDSNID,int p_iNumLicenses,DateTime p_datLicenseUpdtDate)
		{
			string sConnectStr = "";
			bool bReturn = false;

			DataSources objDataSources = new DataSources(m_iClientId);
				
			try
			{
				if(p_sUserName == "-1")
					p_sUserName = "";
			
				if(p_sPassword == "-1")
					p_sPassword = "";
						
				objDataSources.RMUserId = p_sUserName;
				objDataSources.RMPassword = p_sPassword;
			
				if(p_sUseDSN == USE_DSN)
				{
					//-- DSN Type Connections
					objDataSources.ConnectionString = "";
					objDataSources.DataSourceName = p_sDSName;
					if(p_sODBCDriverName.ToLower().IndexOf("microsoft access") > -1)
						objDataSources.DbType = Db.eDatabaseType.DBMS_IS_SQLSRVR;
					else
						objDataSources.DbType = Db.eDatabaseType.DBMS_IS_SYBASE;
				}
				else
				{
					//-- DSN Less Connections
					objDataSources.DataSourceName = p_sDSName;
					if(p_sODBCDriverName.ToLower().IndexOf("microsoft access") > -1)
						objDataSources.DbType = Db.eDatabaseType.DBMS_IS_SQLSRVR;
					else
						objDataSources.DbType = Db.eDatabaseType.DBMS_IS_SYBASE;
				
					//-- Create Connection string for dsn-less connections
					sConnectStr = "Driver={" + p_sODBCDriverName + "};";
				
					if(p_sView != "server")
						//-- Add file name
						sConnectStr = sConnectStr + "Dbq=" + p_sDBFileName + ";";
					else
						if(p_sODBCDriverName.ToLower().IndexOf("sybase") > -1 || p_sODBCDriverName.ToLower().IndexOf("informix") > -1) 
						sConnectStr = sConnectStr + "SRVR=" + p_sServerName + ";";
					else if (p_sODBCDriverName.ToLower().IndexOf("oracle") > -1)  // JP 06.16.2006   Oracle needs both Server= and DBQ= to work.
						sConnectStr = sConnectStr + "Server=" + p_sServerName + ";DBQ=" + p_sServerName + ";";  // JP 6.16.2006
					else if (p_sODBCDriverName.ToLower().IndexOf("db2") > -1)  //-- DB2
						sConnectStr = sConnectStr + "DBALIAS=" + p_sServerName + ";";
					else
						sConnectStr = sConnectStr + "Server=" + p_sServerName + ";";
				
					if(p_sDBFileName != "")
					{
						if(p_sODBCDriverName.ToLower().IndexOf("sybase") > -1)
							sConnectStr = sConnectStr + "DB=" + p_sDBFileName + ";";
						else
							sConnectStr = sConnectStr + "Database=" + p_sDBFileName + ";";
					}

					p_sAdditionalParams = p_sAdditionalParams.Trim();
					if(p_sAdditionalParamsChecked == "1" && p_sAdditionalParams != "")
					{
						if(p_sAdditionalParams.Substring(0,1) == ";")
							sConnectStr = sConnectStr + p_sAdditionalParams.Substring(1);
						else
							sConnectStr = sConnectStr + p_sAdditionalParams.Trim();
						if(sConnectStr.Substring(sConnectStr.Length-1) != ";")
							sConnectStr = sConnectStr + ";";
					}
					objDataSources.ConnectionString = sConnectStr;
				}
			
				objDataSources.DocPathType = 0;
				
				objDataSources.NumLicenses = p_iNumLicenses;
				objDataSources.LicUpdDate = p_datLicenseUpdtDate;
				
				objDataSources.Status = true;
				
				objDataSources.Save();
				p_iDSNID = objDataSources.DataSourceId;
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.Finish.FinishError", m_iClientId), p_objException);
			}
			finally
			{
				objDataSources = null;
			}
			return bReturn;
		}

		/// <summary>
		/// This function is used to check the validity of the created Connection String
		/// </summary>
		/// <param name="strDriverName">ODBC DriverName.</param>
		/// <param name="strUserName">User Name.</param>
		/// <param name="strUserPassword">Password.</param>
		/// <param name="strServerName">ServerName.</param>
		/// <param name="strDatabaseName">Database Name</param>
		/// <param name="p_sView">Whether server/file view is being used.</param>
		/// <param name="p_sCallingMethod">Represents the calling method.</param>
		/// <param name="p_sAdditionalParams">Additional Parameters.</param>
		/// <param name="p_sAdditionalParamsChecked">Whether Additional Parameters
		/// checked or not.
		/// </param>
		/// <param name="p_sMessage">Represents the message, if connection is valid/not
		/// </param>
		/// <param name="p_sConnectionStr">Connection string.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool IsConnectionValid(string strDriverName, string strUserName, string strUserPassword, string strServerName, string strDatabaseName, string p_sView, string p_sCallingMethod, string p_sAdditionalParams, string p_sAdditionalParamsChecked, ref string p_sMessage, ref string p_sConnectionStr)
		{
			string sConnectStr = string.Empty, sResult = string.Empty, sErrMsg = string.Empty;
			bool bReturn = false;
			
			sConnectStr = ADODbFactory.BuildRMConnectionString(strDriverName, strServerName, strDatabaseName, strUserName, strUserPassword);
			p_sConnectionStr = ADODbFactory.BuildRMConnectionString(strDriverName, strServerName, strDatabaseName, RMCryptography.EncryptString(strUserName), RMCryptography.EncryptString(strUserPassword));
					
			//-- For additional parameters
			if(p_sAdditionalParamsChecked == "True" && p_sAdditionalParams.Trim() != "")
			{
				sConnectStr = sConnectStr + p_sAdditionalParams.Trim();
				p_sConnectionStr=p_sConnectionStr+p_sAdditionalParams.Trim();
			}
			//p_sConnectionStr=sConnectStr;
			sResult = PublicFunctions.IsDSNValid(sConnectStr, p_sCallingMethod,ref sErrMsg);
			if(! string.IsNullOrEmpty(sErrMsg))
			{
				p_sMessage=sErrMsg;
				bReturn=false;
			}
			else
			{
				if(sResult == "EOF")
				{
					//-- No database version in database
                    p_sMessage = Globalization.GetString("RMWinSecurity.IsConnectionValid.IsConnectionValidVersionIssue", m_iClientId);
				}
				else
				{
					bReturn = true;
                    p_sMessage = Globalization.GetString("RMWinSecurity.IsConnectionValid.IsConnectionValidSuccess", m_iClientId);
				}
			}
			
			return bReturn;
		}
		
		/// <summary>
		/// Used to load all the modules.
		/// </summary>
		/// <param name="p_sRMDSN">The underlying RM DB DSN.</param>
		/// <param name="p_iGroupID">Group ID for which modules are to be loaded.</param>
		/// <param name="p_arrlstAllModules">An array of all the loaded modules.</param>
		/// <param name="p_arrlstIsAllowed">An array representing whether a module is granted
		/// access or not.
		/// </param>
		/// <param name="p_arrlstIsParent">An array representing whether a module is a 
		/// parent/not.
		/// </param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool LoadAllModules(string p_sRMDSN,int p_iGroupID, ref ArrayList p_arrlstAllModules,ref ArrayList p_arrlstIsAllowed, ref ArrayList p_arrlstIsParent)
		{
			bool bReturn = false;
			ArrayList arrlstParentID = new ArrayList();
			ArrayList arrlstID = new ArrayList();
			string[] arrPermittedModules = null;
			int iCount = 0,jCount = 0;
			bool bFound = false;
			string sAllowedModules = "";
			ArrayList arrlstRMAllModules = new ArrayList();
			ModuleGroups objModuleGroups = new ModuleGroups(p_sRMDSN, m_iClientId);
            RMModules objRMModules = new RMModules(p_sRMDSN, m_iClientId);
			
			try
			{
				if(!objModuleGroups.AddPermissions(p_iGroupID,ref sAllowedModules))
				{
					bReturn = false;
					return bReturn;
				}
			
				arrPermittedModules = sAllowedModules.Split(',');
			
				if(!objRMModules.Load(ref arrlstRMAllModules))
				{
					bReturn = false;
					return bReturn;
				}
			
				for(iCount=0;iCount<arrlstRMAllModules.Count;iCount++)
				{
					int iParentID = ((RMModules)arrlstRMAllModules[iCount]).ParentID;
					int iID = ((RMModules)arrlstRMAllModules[iCount]).ID;
					string sModuleName = ((RMModules)arrlstRMAllModules[iCount]).ModuleName;

					p_arrlstAllModules.Add(iParentID + "|" + iID + "|" + sModuleName);
					arrlstParentID.Add(iParentID);
					arrlstID.Add(iID);
					
					for(jCount=0;jCount<arrPermittedModules.Length;jCount++)
					{
						if(arrPermittedModules[jCount] == Convert.ToString(arrlstID[iCount]))
						{
							bFound = true;
							break;
						}
						else
							bFound = false;
					}
					if(bFound)
						p_arrlstIsAllowed.Add(1);
					else
						p_arrlstIsAllowed.Add(0);
				}
							
				for(iCount=0;iCount<arrlstID.Count;iCount++)
				{			
						for(jCount=0;jCount<arrlstParentID.Count;jCount++)
						{
							if(Convert.ToString(arrlstID[iCount]) == Convert.ToString(arrlstParentID[jCount]))
							{
								p_arrlstAllModules[iCount] = p_arrlstAllModules[iCount] + "|" + "1";
								break;
							}
						}
				}
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.LoadAllModules.LoadAllModulesError", m_iClientId), p_objException);
			}
			finally
			{
				arrlstParentID = null;
				arrlstID = null;
				arrlstRMAllModules = null;
				objModuleGroups = null;
				objRMModules = null;
			}
			return bReturn;
		}

		/// <summary>
		/// Used to load the datasources.
		/// </summary>
		/// <param name="p_arrlstAllDataSources">An array of loaded datasources.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool LoadDataSources(ref ArrayList p_arrlstAllDataSources)
		{
			bool bReturn = false;
			int i = 0;
            DataSources objDataSources = new DataSources(m_iClientId);
			ArrayList arrlstDataSourcesCol = new ArrayList();
			
			try
			{
				if(!objDataSources.Load(ref arrlstDataSourcesCol))
				{
					bReturn = false;
					return bReturn;
				}
			
				for(i=0;i<arrlstDataSourcesCol.Count;i++)
				{
					string sDSName = ((DataSources)arrlstDataSourcesCol[i]).DataSourceName;
					int iDataSourceID = ((DataSources)arrlstDataSourcesCol[i]).DataSourceId;
					string sLoginName = ((DataSources)arrlstDataSourcesCol[i]).RMUserId;
					string sPassword = ((DataSources)arrlstDataSourcesCol[i]).RMPassword;
					p_arrlstAllDataSources.Add(sDSName + "^~^~^" + iDataSourceID + "^~^~^" + sLoginName + "^~^~^" + sPassword);
				}
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.LoadDataSources.LoadDataSourcesError", m_iClientId), p_objException);
			}
			finally
			{
				objDataSources = null;
				arrlstDataSourcesCol = null;
			}
			return bReturn;
		}

		/// <summary>
		/// Used to load the languages.
		/// </summary>
		/// <param name="p_arrlstLanguages">An array of loaded languages.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool LoadLanguages(ref ArrayList p_arrlstLanguages)
		{
			bool bReturn = false;
			string sSQL = "";
            DbConnection objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
			DbReader objReader = null;

			try
			{
				objConn.Open();
                //Deb: ML Changes
				//sSQL = "SELECT * FROM SYS_LANGUAGES ORDER BY LANGUAGE_NAME";
                sSQL = "SELECT * FROM SYS_LANGUAGES WHERE SUPPORTED_FLAG = -1 ORDER BY LANGUAGE_NAME";
                //Deb: ML Changes
				objReader = objConn.ExecuteReader(sSQL);
				
				while(objReader.Read())
				{
					string sLanguageName = objReader.GetString("LANGUAGE_NAME");
					int iLanguageID = objReader.GetInt32("LANG_ID");
					p_arrlstLanguages.Add(sLanguageName + "|" + iLanguageID);
				}
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.LoadLanguages.LoadLanguagesError", m_iClientId), p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
			return bReturn;
		}
		
		/// <summary>
		/// Used to load the module groups.
		/// </summary>
		/// <param name="p_arrlstAllModuleGroups">An array of Loaded module groups.</param>
		/// <param name="p_arrlstUsersAllowed">An array of the users allowed in the respective
		/// module groups.
		/// </param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool LoadModuleGroups(int p_iDsnId, ref ArrayList p_arrlstAllModuleGroups,ref ArrayList p_arrlstUsersAllowed)
		{
			bool bReturn = false;
			int iCount = 0, jCount = 0, kCount = 0;
			DataSources objDataSources = new DataSources(m_iClientId);
			Users objUsers = new Users(m_iClientId);
			ArrayList arrlstDataSourcesCol = new ArrayList();
			ArrayList arrlstUserCol = new ArrayList();

			try
			{
				if(!objUsers.Load(ref arrlstUserCol))
				{
					bReturn = false;
					return bReturn;
				}
                DataSources objDataSource = new DataSources(p_iDsnId, false, m_iClientId);
					string sConnectionStr = "";
					string sDSName = objDataSource.DataSourceName;
					string sLoginName = objDataSource.RMUserId;
					string sPassword = objDataSource.RMPassword;
					ArrayList arrlstModuleGroups = new ArrayList();
					ArrayList arrlstColGroups = new ArrayList();

					if(objDataSource.ConnectionString == "")
						sConnectionStr = "DSN=" + sDSName + ";UID=" + sLoginName + ";PWD=" + sPassword;
					else
						sConnectionStr = objDataSource.ConnectionString;// + ";UID=" + sLoginName + ";PWD=" + sPassword;
					
					ModuleGroups objModuleGroups = new ModuleGroups(sConnectionStr,m_iClientId);
					objModuleGroups.Load(ref arrlstModuleGroups,ref arrlstColGroups);
						
					for(jCount=0;jCount<arrlstColGroups.Count;jCount++)
					{
						string sGroupName = ((ModuleGroups)arrlstColGroups[jCount]).GroupName;
						int iDataSourceID = objDataSource.DataSourceId;
						int iGroupID = ((ModuleGroups)arrlstColGroups[jCount]).ModuleGroupID;

						p_arrlstAllModuleGroups.Add(sGroupName + "^~^~^" + iDataSourceID + "^~^~^" + iGroupID);
					}
				
					sDSName = objDataSource.DataSourceName;
					sLoginName = objDataSource.RMUserId;
					sPassword = objDataSource.RMPassword;
					int iDataSourceId = objDataSource.DataSourceId;
					bool bCheckUsers=CheckUsers(sConnectionStr) ;
					for(kCount=0;kCount<arrlstModuleGroups.Count;kCount++)
					{
						ArrayList arrlstTemp = new ArrayList();
                        SplitForStringPattern(arrlstModuleGroups[kCount].ToString(), "^~^~^", ref arrlstTemp, m_iClientId);
						if(bCheckUsers)
						{
							for(jCount=0;jCount<arrlstUserCol.Count;jCount++)
							{
								int iUserID = ((Users)arrlstUserCol[jCount]).UserId;
								string sFirstName = ((Users)arrlstUserCol[jCount]).FirstName;
								string sLastName = ((Users)arrlstUserCol[jCount]).LastName;
								if(Convert.ToInt32(arrlstTemp[1]) == iUserID)
								{
									arrlstModuleGroups[kCount] = arrlstModuleGroups[kCount] + "^~^~^" + sFirstName + "^~^~^" + sLastName;
									p_arrlstUsersAllowed.Add(arrlstModuleGroups[kCount] + "^~^~^" + iDataSourceId);
								}
							}
						}
					}
				

				bReturn = true;
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				objDataSources = null;
				objUsers = null;
				arrlstDataSourcesCol = null;
				arrlstUserCol = null;
			}
			return bReturn;
		}

		/// <summary>
		/// Verifies the user login name & password before logging in.
		/// </summary>
		/// <param name="p_sUserName">User name.</param>
		/// <param name="p_sPassword">Password.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool LoadUserLoginData(string p_sUserName,string p_sPassword)
		{
			bool bReturn = false;
			string sSQL = "", sUserName = "", sPassword = "";
			string sEMail_Address = "", sSTMP_Server = "";
            DbConnection objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
			DbReader objReader = null;

			try
			{
				objConn.Open();
				sSQL = "SELECT * FROM SETTINGS WHERE ID=1";
				objReader = objConn.ExecuteReader(sSQL);
				
				if(!objReader.Read())
					return bReturn;

				sUserName = objReader.GetString("PARAMU");
				sPassword = objReader.GetString("PARAMP");
				sEMail_Address = objReader.GetString("ADMIN_EMAIL_ADDR");
				sSTMP_Server = objReader.GetString("SMTP_SERVER");
				if(sUserName == "" || sPassword == "")
					return bReturn;

				sUserName = PublicFunctions.Decrypt(sUserName);
				sPassword = PublicFunctions.Decrypt(sPassword);

				if((p_sUserName.ToLower() == sUserName.ToLower()) && (p_sPassword.ToLower() == sPassword.ToLower()))
				{
					bReturn = true;
					return bReturn;
				}
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.LoadUserLoginData.LoadUserLoginDataError", m_iClientId), p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
			return bReturn;
		}

		/// <summary>
		/// Loads the users.
		/// </summary>
		/// <param name="p_arrlstAllUsers">An array of loaded users.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool LoadUsers(ref ArrayList p_arrlstAllUsers)
		{
			bool bReturn = false;
			int i = 0;
			Users objUsers = new Users(m_iClientId);
			ArrayList arrlstUserCol = new ArrayList();

			try
			{
				if(!objUsers.Load(ref arrlstUserCol))
					return bReturn;
			
				for(i=0;i<arrlstUserCol.Count;i++)
				{
					string sFirstName = ((Users)arrlstUserCol[i]).FirstName;
					string sLastName = ((Users)arrlstUserCol[i]).LastName;
					int iUserID = ((Users)arrlstUserCol[i]).UserId;
					p_arrlstAllUsers.Add(sFirstName + "^~^~^" + sLastName + "^~^~^" + iUserID);
				}
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.LoadUsers.LoadUsersError", m_iClientId), p_objException);
			}
			finally
			{
				objUsers = null;
				arrlstUserCol = null;
			}
			return bReturn;
		}

		/// <summary>
		/// Loads the users that are allowed to login to a datasource.
		/// </summary>
		/// <param name="p_arrlstPermToLogin">An array of loaded users.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool LoadUsersLogins(ref ArrayList p_arrlstPermToLogin)
		{
		
			bool bReturn = false;
			int i = 0, j = 0;
			Users objUsers = new Users(m_iClientId);
			DataSources objDataSources = new DataSources(m_iClientId);
			UserLogins objUserLogins = new UserLogins(m_iClientId);
			ArrayList arrlstDataSourcesCol = new ArrayList();
			ArrayList arrlstLoginCol = new ArrayList();
			ArrayList arrlstUserCol = new ArrayList();

			try
			{
				if(!objDataSources.Load(ref arrlstDataSourcesCol))
					return bReturn;
				
				if(!objUsers.Load(ref arrlstUserCol))
					return bReturn;
				
				if(!objUserLogins.Load(ref arrlstLoginCol))
					return bReturn;
			
				if(arrlstLoginCol.Count < 1)
					return bReturn;
				
				for(i=0;i<arrlstLoginCol.Count;i++)
				{
					int iUserLoginID = ((UserLogins)arrlstLoginCol[i]).UserId;
					int iDataSourceID = ((UserLogins)arrlstLoginCol[i]).DatabaseId;
					p_arrlstPermToLogin.Add(iDataSourceID + "^~^~^" + iUserLoginID);
				}

				for(i=0;i<p_arrlstPermToLogin.Count;i++)
				{
					ArrayList arrlstTemp = new ArrayList();
                    SplitForStringPattern(p_arrlstPermToLogin[i].ToString(), "^~^~^", ref arrlstTemp, m_iClientId);
					for(j=0;j<arrlstUserCol.Count;j++)
					{
						int iUserID = ((Users)arrlstUserCol[j]).UserId;
						string sFirstName = ((Users)arrlstUserCol[j]).FirstName;
						string sLastName = ((Users)arrlstUserCol[j]).LastName;
						if(arrlstTemp[1].ToString() == Convert.ToString(iUserID))
							p_arrlstPermToLogin[i] = p_arrlstPermToLogin[i] + "^~^~^" + sFirstName + "^~^~^" + sLastName;
					}
				}
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.LoadUsersLogins.LoadUsersLoginsError", m_iClientId), p_objException);
			}
			finally
			{
			   objDataSources = null;
			   objUserLogins = null;
			   arrlstDataSourcesCol = null;
			   arrlstLoginCol = null;
			   arrlstUserCol = null;
			   objUsers = null;
			}
			return bReturn;
		}

		/// <summary>
		/// Used to save the e-mail related data.
		/// </summary>
		/// <param name="p_sSMTP">SMTP Server.</param>
		/// <param name="p_sEMail">E-mail address.</param>
		/// <param name="p_bSecureDSN">Secure DSN/Not.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = CODE_ACCESS_PUBLIC_KEY)]
		public bool SaveEmailData(string p_sSMTP, string p_sEMail, bool p_bSecureDSN,string p_sAtlernateDomain,string p_sReplyToDomain)
		{
			StringBuilder sSQL = new StringBuilder();
			Dictionary<string, string> dictParams = new Dictionary<string, string>();
			const string SECURE_DSN = "0";


			sSQL.Append("UPDATE SETTINGS ");
			sSQL.Append(string.Format("SET SMTP_SERVER={0},", "~SMTP_SERVER~"));
			sSQL.Append(string.Format("ADMIN_EMAIL_ADDR={0},", "~ADMIN_EMAIL~"));
			sSQL.Append(string.Format("SECURE_DSN_LOGIN={0},", "~SECURE_DSN~"));
			sSQL.Append(string.Format("UPDATED_BY_USER={0},", "~UPDATED_USER~"));
			sSQL.Append(string.Format("DTTM_RCD_LAST_UPD={0},", "~LAST_UPDATED~"));
            //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
            sSQL.Append(string.Format("ALTERNATE_DOMAIN={0},", "~ALTERNATE_DOMAIN~"));
            sSQL.Append(string.Format("REPLYTO_DOMAIN={0}", "~REPLYTO_DOMAIN~"));

			dictParams.Add("SMTP_SERVER", p_sSMTP);
			dictParams.Add("ADMIN_EMAIL", p_sEMail);
			dictParams.Add("SECURE_DSN", SECURE_DSN);
			dictParams.Add("UPDATED_USER", m_sUserName);
			dictParams.Add("LAST_UPDATED", Conversion.ToDbDateTime(DateTime.Now));
            //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
            dictParams.Add("ALTERNATE_DOMAIN", p_sAtlernateDomain);
            dictParams.Add("REPLYTO_DOMAIN", p_sReplyToDomain);

			//Execute the query without the overhead of multiple connections
            DbFactory.ExecuteNonQuery(SecurityDatabase.GetSecurityDsn(m_iClientId), sSQL.ToString(), dictParams);
			
			return true;
		}//method: SaveEmailData()

		/// <summary>
		/// Used for adding/editing new Domain names 
		/// </summary>      
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = CODE_ACCESS_PUBLIC_KEY)]
		public bool AddEditDomain(string p_sOldDomainName,string p_sNewDomainName,bool p_bDomainEnabled)
		{
			bool bReturn = false;
			string sSQL = "";
			DbReader objReader = null;
			DbConnection objConn = null;
			int iNewDomainId = 0;

			try
			{
                objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
				objConn.Open();

				if (p_sOldDomainName.Equals(p_sNewDomainName,StringComparison.OrdinalIgnoreCase))
				{
					//updates the status of eixting domain   
					//gagnihotri MITS 11995 Changes made for Audit table
					sSQL = string.Format("UPDATE DOMAIN SET ENABLED={0}, UPDATED_BY_USER='"+ m_sUserName +"', DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) +"WHERE UPPER(NAME)='{1}'", (p_bDomainEnabled == true ? 1 : 0),  p_sNewDomainName.ToUpper());
					objConn.ExecuteNonQuery(sSQL);
					bReturn = true;
					
				}
				else if (p_sOldDomainName.Length==0)
				{
					// adds a new domain name
					sSQL = "SELECT MAX(DOMAINID)+1 FROM DOMAIN";
					objReader = objConn.ExecuteReader(sSQL);
					if (objReader.Read())
					{
						iNewDomainId = objReader.GetInt(0);
					}
					else
						iNewDomainId = 1; 
					//Amit I Bansal reader close to execute query.
					if (objReader != null)
					{
						objReader.Close();
						objReader.Dispose();
					}
					//gagnihotri MITS 11995 Changes made for Audit table
					sSQL = string.Format("INSERT INTO DOMAIN(DOMAINID, NAME, PROVIDER,ENABLED,UPDATED_BY_USER,DTTM_RCD_LAST_UPD) VALUES ({0},'{1}','.',{2},'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")", iNewDomainId, p_sNewDomainName.ToUpper(), (p_bDomainEnabled == true ? 1 : 0));
					objConn.ExecuteNonQuery(sSQL);
					bReturn = true;
					
				}
				else 
				{
					//updates the name/status of exiting domain
					//gagnihotri MITS 11995 Changes made for Audit table
					sSQL = string.Format("UPDATE DOMAIN SET NAME='{0}', ENABLED={1}, UPDATED_BY_USER='" + m_sUserName + "', DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + " WHERE UPPER(NAME)='{2}'", p_sNewDomainName, (p_bDomainEnabled == true ? 1 : 0), p_sOldDomainName.ToUpper());
					objConn.ExecuteNonQuery(sSQL);
					bReturn = true;

				}


				//sSQL = "SELECT * FROM SETTINGS WHERE ID=1";
				//objReader = objConn.ExecuteReader(sSQL);

				//if (objReader.Read())
				//{
				//    objWriter = DbFactory.GetDbWriter(SecurityDatabase.GetSecurityDsn(0));
				//    objWriter.Tables.Add("SETTINGS");
				//    //objWriter.Fields.Add("SMTP_SERVER", p_sSMTP);
				//    //objWriter.Fields.Add("ADMIN_EMAIL_ADDR", p_sEMail);
				//    objWriter.Fields.Add("DOMAIN_LOGIN_ENABLED", p_bDomainEnabled);
				//    objWriter.Where.Add("ID = 1");
				//    objWriter.Execute();
				//}
				//if (objReader != null)
				//{
				//    objReader.Close();
				//}


				/*
				objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(0));
				objConn.Open();
				sSQL = "SELECT * FROM DOMAIN WHERE DOMAINID=1";
				objReader = objConn.ExecuteReader(sSQL);

				if (objReader.Read())
				{
					objWriter = DbFactory.GetDbWriter(SecurityDatabase.GetSecurityDsn(0));
					objWriter.Tables.Add("DOMAIN");
					objWriter.Fields.Add("NAME", p_sDomainName);
					//objWriter.Fields.Add("ENABLED", "1");                    
					objWriter.Where.Add("DOMAINID = 1");
					objWriter.Execute();
				}*/

				bReturn = true;
			}
			catch (Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.SaveDomainAuthenticationData.SaveDomainAuthenticationDataError", m_iClientId), p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					//if(objConn.State == System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
					objConn.Dispose();
					//}
				}
			}
			return bReturn;
		}

		/// <summary>
		/// Used to save the user name and password.
		/// </summary>
		/// <param name="p_sUserName">User name.</param>
		/// <param name="p_sPassword">Password.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool SaveUserLoginData(string p_sUserName,string p_sPassword)
		{
			bool bReturn = false;
			string sSQL = "";
			DbWriter objWriter = null;
			DbReader objReader = null;
			DbConnection objConn = null;
			
			try
			{
                objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
				objConn.Open();
				sSQL = "SELECT * FROM SETTINGS WHERE ID=1";
				objReader = objConn.ExecuteReader(sSQL);
			
				if(objReader.Read())
				{
                    objWriter = DbFactory.GetDbWriter(SecurityDatabase.GetSecurityDsn(m_iClientId));
					objWriter.Tables.Add("SETTINGS");
					objWriter.Fields.Add("PARAMU",PublicFunctions.Encrypt(p_sUserName));
					objWriter.Fields.Add("PARAMP",PublicFunctions.Encrypt(p_sPassword));
					//gagnihotri MITS 11995 Changes made for Audit table
					objWriter.Fields.Add("UPDATED_BY_USER", m_sUserName);
					objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
					objWriter.Where.Add("ID = 1");
					objWriter.Execute();
				}
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.SaveUserLoginData.SaveUserLoginDataError", m_iClientId), p_objException);
			}
			finally
			{
				if(objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objWriter != null)
				{
					objWriter = null;
				}
				if(objConn != null)
				{
					//if(objConn.State == System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
			return bReturn;
		}

		/// <summary>
		/// Used to retrieve various information related to a datasource.
		/// </summary>
		/// <param name="p_iDSNID">DSN ID.</param>
		/// <param name="p_sServer">Server name.</param>
		/// <param name="p_sDatabase">Database name.</param>
		/// <param name="p_sDriver">Driver name.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool SetupData(int p_iDSNID,ref string p_sServer,ref string p_sDatabase,ref string p_sDriver,ref string p_sAddtionalParams, out DataSources p_objDS)
		{
			bool bReturn = false;
			int iPos = 0, iCount = 0, iLastPos = 0;
			string sAdditional = "", sDbq = "", s = "", sKey = "",sUid="",sPwd="";
			string sConnectionStr = "", sValue = "";
			const string DRIVER_KEY = "driver";
			const string DATABASE_KEY = "database";
			const string DATABASESYBASE_KEY = "db";   //--  Sybase
			const string SERVER_KEY = "server";
			const string SERVERSYBASE_KEY = "srvr";   //--  Sybase
			const string SERVERDB2_KEY = "dbalias";   //--  DB2
			const string DBQ_KEY = "dbq";
			const string UID = "uid";
			const string PWD = "pwd";


            DataSources objDataSources = new DataSources(p_iDSNID, false, m_iClientId); 
			p_objDS=objDataSources;
			try
			{
				//--  We can edit only DSN-Less type of the connections
				if(objDataSources.ConnectionString == "")
					return bReturn;

				sConnectionStr = objDataSources.ConnectionString;
			
				iLastPos = 0;
				do
				{
					iPos = sConnectionStr.IndexOf(";",iLastPos);
					if(iPos == -1)
						s = sConnectionStr.Substring(iLastPos,sConnectionStr.Length);
					else
					{
						s = sConnectionStr.Substring(iLastPos, iPos - iLastPos);
						iPos = iPos + 1;
						if(iPos >= sConnectionStr.Length)
							iPos = 0;  //--  Last one
					}

					if(s != "")
					{
						iCount = s.IndexOf("=");
						if(iCount > 0)
						{
							sKey = s.Substring(0,iCount);
							sValue = s.Substring(iCount+1);
							switch(sKey.ToLower())
							{
								case DRIVER_KEY:
									p_sDriver = sValue.Substring(1,sValue.Length - 2);
									break;
								case DATABASE_KEY:
									p_sDatabase = sValue;
									break;
								case DATABASESYBASE_KEY:
									p_sDatabase = sValue;
									break;
								case SERVER_KEY:
									p_sServer = sValue;
									break;
								case SERVERSYBASE_KEY:
									p_sServer = sValue;
									break;
								case SERVERDB2_KEY:
									p_sServer = sValue;
									break;
								case DBQ_KEY:
									sDbq = sValue;
									break;
								case UID:
									sUid = sValue;
									break;
								case PWD:
									sPwd = sValue;
									break;
									
								default:
									p_sAddtionalParams = p_sAddtionalParams + s + ";";
									break;
							}
						}
					}
					iLastPos = iPos;
				}while(iLastPos > 0);
			
				if(sDbq == "")
				{
					if(p_sDriver == "")
						return bReturn;
					if(p_sServer == "")
						return bReturn;
				}
				p_objDS=objDataSources;
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.SetupData.SetupDataError", m_iClientId), p_objException);
			}
			finally
			{
				objDataSources = null;
			}
			return bReturn;
		}
		
		//-- Not Yet Tested -- RemoveTransparencyChars() throwing some error.
		/// <summary>
		/// Used to update the license information.
		/// </summary>
		/// <param name="p_sCode">Code provided to update the information.</param>
		/// <param name="p_iDSNID">DSN ID.</param>
		/// <param name="p_sMessage">Message representing whether the information has
		/// been updated/not.
		/// </param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool UpdateLicenseInfo(string p_sCode,int p_iDSNID,ref string p_sMessage)
		{
			bool bReturn = false;
			string sDate = "", sDSN = "";
			int i = 0, iNumber = 0;
			//gagnihotri MITS 11995 Changes made for Audit table
			//DataSources objDataSources = new DataSources(p_iDSNID,false);
            DataSources objDataSources = new DataSources(p_iDSNID, false, m_sUserName, m_iClientId);
			
			try
			{
				if(p_sCode == "MyndMastSecCode")
				{
					iNumber = 2;
					sDate = DateTime.Now.ToString();
				}
				else
				{
					p_sCode = p_sCode.Trim();
					p_sCode = RMCryptography.DecryptString(p_sCode);
					i = p_sCode.IndexOf(':');
					if(i == -1)
					{
                        p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
						return bReturn;
					}
					
					sDate = p_sCode.Substring(0,i);
					//p_sCode = p_sCode.Substring(i+1,p_sCode.Length);
					p_sCode = p_sCode.Substring(i+1);

					i = p_sCode.IndexOf(':');
					if(i == -1)
					{
                        p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
						return bReturn;
					}
					
					sDSN = p_sCode.Substring(0,i);
					//iNumber = Convert.ToInt32(p_sCode.Substring(i+1,p_sCode.Length));
					iNumber = Convert.ToInt32(p_sCode.Substring(i+1));
					
					if(sDate != Conversion.ToDbDate(DateTime.Now))
					{
                        p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
						return bReturn;
					}
					
					if(objDataSources.DataSourceName != sDSN)
					{
                        p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
						return bReturn;
					}
					
					if(iNumber < -1 || iNumber == 0 || iNumber > 10000)
					{
                        p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
						return bReturn;
					}
				}
				
					objDataSources.NumLicenses = iNumber;
					objDataSources.LicUpdDate = Conversion.ToDate(sDate);//Convert.ToDateTime(sDate);
                    //deb:MITS 25003
                    string strRegExpr = "^(?<CONN_STR_VALUE>[^;].+)UID=(?<UID_VALUE>[^;]+);PWD=(?<PWD_VALUE>[^;]+)[;]*$";
                    const string CONN_STR = "CONN_STR_VALUE";
                    Match regExMatch = Regex.Match(objDataSources.ConnectionString, strRegExpr, RegexOptions.IgnoreCase);
                    //Get the matching value for the specified CONN_STR
                    objDataSources.ConnectionString = regExMatch.Groups[CONN_STR].Value;
                    //deb:MITS 25003
					objDataSources.Save();
				
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.UpdateLicenseInfo.UpdateLicenseInfoError", m_iClientId), p_objException);
			}
			finally
			{
					objDataSources = null;
			}
			return bReturn;
		}

		//-- Not Yet Tested -- RemoveTransparencyChars() throwing some error.
		/// <summary>
		/// Used to validate the license information.
		/// </summary>
		/// <param name="p_sLicenseCode">License Code.</param>
		/// <param name="p_sDSName">DS Name.</param>
		/// <param name="p_iNumLicenses">Number of licenses.</param>
		/// <param name="p_sLicenseUpdateDate">License update date.</param>
		/// <param name="p_sMessage">Message showing whether the code entered is correct
		/// or not.
		/// </param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool ValidateLicenseInfo(string p_sLicenseCode,string p_sDSName,ref int p_iNumLicenses,ref string p_sLicenseUpdateDate,ref string p_sMessage)
		{
			bool bReturn = false;
			string sDate = "", sDSN = "";
			int i = 0, iNumber = 0;
			
			try
			{
				p_sLicenseCode = p_sLicenseCode.Trim();
				if(p_sLicenseCode == "MyndMastSecCode" || p_sLicenseCode == "CSCMastSecCode")
				{
					iNumber = 2;
					sDate = Conversion.ToDbDate(DateTime.Now) ;
				}
				else
				{
					try
					{
						p_sLicenseCode = RMCryptography.DecryptString(p_sLicenseCode);
					}
					catch(Exception p_objException)
					{
						if(!(p_objException is  DTGCryptFormatException))
						{
							// should throw but eat away as of now....
							//silently move on.. exception will be taken care  by the code below...
						}
						
					}
					i = p_sLicenseCode.IndexOf(':');
					if(i == -1)
					{
                        p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
						return bReturn;
					}

					sDate = p_sLicenseCode.Substring( 0,i );
					p_sLicenseCode = p_sLicenseCode.Substring(i+1);

					i = p_sLicenseCode.IndexOf(':');
					if(i == 0)
					{
                        p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
						return bReturn;
					}


					sDSN = p_sLicenseCode.Substring( 0,i );
					iNumber = Convert.ToInt32(p_sLicenseCode.Substring(i+1));
					
					if(sDate != Conversion.ToDbDate(DateTime.Now))
					{
                        p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
						return bReturn;
					}
					
					if(p_sDSName != sDSN)
					{
                        p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
						return bReturn;
					}
					
					if(iNumber < -1 || iNumber == 0 || iNumber > 10000)
					{
                        p_sMessage = Globalization.GetString("RMWinSecurity.ClearLicenseHistory.ClearLicenseHistoryFailure", m_iClientId);
						return bReturn;
					}
				}
				
			//	if(iNumber > 0)
			//	{
					p_iNumLicenses = iNumber;
					p_sLicenseUpdateDate = sDate;
			//	}

				bReturn = true;
			}
			finally
			{
			}
			return bReturn;
		}

		
		/// <summary>
		/// Split a string based on some string pattern.
		/// </summary>
		/// <param name="p_sString">String to be splitted.</param>
		/// <param name="p_sPattern">Pattern with in the string.</param>
		/// <param name="p_arrlstSplit">Array containing the splitted sub strings.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
        public static bool SplitForStringPattern(string p_sString, string p_sPattern, ref ArrayList p_arrlstSplit, int p_iClientId)
		{
			bool bReturn = false;

			int iIndexOf = 0;

			try
			{
				iIndexOf = p_sString.IndexOf(p_sPattern);

				if(iIndexOf == -1)
				{
					p_arrlstSplit.Add(p_sString);
					bReturn = true;
					return bReturn;
				}
				p_arrlstSplit.Add(p_sString.Substring(0,iIndexOf));

				string sAgain = p_sString.Substring(iIndexOf+p_sPattern.Length,p_sString.Length-(iIndexOf+p_sPattern.Length));
				SplitForStringPattern(sAgain,p_sPattern,ref p_arrlstSplit, p_iClientId);
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.SplitForStringPattern.SplitForStringPatternError", p_iClientId), p_objException);
			}

			finally
			{
			}
			return bReturn;
		}
	}
}
