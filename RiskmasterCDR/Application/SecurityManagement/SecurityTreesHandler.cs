﻿using System;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.Common;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.Security;
using System.Text;
using System.Data;

namespace Riskmaster.Application.SecurityManagement
{
	/// <summary>
	/// This class handles security trees implementation.
	/// </summary>
	public class SecurityLeftTreeHandler
	{
        /// <summary>
        /// Private Variable for Client Id
        /// </summary>
        private int m_iClientId = 0;
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		public SecurityLeftTreeHandler(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}
		/// <summary>
		/// This function gets the xml structure for Security left tree.
		/// </summary>
		/// <param name="p_objDocIn">Contains parameters for tree functionality.</param>
		/// <param name="p_objXmlOut">Contains the filled xml document for security left tree.</param>
		/// <returns></returns>
		public bool GetXmlStructureForLeftTree(ref XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut)
		{
            //srajindersin MITS 30263 dt 11/27/2012
			Users objUsers;
			//ArrayList arrlstUsers;
            DataSet dsUser;
			//ArrayList arrlstDatasources;
            DataSet dsDatasources;
			//ArrayList arrlstUserLogins;
            DataSet dsUserLogins;
            //END srajindersin MITS 30263 dt 11/27/2012

			DataSources objDataSources;
			UserLogins objUserLogins;
			XmlDocument objTreeXmlDoc=new XmlDocument();
			XmlNode objTargetNode;
			XmlNodeList objIdList;
			double iTreeIdGenerater=0;
			string sComanyName = string.Empty;
			
			System.Text.StringBuilder sbFinalXml;
			try
			{
				 p_objDocIn.SelectSingleNode("//Errors/Warning").InnerText="";
				  p_objDocIn.SelectSingleNode("//hdIsPwdPolicyEnabled").InnerText = PublicFunctions.IsPwdPolEnabled().ToString() ;
                //rsolanki2:Domain authentication updates
                  p_objDocIn.SelectSingleNode("//hdIsDomainAuthenticationEnabled").InnerText=Convert.ToString(Security.bIsDomainAuthenticationEnabled());

				if(p_objDocIn.SelectSingleNode("//IsPostBack")!=null)
				{
					if(p_objDocIn.SelectSingleNode("//TreePostedBy").InnerText.Trim()=="Delete")
					{
					  	PostbackHandler.DeleteDBEntities(ref p_objDocIn, m_iClientId);
					}
				}
                objTreeXmlDoc.Load(RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"sms\XmlForLeftTree.xml"));
                objUsers = new Users(m_iClientId);
                //srajindersin MITS 30263 dt 11/27/2012
				//arrlstUsers=new ArrayList();
                dsUser = objUsers.Load();

                if (dsUser.Tables.Count > 0)
                {
                    int userId;
                    string firstName;
                    string lastName;
                    string compantName;
                    for (int i = 0; i < dsUser.Tables[0].Rows.Count; i++)
                    //foreach(Users objUser in arrlstUsers)
                    {
                        userId = Riskmaster.Common.Conversion.ConvertObjToInt(dsUser.Tables[0].Rows[i]["USER_ID"], m_iClientId);
                        firstName = Riskmaster.Common.Conversion.ConvertObjToStr(dsUser.Tables[0].Rows[i]["FIRST_NAME"]);
                        lastName = Riskmaster.Common.Conversion.ConvertObjToStr(dsUser.Tables[0].Rows[i]["LAST_NAME"]);
                        compantName = Riskmaster.Common.Conversion.ConvertObjToStr(dsUser.Tables[0].Rows[i]["COMPANY_NAME"]);
                        //Nitesh MITS:6184 starts 12july2006
                        bool bCompany = false;
                        if (((string)compantName).Trim() != "")
                            bCompany = true;

                        if (bCompany)
                        {
                            //Parijat :changes required inorder to incorporate spaces at the end of the company name--Extention to MITS 8887 
                            sComanyName = compantName.Trim();
                            sComanyName = sComanyName.ToUpper();
                            sComanyName = sComanyName.Replace("'", "&quot;");
                            if (objTreeXmlDoc.SelectSingleNode("//entry[@title='COMPANY:" + sComanyName + "']") == null)
                            {
                                XmlElement objUserCompanyElement = objTreeXmlDoc.CreateElement("entry");
                                PublicFunctions.CreateElement(ref objUserCompanyElement, compantName, "COMPANY:" + sComanyName,
                                    SecurityManagement_us_en.NoData,
                                    SecurityManagement_us_en.SecUserCompany,
                                    "link", "Root Node for Company", "COMPANY:" + sComanyName);
                                objTreeXmlDoc.SelectSingleNode("//entry[@title='Users']").AppendChild(objUserCompanyElement);
                            }
                        }
                        XmlElement objUserElement = objTreeXmlDoc.CreateElement("entry");
                        PublicFunctions.CreateElement(ref objUserElement, firstName + " " + lastName, "User",
                            SecurityManagement_us_en.User + "?userid=" + userId.ToString(),
                            SecurityManagement_us_en.SecUser,
                            "link", "User", userId.ToString());
                        if (bCompany)
                            objTreeXmlDoc.SelectSingleNode("//entry[@title='COMPANY:" + sComanyName + "']").AppendChild(objUserElement);
                        else
                            objTreeXmlDoc.SelectSingleNode("//entry[@title='Users']").AppendChild(objUserElement);
                        //Nitesh MITS:6184 Ends 12july2006
                    }
                }

				//arrlstDatasources=new ArrayList();
				objDataSources=new DataSources(m_iClientId);
				//objDataSources.Load(ref arrlstDatasources);
                dsDatasources = objDataSources.Load();

				//arrlstUserLogins=new ArrayList();
				objUserLogins=new UserLogins(false, m_iClientId);
				//objUserLogins.Load(ref arrlstUserLogins);
                dsUserLogins = objUserLogins.Load();


                if (dsDatasources.Tables.Count > 0)
                {
                    int dataSourceID;
                    string dataSourceName;
                    for (int i = 0; i < dsDatasources.Tables[0].Rows.Count; i++)
                    //foreach(DataSources objDataSource in arrlstDatasources)
                    {
                        dataSourceID = Riskmaster.Common.Conversion.ConvertObjToInt(dsDatasources.Tables[0].Rows[i]["DSNID"], m_iClientId);
                        dataSourceName = Riskmaster.Common.Conversion.ConvertObjToStr(dsDatasources.Tables[0].Rows[i]["DSN"]);

                        XmlElement objDataSourceElement = objTreeXmlDoc.CreateElement("entry");

                        PublicFunctions.CreateElement(ref objDataSourceElement, dataSourceName, dataSourceName,
                            SecurityManagement_us_en.Datasource + "?dsnid=" + dataSourceID.ToString(),
                            SecurityManagement_us_en.DatasourceNode,
                            "link", "Datasource", dataSourceID.ToString());


                        XmlElement objPermToLoginElement = objTreeXmlDoc.CreateElement("entry");
                        PublicFunctions.CreateElement(ref objPermToLoginElement, "Permission To Login", "Root Node for PermToLogin",
                             SecurityManagement_us_en.NoData,
                             SecurityManagement_us_en.SecUsers,
                             "link", "Root Node for PermToLogin", dataSourceID.ToString());
                        //create in loop all permitted users...

                        XmlElement objUserPermElement = null;
                        if (dsUserLogins.Tables.Count > 0)
                        {
                            int databaseId;
                            int userId;
                            for (int j = 0; j < dsUserLogins.Tables[0].Rows.Count; j++)
                            //foreach(UserLogins objUserLogin in arrlstUserLogins)
                            {
                                databaseId = Riskmaster.Common.Conversion.ConvertObjToInt(dsUserLogins.Tables[0].Rows[j]["DSNID"], m_iClientId);
                                userId = Riskmaster.Common.Conversion.ConvertObjToInt(dsUserLogins.Tables[0].Rows[j]["USER_ID"], m_iClientId);
                                //if( objUserLogin.DatabaseId==objDataSource.DataSourceId)
                                if (databaseId == dataSourceID)
                                {
                                    Users objPermUser = new Users(m_iClientId);
                                    objPermUser.Load(userId);
                                    objUserPermElement = objTreeXmlDoc.CreateElement("entry");
                                    PublicFunctions.CreateElement(ref objUserPermElement, objPermUser.FirstName + " " + objPermUser.LastName, "Root Node for PermToLogin",
                                        SecurityManagement_us_en.UserPerm + "?dsnid=" + databaseId.ToString() + "&" + "userid=" + objPermUser.UserId.ToString(),
                                        SecurityManagement_us_en.SecUser,
                                        "link", "PermUser", dataSourceID.ToString() + "," + userId.ToString());
                                    objPermToLoginElement.AppendChild(objUserPermElement);
                                }

                            }
                        }
                        objDataSourceElement.AppendChild(objPermToLoginElement);
                        //END srajindersin MITS 30263 dt 11/27/2012

                        XmlElement objModuleSecGroupsElement = objTreeXmlDoc.CreateElement("entry");
                        PublicFunctions.CreateElement(ref objModuleSecGroupsElement, "Module Security Groups", "Module Security Groups",
                            SecurityManagement_us_en.NoData + "?idDb=" + dataSourceID.ToString(),
                            SecurityManagement_us_en.ModuleSecurityGroup,
                            "link", "Root Node Module Security Groups", dataSourceID.ToString());
                        //create in loop all module groups users...
                        if (p_objDocIn.SelectSingleNode("//IsPostBack") != null)
                        {
                            switch (p_objDocIn.SelectSingleNode("//TreePostedBy").InnerText.Trim())
                            {
                                case "LoadModuleGroups":
                                    PostbackHandler.LoadModuleGrpsHandlerForLeftTree(ref p_objDocIn, ref objTreeXmlDoc, dataSourceID, ref objModuleSecGroupsElement, ref objDataSourceElement,m_iClientId);
                                    break;

                                case "UpdateUserPermEntity":
                                    PostbackHandler.UpdateUserPermHandler(ref p_objDocIn, ref objTreeXmlDoc, dataSourceID, ref objModuleSecGroupsElement, ref objDataSourceElement,m_iClientId);
                                    break;

                                case "UpdateDatasourceEntity":
                                    PostbackHandler.AddNewUserHandler(ref p_objDocIn, ref objTreeXmlDoc,m_iClientId);
                                    break;

                                case "AddNewModuleGroupEntity":

                                    PostbackHandler.AddNewModuleGroupHandler(ref p_objDocIn, ref objTreeXmlDoc, dataSourceID, ref objModuleSecGroupsElement, ref objDataSourceElement,m_iClientId);
                                    break;

                                case "UpdateModuleGroupEntity":

                                    PostbackHandler.AddNewUserHandler(ref p_objDocIn, ref objTreeXmlDoc, m_iClientId);
                                    break;

                                case "RenameModuleGroupEntity":
                                    PostbackHandler.RenameModuleGroupHandler(ref p_objDocIn, ref objTreeXmlDoc, dataSourceID, ref objModuleSecGroupsElement, ref objDataSourceElement,m_iClientId);
                                    break;
                                case "Delete":
                                    string sEntityType;
                                    try
                                    {
                                        sEntityType = p_objDocIn.SelectSingleNode("//SelectedEntityFromLeftTree").InnerText;
                                        if (sEntityType == "Module Group User" || sEntityType == "Module Security Group" || sEntityType == "GrpModuleAccessPermission")
                                        {
                                            string sIdDb;
                                            string[] arrIds;
                                            try
                                            {

                                                sIdDb = p_objDocIn.SelectSingleNode("//Delete/EntityIdForDeletion").InnerText;
                                                arrIds = PublicFunctions.BreakDbId(sIdDb);
                                                if (dataSourceID == Conversion.ConvertObjToInt(arrIds[0], m_iClientId))
                                                {
                                                    PostbackHandler.AppendModuleGroups(ref objModuleSecGroupsElement
                                                        , Conversion.ConvertObjToInt(arrIds[0], m_iClientId)
                                                        , ref objTreeXmlDoc, false, m_iClientId);
                                                }
                                            }
                                            catch (Exception p_objException)
                                            {
                                                throw p_objException;
                                            }
                                            finally
                                            {
                                                sIdDb = null;
                                                arrIds = null;
                                            }
                                        }
                                    }
                                    catch (Exception p_objException)
                                    {
                                        throw p_objException;
                                    }
                                    finally
                                    {
                                        sEntityType = null;
                                    }
                                    break;
                                default:
                                    break;

                            }

                        }
                        objDataSourceElement.AppendChild(objModuleSecGroupsElement);
                        objTreeXmlDoc.SelectSingleNode("//entry[@title='Data Sources']").AppendChild(objDataSourceElement);
                    }
                }

				if(p_objDocIn.SelectSingleNode("//IsPostBack")!=null)
				{
					switch (p_objDocIn.SelectSingleNode("//TreePostedBy").InnerText.Trim())
					{
						case "AddNewUser" :
                            PostbackHandler.AddNewUserHandler(ref p_objDocIn, ref objTreeXmlDoc, m_iClientId);
							break;
						case "UpdateUserEntity" :

                            PostbackHandler.UpdateUserHandler(ref p_objDocIn, ref objTreeXmlDoc, m_iClientId);
							break;
						case "AddNewDatasource" :

                            PostbackHandler.AddNewDatasourceHandler(ref p_objDocIn, ref objTreeXmlDoc, m_iClientId);
							break;
						case "Delete" :
							
							PostbackHandler.DeleteHandler(ref p_objDocIn,ref objTreeXmlDoc);
							break;
						default:
					    	break;

					}
				}
				objTargetNode = p_objDocIn.SelectSingleNode("//form");
				sbFinalXml=new System.Text.StringBuilder();
				sbFinalXml.Append(objTargetNode.InnerXml);
				sbFinalXml.Append(objTreeXmlDoc.SelectSingleNode("//toc").OuterXml);
				objTargetNode.InnerXml=sbFinalXml.ToString();
				objIdList=p_objDocIn.SelectNodes("//entry[@id]");
				foreach(XmlNode objNode in objIdList)
				{
					objNode.Attributes["id"].Value=iTreeIdGenerater.ToString();
					iTreeIdGenerater++;
				}
				if(p_objDocIn.SelectSingleNode("//IsPostBack")!=null)
				{
					if(p_objDocIn.SelectSingleNode("//entry[@highlight='true']")!=null)
					{
						
						p_objDocIn.SelectSingleNode("//PostBackResults/NodeId").InnerText=p_objDocIn.SelectSingleNode("//entry[@highlight='true']").Attributes["id"].Value;

					}
				}
				
				p_objXmlOut=p_objDocIn;
			
			}
			catch(Exception p_objException)
			{
				
				Exception objException=null;
				PublicFunctions.WorkerForException(p_objException,out objException);
				p_objDocIn.SelectSingleNode("//Errors/Warning").InnerText=objException.Message;
				p_objXmlOut=p_objDocIn;
				return true;
			}
			finally
			{
				objUsers=null;
                //srajindersin MITS 30263 dt 11/27/2012
				//arrlstUsers=null;
				//arrlstDatasources=null;
				//arrlstUserLogins=null;
				objDataSources=null;
				objUserLogins=null;
				objTargetNode=null;
				objIdList=null;
				objTreeXmlDoc=null;
			}
			return true;
		}
	}

	/// <summary>
	/// This class handles the permissions related functonality. 
	/// </summary>
	public class SecurityPermissionTreeHandler
	{   
		ArrayList m_arrlstFunctionList;//Will hold all the permissions.
		ArrayList m_arrlstDynamicList;//This will hold permissions for specific to a  Dsn. 
		bool m_bConvetedRawPermsToDsnSpecPerms;//Flags whether cached permissions are converted to Dsn specific permissions or not..
		XmlDocument m_objPermTree;//Final pemission xml.
		ArrayList m_arlstPermissiondForDsn;//Dsn specific persmissions.
		XmlDocument p_objInstanceDoc;//Will hold the instance document.
		private static XmlDocument m_objCache;//Raw permissions xml as loaded from function_list table.
		System.Text.StringBuilder m_sbAllowedModules=new System.Text.StringBuilder();//Will track all the allowed modules.
        private static bool m_blnLoadPermsFromCache = false, m_blnCacheTwoLevels = false;
        /// <summary>
        /// ClientId for multi-tenancy, cloud
        /// </summary>
        private int m_iClientId = 0;
		/// <summary>
		/// This constructor will load the cached permissions into memory for further request i.e implementing second level of permission caching. 
		/// </summary>
		static SecurityPermissionTreeHandler()
		{
            m_blnCacheTwoLevels = Convert.ToBoolean(RMConfigurationManager.GetSingleTagSectionSettings("SecurityMgtSystemLoadSettings", string.Empty, 0)["PerformTwoLevelOfCaching"].ToString());
            m_blnLoadPermsFromCache = Convert.ToBoolean(RMConfigurationManager.GetSingleTagSectionSettings("SecurityMgtSystemLoadSettings", string.Empty, 0)["LoadPermissionsFromCache"].ToString());

			if(m_blnLoadPermsFromCache)
			{
				if(m_blnCacheTwoLevels)
				{
                    m_objCache = new XmlDocument();
                    m_objCache.Load(RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"sms\ModuleAccessPermissions.xml"));
				}
			}
		}
		
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		public SecurityPermissionTreeHandler(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}
		/// <summary>
		/// This function returns the XMl for security permissions.
		/// </summary>
		/// <param name="p_objDocIn">Inpuy instance doc.</param>
		/// <param name="p_objXmlOut">Output doc containing security permissions.</param>
		/// <returns>true or false.</returns>
		public bool GetXmlStructureForPermissonsTree(ref XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut)
		{
			int iDsnid=0;
			int iGrpId=0;
            StringBuilder sbFinalXml;
			XmlNodeList objEntryList;
			int[] arrFuncIds;
			int[] arrParentIds;
			int iTemp=0;
            string sUserName = "";              //gagnihotri MITS 11995 Changes made for Audit table
            XmlNode objXMLNode = null;          //gagnihotri MITS 11995 Changes made for Audit table
			try
			{
				p_objDocIn.SelectSingleNode("//Errors/Warning").InnerText="";
                iDsnid = Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//ToBePosted/DbId/DsnId").InnerText, m_iClientId);
                iGrpId = Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//ToBePosted/DbId/GrpId").InnerText, m_iClientId);
				p_objInstanceDoc=p_objDocIn;
				if(p_objDocIn.SelectSingleNode("//PostedData").InnerXml!="")//Ispostback
				{
                    iDsnid = Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//DbId/DsnId").InnerText, m_iClientId);
                    iGrpId = Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//DbId/GrpId").InnerText, m_iClientId);
					string sConnstr="";
					sConnstr=Riskmaster.Application.SecurityManagement.PublicFunctions.GetConnStr(iDsnid,m_iClientId);
					string sAllowedModules="";
					if(p_objDocIn.SelectSingleNode("///PostedByRevokeAndGrantPermissions").InnerText=="true")
					{
						
						if(m_blnLoadPermsFromCache)
						{
							if(m_blnLoadPermsFromCache && m_blnCacheTwoLevels)
							{
								m_objPermTree=(XmlDocument)m_objCache.Clone();
							}
							else
							{
                                m_objPermTree = m_objCache;								
							}
							ConvertRawPermissionsToDsnSpecificPermissions(ref m_objPermTree,sConnstr);  
							
							objEntryList=m_objPermTree.SelectNodes("//entry");
							arrFuncIds=new int[objEntryList.Count];
							arrParentIds=new int[objEntryList.Count];
							foreach(XmlNode objNode in objEntryList)
							{
                               arrFuncIds[iTemp]= Conversion.ConvertStrToInteger(objNode.Attributes["sid"].Value);
							   arrParentIds[iTemp]=Conversion.ConvertStrToInteger(objNode.Attributes["sparentid"].Value);
								++iTemp;
							}
                            m_bConvetedRawPermsToDsnSpecPerms=true;
						}
						else
						{
                            RMModules objRmModule = new Riskmaster.Application.SecurityManagement.RMModules(sConnstr, m_iClientId);
							m_arrlstFunctionList=new ArrayList();
							objRmModule.Load(ref m_arrlstFunctionList);
							arrFuncIds=new int[m_arrlstFunctionList.Count];
							arrParentIds=new int[m_arrlstFunctionList.Count];
							
							foreach(RMModules objRMModule in m_arrlstFunctionList)
							{
								arrFuncIds[iTemp]=objRMModule.ID;
								arrParentIds[iTemp]=objRMModule.ParentID;
								++iTemp;
							}
						}
						
						int iRevokeGrantPermissionsId;
                        iRevokeGrantPermissionsId = Conversion.ConvertObjToInt((p_objDocIn.SelectSingleNode("//RevokeGrantPermissionsId").InnerText.Split('|'))[0], m_iClientId);
						
						if(p_objDocIn.SelectSingleNode("//PermissionAction").InnerText=="1")
						{
							
							string[] arrPreviousAllowed=p_objDocIn.SelectSingleNode("//AllowedModules").InnerText.Split(',');
							
							p_objDocIn.SelectSingleNode("//ToBePosted/DbId/DsnId").InnerText=iDsnid.ToString();
							p_objDocIn.SelectSingleNode("//ToBePosted/DbId/GrpId").InnerText=iGrpId.ToString();
							ModuleGroups objModuleGrp=new ModuleGroups(sConnstr, m_iClientId);
							
							System.Text.StringBuilder sbAllChild=new System.Text.StringBuilder();
							if(m_blnLoadPermsFromCache)
								GetAllSubNodesForCache(iRevokeGrantPermissionsId,ref sbAllChild);
							else
							    GetAllSubNodes(iRevokeGrantPermissionsId,ref sbAllChild);
							string[] arrCurrentAllowed;
							if(sbAllChild.ToString()=="")
							{
								arrCurrentAllowed=new string[0];
							}
							else
							{
								arrCurrentAllowed= sbAllChild.ToString().Split(',');
							}
                            sbAllChild.Append(',');
							sbAllChild.Insert(0,",") ;
							for(int iTmp=0;iTmp<arrPreviousAllowed.Length;iTmp++)
							{
								if(sbAllChild.ToString().IndexOf(","+arrPreviousAllowed[iTmp]+",")>=0)
								{
								  sbAllChild.Replace(","+arrPreviousAllowed[iTmp]+",",",");
								}
							}
							if(sbAllChild.ToString().StartsWith(","))
							{
                                sbAllChild.Remove(0,1);
							}
							if(sbAllChild.ToString().EndsWith(","))
							{
								sbAllChild.Remove(sbAllChild.Length-1,1) ;
							}
							if(sbAllChild.ToString().IndexOf("|")>=0)
							{
								sAllowedModules = sbAllChild.ToString()+","+p_objDocIn.SelectSingleNode("//AllowedModules").InnerText;
							}
							else
							{
								sAllowedModules =  p_objDocIn.SelectSingleNode("//AllowedModules").InnerText;
							}
						}
						else
						{
							int iSelIndex=0;
							string sSelNode;
							string[] arrTemp;
							arrTemp= p_objDocIn.SelectSingleNode("//AllowedModules").InnerText.Split(',');
							for(int iCnt=0;iCnt<arrFuncIds.Length;iCnt++)
							{
								if(iRevokeGrantPermissionsId==arrFuncIds[iCnt])
								{
									iSelIndex=iCnt;
									break;
								}
							}
							sSelNode=arrFuncIds[iSelIndex]+"|"+arrParentIds[iSelIndex];
							sAllowedModules="";
							for(int iAllowed=0;iAllowed<arrTemp.Length;iAllowed++)
							{
								if(arrTemp[iAllowed].ToString()!=sSelNode)
								{
									if(sAllowedModules=="")
									{
										sAllowedModules=arrTemp[iAllowed];
									}
									else
									{
										sAllowedModules = sAllowedModules + "," + arrTemp[iAllowed];
									}
								}

							}

						}
						

					}
					else
					{
					}
					sConnstr=Riskmaster.Application.SecurityManagement.PublicFunctions.GetConnStr(iDsnid,m_iClientId);
					p_objDocIn.SelectSingleNode("//ToBePosted/DbId/DsnId").InnerText=iDsnid.ToString();
					p_objDocIn.SelectSingleNode("//ToBePosted/DbId/GrpId").InnerText=iGrpId.ToString();
                    //gagnihotri MITS 11995 Changes made for Audit table Start
                    objXMLNode = p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                    if (objXMLNode != null)
                        sUserName = objXMLNode.InnerText;
					ModuleGroups objModuleGroup;
					//objModuleGroup=new ModuleGroups(sConnstr);
                    objModuleGroup = new ModuleGroups(sConnstr,sUserName, m_iClientId);
                    //gagnihotri MITS 11995 Changes made for Audit table End
					if(p_objDocIn.SelectSingleNode("//PostedByRevokeAndGrantPermissions").InnerText=="true")
					{
						objModuleGroup.GetAllowedModules=sAllowedModules;
					}
					else
					{
						objModuleGroup.GetAllowedModules=p_objDocIn.SelectSingleNode("//AllowedModules").InnerText;
					}
					
					objModuleGroup.UpdateModuleAccessPermissions(iGrpId);
				
				}
				if(! m_blnLoadPermsFromCache)
				{
					InitPermissionTree(iDsnid,iGrpId,ref p_objDocIn,ref m_sbAllowedModules);
				}
				else
				{
					GetPermissionsTreeFromCache(iDsnid,iGrpId,ref p_objDocIn,ref m_sbAllowedModules);
				}
				sbFinalXml=new StringBuilder();
				sbFinalXml.Append(p_objDocIn.SelectSingleNode("//Document").InnerXml);
				if(m_objPermTree!=null)
					sbFinalXml.Append(m_objPermTree.OuterXml);
				p_objDocIn.SelectSingleNode("//Document").InnerXml=sbFinalXml.ToString();
				p_objXmlOut=p_objDocIn;
			}
			catch(Exception p_objException)
			{
				Exception objException=null;
				try
				{
					
					PublicFunctions.WorkerForException(p_objException,out objException);
					p_objDocIn.SelectSingleNode("//Errors/Warning").InnerText=objException.Message;
					p_objXmlOut=p_objDocIn;
				}
				catch(Exception p_objErr)
				{
					throw p_objErr;
				}
				finally
				{
                    objException=null;
				}
				return true;
			}
			finally
			{
               sbFinalXml=null;
			   m_arrlstFunctionList=null;
			   m_objPermTree=null;
			   m_arlstPermissiondForDsn=null;
			   p_objInstanceDoc=null;
			   m_sbAllowedModules=null;
			   arrFuncIds=null;
			   arrParentIds=null;
               objXMLNode = null;
			}
			return true;
		}
		/// <summary>
		/// This function loads the permissions from function_list table and creates xml out of it.
		/// </summary>
		/// <param name="p_iDsnId">Dsn id for which permissions to be loaded.</param>
		/// <param name="p_iModuleGrpId">Grp id for loading permissions</param>
		/// <param name="p_objInstance">Instance doc</param>
		/// <param name="p_sbAllowedModules">Allowed module data in string format.</param>
		private void InitPermissionTree(int p_iDsnId,int p_iModuleGrpId,ref XmlDocument p_objInstance,ref System.Text.StringBuilder p_sbAllowedModules)
		{   
			string sConnStr;
			Riskmaster.Application.SecurityManagement.RMModules objRmModule;
			Riskmaster.Application.SecurityManagement.ModuleGroups objModuleGrp;
			ArrayList arrlstRootNodes;
			XmlElement objRootElement;
			DataSources objDsn;
			long lId=0;
			XmlNodeList objIdList;
            try
            {
                m_objPermTree = new XmlDocument();
                m_objPermTree.Load(RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"sms\XmlForPermissionTree.xml"));
                sConnStr = Riskmaster.Application.SecurityManagement.PublicFunctions.GetConnStr(p_iDsnId, out objDsn, m_iClientId);
                if (objDsn.Status == false)
                {
                    p_objInstance.SelectSingleNode("//PostBackResults/ModuleAccess").InnerText = "false";
                    return;
                }
                if (m_arrlstFunctionList == null)
                {
                    m_arrlstFunctionList = new ArrayList();
                    objRmModule = new Riskmaster.Application.SecurityManagement.RMModules(sConnStr, m_iClientId);
                    objRmModule.Load(ref m_arrlstFunctionList);
                }
                objModuleGrp = new ModuleGroups(sConnStr, m_iClientId);
                m_arlstPermissiondForDsn = objModuleGrp.GetDsnPermissions(p_iModuleGrpId);
                arrlstRootNodes = this.GetAllChildNodes(0);
                foreach (RMModules objRMModule in arrlstRootNodes)
                {
                    objRootElement = GetEmptyElement();
                    objRootElement.SetAttribute("sid", objRMModule.ID.ToString());
                    objRootElement.SetAttribute("funcname", objRMModule.ModuleName);
                    objRootElement.SetAttribute("sparentid", objRMModule.ParentID.ToString());
                    objRootElement.SetAttribute("sentrytype", objRMModule.EntryType.ToString());
                    PublicFunctions.CreateElement(ref objRootElement, objRMModule.ModuleName, objRMModule.ModuleName, "", "", "static");
                    if (m_arlstPermissiondForDsn.Contains(objRMModule.ID))
                    {
                        objRootElement.SetAttribute("ispermitted", "true");
                        if (p_sbAllowedModules.ToString() == "")
                        {
                            p_sbAllowedModules.Append(objRMModule.ID.ToString() + "|" + objRMModule.ParentID.ToString());
                        }
                        else
                        {
                            p_sbAllowedModules.Append("," + objRMModule.ID.ToString() + "|" + objRMModule.ParentID.ToString());
                        }
                    }
                    GetTreeForRootNodes(objRMModule.ID, ref objRootElement, ref p_sbAllowedModules);
                    ((XmlElement)m_objPermTree.SelectSingleNode("//toc")).AppendChild(objRootElement);
                }
                objIdList = m_objPermTree.SelectNodes("//entry");
                foreach (XmlNode objNode in objIdList)
                {
                    objNode.Attributes["id"].Value = lId.ToString();
                    lId++;
                }
                p_objInstance.SelectSingleNode("//PostBackResults/AllowedModules").InnerText = p_sbAllowedModules.ToString();

            }

            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                objRmModule = null;
                objModuleGrp = null;
                arrlstRootNodes = null;
                objRootElement = null;
                objDsn = null;
                objIdList = null;
            }
		}
		/// <summary>
		/// This function outputs xml tree for supplied node id.
		/// </summary>
		/// <param name="p_iParentNode">Id of the top level entity.</param>
		/// <param name="objNode">This will hold the xml tree.</param>
		/// <param name="p_sbAllowedModules">Will hold the allowed mmodules.</param>
		private void GetTreeForRootNodes(int p_iParentNode,ref XmlElement p_objNode,ref System.Text.StringBuilder p_sbAllowedModules )
		{
			ArrayList arrlstChilds=null;
			try
			{
				arrlstChilds=GetAllChildNodes(p_iParentNode);
				if(arrlstChilds.Count>0)
				{
					foreach(RMModules objRMModule in arrlstChilds)
					{ 
                       
						XmlElement objNodeNew=GetEmptyElement();
						p_objNode.AppendChild(objNodeNew);
						objNodeNew.SetAttribute("sid",objRMModule.ID.ToString());
						objNodeNew.SetAttribute("fname",objRMModule.ModuleName);
						objNodeNew.SetAttribute("sparentid",objRMModule.ParentID.ToString());
						objNodeNew.SetAttribute("sentrytype",objRMModule.EntryType.ToString());
						PublicFunctions.CreateElement(ref objNodeNew,objRMModule.ModuleName,objRMModule.ModuleName,"","","static");
						if(m_arlstPermissiondForDsn.Contains(objRMModule.ID))
						{
							if(p_sbAllowedModules.ToString()=="")
							{
								p_sbAllowedModules.Append(objRMModule.ID.ToString()+ "|" +objRMModule.ParentID.ToString());
							}
							else
							{
								p_sbAllowedModules.Append(","+objRMModule.ID.ToString()+ "|" +objRMModule.ParentID.ToString());
							}
							objNodeNew.SetAttribute("ispermitted","true");
						}
						GetTreeForRootNodes(objRMModule.ID,ref objNodeNew,ref p_sbAllowedModules);
					}
				}
				else
				{
					return;
				}
			}
			catch(Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("RMWinSecurity.CheckUsers.CheckUsersError", m_iClientId),p_objException);
			}
			finally
			{
				arrlstChilds=null;
			}
			
		}
		
		/// <summary>
		/// This function is equivalent of GetTreeForRootNodes() function; loads the data from cache.
		/// </summary>
		/// <param name="p_iParentNode">Id for which xml tree to be created.</param>
		/// <param name="objNode">This will contain the outpot xml tree.</param>
		private void GetTreeForRootNodesForCache(int p_iParentNode,ref XmlElement objNode )
		{
			ArrayList arrlstChilds=null;
			try
			{
				arrlstChilds=GetAllChildNodesCacheArrlst(p_iParentNode);
				if(arrlstChilds.Count>0)
				{
					foreach(RMModules objRMModule in arrlstChilds)
					{ 
                       
						XmlElement objNodeNew=GetEmptyElement();
						objNode.AppendChild(objNodeNew);
						objNodeNew.SetAttribute("sid",objRMModule.ID.ToString());
						objNodeNew.SetAttribute("fname",objRMModule.ModuleName);
						objNodeNew.SetAttribute("sparentid",objRMModule.ParentID.ToString());
						objNodeNew.SetAttribute("sentrytype",objRMModule.EntryType.ToString());
						PublicFunctions.CreateElement(ref objNodeNew,objRMModule.ModuleName,objRMModule.ModuleName,"","","static");
						GetTreeForRootNodesForCache(objRMModule.ID,ref objNodeNew);
					}
				}
				else
				{
					return;
				}
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.CheckUsers.CheckUsersError", m_iClientId), p_objException);
			}
			finally
			{
				arrlstChilds=null;
			}
			
		}
		/// <summary>
		/// This function retuns the child nodes of a node.
		/// </summary>
		/// <param name="p_sParentId">Id for which child nodes to be fetched.</param>
		/// <returns>Returns the child nodes in arraylist.</returns>
		private ArrayList GetAllChildNodes(int p_sParentId)
		{
			ArrayList arrlstChilds=new ArrayList();
			
			try
			{
				foreach(RMModules objRMModule in m_arrlstFunctionList)
				{
					if(objRMModule.ParentID==p_sParentId)
					{
						arrlstChilds.Add(objRMModule);
					}
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			return arrlstChilds;
			
		}
		/// <summary>
		/// This function is equivalent of GetAllChildNodes() function; loads the data from cache.
		/// </summary>
		/// <param name="p_sParentId">Id for which child nodes to be fetched.</param>
		/// <returns>Returns the child nodes in arraylist.</returns>
		private ArrayList GetAllChildNodesForCache(int p_sParentId)
		{
			ArrayList arrlstChilds=new ArrayList();
			XmlNodeList objFinalEntriesList;
			try
			{
				objFinalEntriesList=m_objPermTree.SelectNodes("//entry");
				foreach(XmlNode objNode in objFinalEntriesList)
				{
					if(objNode.Attributes["sparentid"].Value==p_sParentId.ToString())
					{
						arrlstChilds.Add(objNode);
					}
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				objFinalEntriesList=null;
			}
			return arrlstChilds;
			
		}
		/// <summary>
		/// This function returns the child nodes of a particular node(for cache.)
		/// </summary>
		/// <param name="p_sParentId">Id for which child nodes to be fetched.</param>
		/// <returns>Returns the child nodes in arraylist.</returns>
		private ArrayList GetAllChildNodesCacheArrlst(int p_sParentId)
		{
			ArrayList arrlstChilds=new ArrayList();
			
			try
			{
				foreach(RMModules objRMModule in m_arrlstDynamicList)
				{
					if(objRMModule.ParentID==p_sParentId)
					{
						arrlstChilds.Add(objRMModule);
					}
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			return arrlstChilds;
		}
		/// <summary>
		/// This function makes the permission xml from cached xml. 
		/// </summary>
		/// <param name="p_iDsnId">Dsn id for which permissions to be fetched.</param>
		/// <param name="p_iModuleGrpId">Module group id for fetching the permissions.</param>
		/// <param name="p_objInstance">Instance doc.</param>
		/// <param name="p_sbAllowedModules">Allowed modules for this Dsn.</param>
		private void GetPermissionsTreeFromCache(int p_iDsnId,int p_iModuleGrpId,ref XmlDocument p_objInstance,ref System.Text.StringBuilder p_sbAllowedModules)
		{
			string sConnStr;
			Riskmaster.Application.SecurityManagement.ModuleGroups objModuleGrp;
			DataSources objDsn;
			try
			{
				sConnStr=Riskmaster.Application.SecurityManagement.PublicFunctions.GetConnStr(p_iDsnId,out objDsn, m_iClientId);
				if(objDsn.Status==false)
				{
					p_objInstance.SelectSingleNode("//PostBackResults/ModuleAccess").InnerText="false";
					return;
				}
				if(!m_bConvetedRawPermsToDsnSpecPerms)
				{
					if(m_blnLoadPermsFromCache && m_blnCacheTwoLevels)
					{
						m_objPermTree=(XmlDocument)m_objCache.Clone();
					}
					else
					{
                        m_objPermTree = m_objCache;
					}
					ConvertRawPermissionsToDsnSpecificPermissions(ref m_objPermTree,sConnStr);           
				}
				objModuleGrp=new ModuleGroups(sConnStr, m_iClientId);
				m_arlstPermissiondForDsn=objModuleGrp.GetDsnPermissions(p_iModuleGrpId);
				XmlNodeList objEntryListPermissions= m_objPermTree.SelectNodes("//entry");
				foreach(XmlElement objNode in objEntryListPermissions)
				{
					if(m_arlstPermissiondForDsn.Contains(Conversion.ConvertStrToInteger(objNode.Attributes["sid"].Value)))
					{
						objNode.SetAttribute("ispermitted","true");
						if(p_sbAllowedModules.ToString()=="")
						{
							p_sbAllowedModules.Append(objNode.Attributes["sid"].Value+ "|" +objNode.Attributes["sparentid"].Value);
						}
						else
						{
							p_sbAllowedModules.Append(","+objNode.Attributes["sid"].Value+ "|" +objNode.Attributes["sparentid"].Value);
						}
					}
				}

				p_objInstance.SelectSingleNode("//PostBackResults/AllowedModules").InnerText=p_sbAllowedModules.ToString();
			}
			
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			    objModuleGrp=null;
				objDsn=null;
			}
			
		}
        /// <summary>
        /// This function creates a empty 'entry' element.
        /// </summary>
        /// <returns>Empty 'entry' element.</returns>
		private XmlElement GetEmptyElement()
		{
			return m_objPermTree.CreateElement("entry");
		}
		/// <summary>
		/// This function returns 'control' element 
		/// </summary>
		/// <param name="p_sSid">Id from function_list table.</param>
		/// <param name="p_sParentId">ParentId from function_list table.</param>
		/// <returns></returns>
		private XmlElement GetEmptyControlElement(string p_sSid,string p_sParentId)
		{
			XmlElement objElement;
			objElement=p_objInstanceDoc.CreateElement("control");
			objElement.SetAttribute("sid",p_sSid);
			objElement.SetAttribute("sparentid",p_sParentId);
			return objElement;
		}
		/// <summary>
		/// This function returns the sub nodes for a particular node.
		/// </summary>
		/// <param name="p_iParentId">Id, for which sub nodes to be fetched.</param>
		/// <param name="sbAllSubChilds">Will hold all the sub nodes as the return value of this function.</param>
		private  void GetAllSubNodes(int p_iParentId,ref System.Text.StringBuilder sbAllSubChilds)
		{
			ArrayList arrlstChildNodes;
			arrlstChildNodes = GetAllChildNodes(p_iParentId);
			if(arrlstChildNodes.Count>0)
			{
				foreach(RMModules objRMModule in arrlstChildNodes)
				{
					if(sbAllSubChilds.ToString()=="")
					{
						sbAllSubChilds.Append(objRMModule.ID+"|"+objRMModule.ParentID);
					}
					else
					{
						sbAllSubChilds.Append(","+objRMModule.ID+"|"+objRMModule.ParentID);
					}
                     
					GetAllSubNodes(objRMModule.ID,ref sbAllSubChilds);
				}
			}
			else
			{
				return;
			}
         
		}
		/// <summary>
		/// This function is equivalent of GetAllSubNodes() function; implemented for caching functionality.
		/// </summary>
		/// <param name="p_iParentId">Id, for which sub nodes to be fetched.</param>
		/// <param name="sbAllSubChilds">ill hold all the sub nodes as the return value of this function.</param>
		private  void GetAllSubNodesForCache(int p_iParentId,ref System.Text.StringBuilder sbAllSubChilds)
		{
			ArrayList arrlstChildNodes;
			try
			{
				arrlstChildNodes = GetAllChildNodesForCache(p_iParentId);
				if(arrlstChildNodes.Count>0)
				{
					foreach(XmlNode objNode in arrlstChildNodes)
					{
						if(sbAllSubChilds.ToString()=="")
						{
							sbAllSubChilds.Append(objNode.Attributes["sid"].Value+"|"+objNode.Attributes["sparentid"].Value);
						}
						else
						{
							sbAllSubChilds.Append(","+objNode.Attributes["sid"].Value+"|"+objNode.Attributes["sparentid"].Value);
						}
                     
						GetAllSubNodesForCache(Conversion.ConvertStrToInteger(objNode.Attributes["sid"].Value),ref sbAllSubChilds);
					}
				}
				else
				{
					return;
				}
         
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
               arrlstChildNodes=null;
			}
		}
		/// <summary>
		/// This function helps to create the permission xml for a particular Dsn.
		/// Will change when the structure of the function_list will be finalized.
		/// </summary>
		/// <param name="p_objDoc">Raw permissions document.</param>
		/// <param name="iRange1">Starting range.</param>
		/// <param name="iRange2">End of the range.</param>
		/// <returns>Arraylist containing the xmlnodes with sids between the specified range.</returns>
		public ArrayList GetIdsRange(ref XmlDocument p_objDoc,int iRange1, int iRange2)
		{
             ArrayList arrlstIds=new ArrayList();
			 XmlNodeList objIdsList;
			try
			{
				objIdsList =p_objDoc.SelectNodes("//entry[@sid>='5000000' and @sid<'7000000']");
				foreach(XmlNode objNode in objIdsList)
				{
					arrlstIds.Add(objNode);
				}
				return arrlstIds;
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
              objIdsList=null;
			}

		}
		/// <summary>
		/// This function generates dsn specific  permission xml. 
		/// </summary>
		/// <param name="p_arrlstAdminIds">Arraylist of ids for which modules to be fetched dynamically.</param>
		/// <param name="p_arrlstRMAllModules">Holds the dsn specific permissions. </param>
		/// <param name="p_sConnStr">Conn str for Dsn.</param>
		/// <param name="p_objRawFunctionListFrmSecurity">Raw permission doc ;replica of function_list table. </param>
		public void GetAdminTrackingXml(ref ArrayList p_arrlstAdminIds,ref ArrayList p_arrlstRMAllModules,string p_sConnStr,ref XmlDocument p_objRawFunctionListFrmSecurity)
		{
			int iID = 0, iParentID = 0;
			int iSMParentEntryType = 0, iSMParentID = 0;
			string sSQL = "";
			RMModules objRMModule = null;
			ArrayList arrlstAdmTables = new ArrayList();
			DbConnection objRMConn = DbFactory.GetDbConnection(p_sConnStr);
			DbReader objReader = null;
			DbConnection objSecurityConn = null;
			int iAdminEntriesCnt=p_arrlstAdminIds.Count;
			try
			{
				objRMConn.Open();
				objReader = objRMConn.ExecuteReader("SELECT GLOSSARY.TABLE_ID,GLOSSARY_TEXT.TABLE_NAME FROM GLOSSARY,GLOSSARY_TEXT WHERE GLOSSARY_TYPE_CODE = 468 AND GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID ORDER BY GLOSSARY_TEXT.TABLE_NAME");
				while(objReader.Read())
				{
					arrlstAdmTables.Add(objReader.GetInt32(0) + "," + objReader.GetString(1));
				}

				foreach(XmlNode objNode in p_arrlstAdminIds)
				{
					iID=Conversion.ConvertStrToInteger(objNode.Attributes["sid"].Value);
					//-- Admin Tracking tables - dynamically populated from 
					//-- RISKMASTER glossary table.
					if(iID >= 5000000 && iID < 6000000)
					{
						if(iID == 5000000)    //-- module
						{
							//-- all top level adm entries should parent to the 
							//-- master adm entry
							//-- create holding entry 'Administrative Tracking' to 
							//-- parent all of the actual tables 
							iParentID = 5000000;

                            objRMModule = new RMModules(p_sConnStr, m_iClientId);
							objRMModule.ID = Conversion.ConvertStrToInteger(objNode.Attributes["sid"].Value);
							objRMModule.ParentID = Conversion.ConvertStrToInteger(objNode.Attributes["sparentid"].Value);
							objRMModule.ModuleName = objNode.Attributes["text"].Value;
							objRMModule.EntryType = Conversion.ConvertStrToInteger(objNode.Attributes["sentrytype"].Value);
							p_arrlstRMAllModules.Add(objRMModule);
						}
							//-- actual permissions
						else  
						{
							iParentID = -1;
						}    
														                
						foreach(string vTmp in arrlstAdmTables)
						{
                            objRMModule = new RMModules(p_sConnStr, m_iClientId);
							string strTemp = "";
							strTemp = vTmp.Substring(0,vTmp.IndexOf(","));
							//-- * 20 leaves space for all the individual permissions
							objRMModule.ID = iID + Convert.ToInt32(strTemp) * 20;   
											
							//-- modules
							if(iParentID == 5000000) 
							{	
								int iLocation = 0;
								int iLength = 0;
								iLocation = vTmp.IndexOf(",");
								iLength = vTmp.Length;
								//-- parent all these under
								objRMModule.ParentID = 5000000;    
								objRMModule.ModuleName = vTmp.Substring((iLocation+1),(iLength-iLocation-1)); //-- module name is glossary text name
							}
								//-- permissions
							else   
							{
								strTemp = vTmp.Substring(0,vTmp.IndexOf(","));	
								objRMModule.ParentID = 5000000 + Convert.ToInt32(strTemp) * 20;
								objRMModule.ModuleName = objNode.Attributes["text"].Value;
							}						
							objRMModule.EntryType = Conversion.ConvertStrToInteger(objNode.Attributes["sentrytype"].Value);
							p_arrlstRMAllModules.Add(objRMModule);
						}
					}
					else 
					{
						if(iID >= 6000000 && iID < 7000000)
						{
							iSMParentID = Conversion.ConvertStrToInteger(objNode.Attributes["sparentid"].Value);
							iSMParentEntryType = Conversion.ConvertStrToInteger(objNode.Attributes["sentrytype"].Value);
			            
							sSQL = "SELECT FIELD_ID,SUPP_TABLE_NAME,SYS_FIELD_NAME,USER_PROMPT,SEQ_NUM FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + objReader.GetString("FUNCTION_NAME").Replace("'", "''") + "'";
							sSQL = sSQL + " AND FIELD_TYPE NOT IN (7,20) AND DELETE_FLAG = 0 ORDER BY SEQ_NUM,FIELD_ID";
							objReader = objRMConn.ExecuteReader(sSQL);
							while(objReader.Read())
							{
                                objRMModule = new RMModules(p_sConnStr, m_iClientId);
								objRMModule.ID = 6500000 + objReader.GetInt("FIELD_ID");
								objRMModule.ParentID = iSMParentID;
								objRMModule.ModuleName = objReader.GetString("USER_PROMPT");
								objRMModule.EntryType = iSMParentEntryType;
								p_arrlstRMAllModules.Add(objRMModule);						
							}
						}
					}
				}
				p_objRawFunctionListFrmSecurity.SelectSingleNode("//entry[@sid='5000000']").InnerXml="";
				XmlElement objElementAdmTrk=(XmlElement)p_objRawFunctionListFrmSecurity.SelectSingleNode("//entry[@sid='5000000']");
				GetTreeForRootNodesForCache(5000000,ref objElementAdmTrk);
				

		}
			catch(Exception objError)
			{
				throw objError;
			}
			finally
			{
				if(objReader!=null)
				{
					//objReader.Close();
					objReader.Dispose();
				}
				if(objRMConn!=null)
				{
					//if(objRMConn.State==System.Data.ConnectionState.Open)
					//{
					//	objRMConn.Close();
						objRMConn.Dispose();
					//}
				}
				if(objSecurityConn!=null)
				{
					//if(objSecurityConn.State==System.Data.ConnectionState.Open)
					//{
					//	objSecurityConn.Close();
						objSecurityConn.Dispose();
					//}
				}
				objRMModule = null;
			    arrlstAdmTables = null;
			}
			
		}
        /// <summary>
        /// This function converts Raw permisson set to dsn specific permission set.
        /// </summary>
        /// <param name="p_objDocRawPerms">Replica of function_list table.</param>
        /// <param name="p_sConnStr">Conn str fro Dsn.</param>
		private void ConvertRawPermissionsToDsnSpecificPermissions(ref XmlDocument p_objDocRawPerms,string p_sConnStr )
		{
            ArrayList objEntries= GetIdsRange(ref p_objDocRawPerms,5000000,6000000);
			m_arrlstDynamicList=new ArrayList();
			GetAdminTrackingXml(ref objEntries,ref m_arrlstDynamicList,p_sConnStr,ref p_objDocRawPerms);
		}
	}

}
