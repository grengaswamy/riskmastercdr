using System;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;

namespace Riskmaster.Application.SecurityManagement
{
	/// <summary>
    ///Author  :   Aashish Bhateja
	///Dated   :   7th,July 2004
	///Purpose :   Contains the methods & properties related to modules.
	///</summary>

	public class RMModules
	{
		/// <summary>
		/// Represents the connection string for the underlying RM DB.
		/// </summary>
		private string m_sConnectionString = "";

		/// <summary>
		/// Represents the Module ID.
		/// </summary>
		private int m_iID = 0;

		/// <summary>
		/// Represents the Parent Module ID.
		/// </summary>
		private int m_iParentID = 0;

		/// <summary>
		/// Represents the Module Name.
		/// </summary>
		private string m_sModuleName = "";

		/// <summary>
		/// Represents the entry type for a particular module/submodule.
		/// viz. 0=base module, 1=standard permission, 2=specific permission.
		/// </summary>
		private int m_iEntryType = 0;

        private int m_iClientId = 0;

		/// <summary>
		/// Constructor for CRMModules class. 
		/// </summary>
		/// <param name="p_sConnectionstring">
		/// Connection string, to be used to connect to the underlying RM Database.
		/// </param>
		public RMModules(string p_sConnectionstring, int p_iClientId)
		{
			m_sConnectionString = p_sConnectionstring;
            m_iClientId = p_iClientId;
		}


		/// <summary>
		/// Represents the entry type for a particular module/submodule.
		/// viz. 0=base module, 1=standard permission, 2=specific permission.
		/// </summary>
		public int EntryType
		{
			get
			{
				return m_iEntryType;
			}
			set
			{
				m_iEntryType = value;
			}
		}
		
		/// <summary>
		/// Represents the name of the module.
		/// </summary>
		public string ModuleName
		{
			get
			{
				return m_sModuleName;
			}
			set
			{
				m_sModuleName = value;
			}
		}

		/// <summary>
		/// Represents the parent ID for a module.
		/// </summary>
		public int ParentID
		{
			get
			{
				return m_iParentID;
			}
			set
			{
				m_iParentID = value;
			}
		}

		/// <summary>
		/// Represents the ID for a module.
		/// </summary>
		public int ID
		{
			get
			{
				return m_iID;
			}
			set
			{
				m_iID = value;
			}
		}		
		
		/// <summary>
		/// Loads various modules/submodules based on certain conditions.
		/// </summary>
		/// <returns>
		/// Boolean value representing success/failure.
		/// </returns>
		public bool Load(ref ArrayList p_arrlstRMAllModules)
		{
			bool bReturn = false;
			int iID = 0, iParentID = 0;
			int iSMParentEntryType = 0, iSMParentID = 0;
			string sSQL = "";
			RMModules objRMModule = null;
			ArrayList arrlstAdmTables = new ArrayList();
			DbConnection objRMConn = DbFactory.GetDbConnection(m_sConnectionString);
			DbReader objReader = null;
			DbConnection objSecurityConn = null;
			DbReader objReadTemp=null;
			try
			{
				objRMConn.Open();
				objReader = objRMConn.ExecuteReader("SELECT GLOSSARY.TABLE_ID,GLOSSARY_TEXT.TABLE_NAME FROM GLOSSARY,GLOSSARY_TEXT WHERE GLOSSARY_TYPE_CODE = 468 AND GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID ORDER BY GLOSSARY_TEXT.TABLE_NAME");
				while(objReader.Read())
				{
					arrlstAdmTables.Add(objReader.GetInt32(0) + "," + objReader.GetString(1));
				}
				objReader.Dispose();
				objReader.Close();
				objRMConn.Close();
				
				objSecurityConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
				objSecurityConn.Open();
				objReader = objSecurityConn.ExecuteReader("SELECT * FROM FUNCTION_LIST ORDER BY FUNC_ID,PARENT_ID");

				while(objReader.Read())
				{
					iID = objReader.GetInt("FUNC_ID");
					//-- Admin Tracking tables - dynamically populated from 
					//-- RISKMASTER glossary table.
					if(iID >= 5000000 && iID < 6000000)
					{
						if(iID == 5000000)    //-- module
						{
							//-- all top level adm entries should parent to the 
							//-- master adm entry
							//-- create holding entry 'Administrative Tracking' to 
							//-- parent all of the actual tables 
							iParentID = 5000000;    

							objRMModule = new RMModules(m_sConnectionString,m_iClientId);
							objRMModule.ID = objReader.GetInt("FUNC_ID");
							objRMModule.ParentID = objReader.GetInt("PARENT_ID");
							objRMModule.ModuleName = objReader.GetString("FUNCTION_NAME");
							objRMModule.EntryType = objReader.GetInt("ENTRY_TYPE");
							p_arrlstRMAllModules.Add(objRMModule);
						}
							//-- actual permissions
						else  
						{
							iParentID = -1;
						}    
									                
						foreach(string vTmp in arrlstAdmTables)
						{
                            objRMModule = new RMModules(m_sConnectionString, m_iClientId);
							string strTemp = "";
							strTemp = vTmp.Substring(0,vTmp.IndexOf(","));
							//-- * 20 leaves space for all the individual permissions
							objRMModule.ID = iID + Convert.ToInt32(strTemp) * 20;   
						
							//-- modules
							if(iParentID == 5000000) 
							{	
								int iLocation = 0;
								int iLength = 0;
								iLocation = vTmp.IndexOf(",");
								iLength = vTmp.Length;
								//-- parent all these under
								objRMModule.ParentID = 5000000;    
								objRMModule.ModuleName = vTmp.Substring((iLocation+1),(iLength-iLocation-1)); //-- module name is glossary text name
							}
								//-- permissions
							else   
							{
								strTemp = vTmp.Substring(0,vTmp.IndexOf(","));	
								objRMModule.ParentID = 5000000 + Convert.ToInt32(strTemp) * 20;
								objRMModule.ModuleName = objReader.GetString("FUNCTION_NAME");
							}						
							objRMModule.EntryType = objReader.GetInt("ENTRY_TYPE");
							p_arrlstRMAllModules.Add(objRMModule);
						}
					}
						//-- SORTMASTER supp field security entries.
					else if(iID >= 6000000 && iID < 7000000)
					{
						iSMParentID = objReader.GetInt("PARENT_ID");
						iSMParentEntryType = objReader.GetInt("ENTRY_TYPE");
			            
						sSQL = "SELECT FIELD_ID,SUPP_TABLE_NAME,SYS_FIELD_NAME,USER_PROMPT,SEQ_NUM FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + objReader.GetString("FUNCTION_NAME").Replace("'", "''") + "'";
						sSQL = sSQL + " AND FIELD_TYPE NOT IN (7,20) AND DELETE_FLAG = 0 ORDER BY SEQ_NUM,FIELD_ID";
						objReadTemp=DbFactory.GetDbReader(m_sConnectionString,sSQL);
						while(objReadTemp.Read())
						{
                            objRMModule = new RMModules(m_sConnectionString, m_iClientId);
							objRMModule.ID = 6500000 + objReadTemp.GetInt("FIELD_ID");
							objRMModule.ParentID = iSMParentID;
							objRMModule.ModuleName = objReadTemp.GetString("USER_PROMPT");
							objRMModule.EntryType = iSMParentEntryType;
							p_arrlstRMAllModules.Add(objRMModule);						
						}

						objReadTemp.Dispose();
						objReadTemp.Close();
						
						
					}
						//-- All other entries.
					else 
					{
                        objRMModule = new RMModules(m_sConnectionString, m_iClientId);
						objRMModule.ID = objReader.GetInt("FUNC_ID");
						objRMModule.ParentID = objReader.GetInt("PARENT_ID");
						objRMModule.ModuleName = objReader.GetString("FUNCTION_NAME");
						objRMModule.EntryType = objReader.GetInt("ENTRY_TYPE");
						p_arrlstRMAllModules.Add(objRMModule);
					}
				}
				bReturn = true;
			}
			catch(Exception objError)
			{
				throw objError;
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}                
                if (objReadTemp != null)
                {
                    objReadTemp.Close();
                    objReadTemp.Dispose();
                }
				if(objRMConn!=null)
				{
					//if(objRMConn.State==System.Data.ConnectionState.Open)
					//{
					//	objRMConn.Close();
						objRMConn.Dispose();
					//}
				}
				if(objSecurityConn!=null)
				{
					//if(objSecurityConn.State==System.Data.ConnectionState.Open)
					//{
					//	objSecurityConn.Close();
						objSecurityConn.Dispose();
					//}
				}
                objRMModule = null;
                arrlstAdmTables = null;
			}
			return bReturn;
		}
	}

}
