using System;

namespace Riskmaster.Application.SecurityManagement
{
	/// <summary>
	/// Author  :   Tanuj Narula
	/// Dated   :   26th,June 2004
	/// Purpose :  This is used for setting the constant values.All the constant values should be added in here.
	/// </summary>
	public class Constants
	{
		/// <summary>
		/// This is the default constructor
		/// </summary>
		public Constants()
		{
		}
		/// <summary>
		/// This is crypt key's value.
		/// </summary>
		internal const string CRYPTKEY = "6378b87457a5ecac8674e9bac12e7cd9";
		

		
	}
}
