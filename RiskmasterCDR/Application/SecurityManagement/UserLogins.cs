using System;
using System.Collections;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Security.Encryption;
using System.Data;
using System.Collections.Generic;
using Riskmaster.Application.VSSInterface;

namespace Riskmaster.Application.SecurityManagement
{
	/// <summary> 
	///Author  :   Tanuj Narula
	///Dated   :   18th,June 2004
	///Purpose :   Riskmaster.Application.SecurityManagement.UserLogin is used to retrieve the user information,it also check for the user 
	///            Privilege to use the application.
	/// </summary>
	
	[Serializable]
	public class UserLogins : UserLogin
	{
		/// <summary>
		/// This is code access key control.
		/// </summary>
		private const string CODE_ACCESS_PUBLIC_KEY = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";
		
		/// <summary>
		/// Represents the start time for user access permissions.
		/// </summary>
		private const string DEF_TIME_START  = "00:00";

		/// <summary>
		/// Represents the end time for user access permissions.
		/// </summary>
		private const string DEF_TIME_END = "00:00";
		
		/// <summary>
		/// This variable flags the MonAccess permission.
		/// </summary>
		private bool m_bMonAccess=false;

		/// <summary>
		/// This variable flags the TueAccess permission.
		/// </summary>
		private bool m_bTueAccess=false;
		
		/// <summary>
		/// This variable flags the WedAccess permission.
		/// </summary>
		private bool m_bWedAccess=false;
		
		/// <summary>
		/// This variable flags the ThuAccess permission.
		/// </summary>
		private bool m_bThuAccess=false;
		
		/// <summary>
		/// This variable flags the FriAccess permission.
		/// </summary>
		private bool m_bFriAccess=false;
		
		/// <summary>
		/// This variable flags the SatAccess permission.
		/// </summary>
		private bool m_bSatAccess=false;
		
		/// <summary>
		/// This variable flags the SunAccess permission.
		/// </summary>
		private bool m_bSunAccess=false;

		/// <summary>
		/// This variable handles the Permission Expires flag.
		/// </summary>
		private bool m_bPermissionExpires=false;

		/// <summary>
		/// This variable flags whether the object is in deleted state or not.
		/// </summary>
		internal  bool m_bIsDeleted=false;

        /// <summary>
        /// Private Variable for Client Id
        /// </summary>
        private int m_iClientId = 0;

		/// <summary>
		/// Riskmaster.Application.SecurityManagement.UserLogins is the default constructor.
		/// </summary>
		public UserLogins(bool p_bFindDBType, int p_iClientId):base(p_bFindDBType, p_iClientId)
		{
			base.EnforceCheckSum = false;
            m_iClientId = p_iClientId;
		}
        public UserLogins(int p_iClientId): base(p_iClientId)
		{
			base.EnforceCheckSum = false;
            m_iClientId = p_iClientId;
		}
        //gagnihotri MITS 11995 Changes made for Audit table
        private string m_sUserName = "";
        public UserLogins(bool p_bFindDBType, string p_sUserName, int p_iClientId)
            : base(p_bFindDBType, p_sUserName, p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_iClientId = p_iClientId;
            base.EnforceCheckSum = false;
        }
		/// <summary>
		/// Riskmaster.Application.SecurityManagement.UserLogins constructor with parameter. 
		/// This constructor calls the function Load() and serves as a wrapper.
		/// </summary>
		/// <param name="p_iUserId">Value of the User_Id corresponding to which object will be populated.</param>
		/// <param name="p_iDataSourceId">Value of the DataSourceId corresponding to which object will be populated.</param>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
        public UserLogins(int p_iUserId, int p_iDataSourceId, int p_iClientId)
            : base(p_iUserId, p_iClientId)
		{
			try
			{
				base.EnforceCheckSum = false;
                m_iClientId = p_iClientId;
				this.Load(p_iUserId,p_iDataSourceId);                
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
		}

        //gagnihotri MITS 11995 Changes made for Audit table
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = CODE_ACCESS_PUBLIC_KEY)]
        public UserLogins(int p_iUserId, int p_iDataSourceId, string p_sUserName, int p_iClientId) : base(p_iClientId)
        {
            try
            {
                m_sUserName = p_sUserName;
                base.EnforceCheckSum = false;
                m_iClientId = p_iClientId;
                this.Load(p_iUserId, p_iDataSourceId);                
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
            }
        }

		/// <summary>
		/// This function call loads the Login collection(m_arrlstLoginCol) defined in PublicFunctions.cs. 
		/// First Code Access Security Usage.
		/// This attribute requires that any caller to have a strongname proving that it's CSC Riskmaster Code. 
		/// To ensure that your assembly has an appropriate strongname, place the following line in the 
		///	AssemblyInfo.cs file:
		/// [assembly: AssemblyKeyFile("..\\..\\..\\Riskmaster.snk")].Here the path of the .snk file has to be specified relative to your project.
		/// </summary>
		/// <returns>True if successful else throws error.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool Load(ref ArrayList p_arrlstLoginCol)
		{
			bool bRetVal = false;
			DbConnection objConn = null; 
			UserLogins objUserLogin = null;
			DbReader objReader=null;
			try
			{
				
				objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
				objConn.Open();
				objReader=objConn.ExecuteReader("SELECT USER_DETAILS_TABLE.* FROM USER_DETAILS_TABLE,USER_TABLE WHERE USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID ORDER BY FIRST_NAME,LAST_NAME");
				//This loop will load all the persisted UserLogins object to a collection in memory.
				while(objReader.Read())
				{
					objUserLogin=new UserLogins(false,m_iClientId);
					objUserLogin.UserId=objReader.GetInt("USER_ID");
					objUserLogin.DatabaseId = objReader.GetInt("DSNID");
					objUserLogin.LoginName = objReader.GetString("LOGIN_NAME");
					objUserLogin.Password = PublicFunctions.Decrypt(objReader.GetString("PASSWORD"));
					objUserLogin.ForceChangePwd = objReader.GetInt("FORCE_CHANGE_PWD");
					objUserLogin.PasswordExpire = Conversion.ToDate(objReader.GetString("PRIVS_EXPIRE"));
					objUserLogin.SunStart =objReader.GetString("SUN_START");
					objUserLogin.SunEnd = objReader.GetString("SUN_END");
					if(objUserLogin.SunStart=="" && objUserLogin.SunEnd=="")
						objUserLogin.SunAccess=false;
					else
						objUserLogin.SunAccess=true;
					objUserLogin.MonStart = objReader.GetString("MON_START");
					objUserLogin.MonEnd = objReader.GetString("MON_END");
					if(objUserLogin.MonStart=="" && objUserLogin.MonEnd=="")
						objUserLogin.MonAccess=false;
					else
						objUserLogin.MonAccess=true;
					objUserLogin.TueStart = objReader.GetString("TUES_START");
					objUserLogin.TueEnd =objReader.GetString("TUES_END");
					if(objUserLogin.TueStart=="" && objUserLogin.TueEnd=="")
						objUserLogin.TueAccess=false;
					else
						objUserLogin.TueAccess=true;
					objUserLogin.WedStart = objReader.GetString("WEDS_START");
					objUserLogin.WedEnd = objReader.GetString("WEDS_END");
					if(objUserLogin.WedStart=="" && objUserLogin.WedEnd=="")
						objUserLogin.WedAccess=false;
					else
						objUserLogin.WedAccess=true;
					objUserLogin.ThuStart = objReader.GetString("THURS_START");
					objUserLogin.ThuEnd = objReader.GetString("THURS_END");
					if(objUserLogin.ThuStart=="" && objUserLogin.ThuEnd=="")
						objUserLogin.ThuAccess=false;
					else
						objUserLogin.ThuAccess=true;
					objUserLogin.FriStart = objReader.GetString("FRI_START");
					objUserLogin.FriEnd = objReader.GetString("FRI_END");
					if(objUserLogin.FriStart=="" && objUserLogin.FriEnd=="")
						objUserLogin.FriAccess=false;
					else
						objUserLogin.FriAccess=true;
					objUserLogin.SatStart = objReader.GetString("SAT_START");
					objUserLogin.SatEnd = objReader.GetString("SAT_END");
					if(objUserLogin.SatStart=="" && objUserLogin.SatEnd=="")
						objUserLogin.SatAccess=false;
					else
						objUserLogin.SatAccess=true;

					objUserLogin.DocumentPath=objReader.GetString("DOC_PATH");
					p_arrlstLoginCol.Add(objUserLogin);
				}
				objUserLogin=null;
				bRetVal=true;
			}
			
			catch(Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("UserLogins.Load.LoadErrorUserLogins", m_iClientId),p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					//objReader.Close();
					objReader.Dispose();
				}
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
				if(objUserLogin!=null)
				{
					objUserLogin=null;
				}
			}
			return bRetVal;
		}

        //srajindersin MITS 30263 dt 11/27/2012
        /// <summary>
        /// This function returns the UserLogin Dataset 
        /// First Code Access Security Usage.
        /// This attribute requires that any caller to have a strongname proving that it's CSC Riskmaster Code. 
        /// To ensure that your assembly has an appropriate strongname, place the following line in the 
        ///	AssemblyInfo.cs file:
        /// [assembly: AssemblyKeyFile("..\\..\\..\\Riskmaster.snk")].Here the path of the .snk file has to be specified relative to your project.
        /// </summary>
        /// <returns>True if successful else throws error.</returns>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = CODE_ACCESS_PUBLIC_KEY)]
        public DataSet Load()
        {
            DataSet dsUserLogin = new DataSet();
            try
            {
                dsUserLogin = DbFactory.ExecuteDataSet(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT USER_DETAILS_TABLE.DSNID,USER_DETAILS_TABLE.USER_ID FROM USER_DETAILS_TABLE,USER_TABLE WHERE USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID ORDER BY FIRST_NAME,LAST_NAME", new Dictionary<string, string>(),m_iClientId);
            }

            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("UserLogins.Load.LoadErrorUserLogins", m_iClientId), p_objException);
            }

            return dsUserLogin;
        }
		
		/// <summary>
		/// Riskmaster.Application.SecurityManagement.Load uses p_iUserId and p_iDataSourceId
		/// to retrieve information from USER_DETAILS_TABLE through DbReader 
		/// object, that is further passed to LoadData(). 
		/// If it is unable to retrieve the data, InvalidUserNameOrPasswordException is thrown.
		/// </summary>
		/// <param name="p_iUserId">Value of the User_Id column</param>
		/// <param name="p_iDataSourceId">Value of the DataSourceId column</param>
		/// <returns>True if successful else throws error.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool Load(int p_iUserId, int p_iDataSourceId)
		{
            Db.DbConnection objConn = null;
            Db.DbReader objReader = null;
            bool bRetVal = false;
            try
            {
                objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
                objConn.Open();
                objReader = objConn.ExecuteReader("SELECT * FROM USER_DETAILS_TABLE WHERE USER_ID=" + p_iUserId + "AND DSNID=" + p_iDataSourceId);
                //srajindersin 5/24/2014 MITS 36472
                //if(!objReader.Read())
                //    throw new Riskmaster.ExceptionTypes.InvalidUserNameOrPasswordException(Globalization.GetString("UserLogins.Load.InvalidUIdDId"));
                //else
                if (objReader.Read() && LoadData(objReader))
                    bRetVal = true;

            }
            catch (Exception p_objException)
            {
                //throw new DataModelException(Globalization.GetString(""),p_objException);
            }
            finally
            {
                if (objReader != null)
                    //objReader.Close();
                    objReader.Dispose();
                if (objConn != null)
                {
                    //objConn.Close();
                    objConn.Dispose();
                }
            }
            return bRetVal;
		}

		/// <summary>
		/// Riskmaster.Application.SecurityManagement.LoadData populates the attributes for the UserLogin class, from the input 
		/// DbReader object.
		/// Sets the flag m_bDataChanged to false.
		/// </summary>
		/// <param name="p_objReader">The native reader to wrap</param>
		/// <returns>True if successful else throws error.</returns>
		new private bool LoadData(DbReader p_objReader)
		{ 
			bool bRetVal=false;
			try
			{
				//All the attributes corresponding to UserLogin will be loaded.
				UserId=p_objReader.GetInt("USER_ID");
				DatabaseId= p_objReader.GetInt("DSNID");
				LoginName = p_objReader.GetString("LOGIN_NAME");
				Password = PublicFunctions.Decrypt(p_objReader.GetString("PASSWORD"));
				ForceChangePwd = p_objReader.GetInt("FORCE_CHANGE_PWD");
				PasswordExpire = Conversion.ToDate(p_objReader.GetString("PRIVS_EXPIRE"));
				SunStart =PublicFunctions.GetFromDbTime(p_objReader.GetString("SUN_START"));
				SunEnd = PublicFunctions.GetFromDbTime(p_objReader.GetString("SUN_END"));
				if(SunStart=="" && SunEnd=="")
					SunAccess=false;
				else
					SunAccess=true;
				MonStart = PublicFunctions.GetFromDbTime(p_objReader.GetString("MON_START"));
				MonEnd = PublicFunctions.GetFromDbTime(p_objReader.GetString("MON_END"));
				if(MonStart=="" && MonEnd=="")
					MonAccess=false;
				else
					MonAccess=true;
				TueStart = PublicFunctions.GetFromDbTime(p_objReader.GetString("TUES_START"));
				TueEnd =PublicFunctions.GetFromDbTime(p_objReader.GetString("TUES_END"));
				if(TueStart=="" && TueEnd=="")
					TueAccess=false;
				else
					TueAccess=true;
				WedStart = PublicFunctions.GetFromDbTime(p_objReader.GetString("WEDS_START"));
				WedEnd = PublicFunctions.GetFromDbTime(p_objReader.GetString("WEDS_END"));
				if(WedStart=="" && WedEnd=="")
					WedAccess=false;
				else
					WedAccess=true;
				ThuStart = PublicFunctions.GetFromDbTime(p_objReader.GetString("THURS_START"));
				ThuEnd = PublicFunctions.GetFromDbTime(p_objReader.GetString("THURS_END"));
				if(ThuStart=="" && ThuEnd=="")
					ThuAccess=false;
				else
					ThuAccess=true;
				FriStart = PublicFunctions.GetFromDbTime(p_objReader.GetString("FRI_START"));
				FriEnd = PublicFunctions.GetFromDbTime(p_objReader.GetString("FRI_END"));
				if(FriStart=="" && FriEnd=="")
					FriAccess=false;
				else
					FriAccess=true;
				SatStart = PublicFunctions.GetFromDbTime(p_objReader.GetString("SAT_START"));
				SatEnd = PublicFunctions.GetFromDbTime(p_objReader.GetString("SAT_END"));
				if(SatStart=="" && SatEnd=="")
					SatAccess=false;
				else
					SatAccess=true;
				DocumentPath=p_objReader.GetString("DOC_PATH");
				bRetVal=true;
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if(p_objReader!=null)
				{
					p_objReader.Close();
				}
			}
			return bRetVal;
		}

		/// <summary>
		/// This function call actually removes the Login from database.One can directly call this function by passing in the User_Id and DataSourceId to be deleted 
		/// or load this class with User_Id,DataSourceId and call its Remove() function to do the same job.
		/// </summary>
		/// <param name="p_iUserId">User_Id to be deleted.</param>
		/// <param name="p_iDataSourceId">DataSourceId to be deleted.</param>
		/// <returns>True if successful else throws error.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool Remove(int p_iUserId,int p_iDataSourceId)
		{  
			bool bRetVal=false;
			string sSQL="";
			DbConnection objConn = null; 
			int iNoOfRowsAffected=0;

			try
			{
				sSQL = "DELETE FROM USER_DETAILS_TABLE WHERE USER_ID=" + p_iUserId + "AND DSNID=" + p_iDataSourceId;
                objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
				objConn.Open();
				iNoOfRowsAffected=objConn.ExecuteNonQuery(sSQL);
				bRetVal=true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("UserLogins.Remove.RemoveError", m_iClientId), p_objException);

			}
			finally
			{
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
			return bRetVal;
		}
		/// <summary>
		/// This functon will update the password in SMS DB and AD.
		/// </summary>
		/// <param name="p_stUpdatePasswordInfo">Update password related info struct.</param>
		public void UpdateUserPassword(UpdatePasswordInfo p_stUpdatePasswordInfo, bool p_bCalledFrmChangePwdScreen)
		{
			Db.DbConnection objConn = null;
			DbWriter objWriter;
			try
			{
                objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
				objConn.Open();
				objWriter=DbFactory.GetDbWriter(objConn);
				objWriter.Tables.Add("USER_DETAILS_TABLE");
                objWriter.Where.Add("USER_ID=" + p_stUpdatePasswordInfo.UserId);
				if(!p_bCalledFrmChangePwdScreen)
				{
                  objWriter.Fields.Add("LOGIN_NAME",p_stUpdatePasswordInfo.LoginName);
				}
                objWriter.Fields.Add("PASSWORD", RMCryptography.EncryptString(p_stUpdatePasswordInfo.NewPassword));
                objWriter.Execute();

			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				objWriter=null;
				if(objConn!=null)
				{
					//objConn.Close();
					objConn.Dispose();  
				}
			}
			
		}
		public void UpdateLoginPassword(ref UserLogins p_objUserLogin)
		{
			DbReader objReader=null;
			string sSql;
			int iCount=0;
			bool UpdateExisting=false;
			try
			{
				sSql="SELECT * FROM USER_DETAILS_TABLE WHERE USER_ID= "+p_objUserLogin.UserId;
                objReader = DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), sSql);
				while(objReader.Read())
				{
					//User already exists
					++iCount;

				}
				objReader.Close();
				if(iCount==0)//new user login
				{
					return;
				}
				if(iCount>=1)
				{
                    objReader = DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), sSql);
					while(objReader.Read())
					{
						if(objReader.GetInt32("DSNID")==p_objUserLogin.DatabaseId)//Update existing
						{
							UpdateExisting=true;
							break;
						}
					}
					objReader.Close();
						if(UpdateExisting)//Update existing
						{
							UpdateUsersLoginAndPassword(ref p_objUserLogin);
						}
						else
						{
							//Creating a new record.
                            objReader = DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), sSql);
							if(objReader.Read())
							{
								p_objUserLogin.LoginName=objReader.GetString("LOGIN_NAME");
                                //abansal23: To get the decrypted password.
								p_objUserLogin.Password=RMCryptography.DecryptString(objReader.GetString("PASSWORD"));
								objReader.Close();
							}
							return;
						}
                        // Start: Ash - cloud , VSS interface is not moving to cloud
                        if (m_iClientId == 0)
                        {
                            //Start - Export user id and password to VSS
                            string sSqlQuery = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ENABLE_VSS'";
                            objReader = DbFactory.GetDbReader(p_objUserLogin.objRiskmasterDatabase.ConnectionString, sSqlQuery);
                            if (objReader.Read())
                            {
                                if (objReader.GetString("STR_PARM_VALUE").Equals("-1"))
                                {
                                    VssExportAsynCall objVss = new VssExportAsynCall(p_objUserLogin.objRiskmasterDatabase.DataSourceName, p_objUserLogin.LoginName, p_objUserLogin.Password, m_iClientId);
                                    objVss.AsynchVssReserveExport(0, 0, 0, 0, 0, p_objUserLogin.LoginName, p_objUserLogin.Password, p_objUserLogin.objUser.Email, p_objUserLogin.objUser.Dsn, "PassUpdate");
                                    objVss = null;
                                }
                            }
                            objReader.Close();
                            //End - Export user id and password to VSS
                        }// End: Ash- Cloud
                    
                       DbFactory.ExecuteNonQuery(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), "UPDATE USER_DETAILS_TABLE SET IS_PWD_RESET=0 WHERE USER_ID= '" + p_objUserLogin.objUser.UserId.ToString() + "' ");//asharma326 MITS 27586
                    
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if(objReader!=null)
				{
					if(!objReader.IsClosed)
					{
						objReader.Close();
						objReader=null;
					}
				}
			}
		}

        //Asharma326 MITS 27586 Starts
        //public void UpdateflagPwdReset(ref UserLogins p_objUserLogin)
        //{
        //    try
        //    {
        //        DbFactory.ExecuteNonQuery(SecurityDatabase.GetSecurityDsn(0), "UPDATE USER_DETAILS_TABLE SET [IS_PWD_RESET]=0 WHERE USER_ID= '" + p_objUserLogin.objUser.UserId.ToString() + "' ");
        //    }
        //    catch (Exception exe)
        //    {
        //        throw exe;
        //    }

        //} 
        //Asharma326 MITS 27586 Ends
		/// <summary>
		/// This function will update the user login and password for specified user id.
		/// </summary>
		/// <param name="p_objUserLogin">user login object.</param>
		public void UpdateUsersLoginAndPassword(ref UserLogins p_objUserLogin)
		{
			
			string sSql = "" ;
			
			try
			{   
                sSql = "Select * from USER_DETAILS_TABLE where USER_ID= " +p_objUserLogin.objUser.UserId.ToString() ;
				DbReader objReader = null;
				UserLogins objUserLogin ;
				try
				{

                    objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), sSql);
					while( objReader.Read() )
					{
						//gagnihotri MITS 11995 Changes made for Audit table
                        //objUserLogin=new UserLogins(false);
                        objUserLogin = new UserLogins(false, m_sUserName, m_iClientId);
						objUserLogin.UserId=objReader.GetInt("USER_ID");
						objUserLogin.DatabaseId = objReader.GetInt("DSNID");
						objUserLogin.LoginName = p_objUserLogin.LoginName ;
						objUserLogin.Password = p_objUserLogin.Password ;
						objUserLogin.ForceChangePwd = p_objUserLogin.ForceChangePwd; 
                        //Start rsushilaggar MITS-23390 date 01/11/2011
						objUserLogin.PasswordExpire = Conversion.ToDate(objReader.GetString("PASSWD_EXPIRE"));
                        objUserLogin.PrivilegesExpire = Conversion.ToDate(objReader.GetString("PRIVS_EXPIRE"));
                        //End rsushilaggar
						objUserLogin.SunStart =objReader.GetString("SUN_START");
						objUserLogin.SunEnd = objReader.GetString("SUN_END");
						if(objUserLogin.SunStart=="" && objUserLogin.SunEnd=="")
							objUserLogin.SunAccess=false;
						else
							objUserLogin.SunAccess=true;
						objUserLogin.MonStart = objReader.GetString("MON_START");
						objUserLogin.MonEnd = objReader.GetString("MON_END");
						if(objUserLogin.MonStart=="" && objUserLogin.MonEnd=="")
							objUserLogin.MonAccess=false;
						else
							objUserLogin.MonAccess=true;
						objUserLogin.TueStart = objReader.GetString("TUES_START");
						objUserLogin.TueEnd =objReader.GetString("TUES_END");
						if(objUserLogin.TueStart=="" && objUserLogin.TueEnd=="")
							objUserLogin.TueAccess=false;
						else
							objUserLogin.TueAccess=true;
						objUserLogin.WedStart = objReader.GetString("WEDS_START");
						objUserLogin.WedEnd = objReader.GetString("WEDS_END");
						if(objUserLogin.WedStart=="" && objUserLogin.WedEnd=="")
							objUserLogin.WedAccess=false;
						else
							objUserLogin.WedAccess=true;
						objUserLogin.ThuStart = objReader.GetString("THURS_START");
						objUserLogin.ThuEnd = objReader.GetString("THURS_END");
						if(objUserLogin.ThuStart=="" && objUserLogin.ThuEnd=="")
							objUserLogin.ThuAccess=false;
						else
							objUserLogin.ThuAccess=true;
						objUserLogin.FriStart = objReader.GetString("FRI_START");
						objUserLogin.FriEnd = objReader.GetString("FRI_END");
						if(objUserLogin.FriStart=="" && objUserLogin.FriEnd=="")
							objUserLogin.FriAccess=false;
						else
							objUserLogin.FriAccess=true;
						objUserLogin.SatStart = objReader.GetString("SAT_START");
						objUserLogin.SatEnd = objReader.GetString("SAT_END");
						if(objUserLogin.SatStart=="" && objUserLogin.SatEnd=="")
							objUserLogin.SatAccess=false;
						else
							objUserLogin.SatAccess=true;

						objUserLogin.DocumentPath=objReader.GetString("DOC_PATH");
						objUserLogin.Save() ;
						
					}
						
				}
				finally
				{
					if(objReader!=null)
					{
						objReader.Close() ;
					}
					objUserLogin = null ;
				}

			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
			
		}
		/// <summary>
		/// Riskmaster.Application.SecurityManagement.MonAccess flags the MonAccess permission and is set to true or false while this class gets loaded.
		/// </summary>
		public bool MonAccess
		{
			get {return m_bMonAccess;}
			set
			{
				if(m_bMonAccess!=value)
				{
					m_bMonAccess=value;
					if (value && MonStart=="")
						MonStart=DEF_TIME_START;
					if(value && MonEnd=="")
						MonEnd=DEF_TIME_END;
				}
			}
		}

		/// <summary>
		/// Riskmaster.Application.SecurityManagement.TueAccess flags the TueAccess permission and is set to true or false while this class gets loaded.
		/// </summary>
		public bool TueAccess
		{
			get {return m_bTueAccess;}
			set
			{
				if(m_bTueAccess!=value)
				{
					m_bTueAccess=value;
					if (value && TueStart=="")
						TueStart=DEF_TIME_START;
					if(value && TueEnd=="")
						TueEnd=DEF_TIME_END;
				}
			}
		}

		/// <summary>
		/// Riskmaster.Application.SecurityManagement.WedAccess flags the WedAccess permission and is set to true or false while this class gets loaded.
		/// </summary>
		public bool WedAccess
		{
			get {return m_bWedAccess;}
			set
			{
				if(m_bWedAccess!=value)
				{
					m_bWedAccess=value;
					if (value && WedStart=="")
						WedStart=DEF_TIME_START;
					if(value && WedEnd=="")
						WedEnd=DEF_TIME_END;
				}
			}
		}

		/// <summary>
		/// Riskmaster.Application.SecurityManagement.ThuAccess flags the ThuAccess permission and is set to true or false while this class gets loaded.
		/// </summary>
		public bool ThuAccess
		{
			get {return m_bThuAccess;}
			set
			{
				if(m_bThuAccess!=value)
				{
					m_bThuAccess=value;
					if (value && ThuStart=="")
						ThuStart=DEF_TIME_START;
					if(value && ThuEnd=="")
						ThuEnd=DEF_TIME_END;
				}
			}
		}

		/// <summary>
		/// Riskmaster.Application.SecurityManagement.FriAccess flags the FriAccess permission and is set to true or false while this class gets loaded.
		/// </summary>
		public bool FriAccess
		{
			get {return m_bFriAccess;}
			set
			{
				if(m_bFriAccess!=value)
				{
					m_bFriAccess=value;
					if (value && FriStart=="")
						FriStart=DEF_TIME_START;
					if(value && FriEnd=="")
						FriEnd=DEF_TIME_END;
				}
			}
		}

		/// <summary>
		/// Riskmaster.Application.SecurityManagement.SatAccess flags the SatAccess permission and is set to true or false while this class gets loaded.
		/// </summary>
		public bool SatAccess
		{
			get {return m_bSatAccess;}
			set
			{
				if(m_bSatAccess!=value)
				{
					m_bSatAccess=value;
					if (value && SatStart=="")
						SatStart=DEF_TIME_START;
					if(value && SatEnd=="")
						SatEnd=DEF_TIME_END;
				}
			}
		}

		/// <summary>
		/// Riskmaster.Application.SecurityManagement.SunAccess flags the SunAccess permission and is set to true or false while this class gets loaded.
		/// </summary>
		public bool SunAccess
		{
			get {return m_bSunAccess;}
			set
			{
				
				if(m_bSunAccess!=value)
				{
					m_bSunAccess=value;
					if (value && SunStart=="")
						SunStart=DEF_TIME_START;
					if(value && SunEnd=="")
						SunEnd=DEF_TIME_END;
				}
			}
		}

		/// <summary>
		/// Riskmaster.Application.SecurityManagement.PermissionExpires flags the PermissionExpires permission and is set to true or false while this class gets loaded.
		/// </summary>
		public bool PermissionExpires
		{
			get {return m_bPermissionExpires;}
			set
			{				
				m_bPermissionExpires = true;
			}
		}
	}
	public struct UpdatePasswordInfo
	{
		private int m_iDsnId;
		private int m_iUserId;
		private string m_sOldPassword;
		private string m_sNewPassword;
		private string m_sLoginName;
		public UpdatePasswordInfo(int p_iDsnId,int p_iUserId,string p_sOldPassword,string p_sNewPassword,string p_sLoginName)
		{
            m_iDsnId=p_iDsnId;
		    m_iUserId=p_iUserId;
			m_sOldPassword=p_sOldPassword;
			m_sNewPassword=p_sNewPassword;
			m_sLoginName=p_sLoginName;

		}
		public int DsnId
		{
			get
			{
				return m_iDsnId;
			}
			
		}
		public int UserId
		{
			get
			{
				return m_iUserId;
			}
			
		}
		public string OldPassword
		{
			get
			{
				return m_sOldPassword;
			}
			
		}
		public string NewPassword
		{
			get
			{
				return m_sNewPassword;
			}
			
		}
		public string LoginName
		{
			get
			{
				return m_sLoginName;
			}
		}

	}
} 
