﻿using System;
using Riskmaster.Security;
using Riskmaster.Common;
using System.DirectoryServices;
using Riskmaster.ExceptionTypes;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using Riskmaster.Db;
using System.Data;
using Riskmaster.Security.Encryption;
using System.Collections;
using System.Reflection ;
using Riskmaster.Application.AsyncManagement;
namespace Riskmaster.Application.SecurityManagement
{
	public class OpenLdapExporter
	{
		public OpenLdapExporter()
		{
		}
		/// <summary>
		/// Input Doc type:
		/// <LdapExportUsersList>
		/// <Users>
		/// <User login="test" password="test" and so on>
		/// </User>
		/// <User>
		/// </User>
		/// </Users>
		/// </LdapExportUsersList>
		/// </summary>
		/// <param name="p_objDocIn"></param>
		public void GetUsersFromSMSDB(ref XmlDocument p_objDocIn)
		{
			DataSet objUsersData = null;
            string sSql="";
			try
			{
              
                sSql =  "Select UDTFirst.user_id , UDTFirst.dsnid, UDTFirst.login_name,UDTFirst.password, UT.FIRST_NAME ,UT.LAST_NAME from User_Details_Table UDTFirst, User_Table UT where dsnid = (select max(dsnid) from User_Details_Table UDTSecond where UDTFirst.user_id = UDTSecond.user_id) and UDTFirst.user_id = UT.user_id order by UT.FIRST_NAME ,UT.LAST_NAME" ;
				objUsersData = DbFactory.GetDataSet( SecurityDatabase.Dsn, sSql );
				if(objUsersData!=null && objUsersData.Tables.Count!=0)
				{
					foreach(DataRow objRow in objUsersData.Tables[0].Rows)
					{  
					  XmlElement objUserElement =  CreateElement(p_objDocIn,"User");
					  SetAttribute(ref objUserElement,"loginname",Conversion.ConvertObjToStr(objRow["login_name"]));
    
                      string sUserFullName = Conversion.ConvertObjToStr(objRow["LAST_NAME"]) + " " + Conversion.ConvertObjToStr(objRow["FIRST_NAME"]);
					  string sPlainPassword = RMCryptography.DecryptString(Conversion.ConvertObjToStr(objRow["password"]));
				       
					  SetAttribute(ref objUserElement,"userid",Conversion.ConvertObjToStr(objRow["user_id"]));
					  SetAttribute(ref objUserElement,"UserFullName",sUserFullName);
                      p_objDocIn.SelectSingleNode("//Users").AppendChild(objUserElement);
					}
                  
				}
               
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
				if(objUsersData!=null)
				{
					objUsersData.Dispose();
					objUsersData = null;
				}
			}
		}

		private XmlElement CreateElement(XmlDocument p_objDoc,string p_sName)
		{
             return p_objDoc.CreateElement(p_sName);
		}
		private void SetAttribute(ref XmlElement p_objElement, string p_sName, string p_sValue)
		{
          p_objElement.SetAttribute(p_sName,p_sValue);
		}
		private void ValidateAsyncDoc(ref XmlDocument p_objDoc)
		{
			try
			{
				/*
				 * validate the document here and if an invalid doc is passed, throw the exception back to caller.
				 * */
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
		}
		/// <summary>
		/// Exception should not be thrown out of the following function, as this an independent thread, instead catch 
		/// the exception and update it in output xml(to be stored in database), polling function should take care of it. 
		/// </summary>
		public bool ExportUsersToOpenLdap( XmlDocument p_objInDoc,ref XmlDocument p_objOutDoc , AsyncWorkerThread p_objWorkerThread)
		{
			string[] arrUserIds = null;
			DomainUserHandler objDomainHandler = null;
			UserLogins objUserlogin = null;
			DbReader objReader = null;
			ArrayList arrlstNewLogins =new ArrayList();
			ArrayList arrlstExistingLogins =new ArrayList();
			ArrayList arrlstUploadFailures =new ArrayList();
			string sUserIds  = "" ,sUserTempIds ="" ; 
			DataSet objDataSet = null ;
            try
            {
                sUserTempIds = p_objInDoc.SelectSingleNode("//SelectedUserIds").InnerText.Trim();
                arrUserIds = sUserTempIds.Split(',');

                foreach (string sUserId in arrUserIds)
                {
                    if (sUserId.Trim() != "")
                    {

                        p_objWorkerThread.CountToBeExported++;
                        if (sUserIds == "")
                        {
                            sUserIds = sUserId;
                        }
                        else
                        {
                            sUserIds = sUserIds + "," + sUserId;
                        }
                    }
                }

                objDomainHandler = new DomainUserHandler();
                string sSql = "Select UDTFirst.user_id , UDTFirst.dsnid, UDTFirst.login_name,UDTFirst.password, UT.FIRST_NAME ,UT.LAST_NAME from User_Details_Table UDTFirst, User_Table UT where dsnid = (select max(dsnid) from User_Details_Table UDTSecond where UDTFirst.user_id = UDTSecond.user_id) and UDTFirst.user_id = UT.user_id and UDTFirst.user_id in ( " + sUserIds + " )";
                objDataSet = DbFactory.GetDataSet(SecurityDatabase.Dsn, sSql);

                if (objDataSet != null && objDataSet.Tables.Count != 0)
                {
                    foreach (DataRow objRow in objDataSet.Tables[0].Rows)
                    {
                        try
                        {
                            objUserlogin = new UserLogins();
                            objUserlogin.UserId = Conversion.ConvertObjToInt(objRow["user_id"], m_iClientId);
                            objUserlogin.LoginName = Conversion.ConvertObjToStr(objRow["login_name"]);
                            objUserlogin.Password = RMCryptography.DecryptString(Conversion.ConvertObjToStr(objRow["password"]));
                            objDomainHandler.AddUser(ref objUserlogin, arrlstNewLogins, arrlstExistingLogins);
                        }
                        catch (Exception p_objErr)
                        {
                            if (p_objErr.Message.ToLower().IndexOf("object") == -1)//user addition error after successfull connection to open ldap.
                            {
                                this.ExceptionProcessor(ref p_objErr, "2", p_objWorkerThread, ref p_objInDoc);
                                p_objOutDoc = p_objInDoc;
                                return true;
                            }
                            Exception objException = null;
                            PublicFunctions.WorkerForException(p_objErr, out objException);
                            UploadFailure stFailed = new UploadFailure();
                            stFailed.LoginName = objUserlogin.LoginName;
                            stFailed.Reason = p_objErr.Message;

                            arrlstUploadFailures.Add(stFailed);
                        }
                        finally
                        {
                            if (objDataSet != null)
                            {
                                objDataSet.Dispose();
                                objDataSet = null;
                            }
                        }
                        p_objWorkerThread.ProcessedUsersCount++;
                        /*percentage completion block...*/
                        if (p_objWorkerThread.ProcessedUsersCount % 5 == 0) //update after 5 insertions
                        {
                            p_objWorkerThread.UpdatePercentageCompletion();
                        }
                    }

                }
                foreach (UploadFailure sFailLogin in arrlstUploadFailures)
                {
                    XmlElement objFailedLogin = CreateElement(p_objInDoc, "FailedUser");
                    objFailedLogin.SetAttribute("LoginName", sFailLogin.LoginName);
                    objFailedLogin.SetAttribute("Reason", sFailLogin.Reason);
                    p_objInDoc.SelectSingleNode("//UserExportFailuers").AppendChild(objFailedLogin);
                    p_objInDoc.SelectSingleNode("//UserExportFailuers").Attributes["FailedForSomeUsers"].Value = "1";

                }
                p_objInDoc.SelectSingleNode("//UIParams//ExportStatus").InnerText = "1";
                p_objInDoc.SelectSingleNode("//UIParams//Message").InnerText = Globalization.GetString("OpenLdapExporter.ExportUsersToOpenLdap.SuccessMessage");
            }
            catch (Exception p_objErr)
            {
                try
                {
                    this.ExceptionProcessor(ref p_objErr, "3", p_objWorkerThread, ref p_objInDoc);
                    p_objOutDoc = p_objInDoc;
                }
                catch (Exception p_objFatal)
                {
                    PublicFunctions.LoggerForWorkerThreads(p_objFatal);
                }

            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                    objReader = null;
                }
                if(objDomainHandler != null) 
                {
                    objDomainHandler.Dispose();
                    objDomainHandler = null;
                }
                objUserlogin = null;                
                arrlstNewLogins = null;
                arrlstExistingLogins = null;
                arrlstUploadFailures = null;
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }                
            }
			p_objOutDoc = p_objInDoc ;
		    return true ;
			}
		private void GetLoginsToBeUploaded()
		{
			try
			{
			}
			catch( Exception p_objErr )
			{
				throw p_objErr ;
			}
			finally
			{
			}
		}
		public void ExceptionProcessor(ref Exception p_objErr, string p_sExportStatus, AsyncWorkerThread p_objWorker,ref XmlDocument p_objDoc)
		{
			try
			{
				Exception objActualErr = null;
				PublicFunctions.WorkerForException(p_objErr,out objActualErr);
				p_objDoc.SelectSingleNode("//UIParams//ExportStatus").InnerText = p_sExportStatus ;
				if(p_sExportStatus=="2")
				{
					/*In case of open ldap error, lets provide user with some info about it.
					 * */
					p_objDoc.SelectSingleNode("//UIParams//Message").InnerText = objActualErr.Message+":"+"Check Open Ldap server-"+ RMConfigurator.NamedNode("SSOSettings//ServerPath").InnerText ;
				}
				else
				{
					p_objDoc.SelectSingleNode("//UIParams//Message").InnerText= objActualErr.Message;
				}
				p_objWorker.UpdateStatus(2);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
		}
	}
	internal struct UploadFailure
	{
		public string LoginName;
		public string Reason;
	}
	/// <summary>
	/// SSO authentication implementation.
	/// </summary>
	public class DomainUserHandler:IDisposable
	{
		/// <summary>
		/// Variable to Server path.
		/// </summary>
		private string m_sServerPath;
		/// <summary>
		/// Variable to hold user name.
		/// </summary>
		private string m_sUserName;
		/// <summary>
		/// Variable to hold password.
		/// </summary>
		private string m_sPassword;
		/// <summary>
		/// Variable to hold Users node.
		/// </summary>
		private DirectoryEntry m_objADUsersContext;
		
		/// <summary>
		/// Default constructor; it initializes this class.
		/// </summary>
		public DomainUserHandler()
		{
			this.Init();
		}
		/// <summary>
		/// This method reads various parameters and helps to initialize this class.
		/// </summary>
		private void Init()
		{
			string sADSIPath;
			int iADSIProvider;
			int iADType;
			try
			{
				iADSIProvider=Conversion.ConvertStrToInteger(RMConfigurator.NamedNode("SSOSettings/ADSIProvider").InnerText);
				ADSIProviders enumADSIProvider=(ADSIProviders)iADSIProvider;
				iADType=Conversion.ConvertStrToInteger(RMConfigurator.NamedNode("SSOSettings/ADType").InnerText);
				ADType enumADType=(ADType)iADType;
                this.m_sServerPath= RMConfigurator.NamedNode("SSOSettings/ServerPath").InnerText;
				this.m_sUserName= RMConfigurator.NamedNode("SSOSettings/ServerPath").Attributes["UserName"].Value;
				this.m_sPassword= RMConfigurator.NamedNode("SSOSettings/ServerPath").Attributes["Password"].Value;
				sADSIPath=enumADSIProvider.ToString()+"://"+m_sServerPath+"/"+RMConfigurator.NamedNode("DNForUsersNode").InnerText;
				this.m_objADUsersContext=new DirectoryEntry(sADSIPath,m_sUserName,m_sPassword);
				switch(enumADSIProvider)
				{
					case ADSIProviders.LDAP:
					{
						switch(enumADType)
						{
							case ADType.OpenLDAP:
							{
								m_objADUsersContext.AuthenticationType=AuthenticationTypes.ServerBind;
								break;
							}
							case ADType.ADOrADAMWindowsSpecific:
							{
								m_objADUsersContext.AuthenticationType=AuthenticationTypes.Secure;
								break;
							}
							default:
							{
								throw new RMAppException("Directory type not supported.");
							}

						}
						break;
					}
					case ADSIProviders.WINNT:
					{
						/*Code to support WINNT type of ADSI provider.
						 * */
						break;
					}
					case ADSIProviders.IIS:
					{
						/*Code to support IIS type of ADSI provider.
						 * */
						break;
					}
					default:
					{
						throw new RMAppException(Globalization.GetString("DomainUserHandler.Init.UnknownADSI"));
					}
				}

			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
		}
		/// <summary>
		/// This method adds an user to AD.
		/// </summary>
		/// <param name="p_objUserLogin">User login object.</param>
		public void AddUser(ref UserLogins p_objUserLogin)
		{
			string sName;
			DirectoryEntry objADUser=null;
			try
			{
				//Multiple threads can access the AD; so placing a lock on it.
				lock(this)
				{
					if(!UserIDExistsInAD(ref p_objUserLogin,ref objADUser))
					{
						sName=RMConfigurator.NamedNode("SSOSettings/RDNPrefixForAddingUser").InnerText+"="+p_objUserLogin.LoginName;
						objADUser=m_objADUsersContext.Children.Add(sName,"jetspeeduser");
						SetAttributesOnADUser(ref objADUser,ref p_objUserLogin);
					}
					else
					{
						ModifyUser(ref objADUser,ref p_objUserLogin);
					}
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
                if(objADUser!=null)
                {
                    objADUser.Close();
                    objADUser.Dispose();
                    objADUser = null;
                }
			}
		}
		public void AddUser(ref UserLogins p_objUserLogin, ArrayList p_arrlstNewLogins,  ArrayList p_arrlstExistingLogins)
		{
			string sName;
			DirectoryEntry objADUser=null;
			try
			{
				//Multiple threads can access the AD; so placing a lock on it.
				lock(this)
				{
					if(!UserIDExistsInAD(ref p_objUserLogin,ref objADUser))
					{
						sName=RMConfigurator.NamedNode("SSOSettings/RDNPrefixForAddingUser").InnerText+"="+p_objUserLogin.LoginName;
						objADUser=m_objADUsersContext.Children.Add(sName,"jetspeeduser");
						SetAttributesOnADUser(ref objADUser,ref p_objUserLogin);
                        p_arrlstNewLogins.Add(p_objUserLogin.LoginName);
					}
					else
					{
						ModifyUser(ref objADUser,ref p_objUserLogin);
						p_arrlstExistingLogins.Add(p_objUserLogin.LoginName);
					}
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
            finally
            {
                if (objADUser != null)
                {
                    objADUser.Close();
                    objADUser.Dispose();
                    objADUser = null;
                }
            }
		}
		/// <summary>
		/// This function modifies an User.
		/// </summary>
		/// <param name="p_objExistingADUser">Reference to a Directory entry.</param>
		/// <param name="p_objUserLogin">User login object.</param>
		public void ModifyUser(ref DirectoryEntry p_objExistingADUser, ref UserLogins p_objUserLogin)
		{
			try
			{
				SetAttributesOnADUser(ref p_objExistingADUser,ref p_objUserLogin);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
		}
		/// <summary>
		/// This function deletes an User from AD.
		/// </summary>
		/// <param name="p_objUserLogin">User login object.</param>
		public void DeleteUser(ref UserLogins p_objUserLogin)
		{
			DirectoryEntry objUserToDel=null;
			try
			{
				//Multiple threads can access the AD; so placing a lock on it.
				lock(this)
				{
					if(UserIDExistsInAD(ref p_objUserLogin,ref objUserToDel))
					{
						m_objADUsersContext.Children.Remove(objUserToDel);
					}
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
                if (objUserToDel != null)
                {
                    objUserToDel.Close();
                    objUserToDel.Dispose();
                    objUserToDel = null;
                }
			}
		}
		/// <summary>
		/// This function encypt password.
		/// </summary>
		/// <param name="p_sPassword">Password to be encrypted.</param>
		/// <returns>Encrypted password.</returns>
		public string EncryptedPlainTextPassword(string p_sPassword)
		{
			System.Text.UTF8Encoding objUTFEncoding;
			System.Security.Cryptography.SHA1Managed objSHA1Managed;
			try
			{
				/*Check for Null/Empty string.
				 * UTF8Encoding.GetBytes() throws error if called with null values.
				 */
				 
				if(p_sPassword==null || p_sPassword.Trim()=="")
				{
					throw new InvalidValueException(Globalization.GetString("DomainUserHandler.EncryptedPlainTextPassword.InvalidValue"));
				}
				objUTFEncoding=new System.Text.UTF8Encoding();
				objSHA1Managed=new System.Security.Cryptography.SHA1Managed();
				return Convert.ToBase64String(objSHA1Managed.ComputeHash(objUTFEncoding.GetBytes(p_sPassword)));
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				objUTFEncoding=null;
				objSHA1Managed=null;

			}
		}
		/// <summary>
		/// This function tests whether an User exists in AD or not.
		/// </summary>
		/// <param name="p_objUserLogin">User login object.</param>
		/// <param name="p_objExistingUser">Reference to Directory entry.</param>
		/// <returns>true or false.</returns>
		public bool UserExistsInAD(ref UserLogins p_objUserLogin,ref DirectoryEntry p_objExistingUser)
		{
		    bool bRetVal=false;
			string sLoginNameInAD;
			try
			{
				foreach(DirectoryEntry objUser in m_objADUsersContext.Children)
				{
				  sLoginNameInAD=objUser.Properties["uid"].Value.ToString().Substring((objUser.Properties["uid"].Value.ToString().IndexOf("=")+1)).Trim().ToLower();
					if(sLoginNameInAD==p_objUserLogin.LoginName.Trim().ToLower())
					{
						bRetVal=true;
						p_objExistingUser = objUser;
						break;
					}
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
			return bRetVal;
		}
		/// <summary>
		///  This function tests whether an User exists in AD or not.
		/// </summary>
		/// <param name="p_objUserLogin">User login object.</param>
		/// <param name="p_objExistingUser">Reference to Directory entry.</param>
		/// <returns>true or false.</returns>
		public bool UserIDExistsInAD(ref UserLogins p_objUserLogin,ref DirectoryEntry p_objExistingUser)
		{
			bool bRetVal=false;
			int iUidInAD;
			string sRMUserType; 
			string sName;
			try
			{
				sRMUserType=RMConfigurator.NamedNode("SSOSettings/SnADUser").InnerText;
				foreach(DirectoryEntry objUser in m_objADUsersContext.Children)
				{
					iUidInAD=Conversion.ConvertObjToInt(objUser.Properties["uidNumber"], m_iClientId.Value);
					if(iUidInAD==p_objUserLogin.objUser.UserId && (sRMUserType== Conversion.ConvertObjToStr(objUser.Properties["sn"].Value)))
					{
						bRetVal=true;
						sName=RMConfigurator.NamedNode("SSOSettings/RDNPrefixForAddingUser").InnerText+"="+p_objUserLogin.LoginName;
						m_objADUsersContext.Children.Remove(objUser);
                        p_objExistingUser=m_objADUsersContext.Children.Add(sName,"jetspeeduser");
						break;
					}
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
			return bRetVal;
		}
		public void ModifySMSAdminPassword(string p_sUserName, string p_sPassword)
		{
			string sName;
			DirectoryEntry objADSMSAdminUser=null;
			try
			{
				p_sUserName = RMConfigurator.NamedNode("SSOSettings/RDNPrefixForAddingUser").InnerText+"="+p_sUserName;
				
				//Multiple threads can access the AD; so placing a lock on it.
				lock(this)
				{
					
					objADSMSAdminUser = m_objADUsersContext.Children.Find(p_sUserName) ;
					objADSMSAdminUser.Properties["userPassword"].Value="{SHA}"+this.EncryptedPlainTextPassword(p_sPassword);
					objADSMSAdminUser.CommitChanges();
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
                if (objADSMSAdminUser != null)
                {
                    objADSMSAdminUser.Close();
                    objADSMSAdminUser.Dispose();
                    objADSMSAdminUser = null;
                }
			}
		}
		/// <summary>
		/// This function sets various properties for Directory entry object.
		/// </summary>
		/// <param name="p_objADUser">Dirctory entry</param>
		/// <param name="p_objUserLogin">User login object.</param>
		private void SetAttributesOnADUser(ref DirectoryEntry p_objADUser,ref UserLogins p_objUserLogin)
		{
			/*Read Template user and set various default properties.*/
			UserLogins objUserLogin=new UserLogins();
			DirectoryEntry objRMTemplateUser=null;
			try
			{
				//Create a proxy userlogin object for template user.
				objUserLogin.LoginName=RMConfigurator.NamedNode("UidForRMTemplateUser").InnerText.Trim();

				if(!UserExistsInAD(ref objUserLogin,ref objRMTemplateUser))
				{
					throw new RMAppException(Globalization.GetString("DomainUserHandler.SetAttributesOnADUser.UserErr"));
				}
				p_objADUser.Properties["confirm"].Value=objRMTemplateUser.Properties["confirm"].Value;
				p_objADUser.Properties["creationdate"].Value=Conversion.ToDbDateTime(DateTime.Now);
				p_objADUser.Properties["name"].Value=p_objUserLogin.LoginName;
				p_objADUser.Properties["givenName"].Value=p_objUserLogin.objUser.FirstName+ " " + p_objUserLogin.objUser.LastName;
				p_objADUser.Properties["sn"].Value=RMConfigurator.NamedNode("SSOSettings/SnADUser").InnerText;
				p_objADUser.Properties["userPassword"].Value="{SHA}"+this.EncryptedPlainTextPassword(p_objUserLogin.Password);
				p_objADUser.Properties["disabled"].Value=objRMTemplateUser.Properties["disabled"].Value;
				if(p_objUserLogin.objUser.Email==null || p_objUserLogin.objUser.Email.Trim()=="")
				{
                  p_objADUser.Properties["mail"].Value=objRMTemplateUser.Properties["mail"].Value;
				}
				else
				{
					p_objADUser.Properties["mail"].Value=p_objUserLogin.objUser.Email;
				}
				p_objADUser.Properties["objectData"].Value="";
				p_objADUser.Properties["lastmodifieddate"].Value=Conversion.ToDbDateTime(DateTime.Now);
				p_objADUser.Properties["objectClass"].Value=objRMTemplateUser.Properties["objectClass"].Value;
				p_objADUser.Properties["uid"].Value=p_objUserLogin.LoginName;
				p_objADUser.Properties["lastlogindate"].Value=Conversion.ToDbDateTime(DateTime.Now);
				p_objADUser.Properties["usergrouprole"].Value=objRMTemplateUser.Properties["usergrouprole"].Value;
				p_objADUser.Properties["uidNumber"].Value=p_objUserLogin.UserId;

				p_objADUser.CommitChanges();
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
               objUserLogin=null;
               if (objRMTemplateUser != null)
               {
                   objRMTemplateUser.Close();
                   objRMTemplateUser.Dispose();
                   objRMTemplateUser = null;
               }
			}

		}
		/// <summary>
		/// This function parses an object for User id value.
		/// </summary>
		/// <param name="p_sObjectDataValue">Object to be parsed.</param>
		/// <returns>User id.</returns>
		public int ParseObjectDataForUserId(string p_sObjectDataValue)
		{
			int iIndex;
			if(p_sObjectDataValue.IndexOf("RMUser")!=-1)
			{
				iIndex=p_sObjectDataValue.IndexOf(",");
				return Conversion.ConvertStrToInteger(p_sObjectDataValue.Substring(iIndex+1));
			}
			else
			{
				return 0;
			}
		}
		
/// <summary>
/// This function disposes this class. 
/// </summary>
		public void Dispose()
		{
		   m_sServerPath=null;
		   m_sUserName=null;
		   m_sPassword=null;
           if (m_objADUsersContext != null)
           {
               m_objADUsersContext.Close();
               m_objADUsersContext.Dispose();
               m_objADUsersContext = null;
           }
		}
 
		
	}
	/// <summary>
	/// This class controls all AD related functions and invoked by Worker threads.
	/// </summary>
	public class ADController
	{
		/// <summary>
		/// This variable holds the User Login object.
		/// </summary>
		private UserLogins m_objUserLogin;
		/// <summary>
		/// This constructor initializes this class.
		/// </summary>
		/// <param name="p_objUserLogin">User login object.</param>
		public ADController(ref UserLogins p_objUserLogin)
		{
          m_objUserLogin=p_objUserLogin;
		  Thread.CurrentThread.Priority=ThreadPriority.AboveNormal;
		}
		/// <summary>
		/// This function acts as an wrapper over AddUser() function of DomainUserHandler class.
		/// </summary>
		public void SaveUserToAD()
		{
			DomainUserHandler objDm=null;
			try
			{
				objDm = new DomainUserHandler();
				objDm.AddUser(ref m_objUserLogin);
				
			}
			catch(Exception p_objException)
			{
				/*As this function executes in a Worker thread which is independent of Main thread; error should not be 
				 * thrown as this will be considered as Unhandled exception. So try logging maximum possible details about the exception 
				 * and terminate the program.
				 * */
				PublicFunctions.LoggerForWorkerThreads(p_objException);
			}
			finally
			{
				if(objDm!=null)
					{
						objDm.Dispose();
						objDm=null;
					}
				/*This will abort/kill the worker thread,
				 * dont put any code after the following command as no execution can take place afterwards*/
				this.Abort();
				
			}
		}
		/// <summary>
		/// This function aborts the current thread.
		/// </summary>
		private void Abort()
		{
			try
			{
				Thread.CurrentThread.Abort();
			}
			catch(Exception p_objException)
			{
				/*Eat up this exception as thread is being aborted forcefully.
				 * */
				if(!(p_objException is ThreadAbortException))
				{
					throw p_objException;
				}
				
			}
		}
		/// <summary>
		/// This function acts as wrapper over Delete function of DomainUserHandler class.
		/// </summary>
		public void DeleteUserFromAD()
		{
			DomainUserHandler objDm=null;
			try
			{
				objDm = new DomainUserHandler();
				objDm.DeleteUser(ref m_objUserLogin);
				
			}
			catch(Exception p_objException)
			{
				/*As this function executes in a Worker thread which is independent of Main thread; error should not be 
				 * thrown as this will be considered as Unhandled exception. So try logging maximum possible details about the exception 
				 * and terminate the program.
				 * */
				Exception objInnerMost;
				PublicFunctions.WorkerForException(p_objException,out objInnerMost);
				LogItem objEr=new LogItem();
				objEr.Message=Globalization.GetString("ADController.DeleteUserFromAD.DeleteADErr");
				objEr.RMExceptSource=p_objException.Source;
				objEr.RMExceptStack=p_objException.StackTrace;
				objEr.RMParamList.Add("Actual Exception",objInnerMost.Message);	
				Log.Write(objEr);
			}
			finally
			{
				if(objDm!=null)
				{
					objDm.Dispose();
					objDm=null;
				}
				/*This will abort/kill the worker thread,
				 * dont put any code after the following command as no execution can take place afterwards*/
				this.Abort();
			}
		}
	}
	/// <summary>
	/// This enum is for ADSI providers and have mappings in .Config file.
	/// </summary>
	public enum ADSIProviders:int
	{
		LDAP=1,
		WINNT=2,
		IIS=3
	}
	/// <summary>
	/// This enum is for AD type and have mappings in .Config file. 
	/// </summary>
	public enum ADType:int
	{
		OpenLDAP=1,
		ADOrADAMWindowsSpecific=2
	}
	
}
