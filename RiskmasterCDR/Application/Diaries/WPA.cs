﻿using System;
using System.Xml;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.Text;
using Riskmaster.Settings;
using Riskmaster.Application.SecurityManagement;
using Riskmaster.Security;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using C1.C1PrintDocument;
using System.Linq;
using System.Web;
using System.Globalization;
using Riskmaster.Models;

namespace Riskmaster.Application.Diaries
{
    /// <summary>
    ///Author  :   Sumeet Rathod & Parag Sarin
    ///Dated   :   04th October 2004
    ///Purpose :   This class Creates, Edits, Fetches & Voids the Diary information.
    /// </summary>
    public class WPA
    {
        #region Constants declaration
        /// <summary>
        /// Stores user image path
        /// </summary>
        private const string IMG_USER = "/RiskmasterUI/Images/user_task.png";

        /// <summary>
        /// Stores user overdue image path
        /// </summary>
        private const string IMG_USER_OVERDUE = "/RiskmasterUI/Images/user_task_overdue.png";

        /// <summary>
        /// Alternate text for user image
        /// </summary>
        private const string ALT_USER = "My Diary";

        /// <summary>
        /// Stores users image path
        /// </summary>
        private const string IMG_USERS = "/RiskmasterUI/Images/differentuser_generated.png";

        /// <summary>
        /// Stores users overdue image path
        /// </summary>
        private const string IMG_USERS_OVERDUE = "/RiskmasterUI/Images/differentuser_generated_overdue.png";

        /// <summary>
        /// Stores alternate text for users image
        /// </summary>
        private const string ALT_USERS = "Assigned by other users";

        /// <summary>
        /// Stores system image path
        /// </summary>
        private const string IMG_SYSTEM = "/RiskmasterUI/Images/autodiary_generated.png";

        /// <summary>
        /// Stores system overdue image path
        /// </summary>
        private const string IMG_SYSTEM_OVERDUE = "/RiskmasterUI/Images/autodiary_generated_overdue.png";

        /// <summary>
        /// Stores alternate text for system image
        /// </summary>
        private const string ALT_SYSTEM = "System Assigned";

        /// <summary>
        /// Disable Mail options
        /// </summary>
       // public const string DISABLE_MAIL = "MailIntegration"; // rrachev JIRA 4607 change to public
        public const string ENABLE_MAIL = "MailIntegration";//dvatsa JIRA-11627
        /// <summary>
        /// Auto Diary option
        /// </summary>
        private const string AUTO_DIARY = "Diaries_AutoDiary";

        /// <summary>
        /// To Do list option
        /// </summary>
        private const string LOAD_TO_DO = "Diaries_LoadToDo";

        /// <summary>
        /// Mail checking option
        /// </summary>
        private const string MAIL_CHECKING = "Diaries_MailChecking";

        /// <summary>
        /// Constant representing CLAIM
        /// </summary>
        private const string CLAIM = "CLAIM";

        /// <summary>
        /// Constant representing EVENT
        /// </summary>
        private const string EVENT = "EVENT";

        /// <summary>
        /// Constant representing ENTITY
        /// </summary>
        private const string ENTITY = "ENTITY";

        /// <summary>
        /// Constant representing CLAIMANT
        /// </summary>
        private const string CLAIMANT = "CLAIMANT";

        /// <summary>
        /// Constant representing PERSON_INVOLVED
        /// </summary>
        private const string PERSON_INVOLVED = "PERSON_INVOLVED";

        //abisht MITS 10899
        /// <summary>
        /// Constant representing POLICY_ENH
        /// </summary>

        private const string POLICY_ENH = "POLICYENH";
        //abisht MITS 10899
        /// <summary>
        /// Constant representing POLICY
        /// </summary>
        private const string POLICY = "POLICY";

        // Mits 33843
        private const string SUBROGATION = "SUBROGATION";
        //rupal:start r8 auto diary enh
        /// <summary>
        /// Constant representing CLAIM_X_LITIGATION
        /// </summary>
        private const string CLAIM_LITIGATION = "CLAIM_X_LITIGATION";
        private const string LITIGATION = "LITIGATION";
        /// <summary>

        /// <summary>
        /// Constant representing RESERVE_HISTORY
        /// </summary>
        private const string RESERVE_HISTORY = "RESERVE_HISTORY";

        /// <summary>
        /// Constant representing Case Management
        /// </summary>
        private const string CASE_MANAGEMENT = "CM_X_TREATMENT_PLN";
        private const string CMXTREATMENTPLN = "CMXTREATMENTPLN";

        /// <summary>
        /// Constant representing Scheduled Activity For Subrogation
        /// </summary>
        private const string SCHEDULED_ACTIVITY = "SCHEDULED_ACTIVITY";

        /// <summary>
        /// Constant representing RESERVE_CURRENT
        /// </summary>
        private const string RESERVE_CURRENT = "RESERVE_CURRENT";

        /// <summary>
        /// Constant representing FUND_AUTO_BATCH
        /// </summary>
        private const string FUND_AUTO_BATCH = "FUNDS_AUTO_BATCH";
        private const string AUTO_CLAIM_CHECKS = "AUTOCLAIMCHECKS";

        /// <summary>
        /// Constant representing COMMENTS_TEXT
        /// </summary>
        private const string COMMENTS_TEXT = "COMMENTS_TEXT";

        /// <summary>
        /// Constant representing claim Progress Notes
        /// </summary>
        private const string CLAIM_PRG_NOTE = "CLAIM_PRG_NOTE";
        //rupal:end
        
        /// <summary>
        /// Constant representing DEPENDENT
        /// </summary>
        private const string PIDEPENDENT = "PIDEPENDENT";
        //Added by sharishkumar for mits 35291

        private const string TM_JOB_LOG = "TM_JOB_LOG";
        //Code merged by Nitin from Safeway to RMX R6 release on 22-July-2009 starts
        //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config
        /// <summary>
        /// Object to store Diary Configurations
        /// </summary>
        private DiaryConfig m_Diaryconfig;
        //Code merged by Nitin from Safeway to RMX R6 release on 22-July-2009 ends

        //Added by Nitin on 28-Feb-2009 for R5-R4 merging of Mits 10641
        //03/03/2008 Asif:MITS 10641 start 
        /// <summary>
        /// Left alignment
        /// </summary>
        private const string LEFT_ALIGN = "1";
        /// <summary>
        /// Right alignment
        /// </summary>
        private const string RIGHT_ALIGN = "2";
        /// <summary>
        /// Center alignment
        /// </summary>
        private const string CENTER_ALIGN = "3";
        /// <summary>
        /// Number of rows in a report page
        /// </summary>
        //private const int NO_OF_ROWS = 54;
        //MITS 10641 End
        //Ended by Nitin  on 28-Feb-2009 for R5-R4 merging of Mits 10641
        private const int NO_OF_ROWS = 40;//Mits 14020 Asif

        private const string FUNDS = "FUNDS";
        #endregion

        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_sConnectionString = "";

        /// <summary>
        /// The connection string used by the database layer for connection with the security database.
        /// </summary>
        private string m_sSecConnectString = "";

        /// <summary>
        /// Represents the User Id.
        /// </summary>
        private long m_lUserId = 0;

        //03/08/2008 Asif MITS:10641 START
        private C1PrintDocument m_objPrintDoc = null;
        /// <summary>
        /// CurrentX
        /// </summary>
        private double m_dblCurrentX = 0;
        /// <summary>
        /// CurrentY
        /// </summary>
        private double m_dblCurrentY = 0;
        /// <summary>
        /// Font object.
        /// </summary>
        private Font m_objFont = null;
        /// <summary>
        /// Text object.
        /// </summary>
        private RenderText m_objText = null;
        /// <summary>
        /// PageWidth
        /// </summary>
        private double m_dblPageWidth = 0.0;
        /// <summary>
        /// PageHeight
        /// </summary>
        private double m_dblPageHeight = 0.0;
        /// <summary>
        /// ClientWidth
        /// </summary>
        private double m_dblClientWidth = 0.0;
        private int m_intColumns = 7;
        //End Asif MITS 10641



        /// <summary>
        /// Represents the DSN Id.
        /// </summary>
        private long m_lDSNId = 0;

        /// <summary>
        /// The Group id 
        /// </summary>
        private int m_iGroupId = 0;

        //pkandhari Jira 6412 starts
        /// <summary>
        /// The Group Name 
        /// </summary>
        private string m_sGroupName = string.Empty;
        //pkandhari Jira 6412 ends
        /// <summary>
        /// The Page Size
        /// </summary>
        private long m_lPageSize = 0;

        /// <summary>
        /// User Login Name
        /// </summary>
        private string m_sLoginName = string.Empty;

        /// <summary>
        /// User Password
        /// </summary>
        private string m_sPassword = string.Empty;

        /// <summary>
        /// User Data Source Name
        /// </summary>
        private string m_sDSNName = string.Empty;
        private Hashtable m_objConfig = null;
        private Hashtable m_DiariesConfigs = null;
        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database. //Added By Neha Mits 23586
        /// </summary>
        private static string m_sDBType = "";
        //Aman ML Change added the language data member
        ///<summary>
        ///Langugae code for the user
        ///</summary>
        private int m_iLangCode = 0;
        /// <summary>
        /// ClientId variable for cloud
        /// </summary>
        private int m_iClientId = 0;
        /// <summary>
        /// Enum for display order (order by clause)
        /// </summary>
        public enum DIARYSORTORDER : int
        {
            NONE,
            ASC_COMPLETE_DATE,
            ASC_ATTACH_TABLE,
            ASC_ENTRY_NAME,
            ASC_PRIORITY,
            DEC_COMPLETE_DATE,
            DEC_ATTACH_TABLE,
            DEC_ENTRY_NAME,
            DEC_PRIORITY,
            ASC_WORK_ACTIVITY,      //Geeta 11/17/06 : Added to fix mits number 8139 for Work Activity
            ASC_CLAIMANT,//Merged by Nitin for R6 Diary List - sorting
            DEC_WORK_ACTIVITY = 13,
            DEC_CLAIMANT, //Merged by Nitin for R6 Diary List - sorting
            ASC_DEPARTMENT, //hlv MITS 29594 11/6/12  15
            DEC_DEPARTMENT,  //hlv MITS 29594 11/6/12  16
            ASC_CLAIMSTATUS, //hlv MITS 29594 11/6/12  17
            DEC_CLAIMSTATUS, //hlv MITS 29594 11/6/12  18
            ASC_ASSIGNED_USER,   //hlv MITS 29594 11/6/12  19
            DEC_ASSIGNED_USER,   //hlv MITS 29594 11/6/12  20
            ASC_ASSIGNING_USER,  //hlv MITS 29594 11/6/12  21
            DEC_ASSIGNING_USER,  //hlv MITS 29594 11/6/12  22
            //MITS 36519
            ASC_PARENTRECORD,  
            DEC_PARENTRECORD ,
            //igupta3 jira 439
            ASC_NOTROUTE,
            DEC_NOTROUTE,
            ASC_NOTROLL,
            DEC_NOTROLL
        }
        //Indu - Mits 33843 Diary enhancement 
        public enum PARENTEVENT : int
        {
            PERSON_INVOLVED,
            EVENTDATEDTEXT,
            PIPHYSICIAN,
            PIMEDSTAFF,
            PIEMPLOYEE,
            PIOTHER,
            PIPATIENT,
            PIDRIVER,
            PIWITNESS,
            PIDEPENDENT,
            OSHA,
            FALLINFO,
            MEDWATCHTEST,
            CONCOMITANT,
            //zmohammad : Merging 37022 into CDR
            // akaushik5 Added for MITS 37022 Starts
            CMXCMGRHIST,
            // akaushik5 Added for MITS 37022 Ends
            MEDWATCH,
            CASEMGRNOTES, //RMA 1598
            CMXACCOMMODATION,
            CMXVOCREHAB,
            CASEMANAGEMENT,
            CMXTREATMENTPLN
        }
        public enum PARENTCLAIM : int
        {
            CLAIM,
            SUBROGATION,		
            CLAIMANT,
            LITIGATION,
            FUNDS_AUTO_BATCH,	
            AUTOCLAIMCHECKS,		
            //CMXTREATMENTPLN,		
            ADJUSTER,		
            CMXMEDMGTSAVINGS,	
            LEAVE,			
            UNIT,			
            PROPERTYLOSS,		
            //rma 1598
            //CMXACCOMMODATION,	
            //CMXVOCREHAB,
            PIRESTRICTION,
            PIWORKLOSS,
            EXPERT,
            SALVAGE,
            DEFENDANT,
            //zmohammad : Merging 37022 into CDR
            // akaushik5 Commented for MITS 37022 Starts
            //CMXCMGRHIST,
            // akaushik5 Commented for MITS 37022 Ends
            SCHEDULED_ACTIVITY,
            CLAIM_X_LITIGATION,
            AUTO_CLAIM_CHECKS,
            CM_X_TREATMENT_PLN,
            FUNDS,
            RESERVE_HISTORY,
            RESERVE_CURRENT,
            ADJUSTERDATEDTEXT//RMA1598
        }
        //public enum PARENTLITIGATION : int
        //{
        //    EXPERT
        //}
        public enum PARENTPOLICY : int
        {
            POLICYCOVERAGE,
            POLICY_ENH,
            POLICYBILLING,
        }
        

        //Code merged by Nitin from Safeway to RMX R6 release on 22-July-2009 starts

        //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config
        /// <summary>
        /// This structure would contain what all information regarding the Diary Config needs to be displayed.
        /// </summary>
        internal struct DiaryConfig
        {
            private bool m_Priority;
            private bool m_Text;
            private bool m_WorkActivity;
            private bool m_Department;
            //igupta3 jira 439
            private bool m_NotRoutable;
            private bool m_NotRollable;
            private bool m_Due;
            private bool m_AttachedRecord;
            //Indu-Mits33843
            private bool m_ParentRecord;
            private bool m_ClaimStatus;
            private bool m_Claimant;
            private bool m_AssignedUser; //Added by Amitosh for Diary UI changes
            private bool m_AssigningUser; //Added by Amitosh for Diary UI changes
            private string m_PriorityHeader;
            private string m_TextHeader;
            private string m_WorkActivityHeader;
            //igupta3 jira 439
            private string m_NotRoutableHeader;
            private string m_NotRollableHeader;
            private string m_DepartmentHeader;
            private string m_DueHeader;
            private string m_AttachedRecordHeader;
            private string m_ClaimStatusHeader;
            private string m_ClaimantHeader;
            private string m_AssignedUserHeader; //Added by Amitosh for Diary UI changes
            private string m_AssigningUserHeader; //Added by Amitosh for Diary UI changes
            internal bool Priority { get { return m_Priority; } set { m_Priority = value; } }
            internal bool Text { get { return m_Text; } set { m_Text = value; } }
            internal bool WorkActivity { get { return m_WorkActivity; } set { m_WorkActivity = value; } }
            //igupta3 jira 439
            internal bool NotRoutable { get { return m_NotRoutable; } set { m_NotRoutable = value; } }
            internal bool NotRollable { get { return m_NotRollable; } set { m_NotRollable = value; } }
            internal bool Department { get { return m_Department; } set { m_Department = value; } }
            internal bool Due { get { return m_Due; } set { m_Due = value; } }
            internal bool AttachedRecord { get { return m_AttachedRecord; } set { m_AttachedRecord = value; } }
            //Indu - Mits 33843
            internal bool ParentRecord { get { return m_ParentRecord; } set { m_ParentRecord = value; } }

            internal bool ClaimStatus { get { return m_ClaimStatus; } set { m_ClaimStatus = value; } }
            internal bool Claimant { get { return m_Claimant; } set { m_Claimant = value; } }
            internal string PriorityHeader { get { return m_PriorityHeader; } set { m_PriorityHeader = value; } }
            internal string TextHeader { get { return m_TextHeader; } set { m_TextHeader = value; } }
            internal string WorkActivityHeader { get { return m_WorkActivityHeader; } set { m_WorkActivityHeader = value; } }
            //igupta3 jira 439
            internal string NotRoutableHeader { get { return m_NotRoutableHeader; } set { m_NotRoutableHeader = value; } }
            internal string NotRollableHeader { get { return m_NotRollableHeader; } set { m_NotRollableHeader = value; } }

            internal string DepartmentHeader { get { return m_DepartmentHeader; } set { m_DepartmentHeader = value; } }
            internal string DueHeader { get { return m_DueHeader; } set { m_DueHeader = value; } }
            internal string AttachedRecordHeader { get { return m_AttachedRecordHeader; } set { m_AttachedRecordHeader = value; } }
            internal string ClaimStatusHeader { get { return m_ClaimStatusHeader; } set { m_ClaimStatusHeader = value; } }
            internal string ClaimantHeader { get { return m_ClaimantHeader; } set { m_ClaimantHeader = value; } }

            //Added by Amitosh for Diary UI changes
            internal bool AssignedUser { get { return m_AssignedUser; } set { m_AssignedUser = value; } }
            internal bool AssigningUser { get { return m_AssigningUser; } set { m_AssigningUser = value; } }
            internal string AssignedUserHeader { get { return m_AssignedUserHeader; } set { m_AssignedUserHeader = value; } }
            internal string AssigningUserHeader { get { return m_AssigningUserHeader; } set { m_AssigningUserHeader = value; } }
            //End Amitosh
        }

        //Code merged by Nitin from Safeway to RMX R6 release on 22-July-2009 ends


        /// <summary>
        /// This is default constructor.
        /// </summary>
        /// 
        //sonali- cloud
        //for calling parameterless contructor
      /*   public WPA(string p_sConnectionstring = null,int p_iClientId = 0)
            {
                m_iClientId = p_iClientId; //rkaur27
                m_objConfig = RMConfigurationSettings.GetRMMessages(p_sConnectionstring, m_iClientId);
                m_DiariesConfigs = RMConfigurationManager.GetSingleTagSectionSettings("Diaries", p_sConnectionstring, m_iClientId);
            }*/

        /// <summary>
        /// Overloaded constructor, which sets the connection string.
        /// </summary>
         public WPA(string p_sConnectionstring, int p_iClientId)  //rkaur27 , sonali  for parameterless contructor
        {
            DbConnection objConn;
            m_sConnectionString = p_sConnectionstring;
            m_iClientId = p_iClientId; //rkaur27
            m_objConfig = RMConfigurationSettings.GetRMMessages(m_sConnectionString, m_iClientId);//rkaur27
            m_DiariesConfigs = RMConfigurationManager.GetSingleTagSectionSettings("Diaries", m_sConnectionString, m_iClientId);//rkaur27
            m_sSecConnectString = RMConfigurationSettings.GetSecurityDSN(m_iClientId);//Added by Amitosh for MITS 24229 (03/04/2011)
            //Added By Neha Mits 23586
            objConn = DbFactory.GetDbConnection(m_sSecConnectString);
            objConn.Open();
            m_sDBType = objConn.DatabaseType.ToString().ToUpper();
            objConn.Close();
        }
        public WPA(string p_sConnectionstring, string p_sDSNName, string p_sPassword, string p_sLoginName, int p_iClientId)  //rkaur27
        {
            DbConnection objConn;
            m_sConnectionString = p_sConnectionstring;
            m_sDSNName = p_sDSNName;
            m_sLoginName = p_sLoginName;
            m_sPassword = p_sPassword;
            m_iClientId = p_iClientId; //rkaur27
            m_objConfig = RMConfigurationSettings.GetRMMessages(m_sConnectionString, m_iClientId);//rkaur27
            m_DiariesConfigs = RMConfigurationManager.GetSingleTagSectionSettings("Diaries", m_sConnectionString, m_iClientId);//rkaur27
            m_sSecConnectString = RMConfigurationSettings.GetSecurityDSN(m_iClientId);//Added by Amitosh for MITS 24229 (03/04/2011)
            //Added By Neha Mits 23586
            objConn = DbFactory.GetDbConnection(m_sSecConnectString);
            objConn.Open();
            m_sDBType = objConn.DatabaseType.ToString().ToUpper();
            objConn.Close();
        }

        #region Property declaration
        /// <summary>
        /// Gets & sets the user id
        /// </summary>
        public long UserId { get { return m_lUserId; } set { m_lUserId = value; } }

        /// <summary>
        /// Gets & sets the dsn id
        /// </summary>
        public long DSNId { get { return m_lDSNId; } set { m_lDSNId = value; } }

        /// <summary>
        /// Set the Page Size
        /// </summary>
        public long PageSize { set { m_lPageSize = value; } }

        /// <summary>
        /// Set the Group Id Property
        /// </summary>
        public int GroupId { set { m_iGroupId = value; } }

        /// <summary>
        /// SecureDSN Property
        /// </summary>
        public string SecureDSN { get { return m_sSecConnectString; } set { m_sSecConnectString = value; } }

        /// <summary>
        /// DSN Property
        /// </summary>
        public string DSN { get { return m_sConnectionString; } set { m_sConnectionString = value; } }
        //Amandeep ML Change added the language Property
        /// <summary>
        /// Language Property
        /// </summary>
        public int LanguageCode { get { return m_iLangCode; } set { m_iLangCode = value; } }

        //Started by Nitin on 28-Feb-2009 for R5-R4 Merging
        // 03/08/2008 Asif MITS start 10641
        /// <summary>
        /// Read property for PageHeight.
        /// </summary>
        private double PageHeight
        {
            get
            {
                return (m_dblPageHeight);
            }
        }


        /// <summary>
        /// Read property for PageWidth.
        /// </summary>
        private double PageWidth
        {
            get
            {
                return (m_dblPageWidth);
            }
        }
        /// <summary>
        /// Read property for PageWidth.
        /// </summary>
        private double ClientWidth
        {
            get
            {
                return (m_dblClientWidth);
            }
            set
            {
                m_dblClientWidth = value;
            }
        }

        /// <summary>
        /// Read/Write property for CurrentX.
        /// </summary>
        private double CurrentX
        {
            get
            {
                return (m_dblCurrentX);
            }
            set
            {
                m_dblCurrentX = value;
            }
        }
        /// <summary>
        /// Read/Write property for CurrentY.
        /// </summary>
        private double CurrentY
        {
            get
            {
                return (m_dblCurrentY);
            }
            set
            {
                m_dblCurrentY = value;
            }
        }
        /// <summary>
        /// Gets unique filename
        /// </summary>
        private string TempFile
        {
            get
            {
                string sFileName = string.Empty;
                string strDirectoryPath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "Payments");

                DirectoryInfo objReqFolder = new DirectoryInfo(strDirectoryPath);

                if (!objReqFolder.Exists)
                    objReqFolder.Create();

                sFileName = strDirectoryPath + "\\" + Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf");

                return sFileName;
            }
        }
        //End Asif MITS:10641
        //End by Nitin on 28-Feb-2009 for R5-R4 Merging
        #endregion

        #region Get Single String
        /// <summary>
        /// Creates a dynamic query and executes it against the database and returns result as a String value
        /// </summary>
        /// <param name="p_sFieldName">Field Name</param>
        /// <param name="p_sTableName">Table Name</param>
        /// <param name="p_sCriteria">Criteria i.e. Where clause</param>
        /// <param name="p_sConnectString">Connection string</param>
        /// <returns>string</returns>
        private string GetSingleString(string p_sFieldName, string p_sTableName, string p_sCriteria, string p_sConnectString)
        {
            string sGetSingleLong = "";
            string sSql = "";
            DbReader objReader = null;
            try
            {
                if (p_sCriteria.Trim().Length == 0)
                {
                    p_sCriteria = "";
                }
                else
                {
                    p_sCriteria = " WHERE " + p_sCriteria;
                }
                sSql = "SELECT " + p_sFieldName + " FROM " + p_sTableName + p_sCriteria;
                objReader = DbFactory.GetDbReader(p_sConnectString, sSql);

                if (objReader != null)
                    if (objReader.Read())
                    {
                        if (p_sTableName == "CLAIMANT, ENTITY")
                        {
                            if (Conversion.ConvertObjToStr(objReader.GetValue(1)).Trim() != "")
                                sGetSingleLong = Conversion.ConvertObjToStr(objReader.GetValue(0)) + ", " + Conversion.ConvertObjToStr(objReader.GetValue(1));
                            else
                                sGetSingleLong = Conversion.ConvertObjToStr(objReader.GetValue(0));
                        }
                        else
                        {
                            if (objReader.GetValue(0) is System.DBNull)
                            {
                                sGetSingleLong = "";
                            }
                            else
                            {
                                sGetSingleLong = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            }
                        }
                    }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetSingleString.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (sGetSingleLong);
        }

        /// <summary>
        /// Creates a dynamic query and executes it against the database and returns result as a String array.
        /// This function can accept comma separed Feild Names to extract and returns string array
        /// </summary>
        /// <param name="p_sFieldName">Field Name(can be comma separated)</param>
        /// <param name="p_sTableName">Table Name</param>
        /// <param name="p_sCriteria">Criteria i.e. Where clause</param>
        /// <param name="p_sConnectString">Connection string</param>
        /// <returns>string[]</returns>
        private string[] GetMultipleString(string p_sFieldName, string p_sTableName, string p_sCriteria, string p_sConnectString)
        {
            int iFeildLength = p_sFieldName.Split(',').Length;
            string[] sFeilds = new string[iFeildLength];
            string sSql = "";
            DbReader objReader = null;
            try
            {
                if (p_sCriteria.Trim().Length == 0)
                {
                    p_sCriteria = "";
                }
                else
                {
                    p_sCriteria = " WHERE " + p_sCriteria;
                }
                sSql = "SELECT " + p_sFieldName + " FROM " + p_sTableName + p_sCriteria;
                objReader = DbFactory.GetDbReader(p_sConnectString, sSql);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        for (int i = 0; i < iFeildLength; i++)
                        {
                            if (objReader.GetValue(i) is System.DBNull)
                            {
                                sFeilds[i] = "";
                            }
                            else
                            {
                                sFeilds[i] = Conversion.ConvertObjToStr(objReader.GetValue(i));
                            }
                        }
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetMultipleString.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (sFeilds);
        }
        #endregion

        #region Get Single Long
        /// <summary>
        /// Creates a dynamic query and executes it against the database and returns result as a Long value
        /// </summary>
        /// <param name="p_sFieldName">Field Name</param>
        /// <param name="p_sTableName">Table Name</param>
        /// <param name="p_sCriteria">Criteria ie. where clause</param>
        /// <param name="p_sConnectString">Connection string</param>
        /// <returns>long</returns>
        private long GetSingleLong(string p_sFieldName, string p_sTableName, string p_sCriteria, string p_sConnectString)
        {
            long lGetSingleLong = 0;
            string sSql = "";
            DbReader objReader = null;
            try
            {
                if (p_sCriteria.Trim().Length == 0)
                {
                    p_sCriteria = "";
                }
                else
                {
                    p_sCriteria = " WHERE " + p_sCriteria;
                }
                sSql = "SELECT " + p_sFieldName + " FROM " + p_sTableName + p_sCriteria;
                objReader = DbFactory.GetDbReader(p_sConnectString, sSql);

                if (objReader != null)
                    if (objReader.Read())
                    {
                        if (objReader.GetValue(0) is System.DBNull)
                        {
                            lGetSingleLong = 0;
                        }
                        else
                        {
                            lGetSingleLong = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }

                    }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetSingleLong.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (lGetSingleLong);
        }
        #endregion

        // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
        #region Get Single Int
        /// <summary>
        /// Creates a dynamic query and executes it against the database and returns result as an Int value
        /// </summary>
        /// <param name="p_sFieldName">Field Name</param>
        /// <param name="p_sTableName">Table Name</param>
        /// <param name="p_sCriteria">Criteria ie. where clause</param>
        /// <param name="p_sConnectString">Connection string</param>
        /// <returns>Int</returns>
        private int GetSingleInt(string p_sFieldName, string p_sTableName, string p_sCriteria, string p_sConnectString)
        {
            int iGetSingleInt = 0;
            string sSql = "";
            DbReader objReader = null;
            bool blnSuccess = false;
            try
            {
                if (p_sCriteria.Trim().Length == 0)
                {
                    p_sCriteria = "";
                }
                else
                {
                    p_sCriteria = String.Format(" WHERE {0}", p_sCriteria);
                }
                sSql = String.Format("SELECT {0} FROM {1} {2}", p_sFieldName, p_sTableName, p_sCriteria);
                objReader = DbFactory.GetDbReader(p_sConnectString, sSql);

                if (objReader != null)
                    if (objReader.Read())
                    {
                        if (objReader.GetValue(0) is System.DBNull)
                        {
                            iGetSingleInt = 0;
                        }
                        else
                        {
                            iGetSingleInt = Conversion.CastToType<int>(objReader.GetValue(0).ToString(), out blnSuccess);

                            if (!blnSuccess)
                            {
                                iGetSingleInt = 0;
                            }
                        }

                    }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetSingleInt.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (iGetSingleInt);
        }
        #endregion
        // npadhy End MITS 22028 Policies opening up in Maintenance when opened from Diaries

        #region Available Peek List
        /// <summary>
        /// This function returns the list of users/subordinates under a manager
        /// </summary>
        /// <returns>string as list of users separated by "~"</returns>
        public XmlDocument GetAvailablePeekList()
        {
            string sSql = "";
            string sFirstName = "";
            string sLastName = "";
            string sName = "";
            string sTmpUserId = "";
            string sValToCompare = "";
            string sTmpUserIdPrev = "";
            int iCount = 0;
            long lUid = 0;
            long lPrev = 0;
            bool bAllowGlobalPeek = false;
            DbReader objReader = null;
            XmlDocument objRetXML = null;
            XmlElement objElem = null;

            try
            {
                objRetXML = new XmlDocument();
                //Check for global peek setting (controlled from Utilities)
                bAllowGlobalPeek = Conversion.ConvertLongToBool(GetSingleLong("ALLOW_GLOBAL_PEEK", "SYS_PARMS", "", m_sConnectionString), m_iClientId);
                //Find all valid peek targets
                iCount = 0;
                sTmpUserId = " " + m_lUserId + " ";
                if (!bAllowGlobalPeek)
                {
                    sTmpUserIdPrev = sTmpUserId;
                    sSql = "SELECT USER_TABLE.USER_ID " +
                        " FROM USER_TABLE " +
                        " WHERE USER_TABLE.MANAGER_ID IN (" + sTmpUserId + ")";
                    objReader = DbFactory.GetDbReader(m_sSecConnectString, sSql);

                    if (objReader != null)
                    {
                        while ((objReader.Read()) && (iCount < 50))
                        {
                            //If userid not already in the list....
                            sValToCompare = " " + Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")) + " ";
                            if (sTmpUserId.IndexOf(sValToCompare) == -1)
                            {
                                sTmpUserId = sTmpUserId + ", " + Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")) + " ";
                            }
                            sValToCompare = "";
                            iCount = iCount + 1;
                        }
                        objReader.Close();
                    }
                }

                sSql = "SELECT DISTINCT USER_DETAILS_TABLE.USER_ID,LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME " +
                    " FROM USER_DETAILS_TABLE,USER_TABLE " +
                    " WHERE USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID";

                //query was resulting data for all dsn's... query modified by raman bhatia...
                sSql += " AND USER_DETAILS_TABLE.DSNID = (" + m_lDSNId + ")";
                if (!bAllowGlobalPeek)
                {
                    sSql += " AND USER_TABLE.MANAGER_ID IN (" + sTmpUserId + ")";
                }
                sSql += " ORDER BY FIRST_NAME, LAST_NAME";
                objReader = DbFactory.GetDbReader(m_sSecConnectString, sSql);

                if (objReader != null)
                {
                    objElem = objRetXML.CreateElement("diaries");
                    objRetXML.AppendChild(objElem);
                    objElem = null;
                    objElem = objRetXML.CreateElement("Users");
                    objRetXML.FirstChild.AppendChild(objElem);
                    while (objReader.Read())
                    {
                        //If userid not already in the list....
                        lUid = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")));
                        sFirstName = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Trim();
                        sLastName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")).Trim();
                        sName = sFirstName == "" ? sLastName : sFirstName + " " + sLastName;

                        XmlElement objElemUser = null;
                        if ((!sName.Trim().Equals("")) && (lUid != lPrev))
                        {
                            objElemUser = objRetXML.CreateElement("User");
                            objElemUser.SetAttribute("username", sName);
                            objElemUser.SetAttribute("loginname", Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME")).Trim());
                            objElemUser.SetAttribute("id", lUid.ToString());
                            lPrev = lUid;
                        }
                        if (objElemUser != null)
                            objElem.AppendChild(objElemUser);
                    }
                    objReader.Close();
                }
                return (objRetXML);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetAvailablePeekList.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objRetXML = null;
                objElem = null;
            }
        }
        //nsharma202 : RMACLOUD-1638
        /// <summary>
        /// This function returns the list of users/subordinates under a manager
        /// </summary>
        /// <returns>json string as list of users</returns>
        public string GetReportees()
        {
            string sSql = "";
            string sFirstName = "";
            string sLastName = "";
            string sName = "";
            string sTmpUserId = "";
            string sValToCompare = "";
            string sTmpUserIdPrev = "";
            int iCount = 0;
            long lUid = 0;
            long lPrev = 0;
            bool bAllowGlobalPeek = false;
            DbReader objReader = null;
            string peekList = string.Empty;
            try
            {
                
                peekList = "[";

                //Check for global peek setting (controlled from Utilities)
                bAllowGlobalPeek = Conversion.ConvertLongToBool(GetSingleLong("ALLOW_GLOBAL_PEEK", "SYS_PARMS", "", m_sConnectionString), m_iClientId);
                //Find all valid peek targets
                iCount = 0;
                sTmpUserId = " " + m_lUserId + " ";
                if (!bAllowGlobalPeek)
                {
                    sTmpUserIdPrev = sTmpUserId;
                    sSql = "SELECT USER_TABLE.USER_ID " +
                        " FROM USER_TABLE " +
                        " WHERE USER_TABLE.MANAGER_ID IN (" + sTmpUserId + ")";
                    objReader = DbFactory.GetDbReader(m_sSecConnectString, sSql);

                    if (objReader != null)
                    {
                        while ((objReader.Read()) && (iCount < 50))
                        {
                            //If userid not already in the list....
                            sValToCompare = " " + Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")) + " ";
                            if (sTmpUserId.IndexOf(sValToCompare) == -1)
                            {
                                sTmpUserId = sTmpUserId + ", " + Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")) + " ";
                            }
                            sValToCompare = "";
                            iCount = iCount + 1;
                        }
                        objReader.Close();
                    }
                }

                sSql = "SELECT DISTINCT USER_DETAILS_TABLE.USER_ID,LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME " +
                    " FROM USER_DETAILS_TABLE,USER_TABLE " +
                    " WHERE USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID";

                //query was resulting data for all dsn's... query modified by raman bhatia...
                sSql += " AND USER_DETAILS_TABLE.DSNID = (" + m_lDSNId + ")";
                if (!bAllowGlobalPeek)
                {
                    sSql += " AND USER_TABLE.MANAGER_ID IN (" + sTmpUserId + ")";
                }
                sSql += " ORDER BY FIRST_NAME, LAST_NAME";
                objReader = DbFactory.GetDbReader(m_sSecConnectString, sSql);

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        //If userid not already in the list....
                        lUid = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")));
                        sFirstName = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Trim();
                        sLastName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")).Trim();
                        sName = sFirstName == "" ? sLastName : sFirstName + " " + sLastName;

                        if ((!sName.Trim().Equals("")) && (lUid != lPrev))
                        {
                            peekList += "{ \"userName\":\"" + sName + "\", \"loginName\" :\"" + Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME")).Trim() + "\", \"userId\" :\"" + lUid.ToString() + "\"";
                            lPrev = lUid;
                        }
                        peekList += "},";
                    }
                    peekList = peekList.Substring(0, peekList.Length - 1);
                    peekList += "]";
                    objReader.Close();
                }
                return peekList;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetAvailablePeekList.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                
            }
        }
        #endregion

        #region Get Diaries History
        /// <summary>
        /// This function returns Diary records based on passed parameter values
        /// </summary>
        /// <param name="p_iSortOrder">Sort order i.e. ASC_COMPLETE_DATE</param>
        /// <param name="p_sUserName">User name</param>
        /// <param name="p_lRecordCount">No of records returned</param>
        /// <param name="p_lPageCount">Total number of Pages</param>
        /// <param name="p_lPageNumber">Current Page number</param>
        /// <param name="p_sAttachTable">Attach table i.e. Claim,Event</param>
        /// <param name="p_sAttachRecordId">Attached Record id</param>
        /// <returns>Return Diary History list as string</returns>

        //Adaptor specific changes - Changed return type from string to XmlDocument. Sumeet
        public XmlDocument GetDiaryHistoryDom(int p_iSortOrder, string p_sUserName, long p_lRecordCount
            , long p_lPageCount, long p_lPageNumber, string p_sAttachTable, string p_sAttachRecordId)
        {
            string sSQL = "";
            string sWhere = "";
            string sAssignedUser = "";
            string sAssignedGroup = ""; //pkandhari Jira 6412

            string sAssigningUser = "";
            string sAttached = "";
            string sPIXmlFormName = "";
            string sIsAttached = "";
            string sClaimStatus = "";
            string sFname = "";
            string sLname = "";
            string sName = "";
            int iCLnToInt = 0;
            int iSortOrder = 0;
            int iCounter = 0;
            int iUpperBound = 0;
            int iUserId = 0; //pkandhari Jira 6412
            float fCalPages = 0;
            long lCounter = 0;
            long lRid = 0;
            long lTmp = 0;
            long lToday = 0;
            long lCompleteDate = 0;
            long lStartAt = 0;

            XmlDocument objDOM = null;
            XmlElement objElem = null;
            LocalCache objCache = null;
            DataSet objDataSet = null;
            SysSettings objSys = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud
            DataRow[] objRow = null;
            //rupal:r8 auto diary enh
            string sClaimID = string.Empty;
            string[] sProgressNoteDetail;
            string[] sAttachPromptDetail;
            //rupal:end

            //Added by sharishkumar for mits 35291
            string[] sDependentDetail;
            string sDependentRowDetail;
            //End mits 35291
            try
            {
                //pkandhari  Jira 6412 starts
                var ReqUserid = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sSecConnectString, "SELECT DISTINCT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '" + p_sUserName.ToString() + "'"), m_iClientId);
                //update group based on p_sUserName
                this.GroupId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " + ReqUserid.ToString()), m_iClientId);
                //pkandhari  Jira 6412 ends
                lToday = Conversion.ConvertStrToLong(Conversion.GetDate(DateTime.Now.Date.ToString()));
                sSQL = "SELECT ENTRY_ID,COMPLETE_DATE,IS_ATTACHED,ATTACH_TABLE,ATTACH_RECORDID,REGARDING,PRIORITY,ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,ASSIGNED_USER,ASSIGNED_GROUP FROM WPA_DIARY_ENTRY "; //pkandhari Jira 6412
                sWhere = "DIARY_VOID = 0 AND DIARY_DELETED = 0 AND STATUS_OPEN = 0";

                objCache = new LocalCache(m_sConnectionString,m_iClientId); //Geeta 07/06/07: Modified to fix Oracle Diary History loading issue
                //Only if attached diary globally visible is false, or attached diary globally visible is true
                // and not attached to any record
                if (objSys.AttachDiaryVis == false ||
                    (objSys.AttachDiaryVis == true && p_sAttachTable.Trim() == ""))
                {
                    if (p_sUserName != "")
                    {
                        sWhere = sWhere + " AND (ASSIGNED_USER = '" + p_sUserName + "' OR ASSIGNED_GROUP = '" + m_iGroupId.ToString() + "')";
                    }
                }

                if (p_sAttachTable != "")
                {
                    sWhere = sWhere + " AND ATTACH_TABLE='" + p_sAttachTable + "'";
                }
                if (p_sAttachRecordId != "")
                {
                    sWhere = sWhere + " AND ATTACH_RECORDID = " + p_sAttachRecordId;
                    sWhere = sWhere + " AND IS_ATTACHED <> 0";

                }
                sWhere = sWhere.Trim();
                sSQL = sSQL + " WHERE " + sWhere;

                switch (p_iSortOrder)
                {
                    case (int)DIARYSORTORDER.ASC_COMPLETE_DATE:
                        sSQL = sSQL + " ORDER BY COMPLETE_DATE ASC";
                        iSortOrder = (int)DIARYSORTORDER.ASC_COMPLETE_DATE;
                        break;
                    case (int)DIARYSORTORDER.ASC_ATTACH_TABLE:
                        //sSQL = sSQL + " ORDER BY ATTACH_TABLE ASC";
                        iSortOrder = (int)DIARYSORTORDER.ASC_ATTACH_TABLE;
                        break;
                    case (int)DIARYSORTORDER.ASC_ENTRY_NAME:
                        sSQL = sSQL + " ORDER BY ENTRY_NAME ASC";
                        iSortOrder = (int)DIARYSORTORDER.ASC_ENTRY_NAME;
                        break;
                    case (int)DIARYSORTORDER.ASC_PRIORITY:
                        sSQL = sSQL + " ORDER BY PRIORITY ASC";
                        iSortOrder = (int)DIARYSORTORDER.ASC_PRIORITY;
                        break;
                    case (int)DIARYSORTORDER.DEC_ATTACH_TABLE:
                        //sSQL = sSQL + " ORDER BY ATTACH_TABLE DESC";
                        iSortOrder = (int)DIARYSORTORDER.DEC_ATTACH_TABLE;
                        break;
                    case (int)DIARYSORTORDER.DEC_ENTRY_NAME:
                        sSQL = sSQL + " ORDER BY ENTRY_NAME DESC";
                        iSortOrder = (int)DIARYSORTORDER.DEC_ENTRY_NAME;
                        break;
                    case (int)DIARYSORTORDER.DEC_PRIORITY:
                        sSQL = sSQL + " ORDER BY PRIORITY DESC";
                        iSortOrder = (int)DIARYSORTORDER.DEC_PRIORITY;
                        break;
						//tmalhotra3- MITS 25091
                    case (int)DIARYSORTORDER.ASC_ASSIGNING_USER:
                        sSQL = sSQL + " ORDER BY ASSIGNING_USER ASC";
                        iSortOrder = (int)DIARYSORTORDER.ASC_ASSIGNING_USER;
                        break;
                    case (int)DIARYSORTORDER.DEC_ASSIGNING_USER:
                        sSQL = sSQL + " ORDER BY ASSIGNING_USER DESC";
                        iSortOrder = (int)DIARYSORTORDER.DEC_ASSIGNING_USER;
                        break;
                    case (int)DIARYSORTORDER.ASC_ASSIGNED_USER:
                        sSQL = sSQL + " ORDER BY ASSIGNED_USER ASC";
                        iSortOrder = (int)DIARYSORTORDER.ASC_ASSIGNED_USER;
                        break;
                    case (int)DIARYSORTORDER.DEC_ASSIGNED_USER:
                        sSQL = sSQL + " ORDER BY ASSIGNED_USER DESC";
                        iSortOrder = (int)DIARYSORTORDER.DEC_ASSIGNED_USER;
                        break;
                    //MITS 36519
                    case (int)DIARYSORTORDER.ASC_PARENTRECORD:
                        sSQL = sSQL + " ORDER BY ATT_PARENT_CODE ASC";
                        iSortOrder = (int)DIARYSORTORDER.ASC_PARENTRECORD;
                        break;
                    case (int)DIARYSORTORDER.DEC_PARENTRECORD:
                        sSQL = sSQL + " ORDER BY ATT_PARENT_CODE DESC";
                        iSortOrder = (int)DIARYSORTORDER.DEC_PARENTRECORD;
                        break;
                    default:
                        sSQL = sSQL + " ORDER BY COMPLETE_DATE DESC";
                        p_iSortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                        iSortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                        break;
                }

                objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);

                objRow = objDataSet.Tables[0].Select();


                //Umesh MITS 9401 : SORTING BY Attached Record

                iCounter = 0;
                iUpperBound = 0;
                objDataSet.Tables[0].Columns.Add("ATTACH_PROMPT", System.Type.GetType("System.String"));
                iUpperBound = objDataSet.Tables[0].Rows.Count - 1;
                while (iCounter <= iUpperBound)
                {
                    lRid = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_RECORDID"]));
                    if (lRid > 0)
                    {

                        switch (Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"]))
                        {
                            case CLAIM:
                            case "claim":
                                sAttached = "Claim: " + GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + lRid.ToString(), m_sConnectionString);
                                break;
                            case EVENT:
                            case "event":

                                sAttached = "Event: " + GetSingleString("EVENT_NUMBER", "EVENT", "EVENT_ID=" + lRid.ToString(), m_sConnectionString);
                                break;
                            case ENTITY:
                            case "entity":
                                objCache.GetEntityInfo(lRid, ref sFname, ref sLname);
                                sName = sFname.Trim().Equals("") ? sLname : sFname + ", " + sLname;
                                sAttached = "Person\\Entity:" + sName;
                                break;
                            case CLAIMANT:
                            case "claimant":
                                objCache.GetEntityInfo(lRid, ref sFname, ref sLname);
                                sName = sFname.Trim().Equals("") ? sLname : sFname + ", " + sLname;
                                sAttached = "Claimant:" + sName;
                                break;
                            case PERSON_INVOLVED:
                            case "person_involved":
                                sPIXmlFormName = GetPiXMLFormName(lRid, ref sAttached);
                                break;
                            case TM_JOB_LOG:
                            case "tm_job_log":
                                sAttached = "Task Manager Job ( " + lRid.ToString() + ")";
                                break;
                            // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
                            case POLICY_ENH:
                            case "POLICY_ENH":
                            case "policy_enh":
                                // Get the LOB information of the Policy
                                int iLob = GetSingleInt("POLICY_TYPE", "POLICY_ENH", "POLICY_ID = " + lRid.ToString(), m_sConnectionString);
                                objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"] = string.Concat("POLICY_ENH_", objCache.GetShortCode(iLob));
                                sAttached = String.Concat("POLICY MANAGEMENT (", objCache.GetShortCode(iLob), "): ", GetSingleString("POLICY_NAME", "POLICY_ENH", string.Format("POLICY_ID={0}", lRid.ToString()), m_sConnectionString));
                                break;
                            // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
                            //rupal:start r8 auto diary enh
                            //scheduled activity for subrogation
                            case SCHEDULED_ACTIVITY:
                            case "scheduled_activity":
                                sAttached = GetAttachPromptForScheduledActivity(lRid.ToString(), out sClaimID);
                                break;
                            case CASE_MANAGEMENT:
                            case CMXTREATMENTPLN:
                            case "cm_x_treatment_pln":
                            case "cmxtreatmentpln":
                                sAttached = GetAttachPropmtForCaseManagement(lRid.ToString());
                                break;
                            case RESERVE_HISTORY:
                            case "reserve_history":
                                sAttached = GetAttachPromptForReserveHistory(lRid.ToString(), out sClaimID);
                                break;
                            case RESERVE_CURRENT:
                            case "reserve_current":
                                sAttached = GetAttachPromptForReserveReview(lRid.ToString(), out sClaimID);
                                break;
                            case CLAIM_LITIGATION:
                            case LITIGATION:
                            case "claim_x_litigation":
                            case "litigation":
                                sAttached = GetAttachPromptForLitigation(lRid.ToString());
                                break;
                            case FUND_AUTO_BATCH:
                            case AUTO_CLAIM_CHECKS:
                            case "funds_auto_batch":
                            case "autoclaimchecks":
                                sAttached = GetAttachPromptForSchedulePayment(lRid.ToString());
                                break;
                            /*
                            case COMMENTS_TEXT:
                            case "comments_text":
                                //get Attached Table for the comment
                                string[] sCommentDetail = GetMultipleString("ATTACH_RECORDID,ATTACH_TABLE,COMMENT_ID", "COMMENTS_TEXT", "COMMENT_ID=" + lRid.ToString(), m_sConnectionString);
                                sAttached = GetAttachPromptForcommentReview(sCommentDetail);
                                break;
                             */
                            case CLAIM_PRG_NOTE:
                            case "claim_prg_note":
                                sProgressNoteDetail = GetMultipleString("CLAIM_ID,EVENT_ID,CL_PROG_NOTE_ID", "CLAIM_PRG_NOTE", "CL_PROG_NOTE_ID=" + lRid.ToString(), m_sConnectionString);
                                sAttachPromptDetail = GetAttachPromptForProgressNotes(sProgressNoteDetail);
                                sAttached = sAttachPromptDetail[0];
                                break;
                            //rupal:end r8 auto diary enh
                            //Added by sharishkumar for mits 35291
                            case PIDEPENDENT:
                            case "pidependent":                              
                                sDependentRowDetail = GetSingleString("PI_ROW_ID", "PI_X_DEPENDENT", "PI_DEP_ROW_ID=" + lRid.ToString(), m_sConnectionString);
                                sDependentDetail = GetMultipleString("EVENT_ID,PI_ROW_ID", "PERSON_INVOLVED", "PI_ROW_ID=" + sDependentRowDetail, m_sConnectionString);
                                sAttachPromptDetail = GetAttachPromptForDependent(sDependentDetail);
                                sAttached = sAttachPromptDetail[0];                              
                                 break;
                            //End mits 35291
                            default:
                                lTmp = objCache.GetTableId(Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"]));
                                if (Conversion.ConvertLongToBool(lTmp, m_iClientId))
                                {
                                    sAttached = objCache.GetTableName(Conversion.ConvertStrToInteger(lTmp.ToString())) + " (" + lRid.ToString() + ")";
                                }
                                else
                                {
                                    sAttached = Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"]).ToUpper() + lRid.ToString();
                                }
                                break;
                        }
                        objDataSet.Tables[0].Rows[iCounter]["ATTACH_PROMPT"] = sAttached;

                    }


                    iCounter = iCounter + 1;
                }
                //Check for the Attached Record Sort Order

                if (p_iSortOrder == 2)
                {
                    objRow = objDataSet.Tables[0].Select("", "ATTACH_PROMPT ASC");

                }
                if (p_iSortOrder == 6)
                {
                    objRow = objDataSet.Tables[0].Select("", "ATTACH_PROMPT DESC");

                }

                //END MITS 9401 




                if (objDataSet.Tables.Count == 0)
                {
                    throw new RMAppException(Globalization.GetString("WPA.GetDiaryHistoryDom.DatasetCreationError", m_iClientId));
                }
                if (objDataSet.Tables.Count > 0)
                {
                    p_lRecordCount = objDataSet.Tables[0].Rows.Count;
                }
                p_lPageCount = (int)(p_lRecordCount / m_lPageSize);
                fCalPages = (float)((float)p_lRecordCount / (float)m_lPageSize);
                if ((float)(p_lPageCount) < (float)(fCalPages))
                {
                    p_lPageCount = p_lPageCount + 1;
                }
                if (p_lPageCount == 0)
                {
                    p_lPageCount = 1;
                }
                objDOM = new XmlDocument();
                objElem = objDOM.CreateElement("diaries");
                objElem.SetAttribute("user", p_sUserName);
                objElem.SetAttribute("opendiaries", "0");
                objElem.SetAttribute("attach_table", p_sAttachTable);
                objElem.SetAttribute("attach_recordid", p_sAttachRecordId);
                objElem.SetAttribute("orderby", iSortOrder.ToString());
                if (p_lPageNumber > 1)
                {
                    objElem.SetAttribute("previouspage", (p_lPageNumber - 1).ToString());
                    objElem.SetAttribute("firstpage", "1");
                }
                if (p_lPageNumber < p_lPageCount)
                {
                    objElem.SetAttribute("nextpage", (p_lPageNumber + 1).ToString());
                    objElem.SetAttribute("lastpage", p_lPageCount.ToString());
                }
                objElem.SetAttribute("pagenumber", p_lPageNumber.ToString());
                objElem.SetAttribute("pagecount", p_lPageCount.ToString());
                objDOM.AppendChild(objElem);
                objElem = null;
                //we do not want to throw an exception when there is no record..raman bhatia
                /*
                if (p_lRecordCount == 0)
                {
                    throw new RecordNotFoundException(Globalization.GetString("WPA.GetDiaryHistoryDom.NoRecordFound"));
                }
                */
                if (p_lPageNumber == 1)
                {
                    lStartAt = 1;
                }
                else
                {
                    lStartAt = ((p_lPageNumber - 1) * m_lPageSize) + 1;
                }
                lCounter = lStartAt + m_lPageSize;
                while (lStartAt < lCounter && lStartAt <= p_lRecordCount)
                {
                    iCLnToInt = Conversion.ConvertStrToInteger(lStartAt.ToString());
                    sPIXmlFormName = "";
                    objElem = objDOM.CreateElement("diary");
                    objElem.SetAttribute("entry_id", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ENTRY_ID"]));
                    lCompleteDate = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["COMPLETE_DATE"]));
                    sAssigningUser = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ASSIGNING_USER"]).ToLower();
                    //pkandhari Jira 6412 starts
                    if ((Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ASSIGNED_GROUP"]) == "NA") || (Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ASSIGNED_GROUP"]) == "NULL") || (objRow[iCLnToInt - 1]["ASSIGNED_GROUP"] is DBNull))
                    {
                        sAssignedUser = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ASSIGNED_USER"]).ToLower();
                        sAssignedGroup = "0";
                    }
                    else
                    {
                        if (!(objRow[iCLnToInt - 1]["ASSIGNED_GROUP"] is DBNull))
                        {
                            m_sGroupName = Convert.ToString(DbFactory.ExecuteScalar(m_sConnectionString, ("SELECT GROUP_NAME FROM USER_GROUPS WHERE GROUP_ID = '" + Conversion.ConvertObjToInt(objRow[iCLnToInt - 1]["ASSIGNED_GROUP"], m_iClientId) + "'")));
                        }
                        else
                            m_sGroupName = string.Empty;
                        sAssignedUser = m_sGroupName;
                        sAssignedGroup = "1";
                    }
                    //pkandhari Jira 6412 ends
                    if (lCompleteDate < lToday)
                    {
                        if (sAssigningUser == "system")
                        {
                            objElem.SetAttribute("img", IMG_SYSTEM_OVERDUE);
                            objElem.SetAttribute("alt", ALT_SYSTEM);
                        }
                        else if (sAssigningUser == p_sUserName.ToLower())
                        {
                            objElem.SetAttribute("img", IMG_USER_OVERDUE);
                            objElem.SetAttribute("alt", ALT_USER);
                        }
                        else
                        {
                            objElem.SetAttribute("img", IMG_USERS_OVERDUE);
                            objElem.SetAttribute("alt", ALT_USERS);
                        }
                    }
                    else
                    {
                        if (sAssigningUser == "system")
                        {
                            objElem.SetAttribute("img", IMG_SYSTEM);
                            objElem.SetAttribute("alt", ALT_SYSTEM);
                        }
                        else if (sAssigningUser == p_sUserName.ToLower())
                        {
                            objElem.SetAttribute("img", IMG_USER);
                            objElem.SetAttribute("alt", ALT_USER);
                        }
                        else
                        {
                            objElem.SetAttribute("img", IMG_USERS);
                            objElem.SetAttribute("alt", ALT_USERS);
                        }
                    }

                    if (lCompleteDate != 0)
                    {
                        objElem.SetAttribute("complete_date", Conversion.GetDBDateFormat(lCompleteDate.ToString(), "d"));
                    }
                    sIsAttached = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["IS_ATTACHED"]);
                    if (sIsAttached.Equals("0"))
                    {
                        sIsAttached = "0";
                    }
                    else
                    {
                        sIsAttached = "1";
                    }
                    objElem.SetAttribute("assigning_user", sAssigningUser);
                    objElem.SetAttribute("assigned_user", sAssignedUser);
                    objElem.SetAttribute("assigned_group", sAssignedGroup); //pkandhari Jira 6412
                    objElem.SetAttribute("is_attached", sIsAttached);
                    objElem.SetAttribute("attach_table", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]));
                    objElem.SetAttribute("attach_recordid", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_RECORDID"]));
                    objElem.SetAttribute("attsecrecid", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_SEC_REC_ID"]));
                    sAttached = "";
                    sClaimStatus = "";
                    //					objCache=new LocalCache(m_sConnectionString);
                    lRid = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_RECORDID"]));
                    if (lRid > 0)
                    {
                        switch (Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]))
                        {
                            case CLAIM:
                            case "claim":
                                //sAttached = "Claim: " +GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" +lRid.ToString(),m_sConnectionString);
                                sClaimStatus = GetClaimStatus(lRid.ToString());
                                objElem.SetAttribute("claimstatus", sClaimStatus);
                                break;
                            //case EVENT:	
                            //case "event":
                            //    sAttached = "Event: " + GetSingleString("EVENT_NUMBER", "EVENT", "EVENT_ID=" +lRid.ToString(),m_sConnectionString);
                            //    break;
                            //case ENTITY:
                            //case "entity":
                            //    objCache.GetEntityInfo(lRid,ref sFname,ref sLname);
                            //    sName=sFname.Trim().Equals("")?sLname:sFname+", "+sLname;
                            //    sAttached = "Person\\Entity:" ;
                            //    break;
                            //case CLAIMANT:
                            //case "claimant":
                            //    objCache.GetEntityInfo(lRid,ref sFname,ref sLname);
                            //    sName=sFname.Trim().Equals("")?sLname:sFname+", "+sLname;
                            //    sAttached = "Claimant:" + sName;
                            //    break;
                            case PERSON_INVOLVED:
                            case "person_involved":
                                sAttached = "";
                                sPIXmlFormName = GetPiXMLFormName(lRid, ref sAttached);
                                break;
                            //default:
                            //    lTmp = objCache.GetTableId(Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCLnToInt-1]["ATTACH_TABLE"]));
                            //    if (Conversion.ConvertLongToBool(lTmp))
                            //    {
                            //        sAttached = objCache.GetTableName(Conversion.ConvertStrToInteger(lTmp.ToString())) + " (" + lRid.ToString() + ")";
                            //    }
                            //    else
                            //    {
                            //        sAttached =  Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCLnToInt-1]["ATTACH_TABLE"]).ToUpper()+ lRid.ToString();
                            //    }
                            //    break;
                        }
                    }
                    objElem.SetAttribute("attachprompt", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_PROMPT"]));
                    objElem.SetAttribute("pixmlformname", sPIXmlFormName);
                    objElem.SetAttribute("priority", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["PRIORITY"]));
                    objElem.SetAttribute("tasksubject", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ENTRY_NAME"]));
                    objElem.SetAttribute("regarding", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["REGARDING"]));
                    objElem.InnerText = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ENTRY_NOTES"]);
                    objDOM.FirstChild.AppendChild(objElem);
                    lStartAt = lStartAt + 1;
                }
                //Adaptor specific changes Sumeet
                //sReturnXmlValue=objDOM.InnerXml;
                return (objDOM);
            }
            catch (RecordNotFoundException p_objException)
            {
                throw p_objException;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetDiaryHistoryDom.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objDataSet != null) objDataSet.Dispose();
                if (objCache != null) objCache.Dispose();
                objDOM = null;
                objElem = null;
                objSys = null;
            }
            //Adaptor specific changes Sumeet
            //			return (sReturnXmlValue);
        }
        #endregion

        #region Get PiXML
        /// <summary>
        /// This function fetches information like person involved from the database
        /// </summary>
        /// <param name="p_lPiRowId">Person involved id</param>
        /// <param name="p_sPrompt">Stores Prompt string i.e Person involved : Last name</param>
        /// <returns>person involved as string</returns>
        private string GetPiXMLFormName(long p_lPiRowId, ref string p_sPrompt)
        {
            string sSql = "";
            string sPiType = "";
            string sFN = "";
            string sLN = "";
            string sSC = "";
            string sReturn = "";
            DbReader objReader = null;
            try
            {
                sSql = "SELECT CODES.SHORT_CODE, CODES_TEXT.CODE_DESC, ENTITY.LAST_NAME, ENTITY.FIRST_NAME" +
                    " FROM PERSON_INVOLVED, CODES, CODES_TEXT, ENTITY" +
                    " WHERE CODES.CODE_ID = PERSON_INVOLVED.PI_TYPE_CODE" +
                    " AND CODES_TEXT.CODE_ID = CODES.CODE_ID " +
                    " AND PERSON_INVOLVED.PI_EID = ENTITY.ENTITY_ID" +
                    " AND PERSON_INVOLVED.PI_ROW_ID = " + p_lPiRowId.ToString() +
                    " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sSC = objReader.GetValue(0) == System.DBNull.Value ? "" : Conversion.ConvertObjToStr(objReader.GetValue(0));
                        sPiType = objReader.GetValue(1) == System.DBNull.Value ? "" : Conversion.ConvertObjToStr(objReader.GetValue(1));
                        sLN = objReader.GetValue(2) == System.DBNull.Value ? "" : Conversion.ConvertObjToStr(objReader.GetValue(2));
                        sFN = objReader.GetValue(3) == System.DBNull.Value ? "" : Conversion.ConvertObjToStr(objReader.GetValue(3));

                    }
                }
                if (sFN.Trim().Length > 0)
                {
                    sLN = sLN + ", " + sFN;
                    //rupal:start, r8 auto diary enh
                    //i feel that following line should be out side if block. in case where First Name is blank
                    //we are getting attached propmt blank whick should actually contain atleast last name     
                    //p_sPrompt = sPiType + ": " + sLN;
                }
                p_sPrompt = sPiType + ": " + sLN;
                //rupal:end
                switch (sSC.ToUpper())
                {
                    case "E":
                        sReturn = "piemployee";
                        break;
                    case "MED":
                        sReturn = "pimedstaff";
                        break;
                    case "O":
                        sReturn = "piother";
                        break;
                    case "P":
                        sReturn = "piqpatient";
                        break;
                    case "PHYS":
                        sReturn = "piphysician";
                        break;
                    case "W":
                        sReturn = "piwitness";
                        break;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetPiXMLFormName.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (sReturn);
        }
        #endregion

        #region Get Claim Status
        /// <summary>
        /// Gets Claim status for a given claim id
        /// </summary>
        /// <param name="p_sClaimId">claim id</param>
        /// <returns>claim status</returns>
        private string GetClaimStatus(string p_sClaimId)
        {
            string sClaimStatusCode = "";
            string sCodeDesc = "";
            string sSql = "";
            DbReader objReader = null;
            LocalCache objCache = null;   //Aman ML Change
            try
            {
                if (p_sClaimId == null)
                {
                    return "";
                }
                if (p_sClaimId.Trim().Equals(""))
                {
                    return "";
                }
                sSql = "select claim_status_code from claim where claim_id=" + p_sClaimId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sClaimStatusCode = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                }
                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                    {
                        objReader.Close();
                    }
                }
                if (sClaimStatusCode.Trim().Equals(""))
                {
                    return "";
                }
                //Aman ML Change
                int iBaseLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]); ;
                if (this.LanguageCode == 0)
                    this.LanguageCode = iBaseLangCode;
                //sSql = "select code_desc from codes_text where code_id=" + sClaimStatusCode;
                //objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                //if (objReader != null)
                //{
                //    if (objReader.Read())
                //    {
                //        sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(0));
                //    }
                //}               
                objCache = new LocalCache(m_sConnectionString,m_iClientId);
                sCodeDesc = objCache.GetCodeDesc(Conversion.ConvertStrToInteger(sClaimStatusCode), this.LanguageCode);
                //Aman ML Change
                //if (objReader != null)
                //{
                //    if (!objReader.IsClosed)
                //    {
                //        objReader.Close();
                //    }
                //}
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetClaimStatus.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objCache != null) objCache.Dispose();
            }
            return (sCodeDesc);
        }
        #endregion

        #region Delete All Diaries of a Given Claim ID.
        /// <summary>
        /// This function Deletes diaries related to Claim.
        /// </summary>
        /// <param name="p_iClaimId">Attached Claim id</param>
        /// <returns>Returns boolean success.</returns>
        public bool DeleteAllDiaries(int p_iClaimId)
        {
            int iCount = 0;
            int i = 0;
            bool bReturn = false;
            string sSQL = "";
            Event objEvent = null;
            DbReader objReader = null;
            DataModelFactory objDMF = null;
            Claim objClaim = null;

            try
            {
                objDMF = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
                objClaim = (Claim)objDMF.GetDataModelObject("Claim", false);

                objClaim.MoveTo(p_iClaimId);
                objClaim.Context.DbConnLookup.ExecuteNonQuery(
                    String.Format(@"UPDATE WPA_DIARY_ENTRY SET STATUS_OPEN = 0 
					WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = 'CLAIM'", objClaim.ClaimId));

                sSQL = "SELECT TRANS_ID FROM FUNDS WHERE CLAIM_ID=" + objClaim.ClaimId;

                objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iCount++;
                    }
                }
                objReader.Dispose();
                iCount += objClaim.AdjusterList.Count;
                iCount += objClaim.ClaimantList.Count;
                iCount += objClaim.DefendantList.Count;
                iCount += objClaim.LitigationList.Count;
                objEvent = (objClaim.Parent as Event);
                iCount += objEvent.PiList.Count;

                foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                {
                    iCount += objAdjuster.AdjustDatedTextList.Count;
                }

                string[,] sClaimDetails = new string[iCount, 2];

                foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                {
                    sClaimDetails[i, 0] = objAdjuster.AdjRowId.ToString();
                    sClaimDetails[i, 1] = "ADJUSTER";
                    i++;
                }
                foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                {
                    foreach (AdjustDatedText objDatedText in objAdjuster.AdjustDatedTextList)
                    {
                        sClaimDetails[i, 0] = objDatedText.AdjDttextRowId.ToString();
                        sClaimDetails[i, 1] = "ADJUSTERDATEDTEXT";
                        i++;
                    }
                }
                foreach (Claimant objClaimant in objClaim.ClaimantList)
                {
                    sClaimDetails[i, 0] = objClaimant.ClaimantEid.ToString();
                    sClaimDetails[i, 1] = "CLAIMANT";
                    i++;
                }
                foreach (Defendant objDefendant in objClaim.DefendantList)
                {
                    sClaimDetails[i, 0] = objDefendant.DefendantRowId.ToString();
                    sClaimDetails[i, 1] = "DEFENDANT";
                    i++;
                }
                foreach (ClaimXLitigation objLitigation in objClaim.LitigationList)
                {
                    sClaimDetails[i, 0] = objLitigation.JudgeEid.ToString();
                    sClaimDetails[i, 1] = "LITIGATION";
                    i++;
                }
                foreach (PersonInvolved objPersons in objEvent.PiList)
                {
                    int piTypeCode = objPersons.PiTypeCode;
                    string sTable = string.Empty;
                    switch (objClaim.Context.LocalCache.GetShortCode(piTypeCode))
                    {
                        case "E":
                            sTable = "PiEmployee";
                            break;
                        case "MED":
                            sTable = "PiMedicalStaff";
                            break;
                        case "O":
                            sTable = "PiOther";
                            break;
                        case "P":
                            sTable = "PiPatient";
                            break;
                        case "PHYS":
                            sTable = "PiPhysician";
                            break;
                        case "W":
                            sTable = "PiWitness";
                            break;
                    }
                    sTable = sTable.ToUpper();
                    sClaimDetails[i, 0] = objPersons.PiRowId.ToString();
                    sClaimDetails[i, 1] = sTable;
                    i++;
                }

                objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        sClaimDetails[i, 0] = objReader.GetValue("TRANS_ID").ToString();
                        sClaimDetails[i, 1] = "FUNDS";
                        i++;
                    }
                }
                objReader.Dispose();
                for (int j = 0; j < iCount; j++)
                {
                    objClaim.Context.DbConnLookup.ExecuteNonQuery(
                        String.Format(@"UPDATE WPA_DIARY_ENTRY SET STATUS_OPEN = 0 
					WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = '{1}'", sClaimDetails[j, 0], sClaimDetails[j, 1]));
                }
                bReturn = true;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetDiaryDom.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objEvent != null)
                {
                    objEvent.Dispose();
                    objEvent = null;
                }
                if (objDMF != null)
                {
                    objDMF.Dispose();
                    //objDMF = null;
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return bReturn;
        }
        #endregion

        #region Get Diary DOM
        /// <summary>
        /// This function returns Diary's list based on parameters
        /// </summary>
        /// <param name="p_iSortOrder">Sort order i.e ASC_COMPLETE_DATE</param>
        /// <param name="p_sUserName">User name</param>
        /// <param name="p_sDueDate"></param>
        /// <param name="p_lRecordCount">Total No of records</param>
        /// <param name="p_lPageCount">Total Page number</param>
        /// <param name="p_lPageNumber">Current Page number</param>
        /// <param name="p_bOpen"></param>
        /// <param name="p_bReqActDiary"></param>
        /// <param name="p_sAttachTable">Attach table i.e Claim,Event</param>
        /// <param name="p_sAttachRecordId">Attached Record id</param>
        /// <param name="sFunction"></param>
        /// <param name="p_sActiveDiaryChecked">Indicates whether active diary is checked in diary list</param>        
        /// <param name="showNotes"></param>
        /// <param name="sAllClaimDiaries"></param>
        /// <param name="sShowAllDiariesForActiveRec"></param>
        /// <param name="sUserSortOrder"></param>
        /// <param name="sTaskType"></param>
        /// <param name="sTaskName"></param>
        /// <param name="p_sShowRegarding"></param>
        /// <returns>Returns Diary list as string</returns>
        //Ankit-MITS 27074-Change method signature
        //public XmlDocument GetDiaryDom(int p_iSortOrder, string p_sUserName, string p_sDueDate, long p_lRecordCount
        //    , long p_lPageCount, long p_lPageNumber, bool p_bOpen, bool p_bReqActDiary, string p_sAttachTable
        //    , string p_sAttachRecordId, string sFunction, string p_sActiveDiaryChecked, string showNotes, string sAllClaimDiaries, string sShowAllDiariesForActiveRec, string sUserSortOrder, string sTaskType, string sTaskName, string p_sShowRegarding, string p_sFilterExpression)        
        public XmlDocument GetDiaryDom(int p_iSortOrder, string p_sUserName, string p_sDueDate, long p_lRecordCount
            , long p_lPageCount, long p_lPageNumber, bool p_bOpen, bool p_bReqActDiary, string p_sAttachTable
            , string p_sAttachRecordId,string p_parentRecord, string sFunction, string p_sActiveDiaryChecked, string showNotes, string sAllClaimDiaries, string sShowAllDiariesForActiveRec, string sUserSortOrder, string sTaskType, string sTaskName, string p_sShowRegarding, string p_sFilterExpression, string p_sClaimActiveDiaryChecked, string p_sUIDiarySource)
        //Ankit-End changes
        {
            bool bIsAttached = false;
            string sSQL = "";
            string sWhere = "";
            string sWhere1 = string.Empty;
            string sWhere2 = string.Empty;
            string sAssigningUser = "";
            string sAssignedGroup = string.Empty; //pkandhari Jira 6412
            string sAssignedUser = "";
            string sAttached = "";
            string sPIXmlFormName = "";
            string sIsAttached = "";
            string sDueDate = "";
            string sFname = "";
            string sLname = "";
            string sName = "";
            string sClaimStatus = "";
            string sClaimantName = "";
            string sDepartmentName = "";
            string sStatus = "";
            //igupta3 jira 439
            string sNotRoutable = string.Empty;
            int irouteCodeId = 0;
            string sNotRollable = string.Empty;
            int irollCodeId = 0;

            string sOrgLevel = "";
            string sUserPrefsXML = "";
            string sSQL1 = string.Empty;
            //sshrikrishna:Start changes for MITS 27074
            StringBuilder sDiarySource = new StringBuilder();
            string sDiaryDueDate = p_sDueDate;
            string sClaimDiaryDueDate = p_sDueDate;
            long lTempIndex = 0;
            string sSortValue = string.Empty;
            //sshrikrishna:End changes for MITS 27074
            string sAttachTable = "";//nadim 11845
            string sSQL2 = string.Empty;// nadim 11845
            string sTableValue = "";//nadim 11845
            //string []arrTableValue= null;//nadim
            //Raman: R7 Prf Imp
            List<string> TableValue;//nadim 11845
            string sSQL_WPA = string.Empty;
            int iSortOrder = 0;
            int iCLnToInt = 0;
            int iCount = 0;
            int iCountVal = 0;//nadim 11845
            int iSorted = 0;
            int iCheckBoxChecked = 0;
            int iCounter = 0;
            int iUpperBound = 0;
            float fCalPages = 0;
            long lRid = 0;
            long lTmp = 0;
            long lToday = 0;
            long lCompleteDate = 0;
            long lCreateDate = 0;
            long lStartAt = 0;
            long lCounter = 0;
            bool bStatus = false; //Umesh
            bool isAttachTable = false;// nadim 11845
            XmlDocument objDOM = null;
            XmlNode objCShowActiveDiary = null; //sshrikrishna
            XmlDocument objUserPrefXML = null; //Umesh
            XmlDocument objXml = null;  //Umesh
            XmlNode objShowActiveDiary = null; //Umesh
            XmlNode objSaveShowActiveDiary = null; //Umesh
            XmlNode objNode = null;//Neha
            XmlElement objElem = null;
            LocalCache objCache = new LocalCache(m_sConnectionString,m_iClientId);
            SysSettings objSys = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud

            DbConnection objConn = null; //Umesh
            DataSet objDataSet = null;
            //DbReader objRdr1 = null;//Umesh
            DbCommand objCmd = null;//Umesh
            DbParameter objParam = null;//Umesh
            DataRow[] objRow = null;
            string sWhereTemp = "";
            string sSQLOne = string.Empty;
            string sSQL_WPA_ONE = string.Empty;
            StringBuilder sbSql = null;
            bool bIsNoColumnSelected = true;//Nitin for R6
            string sAttachID = string.Empty;  //pmittal5 Confidential Record
            string sPI = string.Empty;
            string sQueryFilter = string.Empty;
            //rupal:r8 auto diary enh
            string sClaimID = string.Empty;
            string[] sProgressNoteDetail;
            string[] sAttachPromptDetail;
            //rupal:end
            //Added by sharishkumar for mits 35291
            string[] sDependentDetail;
            string sDependentRowDetail;
            //End mits 35291
            string s_ModifyFilterExp = string.Empty;
            string sFilterDate = string.Empty;
            string sTableName = string.Empty; //30085
            string sTableName2 = string.Empty; //tkatsarski-MITS 36512
            string sFunctionName = string.Empty;//30085
            string sTableAliasName = string.Empty;//30085
            string sOrgHieColumnName = string.Empty;//30085
            long lOrgLevel = 0;
            string sSortColName = string.Empty;//Ankit-MITS 30085
            string sDeptColumnOne = string.Empty;
            string sDeptColumnTwo = string.Empty;
            bool isExtraAlias = false;      //Ankit Start : MITS 32559
            //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
            XmlNodeList tempNodes = null;
            XmlElement objDiarySuppNode = null;
            XmlElement objRowElement = null;
            string sSuppFieldData = string.Empty;
            int iFieldType = 0;
            bool blnSuccess = false;
            int iFieldID = 0;
            int iRecordID = 0;
            SupplementalFieldTypes enumSuppFieldType = 0;
            string s_SuppJoin = " LEFT OUTER JOIN WPA_DIARY_ENTRY_SUPP ON a.ENTRY_ID = WPA_DIARY_ENTRY_SUPP.ENTRY_ID ", s_SuppQuery = string.Empty;
            var lstSuppColumnNames = new List<string>();
            string s_SuppCommaSeparatedColumn = string.Empty;

            //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
            try
            {
                //pkandhari  Jira 6412 starts
                var ReqUserid = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sSecConnectString, "SELECT DISTINCT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '" + p_sUserName.ToString() + "'"), m_iClientId);
                //update group based on p_sUserName
                this.GroupId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " + ReqUserid.ToString()), m_iClientId);
                //pkandhari  Jira 6412 ends
                //Ashish Ahuja Mits 32961 Start
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, "Select PREF_XML from USER_PREF_XML where USER_ID=" + UserId.ToString()))
                {
                    if (objRdr.Read())
                    {
                        sUserPrefsXML = Common.Conversion.ConvertObjToStr(objRdr.GetValue("PREF_XML"));
                        bStatus = true;
                    }
                    //objRdr.Close();
                }
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();
                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }

                // Create the xml if it doesn't exist in the database
                CreateUserPreferXML(objUserPrefXML);
                //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
                if (sUserSortOrder != "0")
                {
                    //Neha
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/UserSortOrder/@value");
                    objNode.Value = sUserSortOrder;
                    this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(UserId));
                    bStatus = true;//JIRA RMA-618
                }//end Mits 23477

                //Deb: MITS 32961 
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/SortOrderBy/@value");
                if (objNode != null)
                {
                    if (p_iSortOrder == 0)
                    {
                        p_iSortOrder = int.Parse(objNode.Value);
                    }
                    else
                    {
                        if (Convert.ToString(p_iSortOrder) != objNode.Value)
                        {
                            objNode.Value = Convert.ToString(p_iSortOrder);
                            this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(UserId));
                            bStatus = true;//JIRA RMA-618
                        }
                    }
                }
                //MITS 36519
                string columnName = string.Empty;
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/SortExpression/@value");
                if (objNode != null)
                {
                    if (p_iSortOrder != 0)
                    {

                        columnName = GetColumnName(p_iSortOrder);
                        if (columnName != objNode.Value)
                        {
                            objNode.Value = columnName;
                            this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(UserId));
                            bStatus = true; //JIRA RMA-618
                        }
                    }
                }
                    
                    
                //Deb: MITS 32961 
                //Ashish Ahuja Mits 32961 End
                //Ankit Start : MITS 32559
                if (p_iSortOrder == 9 || p_iSortOrder == 13 || p_iSortOrder == 15 || p_iSortOrder == 16)
                    isExtraAlias = true;
                //Ankit End

                if (!string.IsNullOrEmpty(p_sFilterExpression))
                {
                    s_ModifyFilterExp = ModifyFilterexp(p_sFilterExpression, ref sFilterDate);
                    s_ModifyFilterExp = s_ModifyFilterExp.Replace("]", "").Replace("[", "");
                    if (!string.IsNullOrEmpty(sFilterDate))
                    {
                        //p_sDueDate = sFilterDate;
                        if (p_sUIDiarySource == "D")
                            sDiaryDueDate = sFilterDate;
                        else if (p_sUIDiarySource == "C")
                            sClaimDiaryDueDate = sFilterDate;
                    }
                    if (string.Equals(s_ModifyFilterExp, "( )"))
                    {
                        s_ModifyFilterExp = string.Empty;
                    }
                }
                //Sachin-Start changes for 27074-Custom Paging
                sbSql = new StringBuilder();
                this.PageSize = GetRecordsPerPage(); //to be used for custom paging
                //Sachin-End changes for 27074
                sQueryFilter = GetFilterForTaskname(sTaskType, sTaskName); //Neha Mits 23586
                lToday = Conversion.ConvertStrToLong(Conversion.GetDate(DateTime.Now.Date.ToString()));
                //Mukul Added ASSIGNED_USER in sql 2/7/2007 MITS 8782
                //rupal:tmp
                //Ankit-30085 Start
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                

                if ((p_iSortOrder == 15) || (p_iSortOrder == 16))
                {
                    //Start code to fetch org hierarchy level
                    sSQL = "SELECT DIARY_ORG_LEVEL FROM SYS_PARMS";
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        if (objReader.Read())
                            lOrgLevel = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("DIARY_ORG_LEVEL")));                        
                        else
                            lOrgLevel = 1012;
                    }
                    sOrgHieColumnName = GetDiaryOrgName(lOrgLevel);
                    //End.

                    if (objConn.DatabaseType == Db.eDatabaseType.DBMS_IS_SQLSRVR)
                    {
                        sDeptColumnOne = "ISNULL(f.first_name + ' ' + f.last_name,f.LAST_NAME)";
                        sDeptColumnTwo = "ISNULL(d.first_name + ' ' + d.last_name,d.LAST_NAME)";
                    }
                    else
                    {
                        sDeptColumnOne = " nvl(f.first_name || ' ' || f.last_name,f.LAST_NAME) ";
                        sDeptColumnTwo = " nvl(d.first_name || ' ' || d.last_name,d.LAST_NAME) ";
                    }
                    if (p_iSortOrder == 15)
                        sSortColName = " ASC";
                    else
                        sSortColName = " DESC";

                    sSQL = "select * from (select ROW_NUMBER() OVER(ORDER BY department" + sSortColName + ") as rno,ENTRY_ID,COMPLETE_DATE,IS_ATTACHED,ATTACH_TABLE,ATTACH_RECORDID,ATT_PARENT_CODE,NON_ROUTE_FLAG,NON_ROLL_FLAG,";
                    //MITS 36519 Added ATT_PARENT_CODE
                    sSQL = sSQL + "STATUS_OPEN,REGARDING,PRIORITY,ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,CREATE_DATE,ASSIGNED_USER,ASSIGNED_GROUP,DEPARTMENT s_SuppQuery FROM ";
                    //tkatsarski: 06/18/14 Start changes for Mits 36512
                    sSQL = sSQL + "(SELECT a.ENTRY_ID ,COMPLETE_DATE,IS_ATTACHED,ATTACH_TABLE,NON_ROUTE_FLAG,NON_ROLL_FLAG,ATTACH_RECORDID,ATT_PARENT_CODE,";//WWIG GAP20A - agupta298 - MITS 36804
                    //tkatsarski: End changes for Mits 36512
                    sSQL = sSQL + " STATUS_OPEN,REGARDING,PRIORITY,ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,CREATE_DATE,ASSIGNED_USER,ASSIGNED_GROUP,";
                    sSQL = sSQL + "CASE ATTACH_TABLE WHEN 'CLAIM' THEN " + sDeptColumnOne;
                    sSQL = sSQL + " WHEN 'EVENT' THEN " + sDeptColumnTwo + "END  DEPARTMENT s_SuppQuery from WPA_DIARY_ENTRY a ";//WWIG GAP20A - agupta298 - MITS 36804
                    sSQL = sSQL + "left outer join CLAIM b on a.ATTACH_RECORDID = b.CLAIM_ID AND a.ATTACH_TABLE = 'CLAIM' ";
                    sSQL = sSQL + "left outer join EVENT c on a.ATTACH_RECORDID = c.event_id and a.ATTACH_TABLE = 'EVENT'";
                    sSQL = sSQL + "left outer join EVENT e on e.EVENT_ID = b.EVENT_ID and ATTACH_TABLE = 'CLAIM' ";
                    sSQL = sSQL + "left outer join ORG_HIERARCHY g on g.DEPARTMENT_EID = e.DEPT_EID and a.ATTACH_TABLE = 'CLAIM' ";
                    sSQL = sSQL + "left outer join ORG_HIERARCHY h on h.DEPARTMENT_EID = c.DEPT_EID and a.ATTACH_TABLE = 'EVENT' ";
                    sSQL = sSQL + "left outer join ENTITY f on f.ENTITY_ID = g." + sOrgHieColumnName + " and a.ATTACH_TABLE = 'CLAIM' ";
                    sSQL = sSQL + "left outer join ENTITY d on d.ENTITY_ID = h." + sOrgHieColumnName + " and a.ATTACH_TABLE = 'EVENT' ";
                    sSQL = sSQL + "s_SuppJoin";//WWIG GAP20A - agupta298 - MITS 36804
                    
                    sWhere = "DIARY_VOID = 0 AND DIARY_DELETED = 0 ";
                    sSQLOne = sSQL;
                    sSortColName = "";
                }
                else if ((p_iSortOrder == 17) || (p_iSortOrder == 18))
                {
                    if (objConn.DatabaseType == Db.eDatabaseType.DBMS_IS_SQLSRVR)
                        sTableName = " AS dummy_tbl";

                    sSortColName = GetSortColumnName(p_iSortOrder);

                    sSQL = "SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY " + sSortColName + ") as rno,a.ENTRY_ID,COMPLETE_DATE,IS_ATTACHED,ATTACH_TABLE,ATTACH_RECORDID,ATT_PARENT_CODE,NON_ROUTE_FLAG,NON_ROLL_FLAG,STATUS_OPEN,REGARDING,PRIORITY,";//WWIG GAP20A - agupta298 - MITS 36804
                    sSQL = sSQL + "ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,CREATE_DATE,ASSIGNED_USER,ASSIGNED_GROUP s_SuppQuery FROM WPA_DIARY_ENTRY a ";//WWIG GAP20A - agupta298 - MITS 36804 //pkandhari Jira 6412
                    sSQL = sSQL + "LEFT OUTER JOIN (select CODE_DESC CLAIMSTATUS,a.*,b.* from CLAIM a,CODES_TEXT b where a.CLAIM_STATUS_CODE = b.CODE_ID) " + sTableName + " ON a.attach_recordid = claim_id AND a.attach_table= 'CLAIM' ";
                    sSQL = sSQL + " s_SuppJoin  ";//WWIG GAP20A - agupta298 - MITS 36804
                    sWhere = "DIARY_VOID = 0 AND DIARY_DELETED = 0 ";
                    sSQLOne = sSQL;
                    sSortColName = "";
                }
                else if ((p_iSortOrder == 10) || (p_iSortOrder == 14))
                {
                    //tkatsarski: 06/18/14 Mits 36512 Diary list does not sort properly on all fields
                    //tkatsarski: Changes for MITS 36512 start here
                    if (objConn.DatabaseType == Db.eDatabaseType.DBMS_IS_SQLSRVR)
                    {
                        sSortColName = "ISNULL(ISNULL(LAST_NAME+FIRST_NAME, LAST_NAME), FIRST_NAME) ";
                        sTableName = " AS dummy_tbl ";
                        sTableName2 = " AS dummy_tbl2 ";
                    }
                    else
                    {
                        sSortColName = "nvl(nvl(CONCAT(LAST_NAME,FIRST_NAME), LAST_NAME), FIRST_NAME) ";
                    }

                    if (p_iSortOrder == 10)
                        sSortColName = sSortColName + " ASC";
                    else
                        sSortColName = sSortColName + " DESC";

                    //tkatsarski: 02/04/15 - RMA-1601
                    sSQL = sSQL + "SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY " + sSortColName + ") AS rno, ENTRY_ID, COMPLETE_DATE, IS_ATTACHED, ATTACH_TABLE,NON_ROUTE_FLAG,NON_ROLL_FLAG, ";
                    sSQL = sSQL + "ATT_PARENT_CODE, ATTACH_RECORDID, STATUS_OPEN, REGARDING, PRIORITY, ASSIGNING_USER, ENTRY_NAME, ENTRY_NOTES, ATT_SEC_REC_ID, CREATE_DATE, ASSIGNED_USER, FIRST_NAME,ASSIGNED_GROUP , ";
                    sSQL = sSQL + " LAST_NAME s_SuppQuery  FROM( SELECT ENTRY_ID, COMPLETE_DATE, IS_ATTACHED, ATTACH_TABLE, NON_ROUTE_FLAG,NON_ROLL_FLAG, ATT_PARENT_CODE, ATTACH_RECORDID, STATUS_OPEN, REGARDING, PRIORITY, ASSIGNING_USER, ";
                    sSQL = sSQL + "ENTRY_NAME, ENTRY_NOTES, ATT_SEC_REC_ID, CREATE_DATE, ASSIGNED_USER, FIRST_NAME_J AS FIRST_NAME, LAST_NAME_J AS LAST_NAME, ";
                    sSQL = sSQL + "DIARY_VOID, DIARY_DELETED, ASSIGNED_GROUP s_SuppQuery ";
                    sSQL = sSQL + "FROM (SELECT * FROM(SELECT a.ENTRY_ID, COMPLETE_DATE, IS_ATTACHED, ATTACH_TABLE,NON_ROUTE_FLAG,NON_ROLL_FLAG, ATT_PARENT_CODE, ATTACH_RECORDID, STATUS_OPEN, REGARDING, PRIORITY, ";
                    sSQL = sSQL + "ASSIGNING_USER, ENTRY_NAME, ENTRY_NOTES, ATT_SEC_REC_ID, CREATE_DATE, ASSIGNED_USER, DIARY_VOID, DIARY_DELETED, ASSIGNED_GROUP ";
                    sSQL = sSQL + "s_SuppQuery FROM WPA_DIARY_ENTRY a ";
                    sSQL = sSQL + "LEFT OUTER JOIN (SELECT b.PRIMARY_CLMNT_FLAG, b.claim_id as claimid, c.first_name as first_name, c.last_name as last_name ";
                    sSQL = sSQL + "FROM claimant b, entity c  ";
                    sSQL = sSQL + "WHERE b.claimant_eid = c.entity_id and b.PRIMARY_CLMNT_FLAG = -1) " + sTableName;
                    sSQL = sSQL + "ON a.attach_recordid = claimid and a.attach_table = 'CLAIM' s_SuppJoin)" + sTableName2;
                    sSQL = sSQL + "LEFT OUTER JOIN (SELECT FIRST_NAME as FIRST_NAME_J, LAST_NAME as LAST_NAME_J, CLAIM_ID FROM ";
                    sSQL = sSQL + "(SELECT CLAIM_ID, FIRST_NAME, LAST_NAME,ROW_NUMBER() OVER(PARTITION BY CLAIM_ID ORDER BY CLAIM_ID DESC) AS rk ";
                    sSQL = sSQL + "FROM (SELECT MIN(CLAIMANT.CLAIM_ID) AS CLAIM_ID,ENTITY.LAST_NAME as LAST_NAME, ENTITY.FIRST_NAME as FIRST_NAME FROM CLAIMANT, ENTITY ";
                    sSQL = sSQL + "WHERE  CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID GROUP BY CLAIM_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME)" + sTableName + ") h WHERE h.rk = 1) ";
                    sSQL = sSQL + sTableName + " ON ATTACH_RECORDID = CLAIM_ID and attach_table = 'CLAIM'   ) ";
                    sSQL = sSQL + sTableName + ")" + sTableName + " ";

                    //tkatsarski: Changes for MITS 36512 end here

                    sWhere = "DIARY_VOID = 0 AND DIARY_DELETED = 0 ";
                    sSQLOne = sSQL;
                    sSortColName = "";
                }
                //Ankit-end changes
                else
                {
                    //Ankit-MITS 30085-Start Changes
                    if (p_iSortOrder != 0)
                        sSortColName = GetSortColumnName(p_iSortOrder);
                    else
                        sSortColName = "ENTRY_ID DESC";
                    //Ankit-MITS 30085 End changes

                    //Raman 09/29/2014 : setting default sort column name
                    if (sSortColName == String.Empty)
                    {
                        sSortColName = "ENTRY_ID DESC";
                    }

                    //Ankit:Start changes for MITS 30085
                    //sSQL = "SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY " + sSortColName + ") as rno,ENTRY_ID,COMPLETE_DATE,IS_ATTACHED,ATTACH_TABLE,ATTACH_RECORDID,ATT_PARENT_CODE,STATUS_OPEN,REGARDING,PRIORITY,ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,CREATE_DATE,ASSIGNED_USER,NON_ROUTE_FLAG  NON$ROUTE$FLAG,NON_ROLL_FLAG NON$ROLL$FLAG FROM WPA_DIARY_ENTRY "; //igupta3 jira 439
                    sSQL = "SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY " + sSortColName + ") as rno,a.ENTRY_ID,COMPLETE_DATE,IS_ATTACHED,ATTACH_TABLE,ATTACH_RECORDID,ATT_PARENT_CODE,STATUS_OPEN,REGARDING,PRIORITY,ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,CREATE_DATE,ASSIGNED_USER,ASSIGNED_GROUP,NON_ROUTE_FLAG  NON$ROUTE$FLAG,NON_ROLL_FLAG NON$ROLL$FLAG s_SuppQuery  FROM WPA_DIARY_ENTRY a s_SuppJoin "; //pkandhari Jira 6412
                    //Ankit-End changes for MITS 30085
                    sWhere = "DIARY_VOID = 0 AND DIARY_DELETED = 0 ";
                    //Ankit MITS 30085
                    //sSQLOne = "SELECT * FROM ((SELECT ROW_NUMBER() OVER(ORDER BY " + sSortColName + ") as rno,ENTRY_ID,COMPLETE_DATE,IS_ATTACHED,ATTACH_TABLE,ATTACH_RECORDID,ATT_PARENT_CODE,STATUS_OPEN,REGARDING,PRIORITY,ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,CREATE_DATE,ASSIGNED_USER,NON_ROUTE_FLAG NON$ROUTE$FLAG,NON_ROLL_FLAG NON$ROLL$FLAG FROM WPA_DIARY_ENTRY "; //igupta3 jira 439
                    sSQLOne = "SELECT * FROM ((SELECT ROW_NUMBER() OVER(ORDER BY " + sSortColName + ") as rno,a.ENTRY_ID ,COMPLETE_DATE,IS_ATTACHED,ATTACH_TABLE,ATTACH_RECORDID,ATT_PARENT_CODE,STATUS_OPEN,REGARDING,PRIORITY,ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,CREATE_DATE,ASSIGNED_USER,ASSIGNED_GROUP,NON_ROUTE_FLAG NON$ROUTE$FLAG,NON_ROLL_FLAG NON$ROLL$FLAG s_SuppQuery  FROM WPA_DIARY_ENTRY a s_SuppJoin "; //pkandhari Jira 6412

                    if (objConn.DatabaseType == Db.eDatabaseType.DBMS_IS_SQLSRVR)
                    {
                        sSQL = sSQL.Replace("NON_ROUTE_FLAG", "ISNULL(NON_ROUTE_FLAG,0)").Replace("NON_ROLL_FLAG", "ISNULL(NON_ROLL_FLAG,0)");
                        sSQLOne = sSQLOne.Replace("NON_ROUTE_FLAG", "ISNULL(NON_ROUTE_FLAG,0)").Replace("NON_ROLL_FLAG", "ISNULL(NON_ROLL_FLAG,0)"); 
                    }
                    else
                    {
                        sSQL = sSQL.Replace("NON_ROUTE_FLAG", "NVL(NON_ROUTE_FLAG,0)").Replace("NON_ROLL_FLAG", "NVL(NON_ROLL_FLAG,0)");
                        sSQLOne = sSQLOne.Replace("NON_ROUTE_FLAG", "NVL(NON_ROUTE_FLAG,0)").Replace("NON_ROLL_FLAG", "NVL(NON_ROLL_FLAG,0)");
                    }
                    sSQL = sSQL.Replace("$", "_");
                    sSQLOne = sSQLOne.Replace("$", "_");
                    //Ankit-End changes
                //Geeta 11/17/06 : Code added for fixing mits number 8139 for Work Activity
                if (p_iSortOrder == 9 || p_iSortOrder == 13)
                {
                        if (objConn.DatabaseType == Db.eDatabaseType.DBMS_IS_SQLSRVR)
                        {
                            sFunctionName = "ISNULL";
                            sTableAliasName = "tbl";                            
                        }
                        else
                            sFunctionName = "NVL";                            
                            
                        //Ankit:Start changes for MITS 30085
                        sSQL = "select * from (select row_number() OVER (ORDER by " + sSortColName +" ) as rno,ACT_TEXT,ENTRY_ID,COMPLETE_DATE,IS_ATTACHED,ATTACH_TABLE,";
                        //Commented for WWIG GAP20A - agupta298 - MITS 36804 - New Query re-written
                        //MITS 31146 Raman    
                        //tkatsarski: 02/04/15 - RMA-1601
                        sSQL = sSQL + " ATTACH_RECORDID,ATT_PARENT_CODE,STATUS_OPEN,REGARDING,PRIORITY,ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,CREATE_DATE,ASSIGNED_USER,NON_ROUTE_FLAG,NON_ROLL_FLAG,work_act,ASSIGNED_GROUP s_SuppQuery from (SELECT LOWER(" + sFunctionName + "(act_text,'')) as work_act,ACT_TEXT,a.ENTRY_ID,COMPLETE_DATE,IS_ATTACHED,ATTACH_TABLE,NON_ROUTE_FLAG,NON_ROLL_FLAG,";
                        // sSQL = sSQL + " ATTACH_RECORDID,STATUS_OPEN,REGARDING,PRIORITY,ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,CREATE_DATE,ASSIGNED_USER from (SELECT LOWER(" + sFunctionName + "(act_text,'No Work Activity')) as work_act,ACT_TEXT,ENTRY_ID,COMPLETE_DATE,IS_ATTACHED,ATTACH_TABLE,";
                        sSQL = sSQL + " ATTACH_RECORDID,ATT_PARENT_CODE,STATUS_OPEN,REGARDING,PRIORITY,ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,CREATE_DATE,ASSIGNED_USER,ASSIGNED_GROUP s_SuppQuery FROM WPA_DIARY_ENTRY a LEFT JOIN ";
                        sSQL = sSQL + " WPA_DIARY_ACT ON a.ENTRY_ID = WPA_DIARY_ACT.PARENT_ENTRY_ID  s_SuppJoin ";

                        sSQLOne = sSQL;
                }
                //MITS 11998 Abhishek added sShowAllDiariesForActiveRec!="no"
                    sSortColName = "";
                }
                if (p_sAttachTable.Trim() != "" && p_sAttachRecordId.Trim() != "" && p_sAttachRecordId.Trim() != "0" && sShowAllDiariesForActiveRec != "no")
                    bIsAttached = true;

                switch (sFunction)
                {
                    case "peek":
                        if (p_sUserName.Trim() != "")
                        {
                            sWhere = sWhere + " AND (ASSIGNED_USER = '" + p_sUserName + "' OR ASSIGNED_GROUP = '" + m_iGroupId.ToString() + "')";
                        }
                        break;
                    default:

                        //Parijat: Mits 7171 --the changes were reverted back since form.js has been changed to handle the same
                        //if(objSys.AttachDiaryVis == false)
                        if (bIsAttached == false || objSys.AttachDiaryVis == false ||
                            (objSys.AttachDiaryVis == true && p_sAttachTable.Trim() == ""))
                        {
                            if (p_sUserName.Trim() != "")
                            {
                                sWhere = sWhere + " AND (ASSIGNED_USER = '" + p_sUserName + "' OR ASSIGNED_GROUP = '" + m_iGroupId.ToString() + "')";
                            }
                        }

                        break;
                }



                //PSARIN2 R8 Perf Start******************************************************************************************************/
                //By default the due date should be current date.. however the code was not cdonsidering current date.. changed by raman bhatia
                //01/10/2007 REM Umesh
                //Ashish Ahuja Mits 32961 Start
                //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, "Select PREF_XML from USER_PREF_XML where USER_ID=" + UserId.ToString()))
                //{
                //    if (objRdr.Read())
                //    {
                //        sUserPrefsXML = Common.Conversion.ConvertObjToStr(objRdr.GetValue("PREF_XML"));
                //        bStatus = true;
                //    }
                //    //objRdr.Close();
                //}
                //sUserPrefsXML = sUserPrefsXML.Trim();
                //objUserPrefXML = new XmlDocument();
                //if (sUserPrefsXML != "")
                //{
                //    objUserPrefXML.LoadXml(sUserPrefsXML);
                //}

                //// Create the xml if it doesn't exist in the database
                //CreateUserPreferXML(objUserPrefXML);
                ////Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
                //if (sUserSortOrder != "0")
                //{
                //    //Neha
                //    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/UserSortOrder/@value");
                //    objNode.Value = sUserSortOrder;
                //    this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(UserId));
                //}//end Mits 23477

                ////Deb: MITS 32961 
                //objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/SortOrderBy/@value");
                //if (objNode != null)
                //{
                //    if (p_iSortOrder == 0)
                //    {
                //        p_iSortOrder = int.Parse(objNode.Value);
                //    }
                //    else
                //    {
                //        if (Convert.ToString(p_iSortOrder) != objNode.Value)
                //        {
                //            objNode.Value = Convert.ToString(p_iSortOrder);
                //            this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(UserId));
                //        }
                //    }
                //}
                //Deb: MITS 32961 
                //Ashish Ahuja Mits 32961 End
                //tkr 6/2007 HACK
                //<ShowNotes> node value is "0" in instance.xml
                //if this is first time called (showNotes=="0"), then fetch the UserPref and set the checkbox to it
                //if this is a postback (showNotes=="1" or showNotes=="") reset the UserPref
                XmlNode nodeShowNotes = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ShowNotes/@selected");

                //if(showNotes.Equals("0"))
                if (showNotes.Equals(""))       //csingh7 MITS 18435 :if this is first time called (showNotes==""), then fetch the UserPref and set the checkbox to it
                {
                    showNotes = nodeShowNotes.InnerText;
                } // if
                else
                {                                //csingh7 MITS 18435 :if this is a postback (showNotes=="1" or showNotes=="0") reset the UserPref
                    if (showNotes.CompareTo(nodeShowNotes.InnerText) != 0)
                        nodeShowNotes.InnerText = showNotes;
                } // else
                //Added by Amitosh for Terelik Grid UI

                XmlNode nodeShowRegarding = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ShowRegarding/@selected");
                if (p_sShowRegarding.Equals(""))
                {
                    p_sShowRegarding = nodeShowRegarding.InnerText;
                }
                else
                {
                    if (p_sShowRegarding.CompareTo(nodeShowRegarding.InnerText) != 0)
                    {
                        nodeShowRegarding.InnerText = p_sShowRegarding;
                    }
                }
                //End Amitosh
                objShowActiveDiary = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ShowActiveDiaryChecked/@selected");

                //sachin:Start changes for MITS 27074
                objCShowActiveDiary = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ShowCActiveDiaryChecked/@selected");
                //sachin:End Changes

                //Ankit Start : Working on MITS - 30730 (RO)
                long lDueDate = 0;
                if (!string.IsNullOrEmpty(sDiaryDueDate) && !string.Equals(sDiaryDueDate, "today", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        lDueDate = Convert.ToInt64(sDiaryDueDate.Trim());
                    }
                    catch (Exception ex)
                    {
                        lDueDate = 0;
                    }
                    if (string.IsNullOrEmpty(p_sActiveDiaryChecked) && lDueDate > lToday)
                        sDiaryDueDate = "today";

                }
                if (!string.IsNullOrEmpty(sClaimDiaryDueDate) && !string.Equals(sClaimDiaryDueDate, "today", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        lDueDate = Convert.ToInt64(sClaimDiaryDueDate.Trim());
                    }
                    catch (Exception ex)
                    {
                        lDueDate = 0;
                    }
                    if (string.IsNullOrEmpty(p_sClaimActiveDiaryChecked) && lDueDate > lToday)
                        sClaimDiaryDueDate = "today";

                }
                //Ankit End
                if (!p_bReqActDiary)
                {
                    if (objShowActiveDiary != null)
                    {
                        if (objShowActiveDiary.InnerText == "1")
                        {
                            //p_sDueDate = "";
                            //Ankit Start : Working on MITS - 30730 (RO)
                            if(string.Equals(sDiaryDueDate,"today",StringComparison.OrdinalIgnoreCase))
                                sDiaryDueDate = "";
                            //Ankit End
                            sDiarySource.Append("D");//sshrikrishna:MITS 27074
                        }
                    }
                    //sshrikrishna:Start changes for MITS 27074
                    if (objCShowActiveDiary != null)
                    {
                        if (objCShowActiveDiary.InnerText == "1")
                        {
                            //p_sDueDate = "";                          
                            sClaimDiaryDueDate = "";
                            sDiarySource.Append("C");
                        }
                    }
                    //sshrikrishna:End changs for MITS 27074
                }
                objXml = new XmlDocument();
                string strContent = RMSessionManager.GetCustomSettings(m_iClientId);

                if (!string.IsNullOrEmpty(strContent))
                {
                    objXml.LoadXml(strContent);
                    //objRdr.Dispose();
                    objSaveShowActiveDiary = objXml.SelectSingleNode("//Other/SaveShowActiveDiary");
                    if (objSaveShowActiveDiary != null)
                    {
                        if (objSaveShowActiveDiary.InnerText == "-1")
                        {
                            if (p_bReqActDiary)
                            {
                                  //sshrikrishna:Start changes MITS 27074
                                  //  if (p_sActiveDiaryChecked == "")
                                    if (p_sActiveDiaryChecked == "" || p_sActiveDiaryChecked == "0")
                                    objShowActiveDiary.Value = "0";
                                else
                                    objShowActiveDiary.Value = "1";
                                    if (p_sClaimActiveDiaryChecked == "" || p_sClaimActiveDiaryChecked == "0")
                                        objCShowActiveDiary.Value = "0";
                                    else
                                        objCShowActiveDiary.Value = "1";
                                    //sshrikrishna:End changes
                            }
                        }
                    }

                }



                sUserPrefsXML = objUserPrefXML.InnerXml.ToString();
                objConn = DbFactory.GetDbConnection(m_sConnectionString);

                objConn.Open();
                sbSql = new StringBuilder();
                objCmd = objConn.CreateCommand();
                objParam = objCmd.CreateParameter();
                objParam.Direction = ParameterDirection.Input;
                objParam.Value = sUserPrefsXML;
                objParam.ParameterName = "XML";
                objParam.SourceColumn = "PREF_XML";
                objCmd.Parameters.Add(objParam);
                if (bStatus)
                    sbSql.Append("UPDATE USER_PREF_XML SET PREF_XML=~XML~ WHERE USER_ID=" + UserId);
                else
                    sbSql.Append("INSERT INTO USER_PREF_XML VALUES(" + UserId + ",~XML~)");
                objCmd.CommandText = sbSql.ToString();
                objCmd.ExecuteNonQuery();
                //End REM UMESH

                //PSARIN2 R8 Perf End*************************************************************************************************************/
                    //sshrikrishna:Start changes MITS 27074
                //if (p_sDueDate == "today") p_sDueDate = lToday.ToString();
                //sDueDate = Conversion.GetDate(p_sDueDate);
                    if (sDiaryDueDate == "today")
                    {
                        sDiaryDueDate = lToday.ToString();
                    }
                    if (sClaimDiaryDueDate == "today")
                    {
                        sClaimDiaryDueDate = lToday.ToString();
                    }
                    sDiaryDueDate = Conversion.GetDate(sDiaryDueDate);
                    sClaimDiaryDueDate = Conversion.GetDate(sClaimDiaryDueDate);
                    //sshrikrishna:End changes MITS 27074
                    //sshrikrishna:Start changes MITS 27074
                if (p_sUIDiarySource == "D" && !sDiaryDueDate.Trim().Equals(""))
                    sWhere = sWhere + " AND COMPLETE_DATE <= '" + sDiaryDueDate + "'";
                else if (p_sUIDiarySource == "C" && !sClaimDiaryDueDate.Trim().Equals(""))
                    sWhere = sWhere + " AND COMPLETE_DATE <= '" + sClaimDiaryDueDate + "'";
                    //sshrikrishna:End changes
                if (p_bOpen)
                {
                    sWhere = sWhere + " AND STATUS_OPEN <> 0 ";
                }
                else
                {
                    sWhere = sWhere + " AND STATUS_OPEN = 0";
                }
                sSQL_WPA = sSQL + " WHERE " + sWhere; //Neha Mits 23586
                sSQL_WPA_ONE = sSQL + " WHERE " + sWhere;
                Db.DbConnection dbConn = DbFactory.GetDbConnection(m_sConnectionString);
                //MITS 11998 Abhishek added sShowAllDiariesForActiveRec=="yes" condition
                if (sAllClaimDiaries == "yes" && p_sAttachRecordId.Trim() != "" && p_sAttachRecordId.Trim() != "0" && p_sAttachTable.Trim() == "CLAIM" && sShowAllDiariesForActiveRec == "yes")
                {
                    iCheckBoxChecked = 1;
                    //int i = 0;    //tkatsarski: 01/27/15 - RMA-1601
                    DataModelFactory objDMF = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
                    Claim objClaim = (Claim)objDMF.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(Conversion.ConvertStrToInteger(p_sAttachRecordId));
                    Event objEvent = (Event)objDMF.GetDataModelObject("Event", false);
                    objEvent.MoveTo(objClaim.EventId);


                    //Ankit Start : Worked on MITS - 29939
                    sWhere1 = sWhere + " AND ATTACH_TABLE='CLAIM'";
                    sWhere1 = sWhere1 + " AND ATTACH_RECORDID = " + objClaim.ClaimId;
                    //sWhere1 = sWhere;
                    //Ankit End
                    sWhere1 = sWhere1 + " AND IS_ATTACHED <> 0";
                    sWhere1 = sWhere1.Trim();
                    //Ankit MITS 27074 Start changes
                    //sSQL = "(" + sSQL + " WHERE " + sWhere1 + sQueryFilter; //Neha Mits 23586
                    if (s_ModifyFilterExp != string.Empty)
                        sSQL = sSQL + " WHERE " + sWhere1 + " AND " + s_ModifyFilterExp + sQueryFilter;
                    else
                        sSQL = sSQL + " WHERE " + sWhere1 + sQueryFilter; 
                    //Ankit-MITS 27074 end changes
                    sSQLOne = sSQLOne + " WHERE " + sWhere1 + ")";

                    #region Getting Claim Info
                    DbReader objReader;

                    sSQL1 = "SELECT TRANS_ID FROM FUNDS WHERE CLAIM_ID=" + objClaim.ClaimId;

                    objEvent = (objClaim.Parent as Event);
                    //tkatsarski: 01/27/15 - Start changes for RMA-1601: sClaimDetails is populated dynamically, so we don't need to calculate iCount 
                    //objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL1);
                    //if (objReader != null)
                    //{
                    //    while (objReader.Read())
                    //    {
                    //        //sClaimDetails[i,0]=objReader.GetValue("TRANS_ID").ToString();
                    //        //sClaimDetails[i,1]="FUNDS";
                    //        iCount++;
                    //    }
                    //}
                    //objReader = null;
                    //iCount = iCount + objClaim.AdjusterList.Count;
                    //iCount = iCount + objClaim.ClaimantList.Count;
                    //iCount = iCount + objClaim.DefendantList.Count;
                    //iCount = iCount + objClaim.LitigationList.Count;
                    //// kkaur8 Added for MITS 32448 Starts
                    //iCount = iCount + objClaim.SubrogationList.Count;
                    //iCount = iCount + objClaim.PropertyLossList.Count;
                    //// kkaur8 Added for MITS 32448 Ends
                    //objEvent = (objClaim.Parent as Event);
                    //iCount = iCount + objEvent.PiList.Count;                                       
                    //foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                    //{
                    //    iCount = iCount + objAdjuster.AdjustDatedTextList.Count;
                    //}
                    //
                    ////added by Nitin for Mits 12304 on 13-May-2009
                    //foreach (ClaimXLitigation objLitigation in objClaim.LitigationList)
                    //{
                    //    iCount = iCount + objLitigation.ExpertList.Count;
                    //}
                    //
                    ////added by Nitin for Mits 12304 on 13-May-2009
                    //// in case claim VA only
                    //// akaushik5 Changed for MITS 32448 Starts
                    ////if (objClaim.LineOfBusCode == 242)
                    //if (objClaim.LineOfBusCode == 242 || objClaim.LineOfBusCode.Equals(241))
                    //// akaushik5 Changed for MITS 32448 Ends
                    //{
                    //    iCount = iCount + objClaim.UnitList.Count;
                    //
                    //    // kkaur8 Added for MITS 32448 Starts
                    //    foreach (UnitXClaim objUnit in objClaim.UnitList)
                    //    {
                    //        iCount = iCount + objUnit.SalvageList.Count;
                    //    }
                    //    // kkaur8 Added for MITS 32448 Ends
                    //}
                    //
                    //// kkaur8 Added for MITS 32448 Starts
                    //foreach (ClaimXPropertyLoss objPropertyLoss in objClaim.PropertyLossList)
                    //{
                    //    iCount = iCount + objPropertyLoss.SalvageList.Count;
                    //}
                    //// kkaur8 Added for MITS 32448 Ends
                    ////zmohammad : Merging 37022 into CDR.
                    //// akaushik5 Added for MITS 37022 Starts
                    //// Since Case Management applicable for DI and WC, Diary records are fetched for these LOB's 
                    ////Commented for rma 1598,1599
                    ////if (objClaim.LineOfBusCode.Equals(243) || objClaim.LineOfBusCode.Equals(844))
                    //{
                    //    PiEmployee objEmployee = (PiEmployee)objDMF.GetDataModelObject("PiEmployee", false);
                    //    foreach (PersonInvolved objPerson in objEvent.PiList)
                    //    {
                    //        objEmployee.MoveTo(objPerson.PiRowId);
                    //        if (objEmployee.CaseManagementList != null && objEmployee.CaseManagementList.Count > 0)
                    //        {
                    //            iCount = iCount + objEmployee.CaseManagementList.Count;
                    //            foreach (CaseManagement objCaseManagement in objEmployee.CaseManagementList)
                    //            {
                    //                iCount += objCaseManagement.CaseMgrHistList.Count;
                    //                foreach (CmXAccommodation objCmXAccommodation in objCaseManagement.AccommodationList)
                    //                {
                    //                    iCount += iCount + objCaseManagement.AccommodationList.Count;
                    //                }
                    //                foreach (CmXVocrehab objCmXVocrehab in objCaseManagement.VocRehabList)
                    //                {
                    //                    iCount += iCount + objCaseManagement.VocRehabList.Count;
                    //                }
                    //                //rma 1598
                    //                foreach (CmXTreatmentPln objCmXTreatmentPln in objCaseManagement.TreatmentPlanList)
                    //                {
                    //                    iCount += iCount + objCaseManagement.TreatmentPlanList.Count;
                    //                }                                  
                    //            }
                    //        }
                    //    }
                    //
                    //    //if (objClaim.CaseManagement != null && objClaim.CaseManagement.CaseMgrHistList != null && objClaim.CaseManagement.CaseMgrHistList.Count > 0)
                    //    //{
                    //    //    iCount += objClaim.CaseManagement.CaseMgrHistList.Count;
                    //    //}
                    //}
                    //// akaushik5 Added for MITS 37022 Ends

                    //tkatsarski: 01/27/15 - RMA-1601: The type "string[,]" is changed to "List<KeyValuePair<string, string>>", so the list can be dynamically filled
                    //string[,] sClaimDetails = new string[iCount, 2];     
                    List<KeyValuePair<string, string>> sClaimDetails = new List<KeyValuePair<string, string>>();

                    foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                    {
                        //tkatsarski: 01/27/15 - RMA-1601
                        //sClaimDetails[i, 0] = objAdjuster.AdjRowId.ToString();
                        //sClaimDetails[i, 1] = "ADJUSTER";
                        //i++;
                        sClaimDetails.Add(new KeyValuePair<string, string>(objAdjuster.AdjRowId.ToString(), "ADJUSTER"));
                    }

                    foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                    {
                        foreach (AdjustDatedText objDatedText in objAdjuster.AdjustDatedTextList)
                        {
                            //tkatsarski: 01/27/15 - RMA-1601
                            //sClaimDetails[i, 0] = objDatedText.AdjDttextRowId.ToString();
                            //sClaimDetails[i, 1] = "ADJUSTERDATEDTEXT";
                            //i++;
                            sClaimDetails.Add(new KeyValuePair<string, string>(objDatedText.AdjDttextRowId.ToString(), "ADJUSTERDATEDTEXT"));
                        }
                    }

                    foreach (Claimant objClaimant in objClaim.ClaimantList)
                    {
                        //commented by Nitin for Mits 12304 on 13-May-2009
                        //sClaimDetails[i, 0] = objClaimant.ClaimantEid.ToString();
                        //added by Nitin for Mits 12304 on 13-May-2009
                        //tkatsarski: 01/27/15 - RMA-1601
                        //sClaimDetails[i, 0] = objClaimant.ClaimantRowId.ToString();
                        //sClaimDetails[i, 1] = "CLAIMANT";
                        //i++;
                        sClaimDetails.Add(new KeyValuePair<string, string>(objClaimant.ClaimantRowId.ToString(), "CLAIMANT"));
                    }
                    foreach (Defendant objDefendant in objClaim.DefendantList)
                    {
                        //tkatsarski: 01/27/15 - RMA-1601
                        //sClaimDetails[i, 0] = objDefendant.DefendantRowId.ToString();
                        //sClaimDetails[i, 1] = "DEFENDANT";
                        //i++;
                        sClaimDetails.Add(new KeyValuePair<string, string>(objDefendant.DefendantRowId.ToString(), "DEFENDANT"));
                    }

                    //starts by Nitin for Mits 12304 on 13-May-2009
                    //for Litigation
                    foreach (ClaimXLitigation objLitigation in objClaim.LitigationList)
                    {
                        //tkatsarski: 01/27/15 - RMA-1601
                        //sClaimDetails[i, 0] = objLitigation.LitigationRowId.ToString();
                        //sClaimDetails[i, 1] = "LITIGATION";
                        //i++;
                        sClaimDetails.Add(new KeyValuePair<string, string>(objLitigation.LitigationRowId.ToString(), "LITIGATION"));
                    }

                    //for Experts
                    foreach (ClaimXLitigation objLitigation in objClaim.LitigationList)
                    {
                        foreach (Expert objExpert in objLitigation.ExpertList)
                        {
                            //tkatsarski: 01/27/15 - RMA-1601
                            //sClaimDetails[i, 0] = objExpert.ExpertRowId.ToString();
                            //sClaimDetails[i, 1] = "EXPERT";
                            //i++;
                            sClaimDetails.Add(new KeyValuePair<string, string>(objExpert.ExpertRowId.ToString(), "EXPERT"));
                        }
                    }

                    // in case claim VA only
                    // akaushik5 Changed for MITS 32448 Starts
                    //if (objClaim.LineOfBusCode == 242)
                    if (objClaim.LineOfBusCode == 242 || objClaim.LineOfBusCode.Equals(241))
                    // akaushik5 Changed for MITS 32448 Ends
                    {
                        //for Unit
                        foreach (UnitXClaim objUnit in objClaim.UnitList)
                        {
                            //tkatsarski: 01/27/15 - RMA-1601
                            //sClaimDetails[i, 0] = objUnit.UnitRowId.ToString();
                            //sClaimDetails[i, 1] = "UNIT";
                            //i++;
                            sClaimDetails.Add(new KeyValuePair<string, string>(objUnit.UnitRowId.ToString(), "UNIT"));
                        }
                        
                        // kkaur8 Added for MITS 32448 Starts
                        foreach (UnitXClaim objUnit in objClaim.UnitList)
                        {
                            foreach (Salvage objSalvage in objUnit.SalvageList)
                            {
                                //tkatsarski: 01/27/15 - RMA-1601
                                //sClaimDetails[i, 0] = objSalvage.SalvageRowId.ToString();
                                //sClaimDetails[i, 1] = "SALVAGE";
                                //i++;
                                sClaimDetails.Add(new KeyValuePair<string, string>(objSalvage.SalvageRowId.ToString(), "SALVAGE"));
                            }
                        }
                        // kkaur8 Added for MITS 32448 Ends
                    }
                    // kkaur8 Added for MITS 32448 Starts 
                    foreach (ClaimXPropertyLoss objPropertyLoss in objClaim.PropertyLossList)
                    {
                        //tkatsarski: 01/27/15 - RMA-1601
                        //sClaimDetails[i, 0] = objPropertyLoss.RowId.ToString();
                        //sClaimDetails[i, 1] = "PROPERTYLOSS";
                        //i++;
                        sClaimDetails.Add(new KeyValuePair<string, string>(objPropertyLoss.RowId.ToString(), "PROPERTYLOSS"));
                    }

                    foreach (ClaimXPropertyLoss objPropertyLoss in objClaim.PropertyLossList)
                    {
                        foreach (Salvage objSalvage in objPropertyLoss.SalvageList)
                        {
                            //tkatsarski: 01/27/15 - RMA-1601
                            //sClaimDetails[i, 0] = objSalvage.SalvageRowId.ToString();
                            //sClaimDetails[i, 1] = "SALVAGE";
                            //i++;
                            sClaimDetails.Add(new KeyValuePair<string, string>(objSalvage.SalvageRowId.ToString(), "SALVAGE"));
                        }
                    }

                    foreach (ClaimXSubrogation objSubrogation in objClaim.SubrogationList)
                    {
                        //tkatsarski: 01/27/15 - RMA-1601
                        //sClaimDetails[i, 0] = objSubrogation.SubrogationRowId.ToString();
                        //sClaimDetails[i, 1] = "SUBROGATION";
                        //i++;
                        sClaimDetails.Add(new KeyValuePair<string, string>(objSubrogation.SubrogationRowId.ToString(), "SUBROGATION"));
                    }

                    //tkatsarski: 01/12/15 - Start changes for RMA-1601: Diaries are displayed for Medical Mgt Savings and Auto Schedule Checks
                    if (objClaim.CaseManagement != null && objClaim.CaseManagement.MedMgtSavingsList != null)
                    {
                        foreach (CmXMedmgtsavings item in objClaim.CaseManagement.MedMgtSavingsList)
                        {
                            sClaimDetails.Add(new KeyValuePair<string, string>(item.CmmsRowId.ToString(), "CMXMEDMGTSAVINGS"));
                        }
                    }

                    {
                        sSQL2 = "SELECT FUNDS_AUTO.AUTO_BATCH_ID";
                        sSQL2 = sSQL2 + " FROM FUNDS_AUTO, FUNDS_AUTO_BATCH, CODES_TEXT";
                        sSQL2 = sSQL2 + " WHERE FUNDS_AUTO.AUTO_BATCH_ID = FUNDS_AUTO_BATCH.AUTO_BATCH_ID AND PAYEE_TYPE_CODE = CODES_TEXT.CODE_ID";
                        sSQL2 = sSQL2 + " AND FUNDS_AUTO_BATCH.DISABILITY_FLAG <> -1 AND AUTO_TRANS_ID IN (SELECT MIN(AUTO_TRANS_ID) FROM FUNDS_AUTO GROUP BY AUTO_BATCH_ID) ";
                        sSQL2 = sSQL2 + " AND FUNDS_AUTO.CLAIM_ID = '" + objClaim.ClaimId + "'";
                        sSQL2 = sSQL2 + " ORDER BY FUNDS_AUTO.AUTO_BATCH_ID";

                        using (objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL2))
                        {
                            while (objReader.Read())
                            {
                                sClaimDetails.Add(new KeyValuePair<string, string>(objReader.GetValue("AUTO_BATCH_ID").ToString(), "AUTOCLAIMCHECKS"));
                            }
                        }
                        objReader = null;
                    }
                    //tkatsarski: 01/12/15 - End changes for RMA-1601

                    // kkaur8 Added for MITS 32448 Ends

                    //zmohammad : Merging 37022 into CDR.
                    // akaushik5 Added for MITS 37022 Starts
                    // Since Case Management applicable for DI and WC, Diary records are fetched for these LOB's 
                    //Commented for rma 1598,1599
                    //if (objClaim.LineOfBusCode.Equals(243) || objClaim.LineOfBusCode.Equals(844))
                    {
                        PiEmployee objEmployee = (PiEmployee)objDMF.GetDataModelObject("PiEmployee", false);
                        foreach (PersonInvolved objPerson in objEvent.PiList)
                        {
                            objEmployee.MoveTo(objPerson.PiRowId);
                            //rma 1598
                            foreach (PiXDependent objPiXDependent in objEmployee.PiXDependentList)
                                    {
                                        //tkatsarski: 01/27/15 - RMA-1601
                                        //sClaimDetails[i, 0] = objPiXDependent.PiDepRowId.ToString();
                                        //sClaimDetails[i, 1] = "PIDEPENDENT";
                                        //i++;
                                        sClaimDetails.Add(new KeyValuePair<string, string>(objPiXDependent.PiDepRowId.ToString(), "PIDEPENDENT"));
                                    }

                            //tkatsarski: 01/12/15 - Start changes for RMA-1601: Diaries are displayed for Restrictions and Work Loss
                            foreach (PiXRestrict objPiXRestrict in objEmployee.PiXRestrictList)
                            {
                                sClaimDetails.Add(new KeyValuePair<string, string>(objPiXRestrict.PiRestrictRowId.ToString(), "PIRESTRICTION"));
                            }
                            foreach (PiXWorkLoss objPiXWorkLoss in objEmployee.PiXWorkLossList)
                            {
                                sClaimDetails.Add(new KeyValuePair<string, string>(objPiXWorkLoss.PiWlRowId.ToString(), "PIWORKLOSS"));
                            }
                            //tkatsarski: 01/12/15 - End changes for RMA-1601

                            if (objEmployee.CaseManagementList != null && objEmployee.CaseManagementList.Count > 0)
                            {
                                foreach (CaseManagement objCaseManagement in objEmployee.CaseManagementList)
                                {
                                     if (objCaseManagement.CasemgtRowId.ToString() != null)
                                    {
                                        //tkatsarski: 01/27/15 - RMA-1601
                                        //sClaimDetails[i, 0] = objCaseManagement.CasemgtRowId.ToString();
                                        //sClaimDetails[i, 1] = "CASEMANAGEMENT";
                                        //i++;
                                        sClaimDetails.Add(new KeyValuePair<string, string>(objCaseManagement.CasemgtRowId.ToString(), "CASEMANAGEMENT"));
                                    }
                                    foreach (CmXCmgrHist objCmXCmgrHist in objCaseManagement.CaseMgrHistList)
                                    {
                                        //tkatsarski: 01/27/15 - RMA-1601
                                        //sClaimDetails[i, 0] = objCmXCmgrHist.CmcmhRowId.ToString();
                                        //sClaimDetails[i, 1] = "CMXCMGRHIST";
                                        //i++;
                                        sClaimDetails.Add(new KeyValuePair<string, string>(objCmXCmgrHist.CmcmhRowId.ToString(), "CMXCMGRHIST"));
                                    }

                                    //Diary issues RMA 1598
                                    foreach (CmXAccommodation objCmXAccommodation in objCaseManagement.AccommodationList)
                                    {
                                        //tkatsarski: 01/27/15 - RMA-1601
                                        //sClaimDetails[i, 0] = objCmXAccommodation.CmAccommRowId.ToString();
                                        //sClaimDetails[i, 1] = "CMXACCOMMODATION";
                                        //i++;
                                        sClaimDetails.Add(new KeyValuePair<string, string>(objCmXAccommodation.CmAccommRowId.ToString(), "CMXACCOMMODATION"));
                                    }
                                    foreach (CmXVocrehab objCmXVocrehab in objCaseManagement.VocRehabList)
                                    {
                                        //tkatsarski: 01/27/15 - RMA-1601
                                        //sClaimDetails[i, 0] = objCmXVocrehab.CmvrRowId.ToString();
                                        //sClaimDetails[i, 1] = "CMXVOCREHAB";
                                        //i++;
                                        sClaimDetails.Add(new KeyValuePair<string, string>(objCmXVocrehab.CmvrRowId.ToString(), "CMXVOCREHAB"));
                                    }
                                    foreach (CmXCmgrHist objCmXCmgrHist in objCaseManagement.CaseMgrHistList)
                                    {
                                        foreach (CaseMgrNotes objCaseMgrNotes in objCmXCmgrHist.CaseMgrNotesList)
                                        {
                                            //tkatsarski: 01/27/15 - RMA-1601
                                            //sClaimDetails[i, 0] = objCaseMgrNotes.CmgrNotesRowId.ToString();
                                            //sClaimDetails[i, 1] = "CASEMGRNOTES";
                                            //i++;
                                            sClaimDetails.Add(new KeyValuePair<string, string>(objCaseMgrNotes.CmgrNotesRowId.ToString(), "CASEMGRNOTES"));
                                        }
                                    }
                                    foreach (CmXTreatmentPln objCmXTreatmentPln in objCaseManagement.TreatmentPlanList)
                                    {
                                        //tkatsarski: 01/27/15 - RMA-1601
                                        //sClaimDetails[i, 0] = objCmXTreatmentPln.CmtpRowId.ToString();
                                        //sClaimDetails[i, 1] = "CMXTREATMENTPLN";
                                        //i++;
                                        sClaimDetails.Add(new KeyValuePair<string, string>(objCmXTreatmentPln.CmtpRowId.ToString(), "CMXTREATMENTPLN"));
                                    }

                                    //rma 1598 Diary mits ends here
                                }
                            }
                        }

                        //if (objClaim.CaseManagement != null && objClaim.CaseManagement.CaseMgrHistList != null && objClaim.CaseManagement.CaseMgrHistList.Count > 0)
                        //{
                        //    foreach (CmXCmgrHist objCmXCmgrHist in objClaim.CaseManagement.CaseMgrHistList)
                        //    {
                        //        sClaimDetails[i, 0] = objCmXCmgrHist.CmcmhRowId.ToString();
                        //        sClaimDetails[i, 1] = "CMXCMGRHIST";
                        //        i++;
                        //    }
                        //}
                    }
                    // akaushik5 Added for MITS 37022 Ends

                    //ended by Nitin for Mits 12304 on 13-May-2009
                    foreach (PersonInvolved objPersons in objEvent.PiList)
                    {
                        int piTypeCode = objPersons.PiTypeCode;
                        string sTable = string.Empty;
                        switch (objClaim.Context.LocalCache.GetShortCode(piTypeCode))
                        {
                            case "E":
                                sTable = "PiEmployee";
                                break;
                            case "MED":
                                sTable = "PiMedStaff";
                                break;
                            case "O":
                                sTable = "PiOther";
                                break;
                            case "P":
                                sTable = "PiPatient";
                                break;
                            case "PHYS":
                                sTable = "PiPhysician";
                                break;
                            case "W":
                                sTable = "PiWitness";
                                break;
                            case "D":
                                sTable = "PiDriver";
                                break;
                        }
                        sTable = sTable.ToUpper();
                        //tkatsarski: 01/27/15 - RMA-1601
                        //sClaimDetails[i, 0] = objPersons.PiRowId.ToString();
                        //sClaimDetails[i, 1] = sTable;
                        //i++;
                        sClaimDetails.Add(new KeyValuePair<string, string>(objPersons.PiRowId.ToString(), sTable));
                    }

                    objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL1);
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            //tkatsarski: 01/27/15 - RMA-1601
                            //sClaimDetails[i, 0] = objReader.GetValue("TRANS_ID").ToString();
                            //sClaimDetails[i, 1] = "FUNDS";
                            //i++;
                            sClaimDetails.Add(new KeyValuePair<string, string>(objReader.GetValue("TRANS_ID").ToString(), "FUNDS"));
                        }
                    }
                    objReader = null;
                    #endregion

                    iCount = sClaimDetails.Count;   //tkatsarski: 01/27/15 - RMA-1601

                    sWhereTemp = sWhere1;
                    if (dbConn.DatabaseType == Db.eDatabaseType.DBMS_IS_ORACLE)
                    {//oracle
                    for (int j = 0; j < iCount; j++)
                    {
                        //RMA 1986
                        //tkatsarski: 01/27/15 - RMA-1601
                        if (!string.IsNullOrEmpty(sClaimDetails[j].Key))
                        {
                            sWhere1 = "";
                            //tkatsarski: 02/04/15 - RMA-1601 - This was causing issues for Oracle
                            if (isExtraAlias)
                                sWhere1 = ")";
                            sWhere1 = sWhere1 + " UNION ALL " + sSQL_WPA_ONE + " AND ATTACH_TABLE='" + sClaimDetails[j].Value + "'";
                            sWhere1 = sWhere1 + " AND ATTACH_RECORDID = " + sClaimDetails[j].Key;
                            sWhere1 = sWhere1 + " AND IS_ATTACHED <> 0)";

                            sSQLOne = sSQLOne + sWhere1 + sQueryFilter; //Neha Mits 23586
                        }
                    }

                    //tkatsarski: 02/04/15 - RMA-1601
                    if (isExtraAlias && (!((p_iSortOrder == 9) || (p_iSortOrder == 13) || (p_iSortOrder == 15) || (p_iSortOrder == 16))))
                        sSQLOne = sSQLOne + ")";
                    }
                    else
                    {
                        //sql
                        //Ankit Start : MITS - 32559
                        if (!(isExtraAlias || p_iSortOrder == 10 || p_iSortOrder == 14 || p_iSortOrder == 17 || p_iSortOrder == 18))
                        {
                            sWhere1 = ")";
                            sSQLOne = sSQLOne + sWhere1;
                        }
                        //Ankit End

                        for (int j = 0; j < iCount; j++)
                        {
                            //RMA 1598
                            //tkatsarski: 01/27/15 - RMA-1601
                            if (!string.IsNullOrEmpty(sClaimDetails[j].Key))
                            {
                                sWhere1 = "";
                                //Ankit Start : MITS - 32559
                                if (isExtraAlias)
                                    sWhere1 = " AS T" + (j + 1) + ")";
                                sWhere1 = sWhere1 + " as tbl" + (j + 1) + " UNION  ALL " + sSQL_WPA_ONE + " AND ATTACH_TABLE='" + sClaimDetails[j].Value + "'";
                                //Ankit End
                                sWhere1 = sWhere1 + " AND ATTACH_RECORDID = " + sClaimDetails[j].Key;
                                sWhere1 = sWhere1 + " AND IS_ATTACHED <> 0)";

                                sSQLOne = sSQLOne + sWhere1;
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(sSQLOne))
                        sSQL = sSQLOne.Substring(0, sSQLOne.Length - 1); //tmalhotra3-Changes for MITS 31602

                    //srajindersin 02/20/2013 MITS 31607
                    if (!string.IsNullOrEmpty(sSQLOne))
                        sSQL = sSQLOne.Substring(0, sSQLOne.Length - 1);

                    #region  switch (p_iSortOrder)
                    switch (p_iSortOrder)
                    {
                        case (int)DIARYSORTORDER.ASC_COMPLETE_DATE:
                            //Commented by Nitin for Mits 19779 on 12-Feb-2010
                            //sSQL = sSQL + ")";  //ORDER BY COMPLETE_DATE ASC";MITS 27074
                            //sSortValue = ")";       ////Ankit Start : Work for MITS 32559
                            iSortOrder = (int)DIARYSORTORDER.ASC_COMPLETE_DATE;
                            break;
                        case (int)DIARYSORTORDER.ASC_ATTACH_TABLE:
                            //added by Nitin for Mits 12304 on 13-May-2009 
                            //to correct run time error while executing sql statement
                            //sSQL = sSQL + ")";27074
                            //sSortValue = ")";     //Ankit Start : Work for MITS 32559
                            //sSQL = sSQL + ") ORDER BY ATTACH_TABLE ASC";
                            iSortOrder = (int)DIARYSORTORDER.ASC_ATTACH_TABLE;
                            break;
                        case (int)DIARYSORTORDER.ASC_ENTRY_NAME:
                            //sSQL = sSQL + ") ORDER BY ENTRY_NAME ASC";27074                            
                            iSorted = 1;
                            iSortOrder = (int)DIARYSORTORDER.ASC_ENTRY_NAME;
                            break;
                        case (int)DIARYSORTORDER.ASC_PRIORITY:
                            //sSQL = sSQL + ") ORDER BY PRIORITY ASC"; 27074                            
                            iSorted = 1;  
                            iSortOrder = (int)DIARYSORTORDER.ASC_PRIORITY;
                            break;
                        //Merged by Nitin for R6 - Diary List - sorting - start
                        case (int)DIARYSORTORDER.ASC_CLAIMANT:
                            iSortOrder = (int)DIARYSORTORDER.ASC_CLAIMANT;
                            //sSortValue = ")"; //27074     //Ankit Start : Work for MITS 32559
                            break;
                        //Merged by Nitin for R6 - Diary List - sorting - start
                        case (int)DIARYSORTORDER.DEC_ATTACH_TABLE:
                            //added by Nitin for Mits 12304 on 13-May-2009 
                            //to correct run time error while executing sql statement
                            //sSQL = sSQL + ")"; 27074
                            //sSortValue = ")";     //Ankit Start : Work for MITS 32559
                            // sSQL = sSQL + ") ORDER BY ATTACH_TABLE DESC";
                            iSortOrder = (int)DIARYSORTORDER.DEC_ATTACH_TABLE;
                            break;
                        case (int)DIARYSORTORDER.DEC_ENTRY_NAME:
                            //sSQL = sSQL + ") ORDER BY ENTRY_NAME DESC";
                            iSorted = 1;
                            iSortOrder = (int)DIARYSORTORDER.DEC_ENTRY_NAME;
                            break;
                        case (int)DIARYSORTORDER.DEC_PRIORITY:
                            //sSQL = sSQL + ") ORDER BY PRIORITY DESC"; 27074
                            iSorted = 1;
                            iSortOrder = (int)DIARYSORTORDER.DEC_PRIORITY;
                            break;
                        //tkatsarski: 02/04/15 - RMA-1601
                        case (int)DIARYSORTORDER.ASC_WORK_ACTIVITY:
                            iSortOrder = (int)DIARYSORTORDER.ASC_WORK_ACTIVITY;
                            break;
                        case (int)DIARYSORTORDER.DEC_WORK_ACTIVITY:
                            iSortOrder = (int)DIARYSORTORDER.DEC_WORK_ACTIVITY;
                            break;
                        //Merged by Nitin for R6 - Diary List - sorting - start
                        case (int)DIARYSORTORDER.DEC_CLAIMANT:
                            //sSortValue = ")"; //27074     ////Ankit Start : Work for MITS 32559
                            iSortOrder = (int)DIARYSORTORDER.DEC_CLAIMANT;
                            break;
                        //Merged by Nitin for R6 - Diary List - sorting - end
                        //hlv MITS 29594 11/6/12 begin
                        case (int)DIARYSORTORDER.ASC_DEPARTMENT:
                            iSortOrder = (int)DIARYSORTORDER.ASC_DEPARTMENT;
                            break;
                        case (int)DIARYSORTORDER.DEC_DEPARTMENT:
                            iSortOrder = (int)DIARYSORTORDER.DEC_DEPARTMENT;
                            break;
                        case (int)DIARYSORTORDER.ASC_CLAIMSTATUS:
                            iSortOrder = (int)DIARYSORTORDER.ASC_CLAIMSTATUS;
                            break;
                        case (int)DIARYSORTORDER.DEC_CLAIMSTATUS:
                            iSortOrder = (int)DIARYSORTORDER.DEC_CLAIMSTATUS;
                            break;
                        case (int)DIARYSORTORDER.ASC_ASSIGNED_USER:
                            iSortOrder = (int)DIARYSORTORDER.ASC_ASSIGNED_USER;
                            break;
                        case (int)DIARYSORTORDER.DEC_ASSIGNED_USER:
                            iSortOrder = (int)DIARYSORTORDER.DEC_ASSIGNED_USER;
                            break;
                        case (int)DIARYSORTORDER.ASC_ASSIGNING_USER:
                            iSortOrder = (int)DIARYSORTORDER.ASC_ASSIGNING_USER;
                            break;
                        case (int)DIARYSORTORDER.DEC_ASSIGNING_USER:
                            iSortOrder = (int)DIARYSORTORDER.DEC_ASSIGNING_USER;
                            break;
                        //MITS 36519
                        case (int)DIARYSORTORDER.ASC_PARENTRECORD:
                            iSortOrder = (int)DIARYSORTORDER.ASC_PARENTRECORD;
                            break;
                        case (int)DIARYSORTORDER.DEC_PARENTRECORD:
                            iSortOrder = (int)DIARYSORTORDER.DEC_PARENTRECORD;
                            break;
                        //hlv MITS 29594 11/6/12 end
                        //igupta3 jira 439
                        case (int)DIARYSORTORDER.ASC_NOTROUTE:
                            iSortOrder = (int)DIARYSORTORDER.ASC_NOTROUTE;
                            break;
                        case (int)DIARYSORTORDER.DEC_NOTROUTE:
                            iSortOrder = (int)DIARYSORTORDER.DEC_NOTROUTE;
                            break;
                        case (int)DIARYSORTORDER.ASC_NOTROLL:
                            iSortOrder = (int)DIARYSORTORDER.ASC_NOTROLL;
                            break;
                        case (int)DIARYSORTORDER.DEC_NOTROLL:
                            iSortOrder = (int)DIARYSORTORDER.DEC_NOTROLL;
                            break;
                        default:
                            //Commented by Nitin for Mits 19779 on 12-Feb-2010
                            //sSQL = sSQL + ")";  // ORDER BY COMPLETE_DATE DESC"; MITS 27074
                            //sSortValue = ")";      ////Ankit Start : Work for MITS 32559
                            p_iSortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                            iSortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                            break;
                    }
                    #endregion

                }
                else
                {
                    sWhere2 = sWhere;
                    //MITS 11998 Abhishek-start : added condition sShowAllDiariesForActiveRec=="yes"
                    if (p_sAttachTable.Trim() != "" && sShowAllDiariesForActiveRec == "yes")
                    {
                        sWhere2 = sWhere2 + " AND ATTACH_TABLE='" + p_sAttachTable + "'";
                    }
                    if (p_sAttachRecordId != "" && sShowAllDiariesForActiveRec == "yes")
                    {
                        sWhere2 = sWhere2 + " AND ATTACH_RECORDID = " + p_sAttachRecordId;
                        sWhere2 = sWhere2 + " AND IS_ATTACHED <> 0";
                    }
                    //MITS 11998 end
                    sWhere2 = sWhere2.Trim();
                    if (s_ModifyFilterExp != string.Empty)
                        sSQL = sSQL + " WHERE " + sWhere2 + " AND " + s_ModifyFilterExp + sQueryFilter;
                    else
                    sSQL = sSQL + " WHERE " + sWhere2 + sQueryFilter; //Neha Mits 23586
                    #region  switch (p_iSortOrder)
                    switch (p_iSortOrder)
                    {
                        case (int)DIARYSORTORDER.ASC_COMPLETE_DATE:
                            //Commented by Nitin for Mits 19779 on 12-Feb-2010
                            //sSQL = sSQL + " ORDER BY COMPLETE_DATE ASC";
                            iSortOrder = (int)DIARYSORTORDER.ASC_COMPLETE_DATE;
                            break;
                        case (int)DIARYSORTORDER.ASC_ATTACH_TABLE:
                            //sSQL = sSQL + " ORDER BY ATTACH_TABLE ASC";
                            iSortOrder = (int)DIARYSORTORDER.ASC_ATTACH_TABLE;
                            break;
                        case (int)DIARYSORTORDER.ASC_ENTRY_NAME:
                            ////sSQL = sSQL + " ORDER BY ENTRY_NAME ASC";                            
                            iSortOrder = (int)DIARYSORTORDER.ASC_ENTRY_NAME;
                            break;
                        case (int)DIARYSORTORDER.ASC_PRIORITY:
                            ////sSQL = sSQL + " ORDER BY PRIORITY ASC";                            
                            iSortOrder = (int)DIARYSORTORDER.ASC_PRIORITY;
                            break;
                        case (int)DIARYSORTORDER.DEC_ATTACH_TABLE:
                            //sSQL = sSQL + " ORDER BY ATTACH_TABLE DESC";
                            iSortOrder = (int)DIARYSORTORDER.DEC_ATTACH_TABLE;
                            break;
                        case (int)DIARYSORTORDER.DEC_ENTRY_NAME:
                            ////sSQL = sSQL + " ORDER BY ENTRY_NAME DESC";                            
                            iSortOrder = (int)DIARYSORTORDER.DEC_ENTRY_NAME;
                            break;
                        case (int)DIARYSORTORDER.DEC_PRIORITY:
                            ////sSQL = sSQL + " ORDER BY PRIORITY DESC";                            
                            iSortOrder = (int)DIARYSORTORDER.DEC_PRIORITY;
                            break;
                        //Geeta 11/17/06 : Case added to fix mits number 8139 for Sorting Work Activity
                        case (int)DIARYSORTORDER.ASC_WORK_ACTIVITY:
                            iSortOrder = (int)DIARYSORTORDER.ASC_WORK_ACTIVITY;
                            break;
                        case (int)DIARYSORTORDER.DEC_WORK_ACTIVITY:
                            iSortOrder = (int)DIARYSORTORDER.DEC_WORK_ACTIVITY;
                            break;
                            //igupta3 jira 439
                        case (int)DIARYSORTORDER.ASC_NOTROUTE:
                            iSortOrder = (int)DIARYSORTORDER.ASC_NOTROUTE;
                            break;
                        case (int)DIARYSORTORDER.DEC_NOTROUTE:
                            iSortOrder = (int)DIARYSORTORDER.DEC_NOTROUTE;
                            break;
                        case (int)DIARYSORTORDER.ASC_NOTROLL:
                            iSortOrder = (int)DIARYSORTORDER.ASC_NOTROLL;
                            break;
                        case (int)DIARYSORTORDER.DEC_NOTROLL:
                            iSortOrder = (int)DIARYSORTORDER.DEC_NOTROLL;
                            break;
                        //Merged by Nitin for R6 - Diary List - sorting - start
                        case (int)DIARYSORTORDER.DEC_CLAIMANT:
                            iSortOrder = (int)DIARYSORTORDER.DEC_CLAIMANT;
                            break;
                        case (int)DIARYSORTORDER.ASC_CLAIMANT:
                            iSortOrder = (int)DIARYSORTORDER.ASC_CLAIMANT;
                            break;
                        //Merged by Nitin for R6 - Diary List - sorting - end
                        //hlv MITS 29594 11/6/12 begin
                        case (int)DIARYSORTORDER.ASC_DEPARTMENT:
                            iSortOrder = (int)DIARYSORTORDER.ASC_DEPARTMENT;
                            break;
                        case (int)DIARYSORTORDER.DEC_DEPARTMENT:
                            iSortOrder = (int)DIARYSORTORDER.DEC_DEPARTMENT;
                            break;
                        case (int)DIARYSORTORDER.ASC_CLAIMSTATUS:
                            iSortOrder = (int)DIARYSORTORDER.ASC_CLAIMSTATUS;
                            break;
                        case (int)DIARYSORTORDER.DEC_CLAIMSTATUS:
                            iSortOrder = (int)DIARYSORTORDER.DEC_CLAIMSTATUS;
                            break;
                        case (int)DIARYSORTORDER.ASC_ASSIGNED_USER:
                            iSortOrder = (int)DIARYSORTORDER.ASC_ASSIGNED_USER;
                            break;
                        case (int)DIARYSORTORDER.DEC_ASSIGNED_USER:
                            iSortOrder = (int)DIARYSORTORDER.DEC_ASSIGNED_USER;
                            break;
                        case (int)DIARYSORTORDER.ASC_ASSIGNING_USER:
                            iSortOrder = (int)DIARYSORTORDER.ASC_ASSIGNING_USER;
                            break;
                        case (int)DIARYSORTORDER.DEC_ASSIGNING_USER:
                            iSortOrder = (int)DIARYSORTORDER.DEC_ASSIGNING_USER;
                            break;
                        //MITS 36519
                        case (int)DIARYSORTORDER.ASC_PARENTRECORD:
                            iSortOrder = (int)DIARYSORTORDER.ASC_PARENTRECORD;
                            break;
                        case (int)DIARYSORTORDER.DEC_PARENTRECORD:
                            iSortOrder = (int)DIARYSORTORDER.DEC_PARENTRECORD;
                            break;
                        //hlv MITS 29594 11/6/12 end
                        default:
                            //Commented by Nitin for Mits 19779 on 12-Feb-2010
                            //sSQL = sSQL + " ORDER BY COMPLETE_DATE DESC";
                            p_iSortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                            iSortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                            break;
                    }
                    #endregion
                }
                //Ankit-Start Changes for custom paging MITS 27074
                ////PSARIN2 R8 Perf Start******************************************************************************************************/
                //bool bAttachedRecordSortingOrder = false;
                //if (objDataSet.Tables[0].Rows.Count != 0)
                //{

                //    //By default the due date should be current date.. however the code was not cdonsidering current date.. changed by raman bhatia
                //    //01/10/2007 REM Umesh

                //    using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, "Select PREF_XML from USER_PREF_XML where USER_ID=" + UserId.ToString()))
                //    {
                //        if (objRdr.Read())
                //        {
                //            sUserPrefsXML = Common.Conversion.ConvertObjToStr(objRdr.GetValue("PREF_XML"));
                //            bStatus = true;
                //        }
                //        //objRdr.Close();
                //    }
                //    sUserPrefsXML = sUserPrefsXML.Trim();
                //    objUserPrefXML = new XmlDocument();
                //    if (sUserPrefsXML != "")
                //    {
                //        objUserPrefXML.LoadXml(sUserPrefsXML);
                //    }

                //    // Create the xml if it doesn't exist in the database
                //    CreateUserPreferXML(objUserPrefXML);
                //    //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
                //    if (sUserSortOrder != "0")
                //    {
                //        //Neha
                //        objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/UserSortOrder/@value");
                //        objNode.Value = sUserSortOrder;
                //        this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(UserId));
                //    }//end Mits 23477


                //    //tkr 6/2007 HACK
                //    //<ShowNotes> node value is "0" in instance.xml
                //    //if this is first time called (showNotes=="0"), then fetch the UserPref and set the checkbox to it
                //    //if this is a postback (showNotes=="1" or showNotes=="") reset the UserPref
                //    XmlNode nodeShowNotes = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ShowNotes/@selected");

                //    //if(showNotes.Equals("0"))
                //    if (showNotes.Equals(""))       //csingh7 MITS 18435 :if this is first time called (showNotes==""), then fetch the UserPref and set the checkbox to it
                //    {
                //        showNotes = nodeShowNotes.InnerText;
                //    } // if
                //    else
                //    {                                //csingh7 MITS 18435 :if this is a postback (showNotes=="1" or showNotes=="0") reset the UserPref
                //        if (showNotes.CompareTo(nodeShowNotes.InnerText) != 0)
                //            nodeShowNotes.InnerText = showNotes;
                //    } // else
                //    //Added by Amitosh for Terelik Grid UI

                //    XmlNode nodeShowRegarding = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ShowRegarding/@selected");
                //    if (p_sShowRegarding.Equals(""))
                //    {
                //        p_sShowRegarding = nodeShowRegarding.InnerText;
                //    }
                //    else
                //    {
                //        if (p_sShowRegarding.CompareTo(nodeShowRegarding.InnerText) != 0)
                //        {
                //            nodeShowRegarding.InnerText = p_sShowRegarding;
                //        }
                //    }
                //    //End Amitosh
                //    objShowActiveDiary = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ShowActiveDiaryChecked/@selected");

                //    if (!p_bReqActDiary)
                //    {
                //        if (objShowActiveDiary != null)
                //        {
                //            if (objShowActiveDiary.InnerText == "1")
                //            {
                //                p_sDueDate = "";
                //            }
                //        }
                //    }
                //    objXml = new XmlDocument();
                //    string strContent = RMSessionManager.GetCustomSettings();

                //    if (!string.IsNullOrEmpty(strContent))
                //    {
                //        objXml.LoadXml(strContent);
                //        //objRdr.Dispose();
                //        objSaveShowActiveDiary = objXml.SelectSingleNode("//Other/SaveShowActiveDiary");
                //        if (objSaveShowActiveDiary != null)
                //        {
                //            if (objSaveShowActiveDiary.InnerText == "-1")
                //            {
                //                if (p_bReqActDiary)
                //                {
                //                    if (p_sActiveDiaryChecked == "")
                //                        objShowActiveDiary.Value = "0";
                //                    else
                //                        objShowActiveDiary.Value = "1";
                //                }
                //            }
                //        }

                //    }



                //    sUserPrefsXML = objUserPrefXML.InnerXml.ToString();
                //    objConn = DbFactory.GetDbConnection(m_sConnectionString);

                //    objConn.Open();
                //    sbSql = new StringBuilder();
                //    objCmd = objConn.CreateCommand();
                //    objParam = objCmd.CreateParameter();
                //    objParam.Direction = ParameterDirection.Input;
                //    objParam.Value = sUserPrefsXML;
                //    objParam.ParameterName = "XML";
                //    objParam.SourceColumn = "PREF_XML";
                //    objCmd.Parameters.Add(objParam);
                //    if (bStatus)
                //        sbSql.Append("UPDATE USER_PREF_XML SET PREF_XML=~XML~ WHERE USER_ID=" + UserId);
                //    else
                //        sbSql.Append("INSERT INTO USER_PREF_XML VALUES(" + UserId + ",~XML~)");
                //    objCmd.CommandText = sbSql.ToString();
                //    objCmd.ExecuteNonQuery();
                //    //End REM UMESH

                //    //PSARIN2 R8 Perf End*************************************************************************************************************/
                tempNodes = objUserPrefXML.SelectNodes("//setting/DiaryConfig/DiarySupplemental/option");
                if (tempNodes != null && tempNodes.Count > 0)
                {
                    foreach (XmlNode objOptionNode in tempNodes)
                    {
                        lstSuppColumnNames.Add(objOptionNode.Attributes["value"].Value.ToUpper());
                    }
                    if (lstSuppColumnNames != null)
                    {
                        s_SuppCommaSeparatedColumn = string.Join<string>(",", lstSuppColumnNames);
                    } 
                }

                

                string sTempSQL = string.Empty;
                if (sWhere1 != string.Empty)    //Ankit-either swhere1 or swhere2 will be blank, but not both.
                {
                    if (s_ModifyFilterExp != string.Empty)
                        sTempSQL = sTempSQL + "AND " + s_ModifyFilterExp + " ";
                    if (dbConn.DatabaseType == Db.eDatabaseType.DBMS_IS_SQLSRVR)
                    {
                        if (sAllClaimDiaries == "no")
                        {
                            //Ankit Start : MITS - 32559
                            if (isExtraAlias)
                                sTempSQL = sSQLOne + ") as tempt) as temptbl ";
                            else
                                sTempSQL = sSQLOne + ") as temptbl ";
                        }
                        else
                        {
                            if (isExtraAlias)
                                sTempSQL = sSQLOne + " as tempt) as temptbl ";
                            else
                                sTempSQL = sSQLOne + " as temptbl ";
                        }
                        //Ankit End
                    }
                    else
                    {
                        //tkatsarski: 02/04/15 - RMA-1601
                        if ((isExtraAlias) || (sAllClaimDiaries == "yes" && p_iSortOrder != 10 && p_iSortOrder != 14 && p_iSortOrder != 17 && p_iSortOrder != 18))
                        {
                            sTempSQL = sSQLOne + ")";
                        }
                        else
                        {
                            sTempSQL = sSQLOne;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(sWhere2))
                {
                    sTempSQL = "SELECT COUNT(*) as rwcnt FROM WPA_DIARY_ENTRY WHERE " + sWhere2;
                    if (s_ModifyFilterExp != string.Empty)
                        sTempSQL = sTempSQL + " AND " + s_ModifyFilterExp;
                }
                if (!string.IsNullOrEmpty(s_SuppJoin) && !(string.IsNullOrEmpty(s_SuppCommaSeparatedColumn)))
                    sTempSQL = sTempSQL.Replace("s_SuppJoin", s_SuppJoin).Replace("s_SuppQuery", ", " + s_SuppCommaSeparatedColumn);
                else
                    sTempSQL = sTempSQL.Replace("s_SuppJoin", " ").Replace("s_SuppQuery", " ");

                objDataSet = DbFactory.GetDataSet(m_sConnectionString, sTempSQL, m_iClientId);
                if (objDataSet.Tables.Count == 0)
                {
                    throw new RMAppException(Globalization.GetString("WPA.GetDiaryDom.DatasetCreationError", m_iClientId));
                }
                if (objDataSet.Tables.Count > 0)
                {
                    if (sWhere2 != string.Empty)
                        p_lRecordCount = Conversion.ConvertObjToInt(objDataSet.Tables[0].Rows[0]["rwcnt"], m_iClientId);
                    else
                        p_lRecordCount = Conversion.ConvertObjToInt(objDataSet.Tables[0].Rows.Count, m_iClientId);
                }
                //Ankit-End changes for custom paging

                #region paging-logic
                long maxcount = p_lRecordCount;
                p_lPageCount = (int)(p_lRecordCount / m_lPageSize);
                fCalPages = (float)((float)p_lRecordCount / (float)m_lPageSize);
                if ((float)(p_lPageCount) < (float)(fCalPages))
                {
                    p_lPageCount = p_lPageCount + 1;
                }
                if (p_lPageCount == 0)
                {
                    p_lPageCount = 1;
                }

                long uppercount = 0;
                if (p_lPageNumber == 1)
                {
                    lStartAt = 1;
                    uppercount = maxcount;
                }
                else
                {
                    lStartAt = ((p_lPageNumber - 1) * m_lPageSize) + 1;
                }
                lCounter = lStartAt + m_lPageSize;
                if (p_lPageCount > 1)
                    if (p_lPageNumber == p_lPageCount)
                        uppercount = maxcount;
                    else
                        uppercount = lCounter - 1;
                else
                    uppercount = maxcount;
                #endregion
                //Ankit-MITS 27074 Custompaging- Query modified to incorporate paging logic
                //Ankit Start : Work for MITS 32559
                //tkatsarski: 02/04/15 - RMA-1601
                if (sAllClaimDiaries == "yes" && p_iSortOrder != 10 && p_iSortOrder != 14 && p_iSortOrder != 17 && p_iSortOrder != 18 && p_iSortOrder != 9 && p_iSortOrder != 13 && p_iSortOrder != 15 && p_iSortOrder != 16)
                    sSortValue = " Order By " + GetSortColumnName(p_iSortOrder);
                //Ankit End
                if (dbConn.DatabaseType == Db.eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    if (isExtraAlias)       //Ankit Start : Work for MITS 32559
                    {
                        sSQLOne = sSQLOne + " ) as tbl";
                        sSQL = sSQL + " ) as tbl";
                    }
                    else if((p_iSortOrder == 15 ) || (p_iSortOrder == 16))
                    {
                        sSQLOne = sSQLOne + " ) as tbl";
                        sSQL = sSQL + " ) as tbl";
                    }

                    if (sSortValue == ")")
                        sSQL = sSQL + " ) as mytable WHERE rno BETWEEN " + lStartAt + " AND " + uppercount;
                    else
                        sSQL = sSQL + " ) as mytable WHERE rno BETWEEN " + lStartAt + " AND " + uppercount + sSortValue;
                }
                else
                {
                    if (isExtraAlias)       //Ankit Start : Work for MITS 32559
                    {
                        sSQLOne = sSQLOne + " )";
                        sSQL = sSQL + " )";
                    }
                    else if ((p_iSortOrder == 15) || (p_iSortOrder == 16))
                    {
                        sSQLOne = sSQLOne + " )";//skhare7 MITS 31105
                        sSQL = sSQL + " )";//skhare7 MITS 31105
                    }
                    if (iCheckBoxChecked == 0)
                        sSQL = sSQL + " ) WHERE rno BETWEEN " + lStartAt + " AND " + uppercount + sSortValue;
                    else
                    {
                        if (iSorted == 0)
                        {
                            if (iCount == 0 && !(((p_iSortOrder == 9) || (p_iSortOrder == 13) || (p_iSortOrder == 10) || (p_iSortOrder == 14) || (p_iSortOrder == 15) || (p_iSortOrder == 16) || (p_iSortOrder == 17) || (p_iSortOrder == 18))))
                                sSQL = sSQLOne + ")  WHERE rno BETWEEN " + lStartAt + " AND " + uppercount;
                            else
                            // akaushik5 Changed for MITS 32448 Starts
                            //sSQL = sSQLOne + "  WHERE rno BETWEEN " + lStartAt + " AND " + uppercount + sSortValue;
                            {
                                //tkatsarski: 02/04/15 - RMA-1601
                                if ((p_iSortOrder == 9) || (p_iSortOrder == 13) || (p_iSortOrder == 10) || (p_iSortOrder == 14) || (p_iSortOrder == 15) || (p_iSortOrder == 16) || (p_iSortOrder == 17) || (p_iSortOrder == 18))
                                {
                                    sSQL = sSQLOne + " WHERE rno BETWEEN " + lStartAt + " AND " + uppercount + sSortValue;
                                }
                                else
                                {
                                    sSQL = sSQLOne + ")  WHERE rno BETWEEN " + lStartAt + " AND " + uppercount + sSortValue;
                                }
                            }
                            // akaushik5 Changed for MITS 32448 Ends
                        }
                        else
                            sSQL = sSQLOne + " ) WHERE rno BETWEEN " + lStartAt + " AND " + uppercount + sSortValue;
                    }
                }
                //Ankit-MITS 27074 Custompaging-End changes
                //set supp column and join
                //s_SuppQuery = s_SuppCommaSeparatedColumn;
                if (!string.IsNullOrEmpty(s_SuppJoin) && !(string.IsNullOrEmpty(s_SuppCommaSeparatedColumn)))
                    sSQL = sSQL.Replace("s_SuppJoin", s_SuppJoin).Replace("s_SuppQuery", ", " + s_SuppCommaSeparatedColumn);
                else
                    sSQL = sSQL.Replace("s_SuppJoin", " ").Replace("s_SuppQuery", " ");
                objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
                objRow = objDataSet.Tables[0].Select();
                if (objDataSet.Tables.Count == 0)
                {
                    throw new RMAppException(Globalization.GetString("WPA.GetDiaryDom.DatasetCreationError", m_iClientId));
                }                
                iCounter = 0;
                iUpperBound = 0;
                objDataSet.Tables[0].Columns.Add("ATTACH_PROMPT", System.Type.GetType("System.String"));
                objDataSet.Tables[0].Columns.Add("CLAIMANT", System.Type.GetType("System.String"));
                //hlv MITS 29594 11/6/12 begin
                objDataSet.Tables[0].Columns.Add("Department", System.Type.GetType("System.String"));
                objDataSet.Tables[0].Columns.Add("ClaimStatus", System.Type.GetType("System.String"));
                //hlv MITS 29594 11/6/12 end
                

                iUpperBound = objDataSet.Tables[0].Rows.Count - 1;

                //Raman R7 Prf Imp
                //If sortorder is not 2 or 6 then lets not find attachtable for all records in database. Instead lets focus only on the records which
                //would be displayed on the screen
                //Attached records code would be moved to the filtered records after paging to reduce the database hits.

                    bool bAttachedRecordSortingOrder = false;
                if (p_iSortOrder == 2 || p_iSortOrder == 6)
                {
                    bAttachedRecordSortingOrder = true;
                }

                while (iCounter <= iUpperBound)
                {
                    lRid = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_RECORDID"]));
                    if (lRid > 0)
                    {
                        #region switch Attach table
                        switch (Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"]))
                        {
                            case CLAIM:
                            case "claim":
                                //Raman R7 Prf Imp
                                //If sortorder is not 2 or 6 then lets not find attachtable for all records in database. Instead lets focus only on the records which
                                //would be displayed on the screen
                                //Attached records code would be moved to the filtered records after paging to reduce the database hits.    
                                if (bAttachedRecordSortingOrder)
                                {
                                    sAttached = "Claim: " + GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + lRid.ToString(), m_sConnectionString);
                                }
                                sClaimantName = GetClaimantsName(lRid);

                                //hlv MITS 29594 11/6/12 begin
                                sDepartmentName = GetDeptName(0, lRid, ref sOrgLevel);
                                sClaimStatus = GetClaimStatus(lRid.ToString());
                                //hlv MITS 29594 11/6/12 end
                                break;
                            case EVENT:
                            case "event":
                                //Raman R7 Prf Imp
                                //If sortorder is not 2 or 6 then lets not find attachtable for all records in database. Instead lets focus only on the records which
                                //would be displayed on the screen
                                //Attached records code would be moved to the filtered records after paging to reduce the database hits.    
                                if (bAttachedRecordSortingOrder)
                                {
                                    sAttached = "Event: " + GetSingleString("EVENT_NUMBER", "EVENT", "EVENT_ID=" + lRid.ToString(), m_sConnectionString);
                                }
                                sDepartmentName = GetDeptName(lRid, 0, ref sOrgLevel); //hlv MITS 29594 11/6/12
                                break;
                            case ENTITY:
                            case "entity":
                                //Raman R7 Prf Imp
                                //If sortorder is not 2 or 6 then lets not find attachtable for all records in database. Instead lets focus only on the records which
                                //would be displayed on the screen
                                //Attached records code would be moved to the filtered records after paging to reduce the database hits.    
                                if (bAttachedRecordSortingOrder)
                                {
                                    objCache.GetEntityInfo(lRid, ref sFname, ref sLname);
                                    sName = sFname.Trim().Equals("") ? sLname : sFname + ", " + sLname;
                                    sAttached = "Person\\Entity: " + sName;
                                }
                                break;
                            case CLAIMANT:
                            case "claimant":
                                //Raman R7 Prf Imp
                                //If sortorder is not 2 or 6 then lets not find attachtable for all records in database. Instead lets focus only on the records which
                                //would be displayed on the screen
                                //Attached records code would be moved to the filtered records after paging to reduce the database hits.    
                                if (bAttachedRecordSortingOrder)
                                {
                                    objCache.GetClaimantInfo(lRid, ref sFname, ref sLname);
                                    sName = sFname.Trim().Equals("") ? sLname : sFname + ", " + sLname;
                                    sAttached = "Claimant:" + sName;
                                }
                                break;
                            case PERSON_INVOLVED:
                            case "person_involved":
                                if (bAttachedRecordSortingOrder)
                                {
                                    sPIXmlFormName = GetPiXMLFormName(lRid, ref sAttached);
                                }
                                break;
                            case TM_JOB_LOG:
                            case "tm_job_log":
                                //Raman R7 Prf Imp
                                //If sortorder is not 2 or 6 then lets not find attachtable for all records in database. Instead lets focus only on the records which
                                //would be displayed on the screen
                                //Attached records code would be moved to the filtered records after paging to reduce the database hits.    
                                if (bAttachedRecordSortingOrder)
                                {
                                    sAttached = "Task Manager Job ( " + lRid.ToString() + ")";
                                }
                                break;
                            case FUNDS: // MITS: 22769 Case added for funds. We need to display the control number in the Attached Record field on the diary list screen.
                            case "funds":
                                //Raman R7 Prf Imp
                                //If sortorder is not 2 or 6 then lets not find attachtable for all records in database. Instead lets focus only on the records which
                                //would be displayed on the screen
                                //Attached records code would be moved to the filtered records after paging to reduce the database hits.    
                                if (bAttachedRecordSortingOrder)
                                {
                                    sAttached = "FUNDS: " + GetSingleString("CTL_NUMBER", "FUNDS", "TRANS_ID=" + lRid.ToString(), m_sConnectionString);
                                }
                                break;
                            // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
                            case POLICY_ENH:
                            case "POLICY_ENH":
                            case "policy_enh":
                                //Raman R7 Prf Imp
                                //If sortorder is not 2 or 6 then lets not find attachtable for all records in database. Instead lets focus only on the records which
                                //would be displayed on the screen
                                //Attached records code would be moved to the filtered records after paging to reduce the database hits.    
                                if (bAttachedRecordSortingOrder)
                                {
                                    // Get the LOB information of the Policy
                                    int iLob = GetSingleInt("POLICY_TYPE", "POLICY_ENH", "POLICY_ID = " + lRid.ToString(), m_sConnectionString);
                                    objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"] = string.Concat("POLICY_ENH_", objCache.GetShortCode(iLob));
                                    sAttached = String.Concat("POLICY MANAGEMENT (", objCache.GetShortCode(iLob), "): ", GetSingleString("POLICY_NAME", "POLICY_ENH", string.Format("POLICY_ID={0}", lRid.ToString()), m_sConnectionString));
                                }
                                break;
                            // npadhy End MITS 22028 Policies opening up in Maintenance when opened from Diaries

                            //rupal:start, r8 auto diary enh    
                            case SCHEDULED_ACTIVITY:
                            case "scheduled_activity":
                                if (bAttachedRecordSortingOrder)
                                {
                                    sAttached = GetAttachPromptForScheduledActivity(lRid.ToString(), out sClaimID);
                                }
                                break;
                            case CASE_MANAGEMENT:
                            case CMXTREATMENTPLN:
                            case "cm_x_treatment_pln":
                            case "cmxtreatmentpln":
                                if (bAttachedRecordSortingOrder)
                                {
                                    sAttached = GetAttachPropmtForCaseManagement(lRid.ToString());
                                }
                                break;
                            case RESERVE_HISTORY:
                            case "reserve_history":
                                if (bAttachedRecordSortingOrder)
                                {
                                    sAttached = GetAttachPromptForReserveHistory(lRid.ToString(), out sClaimID);
                                }
                                break;
                            case RESERVE_CURRENT:
                            case "reserve_current":
                                if (bAttachedRecordSortingOrder)
                                {
                                    sAttached = GetAttachPromptForReserveReview(lRid.ToString(), out sClaimID);
                                }
                                break;
                            case CLAIM_LITIGATION:
                            case LITIGATION:
                            case "claim_x_litigation":
                            case "litigation":
                                if (bAttachedRecordSortingOrder)
                                {
                                    sAttached = GetAttachPromptForLitigation(lRid.ToString());
                                }
                                break;
                            case FUND_AUTO_BATCH:
                            case AUTO_CLAIM_CHECKS:
                            case "funds_auto_batch":
                            case "autoclaimchecks":
                                if (bAttachedRecordSortingOrder)
                                {
                                    sAttached = GetAttachPromptForSchedulePayment(lRid.ToString());
                                }
                                break;
                            /*
                            case COMMENTS_TEXT:
                            case "comments_text":
                                if (bAttachedRecordSortingOrder)
                                {                                    
                                    string[] sCommentDetail = GetMultipleString("ATTACH_RECORDID,ATTACH_TABLE,COMMENT_ID", "COMMENTS_TEXT", "COMMENT_ID=" + lRid.ToString(), m_sConnectionString);                                    
                                    sAttached = GetAttachPromptForcommentReview(sCommentDetail);
                                }
                                break;
                             */
                            case CLAIM_PRG_NOTE:
                            case "claim_prg_note":
                                if (bAttachedRecordSortingOrder)
                                {
                                    //get Claim_ID from CLAIM_X_LITIGATION table 
                                    sProgressNoteDetail = GetMultipleString("CLAIM_ID,EVENT_ID,CL_PROG_NOTE_ID", "CLAIM_PRG_NOTE", "CL_PROG_NOTE_ID=" + lRid.ToString(), m_sConnectionString);
                                    sAttachPromptDetail = GetAttachPromptForProgressNotes(sProgressNoteDetail);
                                    sAttached = sAttachPromptDetail[0];
                                }
                                break;
                            //rupal:end r8 auto diary enh
                            //Added by sharishkumar for mits 35291
                            case PIDEPENDENT:
                            case "pidependent":
                                if (bAttachedRecordSortingOrder)
                                {
                                    sDependentRowDetail = GetSingleString("PI_ROW_ID", "PI_X_DEPENDENT", "PI_DEP_ROW_ID=" + lRid.ToString(), m_sConnectionString);
                                    sDependentDetail = GetMultipleString("EVENT_ID,PI_ROW_ID", "PERSON_INVOLVED", "PI_ROW_ID=" + sDependentRowDetail, m_sConnectionString);
                                    sAttachPromptDetail = GetAttachPromptForDependent(sDependentDetail);
                                    sAttached = sAttachPromptDetail[0];
                                }
                                break;
                            //End mits 35291
                            default:
                                //Raman R7 Prf Imp
                                //If sortorder is not 2 or 6 then lets not find attachtable for all records in database. Instead lets focus only on the records which
                                //would be displayed on the screen
                                //Attached records code would be moved to the filtered records after paging to reduce the database hits.    
                                if (bAttachedRecordSortingOrder)
                                {
                                    lTmp = objCache.GetTableId(Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"]));
                                    if (Conversion.ConvertLongToBool(lTmp, m_iClientId))
                                    {
                                        sAttached = objCache.GetTableName(Conversion.ConvertStrToInteger(lTmp.ToString())) + " (" + lRid.ToString() + ")";
                                    }
                                    else
                                    {
                                        sAttached = Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"]).ToUpper() + lRid.ToString();
                                    }
                                }
                                break;
                        }
                        #endregion
                        //Mits 33843 - Diary enhancement for adding parent record
                        if (objDataSet.Tables[0].Rows[iCounter]["ATT_PARENT_CODE"].ToString() != "0")
                        {
                            if (Enum.GetNames(typeof(PARENTCLAIM)).Any(w => w == Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"]).ToUpper()) == true)
                            {
                                string sql = "SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID = " + objDataSet.Tables[0].Rows[iCounter]["ATT_PARENT_CODE"].ToString();
                                if (objDataSet.Tables[0].Rows[iCounter]["ATT_PARENT_CODE"].ToString() != null && objDataSet.Tables[0].Rows[iCounter]["ATT_PARENT_CODE"].ToString() != string.Empty)
                                {
                                    DbReader objRd = DbFactory.GetDbReader(m_sConnectionString, sql);
                                    using (objRd)
                                    {
                                        if (objRd.Read())
                                        {
                                            //Claim Id is passed for opening the parent claim from DiaryList and Claim Number is for showing it in the DiaryList grid
                                            string claimNumber = Conversion.ConvertObjToStr(objRow[iCounter]["ATT_PARENT_CODE"]) + "Æ" + (string)objRd.GetValue("CLAIM_NUMBER") + "Æ" + "claim";
                                            p_parentRecord = claimNumber;
                                        }
                                    }
                                }
                            }
                            else if (Enum.GetNames(typeof(PARENTEVENT)).Any(w => w == Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"]).ToUpper()) == true)
                            {
                                string sql = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID = " + objDataSet.Tables[0].Rows[iCounter]["ATT_PARENT_CODE"].ToString();
                                if (objDataSet.Tables[0].Rows[iCounter]["ATT_PARENT_CODE"].ToString() != null && objDataSet.Tables[0].Rows[iCounter]["ATT_PARENT_CODE"].ToString() != string.Empty)
                                {
                                    using (DbReader objRd = DbFactory.GetDbReader(m_sConnectionString, sql))
                                    {
                                        if (objRd.Read())
                                        {
                                            //Event Id is passed for opening the parent event from DiaryList and event Number is for showing in the DiaryList grid
                                            string eventNumber = Conversion.ConvertObjToStr(objRow[iCounter]["ATT_PARENT_CODE"]) + "Æ" + (string)objRd.GetValue("EVENT_NUMBER") + "Æ" + "event";
                                            p_parentRecord = eventNumber;
                                        }
                                    }
                                }
                            }
                            //tkatsarski: 08/05/14 - Mits 36512: Changed typeof(PARENTEVENT) to typeof(PARENTPOLICY)
                            else if (Enum.GetNames(typeof(PARENTPOLICY)).Any(w => w == Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"]).ToUpper()) == true)
                            {
                                string sql = "SELECT POLICY_NAME FROM POLICY WHERE POLICY_ID = " + objDataSet.Tables[0].Rows[iCounter]["ATT_PARENT_CODE"].ToString();
                                if (objDataSet.Tables[0].Rows[iCounter]["ATT_PARENT_CODE"].ToString() != null && objDataSet.Tables[0].Rows[iCounter]["ATT_PARENT_CODE"].ToString() != string.Empty)
                                {
                                    DbReader objRd = DbFactory.GetDbReader(m_sConnectionString, sql);

                                    using (objRd = DbFactory.GetDbReader(m_sConnectionString, sql))
                                    {
                                        string policyNumber = string.Empty;
                                        string policyName = string.Empty;
                                        if (objRd.Read())
                                        {
                                            policyNumber = Conversion.ConvertObjToStr(objRow[iCounter]["ATT_PARENT_CODE"]) + "Æ";
                                            policyName = !(objRd.IsDBNull("POLICY_NAME")) ? (string)objRd.GetValue("POLICY_NAME") : string.Empty;
                                            policyNumber = policyNumber + policyName + "Æ" + "policy";
                                            p_parentRecord = policyNumber;

                                        }
                                    }
                                }
                            }
                        }
                        
                        
                        //End of Mits 33843

                        objDataSet.Tables[0].Rows[iCounter]["ATTACH_PROMPT"] = sAttached;

                        //Added by Nitin for Claimant sorting in R6 starts
                        if (sClaimantName == "")
                        {
                            sClaimantName = " ";
                        }
                        objDataSet.Tables[0].Rows[iCounter]["CLAIMANT"] = sClaimantName;
                        //Changed by Gagan for mits 19656 : Start
                        sClaimantName = "";
                        //Changed by Gagan for mits 19656 : end
                        //Added by Nitin for Claimant sorting in R6 ends

                        //hlv MITS 29594 11/6/12 begin
                        if (sDepartmentName == "")
                        {
                            sDepartmentName = " ";
                        }
                        objDataSet.Tables[0].Rows[iCounter]["Department"] = sDepartmentName;
                        sDepartmentName = "";

                        if (sClaimStatus == "")
                        {
                            sClaimStatus = " ";
                        }
                        objDataSet.Tables[0].Rows[iCounter]["ClaimStatus"] = sClaimStatus;
                        sClaimStatus = "";
                        //hlv MITS 29594 11/6/12 end

                    }
                    iCounter = iCounter + 1;
                }
                //Check for the Attached Record Sort Order

                #region switch p_iSortOrder
                //Added by Nitin for Mits 19779 on 12-Feb-2010

                //hlv MITS 29594 11/6/12 end


                //Geeta 11/21/06 : Modified to fix mits number 8139 for Sorting Work Activity
                if (p_iSortOrder == 9 || p_iSortOrder == 13)
                {
                    iCounter = 0;
                    while (iCounter <= iUpperBound)
                    {
                        if (objDataSet.Tables[0].Rows[iCounter]["ACT_TEXT"] == System.DBNull.Value)
                        {
                            //MITS 31146 Raman
                            //objDataSet.Tables[0].DefaultView.Table.Rows[iCounter]["ACT_TEXT"] = "No Work Activity";
                            objDataSet.Tables[0].DefaultView.Table.Rows[iCounter]["ACT_TEXT"] = "";
                            
                        }
                        iCounter = iCounter + 1;
                    }

                }
                objRow = objDataSet.Tables[0].Select("", sSortColName);
                //Ankit-End changes                
                //}//PSARIN2 End 


                objDOM = new XmlDocument();
                objElem = (XmlElement)objDOM.CreateNode(XmlNodeType.Element, "diaries", "");

                //WWIG GAP20A - agupta298 - MITS 36804 - Start
                objDiarySuppNode = (XmlElement)objDOM.CreateNode(XmlNodeType.Element, "DiarySupplemental", "");
                //WWIG GAP20A - agupta298 - MITS 36804 - End
                #region Diary Config
				//Mits 36708
                int claimantlength = objSys.ClaimantLength;
                objElem.SetAttribute("ClaimantLength", claimantlength.ToString());
                objElem.SetAttribute("user", p_sUserName);
                objElem.SetAttribute("opendiaries", Conversion.ConvertBoolToInt(p_bOpen, m_iClientId).ToString());
                //aanandpraka2:Start changes for mITS 27074
                //objElem.SetAttribute("datedue", Conversion.GetDBDateFormat(sDueDate, "d"));
                if (p_sUIDiarySource == "D")
                    objElem.SetAttribute("datedue", Conversion.GetDBDateFormat(sDiaryDueDate, "d"));
                else
                    objElem.SetAttribute("datedue", Conversion.GetDBDateFormat(sClaimDiaryDueDate, "d"));
                //aanandpraka2:End changes for MITS 27074
                objElem.SetAttribute("diarysource", sDiarySource.ToString()); //aanandpraka2:New attribute added for determining whether diary page is opened from claim.
                //Raman Bhatia 06/05/2007 Raman Bhatia: Adding a new attribute to maintain Current Date
                objElem.SetAttribute("currentsystemdate", DateTime.Now.ToShortDateString());
                objElem.SetAttribute("attach_table", p_sAttachTable);
                objElem.SetAttribute("attach_recordid", p_sAttachRecordId);
                //Indu - mits 33843 adding parent record
                objElem.SetAttribute("ATT_PARENT_CODE", p_parentRecord);
                //MITS 36519
                objElem.SetAttribute("sortexpression", columnName);
                //objElem.SetAttribute("", );
                objElem.SetAttribute("orderby", iSortOrder.ToString());
                if (p_lPageNumber > 1)
                {
                    objElem.SetAttribute("previouspage", (p_lPageNumber - 1).ToString());
                    objElem.SetAttribute("firstpage", "1");
                }
                if (p_lPageNumber < p_lPageCount)
                {
                    objElem.SetAttribute("nextpage", (p_lPageNumber + 1).ToString());
                    objElem.SetAttribute("lastpage", p_lPageCount.ToString());
                }
                objElem.SetAttribute("pagenumber", p_lPageNumber.ToString());
                objElem.SetAttribute("maxrecord", maxcount.ToString());
                objElem.SetAttribute("pagesize", m_lPageSize.ToString());//MITS 27074-Ankit
                objElem.SetAttribute("pagecount", p_lPageCount.ToString());
                if (sAllClaimDiaries == "yes")
                    objElem.SetAttribute("showallclaimdiaries", "yes");
                else
                    objElem.SetAttribute("showallclaimdiaries", "no");

                //MITS 11998 Abhishek start
                if (sShowAllDiariesForActiveRec == "yes")
                {
                    objElem.SetAttribute("showalldiariesforactiverecord", "yes");
                }
                else
                {
                    objElem.SetAttribute("showalldiariesforactiverecord", "no");
                }
                //MITS 11998 end
                //tkr checkbox to show/hide notes
                objElem.SetAttribute("shownotes", showNotes);
                objElem.SetAttribute("allowactionsonothersdiariesperm",this.SetOtherDiariesEditPermission);//asharma326 jira 11081
                objElem.SetAttribute("loggedinusergroup", Convert.ToString(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT GROUP_NAME FROM USER_GROUPS WHERE GROUP_ID = '" + m_iGroupId + "'")));//asharma326 jira 11081
                objElem.SetAttribute("loggedinuser", m_sLoginName);//asharma326 jira 11081

                objElem.SetAttribute("showregarding", p_sShowRegarding);
                //Merged by Nitin for R6 starts
                //SAFEWAY: paggarwal2-4: 04/11/2008: Added for Diary Screens - start
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    objElem.SetAttribute("Priority",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Priority/@selected").Value.ToString());
                    objElem.SetAttribute("Text",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Text/@selected").Value.ToString());
                    objElem.SetAttribute("Due",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Due/@selected").Value.ToString());
                    objElem.SetAttribute("WorkActivity",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/WorkActivity/@selected").Value.ToString());
                    //igupta3 jira 439
                    objElem.SetAttribute("NotRoutable",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRoutable/@selected").Value.ToString());
                    objElem.SetAttribute("NotRollable",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRollable/@selected").Value.ToString());

                    objElem.SetAttribute("AttachedRecord",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AttachedRecord/@selected").Value.ToString());
                    objElem.SetAttribute("ParentRecord",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ParentRecord/@selected").Value.ToString());
                    objElem.SetAttribute("Claimant",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Claimant/@selected").Value.ToString());
                    objElem.SetAttribute("ClaimStatus",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimStatus/@selected").Value.ToString());
                    objElem.SetAttribute("Department",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Department/@selected").Value.ToString());
                    objElem.SetAttribute("PriorityHeader",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/PriorityHeader/@value").Value.ToString());
                    objElem.SetAttribute("TextHeader",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/TextHeader/@value").Value.ToString());
                    objElem.SetAttribute("DueHeader",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/DueHeader/@value").Value.ToString());
                    objElem.SetAttribute("WorkActivityHeader",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/WorkActivityHeader/@value").Value.ToString());
                    //igupta3 jira 439
                    objElem.SetAttribute("NotRoutableHeader",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRoutableHeader/@value").Value.ToString());
                    objElem.SetAttribute("NotRollableHeader",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRollableHeader/@value").Value.ToString());
                    

                    objElem.SetAttribute("AttachedRecordHeader",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AttachedRecordHeader/@value").Value.ToString());
                    objElem.SetAttribute("ParentRecordHeader",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ParentRecordHeader/@value").Value.ToString());
                    objElem.SetAttribute("ClaimantHeader",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimantHeader/@value").Value.ToString());
                    objElem.SetAttribute("ClaimStatusHeader",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimStatusHeader/@value").Value.ToString());
                    objElem.SetAttribute("DepartmentHeader",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/DepartmentHeader/@value").Value.ToString());
                objElem.SetAttribute("lowercount", lStartAt.ToString());//27074
                objElem.SetAttribute("uppercount", uppercount.ToString());//27074
                    //Neha Mits 23347
                    objElem.SetAttribute("UserSortOrder",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/UserSortOrder/@value").Value.ToString());
                    //MITS 36519
                    objElem.SetAttribute("SortExpression",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/SortExpression/@value").Value.ToString());
                    //Added by amitosh for Dairy UI Changes
                    objElem.SetAttribute("AssignedUser",
                     objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssignedUser/@selected").Value.ToString());
                    objElem.SetAttribute("AssigningUser",
                        objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssigningUser/@selected").Value.ToString());
                    objElem.SetAttribute("AssignedUserHeader",
                         objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssignedUserHeader/@value").Value.ToString());
                    objElem.SetAttribute("AssigningUserHeader",
                objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssigningUserHeader/@value").Value.ToString());
                    //End Amitosh
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Priority/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Text/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Due/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Department/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Claimant/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimStatus/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AttachedRecord/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    //Indu - Mits 33843
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ParentRecord/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/WorkActivity/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    //igupta3 jira 439
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRoutable/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRollable/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssignedUser/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssigningUser/@selected").Value) > 0)
                    {
                        bIsNoColumnSelected = false;
                    }
                    objElem.SetAttribute("NoHeaderSelected", bIsNoColumnSelected.ToString());
                }//END PSARIN2
                //SAFEWAY: paggarwal2-4: 04/11/2008: Added for Diary Screens - end
                //Merged by Nitin for R6 ends

                //objDOM.Save (@"C:/objDOM.xml");

                //we do not want to throw an exception when there is no record..raman bhatia
                /*
                if (p_lRecordCount == 0)
                {
                    throw new RecordNotFoundException(Globalization.GetString("WPA.GetDiaryDom.NoRecordFound"));
                }
                */
                #endregion

                objDOM.AppendChild(objElem);
                objElem = null;
                TableValue = new List<string>();
                //PSARIN2 R8 Perf Imp
                if (p_lRecordCount != 0)
                {
                    //Raman: R7 Prf Imp
                    //TableValue arraylist maintains list of all admintracking tables. However this list is getting populated for every dairy
                    //Moving this arraylist population outside the while block

                    sSQL2 = "SELECT DISTINCT SYSTEM_TABLE_NAME FROM GLOSSARY Where GLOSSARY_TYPE_CODE = '468' ";
                    //objConn = DbFactory.GetDbConnection(m_sConnectionString);
                    //objConn.Open();
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL2))
                    {


                        while (objReader.Read())
                        {
                            TableValue.Add(Conversion.ConvertObjToStr(objReader.GetValue("SYSTEM_TABLE_NAME")));

                        }
                        //objConn.Close();
                    }
                }
                //PSARIN2 R8 Perf Imp
                lTempIndex = 1;
                while (lStartAt < lCounter && lStartAt <= p_lRecordCount)
                {
                    //Ankit-Start changes for mITS 27074 Custom paging
                    //iCLnToInt=Conversion.ConvertStrToInteger(lStartAt.ToString());
                    iCLnToInt = Conversion.ConvertStrToInteger(lTempIndex.ToString());
                    //Ankit-End changes                    
                    sPIXmlFormName = "";
                    objElem = objDOM.CreateElement("diary");
                    objElem.SetAttribute("entry_id", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ENTRY_ID"]));
                    lCompleteDate = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["COMPLETE_DATE"]));
                    lCreateDate = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["CREATE_DATE"]));

                    //Raman R7 Prf Imp                    //If sortorder is not 2 or 6 then lets not find attachtable for all records in database. Instead lets focus only on the records which
                    //would be displayed on the screen
                    //Attached records code is now moved to the filtered records after paging to reduce the database hits.     
                    #region  if (!bAttachedRecordSortingOrder)
                    if (!bAttachedRecordSortingOrder)
                    {
                        lRid = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_RECORDID"]));
                        if (lRid > 0)
                        {

                            switch (Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]))
                            {
                                case CLAIM:
                                case "claim":
                                    sAttached = "Claim: " + GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + lRid.ToString(), m_sConnectionString);
                                    break;
                                case EVENT:
                                case "event":
                                    sAttached = "Event: " + GetSingleString("EVENT_NUMBER", "EVENT", "EVENT_ID=" + lRid.ToString(), m_sConnectionString);

                                    break;
                                case ENTITY:
                                case "entity":

                                    objCache.GetEntityInfo(lRid, ref sFname, ref sLname);
                                    sName = sFname.Trim().Equals("") ? sLname : sFname + ", " + sLname;
                                    sAttached = "Person\\Entity: " + sName;
                                    break;
                                case CLAIMANT:
                                case "claimant":
                                    objCache.GetClaimantInfo(lRid, ref sFname, ref sLname);
                                    sName = sFname.Trim().Equals("") ? sLname : sFname + ", " + sLname;
                                    sAttached = "Claimant:" + sName;
                                    break;
                                //Start averma62 MITS - 28002
                                case POLICY:
                                case "policy":
                                    sAttached = "Policy: " + GetSingleString("POLICY_NAME", "POLICY", "POLICY_ID=" + lRid.ToString(), m_sConnectionString);
                                    break;
                                //End averma62  MITS - 28002
                                case PERSON_INVOLVED:
                                case "person_involved":
                                    sPIXmlFormName = GetPiXMLFormName(lRid, ref sAttached);
                                    break;
                                case TM_JOB_LOG:
                                case "tm_job_log":
                                    sAttached = "Task Manager Job ( " + lRid.ToString() + ")";
                                    break;
                                case FUNDS: // MITS: 22769 Case added for funds. We need to display the control number in the Attached Record field on the diary list screen.
                                case "funds":
                                    sAttached = "FUNDS: " + GetSingleString("CTL_NUMBER", "FUNDS", "TRANS_ID=" + lRid.ToString(), m_sConnectionString);
                                    break;
                                // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
                                case POLICY_ENH:
                                case "POLICY_ENH":
                                case "policy_enh":
                                    //Raman R7 Prf Imp
                                    //If sortorder is not 2 or 6 then lets not find attachtable for all records in database. Instead lets focus only on the records which
                                    //would be displayed on the screen
                                    //Attached records code would be moved to the filtered records after paging to reduce the database hits.    
                                    // Get the LOB information of the Policy
                                    int iLob = GetSingleInt("POLICY_TYPE", "POLICY_ENH", "POLICY_ID = " + lRid.ToString(), m_sConnectionString);
                                    objRow[iCLnToInt - 1]["ATTACH_TABLE"] = string.Concat("POLICY_ENH_", objCache.GetShortCode(iLob));
                                    sAttached = String.Concat("POLICY MANAGEMENT (", objCache.GetShortCode(iLob), "): ", GetSingleString("POLICY_NAME", "POLICY_ENH", string.Format("POLICY_ID={0}", lRid.ToString()), m_sConnectionString));
                                    break;
                                // npadhy End MITS 22028 Policies opening up in Maintenance when opened from Diaries

                                //rupal:start, r8 auto diary enh   
                                //scheduled activity for subrogation
                                case SCHEDULED_ACTIVITY:
                                case "scheduled_activity":
                                    sAttached = GetAttachPromptForScheduledActivity(lRid.ToString(), out sClaimID);
                                    break;
                                case CASE_MANAGEMENT:
                                case CMXTREATMENTPLN:
                                case "cm_x_treatment_pln":
                                case "cmxtreatmentpln":
                                    sAttached = GetAttachPropmtForCaseManagement(lRid.ToString());
                                    break;
                                case RESERVE_HISTORY:
                                case "reserve_history":
                                    sAttached = GetAttachPromptForReserveHistory(lRid.ToString(), out sClaimID);
                                    break;
                                case RESERVE_CURRENT:
                                case "reserve_current":
                                    sAttached = GetAttachPromptForReserveReview(lRid.ToString(), out sClaimID);
                                    break;
                                case CLAIM_LITIGATION:
                                case LITIGATION:
                                case "claim_x_litigation":
                                case "litigation":
                                    sAttached = GetAttachPromptForLitigation(lRid.ToString());
                                    break;
                                case FUND_AUTO_BATCH:
                                case AUTO_CLAIM_CHECKS:
                                case "funds_auto_batch":
                                case "autoclaimchecks":
                                    sAttached = GetAttachPromptForSchedulePayment(lRid.ToString());
                                    break;
                                /* 
                                case COMMENTS_TEXT:
                                case "comments_text":
                                    //get Attached Table for the comment
                                    string[] sCommentDetail = GetMultipleString("ATTACH_RECORDID,ATTACH_TABLE,COMMENT_ID", "COMMENTS_TEXT", "COMMENT_ID=" + lRid.ToString(), m_sConnectionString);
                                    sAttached = GetAttachPromptForcommentReview(sCommentDetail);                                    
                                break;*/
                                case CLAIM_PRG_NOTE:
                                case "claim_prg_note":
                                    sProgressNoteDetail = GetMultipleString("CLAIM_ID,EVENT_ID,CL_PROG_NOTE_ID", "CLAIM_PRG_NOTE", "CL_PROG_NOTE_ID=" + lRid.ToString(), m_sConnectionString);
                                    sAttachPromptDetail = GetAttachPromptForProgressNotes(sProgressNoteDetail);
                                    sAttached = sAttachPromptDetail[0];
                                    break;
                                //rupal:end r8 auto diary enh
                                //Added by sharishkumar for mits 35291
                                case PIDEPENDENT:
                                case "pidependent":                                    
                                        sDependentRowDetail = GetSingleString("PI_ROW_ID", "PI_X_DEPENDENT", "PI_DEP_ROW_ID=" + lRid.ToString(), m_sConnectionString);
                                        sDependentDetail = GetMultipleString("EVENT_ID,PI_ROW_ID", "PERSON_INVOLVED", "PI_ROW_ID=" + sDependentRowDetail, m_sConnectionString);
                                        sAttachPromptDetail = GetAttachPromptForDependent(sDependentDetail);
                                        sAttached = sAttachPromptDetail[0];                                   
                                    break;
                                //End mits 35291
                                default:
                                    //Raman R7 Prf Imp
                                    //If sortorder is not 2 or 6 then lets not find attachtable for all records in database. Instead lets focus only on the records which
                                    //would be displayed on the screen
                                    //Attached records code would be moved to the filtered records after paging to reduce the database hits.    
                                    lTmp = objCache.GetTableId(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]));
                                    if (Conversion.ConvertLongToBool(lTmp, m_iClientId))
                                    {
                                        sAttached = objCache.GetTableName(Conversion.ConvertStrToInteger(lTmp.ToString())) + " (" + lRid.ToString() + ")";
                                    }
                                    else
                                    {
                                        sAttached = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper() + lRid.ToString();
                                    }
                                    break;
                            }
                            objRow[iCLnToInt - 1]["ATTACH_PROMPT"] = sAttached;

                        }
                    }

                    //Mukul Added 2/7/2007 MITS 8782
                    #endregion

                    #region Set Image Attaribute
                    //pkandhari Jira 6412 starts
                    if ((Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ASSIGNED_GROUP"]) == "NA") || (string.IsNullOrEmpty(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ASSIGNED_GROUP"]).ToString())))
                    {
                        sAssignedUser = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ASSIGNED_USER"]).ToLower();
                        sAssignedGroup = "0";
                    }
                    else
                    {
                        if (!(objRow[iCLnToInt - 1]["ASSIGNED_GROUP"] is DBNull))
                        {
                            m_sGroupName = Convert.ToString(DbFactory.ExecuteScalar(m_sConnectionString, ("SELECT GROUP_NAME FROM USER_GROUPS WHERE GROUP_ID = '" + Conversion.ConvertObjToInt(objRow[iCLnToInt - 1]["ASSIGNED_GROUP"], m_iClientId) + "'")));
                        }
                        else
                            m_sGroupName = string.Empty;
                        sAssignedUser = m_sGroupName;
                        sAssignedGroup = "1";
                    }
                    //pkandhari Jira 6412 ends
                    sAssigningUser = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ASSIGNING_USER"]).ToLower();
                    if (lCompleteDate < lToday)
                    {
                        if (sAssigningUser == "system")
                        {
                            objElem.SetAttribute("img", IMG_SYSTEM_OVERDUE);
                            objElem.SetAttribute("alt", ALT_SYSTEM);
                        }
                        else if (sAssigningUser == p_sUserName.ToLower())
                        {
                            objElem.SetAttribute("img", IMG_USER_OVERDUE);
                            objElem.SetAttribute("alt", ALT_USER);
                        }
                        else
                        {
                            objElem.SetAttribute("img", IMG_USERS_OVERDUE);
                            objElem.SetAttribute("alt", ALT_USERS);
                        }
                    }
                    else
                    {
                        if (sAssigningUser == "system")
                        {
                            objElem.SetAttribute("img", IMG_SYSTEM);
                            objElem.SetAttribute("alt", ALT_SYSTEM);
                        }
                        else if (sAssigningUser == p_sUserName.ToLower())
                        {
                            objElem.SetAttribute("img", IMG_USER);
                            objElem.SetAttribute("alt", ALT_USER);
                        }
                        else
                        {
                            objElem.SetAttribute("img", IMG_USERS);
                            objElem.SetAttribute("alt", ALT_USERS);
                        }
                    }
                    #endregion

                    if (lCompleteDate != 0)
                    {
                        objElem.SetAttribute("complete_date", Conversion.GetDBDateFormat(lCompleteDate.ToString(), "d"));
                    }
                    if (lCreateDate != 0)
                    {
                        objElem.SetAttribute("create_date", Conversion.GetDBDateFormat(lCreateDate.ToString(), "d"));
                    }
                    sIsAttached = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["IS_ATTACHED"]);
                    if (sIsAttached.Equals("0"))
                    {
                        sIsAttached = "0";
                    }
                    else
                    {
                        sIsAttached = "1";
                    }
                    sStatus = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["STATUS_OPEN"]);
                    //Shruti for 10384
                    if (sStatus.Equals("0"))
                    {
                        sStatus = "Closed";
                    }
                    else
                    {
                        sStatus = "Open";
                    }
                    objElem.SetAttribute("status", sStatus);

                    //igupta3 Non roll/route check jira 439
                    sNotRoutable = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["NON_ROUTE_FLAG"]);
                    objElem.SetAttribute("notroutablecode", sNotRoutable);
                    if (sNotRoutable.Equals("-1"))
                    {
                        irouteCodeId = objCache.GetCodeId("N", "YES_NO"); //assigning reversly to handle routable field on UI 
                        sNotRoutable = objCache.GetCodeDesc(irouteCodeId, m_iLangCode);
                    }
                    else
                    {
                        irouteCodeId = objCache.GetCodeId("Y", "YES_NO");
                        sNotRoutable = objCache.GetCodeDesc(irouteCodeId, m_iLangCode);
                    }
                    
                    objElem.SetAttribute("routable", sNotRoutable);
                    sNotRollable = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["NON_ROLL_FLAG"]);
                    objElem.SetAttribute("notrollablecode", sNotRollable);
                    if (sNotRollable.Equals("-1"))
                    {
                        irollCodeId = objCache.GetCodeId("N", "YES_NO"); //assigning reversly to handle rollable field on UI
                        sNotRollable = objCache.GetCodeDesc(irollCodeId, m_iLangCode);
                    }
                    else
                    {
                        irollCodeId = objCache.GetCodeId("Y", "YES_NO");
                        sNotRollable = objCache.GetCodeDesc(irollCodeId, m_iLangCode);
                    }
                    objElem.SetAttribute("rollable", sNotRollable);

                    //Mukul Added 2/7/2007 MITS 8782
                    objElem.SetAttribute("assigned_user", sAssignedUser);
                    objElem.SetAttribute("assigned_group", sAssignedGroup); //pkandhari Jira 6412
                    objElem.SetAttribute("assigning_user", sAssigningUser);
                    objElem.SetAttribute("is_attached", sIsAttached);

                    sAttachTable = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper();

                    //Raman R7 Prf Imp 
                    //Converted ArrayList to List<string>
                    //Lets use available methods to search for item

                    if (TableValue.Contains(sAttachTable))
                    {
                        isAttachTable = true;
                    }

                    if (isAttachTable == true)
                    {
                        sAttachTable = "admintracking" + "|" + sAttachTable;

                        objElem.SetAttribute("attach_table", sAttachTable);
                        sAttachTable = "";
                        isAttachTable = false;
                    }

                //Added by Nitin for R5-R4 merging on 23Feb2009 for Mits 12308
                    //Changed by nadim For MITS 12308
                    else if (Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper() == "DISABILITY_PLAN")
                        // objElem.SetAttribute("attach_table", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper());
                        objElem.SetAttribute("attach_table", "PLAN");
                    //nadim MITS 12308
                    //by Nitin for R5-R4 merging on 23Feb2009 for Mits 12308
                    else
                        objElem.SetAttribute("attach_table", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper());
                    //nadim 11845

                    objElem.SetAttribute("attach_recordid", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_RECORDID"]));
                    objElem.SetAttribute("attsecrecid", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_SEC_REC_ID"]));
                    lRid = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_RECORDID"]));

                    #region sActivity
                    string sActivity = "";

                    //Geeta 11/21/06 : Modified to fix mits number 8139 for Sorting Work Activity
                    if (p_iSortOrder == 9 || p_iSortOrder == 13)
                    {
                        //Ankit-MITS 27074 start changes
                        sActivity = objRow[iCLnToInt - 1].ItemArray.GetValue(1).ToString();
                        //Ankit-MITS 27074 End changes
                    }
                    else
                    {
                        //Raman: R7 Prf Imp
                        //string sActivitySQL = "SELECT * FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID = " + objRow[iCLnToInt - 1]["ENTRY_ID"].ToString();
                        string sActivitySQL = "SELECT ACT_TEXT FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID = " + objRow[iCLnToInt - 1]["ENTRY_ID"].ToString();
                        DbReader objActivity = null;
                        objActivity = DbFactory.GetDbReader(m_sConnectionString, sActivitySQL);
                        if (objActivity != null)
                        {
                            try
                            {
                                if (objActivity.Read())
                                {
                                    sActivity = objActivity.GetString("ACT_TEXT");
                                }
                                else
                                {
                                    sActivity = "";
                                }
                            }
                            finally
                            {
                                objActivity.Close();
                                objActivity.Dispose();
                            }

                        }
                        else
                        {
                            sActivity = "";
                        }
                    }
                    objElem.SetAttribute("work_activity", sActivity);
                    sAttached = "";
                    sClaimStatus = "";
                    sClaimantName = "";
                    sDepartmentName = "";
                    
                    #endregion

                    if (lRid > 0)
                    {
                        #region switch Attach Table
                        switch (Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]))
                        {
                            case CLAIM:
                            case "claim":
                                //sAttached = "Claim: " +GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" +lRid.ToString(),m_sConnectionString);
                                //pmittal5 Confidential Record - If Attached claim record is confidential to current user, its diary should not appear  
                                sAttachID = GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + lRid.ToString(), m_sConnectionString);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                //End
                                sClaimStatus = GetClaimStatus(lRid.ToString());
                                if (sClaimStatus == "")
                                    sClaimStatus = " ";
                                // sClaimantName=GetSingleString("ENTITY.LAST_NAME, ENTITY.FIRST_NAME, PRIMARY_CLMNT_FLAG","CLAIMANT, ENTITY","CLAIMANT.CLAIM_ID ="+lRid.ToString()+" AND CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND CLAIMANT.PRIMARY_CLMNT_FLAG <> 0",m_sConnectionString);
                                //sClaimantName = GetClaimantsName(lRid);
                                //if(sClaimantName=="")
                                //    sClaimantName=" ";
                                sDepartmentName = GetDeptName(0, lRid, ref sOrgLevel);
                                if (sDepartmentName == "")
                                    sDepartmentName = " ";
                                //objElem.SetAttribute("claimant", sClaimantName);
                                //objElem.SetAttribute("department", sDepartmentName);
                                //objElem.SetAttribute("claimstatus", sClaimStatus);
                                break;
                            case EVENT:
                            case "event":
                                sDepartmentName = GetDeptName(lRid, 0, ref sOrgLevel);
                                //objElem.SetAttribute("department", sDepartmentName);
                                //sAttached = "Event: " + GetSingleString("EVENT_NUMBER", "EVENT", "EVENT_ID=" +lRid.ToString(),m_sConnectionString);
                                //pmittal5 Confidential Record - If Attached event record is confidential to current user, its diary should not appear  
                                sAttachID = GetSingleString("EVENT_NUMBER", "EVENT", "EVENT_ID=" + lRid.ToString(), m_sConnectionString);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                //End
                                break;
                            //Start averma62 MITS - 28002
                            case POLICY:
                            case "policy":
                                sAttachID = GetSingleString("POLICY_NAME", "POLICY", "POLICY_ID=" + lRid.ToString(), m_sConnectionString);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            // End averma62 MITS - 28002
                            //case ENTITY:
                            //case "entity":
                            //    objCache.GetEntityInfo(lRid,ref sFname,ref sLname);
                            //    sName=sFname.Trim().Equals("")?sLname:sFname+", "+sLname;
                            //    sAttached = "Person\\Entity:" ;
                            //    break;
                            //case CLAIMANT:
                            //case "claimant":
                            //    objCache.GetEntityInfo(lRid,ref sFname,ref sLname);
                            //    sName=sFname.Trim().Equals("")?sLname:sFname+", "+sLname;
                            //    sAttached = "Claimant:" + sName;
                            //    break;                          
                            case PERSON_INVOLVED:
                            case "person_involved":
                                sAttached = "";
                                sPIXmlFormName = GetPiXMLFormName(lRid, ref sAttached);
                                //pmittal5 Confidential Record
                                sAttachID = sPIXmlFormName;
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                //End
                                break;
                            //pmittal5 Confidential Record - If Attached funds record is confidential to current user, its diary should not appear  
                            case "funds":
                                sAttachID = GetSingleString("TRANS_ID", "FUNDS", "TRANS_ID=" + lRid.ToString(), m_sConnectionString);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;

                            //rupal:start, r8 auto diary enh
                            //scheduled activity for subrogation  
                            case SCHEDULED_ACTIVITY:
                            case "scheduled_activity":
                                sAttachID = GetAttachPromptForScheduledActivity(lRid.ToString(), out sClaimID);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                else
                                    SetCommonClaimDetails(sClaimID, objElem);
                                break;
                            case CASE_MANAGEMENT:
                            case CMXTREATMENTPLN:
                            case "cm_x_treatment_pln":
                            case "cmxtreatmentpln":
                                sAttachID = GetAttachPropmtForCaseManagement(lRid.ToString());
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            case RESERVE_HISTORY:
                            case "reserve_history":
                                sAttachID = GetAttachPromptForReserveHistory(lRid.ToString(), out sClaimID);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                else
                                    SetCommonClaimDetails(sClaimID, objElem);
                                break;
                            case RESERVE_CURRENT:
                            case "reserve_current":
                                sAttachID = GetAttachPromptForReserveReview(lRid.ToString(), out sClaimID);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                else
                                    SetCommonClaimDetails(sClaimID, objElem);
                                break;
                            case CLAIM_LITIGATION:
                            case LITIGATION:
                            case "claim_x_litigation":
                            case "litigation":
                                sAttachID = GetAttachPromptForLitigation(lRid.ToString());
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            case FUND_AUTO_BATCH:
                            case AUTO_CLAIM_CHECKS:
                            case "funds_auto_batch":
                            case "autoclaimchecks":
                                sAttachID = GetAttachPromptForSchedulePayment(lRid.ToString());
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            /*
                            case COMMENTS_TEXT:
                            case "comments_text":
                                //get Attached Table for the comment
                                string[] sCommentDetail = GetMultipleString("ATTACH_RECORDID,ATTACH_TABLE,COMMENT_ID", "COMMENTS_TEXT", "COMMENT_ID=" + lRid.ToString(), m_sConnectionString);
                                sAttachID = GetAttachPromptForcommentReview(sCommentDetail);
                                if (sAttachID != "")
                                {
                                    if (sCommentDetail.Length >= 2)
                                    {
                                        objElem.SetAttribute("attach_recordid_for_nav", sCommentDetail[0]);
                                        objElem.SetAttribute("attach_table_for_nav", sCommentDetail[1]);
                                    }
                                }
                                else
                                {
                                    sAttachID = "NOREC";
                                }
                                break;
                             */
                            case CLAIM_PRG_NOTE:
                            case "claim_prg_note":
                                sProgressNoteDetail = GetMultipleString("CLAIM_ID,EVENT_ID,CL_PROG_NOTE_ID", "CLAIM_PRG_NOTE", "CL_PROG_NOTE_ID=" + lRid.ToString(), m_sConnectionString);
                                sAttachPromptDetail = GetAttachPromptForProgressNotes(sProgressNoteDetail);
                                sAttachID = sAttachPromptDetail[0];
                                if (sAttachPromptDetail.Length >= 2)
                                {
                                    objElem.SetAttribute("attach_recordid_for_nav", sAttachPromptDetail[2]);
                                    objElem.SetAttribute("attach_table_for_nav", sAttachPromptDetail[1]);
                                    //objElem.SetAttribute("attach_recordid", sAttachPromptDetails[2]);
                                    //objElem.SetAttribute("attach_table", sAttachPromptDetails[1]);
                                }
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            //Added by sharishkumar for mits 35291
                            case PIDEPENDENT:
                            case "pidependent":
                                if (bAttachedRecordSortingOrder)
                                {
                                    sDependentRowDetail = GetSingleString("PI_ROW_ID", "PI_X_DEPENDENT", "PI_DEP_ROW_ID=" + lRid.ToString(), m_sConnectionString);
                                    sDependentDetail = GetMultipleString("EVENT_ID,PI_ROW_ID", "PERSON_INVOLVED", "PI_ROW_ID=" + sDependentRowDetail, m_sConnectionString);
                                    sAttachPromptDetail = GetAttachPromptForDependent(sDependentDetail);
                                    //tkatsarski: 02/04/15 - RMA-1601
                                    sAttachID = sAttachPromptDetail[0];
                                    if (sAttachPromptDetail.Length >= 2)
                                    {
                                        objElem.SetAttribute("attach_recordid_for_nav", sAttachPromptDetail[2]);
                                        objElem.SetAttribute("attach_table_for_nav", sAttachPromptDetail[1]);                                       
                                    }
                                    if (sAttachID == "")
                                        sAttachID = "NOREC";
                                }
                                break;
                            //End mits 35291
                            case CLAIMANT:
                            case "claimant":
                                sAttachID = GetSingleString("CLAIMANT_ROW_ID", "CLAIMANT", "CLAIMANT_ROW_ID=" + lRid.ToString(), m_sConnectionString);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            //rupal:end, r8 auto diary enh
                            default: //If Attached PI record is confidential to current user, its diary should not appear  
                                sPI = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).Substring(0, 2);
                                if (sPI == "PI")
                                {
                                    sAttachID = GetSingleString("PI_ROW_ID", "PERSON_INVOLVED", "PI_ROW_ID=" + lRid.ToString(), m_sConnectionString);
                                    if (sAttachID == "")
                                        sAttachID = "NOREC";
                                }
                                //End
                                break;
                            //default:
                            //    lTmp = objCache.GetTableId(Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCLnToInt-1]["ATTACH_TABLE"]));
                            //    if (Conversion.ConvertLongToBool(lTmp))
                            //    {
                            //        sAttached = objCache.GetTableName(Conversion.ConvertStrToInteger(lTmp.ToString())) + " (" + lRid.ToString() + ")";
                            //    }
                            //    else
                            //    {
                            //        sAttached =  Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCLnToInt-1]["ATTACH_TABLE"]).ToUpper()+ lRid.ToString();
                            //    }
                            //    break;
                        }
                        #endregion
                    }

                    //Merged by Nitin for R6 - Diary List - sorting start
                    //if (objElem.GetAttribute("claimant") == null || objElem.GetAttribute("claimant") == string.Empty)
                    //{
                    //    objElem.SetAttribute("claimant", "");
                    //}
                    //Merged by Nitin for R6 Diary List - sorting end
                    //Added by Amitosh to add attachPrompt as No if ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_PROMPT"])) is empty
                    if (!string.IsNullOrEmpty(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_PROMPT"])))
                        objElem.SetAttribute("attachprompt", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_PROMPT"]));
                    else
                        objElem.SetAttribute("attachprompt", "No");
                    //end Amitosh
                    objElem.SetAttribute("claimant", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["CLAIMANT"]));
                    objElem.SetAttribute("pixmlformname", sPIXmlFormName);
                    objElem.SetAttribute("priority", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["PRIORITY"]));
                    objElem.SetAttribute("tasksubject", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ENTRY_NAME"]));
                    objElem.SetAttribute("regarding", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["REGARDING"]));
                    objElem.InnerText = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ENTRY_NOTES"]);
                    // akaushik5 Changed for MITS 37900 Starts
                    //if (sAttachID != "NOREC") //pmittal5 Confidential Claim
                    objElem.SetAttribute("attachedrecordaccessible", (!sAttachID.Equals("NOREC")).ToString());
                    // akaushik5 Changed for MITS 37900 Ends
                    objDOM.FirstChild.AppendChild(objElem);
                    lStartAt = lStartAt + 1;
                    lTempIndex = lTempIndex + 1; //ankit-Custom paging
                    sAttachID = string.Empty;

                    //Added by Amitosh for Mits 27454
                    objElem.SetAttribute("department", sDepartmentName);
                    objElem.SetAttribute("claimstatus", sClaimStatus);
                    //Indu - mits 33843
                    if (Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]) != "0")
                    {
                        if (Enum.GetNames(typeof(PARENTCLAIM)).Any(w => w == Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper()) == true)
                        {
                            string sql = "SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID = " + Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]);
                            if (!string.IsNullOrEmpty(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"])))
                            {
                                DbReader objRd = DbFactory.GetDbReader(m_sConnectionString, sql);

                                using (objRd = DbFactory.GetDbReader(m_sConnectionString, sql))
                                {
                                    if (objRd.Read())
                                    {
                                        //Claim Id is passed for opening the parent claim from DiaryList and Cliam Number is for showing in the DiaryList grid
                                        string claimNumber = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]) + "Æ" + (string)objRd.GetValue("CLAIM_NUMBER") + "Æ" + "claim";
                                        objElem.SetAttribute("parentrecord", claimNumber);
                                    }
                                }
                            }
                        }
                        else if (Enum.GetNames(typeof(PARENTEVENT)).Any(w => w == Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper()) == true)
                        {
                            string sql = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID = " + Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]);
                            DbReader objRd = DbFactory.GetDbReader(m_sConnectionString, sql);

                            using (objRd = DbFactory.GetDbReader(m_sConnectionString, sql))
                            {
                                if (objRd.Read())
                                {
                                    //Event Id is passed for opening the parent event from DiaryList and Event Number is for showing in the DiaryList grid
                                    string eventNumber = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]) + "Æ" + (string)objRd.GetValue("EVENT_NUMBER") + "Æ" + "event";
                                    objElem.SetAttribute("parentrecord", eventNumber);
                                }
                            }
                        }
                        else if (Enum.GetNames(typeof(PARENTPOLICY)).Any(w => w == Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper()) == true)
                        {
                            string sql = "SELECT policy_name FROM POLICY WHERE POLICY_ID = " + Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]);
                            DbReader objRd = DbFactory.GetDbReader(m_sConnectionString, sql);

                            using (objRd = DbFactory.GetDbReader(m_sConnectionString, sql))
                            {
                                string policyNumber = string.Empty;
                                string policyName = string.Empty;
                                if (objRd.Read())
                                {
                                    policyNumber = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]) + "Æ";
                                    policyName = !(objRd.IsDBNull("POLICY_NAME")) ? (string)objRd.GetValue("POLICY_NAME") : string.Empty;
                                    policyNumber = policyNumber + policyName + "Æ" + "policy";
                                    objElem.SetAttribute("parentrecord", policyNumber);

                                }
                            }
                        }
                    }
                    
                    #region Adding DiarySupplemental Coulumn's Value in returning XML for each WPA_DIARY_ENTRY row.
                    //WWIG GAP20A - agupta298 - MITS 36804 - Start
                    tempNodes = objUserPrefXML.SelectNodes("//setting/DiaryConfig/DiarySupplemental/option");
                    
                    if (tempNodes != null && tempNodes.Count > 0)
                    {
                        foreach (XmlNode objOptionNode in tempNodes)
                        {
                            iFieldType = Conversion.CastToType<int>(objOptionNode.Attributes["type"].Value, out blnSuccess);
                            iFieldID = Conversion.CastToType<int>(objOptionNode.Attributes["field_id"].Value, out blnSuccess);
                            iRecordID = Conversion.CastToType<int>(objRow[iCLnToInt - 1]["ENTRY_ID"].ToString(), out blnSuccess);

                            enumSuppFieldType = (SupplementalFieldTypes)iFieldType;

                            //if ((enumSuppFieldType != SupplementalFieldTypes.SuppTypeMultiCode) && (enumSuppFieldType != SupplementalFieldTypes.SuppTypeMultiEntity) && (enumSuppFieldType != SupplementalFieldTypes.SuppTypeMultiState))
                                sSuppFieldData = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1][objOptionNode.Attributes["value"].Value]) != null ? Conversion.ConvertObjToStr(objRow[iCLnToInt - 1][objOptionNode.Attributes["value"].Value]) : "";
                            //else
                            //    sSuppFieldData = "MULTIVALUES";

                            objElem.SetAttribute(objOptionNode.Attributes["value"].Value, FormatDataBySuppFieldType(sSuppFieldData, enumSuppFieldType, iFieldID, iRecordID));
                        }
                    }
                    //WWIG GAP20A - agupta298 - MITS 36804 - End
                    #endregion
                   
                }
                //Adpator layer changes Sumeet
                //sReturnXmlValue=objDOM.InnerXml;
                //objDOM.InnerXml=objDOM.InnerXml.Replace(":","+");
                //objDOM.Save (@"C:/objDOM.xml");
                #region Adding DiarySupplemental Nodes for header config in returning XML in the end.
                //WWIG GAP20A - agupta298 - MITS 36804 - Start
                tempNodes = objUserPrefXML.SelectNodes("//setting/DiaryConfig/DiarySupplemental/option");

                if (tempNodes != null && tempNodes.Count > 0)
                {
                    //Case : when diaries element present but not the diary
                    if (objDOM.FirstChild != null && objDOM.FirstChild.FirstChild == null)
                        objDOM.FirstChild.AppendChild(objDiarySuppNode);

                    //Case : when diaries & diary both element present
                    else if (objDOM.FirstChild != null && objDOM.FirstChild.FirstChild != null)
                    {
                        if (objElem.ParentNode != null)
                        {
                            objElem.ParentNode.AppendChild(objDiarySuppNode);
                        }
                        else // When Last Diary is "NOREC" and objElem.ParentNode is Null
                        {
                            objDOM.FirstChild.AppendChild(objDiarySuppNode);
                        }
                    }
                    foreach (XmlNode objOptionNode in tempNodes)
                    {
                        objRowElement = objDOM.CreateElement("option");
                        objRowElement.SetAttribute("ColumnName", objOptionNode.Attributes["value"].Value);
                        objRowElement.SetAttribute("HeaderText", objOptionNode.InnerText);

                        iFieldType = Conversion.CastToType<int>(objOptionNode.Attributes["type"].Value, out blnSuccess);
                        enumSuppFieldType = (SupplementalFieldTypes)iFieldType;

                        objRowElement.SetAttribute("FieldType", enumSuppFieldType.ToString());
                        objRowElement.SetAttribute("IsSortable", IsDiarySuppSortable(enumSuppFieldType).ToString());

                        if (SupplementalFieldTypes.SuppTypeHyperlink == (SupplementalFieldTypes)iFieldType)
                        {
                            string[] arrData = objCache.GetWebLinkDataBySuppFieldId(Convert.ToInt32(objOptionNode.Attributes["field_id"].Value));
                            if (!string.IsNullOrEmpty(arrData[0]) && !string.IsNullOrEmpty(arrData[1]))
                            {
                                objRowElement.SetAttribute("WebLinkName", arrData[0]);
                                objRowElement.SetAttribute("WebLinkURL", arrData[1]);
                            }
                        }

                        objRowElement.SetAttribute("FieldID", objOptionNode.Attributes["field_id"].Value);
                        objDiarySuppNode.AppendChild((XmlNode)objRowElement);
                    }
                }
                //WWIG GAP20A - agupta298 - MITS 36804 - End
                #endregion

                return (objDOM);
            }
            catch (RecordNotFoundException p_objException)
            {
                throw p_objException;

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetDiaryDom.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objDataSet != null) objDataSet.Dispose();
                if (objCache != null) objCache.Dispose();
                objRow = null;
                objDOM = null;
                objElem = null;
                objUserPrefXML = null; //Umesh
                objXml = null;  //Umesh
                objShowActiveDiary = null; //Umesh
                objSaveShowActiveDiary = null; //Umesh
                objCShowActiveDiary = null; //Ankit-MITS 27074
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                //WWIG GAP20A - agupta298 - MITS 36804 - Start
                tempNodes = null;
                objDiarySuppNode = null;
                objRowElement = null;
                //WWIG GAP20A - agupta298 - MITS 36804 - End
            }
            //Adpator layer changes Sumeet
            //return (sReturnXmlValue);
        }

       #endregion getdiarydom

        /// <summary>
        /// Mits 23568 added filter to diary screen.
        /// Gets the filter part of query.
        /// </summary>
        /// <param name="sTaskType"></param>
        /// <param name="sTaskName"></param>
        /// <returns></returns>
        private string GetFilterForTaskname(string sTaskType, string sTaskName)
        {
            string sReturnFilterQuery = "";
            switch (sTaskType)
            {
                case "C":
                    sReturnFilterQuery = " AND (REGARDING LIKE '%" + sTaskName + "%' OR ENTRY_NAME LIKE '%" + sTaskName + "%' OR ENTRY_NOTES LIKE '%" + sTaskName + "%' " +
                                           " OR (REGARDING IS  NULL AND ENTRY_NOTES IS NOT NULL AND(ENTRY_NAME+ENTRY_NOTES) LIKE '%" + sTaskName + "%')" +
" OR (ENTRY_Notes IS NULL AND REGARDING IS NOT NULL AND  (ENTRY_NAME+REGARDING) LIKE '%" + sTaskName + "%')" +
" OR(ENTRY_NAME+REGARDING+ENTRY_NOTES LIKE'%" + sTaskName + "%')) ";
                    break;
                case "DC":
                    sReturnFilterQuery = " AND( REGARDING NOT LIKE '%" + sTaskName + "%' OR REGARDING IS NULL)  AND ( ENTRY_NOTES NOT LIKE '%" + sTaskName + "%' OR ENTRY_NOTES IS NULL) " +
                        " AND ((REGARDING IS  NULL AND ENTRY_NOTES IS  NULL AND (ENTRY_NAME) NOT LIKE '%" + sTaskName + "%')" +
" OR (REGARDING IS  NULL AND ENTRY_NOTES IS NOT NULL AND(ENTRY_NAME+ENTRY_NOTES) NOT LIKE '%" + sTaskName + "%')" +
" OR (ENTRY_Notes IS NULL AND REGARDING IS NOT NULL AND  (ENTRY_NAME+REGARDING) NOT LIKE '%" + sTaskName + "%')" +
" OR(ENTRY_NAME+REGARDING+ENTRY_NOTES NOT LIKE'%" + sTaskName + "%' ))";
                    break;
                case "SW":
                    sReturnFilterQuery = " AND ENTRY_NAME LIKE '" + sTaskName + "%' ";
                    break;
                case "EW":
                    sReturnFilterQuery = " AND ((REGARDING IS  NULL AND ENTRY_NOTES IS NOT NULL AND(ENTRY_NAME+ENTRY_NOTES) LIKE '%" + sTaskName + "')" +
                    " OR (ENTRY_NOTES IS  NULL AND REGARDING IS NOT NULL AND (ENTRY_NAME+REGARDING)  LIKE '%" + sTaskName + "') " +
                    " OR (ENTRY_NOTES IS NOT NULL AND REGARDING IS NOT NULL AND (ENTRY_NAME+REGARDING+ENTRY_NOTES)  LIKE '%" + sTaskName + "') " +
                    " OR(ENTRY_NAME )LIKE '%" + sTaskName + "' )";
                    break;
                case "ET":
                    sReturnFilterQuery = " AND ((REGARDING IS  NULL AND ENTRY_NOTES IS  NULL AND (ENTRY_NAME)= '" + sTaskName + "')" +
" OR (REGARDING IS  NULL AND ENTRY_NOTES IS NOT NULL AND(ENTRY_NAME+ENTRY_NOTES)= '" + sTaskName + "')" +
" OR (ENTRY_Notes IS NULL AND REGARDING IS NOT NULL AND  (ENTRY_NAME+REGARDING)= '" + sTaskName + "')" +
" OR(ENTRY_NAME+REGARDING+ENTRY_NOTES)='" + sTaskName + "' )";
                    break;
                case "NE":
                    sReturnFilterQuery = " AND ((REGARDING IS  NULL AND ENTRY_NOTES IS  NULL AND (ENTRY_NAME)!= '" + sTaskName + "')" +
" OR (REGARDING IS  NULL AND ENTRY_NOTES IS NOT NULL AND(ENTRY_NAME+ENTRY_NOTES)!= '" + sTaskName + "')" +
" OR (ENTRY_Notes IS NULL AND REGARDING IS NOT NULL AND  (ENTRY_NAME+REGARDING)!= '" + sTaskName + "')" +
" OR(ENTRY_NAME+REGARDING+ENTRY_NOTES)!='" + sTaskName + "' )";
                    break;
            }
            if (m_sDBType == Constants.DB_ORACLE)
                sReturnFilterQuery = sReturnFilterQuery.Replace("+", "||");

            return sReturnFilterQuery;
        }
        #endregion

        #region Void Diary
        /// <summary>
        /// This function is use to make diary VOID
        /// </summary>
        /// <param name="p_sFromDate">From date</param>
        /// <param name="p_sToDate">To date</param>
        /// <param name="p_sAssignUser">Assigned User</param>
        /// <param name="p_lDiaryId">Diary Id</param>
        public void VoidDiary(string p_sFromDate, string p_sToDate, string p_sAssignUser, long p_lDiaryId)
        {
            string sSQL = "";
            DbConnection objConn = null;
            try
            {
                if (p_lDiaryId > 0)
                {
                    sSQL = "UPDATE WPA_DIARY_ENTRY SET DIARY_VOID=1 WHERE ENTRY_ID = " + p_lDiaryId.ToString();
                }
                else
                {
                    sSQL = "UPDATE WPA_DIARY_ENTRY SET DIARY_VOID=1 " +
                        " WHERE COMPLETE_DATE >= '" + Conversion.GetDate(p_sFromDate) + "'" +
                        " AND COMPLETE_DATE <= '" + Conversion.GetDate(p_sToDate) + "'" +
                        " AND ASSIGNED_USER = '" + p_sAssignUser + "'";
                }
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.VoidDiary.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
            }
        }
        #endregion

        #region Attach Diary
        /// <summary>
        /// This function returns list of users having a common dsn id
        /// </summary>
        /// <param name="p_sDsnId">dsn id</param>
        /// <returns>Returns diaries attached as xml string</returns>
        public XmlDocument AttachDiary(string p_sDsnId)
        {
            string sSQL = "";
            XmlDocument objDOM = null;
            XmlElement objElem = null;
            DbReader objReader = null;
            try
            {
                sSQL = " SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.DSNID = " + p_sDsnId + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME";
                objDOM = new XmlDocument();
                objElem = objDOM.CreateElement("users");
                objDOM.AppendChild(objElem);
                objElem = null;
                objReader = DbFactory.GetDbReader(m_sSecConnectString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        objElem = objDOM.CreateElement("user");
                        objElem.SetAttribute("userid", Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")).Trim());
                        objElem.SetAttribute("firstname", Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")).Trim());
                        objElem.SetAttribute("lastname", Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Trim());
                        objElem.SetAttribute("loginname", Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME")).Trim());
                        objDOM.FirstChild.AppendChild(objElem);
                        objElem = null;
                    }
                }
                // Adaptor layer chages Sumeet
                //sReturnXmlValue=objDOM.InnerXml;
                return (objDOM);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.AttachDiary.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objDOM = null;
                objElem = null;
            }
            // Adaptor layer chages Sumeet
            //return sReturnXmlValue;

        }
        #endregion

        #region Diary Calendar
        /// <summary>
        /// This function returns Diary Calendar based on passed parameters
        /// </summary>
        /// <param name="p_sAssignedUser">Passed Assigned User name</param>
        /// <param name="p_sCompleteDate">Passed Complete date</param>
        /// <param name="p_sSortBy">Order by clause of sql query</param>
        /// <param name="p_sSortOrder">Order by clause ASC/DESC</param>
        /// <param name="sLangCode">Language Code</param>
	    /// <param name="sPageId">Page Id of CreateDiary Page</param>
        /// <returns>Returns diary calendar in XML string</returns>
        public XmlDocument DiaryCalender(string p_sAssignedUser, string p_sFromDate, string p_sToDate, string p_sSortBy, string p_sSortOrder, int p_iDateRange, int p_iDiaryStyle, int p_iSelectedMonth, int p_iSelectedYear, string sLangCode, string sPageId)
        {
            string sSQL = "";
            string sPrevDate = "";
            string sTempDate = "";
            string sPriority = "";
            string sActPriority = "";
            string sCompleteDate = "";
            string sDate = "";
            string[] arrSortBy = null;
            string[] arrSortOrder = null;
            int iCount = 0;
            int iTempDate = 0;
            DateTime datFrom;
            DateTime datTo;
            DateTime datTempDate;
            XmlDocument objDOM = null;
            XmlElement objElem = null;
            DbReader objReader = null;
            DataSet objData = null;
            DataRow objDataRow = null;
            XmlElement objNode = null;
            DataColumn objDC = null;
            SortedList objStoreInsertPosition = null;
            StringBuilder sbSortBy = null;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            string priority = string.Empty;
            DateTimeFormatInfo currentDateTimeFormat = null;
            int iMonth = 0;
            
            try
            {
                currentDateTimeFormat = (new CultureInfo(Conversion.ConvertStrToInteger(sLangCode))).DateTimeFormat;
                sbSortBy = new StringBuilder();
                datFrom = new DateTime();
                datTo = new DateTime();

                if (p_sFromDate != "")
                    datFrom = Conversion.ToDate(Conversion.GetDate(p_sFromDate));
                if (p_sToDate != "")
                    datTo = Conversion.ToDate(Conversion.GetDate(p_sToDate));

                //altering From and To Date depending on diary range selection...added by Raman Bhatia

                switch (p_iDateRange)
                {
                    case 1:
                        while (datFrom.DayOfWeek.ToString() != "Monday")
                            datFrom = datFrom.AddDays(-1);
                        p_sFromDate = datFrom.ToString();
                        while (datTo.DayOfWeek.ToString() != "Sunday")
                            datTo = datTo.AddDays(1);
                        p_sToDate = datTo.ToString();
                        break;

                    case 2:
                        if (p_iDiaryStyle == 1)
                        {
                            while (datFrom.DayOfWeek.ToString() != "Monday")
                                datFrom = datFrom.AddDays(-1);
                            p_sFromDate = datFrom.ToString();
                            while (datTo.DayOfWeek.ToString() != "Sunday")
                                datTo = datTo.AddDays(1);
                            p_sToDate = datTo.ToString();
                        }
                        break;

                    case 4:
                        if (p_iDiaryStyle == 2)
                        {
                            datFrom = new DateTime(p_iSelectedYear, p_iSelectedMonth, 1);
                            datTo = datFrom.AddMonths(1).AddDays(-1);
                            p_sFromDate = datFrom.ToString();
                            p_sToDate = datTo.ToString();
                        }
                        break;



                }


                datTempDate = new DateTime();
                sSQL = "SELECT * FROM WPA_DIARY_ENTRY WHERE DIARY_VOID=0 AND DIARY_DELETED=0 AND STATUS_OPEN <> 0 AND ASSIGNED_USER='" + p_sAssignedUser + "'";
                sSQL = sSQL + " AND COMPLETE_DATE >= '" + Conversion.GetDate(p_sFromDate) + "'";
                sSQL = sSQL + " AND COMPLETE_DATE <= '" + Conversion.GetDate(p_sToDate) + "'";
                arrSortBy = p_sSortBy.Split(new char[] { ',' });
                arrSortOrder = p_sSortOrder.Split(new char[] { ',' });
                sbSortBy.Append(" COMPLETE_DATE ASC ;");



                for (int iCnt = 0; iCnt < arrSortBy.Length; iCnt++)
                {
                    switch (arrSortBy[iCnt])
                    {
                        case "1":
                            sbSortBy.Append(" PRIORITY " + arrSortOrder[iCnt] + " ;");
                            break;
                        case "2":
                            sbSortBy.Append(" ENTRY_NAME " + arrSortOrder[iCnt] + " ;");
                            break;
                        case "3":
                            sbSortBy.Append(" COMPLETE_TIME " + arrSortOrder[iCnt] + " ;");
                            break;
                    }
                }
                if (!Conversion.ConvertObjToStr(sbSortBy).Trim().Equals(""))
                {
                    sSQL = sSQL + " ORDER BY ";
                }
                arrSortBy = Conversion.ConvertObjToStr(sbSortBy).Split(new char[] { ';' });
                for (int i = 0; i < arrSortBy.Length; i++)
                {
                    if (i < (arrSortBy.Length - 1))
                    {
                        sSQL = sSQL + arrSortBy[i];
                        if (!arrSortBy[i + 1].Equals(""))
                        {
                            sSQL = sSQL + ",";
                        }
                    }
                }
                objData = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
                if (objData.Tables.Count == 0)
                {
                    throw new RMAppException(Globalization.GetString("WPA.DiaryCalender.DatasetCreationError", m_iClientId));
                }
                objDC = new DataColumn("Show_Value", System.Type.GetType("System.String"));
                objDC.DefaultValue = "0";
                objData.Tables[0].Columns.Add(objDC);
                objStoreInsertPosition = new SortedList();
                iCount = 0;
                iTempDate = 0;
                datTempDate = datFrom.AddDays(iTempDate);
                foreach (DataRow objDR in objData.Tables[0].Rows)
                {
                    sCompleteDate = Conversion.ConvertObjToStr(objDR["COMPLETE_DATE"]).Trim();
                    if (sPrevDate == sCompleteDate)
                    {
                        iCount++;
                        continue;
                    }
                    if (sCompleteDate == Conversion.GetDate(Conversion.ConvertObjToStr(datTempDate.Date)))
                    {
                        iCount++;
                        iTempDate++;
                        datTempDate = datFrom.AddDays(iTempDate);
                    }
                    if (Conversion.ConvertStrToInteger(Conversion.GetDate(Conversion.ConvertObjToStr(datTempDate.Date))) < Conversion.ConvertStrToInteger(sCompleteDate))
                    {
                        while (Conversion.ConvertStrToInteger(Conversion.GetDate(Conversion.ConvertObjToStr(datTempDate.Date))) < Conversion.ConvertStrToInteger(sCompleteDate))
                        {
                            sDate = Conversion.GetDate(Conversion.ConvertObjToStr(datTempDate.Date));
                            objStoreInsertPosition.Add(iCount, sDate);
                            iCount++;
                            iTempDate++;
                            datTempDate = datFrom.AddDays(iTempDate);
                        }
                        if (sCompleteDate == Conversion.GetDate(Conversion.ConvertObjToStr(datTempDate.Date)))
                        {
                            iCount = iCount + 1;
                            iTempDate++;
                            datTempDate = datFrom.AddDays(iTempDate);
                        }
                    }
                    sPrevDate = sCompleteDate;
                }

                while (Conversion.ConvertStrToInteger(Conversion.GetDate(Conversion.ConvertObjToStr(datTempDate.Date))) <= Conversion.ConvertStrToInteger(Conversion.GetDate(p_sToDate)))
                {
                    sDate = Conversion.GetDate(Conversion.ConvertObjToStr(datTempDate.Date));
                    objStoreInsertPosition.Add(iCount, sDate);
                    iCount++;
                    iTempDate++;
                    datTempDate = datFrom.AddDays(iTempDate);
                }
                objDOM = new XmlDocument();
                objElem = objDOM.CreateElement("Diaries");
                objDOM.AppendChild(objElem);
                objElem = null;
                for (int i = 0; i < objStoreInsertPosition.Count; i++)
                {
                    objDataRow = objData.Tables[0].NewRow();
                    objDataRow["COMPLETE_DATE"] = Conversion.ConvertObjToStr(objStoreInsertPosition.GetByIndex(i));
                    objDataRow["Show_Value"] = "1";
                    objData.Tables[0].Rows.InsertAt(objDataRow, Conversion.ConvertObjToInt(objStoreInsertPosition.GetKey(i), m_iClientId));
                }
                foreach (DataRow objTempDataRow in objData.Tables[0].Rows)
                {
                    sCompleteDate = Conversion.ConvertObjToStr(objTempDataRow["COMPLETE_DATE"]).Trim();
                    //sDate = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objTempDataRow["COMPLETE_DATE"]).Trim(), "m");
                    sDate = Conversion.ConvertObjToStr(objTempDataRow["COMPLETE_DATE"]);
                    iMonth = Conversion.ConvertStrToInteger(sDate.Substring(4,2));
                    sDate = sDate.Substring(6, 2);
                    sDate = currentDateTimeFormat.GetMonthName(iMonth) + " " + sDate;
                    if (sTempDate != sCompleteDate)
                    {
                        objElem = null;
                        objElem = objDOM.CreateElement("DiaryCalendar");
                        objElem.SetAttribute("Date", sDate);
                        objDOM.FirstChild.AppendChild(objElem);
                    }
                    objNode = objDOM.CreateElement("Diary");
                    sPriority = Conversion.ConvertObjToStr(objTempDataRow["PRIORITY"]).Trim();
                    sActPriority = "";
                    if (!strDictParams.ContainsKey("PageId"))
                        strDictParams.Add("PageId", sPageId);

                    //if (!strDictParams.ContainsKey("LangCode"))
                    //    strDictParams.Add("LangCode", m_iLangCode.ToString());
                    strDictParams.Remove("ResourceKey");

                    switch (sPriority)
                    {
                        case "1":
                            sSQL = string.Empty;
                            strDictParams.Add("ResourceKey", "Optional");
                            //sSQL = "SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID=~LangCode~";
                            sSQL = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID={0}", m_iLangCode);
                            priority = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource",m_iClientId), sSQL.ToString(), strDictParams));
                            sActPriority = priority;
                            // sActPriority = "Optional";
                            break;
                        case "2":
                            sSQL = string.Empty;
                            strDictParams.Add("ResourceKey", "Important");
                            sSQL = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID={0}", m_iLangCode);
                            priority = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), sSQL.ToString(), strDictParams));
                            sActPriority = priority;
                            //sActPriority = "Important";
                            break;
                        case "3":
                            sSQL = string.Empty;
                            strDictParams.Add("ResourceKey", "Required");
                            sSQL = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID={0}", m_iLangCode);
                            priority = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), sSQL.ToString(), strDictParams));
                            sActPriority = priority;
                            // sActPriority = "Required";
                            break;
                    }
                    if (!Conversion.ConvertObjToStr(objTempDataRow["Show_Value"]).Trim().Equals("1"))
                    {
                        objNode.SetAttribute("Priority", sActPriority);
                        objNode.SetAttribute("Time_Due", Conversion.GetUITime(Conversion.ConvertObjToStr(objTempDataRow["COMPLETE_TIME"]).Trim(),sLangCode,  m_iClientId));
                        objNode.SetAttribute("Time_Estimated", Conversion.ConvertObjToStr(objTempDataRow["ESTIMATE_TIME"]).Trim());
                        objNode.SetAttribute("Task_Name", Conversion.ConvertObjToStr(objTempDataRow["ENTRY_NAME"]).Trim());
                        objNode.SetAttribute("Regarding", Conversion.ConvertObjToStr(objTempDataRow["REGARDING"]).Trim());
                        objElem.AppendChild(objNode);
                    }
                    if (sTempDate == sCompleteDate)
                    {
                        objDOM.FirstChild.AppendChild(objElem);
                    }
                    objNode = null;
                    sTempDate = sCompleteDate;
                }
                //Adaptor layer changes Sumeet
                //sReturnXmlValue=objDOM.InnerXml;
                return (objDOM);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.DiaryCalender.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objDOM = null;
                objElem = null;
                if (objData != null) objData.Dispose();
                objDataRow = null;
                objNode = null;
                objDC = null;
                sbSortBy = null;
                objStoreInsertPosition = null;
            }
        }
        #endregion

        #region Diary List
        /// <summary>
        /// This function returns Diary List based on passed parameters
        /// </summary>
        /// <param name="p_sAssignedUser">Passed Assigned User name</param>
        /// <param name="p_iDiarySel"></param>
        /// <param name="p_sFromDate">Passed From date</param>
        /// /// <param name="p_sToDate">Passed To date</param>
        /// <param name="p_sSortBy">Order by clause</param>
        /// <param name="p_sTaskDesciption">Task Description</param>
        /// <returns>Returns Xml string containing Diary list</returns>
        /// 
        public XmlDocument DiaryList(string p_sAssignedUser, int p_iDiarySel, string p_sFromDate, string p_sToDate, string p_sSortBy, string p_sTaskDesciption, string p_sTaskName, string p_sCreatedOn, string p_sDueDate, string p_sWorkActivity, string p_sEstimatedTime, string p_sPriority, string p_sNotes, string p_sRegarding, string p_sScreen, string p_sPageId, string p_sCurrentPageId, string p_sSortOrder) // new parameter added for MITS 36691:aaggarwal29
        {
            string sSQL = "";
            string sCompleteDate = "";
            string sPriority = "";
            string sActPriority = "";
            string strTodaysDate = "";
            string sAssignor = "";
            string sEntryName = "";
            string sAttachedRecord = "";
            string sAttachedTable = "";
            string sCreateDate = "";
            string sEntryId = "";
            string sTempActivity = "";
            string sIsAttached = "";
            string sIdNumber = "";
            string sClaimantName = "";
            string sDepartmentName = "";
            string sOrgLevel = "";
            string[] arrSortBy = null;
            int iInLoop = 0;
            XmlDocument objDOM = null;
            XmlElement objElem = null;
            DataSet objData = null;
            DbReader objActivityReader = null;
            XmlElement objActivities = null;
            XmlElement objActivity = null;
            DbReader objReader = null;
            StringBuilder sbSortBy = null;
            DateTime datFrom;
            DateTime datTo;
            LocalCache objCache = null;
            //rupal:start,r8 auto diary enh
            string sClaimID = string.Empty;
            string[] sProgressNoteDetail;
            string[] sAttachPromptDetail;
            //Added by sharishkumar for mits 35291
            string sDependentRowDetail;
            string[] sDependentDetail;
            //End mits 35291
            bool bConverted = false;
            string sFname = string.Empty;
            string sLname = string.Empty;
            string sName = string.Empty;
            long lClaimantEId = 0;
            //rupal:end

            //asingh263 MITS 34874 starts
            string sTaskName=string.Empty;
            string sCreatedOn=string.Empty;
            string sDueDate=string.Empty;
            string sWorkActivity=string.Empty;
            string sEstimatedTime=string.Empty;
            string sNotes=string.Empty;
            string sRegarding=string.Empty;
            string sScreen=string.Empty;
            //asingh263 MITS 34874 ends

            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            string priority = string.Empty;

            string[] arrSortOrder = null;  // MITS 36691: aaggarwal29

            try
            {
                sbSortBy = new StringBuilder();
                datFrom = new DateTime();
                datTo = new DateTime();
                strTodaysDate = Conversion.GetDate(DateTime.Now.Date.ToString());
                objCache = new LocalCache(m_sConnectionString,m_iClientId);
                sSQL = "SELECT * FROM WPA_DIARY_ENTRY WHERE DIARY_VOID=0 AND DIARY_DELETED=0 AND STATUS_OPEN <> 0 AND ASSIGNED_USER='" + p_sAssignedUser + "' ";
                if (p_sTaskDesciption != "")
                {
                    //MITS 32210 Raman Bhatia
                    sSQL += " AND ENTRY_NAME like'%" + p_sTaskDesciption + "%'";
                    //sSQL += " AND ENTRY_NAME ='" + p_sTaskDesciption + "'";
                }
                switch (p_iDiarySel)
                {
                    case 0:
                        sSQL = sSQL + " AND COMPLETE_DATE = '" + strTodaysDate + "'";
                        break;
                    case 1:
                        sSQL = sSQL + " AND COMPLETE_DATE >= '" + strTodaysDate + "'";
                        break;
                    case 2:
                        sSQL = sSQL + " AND COMPLETE_DATE < '" + strTodaysDate + "'";
                        break;
                    case 4:
                        sSQL = sSQL + " AND COMPLETE_DATE BETWEEN '" + Conversion.GetDate(p_sFromDate) + "' AND '" + Conversion.GetDate(p_sToDate) + "'";
                        break;
                }
                arrSortBy = p_sSortBy.Split(new char[] { ',' });
                arrSortOrder = p_sSortOrder.Split(new char[] { ',' }); //aaggarwal29: MITS 36691
                if ((p_sFromDate != null))
                {
                    if (p_sFromDate != "")
                    {
                        datFrom = Conversion.ToDate(Conversion.GetDate(p_sFromDate));
                    }
                }
                if ((p_sToDate != null))
                {
                    if (p_sFromDate != "")
                    {
                        datTo = Conversion.ToDate(Conversion.GetDate(p_sToDate));
                    }
                }
                // aaggarwal29: MITS 36691 sbSortBy modified to add sort direction in below switch case
                for (int i = 0; i < arrSortBy.Length; i++)
                {
                    switch (arrSortBy[i])
                    {
                        case "1":
                            sbSortBy.Append(" COMPLETE_DATE ");
                            sbSortBy.Append(arrSortOrder[i] + " ;");
                            break;
                        case "2":
                            sbSortBy.Append(" PRIORITY ");
                            sbSortBy.Append(arrSortOrder[i] + " ;");
                            break;
                        case "3":
                            sbSortBy.Append(" ATTACH_TABLE ");
                            sbSortBy.Append(arrSortOrder[i] + ",");
                            sbSortBy.Append(" ATTACH_RECORDID ");
                            sbSortBy.Append(arrSortOrder[i] + " ;");
                            break;
                        case "4":
                            sbSortBy.Append(" ENTRY_NAME ");
                            sbSortBy.Append(arrSortOrder[i] + " ;");
                            break;
                    }
                }
                if (!Conversion.ConvertObjToStr(sbSortBy).Trim().Equals(""))
                {
                    sSQL = sSQL + " ORDER BY ";
                }
                arrSortBy = Conversion.ConvertObjToStr(sbSortBy).Split(new char[] { ';' });
                for (int i = 0; i < arrSortBy.Length; i++)
                {
                    if (i < (arrSortBy.Length - 1))
                    {
                        sSQL = sSQL + arrSortBy[i];
                        if (!arrSortBy[i + 1].Equals(""))
                        {
                            sSQL = sSQL + ",";
                        }
                    }
                }
                objData = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
                if (objData.Tables.Count == 0)
                {
                    throw new RMAppException(Globalization.GetString("WPA.DiaryList.DatasetCreationError", m_iClientId));
                }
                objDOM = new XmlDocument();
                objElem = objDOM.CreateElement("Diaries");
                objDOM.AppendChild(objElem);
                objElem = null;
                objElem = objDOM.CreateElement("DiaryList");
                objDOM.FirstChild.AppendChild(objElem);
                objElem = null;

                foreach (DataRow objDataRow in objData.Tables[0].Rows)
                {
                    sAssignor = Conversion.ConvertObjToStr(objDataRow["ASSIGNING_USER"]).Trim();
                    sEntryName = Conversion.ConvertObjToStr(objDataRow["ENTRY_NAME"]).Trim();
                    sCompleteDate = Conversion.GetUIDate(Conversion.ConvertObjToStr(objDataRow["COMPLETE_DATE"]).Trim(), Convert.ToString(m_iLangCode),m_iClientId);
                    sCreateDate = Conversion.GetUIDate(Conversion.ConvertObjToStr(objDataRow["CREATE_DATE"]).Trim(), Convert.ToString(m_iLangCode),m_iClientId);
                    sEntryId = Conversion.ConvertObjToStr(objDataRow["ENTRY_ID"]).Trim();
                    sPriority = Conversion.ConvertObjToStr(objDataRow["PRIORITY"]).Trim();
                    sIsAttached = Conversion.ConvertObjToStr(objDataRow["IS_ATTACHED"]).Trim();
                    //asingh263 MITS 34874
                    sTaskName = Conversion.ConvertObjToStr(objDataRow["ENTRY_NAME"]).Trim();
                    sDueDate = Conversion.GetUIDate(Conversion.ConvertObjToStr(objDataRow["COMPLETE_DATE"]).Trim(), Convert.ToString(m_iLangCode),m_iClientId);
                    sCreatedOn = Conversion.GetUIDate(Conversion.ConvertObjToStr(objDataRow["CREATE_DATE"]).Trim(), Convert.ToString(m_iLangCode),m_iClientId);
                    sEstimatedTime = Conversion.ConvertObjToStr(objDataRow["ESTIMATE_TIME"]).Trim();
                    sNotes = Conversion.ConvertObjToStr(objDataRow["ENTRY_NOTES"]).Trim();
                    sRegarding = Conversion.ConvertObjToStr(objDataRow["REGARDING"]).Trim();
                    // akaushik5 Added for MITS 37900 Starts
                    sIdNumber = string.Empty;
                    // akaushik5 Added for MITS 37900 Ends
                    //asingh263 MITS 34874 ends
                    if (!sIsAttached.Equals("0"))
                    {
                        sAttachedTable = Conversion.ConvertObjToStr(objDataRow["ATTACH_TABLE"]).Trim();
                        sAttachedRecord = Conversion.ConvertObjToStr(objDataRow["ATTACH_RECORDID"]).Trim();
                        switch (sAttachedTable)
                        {
                            case CLAIM:
                                sSQL = "SELECT CLAIM_NUMBER, LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + sAttachedRecord;
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                while (objReader.Read())
                                {
                                    sIdNumber = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER")).Trim();
                                }
                                objReader.Close();
                                break;
                            case ENTITY:
                                sSQL = "SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID= " + sAttachedRecord;
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                while (objReader.Read())
                                {
                                    sIdNumber = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Trim() != "" ? Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")).Trim() + ", " + Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Trim() : Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")).Trim();
                                }
                                objReader.Close();
                                break;
                            //abisht MITS 10899 
                            //Case for event was missing that is why it was printing Event Id instead of Event Number.
                            case EVENT:
                                sSQL = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID = " + sAttachedRecord;
                                using (DbReader objReaderLocal = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                {
                                    while (objReaderLocal.Read())
                                    {
                                        sIdNumber = Conversion.ConvertObjToStr(objReaderLocal.GetValue("EVENT_NUMBER")).Trim();
                                    }
                                }
                                break;
                            case POLICY_ENH:
                            case "POLICY_ENH":
                                sSQL = "SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID = " + sAttachedRecord;
                                using (DbReader objReaderLocal = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                {
                                    while (objReaderLocal.Read())
                                    {
                                        sIdNumber = Conversion.ConvertObjToStr(objReaderLocal.GetValue("POLICY_NAME")).Trim();
                                    }
                                }
                                //objReader.Close();
                                break;
                            case POLICY:
                                sSQL = "SELECT POLICY_NAME FROM POLICY WHERE POLICY_ID = " + sAttachedRecord;
                                using (DbReader objReaderLocal = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                {
                                    while (objReaderLocal.Read())
                                    {
                                        sIdNumber = Conversion.ConvertObjToStr(objReaderLocal.GetValue("POLICY_NAME")).Trim();
                                    }
                                }
                                break;
                            //rupal:start, r8 auto diary enh      
                            case CLAIMANT:
                            case "claimant":
                                lClaimantEId = Conversion.CastToType<long>(sAttachedRecord, out bConverted);
                                if (lClaimantEId != 0)
                                {
                                    objCache.GetClaimantInfo(lClaimantEId, ref sFname, ref sLname);
                                    sName = sFname.Trim().Equals("") ? sLname : sFname + ", " + sLname;
                                    sIdNumber = "Claimant:" + sName;
                                }
                                break;
                            //scheduled activity for subrogation  
                            case SCHEDULED_ACTIVITY:
                            case "scheduled_activity":
                                sIdNumber = GetAttachPromptForScheduledActivity(sAttachedRecord, out sClaimID);
                                break;
                            case CASE_MANAGEMENT:
                            case CMXTREATMENTPLN:
                            case "cm_x_treatment_pln":
                            case "cmxtreatmentpln":
                                sIdNumber = GetAttachPropmtForCaseManagement(sAttachedRecord);
                                break;
                            case RESERVE_HISTORY:
                            case "reserve_history":
                                sIdNumber = GetAttachPromptForReserveHistory(sAttachedRecord, out sClaimID);
                                break;
                            case RESERVE_CURRENT:
                            case "reserve_current":
                                sIdNumber = GetAttachPromptForReserveReview(sAttachedRecord, out sClaimID);
                                break;
                            case CLAIM_LITIGATION:
                            case LITIGATION:
                            case "claim_x_litigation":
                            case "litigation":
                                sIdNumber = GetAttachPromptForLitigation(sAttachedRecord);
                                break;
                            case FUND_AUTO_BATCH:
                            case AUTO_CLAIM_CHECKS:
                            case "funds_auto_batch":
                            case "autoclaimchecks":
                                sIdNumber = GetAttachPromptForSchedulePayment(sAttachedRecord);
                                break;
                            /*
                            case COMMENTS_TEXT:
                            case "comments_text":                                
                                string[] sCommentDetail = GetMultipleString("ATTACH_RECORDID,ATTACH_TABLE,COMMENT_ID", "COMMENTS_TEXT", "COMMENT_ID=" + sAttachedRecord, m_sConnectionString);
                                sIdNumber = GetAttachPromptForcommentReview(sCommentDetail);
                                break;*/
                            case CLAIM_PRG_NOTE:
                            case "claim_prg_note":
                                sProgressNoteDetail = GetMultipleString("CLAIM_ID,EVENT_ID,CL_PROG_NOTE_ID", "CLAIM_PRG_NOTE", "CL_PROG_NOTE_ID=" + sAttachedRecord, m_sConnectionString);
                                sAttachPromptDetail = GetAttachPromptForProgressNotes(sProgressNoteDetail);
                                sIdNumber = sAttachPromptDetail[0];
                                break;
                            //rupal:end                                                                                                
                            //Added by sharishkumar for mits 35291
                            case PIDEPENDENT:
                            case "pidependent":
                                sDependentRowDetail = GetSingleString("PI_ROW_ID", "PI_X_DEPENDENT", "PI_DEP_ROW_ID=" + sAttachedRecord, m_sConnectionString);
                                sDependentDetail = GetMultipleString("EVENT_ID,PI_ROW_ID", "PERSON_INVOLVED", "PI_ROW_ID=" + sDependentRowDetail, m_sConnectionString);
                                sAttachPromptDetail = GetAttachPromptForDependent(sDependentDetail);                                    
                                sIdNumber = sAttachPromptDetail[0];
                                break;
                            //End mits 35291
                            default:
                                sIdNumber = sAttachedRecord;
                                break;
                        }
                        // akaushik5 Added for MITS 37900 Starts
                        if (string.IsNullOrEmpty(sIdNumber))
                        {
                            sIdNumber = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("RecordNotAccessible", 0, p_sCurrentPageId, m_iClientId), this.m_iLangCode.ToString());
                        }
                        // akaushik5 Added for MITS 37900 Ends
                    }
                    else
                    {
                       // sAttachedTable = "No";
                        //sAttachedTable = CommonFunctions.FilterBusinessMessage(Globalization.GetString("WPA.DiaryList.NoAttachedTable"), this.m_iLangCode.ToString()); 
                        sAttachedTable = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("NoAttachedTable",0,p_sCurrentPageId, m_iClientId), this.m_iLangCode.ToString());
                        sIdNumber = "";
                    }
                    sActPriority = "";
                    if (!strDictParams.ContainsKey("PageId"))
                        strDictParams.Add("PageId", p_sPageId);

                    //if (!strDictParams.ContainsKey("LangCode"))
                    //    strDictParams.Add("LangCode", m_iLangCode.ToString());
                    strDictParams.Remove("ResourceKey");

                    switch (sPriority)
                    {
                        case "1":
                            sSQL = string.Empty;
                            strDictParams.Add("ResourceKey", "Optional");
                            //sSQL = "SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID=~LangCode~";
                            sSQL = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID={0}", m_iLangCode);
                            priority = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), sSQL.ToString(), strDictParams));
                            sActPriority = priority;
                            // sActPriority = "Optional";
                            break;
                        case "2":
                            sSQL = string.Empty;
                            strDictParams.Add("ResourceKey", "Important");
                            sSQL = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID={0}", m_iLangCode);
                            priority = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), sSQL.ToString(), strDictParams));
                            sActPriority = priority;
                            //sActPriority = "Important";
                            break;
                        case "3":
                            sSQL = string.Empty;
                            strDictParams.Add("ResourceKey", "Required");
                            sSQL = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID={0}", m_iLangCode);
                            priority = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), sSQL.ToString(), strDictParams));
                            sActPriority = priority;
                            // sActPriority = "Required";
                            break;
                    }
                    objElem = objDOM.CreateElement("Diary");
                    //asingh263 MITS 34874 starts
                    if(sScreen=="selected")
                    {
                    objElem.SetAttribute("taskname",sTaskName);
                    objElem.SetAttribute("createdon",sCreatedOn);
                    objElem.SetAttribute("estimatetime",sEstimatedTime);
                    objElem.SetAttribute("duedate", sCompleteDate);
                    objElem.SetAttribute("priority", sPriority);
                    objElem.SetAttribute("notes", sNotes);
                    objElem.SetAttribute("regarding",sRegarding);
                    objDOM.FirstChild.FirstChild.AppendChild(objElem);
                    }
                    else
                    {
                        //asingh263 MITS 34874 ENDS
                    objElem.SetAttribute("Assigned_By", sAssignor);
                    objElem.SetAttribute("Due_Date", sCompleteDate);
                    objElem.SetAttribute("Priority", sActPriority);
                    objActivities = objDOM.CreateElement("Activities");
                    sSQL = "";
                    sSQL = "SELECT ACT_TEXT FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID = " + sEntryId;
                    if (objActivityReader != null)
                        objActivityReader.Close();
                    objActivityReader = null;
                    objActivityReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    iInLoop = 0;
                    //while (objActivityReader.Read())  //pmittal5 Mits 18578 11/18/09 - Only first work activity for a diary should be visible while Printing
                    if (objActivityReader.Read())
                    {
                        iInLoop = 1;
                        sTempActivity = Conversion.ConvertObjToStr(objActivityReader.GetValue("ACT_TEXT")).Trim();
                        objActivity = objDOM.CreateElement("Activity");
                        objActivity.InnerText = sTempActivity;
                        objActivities.AppendChild(objActivity);
                    }
                    if (iInLoop == 0)
                    {
                        //sTempActivity = "No Activity Specified";
                        //sTempActivity = CommonFunctions.FilterBusinessMessage(Globalization.GetString("WPA.DiaryList.NoActivitySpecified"), this.m_iLangCode.ToString()); 
                        sTempActivity = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("NoActivitySpecified", 0, p_sCurrentPageId, m_iClientId), this.m_iLangCode.ToString());
                        objActivity = objDOM.CreateElement("Activity");
                        objActivity.InnerText = sTempActivity;
                        objActivities.AppendChild(objActivity);
                    }
                    objElem.AppendChild(objActivities);
                    //abisht MITS 10899
                    // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
                    switch (sAttachedTable)
                    {
                        case "POLICYENH":
                        case "POLICY_ENH":
                            int iLob = GetSingleInt("POLICY_TYPE", "POLICY_ENH", "POLICY_ID = " + sAttachedRecord.ToString(), m_sConnectionString);
                            objElem.SetAttribute("AttachedRecord", string.Concat("POLICY MANAGEMENT (", objCache.GetShortCode(iLob), ") ", sIdNumber));
                            break;
                        case "POLICY":
                            objElem.SetAttribute("AttachedRecord", string.Concat("POLICY TRACKING ", sIdNumber));
                            break;
                        //rupal:start r8 auto diary enh
                        //scheduled activity for subrogation  
                        //for followinf templates, we already have formatted string for attach record    
                        case SCHEDULED_ACTIVITY:
                        case "scheduled_activity":
                        case CASE_MANAGEMENT:
                        case CMXTREATMENTPLN:
                        case "cm_x_treatment_pln":
                        case "cmxtreatmentpln":
                        case CLAIM_PRG_NOTE:
                        case "claim_prg_note":
                        //Added by sharishkumar for mits 35291
                        case PIDEPENDENT:
                        case "pidependent":
                        //End mits 35291
                        case RESERVE_CURRENT:
                        case CLAIM_LITIGATION:
                        case LITIGATION:
                        case "claim_x_litigation":
                        case "litigation":
                        case FUND_AUTO_BATCH:
                        case AUTO_CLAIM_CHECKS:
                        case "funds_auto_batch":
                        case "autoclaimchecks":
                        case RESERVE_HISTORY:
                        case CLAIMANT:
                        case "claimant":
                            objElem.SetAttribute("AttachedRecord", sIdNumber);
                            break;
                        //rupal:end r8
                        default:
                            objElem.SetAttribute("AttachedRecord", string.Concat(sAttachedTable, " ", sIdNumber));
                            break;
                    }
                    // npadhy End MITS 22028 Policies opening up in Maintenance when opened from Diaries 
                    if (sAttachedTable.ToUpper() == "CLAIM")
                    {
                        sClaimantName = GetClaimantsName(Conversion.ConvertStrToLong(sAttachedRecord));
                        objElem.SetAttribute("claimantname", sClaimantName);
                        //rupal:start, r8 auto diary enh
                        // akaushik5 Commented for MITS 30888 Starts
                        //if (Conversion.ConvertStrToLong(sAttachedRecord) > 0)
                        //    sDepartmentName = GetDeptName(0, Conversion.ConvertStrToLong(sAttachedRecord), ref sOrgLevel);
                        //else
                        //    sDepartmentName = "";
                        // akaushik5 Commented for MITS 30888 Ends
                        //rupal:end
                    }
                    //rupal:start, r8 auto diary enh
                    //GetDeptName function accepts eventid, and claim id as input parameter
                    //below line makes call to this function with out realizing if attachedrecordid is claimid or not
                    //i feel this call to the function 'GetDeptName()' should be made only if attached recordid is claim
                    //therefore this lines are commented here and placed inside above mentioned if statement
                    /*
                    if (Conversion.ConvertStrToLong(sAttachedRecord) > 0)
                        sDepartmentName = GetDeptName(0, Conversion.ConvertStrToLong(sAttachedRecord), ref sOrgLevel);
                    else
                        sDepartmentName = "";
                     */
                    //rupal:end

                    // akaushik5 Added for MITS 30888 Starts
                    sDepartmentName = string.Empty;
                    if (!string.IsNullOrEmpty(sAttachedTable) && (sAttachedTable.ToUpper().Equals("CLAIM") || sAttachedTable.ToUpper().Equals("EVENT")))
                    {
                        bool success;
                        long attachedRecord = Conversion.CastToType<long>(sAttachedRecord, out success);
                        
                        if(sAttachedTable.ToUpper().Equals("CLAIM"))
                        {
                            sDepartmentName = success && !attachedRecord.Equals(default(long)) ? GetDeptName(0, attachedRecord, ref sOrgLevel) : string.Empty;
                        }
                        else if (sAttachedTable.ToUpper().Equals("EVENT"))
                        {
                            sDepartmentName = success && !attachedRecord.Equals(default(long)) ? GetDeptName(attachedRecord, 0, ref sOrgLevel) : string.Empty;
                        }
                    }
                    // akaushik5 Added for MITS 30888 Ends

                    objElem.SetAttribute("departmentname", sDepartmentName);
                    objElem.SetAttribute("Create_Date", sCreateDate);
                    objElem.SetAttribute("Task_Description", sEntryName);
                    objDOM.FirstChild.FirstChild.AppendChild(objElem);
                    }
                }//mits 34874
                //Adaptor layer changes Sumeet
                //sReturnXmlValue=objDOM.InnerXml;


                return (objDOM);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.DiaryList.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objActivityReader != null)
                {
                    objActivityReader.Close();
                    objActivityReader.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objCache != null)
                    objCache.Dispose();
                objDOM = null;
                objElem = null;
                objData = null;
                objActivities = null;
                objActivity = null;
                sbSortBy = null;
            }
            //Adaptor layer changes Sumeet
            //return (sReturnXmlValue);

        }
        #endregion

        #region Diary list for Mobile Adjuster
        /// <summary>
        /// This function returns Diary List based on passed parameters
        /// </summary>
        /// <param name="p_sAssignedUser">Passed Assigned User name</param>
        /// <param name="p_iDiarySel"></param>
        /// <param name="p_sFromDate">Passed From date</param>
        /// /// <param name="p_sToDate">Passed To date</param>
        /// <param name="p_sSortBy">Order by clause</param>
        /// <param name="p_sTaskDesciption">Task Description</param>
        /// <param name="p_sClaimNumber">Claim Number</param>
        /// <returns>Returns Xml string containing Diary list</returns>
        public XmlDocument DiaryList(string p_sAssignedUser, string p_sFromDate, string p_sToDate, string p_sSortBy, string p_sTaskDesciption, string p_sClaimNumber, string caller, string p_sPageId, string p_sCurrentPageId, string createDate)//Mobility changes Mits 35351
        {
            string sSQL = string.Empty;
            string sSQLClaim = string.Empty;
            string sCompleteDate = string.Empty;
            string sPriority = string.Empty;
            string sActPriority = string.Empty;
            string strTodaysDate = string.Empty;
            string sAssignor = string.Empty;
            string sEntryName = string.Empty;
            string sAttachedRecord = string.Empty;
            string sAttachedTable = string.Empty;
            string sCreateDate = string.Empty;
            string sEntryId = string.Empty;
            string sTempActivity = string.Empty;
            string sIsAttached = string.Empty;
            string sIdNumber = string.Empty;
            string sClaimantName = string.Empty;
            string sDepartmentName = string.Empty;
            string sOrgLevel = string.Empty;
            string sClaimNumber = string.Empty;
            string sClaimId = string.Empty;
            string[] arrSortBy = null;
            string lastSync = string.Empty;
            int iInLoop = 0;
            XmlDocument objDOM = null;
            XmlElement objElem = null;
            DataSet objData = null;
            DbReader objActivityReader = null;
            XmlElement objActivities = null;
            XmlElement objActivity = null;
            DbReader objReader = null;
            StringBuilder sbSortBy = null;
            DateTime datFrom;
            DateTime datTo;
            LocalCache objCache = null;
            string sNotes = string.Empty; //Mobility changes Mits  35351
            string sCompleteTime = string.Empty; //Mobility changes Mits  35447

            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            string priority = string.Empty;

            try
            {
                if (!(string.IsNullOrEmpty(p_sClaimNumber)))
                {
                    sSQLClaim = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + p_sClaimNumber + "'";
                    using (DbReader objReaderClaim = DbFactory.GetDbReader(m_sConnectionString, sSQLClaim))
                    {
                        //Yatharth: Change if there is dummy claim number entered by the user in the search
                        if (objReaderClaim.Read())
                        {
                            sClaimId = Conversion.ConvertObjToStr(objReaderClaim.GetValue("CLAIM_ID")).Trim();
                        }
                        else
                        {
                            sClaimId = "-1";
                        }
                    }
                }

                sbSortBy = new StringBuilder();
                datFrom = new DateTime();
                datTo = new DateTime();
                strTodaysDate = Conversion.GetDate(DateTime.Now.Date.ToString());
                objCache = new LocalCache(m_sConnectionString,m_iClientId);
                if (!(string.IsNullOrEmpty(sClaimId)))
                    sSQL = "SELECT * FROM WPA_DIARY_ENTRY WHERE DIARY_VOID=0 AND DIARY_DELETED=0 AND STATUS_OPEN <> 0 AND ASSIGNED_USER='" + p_sAssignedUser + "' AND ATTACH_TABLE = 'CLAIM' AND ATTACH_RECORDID ='" + sClaimId + "' ";
                else
                    sSQL = "SELECT * FROM WPA_DIARY_ENTRY WHERE DIARY_VOID=0 AND DIARY_DELETED=0 AND STATUS_OPEN <> 0 AND ASSIGNED_USER='" + p_sAssignedUser + "' ";
                if (p_sTaskDesciption != "")
                    sSQL += " AND ENTRY_NAME ='" + p_sTaskDesciption + "'";
                if (string.IsNullOrEmpty(createDate))
                    sSQL += " AND CREATE_DATE>='" + createDate + "'";

                if (string.IsNullOrEmpty(p_sToDate) && !(string.IsNullOrEmpty(p_sFromDate)))
                    sSQL = sSQL + " AND COMPLETE_DATE >= '" + p_sFromDate + "'";
                else if (string.IsNullOrEmpty(p_sFromDate) && !(string.IsNullOrEmpty(p_sToDate)))
                    sSQL = sSQL + " AND COMPLETE_DATE < '" + p_sToDate + "'";
                else if (!(string.IsNullOrEmpty(p_sFromDate)) && !(string.IsNullOrEmpty(p_sToDate)))
                    sSQL = sSQL + " AND COMPLETE_DATE BETWEEN '" + Conversion.GetDate(p_sFromDate) + "' AND '" + Conversion.GetDate(p_sToDate) + "'";

                arrSortBy = p_sSortBy.Split(new char[] { ',' });
                if ((p_sFromDate != null))
                {
                    if (p_sFromDate != "")
                    {
                        datFrom = Conversion.ToDate(Conversion.GetDate(p_sFromDate));
                    }
                }
                if ((p_sToDate != null))
                {
                    if (p_sFromDate != "")
                    {
                        datTo = Conversion.ToDate(Conversion.GetDate(p_sToDate));
                    }
                }

                for (int i = 0; i < arrSortBy.Length; i++)
                {
                    switch (arrSortBy[i])
                    {
                        case "5":
                            sbSortBy.Append(" COMPLETE_DATE DESC ;");
                            break;
                        case "2":
                            sbSortBy.Append(" PRIORITY DESC ;");
                            break;
                        case "3":
                            sbSortBy.Append(" ATTACH_TABLE, ATTACH_RECORDID ;");
                            break;
                        case "4":
                            sbSortBy.Append(" ENTRY_NAME ;");
                            break;
                    }
                }
                if (!Conversion.ConvertObjToStr(sbSortBy).Trim().Equals(""))
                {
                    sSQL = sSQL + " ORDER BY ";
                }
                arrSortBy = Conversion.ConvertObjToStr(sbSortBy).Split(new char[] { ';' });
                for (int i = 0; i < arrSortBy.Length; i++)
                {
                    if (i < (arrSortBy.Length - 1))
                    {
                        sSQL = sSQL + arrSortBy[i];
                        if (!arrSortBy[i + 1].Equals(""))
                        {
                            sSQL = sSQL + ",";
                        }
                    }
                }
                objData = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
                if (objData.Tables.Count == 0)
                {
                    throw new RMAppException(Globalization.GetString("WPA.DiaryList.DatasetCreationError", m_iClientId));
                }
                objDOM = new XmlDocument();
                objElem = objDOM.CreateElement("diaries");
                objDOM.AppendChild(objElem);
                objElem = null;
                //objElem = objDOM.CreateElement("DiaryList");
                //objDOM.FirstChild.AppendChild(objElem);
                //objElem = null;

                foreach (DataRow objDataRow in objData.Tables[0].Rows)
                {
                    sAssignor = Conversion.ConvertObjToStr(objDataRow["ASSIGNING_USER"]).Trim();
                    sEntryName = Conversion.ConvertObjToStr(objDataRow["ENTRY_NAME"]).Trim();
                    sCompleteDate = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDataRow["COMPLETE_DATE"]).Trim(), "d");
                    sCreateDate = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDataRow["CREATE_DATE"]).Trim(), "d");
                    sEntryId = Conversion.ConvertObjToStr(objDataRow["ENTRY_ID"]).Trim();
                    sPriority = Conversion.ConvertObjToStr(objDataRow["PRIORITY"]).Trim();
                    sIsAttached = Conversion.ConvertObjToStr(objDataRow["IS_ATTACHED"]).Trim();
                    //Mobility changes Mits 35351

                    if (caller == "MobilityAdjuster")
                    {
                        sNotes = Conversion.ConvertObjToStr(objDataRow["ENTRY_NOTES"]).Trim(); //notes mini
                        sCompleteTime = Conversion.GetDBTimeFormat(Conversion.ConvertObjToStr(objDataRow["COMPLETE_TIME"]).Trim(), "t"); //Mobility changes Mits  35447

                    }
                    //Mobility changes Mits 35351

                    if (!sIsAttached.Equals("0"))
                    {
                        sAttachedTable = Conversion.ConvertObjToStr(objDataRow["ATTACH_TABLE"]).Trim();
                        sAttachedRecord = Conversion.ConvertObjToStr(objDataRow["ATTACH_RECORDID"]).Trim();
                        switch (sAttachedTable)
                        {
                            case CLAIM:
                                sSQL = "SELECT CLAIM_NUMBER, LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + sAttachedRecord;
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                while (objReader.Read())
                                {
                                    sIdNumber = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER")).Trim();
                                }
                                objReader.Close();
                                break;
                            case ENTITY:
                                sSQL = "SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID= " + sAttachedRecord;
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                while (objReader.Read())
                                {
                                    sIdNumber = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Trim() != "" ? Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")).Trim() + ", " + Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Trim() : Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")).Trim();
                                }
                                objReader.Close();
                                break;
                            //abisht MITS 10899 
                            //Case for event was missing that is why it was printing Event Id instead of Event Number.
                            case EVENT:
                                sSQL = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID = " + sAttachedRecord;
                                using (DbReader objReaderLocal = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                {
                                    while (objReaderLocal.Read())
                                    {
                                        sIdNumber = Conversion.ConvertObjToStr(objReaderLocal.GetValue("EVENT_NUMBER")).Trim();
                                    }
                                }
                                break;
                            case POLICY_ENH:
                            case "POLICY_ENH":
                                sSQL = "SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID = " + sAttachedRecord;
                                using (DbReader objReaderLocal = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                {
                                    while (objReaderLocal.Read())
                                    {
                                        sIdNumber = Conversion.ConvertObjToStr(objReaderLocal.GetValue("POLICY_NAME")).Trim();
                                    }
                                }
                                //objReader.Close();
                                break;
                            case POLICY:
                                sSQL = "SELECT POLICY_NAME FROM POLICY WHERE POLICY_ID = " + sAttachedRecord;
                                using (DbReader objReaderLocal = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                {
                                    while (objReaderLocal.Read())
                                    {
                                        sIdNumber = Conversion.ConvertObjToStr(objReaderLocal.GetValue("POLICY_NAME")).Trim();
                                    }
                                }
                                //objReader.Close();
                                break;
                            default:
                                sIdNumber = sAttachedRecord;
                                break;
                        }
                    }
                    else
                    {
                        //sAttachedTable = "No";
                       // sAttachedTable = CommonFunctions.FilterBusinessMessage(Globalization.GetString("WPA.DiaryList.NoAttachedTable"), this.m_iLangCode.ToString());
                        sAttachedTable = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("NoAttachedTable", 0, p_sCurrentPageId, m_iClientId), this.m_iLangCode.ToString());
                        sIdNumber = "";
                    }
                    sActPriority = "";
                    if (!strDictParams.ContainsKey("PageId"))
                        strDictParams.Add("PageId", p_sPageId);

                    //if (!strDictParams.ContainsKey("LangCode"))
                    //    strDictParams.Add("LangCode", m_iLangCode.ToString());
                    strDictParams.Remove("ResourceKey");

                    switch (sPriority)
                    {
                        case "1":
                            sSQL = string.Empty;
                            strDictParams.Add("ResourceKey", "Optional");
                            //sSQL = "SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID=~LangCode~";
                            sSQL = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID={0}", m_iLangCode);
                            priority = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), sSQL.ToString(), strDictParams));
                            sActPriority = priority;
                            // sActPriority = "Optional";
                            break;
                        case "2":
                            sSQL = string.Empty;
                            strDictParams.Add("ResourceKey", "Important");
                            sSQL = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID={0}", m_iLangCode);
                            priority = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), sSQL.ToString(), strDictParams));
                            sActPriority = priority;
                            //sActPriority = "Important";
                            break;
                        case "3":
                            sSQL = string.Empty;
                            strDictParams.Add("ResourceKey", "Required");
                            sSQL = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID={0}", m_iLangCode);
                            priority = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), sSQL.ToString(), strDictParams));
                            sActPriority = priority;
                            // sActPriority = "Required";
                            break;
                    }
                    objElem = objDOM.CreateElement("diary");
                    //objElem.SetAttribute("Assigned_By", sAssignor);
                    objElem.SetAttribute("complete_date", sCompleteDate);
                    //objElem.SetAttribute("Priority", sActPriority);
                    //objActivities = objDOM.CreateElement("Activities");
                    sSQL = "";
                    sSQL = "SELECT ACT_TEXT FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID = " + sEntryId;
                    if (objActivityReader != null)
                        objActivityReader.Close();
                    objActivityReader = null;
                    objActivityReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    iInLoop = 0;
                    //while (objActivityReader.Read())  //pmittal5 Mits 18578 11/18/09 - Only first work activity for a diary should be visible while Printing
                    if (objActivityReader.Read())
                    {
                        iInLoop = 1;
                        sTempActivity = Conversion.ConvertObjToStr(objActivityReader.GetValue("ACT_TEXT")).Trim();
                        //objActivity = objDOM.CreateElement("Activity");
                        //objActivity.InnerText = sTempActivity;
                        //objActivities.AppendChild(objActivity);
                        objElem.SetAttribute("work_activity", sTempActivity);
                    }
                    if (iInLoop == 0)
                    {
                        //sTempActivity = "No Activity Specified";
                        //sTempActivity = CommonFunctions.FilterBusinessMessage(Globalization.GetString("WPA.DiaryList.NoActivitySpecified"), this.m_iLangCode.ToString());  
                        sTempActivity = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("NoActivitySpecified", 0, p_sCurrentPageId, m_iClientId), this.m_iLangCode.ToString());
                        //objActivity = objDOM.CreateElement("Activity");
                        //objActivity.InnerText = sTempActivity;
                        //objActivities.AppendChild(objActivity);
                        objElem.SetAttribute("work_activity", sTempActivity);
                    }
                    //objElem.AppendChild(objActivities);
                    //abisht MITS 10899
                    // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
                    switch (sAttachedTable)
                    {
                        case "POLICYENH":
                        case "POLICY_ENH":
                            int iLob = GetSingleInt("POLICY_TYPE", "POLICY_ENH", "POLICY_ID = " + sAttachedRecord.ToString(), m_sConnectionString);
                            objElem.SetAttribute("attachprompt", string.Concat("POLICY MANAGEMENT (", objCache.GetShortCode(iLob), ") ", sIdNumber));
                            break;
                        case "POLICY":
                            objElem.SetAttribute("attachprompt", string.Concat("POLICY TRACKING ", sIdNumber));
                            break;
                        default:
                            objElem.SetAttribute("attachprompt", string.Concat(sAttachedTable, " ", sIdNumber));
                            break;
                    }
                    // npadhy End MITS 22028 Policies opening up in Maintenance when opened from Diaries 
                    //if (sAttachedTable.ToUpper() == "CLAIM")
                    //{
                    //    sClaimantName = GetClaimantsName(Conversion.ConvertStrToLong(sAttachedRecord));
                    //    objElem.SetAttribute("claimantname", sClaimantName);
                    //}
                    //if (Conversion.ConvertStrToLong(sAttachedRecord) > 0)
                    //    sDepartmentName = GetDeptName(0, Conversion.ConvertStrToLong(sAttachedRecord), ref sOrgLevel);
                    //else
                    //    sDepartmentName = "";
                    //objElem.SetAttribute("departmentname", sDepartmentName);
                    //objElem.SetAttribute("Create_Date", sCreateDate);
                    objElem.SetAttribute("tasksubject", sEntryName);
                    //Mobility changes Mits 35351

                    if (caller == "MobilityAdjuster")
                    {
                        objElem.SetAttribute("EntryNotes", sNotes);
                        objElem.SetAttribute("CompleteTime", sCompleteTime); //Mobility changes Mits  35447
                    }
                    //Mobility changes Mits 35351

                    //Yatharth: Mobile App needs the entry id in the diary search
                    objElem.SetAttribute("entry_id", sEntryId);
                    objDOM.FirstChild.AppendChild(objElem);
                }
                //Adaptor layer changes Sumeet
                //sReturnXmlValue=objDOM.InnerXml;


                return (objDOM);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.DiaryList.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objActivityReader != null)
                {
                    objActivityReader.Close();
                    objActivityReader.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objCache != null)
                    objCache.Dispose();
                objDOM = null;
                objElem = null;
                objData = null;
                objActivities = null;
                objActivity = null;
                sbSortBy = null;
            }
            //Adaptor layer changes Sumeet
            //return (sReturnXmlValue);

        }
		// <returns>Returns model object of diary list</returns>
        public DiaryList DiaryListObject(string p_sAssignedUser, string p_sFromDate, string p_sToDate, string p_sSortBy, string p_sTaskDesciption, string p_sClaimNumber, string p_sPageId, string p_sCurrentPageId, string createDate)//Mobility changes Mits 35351
        {
            string sSQL = string.Empty;
            string sSQLClaim = string.Empty;
            string sCompleteDate = string.Empty;
            string sPriority = string.Empty;
            string sActPriority = string.Empty;
            string strTodaysDate = string.Empty;
            string sAssignor = string.Empty;
            string sEntryName = string.Empty;
            string sAttachedRecord = string.Empty;
            string sAttachedTable = string.Empty;
            string sCreateDate = string.Empty;
            string sEntryId = string.Empty;
            string sTempActivity = string.Empty;
            string sIsAttached = string.Empty;
            string sIdNumber = string.Empty;
            string sClaimantName = string.Empty;
            string sDepartmentName = string.Empty;
            string sOrgLevel = string.Empty;
            string sClaimNumber = string.Empty;
            string sClaimId = string.Empty;
            string[] arrSortBy = null;
            string lastSync = string.Empty;
            int iInLoop = 0;
            Models.Diary diary = null;
            Models.DiaryList diaryList = null; 
            DataSet objData = null;
            DbReader objActivityReader = null;
            DbReader objReader = null;
            StringBuilder sbSortBy = null;
            DateTime datFrom;
            DateTime datTo;
            LocalCache objCache = null;
            string sNotes = string.Empty; 
            string sCompleteTime = string.Empty; 
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            string priority = string.Empty;
            try
            {
                if (!(string.IsNullOrEmpty(p_sClaimNumber)))
                {
                    sSQLClaim = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + p_sClaimNumber + "'";
                    using (DbReader objReaderClaim = DbFactory.GetDbReader(m_sConnectionString, sSQLClaim))
                    {
                        if (objReaderClaim.Read())
                        {
                            sClaimId = Conversion.ConvertObjToStr(objReaderClaim.GetValue("CLAIM_ID")).Trim();
                        }
                        else
                        {
                            sClaimId = "-1";
                        }
                    }
                }
                sbSortBy = new StringBuilder();
                datFrom = new DateTime();
                datTo = new DateTime();
                strTodaysDate = Conversion.GetDate(DateTime.Now.Date.ToString());
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                if (!(string.IsNullOrEmpty(sClaimId)))
                    sSQL = "SELECT * FROM WPA_DIARY_ENTRY WHERE DIARY_VOID=0 AND DIARY_DELETED=0 AND STATUS_OPEN <> 0 AND ASSIGNED_USER='" + p_sAssignedUser + "' AND ATTACH_TABLE = 'CLAIM' AND ATTACH_RECORDID ='" + sClaimId + "' ";
                else
                    sSQL = "SELECT * FROM WPA_DIARY_ENTRY WHERE DIARY_VOID=0 AND DIARY_DELETED=0 AND STATUS_OPEN <> 0 AND ASSIGNED_USER='" + p_sAssignedUser + "' ";
                if (p_sTaskDesciption != "")
                    sSQL += " AND ENTRY_NAME ='" + p_sTaskDesciption + "'";
                if (!string.IsNullOrEmpty(createDate))
                    sSQL += " AND CREATE_DATE>='" + createDate + "'";
                if (string.IsNullOrEmpty(p_sToDate) && !(string.IsNullOrEmpty(p_sFromDate)))
                    sSQL = sSQL + " AND COMPLETE_DATE >= '" + p_sFromDate + "'";
                else if (string.IsNullOrEmpty(p_sFromDate) && !(string.IsNullOrEmpty(p_sToDate)))
                    sSQL = sSQL + " AND COMPLETE_DATE < '" + p_sToDate + "'";
                else if (!(string.IsNullOrEmpty(p_sFromDate)) && !(string.IsNullOrEmpty(p_sToDate)))
                    sSQL = sSQL + " AND COMPLETE_DATE BETWEEN '" + Conversion.GetDate(p_sFromDate) + "' AND '" + Conversion.GetDate(p_sToDate) + "'";
                arrSortBy = p_sSortBy.Split(new char[] { ',' });
                if ((p_sFromDate != null))
                {
                    if (p_sFromDate != "")
                    {
                        datFrom = Conversion.ToDate(Conversion.GetDate(p_sFromDate));
                    }
                }
                if ((p_sToDate != null))
                {
                    if (p_sFromDate != "")
                    {
                        datTo = Conversion.ToDate(Conversion.GetDate(p_sToDate));
                    }
                }
                for (int i = 0; i < arrSortBy.Length; i++)
                {
                    switch (arrSortBy[i])
                    {
                        case "5":
                            sbSortBy.Append(" COMPLETE_DATE DESC ;");
                            break;
                        case "2":
                            sbSortBy.Append(" PRIORITY DESC ;");
                            break;
                        case "3":
                            sbSortBy.Append(" ATTACH_TABLE, ATTACH_RECORDID ;");
                            break;
                        case "4":
                            sbSortBy.Append(" ENTRY_NAME ;");
                            break;
                    }
                }
                if (!Conversion.ConvertObjToStr(sbSortBy).Trim().Equals(""))
                {
                    sSQL = sSQL + " ORDER BY ";
                }
                arrSortBy = Conversion.ConvertObjToStr(sbSortBy).Split(new char[] { ';' });
                for (int i = 0; i < arrSortBy.Length; i++)
                {
                    if (i < (arrSortBy.Length - 1))
                    {
                        sSQL = sSQL + arrSortBy[i];
                        if (!arrSortBy[i + 1].Equals(""))
                        {
                            sSQL = sSQL + ",";
                        }
                    }
                }
                objData = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
                if (objData.Tables.Count == 0)
                {
                    throw new RMAppException(Globalization.GetString("WPA.DiaryList.DatasetCreationError", m_iClientId));
                }
                diaryList = new Models.DiaryList();
                foreach (DataRow objDataRow in objData.Tables[0].Rows)
                {
                    diary = new Models.Diary();
                    sAssignor = Conversion.ConvertObjToStr(objDataRow["ASSIGNING_USER"]).Trim();
                    sEntryName = Conversion.ConvertObjToStr(objDataRow["ENTRY_NAME"]).Trim();
                    sCompleteDate = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDataRow["COMPLETE_DATE"]).Trim(), "d");
                    sCreateDate = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDataRow["CREATE_DATE"]).Trim(), "d");
                    sEntryId = Conversion.ConvertObjToStr(objDataRow["ENTRY_ID"]).Trim();
                    sPriority = Conversion.ConvertObjToStr(objDataRow["PRIORITY"]).Trim();
                    sIsAttached = Conversion.ConvertObjToStr(objDataRow["IS_ATTACHED"]).Trim();
                        sNotes = Conversion.ConvertObjToStr(objDataRow["ENTRY_NOTES"]).Trim(); //notes mini
                        sCompleteTime = Conversion.GetDBTimeFormat(Conversion.ConvertObjToStr(objDataRow["COMPLETE_TIME"]).Trim(), "t"); //Mobility changes Mits  35447
                    if (!sIsAttached.Equals("0"))
                    {
                        sAttachedTable = Conversion.ConvertObjToStr(objDataRow["ATTACH_TABLE"]).Trim();
                        sAttachedRecord = Conversion.ConvertObjToStr(objDataRow["ATTACH_RECORDID"]).Trim();
                        switch (sAttachedTable)
                        {
                            case CLAIM:
                                sSQL = "SELECT CLAIM_NUMBER, LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + sAttachedRecord;
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                while (objReader.Read())
                                {
                                    sIdNumber = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER")).Trim();
                                }
                                objReader.Close();
                                break;
                            case ENTITY:
                                sSQL = "SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID= " + sAttachedRecord;
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                while (objReader.Read())
                                {
                                    sIdNumber = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Trim() != "" ? Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")).Trim() + ", " + Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Trim() : Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")).Trim();
                                }
                                objReader.Close();
                                break;
                            case EVENT:
                                sSQL = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID = " + sAttachedRecord;
                                using (DbReader objReaderLocal = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                {
                                    while (objReaderLocal.Read())
                                    {
                                        sIdNumber = Conversion.ConvertObjToStr(objReaderLocal.GetValue("EVENT_NUMBER")).Trim();
                                    }
                                }
                                break;
                            case POLICY_ENH:
                            case "POLICY_ENH":
                                sSQL = "SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID = " + sAttachedRecord;
                                using (DbReader objReaderLocal = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                {
                                    while (objReaderLocal.Read())
                                    {
                                        sIdNumber = Conversion.ConvertObjToStr(objReaderLocal.GetValue("POLICY_NAME")).Trim();
                                    }
                                }
                                break;
                            case POLICY:
                                sSQL = "SELECT POLICY_NAME FROM POLICY WHERE POLICY_ID = " + sAttachedRecord;
                                using (DbReader objReaderLocal = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                {
                                    while (objReaderLocal.Read())
                                    {
                                        sIdNumber = Conversion.ConvertObjToStr(objReaderLocal.GetValue("POLICY_NAME")).Trim();
                                    }
                                }
                                break;
                            default:
                                sIdNumber = sAttachedRecord;
                                break;
                        }
                    }
                    else
                    {
                        sAttachedTable = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("NoAttachedTable", 0, p_sCurrentPageId, m_iClientId), this.m_iLangCode.ToString());
                        sIdNumber = "";
                    }
                    sActPriority = "";
                    if (!strDictParams.ContainsKey("PageId"))
                        strDictParams.Add("PageId", p_sPageId);
                    strDictParams.Remove("ResourceKey");
                    switch (sPriority)
                    {
                        case "1":
                            sSQL = string.Empty;
                            strDictParams.Add("ResourceKey", "Optional");
                            sSQL = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID={0}", m_iLangCode);
                            priority = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), sSQL.ToString(), strDictParams));
                            sActPriority = priority;
                            break;
                        case "2":
                            sSQL = string.Empty;
                            strDictParams.Add("ResourceKey", "Important");
                            sSQL = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID={0}", m_iLangCode);
                            priority = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), sSQL.ToString(), strDictParams));
                            sActPriority = priority;
                            break;
                        case "3":
                            sSQL = string.Empty;
                            strDictParams.Add("ResourceKey", "Required");
                            sSQL = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND RESOURCE_KEY=~ResourceKey~ AND LANGUAGE_ID={0}", m_iLangCode);
                            priority = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), sSQL.ToString(), strDictParams));
                            sActPriority = priority;
                            break;
                    }
                    diary.completeDate = sCompleteDate;
                    sSQL = "";
                    sSQL = "SELECT ACT_TEXT FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID = " + sEntryId;
                    if (objActivityReader != null)
                        objActivityReader.Close();
                    objActivityReader = null;
                    objActivityReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    iInLoop = 0;
                    if (objActivityReader.Read())
                    {
                        iInLoop = 1;
                        sTempActivity = Conversion.ConvertObjToStr(objActivityReader.GetValue("ACT_TEXT")).Trim();
                        diary.workActivity = sTempActivity;
                    }
                    if (iInLoop == 0)
                    {
                        sTempActivity = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("NoActivitySpecified", 0, p_sCurrentPageId, m_iClientId), this.m_iLangCode.ToString());
                        diary.workActivity = sTempActivity;
                    }
                    switch (sAttachedTable)
                    {
                        case "POLICYENH":
                        case "POLICY_ENH":
                            int iLob = GetSingleInt("POLICY_TYPE", "POLICY_ENH", "POLICY_ID = " + sAttachedRecord.ToString(), m_sConnectionString);
                            diary.attachPrompt = string.Concat("POLICY MANAGEMENT (", objCache.GetShortCode(iLob), ") ", sIdNumber);
                            break;
                        case "POLICY":
                            diary.attachPrompt = string.Concat("POLICY TRACKING ", sIdNumber);
                            break;
                        default:
                            diary.attachPrompt = string.Concat(sAttachedTable, " ", sIdNumber);
                            break;
                    }
                    diary.name = sEntryName;
                        diary.entryNotes = sNotes;
                        diary.completeTime = sCompleteTime;
                    diary.entryId = sEntryId;
                    diaryList.Diaries.Add(diary);
                }
                return diaryList;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.DiaryList.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objActivityReader != null)
                {
                    objActivityReader.Close();
                    objActivityReader.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objCache != null)
                    objCache.Dispose();
            }
        }        
        #endregion
        /// <summary>
        /// Prepares the query for fetching the codes based on the language code of the user who has logged in.
        /// </summary>
        /// Created by Amandeep Kaur 12/12/2012
        /// <returns>XmlDocument</returns>
//        private StringBuilder PrepareMultilingualQuery(string p_sSQL, string p_sTableName,int p_iLangCode)
//        {           
//            int iBaseLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);          
//            StringBuilder sReturn = new StringBuilder();
//            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
//            string sOrderByCmd = string.Empty;
//            int iIndex = p_sSQL.IndexOf("ORDER BY");
//            if (iIndex > 0)
//            {
//                sOrderByCmd = p_sSQL.Substring(iIndex);
//                p_sSQL = p_sSQL.Replace(sOrderByCmd, "");
//            }
//            sReturn.Append(p_sSQL.Replace("1033", p_iLangCode.ToString()) + " UNION " + p_sSQL.Replace("1033", sBaseLangCode) + @" AND CODES.CODE_ID NOT IN (SELECT CODES.CODE_ID FROM CODES INNER JOIN CODES_TEXT
//                                          ON CODES.CODE_ID = CODES_TEXT.CODE_ID WHERE CODES.TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + p_sTableName + "') AND CODES_TEXT.LANGUAGE_CODE = " + p_iLangCode.ToString() + ")");
//            if (iIndex > 0)
//            {
//                sReturn.Append(sOrderByCmd);
//            }
//            return sReturn;

//        }
        #region Get Activity Codes
        /// <summary>
        /// Fetches the activity codes from the Database.
        /// </summary>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetActivityCodes(UserLogin objUserLogin)
        {
            string sSQL = "";
            string sCodeId = "";
            string sShortCode = "";
            string sCodeDesc = "";
            int iCount = 0;
            DbReader objReader = null;
            XmlDocument objXmlDocument = null;
            XmlElement objXmlElement = null;
            XmlElement objParentElement = null;
            XmlElement objChildElement = null;
            int iLangCode = 0;
            string sTableName = string.Empty;
            try
            {
                //aman ml change
                iLangCode = objUserLogin.objUser.NlsCode;
                sTableName = "WPA_ACTIVITIES";
                if(iLangCode == 0 )
                    iLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]); 
                objXmlDocument = new XmlDocument();
                objParentElement = objXmlDocument.CreateElement("Diaries");
                objXmlDocument.AppendChild(objParentElement);

                objChildElement = objXmlDocument.CreateElement("ActivityCodes");
                objParentElement.AppendChild(objChildElement);
                //Asharma326 MITS 33772 Starts

                eDatabaseType edt = DbFactory.GetDatabaseType(m_sConnectionString);
                string DateforDB = "";
                if (edt == eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    DateforDB = "GETDATE()";
                }
                else { DateforDB = " TO_CHAR(sysdate,'YYYYMMDD')"; }
                
                //Asharma326 MITS 33772 Ends
                sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC";
                sSQL = sSQL + " FROM CODES, CODES_TEXT, GLOSSARY";
                sSQL = sSQL + " WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'WPA_ACTIVITIES'";
                sSQL = sSQL + " AND CODES.CODE_ID = CODES_TEXT.CODE_ID";
                sSQL = sSQL + " AND CODES.TABLE_ID = GLOSSARY.TABLE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033";
                sSQL = sSQL + " AND (CODES.DELETED_FLAG=0 OR CODES.DELETED_FLAG IS NULL)";
                sSQL = sSQL + " AND (GLOSSARY.DELETED_FLAG=0 OR GLOSSARY.DELETED_FLAG IS NULL)";

                sSQL = sSQL + "AND (NULLIF(CODES.EFF_END_DATE,'NULL') >= " + DateforDB + " or CODES.EFF_END_DATE IS NULL or CODES.EFF_END_DATE = 'NULL') ";//Added for mits 32874 asingh263 //nnithiyanand 34072 - Used NULLIF to check NULL strings in EFF_END_DATE  // MITS 34316 nnithiyanand (checked NULL string) ////Added for mits 35036 sharishkumar
                sSQL = sSQL + "AND (NULLIF(CODES.EFF_START_DATE,'NULL') <= " + DateforDB + " or CODES.EFF_START_DATE IS NULL or CODES.EFF_START_DATE = 'NULL') ";//Added for mits 35036 sharishkumar
                sSQL = sSQL + " ORDER BY CODES_TEXT.CODE_DESC";
                StringBuilder sbSQL = new StringBuilder();
                sbSQL = CommonFunctions.PrepareMultilingualQuery(sSQL.ToString(), sTableName, iLangCode);

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iCount = iCount + 1;
                        //Aman Used the indexes instead of the Column Names for the oracle order by query issue when there is union (ML Change)
                        sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0)).Trim();
                        sShortCode = Conversion.ConvertObjToStr(objReader.GetValue(1)).Trim();
                        sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(2)).Trim();

                        objXmlElement = objXmlDocument.CreateElement("Activity");

                        objXmlElement.SetAttribute("codeid", sCodeId);
                        objXmlElement.InnerText = sShortCode + ' ' + sCodeDesc;

                        objChildElement.AppendChild(objXmlElement);
                    }
                    objReader.Close();

                    objChildElement.SetAttribute("Count", iCount.ToString());

                }


            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetActivities.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objChildElement = null;
                objParentElement = null;

            }
            return (objXmlDocument);


        }
        #endregion

        # region Route Diary
        /// Added By Rahul Sharma 01/18/2006.
        /// <summary>
        /// Routes the Diary & corresponding activities in the Database.
        /// </summary>
        /// <param name="p_sInputString">Input string</param>
        /// <param name="p_sUserName">User name</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="p_sDsnName">DSN Name</param>
        /// <param name="p_bNotify">Notify the manager flag</param>
        /// <param name="p_sBase">Base i.e. http or https</param>
        /// <param name="p_sServer">Server name</param>
        /// <param name="p_sRoot">Root directory of the server</param>
        /// <returns>True if successful else false.</returns>
        public bool RouteDiary(ref XmlDocument p_objXMLInput, string p_sUserName, string p_sPassword, string p_sDsnName, bool p_bNotify, string p_sBase, string p_sRoot, string sLoginName, long p_lUserId)
        {
            int iCounter = 0;
            string sAssignedUsers = "";
            //Parijat: Mits 11850
            string taskNote = "";
            string[] sAssignedUsersArray = null;
            //pkandhari Jira 6412 starts
            int iGroupCounter = 0;
            string sAssignedGroup = "";
            string[] sAssignedGroupArray = null;
            //pkandhari Jira 6412 ends
            bool bRetVal = false;

            DataModelFactory objDMF = null;
            XmlElement objXmlElem = null;
            WpaDiaryEntry objWPADiary = null;
            WpaDiaryEntry objWPADiary_route = null;
            WpaDiaryAct objDiaryAct_route = null;
            WpaDiaryActList objDiaryActList_route = null;
            WpaDiaryActList objDiaryActList = null;
            XmlNodeList objDiaryNode = null;
            DbConnection objConn = null;
            RMConfigurator objConfig = null;
            string sSQL = "";//Mits 15176 Asif
            int iEntryId = 0;//Mits 15176:  Asif
            DbReader objReader = null;//Mits 15176:Asif
            //07/26/2011 SMISHRA54: WPA Email Notification
            bool emailNotifyFlag = false;
            string sEmail = string.Empty;
            Mailer objMail = null;
            //07/26/2011 SMISHRA54: End
            try
            {
                objDMF = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);
                //Get WPADiaryEntry instance from DataModel
                objDiaryActList = (WpaDiaryActList)objDMF.GetDataModelObject("WpaDiaryActList", false);
                objWPADiary = (WpaDiaryEntry)objDMF.GetDataModelObject("WpaDiaryEntry", false);
                objDiaryNode = p_objXMLInput.GetElementsByTagName("RouteDiary");
                if (objDiaryNode == null)
                    throw new XmlOperationException(Globalization.GetString("WPA.RouteDiary.InvalidXML", m_iClientId));

                for (int iCnt = 0; iCnt < objDiaryNode.Count; iCnt++)
                {
                    objXmlElem = (XmlElement)objDiaryNode[iCnt];
                    if (Conversion.ConvertStrToLong(objXmlElem.GetElementsByTagName("EntryId")[0].InnerXml) == 0)
                    { }
                    else
                    {
                        //Mits 15176 :Asif Start
                        iEntryId = Conversion.ConvertStrToInteger(objXmlElem.GetElementsByTagName("EntryId")[0].InnerXml);
                        //objWPADiary.MoveTo(Conversion.ConvertStrToInteger (objXmlElem.GetElementsByTagName("EntryId")[0].InnerXml));
                        objWPADiary.MoveTo(iEntryId);
                        //Mits 15176:Asif End	
                    }
                    //Getting No. of users to whom diary is to be routed.
                    //pkandhari Jira 6412 starts
                    if (objXmlElem.GetElementsByTagName("AssignedUser")[0] != null)
                    {
                        if (!(string.IsNullOrWhiteSpace(objXmlElem.GetElementsByTagName("AssignedUser")[0].InnerText.ToString())) && objXmlElem.GetElementsByTagName("AssignedUser")[0].InnerText.ToString() != "NA")
                        {
                            sAssignedUsers = objXmlElem.GetElementsByTagName("AssignedUser")[0].InnerText;
                            sAssignedUsersArray = sAssignedUsers.Split(new char[] { ',' });
                            iCounter = sAssignedUsersArray.GetLength(0);
                        }
                    }
                    //else
                    //{
                    //    iCounter = 1;
                    //}
                    if (objXmlElem.GetElementsByTagName("AssignedGroup")[0] != null)
                    {
                        if (!(string.IsNullOrWhiteSpace(objXmlElem.GetElementsByTagName("AssignedGroup")[0].InnerText.ToString())) && objXmlElem.GetElementsByTagName("AssignedGroup")[0].InnerText.ToString() != "NA")
                        {
                            sAssignedGroup = objXmlElem.GetElementsByTagName("AssignedGroup")[0].InnerText;
                            sAssignedGroupArray = sAssignedGroup.Split(new char[] { ',' });
                            iGroupCounter = sAssignedGroupArray.GetLength(0);
                        }
                        iCounter = iCounter + iGroupCounter;
                    }
                    //pkandhari Jira 6412 ends
                    //Parijat: Mits 11850
                    if (objXmlElem.GetElementsByTagName("TaskNotes")[0] != null)
                        taskNote = objXmlElem.GetElementsByTagName("TaskNotes")[0].InnerText;
                    else
                        taskNote = "";
                    //07/26/2011 SMISHRA54: WPA Email Notification
                    if ((objXmlElem.GetElementsByTagName("EmailNotifyFlag")[0] != null) && (objXmlElem.GetElementsByTagName("EmailNotifyFlag")[0].InnerXml != ""))
                    {
                        emailNotifyFlag = Convert.ToBoolean(objXmlElem.GetElementsByTagName("EmailNotifyFlag")[0].InnerXml);
                    }
                    //07/26/2011 SMISHRA54: End
                    //pkandhari Jira 6412 starts
                    objWPADiary_route = (WpaDiaryEntry)objDMF.GetDataModelObject("WpaDiaryEntry", false);
                    objDiaryActList_route = (WpaDiaryActList)objDMF.GetDataModelObject("WpaDiaryActList", false);

                    objWPADiary_route = objWPADiary;

                    //if (taskNote == "")
                        objWPADiary_route.EntryNotes = objWPADiary_route.EntryNotes + " --Route from " + sLoginName + " on " + System.DateTime.Now.Date.ToShortDateString();
                    //else
                        //objWPADiary_route.EntryNotes = objWPADiary_route.EntryNotes + taskNote;
                    //pkandhari Jira 6412 ends
                    for (int iCount = 0; iCount < iCounter; iCount++)
                    {
                        if (objXmlElem.GetElementsByTagName("AssignedUser")[0] != null)
                        {
                            //objWPADiary_route = (WpaDiaryEntry)objDMF.GetDataModelObject("WpaDiaryEntry", false);
                            //objDiaryActList_route = (WpaDiaryActList)objDMF.GetDataModelObject("WpaDiaryActList", false);

                            //objWPADiary_route = objWPADiary;                            
                            //pkandhari Jira 6412 starts
                            objWPADiary_route.AssignedUser = "";
                            objWPADiary_route.AssignedGroup = "";
                            if (iCount >= (iCounter - iGroupCounter))
                            {
                                if (objXmlElem.GetElementsByTagName("AssignedGroup")[0] != null)
                                {
                                    if (!(string.IsNullOrWhiteSpace(objXmlElem.GetElementsByTagName("AssignedGroup")[0].InnerText.ToString())) && objXmlElem.GetElementsByTagName("AssignedGroup")[0].InnerText.ToString() != "NA")
                                    {
                                        objWPADiary_route.AssignedGroup = sAssignedGroupArray[iCount - (iCounter - iGroupCounter)];
                                        m_sGroupName = Convert.ToString(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT GROUP_NAME FROM USER_GROUPS WHERE GROUP_ID = '" + sAssignedGroupArray[iCount - (iCounter - iGroupCounter)] + "'"));
                                        sAssignedUsers = sAssignedUsers + "," + m_sGroupName;
                                    }
                                }
                            }
                            else
                            {
                                if (objXmlElem.GetElementsByTagName("AssignedUser")[0] != null)
                                {
                                    if (!(string.IsNullOrWhiteSpace(objXmlElem.GetElementsByTagName("AssignedUser")[0].InnerText.ToString())) && objXmlElem.GetElementsByTagName("AssignedUser")[0].InnerText.ToString() != "NA")
                                    {
                                        objWPADiary_route.AssignedUser = sAssignedUsersArray[iCount];
                                    }
                                }
                            }
                            //pkandhari Jira 6412 ends
                            objWPADiary_route.AssigningUser = sLoginName;
                            //Parijat: Mits 11850
                            //if (taskNote == "")
                            //    objWPADiary_route.EntryNotes = objWPADiary_route.EntryNotes + " --Route from " + sLoginName + " on " + System.DateTime.Today.ToShortDateString();
                            //else
                            //    objWPADiary_route.EntryNotes = objWPADiary_route.EntryNotes + taskNote;
                            //in case of multiple issue we need to reset entry id
                            objWPADiary_route.EntryId = 0;
                            //Mits 15176 :Start Asif
                            //if (objWPADiary.WpaDiaryActList.Count > 0)
                            //{
                            //    foreach (WpaDiaryAct objAct in objWPADiary.WpaDiaryActList)
                            //    {
                            //        objWPADiary_route.WpaDiaryActList.AddNew();
                            //        objDiaryAct_route = objAct;
                            //        objDiaryAct_route.ActivityId = 0;
                            //        objWPADiary_route.WpaDiaryActList.Add(objDiaryAct_route);
                            //    }
                            //}
                            //pmittal5 - If the Assigned User does not have access permission to the attached Confidential Record, error message is thrown.
                            //if (IsConfRec(objWPADiary.AttachTable, objWPADiary.AttachRecordid, sAssignedUsersArray[iCount].ToString(), objUserLogin))
                            if (iCount < (iCounter - iGroupCounter)) //pkandhari Jira 6412
                            {
                                if (IsConfRec(objWPADiary.AttachTable, objWPADiary.AttachRecordid, sAssignedUsersArray[iCount].ToString(), p_lUserId))
                                {
                                    throw new RMAppException(string.Format(Globalization.GetString("WPA.Confidential.Error", m_iClientId), sAssignedUsersArray[iCount].ToString()));
                                }
                            }
                            //End
                            objWPADiary_route.Save();
                            if (iEntryId != 0)
                            {
                                sSQL = "SELECT * FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID=" + iEntryId;
                                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                //Add activities for the given diary
                                while (objReader.Read())
                                {
                                    objDiaryAct_route = (WpaDiaryAct)objDMF.GetDataModelObject("WpaDiaryAct", false);
                                    objDiaryAct_route.ParentEntryId = objWPADiary_route.EntryId;
                                    objDiaryAct_route.ActCode = Conversion.ConvertObjToInt(objReader.GetValue("ACT_CODE"), m_iClientId);
                                    objDiaryAct_route.ActText = Conversion.ConvertObjToStr(objReader.GetValue("ACT_TEXT"));
                                    objDiaryAct_route.Save();
                                }
                            }
                            //Mits 15176 :End Asif
                            //07/26/2011 SMISHRA54: WPA Email Notification
                            if ((emailNotifyFlag || p_bNotify)
                                && ((!string.IsNullOrEmpty(objWPADiary.AssigningUser)) && (!string.Equals(objWPADiary.AssigningUser.ToUpper(), "SYSTEM")))
                                && objWPADiary.StatusOpen)
                            {
                                sEmail = string.Empty;
                                if (!string.IsNullOrEmpty(objWPADiary.AssignedUser))
                                {
                                    sEmail = GetMailId(objWPADiary.AssignedUser);
                                    if (!string.IsNullOrEmpty(sEmail))
                                    {
                                        objMail = new Mailer(m_iClientId);
                                        objMail.To = sEmail;
                                        objMail.From = GetMailId(objWPADiary.AssigningUser);
                                        objMail.Subject = "A new task has been assigned";
                                        objMail.IsBodyHtml = true;
                                        objMail.Body = objWPADiary.GetEmailNotificationHtmlBody(null, Riskmaster.DataModel.DiaryAction.Route);
                                        objMail.SendMail();
                                    }
                                }
                                else if (!string.IsNullOrEmpty(objWPADiary.AssignedGroup))
                                {
                                    //sSQL = "SELECT USER_ID FROM USER_MEMBERSHIP WHERE GROUP_ID = " + objWPADiary.AssignedGroup;
                                    objReader = DbFactory.GetDbReader(m_sConnectionString, ("SELECT USER_ID FROM USER_MEMBERSHIP WHERE GROUP_ID = " + objWPADiary.AssignedGroup));
                                    
                                    while (objReader.Read())
                                    {
                                        sEmail = sEmail + "," + GetMailId(Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sSecConnectString, "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID = " + Convert.ToInt32(objReader.GetValue(0)))));
                                    }
                                    if (!string.IsNullOrEmpty(sEmail))
                                    {
                                        objMail = new Mailer(m_iClientId);
                                        objMail.To = sEmail.Trim(',');
                                        objMail.From = GetMailId(objWPADiary.AssigningUser);
                                        objMail.Subject = "A new task has been assigned";
                                        objMail.IsBodyHtml = true;
                                        objMail.Body = objWPADiary.GetEmailNotificationHtmlBody(null, Riskmaster.DataModel.DiaryAction.Route);
                                        objMail.SendMail();
                                    }
                                }
                            }
                            //07/26/2011 SMISHRA54: End
                        }
                    }//end for user.		
                    if (Conversion.ConvertStrToLong(objXmlElem.GetElementsByTagName("EntryId")[0].InnerXml) == 0)
                    { }
                    else
                    {
                        objWPADiary.MoveTo(Conversion.ConvertStrToInteger(objXmlElem.GetElementsByTagName("EntryId")[0].InnerXml));

                    }
                    sAssignedUsers = sAssignedUsers.Trim(','); //pkandhari Jira 6412
                    objWPADiary.EntryNotes = objWPADiary.EntryNotes + " --Routed To: " + sAssignedUsers + " on " + System.DateTime.Now.Date.ToShortDateString(); //pkandhari Jira 6412
                    if (objWPADiary.EntryNotes.Length > 2000)
                        objWPADiary.EntryNotes = objWPADiary.EntryNotes.Remove(objWPADiary.EntryNotes.Length - 3, 3) + "...";
                    objWPADiary.StatusOpen = false;
                    //Raman 02/22/2010: Routed diary completes without any completion info
                    objWPADiary.ResponseDate = DateTime.Now.ToString();
                    objWPADiary.CompletedByUser = m_sLoginName;
                    objWPADiary.Save();
                }//end for "RouteDiary" element tag.
                bRetVal = true; // csingh7 for MITS 14637
            }//end of try block.			
            catch (RMAppException p_objException)
            {
                bRetVal = false;
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.RouteDiary.RouteError", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                if (objDMF != null)
                {
                    //objDMF.UnInitialize();
                    objDMF.Dispose();
                    //objDMF = null;
                }
                objXmlElem = null;
                objWPADiary = null;
                objWPADiary_route = null;
                objDiaryAct_route = null;
                objDiaryActList_route = null;
                objDiaryActList = null;
                objDiaryNode = null;
                objReader = null;//Mits 15176 
            }
            return (bRetVal);
        }
        # endregion

        #region Save Diary Information
        /// <summary>
        /// Saves the Diary & corresponding activities in the Database.
        /// </summary>
        /// <param name="p_sInputString">Input string</param>
        /// <param name="p_sUserName">User name</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="p_sDsnName">DSN Name</param>
        /// <param name="p_bNotify">Notify the manager flag</param>
        /// <param name="p_sBase">Base i.e. http or https</param>
        /// <param name="p_sServer">Server name</param>
        /// <param name="p_sRoot">Root directory of the server</param>
        /// <returns>True if successful else false.</returns>
        /// <remarks>All Diary information is being passed as a string array
        /// with a specific special character delimiter.  For this particular function,
        /// the special delimiter being used is a pipe symbol/character.</remarks>
        //public bool SaveDiary(ref XmlDocument p_objXMLInput, string p_sUserName, string p_sPassword, string p_sDsnName, bool p_bNotify, string p_sBase, string p_sRoot, UserLogin objUserLogin)
        //R7 Prf Imp
        //public bool SaveDiary(ref XmlDocument p_objXMLInput, string p_sUserName, string p_sPassword, string p_sDsnName, bool p_bNotify, string p_sBase, string p_sRoot, long p_lUserId) //srajindersin MITS 26319 04/20/2012
        public bool SaveDiary(ref XmlDocument p_objXMLInput, string p_sUserName, string p_sPassword, string p_sDsnName, bool p_bNotify, string p_sBase, string p_sRoot, long p_lUserId, ref Hashtable objErrors) //srajindersin MITS 26319 04/20/2012
        {
            string sEmail = string.Empty;
            string sBase = string.Empty;
            string[] arrActivity = null;
            string sAssignedUsers = string.Empty;
            string[] sAssignedUsersArray = null;
            //pkandhari Jira 6412 starts
            string sAssignedGroup = string.Empty;
            string[] sAssignedGroupArray = null;
            int iGroupCounter = 0;
            //pkandhari Jira 6412 ends

            bool bRetVal = false;
            DataModelFactory objDMF = null;
            XmlElement objXmlElem = null;
            WpaDiaryEntry objWPADiary = null;
            Claim objClaim = null;
            WpaDiaryAct objDiaryAct = null;
            XmlNodeList objDiaryNode = null;
            DbConnection objConn = null;
            Mailer objMail = null;
            int iIssueDateText = 0;
            int iIssuePeriod = 0;
            long lDays = 0;
            int iCounter = 0;
            int iActivityCount = 0;
            string sClaimNumber = string.Empty;
            string sClaimantName = string.Empty;
            string sDepartmentName = string.Empty;
            string sOrgLevel = string.Empty;
            int iDiaryID = 0;
            string sUserName = string.Empty; //pmittal5 Confidential Record
            //07/26/2011 SMISHRA54: WPA Email Notification
            bool emailNotifyFlag = false;
            bool isNewDiary = false;
            //07/26/2011 SMISHRA54: End
            XmlNode objEntryID = null;//Mobile apps
            XmlElement objCaller = null;//Mobile apps
            DbReader objReader = null;//Mobile apps
            int iClaimID = 0;// Mobile apps
            //igupta3 Mits# 32846 
            XmlDocument objXMLInputDoc = null;
            bool isvalueset = false;
            XmlNodeList activitiesList = null;
            XmlNode loadDiaryNode = null;
            string sUsrNameWtMail = string.Empty;//Add by kuladeep for MITS:34958

            int iEventID = 0; //mbahl3 Mobility mits 35387
            string sEventNumber = string.Empty; //mbahl3 Mobility mits 35387
            Event objEvent = null; //mbahl3 Mobility mits 35387
            XmlDocument objSuppXMlDoc = new XmlDocument();//JIRA - 4691 - Diary Supplemental Changes.

            try
            {
                objDMF = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);
                objWPADiary = (WpaDiaryEntry)objDMF.GetDataModelObject("WpaDiaryEntry", false);
                objWPADiary.FiringScriptFlag = 2;
                objDiaryNode = p_objXMLInput.GetElementsByTagName("SaveDiary");

                if (objDiaryNode == null)
                    throw new XmlOperationException(Globalization.GetString("WPA.SaveDiary.InvalidXML", m_iClientId));

                for (int iCnt = 0; iCnt < objDiaryNode.Count; iCnt++)
                {
                    objXmlElem = (XmlElement)objDiaryNode[iCnt];
                    iDiaryID = Conversion.ConvertStrToInteger(objXmlElem.GetElementsByTagName("EntryId")[0].InnerXml);
                    if (iDiaryID != 0)
                    {
                        objWPADiary.MoveTo(iDiaryID);
                    }

                    if (objWPADiary.EntryId == 0)
                        objWPADiary.CreateDate = System.DateTime.Now.ToString();

                    //JIRA - 4691 - Diary Supplemental Changes.
                    XmlNode objSuppNode = p_objXMLInput.SelectSingleNode("//Supplementals");
                    if (objSuppNode != null && objSuppNode.InnerXml != "")
                    {
                        objSuppXMlDoc.LoadXml(objSuppNode.OuterXml);
                        objWPADiary.Supplementals.PopulateObject(objSuppXMlDoc);
                    }
                    objSuppNode = null;
                    //JIRA - 4691 - Diary Supplemental Changes.

                    objXMLInputDoc = LoadDiary(iDiaryID, p_sUserName, p_sPassword, objDMF.Context.RMUser);
                    //igupta3 Mits:32846 Changes starts
                    if (p_objXMLInput.SelectSingleNode("//CurrentDateSetting") != null && p_objXMLInput.SelectSingleNode("//CurrentDateSetting").InnerText == "TRUE")
                    {
                        if (objXmlElem.GetElementsByTagName("DueDate")[0] != null)
                        {
                            objXmlElem.GetElementsByTagName("DueDate")[0].InnerXml = objXMLInputDoc.SelectSingleNode("LoadDiary/CompleteDate").InnerText;
                        }
                        if ((objXmlElem.GetElementsByTagName("AutoConfirm")[0] != null) && (!string.IsNullOrEmpty(objXmlElem.GetElementsByTagName("AutoConfirm")[0].InnerXml)))
                        {
                            objXmlElem.GetElementsByTagName("AutoConfirm")[0].InnerXml = objXMLInputDoc.SelectSingleNode("LoadDiary/AutoConfirm").InnerText;
                        }
                        if (objXmlElem.GetElementsByTagName("ActivityString")[0] != null)
                        {
                            loadDiaryNode = objXMLInputDoc.SelectSingleNode("LoadDiary");
                            activitiesList = objXMLInputDoc.SelectNodes("LoadDiary/Activities/Activity");
                            if (activitiesList != null)
                            {
                                if (activitiesList.Count > 0)
                                {
                                    objXmlElem.GetElementsByTagName("ActivityString")[0].InnerText = loadDiaryNode.SelectSingleNode("Activities/sActs").InnerText;
                                }
                            }
                        }

                        if (objWPADiary.Context.InternalSettings.SysSettings.NotifyAssigner)
                        { p_bNotify = true; }
                        isvalueset = true;
                    }
                    //igupta3 Mits:32846 Changes ends


                    //Start: BRD#:5.1.18; MITS:29791; nsureshjain; 09/27/2012; 
                    if (objDiaryNode.Count > 1 && !isvalueset)
                    {


                        // Check if action is 'Complete' 
                        if (p_objXMLInput.SelectSingleNode("//StatusOpen").InnerText == "False")
                        {
                            //objXMLInputDoc = LoadDiary(iDiaryID, p_sUserName, p_sPassword, objDMF.Context.RMUser);
                            if (objXmlElem.GetElementsByTagName("DueDate")[0] != null)
                            {
                                objXmlElem.GetElementsByTagName("DueDate")[0].InnerXml = objXMLInputDoc.SelectSingleNode("LoadDiary/CompleteDate").InnerText;
                            }
                            if ((objXmlElem.GetElementsByTagName("AutoConfirm")[0] != null) && (!string.IsNullOrEmpty(objXmlElem.GetElementsByTagName("AutoConfirm")[0].InnerXml)))
                            {
                                objXmlElem.GetElementsByTagName("AutoConfirm")[0].InnerXml = objXMLInputDoc.SelectSingleNode("LoadDiary/AutoConfirm").InnerText;
                            }
                            if (!string.IsNullOrEmpty(objXmlElem.GetElementsByTagName("ActivityString")[0].InnerText))
                            {
                                loadDiaryNode = objXMLInputDoc.SelectSingleNode("LoadDiary");
                                activitiesList = objXMLInputDoc.SelectNodes("LoadDiary/Activities/Activity");
                                if (activitiesList != null)
                                {
                                    if (activitiesList.Count > 0)
                                    {
                                        objXmlElem.GetElementsByTagName("ActivityString")[0].InnerText = loadDiaryNode.SelectSingleNode("Activities/sActs").InnerText;
                                    }
                                }
                            }
                        }
                    }
                    //End: BRD#:5.1.18; MITS:29791; nsureshjain; 09/27/2012; 	
                    //07/26/2011 SMISHRA54: WPA Email Notification
                    if (objWPADiary.IsNew) isNewDiary = true;
                    //07/26/2011 SMISHRA54: End

                    //There could be multiple users for which diary has to be created which is handled later.. commented out by raman bhatia
                    if (objXmlElem.GetElementsByTagName("TaskSubject")[0] != null) objWPADiary.EntryName = objXmlElem.GetElementsByTagName("TaskSubject")[0].InnerText;//Shruti for 11098
                    if (objXmlElem.GetElementsByTagName("DueDate")[0] != null) objWPADiary.CompleteDate = Conversion.GetDate(objXmlElem.GetElementsByTagName("DueDate")[0].InnerXml);
                    if (objXmlElem.GetElementsByTagName("CompleteTime")[0] != null) objWPADiary.CompleteTime = objXmlElem.GetElementsByTagName("CompleteTime")[0].InnerXml;
                    if (objXmlElem.GetElementsByTagName("EstimateTime")[0] != null) objWPADiary.EstimateTime = Conversion.ConvertStrToDouble(objXmlElem.GetElementsByTagName("EstimateTime")[0].InnerXml);
                    if (objXmlElem.GetElementsByTagName("Priority")[0] != null) objWPADiary.Priority = Conversion.ConvertStrToInteger(objXmlElem.GetElementsByTagName("Priority")[0].InnerXml);
                    //rsushilaggar MITS 31377 Date 02/16/2013
                    if (objXmlElem.GetElementsByTagName("CreateDate")[0] != null) objWPADiary.CreateDate = Conversion.GetDate(objXmlElem.GetElementsByTagName("CreateDate")[0].InnerXml);
                    if (objXmlElem.GetElementsByTagName("TaskNotes")[0] != null)
                    {
                        //Previous Notes should not be deleted..modified by raman bhatia
                        if (objXmlElem.GetElementsByTagName("Action")[0] != null)
                        {
                            if (objXmlElem.GetElementsByTagName("Action")[0].InnerXml == "Route")
                                objWPADiary.EntryNotes = objWPADiary.EntryNotes + objXmlElem.GetElementsByTagName("TaskNotes")[0].InnerText; //Shruti for 11098
                            else if (objXmlElem.GetElementsByTagName("Action")[0].InnerXml != "Roll")
                                objWPADiary.EntryNotes = objXmlElem.GetElementsByTagName("TaskNotes")[0].InnerText; //Shruti for 11098
                        }
                        else
                            objWPADiary.EntryNotes = objWPADiary.EntryNotes + objXmlElem.GetElementsByTagName("TaskNotes")[0].InnerText; //Shruti for 11098
                    }

                    //Notify Flag may not come from the UI.. changed by raman bhatia
                    if ((objXmlElem.GetElementsByTagName("NotifyFlag")[0] != null) && (objXmlElem.GetElementsByTagName("NotifyFlag")[0].InnerXml != ""))
                    {
                        objWPADiary.NotifyFlag = Convert.ToBoolean(objXmlElem.GetElementsByTagName("NotifyFlag")[0].InnerXml);
                    }

                    if ((objXmlElem.GetElementsByTagName("AutoConfirm")[0] != null) && (objXmlElem.GetElementsByTagName("AutoConfirm")[0].InnerXml != ""))
                    {
                        objWPADiary.AutoConfirm = Convert.ToBoolean(objXmlElem.GetElementsByTagName("AutoConfirm")[0].InnerXml);
                    }
                    //07/26/2011 SMISHRA54: WPA Email Notification
                    //Added by amitosh for Auto  diart notification for R8 
                    if ((objXmlElem.GetElementsByTagName("OverDueDiaryNotify")[0] != null) && (objXmlElem.GetElementsByTagName("OverDueDiaryNotify")[0].InnerXml != ""))
                    {
                        objWPADiary.AutoLateNotify = Convert.ToBoolean(objXmlElem.GetElementsByTagName("OverDueDiaryNotify")[0].InnerXml);
                    }
                    else
                    {
                        objWPADiary.AutoLateNotify = false;
                    }

                    //igupta3 non rollable/routable permission jira 439
                    if ((objXmlElem.GetElementsByTagName("NonRoutableFlag")[0] != null) && (objXmlElem.GetElementsByTagName("NonRoutableFlag")[0].InnerXml != ""))
                    {
                        objWPADiary.NonRouteFlag = Convert.ToBoolean(objXmlElem.GetElementsByTagName("NonRoutableFlag")[0].InnerXml);
                    }
                    else
                    {
                        objWPADiary.NonRouteFlag = false;
                    }
                    if ((objXmlElem.GetElementsByTagName("NonRollableFlag")[0] != null) && (objXmlElem.GetElementsByTagName("NonRollableFlag")[0].InnerXml != ""))
                    {
                        objWPADiary.NonRollFlag = Convert.ToBoolean(objXmlElem.GetElementsByTagName("NonRollableFlag")[0].InnerXml);
                    }
                    else
                    {
                        objWPADiary.NonRollFlag = false;
                    }
                    //igupta3 ends

                    if ((objXmlElem.GetElementsByTagName("EmailNotifyFlag")[0] != null) && (objXmlElem.GetElementsByTagName("EmailNotifyFlag")[0].InnerXml != ""))
                    {
                        emailNotifyFlag = Convert.ToBoolean(objXmlElem.GetElementsByTagName("EmailNotifyFlag")[0].InnerXml);
                    }
                    //07/26/2011 SMISHRA54: End
                    if ((objXmlElem.GetElementsByTagName("StatusOpen")[0] != null) && (objXmlElem.GetElementsByTagName("StatusOpen")[0].InnerXml != ""))
                    {
                        objWPADiary.StatusOpen = Conversion.ConvertStrToBool(objXmlElem.GetElementsByTagName("StatusOpen")[0].InnerXml.Trim());
                        if (!Conversion.ConvertStrToBool(objXmlElem.GetElementsByTagName("StatusOpen")[0].InnerXml.Trim()))
                        {
                            objWPADiary.CompletedByUser = p_sUserName;
                        }
                    }

                    if (objXmlElem.GetElementsByTagName("AssigningUser")[0] != null)
                    {
                        if (objXmlElem.GetElementsByTagName("AssigningUser")[0].InnerXml == "currentuser" && !(objWPADiary.EntryId > 0)) objWPADiary.AssigningUser = p_sUserName;
                        else if (!(objWPADiary.EntryId > 0)) objWPADiary.AssigningUser = objXmlElem.GetElementsByTagName("AssigningUser")[0].InnerXml;
                    }

                    //the following code should not be commented.. was originally commented.. raman bhatia
                    if ((objXmlElem.GetElementsByTagName("TeTracked")[0] != null) && (objXmlElem.GetElementsByTagName("TeTracked")[0].InnerXml != ""))
                        objWPADiary.TeTracked = Convert.ToBoolean(objXmlElem.GetElementsByTagName("TeTracked")[0].InnerXml);
                    if (objXmlElem.GetElementsByTagName("TeTotalHours")[0] != null) objWPADiary.TeTotalHours = Conversion.ConvertStrToDouble(objXmlElem.GetElementsByTagName("TeTotalHours")[0].InnerXml);
                    if (objXmlElem.GetElementsByTagName("TeExpenses")[0] != null) objWPADiary.TeExpenses = Conversion.ConvertStrToDouble(objXmlElem.GetElementsByTagName("TeExpenses")[0].InnerXml);
                    if (objXmlElem.GetElementsByTagName("TeStartTime")[0] != null) objWPADiary.TeStartTime = objXmlElem.GetElementsByTagName("TeStartTime")[0].InnerXml;
                    if (objXmlElem.GetElementsByTagName("TeEndTime")[0] != null) objWPADiary.TeEndTime = objXmlElem.GetElementsByTagName("TeEndTime")[0].InnerXml;
                    if (objXmlElem.GetElementsByTagName("ResponseDate")[0] != null) objWPADiary.ResponseDate = Conversion.GetDate(objXmlElem.GetElementsByTagName("ResponseDate")[0].InnerXml);
                    if (objXmlElem.GetElementsByTagName("Response")[0] != null) objWPADiary.Response = objXmlElem.GetElementsByTagName("Response")[0].InnerXml;

                    //added code for attaching record to a diary
                    if (objXmlElem.GetElementsByTagName("AttachTable")[0] != null) objWPADiary.AttachTable = objXmlElem.GetElementsByTagName("AttachTable")[0].InnerXml.ToUpper();
                    if (objXmlElem.GetElementsByTagName("AttachRecordId")[0] != null)
                    {
                        //  Ishan :Mobile Adjuster- attach claim to a diary with a claim number
                        objCaller = (XmlElement)p_objXMLInput.SelectSingleNode("//caller");
                        if (objCaller == null)
                        {
                            objWPADiary.AttachRecordid = Conversion.ConvertStrToInteger(objXmlElem.GetElementsByTagName("AttachRecordId")[0].InnerXml);
                            objWPADiary.IsAttached = true;
                        }
                      
                        else if (objCaller != null && (objCaller.InnerText == "MobileAdjuster"))
                        {
                            sClaimNumber = Conversion.ConvertObjToStr(objXmlElem.GetElementsByTagName("AttachRecordId")[0].InnerXml);
                            string sSQLM = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER='" + sClaimNumber.Trim() + "'";

                            //objReader = DbFactory.ExecuteReader(this.m_sConnectionString, sSQLM);
                            using (DbReader objReader1 = DbFactory.GetDbReader(this.m_sConnectionString, sSQLM))
                            {
                                if (objReader1.Read())
                                {
                                    iClaimID = Conversion.ConvertObjToInt(objReader1.GetValue("CLAIM_ID"), m_iClientId);
                                }
                            }
                            objWPADiary.AttachRecordid = iClaimID;
                            objWPADiary.IsAttached = true; //akaur9 Mobile Adjuster
                        }
                        //mbahl3 Mobility mits 35387
                        else if (objCaller != null && (objCaller.InnerText == "MobilityAdjuster"))
                        {
                            if (objXmlElem.GetElementsByTagName("AttachTable")[0].InnerXml == "CLAIM")
                            {
                                sClaimNumber = Conversion.ConvertObjToStr(objXmlElem.GetElementsByTagName("AttachRecordId")[0].InnerXml);
                                string sSQLM = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER='" + sClaimNumber.Trim() + "'";


                                using (DbReader objReader1 = DbFactory.GetDbReader(this.m_sConnectionString, sSQLM))
                                {
                                    if (objReader1.Read())
                                    {
                                        iClaimID = Conversion.ConvertObjToInt(objReader1.GetValue("CLAIM_ID"), m_iClientId);
                                    }
                                }
                                objWPADiary.AttachRecordid = iClaimID;
                            }
                            if (objXmlElem.GetElementsByTagName("AttachTable")[0].InnerXml == "EVENT")
                            {
                                sEventNumber = Conversion.ConvertObjToStr(objXmlElem.GetElementsByTagName("AttachRecordId")[0].InnerXml);
                                string sSQLM = "SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER='" + sEventNumber.Trim() + "'";
                                using (DbReader objReader1 = DbFactory.GetDbReader(this.m_sConnectionString, sSQLM))
                                {
                                    if (objReader1.Read())
                                    {
                                        iEventID = Conversion.ConvertObjToInt(objReader1.GetValue("EVENT_ID"), m_iClientId);
                                    }
                                }
                                objWPADiary.AttachRecordid = iEventID;
                            }
                            objWPADiary.IsAttached = true;


                        }
                        //mbahl3 Mobility mits 35387

                        if (objWPADiary.AttachTable.ToUpper() == "CLAIM")
                        {
                            objClaim = (Claim)objDMF.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(objWPADiary.AttachRecordid);
                            sClaimNumber = objClaim.ClaimNumber;
                            sClaimantName = GetClaimantsName(objWPADiary.AttachRecordid);
                            sDepartmentName = GetDeptName(0, objWPADiary.AttachRecordid, ref sOrgLevel);
                            objWPADiary.Regarding = String.Format("{0} * {1} * {2}", sClaimNumber, sClaimantName, sDepartmentName);
                        }
                        //mbahl3 Mobility mits 35387
                        else if (objWPADiary.AttachTable.ToUpper() == "EVENT")
                        {
                            objEvent = (Event)objDMF.GetDataModelObject("Event", false);
                            objEvent.MoveTo(objWPADiary.AttachRecordid);
                            sEventNumber = objEvent.EventNumber;
                            // rrachev JIRA RMA-4071 Begin                                                        
                            // sClaimantName = GetClaimantsName(objWPADiary.AttachRecordid);
                            // sDepartmentName = GetDeptName(0, objWPADiary.AttachRecordid, ref sOrgLevel);
                            // objWPADiary.Regarding = String.Format("{0} * {1} * {2}", sEventNumber, sClaimantName, sDepartmentName);
                            sDepartmentName = GetDeptName(objWPADiary.AttachRecordid, 0, ref sOrgLevel);
                            objWPADiary.Regarding = String.Format("{0} * {1}", sEventNumber, sDepartmentName);
                            // rrachev JIRA RMA-4071 End
                        }
                        //mbahl3 Mobility mits 35387
                    }
                    //Mits 33843 - Diary enhancement for adding parent record
                    // akaushik5 Uncommented for MITS 37022 Starts
                    objWPADiary.AttParentCode = GetParentRecord(objWPADiary.AttachTable.ToUpper(), objWPADiary.AttachRecordid);
                    // akaushik5 Uncommented for MITS 37022 Ends
                    // End of Mits 33843



                    if (objXmlElem.GetElementsByTagName("IssueDateText")[0] != null)
                    {
                        iIssueDateText = Conversion.ConvertStrToInteger(objXmlElem.GetElementsByTagName("IssueDateText")[0].InnerXml);
                        iIssuePeriod = Conversion.ConvertStrToInteger(objXmlElem.GetElementsByTagName("IssueCompletion")[0].InnerXml);
                    }

                    //setting deleted flag and void flag as zero..raman bhatia
                    objWPADiary.DiaryVoid = false;
                    objWPADiary.DiaryDeleted = false;
                    //abisht MITS 11622
                    //When a new diary entry gets entered into WPA_DIARY_ENTRY a value for ASSIGNED_GROUP 
                    //column should be NA (Not available) instead of null.
                    //Null values cause a diary lag on Oracle db with a large numbers of records.
                    //objWPADiary.AssignedGroup = "NA";
                    //Handling case of multiple users diary creation...raman bhatia
                    //pkandhari Jira 6412 starts
                    if (p_objXMLInput.SelectSingleNode("//Action") != null)
                    {
                        if (p_objXMLInput.SelectSingleNode("//Action").InnerText.ToString() == "Roll" && objXmlElem.GetElementsByTagName("AssignedGroup")[0] != null)
                        {
                            objXmlElem.GetElementsByTagName("AssignedGroup")[0].InnerText = Convert.ToString(DbFactory.ExecuteScalar(m_sConnectionString,
                                ("SELECT GROUP_ID FROM USER_GROUPS WHERE GROUP_NAME = '" + objXmlElem.GetElementsByTagName("AssignedGroup")[0].InnerText + "'").ToString()));
                        }
                    }
                    if (objXmlElem.GetElementsByTagName("AssignedUser")[0] != null)
                    {
                        if (!(string.IsNullOrWhiteSpace(objXmlElem.GetElementsByTagName("AssignedUser")[0].InnerText.ToString())) && objXmlElem.GetElementsByTagName("AssignedUser")[0].InnerText.ToString() != "NA")
                        {
                            sAssignedUsers = objXmlElem.GetElementsByTagName("AssignedUser")[0].InnerText;
                            sAssignedUsersArray = sAssignedUsers.Split(new char[] { ',' });
                            iCounter = sAssignedUsersArray.GetLength(0);
                        }
                    }
                    else
                    {
                        iCounter = 1;
                    }
                    if (objXmlElem.GetElementsByTagName("AssignedGroup")[0] != null)
                    {
                        if (!(string.IsNullOrWhiteSpace(objXmlElem.GetElementsByTagName("AssignedGroup")[0].InnerText.ToString())) && objXmlElem.GetElementsByTagName("AssignedGroup")[0].InnerText.ToString() != "NA")
                        {
                            sAssignedGroup = objXmlElem.GetElementsByTagName("AssignedGroup")[0].InnerText;
                            sAssignedGroupArray = sAssignedGroup.Split(new char[] { ',' });
                            iGroupCounter = sAssignedGroupArray.GetLength(0);
                        }

                        iCounter = iCounter + iGroupCounter;
                    }
                    //pkandhari Jira 6412 ends

                    //tkr 7/6/06 do this once instead of 4 times, and preserve so can use in email message body
                    if (objXmlElem.GetElementsByTagName("ActivityString")[0].InnerText != "")
                    {
                        arrActivity = objXmlElem.GetElementsByTagName("ActivityString")[0].InnerText.Split(new char[] { '|' });
                    }
                    for (int iCount = 0; iCount < iCounter - iGroupCounter; iCount++) //pkandhari Jira 6412
                    {
                        if (objXmlElem.GetElementsByTagName("AssignedUser")[0] != null)
                        {
                            if (!(string.IsNullOrWhiteSpace(objXmlElem.GetElementsByTagName("AssignedUser")[0].InnerText.ToString())) && objXmlElem.GetElementsByTagName("AssignedUser")[0].InnerText.ToString() != "NA") //pkandhari Jira 6412
                            {
                                objWPADiary.AssignedUser = sAssignedUsersArray[iCount];
                                //in case of multiple issue we need to reset entry id
                                //pmittal5 - If any of the Assigned Users does not have access permission to the attached Confidential Record, error message is thrown.
                                //if (IsConfRec(objWPADiary.AttachTable, objWPADiary.AttachRecordid, objWPADiary.AssignedUser, objUserLogin))
                                //r7 prf imp
                                if (IsConfRec(objWPADiary.AttachTable, objWPADiary.AttachRecordid, objWPADiary.AssignedUser, p_lUserId))
                                {
                                    sUserName = objWPADiary.AssignedUser + "," + sUserName;
                                }
                            }
                        }
                    }
                    sUserName = sUserName.TrimEnd(',').Trim();
                    if (sUserName.Length > 0)
                    {
                        throw new RMAppException(string.Format(Globalization.GetString("WPA.Confidential.Error", m_iClientId), sUserName));
                    }
                    //End - pmittal5

                    for (int iCount = 0; iCount < iCounter; iCount++)
                    {
                        //pkandhari Jira 6412 starts
                        if (iCount >= (iCounter - iGroupCounter))
                        {
                            if (objXmlElem.GetElementsByTagName("AssignedGroup")[0] != null)
                            {
                                if (!(string.IsNullOrWhiteSpace(objXmlElem.GetElementsByTagName("AssignedGroup")[0].InnerText.ToString())) && objXmlElem.GetElementsByTagName("AssignedGroup")[0].InnerText.ToString() != "NA")
                                {
                                    objWPADiary.AssignedUser = "";
                                    objWPADiary.AssignedGroup = sAssignedGroupArray[iCount - (iCounter - iGroupCounter)];
                                    if (iGroupCounter > 1)
                                    {
                                        objWPADiary.EntryId = 0;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (objXmlElem.GetElementsByTagName("AssignedUser")[0] != null)
                            {
                                if (!(string.IsNullOrWhiteSpace(objXmlElem.GetElementsByTagName("AssignedUser")[0].InnerText.ToString())) && objXmlElem.GetElementsByTagName("AssignedUser")[0].InnerText.ToString() != "NA")
                                {
                                    objWPADiary.AssignedGroup = "";
                                    objWPADiary.AssignedUser = sAssignedUsersArray[iCount];
                                    if (iCounter > 1)
                                    {
                                        objWPADiary.EntryId = 0;
                                    }
                                }
                            }
                        }
                        //pkandhari Jira 6412 ends
                        if (objXmlElem.GetElementsByTagName("IssueDateText")[0] != null)
                        {
                            if ((iIssuePeriod == 2) && (iIssuePeriod != 0) && Conversion.IsNumeric(iIssuePeriod))
                            {
                                //Issue every n day till due date
                                string sDate = objXmlElem.GetElementsByTagName("DueDate")[0].InnerXml;
                               //mbahl3 mits 31486
							    //DateTime dttm = System.DateTime.Parse(sDate);
                                DateTime dttm = System.DateTime.Parse(Conversion.GetDBDateFormat(sDate, "MM/dd/yyyy"));
                               //mbahl3  mits 31486
								DateTime dtNow = DateTime.Now;
                                TimeSpan ts = dttm - dtNow;
                                lDays = ts.Days;
                                if (lDays > 0 && iIssueDateText > 0)
                                {
                                    //Interval of issuing diary is coming in different variable...raman bhatia
                                    //for (double i = 1; i <= lDays; i=i+iIssuePeriod)
                                    for (double i = 1; i <= lDays; i = i + iIssueDateText)
                                    {
                                        objWPADiary.CompleteDate = dtNow.AddDays(i).ToString();
                                        objWPADiary.EntryId = 0;
                                        objWPADiary.Save();

                                        //Activity data was not getting updated in multiple issue case..raman bhatia
                                        if (arrActivity != null)
                                        {
                                            objConn = DbFactory.GetDbConnection(m_sConnectionString);

                                            //Delete activities related to a Diary prior to saving.
                                            objConn.Open();
                                            objConn.ExecuteNonQuery("DELETE FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID=" + objWPADiary.EntryId);
                                            objConn.Close();

                                            for (iActivityCount = 0; iActivityCount < arrActivity.GetLength(0); iActivityCount += 2)
                                            {
                                                objDiaryAct = (WpaDiaryAct)objDMF.GetDataModelObject("WpaDiaryAct", false);
                                                objDiaryAct.ParentEntryId = objWPADiary.EntryId;
                                                objDiaryAct.ActCode = Conversion.ConvertStrToInteger(arrActivity[iActivityCount]);
                                                objDiaryAct.ActText = arrActivity[iActivityCount + 1].Replace("&#124;", "|");
                                                objDiaryAct.Save();
                                            }
                                        }
                                    }
                                }
                                //creating diary on due date
                                objWPADiary.CompleteDate = Conversion.GetDate(objXmlElem.GetElementsByTagName("DueDate")[0].InnerXml);
                                objWPADiary.EntryId = 0;
                                objWPADiary.Save();

                                if (arrActivity != null)
                                {
                                    objConn = DbFactory.GetDbConnection(m_sConnectionString);

                                    //Delete activities related to a Diary prior to saving.
                                    objConn.Open();
                                    objConn.ExecuteNonQuery("DELETE FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID=" + objWPADiary.EntryId);
                                    objConn.Close();

                                    for (iActivityCount = 0; iActivityCount < arrActivity.GetLength(0); iActivityCount += 2)
                                    {
                                        objDiaryAct = (WpaDiaryAct)objDMF.GetDataModelObject("WpaDiaryAct", false);
                                        objDiaryAct.ParentEntryId = objWPADiary.EntryId;
                                        objDiaryAct.ActCode = Conversion.ConvertStrToInteger(arrActivity[iActivityCount]); //Activity Code
                                        objDiaryAct.ActText = arrActivity[iActivityCount + 1].Replace("&#124;", "|"); //Activity Text
                                        objDiaryAct.Save();
                                    }
                                }
                            }
                        }
                        else if (objWPADiary.EntryId > 0)  //case of edit diary..just update activities
                        {
                            //saving diary first
                            //Autoclose the diary in case of Done button pressed in Mobile Adjuster Screen : yatharth
                            //if (p_objXMLInput.SelectSingleNode("//SaveDiary//MobAdjuster") != null && p_objXMLInput.SelectSingleNode("//SaveDiary//MobAdjuster").InnerText == "true"
                            //      && p_objXMLInput.SelectSingleNode("//SaveDiary//DiaryAction") != null && p_objXMLInput.SelectSingleNode("//SaveDiary//DiaryAction").InnerText == "Complete")
                            //akaur9 R8 Mobile Adjuster
                            objCaller = (XmlElement)p_objXMLInput.SelectSingleNode("//MobAdjuster");
                            if (objCaller != null)
                            {
                                if (objCaller.InnerText.Equals("true"))
                                {
                                    // Ijha: changed for diary closed not able to disappear from the diary list
                                    //  if (p_objXMLInput.SelectSingleNode("MobAdjuster") != null && p_objXMLInput.SelectSingleNode("MobAdjuster").Value.ToString() == "true"
                                    //    && p_objXMLInput.SelectSingleNode("DiaryAction") != null && p_objXMLInput.SelectSingleNode("DiaryAction").Value.ToString() == "Complete")
                                    objWPADiary.StatusOpen = false;
                                    objWPADiary.CompletedByUser = p_sUserName;
                                    objWPADiary.CompleteDate = Conversion.ToDbDate(System.DateTime.Now);
                                    objWPADiary.CompleteTime = Conversion.ToDbTime(System.DateTime.Now);
                                }
                            }
                            objWPADiary.Save();

                            // MITS 9392 - In case arrActivity is null then also delete activities related to a Diary

                            objConn = DbFactory.GetDbConnection(m_sConnectionString);

                            //Delete activities related to a Diary prior to saving.
                            objConn.Open();
                            objConn.ExecuteNonQuery("DELETE FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID=" + objWPADiary.EntryId);
                            objConn.Close();

                            if (arrActivity != null)
                            {
                                for (iActivityCount = 0; iActivityCount < arrActivity.GetLength(0); iActivityCount += 2)
                                {
                                    objDiaryAct = (WpaDiaryAct)objDMF.GetDataModelObject("WpaDiaryAct", false);
                                    objDiaryAct.ParentEntryId = objWPADiary.EntryId;
                                    objDiaryAct.ActCode = Conversion.ConvertStrToInteger(arrActivity[iActivityCount]);
                                    objDiaryAct.ActText = arrActivity[iActivityCount + 1].Replace("&#124;", "|");
                                    objDiaryAct.Save();
                                }
                            }
                        }
                        else
                        {
                            //case of single issue on on due date
                            objWPADiary.EntryId = 0;

                            objWPADiary.Save();

                            //  Ishan :Mobile Adjuster- Add EntryId in the response
                            objCaller = (XmlElement)p_objXMLInput.SelectSingleNode("//caller");
                            if (objCaller != null)
                            {
                                if (objCaller.InnerText.Equals("MobileAdjuster") || objCaller.InnerText.Equals("MobilityAdjuster"))
                                {
                                    objEntryID = p_objXMLInput.SelectSingleNode("/SaveDiary//EntryId");
                                    if (objEntryID != null)
                                    {
                                        objEntryID.InnerText = objWPADiary.EntryId + "";
                                    }
                                }
                                //mbahl3 cloud changes for mobile
                                if (objCaller.InnerText.Equals("MobilityAdjuster"))
                                {
                                    XmlNode objRoot = p_objXMLInput.SelectSingleNode("/SaveDiary");
                                    XmlElement objTempElement = p_objXMLInput.CreateElement("TimeCreated");
                                    objTempElement.InnerText = Conversion.GetDBTimeFormat(objWPADiary.CreateDate.Substring(8), "t"); //Conversion.GetDateTime(objWPADiary.CreateDate);
                                    objRoot.AppendChild(objTempElement);
                                    objTempElement = null;
                                    objRoot = null;
                                }
                                //mbahl3 cloud changesfor mobile
                                  //  objTimecreated
                            }
                            if (arrActivity != null)
                            {
                                objConn = DbFactory.GetDbConnection(m_sConnectionString);

                                //Delete activities related to a Diary prior to saving.
                                objConn.Open();
                                objConn.ExecuteNonQuery("DELETE FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID=" + objWPADiary.EntryId);
                                objConn.Close();
                                for (iActivityCount = 0; iActivityCount < arrActivity.Length; iActivityCount += 2)
                                {
                                    objDiaryAct = (WpaDiaryAct)objDMF.GetDataModelObject("WpaDiaryAct", false);
                                    objDiaryAct.ParentEntryId = objWPADiary.EntryId;
                                    objDiaryAct.ActCode = Conversion.ConvertStrToInteger(arrActivity[iActivityCount]);
                                    if (arrActivity[iActivityCount + 1] != null)//rkaur27 RMA-6330
                                        objDiaryAct.ActText = arrActivity[iActivityCount + 1].Replace("&#124;", "|"); 
                                    objDiaryAct.Save();
                                }
                            }
                        }
                        //07/06/2011 SMISHRA54: WPA Email Notification
                        if ((emailNotifyFlag || p_bNotify)
                            && ((!string.IsNullOrEmpty(objWPADiary.AssigningUser)) && (!string.Equals(objWPADiary.AssigningUser.ToUpper(), "SYSTEM")))
                            && objWPADiary.StatusOpen)
                        {
                            //Add by kualdeep for MITS:34958 Start
                            //If Multiple user available for diary in that case check how many user does not have e-mail id.
                            //MITS 35289 srajindersin 2/13/2014
                            if (sAssignedUsersArray != null || sAssignedGroupArray != null)
                            {
                                //iCounter = sAssignedUsersArray.GetLength(0);
                                if (iCounter > 0)
                                {
                                    string UsersInGroup = string.Empty;
                                    string TempEmail = string.Empty;
                                    sUserName = "";
                                    //for (int iUserLst = 0; iUserLst < iCounter; iUserLst++)
                                    {
                                        if (iCount >= (iCounter - iGroupCounter))
                                        {
                                            objReader = DbFactory.GetDbReader(m_sConnectionString, ("SELECT USER_ID FROM USER_MEMBERSHIP WHERE GROUP_ID = " + sAssignedGroupArray[iCount - (iCounter - iGroupCounter)]));

                                            while (objReader.Read())
                                            {
                                                UsersInGroup = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sSecConnectString, "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID = " + Convert.ToInt32(objReader.GetValue(0))));
                                                sUserName = sUserName + "'" + UsersInGroup + "'" + ",";
                                            }
                                        }
                                        else
                                        {
                                            //iUserID = Convert.ToInt32(sAssignedUsersArray[iUserLst]);
                                            //string sUserLoginName = Context.LocalCache.GetSystemLoginName(iUserID, Context.RMUser.DatabaseId, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(Context.ClientId), Context.ClientId);
                                            string sUserLoginName = sAssignedUsersArray[iCount];
                                            sUserName = sUserName + "'" + sUserLoginName + "'" + ",";
                                        }
                                    }
                                    sUserName = sUserName.TrimEnd(',').Trim();
                                    sUsrNameWtMail = GetNameWithoutMail(sUserName, objDMF.Context.RMDatabase.DataSourceId);
                                }
                                if (string.IsNullOrEmpty(sUsrNameWtMail) == true)
                                {
                                    var UserNameArray = sUserName.Split(',');
                                    for (int i = 0; i < UserNameArray.Length; i++)
                                    {
                                        sEmail = sEmail + "," + GetMailId(UserNameArray[i].Trim('\''));
                                    }
                                }
                            }
                            else//MITS 35289 srajindersin 2/18/2014
                            {
                                sEmail = GetMailId(objWPADiary.AssignedUser);
                            }
                            //Add by kualdeep for MITS:34958 End

                            if (!string.IsNullOrEmpty(sEmail))
                            {
                                objMail = new Mailer(m_iClientId);
                                objMail.To = sEmail;
                                objMail.From = GetMailId(objWPADiary.AssigningUser);
                                objMail.Subject = isNewDiary ? "A new task has been assigned" : "Your diary has been updated";
                                objMail.IsBodyHtml = true;
                                if (arrActivity == null)
                                {
                                    //when the diary is completed the activity element is not passed into this method
                                    List<string> values = new List<string>();
                                    string SQL = "SELECT ACT_TEXT FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID=" + objWPADiary.EntryId;
                                    using (DbReader rdr = DbFactory.GetDbReader(m_sConnectionString, SQL))
                                    {
                                        while (rdr.Read())
                                        {
                                            values.Add(rdr.GetValue(0) == null ? string.Empty : rdr.GetValue(0).ToString());
                                        }
                                        // rdr.Close();
                                    }
                                    if (values.Count > 0)
                                    {
                                        arrActivity = values.ToArray();
                                    }
                                }
                                //Asharma326 MITS 22047 Starts
                                if (arrActivity != null)
                                {
                                    string[] TemparrActivity = arrActivity;
                                    arrActivity = new string[TemparrActivity.Length / 2];
                                    int count = 0;
                                    for (int i = 0; i < TemparrActivity.Length; i++)
                                    {
                                        if (i % 2 != 0)
                                        {
                                            arrActivity[count] = TemparrActivity[i];
                                            count++;
                                        }
                                    }
                                }
                                //Asharma326 MITS 22047 Ends
                                objMail.Body = objWPADiary.GetEmailNotificationHtmlBody(arrActivity, isNewDiary ? Riskmaster.DataModel.DiaryAction.Assign : Riskmaster.DataModel.DiaryAction.Update);
                                objMail.SendMail();
                            }
                        }
                        //07/06/2011 SMISHRA54: End
                    }

                    //Abhishek MITS 11260, Mails should not be sent in case of system user
                    //Send mail				
                    if (((Convert.ToBoolean(objWPADiary.AutoConfirm)) ||
                        (Convert.ToBoolean(objWPADiary.NotifyFlag)) ||
                        (p_bNotify == true)) && (objWPADiary.AssigningUser != "" && objWPADiary.AssigningUser.ToUpper() != "SYSTEM")
                        && (objWPADiary.StatusOpen == false))
                    {

                    //Arnab: MITS-10912 - Changed the notification to Assigning User
                    //sEmail = GetMailId(objWPADiary.AssignedUser);
                    sEmail = GetMailId(objWPADiary.AssigningUser);

                    if (sEmail != "")
                    {
                        objMail = new Mailer(m_iClientId);
                        objMail.To = sEmail;
                        //Mukul 3/12/2007 MITS 9020 
                        //objMail.From = GetMailId(objWPADiary.AssigningUser);
                        //Abhishek 09/01/2008 completed MITS 10912
                        if (!string.IsNullOrEmpty(objWPADiary.AssignedGroup) && (string.IsNullOrEmpty(objWPADiary.AssignedUser) || (objWPADiary.AssignedUser == "NA")))
                        {
                            objMail.From = GetMailId(p_sUserName);
                        }
                        else
                            objMail.From = GetMailId(objWPADiary.AssignedUser);
                        objMail.Subject = "Task Completed";
                        objMail.IsBodyHtml = true;
                        if (arrActivity == null)
                        {
                            //when the diary is completed the activity element is not passed into this method
                            List<string> values = new List<string>();
                            string SQL = "SELECT ACT_TEXT FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID=" + objWPADiary.EntryId;
                            using (DbReader rdr = DbFactory.GetDbReader(m_sConnectionString, SQL))
                            {
                                while (rdr.Read())
                                {
                                    values.Add(rdr.GetValue(0) == null ? string.Empty : rdr.GetValue(0).ToString());
                                }
                                // rdr.Close();
                            }
                            if (values.Count > 0)
                            {
                                arrActivity = values.ToArray();
							}
                        }
                            //Asharma326 MITS 22047 Starts
                            if (arrActivity != null)
                            {
                                string[] TemparrActivity = arrActivity;
                                arrActivity = new string[TemparrActivity.Length / 2];
                                int count = 0;
                                for (int i = 0; i < TemparrActivity.Length; i++)
                                {
                                    if (i % 2 != 0)
                                    {
                                        arrActivity[count] = TemparrActivity[i];
                                        count++;
                                    }
                                }
                            }
                            //Asharma326 MITS 22047 Ends
                            objMail.Body = objWPADiary.ToHtml(arrActivity);
                          objMail.SendMail();
                        }
                    }
                }
                bRetVal = true;
            }
            catch (RMAppException p_objException) //pmittal5
            {
                objErrors = objWPADiary.Context.ScriptValidationErrors;//srajindersin MITS 26319 04/20/2012
                bRetVal = false;
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                bRetVal = false;
                throw new RMAppException(Globalization.GetString("WPA.SaveDiary.SaveError", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }

                if (objDMF != null)
                {
                    //objDMF.UnInitialize();
                    objDMF.Dispose();
                    objDMF = null;
                }

                objXmlElem = null;
                objWPADiary = null;
                objDiaryAct = null;
                //p_objXMLInput = null;  // Mob app : Ishan
                objDiaryNode = null;
                objMail = null;
                //igupta3 Mits# 32846
                loadDiaryNode = null;
                activitiesList = null;
                objXMLInputDoc = null;
                objSuppXMlDoc = null;//JIRA - 4691 - Diary Supplemental Changes.
            }
            return (bRetVal);
        }
        #endregion

        #region Load Diary Information

        /// <summary>
        /// Load the Diary information for a given Diary
        /// </summary>
        /// <param name="p_iEntryId">Entry Id</param>
        /// <param name="p_sUserName">User name</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="objUserLogin">User Login Object</param>
        /// <returns>XML string containing Diary data</returns>
        public XmlDocument LoadDiary(int p_iEntryId, string p_sUserName, string p_sPassword, UserLogin objUserLogin)
        {
            string sClaimantName = "";
            string sDepartmentName = "";
            string sOrgLevel = "";
            string sSQL = "";
            string sActs = "";
            string sUserId = "";
            string sUser = "";
            string sUserTemp = "";
            int iIterator = 0;
            string p_sDsnName = string.Empty; //Aman ML Change
            int iCnt = 0;
            DataModelFactory objDMF = null;
            WpaDiaryEntry objWPADiary = null;
            XmlDocument objXMLDom = null;
            WpaDiaryAct objDiaryAct = null;
            DbConnection objConn = null;
            DbReader objReader = null;
            RMConfigurator objConfig = null;
            XmlElement objElement = null;
            // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
            string sAttachRecordInfo = string.Empty;
            // npadhy End MITS 22028 Policies opening up in Maintenance when opened from Diaries

            //Manish Multicurrency
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sCurrency = string.Empty;
            LocalCache objCache = null;
            objCache = new LocalCache(m_sConnectionString,m_iClientId);

            objCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(m_sConnectionString), ref sShortCode, ref sDesc);
            sCurrency = sDesc.Split('|')[1];
            string sCodeDescRouteFlag = string.Empty;
            string sCodeDescRollFlag = string.Empty;

            try
            {
                p_sDsnName = objUserLogin.objRiskmasterDatabase.DataSourceName; //Aman ML Change
                objDMF = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);
                objXMLDom = new XmlDocument();
                //Get WPADiaryEntry instance
                objWPADiary = (WpaDiaryEntry)objDMF.GetDataModelObject("WpaDiaryEntry", false);
                objDiaryAct = (WpaDiaryAct)objDMF.GetDataModelObject("WpaDiaryAct", false);

                if (p_iEntryId > 0)
                    objWPADiary.MoveTo(p_iEntryId);
                else if (p_iEntryId == 0)
                {
                    objWPADiary.EntryId = 0;
                    //zmohammad MITs 33655 Diary Initialisation script : Start
                    objWPADiary.FiringScriptFlag = 1;
                    ((DataRoot)objWPADiary).InitializeScriptData();
                }
                else
                    throw new RMAppException(Globalization.GetString("WPA.LoadDiary.InvalidDiaryId", m_iClientId));


                CreateXML(ref objXMLDom, "TaskNotes", objWPADiary.EntryNotes, "", 0);
                CreateXML(ref objXMLDom, "TaskSubject", objWPADiary.EntryName, "", 0);
                // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
                sAttachRecordInfo = GetAttachRecordInfoDetails(objWPADiary.AttachTable, objWPADiary.AttachRecordid);
                CreateXML(ref objXMLDom, "AttachRecord", sAttachRecordInfo, "", 0);
                // npadhy End MITS 22028 Policies opening up in Maintenance when opened from Diaries
                CreateXML(ref objXMLDom, "CompleteDate", Conversion.GetDBDateFormat(objWPADiary.CompleteDate, "d"), "", 0);
                CreateXML(ref objXMLDom, "CompleteTime", Conversion.GetDBTimeFormat(Conversion.ConvertObjToStr(objWPADiary.CompleteTime), "t"), "", 0);
                CreateXML(ref objXMLDom, "EstimateTime", Conversion.ConvertObjToStr(objWPADiary.EstimateTime), "", 0);
                CreateXML(ref objXMLDom, "Priority", Conversion.ConvertObjToStr(objWPADiary.Priority), "", 0);
                CreateXML(ref objXMLDom, "AutoConfirm", Conversion.ConvertObjToStr(objWPADiary.AutoConfirm), "", 0);
                //Added by Amitosh for R8 enhancement of Auto late diary notify
                CreateXML(ref objXMLDom, "OverDueDiaryNotify", Conversion.ConvertObjToStr(objWPADiary.AutoLateNotify), "", 0);
                //igupta3 non rollable/routable permission jira 439

                if (Conversion.ConvertObjToStr(objWPADiary.NonRouteFlag) == Boolean.TrueString)
                {
                    sCodeDescRouteFlag = objCache.GetCodeDesc(objCache.GetCodeId("Y", "YES_NO"), m_iLangCode);
 
                }
                else if (Conversion.ConvertObjToStr(objWPADiary.NonRouteFlag) == Boolean.FalseString)
                {
                    sCodeDescRouteFlag = objCache.GetCodeDesc(objCache.GetCodeId("N", "YES_NO"), m_iLangCode);
                }
                CreateXML(ref objXMLDom, "NonRoutableFlag", sCodeDescRouteFlag, "", 0);

                if (Conversion.ConvertObjToStr(objWPADiary.NonRollFlag) == Boolean.TrueString)
                {
                    sCodeDescRollFlag = objCache.GetCodeDesc(objCache.GetCodeId("Y", "YES_NO"), m_iLangCode);

                }
                else if (Conversion.ConvertObjToStr(objWPADiary.NonRollFlag) == Boolean.FalseString)
                {
                    sCodeDescRollFlag = objCache.GetCodeDesc(objCache.GetCodeId("N", "YES_NO"), m_iLangCode);
                }
                CreateXML(ref objXMLDom, "NonRollableFlag", sCodeDescRollFlag, "", 0);

                CreateXML(ref objXMLDom, "AssignedUser", Conversion.ConvertObjToStr(objWPADiary.AssignedUser), "", 0);

                sUser = objWPADiary.AssignedUser;

                CreateXML(ref objXMLDom, "AssignedGroup", Conversion.ConvertObjToStr(objWPADiary.AssignedGroup), "", 0);//pkandhari Jira 6412

                if (!string.IsNullOrEmpty(sUser) && objWPADiary.FiringScriptFlag == 1)
                {
                    string[] arr = sUser.Split(new char[] { ' ' });
                    for (iIterator = 0; iIterator < arr.Length; iIterator++)
                    {
                        Dictionary<string, string> dictParams = new Dictionary<string, string>();
                        sSQL = string.Format("SELECT DISTINCT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = {0}", "~USERNAME~");
                        dictParams.Add("USERNAME", arr[iIterator]);
                        using (objReader = DbFactory.ExecuteReader(m_sSecConnectString, sSQL, dictParams))
                        {
                            while (objReader.Read())
                            {
                                if (string.IsNullOrEmpty(sUserId))
                                {
                                    sUserId = Convert.ToString(objReader.GetValue(0));
                                }
                                else
                                {
                                    sUserId = sUserId + " " + Convert.ToString(objReader.GetValue(0));
                                }
                            }

                        }

                    }
                }
                CreateXML(ref objXMLDom, "AssignedUserID", sUserId, "", 0);

                //Abhishek MITS 11260, for system generated diaries notify flag should not be set
                if (objWPADiary.AssigningUser.ToUpper() != "SYSTEM")
                {
                    //Notify Flag is also needed..raman bhatia
                    CreateXML(ref objXMLDom, "NotifyFlag", Conversion.ConvertObjToStr(objWPADiary.NotifyFlag), "", 0);
                }
                else
                {
                    CreateXML(ref objXMLDom, "NotifyFlag", false.ToString(), "", 0);
                }
                if (p_iEntryId > 0)
                {
                    CreateXML(ref objXMLDom, "EntryId", Conversion.ConvertObjToStr(objWPADiary.EntryId), "", 0);
                    CreateXML(ref objXMLDom, "Regarding", objWPADiary.Regarding, "", 0);
                    CreateXML(ref objXMLDom, "CreateDate", Conversion.ToDate(objWPADiary.CreateDate).ToString(), "", 0);
                    CreateXML(ref objXMLDom, "Activities", "", "", 0);
                    CreateXML(ref objXMLDom, "StatusOpen", Conversion.ConvertObjToStr(objWPADiary.StatusOpen), "", 0);
                    CreateXML(ref objXMLDom, "ResponseDate", Conversion.GetDBDateFormat(objWPADiary.ResponseDate, "d"), "", 0);
                    CreateXML(ref objXMLDom, "Response", objWPADiary.Response, "", 0);
                    CreateXML(ref objXMLDom, "TeTracked", Conversion.ConvertObjToStr(objWPADiary.TeTracked), "", 0);
                    CreateXML(ref objXMLDom, "TEStartTime", Conversion.GetDBTimeFormat(objWPADiary.TeStartTime, "t"), "", 0);
                    CreateXML(ref objXMLDom, "TEEndTime", Conversion.GetDBTimeFormat(objWPADiary.TeEndTime, "t"), "", 0);
                    CreateXML(ref objXMLDom, "TeTotalHours", Conversion.ConvertObjToStr(objWPADiary.TeTotalHours), "", 0);
                    CreateXML(ref objXMLDom, "TeExpenses", Conversion.ConvertObjToStr(objWPADiary.TeExpenses), "", 0);
                    CreateXML(ref objXMLDom, "CompletedByUser", Conversion.ConvertObjToStr(objWPADiary.CompletedByUser), "", 0);
                    CreateXML(ref objXMLDom, "BaseCurrency", sCurrency, "", 0);
                CreateXML(ref objXMLDom, "AssigningUser", Conversion.ConvertObjToStr(objWPADiary.AssigningUser.ToLower()), "", 0); //tmalhotra3-MITS 25091


                    //MAIL_DISABLED FLAG IS ALSO NEEDED..raman bhatia
                    //CreateXML(ref objXMLDom, "MAIL_DISABLED", RMConfigurationManager.GetSingleTagSectionSettings("Diaries")[DISABLE_MAIL].ToString(), "", 0);
                   // CreateXML(ref objXMLDom, "MAIL_DISABLED", m_DiariesConfigs[DISABLE_MAIL].ToString(), "", 0);
                  //dvatsa JIRA-11627(start)
                    //if (m_DiariesConfigs[ENABLE_MAIL].ToString() == Boolean.TrueString)
                    CreateXML(ref objXMLDom, "MAIL_ENABLED", m_DiariesConfigs[ENABLE_MAIL].ToString(), "", 0);

                    //dvatsa JIRA-11627(end)
                    //tanwar2 - MCIC - mits 30532  - start
                    CreateXML(ref objXMLDom, "LoggedInUser", p_sUserName, string.Empty, 0);
                    //tanwar2 - MCIC - mits 30532  - end					
                    sSQL = "SELECT * FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID=" + objWPADiary.EntryId;

                    objConn = DbFactory.GetDbConnection(m_sConnectionString);
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    //Add activities for the given diary
                    while (objReader.Read())
                    {
                        CreateXML(ref objXMLDom, "Activity", "", "Activities", iCnt);
                        CreateXML(ref objXMLDom, "ActivityId", Conversion.ConvertObjToStr(objReader.GetValue("ACT_CODE")), "Activity", iCnt);
                        CreateXML(ref objXMLDom, "ActivityName", Conversion.ConvertObjToStr(objReader.GetValue("ACT_TEXT")), "Activity", iCnt);
                        iCnt++;
                        //Activity data is needed in a string format too..raman bhatia
                        if (sActs != "")
                        {
                            sActs = (sActs + "|");
                        }
                        sActs = sActs + objReader.GetValue("ACT_CODE") + "|" + objReader.GetValue("ACT_TEXT");
                    }
                    objElement = objXMLDom.CreateElement("sActs");
                    objElement.InnerText = sActs;
                    objXMLDom.SelectSingleNode("//Activities").AppendChild(objElement);

                    //objElement = objXMLDom.CreateElement("BaseCurrency");
                    //XmlNode xNode = objXMLDom.CreateNode(XmlNodeType.Element, "BaseCurrency", "");
                    //objElement.InnerText = sCurrency;
                    //xNode.AppendChild(objElement);


                    if (objWPADiary.AttachTable.ToUpper() == "CLAIM")
                    {
                        sClaimantName = GetClaimantsName(objWPADiary.AttachRecordid);
                        sDepartmentName = GetDeptName(0, objWPADiary.AttachRecordid, ref sOrgLevel);
                        CreateXML(ref objXMLDom, "ClaimantName", sClaimantName, "", 0);
                        CreateXML(ref objXMLDom, "DepartmentName", sDepartmentName, "", 0);
                        CreateXML(ref objXMLDom, "OrgLevel", sOrgLevel, "", 0);
                    }
                    else if (objWPADiary.AttachTable.ToUpper() == "CLAIMANT")
                    {
                        sClaimantName = GetEntityName(objWPADiary.AttachRecordid);
                        CreateXML(ref objXMLDom, "ClaimantName", sClaimantName, "", 0);
                    }
                    else if (objWPADiary.AttachTable.ToUpper() == "EVENT")
                    {
                        sDepartmentName = GetDeptName(objWPADiary.AttachRecordid, 0, ref sOrgLevel);
                        CreateXML(ref objXMLDom, "DepartmentName", sDepartmentName, "", 0);
                        CreateXML(ref objXMLDom, "OrgLevel", sOrgLevel, "", 0);
                    }

                    GetWpaTaskList(objUserLogin, objXMLDom);  //mits 9388

                    //Adaptor layer changes Sumeet
                    //sRetVal = objXMLDom.InnerXml;
                    //nsachdeva2 MITS:23654 09/19/2011
                    sSQL = "SELECT FREEZE_DIARY_COMPLETION_DT FROM SYS_PARMS";
                    objConn = DbFactory.GetDbConnection(m_sConnectionString);
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        CreateXML(ref objXMLDom, "FreezeCompletionDate", Conversion.ConvertStrToBool(objReader.GetValue("FREEZE_DIARY_COMPLETION_DT").ToString()).ToString(), "", 0);
                    }
                    //End MITS:23654
                }

                //zmohammad MITs 33655 Diary Initialisation script : End
                return (objXMLDom);
            }

            catch (DataModelException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.LoadDiary.Error", m_iClientId), p_objException);
            }

            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objDMF != null) objDMF.Dispose();
                if (objWPADiary != null) objWPADiary.Dispose();
                objXMLDom = null;
                if (objDiaryAct != null) objDiaryAct.Dispose();
                if (objConfig != null) objConfig = null;
            }
            //Adaptor layer changes Sumeet
            //return (sRetVal);
        }

        // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
        public string GetAttachRecordInfoDetails(string p_sAttachTable, long p_lRecordId)
        {
            string sAttachRecordInfo = string.Empty;
            string sPIXmlFormName = string.Empty;
            string sFName = string.Empty;
            string sLName = string.Empty;
            string sName = string.Empty;
            LocalCache objCache = null;
            try
            {
                objCache = new LocalCache(m_sConnectionString,m_iClientId);
                switch (p_sAttachTable)
                {
                    case CLAIM:
                    case "claim":
                        sAttachRecordInfo = String.Concat("Claim: ", GetSingleString("CLAIM_NUMBER", "CLAIM", String.Concat("CLAIM_ID = ", p_lRecordId.ToString()), m_sConnectionString));
                        break;
                    case EVENT:
                    case "event":
                        sAttachRecordInfo = String.Concat("Event: ", GetSingleString("EVENT_NUMBER", "EVENT", String.Concat("EVENT_ID = ", p_lRecordId.ToString()), m_sConnectionString));
                        break;
                    case ENTITY:
                    case "entity":
                        objCache.GetEntityInfo(p_lRecordId, ref sFName, ref sLName);
                        sName = sFName.Trim().Equals("") ? sLName : sFName + ", " + sLName;
                        sAttachRecordInfo = String.Concat("Person\\Entity: ", sName);
                        break;
                    case CLAIMANT:
                    case "claimant":
                        objCache.GetClaimantInfo(p_lRecordId, ref sFName, ref sLName);
                        sName = sFName.Trim().Equals("") ? sLName : String.Concat(sFName, ", ", sLName);
                        sAttachRecordInfo = String.Concat("Claimant:", sName);
                        break;
                    case PERSON_INVOLVED:
                    case "person_involved":
                        sPIXmlFormName = GetPiXMLFormName(p_lRecordId, ref sAttachRecordInfo);
                        break;
                    case TM_JOB_LOG:
                    case "tm_job_log":
                        sAttachRecordInfo = "Task Manager Job ( " + p_lRecordId.ToString() + ")";
                        break;
                    case FUNDS: // MITS: 22769 Case added for funds. We need to display the control number in the Attached Record field on the diary list screen.
                    case "funds":
                        sAttachRecordInfo = String.Concat("FUNDS: ", GetSingleString("CTL_NUMBER", "FUNDS", String.Concat("TRANS_ID = ", p_lRecordId.ToString()), m_sConnectionString));
                        break;
                    case POLICY_ENH:
                    case "POLICY_ENH":
                    case "policy_enh":
                        // Get the LOB information of the Policy
                        int iLob = GetSingleInt("POLICY_TYPE", "POLICY_ENH", String.Concat("POLICY_ID = ", p_lRecordId.ToString()), m_sConnectionString);
                        sAttachRecordInfo = String.Concat("POLICY MANAGEMENT (", objCache.GetShortCode(iLob), ") ", GetSingleString("POLICY_NAME", "POLICY_ENH", String.Concat("POLICY_ID = ", p_lRecordId.ToString()), m_sConnectionString));
                        break;
                    default:

                        sAttachRecordInfo = String.Concat(p_sAttachTable.ToUpper(), "( ", p_lRecordId.ToString(), ")");
                        break;
                }
                return sAttachRecordInfo;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.EditDiary.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }

            }

        }
        // npadhy End MITS 22028 Policies opening up in Maintenance when opened from Diaries
        #endregion

        #region Add XML
        /// <summary>
        /// Appends XML element to the XML Document
        /// </summary>
        /// <param name="p_objXMLDoc">XML document containing data for the Diary</param>
        /// <param name="p_sName">Name of the element</param>
        /// <param name="p_sVal">Value of the element</param>
        private void AppendChildWithText(ref XmlDocument p_objXMLDoc, String p_sName, String p_sVal)
        {
            XmlElement objRoot = null;
            XmlElement objElement = null;
            try
            {
                if (p_objXMLDoc.DocumentElement == null)
                {
                    objRoot = p_objXMLDoc.CreateElement("LoadDiary");
                    p_objXMLDoc.AppendChild(objRoot);
                }
                else
                {
                    objRoot = p_objXMLDoc.DocumentElement;
                }

                objElement = p_objXMLDoc.CreateElement(p_sName);
                objElement.AppendChild(p_objXMLDoc.CreateTextNode(p_sVal));
                objRoot.AppendChild(objElement);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.AppendChildWithText.XMLError", m_iClientId), p_objException);
            }
            finally
            {
                objRoot = null;
                objElement = null;
            }
        }
        #endregion

        #region Create XML
        /// <summary>
        /// Create XML from the data received from DB.
        /// </summary>
        /// <param name="p_objXML">Main document element</param>
        /// <param name="p_sTagName">Name of the element</param>
        /// <param name="p_sTagValue">Value of the element</param>
        /// <param name="p_sParentTag">Parent element</param>
        /// <param name="p_iLevel">Level at which to put an element</param>
        private void CreateXML(ref XmlDocument p_objXML, string p_sTagName, string p_sTagValue, string p_sParentTag, int p_iLevel)
        {
            XmlElement objMainElement = null;
            XmlElement objSubElement = null;
            XmlElement objChildElem = null;

            try
            {
                if (!(p_objXML.HasChildNodes))
                {
                    objMainElement = p_objXML.CreateElement("LoadDiary");
                    p_objXML.AppendChild(objMainElement);
                }
                else
                    objMainElement = (XmlElement)(p_objXML.GetElementsByTagName("LoadDiary").Item(0));

                if (p_sParentTag == "Activities")
                {
                    objSubElement = (XmlElement)(p_objXML.GetElementsByTagName("Activities").Item(0));
                    objChildElem = p_objXML.CreateElement(p_sTagName);
                    objSubElement.AppendChild(objChildElem);
                }
                else if (p_sParentTag == "Activity")
                {
                    objSubElement = (XmlElement)(p_objXML.GetElementsByTagName("Activity").Item(p_iLevel));
                    objChildElem = p_objXML.CreateElement(p_sTagName);
                    objChildElem.InnerText = p_sTagValue;
                    objSubElement.AppendChild(objChildElem);
                }
                else
                {
                    objSubElement = p_objXML.CreateElement(p_sTagName);
                    objSubElement.InnerText = p_sTagValue;
                    objMainElement.AppendChild(objSubElement);
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.CreateXML.CreateError", m_iClientId), p_objException);
            }
            finally
            {
                objMainElement = null;
                objSubElement = null;
                objChildElem = null;
            }
        }
        #endregion

        #region Get Claimant Name
        /// <summary>
        /// Gets the claimant name
        /// </summary>
        /// <param name="p_lClaimID">Claim id</param>
        /// <returns>Returns the first & last name of the claimant</returns>
        public string GetClaimantsName(long p_lClaimID)
        {
            string sSQL = string.Empty, sRetVal = string.Empty;
            //DbReader objReader = null;
            //bool bHasResults = false;

            try
            {
                if (p_lClaimID == 0)
                    return (sRetVal);

                //Raman: R7 Prf Imp
                //2 queries are being fired unneccasarily 

                /*
                sSQL = String.Format("SELECT ENTITY.LAST_NAME, ENTITY.FIRST_NAME, PRIMARY_CLMNT_FLAG FROM CLAIMANT, ENTITY WHERE CLAIMANT.CLAIM_ID = {0} AND CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID", p_lClaimID);
				objReader = DbFactory.GetDbReader(m_sConnectionString,String.Format("{0} AND CLAIMANT.PRIMARY_CLMNT_FLAG <> 0", sSQL));
			
				bHasResults = objReader.Read();
				if (!bHasResults)
				{
					objReader.Close();
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
					bHasResults = objReader.Read();
				}

				if (bHasResults)
					sRetVal = String.Format("{0}, {1}", objReader.GetValue("LAST_NAME"), objReader.GetValue("FIRST_NAME"));

				objReader.Close();
                 */

                sSQL = String.Format("SELECT ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM CLAIMANT, ENTITY WHERE CLAIMANT.CLAIM_ID = {0} AND CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND PRIMARY_CLMNT_FLAG = -1 ORDER BY PRIMARY_CLMNT_FLAG ASC ", p_lClaimID);
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        sRetVal = String.Format("{0}, {1}", objReader.GetValue("LAST_NAME"), objReader.GetValue("FIRST_NAME"));
                    }
                }
            }
            finally
            {

            }
            return (sRetVal);
        }


        #endregion

        #region Department Name
        /// <summary>
        /// Gets the department name.
        /// </summary>
        /// <param name="p_lTmpEventID">Event Id</param>
        /// <param name="p_lTmpClaimID">Claim Id</param>
        /// <param name="p_sOrgLevel">Organizational level</param>
        /// <returns>Name of the department</returns>
        public string GetDeptName(long p_lTmpEventID, long p_lTmpClaimID, ref string p_sOrgLevel)
        {
            string sSQL = "";
            string sOrgLevelName = "";
            string sLN = "";
            string sFN = "";
            long lTmpDeptID = 0;
            long lOrgLevel = 0;
            //DbReader objReader = null;

            try
            {
                sSQL = "SELECT DIARY_ORG_LEVEL FROM SYS_PARMS";
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {

                    if (objReader.Read())
                    {
                        lOrgLevel = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("DIARY_ORG_LEVEL")));
                        //objReader.Close();
                    }
                    else
                        lOrgLevel = 1012;
                }
                //Get the Name of the org Hier level
                if (p_lTmpEventID != 0)
                    sOrgLevelName = GetDiaryOrgName(false, p_lTmpEventID, lOrgLevel, p_sOrgLevel);
                else if (p_lTmpClaimID != 0)
                    sOrgLevelName = GetDiaryOrgName(true, p_lTmpClaimID, lOrgLevel, p_sOrgLevel);

                if (sOrgLevelName == "")
                {
                    if (p_lTmpEventID != 0)
                    {
                        sSQL = "SELECT DEPT_EID FROM EVENT WHERE EVENT_ID = " + p_lTmpEventID;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {

                            if (objReader.Read())
                            {
                                lTmpDeptID = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("DEPT_EID")));
                                //objReader.Close();
                            }
                        }
                    }
                    else if (p_lTmpClaimID != 0)
                    {
                        sSQL = "SELECT DEPT_EID FROM CLAIM, EVENT WHERE CLAIM.EVENT_ID = EVENT.EVENT_ID AND CLAIM.CLAIM_ID = " + p_lTmpClaimID;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {

                            if (objReader.Read())
                            {
                                lTmpDeptID = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("DEPT_EID")));
                                //objReader.Close();
                            }
                        }
                    }

                    sSQL = "SELECT LAST_NAME, FIRST_NAME FROM ENTITY WHERE ENTITY.ENTITY_ID = " + lTmpDeptID;
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {

                        if (objReader.Read())
                        {
                            sFN = objReader.GetString("FIRST_NAME");
                            sLN = objReader.GetString("LAST_NAME");

                            if (sFN.Trim().Length > 0)
                                sOrgLevelName = sFN + " " + sLN;
                            else
                                sOrgLevelName = sLN;

                        }
                        p_sOrgLevel = "Department";
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetDeptName.Error", m_iClientId), p_objException);
            }
            finally
            {
                //				if (objReader != null) 
                //				{
                //					objReader.Close();
                //					objReader.Dispose();
                //				}
            }
            return (sOrgLevelName);
        }
        #endregion

        #region Get the Organization name
        /// <summary>
        /// Gets the name of the Organization
        /// </summary>
        /// <param name="p_bClaimID">Claim Id</param>
        /// <param name="p_lThisID">Name of the Event or Claim Id</param>
        /// <param name="p_lThisOrgLevel">Organizational level for the given Claim or Event</param>
        /// <param name="p_sOrgLevel">Organizational level</param>
        /// <returns>Name of the Organization</returns>
        private string GetDiaryOrgName(bool p_bClaimID, long p_lThisID, long p_lThisOrgLevel, string p_sOrgLevel)
        {
            string sTmp = "";
            string sSQL = "";
            string sLN = "";
            string sFN = "";
            long lThisEID = 0;
            //	DbReader objReader = null;

            try
            {
                switch (p_lThisOrgLevel)
                {
                    case 1005:   //client
                        p_sOrgLevel = "CLIENT_EID";
                        break;
                    case 1006:   //company
                        p_sOrgLevel = "COMPANY_EID";
                        break;
                    case 1007:   //operation
                        p_sOrgLevel = "OPERATION_EID";
                        break;
                    case 1008:   //region
                        p_sOrgLevel = "REGION_EID";
                        break;
                    case 1009:   //division
                        p_sOrgLevel = "DIVISION_EID";
                        break;
                    case 1010:   //location
                        p_sOrgLevel = "LOCATION_EID";
                        break;
                    case 1011:   //facility
                        p_sOrgLevel = "FACILITY_EID";
                        break;
                    case 1012:   //department
                        p_sOrgLevel = "DEPARTMENT_EID";
                        break;
                    default:
                        p_sOrgLevel = "";
                        break;
                }

                lThisEID = 0;
                sTmp = "";

                if (p_sOrgLevel != "")
                {
                    if (p_bClaimID)
                    {
                        sSQL = "SELECT " + p_sOrgLevel + ", EVENT.DEPT_EID FROM CLAIM, EVENT, ORG_HIERARCHY WHERE CLAIM_ID = " + p_lThisID + " AND CLAIM.EVENT_ID = EVENT.EVENT_ID AND ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID";
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                lThisEID = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue(0)));
                                //objReader.Close();
                            }
                            else
                            {
                                sSQL = "SELECT " + p_sOrgLevel + ", EVENT.DEPT_EID FROM EVENT, ORG_HIERARCHY WHERE EVENT_ID = " + p_lThisID + " AND ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID";
                                using (DbReader objReader_inn = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                {
                                    if (objReader_inn.Read())
                                    {
                                        lThisEID = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader_inn.GetValue(0)));
                                        //objReader_inn.Close();
                                    }
                                }
                            }
                        }
                        if (lThisEID != 0)
                        {
                            //Raman: R7 Prf Imp
                            //sSQL = "SELECT * FROM ENTITY WHERE ENTITY_ID = " + lThisEID;
                            sSQL = "SELECT FIRST_NAME , LAST_NAME FROM ENTITY WHERE ENTITY_ID = " + lThisEID;
                            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                            {
                                if (objReader.Read())
                                {
                                    sFN = objReader.GetString("FIRST_NAME");
                                    sLN = objReader.GetString("LAST_NAME");
                                    //objReader.Close();

                                    if (sFN.Trim().Length > 0)
                                        sTmp = sFN + " " + sLN;
                                    else
                                        sTmp = sLN;
                                }
                            }
                        }

                        if (p_sOrgLevel != "")
                            p_sOrgLevel = p_sOrgLevel.Substring(0, p_sOrgLevel.Length - 4);
                    }
                    else //Added by Nitin on 20-Feb-2009 to R5-R4 merging of mits 12505 
                    {
                        // Starts by Nitin for Mits 12505 ,for the case of Event
                        sSQL = "SELECT " + p_sOrgLevel + ", EVENT.DEPT_EID FROM EVENT, ORG_HIERARCHY WHERE EVENT_ID = " + p_lThisID + " AND ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID";
                        using (DbReader objReader_inn = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (objReader_inn.Read())
                            {
                                lThisEID = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader_inn.GetValue(0)));
                                //objReader_inn.Close();
                            }
                        }

                        if (lThisEID != 0)
                        {
                            //Raman: R7 Prf Imp
                            //sSQL = "SELECT * FROM ENTITY WHERE ENTITY_ID = " + lThisEID;
                            sSQL = "SELECT FIRST_NAME , LAST_NAME FROM ENTITY WHERE ENTITY_ID = " + lThisEID;
                            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                            {
                                if (objReader.Read())
                                {
                                    sFN = objReader.GetString("FIRST_NAME");
                                    sLN = objReader.GetString("LAST_NAME");
                                    //objReader.Close();

                                    if (sFN.Trim().Length > 0)
                                        sTmp = sFN + " " + sLN;
                                    else
                                        sTmp = sLN;
                                }
                            }
                        }

                        if (p_sOrgLevel != "")
                            p_sOrgLevel = p_sOrgLevel.Substring(0, p_sOrgLevel.Length - 4);
                    }
                    //End by Nitin for Mits 12505 ,for the case of Event
                }
            }
            //Ended by Nitin on 20-Feb-2009 to R5-R4 merging of mits 12505 
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetDiaryOrgName.Error", m_iClientId), p_objException);
            }

            finally
            {
                //				if (objReader != null) 
                //				{
                //					objReader.Close();
                //					objReader.Dispose();
                //				}
            }
            return (sTmp);
        }
        #endregion

        #region Entity Name
        /// <summary>
        /// Gets Entity Name
        /// </summary>
        /// <param name="p_lEntityId">Entity Id</param>
        /// <returns>Entity first name & last name</returns>
        private string GetEntityName(long p_lEntityId)
        {
            string sSQL = "";
            string sLN = "";
            string sFN = "";
            string sRetVal = "";
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID = " + p_lEntityId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                {
                    sFN = objReader.GetString("FIRST_NAME");
                    sLN = objReader.GetString("LAST_NAME");
                    sRetVal = sLN + ", " + sFN;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetEntityName.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (sRetVal);
        }
        #endregion

        #region Get Options
        /// <summary>
        /// Gets the options from config file
        /// </summary>
        /// <param name="p_sKey">key representing XML element</param>
        /// <returns></returns>
        public bool GetOption(string p_sKey)
        {
            Hashtable objLoadOptions = null;
            bool bRet = false;
            try
            {
                objLoadOptions = new Hashtable();
                LoadOptions(ref objLoadOptions);

                if (objLoadOptions.ContainsKey(p_sKey))
                {
                    bRet = true;
                }
            }
            catch (FileInputOutputException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetOption.Error", m_iClientId), p_objException);
            }
            finally
            {
                objLoadOptions = null;
            }
            return (bRet);
        }
        #endregion

        #region Load Options
        /// <summary>
        /// Load the options from the config file.
        /// </summary>
        /// <param name="p_objLoadOptions">Hash table containing the config keys & corresponding values</param>
        private void LoadOptions(ref Hashtable p_objLoadOptions)
        {
            string sValue = string.Empty;
            const string AUTO_DIARY = "AutoDiary";
            const string LOAD_TO_DO = "LoadToDo";
            const string MAIL_CHECKING = "MailChecking";

            // Ash - cloud, replace objConfig with existing m_DiariesConfigs
            //Hashtable objConfig = RMConfigurationManager.GetSingleTagSectionSettings("Diaries");

            //if (Convert.ToBoolean(m_DiariesConfigs[DISABLE_MAIL].ToString()))
            //{
            //    p_objLoadOptions.Add(DISABLE_MAIL, m_DiariesConfigs[DISABLE_MAIL].ToString());
            //} // if
            //dvatsa JIRA-11627(start)
            if (Convert.ToBoolean(m_DiariesConfigs[ENABLE_MAIL].ToString()))
            {
                p_objLoadOptions.Add(ENABLE_MAIL, m_DiariesConfigs[ENABLE_MAIL].ToString());
            }
           //dvatsa JIRA-11627 (end)
            if (Convert.ToBoolean(m_DiariesConfigs[AUTO_DIARY].ToString()))
            {
                p_objLoadOptions.Add(AUTO_DIARY, m_DiariesConfigs[AUTO_DIARY].ToString());
            } // if
            if (Convert.ToBoolean(m_DiariesConfigs[LOAD_TO_DO].ToString()))
            {
                p_objLoadOptions.Add(LOAD_TO_DO, m_DiariesConfigs[LOAD_TO_DO].ToString());
            } // if
            if (Convert.ToBoolean(m_DiariesConfigs[MAIL_CHECKING].ToString()))
            {
                p_objLoadOptions.Add(MAIL_CHECKING, m_DiariesConfigs[MAIL_CHECKING].ToString());
            } // if

        }
        #endregion

        // 11/01/2007 REM Umesh
        /// <summary>
        /// Create Default User Prefernce Xml.
        /// </summary>
        /// <param name="p_objUserPrefXML">User Preference Xml Doc</param>
        /// 
        private void CreateUserPreferXML(XmlDocument p_objUserPrefXML)
        {
            XmlElement objTempNode = null;
            XmlElement objParentNode = null;
            try
            {
                // Create setting node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                if (objTempNode == null)
                {
                    objTempNode = p_objUserPrefXML.CreateElement("setting");
                    p_objUserPrefXML.AppendChild(objTempNode);
                }

                // Create DiaryConfig node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                    objTempNode = p_objUserPrefXML.CreateElement("DiaryConfig");
                    objParentNode.AppendChild(objTempNode);
                }
                // Create "//DiaryConfig/ShowActiveDiaryChecked" node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/ShowActiveDiaryChecked");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ShowActiveDiaryChecked");
                    objTempNode.SetAttribute("selected", "0");
                    objParentNode.AppendChild(objTempNode);
                }
                //aanandpraka2:Start changes for MITS 27074
                // Create "//DiaryConfig/ShowCActiveDiaryChecked" node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/ShowCActiveDiaryChecked");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ShowCActiveDiaryChecked");
                    objTempNode.SetAttribute("selected", "0");
                    objParentNode.AppendChild(objTempNode);
                }
                //aanandpraka2:End changes for MITS 27074
                //tkr 6/22/2007 add "//DiaryConfig/ShowNotes"
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/ShowNotes");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ShowNotes");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config - start
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/Priority");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Priority");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/Text");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Text");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/WorkActivity");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("WorkActivity");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //igupta3 jira 439
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/NotRoutable");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NotRoutable");                    
                    objTempNode.SetAttribute("selected", "0"); //rkotak:jira 4282
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/NotRollable");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NotRollable");
                    objTempNode.SetAttribute("selected", "0");//rkotak:jira 4282
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/Due");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Due");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/Claimant");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Claimant");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/ClaimStatus");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimStatus");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/Department");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Department");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/AttachedRecord");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AttachedRecord");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //Indu - Mits 33843
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/ParentRecord");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ParentRecord");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/PriorityHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("PriorityHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Priority");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/TextHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TextHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Task Name");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/WorkActivityHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("WorkActivityHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Work Activity");
                    objParentNode.AppendChild(objTempNode);
                }               
                //igupta3 jira 439
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/NotRoutableHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NotRoutableHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Routable");//rkotak:jira 4059
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/NotRollableHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NotRollableHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Rollable");//rkotak:jira 4059
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/DueHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DueHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Due");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/ClaimantHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimantHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Claimant");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/ClaimStatusHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimStatusHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Claim Status");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/DepartmentHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DepartmentHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Department");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/AttachedRecordHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AttachedRecordHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Attached Record");
                    objParentNode.AppendChild(objTempNode);
                }
                //Indu - Mits 33843
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/ParentRecordHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ParentRecordHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Parent Record");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/ColspanValue");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ColspanValue");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "0");
                    objParentNode.AppendChild(objTempNode);
                }
                //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config - end
                //SAFEWAY: paggarwal2: 04/11/2008: Added for Diary Screens - start
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/CalendarView");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CalendarView");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "0");
                    objParentNode.AppendChild(objTempNode);
                }
                //SAFEWAY: paggarwal2: 04/11/2008: Added for Diary Screens - end

                //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/UserSortOrder");

                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("UserSortOrder");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "0");
                    objParentNode.AppendChild(objTempNode);
                }
				//Deb: MITS 32961 
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/SortOrderBy");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("SortOrderBy");
                    objTempNode.SetAttribute("value", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //Deb: MITS 32961 
                //Neha End Mits 23477
                //Added by Amitosh for Terelik Grid

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/ShowRegarding");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ShowRegarding");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //End Amitosh


                //Added by Amitosh for Diary UI Change
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/AssigningUser");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AssignedUser");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //pkandhari Jira 6412 starts
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/AssignedGroup");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AssignedGroup");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //pkandhari Jira 6412 ends
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/AssigningUser");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AssigningUser");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/AssignedUserHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AssignedUserHeader");
                    objTempNode.SetAttribute("value", "Assigned User");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/AssigningUserHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AssigningUserHeader");
                    objTempNode.SetAttribute("value", "Assigning User");
                    objParentNode.AppendChild(objTempNode);
                }
                //MITS 36519
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig/SortExpression");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("SortExpression");
                    objTempNode.SetAttribute("value", "Sort Expression");
                    objParentNode.AppendChild(objTempNode);
                }
                //End Amitosh


            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.CreateUserPreferXML.Err", m_iClientId), p_objException);
            }
            finally
            {
                objTempNode = null;
                objParentNode = null;
            }
        }

        // End REM Umesh
        #region Get Mail id
        /// <summary>
        /// Gets the e-mail id
        /// </summary>
        /// <param name="p_sLogInName">Log-in name of the user</param>
        /// <returns>E-mail id of the user</returns>
        private string GetMailId(string p_sLogInName)
        {
            string sRetVal = "";
            string sSQL = "";
            DataSet objDS = null;

            try
            {
                sSQL = "SELECT USER_TABLE.EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.LOGIN_NAME='" + p_sLogInName + "'";
                objDS = DbFactory.GetDataSet(m_sSecConnectString, sSQL, m_iClientId);
                sRetVal = Conversion.ConvertObjToStr(objDS.Tables[0].Rows[0]["EMAIL_ADDR"]);
                ////Added by Amitosh for MITS 24229 (03/04/2011)
                if (string.IsNullOrEmpty(sRetVal))
                {
                    throw new RMAppException(Globalization.GetString("WPA.GetMailId.Error", m_iClientId));
                }

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            //end Amitosh
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetMailId.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objDS != null) objDS.Dispose();
            }
            return (sRetVal);
        }
        #endregion

        #region Delete Diary
        /// <summary>
        /// Delete Diary
        /// </summary>
        /// <param name="p_lDiaryId">Diary Id</param>
        public void DeleteDiary(long p_lDiaryId)
        {
            string sSQL = "";
            DbConnection objConn = null;

            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnectionString);

                if (p_lDiaryId == 0)
                    return;

                sSQL = "UPDATE WPA_DIARY_ENTRY SET DIARY_DELETED=1 WHERE ENTRY_ID= " + p_lDiaryId;

                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.DeleteDiary.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
            }
        }
        #endregion

        #region GetWpaTaskList
        /// <summary>
        /// Tom Regan, MITS 9398.  Get list of Wpa Tasks (codes) when creating a diary.
        /// </summary>
        /// <param name="connectionString">string containing the database connection string</param>
        /// <param name="doc">Node "WpaTaskList" is appended to this node</param>
        /// <returns><WpaTaskList><WpaTask code_id="[n]" code_desc="[a code description]"/></WpaTaskList></returns>
        public void GetWpaTaskList(UserLogin objUserLogin, XmlDocument doc)
        {
            XmlElement taskList = doc.CreateElement("WpaTaskList");
            doc.ChildNodes[0].AppendChild(taskList as XmlNode);
            string SQL = string.Empty;
            string connectionString = objUserLogin.objRiskmasterDatabase.ConnectionString;
            int iLangCode = 0;
            string sTableName = string.Empty;            
            //Aman ml change
            iLangCode = objUserLogin.objUser.NlsCode;
            sTableName = "WPA_TASKS";
            //caggarwal4 RMA-10486
            string sTodayDate = Conversion.ToDbDate(DateTime.Now);
            // Added by Nitin on 20-Feb-2009 to R5-R4 merging of mits 12701 
            //MITS 12701 : Umesh
            //Aman ML change 
            //if (DbFactory.IsOracleDatabase(connectionString))
            //{
            //    SQL = "SELECT c.CODE_ID, c.SHORT_CODE || ' - ' || ct.CODE_DESC FROM CODES c INNER JOIN CODES_TEXT ct ON c.CODE_ID = ct.CODE_ID INNER JOIN GLOSSARY g ON c.TABLE_ID = g.TABLE_ID WHERE g.SYSTEM_TABLE_NAME = 'WPA_TASKS' AND (c.DELETED_FLAG = 0 OR c.DELETED_FLAG IS NULL) AND ct.LANGUAGE_CODE = 1033";
            //}
            //else
            //{

            //    SQL = "SELECT c.CODE_ID, c.SHORT_CODE + ' - ' + ct.CODE_DESC FROM CODES c INNER JOIN CODES_TEXT ct ON c.CODE_ID = ct.CODE_ID INNER JOIN GLOSSARY g ON c.TABLE_ID = g.TABLE_ID WHERE g.SYSTEM_TABLE_NAME = 'WPA_TASKS' AND (c.DELETED_FLAG = 0 OR c.DELETED_FLAG IS NULL)  AND ct.LANGUAGE_CODE = 1033";
            //}
            if (DbFactory.IsOracleDatabase(connectionString))
            {
                //bharani - MITS : 32701 - Change 1 - Start

                //SQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE || ' - ' || CODES_TEXT.CODE_DESC FROM CODES INNER JOIN CODES_TEXT ON CODES.CODE_ID = CODES_TEXT.CODE_ID INNER JOIN GLOSSARY g ON CODES.TABLE_ID = g.TABLE_ID WHERE g.SYSTEM_TABLE_NAME = 'WPA_TASKS' AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES_TEXT.LANGUAGE_CODE = 1033";
                //caggarwal4 RMA-10486
                SQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE || ' - ' || CODES_TEXT.CODE_DESC AS CodeDescription FROM CODES INNER JOIN CODES_TEXT ON CODES.CODE_ID = CODES_TEXT.CODE_ID INNER JOIN GLOSSARY g ON CODES.TABLE_ID = g.TABLE_ID WHERE g.SYSTEM_TABLE_NAME = 'WPA_TASKS' AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES_TEXT.LANGUAGE_CODE = 1033 AND  ((NVL(CODES.EFF_START_DATE,'" + sTodayDate + "') <='" + sTodayDate + "')   AND (NVL(CODES.EFF_END_DATE,'" + sTodayDate + "')>='" + sTodayDate + "'))";

                //bharani - MITS : 32701 - Change 1 - End
            }
            else
            {
				//bharani - MITS : 32701 - Change 2 - Start
               //SQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE + ' - ' + CODES_TEXT.CODE_DESC FROM CODES INNER JOIN CODES_TEXT ON CODES.CODE_ID = CODES_TEXT.CODE_ID INNER JOIN GLOSSARY g ON CODES.TABLE_ID = g.TABLE_ID WHERE g.SYSTEM_TABLE_NAME = 'WPA_TASKS' AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)  AND CODES_TEXT.LANGUAGE_CODE = 1033";
                //caggarwal4 RMA-10486
                SQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE + ' - ' + CODES_TEXT.CODE_DESC AS CodeDescription FROM CODES INNER JOIN CODES_TEXT ON CODES.CODE_ID = CODES_TEXT.CODE_ID INNER JOIN GLOSSARY g ON CODES.TABLE_ID = g.TABLE_ID WHERE g.SYSTEM_TABLE_NAME = 'WPA_TASKS' AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)  AND CODES_TEXT.LANGUAGE_CODE = 1033 AND ((ISNULL(CODES.EFF_START_DATE,'" + sTodayDate + "') <='" + sTodayDate + "')   AND (ISNULL(CODES.EFF_END_DATE,'" + sTodayDate + "')>='" + sTodayDate + "'))";

                //bharani - MITS : 32701 - Change 2 - End
            }
            StringBuilder sbSQL = new StringBuilder();
            sbSQL = CommonFunctions.PrepareMultilingualQuery(SQL.ToString(), sTableName, iLangCode);
			sbSQL.Append(" ORDER BY CodeDescription");//bharani - MITS : 32701 - Change 3
            //Aman ML change 
            using (DbReader rdr = DbFactory.GetDbReader(connectionString, sbSQL.ToString()))  
            {
                while (rdr.Read())
                {
                    XmlElement element = doc.CreateElement("WpaTask");
                    element.SetAttribute("code_id", rdr.GetValue(0).ToString());
                    element.SetAttribute("code_desc", rdr.GetValue(1).ToString());
                    taskList.AppendChild(element);
                }//while
            }//using


        }
        #endregion

        #region Get Users
        /// Name			: GetUsers
        /// Author			: Rahul SHarma
        /// Date Created	: 12-May-2006
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// WPA.GetUsers of Application layer that Loads the users details 
        /// that are allowed to login to a current datasource.
        /// </summary>
        ///
        /// <param name="p_sCurrentDSN">An current DSN.</param>
        /// <param name="p_objXmlDocOut">An current DSN.</param>
        /// <param name="iPSID">Security ID.</param>
        /// Structure of the output XML is:
        ///		<user id="" firstname="" lastname="" username="" email=""/>
        /// <returns>A boolean values that represents the success/failure.</returns>
        ///	<returns>A XML with user details.</returns>	
        public bool GetUsers(string p_sCurrentDSN, int iPSID, out string p_objXmlDocOut, UserLogin objUserLogin)
        {
            bool bReturnValue = false;
            bool bOrgSec = false;
            string sGroupIds = string.Empty;
            ArrayList sUserOrgSec = new ArrayList();
            XmlAttribute objAttribute = null;
            XmlNode objXmlNode = null;
            string sSQL = string.Empty;
            XmlDocument objXmlDocOut = null;
            p_objXmlDocOut = string.Empty;

            try
            {
                objXmlDocOut = new XmlDocument();
                objXmlNode = objXmlDocOut.CreateElement("data");
                objAttribute = objXmlDocOut.CreateAttribute("", "psid", "");
                objAttribute.InnerText = iPSID.ToString();
                objXmlNode.Attributes.Append(objAttribute);
                objXmlDocOut.AppendChild(objXmlNode);

                bOrgSec = objUserLogin.objRiskmasterDatabase.OrgSecFlag;
                if (bOrgSec == false)
                {
                    sSQL = "SELECT USER_DETAILS_TABLE.LOGIN_NAME, USER_TABLE.USER_ID, USER_TABLE.FIRST_NAME, USER_TABLE.LAST_NAME, USER_TABLE.EMAIL_ADDR " +
                        "FROM USER_DETAILS_TABLE INNER JOIN " +
                        "USER_TABLE ON USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID " +
                        "WHERE (USER_DETAILS_TABLE.DSNID = " + objUserLogin.DatabaseId + ") " +
                        "ORDER BY USER_TABLE.FIRST_NAME, USER_TABLE.LAST_NAME";
                    GetUserInfo(sSQL, ref objXmlDocOut);
                }
                else
                {
                    //get all user id values within the same BES group. Oracle cannot handle too long sql command, so we
                    //may need to split it to multiple sql command
                    StringBuilder sbOrgSecUserIDs = new StringBuilder();
                    string sSQL_OrgSecUser = "SELECT USER_ID FROM ORG_SECURITY_USER WHERE GROUP_ID IN (SELECT GROUP_ID FROM ORG_SECURITY_USER WHERE USER_ID=" + objUserLogin.UserId + ")";
                    using (DbReader objReader = DbFactory.GetDbReader(objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL_OrgSecUser))
                    {
                        while (objReader.Read())
                        {
                            if (sbOrgSecUserIDs.Length > 0)
                                sbOrgSecUserIDs.Append(",");
                            sbOrgSecUserIDs.Append(objReader.GetValue("USER_ID"));

                            if (sbOrgSecUserIDs.Length > 2000)
                            {
                                sSQL = "SELECT USER_DETAILS_TABLE.LOGIN_NAME, USER_TABLE.USER_ID, USER_TABLE.FIRST_NAME, USER_TABLE.LAST_NAME, USER_TABLE.EMAIL_ADDR " +
                                    "FROM USER_DETAILS_TABLE INNER JOIN " +
                                    "USER_TABLE ON USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID " +
                                    "WHERE (USER_DETAILS_TABLE.DSNID = " + objUserLogin.DatabaseId + ") AND " +
                                    " (USER_TABLE.USER_ID IN (" + sbOrgSecUserIDs.ToString() + ")) " +
                                    "ORDER BY USER_TABLE.FIRST_NAME, USER_TABLE.LAST_NAME";
                                GetUserInfo(sSQL, ref objXmlDocOut);
                                sbOrgSecUserIDs.Length = 0;
                            }
                        }

                        if (sbOrgSecUserIDs.Length > 0)
                        {
                            sSQL = "SELECT USER_DETAILS_TABLE.LOGIN_NAME, USER_TABLE.USER_ID, USER_TABLE.FIRST_NAME, USER_TABLE.LAST_NAME, USER_TABLE.EMAIL_ADDR " +
                                "FROM USER_DETAILS_TABLE INNER JOIN " +
                                "USER_TABLE ON USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID " +
                                "WHERE (USER_DETAILS_TABLE.DSNID = " + objUserLogin.DatabaseId + ") AND " +
                                " (USER_TABLE.USER_ID IN (" + sbOrgSecUserIDs.ToString() + ")) " +
                                "ORDER BY USER_TABLE.FIRST_NAME, USER_TABLE.LAST_NAME";
                            GetUserInfo(sSQL, ref objXmlDocOut);
                            sbOrgSecUserIDs.Length = 0;
                        }
                    }
                }

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetUsers.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetUsers.Exception", m_iClientId), p_objException);
            }
            finally
            {
            }

            p_objXmlDocOut = objXmlDocOut.OuterXml;
            return bReturnValue;
        }

        /// <summary>
        /// create the xml element for all related users
        /// </summary>
        /// <param name="sSQL">SQL command used to retrieve the user information</param>
        /// <param name="objXmlDocOut">output XML document</param>
        private void GetUserInfo(string sSQL, ref XmlDocument objXmlDocOut)
        {
            XmlNode objXmlNode = objXmlDocOut.SelectSingleNode("//data");
            using (DbReader objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), sSQL))
            {
                while (objReader.Read())
                {
                    XmlElement objNewElement = objXmlDocOut.CreateElement("user");
                    objNewElement.SetAttribute("id", objReader.GetValue("USER_ID").ToString());
                    objNewElement.SetAttribute("username", objReader.GetValue("LOGIN_NAME").ToString());
                    objNewElement.SetAttribute("firstname", objReader.GetValue("FIRST_NAME").ToString());
                    objNewElement.SetAttribute("lastname", objReader.GetValue("LAST_NAME").ToString());
                    objNewElement.SetAttribute("email", objReader.GetValue("EMAIL_ADDR").ToString());
                    objXmlNode.AppendChild(objNewElement);
                }
            }
        }

        #endregion

        //Added by Nitin on 28-Feb-2009 for Mergring Mits 10641 with R5
        #region  For Printing Active Diaries START 03/08/2008 Asif MITS:10641
        /// Name: SetFontBold
        /// Date Created: 08/01/2005	
        ///************************************************************
        /// Amendment History
        ///***************************************************
        /// Date Amended   *   Amendment   *    Author
        /// <summary>
        /// Set font bold
        /// </summary>
        /// <param name="p_bBold">Bool flag</param>
        private void SetFontBold(bool p_bBold)
        {
            try
            {
                if (p_bBold)
                    m_objFont = new Font(m_objFont.Name, m_objFont.Size, FontStyle.Bold);
                else
                    m_objFont = new Font(m_objFont.Name, m_objFont.Size);

                m_objText.Style.Font = m_objFont;

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error has occurred while setting the fonts bold property.", p_objEx);
            }

        }
        /// Name		: GetTextWidth
        /// Date Created: 08/01/2005	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get text width.
        /// </summary>
        /// <param name="p_sText">String Text</param>
        /// <returns>Width</returns>
        private double GetTextWidth(string p_sText)
        {
            try
            {
                m_objText.Text = p_sText + "";
                return (m_objText.BoundWidth);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetTextWidth.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: GetTextHeight
        ///************************************************************
        /// Amendment History
        ///***************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get Text Height
        /// </summary>
        /// <param name="p_sText">String Text</param>
        /// <returns>Height</returns>
        private double GetTextHeight(string p_sText)
        {
            try
            {
                m_objText.Text = p_sText + "";
                return (m_objText.BoundHeight);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetTextHeight.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: PrintText
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Render Text on the document.
        /// </summary>
        /// <param name="p_sPrintText">String text to print</param>
        private void PrintText(string p_sPrintText)
        {
            try
            {
                m_objText.Text = p_sPrintText + "\r";
                m_objText.AutoWidth = true;
                m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WPA.PrintText.Error", m_iClientId), p_objEx);
            }
        }
        /// Name		: PrintTextNoCr
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// <summary>
        /// Render Text on the document with no "\r" on the end.
        /// </summary>
        /// <param name="p_sPrintText">String text to print</param>
        private void PrintTextNoCr(string p_sPrintText)
        {
            try
            {
                m_objText.Text = p_sPrintText + "";
                m_objText.AutoWidth = true;
                m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WPA.PrintText.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: Save
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Save the C1PrintDocument in PDF format.
        /// </summary>
        /// <param name="p_sPath">Path</param>
        private void Save(string p_sPath)
        {
            try
            {
                m_objPrintDoc.ExportToPDF(p_sPath, false);
            }
            catch (Exception p_objEx)
            {

                throw new RMAppException(Globalization.GetString("WPA.Save.Error", m_iClientId), p_objEx);
            }
        }
        /// Name		: EndDoc
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Call the end event.
        /// </summary>
        private void EndDoc()
        {
            try
            {
                m_objPrintDoc.EndDoc();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WPA.EndDoc.Error", m_iClientId), p_objEx);

            }
        }

        /// Name		: PrintHeader
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Prints report header.
        /// </summary>
        /// <param name="p_dblCurrentY">Current Y</param>
        /// <param name="p_iColumnCount">Columns Count</param>
        /// <param name="p_dblPageWidth">Page Width</param>
        /// <param name="p_dblLeftMargin">Left Margin</param>
        /// <param name="p_dblLineHeight">Line Height</param>
        /// <param name="p_arrCol">Columns Text</param>
        /// <param name="p_arrColWidth">Columns Width</param>
        /// <param name="p_arrColAlign">Columns Alignment.</param>
        private void PrintHeader(ref double p_dblCurrentY, int p_iColumnCount, double p_dblPageWidth,
            double p_dblLeftMargin, double p_dblLineHeight, string[] p_arrCol,
            double[] p_arrColWidth, string[] p_arrColAlign)
        {
            double dblCurrentX = 0.0;
            int iIndex = 0;

            try
            {
                dblCurrentX = p_dblLeftMargin;
                for (iIndex = 0; iIndex < p_iColumnCount; iIndex++)
                {
                    CurrentY = p_dblCurrentY;
                    if (p_arrColAlign[iIndex] == LEFT_ALIGN)
                    {
                        CurrentX = dblCurrentX;
                        PrintText(p_arrCol[iIndex]);
                    }
                    else
                    {
                        CurrentX = dblCurrentX + p_arrColWidth[iIndex]
                            - GetTextWidth(p_arrCol[iIndex]);
                        PrintText(p_arrCol[iIndex]);
                    }

                    dblCurrentX += p_arrColWidth[iIndex];
                }
                p_dblCurrentY += p_dblLineHeight * 2;

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.PrintHeader.Error", m_iClientId), p_objException);

            }
        }
        /// Name		: CreateRow
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Creates a record in a report
        /// </summary>
        /// <param name="p_objData">DataRow object containing a row data</param>
        /// <param name="p_dblCurrentX">Current X position</param>
        /// <param name="p_dblCurrentY">Current Y position</param>
        /// <param name="p_arrColAlign">Columns alignment</param>
        /// <param name="p_dblLeftMargin">Left margin</param>
        /// <param name="p_dblLineHeight">Line height</param>
        /// <param name="p_arrColWidth">Column width</param>
        /// <param name="p_dblCharWidth">Character width</param>

        private void CreateRow(DataRow p_objData, double p_dblCurrentX,
            ref double p_dblCurrentY, string[] p_arrColAlign, double p_dblLeftMargin, double p_dblLineHeight,
            double[] p_arrColWidth, double p_dblCharWidth)
        {
            string sData = "";
            string sCheckStatusCode = string.Empty;
            string sCheckStatusDesc = string.Empty;

            try
            {
                CurrentX = p_dblCurrentX;
                CurrentY = p_dblCurrentY;
                for (int iIndexJ = 0; iIndexJ < m_intColumns; iIndexJ++)
                {
                    switch (iIndexJ)
                    {

                        case 0:
                            sData = Conversion.ConvertObjToStr(p_objData["DUE DATE"]);
                            break;
                        case 1:
                            sData = Conversion.ConvertObjToStr(p_objData["PRIORITY"]);
                            break;
                        case 2:
                            sData = Conversion.ConvertObjToStr(p_objData["ACTIVITIES"]);
                            break;
                        case 3:
                            sData = Conversion.ConvertObjToStr(p_objData["ATTACHED_RECORD"]); 
                            break;
                        case 4:
                            sData = Conversion.ConvertObjToStr(p_objData["TASK_DESCRIPTION"]);
                            break;
                        case 5:
                            sData = Conversion.ConvertObjToStr(p_objData["CLAIMANT"]);
                            break;
                        case 6:
                            sData = Conversion.ConvertObjToStr(p_objData["DEPARTMENT"]);
                            break;

                    }
                    if (sData == "")
                        sData = " ";

                    while (GetTextWidth(sData) > p_arrColWidth[iIndexJ])
                    {
                        // sData = sData.Substring(0, sData.Length - 1);
                        sData = sData.Substring(0, sData.Length - 3);
                        sData = sData + "..";

                    }
                    // Print using alignment
                    if (p_arrColAlign[iIndexJ] == LEFT_ALIGN)
                    {
                        CurrentX = p_dblCurrentX;
                        PrintTextNoCr(" " + sData);
                    }
                    else
                    {
                        CurrentX = p_dblCurrentX + p_arrColWidth[iIndexJ]
                            - GetTextWidth(sData) - p_dblCharWidth;
                        PrintTextNoCr(sData);
                    }
                    p_dblCurrentX += p_arrColWidth[iIndexJ];
                }

                p_dblCurrentX = p_dblLeftMargin;
                p_dblCurrentY += p_dblLineHeight;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.CreateRow.Error", m_iClientId), p_objException);
            }
        }
        //asingh263 MITS 34874 starts
        private void CreateRowDetails(DataRow p_objData, double p_dblCurrentX,
           ref double p_dblCurrentY, string[] p_arrColAlign, double p_dblLeftMargin, double p_dblLineHeight,
           double[] p_arrColWidth, double p_dblCharWidth)
        {
            string sData = "";
            string sCheckStatusCode = string.Empty;
            string sCheckStatusDesc = string.Empty;

            try
            {
                CurrentX = p_dblCurrentX;
                CurrentY = p_dblCurrentY;
                for (int iIndexJ = 0; iIndexJ < 8; iIndexJ++)
                {
                    switch (iIndexJ)
                    {
                        case 0:
                            sData = Conversion.ConvertObjToStr(p_objData["taskname"]);
                            break;
                        case 1:
                            sData = Conversion.ConvertObjToStr(p_objData["createdon"]);
                            break;
                        case 2:
                            sData = Conversion.ConvertObjToStr(p_objData["duedate"]);
                            break;
                        case 3:
                            sData = Conversion.ConvertObjToStr(p_objData["workactivity"]);
                            break;
                        case 4:
                            sData = Conversion.ConvertObjToStr(p_objData["estimatetime"]);
                            break;
                        case 5:
                            sData = Conversion.ConvertObjToStr(p_objData["priority"]);
                            break;
                        case 6:
                            sData = Conversion.ConvertObjToStr(p_objData["notes"]);
                            break;
                        case 7:
                            sData = Conversion.ConvertObjToStr(p_objData["regarding"]);
                            break;

                    }
                    if (sData == "")
                        sData = " ";

                    //while (GetTextWidth(sData) > p_arrColWidth[iIndexJ])
                    //{
                    //    sData = sData.Substring(0, sData.Length - 3);
                    //    sData = sData + "..";

                    //}
                    if (p_arrColAlign[iIndexJ] == LEFT_ALIGN)
                    {
                        CurrentX = p_dblCurrentX;
                        PrintTextNoCr(" " + sData);
                    }
                    else
                    {
                        CurrentX = p_dblCurrentX + p_arrColWidth[iIndexJ]
                            - GetTextWidth(sData) - p_dblCharWidth;
                        PrintTextNoCr(sData);
                    }
                    p_dblCurrentX += p_arrColWidth[iIndexJ];
                }

                p_dblCurrentX = p_dblLeftMargin;
                p_dblCurrentY += p_dblLineHeight;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.CreateRow.Error", m_iClientId), p_objException);
            }
        }
        //asingh263 MITS 34874 ends
        /// Name		: CreateFooter
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Creates Report Footer
        /// </summary>
        /// <param name="p_dCurrentX">X Position</param>
        /// <param name="p_dCurrentY">Y Position</param>
        /// <param name="p_dHeight">Height</param>
        /// <param name="p_dblSpace">Space between 2 words</param>
        private void CreateFooter(double p_dCurrentX, double p_dCurrentY, double p_dHeight, double p_dblSpace)
        {

            m_objText.Style.Font = new Font(CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.InitializeReport.FontName", m_iClientId), this.m_iLangCode.ToString()), 7f, FontStyle.Underline | FontStyle.Bold);
                m_objText.Text = m_objConfig["DEF_NOTE"].ToString();
                p_dCurrentY = p_dCurrentY + p_dHeight;
                m_objPrintDoc.RenderDirect(p_dCurrentX, p_dCurrentY, m_objText);

                //m_objText.Style.Font = new Font("Microsoft YaHei", 7f, FontStyle.Bold); MITS 34392 hlv MITS 34392 hlv
                m_objText.Style.Font = new Font(CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.InitializeReport.FontName", m_iClientId), this.m_iLangCode.ToString()), 7f, FontStyle.Bold);
                p_dCurrentY = p_dCurrentY + p_dHeight;
                m_objText.Text = m_objConfig["DEF_RMPROPRIETARY"].ToString();
                m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

                p_dCurrentY = p_dCurrentY + p_dHeight;
                m_objText.Text = m_objConfig["DEF_NAME"].ToString();
                m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

                p_dCurrentY = p_dCurrentY + p_dHeight;
                m_objText.Text = m_objConfig["DEF_RMCOPYRIGHT"].ToString();
                m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

                p_dCurrentY = p_dCurrentY + p_dHeight;
                m_objText.Text = m_objConfig["DEF_RMRIGHTSRESERVED"].ToString();
                m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);
           
        }

        /// Name		: StartDoc
        /// Date Created: 08/01/2005	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************

        /// <summary>
        /// Initialize the document
        /// </summary>
        /// <param name="p_bIsLandscape">Landscape or Portrait</param>
        /// <param name="p_Left">Left</param>
        /// <param name="p_Right">Right</param>
        /// <param name="p_Top">Top</param>
        /// <param name="p_Bottom">Bottom</param>
        private void StartDoc(bool p_bIsLandscape, int p_Left, int p_Right, int p_Top, int p_Bottom)
        {
            try
            {
                m_objPrintDoc = new C1PrintDocument();
                m_objPrintDoc.DefaultUnit = UnitTypeEnum.Twip;

                m_objPrintDoc.PageSettings.Landscape = p_bIsLandscape;
                m_objPrintDoc.PageSettings.Margins.Left = p_Left;
                m_objPrintDoc.PageSettings.Margins.Right = p_Right;
                m_objPrintDoc.PageSettings.Margins.Top = p_Top;
                m_objPrintDoc.PageSettings.Margins.Bottom = p_Bottom;

                m_objPrintDoc.StartDoc();

                // Causes access to default printer     
                m_dblPageHeight = 11 * 1440;
                m_dblPageWidth = 8.5 * 1440;
                if (p_bIsLandscape)
                    m_dblPageHeight += 700;
                else
                    m_dblPageWidth += 700;
                //Mits 14020 Asif Start
                // m_objFont = new Font("Arial", 7); //Microsoft YaHei
                //m_objFont = new Font("Microsoft YaHei", 10); MITS MITS 34392 hlv
                m_objFont = new Font(CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.InitializeReport.FontName", m_iClientId), this.m_iLangCode.ToString()), 10);
                //Mits 14020 Asif End
                m_objText = new RenderText(m_objPrintDoc);
                m_objText.Style.Font = m_objFont;
                m_objText.Style.WordWrap = false;
                m_objText.AutoWidth = true;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WPA.StartDoc.Error", m_iClientId), p_objEx);
            }
        }
        /// <summary>
        /// Creates new page,defines header,footer of the page
        /// </summary>
        /// <param name="p_sDSN">credentials required for connecting database</param>
        /// <param name="p_bFirstPage"></param>
        /// <param name="p_strSearchFor">form name</param>
        private void CreateNewPage(string p_sDSN, bool p_bFirstPage, string p_strSearchFor)
        {
            SysParms objSysSetting = null;

            RenderText m_objText = null;
            try
            {
                if (!p_bFirstPage)
                {
                    m_objPrintDoc.NewPage();
                }
                m_objPrintDoc.RenderDirectRectangle(0, 0, 15600, 11700);

                m_objPrintDoc.RenderDirectRectangle(0, 0, 15600, 500, Color.Black, 1, Color.LightGray);
                m_objPrintDoc.RenderDirectRectangle(0, 11300, 15600, 11900, Color.Black, 1, Color.LightGray);
                m_objText = new RenderText(m_objPrintDoc);
                m_objText.Text = " ";
                m_objText.AutoWidth = true;
                CurrentX = m_objText.BoundWidth;


                //m_objText.Text =p_strSearchFor;
                //m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.Width, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Left);
                //m_objText = new RenderText(m_objPrintDoc);

                string lsFontName = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.InitializeReport.FontName", m_iClientId), this.m_iLangCode.ToString()); //MITS 34392 hlv 

                objSysSetting = new SysParms(p_sDSN, m_iClientId);
                // m_objText.Text = objSysSetting.SysSettings.ClientName + "  " + Conversion.GetDBDateFormat(Conversion.ToDbDate(DateTime.Now), "d") + "  ";
                m_objText.Text = p_strSearchFor;
                CurrentX = 50;
                //m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.BoundWidth, new Font("Microsoft YaHei", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Left); //MITS 34392 hlv 
                m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.BoundWidth, new Font(lsFontName, 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Left); //MITS 34392 hlv 
                m_objText = new RenderText(m_objPrintDoc);

                m_objText.Text = objSysSetting.SysSettings.ClientName;
                CurrentX = (ClientWidth - m_objText.BoundWidth) / 2;
                //m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.BoundWidth, new Font("Microsoft YaHei", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Center); //MITS 34392 hlv 
                m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.BoundWidth, new Font(lsFontName, 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Center); //MITS 34392 hlv 
                m_objText = new RenderText(m_objPrintDoc);
                objSysSetting = null;

                m_objText.Text = Conversion.GetUIDate(Conversion.ToDbDate(DateTime.Now), this.m_iLangCode.ToString(),m_iClientId);
                CurrentX = (ClientWidth - m_objText.BoundWidth);
                //m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.BoundWidth, new Font("Microsoft YaHei", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Right); //MITS 34392 hlv 
                m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.BoundWidth, new Font(lsFontName, 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Right); //MITS 34392 hlv 
                m_objText = new RenderText(m_objPrintDoc);

                //m_objText.Text = "Page" + " [@@PageNo@@]";
                m_objText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.CreateReport.Page", m_iClientId), this.m_iLangCode.ToString()) + " [@@PageNo@@]";
                CurrentX = (PageWidth - m_objText.BoundWidth) / 2;
                //m_objPrintDoc.RenderDirectText(CurrentX, 11600, m_objText.Text, m_objText.BoundWidth, new Font("Microsoft YaHei", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Center); //MITS 34392 hlv 
                m_objPrintDoc.RenderDirectText(CurrentX, 11600, m_objText.Text, m_objText.BoundWidth, new Font(lsFontName, 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Center); //MITS 34392 hlv 

                //m_objText.Text = String.Format("Confidential Data - {0}  ", m_objConfig["DEF_RMCAPTION"]);
                m_objText.Text = String.Format(CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.CreateReport.ConfidentialData", m_iClientId), this.m_iLangCode.ToString()) + " - {0}  ", m_objConfig["DEF_RMCAPTION"]);
                CurrentX = (ClientWidth - m_objText.BoundWidth);
                //m_objPrintDoc.RenderDirectText(CurrentX, 11600, m_objText.Text, m_objText.BoundWidth, new Font("Microsoft YaHei", 11, FontStyle.Bold | FontStyle.Italic), Color.Black, AlignHorzEnum.Right); //MITS 34392 hlv 
                m_objPrintDoc.RenderDirectText(CurrentX, 11600, m_objText.Text, m_objText.BoundWidth, new Font(lsFontName, 11, FontStyle.Bold | FontStyle.Italic), Color.Black, AlignHorzEnum.Right); //MITS 34392 hlv 
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WPA.CreateNewPage.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objSysSetting = null;
                m_objText = null;
            }
        }

        /// <summary>
        /// Stores the data in memory stream based upon the input xml.
        /// </summary>
        /// <param name="xmlUserData">The input xml</param>
        /// <param name="strFormName">Form name which will be dispayed in the header of the report</param>
        /// <returns>returns memorystream object to wpaAdaptor.cs </returns>
        public MemoryStream PrintActiveDiaries(XmlDocument xmlUserData, string strFormName, string sCurrentPageId)
        {
            MemoryStream objMemory = null;
            try
            {
                //CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckManager.PrintCheckCore.VOID"), p_sLangCode)
                //strFormName = "Active Diaries for " + strFormName; MITS 34392 hlv
                strFormName = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("ActiveDiariesFor", 0, sCurrentPageId, m_iClientId), this.m_iLangCode.ToString()) + strFormName;
                CreateActiveDiariesReport(xmlUserData.InnerXml, out objMemory, strFormName, sCurrentPageId);
                return objMemory;
            }

            catch (XmlOperationException p_objException)
            {
                throw p_objException;
            }

            catch (FileInputOutputException p_objException)
            {
                throw p_objException;
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (CollectionException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.PrintActiveDiaries.Error", m_iClientId), p_objException);
            }
            finally
            {
                objMemory = null;
            }
        }


        /// <summary>
        /// Creates report based on passed XML document
        /// </summary>
        /// <param name="p_objXmlDoc">The input XML document</param>
        ///   <param name="p_strSearchFor">The input Form Name</param>
        ///   <param name="p_objMemory">output Memory Object</param>
        /// <returns>Success: 1 or Failure: 0</returns>
        public int CreateActiveDiariesReport(string p_objXmlDoc, out MemoryStream p_objMemory, string p_strSearchFor,string sCurrentPageId)
        {
            DataSet objDS = null;
            double dblCharWidth = 0.0;
            double dblLineHeight = 0.0;
            double dblLeftMargin = 8.0;
            double dblRightMargin = 8.0;
            double dblTopMargin = 8.0;
            double dblPageWidth = 0.0;
            double dblPageHeight = 0.0;
            double dblBottomHeader = 0.0;
            double dblCurrentY = 0.0;
            double dblCurrentX = 0.0;
            double iCounter = 0.0;
            double[] arrColWidth = null;
            string sHeader = string.Empty;
            int iReturnValue = 0;
            string sFile = string.Empty;
            string sSQL = string.Empty;
            string[] arrColAlign = null;
            string[] arrCol = null;
            StringReader objReader = null;

            p_objMemory = null;

            try
            {
                arrColAlign = new string[7];
                arrColWidth = new double[7];
                arrCol = new string[7];
                objReader = new StringReader(p_objXmlDoc);
                objDS = new DataSet();
                objDS.ReadXml(objReader);
                DataTable objDT = new DataTable("Dummy");
                if (objDS.Tables.Count > 1)
                {

                    DataColumn objdc1 = new DataColumn("DUE DATE");
                    DataColumn objdc2 = new DataColumn("PRIORITY");
                    DataColumn objdc3 = new DataColumn("ACTIVITIES");
                    DataColumn objdc4 = new DataColumn("ATTACHED_RECORD");      
                    DataColumn objdc5 = new DataColumn("TASK_DESCRIPTION");
                    DataColumn objdc6 = new DataColumn("CLAIMANT");
                    DataColumn objdc7 = new DataColumn("DEPARTMENT");


                    objDT.Columns.Add(objdc1);
                    objDT.Columns.Add(objdc2);
                    objDT.Columns.Add(objdc3);
                    objDT.Columns.Add(objdc4);
                    objDT.Columns.Add(objdc5);
                    objDT.Columns.Add(objdc6);
                    objDT.Columns.Add(objdc7);

                    for (int i = 0; i < objDS.Tables[1].Rows.Count; i++)
                    {
                        DataRow dr = objDT.NewRow();
                        dr[0] = objDS.Tables[1].Rows[i]["Due_Date"].ToString();//DueDate
                        dr[1] = objDS.Tables[1].Rows[i]["Priority"].ToString();//Priority
                        dr[2] = objDS.Tables[2].Rows[i][0].ToString();//Activities
                        dr[3] = objDS.Tables[1].Rows[i]["AttachedRecord"].ToString();//AttatchedRecrd
                        dr[4] = objDS.Tables[1].Rows[i]["Task_Description"].ToString();//TaskDescription
                        if (objDS.Tables[1].Columns.Contains("claimantname"))
                        {
                            dr[5] = objDS.Tables[1].Rows[i]["claimantname"].ToString();//Claimant
                        }
                        else
                        {
                            dr[5] = "";
                        }
                        dr[6] = objDS.Tables[1].Rows[i]["departmentname"].ToString();//Department

                        objDT.Rows.Add(dr);

                    }
                }
                StartDoc(true, 10, 10, 10, 10);
                SetFontBold(true);
                ClientWidth = 15400;
                dblCharWidth = GetTextWidth("0123456789/$,.ABCDEFGHIJKLMNOPQRSTUVWXYZ") / 40;
                dblLineHeight = GetTextHeight("W");
                dblLeftMargin = dblCharWidth * 2;
                dblRightMargin = dblCharWidth * 2;
                dblTopMargin = dblLineHeight * 5;
                dblPageWidth = PageWidth - dblLeftMargin - dblRightMargin;
                dblPageHeight = PageHeight;
                dblBottomHeader = dblPageHeight - (5 * dblLineHeight);

                //Mits 14020:Asif Start
                //Setting Column width
                //arrColWidth[0] = dblCharWidth * 15;// First Column
                //arrColWidth[1] = dblCharWidth * 15;// 2nd  Column
                //arrColWidth[2] = dblCharWidth * 35;// 3rd Column
                //arrColWidth[3] = dblCharWidth * 30;// 4th Column
                //arrColWidth[4] = dblCharWidth * 34;// 5th Column
                //arrColWidth[5] = dblCharWidth * 30;// 6th Column
                //arrColWidth[6] = dblCharWidth * 20;// 7th Column


                arrColWidth[0] = dblCharWidth * 10;// First Column
                arrColWidth[1] = dblCharWidth * 10;// 2nd  Column
                arrColWidth[2] = dblCharWidth * 15;// 3rd Column
                arrColWidth[3] = dblCharWidth * 28;// 4th Column
                arrColWidth[4] = dblCharWidth * 22;// 5th Column
                arrColWidth[5] = dblCharWidth * 20;// 6th Column
                arrColWidth[6] = dblCharWidth * 20;// 7th Column
                //Mits 14020:Asif End

                dblCurrentX = dblLeftMargin;
                dblCurrentY = 900;
                //for creating new pages
                CreateNewPage(m_sConnectionString, true, p_strSearchFor);
                //For Header
                sHeader = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("Heading", 0, sCurrentPageId, m_iClientId), this.m_iLangCode.ToString());//sonali
                CurrentX = dblLeftMargin;
                CurrentY = dblCurrentY;
                PrintTextNoCr(sHeader);

                arrColAlign[0] = LEFT_ALIGN;
                arrColAlign[1] = LEFT_ALIGN;
                arrColAlign[2] = LEFT_ALIGN;
                arrColAlign[3] = LEFT_ALIGN;
                arrColAlign[4] = LEFT_ALIGN;
                arrColAlign[5] = LEFT_ALIGN;
                arrColAlign[6] = LEFT_ALIGN;
                CurrentX = dblLeftMargin;
                CurrentY = dblCurrentY;
                SetFontBold(false);
                dblCurrentY = 1200;
                iCounter = 0;

                for (int i = 0; i < objDT.Rows.Count; i++)
                {
                    //for creating rows in the report
                    CreateRow(objDT.Rows[i], dblLeftMargin, ref dblCurrentY, arrColAlign,
                       dblLeftMargin, dblLineHeight, arrColWidth, dblCharWidth);
                    iCounter++;
                    if (iCounter == NO_OF_ROWS)
                    {
                        CreateNewPage(m_sConnectionString, false, p_strSearchFor);
                        iCounter = 0;
                        dblCurrentY = 900;
                    }
                }

                // Printing the Summary
                if ((iCounter + 5) > NO_OF_ROWS)
                {
                    CreateNewPage(m_sConnectionString, false, p_strSearchFor);
                    dblCurrentY = 900;
                    iCounter = 0;
                }
                else
                {
                    dblCurrentY = dblCurrentY + dblLineHeight;
                }
                SetFontBold(true);
                CurrentX = dblLeftMargin;
                CurrentY = dblCurrentY;
                // Printing the Footer
                if ((iCounter + 7) > NO_OF_ROWS)
                {
                    CreateNewPage(m_sConnectionString, false, p_strSearchFor);
                    dblCurrentY = 900;
                }
                else
                {
                    dblCurrentY = dblCurrentY + dblLineHeight * 2;
                }

                CreateFooter(dblLeftMargin, dblCurrentY, dblLineHeight, 250);
                EndDoc();
                sFile = TempFile;
                Save(sFile);
                p_objMemory = Utilities.ConvertFilestreamToMemorystream(sFile, m_iClientId);
                File.Delete(sFile);
                iReturnValue = 1;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.DiaryList.GeneralReportError", m_iClientId), p_objException);
            }
            finally
            {
                arrColAlign = null;
                arrColWidth = null;
                m_objPrintDoc = null;
                m_objFont = null;
                m_objText = null;
            }
            return iReturnValue;


        }

        #endregion
        //Ended by Nitin on 28-Feb-2009 for Mergring Mits 10641 with R5

        ////asingh263 MITS 34874 starts
        string dataMultiline(string data)
        {
            string tempdata = data.Trim();
            string sData = string.Empty;
            int startindex = 0;
            int lastindex = 16;
            int len = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(tempdata.Length) / Convert.ToDouble(lastindex)));
            for (int i = 0; i < len; i++)
            {
                if (tempdata != "")
                {
                    if (i != len - 1)
                    {
                        sData += tempdata.Substring(startindex, lastindex) + "\n ";
                        tempdata = tempdata.Remove(startindex, lastindex);
                    }
                    else
                    {
                        sData += tempdata.Substring(startindex, tempdata.Length);
                    }
                }
            }
            return (sData);
        }
        public int CreateActiveDiariesReportDetails(string p_objXmlDoc, out MemoryStream p_objMemory, string p_strSearchFor)
        {
            DataSet objDS = null;
            double dblCharWidth = 0.0;
            double dblLineHeight = 0.0;
            double dblLeftMargin = 8.0;
            double dblRightMargin = 8.0;
            double dblTopMargin = 8.0;
            double dblPageWidth = 0.0;
            double dblPageHeight = 0.0;
            double dblBottomHeader = 0.0;
            double dblCurrentY = 0.0;
            double dblCurrentX = 0.0;
            double iCounter = 0.0;
            double[] arrColWidth = null;
            string sHeader = string.Empty;
            int iReturnValue = 0;
            string sFile = string.Empty;
            string sSQL = string.Empty;
            string[] arrColAlign = null;
            string[] arrCol = null;
            StringReader objReader = null;
            WpaDiaryEntry objWPADiary = null;
            p_objMemory = null;
            string sActs = string.Empty;

            try
            {

                arrColAlign = new string[8];
                arrColWidth = new double[8];
                arrCol = new string[8];
                objReader = new StringReader(p_objXmlDoc);
                objDS = new DataSet();
                objDS.ReadXml(objReader);
                var stringEntryid = Convert.ToString(objDS.Tables[0].Select("entryid>0")[0].ItemArray[0]);
                DataTable objDT = new DataTable("Dummy");
            
                    DataColumn objdc1 = new DataColumn("taskname");
                    DataColumn objdc2 = new DataColumn("createdon");
                    DataColumn objdc3 = new DataColumn("duedate");
                    DataColumn objdc4 = new DataColumn("workactivity");
                    DataColumn objdc5 = new DataColumn("estimatetime");
                    DataColumn objdc6 = new DataColumn("priority");
                    DataColumn objdc7 = new DataColumn("notes");
                    DataColumn objdc8 = new DataColumn("regarding");


                    objDT.Columns.Add(objdc1);
                    objDT.Columns.Add(objdc2);
                    objDT.Columns.Add(objdc3);
                    objDT.Columns.Add(objdc4);
                    objDT.Columns.Add(objdc5);
                    objDT.Columns.Add(objdc6);
                    objDT.Columns.Add(objdc7);
                    objDT.Columns.Add(objdc8);

                   DataRow dr = objDT.NewRow();

                   sSQL = "SELECT * FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID=" + stringEntryid;
                        using (DbReader objReader2 = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {

                            while (objReader2.Read())
                            {
                                if (sActs != "")
                                {
                                    sActs = (sActs + "|");
                                }
                                sActs = sActs + objReader2.GetValue("ACT_TEXT");
                            }
                        }
                        sSQL = string.Empty;
                        sSQL = "SELECT * FROM WPA_DIARY_ENTRY WHERE ENTRY_ID=" + stringEntryid;
                        using (DbReader objReader1 = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {

                            while (objReader1.Read())
                            {
                                dr[0] = dataMultiline(objReader1.GetValue("ENTRY_NAME").ToString());
                                dr[1] = Conversion.GetDBDateFormat(objReader1.GetValue("CREATE_DATE").ToString(), "d");
                                dr[2] = Conversion.GetDBDateFormat(objReader1.GetValue("COMPLETE_DATE").ToString(), "MM/dd/yyyy") + " " + Conversion.GetDBTimeFormat(Conversion.ConvertObjToStr((objReader1.GetValue("COMPLETE_TIME"))), "t"); 
                                dr[3] = dataMultiline(sActs.ToString());
                                dr[4] = dataMultiline(objReader1.GetValue("ESTIMATE_TIME").ToString());
                                dr[5] = dataMultiline(objReader1.GetValue("PRIORITY").ToString());
                                dr[6] = dataMultiline(objReader1.GetValue("ENTRY_NOTES").ToString());
                                dr[7] = dataMultiline(objReader1.GetValue("REGARDING").ToString());
                            }
                        }
                        objDT.Rows.Add(dr);

                StartDoc(true, 10, 10, 10, 10);
                SetFontBold(true);
                ClientWidth = 15400;
                dblCharWidth = GetTextWidth("0123456789/$,.ABCDEFGHIJKLMNOPQRSTUVWXYZ") / 40;
                dblLineHeight = GetTextHeight("W");
                dblLeftMargin = dblCharWidth * 2;
                dblRightMargin = dblCharWidth * 2;
                dblTopMargin = dblLineHeight * 5;
                dblPageWidth = PageWidth - dblLeftMargin - dblRightMargin;
                dblPageHeight = PageHeight;
                dblBottomHeader = dblPageHeight - (5 * dblLineHeight);

                arrColWidth[0] = dblCharWidth * 14;// First Column
                arrColWidth[1] = dblCharWidth * 13;// 2nd  Column
                arrColWidth[2] = dblCharWidth * 16;// 3rd Column
                arrColWidth[3] = dblCharWidth * 22;// 4th Column
                arrColWidth[4] = dblCharWidth * 14;// 5th Column
                arrColWidth[5] = dblCharWidth * 10;// 6th Column
                arrColWidth[6] = dblCharWidth * 20;// 7th Column
                arrColWidth[7] = dblCharWidth * 30;// 8th Column

                dblCurrentX = dblLeftMargin;
                dblCurrentY = 900;
                //For Header
                sHeader = CommonFunctions.FilterBusinessMessage(Globalization.GetString("WPA.CreateReportDetails.Heading", m_iClientId));
                CurrentX = dblLeftMargin;
                CurrentY = dblCurrentY;
                PrintTextNoCr(sHeader);

                arrColAlign[0] = LEFT_ALIGN;
                arrColAlign[1] = LEFT_ALIGN;
                arrColAlign[2] = LEFT_ALIGN;
                arrColAlign[3] = LEFT_ALIGN;
                arrColAlign[4] = LEFT_ALIGN;
                arrColAlign[5] = LEFT_ALIGN;
                arrColAlign[6] = LEFT_ALIGN;
                arrColAlign[7] = LEFT_ALIGN;
                CurrentX = dblLeftMargin;
                CurrentY = dblCurrentY;
                SetFontBold(false);
                dblCurrentY = 1200;
                iCounter = 0;

                for (int i = 0; i < objDT.Rows.Count; i++)
                {
                    
                    CreateRowDetails(objDT.Rows[i], dblLeftMargin, ref dblCurrentY, arrColAlign,
                       dblLeftMargin, dblLineHeight, arrColWidth, dblCharWidth);
                    iCounter++;
                    if (iCounter == NO_OF_ROWS)
                    {
                        CreateNewPage(m_sConnectionString, false, p_strSearchFor);
                        iCounter = 0;
                        dblCurrentY = 900;
                    }
                }

                 //Printing the Summary
                if ((iCounter + 5) > NO_OF_ROWS)
                {
                    CreateNewPage(m_sConnectionString, false, p_strSearchFor);
                    dblCurrentY = 900;
                    iCounter = 0;
                }
                else
                {
                    dblCurrentY = dblCurrentY + dblLineHeight;
                }
                SetFontBold(true);
                CurrentX = dblLeftMargin;
                CurrentY = dblCurrentY;
                // Printing the Footer
                if ((iCounter + 8) > NO_OF_ROWS)
                {
                    CreateNewPage(m_sConnectionString, false, p_strSearchFor);
                    dblCurrentY = 900;
                }
                else
                {
                    dblCurrentY = dblCurrentY + dblLineHeight * 2;
                }

                CreateFooter(dblLeftMargin, dblCurrentY, dblLineHeight, 250);
                EndDoc();
                sFile = TempFile;
                Save(sFile);
                p_objMemory = Utilities.ConvertFilestreamToMemorystream(sFile, m_iClientId);
                File.Delete(sFile);
                iReturnValue = 1;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException("An error has occurred generating Report.", p_objException);
            }
            finally
            {
                arrColAlign = null;
                arrColWidth = null;
                m_objPrintDoc = null;
                m_objFont = null;
                m_objText = null;
            }
            return iReturnValue;


        }

        ////asingh263 MITS 34874 ends

        #region SplitForStringPattern(string p_sString,string p_sPattern,ref ArrayList p_arrlstSplit)
        /// Name			: SplitForStringPattern
        /// Author			: Rahul Sharma
        /// Date Created	: 12-June-2006
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Split a string based on some string pattern.
        /// </summary>
        /// <param name="p_sString">String to be splitted.</param>
        /// <param name="p_sPattern">Pattern with in the string.</param>
        /// <param name="p_arrlstSplit">Array containing the splitted sub strings.</param>
        /// <returns>A boolean values that represents the success/failure.</returns>

        private bool SplitForStringPattern(string p_sString, string p_sPattern, ref ArrayList p_arrlstSplit)
        {
            bool bReturn = false;

            int iIndexOf = 0;

            try
            {
                iIndexOf = p_sString.IndexOf(p_sPattern);

                if (iIndexOf == -1)
                {
                    p_arrlstSplit.Add(p_sString);
                    bReturn = true;
                    return bReturn;
                }
                p_arrlstSplit.Add(p_sString.Substring(0, iIndexOf));

                string sAgain = p_sString.Substring(iIndexOf + p_sPattern.Length, p_sString.Length - (iIndexOf + p_sPattern.Length));
                SplitForStringPattern(sAgain, p_sPattern, ref p_arrlstSplit);
                bReturn = true;
            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("WPA.SplitForStringPattern.SplitForStringPatternError", m_iClientId), p_objException);
            }

            finally
            {
            }
            return bReturn;

        }
        #endregion

        /// <summary>
        /// Fetches the task description items
        /// </summary>        
        /// <returns></returns>
        public XmlDocument GetDiaryListing()
        {
            XmlElement objElement = null;
            XmlNode objNode = null;
            string sSQL = String.Empty;
            DbReader objReader = null;
            XmlDocument objDocument = null;
            SysSettings objSys = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud

            try
            {
                objDocument = new XmlDocument();
                objElement = objDocument.CreateElement("TaskDescList");
                if (Convert.ToBoolean(objSys.AllowTDFilter))
                {
                    objElement.SetAttribute("visible", "true");
                    objDocument.AppendChild(objElement);
                    sSQL = "SELECT DISTINCT ENTRY_NAME from WPA_DIARY_ENTRY ORDER BY ENTRY_NAME";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        objNode = objDocument.SelectSingleNode("//TaskDescList");
                        objElement = objNode.OwnerDocument.CreateElement("option");
                        objNode.AppendChild(objElement);
                        objElement.InnerText = objReader[0].ToString();
                        objElement.SetAttribute("value", objReader[0].ToString());
                    }
                }
                else
                {
                    objElement.SetAttribute("visible", "false");
                    objDocument.AppendChild(objElement);
                }

                return objDocument;

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetDiaryListing.Err", m_iClientId), p_objException);
            }
            finally
            {
                objElement = null;
                objNode = null;
                objDocument = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader = null;
                }
            }
        }


        /// <summary>
        /// Fetches the task description setting MITS 36764 srajindersin 5/23/2014
        /// </summary>        
        /// <returns></returns>
        public XmlDocument GetTaskDescSetting()
        {
            XmlElement objElement = null;
            XmlNode objNode = null;
            string sSQL = String.Empty;
            DbReader objReader = null;
            XmlDocument objDocument = null;
            SysSettings objSys = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud

            try
            {
                objDocument = new XmlDocument();
                objElement = objDocument.CreateElement("TaskDescList");
                if (Convert.ToBoolean(objSys.AllowTDFilter))
                {
                    objElement.SetAttribute("visible", "true");
                    objDocument.AppendChild(objElement);
                }
                else
                {
                    objElement.SetAttribute("visible", "false");
                    objDocument.AppendChild(objElement);
                }

                return objDocument;

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetDiaryListing.Err", m_iClientId), p_objException);
            }
            finally
            {
                objElement = null;
                objNode = null;
                objDocument = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader = null;
                }
            }
        }

        /// <summary>
        /// Fetches the Default assigned to flag from SYS_PARMS
        /// </summary>        
        /// <returns></returns>
        public string GetDefaultAssignedTo()
        {
            string sSQL = String.Empty;
            DbReader objReader = null;
            string sDefaultAssignedTo = "false";
            try
            {
                sSQL = "SELECT DEFAULT_ASSIGNED_TO FROM SYS_PARMS";
                objReader = DbFactory.ExecuteReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    sDefaultAssignedTo = Conversion.ConvertStrToBool(objReader.GetValue(0).ToString()).ToString();
                }
                return sDefaultAssignedTo;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetDiaryListing.Err", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader = null;
                }
            }
        }

        #region Changes for R5 DiaryCalender Module Added by Nitin
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        public void SetCalendarView(string code)
        {
            XmlDocument objUserPrefXML = null;
            XmlNode objNode = null; ;
            DbReader objReader = null;
            string sUserPrefsXML = "";
            string sSQL = "";
            try
            {
                sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + UserId.ToString();
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }
                CreateUserPreferXML(objUserPrefXML);
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/CalendarView/@value");
                objNode.Value = code;
                this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(UserId));
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.SaveUserPreference.Err", m_iClientId), p_objException);
            }
            finally
            {
                objUserPrefXML = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isBESEnabled"></param>
        /// <returns></returns>
        public XmlDocument GetOnLoadInformation(bool isBESEnabled)
        {
            DbReader objReader = null;
            XmlDocument objOutDoc = null;
            XmlElement objRootNode = null;
            string sSQL = string.Empty;
            string sUserPrefsXML = string.Empty;
            XmlDocument objUserPrefXML = null;
            string code = string.Empty;

            try
            {
                objOutDoc = new XmlDocument();
                objRootNode = objOutDoc.CreateElement("DiaryConfig");
                objOutDoc.AppendChild(objRootNode);

                sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + UserId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }
                XmlNode objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/CalendarView/@value");
                if (objNode != null && objNode.Value != null)
                {
                    code = objNode.Value.ToString();
                }
                else
                {
                    code = "0";
                }
                this.CreateAndSetElement(objRootNode, "CalendarView", code);
                this.CreateAndSetElement(objRootNode, "BESEnabled", isBESEnabled.ToString());
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.GetUserPreference.Err", m_iClientId), p_objException);
            }
            finally
            {
                objRootNode = null;
                objUserPrefXML = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return objOutDoc;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public XmlDocument GetCalendarView()
        {
            DbReader objReader = null;
            XmlDocument objOutDoc = null;
            XmlElement objRootNode = null;
            string sSQL = string.Empty;
            string sUserPrefsXML = string.Empty;
            XmlDocument objUserPrefXML = null;
            string code = string.Empty;

            try
            {
                objOutDoc = new XmlDocument();
                objRootNode = objOutDoc.CreateElement("DiaryConfig");
                objOutDoc.AppendChild(objRootNode);

                sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + UserId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }
                XmlNode objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/CalendarView/@value");
                if (objNode != null && objNode.Value != null)
                {
                    code = objNode.Value.ToString();
                }
                else
                {
                    code = "0";
                }
                this.CreateAndSetElement(objRootNode, "CalendarView", code);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.GetUserPreference.Err", m_iClientId), p_objException);
            }
            finally
            {
                objRootNode = null;
                objUserPrefXML = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return objOutDoc;
        }

        /// <summary>
        /// Create and Set the Text of the New Node.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Inner Text.</param>
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.CreateAndSetElement.Err", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Create and Set the Text of the New Node.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Inner Text.</param>
        /// <param name="p_objChildNode">Child Node Refrence</param>
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = p_sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.CreateAndSetElement.Err", m_iClientId), p_objEx);
            }
        }

        ///// <summary>
        ///// Create and Set the Text of the New Node.
        ///// </summary>
        ///// <param name="p_objParentNode">Parent node</param>
        ///// <param name="p_sNodeName">Node Name</param>
        ///// <param name="p_sText">Inner Text.</param>
        ///// <param name="p_objChildNode">Child Node Refrence</param>

        ///// <summary>
        ///// Create new Element in the Xml Document.
        ///// </summary>
        ///// <param name="p_objParentNode">Parent node</param>
        ///// <param name="p_sNodeName">Node Name</param>
        ///// <param name="p_objChildNode">Refrence to the New Node Created.</param>
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.CreateElement.Err", m_iClientId), p_objEx);
            }
        }

        private void SaveUserPrefersXmlToDb(string p_sUserPrefXml, int p_iUserId)
        {
            DbConnection objConn = null;
            DbWriter objWriter = null;
            string sSQL = string.Empty;

            try
            {
                //Try to delete the old one.
                sSQL = "DELETE FROM USER_PREF_XML WHERE USER_ID = " + p_iUserId;
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
                objConn.Close();

                objWriter = DbFactory.GetDbWriter(m_sConnectionString);
                objWriter.Tables.Add("USER_PREF_XML");
                objWriter.Fields.Add("USER_ID", p_iUserId);
                objWriter.Fields.Add("PREF_XML", p_sUserPrefXml);
                objWriter.Execute();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.SaveUserPrefersXmlToDb.Err", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objWriter != null)
                {
                    objWriter = null;
                }
            }
        }

        /// <summary>
        /// Created By Nitin for DiaryCalender
        /// This function returns Diary's list based on parameters 
        /// </summary>
        /// <param name="p_iSortOrder"></param>
        /// <param name="p_sUserName"></param>
        /// <param name="p_sDueDate"></param>
        /// <param name="p_lRecordCount"></param>
        /// <param name="p_lPageCount"></param>
        /// <param name="p_lPageNumber"></param>
        /// <param name="p_bOpen"></param>
        /// <param name="p_bReqActDiary"></param>
        /// <param name="p_sAttachTable"></param>
        /// <param name="p_sAttachRecordId"></param>
        /// <param name="sFunction"></param>
        /// <param name="p_sActiveDiaryChecked"></param>
        /// <param name="showNotes"></param>
        /// <param name="sAllClaimDiaries"></param>
        /// <param name="sShowAllDiariesForActiveRec"></param>
        /// <param name="p_sNavigationMonth"></param>
        /// <param name="p_sNavigationYear"></param>
        /// <param name="p_sCurrentScheduleControl"></param>
        /// <param name="p_sCurrentActiveDate"></param>
        /// <param name="p_sWeekViewFromDate"></param>
        /// <param name="p_sWeekViewToDate"></param>
        /// <returns>Returns Diary list as string</returns>
        public XmlDocument GetDiaryDomForDiaryCalender(int p_iSortOrder, string p_sUserName, string p_sDueDate, long p_lRecordCount
            , long p_lPageCount, long p_lPageNumber, bool p_bOpen, bool p_bReqActDiary, string p_sAttachTable
            , string p_sAttachRecordId, string sFunction, string p_sActiveDiaryChecked, string showNotes, string sAllClaimDiaries, string sShowAllDiariesForActiveRec, string p_sNavigationMonth, string p_sNavigationYear
            , string p_sCurrentScheduleControl, string p_sCurrentActiveDate, string p_sWeekViewFromDate, string p_sWeekViewToDate)
        {
            #region local variables
            bool bIsAttached = false;
            string sSQL = "";
            string sWhere = "";
            string sWhere1 = "";
            string sWhere2 = "";
            string sAssigningUser = "";
            string sAssignedUser = "";
            string sAttached = "";
            string sPIXmlFormName = "";
            string sIsAttached = "";
            string sDueDate = "";
            string sFname = "";
            string sLname = "";
            string sName = "";
            string sClaimStatus = "";
            string sClaimantName = "";
            string sDepartmentName = "";
            string sStatus = "";
            string sOrgLevel = "";
            string sUserPrefsXML = "";
            string sSQL1 = string.Empty;
            string sAttachTable = "";//nadim 11845
            string sSQL2 = string.Empty;// nadim 11845
            string sTableValue = "";//nadim 11845
            //string []arrTableValue= null;//nadim
            ArrayList TableValue;//nadim 11845
            string sSQL_WPA = string.Empty;
            int iSortOrder = 0;
            int iCLnToInt = 0;
            int iCount = 0;
            int iCountVal = 0;//nadim 11845
            int iCounter = 0;
            int iUpperBound = 0;
            float fCalPages = 0;
            long lRid = 0;
            long lTmp = 0;
            long lToday = 0;
            long lCompleteDate = 0;
            long lCreateDate = 0;
            long lStartAt = 0;
            long lCounter = 0;
            bool bStatus = false; //Umesh
            bool isAttachTable = false;// nadim 11845
            XmlDocument objDOM = null;
            XmlDocument objUserPrefXML = null; //Umesh
            XmlDocument objXml = null;  //Umesh
            XmlNode objShowActiveDiary = null; //Umesh
            XmlNode objSaveShowActiveDiary = null; //Umesh
            XmlElement objElem = null;
            LocalCache objCache = new LocalCache(m_sConnectionString,m_iClientId);
            SysSettings objSys = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud

            DbConnection objConn = null; //Umesh
            DataSet objDataSet = null;
            //DbReader objRdr1 = null;//Umesh
            DbCommand objCmd = null;//Umesh
            DbParameter objParam = null;//Umesh
            DataRow[] objRow = null;
            StringBuilder sbSql = null;
            bool isOracleDataBase = false; //nitin
            string sAttachID = string.Empty; //pmittal5 Confidential Record
            string sPI = string.Empty;
            //rupal:start.r8 auto diary enh
            string sClaimID = string.Empty;
            string[] sProgressNoteDetail;
            string[] sAttachPromptDetail;
            //rupal:end
            //Added by sharishkumar for mits 35291
            string sDependentRowDetail;
            string[] sDependentDetail;
            //End mits 35291
            #endregion

            try
            {
                sDependentDetail = null;
                sAttachPromptDetail = null;

                lToday = Conversion.ConvertStrToLong(Conversion.GetDate(DateTime.Now.Date.ToString()));
                //Mukul Added ASSIGNED_USER in sql 2/7/2007 MITS 8782
                 sSQL = "SELECT ENTRY_ID,COMPLETE_DATE,ATT_PARENT_CODE,IS_ATTACHED,ATTACH_TABLE,ATTACH_RECORDID,STATUS_OPEN,REGARDING,PRIORITY,ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,CREATE_DATE,ASSIGNED_USER FROM WPA_DIARY_ENTRY ";
                sWhere = "DIARY_VOID = 0 AND DIARY_DELETED = 0 ";

                //Geeta 11/17/06 : Code added for fixing mits number 8139 for Work Activity
                if (p_iSortOrder == 9 || p_iSortOrder == 13)
                {
                    sSQL = "SELECT ACT_TEXT,ENTRY_ID,COMPLETE_DATE,IS_ATTACHED,ATTACH_TABLE,ATTACH_RECORDID,STATUS_OPEN,REGARDING,PRIORITY,ASSIGNING_USER,ENTRY_NAME,ENTRY_NOTES,ATT_SEC_REC_ID,CREATE_DATE,ASSIGNED_USER FROM WPA_DIARY_ENTRY " +
                          "LEFT JOIN WPA_DIARY_ACT ON WPA_DIARY_ENTRY.ENTRY_ID = WPA_DIARY_ACT.PARENT_ENTRY_ID";
                }
                //MITS 11998 Abhishek added sShowAllDiariesForActiveRec!="no"
                if (p_sAttachTable.Trim() != "" && p_sAttachRecordId.Trim() != "" && p_sAttachRecordId.Trim() != "0" && sShowAllDiariesForActiveRec != "no")
                    bIsAttached = true;

                switch (sFunction)
                {
                    case "peek":
                        if (p_sUserName.Trim() != "")
                        {
                            sWhere = sWhere + " AND (ASSIGNED_USER = '" + p_sUserName + "' OR ASSIGNED_GROUP = '" + m_iGroupId.ToString() + "')";
                        }
                        break;
                    default:

                        //Parijat: Mits 7171 --the changes were reverted back since form.js has been changed to handle the same
                        //if(objSys.AttachDiaryVis == false)
                        if (bIsAttached == false || objSys.AttachDiaryVis == false ||
                            (objSys.AttachDiaryVis == true && p_sAttachTable.Trim() == ""))
                        {
                            if (p_sUserName.Trim() != "")
                            {
                                sWhere = sWhere + " AND (ASSIGNED_USER = '" + p_sUserName + "' OR ASSIGNED_GROUP = '" + m_iGroupId.ToString() + "')";
                            }
                            else if (!string.IsNullOrEmpty(Convert.ToString(m_iGroupId)))
                                sWhere = sWhere + " AND (ASSIGNED_GROUP = '" + m_iGroupId.ToString() + "')";
                        }

                        break;
                }
                //By default the due date should be current date.. however the code was not cdonsidering current date.. changed by raman bhatia
                //01/10/2007 REM Umesh

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, "Select PREF_XML from USER_PREF_XML where USER_ID=" + UserId.ToString()))
                {
                    if (objRdr.Read())
                    {
                        sUserPrefsXML = Common.Conversion.ConvertObjToStr(objRdr.GetValue("PREF_XML"));
                        bStatus = true;
                    }
                    //objRdr.Close();
                }
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();
                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }

                // Create the xml if it doesn't exist in the database
                CreateUserPreferXML(objUserPrefXML);

                //tkr 6/2007 HACK
                //<ShowNotes> node value is "0" in instance.xml
                //if this is first time called (showNotes=="0"), then fetch the UserPref and set the checkbox to it
                //if this is a postback (showNotes=="1" or showNotes=="") reset the UserPref
                XmlNode nodeShowNotes = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ShowNotes/@selected");
                if (showNotes.Equals("0"))
                {
                    showNotes = nodeShowNotes.InnerText;
                } // if
                else
                {
                    nodeShowNotes.InnerText = showNotes;
                } // else

                objShowActiveDiary = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ShowActiveDiaryChecked/@selected");

                if (!p_bReqActDiary)
                {
                    if (objShowActiveDiary != null)
                    {
                        if (objShowActiveDiary.InnerText == "1")
                            p_sDueDate = "";
                    }
                }

                objXml = new XmlDocument();
                string strContent = RMSessionManager.GetCustomSettings(m_iClientId);

                if (!string.IsNullOrEmpty(strContent))
                {
                    objXml.LoadXml(strContent);
                    //objRdr.Dispose();
                    objSaveShowActiveDiary = objXml.SelectSingleNode("//Other/SaveShowActiveDiary");
                    if (objSaveShowActiveDiary != null)
                    {
                        if (objSaveShowActiveDiary.InnerText == "-1")
                        {
                            if (p_bReqActDiary)
                            {
                                if (p_sActiveDiaryChecked == "")
                                    objShowActiveDiary.Value = "0";
                                else
                                    objShowActiveDiary.Value = "1";
                            }
                        }
                    }
                }


                sUserPrefsXML = objUserPrefXML.InnerXml.ToString();
                objConn = DbFactory.GetDbConnection(m_sConnectionString);

                objConn.Open();


                //added by Nitin for R5 Diary Calendar
                if (DbFactory.IsOracleDatabase(m_sConnectionString))
                {
                    isOracleDataBase = true;
                }
                else
                {
                    isOracleDataBase = false;
                }

                sbSql = new StringBuilder();
                objCmd = objConn.CreateCommand();
                objParam = objCmd.CreateParameter();
                objParam.Direction = ParameterDirection.Input;
                objParam.Value = sUserPrefsXML;
                objParam.ParameterName = "XML";
                objParam.SourceColumn = "PREF_XML";
                objCmd.Parameters.Add(objParam);
                if (bStatus)
                    sbSql.Append("UPDATE USER_PREF_XML SET PREF_XML=~XML~ WHERE USER_ID=" + UserId);
                else
                    sbSql.Append("INSERT INTO USER_PREF_XML VALUES(" + UserId + ",~XML~)");
                objCmd.CommandText = sbSql.ToString();
                objCmd.ExecuteNonQuery();
                //End REM UMESH
                if (p_sDueDate == "today") p_sDueDate = lToday.ToString();
                sDueDate = Conversion.GetDate(p_sDueDate);

                if (!sDueDate.Trim().Equals(""))
                {
                    sWhere = sWhere + " AND COMPLETE_DATE <= '" + sDueDate + "'";
                }
                if (p_bOpen)
                {
                    sWhere = sWhere + " AND STATUS_OPEN <> 0 ";
                }
                else
                {
                    sWhere = sWhere + " AND STATUS_OPEN = 0";
                }
                sSQL_WPA = sSQL + " WHERE " + sWhere;
                //MITS 11998 Abhishek added sShowAllDiariesForActiveRec=="yes" condition
                if (sAllClaimDiaries == "yes" && p_sAttachRecordId.Trim() != "" && p_sAttachRecordId.Trim() != "0" && p_sAttachTable.Trim() == "CLAIM" && sShowAllDiariesForActiveRec == "yes")
                {
                    int i = 0;
                    DataModelFactory objDMF = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
                    Claim objClaim = (Claim)objDMF.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(Conversion.ConvertStrToInteger(p_sAttachRecordId));
                    Event objEvent = (Event)objDMF.GetDataModelObject("Event", false);
                    objEvent.MoveTo(objClaim.EventId);

                    sWhere1 = sWhere + " AND ATTACH_TABLE='CLAIM'";
                    sWhere1 = sWhere1 + " AND ATTACH_RECORDID = " + objClaim.ClaimId;
                    sWhere1 = sWhere1 + " AND IS_ATTACHED <> 0";
                    sWhere1 = sWhere1.Trim();
                    sSQL = "(" + sSQL + " WHERE " + sWhere1;

                    /*switch (p_iSortOrder)
                    {
                        case (int)DIARYSORTORDER.ASC_COMPLETE_DATE:								
                            sSQL = sSQL + " ORDER BY COMPLETE_DATE ASC";
                            iSortOrder=(int)DIARYSORTORDER.ASC_COMPLETE_DATE;
                            break;
                        case (int)DIARYSORTORDER.ASC_ATTACH_TABLE:								
                            sSQL = sSQL + " ORDER BY ATTACH_TABLE ASC";
                            iSortOrder=(int)DIARYSORTORDER.ASC_ATTACH_TABLE;
                            break;
                        case (int)DIARYSORTORDER.ASC_ENTRY_NAME:								
                            sSQL = sSQL + " ORDER BY ENTRY_NAME ASC";
                            iSortOrder=(int)DIARYSORTORDER.ASC_ENTRY_NAME;
                            break;
                        case (int)DIARYSORTORDER.ASC_PRIORITY:								
                            sSQL = sSQL + " ORDER BY PRIORITY ASC";
                            iSortOrder=(int)DIARYSORTORDER.ASC_PRIORITY;
                            break;
                        case (int)DIARYSORTORDER.DEC_ATTACH_TABLE:								
                            sSQL = sSQL + " ORDER BY ATTACH_TABLE DESC";
                            iSortOrder=(int)DIARYSORTORDER.DEC_ATTACH_TABLE;
                            break;
                        case (int)DIARYSORTORDER.DEC_ENTRY_NAME:								
                            sSQL = sSQL + " ORDER BY ENTRY_NAME DESC";
                            iSortOrder=(int)DIARYSORTORDER.DEC_ENTRY_NAME;
                            break;
                        case (int)DIARYSORTORDER.DEC_PRIORITY:								
                            sSQL = sSQL + " ORDER BY PRIORITY DESC";
                            iSortOrder=(int)DIARYSORTORDER.DEC_PRIORITY;
                            break;
                        default:					
                            sSQL = sSQL + " ORDER BY COMPLETE_DATE DESC";
                            p_iSortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                            iSortOrder=(int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                            break;
                    }*/
                    #region Getting Claim Info
                    DbReader objReader;

                    sSQL1 = "SELECT TRANS_ID FROM FUNDS WHERE CLAIM_ID=" + objClaim.ClaimId;

                    objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL1);
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            //sClaimDetails[i,0]=objReader.GetValue("TRANS_ID").ToString();
                            //sClaimDetails[i,1]="FUNDS";
                            iCount++;
                        }
                    }
                    objReader = null;
                    iCount = iCount + objClaim.AdjusterList.Count;
                    iCount = iCount + objClaim.ClaimantList.Count;
                    iCount = iCount + objClaim.DefendantList.Count;
                    iCount = iCount + objClaim.LitigationList.Count;
                    objEvent = (objClaim.Parent as Event);
                    iCount = iCount + objEvent.PiList.Count;
                    foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                    {
                        iCount = iCount + objAdjuster.AdjustDatedTextList.Count;
                    }

                    string[,] sClaimDetails = new string[iCount, 2];

                    foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                    {
                        sClaimDetails[i, 0] = objAdjuster.AdjRowId.ToString();
                        sClaimDetails[i, 1] = "ADJUSTER";
                        i++;
                    }
                    foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                    {
                        foreach (AdjustDatedText objDatedText in objAdjuster.AdjustDatedTextList)
                        {
                            sClaimDetails[i, 0] = objDatedText.AdjDttextRowId.ToString();
                            sClaimDetails[i, 1] = "ADJUSTERDATEDTEXT";
                            i++;
                        }
                    }
                    foreach (Claimant objClaimant in objClaim.ClaimantList)
                    {
                        sClaimDetails[i, 0] = objClaimant.ClaimantEid.ToString();
                        sClaimDetails[i, 1] = "CLAIMANT";
                        i++;
                    }
                    foreach (Defendant objDefendant in objClaim.DefendantList)
                    {
                        sClaimDetails[i, 0] = objDefendant.DefendantRowId.ToString();
                        sClaimDetails[i, 1] = "DEFENDANT";
                        i++;
                    }
                    foreach (ClaimXLitigation objLitigation in objClaim.LitigationList)
                    {
                        sClaimDetails[i, 0] = objLitigation.JudgeEid.ToString();
                        sClaimDetails[i, 1] = "LITIGATION";
                        i++;
                    }
                    foreach (PersonInvolved objPersons in objEvent.PiList)
                    {
                        int piTypeCode = objPersons.PiTypeCode;
                        string sTable = string.Empty;
                        switch (objClaim.Context.LocalCache.GetShortCode(piTypeCode))
                        {
                            case "E":
                                sTable = "PiEmployee";
                                break;
                            case "MED":
                                sTable = "PiMedicalStaff";
                                break;
                            case "O":
                                sTable = "PiOther";
                                break;
                            case "P":
                                sTable = "PiPatient";
                                break;
                            case "PHYS":
                                sTable = "PiPhysician";
                                break;
                            case "W":
                                sTable = "PiWitness";
                                break;
                        }
                        sTable = sTable.ToUpper();
                        sClaimDetails[i, 0] = objPersons.PiRowId.ToString();
                        sClaimDetails[i, 1] = sTable;
                        i++;
                    }

                    objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL1);
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            sClaimDetails[i, 0] = objReader.GetValue("TRANS_ID").ToString();
                            sClaimDetails[i, 1] = "FUNDS";
                            i++;
                        }
                    }
                    objReader = null;
                    #endregion

                    for (int j = 0; j < iCount; j++)
                    {
                        sWhere1 = "";
                        sWhere1 = "  UNION " + sSQL_WPA + " AND ATTACH_TABLE='" + sClaimDetails[j, 1] + "'";
                        sWhere1 = sWhere1 + " AND ATTACH_RECORDID = " + sClaimDetails[j, 0];
                        sWhere1 = sWhere1 + " AND IS_ATTACHED <> 0";

                        sSQL = sSQL + sWhere1;


                    }
                    switch (p_iSortOrder)
                    {
                        case (int)DIARYSORTORDER.ASC_COMPLETE_DATE:
                            sSQL = sSQL + ") ORDER BY COMPLETE_DATE ASC";
                            iSortOrder = (int)DIARYSORTORDER.ASC_COMPLETE_DATE;
                            break;
                        case (int)DIARYSORTORDER.ASC_ATTACH_TABLE:
                            // sSQL = sSQL + ") ORDER BY ATTACH_TABLE ASC";
                            iSortOrder = (int)DIARYSORTORDER.ASC_ATTACH_TABLE;
                            break;
                        case (int)DIARYSORTORDER.ASC_ENTRY_NAME:
                            sSQL = sSQL + ") ORDER BY ENTRY_NAME ASC";
                            iSortOrder = (int)DIARYSORTORDER.ASC_ENTRY_NAME;
                            break;
                        case (int)DIARYSORTORDER.ASC_PRIORITY:
                            sSQL = sSQL + ") ORDER BY PRIORITY ASC";
                            iSortOrder = (int)DIARYSORTORDER.ASC_PRIORITY;
                            break;
                        case (int)DIARYSORTORDER.DEC_ATTACH_TABLE:
                            // sSQL = sSQL + ") ORDER BY ATTACH_TABLE DESC";
                            iSortOrder = (int)DIARYSORTORDER.DEC_ATTACH_TABLE;
                            break;
                        case (int)DIARYSORTORDER.DEC_ENTRY_NAME:
                            sSQL = sSQL + ") ORDER BY ENTRY_NAME DESC";
                            iSortOrder = (int)DIARYSORTORDER.DEC_ENTRY_NAME;
                            break;
                        case (int)DIARYSORTORDER.DEC_PRIORITY:
                            sSQL = sSQL + ") ORDER BY PRIORITY DESC";
                            iSortOrder = (int)DIARYSORTORDER.DEC_PRIORITY;
                            break;
                        default:
                            sSQL = sSQL + ") ORDER BY COMPLETE_DATE DESC";
                            p_iSortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                            iSortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                            break;
                    }

                }
                else
                {
                    sWhere2 = sWhere;
                    //MITS 11998 Abhishek-start : added condition sShowAllDiariesForActiveRec=="yes"
                    if (p_sAttachTable.Trim() != "" && sShowAllDiariesForActiveRec == "yes")
                    {
                        sWhere2 = sWhere2 + " AND ATTACH_TABLE='" + p_sAttachTable + "'";
                    }
                    if (p_sAttachRecordId != "" && sShowAllDiariesForActiveRec == "yes")
                    {
                        sWhere2 = sWhere2 + " AND ATTACH_RECORDID = " + p_sAttachRecordId;
                        sWhere2 = sWhere2 + " AND IS_ATTACHED <> 0";
                    }
                    //MITS 11998 end


                    //nitin
                    if (p_sCurrentScheduleControl == "webmonthview")
                    {
                        if (isOracleDataBase)  //if current database is oracle then use SUBSTR 
                        {
                            sWhere2 = sWhere2 + " AND SUBSTR(complete_date,1,4) = '" + p_sNavigationYear + "' and SUBSTR(complete_date,5,2) = '" + p_sNavigationMonth + "'";
                        }
                        else
                        {
                            sWhere2 = sWhere2 + " AND substring(complete_date,1,4) = '" + p_sNavigationYear + "' and substring(complete_date,5,2) = '" + p_sNavigationMonth + "'";
                        }
                    }
                    else if (p_sCurrentScheduleControl == "webweekview")
                    {
                        sWhere2 = sWhere2 + " AND COMPLETE_DATE >= '" + Conversion.GetDate(p_sWeekViewFromDate) + "' AND COMPLETE_DATE <= '" + Conversion.GetDate(p_sWeekViewToDate) + "'";
                    }
                    else
                    {
                        sWhere2 = sWhere2 + " AND COMPLETE_DATE = '" + Conversion.GetDate(p_sCurrentActiveDate) + "'";
                    }

                    //      sWhere2 = sWhere2 + " AND substring(complete_date,1,4) = '" + navigationYear + "' and substring(complete_date,5,2) = '" + navigationMonth + "'";


                    sWhere2 = sWhere2.Trim();
                    sSQL = sSQL + " WHERE " + sWhere2;

                    switch (p_iSortOrder)
                    {
                        case (int)DIARYSORTORDER.ASC_COMPLETE_DATE:
                            sSQL = sSQL + " ORDER BY COMPLETE_DATE ASC";
                            iSortOrder = (int)DIARYSORTORDER.ASC_COMPLETE_DATE;
                            break;
                        case (int)DIARYSORTORDER.ASC_ATTACH_TABLE:
                            //sSQL = sSQL + " ORDER BY ATTACH_TABLE ASC";
                            iSortOrder = (int)DIARYSORTORDER.ASC_ATTACH_TABLE;
                            break;
                        case (int)DIARYSORTORDER.ASC_ENTRY_NAME:
                            sSQL = sSQL + " ORDER BY ENTRY_NAME ASC";
                            iSortOrder = (int)DIARYSORTORDER.ASC_ENTRY_NAME;
                            break;
                        case (int)DIARYSORTORDER.ASC_PRIORITY:
                            sSQL = sSQL + " ORDER BY PRIORITY ASC";
                            iSortOrder = (int)DIARYSORTORDER.ASC_PRIORITY;
                            break;
                        case (int)DIARYSORTORDER.DEC_ATTACH_TABLE:
                            //sSQL = sSQL + " ORDER BY ATTACH_TABLE DESC";
                            iSortOrder = (int)DIARYSORTORDER.DEC_ATTACH_TABLE;
                            break;
                        case (int)DIARYSORTORDER.DEC_ENTRY_NAME:
                            sSQL = sSQL + " ORDER BY ENTRY_NAME DESC";
                            iSortOrder = (int)DIARYSORTORDER.DEC_ENTRY_NAME;
                            break;
                        case (int)DIARYSORTORDER.DEC_PRIORITY:
                            sSQL = sSQL + " ORDER BY PRIORITY DESC";
                            iSortOrder = (int)DIARYSORTORDER.DEC_PRIORITY;
                            break;
                        //Geeta 11/17/06 : Case added to fix mits number 8139 for Sorting Work Activity
                        case (int)DIARYSORTORDER.ASC_WORK_ACTIVITY:
                            iSortOrder = (int)DIARYSORTORDER.ASC_WORK_ACTIVITY;
                            break;
                        case (int)DIARYSORTORDER.DEC_WORK_ACTIVITY:
                            iSortOrder = (int)DIARYSORTORDER.DEC_WORK_ACTIVITY;
                            break;
                            //igupta3 jira 439
                        case (int)DIARYSORTORDER.ASC_NOTROUTE:
                            iSortOrder = (int)DIARYSORTORDER.ASC_NOTROUTE;
                            break;
                        case (int)DIARYSORTORDER.DEC_NOTROUTE:
                            iSortOrder = (int)DIARYSORTORDER.DEC_NOTROUTE;
                            break;
                        case (int)DIARYSORTORDER.ASC_NOTROLL:
                            iSortOrder = (int)DIARYSORTORDER.ASC_NOTROLL;
                            break;
                        case (int)DIARYSORTORDER.DEC_NOTROLL:
                            iSortOrder = (int)DIARYSORTORDER.DEC_NOTROLL;
                            break;
                        //MITS 36519
                        case (int)DIARYSORTORDER.ASC_PARENTRECORD:
                            iSortOrder = (int)DIARYSORTORDER.ASC_PARENTRECORD;
                            break;
                        case (int)DIARYSORTORDER.DEC_PARENTRECORD:
                            iSortOrder = (int)DIARYSORTORDER.DEC_PARENTRECORD;
                            break;
                        default:
                            sSQL = sSQL + " ORDER BY COMPLETE_DATE DESC";
                            p_iSortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                            iSortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                            break;
                    }
                }

                //nitin               

                objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
                objRow = objDataSet.Tables[0].Select();



                //Umesh MITS 9401 : SORTING BY Attached Record

                iCounter = 0;
                iUpperBound = 0;
                objDataSet.Tables[0].Columns.Add("ATTACH_PROMPT", System.Type.GetType("System.String"));
                iUpperBound = objDataSet.Tables[0].Rows.Count - 1;
                while (iCounter <= iUpperBound)
                {
                    lRid = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_RECORDID"]));
                    if (lRid > 0)
                    {

                        switch (Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"]))
                        {
                            case CLAIM:
                            case "claim":
                                sAttached = "Claim: " + GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + lRid.ToString(), m_sConnectionString);
                                break;
                            case EVENT:
                            case "event":

                                sAttached = "Event: " + GetSingleString("EVENT_NUMBER", "EVENT", "EVENT_ID=" + lRid.ToString(), m_sConnectionString);
                                break;
                            case ENTITY:
                            case "entity":
                                objCache.GetEntityInfo(lRid, ref sFname, ref sLname);
                                sName = sFname.Trim().Equals("") ? sLname : sFname + ", " + sLname;
                                sAttached = "Person\\Entity:" + sName;
                                break;
                            case CLAIMANT:
                            case "claimant":
                                objCache.GetClaimantInfo(lRid, ref sFname, ref sLname);
                                sName = sFname.Trim().Equals("") ? sLname : sFname + ", " + sLname;
                                sAttached = "Claimant:" + sName;
                                break;
                            case PERSON_INVOLVED:
                            case "person_involved":
                                sPIXmlFormName = GetPiXMLFormName(lRid, ref sAttached);
                                break;
                            // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
                            case POLICY_ENH:
                            case "POLICY_ENH":
                            case "policy_enh":
                                // Get the LOB information of the Policy
                                int iLob = GetSingleInt("POLICY_TYPE", "POLICY_ENH", "POLICY_ID = " + lRid.ToString(), m_sConnectionString);
                                objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"] = string.Concat("POLICY_ENH_", objCache.GetShortCode(iLob));
                                sAttached = String.Concat("POLICY MANAGEMENT (", objCache.GetShortCode(iLob), "): ", GetSingleString("POLICY_NAME", "POLICY_ENH", string.Format("POLICY_ID={0}", lRid.ToString()), m_sConnectionString));
                                break;
                            // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries                           
                            //rupal:start, r8 auto diary enh                                
                            case RESERVE_HISTORY:
                            case "reserve_history":
                                sAttached = GetAttachPromptForReserveHistory(lRid.ToString(), out sClaimID);
                                break;
                            case RESERVE_CURRENT:
                            case "reserve_current":
                                sAttached = GetAttachPromptForReserveReview(lRid.ToString(), out sClaimID);
                                break;
                            case CLAIM_LITIGATION:
                            case LITIGATION:
                            case "claim_x_litigation":
                            case "litigation":
                                sAttached = GetAttachPromptForLitigation(lRid.ToString());
                                break;
                            case FUND_AUTO_BATCH:
                            case AUTO_CLAIM_CHECKS:
                            case "funds_auto_batch":
                            case "autoclaimchecks":
                                sAttached = GetAttachPromptForSchedulePayment(lRid.ToString());
                                break;
                            /*
                             case COMMENTS_TEXT:
                             case "comments_text":                                                             
                                     string[] sCommentDetail = GetMultipleString("ATTACH_RECORDID,ATTACH_TABLE,COMMENT_ID", "COMMENTS_TEXT", "COMMENT_ID=" + lRid.ToString(), m_sConnectionString);
                                     sAttached = GetAttachPromptForcommentReview(sCommentDetail);                                
                                 break;
                             */
                            case CLAIM_PRG_NOTE:
                            case "claim_prg_note":
                                sProgressNoteDetail = GetMultipleString("CLAIM_ID,EVENT_ID,CL_PROG_NOTE_ID", "CLAIM_PRG_NOTE", "CL_PROG_NOTE_ID=" + lRid.ToString(), m_sConnectionString);
                                sAttachPromptDetail = GetAttachPromptForProgressNotes(sProgressNoteDetail);
                                sAttached = sAttachPromptDetail[0];
                                break;
                            //Added by sharishkumar for mits 35291
                            case PIDEPENDENT:
                            case "pidependent":
                                sDependentRowDetail = GetSingleString("PI_ROW_ID", "PI_X_DEPENDENT", "PI_DEP_ROW_ID=" + lRid.ToString(), m_sConnectionString);

                                // RMA-6445 - Error occurs on the load of Diary list if the Attached Record is missing in DB
                                // We now try to retrieve the details of the Attached Record if we have a Validrecord attached with it
                                if(! string.IsNullOrEmpty(sDependentRowDetail))
                                    sDependentDetail = GetMultipleString("EVENT_ID,PI_ROW_ID", "PERSON_INVOLVED", "PI_ROW_ID=" + sDependentRowDetail, m_sConnectionString);

                                if(sDependentDetail != null)
                                    sAttachPromptDetail = GetAttachPromptForDependent(sDependentDetail);  

                                if(sAttachPromptDetail.Length > 0)
                                	sAttached = sAttachPromptDetail[0];
                                break;
                            //End mits 35291
                            case CASE_MANAGEMENT:
                            case CMXTREATMENTPLN:
                            case "cm_x_treatment_pln":
                            case "cmxtreatmentpln":
                                sAttached = GetAttachPropmtForCaseManagement(lRid.ToString());
                                break;
                            //scheduled activity for subrogation  
                            case SCHEDULED_ACTIVITY:
                            case "scheduled_activity":
                                sAttached = GetAttachPromptForScheduledActivity(lRid.ToString(), out sClaimID);
                                break;
                            //rupal:end
                            default:
                                lTmp = objCache.GetTableId(Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"]));
                                if (Conversion.ConvertLongToBool(lTmp, m_iClientId))
                                {
                                    sAttached = objCache.GetTableName(Conversion.ConvertStrToInteger(lTmp.ToString())) + " (" + lRid.ToString() + ")";
                                }
                                else
                                {
                                    sAttached = Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCounter]["ATTACH_TABLE"]).ToUpper() + lRid.ToString();
                                }
                                break;
                        }
                        objDataSet.Tables[0].Rows[iCounter]["ATTACH_PROMPT"] = sAttached;

                    }


                    iCounter = iCounter + 1;
                }
                //Check for the Attached Record Sort Order

                if (p_iSortOrder == 2)
                {
                    objRow = objDataSet.Tables[0].Select("", "ATTACH_PROMPT ASC");

                }
                if (p_iSortOrder == 6)
                {
                    objRow = objDataSet.Tables[0].Select("", "ATTACH_PROMPT DESC");

                }

                //END MITS 9401 


                //Geeta 11/21/06 : Modified to fix mits number 8139 for Sorting Work Activity
                if (p_iSortOrder == 9 || p_iSortOrder == 13)
                {
                    iCounter = 0;
                    while (iCounter <= iUpperBound)
                    {
                        if (objDataSet.Tables[0].Rows[iCounter]["ACT_TEXT"] == System.DBNull.Value)
                        {
                            //MITS 31146 Raman
                            //objDataSet.Tables[0].DefaultView.Table.Rows[iCounter]["ACT_TEXT"] = "No Work Activity";
                            objDataSet.Tables[0].DefaultView.Table.Rows[iCounter]["ACT_TEXT"] = "";
                        }
                        iCounter = iCounter + 1;
                    }

                    //Check for the Work Activity Sort Order
                    if (p_iSortOrder == 9)
                    {
                        objRow = objDataSet.Tables[0].Select("", "ACT_TEXT ASC");
                    }
                    else if (p_iSortOrder == 13)
                    {
                        objRow = objDataSet.Tables[0].Select("", "ACT_TEXT DESC");
                    }
                }
                if (objDataSet.Tables.Count == 0)
                {
                    throw new RMAppException(Globalization.GetString("WPA.GetDiaryDom.DatasetCreationError", m_iClientId));
                }
                if (objDataSet.Tables.Count > 0)
                {
                    p_lRecordCount = objDataSet.Tables[0].Rows.Count;
                }
                long maxcount = p_lRecordCount;
                p_lPageCount = (int)(p_lRecordCount / m_lPageSize);
                fCalPages = (float)((float)p_lRecordCount / (float)m_lPageSize);
                if ((float)(p_lPageCount) < (float)(fCalPages))
                {
                    p_lPageCount = p_lPageCount + 1;
                }
                if (p_lPageCount == 0)
                {
                    p_lPageCount = 1;
                }
                objDOM = new XmlDocument();
                objElem = (XmlElement)objDOM.CreateNode(XmlNodeType.Element, "diaries", "");
                objElem.SetAttribute("user", p_sUserName);
                objElem.SetAttribute("opendiaries", Conversion.ConvertBoolToInt(p_bOpen, m_iClientId).ToString());
                objElem.SetAttribute("datedue", Conversion.GetDBDateFormat(sDueDate, "d"));
                //Raman Bhatia 06/05/2007 Raman Bhatia: Adding a new attribute to maintain Current Date
                objElem.SetAttribute("currentsystemdate", DateTime.Now.ToShortDateString());
                objElem.SetAttribute("attach_table", p_sAttachTable);
                objElem.SetAttribute("attach_recordid", p_sAttachRecordId);
                objElem.SetAttribute("orderby", iSortOrder.ToString());
                if (p_lPageNumber > 1)
                {
                    objElem.SetAttribute("previouspage", (p_lPageNumber - 1).ToString());
                    objElem.SetAttribute("firstpage", "1");
                }
                if (p_lPageNumber < p_lPageCount)
                {
                    objElem.SetAttribute("nextpage", (p_lPageNumber + 1).ToString());
                    objElem.SetAttribute("lastpage", p_lPageCount.ToString());
                }
                objElem.SetAttribute("pagenumber", p_lPageNumber.ToString());
                objElem.SetAttribute("maxrecord", maxcount.ToString());
                objElem.SetAttribute("currentControl", p_sCurrentScheduleControl);//asharma326 jira 11081
                objElem.SetAttribute("pagecount", p_lPageCount.ToString());
                if (sAllClaimDiaries == "yes")
                    objElem.SetAttribute("showallclaimdiaries", "yes");
                else
                    objElem.SetAttribute("showallclaimdiaries", "no");

                //MITS 11998 Abhishek start
                if (sShowAllDiariesForActiveRec == "yes")
                {
                    objElem.SetAttribute("showalldiariesforactiverecord", "yes");
                }
                else
                {
                    objElem.SetAttribute("showalldiariesforactiverecord", "no");
                }
                //MITS 11998 end
                //tkr checkbox to show/hide notes
                objElem.SetAttribute("shownotes", showNotes);
                //objDOM.Save (@"C:/objDOM.xml");

                //we do not want to throw an exception when there is no record..raman bhatia
                /*
                if (p_lRecordCount == 0)
                {
                    throw new RecordNotFoundException(Globalization.GetString("WPA.GetDiaryDom.NoRecordFound"));
                }
                */
                long uppercount = 0;
                if (p_lPageNumber == 1)
                {
                    lStartAt = 1;
                    uppercount = maxcount;
                }
                else
                {
                    lStartAt = ((p_lPageNumber - 1) * m_lPageSize) + 1;
                }
                lCounter = lStartAt + m_lPageSize;
                if (p_lPageCount > 1)
                    if (p_lPageNumber == p_lPageCount)
                        uppercount = maxcount;
                    else
                        uppercount = lCounter - 1;
                else
                    uppercount = maxcount;
                objElem.SetAttribute("lowercount", lStartAt.ToString());
                objElem.SetAttribute("uppercount", uppercount.ToString());
                objDOM.AppendChild(objElem);
                objElem = null;

                //nitin

                if (p_lRecordCount == 0)
                {
                    return (objDOM);
                }

                //by Nitin


                while (lStartAt != p_lRecordCount + 1)
                //while (lStartAt < lCounter && lStartAt <= p_lRecordCount)
                {
                    iCLnToInt = Conversion.ConvertStrToInteger(lStartAt.ToString());
                    sPIXmlFormName = "";
                    objElem = objDOM.CreateElement("diary");
                    objElem.SetAttribute("entry_id", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ENTRY_ID"]));
                    lCompleteDate = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["COMPLETE_DATE"]));
                    lCreateDate = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["CREATE_DATE"]));

                    //Mukul Added 2/7/2007 MITS 8782

                    sAssignedUser = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ASSIGNED_USER"]).ToLower();
                    sAssigningUser = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ASSIGNING_USER"]).ToLower();
                    if (lCompleteDate < lToday)
                    {
                        if (sAssigningUser == "system")
                        {
                            objElem.SetAttribute("img", IMG_SYSTEM_OVERDUE);
                            objElem.SetAttribute("alt", ALT_SYSTEM);
                        }
                        else if (sAssigningUser == p_sUserName.ToLower())
                        {
                            objElem.SetAttribute("img", IMG_USER_OVERDUE);
                            objElem.SetAttribute("alt", ALT_USER);
                        }
                        else
                        {
                            objElem.SetAttribute("img", IMG_USERS_OVERDUE);
                            objElem.SetAttribute("alt", ALT_USERS);
                        }
                    }
                    else
                    {
                        if (sAssigningUser == "system")
                        {
                            objElem.SetAttribute("img", IMG_SYSTEM);
                            objElem.SetAttribute("alt", ALT_SYSTEM);
                        }
                        else if (sAssigningUser == p_sUserName.ToLower())
                        {
                            objElem.SetAttribute("img", IMG_USER);
                            objElem.SetAttribute("alt", ALT_USER);
                        }
                        else
                        {
                            objElem.SetAttribute("img", IMG_USERS);
                            objElem.SetAttribute("alt", ALT_USERS);
                        }
                    }

                    if (lCompleteDate != 0)
                    {
                        objElem.SetAttribute("complete_date", Conversion.GetDBDateFormat(lCompleteDate.ToString(), "d"));
                    }
                    if (lCreateDate != 0)
                    {
                        objElem.SetAttribute("create_date", Conversion.GetDBDateFormat(lCreateDate.ToString(), "d"));
                    }
                    sIsAttached = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["IS_ATTACHED"]);
                    if (sIsAttached.Equals("0"))
                    {
                        sIsAttached = "0";
                    }
                    else
                    {
                        sIsAttached = "1";
                    }
                    sStatus = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["STATUS_OPEN"]);
                    //Shruti for 10384
                    if (sStatus.Equals("0"))
                    {
                        sStatus = "Closed";
                    }
                    else
                    {
                        sStatus = "Open";
                    }
                    objElem.SetAttribute("status", sStatus);
                    //Mukul Added 2/7/2007 MITS 8782
                    objElem.SetAttribute("assigned_user", sAssignedUser);
                    objElem.SetAttribute("assigning_user", sAssigningUser);
                    objElem.SetAttribute("is_attached", sIsAttached);
                    //nadim 11845 code added for getting diary of admin tracking.
                    sSQL2 = "SELECT DISTINCT SYSTEM_TABLE_NAME FROM GLOSSARY Where GLOSSARY_TYPE_CODE = '468' ";
                    objConn = DbFactory.GetDbConnection(m_sConnectionString);
                    objConn.Open();
                    DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL2);

                    TableValue = new ArrayList();
                    while (objReader.Read())
                    {
                        TableValue.Add(Conversion.ConvertObjToStr(objReader.GetValue("SYSTEM_TABLE_NAME")));

                    }
                    objConn.Close();
                    objReader.Close();
                    sAttachTable = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper();
                    foreach (string s in TableValue)
                    {
                        if (sAttachTable == s)
                        {
                            isAttachTable = true;
                            break;
                        }
                    }
                    if (isAttachTable == true)
                    {
                        sAttachTable = "admintracking" + "|" + sAttachTable;

                        objElem.SetAttribute("attach_table", sAttachTable);
                        sAttachTable = "";
                        isAttachTable = false;
                    }
                    else
                        objElem.SetAttribute("attach_table", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper());
                    //nadim 11845
                    objElem.SetAttribute("attach_recordid", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_RECORDID"]));
                    objElem.SetAttribute("attsecrecid", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_SEC_REC_ID"]));
                    lRid = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_RECORDID"]));
                    string sActivity = "";

                    //Geeta 11/21/06 : Modified to fix mits number 8139 for Sorting Work Activity
                    if (p_iSortOrder == 9 || p_iSortOrder == 13)
                    {
                        sActivity = objRow[iCLnToInt - 1].ItemArray.GetValue(0).ToString();
                    }
                    else
                    {
                        //Raman: R7 Prf Imp
                        string sActivitySQL = "SELECT ACT_TEXT FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID = " + objRow[iCLnToInt - 1]["ENTRY_ID"].ToString();
                        //string sActivitySQL = "SELECT * FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID = " + objRow[iCLnToInt - 1]["ENTRY_ID"].ToString();
                        DbReader objActivity = null;
                        objActivity = DbFactory.GetDbReader(m_sConnectionString, sActivitySQL);
                        if (objActivity != null)
                        {
                            try
                            {
                                if (objActivity.Read())
                                {
                                    sActivity = objActivity.GetString("ACT_TEXT");
                                }
                                else
                                {
                                    sActivity = "";
                                }
                            }
                            finally
                            {
                                objActivity.Close();
                                objActivity.Dispose();
                            }

                        }
                        else
                        {
                            sActivity = "";
                        }
                    }
                    objElem.SetAttribute("work_activity", sActivity);
                    sAttached = "";
                    sClaimStatus = "";
                    sClaimantName = "";
                    sDepartmentName = "";

                    if (lRid > 0)
                    {
                        switch (Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]))
                        {
                            case CLAIM:
                            case "claim":
                                //sAttached = "Claim: " +GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" +lRid.ToString(),m_sConnectionString);
                                //pmittal5 Confidential Record - If Attached claim record is confidential to current user, its diary should not appear                               
                                sAttachID = GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + lRid.ToString(), m_sConnectionString); //Added By Tushar Agarwal for Confidential Record
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                //End
                                sClaimStatus = GetClaimStatus(lRid.ToString());
                                if (sClaimStatus == "")
                                    sClaimStatus = " ";
                                // sClaimantName=GetSingleString("ENTITY.LAST_NAME, ENTITY.FIRST_NAME, PRIMARY_CLMNT_FLAG","CLAIMANT, ENTITY","CLAIMANT.CLAIM_ID ="+lRid.ToString()+" AND CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND CLAIMANT.PRIMARY_CLMNT_FLAG <> 0",m_sConnectionString);
                                sClaimantName = GetClaimantsName(lRid);
                                if (sClaimantName == "")
                                    sClaimantName = " ";
                                sDepartmentName = GetDeptName(0, lRid, ref sOrgLevel);
                                if (sDepartmentName == "")
                                    sDepartmentName = " ";
                                objElem.SetAttribute("claimant", sClaimantName);
                                objElem.SetAttribute("department", sDepartmentName);
                                objElem.SetAttribute("claimstatus", sClaimStatus);
                                break;
                            case EVENT:
                            case "event":
                                sDepartmentName = GetDeptName(lRid, 0, ref sOrgLevel);
                                objElem.SetAttribute("department", sDepartmentName);
                                //sAttached = "Event: " + GetSingleString("EVENT_NUMBER", "EVENT", "EVENT_ID=" +lRid.ToString(),m_sConnectionString);
                                //pmittal5 Confidential Record - If Attached event record is confidential to current user, its diary should not appear
                                sAttachID = GetSingleString("EVENT_NUMBER", "EVENT", "EVENT_ID=" + lRid.ToString(), m_sConnectionString);//Added By Tushar Agarwal for Confidential Record
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                //End
                                break;
                            //case ENTITY:
                            //case "entity":
                            //    objCache.GetEntityInfo(lRid,ref sFname,ref sLname);
                            //    sName=sFname.Trim().Equals("")?sLname:sFname+", "+sLname;
                            //    sAttached = "Person\\Entity:" ;
                            //    break;
                            //case CLAIMANT:
                            //case "claimant":
                            //    objCache.GetEntityInfo(lRid,ref sFname,ref sLname);
                            //    sName=sFname.Trim().Equals("")?sLname:sFname+", "+sLname;
                            //    sAttached = "Claimant:" + sName;
                            //    break;
                            case PERSON_INVOLVED:
                            case "person_involved":
                                sAttached = "";
                                sPIXmlFormName = GetPiXMLFormName(lRid, ref sAttached);
                                //pmittal5 Confidential Record - If Attached Person Involved record is confidential to current user, its diary should not appear
                                sAttachID = sPIXmlFormName;
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            case "funds": //If Attached Funds record is confidential to current user, its diary should not appear
                                sAttachID = GetSingleString("TRANS_ID", "FUNDS", "TRANS_ID=" + lRid.ToString(), m_sConnectionString);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            //rupal:start, r8 auto diary enh                                
                            case RESERVE_HISTORY:
                            case "reserve_history":
                                sAttachID = GetAttachPromptForReserveHistory(lRid.ToString(), out sClaimID);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            case RESERVE_CURRENT:
                            case "reserve_current":
                                sAttachID = GetAttachPromptForReserveReview(lRid.ToString(), out sClaimID);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            case CLAIM_LITIGATION:
                            case LITIGATION:
                            case "claim_x_litigation":
                            case "litigation":
                                sAttachID = GetAttachPromptForLitigation(lRid.ToString());
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            case FUND_AUTO_BATCH:
                            case AUTO_CLAIM_CHECKS:
                            case "funds_auto_batch":
                            case "autoclaimchecks":
                                sAttachID = GetAttachPromptForSchedulePayment(lRid.ToString());
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            /*
                            case COMMENTS_TEXT:
                            case "comments_text":                                
                                string[] sCommentDetail = GetMultipleString("ATTACH_RECORDID,ATTACH_TABLE,COMMENT_ID", "COMMENTS_TEXT", "COMMENT_ID=" + lRid.ToString(), m_sConnectionString);
                                sAttachID = GetAttachPromptForcommentReview(sCommentDetail);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;*/
                            case CLAIM_PRG_NOTE:
                            case "claim_prg_note":
                                sAttachID = "";
                                sProgressNoteDetail = GetMultipleString("CLAIM_ID,EVENT_ID,CL_PROG_NOTE_ID", "CLAIM_PRG_NOTE", "CL_PROG_NOTE_ID=" + lRid.ToString(), m_sConnectionString);
                                sAttachPromptDetail = GetAttachPromptForProgressNotes(sProgressNoteDetail);
                                if (sAttachPromptDetail[0] != "")
                                {
                                    sAttachID = sAttachPromptDetail[0];
                                }
                                else
                                {
                                    sAttachID = "NOREC";
                                }
                                break;
                            //Added by sharishkumar for mits 35291
                            case PIDEPENDENT:
                            case "pidependent":
                                sAttachID = "";
                                sDependentRowDetail = GetSingleString("PI_ROW_ID", "PI_X_DEPENDENT", "PI_DEP_ROW_ID=" + lRid.ToString(), m_sConnectionString);
                                sDependentDetail = GetMultipleString("EVENT_ID,PI_ROW_ID", "PERSON_INVOLVED", "PI_ROW_ID=" + sDependentRowDetail, m_sConnectionString);
                                sAttachPromptDetail = GetAttachPromptForDependent(sDependentDetail);                               
                                if (sAttachPromptDetail[0] != "")
                                {
                                    sAttachID = sAttachPromptDetail[0];
                                }
                                else
                                {
                                    sAttachID = "NOREC";
                                }
                                break;
                            //End mits 35291
                            //scheduled activity for subrogation  
                            case SCHEDULED_ACTIVITY:
                            case "scheduled_activity":
                                sAttachID = GetAttachPromptForScheduledActivity(lRid.ToString(), out sClaimID);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            case CASE_MANAGEMENT:
                            case CMXTREATMENTPLN:
                            case "cm_x_treatment_pln":
                            case "cmxtreatmentpln":
                                sAttachID = GetAttachPropmtForCaseManagement(lRid.ToString());
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            case CLAIMANT:
                            case "claimant":
                                sAttachID = GetSingleString("CLAIMANT_ROW_ID", "CLAIMANT", "CLAIMANT_ROW_ID=" + lRid.ToString(), m_sConnectionString);
                                if (sAttachID == "")
                                    sAttachID = "NOREC";
                                break;
                            //rupal:end                       
                            default: //If Attached PI record is confidential to current user, its diary should not appear
                                sPI = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).Substring(0, 2);
                                if (sPI == "PI")
                                {
                                    sAttachID = GetSingleString("PI_ROW_ID", "PERSON_INVOLVED", "PI_ROW_ID=" + lRid.ToString(), m_sConnectionString);
                                    if (sAttachID == "")
                                        sAttachID = "NOREC";
                                }
                                break;
                            //End - pmittal5
                            //default:
                            //    lTmp = objCache.GetTableId(Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCLnToInt-1]["ATTACH_TABLE"]));
                            //    if (Conversion.ConvertLongToBool(lTmp))
                            //    {
                            //        sAttached = objCache.GetTableName(Conversion.ConvertStrToInteger(lTmp.ToString())) + " (" + lRid.ToString() + ")";
                            //    }
                            //    else
                            //    {
                            //        sAttached =  Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCLnToInt-1]["ATTACH_TABLE"]).ToUpper()+ lRid.ToString();
                            //    }
                            //    break;
                        }
                    }
   
                    if (Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]) != "0")
                    {
                        if (Enum.GetNames(typeof(PARENTCLAIM)).Any(w => w == Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper()) == true)
                        {
                            string sql = "SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID = " + Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]);
                            if (!string.IsNullOrEmpty(Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"])))
                            {
                                DbReader objRd = DbFactory.GetDbReader(m_sConnectionString, sql);

                                using (objRd = DbFactory.GetDbReader(m_sConnectionString, sql))
                                {
                                    if (objRd.Read())
                                    {
                                        //Claim Id is passed for opening the parent claim from DiaryList and Cliam Number is for showing in the DiaryList grid
                                        string claimNumber = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]) + "Æ" + (string)objRd.GetValue("CLAIM_NUMBER") + "Æ" + "claim";
                                        objElem.SetAttribute("parentrecord", claimNumber);
                                    }
                                }
                            }
                        }
                        else if (Enum.GetNames(typeof(PARENTEVENT)).Any(w => w == Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper()) == true)
                        {
                            string sql = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID = " + Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]);
                            DbReader objRd = DbFactory.GetDbReader(m_sConnectionString, sql);

                            using (objRd = DbFactory.GetDbReader(m_sConnectionString, sql))
                            {
                                if (objRd.Read())
                                {
                                    //Event Id is passed for opening the parent event from DiaryList and Event Number is for showing in the DiaryList grid
                                    string eventNumber = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]) + "Æ" + (string)objRd.GetValue("EVENT_NUMBER") + "Æ" + "event";
                                    objElem.SetAttribute("parentrecord", eventNumber);
                                }
                            }
                        }
                        else if (Enum.GetNames(typeof(PARENTPOLICY)).Any(w => w == Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_TABLE"]).ToUpper()) == true)
                        {
                            string sql = "SELECT policy_name FROM POLICY WHERE POLICY_ID = " + Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]);
                            DbReader objRd = DbFactory.GetDbReader(m_sConnectionString, sql);

                            using (objRd = DbFactory.GetDbReader(m_sConnectionString, sql))
                            {
                                string policyNumber = string.Empty;
                                string policyName = string.Empty;
                                if (objRd.Read())
                                {
                                    policyNumber = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATT_PARENT_CODE"]) + "Æ";
                                    policyName = !(objRd.IsDBNull("POLICY_NAME")) ? (string)objRd.GetValue("POLICY_NAME") : string.Empty;
                                    policyNumber = policyNumber + policyName + "Æ" + "policy";
                                    objElem.SetAttribute("parentrecord", policyNumber);

                                }
                            }
                        }
                    }               
                    objElem.SetAttribute("attachprompt", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ATTACH_PROMPT"]));
                    objElem.SetAttribute("pixmlformname", sPIXmlFormName);
                    objElem.SetAttribute("priority", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["PRIORITY"]));
                    objElem.SetAttribute("tasksubject", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ENTRY_NAME"]));
                    objElem.SetAttribute("regarding", Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["REGARDING"]));
                    objElem.InnerText = Conversion.ConvertObjToStr(objRow[iCLnToInt - 1]["ENTRY_NOTES"]);
                    if (sAttachID != "NOREC")  //pmittal5 Confidential Record
                        objDOM.FirstChild.AppendChild(objElem);
                    lStartAt = lStartAt + 1;
                }
                //Adpator layer changes Sumeet
                //sReturnXmlValue=objDOM.InnerXml;
                //objDOM.InnerXml=objDOM.InnerXml.Replace(":","+");
                //objDOM.Save (@"C:/objDOM.xml");
                return (objDOM);
            }
            catch (RecordNotFoundException p_objException)
            {
                throw p_objException;

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetDiaryDom.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objDataSet != null) objDataSet.Dispose();
                if (objCache != null) objCache.Dispose();
                objRow = null;
                objDOM = null;
                objElem = null;
                objUserPrefXML = null; //Umesh
                objXml = null;  //Umesh
                objShowActiveDiary = null; //Umesh
                objSaveShowActiveDiary = null; //Umesh
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
            }
            //Adpator layer changes Sumeet
            //return (sReturnXmlValue);

        }

        //Code merged by Nitin from Safeway to RMX R6 release on 22-July-2009 starts

        //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config
        /// <summary>
        /// Author		: paggarwal2
        /// Date Created: 11/08/2008
        /// Gets the configuration settings for the diaries
        /// SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config
        /// </summary>       
        /// <param name="p_iUserId">int</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetDiaryConfig(int p_iUserId)
        {
            DbReader objReader = null;
            XmlDocument objOutDoc = null;
            XmlElement objRootNode = null;
            string sSQL = string.Empty;
            string sUserPrefsXML = string.Empty;
            XmlDocument objUserPrefXML = null;
            try
            {
                objOutDoc = new XmlDocument();
                objRootNode = objOutDoc.CreateElement("DiaryConfig");
                objOutDoc.AppendChild(objRootNode);

                sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + p_iUserId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }

                XmlNode objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Priority/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.Priority = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Text/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.Text = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AttachedRecord/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.AttachedRecord = objNode.Value.ToString() == "1" ? true : false;
                }
                //Indu - Mits 33843
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ParentRecord/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.ParentRecord = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Claimant/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.Claimant = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimStatus/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.ClaimStatus = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Department/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.Department = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Due/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.Due = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/WorkActivity/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.WorkActivity = objNode.Value.ToString() == "1" ? true : false;
                }
                //igupta3 jira 439
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRoutable/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.NotRoutable = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRollable/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.NotRollable = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/PriorityHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.PriorityHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/TextHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.TextHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AttachedRecordHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.AttachedRecordHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimantHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.ClaimantHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimStatusHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.ClaimStatusHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/DepartmentHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.DepartmentHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/DueHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.DueHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/WorkActivityHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.WorkActivityHeader = objNode.Value.ToString();
                }
                //igupta3 jira 439
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRoutableHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.NotRoutableHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRollableHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.NotRollableHeader = objNode.Value.ToString();
                }
                this.CreateAndSetElement(objRootNode, "Priority", m_Diaryconfig.Priority.ToString());
                this.CreateAndSetElement(objRootNode, "Text", m_Diaryconfig.Text.ToString());
                this.CreateAndSetElement(objRootNode, "WorkActivity", m_Diaryconfig.WorkActivity.ToString());
                //igupta3 jira 439
                this.CreateAndSetElement(objRootNode, "NotRoutable", m_Diaryconfig.NotRoutableHeader.ToString());
                this.CreateAndSetElement(objRootNode, "NotRollable", m_Diaryconfig.NotRollableHeader.ToString());
                this.CreateAndSetElement(objRootNode, "Department", m_Diaryconfig.Department.ToString());
                this.CreateAndSetElement(objRootNode, "Due", m_Diaryconfig.Due.ToString());
                this.CreateAndSetElement(objRootNode, "AttachedRecord", m_Diaryconfig.AttachedRecord.ToString());
                this.CreateAndSetElement(objRootNode, "Claimant", m_Diaryconfig.Claimant.ToString());
                this.CreateAndSetElement(objRootNode, "ClaimStatus", m_Diaryconfig.ClaimStatus.ToString());
                this.CreateAndSetElement(objRootNode, "PriorityHeader", m_Diaryconfig.PriorityHeader.ToString());
                this.CreateAndSetElement(objRootNode, "TextHeader", m_Diaryconfig.TextHeader.ToString());
                this.CreateAndSetElement(objRootNode, "WorkActivityHeader", m_Diaryconfig.WorkActivityHeader.ToString());
                //igupta3 jira 439
                this.CreateAndSetElement(objRootNode, "NotRoutableHeader", m_Diaryconfig.NotRoutableHeader.ToString());
                this.CreateAndSetElement(objRootNode, "NotRollableHeader", m_Diaryconfig.NotRollableHeader.ToString());
                this.CreateAndSetElement(objRootNode, "DepartmentHeader", m_Diaryconfig.DepartmentHeader.ToString());
                this.CreateAndSetElement(objRootNode, "DueHeader", m_Diaryconfig.DueHeader.ToString());
                this.CreateAndSetElement(objRootNode, "AttachedRecordHeader", m_Diaryconfig.AttachedRecordHeader.ToString());
                this.CreateAndSetElement(objRootNode, "ClaimantHeader", m_Diaryconfig.ClaimantHeader.ToString());
                this.CreateAndSetElement(objRootNode, "ClaimStatusHeader", m_Diaryconfig.ClaimStatusHeader.ToString());
                //Indu - Mits 33843
                this.CreateAndSetElement(objRootNode, "ParentRecord", m_Diaryconfig.ParentRecord.ToString());

                //Added by Amitosh for Diary UI changes
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssignedUser/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.AssignedUser = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssigningUser/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.AssigningUser = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssignedUserHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.AssignedUserHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssigningUserHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Diaryconfig.AssigningUserHeader = objNode.Value.ToString();
                }
                this.CreateAndSetElement(objRootNode, "AssignedUser", m_Diaryconfig.AssignedUser.ToString());
                this.CreateAndSetElement(objRootNode, "AssigningUser", m_Diaryconfig.AssigningUser.ToString());
                this.CreateAndSetElement(objRootNode, "AssignedUserHeader", m_Diaryconfig.AssignedUserHeader.ToString());
                this.CreateAndSetElement(objRootNode, "AssigningUserHeader", m_Diaryconfig.AssigningUserHeader.ToString());
                //End Amitosh
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.GetUserPreference.Err", m_iClientId), p_objException);
            }
            finally
            {
                objRootNode = null;
                objUserPrefXML = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return objOutDoc;
        }

        //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config
        /// <summary>
        /// Author		: paggarwal2
        /// Date Created: 11/08/2008 
        /// Sets the configuration for the diaries
        /// SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config
        /// </summary>
        /// <param name="p_objInputDoc">XmlDocument</param>
        public void SetDiaryConfig(XmlDocument p_objInputDoc)
        {
            XmlDocument objUserPrefXML = null;
            DbReader objReader = null;
            string sUserPrefsXML = "";
            string sSQL = "";

            try
            {
                sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + UserId.ToString();
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }
                m_Diaryconfig.Priority = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/Priority"));
                m_Diaryconfig.Text = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/Text"));
                m_Diaryconfig.AttachedRecord = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/AttachedRecord"));
                //Indu - Mits 33843
                m_Diaryconfig.ParentRecord = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/ParentRecord"));
                m_Diaryconfig.Claimant = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/Claimant"));
                m_Diaryconfig.ClaimStatus = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/ClaimStatus"));
                m_Diaryconfig.Department = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/Department"));
                m_Diaryconfig.Due = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/Due"));
                m_Diaryconfig.WorkActivity = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/WorkActivity"));
                //igupta3 jira 439
                m_Diaryconfig.NotRoutable = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/NotRoutable"));
                m_Diaryconfig.NotRollable = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/NotRollable"));
                m_Diaryconfig.PriorityHeader = this.GetValue(p_objInputDoc, "DiaryConfig/PriorityHeader").ToString();
                m_Diaryconfig.TextHeader = this.GetValue(p_objInputDoc, "DiaryConfig/TextHeader").ToString();
                m_Diaryconfig.AttachedRecordHeader = this.GetValue(p_objInputDoc, "DiaryConfig/AttachedRecordHeader").ToString();
                m_Diaryconfig.ClaimantHeader = this.GetValue(p_objInputDoc, "DiaryConfig/ClaimantHeader").ToString();
                m_Diaryconfig.ClaimStatusHeader = this.GetValue(p_objInputDoc, "DiaryConfig/ClaimStatusHeader").ToString();
                m_Diaryconfig.DepartmentHeader = this.GetValue(p_objInputDoc, "DiaryConfig/DepartmentHeader").ToString();
                m_Diaryconfig.DueHeader = this.GetValue(p_objInputDoc, "DiaryConfig/DueHeader").ToString();
                m_Diaryconfig.WorkActivityHeader = this.GetValue(p_objInputDoc, "DiaryConfig/WorkActivityHeader").ToString();
                //igupta3 jira 439
                m_Diaryconfig.NotRoutableHeader = this.GetValue(p_objInputDoc, "DiaryConfig/NotRoutableHeader").ToString();
                m_Diaryconfig.NotRollableHeader = this.GetValue(p_objInputDoc, "DiaryConfig/NotRollableHeader").ToString();
                //Added by Amitosh for Diary UI Change
                m_Diaryconfig.AssignedUser = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/AssignedUser"));
                m_Diaryconfig.AssigningUser = Common.Conversion.ConvertStrToBool((this.GetValue(p_objInputDoc, "DiaryConfig/AssigningUser").ToString()));
                m_Diaryconfig.AssigningUserHeader = this.GetValue(p_objInputDoc, "DiaryConfig/AssigningUserHeader").ToString();
                m_Diaryconfig.AssignedUserHeader = this.GetValue(p_objInputDoc, "DiaryConfig/AssignedUserHeader").ToString();
                //End Amitosh
                CreateUserPreferXML(objUserPrefXML);
                XmlNode objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Priority/@selected");
                objNode.Value = m_Diaryconfig.Priority ? "1" : "0";
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Text/@selected");
                objNode.Value = m_Diaryconfig.Text ? "1" : "0";
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AttachedRecord/@selected");
                objNode.Value = m_Diaryconfig.AttachedRecord ? "1" : "0";
                //Indu - Mits 33843
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ParentRecord/@selected");
                objNode.Value = m_Diaryconfig.ParentRecord ? "1" : "0";
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Claimant/@selected");
                objNode.Value = m_Diaryconfig.Claimant ? "1" : "0";
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimStatus/@selected");
                objNode.Value = m_Diaryconfig.ClaimStatus ? "1" : "0";
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Department/@selected");
                objNode.Value = m_Diaryconfig.Department ? "1" : "0";
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Due/@selected");
                objNode.Value = m_Diaryconfig.Due ? "1" : "0";
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/WorkActivity/@selected");
                objNode.Value = m_Diaryconfig.WorkActivity ? "1" : "0";
                //igupta3 jira 439
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRoutable/@selected");
                objNode.Value = m_Diaryconfig.NotRoutable ? "1" : "0";
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRollable/@selected");
                objNode.Value = m_Diaryconfig.NotRollable ? "1" : "0";

                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/PriorityHeader/@value");
                objNode.Value = m_Diaryconfig.PriorityHeader;
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/TextHeader/@value");
                objNode.Value = m_Diaryconfig.TextHeader;
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AttachedRecordHeader/@value");
                objNode.Value = m_Diaryconfig.AttachedRecordHeader;
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimantHeader/@value");
                objNode.Value = m_Diaryconfig.ClaimantHeader;
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimStatusHeader/@value");
                objNode.Value = m_Diaryconfig.ClaimStatusHeader;
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/DepartmentHeader/@value");
                objNode.Value = m_Diaryconfig.DepartmentHeader;
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/DueHeader/@value");
                objNode.Value = m_Diaryconfig.DueHeader;
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/WorkActivityHeader/@value");
                objNode.Value = m_Diaryconfig.WorkActivityHeader;
                //igupta3 jira 439
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRoutableHeader/@value");
                objNode.Value = m_Diaryconfig.NotRoutableHeader;
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRollableHeader/@value");
                objNode.Value = m_Diaryconfig.NotRollableHeader;

                //Added by Amitosh for Diary UI Change

                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssignedUser/@selected");
                objNode.Value = m_Diaryconfig.AssignedUser ? "1" : "0";
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssigningUser/@selected");
                objNode.Value = m_Diaryconfig.AssigningUser ? "1" : "0";

                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssignedUserHeader/@value");
                objNode.Value = m_Diaryconfig.AssignedUserHeader;
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssigningUserHeader/@value");
                objNode.Value = m_Diaryconfig.AssigningUserHeader;
                //End Amitosh


                this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(UserId));
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.SaveUserPreference.Err", m_iClientId), p_objException);
            }
            finally
            {
                objUserPrefXML = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }

            }
        }
        //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config

        //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config
        /// <summary>
        /// Get Value of the Node.
        /// </summary>
        /// <param name="p_objDocument">Input document.</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <returns>Node Inner Text.</returns>
        private string GetValue(XmlDocument p_objDocument, string p_sNodeName)
        {
            XmlElement objNode = null;
            string sValue = "";

            try
            {
                objNode = (XmlElement)p_objDocument.SelectSingleNode("//" + p_sNodeName);

                if (objNode != null)
                    sValue = objNode.InnerText;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.GetValue.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }
            return (sValue);
        }

        //Code merged by Nitin from Safeway to RMX R6 release on 22-July-2009 ends
        #endregion

        #region Confidential Record
        /// <summary>
        ///pmittal5 Confidential Record
        ///</summary>
        //r7 prf imp
        //public bool IsConfRec(string sAttachTable, long lAttachRecordID, string sAssignedUser, UserLogin objUserLogin)
        public bool IsConfRec(string sAttachTable, long lAttachRecordID, string sAssignedUser, long luserId)
        {
            string sSQL = "";
            string sSQLConf = "";
            bool bIsConf = false;
            bool bConfFlag = false;
            //long luserId = 0;
            string sUserLoginID = string.Empty;
            string sPI = string.Empty;

            try
            {
                //sSQL = "SELECT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME='" + sAssignedUser + "' AND DSNID=" + objUserLogin.objRiskmasterDatabase.DataSourceId;
                //using (DbReader objReader = DbFactory.GetDbReader(m_sSecConnectString, sSQL))
                //{
                //    if (objReader.Read())
                //    {
                //        luserId = Conversion.CastToType<long>(objReader["USER_ID"].ToString(),out bCastToTypeSuccess);
                //        if (!bCastToTypeSuccess)
                //            luserId = 0;
                //    }
                //}


                if (String.IsNullOrEmpty(sAttachTable))
                {
                    return false;
                }

                switch (sAttachTable.ToUpper())
                {
                    case CLAIM:
                        sUserLoginID = ConfRecSupervisor(luserId.ToString());
                        sSQL = "SELECT CLAIM.EVENT_ID FROM CONF_EVENT_PERM,CLAIM WHERE CONF_EVENT_PERM.EVENT_ID=CLAIM.EVENT_ID AND  CLAIM.CLAIM_ID=" + lAttachRecordID + " AND CONF_EVENT_PERM.USER_ID IN(" + sUserLoginID + " )AND CONF_EVENT_PERM.DELETED_FLAG=0";
                        sSQLConf = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_ID=" + lAttachRecordID + " AND CONF_FLAG<>0";
                        break;
                    case EVENT:
                        sUserLoginID = ConfRecSupervisor(luserId.ToString());
                        sSQL = "SELECT CONF_EVENT_PERM.EVENT_ID FROM CONF_EVENT_PERM WHERE CONF_EVENT_PERM.EVENT_ID=" + lAttachRecordID + " AND CONF_EVENT_PERM.USER_ID IN (" + sUserLoginID + " ) AND CONF_EVENT_PERM.DELETED_FLAG=0";
                        sSQLConf = "SELECT EVENT_ID FROM EVENT WHERE EVENT_ID=" + lAttachRecordID + " AND CONF_FLAG<>0";
                        break;
                    case FUNDS:
                        sUserLoginID = ConfRecSupervisor(luserId.ToString());
                        sSQL = "SELECT CLAIM.EVENT_ID FROM CONF_EVENT_PERM,CLAIM,FUNDS WHERE CONF_EVENT_PERM.EVENT_ID=CLAIM.EVENT_ID AND CLAIM.CLAIM_ID=FUNDS.CLAIM_ID AND FUNDS.TRANS_ID=" + lAttachRecordID + " AND CONF_EVENT_PERM.USER_ID IN (" + sUserLoginID + " )  AND CONF_EVENT_PERM.DELETED_FLAG=0";
                        sSQLConf = "SELECT TRANS_ID FROM FUNDS WHERE TRANS_ID=" + lAttachRecordID + " AND CONF_FLAG<>0";
                        break;
                    default:
                        if (sAttachTable.Length > 0)
                        {
                            sPI = sAttachTable.ToUpper().Substring(0, 2);
                            if (sPI == "PI")
                            {
                                sUserLoginID = ConfRecSupervisor(luserId.ToString());
                                sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID FROM PERSON_INVOLVED,CONF_EVENT_PERM WHERE CONF_EVENT_PERM.EVENT_ID=PERSON_INVOLVED.EVENT_ID AND PERSON_INVOLVED.PI_ROW_ID=" + lAttachRecordID + " AND CONF_EVENT_PERM.USER_ID IN (" + sUserLoginID + " )  AND CONF_EVENT_PERM.DELETED_FLAG=0";
                                sSQLConf = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PI_ROW_ID=" + lAttachRecordID + " AND CONF_FLAG<>0";
                            }
                        }
                        break;
                }

                if (sSQLConf.Length > 0)
                {
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLConf))
                    {
                        if (objReader.Read())
                            bConfFlag = true;
                    }

                    if (bConfFlag)
                    {
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (objReader.Read())
                                bIsConf = false;
                            else
                                bIsConf = true;
                        }

                    }
                }
            }
            catch (Exception p_objException)
            {
                throw (p_objException);
            }
            finally
            {

            }
            return (bIsConf);
        }

        //pmittal5 - Obtain Subordinate list of users for current User Id
        private string ConfRecSupervisor(string p_sUserId)
        {
            DbReader objRdManager = null;
            String sSQL = String.Empty;
            String sManagerID = String.Empty;
            String sUserID = String.Empty;
            String sTmp = String.Empty;
            String[] sArrUsers;

            sManagerID = p_sUserId;
            try
            {
                while (sManagerID != "")
                {
                    sSQL = "SELECT USER_ID FROM USER_TABLE WHERE MANAGER_ID IN (" + sManagerID + ")";
                    using (objRdManager = DbFactory.GetDbReader(m_sSecConnectString, sSQL))
                    {
                        sUserID = sManagerID + "," + sUserID;
                        sManagerID = "";
                        if (objRdManager != null)
                        {
                            while (objRdManager.Read())
                            {
                                sTmp = "";
                                sTmp = Conversion.ConvertObjToStr(objRdManager.GetValue("USER_ID"));

                                sArrUsers = sUserID.Split(',');
                                for (int iCount = 0; iCount < sArrUsers.GetUpperBound(0); iCount++)
                                {
                                    if (sArrUsers[iCount] == sTmp)
                                    {
                                        sTmp = "";
                                        break;
                                    }
                                }
                                if (sTmp != "")
                                    sManagerID = sTmp + "," + sManagerID;
                            }
                        }
                    }
                    sManagerID = sManagerID.TrimEnd(',');
                }
                sUserID = sUserID.TrimEnd(',');
            }
            catch (Exception p_objExp)
            {
                throw (p_objExp);
            }
            finally
            {
            }
            return sUserID;
        }
        #endregion

        #region Get Number of Records Per Page
        //Sachin:Start changes for 27074-custom paging
        /// <summary>
        /// Function added to fetch number of records per page for diary list 
        /// </summary>
        /// <returns></returns>
        public int GetRecordsPerPage()
        {
            StringBuilder sbSQL = null;
            int iReturn = 0;
            DbReader objReader = null;

            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE");
                sbSQL.Append(" WHERE PARM_NAME = 'DIARYLIST_USER_LIMIT' ");
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader.Read())
                {
                    iReturn = Conversion.ConvertObjToInt(objReader.GetValue("STR_PARM_VALUE"), m_iClientId);
                    if (iReturn < 10)
                        iReturn = 10;//Record per Page should greater than equal to 10
                }
                else
                {
                    //iReturn=0;
                    iReturn = 10;//Record per Page should greater than equal to 10
                }
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                }
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                sbSQL = null;
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                    //Shivendu to dispose the object
                    objReader.Dispose();
                }
                objReader = null;
            }
            return iReturn;
        }
        //Sachin:End changes for custom paging
        #endregion
        //RUPAL:START R8 AUTO DIARY ENH

        #region Common Function  required for new templates in r8
        //rupal:start, r8 auto diary enh
        /// <summary>
        /// this function is used to set attach reord, and other claim details in xml for
        /// auto diary templates who need to be redirected to claim, for e.g, reserve history, reserve review
        /// </summary>
        /// <param name="sClaimID">ClaimID</param>
        /// <param name="objElem">XML element where attributes need to be set</param>
        private void SetCommonClaimDetails(string sClaimID, XmlElement objElem)
        {
            bool bConverted = false;
            long lClaimID = 0;
            string sOrgLevel = string.Empty;
            //ATTACH_RECIRDID_FOR_NAV - Attach record id for MDI navigation.
            //for e.g, if diary is created for claim_x_litigation, attach_record_id would be LITIGATION_ROW_ID
            //but we need claim id to allow user to navigate to the record from diary list                                                   
            objElem.SetAttribute("attach_recordid_for_nav", sClaimID);
            objElem.SetAttribute("attach_table_for_nav", "claim");
            //objElem.SetAttribute("attach_recordid", sClaimID);

            //dept name
            lClaimID = Conversion.CastToType<long>(sClaimID, out bConverted);
            if (bConverted)
            {
                string sDepartmentName = GetDeptName(0, lClaimID, ref sOrgLevel);
                if (sDepartmentName == "")
                    sDepartmentName = " ";
                objElem.SetAttribute("department", sDepartmentName);
                //claim status
                string sClaimStatus = GetClaimStatus(lClaimID.ToString());
                if (sClaimStatus == "")
                    sClaimStatus = " ";
                objElem.SetAttribute("claimstatus", sClaimStatus);
                //claimant name
                string sClaimantName = GetClaimantsName(lClaimID);
                if (sClaimantName == "")
                    sClaimantName = " ";
                objElem.SetAttribute("claimant", sClaimantName);
            }
        }

        /// <summary>
        /// To get attach propmt for Comment Review Auto Diary Template
        /// </summary>
        /// <param name="sCommentDetail">first array element is expected to be attach record id,
        /// secoud array element is expected to be attach table
        /// thrid array element is expected to be comment id
        /// </param>
        /// <returns></returns>
        private string GetAttachPromptForcommentReview(string[] sCommentDetail)
        {
            string sAttachedPrompt = string.Empty;
            if (sCommentDetail.Length >= 2)
            {
                //AS OF NOW ONLY TWO TABLES HAVE COMMENTS
                switch (sCommentDetail[1])//Attach Table
                {
                    case CLAIM:
                        sAttachedPrompt = "Comment For Claim: " + GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + sCommentDetail[0], m_sConnectionString);
                        break;
                    case EVENT:
                        sAttachedPrompt = "Comment For Event: " + GetSingleString("EVENT_NUMBER", "EVENT", "EVENT_ID=" + sCommentDetail[1], m_sConnectionString);
                        break;
                    default:
                        sAttachedPrompt = "Comment Review - " + sCommentDetail[2];
                        break;
                }
            }
            return sAttachedPrompt;
        }

        /// <summary>
        /// To get attach propmt,attach table, attatch record id for Progress Notes Auto Diary Template
        /// </summary>
        /// <param name="sProgressNoteDetail">first array element is expected to be claim_id,
        /// secoud array element is expected to be event_id
        /// thrid array element is expected to be cl_progress_note_id
        /// </param>
        /// <returns>string[]
        /// string[0] returns attach propmt, will return blank in worst condition
        /// string[1] returns attach table
        /// string[2] returns attach record id
        /// </returns>
        private string[] GetAttachPromptForProgressNotes(string[] sProgressNoteDetail)
        {
            string[] sAttachedPrompt = new string[3];

            sAttachedPrompt[0] = string.Empty;
            if (sProgressNoteDetail.Length >= 2)
            {
                //AS OF NOW ONLY TWO TABLES HAVE Progress notes (Claim and event)
                //sProgressNoteDetail[0] contains claim id
                //sProgressNoteDetail[0] contains event id
                if (!string.IsNullOrEmpty(sProgressNoteDetail[0]) && sProgressNoteDetail[0] != "0")
                {
                    sAttachedPrompt[0] = "Progress Note For Claim: " + GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + sProgressNoteDetail[0], m_sConnectionString);
                    sAttachedPrompt[1] = "claim"; //attach table
                    sAttachedPrompt[2] = sProgressNoteDetail[0]; //attact record id
                }
                else if (!string.IsNullOrEmpty(sProgressNoteDetail[1]) && sProgressNoteDetail[1] != "0")
                {
                    sAttachedPrompt[0] = "Progress Note For Event: " + GetSingleString("EVENT_NUMBER", "EVENT", "EVENT_ID=" + sProgressNoteDetail[1], m_sConnectionString);
                    sAttachedPrompt[1] = "event"; //attact table
                    sAttachedPrompt[2] = sProgressNoteDetail[1]; //attach record
                }
            }
            return sAttachedPrompt;
        }
        //Added by sharishkumar for mits 35291
        /// <summary>
        /// To get attach propmt,attach table, attach record id for Dependent
        /// </summary>
        /// <param name="sDependentDetail">first array element is expected to be event_id,
        /// secoud array element is expected to be pi_row_id       
        /// </param>
        /// <returns>string[]
        /// string[0] returns attach prompt, will return blank in worst condition
        /// string[1] returns attach table
        /// string[2] returns attach record id
        /// </returns>
        private string[] GetAttachPromptForDependent(string[] sDependentDetail)
        {
            string[] sAttachedPrompt = new string[3];

            sAttachedPrompt[0] = string.Empty;
            if (sDependentDetail.Length >= 1)
            {                            
                //sDependentDetail[0] contains event id               
                if (!string.IsNullOrEmpty(sDependentDetail[0]) && sDependentDetail[0] != "0")
                {
                    sAttachedPrompt[0] = "Dependent: " + GetSingleString("CLAIM_NUMBER", "CLAIM", "EVENT_ID=" + sDependentDetail[0], m_sConnectionString);
                    sAttachedPrompt[1] = "pidependent"; //attach table
                    sAttachedPrompt[2] = sDependentDetail[1]; //attach record
                }
            }
            return sAttachedPrompt;
        }
        //End mits 35291

        /// <summary>
        /// To get attach propmt,attach table, attatch record id for Case Management Auto Diary Template
        /// </summary>
        /// <param name="sRowID">CMTP_RowID</param>
        /// <returns>
        /// attach Promt for Case Mgt
        /// </returns>
        private string GetAttachPropmtForCaseManagement(string sRowID)
        {
            //CM_X_TREATMENT_PLAN contains CASEMANAGEMENT_ROWID
            //based on this CASEMANAGEMENT_ROWID, we can identify if case mgt was for claim or person involved

            string sAttachedPrompt = string.Empty;
            bool bConverted = false;

            string sCaseMgtRowID = GetSingleString("CASEMGT_ROW_ID", "CM_X_TREATMENT_PLN", "CMTP_ROW_ID=" + sRowID, m_sConnectionString);
            string[] sCaseMgtDetail = null;

            // RMA-6445 - Error occurs on the load of Diary list if the Attached Record is missing in DB
            // We now try to retrieve the details of the Attached Record if we have a Validrecord attached with it
            if (!string.IsNullOrEmpty(sCaseMgtRowID))
                sCaseMgtDetail = GetMultipleString("CLAIM_ID,PI_ROW_ID", "CASE_MANAGEMENT", "CASEMGT_ROW_ID=" + sCaseMgtRowID, m_sConnectionString);

            if (sCaseMgtDetail != null)
            {
                if (sCaseMgtDetail.Length > 0 && !string.IsNullOrEmpty(sCaseMgtDetail[0]) && sCaseMgtDetail[0] != "0")
                {
                    sAttachedPrompt = "Case Mgt,Claim : " + GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + sCaseMgtDetail[0], m_sConnectionString);
                }
                else if (sCaseMgtDetail.Length > 1 && !string.IsNullOrEmpty(sCaseMgtDetail[1]) && sCaseMgtDetail[1] != "0")//sCaseMgtDetail[1] = PiRowID
                {
                    string sPIXMLFornName = GetPiXMLFormName(Conversion.CastToType<long>(sCaseMgtDetail[1], out bConverted), ref sAttachedPrompt);
                    sAttachedPrompt = "Case Mgt," + sAttachedPrompt;
                }
            }
            return sAttachedPrompt;
        }

        /// <summary>
        /// To get attach propmt for litigation
        /// </summary>
        /// <param name="sRowID">row Id</param>
        /// <returns>
        /// Attach Prompt
        /// </returns>
        private string GetAttachPromptForLitigation(string sRowID)
        {
            //get Claim_ID from CLAIM_X_LITIGATION table 
            string[] sLitigationDetail = GetMultipleString("LITIGATION_ROW_ID,DOCKET_NUMBER", "CLAIM_X_LITIGATION", "LITIGATION_ROW_ID=" + sRowID, m_sConnectionString);
            //get claim number based on claim_id
            string sAttached = string.Empty;
            string sdocketNumber = string.Empty;
            if (!string.IsNullOrEmpty(sLitigationDetail[0]))
            {
                if (string.IsNullOrEmpty(sLitigationDetail[1]))
                    sAttached = "Claim Litigation: Docket Number : NA";
                else
                    sAttached = "Claim Litigation: Docket Number : " + sLitigationDetail[1];
            }
            return sAttached;
        }

        /// <summary>
        /// To get attach propmt for Schedule Payment
        /// </summary>
        /// <param name="sRowID">row Id</param>
        /// <returns>
        /// Attach Prompt
        /// </returns>
        private string GetAttachPromptForSchedulePayment(string sRowID)
        {
            //get CTL_NUMBER from FUNDS_AUTO
            string sAttached = string.Empty;
            sAttached = GetSingleString("CTL_NUMBER", "FUNDS_AUTO", "AUTO_BATCH_ID=" + sRowID, m_sConnectionString);
            if (sAttached != string.Empty)
                sAttached = "Scheduled Payment: " + sAttached;
            return sAttached;
        }

        /// <summary>
        /// To get attach propmt for Reserve review
        /// </summary>
        /// <param name="sRowID">row Id</param>
        /// <returns>
        /// Attach Prompt
        /// </returns>
        private string GetAttachPromptForReserveReview(string sRowID, out string sClaimID)
        {
            string sAttached = string.Empty;
            //get Claim_ID from RESERVE_HISTORY table 
            sClaimID = GetSingleString("CLAIM_ID", "RESERVE_CURRENT", "RC_ROW_ID=" + sRowID, m_sConnectionString);
            //get claim number absed on claim_id      
            if (sClaimID != string.Empty)
            {
                sAttached = GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + sClaimID, m_sConnectionString);
                if (sAttached != string.Empty)
                {
                    sAttached = "Reserve Review for Claim: " + sAttached;
                }
            }
            return sAttached;
        }

        /// <summary>
        /// To get attach propmt for Reserve history
        /// </summary>
        /// <param name="sRowID">row Id</param>
        /// <returns>
        /// Attach Prompt
        /// </returns>
        private string GetAttachPromptForReserveHistory(string sRowID, out string sClaimID)
        {
            string sAttached = string.Empty;
            //get Claim_ID from RESERVE_HISTORY table 
            sClaimID = GetSingleString("CLAIM_ID", "RESERVE_HISTORY", "RSV_ROW_ID=" + sRowID, m_sConnectionString);
            if (sClaimID != string.Empty)
            {
                //get claim number absed on claim_id                
                sAttached = GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + sClaimID, m_sConnectionString);
                if (sAttached != string.Empty)
                {
                    sAttached = "Reserve History for Claim: " + sAttached;
                }
            }
            return sAttached;
        }

        /// <summary>
        /// To get attach propmt for Reserve history
        /// </summary>
        /// <param name="sRowID">row Id</param>
        /// <returns>
        /// Attach Prompt
        /// </returns>
        private string GetAttachPromptForScheduledActivity(string sRowID, out string sClaimID)
        {
            string sAttached = string.Empty;
            //get Claim_ID from RESERVE_HISTORY table 
            sClaimID = GetSingleString("CLAIM_ID", "SCHEDULED_ACTIVITY", "SCHEDULED_ACT_ROW_ID=" + sRowID, m_sConnectionString);
            //get claim number absed on claim_id
            if (sClaimID != string.Empty)
            {
                sAttached = GetSingleString("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + sClaimID, m_sConnectionString);
                if (sAttached != string.Empty)
                    sAttached = "Scheduled Activity For Subrogation,Claim: " + sAttached;
            }
            return sAttached;
        }
        //RUPAL:END
        //rupal:end
        #endregion

        private string ModifyFilterexp(string s_FilterExp, ref string s_dtFexp)
        {
            string dsubfilter = string.Empty;
            string s_DtOrgfilter = string.Empty;
            DbReader objRead = null;
            if (s_FilterExp.Contains("[COMPLETE_DATE]"))
            {
                dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[COMPLETE_DATE]"));
                s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter);
                if (s_FilterExp.Contains("AND"))
                    s_FilterExp = dsubfilter.Substring((dsubfilter.IndexOf("AND")) + 5);
                s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, " ").Replace("( )", "");
                if (!s_FilterExp.Contains("[ASSIGNED_USER]"))
                {
                    s_FilterExp = s_FilterExp.Replace(")", "");//27074
                    s_FilterExp = s_FilterExp.Replace("(", "");//27074
                }
                else
                {
                    s_FilterExp = "(" + s_FilterExp;
                }

            }
            // npadhy JIRA 6412 Adding Filter capability for Groups
            if (s_FilterExp.Contains("[ASSIGNED_USER]"))
            {
                int iIndexOfFilter = s_FilterExp.IndexOf("[ASSIGNED_USER]");

                string sGroupName = s_FilterExp.Substring(s_FilterExp.IndexOf("LIKE", iIndexOfFilter));
                sGroupName = sGroupName.Substring(0, sGroupName.IndexOf(")"));

                // Get the Group ID from Group Name
                string sSql = "SELECT GROUP_ID FROM USER_GROUPS WHERE GROUP_NAME " + sGroupName;

                // This is thelocation where we will add our filter for Group
                iIndexOfFilter = s_FilterExp.IndexOf(")", iIndexOfFilter);

                string groupIds = " OR [ASSIGNED_GROUP] IN (";
                bool bFound = false;

                using (objRead = DbFactory.GetDbReader(m_sConnectionString, sSql))
                {
                    if (objRead != null)
                    {
                        while (objRead.Read())
                        {
                            groupIds = groupIds + "'" + objRead["GROUP_ID"] + "',";
                            bFound = true;
                        }
                    }
                }
                if (bFound)
                {
                    groupIds = groupIds.Substring(0, groupIds.Length - 1) + ")";
                    s_FilterExp = s_FilterExp.Substring(0, iIndexOfFilter) + groupIds + s_FilterExp.Substring(iIndexOfFilter, s_FilterExp.Length - iIndexOfFilter);
                }
            }


            return s_FilterExp;
        }
        private string GetDateStrng(string s_DtSubfilter, out string s_Dtorgfilter)
        {

            s_Dtorgfilter = s_DtSubfilter.Substring(0, s_DtSubfilter.IndexOf(")"));

            string s_Dtorgsubfilter = s_Dtorgfilter.Substring(s_Dtorgfilter.IndexOf("]") + 1);

            string dexp = s_DtSubfilter.Substring((s_DtSubfilter.IndexOf("'")) + 1);
            string dfexp = dexp.Substring(0, dexp.IndexOf("'"));

            return dfexp;


        }
       /// <summary>
       /// Ankit-MITS 30085
       /// </summary>
       /// <param name="objDataSet"></param>
       /// <param name="p_iSortOrder"></param>
       /// <returns></returns>

       public string GetSortColumnName(int p_iSortOrder)
       {
           string sSortColumnName = string.Empty;
           #region original code
           if (p_iSortOrder == 1)
           {
               sSortColumnName = "COMPLETE_DATE ASC";
           }

           if (p_iSortOrder == 5)
           {
               sSortColumnName = "COMPLETE_DATE DESC";
           }

           if (p_iSortOrder == 2)
           {
               //sSortColumnName = "ATTCH_PROMPT ASC";
               sSortColumnName = "IS_ATTACHED ASC, ATTACH_TABLE ASC, ATTACH_RECORDID ASC";
           }

           if (p_iSortOrder == 6)
           {
               //sSortColumnName = "ATTACH_PROMPT DESC";               
               sSortColumnName = "IS_ATTACHED DESC, ATTACH_TABLE DESC, ATTACH_RECORDID DESC";
           }
           
           if (p_iSortOrder == 10)
           {
               sSortColumnName = "CLAIMANT ASC";               
           }

           if (p_iSortOrder == 14)
           {
               sSortColumnName = "CLAIMANT DESC";               
           }
           
           if (p_iSortOrder == 9)
           {               
               sSortColumnName = "work_act ASC";
           }
           
           if (p_iSortOrder == 13)
           {
               sSortColumnName = "work_act DESC";
           }

           if (p_iSortOrder == 3)
           {
               sSortColumnName = "ENTRY_NAME ASC";
           }

           if (p_iSortOrder == 7)
           {
               sSortColumnName = "ENTRY_NAME DESC";
           }

           if (p_iSortOrder == 4)
           {
               sSortColumnName = "PRIORITY ASC";
           }

           if (p_iSortOrder == 8)
           {
               sSortColumnName = "PRIORITY DESC";
           }
           //hlv MITS 29594 11/6/12 begin
           if (p_iSortOrder == 15)
           {
	           sSortColumnName = "DEPARTMENT ASC";
           }

           if (p_iSortOrder == 16)
           {
               sSortColumnName = "DEPARTMENT DESC";
           }

           if (p_iSortOrder == 17)
           {
	           sSortColumnName = "CLAIMSTATUS ASC";
           }

           if (p_iSortOrder == 18)
           {
	           sSortColumnName = "CLAIMSTATUS DESC";
           }
           //Ankit-27074-Start
           if(p_iSortOrder == 19)
           {
               sSortColumnName = "ASSIGNED_USER ASC";
           }

           if(p_iSortOrder == 20)
           {
               sSortColumnName = "ASSIGNED_USER DESC";
           }

           if(p_iSortOrder == 21)
           {
               sSortColumnName = "ASSIGNING_USER ASC";
           }

           if(p_iSortOrder == 22)
           {
               sSortColumnName = "ASSIGNING_USER DESC";
           }
           //MITS 36519
           if (p_iSortOrder == 23)
           {
               sSortColumnName = "ATT_PARENT_CODE ASC";
           }

           if (p_iSortOrder == 24)
           {
               sSortColumnName = "ATT_PARENT_CODE DESC";
           }
           //Ankit 27074-End
           //igupta3 jira 439
           if (p_iSortOrder == 25)
           {
               sSortColumnName = "NON_ROUTE_FLAG ASC";
           }

           if (p_iSortOrder == 26)
           {
               sSortColumnName = "NON_ROUTE_FLAG DESC";
           }
           if (p_iSortOrder == 27)
           {
               sSortColumnName = "NON_ROLL_FLAG ASC";
           }

           if (p_iSortOrder == 28)
           {
               sSortColumnName = "NON_ROLL_FLAG DESC";
           }
           #endregion
           return sSortColumnName;
       }

       public string GetDiaryOrgName(long p_lThisOrgLevel)
       {
           string sOrgHieColName = string.Empty;
           switch (p_lThisOrgLevel)
           {
               case 1005:   //client
                   sOrgHieColName = "CLIENT_EID";
                   break;
               case 1006:   //company
                   sOrgHieColName = "COMPANY_EID";
                   break;
               case 1007:   //operation
                   sOrgHieColName = "OPERATION_EID";
                   break;
               case 1008:   //region
                   sOrgHieColName = "REGION_EID";
                   break;
               case 1009:   //division
                   sOrgHieColName = "DIVISION_EID";
                   break;
               case 1010:   //location
                   sOrgHieColName = "LOCATION_EID";
                   break;
               case 1011:   //facility
                   sOrgHieColName = "FACILITY_EID";
                   break;
               case 1012:   //department
                   sOrgHieColName = "DEPARTMENT_EID";
                   break;
               default:
                   sOrgHieColName = "DEPARTMENT_EID";
                   break;
           }
           return sOrgHieColName;
       }
        /// <summary>
        /// To get the parent record id of diary
        /// </summary>
        /// <param name="attachRecord">Attach table</param>
        /// <param name="attachRecordId">attachRecordId</param>
        /// <returns>parent record id</returns>
       public int GetParentRecord(string attachRecord,int attachRecordId)
       {
           string sSQL = string.Empty;
           DbReader objRead = null;
           int parentRecordId = 0;
           switch (attachRecord)
           {
               /* Event as Parent Record category */
               case "EVENTDATEDTEXT":
                   sSQL = "select event_id from EVENT_X_DATED_TEXT where ev_dt_row_id =" + attachRecordId;
                   break;
               case "OSHA":
               case "FALLINFO":
               case "MEDWATCHTEST":
               case "MEDWATCH":
                   parentRecordId = attachRecordId;
                   break;
               case "PERSON_INVOLVED":
               case "PIPHYSICIAN":
               case "PIMEDSTAFF":
               case "PIEMPLOYEE":
               case "PIPATIENT":
               case "PIDRIVER":
               case "PIWITNESS":
               case "PIOTHER":
               case "PIDEPENDENT":
                   sSQL = "select event_id as EVENT from PERSON_INVOLVED where pi_row_id=" + attachRecordId;
                   break;
               case "CONCOMITANT":
                   sSQL = "select event_id from EV_X_CONCOM_PROD where EV_CONCOM_ROW_ID = " + attachRecordId;
                   break;
               case "CASEMGRNOTES":
                   sSQL = string.Format("SELECT EVENT_ID FROM PERSON_INVOLVED JOIN CASE_MANAGEMENT ON PERSON_INVOLVED.PI_ROW_ID = CASE_MANAGEMENT.PI_ROW_ID JOIN CM_X_CMGR_HIST ON CASE_MANAGEMENT.CASEMGT_ROW_ID = CM_X_CMGR_HIST.CASEMGT_ROW_ID JOIN CASE_MGR_NOTES ON CM_X_CMGR_HIST.CMCMH_ROW_ID = CASE_MGR_NOTES.CASEMGR_ROW_ID WHERE CASE_MGR_NOTES.CMGR_NOTES_ROW_ID ={0}",attachRecordId);
                   break;
               /* Claim as Parent Record category */
               case SUBROGATION:
               case "SCHEDULED_ACTIVITY":
                   sSQL = "select claim_id from claim_x_subro where subrogation_row_id = " + attachRecordId;
                   break;
               case CLAIMANT:
                   sSQL = "select claim_id from claimant where claimant_row_id =" + attachRecordId;
                   break;
               case "CLAIM_X_LITIGATION":
               case LITIGATION:
                   sSQL = "select claim_id from CLAIM_X_LITIGATION where litigation_row_id = " + attachRecordId;
                   break;
               case FUND_AUTO_BATCH:
               case "FUNDS":
                   sSQL = " select claim_id from funds where trans_id = " + attachRecordId;
                   break;
               case AUTO_CLAIM_CHECKS:
                   sSQL = " select claim_id from FUNDS_AUTO where AUTO_batch_ID =  " + attachRecordId;
                   break;
               case "CM_X_TREATMENT_PLN":
               case CMXTREATMENTPLN:
                   //rma 1598
                   //sSQL = "select claim_id from CM_X_TREATMENT_PLN tp INNER JOIN CASE_MANAGEMENT cm on tp.CASEMGT_ROW_ID = cm.CASEMGT_ROW_ID  where cmtp_row_id = " + attachRecordId;
                   sSQL = string.Format("SELECT EVENT_ID FROM PERSON_INVOLVED JOIN CASE_MANAGEMENT ON PERSON_INVOLVED.PI_ROW_ID = CASE_MANAGEMENT.PI_ROW_ID JOIN CM_X_TREATMENT_PLN  ON CM_X_TREATMENT_PLN.CASEMGT_ROW_ID = CASE_MANAGEMENT.CASEMGT_ROW_ID where cmtp_row_id ={0}", attachRecordId);
                   break;
               case "ADJUSTER":
                   sSQL = "select claim_id from CLAIM_ADJUSTER where adj_row_id = " + attachRecordId;
                   break;
               //RMA 1598
               case "ADJUSTERDATEDTEXT":
                   sSQL = "select claim_id from ADJUST_DATED_TEXT adt INNER JOIN CLAIM_ADJUSTER adj on adj.ADJ_ROW_ID = adt.ADJ_ROW_ID where adt.ADJ_DTTEXT_ROW_ID =" + attachRecordId;
                   break;
               case "CMXMEDMGTSAVINGS"://MITS 35390
                   //sSQL = "select claim_id from CM_X_MEDMGTSAVINGS tp INNER JOIN CASE_MANAGEMENT cm on tp.CASEMGT_ROW_ID = cm.CASEMGT_ROW_ID  where cmms_row_id = " + attachRecordId;
                   sSQL = string.Format("SELECT EVENT_ID FROM PERSON_INVOLVED JOIN CASE_MANAGEMENT ON PERSON_INVOLVED.PI_ROW_ID = CASE_MANAGEMENT.PI_ROW_ID JOIN CM_X_MEDMGTSAVINGS ON CM_X_MEDMGTSAVINGS.CASEMGT_ROW_ID = CASE_MANAGEMENT.CASEMGT_ROW_ID WHERE CM_X_MEDMGTSAVINGS.CMMS_ROW_ID = {0}", attachRecordId);
                   break;
               case "PIRESTRICTION":
                   sSQL = "select claim_id from CLAIM where claim_id  = ( select claim_id from PI_X_RESTRICT pr INNER JOIN PERSON_INVOLVED pin ON pr.pi_row_id = pin.pi_row_id INNER JOIN CASE_MANAGEMENT cm ON cm.PI_ROW_ID = pin.pi_row_id where pi_restrict_row_id =" + attachRecordId + ")";
                   break;
               case "PIWORKLOSS":
                   sSQL = "select claim_id from CLAIM where claim_id  = ( select claim_id from PI_X_WORK_LOSS pw INNER JOIN PERSON_INVOLVED pin ON pw.pi_row_id = pin.pi_row_id INNER JOIN CASE_MANAGEMENT cm ON cm.PI_ROW_ID = pin.pi_row_id where pi_wl_row_id =" + attachRecordId + " )";
                   break;
               case "LEAVE":
                   sSQL = "select claim_id from CLAIM_LEAVE where leave_row_id = " + attachRecordId;
                   break;
               case "UNIT":
                   sSQL = "select claim_id from UNIT_X_CLAIM where unit_row_id = " + attachRecordId;
                   break;
               case "PROPERTYLOSS":
                   sSQL = "select claim_id from CLAIM_X_PROPERTYLOSS  where property_id = " + attachRecordId;
                   break;
              case "CMXACCOMMODATION":
                   //rma 1598
                   //sSQL = "select claim_id from CM_X_ACCOMMODATION ca INNER JOIN CASE_MANAGEMENT cm on ca.cm_row_id = casemgt_row_id where cm_accomm_row_id =" + attachRecordId;
                   sSQL = string.Format("SELECT EVENT_ID FROM PERSON_INVOLVED JOIN CASE_MANAGEMENT ON PERSON_INVOLVED.PI_ROW_ID = CASE_MANAGEMENT.PI_ROW_ID JOIN CM_X_ACCOMMODATION ON CASE_MANAGEMENT.CASEMGT_ROW_ID = CM_X_ACCOMMODATION.CM_ROW_ID WHERE CM_X_ACCOMMODATION.CM_ACCOMM_ROW_ID =  {0}", attachRecordId);
                   break;
               case "CMXVOCREHAB":
                   //sSQL = "select claim_id from CM_X_VOCREHAB cv INNER JOIN CASE_MANAGEMENT cm on cv.casemgt_row_id = cm.casemgt_row_id where cmvr_row_id = " + attachRecordId;
                   sSQL = string.Format("SELECT EVENT_ID FROM PERSON_INVOLVED JOIN CASE_MANAGEMENT ON PERSON_INVOLVED.PI_ROW_ID = CASE_MANAGEMENT.PI_ROW_ID JOIN CM_X_VOCREHAB ON CASE_MANAGEMENT.CASEMGT_ROW_ID = CM_X_VOCREHAB.CASEMGT_ROW_ID WHERE CM_X_VOCREHAB.CMVR_ROW_ID = {0}", attachRecordId);
                   break;
               case "EXPERT":
                   sSQL = "select claim_id from EXPERT ex INNER JOIN claim_x_Litigation li on ex.litigation_row_id = li.litigation_row_id where EXPERT_ROW_ID =" + attachRecordId;
                   break;
               case "SALVAGE":
                   //sSQL = "select claim_id from SALVAGE s INNER JOIN CLAIM_X_PROPERTYLOSS cp ON s.parent_id = cp.property_id where salvage_row_id = " + attachRecordId;
                   //rma 1598
                   sSQL = "select CLAIM_ID from SALVAGE s  INNER JOIN  UNIT_X_CLAIM u on u.UNIT_ROW_ID = s.PARENT_ID where salvage_row_id =" + attachRecordId;
                   break;
               case "DEFENDANT":
                   sSQL = "select claim_id from DEFENDANT where defendant_row_id = " + attachRecordId;
                   break;
               case "CMXCMGRHIST":
                    // akaushik5 Changed for MITS 37022 Starts
                   //sSQL = "select claim_id from CASE_MANAGEMENT where casemgt_row_id =" + attachRecordId;
                   sSQL = string.Format("SELECT EVENT_ID FROM PERSON_INVOLVED JOIN CASE_MANAGEMENT ON PERSON_INVOLVED.PI_ROW_ID = CASE_MANAGEMENT.PI_ROW_ID JOIN CM_X_CMGR_HIST ON CASE_MANAGEMENT.CASEMGT_ROW_ID = CM_X_CMGR_HIST.CASEMGT_ROW_ID WHERE CM_X_CMGR_HIST.CMCMH_ROW_ID = {0}", attachRecordId);
                   // akaushik5 Changed for MITS 37022 Ends
                   break;
               case "CASEMANAGEMENT":
                   //rma 1598 
                   sSQL = string.Format("SELECT EVENT_ID FROM PERSON_INVOLVED JOIN CASE_MANAGEMENT ON PERSON_INVOLVED.PI_ROW_ID = CASE_MANAGEMENT.PI_ROW_ID WHERE CASE_MANAGEMENT.CASEMGT_ROW_ID ={0}", attachRecordId);
                   break;
               /* Policy as Parent Record Category */
               case "POLICYCOVERAGE":
                   sSQL = "select policy_id from POLICY_X_CVG_TYPE where POLCVG_ROW_ID =" + attachRecordId;
                   break;
               case "POLICYBILLING":
                   sSQL = "SELECT POLICY_ID FROM POLICY_ENH WHERE POLICY_ID =" + attachRecordId;
                   break;
           }
           try
           {
               if (!string.IsNullOrEmpty(sSQL))
               {
                   using (objRead = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                   {
                       if (objRead != null)
                       {
                           if (objRead.Read())
                           {
                               parentRecordId = Conversion.ConvertObjToInt(objRead[0], m_iClientId);
                           }
                       }
                   }
               }
           }
           catch (Exception p_objException)
           {
               throw new RMAppException(Globalization.GetString("WPA.GetSingleInt.GeneralError", m_iClientId), p_objException);
           }
           finally
           {
               if (objRead != null)
               {
                   objRead.Close();
                   objRead.Dispose();
               }
           }
           return parentRecordId;
           // End of Mits 33843
       }
       //Add by kuladeep for MITS:34958
        /// <summary>
        /// Function for Return User list Name without Email detail
        /// </summary>
        /// <returns></returns>
       private string GetNameWithoutMail(string sUserNamelist,int iDsnId)
       {
           string sRetUserNameWtEmail = string.Empty;
           string sSQL = string.Empty;
           try
           {
               //Change by kuladeep for MITS:35142 as per QA suggestion --Start
               //if (DbFactory.GetDbConnection(m_sConnectionString).DatabaseType == Db.eDatabaseType.DBMS_IS_SQLSRVR)
               //{
               //    sSQL = "SELECT ISNULL(USER_TABLE.LAST_NAME+' '+USER_TABLE.FIRST_NAME,USER_TABLE.LAST_NAME) FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID  AND USER_TABLE.EMAIL_ADDR IS NULL AND USER_DETAILS_TABLE.DSNID=" + iDsnId + " AND USER_DETAILS_TABLE.LOGIN_NAME IN (" + sUserNamelist + ")";
               //}
               //else
               //{
               //    sSQL = "SELECT nvl(USER_TABLE.LAST_NAME,'') ||' '|| nvl(USER_TABLE.FIRST_NAME,'') FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID  AND USER_TABLE.EMAIL_ADDR IS NULL AND USER_DETAILS_TABLE.DSNID=" + iDsnId + " AND USER_DETAILS_TABLE.LOGIN_NAME IN (" + sUserNamelist + ")";
               //}
               sSQL = "SELECT USER_DETAILS_TABLE.LOGIN_NAME FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID  AND USER_TABLE.EMAIL_ADDR IS NULL AND USER_DETAILS_TABLE.DSNID=" + iDsnId + " AND USER_DETAILS_TABLE.LOGIN_NAME IN (" + sUserNamelist + ")";
               //Change by kuladeep for MITS:35142 as per QA suggestion --End
               using (DbReader rdr = DbFactory.GetDbReader(m_sSecConnectString, sSQL))
               {
                   while (rdr.Read())
                   {
                       sRetUserNameWtEmail = sRetUserNameWtEmail + rdr.GetValue(0).ToString() + ",";
                   }
                   sRetUserNameWtEmail = sRetUserNameWtEmail.TrimEnd(',').Trim();
               }
               if (string.IsNullOrEmpty(sRetUserNameWtEmail)==false)
               {
                   throw new RMAppException(Globalization.GetString("WPA.GetMailId.Error", m_iClientId) + "(" + sRetUserNameWtEmail + ")");
               }
           }
           catch (RMAppException p_objException)
           {
               throw p_objException;
           }
           catch (Exception p_objException)
           {
               throw new RMAppException(Globalization.GetString("WPA.GetMailId.Error", m_iClientId), p_objException);
           }
           finally
           {
               sSQL = null;
           }
           return (sRetUserNameWtEmail);
       }
       //MITS 36519
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iSortOrder"></param>
        /// <returns></returns>
       public string GetColumnName(int iSortOrder)
       {
           string columnName = "COMPLETEDATE";
           switch (iSortOrder)
           {
               case 4:
               case 8:
                   columnName = "priority";
                   break;
               case 1:
               case 5:
                   columnName = "COMPLETEDATE";
                   break;
               case 3:
               case 7:
                   columnName = "tasksubject";
                   break;
               case 2:
               case 6:
                   columnName = "attachrecord";
                   break;
               case 9:
               case 13:
                   columnName = "work_activity";
                   break;
               case 10:
               case 14:
                   columnName = "claimant";
                   break;
               case 15:
               case 16:
                   columnName = "department";
                   break;
               case 17:
               case 18:
                   columnName = "claimstatus";
                   break;
               case 19:
               case 20:
                   columnName = "assigned_user";
                   break;
               case 21:
               case 22:
                   columnName = "assigning_user";
                   break;
               case 23:
               case 24:
                   columnName = "parentrecord";
                   break;
               case 25:
               case 26:
                   columnName = "notroutable";
                   break;
               case 27:
               case 28:
                   columnName = "notrollable";
                   break;
           }
           return columnName;
       }
       //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
       /// <summary>
       /// Gets the supplemental information.
       /// </summary>
       /// <param name="objUserLogin">The object user login.</param>
       /// <param name="doc">The document.</param>
       public void GetSuppInformation(UserLogin objUserLogin, XmlDocument doc)
       {
           DataModelFactory objDMF = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
           WpaDiaryEntry objDiary = (WpaDiaryEntry)objDMF.GetDataModelObject("WpaDiaryEntry", false);
           XmlDocument objWPADiary = new XmlDocument();
           XmlDocument objSerilzationXMl = new XmlDocument();
           objSerilzationXMl.LoadXml("<WpaDiaryEntry><Supplementals/></WpaDiaryEntry>");
           objWPADiary.LoadXml(objDiary.SerializeObject(objSerilzationXMl));
           doc.DocumentElement.AppendChild(doc.ImportNode(objWPADiary.SelectSingleNode("//Supplementals"), true));
       }
       //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691

       //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691- Start
       /// <summary>
       /// Function for Formatting Data By Supplemental FieldType
       /// </summary>
       /// <Author>agupta298</Author>
       private string FormatDataBySuppFieldType(string p_Data, SupplementalFieldTypes p_enumFieldType, int p_iFieldId, int p_iRecordId)
       {
           LocalCache objCache = null;
           string sFormattedData = string.Empty;
           string sShortCode = string.Empty;
           string sCodeDesc = string.Empty;
           string sSql = string.Empty;
           bool blnSuccess = false;
           DbReader objFieldValueReader = null;
           string sDataString = string.Empty;
           string sName = string.Empty;

           try
           {
               if (!string.IsNullOrEmpty(p_Data))
               {
                   objCache = new LocalCache(m_sConnectionString, m_iClientId);

                   switch (p_enumFieldType)
                   {
                       case SupplementalFieldTypes.SuppTypeClaimLookup://CLAIM_NUMBER
                       case SupplementalFieldTypes.SuppTypeEventLookup://EVENT_NUMBER
                       case SupplementalFieldTypes.SuppTypeVehicleLookup://LICENSE_NUMBER
                       case SupplementalFieldTypes.SuppTypeText:
                       case SupplementalFieldTypes.SuppTypeFreeText:
                       case SupplementalFieldTypes.SuppTypePolicyManagementLookup:
                       case SupplementalFieldTypes.SuppTypePolicyTrackingLookup:
                       case SupplementalFieldTypes.SuppTypeMemo:
                       case SupplementalFieldTypes.SuppTypeHTML:
                           sFormattedData = Convert.ToString(p_Data);
                           break;

                       case SupplementalFieldTypes.SuppTypeDate:
                           sFormattedData = Conversion.GetDBDateFormat(p_Data, "d");
                           break;

                       case SupplementalFieldTypes.SuppTypeCode:
                           sFormattedData = objCache.GetCodeDesc(Conversion.CastToType<int>(p_Data, out blnSuccess));
                           break;

                       case SupplementalFieldTypes.SuppTypeState:
                           objCache.GetStateInfo(Conversion.CastToType<int>(p_Data, out blnSuccess), ref sShortCode, ref sCodeDesc);
                           sFormattedData = sCodeDesc;
                           break;

                       case SupplementalFieldTypes.SuppTypeTime:
                           //sFormattedData = Conversion.GetDBDateFormat(p_Data, "t");
                           sFormattedData = Conversion.GetTimeAMPM(p_Data); //Conversion.GetDBTimeFormat(sDate,"HHMMSS");								
                           break;

                       case SupplementalFieldTypes.SuppTypeEntity:
                           sFormattedData = objCache.GetEntityLastFirstName(Conversion.CastToType<int>(p_Data, out blnSuccess));
                           break;

                       case SupplementalFieldTypes.SuppTypeNumber:
                       case SupplementalFieldTypes.SuppTypeCurrency:
                           double dValue = Conversion.CastToType<double>(p_Data, out blnSuccess);
                           if (blnSuccess)
                               sFormattedData = Convert.ToString(dValue);
                           break;
                       case SupplementalFieldTypes.SuppTypeHyperlink:
                           //string[] arrData = objCache.GetWebLinkDataBySuppFieldId(p_iFieldId);
                           //sFormattedData = "<a id='link_" + p_iFieldId + "_Diary_supp' href='" + p_Data + "' class='LightBlue' style='display:inline-block;color:#330099;font-weight:bold;text-decoration:underline;width:5px;cursor:pointer' target='_blank'>" + arrData[0] + "</a>";
                           sFormattedData = p_Data;
                           break;
                       case SupplementalFieldTypes.SuppTypeCheckBox:
                           sFormattedData = string.IsNullOrEmpty(p_Data) ? "No" : !p_Data.Equals("0") ? "Yes" : "No";
                           break;
                   }
               }
           }
           catch (Exception)
           {

           }
           finally
           {
               if (objCache != null)
                   objCache.Dispose();

               if (objFieldValueReader != null)
               {
                   objFieldValueReader.Close();
                   objFieldValueReader.Dispose();
               }
           }
           return sFormattedData;
       }

       /// <summary>
       /// Function for Checking Sort Flag
       /// </summary>
       /// <Author>agupta298</Author>
       private bool IsDiarySuppSortable(SupplementalFieldTypes p_enumSuppFieldType)
       {
           bool IsSortable = true;

           if ((p_enumSuppFieldType == SupplementalFieldTypes.SuppTypeMultiCode) || (p_enumSuppFieldType == SupplementalFieldTypes.SuppTypeMultiEntity) || (p_enumSuppFieldType == SupplementalFieldTypes.SuppTypeMultiState))
               IsSortable = false;

           return IsSortable;
       }
        //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691- End

        /// <summary>
       /// asharma326 jira 11081 permission for Allow Edit due diaries for other users. 
        /// </summary>
       public string SetOtherDiariesEditPermission { get; set; }
    }
}
