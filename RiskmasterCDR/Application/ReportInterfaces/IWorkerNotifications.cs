using System;

namespace Riskmaster.Application.ReportInterfaces
{
	///************************************************************** 
	///* $File		: IWorkerNotifications.cs 
	///* $Revision	: 1.0
	///* $Date		: 22-Dec-2004 
	///* $Author	: Aditya Babbar
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>
	/// This interface is implemented by SMWorker object.
	/// Reporting Engines such as those for Executive Summary 
	/// and OSHA reports, update the progress of a job to the
	/// worker process and check for cancellation of a job 
	/// using this interface. 
	/// </summary>
	public interface IWorkerNotifications
	{
		/// Name			: UpdateProgress
		/// Author			: Aditya Babbar
		/// Date Created	: 22-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************		
		/// <summary>
		/// Reporting engines update the progress of their job using this method 
		/// that is called at various stages of job execution.
		/// </summary>
		/// <param name="p_sReportStage">
		///		Report Stage.
		///	</param>
		/// <param name="p_iTimeElapsed">
		///		Time Elapsed.
		///	</param>
		/// <param name="p_iTimeLeft">
		///		Time Left.
		///	</param>
		/// <param name="p_iPercentComplete">
		///		Percent Complete.
		///	</param>		
		void UpdateProgress(string p_sReportStage,
							int p_iTimeElapsed,
							int p_iTimeLeft,
							int p_iPercentComplete);

		/// Name			: CheckAbort
		/// Author			: Aditya Babbar
		/// Date Created	: 22-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************		
		/// <summary>
		/// Reporting Engines check whether they need to abort the current job
		/// by calling this method at various stages while generating a report. 
		/// This gives them the opportunity to cleanup and exit cleanly.
		/// </summary>
		/// <returns>
		///		1 - Engine should abort job
		///		0 - Engine should continue job
		/// </returns>
		int CheckAbort();						
		
	}
}
