using System;
using System.Xml;
using System.Collections;

namespace Riskmaster.Application.ReportInterfaces
{
	/// <summary>
	///  This interface is implemented by reporting engines.
	///  SMWorker executes reporting jobs using engine obtained 
	///  from ReportEngineFactory, that returns a reference 
	///  of this interface. 
	/// </summary>
	public interface IReportEngine
	{
		#region Properties

		/// <summary>
		///  Report Criteria XML DOM
		/// </summary>
		XmlDocument CriteriaXmlDom
		{
			get;
			set;
		}

		/// <summary>
		///  Criteria HTML
		/// </summary>
		string CriteriaHtml
		{
			get;			
		}

		/// <summary>
		/// HTML Criteria Supported 
		/// </summary>
		bool HtmlCriteriaSupported
		{
			get;			
		}

		/// <summary>
		///  XML Criteria Supported
		/// </summary>
		bool XmlCriteriaSupported
		{
			get;			
		}

		/// <summary>
		///  Outputs Supported
		/// </summary>
		int OutputsSupported
		{
			get;			
		}

		/// <summary>
		///  Output Report Files.
		/// </summary>
		ArrayList ReportOutput
		{
			get;			
		}
		
		/// <summary>
		/// Executing Worker Object.
		/// </summary>
		IWorkerNotifications Notify
		{
			get;
			set;
		}			

		/// <summary>
		/// Database Connection String. 
		/// </summary>
		string ConnectionString
		{
			get;
			set;
		}

		/// <summary>
		/// Output Directory. 
		/// </summary>
		string OutputDirectory
		{
			get;
			set;
		}

		/// <summary>
		/// Output Format. 
		/// </summary>
		eReportFormat OutputFormat
		{
			get;
			set;
		}
		#endregion

		#region Methods

		/// Name			: RunReport
		/// Author			: Aditya Babbar
		/// Date Created	: 22-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************		
		/// <summary>
		/// Generates report output.
		/// </summary>
		void RunReport();

		/// Name			: Init
		/// Author			: Aditya Babbar
		/// Date Created	: 22-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************		
		/// <summary>
		/// Initializes report engine. 
		/// </summary>
		void Init();
		
		#endregion
	
	}

	/// <summary>
	/// Report Type
	/// </summary>
	public enum eReportFormat:int
	{
		Html = 1,
		Pdf = 2,
		Xml = 4,
		PlainText = 8,
		VSView = 16,
		CrystalReport = 32
	}
	
}
