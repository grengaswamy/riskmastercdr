﻿
using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Collections; 
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
namespace Riskmaster.Application.BRS
{
    /// <summary>
    /// A class representing the Bill Items. 
    /// </summary>
	public class BillItem :IDisposable
	{
		#region Variable Declarations
	
		/// <summary>
		/// Represents the Module variable for Common Class.
		/// </summary>
		private Common m_objCommon;
	
		private const int SPLITPAYMENTFLAG = 1234567890;

        public const int ANESTHESIA_SHORT_CODE = 7;

		/// <summary>
		/// Represents the Percentile
		/// </summary>
		internal string m_sPercentile="";

		/// <summary>
		/// Represents the Fee Table used
		/// </summary>
		private string m_sFeeTable = "";

		/// <summary>
		/// Represents the Contract Description
		/// </summary>
		private string m_sContractDesc = "";

		/// <summary>
		/// Represents the Begining of Contract
		/// </summary>
		private string m_sContractBegin = "";

		/// <summary>
		/// Represents the End of Contract
		/// </summary>
		private string m_sContractEnd = "";

		/// <summary>
		/// Represents the Fee Table State
		/// </summary>
		private string m_sFeeTableState = "";

		/// <summary>
		/// Represents the Fee Table Study 
		/// </summary>
		private string m_sFeeTableStudy = "";

		/// <summary>
		/// Represents the Fee Data Revision
		/// </summary>
		private string m_sFeeDataRevision = "";

		/// <summary>
		/// Private variable for Prescription-No
		/// </summary>
		private string m_sPrescriptionNo = "";

        /// <summary>
        /// Private variable for Prescription Date
        /// </summary>
        private string m_sPrescriptionDate = "";

		/// <summary>
		/// Private variable for Drug Name
		/// </summary>
		private string m_sDrugName = "";

        /// <summary>
        /// Prescription Certification
        /// </summary>
        private int m_iPrescriptionCertification = 0;

        /// <summary>
        /// Medication Quantity
        /// </summary>
        private int m_iMedQuantity = 0;

        /// <summary>
        /// Medication Days Supply
        /// </summary>
        private int m_iMedDaysSupply = 0;

        /// <summary>
        /// Prescription Indicator
        /// </summary>
        private int m_iPrescriptionIndicator = 0;

        /// <summary>
        /// Purchased Indicator
        /// </summary>
        private int m_iPurchasedIndicator = 0;

        /// <summary>
        /// Supply Charge
        /// </summary>
        private bool m_bSupplyChargeFlag = false;

        /// <summary>
        /// Pharmacy Usual Charge
        /// </summary>
        private double m_dUsualCharge = 0.0;

        /// <summary>
        /// Dispensed as Written
        /// </summary>
        private int m_iDispensed = 0;

        /// <summary>
        /// HCPCS Level II Code
        /// </summary>
        private string m_sHcpcsCode = "";

		/// <summary>
		/// Private variable for Follow-Up Message
		/// </summary>
		private string m_sFollowUpMessage = "";

		/// <summary>
		/// Private variable for Stop Loss Amount
		/// </summary>
		private double m_dStopLossAmt = 0.0;

		/// <summary>
		/// Private variable for Per-Diem Amount
		/// </summary>
		private double m_dPerDiemAmount = 0.0;

		/// <summary>
		/// Private variable for Entire Fee Schedule Amount
		/// </summary>
		private double m_dEntFeeSchdAmt = 0.0;

		/// <summary>
		/// Private variable for Entire Fee Schedule Amount 2
		/// </summary>
		private double m_dEntFeeSchdAmt2nd = 0.0;

		/// <summary>
		/// Private variable for 2nd Rate
		/// </summary>
		private double m_d2ndRate = 0.0;

		/// <summary>
		/// Private variable for Last Rate
		/// </summary>
		private double m_dLastRate = 0.0;

		/// <summary>
		/// Private variable for First Rate
		/// </summary>
		private double m_dFirstRate = 0.0;

		/// <summary>
		/// Private variable for Second Discount
		/// </summary>
		private double m_dSecondDiscount = 0.0;

		/// <summary>
		/// Private variable for Discount Bill
		/// </summary>
		private double m_dDiscountBill = 0.0;

		/// <summary>
		/// Private variable for Discount 2nd
		/// </summary>
		private double m_dDiscount2nd = 0.0;

		/// <summary>
		/// Private variable for Fee Data Source
		/// </summary>
		private int m_iFeeDataSource = 0;

		/// <summary>
		/// Private variable for Entire Fee Schedule Used
		/// </summary>
		private int m_iEntFeeSchdUsed = 0;

		/// <summary>
		/// Private variable for Rate Change
		/// </summary>
		private int m_iRateChange = 0;

		/// <summary>
		/// Private variable for 2nd Rate Change
		/// </summary>
		private int m_i2ndRateChg = 0;

		/// <summary>
		/// Private variable for Red Type
		/// </summary>
		private int m_iRedType = 0;

		/// <summary>
		/// Private variable for Payee-Id
		/// </summary>
		private int m_iPayeeId = 0;

		/// <summary>
		/// Private variable for Entire Fee Schedule Id
		/// </summary>
		private int m_iEntFeeSchdId = 0;

		/// <summary>
		/// Private variable for Entire Fee Schedule Id 2
		/// </summary>
		private int m_iEntFeeSchdId2 = 0;

		/// <summary>
		/// Private variable for Stop Loss Flag
		/// </summary>
		private int m_iStopLossFlag = 0;


		private int m_iModifierCode = 0;
		/// <summary>
		/// BrsInvoiceDetail class object
		/// </summary>
		BrsInvoiceDetail m_objBrsInvoiceDetail ;

		/// <summary>
		/// FundsTransSplit class object
		/// </summary>
		FundsTransSplit m_objFundsTransSplit;

		/// <summary>
		/// ModValues class object
		/// </summary>
		ModifierValues m_objModValues ;

        /// <summary>
        /// flag to indicate if it's invokded from FundsForm and the SPlit object is passed in.
        /// </summary>
        private bool m_bInvokedThroughFunds = false;

		/// <summary>
		/// Line of business. It will be used in retrieving possible Trans Types.
		/// </summary>
		private int m_iLOB;

        /// <summary>
        /// The specialty code selected
        /// </summary>
        private string m_SpecialtyCode = string.Empty;

        /// <summary>
        /// The multiplication factor for the specialty code selected
        /// </summary>
        private string m_SpecialtyCodeFactor = string.Empty;
        
        /// <summary>
        /// list of all available specialty codes for the selected CPT codes
        /// </summary>
        private string m_SpecialtyCodesList = string.Empty;

        /// <summary>
        /// Private variable for Funds Split Row Id
        /// </summary>
        private int m_iSplitRowId = 0;

		/// <summary>
		/// Tanuj->Should contain errors/alerts for BRS module, will be set by BRS adaptor and errors/alerts will be displayed onto UI. 
		/// </summary>
		private BusinessAdaptorErrors m_objErrors;

		public BusinessAdaptorErrors Errors
		{
			get
			{
				return m_objErrors;
			}
			set
			{
				m_objErrors=value;
			}
		}

        private int m_iClientId = 0;//rkaur27

        UserLogin m_oUserLogin = null;//vkumar258 RMA-6037

		#endregion
		
		#region Constructor
		/// Name		: BillItem
		/// Author		: Navneet Sota
		/// Date Created: 12/14/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
		/// <param name="p_objCommon">Common Class object instantiated by Manager </param>
		public BillItem(Common p_objCommon, int p_iClientId)
		{
			m_objCommon = p_objCommon;
            m_iClientId = p_iClientId;
			if(m_objCommon.g_objDataModelFactory == null)
				this.Initialize();

			m_objFundsTransSplit = (FundsTransSplit)m_objCommon.g_objDataModelFactory.GetDataModelObject("FundsTransSplit",false);
            m_objBrsInvoiceDetail = m_objFundsTransSplit.BrsInvoiceDetail;
            m_objModValues = new ModifierValues(m_objCommon,m_iClientId);
		}

        /// <summary>
        /// Constructor for the class. It will take a FundsTransSplit object as input parameter
        /// </summary>
        /// <param name="p_objCommon">Common Class object instantiated by Manager </param>
        /// <param name="objFundTransSplit">FundsTransSplit object</param>
        public BillItem(Common p_objCommon, ref FundsTransSplit objFundTransSplit, int p_iClientId)
        {
            m_objCommon = p_objCommon;
            m_iClientId = p_iClientId;
            if (m_objCommon.g_objDataModelFactory == null)
                this.Initialize();

            if (objFundTransSplit != null)
            {
                m_bInvokedThroughFunds = true;
                m_objBrsInvoiceDetail = objFundTransSplit.BrsInvoiceDetail;
                m_objFundsTransSplit = objFundTransSplit;
            }
            else
            {
                m_objFundsTransSplit = (FundsTransSplit)m_objCommon.g_objDataModelFactory.GetDataModelObject("FundsTransSplit",false);
                m_objBrsInvoiceDetail = m_objFundsTransSplit.BrsInvoiceDetail;
            }
            m_objModValues = new ModifierValues(m_objCommon,m_iClientId);
        }

		#endregion

		#region Properties

		/// <summary>
		/// Internal property for Prescription number
		/// </summary>
		internal string PrescriptionNo
		{
			set
			{
				m_sPrescriptionNo = value;
                m_objFundsTransSplit.BrsInvoiceDetail.PrescripNo = value;
			}
			get
			{
				return m_sPrescriptionNo;
			}	
		}
		internal int ModifierCode
		{
			set
			{
				m_iModifierCode = value;
			}
			get
			{
				return m_iModifierCode;
			}	
		}

		/// <summary>
		/// Internal property for Drug Name
		/// </summary>
		internal string DrugName
		{
			set
			{
				m_sDrugName = value;
                m_objFundsTransSplit.BrsInvoiceDetail.DrugName = value;
			}
			get
			{
				return m_sDrugName;
			}	
		}

		/// <summary>
		/// Internal property for Prescription Date
		/// </summary>
		internal string PrescriptionDate
		{
			set
			{
				m_sPrescriptionDate = value;
                m_objFundsTransSplit.BrsInvoiceDetail.PrescripDate = value;
			}
			get
			{
				return m_sPrescriptionDate;
			}	
		}

        public int PrescriptionCertification
        {
            set
            {
                m_objFundsTransSplit.BrsInvoiceDetail.Certification = value;
                m_iPrescriptionCertification = value;
            }
            get
            {
                return m_iPrescriptionCertification;
            }
        }

        public int MedQuantity
        {
            set
            {
                m_objFundsTransSplit.BrsInvoiceDetail.MedsQuantity = value;
                m_iMedQuantity = value;
            }
            get
            {
                return m_iMedQuantity;
            }
        }

        public int MedDaysSupply
        {
            set
            {
                m_objFundsTransSplit.BrsInvoiceDetail.DaysSupplied = value;
                m_iMedDaysSupply = value;
            }
            get
            {
                return m_iMedDaysSupply;
            }
        }

        public int PrescriptionIndicator
        {
            set
            {
                m_objFundsTransSplit.BrsInvoiceDetail.RxInd = value;
                m_iPrescriptionIndicator = value;
            }
            get
            {
                return m_iPrescriptionIndicator;
            }
        }

        public int PurchasedIndicator
        {
            set
            {
                m_objFundsTransSplit.BrsInvoiceDetail.PurchasedInd = value;
                m_iPurchasedIndicator = value;
            }
            get
            {
                return m_iPurchasedIndicator;
            }
        }

        public bool SupplyChargeFlag
        {
            set
            {
                m_objFundsTransSplit.BrsInvoiceDetail.MedSupplyFlag = value;
                m_bSupplyChargeFlag = value;
            }
            get
            {
                return m_bSupplyChargeFlag;
            }
        }

        public double UsualCharge
        {
            set
            {
                m_objFundsTransSplit.BrsInvoiceDetail.RxUsualCharge = value;
                m_dUsualCharge = value;
            }
            get
            {
                return m_dUsualCharge;
            }
        }

        public int Dispensed
        {
            set
            {
                m_objFundsTransSplit.BrsInvoiceDetail.Dispensed = value;
                m_iDispensed = value;
            }
            get
            {
                return m_iDispensed;
            }
        }

        public string HcpcsCode
        {
            set
            {
                m_objFundsTransSplit.BrsInvoiceDetail.HcpcsCode = value;
                m_sHcpcsCode = value;
            }
            get
            {
                return m_sHcpcsCode;
            }
        }

		/// <summary>
		/// Internal property for Fee Data Revision
		/// </summary>
		internal string FeeDataRevision
		{
			set
			{
				m_sFeeDataRevision = value;
                m_objFundsTransSplit.BrsInvoiceDetail.FeeDataRevision = value;
			}
			get
			{
				return m_sFeeDataRevision;
			}	
		}

		/// <summary>
		/// Internal property for Fee Table Study
		/// </summary>
		internal string FeeTableStudy
		{
			set
			{
				m_sFeeTableStudy = value;
                m_objFundsTransSplit.BrsInvoiceDetail.FeeTableStudy = value;
			}
			get
			{
				return m_sFeeTableStudy;
			}	
		}
		
		/// <summary>
		/// Internal property for Fee Table State
		/// </summary>
		internal string FeeTableState
		{
			set
			{
				m_sFeeTableState = value;
                m_objFundsTransSplit.BrsInvoiceDetail.FeeTableState = value;
			}
			get
			{
				return m_sFeeTableState;
			}	
		}

		/// <summary>
		/// Internal property for Fee Data Source
		/// </summary>
		internal int FeeDataSource
		{
			set
			{
				m_iFeeDataSource = value;
                m_objFundsTransSplit.BrsInvoiceDetail.FeeDataSource = value;
			}
			get
			{
				return m_iFeeDataSource;
			}	
		}

		/// <summary>
		/// Internal property for Stop Loss Flag
		/// </summary>
		internal int StopLossFlag
		{
			set
			{
				m_iStopLossFlag = value;
                m_objFundsTransSplit.BrsInvoiceDetail.StopLossFlag = value;
			}
			get
			{
				return m_iStopLossFlag;
			}	
		}

		/// <summary>
		/// Internal property for Stop Loss Amount
		/// </summary>
		internal double StopLossAmt
		{
			set
			{
				m_dStopLossAmt = value;
                m_objFundsTransSplit.BrsInvoiceDetail.StopLossAmt = value;
			}
			get
			{
				return m_dStopLossAmt;
			}	
		}

		/// <summary>
		/// Internal property for Per-Diem Amount
		/// </summary>
		internal double PerDiemAmount
		{
			set
			{
				m_dPerDiemAmount = value;
                m_objFundsTransSplit.BrsInvoiceDetail.PerDiemAmt = value;
			}
			get
			{
				return m_dPerDiemAmount;
			}	
		}

		/// <summary>
		/// Internal property for Entire Fee Scheduled Amount
		/// </summary>
		internal double EntFeeSchdAmt
		{
			set
			{
				m_dEntFeeSchdAmt = value;
			}
			get
			{
				return m_dEntFeeSchdAmt;
			}	
		}

		/// <summary>
		/// Internal property for Entire Fee Scheduled Amount
		/// </summary>
		internal double EntFeeSchdAmt2nd
		{
			set
			{
				m_dEntFeeSchdAmt2nd = value;
			}
			get
			{
				return m_dEntFeeSchdAmt2nd;
			}	
		}

		/// <summary>
		/// Internal property for Entire Fee Scheduled ID
		/// </summary>
		internal int EntFeeSchdId
		{
			set
			{
				m_iEntFeeSchdId = value;
                m_objBrsInvoiceDetail.TableCode = value;
			}
			get
			{
				return m_iEntFeeSchdId;
			}	
		}

		/// <summary>
		/// Internal property for Entire Fee Scheduled ID
		/// </summary>
		internal int EntFeeSchdId2
		{
			set
			{
				m_iEntFeeSchdId2 = value;
			}
			get
			{
				return m_iEntFeeSchdId2;
			}	
		}

		/// <summary>
		/// Internal property for Entire Fee Schedule Used
		/// </summary>
		internal int EntFeeSchdUsed
		{
			set
			{
				m_iEntFeeSchdUsed = value;
			}
			get
			{
				return m_iEntFeeSchdUsed;
			}	
		}

		/// <summary>
		/// Internal property for Rate Change
		/// </summary>
		internal int RateChange
		{
			set
			{
				m_iRateChange = value;
			}
			get
			{
				return m_iRateChange;
			}	
		}

		/// <summary>
		/// Internal property for Second Rate Change 
		/// </summary>
		internal int SecondRateChg
		{
			set
			{
				m_i2ndRateChg = value;
			}
			get
			{
				return m_i2ndRateChg;
			}	
		}

		/// <summary>
		/// Internal property for Second Rate
		/// </summary>
		internal double SecondRate
		{
			set
			{
				m_d2ndRate = value;
			}
			get
			{
				return m_d2ndRate;
			}	
		}

		/// <summary>
		/// Internal property for Last Rate
		/// </summary>
		internal double LastRate
		{
			set
			{
				m_dLastRate = value;
			}
			get
			{
				return m_dLastRate;
			}	
		}

		/// <summary>
		/// Internal property for First Rate
		/// </summary>
		internal double FirstRate
		{
			set
			{
				m_dFirstRate = value;
			}
			get
			{
				return m_dFirstRate;
			}	
		}

		/// <summary>
		/// Internal property for Amount Allowed
		/// </summary>
		internal double AmountAllowed
		{
			set
			{
				m_objBrsInvoiceDetail.AmountAllowed = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.AmountAllowed;
			}	
		}

		/// <summary>
		/// Internal property for Override Type
		/// </summary>
		internal int OverrideType
		{
			set
			{
				m_objBrsInvoiceDetail.OverrideType = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.OverrideType;
			}	
		}

		/// <summary>
		/// Internal property for Second Discount
		/// </summary>
		internal double SecondDiscount
		{
			set
			{
				m_dSecondDiscount = value;
			}
			get
			{
				return m_dSecondDiscount;
			}	
		}

		/// <summary>
		/// Internal property for Discount Bill
		/// </summary>
		internal double DiscountBill
		{
			set
			{
				m_dDiscountBill = value;
			}
			get
			{
				return m_dDiscountBill;
			}	
		}

		/// <summary>
		/// Internal property for Discount
		/// </summary>
		internal double Discount
		{
			set
			{
				m_objBrsInvoiceDetail.Discount = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.Discount;
			}	
		}

		/// <summary>
		/// Internal property for Second Discount
		/// </summary>
		internal double Discount2nd
		{
			set
			{
				m_dDiscount2nd = value;
			}
			get
			{
				return m_dDiscount2nd;
			}	
		}

		/// <summary>
		/// Internal property for Contract Amount
		/// </summary>
		internal double ContractAmount
		{
			set
			{
				m_objBrsInvoiceDetail.ContractAmount = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.ContractAmount;
			}	
		}

		/// <summary>
		/// Internal property for Contract Exists
		/// </summary>
		internal bool ContractExists
		{
			set
			{
                m_objBrsInvoiceDetail.ContractExists = Conversion.ConvertBoolToInt(value, m_iClientId);
			}
			get
			{
                return Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(m_objBrsInvoiceDetail.ContractExists.ToString()), m_iClientId);
			}	
		}

		/// <summary>
		/// Internal property for GL Account
		/// </summary>
		internal int GLAccount
		{
			set
			{
				m_objFundsTransSplit.GlAccountCode = value;
			}
			get
			{
				return m_objFundsTransSplit.GlAccountCode;
			}	
		}

		/// <summary>
		/// Internal property for EOB-Codes List
		/// </summary>
		internal DataSimpleList EOBCodes
		{
			get
			{
				return m_objBrsInvoiceDetail.EOBList;
			}
		}

		/// <summary>
		/// Internal property for Diagnosis Codes List
		/// </summary>
		internal DataSimpleList DiagnosisCodes
		{
			get
			{
				return m_objBrsInvoiceDetail.DiagnosisList;			
			}
		}
        /// <summary>
        /// Internal property for Diagnosis Codes List ICD10
        /// </summary>
        internal DataSimpleList DiagnosisCodesicd10
        {
            get
            {
                return m_objBrsInvoiceDetail.DiagnosisListicd10;
            }
        }
        /// <summary>
        /// Internal property for Diagnosis Ref. No.
        /// </summary>
        internal string DiagRefNo
        {
            set
            {
                m_objBrsInvoiceDetail.DiagRefNo = value;
            }

            get
            {
                return m_objBrsInvoiceDetail.DiagRefNo;
            }
            
        }
		/// <summary>
		/// Internal property for Contract Description
		/// </summary>
		internal string ContractDesc
		{
			set
			{
				m_sContractDesc = value;
			}
			get
			{
				return m_sContractDesc;
			}	
		}

		/// <summary>
		/// Internal property for Contract Begin
		/// </summary>
		internal string ContractBegin
		{
			set
			{
				m_sContractBegin = value;
			}
			get
			{
				return m_sContractBegin;
			}	
		}

		/// <summary>
		/// Internal property for Contract End
		/// </summary>
		internal string ContractEnd
		{
			set
			{
				m_sContractEnd = value;
			}
			get
			{
				return m_sContractEnd;
			}	
		}

		/// <summary>
		/// Internal property for Fee Table
		/// </summary>
		internal string FeeTable
		{
			set
			{
				m_sFeeTable = value;

                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append(" SELECT TABLE_ID, CALC_TYPE FROM FEE_TABLES WHERE USER_TABLE_NAME = '").Append(this.FeeTable.ToString()).Append("'");
                DataSet objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString, sbSQL.ToString(), m_iClientId);
                if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
                {
                    m_objFundsTransSplit.BrsInvoiceDetail.TableCode = Conversion.ConvertStrToInteger(objDataSet.Tables[0].Rows[0]["TABLE_ID"].ToString());
                }
			}
			get
			{
				return m_sFeeTable;
			}	
		}

		/// <summary>
		/// Internal property for Training Type
		/// </summary>
		internal int TrnType
		{
			set
			{
				m_objFundsTransSplit.TransTypeCode = value;

                int iTemp = GetIntDataFromDB("RELATED_CODE_ID", "CODES", "CODE_ID=" + this.TrnType);
                if (iTemp < 0)
                    iTemp = 0;
                m_objFundsTransSplit.ReserveTypeCode = iTemp;
			}
			get
			{
				return m_objFundsTransSplit.TransTypeCode;
			}	
		}

		/// <summary>
        /// Mona: R8.2 : BRS in Carrier claim
		/// Internal property for Coverage Id
		/// </summary>
        //internal int CoverageId
        //{
        //    set
        //    {
        //        m_objFundsTransSplit.CoverageId = value;
        //    }
        //    get
        //    {
        //        return m_objFundsTransSplit.CoverageId;
        //    }	
        //}

        /// <summary>
        /// Mona: R8.2 : BRS in Carrier claim
        /// Internal property for First n Final
        /// </summary>
        internal bool IsFirstFinal
        {
            set
            {
                m_objFundsTransSplit.IsFirstFinal = value;
            }
            get
            {
                return m_objFundsTransSplit.IsFirstFinal;
            }
        }

        /// <summary>
        /// Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID
        /// Internal property for First n Final
        /// </summary>
        internal int RCRowId
        {
            set
            {
                m_objFundsTransSplit.RCRowId = value;
            }
            get
            {
                return m_objFundsTransSplit.RCRowId;
            }
        }


        /// <summary>
        /// Internal property for Surface Text
        /// </summary>
		internal string SurfaceText
		{
			set
			{
				m_objBrsInvoiceDetail.SurfaceText = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.SurfaceText;
			}	
		}

		/// <summary>
		/// Internal property for Tooth No
		/// </summary>
		internal bool ToothNo
		{
			set
			{
				m_objBrsInvoiceDetail.ToothNumber = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.ToothNumber;
			}	
		}

		/// <summary>
		/// Internal property for Amount Saved
		/// </summary>
		internal double AmountSaved
		{
			set
			{
				m_objBrsInvoiceDetail.AmountSaved = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.AmountSaved;
			}	
		}

		/// <summary>
		/// Internal property for Red Type
		/// </summary>
		internal int RedType
		{
			set
			{
				m_iRedType = value;
			}
			get
			{
				return m_iRedType;
			}	
		}

		/// <summary>
		/// Internal property for Red Amount
		/// </summary>
		internal double RedAmount
		{
			set
			{
				m_objBrsInvoiceDetail.AmountReduced = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.AmountReduced;
			}	
		}

		/// <summary>
		/// Internal property for Amount To Pay
		/// </summary>
		internal double AmountToPay
		{
			set
			{
				m_objBrsInvoiceDetail.AmountToPay = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.AmountToPay;
			}	
		}

		/// <summary>
		/// Internal property for Invoice Date
		/// </summary>
		internal string InvoiceDate
		{
			set
			{
				m_objFundsTransSplit.InvoiceDate = value;
			}
			get
			{
				return m_objFundsTransSplit.InvoiceDate;
			}	
		}

		/// <summary>
		/// Internal property for Invoiced By
		/// </summary>
		internal string InvoicedBy
		{
			set
			{
				m_objFundsTransSplit.InvoicedBy = value;
			}
			get
			{
				return m_objFundsTransSplit.InvoicedBy;
			}	
		}

		/// <summary>
		/// Internal property for Invoice Number
		/// </summary>
		internal string InvoiceNumber
		{
			set
			{
				m_objFundsTransSplit.InvoiceNumber = value;
			}
			get
			{
				return m_objFundsTransSplit.InvoiceNumber;
			}	
		}

		/// <summary>
		/// Internal property for Percentile
		/// </summary>
		internal string Percentile
		{
			set
			{
				m_objBrsInvoiceDetail.Percentile = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.Percentile;
			}	
		}

		/// <summary>
		/// Internal property for End Date
		/// </summary>
		internal string EndDate
		{
			set
			{
				m_objFundsTransSplit.ToDate = value;
			}
			get
			{
				return m_objFundsTransSplit.ToDate;
			}	
		}

		/// <summary>
		/// Internal property for Start Date
		/// </summary>
		internal string StartDate
		{
			set
			{
				m_objFundsTransSplit.FromDate = value;
			}
			get
			{
				return m_objFundsTransSplit.FromDate;
			}	
		}

		/// <summary>
		/// Internal property for Scheduled Amount
		/// </summary>
		internal double SchdAmount
		{
			set
			{
				m_objBrsInvoiceDetail.ScheduledAmount = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.ScheduledAmount;
			}	
		}

		/// <summary>
		/// Internal property for Amount Billed
		/// </summary>
		internal double AmountBilled
		{
			set
			{
				m_objBrsInvoiceDetail.AmountBilled = value;
                m_objFundsTransSplit.InvoiceAmount = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.AmountBilled;
			}	
		}

		/// <summary>
		/// Internal property for Units Billed Type
		/// </summary>
		internal int UnitsBilledType
		{
			set
			{
				m_objBrsInvoiceDetail.UnitsBilledType = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.UnitsBilledType;
			}	
		}

		/// <summary>
		/// Internal property for Units Billed
		/// </summary>
		internal double UnitsBilled
		{
			set
			{
				m_objBrsInvoiceDetail.UnitsBilledNum = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.UnitsBilledNum;
			}	
		}

		/// <summary>
		/// Internal property for POS
		/// </summary>
		internal int POS
		{
			set
			{
				m_objBrsInvoiceDetail.PlaceOfSerCode = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.PlaceOfSerCode;
			}	
		}

		/// <summary>
		/// Internal property for TOS
		/// </summary>
		internal int TOS
		{
			set
			{
				m_objBrsInvoiceDetail.TypeOfSerCode = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.TypeOfSerCode;
			}	
		}

		/// <summary>
		/// Internal property for Billing Code Text
		/// </summary>
		internal string CPT
		{
			set
			{
				m_objBrsInvoiceDetail.BillingCodeText = value;
                if (value != "" && value.Length > 5)
                    m_objFundsTransSplit.PoNumber = value.Substring(0, 5);
                else
                    m_objFundsTransSplit.PoNumber = value;
			}
			get
			{
				string sRetVal="";
				if (m_objBrsInvoiceDetail.BillingCodeText.IndexOf(" - ") >= 0)
				{
					sRetVal = m_objBrsInvoiceDetail.BillingCodeText.Substring(0,m_objBrsInvoiceDetail.BillingCodeText.IndexOf(" - "));
					return sRetVal.Trim();
				}
				else
					return m_objBrsInvoiceDetail.BillingCodeText.Trim();
			}	
		}

		/// <summary>
		/// Internal property for Zip Code
		/// </summary>
		internal string ZipCode
		{
			set
			{
				m_objBrsInvoiceDetail.ZipCode = value;
			}
			get
			{
				return m_objBrsInvoiceDetail.ZipCode;
			}	
		}

		/// <summary>
		/// Internal property for Payee Id
		/// </summary>
		internal int PayeeId
		{
			set
			{
				m_iPayeeId = value;
			}
			get
			{
				return m_iPayeeId;
			}	
		}

		/// <summary>
		/// Internal property for Follow Up Message
		/// </summary>
		internal string FollowUpMessage
		{
			set
			{
				m_sFollowUpMessage = value;
			}
			get
			{
				return m_sFollowUpMessage;
			}	
		}

		/// <summary>
		/// LOB code for the claim related to this Bill.
		/// </summary>
		internal int LineOfBusinessCode
		{
			set
			{
				m_iLOB = value;
			}
			get
			{
				return m_iLOB;
			}	
		}

        // BRS FL Merge : Umesh


        /// <summary>
        /// Internal property for Bill Type
        /// </summary>
        internal int BillType
        {
            set
            {
                m_objBrsInvoiceDetail.BillType = value;
            }
            get
            {
                return m_objBrsInvoiceDetail.BillType;
            }
        }


        /// <summary>
        /// Internal property for Physician Pharmacy NDC
        /// </summary>
        internal string PhyPharmNDC 
        {
            set
            {
                m_objBrsInvoiceDetail.PhyPharmNdc = value;
            }
            get
            {
                return m_objBrsInvoiceDetail.PhyPharmNdc;
            }
        }


        /// <summary>
        /// Internal property for Physician Pharmacy NDC Description
        /// </summary>
        internal string PhyPharmNDCDes
        {
            set
            {
                m_objBrsInvoiceDetail.PhyPharmNdcDes = value;
            }
            get
            {
                return m_objBrsInvoiceDetail.PhyPharmNdcDes;
            }
        }

        /// <summary>
        /// Internal property for FL License
        /// </summary>
        internal string FLLicense
        {
            set
            {
                m_objBrsInvoiceDetail.FlLicense = value;
            }
            get
            {
                return m_objBrsInvoiceDetail.FlLicense;
            }
        }

        /// <summary>
        /// Internal property for Revenue Code
        /// </summary>
        internal int RevenueCode
        {
            set
            {
                m_objBrsInvoiceDetail.RevenueCode = value;
            }
            get
            {
                return m_objBrsInvoiceDetail.RevenueCode;
            }
        }

        /// <summary>
        /// Internal property for Physician entity Id
        /// </summary>
        internal int PhyEid
        {
            set
            {
                m_objBrsInvoiceDetail.PhyEid = value;
            }
            get
            {
                return m_objBrsInvoiceDetail.PhyEid;
            }
        }

        /// <summary>
        /// property for SpecialtyCode
        /// </summary>
        internal string SpecialtyCode
        {
            set
            {
                m_SpecialtyCode = value;
            }
            get
            {
                return m_SpecialtyCode;
            }
        }

        /// <summary>
        /// property for Specialty Code related multiplication factor
        /// </summary>
        internal string SpecialtyCodeFactor
        {
            set
            {
                m_SpecialtyCodeFactor = value;
            }
            get
            {
                return m_SpecialtyCodeFactor;
            }
        }


        /// <summary>
        /// property for Specialty Codes for the selected CPT code
        /// </summary>
        internal string SpecialtyCodeList
        {
            set
            {
                m_SpecialtyCodesList = value;
            }
            get
            {
                return m_SpecialtyCodesList;
            }
        }

        /// <summary>
        /// property for Funds Split Row Id
        /// </summary>
        internal int SplitRowId
        {
            set
            {
                m_objFundsTransSplit.SplitRowId = value;
                m_iSplitRowId = value;
            }
            get
            {
                return m_objFundsTransSplit.SplitRowId;
            }
        }

        // BRS FL Merge : End
		/// <summary>
		/// Internal property for objSplit
		/// </summary>
		public FundsTransSplit objSplit
		{
			set
			{
				int iInvoiceId = 0;
				IEnumerator objEnum=null;
				FeeSchedule objFeeSchedule = null;
                m_objFundsTransSplit = value;
                 
				if (m_objFundsTransSplit != null)
				{
					this.ZipCode = m_objFundsTransSplit.BrsInvoiceDetail.ZipCode;

					objFeeSchedule = new FeeSchedule(m_objCommon, m_iClientId);
					this.FeeTable = m_objCommon.GetSingleString("USER_TABLE_NAME", "FEE_TABLES", 
										"TABLE_ID=" + m_objFundsTransSplit.BrsInvoiceDetail.TableCode);
                    this.FeeTableState = m_objFundsTransSplit.BrsInvoiceDetail.FeeTableState;

                    //MITS 26353: Modifier code not saving.
                    //modified by Raman Bhatia on 10/23/2011
                    //Inside the "Set" property we are referring to the same object and hence leading to an unintentional 
                    //"Get" call which is clearing the modifier codes!!!
                    //Modifying the code below to use the module level split object instead

                    /*
                    if (objSplit.Parent != null)
                    {
                        this.PayeeId = ((Funds)objSplit.Parent).PayeeEid;
                    }
                     */

                    if (m_objFundsTransSplit.Parent != null)
                    {
                        this.PayeeId = ((Funds)m_objFundsTransSplit.Parent).PayeeEid;
                    }

                    //end of changes for MITS 26353 : Raman Bhatia

					this.Percentile = m_objFundsTransSplit.BrsInvoiceDetail.Percentile;
					this.TOS = m_objFundsTransSplit.BrsInvoiceDetail.TypeOfSerCode;
					this.CPT = m_objFundsTransSplit.BrsInvoiceDetail.BillingCodeText;
					this.POS = m_objFundsTransSplit.BrsInvoiceDetail.PlaceOfSerCode;
					this.UnitsBilled = m_objFundsTransSplit.BrsInvoiceDetail.UnitsBilledNum;
					this.UnitsBilledType = m_objFundsTransSplit.BrsInvoiceDetail.UnitsBilledType;
					this.SchdAmount = m_objFundsTransSplit.BrsInvoiceDetail.ScheduledAmount;
					this.RedAmount = m_objFundsTransSplit.BrsInvoiceDetail.AmountReduced;
					this.AmountSaved = m_objFundsTransSplit.BrsInvoiceDetail.AmountSaved;
					this.ToothNo = m_objFundsTransSplit.BrsInvoiceDetail.ToothNumber;
					this.SurfaceText = m_objFundsTransSplit.BrsInvoiceDetail.SurfaceText;
					this.ContractExists = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                    m_objFundsTransSplit.BrsInvoiceDetail.ContractExists.ToString()), m_iClientId);
					this.ContractAmount = m_objFundsTransSplit.BrsInvoiceDetail.ContractAmount;
				
					if (this.EntFeeSchdUsed == 1)
						this.Discount = m_objFundsTransSplit.BrsInvoiceDetail.Discount;
 			        else if(this.EntFeeSchdUsed == 2 )
                        this.SecondDiscount = m_objFundsTransSplit.BrsInvoiceDetail.Discount;
			        else
						this.DiscountBill = m_objFundsTransSplit.BrsInvoiceDetail.Discount;
       
					this.OverrideType = m_objFundsTransSplit.BrsInvoiceDetail.OverrideType;
					this.AmountAllowed = m_objFundsTransSplit.BrsInvoiceDetail.AmountAllowed;
					this.EntFeeSchdAmt = m_objFundsTransSplit.BrsInvoiceDetail.FeeTableAmt;
					this.PerDiemAmount = m_objFundsTransSplit.BrsInvoiceDetail.PerDiemAmt;
					this.StopLossAmt = m_objFundsTransSplit.BrsInvoiceDetail.StopLossAmt;
		
					if(this.EntFeeSchdUsed == 1)
						this.EntFeeSchdId = m_objFundsTransSplit.BrsInvoiceDetail.TableCode;
					else if (this.EntFeeSchdUsed == 2)
						this.EntFeeSchdId2 = m_objFundsTransSplit.BrsInvoiceDetail.TableCode;

					iInvoiceId = m_objFundsTransSplit.BrsInvoiceDetail.InvoiceDetailId;

					this.PrescriptionNo = m_objFundsTransSplit.BrsInvoiceDetail.PrescripNo;
					this.DrugName = m_objFundsTransSplit.BrsInvoiceDetail.DrugName;
					this.PrescriptionDate = m_objFundsTransSplit.BrsInvoiceDetail.PrescripDate;
                    this.PrescriptionCertification = m_objFundsTransSplit.BrsInvoiceDetail.Certification;
                    this.MedQuantity = m_objFundsTransSplit.BrsInvoiceDetail.MedsQuantity;
                    this.MedDaysSupply = m_objFundsTransSplit.BrsInvoiceDetail.DaysSupplied;
                    this.PrescriptionIndicator = m_objFundsTransSplit.BrsInvoiceDetail.RxInd;
                    this.PurchasedIndicator = m_objFundsTransSplit.BrsInvoiceDetail.PurchasedInd;
                    this.SupplyChargeFlag = m_objFundsTransSplit.BrsInvoiceDetail.MedSupplyFlag;
                    this.UsualCharge = m_objFundsTransSplit.BrsInvoiceDetail.RxUsualCharge;
                    this.Dispensed = m_objFundsTransSplit.BrsInvoiceDetail.Dispensed;
                    this.HcpcsCode = m_objFundsTransSplit.BrsInvoiceDetail.HcpcsCode;

					this.TrnType = m_objFundsTransSplit.TransTypeCode;
					this.AmountToPay = m_objFundsTransSplit.Amount;
					this.GLAccount = m_objFundsTransSplit.GlAccountCode;
					this.EndDate = m_objFundsTransSplit.ToDate;
					this.StartDate = m_objFundsTransSplit.FromDate;
					this.InvoicedBy = m_objFundsTransSplit.InvoicedBy;
					this.InvoiceNumber = m_objFundsTransSplit.InvoiceNumber;
					this.AmountBilled = m_objFundsTransSplit.InvoiceAmount;
					this.InvoiceDate = m_objFundsTransSplit.InvoiceDate;
                    //BRS FL Merge
                    this.BillType = m_objFundsTransSplit.BrsInvoiceDetail.BillType;
                    this.PhyPharmNDC = m_objFundsTransSplit.BrsInvoiceDetail.PhyPharmNdc;
                    this.PhyPharmNDCDes = m_objFundsTransSplit.BrsInvoiceDetail.PhyPharmNdcDes;
                    this.FLLicense = m_objFundsTransSplit.BrsInvoiceDetail.FlLicense;
                    this.RevenueCode = m_objFundsTransSplit.BrsInvoiceDetail.RevenueCode;
                    this.PhyEid = m_objFundsTransSplit.BrsInvoiceDetail.PhyEid;
                    this.DiagRefNo = m_objFundsTransSplit.BrsInvoiceDetail.DiagRefNo;
                    //srajindersin MITS 29012 dt-09/26/2012
                    this.DenyLineItem = m_objFundsTransSplit.BrsInvoiceDetail.DenyLineItem;

                    this.ModValues = new ModifierValues(m_objCommon, m_iClientId);
					objEnum = m_objFundsTransSplit.BrsInvoiceDetail.ModifierList.GetEnumerator();
					while(objEnum.MoveNext())
					{
						this.ModValues.Add(Conversion.ConvertObjToInt(((DictionaryEntry)objEnum.Current).Value, m_iClientId),"" );
					}
					objEnum = m_objFundsTransSplit.BrsInvoiceDetail.ModifierList.GetEnumerator();
					while(objEnum.MoveNext())
					{
                        this.ModifierCode = Conversion.ConvertObjToInt(((DictionaryEntry)objEnum.Current).Value, m_iClientId);
						break;
					}

				}//if (m_objFundsTransSplit != null)
			}//End of Set 
			get
			{
				IEnumerator objEnum=null;
				double dAmountReduced = 0.0;
				double dBaseAmount = 0.0;
				double dDiscount = 0.0;
                double dFeeTableAmount = 0.0;


				ProviderContractOverride objProviderContractOverride = null;
				try
				{
                    m_objBrsInvoiceDetail.AmountToPay = this.AmountToPay;
                    m_objFundsTransSplit.Amount = this.AmountToPay;
					m_objFundsTransSplit.Crc = SPLITPAYMENTFLAG;
					m_objFundsTransSplit.BrsInvoiceDetail.Quantity = "";
					m_objFundsTransSplit.BrsInvoiceDetail.Store = "";
					if(this.RedType == 1)
					{
						if(this.AmountBilled < this.SchdAmount)
							dAmountReduced = this.AmountBilled;
						else
							dAmountReduced = this.SchdAmount;

						dAmountReduced = (dAmountReduced * this.RedAmount) / 100;
					}
					else
						dAmountReduced = this.RedAmount;

					m_objFundsTransSplit.BrsInvoiceDetail.AmountReduced = dAmountReduced;
                    m_objFundsTransSplit.BrsInvoiceDetail.ContractAmount = this.ContractAmount;
					objProviderContractOverride = new ProviderContractOverride(m_objCommon, m_iClientId);
					switch (OverrideType)
					{
						case (int)Common.BillOverrideType.APPLY_CONTRACT_AMOUNT:
							dBaseAmount = this.ContractAmount;
							dDiscount = 0;
							break;
						case (int)Common.BillOverrideType.APPLY_DISC_ON_CONTRACT:
							dBaseAmount = this.ContractAmount;
							dDiscount = this.DiscountBill;
							break;
						case (int)Common.BillOverrideType.APPLY_DISC_ON_SCHD:
							dBaseAmount = this.SchdAmount;
							dDiscount = this.DiscountBill;
							break;
						case (int)Common.BillOverrideType.DO_NOT_OVERRIDE:
							dBaseAmount = this.SchdAmount;
							dDiscount = 0;
							break;
						case (int)Common.BillOverrideType.APPLY_PER_DIEM:
							dBaseAmount = this.PerDiemAmount;
							dDiscount = 0;
							break;
						case (int)Common.BillOverrideType.APPLY_PER_DIEM_STOP_LOSS:
							if (this.StopLossFlag == 0)
							{
								dBaseAmount = this.AmountBilled;
								dDiscount = this.DiscountBill;
							}
							else
							{
								dBaseAmount = this.PerDiemAmount;
								dDiscount = 0;
							}
							break;
						case (int) Common.BillOverrideType.APPLY_ENT_FEE_SCHD:
							dBaseAmount = this.EntFeeSchdAmt;
							dDiscount = 0;
							break;
						case (int)Common.BillOverrideType.APPLY_DISC_ON_ENT_FEE:
							if(this.EntFeeSchdUsed == 1)
							{
								dBaseAmount = this.EntFeeSchdAmt;
								dDiscount = this.Discount;
							}
							else if(this.EntFeeSchdUsed == 2)
							{
								dBaseAmount = this.EntFeeSchdAmt2nd;
								dDiscount = this.Discount2nd;
							}
							break;
					}//end Switch

					m_objFundsTransSplit.BrsInvoiceDetail.Discount = dDiscount;
					m_objFundsTransSplit.BrsInvoiceDetail.BaseAmount = dBaseAmount;

					if (this.m_iEntFeeSchdUsed == 1)
					{
						if(this.m_dEntFeeSchdAmt > 0)
							dFeeTableAmount =this.m_dEntFeeSchdAmt;
						else
							dFeeTableAmount = 0;
					}
					else if (this.m_iEntFeeSchdUsed == 2) 
					{
						if(this.m_dEntFeeSchdAmt2nd > 0)
							dFeeTableAmount = this.m_dEntFeeSchdAmt2nd;
						else
							dFeeTableAmount = 0;
					}
                    m_objFundsTransSplit.BrsInvoiceDetail.FeeTableAmt = dFeeTableAmount;

					m_objFundsTransSplit.BrsInvoiceDetail.ModifierList.ClearAll();

					objEnum = this.ModValues.GetEnumerator();
					while(objEnum.MoveNext())
					{
						m_objFundsTransSplit.BrsInvoiceDetail.ModifierList.Add(((ModifierValues.ModifierValue)objEnum.Current).m_iModCodeID);
					}
                   m_objFundsTransSplit.BrsInvoiceDetail.ModifierCode=this.ModifierCode;
				}//End try
				catch (Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("BillItem.GetobjSplit.GenericError", m_iClientId) ,p_objException);
				}
				finally
				{
					objProviderContractOverride = null;
				}

				return m_objFundsTransSplit;
			}//end of get
		}//end property objSplit


		/// <summary>
		/// Internal property for ModValues
		/// </summary>
		internal ModifierValues ModValues
		{
			set
			{
				m_objModValues = value;
			}
			get
			{
				return m_objModValues;
			}	
		}

        //srajindersin MITS 29012 dt-09/26/2012
        /// <summary>
        /// Internal property for Deny Line Item
        /// </summary>
        internal bool DenyLineItem
        {
            set
            {
                m_objBrsInvoiceDetail.DenyLineItem = value;
            }
            get
            {
                return m_objBrsInvoiceDetail.DenyLineItem;
            }
        }
		#endregion

		#region Private Methods

		#region "Initialize"
		/// Name			: Initialize
		/// Author			: Navneet Sota
		/// Date Created	: 12/30/2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Initialize the DataModal factory class object and Connection string;
		/// </summary>
		private void Initialize()
		{
			try
			{
				m_objCommon.g_objDataModelFactory = 
					new DataModelFactory(m_objCommon.g_sDsnName, m_objCommon.g_sUserName, m_objCommon.g_sPassword, m_iClientId);	
				
				if(m_objCommon.g_sConnectionString == "")
					m_objCommon.g_sConnectionString = m_objCommon.g_objDataModelFactory.Context.DbConn.ConnectionString;
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("BillItem.Initialize.ErrorInit", m_iClientId) , p_objEx );				
			}			
		}	
		#endregion

		#region GetDataFromDB(p_sFieldName, p_sTableName, p_sCriteria)
		/// Name			: GetDataFromDB
		/// Author			: Navneet Sota
		/// Date Created	: 12/30/2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Formats the required SQL based on passed in values and then,
		/// gets single data value from Database.
		/// </summary>
		/// <param name="p_sFieldName">Field to be retrieved from Database</param>
		/// <param name="p_sTableName">Table from which the value needs to be retrieved</param>
		/// <param name="p_sCriteria">Criteria on which basis the value needs to be retrieved</param>
		internal int GetIntDataFromDB(string p_sFieldName, string p_sTableName, string p_sCriteria)
		{
			//String Objects
			StringBuilder sbSQL = null;

			//Database objects
			DataSet objDataSet=null;

			//Return Variable
			int iReturnValue = 0;

			try
			{
				if(p_sFieldName != "" && p_sTableName != "")				
				{
					//Instantiate the sbSQL object for appending the SQL's	
					sbSQL = new StringBuilder();
					if(p_sCriteria != "")
						sbSQL.Append(" SELECT " + p_sFieldName + " FROM " + p_sTableName + " WHERE " + p_sCriteria);	
					else
						sbSQL.Append(" SELECT " + p_sFieldName + " FROM " + p_sTableName);	
				}
				objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString,sbSQL.ToString(), m_iClientId);
				if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
                    iReturnValue = Conversion.ConvertObjToInt(objDataSet.Tables[0].Rows[0][p_sFieldName], m_iClientId);

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("BillItem.GetDataFromDB.GenericError", m_iClientId) , p_objEx );				
			}			
			finally
			{
				sbSQL = null;
				if (objDataSet != null)
					objDataSet.Dispose();
			}
			return iReturnValue ;
		}

		internal string GetStringDataFromDB(string p_sFieldName, string p_sTableName, string p_sCriteria)
		{
			//String Objects
			StringBuilder sbSQL = null;

			//Database objects
			DataSet objDataSet=null;

			//Return Variable
			string sReturnValue = "";

			try
			{
				if(p_sFieldName != "" && p_sTableName != "")				
				{
					//Instantiate the sbSQL object for appending the SQL's	
					sbSQL = new StringBuilder();
					if(p_sCriteria != "")
						sbSQL.Append(" SELECT " + p_sFieldName + " FROM " + p_sTableName + " WHERE " + p_sCriteria);	
					else
						sbSQL.Append(" SELECT " + p_sFieldName + " FROM " + p_sTableName);	
				}
				objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString,sbSQL.ToString(), m_iClientId);
				if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
					sReturnValue = Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[0][p_sFieldName]);

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("BillItem.GetDataFromDB.GenericError", m_iClientId) , p_objEx );				
			}			
			finally
			{
				sbSQL = null;
				if (objDataSet != null)
					objDataSet.Dispose();
			}

			//Return the result to calling function
			return sReturnValue;
		}	
		#endregion

		# endregion

		

		#region IDisposable Members

        /// <summary>
        /// Release resources
        /// </summary>
		public void Dispose()
		{
			m_objModValues=null;
            m_objCommon = null;

            //If called from FundsForm, these two objects are passed in and should not be disposed
            if (!m_bInvokedThroughFunds)
            {
                if (m_objBrsInvoiceDetail != null)
                    m_objBrsInvoiceDetail.Dispose();
                m_objBrsInvoiceDetail = null;
                if (m_objFundsTransSplit != null)
                    m_objFundsTransSplit.Dispose();
                m_objFundsTransSplit = null;
            }
		}

		#endregion

        #region Destructor
        /// <summary>
        /// destructor for the class
        /// </summary>
		~BillItem()
		{			
            Dispose();
		}
		# endregion
        			

	}// End class
} // End namespace
