﻿
using System;
using System.Collections;
using System.Text;
using System.Data;

using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.Application.BRS
{
	/**************************************************************
	 * $File		: CalculateFunctions.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/05/2004
	 * $Author		: Navneet Sota
	 * $Comment		: This class contains a collection of ModifierValue objects
	 * $Source		: Riskmaster.Application.BRS
	**************************************************************/

	internal class CalculateFunctions : IDisposable
	{
		#region Variable Declarations	

		/// <summary>
		/// Represents the Module variable for Common Class.
		/// </summary>
		private Common m_objCommon = null;

		/// <summary>
		/// Represents the Module variable for Common.LocalCache Class.
		/// </summary>
		private LocalCache m_objLocalCache = null;
		/// <summary>
		/// Tanuj->Should contain errors/alerts for BRS module, will be set by BRS adaptor and errors/alerts will be displayed onto UI. 
		/// </summary>
		private BusinessAdaptorErrors m_objErrors;

		public BusinessAdaptorErrors Errors
		{
			get
			{
				return m_objErrors;
			}
			set
			{
				m_objErrors=value;
			}
		}

        private int m_iClientId = 0;//rkaur27
		# endregion

		#region Constructor	
		/// Name		: CalculateFunctions
		/// Author		: Navneet Sota
		/// Date Created: 01/11/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
		/// <param name="p_objCommon">Common Class object instantiated by Manager </param>
		internal CalculateFunctions(Common p_objCommon, int p_iClientId)
		{
			m_objCommon = p_objCommon;
            m_iClientId = p_iClientId;
            m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString, m_iClientId);
		}
		
		# endregion

		#region Internal Methods

		//For CalcAnesthesia
		#region internal CalcAnesthesia(p_objFeeSchedule, p_objRequest)
		/// Name		: CalcAnesthesia
		/// Author		: Navneet Sota
		/// Date Created: 01/05/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Calculate for Anesthesia
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule object</param>
		/// <param name="p_objRequest">Request Object</param>
		internal double CalcAnesthesia(FeeSchedule p_objFeeSchedule, Request p_objRequest)
		{        
			int iModCodeID = 0;
			int iPhyRelVal = 0;
            
			double dReturnValue = -1;
			double dRV = 0.0;
			double dLoBase = 0.0;
			double dLoMin = 0.0;
			double dTime = 0.0;
			
			StringBuilder sbSQL = null;

			string sSQL = "";
			string sModCode = "";
			string sGeoZip = "";
			string sRvsID = "";
			string sCPT= "";
			string sAnes = "";

			//objects
			DbReader objDbReader = null;
			try
			{
				iModCodeID = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierCode;

				if(m_objLocalCache == null)
                    m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString, m_iClientId);

				m_objLocalCache.GetCodeInfo(iModCodeID, ref sModCode, ref sGeoZip);

				iPhyRelVal = GetPhyRelVal(sModCode);

				//if mod entered then should be P1 thru P6
				if(iPhyRelVal < 0)
					m_objCommon.AddAlert(Globalization.GetString("CalculateFunctions.CalcAnesthesia.PhyRelVal",m_iClientId));

				//Get equiv. anes proc code for surg. CPTs
				if(Conversion.ConvertStrToInteger(p_objRequest.CPT) > 10000)
				{
					sSQL = "SELECT ANE,ANESUB FROM ANECRX";
					sSQL = sSQL + " WHERE CPT = '" + p_objRequest.CPT + "'" ;

					if(m_objCommon.GetDbReader(ref objDbReader,sSQL))
					{
						if(objDbReader.Read())
						{
							sCPT = objDbReader.GetValue("ANE").ToString();
							sAnes = objDbReader.GetValue("ANESUB").ToString();
						}
						else
							m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrCpt",m_iClientId));
					}
					

					if(sAnes.ToUpper() == "IC")
					{
						sCPT = Conversion.ConvertObjToStr(p_objRequest.GetParam("ICCPT"));
						if(sCPT != "")
						{
							m_objCommon.AddAlert(Globalization.GetString("CalculateFunctions.CalcAnesthesia.CPT",m_iClientId));
							p_objRequest.AddParam(GenerateICCPTList(p_objFeeSchedule, p_objRequest),"ICCPTLIST");
							p_objRequest.AddMissingParam("ICCPT");
						}//end if(sCPT != "")
					}//end if(sAnes == "IC")
	
					m_objCommon.HandleSubCode(p_objRequest,sAnes);
				}//if(Conversion.ConvertStrToInteger(p_objRequest.CPT) > 10000)
				else
				{
					sCPT = p_objRequest.CPT;
				}//end if(Conversion.ConvertStrToInteger(p_objRequest.CPT) > 10000)

				objDbReader = null;

				sSQL = "SELECT GEOZIP,RVSID FROM ANEZIP ";
				sSQL = sSQL + " WHERE '" + p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ZipCode 
					+ "' BETWEEN BEGZIP AND ENDZIP" ;
                
				if(m_objCommon.GetDbReader(ref objDbReader,sSQL))
				{
					if(objDbReader.Read())
					{
						sGeoZip = objDbReader.GetValue("GEOZIP").ToString();
						sRvsID = objDbReader.GetValue("RVSID").ToString();
					}
					else
						m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrZipCode",m_iClientId));
				}
				

				sbSQL = new StringBuilder();
				objDbReader = null;

				//Table[0]
				sbSQL.Append("SELECT RV FROM ANERVS ");
				sbSQL.Append(" WHERE RVSID = '" + sRvsID);
				sbSQL.Append(" AND CPT = '" + sCPT + "'");
				               
				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						dRV = Conversion.ConvertStrToDouble(objDbReader.GetValue("RV").ToString());
						dRV = dRV/100;
					}
					else
						m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrCpt",m_iClientId));
				}
				

				objDbReader = null;
				//Table[1]
				sbSQL.Remove(0,sbSQL.Length);
				//sbSQL.Append(" \r\n ");
				sbSQL.Append(" SELECT LOBASE,LOMIN FROM ANEFCT ");
				sbSQL.Append(" WHERE GEOZIP = '" + sGeoZip + "'");
				sbSQL.Append(" AND SPEC = '00' ");
				sbSQL.Append(" AND '" + sCPT + "' BETWEEN BEGPROC AND ENDPROC ");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						dLoBase = Conversion.ConvertStrToDouble(objDbReader.GetValue("LOBASE").ToString());
						dLoMin = Conversion.ConvertStrToDouble(objDbReader.GetValue("LOMIN").ToString());
					
						dLoBase = dLoBase/1000;
						dLoMin = dLoMin/1000;
					}
					else
						m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrCpt",m_iClientId));
				}
				

				dTime = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
				if (sAnes.ToUpper() == "NT")
					dTime = 0;
				else if(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType == (int)Common.UnitsBilledType.ubtUnits)
					dTime = dTime * 15 ; //convert to minutes

				dReturnValue = (dRV * dLoBase) + (iPhyRelVal * dLoBase) + (dTime * dLoMin);

				//Apply Modifier
				dReturnValue = dReturnValue * m_objCommon.GetModifierValue(p_objFeeSchedule.FeeScheduleId, iModCodeID);
			}//end try
			catch (RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CalculateFunctions.CalcAnesthesia.GenericErr",m_iClientId) ,p_objException);
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}

				objDbReader = null;
				sbSQL = null;
			}

			//Return value to the calling function
			return dReturnValue;

		}//end function
		#endregion

		//For CalcDental
		#region internal CalcDental(p_objFeeSchedule, p_objRequest)
		/// Name			: CalcDental
		/// Author			: Navneet Sota
		/// Date Created	: 01/05/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Calculate for Dental
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Object</param>
		internal double CalcDental(FeeSchedule p_objFeeSchedule, Request p_objRequest)
		{        
			//int variable
			int iModCodeID = 0;
            
			//double variables
			double dReturnValue = -1;
			double dRV = 0.0;
			double dUnitsBilled = 0.0;
			double dFactor = 0.0;
			
			//string variables
			string sGeoZip = "";
			string sRvsID = "";
			string sRvSub= "";

			//object
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			try
			{
				sbSQL = new StringBuilder();

				//Table[0]
				sbSQL.Append(" SELECT GEOZIP,RVSID FROM DENZIP ");
				sbSQL.Append(" WHERE '" + p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ZipCode);
				sbSQL.Append("' BETWEEN BEGZIP AND ENDZIP");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						sGeoZip = objDbReader.GetValue("GEOZIP").ToString();
						sRvsID = objDbReader.GetValue("RVSID").ToString();
					}
					else
						m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrZipCode",m_iClientId));
				}
				

				objDbReader = null;
				//Table[0]
				sbSQL.Remove(0,sbSQL.Length);
				//sbSQL.Append(" \r\n ");
				sbSQL.Append(" SELECT RV,RVSUB FROM DENRVSCE WHERE ");
				sbSQL.Append(" RVSID = '" + sRvsID + "'");
				sbSQL.Append(" AND PROC_ = '" + p_objRequest.CPT + "'");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						dRV = Conversion.ConvertStrToDouble(objDbReader.GetValue("RV").ToString());
						sRvSub = objDbReader.GetValue("RVSUB").ToString();
					}
				}

				m_objCommon.HandleSubCode(p_objRequest,sRvSub);

				sbSQL.Remove(0,sbSQL.Length);
				objDbReader = null;
				objDbReader.Close();

				//Table[0]
				//sbSQL.Append(" \r\n ");
				
				sbSQL.Append(" SELECT PER" + p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.Percentile);
				sbSQL.Append("  PERCENTILE,SPEC FROM DENFCT ");
				sbSQL.Append(" WHERE GEOZIP = '" + sGeoZip + "'");
				sbSQL.Append(" AND (SPEC = '01' OR SPEC = '00')");
				sbSQL.Append(" AND '" + p_objRequest.CPT + "' BETWEEN BEGPROC AND ENDPROC");
				sbSQL.Append(" ORDER BY SPEC DESC ");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
						dFactor = Conversion.ConvertStrToDouble(objDbReader.GetValue("PERCENTILE").ToString());
				}

				dRV = (dRV / 100) * (dFactor / 1000);
				//Apply units billed
				dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
                //abisht MITS 11156
                string sTempTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);
                //if (int.Parse(m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode)) == BillItem.ANESTHESIA_SHORT_CODE &&
                if (sTempTos.Trim() == BillItem.ANESTHESIA_SHORT_CODE.ToString() &&
					p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType == (int)Common.UnitsBilledType.ubtMINUTES)
					dRV = dRV * (dUnitsBilled / 15);
				else
					dRV = dRV * dUnitsBilled;

				//Apply Modifier
				iModCodeID = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierCode;

				dReturnValue = m_objCommon.GetModifierValue(p_objFeeSchedule.FeeScheduleId,iModCodeID);
			}//end try
			catch (RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CalculateFunctions.CalcDental.GenericErr",m_iClientId) ,p_objException);
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}

				objDbReader = null;
				sbSQL = null;
			}

			//Return value to the calling function
			return dReturnValue;

		}//end function

		#endregion

		//For CalcExtended
		#region internal CalcExtended(p_objFeeSchedule, p_objRequest, p_sDsnName, p_sUserName, p_sPassword)
		/// Name		: CalcExtended
		/// Author		: Navneet Sota
		/// Date Created: 01/05/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Calculate for Extended
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Class Object</param>
		internal double CalcExtended(FeeSchedule p_objFeeSchedule, Request p_objRequest)
		{        
			int iFeeScheduleId = 0;
			int iCnt = 0;
			int iTemp = 0;
            
			double dReturnValue = -1;
			double dFactor = 0.0;
			double dModValue = 0.0;
			double dMaxRlv = 0.0;
			double dTemp = 0.0;
			
			StringBuilder sbSQL = null;

			string sCPT = "";
			string sShortCode = "";
			string sCodeDesc = "";
			string sTemp = "";
			string sErrMsg = "";
			string sStateCode = string.Empty;
			string sStateName = string.Empty;

			bool bPC = false;
			bool bTC = false;
            
			RelativeValues objRelativeValues = null;
			FeeSchedule objFeeSchedule = null;
			DataSimpleList objDSList = null;

			//Database objects
			DataSet objDataSet=null;

			try
			{
				objRelativeValues = new RelativeValues(m_iClientId);
				if(m_objLocalCache == null)
                    m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString, m_iClientId);
                //abisht MITS 11156
                //int tos = int.Parse(m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode));
                string sTempTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);
				iFeeScheduleId = p_objFeeSchedule.FeeScheduleId;
				sCPT = p_objRequest.CPT;
				m_objLocalCache.GetStateInfo(p_objFeeSchedule.StateId,ref sStateCode,ref sStateName);
				sbSQL = new StringBuilder();

				sbSQL.Append(" SELECT CPT_DESC,QUESTION,AMOUNT, PROF_COMPONENT,FOLLUP, BYREPORT, ");
				sbSQL.Append(" STARRED, ASSISTSURG,ANESTHESIA_VU, ANES_BR, ANES_NA, TOS, POS ");
				sbSQL.Append(" FROM WRCMP_CPT WHERE TABLE_ID = " + iFeeScheduleId);
				sbSQL.Append(" AND CPT = '" + sCPT + "' ORDER BY CPT" );

				objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString,sbSQL.ToString(),m_iClientId);
				if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
				{
					objRelativeValues = GetMatchingRLV(objDataSet,p_objRequest,sStateCode);
//					objRelativeValues.RelativeValueObject.m_sCPT = 
//						objDataSet.Tables[0].Rows[0]["CPT_DESC"].ToString();
//					objRelativeValues.RelativeValueObject.m_sByReport = 
//						objDataSet.Tables[0].Rows[0]["BYREPORT"].ToString();
//					objRelativeValues.RelativeValueObject.m_sStarred = 
//						objDataSet.Tables[0].Rows[0]["STARRED"].ToString();
//					objRelativeValues.RelativeValueObject.m_sQuestion = 
//						objDataSet.Tables[0].Rows[0]["QUESTION"].ToString();
//					objRelativeValues.RelativeValueObject.m_sAssistSurg = 
//						objDataSet.Tables[0].Rows[0]["ASSISTSURG"].ToString();
//					objRelativeValues.RelativeValueObject.m_sAnesBr = 
//						objDataSet.Tables[0].Rows[0]["ANES_BR"].ToString();
//					objRelativeValues.RelativeValueObject.m_sAnesNa = 
//						objDataSet.Tables[0].Rows[0]["ANES_NA"].ToString();
//					objRelativeValues.RelativeValueObject.m_dAmount = 
//						Conversion.ConvertStrToDouble(objDataSet.Tables[0].Rows[0]["AMOUNT"].ToString());
//					objRelativeValues.RelativeValueObject.m_dProfComponent = 
//						Conversion.ConvertStrToDouble(objDataSet.Tables[0].Rows[0]["PROF_COMPONENT"].ToString());
//					objRelativeValues.RelativeValueObject.m_dAnesthesiaVu = 
//						Conversion.ConvertStrToDouble(objDataSet.Tables[0].Rows[0]["ANESTHESIA_VU"].ToString());
//					objRelativeValues.RelativeValueObject.m_iFollup = 
//						Conversion.ConvertStrToInteger(objDataSet.Tables[0].Rows[0]["FOLLUP"].ToString());
				}//end if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
				else
				{
					m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrCpt",m_iClientId) + " CPT=" + sCPT + ", TABLE_ID=" + iFeeScheduleId );
					//Exit function
					return dReturnValue;
				}
                //abisht MITS 11156
                if (sTempTos.Trim() == BillItem.ANESTHESIA_SHORT_CODE.ToString()) //Anesthesia
				{
					m_objLocalCache.GetCodeInfo(p_objFeeSchedule.FeeScheduleId, ref sShortCode, ref sCodeDesc);

					//Maryland Anessthesia codes are listed by Surgery CPT
					if(sShortCode.ToUpper() != "MD")
					{
						//override with Anesthesia values
						objRelativeValues.RelativeValueObject.m_dAmount = objRelativeValues.RelativeValueObject.m_dAnesthesiaVu;
						objRelativeValues.RelativeValueObject.m_sByReport = objRelativeValues.RelativeValueObject.m_sAnesBr;
					}
					else
						objRelativeValues.RelativeValueObject.m_sAnesNa = ""; 
				}//end if(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode == BillItem.ANESTHESIA_SHORT_CODE)
				else
					objRelativeValues.RelativeValueObject.m_sAnesNa = ""; 

				dFactor = 1;
				sbSQL.Remove(0,sbSQL.Length);

				//sbSQL.Append(" \r\n ");
				sbSQL.Append(" SELECT FACTOR FROM WRCMP_FACTOR, CODES ");
				sbSQL.Append(" WHERE CLINICAL_CAT_CODE = CODES.CODE_ID ");
				sbSQL.Append(" AND WRCMP_FACTOR.TABLE_ID = " + iFeeScheduleId);
				sbSQL.Append(" AND '" + p_objRequest.CPT + "' BETWEEN BEGPROC AND ENDPROC ");
				//abisht MITS 11156
				if(sTempTos.Trim() == BillItem.ANESTHESIA_SHORT_CODE.ToString()) //Anesthesia
					sbSQL.Append(" AND CODES.SHORT_CODE IN ('ANES','MD-ANES')");
				else
					sbSQL.Append(" AND CODES.SHORT_CODE <> 'ANES' AND CODES.SHORT_CODE <> 'MD-ANES' ");

				objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString,sbSQL.ToString(), m_iClientId);
				if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
					dFactor = Conversion.ConvertStrToDouble(objDataSet.Tables[0].Rows[0]["FACTOR"].ToString());

				//Anesthesia
                //abisht MITS 11156
				if(sTempTos.Trim() == BillItem.ANESTHESIA_SHORT_CODE.ToString()) 
				{
					//Enforce Minimum Billed Units
					p_objRequest.UnitsBilledMin = 0;
					if(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum < p_objRequest.UnitsBilledMin)
						p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum = p_objRequest.UnitsBilledMin;

					dTemp = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
					if(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType == (int)Common.UnitsBilledType.ubtMINUTES)
						dTemp = dTemp /15;

					dReturnValue = (objRelativeValues.RelativeValueObject.m_dAmount + dTemp) * dFactor;

				}//end if(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode == BillItem.ANESTHESIA_SHORT_CODE) //Anesthesia
				else //For All others - Not Anesthesia  
				{
					//Enforce Minimum Billed Units
					p_objRequest.UnitsBilledMin = 1;
					if(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum < p_objRequest.UnitsBilledMin)
						p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum = p_objRequest.UnitsBilledMin;
					
					objDSList = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList;
					if(objDSList.Count > 0)
					{
						//Apply modifiers if user added to codelist
						dMaxRlv = -1;
						dModValue = 1;
						for(iCnt = 0; iCnt < objDSList.Count;iCnt++)
						{
                            iTemp = Conversion.ConvertObjToInt(objDSList[iCnt], m_iClientId);
							m_objLocalCache.GetCodeInfo(iTemp, ref sShortCode, ref sCodeDesc);
							sTemp += ", " + sShortCode;

							//Grab modifier value and max_rlv for the modifier code
							objFeeSchedule = new FeeSchedule(m_objCommon, m_iClientId);
							objFeeSchedule.GetMaxRLVAndModValue(iTemp,p_objRequest.CPT, ref dModValue, ref dTemp);
							if (dMaxRlv < dTemp)
								dMaxRlv = dTemp;

							//create error message if max_rlv value not found
							if(dTemp == -1)
								sErrMsg = sErrMsg + ", " + sShortCode;
						}//for(iCnt = 0; iCnt < objDSList.Count;iCnt++)

						if(dMaxRlv < objRelativeValues.RelativeValueObject.m_dAmount && dMaxRlv >= 0)
							objRelativeValues.RelativeValueObject.m_dAmount = dMaxRlv;

						if(sErrMsg.Length > 0)
						{
							sErrMsg = "Max rlv value not found for modifier codes (" + sErrMsg.Substring(0, 2);
							sErrMsg = sErrMsg + ") for CPT " + p_objRequest.CPT + " and will not reduce the amount.";
							//AddAlert (objRequest, sErrMsg);
						}//end if(sErrMsg.Length > 0)

						//PC and TC are mutually exclusive in a semantic sense.
						Array arrTemp = sTemp.Substring(2).Split(',');
						for(iCnt = 0; iCnt<arrTemp.Length - 1; iCnt++)
						{
							sTemp = arrTemp.GetValue(iCnt).ToString();
							if(sTemp == "PC" || sTemp == "26" || sTemp == "-26")
								bPC = true;
							if(sTemp == "TC" || sTemp == "27" || sTemp == "-27")
								bTC = true;
						}//for(iCnt = 0; iCnt<arrTemp.Length - 1; iCnt++)

						if(bPC) //Professional/Physician Component
							dReturnValue = objRelativeValues.RelativeValueObject.m_dProfComponent * dFactor * 
								p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
						else if(bTC) //Technical Component
							dReturnValue = (objRelativeValues.RelativeValueObject.m_dAmount - objRelativeValues.RelativeValueObject.m_dProfComponent)
								* dFactor * p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
						else //Reported as one bill
							dReturnValue = objRelativeValues.RelativeValueObject.m_dAmount * dFactor * 
								dModValue * p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
					}//end if(objDSList.Count > 0)
					else//no modifiers
						dReturnValue = objRelativeValues.RelativeValueObject.m_dAmount * dFactor * 
							p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
				}//end else for All other - Not Anesthesia  

				//Display Subcode sensitive messages
				HandleSubCodeExtended(p_objRequest,objRelativeValues);
			}//end try
			catch (RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CalculateFunctions.CalcExtended.GenericErr", m_iClientId) ,p_objException);
			}
			finally
			{
				if (objDataSet != null)
					objDataSet.Dispose();

				objDataSet = null;
				sbSQL = null;
				objDSList = null;
				objFeeSchedule = null;
				objRelativeValues = null;
			}

			//Return value to the calling function
			return dReturnValue;

		}//end function

		#endregion

		//For CalcHCPCS
		#region internal CalcHCPCS(p_objFeeSchedule, p_objRequest)
		/// Name		: CalcHCPCS
		/// Author		: Navneet Sota
		/// Date Created: 01/06/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Calculate for HCPCS
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Object</param>
		internal double CalcHCPCS(FeeSchedule p_objFeeSchedule, Request p_objRequest)
		{        
			//int variable
			int iModCodeID = 0;
            
			//double variable
			double dReturnValue = -1;
			double dRV = 0.0;
			double dUnitsBilled = 0.0;
			double dFactor = 0.0;
			
			//string variable
			string sGeoZip = "";
			string sRvsID = "";
			string sRvSub = "";
			string sMod = "";
			string sTempMOD = "";

			//bool variable
			bool bFlag = false;

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			try
			{
				sbSQL = new StringBuilder();

				//Table[0]
				sbSQL.Append(" SELECT GEOZIP,RVSID FROM HCPZIP ");
				sbSQL.Append(" WHERE '" + p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ZipCode);
				sbSQL.Append("' BETWEEN BEGZIP AND ENDZIP");

				if (p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList.Count > 0)
				{
					bFlag = true;
					sbSQL.Append(" AND [MOD] = '" + sMod + "'");
				}
				
				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						sGeoZip = objDbReader.GetValue("GEOZIP").ToString();
						sRvsID = objDbReader.GetValue("RVSID").ToString();
					}
					else
						m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrZipCode", m_iClientId));
				}
				

				if (bFlag)
				{
                    iModCodeID = Conversion.ConvertObjToInt(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList[1], m_iClientId);
					if(m_objLocalCache == null)
                        m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString, m_iClientId);
					m_objLocalCache.GetCodeInfo(iModCodeID, ref sMod, ref sTempMOD);
				}

				objDbReader = null;
				sbSQL.Remove(0, sbSQL.Length);

				sbSQL.Append(" SELECT RV,RVSUB, [MOD] FROM HCPRVSCE WHERE ");
				sbSQL.Append(" RVSID = '" + sRvsID + "'");
				sbSQL.Append(" AND PROC_ = '" + p_objRequest.CPT + "'");
			
				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						dRV = Conversion.ConvertStrToDouble(objDbReader.GetValue("RV").ToString());
						sRvSub = objDbReader.GetValue("RVSUB").ToString();
						if(bFlag)
							sTempMOD = objDbReader.GetValue("MOD").ToString();
					}
					else
						m_objCommon.AddAlert(Globalization.GetString("CalculateFunctions.CalcHCPCS.Alert1", m_iClientId));
				}
				

				if(sMod == "" &&
					(sTempMOD.ToUpper() == "NU" || sTempMOD.ToUpper() == "UE" || sTempMOD.ToUpper() == "RR"))
					m_objCommon.AddAlert(Globalization.GetString("CalculateFunctions.CalcHCPCS.Alert1", m_iClientId));
					
				m_objCommon.HandleSubCode(p_objRequest,sRvSub);
				objDbReader = null;
				sbSQL.Remove(0,sbSQL.Length);

				//Table[0]
				//sbSQL.Append(" \r\n ");

				sbSQL.Append(" SELECT PER" + p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.Percentile);
				sbSQL.Append("  PERCENTILE,SPEC FROM HCPFCT ");
				sbSQL.Append(" WHERE GEOZIP = '" + sGeoZip + "'");
				sbSQL.Append(" AND (SPEC = '00' OR SPEC = '02')");
				sbSQL.Append(" AND '" + p_objRequest.CPT + "' BETWEEN BEGPROC AND ENDPROC");
				sbSQL.Append(" ORDER BY SPEC DESC ");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
						dFactor = Conversion.ConvertStrToDouble(objDbReader.GetValue("PERCENTILE").ToString());
				}

				dRV = (dRV / 100) * (dFactor / 1000);
				//Apply units billed
				dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
                //abisht MITS 11156
                //if (int.Parse(m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode)) == BillItem.ANESTHESIA_SHORT_CODE &&
                string sTempTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);
                if (sTempTos.Trim() == BillItem.ANESTHESIA_SHORT_CODE.ToString() &&
					p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType == (int)Common.UnitsBilledType.ubtMINUTES)
					dRV = dRV * (dUnitsBilled / 15);
				else
					dRV = dRV * dUnitsBilled;

				//Apply Modifier
				iModCodeID = Conversion.ConvertObjToInt(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList[1], m_iClientId);

				dReturnValue = m_objCommon.GetModifierValue(p_objFeeSchedule.FeeScheduleId,iModCodeID);
			}//end try
			catch (RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CalculateFunctions.CalcHCPCS.GenericErr", m_iClientId) ,p_objException);
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}

				objDbReader = null;
				sbSQL = null;
			}

			//Return value to the calling function
			return dReturnValue;

		}//end function

		#endregion

		//For CalcOHWC
		#region internal CalcOHWC(p_objFeeSchedule, p_objRequest)
		/// Name		: CalcOHWC
		/// Author		: Navneet Sota
		/// Date Created: 01/06/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Calaculte for OHWC
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Object</param>
		internal double CalcOHWC(FeeSchedule p_objFeeSchedule, Request p_objRequest)
		{        
			//int variable
			int iModCodeID = 0;
            
			//double variable
			double dReturnValue = -1;
			double dProfComp = 0.0;
			double dUnitsBilled = 0.0;
			
			//string variables
			string sTable = "";
			string sMod = "";
			string sTemp = "";

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			try
			{
				sbSQL = new StringBuilder();

				//Table[0]
				sbSQL.Append(" SELECT DISTINCT TABLE FROM AREA ");
				sbSQL.Append(" WHERE SHORT_ZIP = '" + p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ZipCode.Substring(0,3));
				
				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
						sTable = objDbReader.GetValue("TABLE").ToString();
					else
					{
						m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrZipCode", m_iClientId));
						//exit function
						return dReturnValue;
					}
				}
				

				objDbReader = null;
				sbSQL.Remove(0,sbSQL.Length);

				sbSQL.Append(" SELECT AMOUNT,PROF_COMPONENT FROM ").Append(sTable).Append(" WHERE ");
				sbSQL.Append(" BILLING_CODE = '").Append(p_objRequest.CPT).Append("'");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						dReturnValue = Conversion.ConvertStrToDouble(objDbReader.GetValue("AMOUNT").ToString());
						dProfComp = Conversion.ConvertStrToDouble(objDbReader.GetValue("PROF_COMPONENT").ToString());
					}
					else
					{
						m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrZipCode", m_iClientId));
						//exit function
						return dReturnValue;
					}
				}
				

				if(dReturnValue < 0 || dProfComp < 0)
				{
					m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrZipCode", m_iClientId));
					//exit function
					return dReturnValue;
				}

				if (p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList.Count > 0)
				{
                    iModCodeID = Conversion.ConvertObjToInt(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList[1], m_iClientId);
					if(m_objLocalCache == null)
                        m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString, m_iClientId);
					m_objLocalCache.GetCodeInfo(iModCodeID, ref sMod, ref sTemp);
					
					if(sMod == "26")
						dReturnValue = dProfComp;
				}

				//Apply units billed
				dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
                //abisht MITS 11156
                //if (int.Parse(m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode)) == BillItem.ANESTHESIA_SHORT_CODE &&
                string sTempTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);
                if (sTempTos.Trim()  == BillItem.ANESTHESIA_SHORT_CODE.ToString() &&
					p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType == (int)Common.UnitsBilledType.ubtMINUTES)
					dReturnValue = dReturnValue * (dUnitsBilled / 15);
				else
					dReturnValue = dReturnValue * dUnitsBilled;

				//Apply Modifier
				dReturnValue = dReturnValue * m_objCommon.GetModifierValue(p_objFeeSchedule.FeeScheduleId,iModCodeID);
			}//end try
			catch (RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CalculateFunctions.CalcOHWC.GenericErr", m_iClientId) ,p_objException);
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}

				objDbReader = null;
				sbSQL = null;
			}

			//Return value to the calling function
			return dReturnValue;

		}//end function

		#endregion

		//For CalcOHWC97
		#region internal CalcOHWC97(p_objFeeSchedule, p_objRequest)
		/// Name		: CalcOHWC97
		/// Author		: Navneet Sota
		/// Date Created: 01/06/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Calaculate for OHWC-97
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Object</param>
		internal double CalcOHWC97(FeeSchedule p_objFeeSchedule, Request p_objRequest)
		{        
			//int variable
			int iModCodeID = 0;
            
			//double variables
			double dReturnValue = -1;
			double dUnitsBilled = 0.0;
			double dWCTF = 0.0;
			double dWCPF = 0.0;
			double dWCTECH = 0.0;
			
			//string variables
			string sMod = "";
			string sPrimaryMarket = "";
			string sSecondaryMarket = "";

			//bool variable
			bool bCheckSecMarket = false;

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			try
			{
				sbSQL = new StringBuilder();

				//Table[0]
				sbSQL.Append(" SELECT PRIMARY_MARKET,SECONDARY_MARKET FROM AREA ");
				sbSQL.Append(" WHERE SHORT_ZIP = '" + p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ZipCode.Substring(0,3));
				
				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						sPrimaryMarket = objDbReader.GetValue("PRIMARY_MARKET").ToString();
						sSecondaryMarket = objDbReader.GetValue("SECONDARY_MARKET").ToString();
					}
					else
					{
						m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrZipCode", m_iClientId));
						//exit function
						return dReturnValue;
					}
				}
				

				bCheckSecMarket = true;
				objDbReader = null;
				sbSQL.Remove(0,sbSQL.Length);

				//sbSQL.Append(" \r\n ");
				sbSQL.Append(" SELECT WCPF,WCTF,WCTECH FROM PLUSFEE WHERE ");
				sbSQL.Append(" MARKET = '").Append(sPrimaryMarket).Append("'");
				sbSQL.Append(" AND CPT = '").Append(p_objRequest.CPT).Append("'");
				
				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						dWCPF = Conversion.ConvertStrToDouble(objDbReader.GetValue("WCPF").ToString());
						dWCTF = Conversion.ConvertStrToDouble(objDbReader.GetValue("WCTF").ToString());
						dWCTECH = Conversion.ConvertStrToDouble(objDbReader.GetValue("WCTECH").ToString());
						bCheckSecMarket = false;
					}
				}

				if(!bCheckSecMarket)
					bCheckSecMarket = IsAmountNegative(dWCPF,dWCTF,dWCTECH,sMod);

				if(bCheckSecMarket)
				{
					objDbReader = null;
					sbSQL.Remove(0,sbSQL.Length);

					//sbSQL.Append(" \r\n ");
					sbSQL.Append(" SELECT WCPF,WCTF,WCTECH FROM PLUSFEE WHERE ");
					sbSQL.Append(" MARKET = '").Append(sSecondaryMarket).Append("'");
					sbSQL.Append(" AND CPT = '").Append(p_objRequest.CPT).Append("'");

					if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
					{
						if(objDbReader.Read())
						{
							dWCPF = Conversion.ConvertStrToDouble(objDbReader.GetValue("WCPF").ToString());
							dWCTF = Conversion.ConvertStrToDouble(objDbReader.GetValue("WCTF").ToString());
							dWCTECH = Conversion.ConvertStrToDouble(objDbReader.GetValue("WCTECH").ToString());
						}
					}

					if(IsAmountNegative(dWCPF,dWCTF,dWCTECH,sMod))
					{
						m_objCommon.AddAlert(Globalization.GetString("CalculateFunctions.CalcOHWC97.AmtNegative", m_iClientId));
						// exit function
						return dReturnValue;
					}
				}//end if(bCheckSecMarket)

				switch(sMod.Substring(2))
				{
					case "26":
						dReturnValue = dWCPF;
						break;
					case "27":
						dReturnValue = dWCTECH;
						break;
					default:
						dReturnValue = dWCTF;
						break;
				}

				//Apply units billed
				dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
                //abisht MITS 11156
                //if (int.Parse(m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode)) == BillItem.ANESTHESIA_SHORT_CODE &&
                string sTempTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);
                if (sTempTos.Trim() == BillItem.ANESTHESIA_SHORT_CODE.ToString() &&
					p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType == (int)Common.UnitsBilledType.ubtMINUTES)
					dReturnValue = dReturnValue * (dUnitsBilled / 15);
				else
					dReturnValue = dReturnValue * dUnitsBilled;

				//Apply Modifier
				dReturnValue = dReturnValue * m_objCommon.GetModifierValue(p_objFeeSchedule.FeeScheduleId,iModCodeID);
			}//end try
			catch (RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CalculateFunctions.CalcOHWC97.GenericErr", m_iClientId) ,p_objException);
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}

				objDbReader = null;
				sbSQL = null;
			}

			//Return value to the calling function
			return dReturnValue;

		}//end function

		#endregion

		//For CalcOutpatient
		#region internal CalcOutpatient(p_objFeeSchedule, p_objRequest)
		/// Name		: CalcOutpatient
		/// Author		: Navneet Sota
		/// Date Created: 01/06/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Calculate for out patient
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Object</param>
		internal double CalcOutpatient(FeeSchedule p_objFeeSchedule, Request p_objRequest)
		{        
			//int variable
			int iModCodeID = 0;
            
			//double variables
			double dReturnValue = -1;
			double dRV = 0.0;
			double dUnitsBilled = 0.0;
			double dFactor = 0.0;
			
			//string variable
			string sGeoZip = "";
			string sRvsID = "";
			string sRvSub = "";

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			try
			{
				sbSQL = new StringBuilder();

				//Table[0]
				sbSQL.Append(" SELECT GEOZIP,RVSID FROM OPUZIP ");
				sbSQL.Append(" WHERE '" + p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ZipCode);
				sbSQL.Append("' BETWEEN BEGZIP AND ENDZIP");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						sGeoZip = objDbReader.GetValue("GEOZIP").ToString();
						sRvsID = objDbReader.GetValue("RVSID").ToString();
					}
					else
						m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrZipCode", m_iClientId));
				}
				

				sbSQL.Remove(0, sbSQL.Length);
				objDbReader = null;
				//Table[1]
				//sbSQL.Append(" \r\n ");
				sbSQL.Append(" SELECT RV,RVSUB FROM OPURVSCE WHERE ");
				sbSQL.Append(" RVSID = '" + sRvsID + "'");
				sbSQL.Append(" AND PROC_ = '" + p_objRequest.CPT + "'");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						dRV = Conversion.ConvertStrToDouble(objDbReader.GetValue("RV").ToString());
						sRvSub = objDbReader.GetValue("RVSUB").ToString();
					}
					else
						m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrZipCode", m_iClientId));
				}
				

				objDbReader = null;
				sbSQL.Remove(0,sbSQL.Length);
				//Table[0]
				//sbSQL.Append(" \r\n ");
				sbSQL.Append(" SELECT PER" + p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.Percentile);
				sbSQL.Append("  PERCENTILE,SPEC FROM OPUFCT ");
				sbSQL.Append(" WHERE GEOZIP = '" + sGeoZip + "'");

				if(sRvSub.ToUpper() == "NR")
					sbSQL.Append(" AND SPEC = '11'");
				else
					sbSQL.Append(" AND SPEC = '10'");

				sbSQL.Append(" AND '" + p_objRequest.CPT + "' BETWEEN BEGPROC AND ENDPROC");
				sbSQL.Append(" ORDER BY SPEC DESC ");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
						dFactor = Conversion.ConvertStrToDouble(objDbReader.GetValue("PERCENTILE").ToString());
				}

				dRV = (dRV / 100) * (dFactor / 1000);

				m_objCommon.HandleSubCode(p_objRequest,sRvSub);

				//Apply units billed
				dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
                //abisht MITS 11156
                //if (int.Parse(m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode)) == BillItem.ANESTHESIA_SHORT_CODE &&
                string sTempTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);
                if (sTempTos.Trim() == BillItem.ANESTHESIA_SHORT_CODE.ToString() &&
					p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType == (int)Common.UnitsBilledType.ubtMINUTES)
					dRV = dRV * (dUnitsBilled / 15);
				else
					dRV = dRV * dUnitsBilled;

				//Apply Modifier
				if (p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList.Count > 0)
                    iModCodeID = Conversion.ConvertObjToInt(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList[1], m_iClientId);

				dReturnValue = dRV * m_objCommon.GetModifierValue(p_objFeeSchedule.FeeScheduleId,iModCodeID);

			}//end try
			catch (RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CalculateFunctions.CalcOutPatient.GenericErr", m_iClientId) ,p_objException);
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}

				objDbReader = null;
				sbSQL = null;
			}

			//Return value to the calling function
			return dReturnValue;

		}//end function

		#endregion

		//For CalcStAnthony
		#region internal CalcStAnthony(p_objFeeSchedule, p_objRequest)
		/// Name		: CalcStAnthony
		/// Author		: Navneet Sota
		/// Date Created: 01/06/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Calculate for St. Anthony
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Object</param>
		internal double CalcStAnthony(FeeSchedule p_objFeeSchedule, Request p_objRequest)
		{        
			//int variable
			int iModCodeID = 0;
            
			//double variables
			double dReturnValue = -1;
			double dRLV = 0.0;
			double dMaxSchdAmount = -1;
			double dMaxRlv = 0.0;
			double dModValue = 0.0;
			double dUnitsBilled = 0.0;
			double dFactor = 0.0;
			
			//string variables
			string sSubCode = "";
			string sMod = "";
			string sTos = "";
			string sTemp = "";

			try
			{
				if(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList.Count > 0)
				{
                    iModCodeID = Conversion.ConvertObjToInt(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList[1], m_iClientId);
					if(m_objLocalCache == null)
                        m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString, m_iClientId);
					m_objLocalCache.GetCodeInfo(iModCodeID, ref sMod, ref sTemp);
				}

                sTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode).ToString();

				//If Modifier is B then use Flat fee from Payment Groups Table this table is created during import.
				//These tables define max fees for Ambulatory Surgical Center (ASC) procdures.
				//If payment grp table not found then use the RVS tables to compute Scheduled Amount
				if(sMod == "B")
					dMaxSchdAmount = p_objFeeSchedule.CalcASCAmount(p_objRequest);

				dRLV = GetMatchingRLV(p_objFeeSchedule,p_objRequest,sMod, ref sSubCode);
				if(dRLV == -1)
				{
					m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrCpt", m_iClientId));
					//exit function
					return dReturnValue;
				}

				dFactor = 1;
				dFactor = GetMatchingFactor(p_objFeeSchedule,p_objRequest,sMod,sTos);
				if(dFactor == -1)
				{
					m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrCpt", m_iClientId));
					//exit function
					return dReturnValue;
				}

				if(sSubCode.ToUpper() != "BR" || dRLV != 0)
					m_objCommon.HandleSubCode(p_objRequest,sSubCode);
				else
				{
					m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrCpt", m_iClientId));
					//exit function
					return dReturnValue;
				}

				//Handle special modifiers like 51A, 51B and 51C (added for NV codes 97010 to 97799 inclusive)
				p_objFeeSchedule.GetMaxRLVAndModValue(iModCodeID,p_objRequest.CPT,ref dModValue,ref dMaxRlv);
				if(dMaxRlv >= 0 && dMaxRlv < dRLV)
					dRLV = dMaxRlv;

				dReturnValue = dRLV * dFactor;
				//Multiply amount by Units
				dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;

				if(sTos == "7")
				{
					if (p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType == (int)Common.UnitsBilledType.ubtMINUTES)
						dUnitsBilled = dUnitsBilled / 15;
					dReturnValue = dReturnValue + dUnitsBilled * dFactor;
				}
				else
					dReturnValue = dReturnValue * dUnitsBilled;

				//apply modifier, if specified
				if(sMod != "") 
					dReturnValue = dReturnValue * dModValue;

				//limit Scheduled Amount by max specified for the group 
				//(Or in the case of a 0 Scheduled Amount then limit to group limit)
				if ((dMaxSchdAmount >= 0 && dReturnValue > dMaxSchdAmount) || dReturnValue == 0)
					dReturnValue = dMaxSchdAmount;

			}//end try
			catch (RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CalculateFunctions.CalcStAnthony.GenericErr", m_iClientId) ,p_objException);
			}

			//Return value to the calling function
			return dReturnValue;

		}//end function

		#endregion

		//For CalcUCR
		#region internal CalcUCR(p_objFeeSchedule, p_objRequest)
		/// Name		: CalcUCR
		/// Author		: Navneet Sota
		/// Date Created: 01/06/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Calculate for UCR
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Object</param>
		internal double CalcUCR(FeeSchedule p_objFeeSchedule, Request p_objRequest)
		{        
			//int variable
			int iModCodeID = 0;
            
			//double variables
			double dReturnValue = -1;
			double dRV = 0.0;
			double dTemp = 0.0;
			double dUnitsBilled = 0.0;
			
			//string variables
			string sGeoZip = "";
			string sRvsID = "";
			string sSubCode = "";
			string sShortCode = "";
			string sCodeDesc = "";

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			try
			{
				sbSQL = new StringBuilder();

				//Table[0]
				sbSQL.Append(" SELECT GEOZIP,RVSID FROM UCRZIP ");
				sbSQL.Append(" WHERE '" + p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ZipCode.Substring(0,3));
				sbSQL.Append("' BETWEEN BEGZIP AND ENDZIP");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						sGeoZip = objDbReader.GetValue("GEOZIP").ToString();
						sRvsID = objDbReader.GetValue("RVSID").ToString();
					}
					else
					{
						m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrZipCode", m_iClientId));
						//exit function
						return dReturnValue;
					}
				}
				

				//In brsworld modifier is a multi-select code box combined with combo box.  
				//whatever is selected in combo box is the one used for calculation.  rest are ignored
				//In rmnet, change "modifierlist" control from "codelist" to "code" in FillBrsDom 
				//if calctype in (idbucr1194,idbucr9915,idbAnes)
				if (p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList.Count > 0)
				{
					foreach (DictionaryEntry objItem in p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList)
					{
                        iModCodeID = Conversion.ConvertObjToInt(objItem.Value, m_iClientId);
						break;
					}
					
					if(m_objLocalCache == null)
                        m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString, m_iClientId);
					m_objLocalCache.GetCodeInfo(iModCodeID, ref sShortCode, ref sCodeDesc);
                    
					switch (sShortCode.ToUpper())
					{
						case "26":
						case "26E":
						case "26L":
						case "26S":
							//Prof Component
							dRV = GetRVAndSubCode(p_objFeeSchedule, p_objRequest, "03", sRvsID, sGeoZip, ref sSubCode);
							break;
						case "52":
						case "27":
						case "27E":
						case "27L":
						case "27S":
							//Technical Component
							dRV = GetRVAndSubCode(p_objFeeSchedule, p_objRequest, "00", sRvsID, sGeoZip, ref sSubCode);
							if(dRV >= 0)
							{
								dTemp = GetRVAndSubCode(p_objFeeSchedule,p_objRequest, "03", sRvsID, sGeoZip, ref sSubCode);
								if(dTemp >= 0)
									dRV = dRV - dTemp;
								else
									dRV = dTemp;
							}
							break;
						default:
							m_objCommon.AddAlert(Globalization.GetString("CalculateFunctions.CalcUCR.Default", m_iClientId));
							dRV = GetRVAndSubCode(p_objFeeSchedule,p_objRequest,"00", sRvsID, sGeoZip, ref sSubCode);
							break;
					}//end switch
				}//end if (p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList.Count > 0)
				else
					dRV = GetRVAndSubCode(p_objFeeSchedule,p_objRequest,"00", sRvsID, sGeoZip, ref sSubCode);
				if(dRV != -1)
				{
					m_objCommon.HandleSubCode(p_objRequest,sSubCode);
				
					//Apply units billed
					dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
                    //abisht MITS 11156
                    string sTempTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);
                    //if (int.Parse(m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode)) == BillItem.ANESTHESIA_SHORT_CODE &&
                    if (sTempTos.Trim() == BillItem.ANESTHESIA_SHORT_CODE.ToString() &&
						p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType == 
						(int)Common.UnitsBilledType.ubtMINUTES)
					{
						dRV = dRV * (dUnitsBilled / 15);
					}
					else
						dRV = dRV * dUnitsBilled;

					//Apply Modifier
					if(iModCodeID != 0)
						dReturnValue = m_objCommon.GetModifierValue(p_objFeeSchedule.FeeScheduleId,iModCodeID);

					dReturnValue = 1 * dRV;

				}//end if(dRV != -1)
			}//end try
			catch (RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CalculateFunctions.CalcUCR.GenericErr", m_iClientId) ,p_objException);
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}

				objDbReader = null;
				sbSQL = null;
			}

			//Return value to the calling function
			return dReturnValue;

		}//end function

		#endregion

		#endregion

		#region Private Methods

		#region private GetPhyRelVal(p_sMod)
		/// Name		: GetPhyRelVal
		/// Author		: Navneet Sota
		/// Date Created: 1/5/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// This function is used for retreiving physical relative value
		/// </summary>
		/// <param name="p_sMod">Module</param>
		private int GetPhyRelVal(string p_sMod)
		{
			int iReturnValue = -1;

			switch (p_sMod.ToUpper())
			{
				case "P1":
				case "P2":
				case "P6":
					iReturnValue = 0;
					break;
				case "P3":
					iReturnValue = 1;
					break;
				case "P4":
					iReturnValue = 2;
					break;
				case "P5":
					iReturnValue = 3;
					break;
			}
			return iReturnValue;
		}//End GetPhyRelVal

		#endregion

		#region private GenerateICCPTList (p_objFeeSchedule, p_objRequest)
		/// Name			: GenerateICCPTList
		/// Author			: Navneet Sota
		/// Date Created	: 01/05/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Called from Manager to get the list of possibilities for the GUI 
		/// when an ICCPT code has been requested by the Business Logic
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Object</param>
		private Hashtable GenerateICCPTList(FeeSchedule p_objFeeSchedule, Request p_objRequest)
		{
			//string variables
			string sSQL = "";
			string sInParam = "";

			//objects
			Hashtable hstReturnValue = null;
			DbReader objDbReader = null;

			try
			{
				sSQL = "SELECT ANE,ANESUB FROM ANECRX";
				sSQL = sSQL + " WHERE CPT = '" + p_objRequest.CPT + "'" ;

				if(m_objCommon.GetDbReader(ref objDbReader,sSQL))
				{
					//for(iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
					while(objDbReader.Read())
					{
						sInParam = sInParam + Conversion.ConvertObjToStr(objDbReader.GetValue("ANE"));
					}
				}

				objDbReader = null;

				sSQL = "SELECT CPT,DESC FROM ANECPT ";
				sSQL += " WHERE CPT IN(" + sInParam + ")"; 
				
				if(m_objCommon.GetDbReader(ref objDbReader,sSQL))
				{
					//for(iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
					while(objDbReader.Read())
					{
						hstReturnValue.Add(objDbReader.GetValue("ANE"),
							objDbReader.GetValue("ANE").ToString());
					}
				}
			}//end try
			catch (RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CalculateFunctions.GenerateICCPTList.GenericErr", m_iClientId) ,p_objException);
			}

			//Return to the calling function
			return hstReturnValue;
		}
		#endregion

		#region private HandleSubCodeExtended(p_objRequest, p_objRelativeValues)
		/// Name		: HandleSubCodeExtended
		/// Author		: Navneet Sota
		/// Date Created: 1/5/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Function used for setting the relevannt message
		/// </summary>
		///<param name="p_objRelativeValues">Relative Value Object</param>
		///<param name="p_objRequest">Request Class Object</param>
		private void HandleSubCodeExtended(Request p_objRequest, RelativeValues p_objRelativeValues)
		{
			//string variables
			string sMsg = "";			
			string sTemp = "";
			
			if (p_objRelativeValues.RelativeValueObject.m_sByReport != "")
			{
				sTemp = p_objRelativeValues.RelativeValueObject.m_sByReport.ToUpper();
				switch (sTemp)
				{
					case "BR":
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.ByReportBR", m_iClientId);
						break;
					case "NA":
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.ByReportNA", m_iClientId);
						break;
					default:
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.ByReportDefault", m_iClientId);
						break;
				}//end switch
				
				m_objCommon.AddAlert(sMsg);
			}//end if (p_objRelativeValues.RelativeValueObject.m_sByReport != "")

			if(p_objRelativeValues.RelativeValueObject.m_sStarred != "")
			{
				sTemp = p_objRelativeValues.RelativeValueObject.m_sStarred.ToUpper();
				switch (sTemp)
				{
					case "*":
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.Starred", m_iClientId);
						break;
					default:
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.StarredDefault", m_iClientId);
						break;
				}//end switch
				m_objCommon.AddAlert(sMsg);
			}//end if(p_objRelativeValues.RelativeValueObject.m_sStarred != "")

			if(p_objRelativeValues.RelativeValueObject.m_sQuestion != "")
			{
				sTemp = p_objRelativeValues.RelativeValueObject.m_sQuestion.ToUpper();
				switch (sTemp)
				{
					case "*":
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.Question", m_iClientId);
						break;
					default:
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.QuestionDefault", m_iClientId);
						break;
				}//end switch
				m_objCommon.AddAlert(sMsg);
			}//end if(p_objRelativeValues.RelativeValueObject.m_sQuestion != "")

			if(p_objRelativeValues.RelativeValueObject.m_sAssistSurg != "")
			{
				sTemp = p_objRelativeValues.RelativeValueObject.m_sAssistSurg.ToUpper();
				switch (sTemp)
				{
					case "Y":
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.AssistSurgY",m_iClientId);
						break;
					case "N":
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.AssistSurgN",m_iClientId);
						break;
					case "R":
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.AssistSurgR",m_iClientId);
						break;
					default:
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.AssistSurgDefault",m_iClientId);
						break;
				}//end switch
				m_objCommon.AddAlert(sMsg);
			}//end if(p_objRelativeValues.RelativeValueObject.m_sAssistSurg != "")

			if(p_objRelativeValues.RelativeValueObject.m_sAnesNa != "")
			{
				sTemp = p_objRelativeValues.RelativeValueObject.m_sAnesNa.ToUpper();
				switch (sTemp)
				{
					case "NC":
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.AnesNC",m_iClientId);
						break;
					default:
						sMsg = Globalization.GetString("CalculateFunctions.HandleSubCodeExtended.AnesDefault",m_iClientId);
						break;
				}//end switch
				m_objCommon.AddAlert(sMsg);
			}//end if(p_objRelativeValues.RelativeValueObject.m_sAnesNa != "")
		}//End HandleSubCodeExtended

		#endregion

		#region IsAmountNegative(p_sFieldName, p_sTableName, p_sCriteria)
		/// Name			: IsAmountNegative
		/// Author			: Navneet Sota
		/// Date Created	: 1/6/2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Checks if the passed in values are negative
		/// </summary>
		/// <param name="p_dWCPF"></param>
		/// <param name="p_dWCTECH"></param>
		/// <param name="p_dWCTF"></param>
		/// <param name="p_sMod"></param>
		private bool IsAmountNegative(double p_dWCPF,double p_dWCTF,double p_dWCTECH, string p_sMod)
		{
			//Return Variable
			bool bReturnValue = false;

			switch(p_sMod.Substring(2))
			{
				case "26":
					if(p_dWCPF < 0)
						bReturnValue = true;
					break;
				case "27":
					if(p_dWCTECH < 0)
						bReturnValue = true;
					break;
				default:
					if(p_dWCTF < 0)
						bReturnValue = true;
					break;
			}
			//Return the result to calling function
			return bReturnValue;
		}	
		#endregion

		#region private RelativeValues GetMatchingRLV(DataSet objDataSet,Request p_objRequest, string p_stateCode)
		/// Name			: GetMatchingRLV
		/// Author			: Rahul Sharma
		/// Date Created	: 02/15/2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 
		/// ************************************************************
		/// <summary>		
		/// Checks if at least one RLV exists.
		/// </summary>
		/// <param name="objDataSet">DataSet</param>
		/// <param name="p_objRequest">Request Object</param>
		/// <param name="p_stateCode">State Code</param>
		private RelativeValues GetMatchingRLV(DataSet objDataSet,Request p_objRequest, 
			string p_stateCode)
		{
			RelativeValues objFinalValues = null;
			RelativeValues objRelativeValues = null;
			RelativeValues objRelativeValues_NoAnesthesia = null;
			string sPOS = string.Empty;
			string sTOS =  string.Empty;
			string sTempPOS = string.Empty;
			string sTempTOS = string.Empty;
			int iLocMatch = 0;
			int iFinalMatch = 0;
			bool bPOSMatch = false;
			bool bTOSMatch = false;

			try
			{
				sPOS = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.POS);
				sTOS = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.TOS);
				for(int i=0;i<objDataSet.Tables[0].Rows.Count;i++)
				{
					if(sTOS == "7" && p_stateCode == "MD")
						GetRLV(objDataSet, i, ref objRelativeValues);
					else
					{
						sTempTOS = objDataSet.Tables[0].Rows[i]["TOS"].ToString();
						sTempPOS = objDataSet.Tables[0].Rows[i]["POS"].ToString();
						
						if(sTempPOS == sPOS)
						{
							iLocMatch = iLocMatch + 1;
							bPOSMatch = true;
						}
						else
							if(sTempPOS == "")
								bPOSMatch = true;
						if(sTempTOS == sTOS)
						{
							iLocMatch = iLocMatch + 1;
							bTOSMatch = true;
						}
						else
							if(sTempTOS == "")
							bTOSMatch = true;
						if(bTOSMatch && bPOSMatch && (iLocMatch >= iFinalMatch))
							GetRLV(objDataSet, i, ref objRelativeValues);
						else 
							if(sTOS != "7" && sTempTOS != "7")
								GetRLV(objDataSet, i, ref objRelativeValues_NoAnesthesia);

					}
				}
				if(sTOS == "" && objRelativeValues_NoAnesthesia != null)
				{
					objFinalValues = objRelativeValues_NoAnesthesia;
					return objFinalValues;
				}
				
				if(objRelativeValues !=null)
				{
					objFinalValues = objRelativeValues;
					return objFinalValues;
				}
				return objFinalValues;
			}
			catch (RMAppException p_objAppException)
			{
				throw p_objAppException;
			}
			finally
			{
				objFinalValues = null;
				objRelativeValues = null;
				objRelativeValues_NoAnesthesia = null;
			}
		}
		private void GetRLV(DataSet objDataSet,int iRow, ref RelativeValues objRelativeValues)
		{
			objRelativeValues = new RelativeValues(m_iClientId);
			objRelativeValues.RelativeValueObject.m_sTOS=
				objDataSet.Tables[0].Rows[iRow]["TOS"].ToString();
			objRelativeValues.RelativeValueObject.m_sPOS=
				objDataSet.Tables[0].Rows[iRow]["POS"].ToString();
			objRelativeValues.RelativeValueObject.m_sCPT = 
				objDataSet.Tables[0].Rows[iRow]["CPT_DESC"].ToString();
			objRelativeValues.RelativeValueObject.m_sByReport = 
				objDataSet.Tables[0].Rows[iRow]["BYREPORT"].ToString();
			objRelativeValues.RelativeValueObject.m_sStarred = 
				objDataSet.Tables[0].Rows[iRow]["STARRED"].ToString();
			objRelativeValues.RelativeValueObject.m_sQuestion = 
				objDataSet.Tables[0].Rows[iRow]["QUESTION"].ToString();
			objRelativeValues.RelativeValueObject.m_sAssistSurg = 
				objDataSet.Tables[0].Rows[iRow]["ASSISTSURG"].ToString();
			objRelativeValues.RelativeValueObject.m_sAnesBr = 
				objDataSet.Tables[0].Rows[iRow]["ANES_BR"].ToString();
			objRelativeValues.RelativeValueObject.m_sAnesNa = 
				objDataSet.Tables[0].Rows[iRow]["ANES_NA"].ToString();
			objRelativeValues.RelativeValueObject.m_dAmount = 
				Conversion.ConvertStrToDouble(objDataSet.Tables[0].Rows[iRow]["AMOUNT"].ToString());
			objRelativeValues.RelativeValueObject.m_dProfComponent = 
				Conversion.ConvertStrToDouble(objDataSet.Tables[0].Rows[iRow]["PROF_COMPONENT"].ToString());
			objRelativeValues.RelativeValueObject.m_dAnesthesiaVu = 
				Conversion.ConvertStrToDouble(objDataSet.Tables[0].Rows[iRow]["ANESTHESIA_VU"].ToString());
			objRelativeValues.RelativeValueObject.m_iFollup = 
				Conversion.ConvertStrToInteger(objDataSet.Tables[0].Rows[iRow]["FOLLUP"].ToString());
		}
		#endregion

		#region private GetMatchingRLV(p_objFeeSchedule,p_objRequest, p_sMod, p_sSubCode)
		/// Name			: GetMatchingRLV
		/// Author			: Navneet Sota
		/// Date Created	: 1/6/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Checks if at least one RLV exists.
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Object</param>
		/// <param name="p_sMod">Mod Value</param>
		/// <param name="p_sSubCode">Sub Code</param>
		private double GetMatchingRLV(FeeSchedule p_objFeeSchedule,Request p_objRequest, 
			string p_sMod, ref string p_sSubCode)
		{
			string sTempSubCode = "";
			string sTempMOD = "";
			string sFieldName = "";
			
			double dReturnValue = 0.0;
			double dTempRlv = 0.0;
			
			int iLocMatch = 0;
			int iFinalMatch = 0;

			bool bModMatch = false;
			bool bAtleastOneFound = false;

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			try
			{
                //abisht MITS 11156
                //if(int.Parse(m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode)) == BillItem.ANESTHESIA_SHORT_CODE)
                string sTempTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);
                if (sTempTos.Trim() == BillItem.ANESTHESIA_SHORT_CODE.ToString())
					sFieldName ="ANES_BASE_VALUE";
				else
					sFieldName = "RVUNIT";

				sbSQL = new StringBuilder();

				sbSQL.Append(" SELECT MOD_SURG_FLAG,RVUNIT,VALUE_FLAG,ANES_BASE_VALUE ");
				sbSQL.Append(" FROM RVP WHERE CPT = '" + p_objRequest.CPT + "'");
				
				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					while(objDbReader.Read())
					{
						iLocMatch = 0;
						bModMatch = false;
						sTempMOD = objDbReader.GetValue("MOD_SURG_FLAG").ToString();	
						if(p_sMod == "" || p_sMod != sTempMOD)
						{
							if(sTempMOD == "" || sTempMOD == "*" || sTempMOD == "0")
								bModMatch = true;
						}
						if(p_sMod == sTempMOD)
						{
							iLocMatch = iLocMatch + 1;
							bModMatch = true;
						}
						if(bModMatch && (iLocMatch >= iFinalMatch || (iLocMatch == 0 && iFinalMatch == 0)))
						{
							dTempRlv = Conversion.ConvertStrToDouble(objDbReader.GetValue(sFieldName).ToString());
							sTempSubCode = objDbReader.GetValue("VALUE_FLAG").ToString();	
							iFinalMatch = iLocMatch;
							bAtleastOneFound = true;							
						}//end if(bModMatch && (iLocMatch >= iFinalMatch || (iLocMatch == 0 && iFinalMatch == 0)))
					}
				}//end if(objDbReader != null)

				if (bAtleastOneFound)
				{
					p_sSubCode = sTempSubCode;
					dReturnValue = dTempRlv;
				}//end if (bAtleastOneFound)
				else
				{
					p_sSubCode = "";
					dReturnValue = -1;
				}
			}//end try
			catch (RMAppException p_objAppException)
			{
				throw p_objAppException;
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}

				objDbReader = null;
				sbSQL = null;
			}
			//Return the value to calling function
			return dReturnValue;
		}//end function
		
		#endregion

		#region private GetMatchingFactor(p_objFeeSchedule,p_objRequest, p_sMod, p_sTOS)
		/// Name		: GetMatchingFactor
		/// Author		: Navneet Sota
		/// Date Created: 1/6/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Get matching factor
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Object</param>
		/// <param name="p_sMod">Mod</param>
		/// <param name="p_sTOS">TOS</param>
		private double GetMatchingFactor(FeeSchedule p_objFeeSchedule,Request p_objRequest, 
			string p_sMod, string p_sTOS)
		{
			//string variable
			string sTempTOS = "";
			string sTempMOD = "";
			
			//double variables
			double dReturnValue = 1;
			double dTempFactor = 0.0;
			
			//int variables
			int iLocMatch = 0;
			int iFinalMatch = 0;

			//bool variables
			bool bModMatch = false;
			bool bTOSMatch = false;
			bool bAtleastOneFound = false;

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			try
			{
				sbSQL = new StringBuilder();

				sbSQL.Append(" SELECT FACTOR,TOS,MODIFIER FROM CONV_FACTOR ");
				sbSQL.Append(" WHERE '" + p_objRequest.CPT + "' BETWEEN BEGCPT AND ENDCPT ");
				
				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					while(objDbReader.Read())
					{
						iLocMatch = 0;
						bModMatch = false;
						bTOSMatch = false;

						sTempTOS = objDbReader.GetValue("TOS").ToString();	
						sTempMOD = objDbReader.GetValue("MODIFIER").ToString();	

						if(p_sTOS == "" || sTempTOS == "")
							bTOSMatch = true;
						if(p_sTOS == sTempTOS)
						{
							iLocMatch = iLocMatch + 1;
							bTOSMatch = true;
						}

						if(p_sMod == "" || sTempMOD == "")
							bModMatch = true;
						if(p_sMod == sTempMOD)
						{
							iLocMatch = iLocMatch + 1;
							bModMatch = true;
						}

						if(bModMatch && bTOSMatch && 
							(iLocMatch >= iFinalMatch || (iLocMatch == 0 && iFinalMatch == 0))) 
						{
							dTempFactor = Conversion.ConvertStrToDouble(objDbReader.GetValue("FACTOR").ToString());
							iFinalMatch = iLocMatch;
							bAtleastOneFound = true;							
						}
					}
				}//end if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)

				if (bAtleastOneFound)
					dReturnValue = dTempFactor;
			}//end try
			catch (RMAppException p_objAppException)
			{
				throw p_objAppException;
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}

				objDbReader = null;
				sbSQL = null;
			}
			//Return the value to calling function
			return dReturnValue;
		}//end function

		#endregion

		#region GetRVAndSubCode(p_objFeeSchedule,p_objRequest,p_sSpec,p_sRVSID,p_sGEOZIP,ref p_sSubCode)
		/// Name			: GetRVAndSubCode
		/// Author			: Navneet Sota
		/// Date Created	: 1/6/2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the RV value and Sub-Code
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Object</param>
		/// <param name="p_sGEOZIP">Geographical Zip Code</param>
		/// <param name="p_sRVSID">RVS-Id</param>
		/// <param name="p_sSpec">Spec</param>
		/// <param name="p_sSubCode">Sub-Code</param>
		private double GetRVAndSubCode(FeeSchedule p_objFeeSchedule, Request p_objRequest, 
			string p_sSpec, string p_sRVSID, string p_sGEOZIP, ref string p_sSubCode)
		{
			//Return Variable
			double dReturnValue = -1;
			double dRV = 0.0;
			double dFactor = 0.0;
			
			string sRVSUB = "";

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;

			try
			{
				sbSQL = new StringBuilder();

				//Table[0]
				sbSQL.Append(" SELECT RV,RVSUB FROM UCRRVSCE WHERE PROC_ = '").Append(p_objRequest.CPT).Append("'");
				sbSQL.Append(" AND RVSID = '").Append(p_sRVSID).Append("'");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						dRV =Conversion.ConvertStrToDouble(objDbReader.GetValue("RV").ToString());
						sRVSUB = objDbReader.GetValue("RVSUB").ToString();
					}
					else
					{
						m_objCommon.AddAlert("No record in UCRRVS where PROC_=" + p_objRequest.CPT + " and RVSID=" + p_sRVSID);
						//exit function
						return dReturnValue;
					}
				}
				

				objDbReader = null;
				sbSQL.Remove(0,sbSQL.Length);
				//Table[0]
				//sbSQL.Append(" \r\n ");
				sbSQL.Append(" SELECT PER").Append(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.Percentile.Substring(0,2));
                // MITS 13547 : Umesh
                //sbSQL.Append("  PERCENTILE FROM UCRFCT ");
                sbSQL.Append(" FROM UCRFCT ");
				sbSQL.Append(" WHERE GEOZIP = '").Append(p_sGEOZIP).Append("'");
				sbSQL.Append(" AND SPEC = '").Append(p_sSpec).Append("'");
				sbSQL.Append(" AND '" + p_objRequest.CPT + "' BETWEEN BEGPROC AND ENDPROC");
				sbSQL.Append(" ORDER BY GEOZIP,BEGPROC ");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
                        // MITS 13547 : Umesh
                        //dFactor = Conversion.ConvertStrToDouble(objDbReader.GetValue("PERCENTILE").ToString());
						dFactor = Conversion.ConvertStrToDouble(objDbReader.GetValue("PER"+p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.Percentile.Substring(0,2)).ToString());
					}
					else
					{
						m_objCommon.AddAlert("No record in UCRFCT where " + p_objRequest.CPT + " Between BegProc And EndProc and GeoZip=" + p_sGEOZIP + " and Spec=" + p_sSpec);
						//exit function
						return dReturnValue;
					}
				}
				

				p_sSubCode = sRVSUB;
				dReturnValue = (dRV / 100) * (dFactor / 1000);
			}
			catch (RMAppException p_objAppException)
			{
				throw p_objAppException;
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}

				objDbReader = null;
				sbSQL = null;
			}
			//Return the result to calling function
			return dReturnValue;
		}//End Function
		#endregion

		#endregion

		#region Destructor
		~CalculateFunctions()
		{
			m_objCommon = null;
            Dispose();
		}
		# endregion

        public void Dispose()
        {
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
            }           
        }
      
        #region internal CalcOWCP(p_objFeeSchedule, p_objRequest)
        /// Name		: CalcOWCP
        /// Author		: Geeta Sharma
        /// Date Created: 09/09/09
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Calaculte for OWCP
        /// </summary>
        /// <param name="p_objFeeSchedule">Fee Schedule Object</param>
        /// <param name="p_objRequest">Request Object</param>
        internal double CalcOWCP(FeeSchedule p_objFeeSchedule, Request p_objRequest)
        {                      
           StringBuilder sbSQL = null;
           DbReader objDbReader = null;

           double dMaxSchdAmount = 0.0;
           double dUnitsBilled = 0.0;  
           double dFactor = 0.0;
           double dModValue = 0.0;
           double dTmpUnits = 0.0;
           double dWrvu = 0.0;
           double dWgpci = 0.0;
           double dPErvu = 0.0;
           double dPEgpci = 0.0;
           double dMPrvu = 0.0;
           double dMPgpci = 0.0;
           double dCF = 0.0;
           double dReturnValue = -1;
           double dTempRlv = 0.0;

           string sModLevel = "";          
           string sTempMOD = "";           
           string sError = "";
           string sMod = "";
           string sTOS = "";
           string sTemp = "";
           string sPOS = "";     

           double dMult = 0.0;
           int iLocMatch = 0;
           int iFinalMatch = 0;
           int iModCodeID = 0;

           bool bModMatch = false;
           bool bAtleastOneFound = false;               

            try
            {            
                sbSQL = new StringBuilder();
                dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;

                if (p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList.Count > 0)
                {
                    iModCodeID = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierCode;
                    if (m_objLocalCache == null)
                        m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString,m_iClientId);
                    m_objLocalCache.GetCodeInfo(iModCodeID, ref sMod, ref sTemp);
                }                
                 
                 sPOS = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.POS);                
                 sTOS = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.TOS);  
                
                dModValue = 1;
                dMaxSchdAmount = -1;

                // If payment grp table not found then use the RVS tables 
                //  to compute Scheduled Amount
                if (sMod == "B") 
                {                     
                    dMaxSchdAmount = p_objFeeSchedule.CalcASCAmount(p_objRequest);
                }              

                // Get RTV and related values from db
                // Only retrieves relative values that are in Active(C) status
                // Status are: C = Active; D = non-covered service; S = Suspend for Review

                sbSQL.Append(" SELECT MODIFIER,MOD_LEVEL,WORK_RVU,NF_PE_RVU,PE_RVU,MPE_RVU,CF ");
				sbSQL.Append(" FROM CODE_RVU_CF ");
				sbSQL.Append(" WHERE CPT = '").Append(p_objRequest.CPT).Append("'");
                sbSQL.Append(" AND STATUS = 'C'");

                if (m_objCommon.GetDbReader(ref objDbReader, sbSQL.ToString()))
                {                  
                    while(objDbReader.Read())
                    {
                        iLocMatch = 0;
                        bModMatch = false;
                        sTempMOD = Conversion.ConvertObjToStr(objDbReader.GetValue("MODIFIER"));

                        if (sMod.Trim() == "")
                        {
                            if (sTempMOD == "" || sTempMOD == "*" || sTempMOD == "0")
                            {
                                bModMatch = true;
                            }
                        }
                        else
                        {
                            if (sMod == sTempMOD)
                            {
                                iLocMatch = iLocMatch + 1;
                                bModMatch = true;
                            }
                            else
                            {
                                if (sTempMOD == "" || sTempMOD == "*" || sTempMOD == "0")
                                {
                                    bModMatch = true;
                                }
                            }
                        }

                        if (bModMatch && (iLocMatch >= iFinalMatch || (iLocMatch == 0 && iFinalMatch == 0)))
                        {
                            if ((sPOS == "0") || (sPOS == ""))
                            {
                                dTempRlv = Conversion.ConvertStrToDouble(objDbReader.GetValue("NF_PE_RVU").ToString());
                            }
                            else
                            {
                                dTempRlv = Conversion.ConvertStrToDouble(objDbReader.GetValue("PE_RVU").ToString());
                            }
                            
                            dWrvu = Conversion.ConvertStrToDouble(objDbReader.GetValue("WORK_RVU").ToString());
                            dMPrvu = Conversion.ConvertStrToDouble(objDbReader.GetValue("MPE_RVU").ToString());
                            dCF = Conversion.ConvertStrToDouble(objDbReader.GetValue("CF").ToString());
                            
                            if (sModLevel == "")
                            {
                                sModLevel = Conversion.ConvertObjToStr((objDbReader.GetValue("MOD_LEVEL")));
                            }
                            iFinalMatch = iLocMatch;
                            bAtleastOneFound = true;
                        }
                   
                        if (bAtleastOneFound)
                        {
                            dPErvu = dTempRlv;
                        }
                        else
                        {
                            dWrvu = 0;
                            dMPrvu = 0;
                            dCF = 0;
                            dPErvu = -1;
                        }
                    }
                }               
                
                objDbReader.Close();
                objDbReader = null;

                if ( dPErvu < 0 )
                {
                    sError = Globalization.GetString("Common.Constants.ErrCptModifier",m_iClientId);
                    m_objCommon.AddAlert(sError);
                    return dReturnValue;
                }
                //Get GPCI
                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append(" SELECT WORK_GPCI,PE_GPCI,ME_GPCI");
                sbSQL.Append(" FROM GPCI_BY_ZIP ");
                sbSQL.Append(" WHERE ZIP= '").Append(p_objRequest.BillItemObject.ZipCode).Append("'");

                if (m_objCommon.GetDbReader(ref objDbReader, sbSQL.ToString()))
                {
                    if (objDbReader.Read())
                    {
                          dWgpci = Conversion.ConvertStrToDouble(objDbReader.GetValue("WORK_GPCI").ToString());
                          dPEgpci = Conversion.ConvertStrToDouble(objDbReader.GetValue("PE_GPCI").ToString());
                          dMPgpci = Conversion.ConvertStrToDouble(objDbReader.GetValue("ME_GPCI").ToString());
                    }
                    else
                    {
                        sError =  Globalization.GetString("Common.Constants.ErrZipCode",m_iClientId);
                        m_objCommon.AddAlert(sError);                                                      
                        objDbReader.Close();
                        objDbReader = null;
                        return dReturnValue;
                    }
                }
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader = null;
                }

               //Get Multiplier
                if (sMod !="")
                {
                    sbSQL.Remove(0, sbSQL.Length);
                    sbSQL.Append(" SELECT MULTIPLIER");
                    sbSQL.Append(" FROM MODS ");
                    sbSQL.Append(" WHERE MOD_LEVEL= '").Append(sModLevel).Append("'");
                    sbSQL.Append(" AND MODIFIER= '").Append(sMod).Append("'");                                    
                    
                    if (m_objCommon.GetDbReader(ref objDbReader, sbSQL.ToString()))
                    {
                       while(objDbReader.Read())
                       {
                           dMult = Conversion.ConvertObjToDouble(objDbReader.GetValue("MULTIPLIER"), m_iClientId);
                            dModValue = dMult / 100;
                       }
                       objDbReader.Close();
                       objDbReader = null;
                    }                    
                }
                 // Calculate maximum allowable amount according to retrieved amounts from OWCP fee schedules
                dReturnValue = ((dWrvu * dWgpci) + (dPErvu * dPEgpci) + (dMPrvu * dMPgpci)) * dCF;    
                if(dReturnValue == 0)
                {
                     sError = "Relative value not established. Cannot calculate the Scheduled Amount.";
                     m_objCommon.AddAlert(sError);
                     return dReturnValue;
                }
                 // Multiply amount by Units
                dTmpUnits = dUnitsBilled;
                if (sTOS  == "7") 
                {
                 if(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType == (int)Common.UnitsBilledType.ubtMINUTES)
                 {
                    //Get state specific units
                     dTmpUnits = dUnitsBilled / 15;                    
                 }            
                    dReturnValue = dReturnValue + dTmpUnits * dFactor;
                }
                else
                {
                    dReturnValue = dReturnValue * dTmpUnits;
                }

                // Apply modifier, if specified
                if (sMod != "") 
                {
                    dReturnValue = dReturnValue * dModValue;
                }   
                // Limit Scheduled Amount by max specified for the group
                if (dMaxSchdAmount >= 0 && dReturnValue > dMaxSchdAmount)
                {
                    dReturnValue = dMaxSchdAmount;
                }                    

            } //End try
            catch (RMAppException p_objRmAEx)
            {
                throw p_objRmAEx;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CalculateFunctions.CalcOWCP.GenericErr",m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }

                objDbReader = null;
                sbSQL = null;
            }

            //Return value to the calling function
            return dReturnValue;

        }//end function       

   
        #endregion









	}//End Class
}//End Namespace
