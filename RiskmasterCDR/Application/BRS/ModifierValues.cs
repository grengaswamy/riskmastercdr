using System;
using System.Collections;

using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using Riskmaster.Common; 

using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.Application.BRS
{
	/**************************************************************
	 * $File		: ModifierValues.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/03/2004
	 * $Author		: Navneet Sota
	 * $Comment		: This class contains a collection of ModifierValue objects
	 * $Source		: Riskmaster.Application.BRS
	**************************************************************/
	internal class ModifierValues:CollectionBase,IDisposable
	{

		#region Variable Declarations	

		/// <summary>
		/// Represents the Module variable for Common Class.
		/// </summary>
		private Common m_objCommon = null;

		/// <summary>
		/// Represents the Module variable for Common.LocalCache Class.
		/// </summary>
		private LocalCache m_objLocalCache = null;
        private int m_iClient = 0;
		/// <summary>
		/// Tanuj->Should contain errors/alerts for BRS module, will be set by BRS adaptor and errors/alerts will be displayed onto UI. 
		/// </summary>
		private BusinessAdaptorErrors m_objErrors;

		public BusinessAdaptorErrors Errors
		{
			get
			{
				return m_objErrors;
			}
			set
			{
				m_objErrors=value;
			}
		}
		# endregion

		#region Constructor
		/// Name		: ModifierValues
		/// Author		: Navneet Sota
		/// Date Created: 01/03/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
		/// <param name="p_objCommon">Common Class object instantiated by Manager </param>
        internal ModifierValues(Common p_objCommon,int p_iClient = 0)
		{
            m_iClient = p_iClient;
			m_objCommon = p_objCommon;
            m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString, m_iClient);
		}
		
		#endregion

		#region ModifierValue class
		/// <summary>
		/// class containing ModifierValue.
		/// </summary>
		internal class ModifierValue
		{
			internal int m_iModCodeID;

			internal string m_sModCode;
			internal string m_sModDesc;
			internal string m_sModDescStr;
		}
		#endregion

//
//		internal int ModCodeID
//		{
//			set
//			{
//				m_iModCodeID = value;
//			}
//			get
//			{
//				return m_iModCodeID;
//			}	
//		}

		#region Internal Methods

		#region Add(p_iModCodeID, p_sModDescStr)
		/// Name			: Add
		/// Author			: Navneet Sota
		/// Date Created	: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Adds Code Object to the collection
		/// </summary>
		/// <param name="p_iModCodeID">Modifier Code Id</param>
		/// <param name="p_sModDescStr">Modifier Code Description</param>
		internal void Add(int p_iModCodeID, string p_sModDescStr)
		{
			//objects
			ModifierValue objModValue = null;
            try
            {
                if (m_objLocalCache == null)
                    m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString,m_iClient);

                objModValue = new ModifierValue();

                objModValue.m_iModCodeID = p_iModCodeID;
                objModValue.m_sModDescStr = p_sModDescStr;
                objModValue.m_sModCode = "";
                objModValue.m_sModDesc = "";

                m_objLocalCache.GetCodeInfo(p_iModCodeID, ref objModValue.m_sModCode, ref objModValue.m_sModDesc);

                this.List.Add(objModValue);
            }
            catch (RMAppException p_objRmAEx)
            {
                throw p_objRmAEx;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ModifierValues.Add.DataErr", m_iClient), p_objException);
            }
            finally
            {
                objModValue = null;
            }
		}
		#endregion

		#region "Return ModValue object"

		/// Name		: this
		/// Author		: Navneet Sota
		/// Date Created: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the ModifierValue object for that index
		/// </summary>
		internal ModifierValue this[int p_iIndex]
		{        
			get
			{
				return (ModifierValue)this.List[p_iIndex];
			}        
		}

		#endregion

		#region "Return Number of items in Collection"
		/// Name		: NumberofObjects
		/// Author		: Navneet Sota
		/// Date Created: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the number of ModValue objects in collection
		/// </summary>
		internal int NumberofObjects
		{        
			get
			{
				return this.List.Count;
			}        
		}

		#endregion

		#region "Remove ModValue object"
		/// Name		: RemoveObject
		/// Author		: Navneet Sota
		/// Date Created: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Remove ModValue obect from collection
		/// </summary>
		internal void RemoveObject(int p_iIndex)
		{        
			this.List.RemoveAt(p_iIndex);
		}

		#endregion

		#endregion

		#region Destructor
		~ModifierValues()
		{
            Dispose();
		}
		# endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (m_objLocalCache != null)
                m_objLocalCache.Dispose();
            m_objCommon = null;
            m_objLocalCache = null;       
        }

        #endregion

	}//End Class
}//End Namespace
