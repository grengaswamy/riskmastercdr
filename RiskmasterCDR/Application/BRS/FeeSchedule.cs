﻿
using System;
using System.Text;
using System.Data;
using System.Xml;
using System.Collections;

using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.Application.BRS
{

	#region FeeScheduleType Enumeration
	/// <summary>
	/// Enum containing Fee Schedule Types.
	/// </summary>
	internal enum FeeScheduleType:int
	{
		UCR94 = 0,		 // ingenix/Medicode Table Type (CALC_TYPE)
		UCR95 = 1,   
		WC99 = 2,
		WC00 = 13,       // "Workers' Compensation 2000" ingenix/Medicode Table Type
		OPT = 3,
		HCPCS = 4,
		ANES = 7,
        DENT = 18,       //caggarwal4 Merged for issue RMA-9115
		OHIO96 = 9,      // Caredata/Medirisk Table Type (CALC_TYPE)
		OHIO97 = 10,     // 98,99 Caredata/Medirisk
		STANTH = 11,   
		BASIC = 12,      // "Basic User Entered Schedule"
		EXTENDED = 14,   // "Extended BRS Schedule"
		FREEPHARMA = 15, // "Free Entry Pharmacy" To allow Pharmacy Fee Entry without a Table.
		FREEHOSP = 16,   // "Free Entry Hospital" To allow Hospital Fee Entry without a Table. 6/2003 dcm
		FREEGENERIC = 17, //"Generic Free Entry" To allow Fee Entry without a Table. 6/2003 dcm
        FLHOSP = 20, // Florida only hospital
        OWCP = 19 //Geeta MITS 17664
	};

	#endregion

	/**************************************************************
	 * $File		: FeeSchedule.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 12/14/2004
	 * $Author		: Navneet Sota
	 * $Comment		: This class signifies the FeeSchedule object 
	 * $Source		: Riskmaster.Application 	
	**************************************************************/
	internal class FeeSchedule
	{
		#region Constant Declarations	

		//For INavigation
		internal byte MOVE_FIRST = 1;
		internal byte MOVE_LAST = 2;
		internal byte MOVE_NEXT = 3;
		internal byte MOVE_PREVIOUS = 4;

		# endregion

		#region Variable Declarations	

		/// <summary>
		/// Represents the Module variable for Common Class.
		/// </summary>
		private Common m_objCommon = null;

		/// <summary>
		/// Represents the FeeScheduleType enumeration
		/// </summary>
		private FeeScheduleType m_enumFeeScheduleType;

		/// <summary>
		/// Represents the Fee Schedule Id
		/// </summary>
		private int m_iFeeScheduleId = 0;

		/// <summary>
		/// Represents the State-Id
		/// </summary>
		private int m_iStateID = 0;

		/// <summary>
		/// Represents the UCR Percentile
		/// </summary>
		private int m_iUCRPercentile = 0;

		/// <summary>
		/// Represents the Default Percentile
		/// </summary>
		private int m_iDefaultPercentile;

		/// <summary>
		/// Represents the Name
		/// </summary>
		private string m_sName = "";
		/// <summary>
		/// Tanuj->Should contain errors/alerts for BRS module, will be set by BRS adaptor and errors/alerts will be displayed onto UI. 
		/// </summary>
		private BusinessAdaptorErrors m_objErrors;

		public BusinessAdaptorErrors Errors
		{
			get
			{
				return m_objErrors;
			}
			set
			{
				m_objErrors=value;
			}
		}

        private int m_iClientId = 0;//rkaur27
		# endregion

		#region Constructor
		/// Name		: FeeSchedule
		/// Author		: Navneet Sota
		/// Date Created: 01/04/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
		/// <param name="p_objCommon">Common Class object instantiated by Manager </param>
		internal FeeSchedule(Common p_objCommon, int p_iClientId)//rkaur27
		{
			m_objCommon = p_objCommon;
            m_iClientId = p_iClientId;//rkaur27
		}
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Internal property for State-ID
		/// </summary>
		internal int StateId
		{
			set
			{
				m_iStateID = value;
			}
			get
			{
				return m_iStateID;
			}	
		}

		/// <summary>
		/// Internal property for Fee Schedule Type
		/// </summary>
		internal FeeScheduleType ScheduleType
		{
			set
			{
				m_enumFeeScheduleType = value;
			}
			get
			{
				return m_enumFeeScheduleType;
			}	
		}

		/// <summary>
		/// Internal property for FeeSchedule-ID
		/// </summary>
		internal int FeeScheduleId
		{
			set
			{
				m_iFeeScheduleId = value;
			}
			get
			{
				return m_iFeeScheduleId;
			}	
		}

		/// <summary>
		/// Internal property for Requires External Data
		/// </summary>
		internal bool RequiresExternalData
		{
			get
			{
				bool bReturnValue = false;

				if(ScheduleType == FeeScheduleType.BASIC || ScheduleType == FeeScheduleType.EXTENDED || 
					ScheduleType == FeeScheduleType.FREEPHARMA || ScheduleType == FeeScheduleType.FREEGENERIC || 
					ScheduleType == FeeScheduleType.FREEHOSP)
					bReturnValue = false;
				else
					bReturnValue = true;


				return bReturnValue;
			}	
		}

		/// <summary>
		/// Internal property for Name
		/// </summary>
		internal string Name
		{
			set
			{
				m_sName = value;
			}
			get
			{
				return m_sName;
			}	
		}

		/// <summary>
		/// Internal property for UCRPercentile
		/// </summary>
		internal int UCRPercentile
		{
			set
			{
				m_iUCRPercentile = value;
			}
			get
			{
				return m_iUCRPercentile;
			}	
		}

		/// <summary>
		/// Internal property for DefaultPercentile
		/// </summary>
		internal int DefaultPercentile
		{
			set
			{
				m_iDefaultPercentile = value;
			}
			get
			{
				return m_iDefaultPercentile;
			}	
		}

		/// <summary>
		/// Internal property for CPTCodeColumnName
		/// </summary>
		internal string CPTCodeColumnName
		{
			get
			{
				string sReturnValue = "";

				switch (this.ScheduleType)
				{
					case FeeScheduleType.UCR94:
					case FeeScheduleType.UCR95:
					case FeeScheduleType.OPT:
					case FeeScheduleType.HCPCS:
					case FeeScheduleType.DENT:
						sReturnValue = "PROC_";
						break;
					case FeeScheduleType.WC00:
						sReturnValue = "PROC";
						break;
					case FeeScheduleType.WC99:
					case FeeScheduleType.ANES:
					case FeeScheduleType.OHIO97:
					case FeeScheduleType.STANTH:
					case FeeScheduleType.BASIC:
					case FeeScheduleType.EXTENDED:
                    case FeeScheduleType.OWCP: //Geeta MITS 17664
						sReturnValue = "CPT";
						break;
					case FeeScheduleType.OHIO96:
						sReturnValue = "BILLING_CODE";
						break;
					case FeeScheduleType.FREEPHARMA:
						sReturnValue = "NDC";
						break;
				}//end Switch

                if (string.Compare(sReturnValue, "PROC", true) == 0)
                    sReturnValue = "[" + sReturnValue + "]";

				return sReturnValue;
			}	
		}

		/// <summary>
		/// Internal property for CPT Description Column Name
		/// </summary>
		internal string CPTDescriptionColumnName
		{
			get
			{
				string sReturnValue = "";

				switch (this.ScheduleType)
				{
					//UCR94 UCR95 Workers'Comp Outpatient HCPCS Anesthesia Dental OHWC
					case FeeScheduleType.UCR94:
					case FeeScheduleType.UCR95:
					case FeeScheduleType.OPT:
					case FeeScheduleType.HCPCS:
					case FeeScheduleType.DENT:
					case FeeScheduleType.WC00:
					case FeeScheduleType.WC99:
					case FeeScheduleType.OHIO96:
                    case FeeScheduleType.OWCP:  //Geeta MITS 17664
						//DESC is a reserved word which can cause problems in SQL (descending).  
						//Access likes [DESC].
						sReturnValue = "[DESC]"; 
						break;
					case FeeScheduleType.OHIO97:
						sReturnValue = "DESC1";
						break;
					case FeeScheduleType.STANTH:
						sReturnValue = "DESCRIPTION";
						break;
					case FeeScheduleType.BASIC:
					case FeeScheduleType.EXTENDED:
						sReturnValue = "CPT_DESC";
						break;
				}//end Switch

				return sReturnValue;
			}	
		}

		/// <summary>
		/// Internal property for CPT Table Name
		/// </summary>
		internal string CPTTableName
		{
			get
			{
				string sReturnValue = "";

				switch (this.ScheduleType)
				{
					case FeeScheduleType.UCR94:
						sReturnValue = "UCRCPT"; 
						break;
					case FeeScheduleType.UCR95:
						sReturnValue = "UCRCPTCE";
						break;
					case FeeScheduleType.WC99: //Workers' Comp
					case FeeScheduleType.WC00:
						sReturnValue = "WRKUVA"; 
						break;
					case FeeScheduleType.OPT:  //Outpatient
						sReturnValue = "OPUCPTCE"; 
						break;
					case FeeScheduleType.HCPCS:
						sReturnValue = "HCPCPTCE"; 
						break;
					case FeeScheduleType.ANES:  //Anesthesia
						sReturnValue = "ANECPT"; 
						break;
					case FeeScheduleType.DENT:  //Dental
						sReturnValue = "DENADACE"; 
						break;
					case FeeScheduleType.OHIO96:  //OHWC
						sReturnValue = "BILL_CODE_DESC"; 
						break;
					case FeeScheduleType.OHIO97:
						sReturnValue = "PLUSDESC";
						break;
					case FeeScheduleType.STANTH:  //StAnthony 
						sReturnValue = "RVP";
						break;
					case FeeScheduleType.BASIC: //Basic Fee Schedule
					case FeeScheduleType.EXTENDED: //Extended Fee Schedule
						sReturnValue = "WRCMP_CPT"; 
						break;
                    case FeeScheduleType.OWCP:  //Geeta MITS 17664
                        sReturnValue = "CODE_RVU_CF";
                        break;
				}//end Switch

				return sReturnValue;
			}	
		}

		#endregion

		#region internal Methods

		#region internal GetBillingCodeDesc(p_sInput)
		/// Name			: GetBillingCodeDesc
		/// Author			: Navneet Sota
		/// Date Created	: 1/4/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Returns the Billing Code Description
		/// </summary>
		/// <param name="p_sInput"></param>
		internal string GetBillingCodeDesc(string p_sInput)
		{
			DbReader objReader=null;
			string sSql = string.Empty;
			string sReturnValue = string.Empty;
			string sTempSQL = string.Empty;
			
			try
			{
				//string variables
				
				sTempSQL = this.CPTCodeColumnName + " LIKE '" + p_sInput;
				//sTempSQL = this.CPTCodeColumnName + " = '" + p_sInput;
				if(this.RequiresExternalData)
				{
					sTempSQL = sTempSQL + "'";
					sSql="SELECT " + this.CPTDescriptionColumnName + " FROM " + this.CPTTableName+ " WHERE "+sTempSQL ;
					if(this.CPTDescriptionColumnName.Trim()=="" || this.CPTTableName.Trim()=="")
					{
						return sReturnValue;
					}
					if(m_objCommon.GetDbReader(ref objReader,sSql))
					if(objReader!=null)
					{
						if(objReader.Read())
						{
                            object oValue = objReader.GetValue(0);
                            if (oValue != System.DBNull.Value && oValue != null)
                            {
                                sReturnValue = oValue.ToString();
                            }
						}
					}

				}

				else
				{
					sTempSQL = sTempSQL + "'  AND TABLE_ID = " + this.FeeScheduleId;
					sReturnValue = m_objCommon.GetSingleString(this.CPTDescriptionColumnName,this.CPTTableName, sTempSQL); 
				}

			}
			catch(Exception p_objErr)
			{
				throw new RMAppException(p_objErr.Message, p_objErr);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
				}
								

			}
			return sReturnValue;
		}

		#endregion

        #region internal GetScheduledAmount(p_objRequest, p_iFLType)
		/// Name		: GetScheduledAmount
		/// Author		: Navneet Sota
		/// Date Created: 1/4/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// sole entry point to all calculations of amount allowed
		/// sole entry point to set global vars g_objCalculate,g_lStateId
		/// on error all other routines called by this function log error then re-raise the error back to here
		/// sole entry point to all the Calc_[...] bas modules.  
		/// these modules do amount allowed calculations using different Access databases
		/// </summary>
		/// <param name="p_objRequest">Request Object</param>
        /// <param name="p_iCount"></param>
        /// <param name="p_iFLType"></param>
        /// <param name="p_objHashtable"></param>
        internal double GetScheduledAmount(Request p_objRequest, int p_iFLType, int p_iCount, Hashtable p_objHashtable)
		{
			//Return Value
			double dReturnValue = -1;
			
			//string variables
			string sSQL = "";
			string sStudyId = "";
			string sRevision = "";
			string sStateCode = "";

			//objects
			CalculateFunctions objCalculateFunctions = null;
			CalculateWC objCalculateWC = null;
			DataSet objDataSet=null;
			try
			{
				objCalculateFunctions = new CalculateFunctions(m_objCommon, m_iClientId);
				switch (this.ScheduleType)
				{
					case FeeScheduleType.BASIC:
						sSQL = "SELECT AMOUNT FROM WRCMP_CPT WHERE TABLE_ID = ";
						sSQL += this.FeeScheduleId + " AND CPT = '" + p_objRequest.CPT + "' ";
						
						objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString,sSQL, m_iClientId);

						if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
							dReturnValue = Conversion.ConvertStrToDouble(objDataSet.Tables[0].Rows[0]["AMOUNT"].ToString());
						else
							m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrState", m_iClientId) + " CPT=" + p_objRequest.CPT + ", TABLE_ID=" + this.FeeScheduleId);    //no calc possible message

						if(dReturnValue > 0 )
							dReturnValue = dReturnValue * p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;

						break;
					case FeeScheduleType.EXTENDED:
						dReturnValue = objCalculateFunctions.CalcExtended(this, p_objRequest);
						break;
					case FeeScheduleType.FREEHOSP:
					case FeeScheduleType.FREEGENERIC:
					case FeeScheduleType.FREEPHARMA:
						//no calculation to do, return amount billed
						dReturnValue = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.AmountBilled;
						break;
					case FeeScheduleType.UCR94:
					case FeeScheduleType.UCR95:
						dReturnValue = objCalculateFunctions.CalcUCR(this, p_objRequest);
						break;
					case FeeScheduleType.WC00: //Workers' Comp. 2000
						objCalculateWC = new CalculateWC(m_objCommon, m_iClientId);
						dReturnValue = objCalculateWC.CalcWC2000(this, p_objRequest, ref sStudyId, ref sStateCode, ref sRevision);
						break;
					case FeeScheduleType.WC99: //Workers' Comp. 1999
						objCalculateWC = new CalculateWC(m_objCommon, m_iClientId);
						dReturnValue = objCalculateWC.CalcWorkComp(this, p_objRequest, ref sStudyId, ref sStateCode);
						break;
					case FeeScheduleType.OPT:  //Outpatient
						dReturnValue = objCalculateFunctions.CalcOutpatient(this, p_objRequest);
						break;
					case FeeScheduleType.HCPCS:
						dReturnValue = objCalculateFunctions.CalcHCPCS(this, p_objRequest);
						break;
					case FeeScheduleType.ANES: //Anesthesia
						dReturnValue = objCalculateFunctions.CalcAnesthesia(this, p_objRequest);
						break;
					case FeeScheduleType.DENT: //Dental
						dReturnValue = objCalculateFunctions.CalcDental(this, p_objRequest);
						break;
					case FeeScheduleType.OHIO96: //Caredata/Medirisk
						dReturnValue = objCalculateFunctions.CalcOHWC(this, p_objRequest);
						break;
					case FeeScheduleType.OHIO97: //Caredata/Medirisk
						dReturnValue = objCalculateFunctions.CalcOHWC97(this, p_objRequest);
						break;
					case FeeScheduleType.STANTH: //St. Anthony
						dReturnValue = objCalculateFunctions.CalcStAnthony(this, p_objRequest);
						break;
                    case FeeScheduleType.FLHOSP://florida only hospital
                        dReturnValue = CalcFLHospital(p_objRequest, p_iFLType, p_iCount, p_objHashtable, ref sStudyId, ref sStateCode, ref sRevision);
                        break;
                    case FeeScheduleType.OWCP : //OWCP fee schedule
                        dReturnValue =  objCalculateFunctions.CalcOWCP(this,p_objRequest);
                        break;
					default:
						throw new RMAppException("Unknown calc type: " + this.ScheduleType); 
				}//end Switch 
			}//end try
			catch (Exception p_objAppException)
			{
				throw new RMAppException(p_objAppException.Message, p_objAppException);
			}
			finally
			{
				if (objDataSet != null)
					objDataSet.Dispose();

				objDataSet = null;
                if (objCalculateFunctions != null)
                    objCalculateFunctions.Dispose();

                if (objCalculateWC != null)
                    objCalculateWC.Dispose();
			}
			
			//Return the result to calling function
			return dReturnValue;
		}//End GetScheduledAmount
		#endregion

        internal double GetScheduledAmount(Request p_objRequest)
        {
            int iFlType = 0;
            int iCount = 0;
            Hashtable objHashtable = null;
            double dReturnValue = GetScheduledAmount(p_objRequest, iFlType, iCount, objHashtable);
            return dReturnValue;
        }

		#region internal GetMaxRLVAndModValue(p_iModCode, p_sCPT, ref p_dModValue,ref p_dMaxRlv)
		/// Name			: GetMaxRLVAndModValue
		/// Author			: Navneet Sota
		/// Date Created	: 1/4/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Gets the Max RLV and Mod Value.
		/// </summary>
		/// <param name="p_iModCode">Mod Code</param>
		/// <param name="p_sCPT">CPT</param>
		/// <param name="p_dModValue">Mod Value</param>
		/// <param name="p_dMaxRlv">Maximum RLV</param>
		internal void GetMaxRLVAndModValue(int p_iModCode, string p_sCPT, ref double p_dModValue,ref double p_dMaxRlv)
		{
			//Objects
			StringBuilder sbSQL = null;
			DataSet objDataSet=null;
			try
			{
				sbSQL = new StringBuilder();

				sbSQL.Append("SELECT MODIFIER_VALUE,MAX_RLV FROM BRS_MOD_VALUES ");
				sbSQL.Append(" WHERE FEETABLE_ID = " + this.FeeScheduleId );
				sbSQL.Append(" AND MODIFIER_CODE = " + p_iModCode) ;
				sbSQL.Append(" AND '" + p_sCPT + "' BETWEEN START_CPT AND END_CPT") ;
						
				objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString,sbSQL.ToString(), m_iClientId);

				if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
				{
//					if(p_dModValue>0 ||p_dModValue>0.00)
//					{
//						p_dModValue = (p_dModValue * Conversion.ConvertStrToDouble(objDataSet.Tables[0].Rows[0]["MODIFIER_VALUE"].ToString()) / 100);
//					}
//					else
//					{
                       p_dModValue = Conversion.ConvertStrToDouble(objDataSet.Tables[0].Rows[0]["MODIFIER_VALUE"].ToString()) / 100 ;
//					}
					p_dMaxRlv = Conversion.ConvertStrToDouble(objDataSet.Tables[0].Rows[0]["MAX_RLV"].ToString());
				}
			}//end try
			catch (Exception p_objAppException)
			{
				throw new RMAppException(p_objAppException.Message, p_objAppException);
			}
			finally
			{
				if (objDataSet != null)
					objDataSet.Dispose();

				objDataSet = null;
			}
		}//end function

		#endregion

		#region internal CalcASCAmount(p_objRequest)
		/// Name			: CalcASCAmount
		/// Author			: Navneet Sota
		/// Date Created	: 1/4/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Calculates the flat fee for procedures at ASC. The values are stored in
		/// tables located in the fee schedule Access database. They are created at the
		/// time of import.
		/// </summary>
		/// <param name="p_objRequest">Request object</param>
		internal double CalcASCAmount(Request p_objRequest)
		{
			//return value
			double dReturnValue = -1;

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			try
			{
				//Obtain flat fees from tables
				sbSQL = new StringBuilder();
				sbSQL.Append(" SELECT AMOUNT FROM GROUP_AMOUNT,GROUP_CPT ");
				sbSQL.Append(" WHERE GROUP_AMOUNT.GROUP_ID=GROUP_CPT.GROUP_ID AND GROUP_CPT.CPT = '" + p_objRequest.CPT + "'" );
				
				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
                        dReturnValue = Conversion.ConvertObjToDouble(objDbReader.GetValue("AMOUNT"), m_iClientId);
					}
				}
			}//end try
			catch (Exception p_objAppException)
			{
				throw new RMAppException(p_objAppException.Message, p_objAppException);
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}
				objDbReader = null;
				sbSQL = null;
			}
			//Return the vaue to the calling function
			return dReturnValue;
		}//end function

		#endregion

		#region internal MoveFirst()
		/// Name		: MoveFirst
		/// Author		: Navneet Sota
		/// Date Created: 1/4/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// </summary>
		internal void MoveFirst()
		{
			MoveAndLoad(MOVE_FIRST);
		}//end function

		#endregion

		#region internal MoveLast()
		/// Name		: MoveLast
		/// Author		: Navneet Sota
		/// Date Created: 1/4/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// </summary>
		internal void MoveLast()
		{
			MoveAndLoad(MOVE_LAST);

		}//end function

		#endregion

		#region internal MoveNext()
		/// Name		: MoveNext
		/// Author		: Navneet Sota
		/// Date Created: 1/4/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// </summary>
		internal void MoveNext()
		{
			MoveAndLoad(MOVE_NEXT);
		}//end function

		#endregion

		#region internal MovePrevious()
		/// Name		: MovePrevious
		/// Author		: Navneet Sota
		/// Date Created: 1/4/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// </summary>
		internal void MovePrevious()
		{
			MoveAndLoad(MOVE_PREVIOUS);
		}//end function

		#endregion

		#region internal MoveToRecord(p_iToID)
		/// Name		: MoveToRecord
		/// Author		: Navneet Sota
		/// Date Created: 1/4/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// </summary>
		/// <param name="p_iToID"></param>
		internal void MoveToRecord(int p_iToID)
		{
			this.FeeScheduleId = p_iToID;
			LoadData();
		}//end function

		#endregion

		#region internal MoveAndLoad(p_bytMoveDirection)
		/// Name		: MoveAndLoad
		/// Author		: Navneet Sota
		/// Date Created: 1/4/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// </summary>
		/// <param name="p_bytMoveDirection"></param>
		internal void MoveAndLoad(byte p_bytMoveDirection)
		{
			int iNewID = 0;
			StringBuilder sbSQL = null;
			//Database objects
			DataSet objDataSet=null;
			try
			{
				sbSQL = new StringBuilder();

				if(p_bytMoveDirection == MOVE_FIRST)
					sbSQL.Append("SELECT MIN(TABLE_ID)  ID FROM FEE_TABLES WHERE TABLE_ID > 0 ");
				else if(p_bytMoveDirection == MOVE_LAST)
					sbSQL.Append("SELECT MAX(TABLE_ID)  ID FROM FEE_TABLES ");
				else if(p_bytMoveDirection == MOVE_NEXT || 
					(p_bytMoveDirection == MOVE_PREVIOUS && m_iFeeScheduleId == 0))
					sbSQL.Append("SELECT MIN(TABLE_ID)  ID FROM FEE_TABLES WHERE TABLE_ID > " + m_iFeeScheduleId );
				else if(p_bytMoveDirection == MOVE_PREVIOUS)
					sbSQL.Append("SELECT MAX(TABLE_ID)  ID FROM FEE_TABLES WHERE TABLE_ID < " + m_iFeeScheduleId);
							
				objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString,sbSQL.ToString(), m_iClientId);

				if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
				{
					iNewID = Conversion.ConvertStrToInteger(objDataSet.Tables[0].Rows[0]["ID"].ToString());
					if(iNewID != 0 && iNewID != this.FeeScheduleId)
					{
						this.FeeScheduleId = iNewID;
						LoadData();
					}//end if						
				}//end if
			}//end try
			catch (Exception p_objAppException)
			{
				throw new RMAppException(p_objAppException.Message, p_objAppException);
			}
			finally
			{
				if (objDataSet != null)
					objDataSet.Dispose();

				objDataSet = null;
			}
		}//end function

		#endregion

	#endregion

		#region Private Methods

		#region private LoadData()
		/// Name			: LoadData
		/// Author			: Navneet Sota
		/// Date Created	: 1/4/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Loads the data from fee tables
		/// </summary>
		private void LoadData()
		{
			//objects
			StringBuilder sbSQL = null;
			DataSet objDataSet=null;
			try
			{
				sbSQL = new StringBuilder();

				sbSQL.Append("SELECT CALC_TYPE,STATE,ODBC_NAME, USER_TABLE_NAME, UCR_PERCENTILES,");
				sbSQL.Append(" DEFAULT_PERCENTILE FROM FEE_TABLES ") ;
				sbSQL.Append(" WHERE TABLE_ID = " + this.FeeScheduleId );
						
				objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString,sbSQL.ToString(), m_iClientId);

				if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
				{
					m_enumFeeScheduleType = (FeeScheduleType)Conversion.ConvertStrToInteger(objDataSet.Tables[0].Rows[0]["CALC_TYPE"].ToString());
					m_iStateID = Conversion.ConvertStrToInteger(objDataSet.Tables[0].Rows[0]["STATE"].ToString());
					m_objCommon.g_sODBCName = objDataSet.Tables[0].Rows[0]["ODBC_NAME"].ToString();
					m_sName = objDataSet.Tables[0].Rows[0]["USER_TABLE_NAME"].ToString();
					m_iUCRPercentile = Conversion.ConvertStrToInteger(objDataSet.Tables[0].Rows[0]["UCR_PERCENTILES"].ToString());
					m_iDefaultPercentile = Conversion.ConvertStrToInteger(objDataSet.Tables[0].Rows[0]["DEFAULT_PERCENTILE"].ToString());
				}
			}//end try
			catch (Exception p_objAppException)
			{
				throw new RMAppException(p_objAppException.Message, p_objAppException);
			}
			finally
			{
				if (objDataSet != null)
					objDataSet.Dispose();
			

				objDataSet = null;
			}
		}//end function

		#endregion

        #region private CalcFLHospital(Request p_objRequest, int p_iFLType, int p_iCount, Hashtable p_objHashtable, ref string p_sStudyId, ref string p_sStateCode, ref string p_sRevision)
        private double CalcFLHospital(Request p_objRequest, int p_iFLType, int p_iCount, Hashtable p_objHashtable, ref string p_sStudyId, ref string p_sStateCode, ref string p_sRevision)
        {
            string sSQL = null;
            string sBillItems = null;
            string sStateCode = null;
            string sStateName = null;
            string sCPTCode = null;

            int iTaotalDays = 0;
            int iRevCodeTemp = 0;
            int iUnitsBilled = 0;
            int itemp = 0;

            double dAmountBilled = 0.0;
            double dItemAmounBilled = 0.0;
            double dReturnValue = 0.0;

            double dAC_SUR_STAY = 0.0;
            double dAC_NONSUR_STAY = 0.0;
            double dTC_SUR_STAY = 0.0;
            double dTC_NONSUR_STAY = 0.0;
            double dSTOP_LOSS_AMT = 0.0;
            double dSTOP_LOSS_PER = 0.0;
            double dREHAB_PHYCH_PER = 0.0;
            double dOP_HOSP_PER = 0.0;
            double dOP_SCH_SURG_PER = 0.0;
            double dAS_GREATER_PER = 0.0;
            double dAS_LESSEQUAL_PER = 0.0;
            double dAS_NOTINSCHED_PER = 0.0;
            double dtemp = 0.0;

            int iFL_FEE_SCHED = 0;

            bool bStopLoss = false;
            bool bNotCovered = false;

            XmlDocument objSessionBillItem = new XmlDocument();
            DbReader objDbReader = null;
            FloridaHospital ObjFLHospital = null;
            LocalCache objCache = null;

            try
            {

                objCache = new LocalCache(m_objCommon.g_sConnectionString,m_iClientId);
                int iFeeTableID = p_objRequest.BillItemObject.EntFeeSchdId;
                sSQL = "SELECT CALC_TYPE, STATE, ODBC_NAME FROM FEE_TABLES WHERE ";
                sSQL = sSQL + " TABLE_ID = " + iFeeTableID;
                objDbReader = DbFactory.GetDbReader(m_objCommon.g_sConnectionString, sSQL);
                if (objDbReader.Read())
                {
                    int iTableType = objDbReader.GetInt32("CALC_TYPE");
                    int iStateID = objDbReader.GetInt32("STATE");
                    objCache.GetStateInfo(iStateID, ref sStateCode, ref sStateName);
                    string sDSNName = objDbReader.GetString("ODBC_NAME");
                }
                objDbReader = null;

                sSQL = "SELECT TABLE_ID,AC_SUR_STAY,AC_NONSUR_STAY,TC_SUR_STAY,TC_NONSUR_STAY,STOP_LOSS_AMT,STOP_LOSS_PER, ";
                sSQL = sSQL + " REHAB_PHYCH_PER,OP_HOSP_PER,OP_SCH_SURG_PER,AS_GREATER_PER,AS_LESSEQUAL_PER,AS_NOTINSCHED_PER, ";
                sSQL = sSQL + " FL_FEE_SCHED FROM WRCMP_PERDIEM_OPT WHERE TABLE_ID = " + iFeeTableID;
                objDbReader = DbFactory.GetDbReader(m_objCommon.g_sConnectionString, sSQL);
                if (objDbReader.Read())
                {
                    dAC_SUR_STAY = objDbReader.GetDouble("AC_SUR_STAY");
                    dAC_NONSUR_STAY = objDbReader.GetDouble("AC_NONSUR_STAY");
                    dTC_SUR_STAY = objDbReader.GetDouble("TC_SUR_STAY");
                    dTC_NONSUR_STAY = objDbReader.GetDouble("TC_NONSUR_STAY");
                    dSTOP_LOSS_AMT = objDbReader.GetDouble("STOP_LOSS_AMT");
                    dSTOP_LOSS_PER = (objDbReader.GetDouble("STOP_LOSS_PER")) / 100;
                    dREHAB_PHYCH_PER = (objDbReader.GetDouble("REHAB_PHYCH_PER")) / 100;
                    dOP_HOSP_PER = (objDbReader.GetDouble("OP_HOSP_PER")) / 100;
                    dOP_SCH_SURG_PER = (objDbReader.GetDouble("OP_SCH_SURG_PER")) / 100;
                    dAS_GREATER_PER = (objDbReader.GetDouble("AS_GREATER_PER")) / 100;
                    dAS_LESSEQUAL_PER = (objDbReader.GetDouble("AS_LESSEQUAL_PER")) / 100;
                    dAS_NOTINSCHED_PER = (objDbReader.GetDouble("AS_NOTINSCHED_PER")) / 100;
                    iFL_FEE_SCHED = objDbReader.GetInt("FL_FEE_SCHED");

                }
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
                this.MoveToRecord(iFL_FEE_SCHED);

                //lines 1,2,3,4 subject to stop loss and only one bill item should have value all others 0
                switch (p_iFLType)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        if (p_iFLType == 1)                     //1 Accuate Care Surgical Stay
                            dReturnValue = dAC_SUR_STAY;
                        else if (p_iFLType == 2)                //2 Non-surgical
                            dReturnValue = dAC_NONSUR_STAY;
                        else if (p_iFLType == 3)                //3 Trauma surgical
                            dReturnValue = dTC_SUR_STAY;
                        else                                   //4 trama non-surgical
                            dReturnValue = dTC_NONSUR_STAY;

                        foreach (DictionaryEntry Item in p_objHashtable)
                        {

                            sBillItems = Item.Value.ToString();
                            objSessionBillItem.LoadXml(sBillItems);
                            //loop to determine days
                            iRevCodeTemp = Conversion.ConvertObjToInt(objSessionBillItem.SelectSingleNode("//RevenueCode").InnerText, m_iClientId);
                            iUnitsBilled = Conversion.ConvertObjToInt(objSessionBillItem.SelectSingleNode("//unitsbillednum").InnerText, m_iClientId);
                            dItemAmounBilled = Conversion.ConvertStrToDouble(objSessionBillItem.SelectSingleNode("//amountbilled").InnerText);
                            if ((iRevCodeTemp >= 120) && (iRevCodeTemp <= 169))
                                iTaotalDays = iTaotalDays + iUnitsBilled;

                            dAmountBilled = dAmountBilled + dItemAmounBilled;
                        }

                        if (dAmountBilled > dSTOP_LOSS_AMT)
                            bStopLoss = true;

                        

                        if (bStopLoss)
                        {//each row times stop loss %
                            if (p_iCount == 1)
                            {
                                //p_objRequest.BillItemObject.AmountBilled = dAmountBilled;
                                dReturnValue = dAmountBilled * dSTOP_LOSS_PER;

                                
                                //lTemp = lGetCodeIDWithShort("92", lGetTableID("EOB_CODES")) 'was 10
                                //objbillitem.EOBCodes.Clear
                                //objbillitem.EOBCodes.Add lTemp, sGetCodeDesc(lTemp)
                                //objbillitem.dAmountAllowed = objbillitem.dAmountToPay
                            }
                            else
                            {
                               // p_objRequest.BillItemObject.AmountBilled = 0.0;
                                dReturnValue = 0;
                                
                                //lTemp = lGetCodeIDWithShort("92", lGetTableID("EOB_CODES")) 'was 12
                                //objbillitem.EOBCodes.Clear
                                //objbillitem.EOBCodes.Add lTemp, sGetCodeDesc(lTemp)
                                //objbillitem.dAmountAllowed = objbillitem.dAmountToPay
                            }

                        }
                        else
                        {
                            if (p_iCount == 1) //hospital amt
                            {
                                //p_objRequest.BillItemObject.AmountBilled = dAmountBilled;
                                dReturnValue = dReturnValue * iTaotalDays;
                                
                                //lTemp = lGetCodeIDWithShort("92", lGetTableID("EOB_CODES"))
                                //objbillitem.EOBCodes.Clear
                                //objbillitem.EOBCodes.Add lTemp, sGetCodeDesc(lTemp)
                                //objbillitem.dAmountAllowed = objbillitem.dAmountToPay
                            }

                            else //zero
                            {
                                //p_objRequest.BillItemObject.AmountBilled = 0.0;
                                dReturnValue = 0;
                                
                                //lTemp = lGetCodeIDWithShort("92", lGetTableID("EOB_CODES")) ' was 12
                                //objbillitem.EOBCodes.Clear
                                //objbillitem.EOBCodes.Add lTemp, sGetCodeDesc(lTemp)
                                //objbillitem.dAmountAllowed = objbillitem.dAmountToPay

                            }


                        }
                        break;

                    case 5:
                        dAmountBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.AmountBilled;
                        dReturnValue = Math.Round(dAmountBilled * Math.Round(dREHAB_PHYCH_PER, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero);

                        ////'impatient rehab
                        //dScheduleAmt = Round(dAmountBilled * Round(REHAB_PHYCH_PER, 2), 2)

                        //objbillitem.dSchdAmount = dScheduleAmt
                        //objbillitem.dAmountAllowed = dScheduleAmt
                        //objbillitem.dAmountToPay = dScheduleAmt
                        //objbillitem.dAmountSaved = dAmountBilled - dScheduleAmt
                        //lTemp = lGetCodeIDWithShort("92", lGetTableID("EOB_CODES"))
                        //objbillitem.EOBCodes.Clear
                        //objbillitem.EOBCodes.Add lTemp, sGetCodeDesc(lTemp)
                        break;
                    case 6:
                    case 7:
                        dAmountBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.AmountBilled;
                        iRevCodeTemp = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.RevenueCode; // 'mjh need revenue code
                        switch (iRevCodeTemp)
                        {
                            case 300:
                            case 320:
                            case 420:
                            case 430:


                                //'NEED TO GET ICD9 CODE - ask for it
                                sCPTCode = p_objRequest.CPT;//Trim$(objbillitem.sCPT)
                                itemp = 0;
                                if (sCPTCode.Length > 6)
                                    itemp = sCPTCode.IndexOf("-");
                                if (itemp > 0 && itemp <= 7)
                                    sCPTCode = sCPTCode.Substring(0, itemp);

                                ObjFLHospital = new FloridaHospital(m_objCommon,m_iClientId);
                                ObjFLHospital.UnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
                                ObjFLHospital.UnitsBilledType = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType;
                                ObjFLHospital.FeeTableID = iFL_FEE_SCHED;
                                ObjFLHospital.CPT = sCPTCode;

                                ObjFLHospital.POS = objCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.PlaceOfSerCode); // sGetShortCode(DTGCodeControlPOS.CodeValue)
                                ObjFLHospital.StateCode = sStateCode;
                                ObjFLHospital.TOS = objCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);    //DTGCodeControlTOS.CodeValue)
                                ObjFLHospital.ZipCode = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ZipCode;  //Left$(MaskEditZipCode.Text, 5)

                                ObjFLHospital.FLLicense = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.FlLicense;
                                dReturnValue = ObjFLHospital.CalcFlHospital(this, p_objRequest, ref p_sStudyId, ref p_sStateCode, ref p_sRevision);
                                //MITS 11339 : Umesh
                                if (m_objCommon.Errors.Count != 0)
                                {  //exist function
                                    return dReturnValue;
                                }
                                //MITS 11339 : End
                                else
                                {
                                bNotCovered = ObjFLHospital.NotCovered;

                                if (bNotCovered || dReturnValue <= 0)
                                {
                                    if (p_iFLType == 6)
                                        dReturnValue = Math.Round(dAmountBilled * Math.Round(dOP_HOSP_PER, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero);

                                    if (p_iFLType == 7)
                                        dReturnValue = Math.Round(dAmountBilled * Math.Round(dOP_SCH_SURG_PER, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero);
                                    }
                                }
                                break;
                            default:
                                //'outpatient
                                if (p_iFLType == 6)
                                    dReturnValue = Math.Round(dAmountBilled * Math.Round(dOP_HOSP_PER, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero);
                                //' scheduled
                                if (p_iFLType == 7)
                                    dReturnValue = Math.Round(dAmountBilled * Math.Round(dOP_SCH_SURG_PER, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero);
                                break;
                        }

                        //objbillitem.dSchdAmount = dScheduleAmt
                        //objbillitem.dAmountAllowed = dScheduleAmt
                        //objbillitem.dAmountToPay = dScheduleAmt
                        //objbillitem.dAmountSaved = dAmountBilled - dScheduleAmt
                        //lTemp = lGetCodeIDWithShort("92", lGetTableID("EOB_CODES"))
                        //objbillitem.EOBCodes.Clear
                        //objbillitem.EOBCodes.Add lTemp, sGetCodeDesc(lTemp)
                        break;
                    case 8: //'AMBULATORY

                        dAmountBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.AmountBilled;       //'mjh 05/11/2003

                        //'NEED TO GET ICD9 CODE - ask for it
                        sCPTCode = p_objRequest.CPT;

                        itemp = 0;
                        if (sCPTCode.Length > 6)
                            itemp = sCPTCode.IndexOf("-");
                        if (itemp > 0 && itemp <= 7)
                            sCPTCode = sCPTCode.Substring(0, itemp);

                        ObjFLHospital = new FloridaHospital(m_objCommon,m_iClientId);
                        ObjFLHospital.UnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
                        ObjFLHospital.UnitsBilledType = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType;
                        ObjFLHospital.FeeTableID = iFL_FEE_SCHED;
                        ObjFLHospital.CPT = sCPTCode;



                        ObjFLHospital.POS = objCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.PlaceOfSerCode); // sGetShortCode(DTGCodeControlPOS.CodeValue)
                        ObjFLHospital.StateCode = sStateCode;
                        ObjFLHospital.TOS = objCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);    //DTGCodeControlTOS.CodeValue)
                        ObjFLHospital.ZipCode = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ZipCode;  //Left$(MaskEditZipCode.Text, 5)

                        ObjFLHospital.FLLicense = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.FlLicense;

                        dReturnValue = ObjFLHospital.CalcFlHospital(this, p_objRequest, ref p_sStudyId, ref p_sStateCode, ref p_sRevision);
                        //MITS 11339 : Umesh
                        if (m_objCommon.Errors.Count!=0)
                        {  //exist function
                            return dReturnValue;
                        }
                        //MITS 11339 : End
                        else
                        {
                        bNotCovered = ObjFLHospital.NotCovered;

                        //'now use to determine what to pay
                        if (bNotCovered || dReturnValue == 0)   // ' use values from db
                            //'AS_NOTINSCHED_PER
                            //'mjh todo - do we product against rounding error
                            dReturnValue = dAmountBilled * Math.Round(dAS_NOTINSCHED_PER, 2, MidpointRounding.AwayFromZero);
                        else
                        {
                            if (dAmountBilled > dReturnValue)
                            {
                                //'value AS_GREATER_PER
                                dtemp = dAmountBilled * Math.Round(dAS_GREATER_PER, 2, MidpointRounding.AwayFromZero);
                                //'greater of dtemp or schedule amount
                                if (dtemp < dReturnValue)
                                {
                                    dtemp = dReturnValue;
                                }
                                dReturnValue = dtemp;
                            }
                            else //' AS_LESSEQUAL_PER
                            {
                                dtemp = dAmountBilled * Math.Round(dAS_LESSEQUAL_PER, 2, MidpointRounding.AwayFromZero);
                                if (dReturnValue < dtemp)
                                {
                                    dtemp = dReturnValue;
                                }
                                dReturnValue = dtemp;
                            }
                        }
                        dReturnValue = Math.Round(dReturnValue, 2, MidpointRounding.AwayFromZero);
                        }
                        //'value AS_GREATER_PER    AS_LESSEQUAL_PER   AS_NOTINSCHED_PER
                        //Set oFLHospital = Nothing

                        //            objbillitem.dSchdAmount = dScheduleAmt
                        //            objbillitem.dAmountAllowed = dScheduleAmt
                        //            objbillitem.dAmountToPay = dScheduleAmt
                        //            objbillitem.dAmountSaved = dAmountBilled - dScheduleAmt
                        //            'abisht 9277 Changed the code.
                        //            lTemp = lGetCodeIDWithShort("91", lGetTableID("EOB_CODES"))
                        //'            lTemp = lGetCodeIDWithShort("08", "EOB_CODES")
                        //            objbillitem.EOBCodes.Clear
                        //            objbillitem.EOBCodes.Add lTemp, sGetCodeDesc(lTemp)
                        break;

                }
            }
            catch (Exception p_objAppException)
            {
                throw new RMAppException(p_objAppException.Message, p_objAppException);
            }
            finally
            {
                objSessionBillItem = null;
                ObjFLHospital = null;
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
                
                if(objCache != null)
                objCache.Dispose();

            }
            return dReturnValue;

        }

        #endregion
		#endregion

		#region Destructor
		~FeeSchedule()
		{
			m_objCommon = null;
		}
		# endregion

	}//end Class
}//end Namespace
