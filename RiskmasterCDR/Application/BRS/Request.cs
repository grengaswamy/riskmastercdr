using System;
using System.Collections;

using Riskmaster.Common;

using Riskmaster.DataModel;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.Application.BRS
{
	/**************************************************************
		 * $File		: Request.cs
		 * $Revision	: 1.0.0.0
		 * $Date		: 01/03/2005
		 * $Author		: Navneet Sota
		 * $Comment		: Wrapper Class to contain all Information required for a BRS Calculation Request.
		 *				  Attempts to minimize duplicate data fielding by using existing objects.
		 * $Source		: Riskmaster.Application 	
		**************************************************************/
	internal class Request:CollectionBase,IDisposable
	{
		#region Variable Declarations	

		/// <summary>
		/// Represents the Module variable for Common Class.
		/// </summary>
		private Common m_objCommon = null;

		/// <summary>
		/// Represents the Units Billed Label.
		/// </summary>
		private string m_sUnitsBilledLabel = "";

		/// <summary>
		/// Represents the Bill Item Class.
		/// </summary>
		private BillItem m_objBillItem = null;
		/// <summary>
		/// Provider contract.
		/// </summary>
		private ProviderContracts m_objProviderContract;
		
		/// <summary>
		/// Represents the Minimum Units Billed.
		/// </summary>
		private int m_iUnitsBilledMin = 0;

		private int m_iStep=0;

        private string m_sCmd = "";
		/// <summary>
		/// Represents the collection of Params.
		/// </summary>
		private Hashtable m_hstParams = null;
        private int m_iClientId = 0;
		/// <summary>
		/// Tanuj->Should contain errors/alerts for BRS module, will be set by BRS adaptor and errors/alerts will be displayed onto UI. 
		/// </summary>
		private BusinessAdaptorErrors m_objErrors;

		public BusinessAdaptorErrors Errors
		{
			get
			{
				return m_objErrors;
			}
			set
			{
				m_objErrors=value;
			}
		}
		#endregion
		
		#region Constructor
		/// Name		: Request
		/// Author		: Navneet Sota
		/// Date Created: 12/14/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
		/// <param name="p_objCommon">Common Class object instantiated by Manager </param>
        internal Request(Common p_objCommon, int p_iClientId)
		{
			m_objCommon = p_objCommon;
            m_iClientId = p_iClientId;
			m_hstParams = new Hashtable();
			m_objCommon.g_hstMissingParams = new Hashtable();
			m_objCommon.g_hstErrors = new Hashtable();
		}
		
		#endregion

		#region Properties
		public Common Common
		{
			get
			{
				return m_objCommon;
			}
		}
		/// <summary>
		/// Internal property for getting Parameters
		/// </summary>
		internal Hashtable Params
		{
			get
			{
				return m_hstParams;
			}	
		}

		/// <summary>
		/// Internal property for getting Missing Parameters
		/// </summary>
		internal Hashtable MissingParams
		{
			get
			{
				return m_objCommon.g_hstMissingParams;
			}	
		}

		/// <summary>
		/// Internal property for getting Alerts collection
		/// </summary>
		internal Hashtable Alerts
		{
			get
			{
				return m_objCommon.g_hstAlerts;
			}	
		}

//		/// <summary>
//		/// Internal property for getting Errors collection
//		/// </summary>
//		internal Hashtable Errors
//		{
//			get
//			{
//				return m_objCommon.g_hstErrors;
//			}	
//		}

		/// <summary>
		/// Internal property for setting and getting Bill Item object
		/// </summary>
		internal BillItem BillItemObject
		{
			set
			{
				m_objBillItem = value;
			}	
			get
			{
				return m_objBillItem;
			}	
		}
		internal ProviderContracts ProviderContract
		{
			set
			{
				m_objProviderContract=value;
			}
			get
			{
				return m_objProviderContract;
			}
		}
		/// <summary>
		/// The units input is normally the # of billed units.  Interpreted as the number
		/// of times the procedure was performed.If the request involved an Anesthesia service 
		/// then the units input means "# of additional billed units".
		/// </summary>
		internal string UnitsBilledLabel
		{
			set
			{
				m_sUnitsBilledLabel = value;
			}	
			get
			{
				string sReturnValue = "";
                using (LocalCache localCache = new LocalCache(m_objCommon.g_sConnectionString, m_iClientId))
                {
                    if (localCache.GetShortCode(BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode) == BillItem.ANESTHESIA_SHORT_CODE.ToString())
                        sReturnValue = Globalization.GetString("Request.UnitsBilledLabel.SerCode7", m_iClientId);
                    else
                        sReturnValue = Globalization.GetString("Request.UnitsBilledLabel.SerCode", m_iClientId);
                }
				return sReturnValue;
			}	
		}

		/// <summary>
		/// If the request involved an Anesthesia service then the minimum
		/// # of "additional billed units" can be zero.
		/// Otherwise the # of billed units is interpreted as the number of times 
		/// the procedure was performed and must be at least 1.
		/// </summary>
		internal int UnitsBilledMin
		{
			set
			{
				m_iUnitsBilledMin = value;
			}	
			get
			{
				return m_iUnitsBilledMin;
			}	
		}

		/// <summary>
		/// Internal property for CPT
		/// </summary>
		internal string CPT
		{
			get
			{
				//Mukul Changed MITS 8667 earlier it was taking 5 characters which was actually removing specialty code
				//if(m_objBillItem.objSplit.BrsInvoiceDetail.BillingCodeText.IndexOf(" - ")>0 && m_objBillItem.objSplit.BrsInvoiceDetail.BillingCodeText.Trim().Length>=5)
				if(m_objBillItem.objSplit.BrsInvoiceDetail.BillingCodeText.IndexOf(" - ")>0)	
				{
					return m_objBillItem.objSplit.BrsInvoiceDetail.BillingCodeText.Substring(0,m_objBillItem.objSplit.BrsInvoiceDetail.BillingCodeText.IndexOf(" - "));
				}
				else
				{ 
					if(m_objBillItem.objSplit.BrsInvoiceDetail.BillingCodeText.Trim().Length>=5)
						return m_objBillItem.objSplit.BrsInvoiceDetail.BillingCodeText.Substring(0,5);
					else
						return m_objBillItem.objSplit.BrsInvoiceDetail.BillingCodeText;
				}
				//return (this.m_objBillItem.objSplit.BrsInvoiceDetail.BillingCodeText);
			}	
		}
		
		//Mukul Added for taking Step value ahead in request for manual overriding
		internal int Step
		{
			set
			{
				m_iStep = value;
			}	
			get
			{
				return m_iStep;
			}	
		}

        //MITS 12372 :ukusvaha
        internal string Command
        {
            set
            {
                m_sCmd = value;
            }
            get
            {
                return m_sCmd;
            }
        }



		#endregion

		#region internal Methods
		
		#region AddMissingParam(p_sInput)
		/// Name			: AddMissingParam
		/// Author			: Navneet Sota
		/// Date Created	: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Adds passed in value to the Missing Param collection
		/// </summary>
		internal void AddMissingParam(string p_sInput)
		{
			if(m_objCommon.g_hstMissingParams.ContainsKey(p_sInput))
				m_objCommon.g_hstMissingParams.Remove(p_sInput);
			
			m_objCommon.g_hstMissingParams.Add(p_sInput,p_sInput);

		}
		#endregion

		#region GetMissingParam(p_sInputKey)
		/// Name			: GetMissingParam
		/// Author			: Navneet Sota
		/// Date Created	: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the value of Missing Param from collection based on passed in key
		/// </summary>
		internal object GetMissingParam(string p_sInputKey)
		{
			object objReturnValue = null;

			if(m_objCommon.g_hstMissingParams.ContainsKey(p_sInputKey))
				objReturnValue = m_objCommon.g_hstMissingParams[p_sInputKey];

			return objReturnValue;
		}
		#endregion

		#region AddParam(p_objInput, p_sInputKey)
		/// Name			: AddParam
		/// Author			: Navneet Sota
		/// Date Created	: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Adds passed in value to the Param collection
		/// </summary>
		internal void AddParam(object p_objInput, string p_sInputKey)
		{
			if(m_hstParams.ContainsKey(p_sInputKey))
				m_hstParams.Remove(p_sInputKey);
			
			//m_hstParams.Add(p_objInput,p_sInputKey);
			m_hstParams.Add(p_sInputKey,p_objInput);
		}
		#endregion

		#region GetParam(p_sInputKey)
		/// Name			: GetParam
		/// Author			: Navneet Sota
		/// Date Created	: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the value of Param from collection based on passed in key
		/// </summary>
		internal object GetParam(string p_sInputKey)
		{
			object objReturnValue = null;

			if(m_hstParams.ContainsKey(p_sInputKey))
				objReturnValue = m_hstParams[p_sInputKey];

			return objReturnValue;
		}
		#endregion

		#region GetParamICCPTList(p_sInputKey)
		/// Name			: GetParamICCPTList
		/// Author			: Navneet Sota
		/// Date Created	: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the value of Param from collection 
		/// when the passed in inputkey is ICCPTLIST
		/// </summary>
		internal Hashtable GetParamICCPTList(string p_sInputKey)
		{
			Hashtable hstReturnValue = null;

			if(p_sInputKey.ToUpper() == "ICCPTLIST" && m_hstParams.ContainsKey(p_sInputKey))
					hstReturnValue = (Hashtable)m_hstParams[p_sInputKey];

			return hstReturnValue;
		}
		#endregion

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
	      m_objCommon = null;
		  m_objBillItem = null;
		  m_objProviderContract=null;
		}

		#endregion
	}// End class
} // End namespace