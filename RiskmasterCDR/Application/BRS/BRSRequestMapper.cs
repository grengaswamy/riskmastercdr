﻿
using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.BRS
{
	/// <summary>
	/// Summary description for RequestMapper.
	/// </summary>
	public class BRSRequestMapper
	{
		XmlDocument m_objReqInstance=null;
		XmlDocument m_objBrsStandardInstance;
        private int m_iClientId = 0;
        public BRSRequestMapper(ref XmlDocument p_objReqInstance, ref XmlDocument p_objBrsStandardInstance, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_objReqInstance=p_objReqInstance;
			m_objBrsStandardInstance=p_objBrsStandardInstance;
		}
		public void MapRequestInstanceToStandardBrsInstance()
		{
			XmlNodeList objTargetList;
			XmlNodeList objSourceList;
			int iPositionOfUnitsBilled = 0;
			try
			{
				
				objTargetList=m_objBrsStandardInstance.SelectNodes("//control");
				objSourceList=m_objReqInstance.SelectNodes("//control");
                int iStep = Conversion.ConvertObjToInt(m_objReqInstance.SelectSingleNode("//step").InnerText, m_iClientId);
				bool bIsCalculateStep = false;
				string sTemp = "" ;
				if(m_objReqInstance.SelectSingleNode("//cmd")!=null)
				{
				  sTemp = m_objReqInstance.SelectSingleNode("//cmd").InnerText.Trim().ToLower();

				}
				if(iStep ==(int)Riskmaster.Application.BRS.Common.eSteps.Calculate || sTemp =="complete")
				{
					bIsCalculateStep = true ;
				}
				if(iStep ==(int)Riskmaster.Application.BRS.Common.eSteps.FeeSchedule )
				{
					bIsCalculateStep = true ;
				}
				XmlNode objTargetNode = null;
				foreach(XmlNode objNodeSrc in objSourceList)
				{
					string temp="";
					temp= objNodeSrc.Attributes.GetNamedItem("name").Value;
					if( temp == "unitsbilled")
					{
						
						objTargetNode = m_objBrsStandardInstance.SelectNodes("//" + objNodeSrc.Attributes["name"].Value)[iPositionOfUnitsBilled];
						++iPositionOfUnitsBilled ;
					}
					else
					{
						objTargetNode= m_objBrsStandardInstance.SelectSingleNode("//" + objNodeSrc.Attributes["name"].Value);
					}
					if(objTargetNode.Attributes["type"].Value.Trim().ToLower()=="controlgroup")
					{
						continue;
					}
					
					if(!bIsCalculateStep )
					{
						if(IsBlank(objNodeSrc))
						{
                             continue;
						}
					 }
						if(!HandleSpecialControls( objNodeSrc))
						{
							if(objTargetNode.Attributes["type"].Value.ToLower().Trim()==objNodeSrc.Attributes["type"].Value.ToLower().Trim()) 
							{
							
								//Tanuj->deprecated, as this was changing the positions of the nodes being modified/copied over.//objTargetNode.ParentNode.AppendChild(m_objBrsStandardInstance.ImportNode(objNodeSrc,true));
                                if (temp != "zip")
                                {
                                    objTargetNode.ParentNode.InsertAfter(m_objBrsStandardInstance.ImportNode(objNodeSrc, true), objTargetNode);
                                    objTargetNode.ParentNode.RemoveChild(objTargetNode);
                                }
                                else
                                {
                                    bool bNewSession = false;
                                    bNewSession = Conversion.ConvertStrToBool( m_objReqInstance.SelectSingleNode("//CreateNewSession").InnerText);
                                    if (objTargetNode.InnerText.Trim() == ""||!bNewSession)
                                    {
                                        objTargetNode.ParentNode.InsertAfter(m_objBrsStandardInstance.ImportNode(objNodeSrc, true), objTargetNode);
                                        objTargetNode.ParentNode.RemoveChild(objTargetNode);
                                    }
                                }
							}
							else
							{
								MapNonMatchingTypeControls(ref objTargetNode, objNodeSrc);
							}
						}
					
				}
			}
			catch(Exception p_objErr)
			{
                throw new RMAppException(Globalization.GetString("BRSRequestMapper.MapRequestInstanceToStandardBrsInstance.Error", m_iClientId), p_objErr);
			}
		}
		private bool IsBlank(XmlNode p_objSrcNode)
		{
			string sNodeType=p_objSrcNode.Attributes["type"].Value.ToLower().Trim();
			switch(sNodeType)
			{
				case Constants.CODE_LIST :
				case "combobox" :
				{
                    return IsNullOrEmpty(p_objSrcNode.Attributes["codeid"].Value);
					
				}
				case Constants.CURRENCY:
				{
					
					if(p_objSrcNode.InnerText=="0.00" || p_objSrcNode.InnerText=="0")
					{
					  return true;
					}
					return IsNullOrEmpty(p_objSrcNode.InnerText.Trim());

				}
				default:
				{
                  return IsNullOrEmpty(p_objSrcNode.InnerText.Trim());
				}
			}
		}
		private bool HandleSpecialControls( XmlNode p_objSrcCtrl)
		{
			bool bControlHandled=false;
			string sCtrlName="";
			string sCtrlType="";
			XmlNode objTargetNode=null;
			try
			{
				 sCtrlName=p_objSrcCtrl.Attributes["name"].Value.ToLower().Trim();
				 sCtrlType=p_objSrcCtrl.Attributes["type"].Value.ToLower().Trim();

				switch(sCtrlName)
				{
				  case "billingcode":
					{
					  if(sCtrlType=="text")
					  {
                         
						  if(p_objSrcCtrl.InnerText.Trim()!="")
						  {
							  objTargetNode = m_objBrsStandardInstance.SelectSingleNode("//billingcode_text");
							  objTargetNode.ParentNode.InsertAfter(m_objBrsStandardInstance.ImportNode(p_objSrcCtrl,true),objTargetNode);
							  objTargetNode.ParentNode.RemoveChild(objTargetNode);
																																																	   
						  }

					  }
					  else
					  {

						  if(sCtrlType=="billingcodelookup")
						  {
							  if(p_objSrcCtrl.Attributes["codeid"].Value.Trim()!="" || p_objSrcCtrl.InnerText.Trim()!="")
							  {
								  objTargetNode = m_objBrsStandardInstance.SelectSingleNode("//billingcode_lookup");
								  objTargetNode.ParentNode.InsertAfter(m_objBrsStandardInstance.ImportNode(p_objSrcCtrl,true),objTargetNode);
								  objTargetNode.ParentNode.RemoveChild(objTargetNode);

							  }
                             
						  }
					  }
					  bControlHandled=true;
					  break;
					}
					default:
					{
                         bControlHandled=false;
						break;
					}
				}

			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}

         return bControlHandled;
		}
		private bool IsNullOrEmpty(string p_sVal)
		{
			//Depracated, as some controls can have "0" as the value and it defines execution logic//if(p_sVal==null || p_sVal.Trim()=="" || p_sVal.Trim()=="0")
		    if(p_sVal==null || p_sVal.Trim()=="" )
			{
				return true;
			}
			return false;

		}
		private void MapNonMatchingTypeControls(ref XmlNode p_objTarget,XmlNode p_objSource)
		{
            string sSrcType=p_objSource.Attributes["type"].Value.ToLower().Trim();
			string sTargetType=p_objTarget.Attributes["type"].Value.ToLower().Trim();
			switch (sSrcType)
			{
				case "combobox":
				{
					p_objTarget.InnerText=p_objSource.Attributes["codeid"].Value;
					break;
				}
				case Constants.CODE_LIST:
				{
					//modifier code vs codelist mapping
					if(p_objSource.Attributes["type"].Value==Constants.CODE)
					{
						p_objTarget.Attributes[Common.ATT_ID].Value=p_objTarget.Attributes[Common.ATT_ID].Value;
					}
					break;
				}
				case Constants.CODE:
				{
					//modifier code vs codelist mapping
					if(p_objTarget.Attributes["type"].Value==Constants.CODE_LIST)
					{
						p_objTarget.Attributes["codeid"].Value=p_objSource.Attributes["codeid"].Value;
						p_objTarget.InnerText=p_objSource.InnerText;
					}
					break;
				}
                case "readonly":
                {
                    p_objTarget.InnerText = p_objSource.InnerText;
                    if (p_objTarget.SelectSingleNode("@codeid") != null)
                    {
                        p_objTarget.Attributes["codeid"].Value = p_objSource.Attributes["codeid"].Value;
                    }

                    break;

                }
				default:
				{
                 p_objTarget.InnerText=p_objSource.InnerText;
					break;
				}
			}
		}
	}

}
