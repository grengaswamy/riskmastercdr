﻿
using System;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using System.Xml;

using Riskmaster.ExceptionTypes;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.Application.BRS
{
	/**************************************************************
	 * $File		: CalculateWC.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/07/2004
	 * $Author		: Navneet Sota
	 * $Comment		: This class 
	 * $Source		: Riskmaster.Application.BRS
	**************************************************************/
	internal class CalculateWC
	{
		#region Variable Declarations	

		/// <summary>
		/// Represents the global variable for ModCode structure.
		/// </summary>
		internal ModCode m_structModCode;

		/// <summary>
		/// Represents the Module variable for Common Class.
		/// </summary>
		private Common m_objCommon = null;

		/// <summary>
		/// Represents the Module variable for Common.LocalCache Class.
		/// </summary>
		private LocalCache m_objLocalCache = null;
		/// <summary>
		/// Tanuj->Should contain errors/alerts for BRS module, will be set by BRS adaptor and errors/alerts will be displayed onto UI. 
		/// </summary>
		private BusinessAdaptorErrors m_objErrors;

		public BusinessAdaptorErrors Errors
		{
			get
			{
				return m_objErrors;
			}
			set
			{
				m_objErrors=value;
			}
		}

        private int m_iClientId = 0; //rkaur27
		# endregion

		#region ModCode Structure

		/// <summary>
		/// Structure containing variables for ModCode.
		/// </summary>
		internal struct ModCode
		{
			internal string sMod;
			internal int iModCode;
			internal double dPercent;
			internal double dPercentMax;
		}
		#endregion
		
		#region Constructor
		/// Name		: CalculateUCR
		/// Author		: Navneet Sota
		/// Date Created: 01/07/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
		/// <param name="p_objCommon">Common Class object instantiated by Manager </param>
		internal CalculateWC(Common p_objCommon, int p_iClientId)//rkaur27
		{
			m_objCommon = p_objCommon;
            m_iClientId = p_iClientId;
            m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString, m_iClientId);
		}
		
		#endregion

		#region Internal Methods

		#region internal CalcWorkComp(p_objFeeSchedule,p_objRequest,p_sStudyId,p_sStateCode)
		/// Name			: CalcWorkComp
		/// Author			: Navneet Sota
		/// Date Created	: 01/10/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
        /// 4/16/2008 abisht.MITS 12005.Changed the signature of the function.
		/// <summary>
		/// Calculates the Workers Compensation
		/// </summary>
		/// <param name="p_objFeeSchedule">FeeSchedule Class Object</param>
		/// <param name="p_objRequest">Request Class Object</param>
		/// <param name="p_sStateCode">State Code</param>
		/// <param name="p_sStudyId">Study ID</param>
		internal double CalcWorkComp(FeeSchedule p_objFeeSchedule, Request p_objRequest, 
                                     ref string p_sStudyId, ref string p_sStateCode)
		{        
			//int Variables
			int iFeeSchedule = 0;
           
			//double variables
			double dReturnValue = -1;
			double dMaxSchdAmount = -1;
			double dFactor = 0.0;
			double dUnitsBilled = 0.0;
			double dModValue = 0.0;
			double dTemp = 0.0;
			double dMaxRlv = 0.0;
			double dTotMod = 0.0;

			//string variables
			string sMods = "";
			string sERRZipCode = "";
			string sStateId = "";
			string sShortCode = "";
			string sCodeDesc = "";
			string sEffDate = "";
			string sFeeFlag = "";
			string sSpecialtyShortCode = "";

			//bool variables
			bool bTemp = false;

			//Collections
			Hashtable hstRelativeValue = null;
			Hashtable hstFactors = null;

			//objects
			StringBuilder sbSQL = null;
			DataModel.DataSimpleList objDSList = null;
			RelativeValues.RelativeValue objRelativeValue = null;
			DataSet objDataSet=null;
			DbReader objDbReader = null;
			try
			{
				iFeeSchedule = p_objFeeSchedule.FeeScheduleId;

				objDSList = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList;
				if(m_objLocalCache == null)
					m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString,m_iClientId);
                //srajindersin MITS 36730 dt 7/14/2014
                m_objLocalCache.GetStateInfo(p_objFeeSchedule.StateId, ref p_sStateCode, ref sCodeDesc);

				//loop through modifier codes collection:
				//	1.  create array of ModCodes for use below
				//	2.  create the sMods list of short codes used in SQL statements
				//	3.  calculate NV dblMaxSchdAmount
			    if(objDSList.Count > 0)
                {
					m_objLocalCache.GetCodeInfo(Conversion.ConvertObjToInt(objDSList[0], m_iClientId), ref sShortCode, ref sCodeDesc);
					sMods = "'" + sShortCode + "'";
					m_structModCode.iModCode = 1;
					m_structModCode.sMod = sShortCode;
					m_structModCode.dPercent = -1;
					if(p_sStateCode.ToUpper() == "NV" && sShortCode.ToUpper() == "B")
						dMaxSchdAmount = p_objFeeSchedule.CalcASCAmount(p_objRequest);
				}//end if(objDSList.Count > 0)

				//get WRKMAS variables for this state
				sbSQL = new StringBuilder();

				//Table[0]
                //srajindersin MITS 36730 dt 7/14/2014
				sbSQL.Append(" SELECT FEEFLAG,EFFDATE,STUDY ");
				sbSQL.Append(" FROM WRKMAS ");
				sbSQL.Append(" WHERE STATE = '").Append(p_sStateCode).Append("'");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						sFeeFlag = objDbReader.GetValue("FEEFLAG").ToString();
						sEffDate = objDbReader.GetValue("EFFDATE").ToString();
						p_sStudyId = objDbReader.GetValue("STUDY").ToString();
					}
					else
					{
						m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrState", m_iClientId));
						//Exit Function
						return dReturnValue;
					}
				}

				sbSQL.Remove(0,sbSQL.Length);
				sbSQL.Append(" SELECT STATEID FROM WRKZIP WHERE STUDY = '").Append(p_sStudyId).Append("' ");
				sbSQL.Append(" AND '").Append(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ZipCode);
				sbSQL.Append("' BETWEEN BEGZIP AND ENDZIP ");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						sStateId = objDbReader.GetValue("STATEID").ToString();
					}
					else
					{
                        sERRZipCode = Globalization.GetString("Common.Constants.ErrZipCode", m_iClientId);
						sERRZipCode = sERRZipCode + GetZipErrorMessage(p_objFeeSchedule, p_objRequest,p_sStudyId);
						m_objCommon.AddAlert(sERRZipCode);
						//Exit Function
						return dReturnValue;
					}
				}
				
				//Get relative value object for studyid/state
				hstRelativeValue = GetRelativeValuesWComp(p_objFeeSchedule,p_objRequest,p_sStudyId,sStateId,sMods); 
                //abisht 4/16/2008 MITS 12005.To retrieve the selected amount from the spec code form.
                string sSpecCodeAmount = string.Empty;
                string sSpecialityCode = string.Empty;
                //MITS 12139  : Umesh
                //To retrieve the speciality code
                sSpecCodeAmount = p_objRequest.BillItemObject.SpecialtyCodeFactor;
                sSpecialityCode = p_objRequest.BillItemObject.SpecialtyCode;

				if(hstRelativeValue.Count == 0)
				{
                    m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrCpt", m_iClientId));
					//Exit Function
					return dReturnValue;
				}
                //abisht MITS 12005.Changed the If condition.No need to go into this condition if there is 
                //some amount in the objCodeAmountNode which gets filled only when the user selects
                //from the choices given to him.
                //Need to execute the last else condiotion in this case.
                else if (hstRelativeValue.Count > 1 && sSpecCodeAmount == "")
				{
					//User needs to choose a specialty.
					//This might be second pass; if it is, find specialty that 
					//user chose and choose that relative value object
					
					//Param is requested from client code by adding "SPECIALTYSHORTCODE" 
					//to MissingParams collection in  a previous calculation attempt.

                    //abisht MITS 12005.This part of code was missing in RMX.
                    //Adding "SPECIALTYSHORTCODE" to the missingparams collection was not giving the desired result.
                    //If more than two relative values are obtained then user will chose that relative value object.
                    //GetSpecialCodesAmount
                    // if more than one Specialty or UniqueSub was found get input from user
                    // This function will return the amount for all the options to the user
                    // from which the user will chose the amount he wants to use.
                    GetSpecialCodesAmount(ref hstRelativeValue, p_objRequest);
					sSpecialtyShortCode = Conversion.ConvertObjToStr(p_objRequest.GetParam("SPECIALTYSHORTCODE"));
					if(sSpecialtyShortCode != "")
					{
						foreach(RelativeValues.RelativeValue objTemp in hstRelativeValue)
						{
							if(objTemp.m_sSpecialty.ToLower() == sSpecialtyShortCode.ToLower())
							{
								bTemp = true;
								break;
							}//end if
						}//end for
						if (!bTemp)
							throw new RMAppException("Unable to locate user-selected specialty code, " + sSpecialtyShortCode);
					}//end if(sSpecialtyShortCode != "")
					else
					{
                        //abisht MITS 12005.If the remaining part of the code is implemented in RMX as per RMWorld
                        //Then if relative values objects are removed and the count comes out to be 1 then no use to
                        //ask for the options from the user.
                        if (hstRelativeValue.Count == 1)
                        {
                            objRelativeValue = new RelativeValues.RelativeValue();
                            foreach (RelativeValues.RelativeValue objTemp in hstRelativeValue.Values)
                            {
                                objRelativeValue = objTemp;
                            }
                            p_objRequest.BillItemObject.SpecialtyCodeFactor = string.Empty;
                        }
                        else
                        {
						p_objRequest.AddMissingParam("SPECIALTYSHORTCODE");
						return dReturnValue;
                        }
					}
				}//if(hstRelativeValue.Count > 1)
				else
                {
                    //srajindersin MITS 36730 dt 7/14/2014
					objRelativeValue = (RelativeValues.RelativeValue)hstRelativeValue[0];
                    //abisht MITS 12005.This code will executed during the second pass
                    //when the user has entered his choice from the speciality code screen.
                    //Choose the appropriate Object from the hashtable and then use it for 
                    //further calculation.
                    if (hstRelativeValue.Count > 1 && sSpecCodeAmount != "")
                    {
                        foreach (RelativeValues.RelativeValue objTemp in hstRelativeValue.Values)
                        {
                            if (objTemp.m_dAmount.ToString() == sSpecCodeAmount && objTemp.m_sSpecialty.Trim() == sSpecialityCode.Trim())
                            {
                                objRelativeValue = objTemp;
                            }
                        }
                        p_objRequest.BillItemObject.SpecialtyCodeFactor = string.Empty;
                    }
                }

				hstRelativeValue = null;
				if (objRelativeValue.m_iFollup > 0)
					p_objRequest.AddParam(objRelativeValue.m_iFollup + " Days", "FOLLOWUPMESSAGE");

				//get factors collection
				//Fee Flag F = Fee, R = Relative (Factor), U = UCR;  Type of Service 7 = Anesthesia
				dFactor = 1;

                //abisht MITS 11156
                //commmented the code below as because of the incorrect format(Input string was not in a correct format.)
                //int tos = int.Parse(m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode));

                string sTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);
                if (sFeeFlag.ToUpper() == "R" || sTos.Trim() == (BillItem.ANESTHESIA_SHORT_CODE).ToString())
				{
					hstFactors = GetFactorsWC1999(p_objFeeSchedule,p_objRequest,p_sStudyId,sStateId,
						objRelativeValue.m_sSpecialty,sMods);
					if(hstFactors.Count > 0)
						dFactor = ((RelativeValues.RelativeValue)hstFactors[hstFactors.Count]).m_dFactor;

					hstFactors = null;
				}//end if(sFeeFlag.ToUpper() == "R" || p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode == BillItem.ANESTHESIA_SHORT_CODE)

				//Display Subcode sensitive messages!!!
				HandleSubCodeWComp(p_objRequest,objRelativeValue.m_sSubCode);

				//handle special modifiers like 51A,51B and 51C for NV
				dMaxRlv = -1;
				dTemp = -1;
				dTotMod = 1;
				if(objDSList.Count > 0)
                    p_objFeeSchedule.GetMaxRLVAndModValue(Conversion.ConvertObjToInt(objDSList[0], m_iClientId), p_objRequest.CPT, ref dModValue, ref dTemp);

				if(dMaxRlv < dTemp)
					dMaxRlv = dTemp;
				
				if(dMaxRlv >= 0 && dMaxRlv < objRelativeValue.m_dAmount)
					objRelativeValue.m_dAmount = dMaxRlv;
				
				dReturnValue = objRelativeValue.m_dAmount * dFactor;

				//Multiply amount by Units
				dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
			    if(sTos.Trim() == (BillItem.ANESTHESIA_SHORT_CODE).ToString())
				{
					if(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType == (int)Common.UnitsBilledType.ubtMINUTES)
						dUnitsBilled = dUnitsBilled / 15;
					dReturnValue = dReturnValue + dUnitsBilled * dFactor;
				}
				else
					dReturnValue = dReturnValue * dUnitsBilled;

				//Apply modifier if specified
				dMaxRlv = 0;
				dTotMod = 1;
				bTemp = false;

				if(objDSList.Count > 0)
				{
					//fill the array of ModCodes with the modifier percents; 
					//if the percent does not exist set dPercent to -1
					GetModifierPercentsWComp(p_objFeeSchedule, p_objRequest.CPT,p_sStudyId, sStateId);

					//Apply modifier percent if specified
					if(m_structModCode.dPercent > -1)
						dTotMod = dTotMod * m_structModCode.dPercent;
					else
						dTotMod = dTotMod * dModValue;

					dReturnValue = dReturnValue * dTotMod;
				}//end if(objDSList.Count > 0)

				//limit the schd amount by max allowed for the group
				if(dMaxSchdAmount != -1 && dReturnValue > dMaxSchdAmount)
					dReturnValue = dMaxSchdAmount;
			}//end try
			catch (RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("CalculateWC.CalcWorkComp.GenericErr", m_iClientId), p_objException);
			}
			finally
			{
				if (objDataSet != null)
					objDataSet.Dispose();
				objDataSet = null;

				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}
				objDbReader = null;

				sbSQL = null;
				objDSList = null;
				objRelativeValue = null;
			}

			//Return value to the calling function
			return dReturnValue;

		}//end function

		#endregion

		#region internal CalcWC2000(p_objFeeSchedule,p_objRequest,ref p_sStudyId,ref p_sStateCode, ref p_sRevision)
		/// Name			: CalcWC2000
		/// Author			: Navneet Sota
		/// Date Created	: 01/11/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
        /// abisht 4/16/2008 MITS 12005.Changed the signature of the function.
		/// <summary>
		/// Calculates the Workers Compensation 2000
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Object</param>
		/// <param name="p_objRequest">Request Object</param>
		/// <param name="p_sStateCode">State Code</param>
		/// <param name="p_sStudyId">Study-Id</param>
		/// <param name="p_sRevision">Revision</param>
		internal double CalcWC2000(FeeSchedule p_objFeeSchedule, Request p_objRequest, 
			ref string p_sStudyId, ref string p_sStateCode, ref string p_sRevision)
		{        
			//int variables
			int iModUnits = 0;
			int iFeeSchedule = 0;
			int iSpecialityCode99ID = 0;
			int iCodeNextId = 0;
			int iCnt = 0;
			int iTemp = 0;
            
			//double variables
			double dReturnValue = -1;
			double dMaxSchdAmount = -1;
			double dFactor = 0.0;
			double dUnitsBilled = 0.0;
			double dBaseSchdAmount = 0.0;
			double dTemp = 0.0;
			double dMaxRlv = 0.0;
			double dTotMod = 0.0;
			
			//string variables
            //nadim for 14687.
            string sLicenseTemp = "";
            //nadim for 14687
			string sMods = "";
			string sTemp = "";
			string sStateId = "";
			string sShortCode = "";
			string sCodeDesc = "";
			string sEffDate = "";
			string sFeeFlag = "";
			string sBaseDesc = "";
			string sSpecialtyShortCode = "";
			string sError = "";
			string sERRZipCode = "";

			//bool variables
			bool bAnesthesia = false;
			bool bTemp = false;

			//Collections
			ModCode[] arrModCodes = null;
			DataModel.DataSimpleList objDSList = null;
			Hashtable hstRelativeValue = null;
			Hashtable hstFactors = null;

			//objects
			StringBuilder sbSQL = null;
			RelativeValues.RelativeValue objRelativeValue = null;
			Riskmaster.Settings.CCacheFunctions objCacheSettings = null;
			DataSet objDataSet=null;
			DbCommand objCommand = null;
			DbConnection objConn = null;
			DbReader objDbReader = null;
			try
			{
				iFeeSchedule = p_objFeeSchedule.FeeScheduleId;
                
				objDSList = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList;
			
				//TODO: Can be removed
				objCacheSettings = new Riskmaster.Settings.CCacheFunctions(m_objCommon.g_sConnectionString, m_iClientId); 

				if(m_objLocalCache == null)
					m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString,m_iClientId);
				m_objLocalCache.GetStateInfo(p_objFeeSchedule.StateId, ref p_sStateCode, ref sCodeDesc);
				p_sStateCode = m_objLocalCache.GetStateCode(p_objFeeSchedule.StateId);

				sbSQL = new StringBuilder();
			    //Florida Only add speciality code 99. This code is used in 2004 Fee schedule
				//If a Speciality code 99 is not found, then add it to the database.
				if(p_sStateCode.ToUpper() == "FL")
				{
					//TODO: Function to be added in LocalCache
					iSpecialityCode99ID = objCacheSettings.GetCodeIDWithShort("99","SPECIALTY_CODES");
					if(iSpecialityCode99ID == 0)
					{
                        iCodeNextId = Utilities.GetNextUID(m_objCommon.g_sConnectionString, "CODES", m_iClientId);

						sbSQL.Append("INSERT INTO CODES (CODE_ID,TABLE_ID,SHORT_CODE,LINE_OF_BUS_CODE,IND_STANDARD_CODE,DELETED_FLAG,RELATED_CODE_ID)");
						sbSQL.Append(" VALUES (").Append(iCodeNextId).Append(",").Append(m_objLocalCache.GetTableId("SPECIALTY_CODES"));
						sbSQL.Append(",").Append("'99',0,0,0,0)");

						objConn = DbFactory.GetDbConnection(m_objCommon.g_sConnectionString);
						objConn.Open();
						
						objCommand = objConn.CreateCommand();
						objCommand.CommandText=sbSQL.ToString();
						objCommand.ExecuteNonQuery();

						sbSQL = new StringBuilder();
						sbSQL.Append(" INSERT INTO CODES_TEXT(CODE_ID,CODE_DESC,SHORT_CODE,LANGUAGE_CODE)");
						sbSQL.Append(" VALUES (").Append(iCodeNextId).Append(",'Maximum Allowable Reimbursement','99','1033')");
						
						objCommand = objConn.CreateCommand();
						objCommand.CommandText=sbSQL.ToString();
						objCommand.ExecuteNonQuery();
						// Execute the Update transaction.
						
					}//end if(iSpecialityCode99ID == 0)
				}//end if(p_sStateCode.ToUpper() == "FL")
				//Florida Only add speciality code 99. This code is used in 2004 Fee schedule

				sbSQL.Remove(0,sbSQL.Length);
				//Table[0]
				sbSQL.Append(" SELECT FEEFLAG,EFFDATE,STUDYID,PRODCODE ");
				sbSQL.Append(" FROM WRKMAS ");
				sbSQL.Append(" WHERE STATE = '").Append(p_sStateCode).Append("'");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						sFeeFlag = Conversion.ConvertObjToStr(objDbReader.GetValue("FEEFLAG"));
						sEffDate = Conversion.ConvertObjToStr(objDbReader.GetValue("EFFDATE"));
						p_sStudyId = Conversion.ConvertObjToStr(objDbReader.GetValue("STUDYID"));
						p_sRevision = Conversion.ConvertObjToStr(objDbReader.GetValue("PRODCODE"));
					}
					else
					{
                        m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrState", m_iClientId));
						//Exit Function
						return dReturnValue;
					}
				}
				
				sbSQL.Remove(0,sbSQL.Length);
				sbSQL.Append(" SELECT STATEID FROM WRKZIP WHERE STUDYID = '").Append(p_sStudyId).Append("' ");
				sbSQL.Append(" AND '").Append(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ZipCode);
				sbSQL.Append("' BETWEEN BEGZIP AND ENDZIP ");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						sStateId = objDbReader.GetValue("STATEID").ToString();
					}
					else
					{
                        sERRZipCode = Globalization.GetString("Common.Constants.ErrZipCode", m_iClientId);
						sERRZipCode = sERRZipCode + GetZipErrorMessage(p_objFeeSchedule, p_objRequest,p_sStudyId);
						m_objCommon.AddAlert(sERRZipCode);
						//Exit Function
						return dReturnValue;
					}
				}

				//loop through modifier codes collection:
				//	1.  create array of ModCodes for use below
				//	2.  create the sMods list of short codes used in SQL statements
				//	3.  calculate NV dblMaxSchdAmount
				if(objDSList.Count > 0)
				{
					arrModCodes = new ModCode[objDSList.Count];
					iCnt=0;
					foreach(DictionaryEntry objEntry in p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList)
					{
						
						m_objLocalCache.GetCodeInfo(Conversion.ConvertObjToInt(objEntry.Value, m_iClientId), ref sShortCode, ref sCodeDesc);
						sMods = sMods + ", '" + sShortCode + "'";
						
						arrModCodes[iCnt].iModCode = Conversion.ConvertObjToInt(objEntry.Value, m_iClientId);
						arrModCodes[iCnt].sMod = sShortCode;
						//Default value, over-write in GetModifierPercentsWC2000()
						m_structModCode.dPercent = -1;
						if(p_sStateCode.ToUpper() == "NV" && sShortCode.ToUpper() == "B")
							dMaxSchdAmount = p_objFeeSchedule.CalcASCAmount(p_objRequest);

						iCnt++;
					}
					if( sMods.Length>=3)
					sMods = sMods.Substring(2);
				}

				//Get relative value object for studyid/state
				hstRelativeValue = GetRelativeValuesWC2000(p_objFeeSchedule,p_objRequest,p_sStudyId,p_sStateCode,sStateId,sMods,ref sError);
                //abisht 4/16/2008 MITS 12005.To retrieve the selected amount from the spec code form.
                string sSpecCodeAmount = string.Empty;
                string sSpecialityCode = string.Empty;
                //MITS 12139  : Umesh
                //To retrieve the speciality code
                sSpecCodeAmount = p_objRequest.BillItemObject.SpecialtyCodeFactor;
                sSpecialityCode = p_objRequest.BillItemObject.SpecialtyCode;
                
				if(hstRelativeValue.Count == 0 && sError != "")
				{
					m_objCommon.AddAlert(sError);
					//Exit Function
					return dReturnValue;
				}
                //abisht MITS 12005.Changed the If condition.No need to go into this condition if there is 
                //some amount in the objCodeAmountNode which gets filled only when the user selects
                //from the choices given to him.
                //Need to execute the last else condiotion in this case.
                else if (hstRelativeValue.Count > 1 && sSpecCodeAmount == "")
				{
					//User needs to choose a specialty.
					//This might be second pass; if it is, find specialty that 
					//user chose and choose that relative value object
					
					//Param is requested from client code by adding "SPECIALTYSHORTCODE" 
					//to MissingParams collection in  a previous calculation attempt.
                    //abisht MITS 12005.This part of code was missing in RMX.
                    //Adding "SPECIALTYSHORTCODE" to the missingparams collection was not giving the desired result.
                    //If more than two relative values are obtained then user will chose that relative value object.
                    //GetSpecialCodesAmount
                    // if more than one Specialty or UniqueSub was found get input from user
                    // This function will return the amount for all the options to the user
                    // from which the user will chose the amount he wants to use.
                    GetSpecialCodesAmount(ref hstRelativeValue, p_objRequest);
					sSpecialtyShortCode = Conversion.ConvertObjToStr(p_objRequest.GetParam("SPECIALTYSHORTCODE"));
					if(sSpecialtyShortCode != "")
					{
						foreach(RelativeValues.RelativeValue objTemp in hstRelativeValue.Values)
						{
							if(objTemp.m_sSpecialty.ToLower() == sSpecialtyShortCode.ToLower())
							{
								bTemp = true;
								break;
							}//end if
						}//end for
						if (!bTemp)
							throw new RMAppException("Unable to locate user-selected specialty code, " + sSpecialtyShortCode);
					}//end if(sSpecialtyShortCode != "")
					else
					{
                        //abisht MITS 12005.If the remaining part of the code is implemented in RMX as per RMWorld
                        //Then if relative values objects are removed and the count comes out to be 1 then no use to
                        //ask for the options from the user.
                        if (hstRelativeValue.Count == 1)
                        {
                            objRelativeValue = new RelativeValues.RelativeValue();
                            foreach (RelativeValues.RelativeValue objTemp in hstRelativeValue.Values)
                            {
                                objRelativeValue = objTemp;
                            }
                            p_objRequest.BillItemObject.SpecialtyCodeFactor = string.Empty;
                        }
                        else
                        {
                            p_objRequest.AddMissingParam("SPECIALTYSHORTCODE");
                            return dReturnValue;
                        }
					}
				}//if(hstRelativeValue.Count > 1)
				else
				{
					objRelativeValue = new RelativeValues.RelativeValue();
                    //abisht MITS 12005.This code will executed during the second pass
                    //when the user has entered his choice from the speciality code screen.
                    //Choose the appropriate Object from the hashtable and then use it for 
                    //further calculation.
					foreach(RelativeValues.RelativeValue objTemp in hstRelativeValue.Values)
					{
						objRelativeValue=objTemp;
					}
				//	objRelativeValue = (RelativeValues.RelativeValue)hstRelativeValue[iLatestCount];
                    if (hstRelativeValue.Count > 1 && sSpecCodeAmount != "")
                    {
                        foreach (RelativeValues.RelativeValue objTemp in hstRelativeValue.Values)
                        {   //MITS 12139  : Umesh
                            if (objTemp.m_dAmount.ToString() == sSpecCodeAmount && objTemp.m_sSpecialty.Trim() == sSpecialityCode.Trim())
                            {
                                objRelativeValue = objTemp;
                            }
                        }
                        p_objRequest.BillItemObject.SpecialtyCodeFactor = string.Empty;
                    }
				}					

				//Single RelativeValue object is now set, can get rid of collection
				hstRelativeValue = null;
                //abisht MITS 11156
                //commmented the code below as because of the incorrect format(Input string was not in a correct format.)
                //int tos = int.Parse(m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode));

                string sTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);
                //nadim for 14687..there is no need to check with m_iTypeOfService..just check it with m_sTOS as done in R m World..

                //if(sTos.Trim() == (BillItem.ANESTHESIA_SHORT_CODE).ToString()
                //    && objRelativeValue.m_iTypeOfService == BillItem.ANESTHESIA_SHORT_CODE)  //TODO this appears to make no sense, the m_iTypeOfService property is never set
                if (sTos.Trim() == (BillItem.ANESTHESIA_SHORT_CODE).ToString()
                    && objRelativeValue.m_sTOS == BillItem.ANESTHESIA_SHORT_CODE.ToString())  //TODO this appears to make no sense,
                    bAnesthesia = true;              

                    //nadim for 14687
				if (objRelativeValue.m_iFollup > 0 || 
					Conversion.ConvertStrToInteger(objRelativeValue.m_sGlblCode) > 0)
				{
					sTemp = "Follow Up For ";
					if(objRelativeValue.m_iFollup > 0)
						sTemp = sTemp + objRelativeValue.m_iFollup  + " Days";
					else
						sTemp = sTemp + objRelativeValue.m_sGlblCode + " Medcr";
					
					p_objRequest.BillItemObject.FollowUpMessage = sTemp;
				}//end if
				
				//p_objRequest.AddParam(objRelativeValue.m_iFollup + " Days", "FOLLOWUPMESSAGE");
				//get record from WRKFCT (dFactor) to match the record selected from WRKUVA (objRelativeValue)
				//get factors collection
				dFactor = 1;
				hstFactors = GetFactorsWC2000(p_objFeeSchedule,p_objRequest,p_sStudyId,sStateId,
								  			  objRelativeValue.m_sSpecialty,sMods);
                if(hstFactors.Count > 0)
				{
					if(bAnesthesia)
						dFactor = ((RelativeValues.RelativeValue)hstFactors[0]).m_dFactor;
					else
				    {
					    bTemp = false;
					    //foreach(RelativeValues.RelativeValue objTemp in hstFactors)
				        foreach(RelativeValues.RelativeValue objTemp in hstFactors.Values)
		                {
			                if(objTemp.m_sUniqueSub.ToLower() == objRelativeValue.m_sUniqueSub.ToLower())
			                {
				                dFactor = objTemp.m_dFactor;
				                bTemp = true;
				                break;
			                }//end if
		                }//end for
					    if(!bTemp)
					    {
                            m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrCpt", m_iClientId));
						    return dReturnValue;
					    }
				    }//end else
				}
                
				hstFactors = null;

				if(bAnesthesia)
				{
					SetBilledUnitMin(p_objRequest,0);
                    //nadim for 14687..Implemented ..because calculation was not like RM World..and this loop was never executed before 
                    if (p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledType == (int)Common.UnitsBilledType.ubtMINUTES)
                    {
                        if (p_sStateCode.ToUpper() == "FL")
                        {
                            sLicenseTemp = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.FlLicense;
                            if (sLicenseTemp.Length >= 2)
                            {
                                if (sLicenseTemp.Substring(0, 2).ToUpper() == "ME")
                                {
                                    dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum / 10;
                                }
                                else
                                {
                                    dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum / 15;
                                }
                            }
                            else
                            {
                                dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum / 10;
                            }

                        }
                        else
                        {
                            dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum / 15;
                        }

                    }
                    else
                    {
                        dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
                    }

                    //arrModCodes = new ModCode[objDSList.Count];
                    //nadim for 14687
					//Apply modifier unit if specified
                    if (objDSList.Count > 0)
						iModUnits = GetModUnitsWC2000(p_objFeeSchedule, p_objRequest.CPT, p_sStudyId, sStateId, sMods);
					else
						iModUnits = 0;
					
					dReturnValue = (objRelativeValue.m_dAmount + iModUnits + dUnitsBilled) * dFactor;
				}//end if(bAnesthesia)
				else if(sTos.Trim() == (BillItem.ANESTHESIA_SHORT_CODE).ToString())
				{
					SetBilledUnitMin(p_objRequest, 1);
                    dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
				}
				else
				{
					SetBilledUnitMin(p_objRequest, 1);
					dUnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
				}//end

				if(!bAnesthesia)
				{
					switch (objRelativeValue.m_sUniqueSub.ToUpper())
					{
						case "":
							sBaseDesc = objRelativeValue.m_sDesc;
							dBaseSchdAmount = objRelativeValue.m_dAmount * dFactor * dUnitsBilled;
							dReturnValue = dBaseSchdAmount;
							break;
                        case "S":
                            sBaseDesc= objRelativeValue.m_sDesc;
                            dBaseSchdAmount = objRelativeValue.m_dAmount *dFactor *dUnitsBilled;
                            dReturnValue=dBaseSchdAmount;
                            break;
						case "A":
						case "M":
						case"T":
							if(p_sStateCode.ToUpper() == "HI" && objRelativeValue.m_sUniqueSub.ToUpper() == "M")
							{
                                m_objCommon.AddAlert(Globalization.GetString("CalculateWC.CalcWC2000.UniqueSubT", m_iClientId));
								return dReturnValue;
							}
							dReturnValue = dBaseSchdAmount + (objRelativeValue.m_dAmount * (dUnitsBilled - 1) * dFactor);
							break;
						case "E":
						case "O":
						case"P":
						case"WR":
							if(Conversion.ConvertObjToInt(p_objRequest.GetParam("PROCEDURECASE"), m_iClientId) == 0)
							{
								p_objRequest.AddMissingParam("PROCEDURECASE");
								//Exit Function
								return dReturnValue;
							}
							else if(Conversion.ConvertObjToInt(p_objRequest.GetParam("PROCEDURECASE"), m_iClientId) == 1)
							{
								dReturnValue = dBaseSchdAmount * dUnitsBilled ;
								p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.BillingCodeText = 
										p_objRequest.CPT + " - " + sBaseDesc;
							}
							else
							{
								dReturnValue = objRelativeValue.m_dAmount * dUnitsBilled * dFactor;
								p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.BillingCodeText = 
										p_objRequest.CPT + " - " + objRelativeValue.m_sDesc;
							}//end if(Conversion.ConvertObjToInt(p_objRequest.GetParam("PROCEDURECASE"), m_iClientId) == 0)
							break;
					}//end switch
				}//end if(!bAnesthesia)

				//Display Subcode sensitive messages!!!
				HandleSubCodeWC2000(p_objRequest,objRelativeValue,p_sStateCode);

				//handle special modifiers like 51A,51B and 51C for NV
				dMaxRlv = -1;
				dTemp = -1;
				dTotMod = 1;
				if(objDSList.Count > 0)
				{
					//fill the array of ModCodes with the modifier percents from WRKMOD; 
					//if the percent does not exist dblPercent is left as -1
					//arrModCodes & sMods are filled from the modifier codes collection, 
					//these 3 variables are necessarily in sync
					GetModifierPercentsWC2000(p_objFeeSchedule,p_objRequest.CPT, ref arrModCodes, p_sStudyId, sStateId,sMods);
					iCnt=0;
					foreach(DictionaryEntry objEntry in p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList)
					{	
						for(iTemp=0; iTemp<=arrModCodes.Length - 1; iTemp++)
						{
                            if (arrModCodes[iTemp].iModCode == Conversion.ConvertObjToInt(objEntry.Value, m_iClientId))
								break;
						}//for(int iTemp=0; iTemp<arrModCodes.Length - 1; iTemp++)

                        //If the Modfier code not found in the fee schedule for the cpt, skip the calculation
                        if (iTemp == arrModCodes.Length)
                        {
                            m_objLocalCache.GetCodeInfo(Conversion.ConvertObjToInt(objEntry.Value, m_iClientId), ref sShortCode, ref sCodeDesc);
                            m_objCommon.AddAlert("Modifier code " + sShortCode + " value not found for CPT " + p_objRequest.CPT + " and will not reduce the amount.");
                            continue;
                        }
						p_objFeeSchedule.GetMaxRLVAndModValue(Conversion.ConvertObjToInt(objEntry.Value, m_iClientId), p_objRequest.CPT, ref arrModCodes[iTemp].dPercentMax, ref dTemp);

						if(dMaxRlv < dTemp)
							dMaxRlv = dTemp;

						sbSQL.Remove(0,sbSQL.Length);
                        sbSQL.Append("SELECT PERCENT,MODIFIER,BEGPROC,ENDPROC FROM WRKMOD WHERE");
						sbSQL.Append(" STUDYID = '" + p_sStudyId.ToString() +"'");
                        sbSQL.Append(" AND STATEID = '" + sStateId+"'");
						sbSQL.Append(" AND MODIFIER = '" + arrModCodes[iTemp].sMod+"'");
                        sbSQL.Append(" AND (UNIT IS NULL OR UNIT='')");
						sbSQL.Append(" ORDER BY MODIFIER");
                        
						if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
						{
							string sTempBEGCPT ="";
							string sTempENDCPT ="";
							if(objDbReader.Read())
							{
								sTempBEGCPT = objDbReader.GetValue("BEGPROC").ToString() ;
								sTempENDCPT = objDbReader.GetValue("ENDPROC").ToString() ;
								//p_objRequest.CPT
								if(arrModCodes[iTemp].sMod.ToLower().Trim() == objDbReader.GetValue("MODIFIER").ToString().ToLower().Trim())
								{
									if(sTempBEGCPT != "")
									{
										if ((m_objCommon.Val(p_objRequest.CPT) >= m_objCommon.Val(sTempBEGCPT)) && 
											(m_objCommon.Val(p_objRequest.CPT) <= m_objCommon.Val(sTempENDCPT))) 
										{
											dTemp = 1;
										}
									}
								}

                               if(arrModCodes[iTemp].dPercent > 0)
						            dTotMod = dTotMod * arrModCodes[iTemp].dPercent;
								else if(arrModCodes[iTemp].dPercentMax >0)
									dTotMod = dTotMod * arrModCodes[iTemp].dPercentMax;
							}
							else
							{
								if(dMaxRlv<0)
								{
									dTemp = -1;
								}
								if(arrModCodes[iTemp].dPercentMax >0)
						     	dTotMod = dTotMod * arrModCodes[iTemp].dPercentMax;
							}
						}
						
						//If the max rlv for this mod code not found add an alert
						if (dTemp == -1)
							m_objCommon.AddAlert("Modifier code " + arrModCodes[iTemp].sMod + " value not found for CPT " + p_objRequest.CPT + " and will not reduce the amount.");
                        
						dTemp = -1;
					}//for(iCnt = 0 ; arrModCodes.Length - 1; iCnt++)
				
					dReturnValue = dReturnValue * dTotMod;

				}//end if(objDSList.Count > 0)

				if(dMaxSchdAmount != -1 && dReturnValue > dMaxSchdAmount)
					dReturnValue = dMaxSchdAmount;
			}//end try
			catch (RMAppException p_objRmAEx)
			{
				throw new RMAppException(p_objRmAEx.Message, p_objRmAEx);
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("CalcWC.CalcWC2000.GenericErr", m_iClientId), p_objException);
			}
			finally
			{
				if(objConn != null)
					objConn.Dispose();
				objConn = null;

				if (objDataSet != null)
					objDataSet.Dispose();
				objDataSet = null;

				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}
				objDbReader = null;

				sbSQL = null;
				objCacheSettings = null;
				objDSList = null;
				objRelativeValue = null;
			}

			//Return value to the calling function
			return dReturnValue;

		}//end function

		#endregion

		#endregion

		#region Private Methods
		
        #region GetSpecialCodesAmount(p_objFeeSchedule,p_objRequest,p_sStudyId)
        /// Name			: GetSpecialCodesAmount
        /// Author			: Alok Singh Bisht
        /// Date Created	: 4/2/2008		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// if more than one Specialty or UniqueSub was found get input from user
        /// This function will return the amount from which the user will chose the 
        /// amount he wants to use.
        /// </summary>
        /// <param name="p_hstRelativeValue">Hash table having relative value objects.</param>
        /// <param name="p_objRequest">Request object</param>
        private void GetSpecialCodesAmount(ref Hashtable p_hstRelativeValue, Request p_objRequest)
        {
            RelativeValues.RelativeValue objRlv = null;
            const string TABLE_NAME = "SPECIALTY_CODES";
            StringBuilder sbSpecialtyCodes = new StringBuilder();
            string sDelimiter = "$%#";

            try
            {
                for (int iCountTemp = 0; iCountTemp < p_hstRelativeValue.Count; iCountTemp++)
                {
                    //string sDescription = string.Empty;
                    double dAmount = 0.0;
                    objRlv = (RelativeValues.RelativeValue)p_hstRelativeValue[iCountTemp];
                    //abisht If condition removed as per Aashish's recommendation
                    //this logic is as per RMWorld which is not as it is merged in RMX for the time being
                    //TODO - to review the logic in RMWorld and then implement it in RMX.
                    //if (bFirst || (sPrevSpec != objRlv.m_sSpecialty))
                    //{
                    if (sbSpecialtyCodes.Length > 0)
                    {
                        sbSpecialtyCodes.Append(sDelimiter);
                    }
                    dAmount = objRlv.m_dAmount;
                    if (objRlv.m_sSpecialty == "")
                    {
                        sbSpecialtyCodes.Append(dAmount.ToString() + " ---> " + " No Specialty");
                    }
                    else
                    {
                        int iCodeID = m_objLocalCache.GetCodeId(objRlv.m_sSpecialty, TABLE_NAME);
                        sbSpecialtyCodes.Append(dAmount.ToString() + " ---> " + objRlv.m_sSpecialty + " " + m_objLocalCache.GetCodeDesc(iCodeID));
                    }
                }

                if (sbSpecialtyCodes.Length > 0)
                {
                    p_objRequest.BillItemObject.SpecialtyCodeList = sbSpecialtyCodes.ToString();
                }
            }//end try
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            finally
            {
                objRlv = null;
            }
            //Return the result to calling function
        }//end function

        #endregion
		#region GetZipErrorMessage(p_objFeeSchedule,p_objRequest,p_sStudyId)
		/// Name			: GetZipErrorMessage
		/// Author			: Navneet Sota
		/// Date Created	: 1/7/2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the error message based on zip code
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule object</param>
		/// <param name="p_objRequest">Request class object</param>
		/// <param name="p_sStudyId">Study Id</param>
		private string GetZipErrorMessage(FeeSchedule p_objFeeSchedule, Request p_objRequest, string p_sStudyId)
		{
			//string Variables
			string sReturnValue = "";
			string sSqlStudy = "";

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			try
			{
				if((int)p_objFeeSchedule.ScheduleType == Common.WC00)
					sSqlStudy = "STUDYID";
				else
					sSqlStudy = "STUDY";
			
				sbSQL = new StringBuilder();

				//Table[0]
				sbSQL.Append(" SELECT MIN(BEGZIP) AS MINZIP, MAX(ENDZIP) AS MAXZIP ");
				sbSQL.Append(" FROM WRKZIP WHERE ").Append(sSqlStudy).Append(" = '").Append(p_sStudyId).Append("'");

				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					if(objDbReader.Read())
					{
						sReturnValue = "(Valid Zip Codes From ";
						sReturnValue = sReturnValue + (objDbReader.GetValue("MINZIP").ToString());
						sReturnValue = sReturnValue + " To " +(objDbReader.GetValue("MAXZIP").ToString());
						sReturnValue = sReturnValue + ").";
					}
				}

				objDbReader = null;

				sbSQL.Remove(0,sbSQL.Length);
				sbSQL.Append(" SELECT Count(*) FROM PROVIDER_CONTRACTS ");
				sbSQL.Append(" WHERE PROVIDER_EID = ").Append(p_objRequest.BillItemObject.PayeeId);

				if((p_objRequest.Common.AnyContractExits(p_objRequest.BillItemObject.PayeeId)) && p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ContractExists > 0)
                    sReturnValue = sReturnValue + Globalization.GetString("CalculateWC.GetZipErrorMessage.Alert1", m_iClientId);
			}//end try
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			finally
			{
				sbSQL = null;
				if(objDbReader != null)
				{
                    objDbReader.Close();
					objDbReader.Dispose();				
					objDbReader = null;
				}
			}
			//Return the result to calling function
			return sReturnValue;
		}//end function

		#endregion

		#region GetModUnitsWC2000(p_objFeeSchedule, p_sCPT, p_sStudyId, p_sStateId, p_sMode)
		/// Name			: GetModUnitsWC2000
		/// Author			: Navneet Sota
		/// Date Created	: 1/7/2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the Modifier Unit
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule object</param>
		/// <param name="p_sCPT">CPT</param>
		/// <param name="p_sMode">Mode</param>
		/// <param name="p_sStateId">State-Id</param>
		/// <param name="p_sStudyId">Study-Id</param>
		private int GetModUnitsWC2000(FeeSchedule p_objFeeSchedule,string p_sCPT,string p_sStudyId, 
										string p_sStateId,string p_sMode)
		{
			//Return Variable
			int iReturnValue = 0;
			
			//string variables
			string sTempBEGCPT = "";
			string sTempENDCPT = "";
			string sMod = "";

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;

            try
            {
                sbSQL = new StringBuilder();

                //Table[0]
                sbSQL.Append(" SELECT UNIT,MODIFIER,BEGPROC,ENDPROC FROM WRKMOD WHERE ");
                sbSQL.Append(" STUDYID = '").Append(p_sStudyId).Append("'");
                sbSQL.Append(" AND STATEID = '").Append(p_sStateId).Append("'");
                sbSQL.Append(" AND MODIFIER IN (").Append(p_sMode).Append(")");
                sbSQL.Append(" ORDER BY MODIFIER ");

                if (m_objCommon.GetDbReader(ref objDbReader, sbSQL.ToString()))
                {
                    while (objDbReader.Read())
                    {
                        sTempBEGCPT = objDbReader.GetValue("BEGPROC").ToString();
                        sTempENDCPT = objDbReader.GetValue("ENDPROC").ToString();
                        if (sMod != objDbReader.GetValue("MODIFIER").ToString())
                        {
                            sMod = objDbReader.GetValue("MODIFIER").ToString();
                            if (sTempBEGCPT != "")
                            {
                                if ((m_objCommon.Val(p_sCPT) >= m_objCommon.Val(sTempBEGCPT)) &&
                                    (m_objCommon.Val(p_sCPT) <= m_objCommon.Val(sTempENDCPT)))
                                    iReturnValue = iReturnValue + Conversion.ConvertStrToInteger(objDbReader.GetValue("UNIT").ToString());
                            }//if(sTempBEGCPT != "")
                            else
                                iReturnValue = iReturnValue + Conversion.ConvertStrToInteger(objDbReader.GetValue("UNIT").ToString());
                        }//end if(sMod != objDbReader.GetValue("MODIFIER").ToString())
                    }
                }//end if(objDbReader != null)            
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                    objDbReader = null;
                }
            }
    			
			//Return the result to calling function
			return iReturnValue;
		}	
		#endregion

		#region SetBilledUnitMin(p_objRequest, p_iNewMinimum)
		/// Name			: SetBilledUnitMin
		/// Author			: Navneet Sota
		/// Date Created	: 1/7/2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Sets the Minimum Units Billed in request object
		/// </summary>
		/// <param name="p_objRequest">Request Object</param>
		/// <param name="p_iNewMinimum">New Minimum value</param>
		private void SetBilledUnitMin(Request p_objRequest,int p_iNewMinimum)
		{
			p_objRequest.UnitsBilledMin = p_iNewMinimum;
			if(p_iNewMinimum > 0)
				p_objRequest.UnitsBilledLabel = "Add'l Units Billed";
			else
				p_objRequest.UnitsBilledLabel = "Units Billed";

			if(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum <= 1)
				p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum = 
						p_objRequest.UnitsBilledMin;
		}//end function
		#endregion

		#region GetModifierPercentsWC2000(p_objFeeSchedule, p_sCPT,arrModCode,p_sStudyId, p_sStateId, p_sMode)
		/// Name			: GetModifierPercentsWC2000
		/// Author			: Navneet Sota
		/// Date Created	: 1/7/2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the Modifier Percentage
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule class object</param>
		/// <param name="p_sCPT">CPT</param>
		/// <param name="arrModCode">Mod Code Array</param>
		/// <param name="p_sMode">Mode</param>
		/// <param name="p_sStateId">State-Id</param>
		/// <param name="p_sStudyId">Study-Id</param>
		private void GetModifierPercentsWC2000(FeeSchedule p_objFeeSchedule,string p_sCPT, ref ModCode[] arrModCode,
			string p_sStudyId,string p_sStateId,string p_sMode)
		{
			int iCnt = 0;
			double dPercent = 0.0;

			//string variable
			string sTempBEGCPT = "";
			string sTempENDCPT = "";
			string sMod = "";
            string sPercent = "";

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;

            try
            {
			    sbSQL = new StringBuilder();

			    //Table[0]
			    sbSQL.Append(" SELECT PERCENT,MODIFIER,BEGPROC,ENDPROC FROM WRKMOD WHERE ");
			    sbSQL.Append(" STUDYID = '").Append(p_sStudyId).Append("'");
			    sbSQL.Append(" AND STATEID = '").Append(p_sStateId).Append("'");
			    sbSQL.Append(" AND MODIFIER IN (").Append(p_sMode).Append(")");
			    sbSQL.Append(" AND UNIT IS NULL ORDER BY MODIFIER ");

			    if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
			    {
				    while(objDbReader.Read())
				    {
					    dPercent = -1;
					    sTempBEGCPT = objDbReader.GetValue("BEGPROC").ToString();
					    sTempENDCPT = objDbReader.GetValue("ENDPROC").ToString();
					    sMod = objDbReader.GetValue("MODIFIER").ToString();
					    if(sTempBEGCPT != "")
					    {
						    if ((m_objCommon.Val(p_sCPT) >= m_objCommon.Val(sTempBEGCPT)) && 
							    (m_objCommon.Val(p_sCPT) <= m_objCommon.Val(sTempENDCPT))) 
                            {
                                //MITS 12764 : Umesh 
                                //dPercent = Conversion.ConvertStrToDouble(objDbReader.GetValue("PERCENT").ToString())/100;
                                sPercent = objDbReader.GetValue("PERCENT").ToString().Trim();
                                if (sPercent == "")
                                    dPercent = 1.00;
                                else
                                    dPercent = Conversion.ConvertStrToDouble(objDbReader.GetValue("PERCENT").ToString()) / 100;
                            }
					    }//if(sTempBEGCPT != "")
					    else
						    dPercent = Conversion.ConvertStrToDouble(objDbReader.GetValue("PERCENT").ToString())/100;

					    if(dPercent != -1)
					    {
						    for(iCnt = 0; iCnt <= arrModCode.Length - 1; iCnt++)
						    {
							    if(arrModCode[iCnt].sMod.Trim().ToLower() == sMod.Trim().ToLower())
								    arrModCode[iCnt].dPercent = dPercent;
						    }
					    }//end if(dPercent != -1)
				    }
			    }//end if(objDbReader != null)                
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                    objDbReader = null;
                }
            }
		}//end function
		#endregion

		#region GetModifierPercentsWComp(p_objFeeSchedule, p_sCPT, p_arrModCode, p_sStudyId, p_sStateId)
		/// Name			: GetModifierPercentsWComp
		/// Author			: Navneet Sota
		/// Date Created	: 1/7/2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the Modifier Percentage for Workers Compensation
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule Class object</param>
		/// <param name="p_sCPT">CPT</param>
		/// <param name="p_sStateId">State-Id</param>
		/// <param name="p_sStudyId">Study-Id</param>
		private void GetModifierPercentsWComp(FeeSchedule p_objFeeSchedule,string p_sCPT,
			string p_sStudyId,string p_sStateId)
		{
			//int iCnt = 0;
			double dPercent = 100;

			//string variables
			string sTempBEGCPT = "";
			string sTempENDCPT = "";

			//objects
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;

            try
            {
			    sbSQL = new StringBuilder();

			    //Table[0]
                //srajindersin MITS 36730 dt 7/14/2014
			    sbSQL.Append(" SELECT PERCENT,MODIFIER,BEGCPT,ENDCPT FROM WRKMOD WHERE ");
			    sbSQL.Append(" STUDYID = '").Append(p_sStudyId).Append("'");
			    sbSQL.Append(" AND STATEID = '").Append(p_sStateId).Append("'");
			    sbSQL.Append(" AND MODIFIER IN ('").Append(m_structModCode.sMod).Append("')");
			    sbSQL.Append(" ORDER BY MODIFIER ");

			    if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
			    {
				    while(objDbReader.Read())
				    {
					    sTempBEGCPT = objDbReader.GetValue("BEGCPT").ToString();
					    sTempENDCPT = objDbReader.GetValue("ENDCPT").ToString();
					    if(sTempBEGCPT != "")
					    {
						    if ((m_objCommon.Val(p_sCPT) >= m_objCommon.Val(sTempBEGCPT)) && 
							    (m_objCommon.Val(p_sCPT) <= m_objCommon.Val(sTempENDCPT))) 
						    {
							    if(objDbReader.GetValue("PERCENT") != null)
								    dPercent = Conversion.ConvertStrToDouble(objDbReader.GetValue("PERCENT").ToString())/100;
							    else
							    {
								    dPercent = 100;
								    break;
							    }
						    }
					    }//if(sTempBEGCPT != "")
					    else
						    dPercent = Conversion.ConvertStrToDouble(objDbReader.GetValue("PERCENT").ToString())/100;

					    if(dPercent < 1)
						    m_structModCode.dPercent = dPercent;
					    else
						    m_structModCode.dPercent = dPercent / 100;
				    }
			    }//end if(objDbReader != null)
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                    objDbReader = null;
                }
            }
		}//end function
		#endregion

		#region HandleSubCodeWC2000(p_objRequest, p_objRelativeValue,p_sStateCode)
		/// Name			: HandleSubCodeWC2000
		/// Author			: Navneet Sota
		/// Date Created	: 1/10/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Adds the relevant message to the alerts collection. 
		/// </summary>
		/// <param name="p_objRelativeValue">Relative Value Class object</param>
		/// <param name="p_objRequest">Request Class object</param>
		/// <param name="p_sStateCode">State Code</param>
		private void HandleSubCodeWC2000(Request p_objRequest, RelativeValues.RelativeValue p_objRelativeValue,string p_sStateCode)
		{
			//string variables
			string sMsg = "";			
			string sTemp = "";

			if(p_objRelativeValue.m_sSubCode != "")
			{
				sTemp = p_objRelativeValue.m_sSubCode.Trim().ToUpper();
				switch (sTemp)
				{
					case "A": //Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeA", m_iClientId);
						break;
					case "B":
			            if(p_sStateCode.ToUpper() == "TX") //' Texas / Medicare
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeBTX", m_iClientId);
			            else     // North Carolina
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeB", m_iClientId);
						break;
					case "C": //Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeC", m_iClientId);
						break;
					case "D":	//Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeD", m_iClientId);
						break;
					case "E":	//Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeE", m_iClientId);
						break;
					case "G": //Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeG", m_iClientId);
						break;
					case "H":	//Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeH", m_iClientId);
						break;
					case "I":	//Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeI", m_iClientId);
						break;
					case "N":
						if(p_sStateCode.ToUpper() == "TX") //Texas / Medicare
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeNTX", m_iClientId);
						else     // Kansas
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeN", m_iClientId);
						break;
					case "P":
						if(p_sStateCode.ToUpper() == "TX") //Texas / Medicare
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodePTX", m_iClientId);
						else     // Mississippi, Connecticut
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeP", m_iClientId);
						break;
					case "R":	//Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeR", m_iClientId);
						break;
					case "T": //Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeT", m_iClientId);
						break;
					case "X":	//Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeX", m_iClientId);
						break;
					case "IP":	//Alaska
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeIP", m_iClientId);
						break;
					case "OF": //oh - Alaska
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeOF", m_iClientId);
						break;
					case "PS":	//Alaska
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodePS", m_iClientId);
						break;
					case "S":	//Arizona
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeS", m_iClientId);
						break;
					case "0": 
						if(p_sStateCode.ToUpper() == "WA") //Hawaii, West Virginia
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode0WA", m_iClientId);
						else     // Washington
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode0", m_iClientId);
						break;
					case "1":	//Hawaii
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode1", m_iClientId);
						break;
					case "2":	//Hawaii, West Virginia, Washington
						if(p_sStateCode.ToUpper() == "WA") //Hawaii, West Virginia
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode2WA", m_iClientId);
						else     // Washington
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode2", m_iClientId);
						break;
					case "3":	//Hawaii, West Virginia, Washington
						if(p_sStateCode.ToUpper() == "WA") //Hawaii, West Virginia
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode3WA", m_iClientId);
						else     // Washington
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode3", m_iClientId);
						break;
					case "9":	//Hawaii, West Virginia, Washington
						if(p_sStateCode.ToUpper() == "WA") //Hawaii, West Virginia
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode9WA", m_iClientId);
						else     // Washington
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode9", m_iClientId);
						break;
					case "&":	//Oklahoma
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode&", m_iClientId);
						break;
					case "#":	
						if(p_sStateCode.ToUpper() == "OK") //Oklahoma
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode#Ok", m_iClientId);
						else if(p_sStateCode.ToUpper() == "WI") //Wisconsin
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode#WI", m_iClientId);
						else
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCode#", m_iClientId);
						break;
					case "U":
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeU", m_iClientId);
						if(p_sStateCode.ToUpper() == "WA")
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeUWA", m_iClientId);
						else
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeUElse", m_iClientId);
						break;
					case "V":
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.SubCodeV", m_iClientId);
						break;
					default:	
						sMsg = "Subcode: " + sTemp + " - Please refer to " + p_sStateCode + " State notes.";
						break;
				}//end switch

				if(sMsg != "")
					m_objCommon.AddAlert(sMsg);
			}//end if(p_sSubCode != "")

			sMsg = "";
			if(p_objRelativeValue.m_sGlblCode != "")
			{
				//Medicare/Tx Follow up comes with Alpha codes
				sTemp = p_objRelativeValue.m_sGlblCode.Trim().ToUpper();

				switch (sTemp)
				{
					case "MMM": //Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.GlblCodeMMM", m_iClientId);
						break;
					case "XXX":
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.GlblCodeXXX", m_iClientId);
						break;
					case "YYY": //Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.GlblCodeYYY", m_iClientId);
						break;
					case "ZZZ":	//Texas / Medicare
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.GlblCodeZZZ", m_iClientId);
						break;
				}//end switch

				if(sMsg != "")
					m_objCommon.AddAlert(sMsg);
			}//end if(p_objRelativeValue.m_sGlblCode != "")

			sMsg = "";
			if(p_objRelativeValue.m_sByReport != "")
			{
				//Medicare/Tx Follow up comes with Alpha codes
				sTemp = p_objRelativeValue.m_sByReport.Trim().ToUpper();

				switch (sTemp)
				{
					case "BR":
					case "B":	//ingenix confusion
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.ByReportBR", m_iClientId);
						break;
					case "IC":
					case "I":	//ingenix confusion
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.ByReportIC", m_iClientId);
						break;
					case "NE":
					case "N":
					case "R":
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.ByReportNE", m_iClientId);
						break;
					default:
						sMsg = "By Report code: " + sTemp + " - Please refer to State notes.";
						break;
				}//end switch

				if(sMsg != "")
					m_objCommon.AddAlert(sMsg);
			}//end if(p_objRelativeValue.m_sByReport != "")

			sMsg = "";
			if(p_objRelativeValue.m_sStarred != "")
			{
				//Medicare/Tx Follow up comes with Alpha codes
				sTemp = p_objRelativeValue.m_sStarred.Trim().ToUpper();

				switch (sTemp)
				{
					case "*":
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.Starred", m_iClientId);
						break;
					default:
						sMsg = "Starred code: " + sTemp + " - Please refer to " + p_sStateCode + " State notes.";
						break;
				}//end switch

				if(sMsg != "")
					m_objCommon.AddAlert(sMsg);
			}//end if(p_objRelativeValue.m_sStarred != "")

			sMsg = "";
			if(p_objRelativeValue.m_sQuestion != "")
			{
				sTemp = p_objRelativeValue.m_sQuestion.Trim().ToUpper();

				switch (sTemp)
				{
					case "?":
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.Question", m_iClientId);
						break;
					default:
						sMsg = "Question code: " + sTemp + " - Please refer to " + p_sStateCode + " State notes.";
						break;
				}//end switch

				if(sMsg != "")
					m_objCommon.AddAlert(sMsg);
			}//end if(p_objRelativeValue.m_sQuestion != "")

			sMsg = "";
			if(p_objRelativeValue.m_sAssistSurg != "")
			{
				//Medicare/Tx Follow up comes with Alpha codes
				sTemp = p_objRelativeValue.m_sAssistSurg.Trim().ToUpper();

				switch (sTemp)
				{
					case "Y":
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.AssistSurgY", m_iClientId);
						break;
					case "N":
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.AssistSurgN", m_iClientId);
						break;
					case "S": //West Virginia, Kansas
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.AssistSurgS", m_iClientId);
						break;
					case "T":	//Michigan 
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.AssistSurgT", m_iClientId);
						break;
					case "0": //Hawaii, Washington
						if(p_sStateCode.ToUpper() == "WA") //Washington (Sometimes)
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.AssistSurg0WA", m_iClientId);
						else //Hawaii (Sometimes)
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.AssistSurg0", m_iClientId);
						break;
					case "1"://Hawaii (No), Washington
						if(p_sStateCode.ToUpper() == "WA") //Washington (Sometimes)
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.AssistSurg1WA", m_iClientId);
						else //Hawaii (Sometimes)
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.AssistSurg1", m_iClientId);
						break;
					case "2": //Hawaii (Yes), Washington
						if(p_sStateCode.ToUpper() == "WA") //Washington (No)
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.AssistSurg2WA", m_iClientId);
						else //Hawaii (Sometimes)
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.AssistSurg2", m_iClientId);
						break;
					case "9": //Hawaii, Washington
						if(p_sStateCode.ToUpper() == "WA") //Washington (No)
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.AssistSurg9WA", m_iClientId);
						else //Hawaii (Sometimes)
                            sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.AssistSurg9", m_iClientId);
						break;
					default:
						sMsg = "Assistant Surgeon code: " + sTemp + " - Please refer to " + p_sStateCode + " State notes.";
						break;
				}//end switch

				if(sMsg != "")
					m_objCommon.AddAlert(sMsg);
			}//end if(p_objRelativeValue.m_sAssistSurg != "")

			sMsg = "";
			if(p_objRelativeValue.m_sNotCovered != "")
			{
				//Medicare/Tx Follow up comes with Alpha codes
				sTemp = p_objRelativeValue.m_sNotCovered.Trim().ToUpper();

				switch (sTemp)
				{
					case "NC":
                        sMsg = sTemp + Globalization.GetString("CalculateWC.HandleSubCodeWC2000.NotCovered", m_iClientId);
						break;
					default:
						sMsg = "Not Covered code: " + sTemp + " - Please refer to " + p_sStateCode + " State notes.";
						break;
				}//end switch

				if(sMsg != "")
					m_objCommon.AddAlert(sMsg);
			}//end if(p_objRelativeValue.m_sNotCovered != "")
		}//End HandleSubCodeWC2000

		#endregion

		#region HandleSubCodeWComp(p_objRequest,p_sSubCode)
		/// Name			: HandleSubCodeWComp
		/// Author			: Navneet Sota
		/// Date Created	: 1/10/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Adds the relevant message, for Workers compensation, to the alerts collection.
		/// </summary>
		/// <param name="p_objRequest">Request Class object</param>
		/// <param name="p_sSubCode">Sub Code</param>
		private void HandleSubCodeWComp(Request p_objRequest, string p_sSubCode)
		{
			//string variables
			string sMsg = "";			
			string sTemp = "";

			if(p_sSubCode != "")
			{
                sTemp = p_sSubCode.Trim().ToUpper();

                //srajindersin MITS 36730 dt 7/14/2014
                if (p_sSubCode.Length >= 2)
				    sTemp = p_sSubCode.Substring(0,2).ToUpper();

				switch (sTemp)
				{
					case "BR":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.BR", m_iClientId);
						break;
					case "V":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.V", m_iClientId);
						break;
					case "I":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.I", m_iClientId);
						break;
					case "S":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.S", m_iClientId);
						break;
					case "R":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.R", m_iClientId);
						break;
					case "?":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.?", m_iClientId);
						break;
					case "N":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.N", m_iClientId);
						break;
					case "B":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.B", m_iClientId);
						break;
					case "Y":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.Y", m_iClientId);
						break;
					case "#":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.#", m_iClientId);
						break;
					case "C":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.C", m_iClientId);
						break;
					case "P":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.P", m_iClientId);
						break;
					case "U":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.U", m_iClientId);
						break;
					case "T":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.T", m_iClientId);
						break;
					case "*":
                        sMsg = Globalization.GetString("CalculateWC.HandleSubCodeWComp.*", m_iClientId);
						break;
				}//end switch

				if(sMsg != "")
					m_objCommon.AddAlert(sMsg);
			}//end if(p_sSubCode != "")
		}//End HandleSubCodeWComp

		#endregion

        #region GetFactorsWC1999(p_objFeeSchedule,p_objRequest,p_sStudyId, p_sStateId, p_sSpecCode, p_sCalcMods)
        /// Name			: GetFactorsWC1999
        /// Author			: saurabh singh
        /// Date Created	: 7/11/2014	
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Get Relevant factors for Workers Compensation 1999
        /// </summary>
        /// <param name="p_objFeeSchedule">Fee Schedule object</param>
        /// <param name="p_objRequest">Request object</param>
        /// <param name="p_sStudyId">Study-Id</param>
        /// <param name="p_sCalcMods">Calculation Mode</param>
        /// <param name="p_sSpecCode">Specification Code</param>
        /// <param name="p_sStateId">State-Id</param>
        private Hashtable GetFactorsWC1999(FeeSchedule p_objFeeSchedule, Request p_objRequest, string p_sStudyId,
            string p_sStateId, string p_sSpecCode, string p_sCalcMods)
        {
            //Return Variable
            Hashtable hstReturnValue = null;

            //string variable
            string sCalcPos = "";
            string sCalcTos = "";
            string sPos = "";
            string sTos = "";
            string sMod = "";
            bool bPosMatch = false;
            bool bTosMatch = false;
            bool bModMatch = false;
            //int variable
            int iMatch = 0;
            int iMaxMatch = 0;
            int iCnt = 0;

            //objects
            RelativeValues.RelativeValue objRelativeValue = null;
            StringBuilder sbSQL = null;
            DbReader objDbReader = null;
            try
            {
                hstReturnValue = new Hashtable();
                sbSQL = new StringBuilder();

                //Table[0]
                sbSQL.Append(" SELECT FACTOR,TOS,POS,MODIFIER ");
                sbSQL.Append(" FROM WRKFCT WHERE STUDYID = '").Append(p_sStudyId).Append("'");
                sbSQL.Append(" AND STATEID = '").Append(p_sStateId).Append("'");
                //MITS 24539
                sbSQL.Append(" AND '").Append(p_objRequest.CPT).Append("' BETWEEN BEGCPT AND ENDCPT AND ");
                if (p_sSpecCode != "")
                    sbSQL.Append(" SPEC ='").Append(p_sSpecCode).Append("'");
                else
                {
                    sbSQL.Append("((SPEC IS NULL) OR (SPEC=''))");
                }


                //sbSQL.Append(" ORDER BY UNIQUESUB ");

                if (m_objCommon.GetDbReader(ref objDbReader, sbSQL.ToString()))
                {
                    //fill short codes from g_objCalculate code values
                    //these short codes are filled by CCalculate because are referred to in great number of places
                    sCalcTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode).ToString();
                    sCalcPos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.PlaceOfSerCode).ToString();
                    //highest number of matches on the 3 criteria
                    iMaxMatch = 0;

                    while (objDbReader.Read())
                    {
                        iMatch = 0;
                        sPos = objDbReader.GetValue("POS").ToString();
                        sTos = objDbReader.GetValue("TOS").ToString();
                        sMod = objDbReader.GetValue("MODIFIER").ToString();

                        bPosMatch = false;
                        bTosMatch = false;
                        bModMatch = false;

                        if (sPos.Trim() == "")
                        {
                            iMatch = iMatch + 1;
                            bPosMatch = true;
                        }
                        else
                        {
                            if (sPos == sCalcPos)
                            {
                                iMatch = iMatch + 1;
                                bPosMatch = true;
                            }
                        }

                        if (sTos.Trim() == "")
                        {
                            iMatch = iMatch + 1;
                            bTosMatch = true;
                        }
                        else
                            if (sTos == sCalcTos)
                            {
                                iMatch = iMatch + 1;
                                bTosMatch = true;
                            }

                        if ((p_sCalcMods.Trim() == "" && sMod.Trim() == "") || (p_sCalcMods.IndexOf("'" + sMod + "'", 0) != -1))
                        {

                            iMatch = iMatch + 1;
                            bModMatch = true;
                        }
                        else
                        {
                            if (sMod.Trim() == "")
                            {
                                bModMatch = true;
                            }
                        }
                        if (iMaxMatch < iMatch)
                            iMaxMatch = iMatch;

                        //if this record has the same number of matches as the record with the most matches, 
                        //then add it to the collection
                        if (bPosMatch == true && bTosMatch == true && bModMatch == true && iMatch == iMaxMatch && iMatch > 0)
                        {
                            objRelativeValue = new RelativeValues.RelativeValue();
                            objRelativeValue.m_iMatches = iMatch;
                            objRelativeValue.m_sPOS = sPos;
                            objRelativeValue.m_sTOS = sTos;
                            objRelativeValue.m_sMODS = sMod;
                            objRelativeValue.m_dFactor = Conversion.ConvertStrToDouble(objDbReader.GetValue("FACTOR").ToString());
                            //objRelativeValue.m_sUniqueSub = objDbReader.GetValue("UNIQUESUB").ToString();

                            //Add to collection
                            hstReturnValue.Add(iCnt, objRelativeValue);
                            objRelativeValue = null;
                            iCnt++;
                        }//end if(iMatch = iMaxMatch && iMatch > 0)
                    }
                    //Remove items from collection if they have fewer matches than the record(s) 
                    //with the max number of matches
                    int iCount = hstReturnValue.Count;
                    for (iCnt = 0; iCnt < iCount; iCnt++)
                    {
                        objRelativeValue = (RelativeValues.RelativeValue)hstReturnValue[iCnt];
                        if (objRelativeValue.m_iMatches != iMaxMatch)
                            hstReturnValue.Remove(iCnt);

                        objRelativeValue = null;
                    }//for(iCnt=0; iCnt < hstReturnValue.Count; iCnt++)
                }//end if(objDbReader != null)
            }//end try
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
                objDbReader = null;
                sbSQL = null;
                objRelativeValue = null;
            }

            //Return the result to calling function
            return hstReturnValue;
        }//end function

        #endregion

		#region GetFactorsWC2000(p_objFeeSchedule,p_objRequest,p_sStudyId, p_sStateId, p_sSpecCode, p_sCalcMods)
		/// Name			: GetFactorsWC2000
		/// Author			: Navneet Sota
		/// Date Created	: 1/10/2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Get Relevant factors for Workers Compensation 2000
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule object</param>
		/// <param name="p_objRequest">Request object</param>
		/// <param name="p_sStudyId">Study-Id</param>
		/// <param name="p_sCalcMods">Calculation Mode</param>
		/// <param name="p_sSpecCode">Specification Code</param>
		/// <param name="p_sStateId">State-Id</param>
		private Hashtable GetFactorsWC2000(FeeSchedule p_objFeeSchedule, Request p_objRequest, string p_sStudyId, 
			string p_sStateId, string p_sSpecCode, string p_sCalcMods)
		{
			//Return Variable
			Hashtable hstReturnValue = null;

			//string variable
			string sCalcPos = "";
			string sCalcTos = "";
			string sPos = "";
			string sTos = "";
			string sMod = "";
			bool bPosMatch = false;
			bool bTosMatch = false;
			bool bModMatch = false;
			//int variable
			int iMatch = 0;
			int iMaxMatch = 0;
			int iCnt = 0;

			//objects
			RelativeValues.RelativeValue objRelativeValue = null;
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			try
			{
				hstReturnValue =  new Hashtable();
				sbSQL = new StringBuilder();

				//Table[0]
				sbSQL.Append(" SELECT FACTOR,TOS,POS,MODIFIER,UNIQUESUB ");
				sbSQL.Append(" FROM WRKFCT WHERE STUDYID = '").Append(p_sStudyId).Append("'");
				sbSQL.Append(" AND STATEID = '").Append(p_sStateId).Append("'");
                //MITS 24539
                sbSQL.Append(" AND '").Append(p_objRequest.CPT).Append("' BETWEEN BEGPROC AND ENDPROC AND ");
                if (p_sSpecCode != "")
                    sbSQL.Append(" SPECIALTY ='").Append(p_sSpecCode).Append("'");
                else
                {
                    sbSQL.Append("((SPECIALTY IS NULL) OR (SPECIALTY=''))");
                }


				sbSQL.Append(" ORDER BY UNIQUESUB ");
            
				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					//fill short codes from g_objCalculate code values
					//these short codes are filled by CCalculate because are referred to in great number of places
                    sCalcTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode).ToString();
                    sCalcPos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.PlaceOfSerCode).ToString();
					//highest number of matches on the 3 criteria
					iMaxMatch = 0;
					
					while(objDbReader.Read())
					{
						iMatch = 0;
						sPos = objDbReader.GetValue("POS").ToString();
						sTos = objDbReader.GetValue("TOS").ToString();
						sMod = objDbReader.GetValue("MODIFIER").ToString();

						bPosMatch = false;
						bTosMatch = false;
						bModMatch = false;

						if(sPos.Trim() == "")
						{
							iMatch = iMatch + 1;
							bPosMatch = true; 
						}
						else 
						{
							if(sPos == sCalcPos)
							{
								iMatch = iMatch + 1;
								bPosMatch = true; 
							}
						}

						if(sTos.Trim() == "")
						{
							iMatch = iMatch + 1;
							bTosMatch = true; 
						}
						else 
							if(sTos == sCalcTos)
						{
							iMatch = iMatch + 1;
							bTosMatch = true; 
						}

						if((p_sCalcMods.Trim()=="" && sMod.Trim()=="") || (p_sCalcMods.IndexOf("'" + sMod + "'",0)!= -1))
						{
							
							iMatch = iMatch + 1;
							bModMatch = true ;
						}
						else
						{
							if(sMod.Trim() == "")
							{
								bModMatch = true ;
							}
						}
						if(iMaxMatch < iMatch)
							iMaxMatch = iMatch;

						//if this record has the same number of matches as the record with the most matches, 
						//then add it to the collection
						if(bPosMatch == true && bTosMatch ==true && bModMatch ==true && iMatch == iMaxMatch && iMatch > 0)
						{
							objRelativeValue = new RelativeValues.RelativeValue();
							objRelativeValue.m_iMatches = iMatch;
							objRelativeValue.m_sPOS = sPos;
							objRelativeValue.m_sTOS = sTos;
							objRelativeValue.m_sMODS = sMod;
							objRelativeValue.m_dFactor = Conversion.ConvertStrToDouble(objDbReader.GetValue("FACTOR").ToString());
							objRelativeValue.m_sUniqueSub = objDbReader.GetValue("UNIQUESUB").ToString();

							//Add to collection
							hstReturnValue.Add(iCnt,objRelativeValue);
							objRelativeValue = null;
							iCnt++;
						}//end if(iMatch = iMaxMatch && iMatch > 0)
					}
					//Remove items from collection if they have fewer matches than the record(s) 
					//with the max number of matches
					int iCount = hstReturnValue.Count;
					for(iCnt=0; iCnt < iCount; iCnt++)
					{
						objRelativeValue = (RelativeValues.RelativeValue)hstReturnValue[iCnt];
						if(objRelativeValue.m_iMatches != iMaxMatch)
							hstReturnValue.Remove(iCnt);

						objRelativeValue = null;
					}//for(iCnt=0; iCnt < hstReturnValue.Count; iCnt++)
				}//end if(objDbReader != null)
			}//end try
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}
				objDbReader = null;
				sbSQL = null;
				objRelativeValue = null;
			}

			//Return the result to calling function
			return hstReturnValue;
		}//end function

		#endregion

		#region GetRelativeValuesWC2000(p_objFeeSchedule,p_objRequest,p_sStudyId, p_sStateId,p_sCalcMods, ref p_sError)
		/// Name			: GetRelativeValuesWC2000
		/// Author			: Navneet Sota
		/// Date Created	: 1/10/2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Fills the passed in Relative-Value object and returns the collection of 
        /// RelativeValue objects. relative value unit (RVU)
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule</param>
		/// <param name="p_objRequest">Request Object</param>
		/// <param name="p_sStudyId">Study-Id</param>
		/// <param name="p_sCalcMods">Calculation Mode</param>
		/// <param name="p_sStateId">State Id</param>
		/// <param name="p_sError">Error string</param>
        /// <param name="p_sStateCode"></param>
		private Hashtable GetRelativeValuesWC2000(FeeSchedule p_objFeeSchedule, Request p_objRequest, string p_sStudyId, 
			string p_sStateCode,string p_sStateId, string p_sCalcMods, ref string p_sError)
		{
			//Return Variable
			Hashtable hstReturnValue = null;

			//string variables
			string sCalcPos = "";
			string sCalcTos = "";
			string sPos = "";
			string sTos = "";
			string sMod = "";
			string sPrevPOS = "" ;
			string sPrevTOS = "" ;
			string sPrevMOD = "" ;
			//int variables
			int iMatch = 0;
			int iMaxMatch = 0;
			int iCnt = 0;
			int iFinalMatch = 0;
            bool bPosMatch = false;
            bool bTosMatch = false;
			bool bModMatch = false;
			//objects
			RelativeValues.RelativeValue objRelativeValue = null;
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			DbReader objDbReader1 = null;
			bool bFirst = true ;
			bool bFLSpec99Exists = false;
			try
			{
				hstReturnValue =  new Hashtable();
				sbSQL = new StringBuilder();

				//Table[0]
				sbSQL.Append(" SELECT RVUFEE,POS,TOS,MODIFIER,SPECIALTY,UNIQUESUB,INDICATOR,[DESC], ");
				sbSQL.Append(" FOLLUP,SUBCODE,BYREPORT,STARRED,QUESTION,ASSISTSURG,NOTCOVERED ");
				sbSQL.Append(" FROM WRKUVA WHERE STUDYID = '").Append(p_sStudyId).Append("'");
				sbSQL.Append(" AND STATEID = '").Append(p_sStateId).Append("'");
				sbSQL.Append(" AND [PROC] = '").Append(p_objRequest.CPT).Append("'");
				sbSQL.Append(" ORDER BY UNIQUESUB ");
            
				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					//tkr mits 10266 compare against the short code, not the code_id
                    int iCalcTos = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode;
                    int iCalcPos = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.PlaceOfSerCode;
                    if (iCalcTos == 0)
                        sCalcTos = "";
                    else
                         sCalcTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);
                    if (iCalcPos == 0)
                        sCalcPos = "";
                    else
                    sCalcPos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.PlaceOfSerCode);
					//highest number of matches on the 3 criteria
					iMaxMatch = 0;
					
					while(objDbReader.Read())
					{
						iMatch = 0;
						sPos = objDbReader.GetValue("POS").ToString();
						sTos = objDbReader.GetValue("TOS").ToString();
						sMod = objDbReader.GetValue("MODIFIER").ToString();
						bPosMatch = false;
						bTosMatch = false;
						bModMatch = false;

                        if (sPos == sCalcPos)
                        {
                            iMatch = iMatch + 1;
                            bPosMatch = true;
                        }
                        else if(sPos.Trim() == "")
                        {
                            bPosMatch = true;
                        }

                        if (sTos == sCalcTos)
                        {
                            iMatch = iMatch + 1;
                            bTosMatch = true;
                        }
                        else if (sTos.Trim() == "")
                        {
                            bTosMatch = true;
                        }

						if((p_sCalcMods.Trim()=="" && sMod.Trim()=="") || (p_sCalcMods.IndexOf("'" + sMod + "'",0)!= -1))
						{
							
								iMatch = iMatch + 1;
								bModMatch = true ;
						}
						else
						{
							if(sMod.Trim() == "")
							{
								bModMatch = true ;
							}
						}

						if(iMaxMatch < iMatch)
							iMaxMatch = iMatch;

						//if this record has the same number of matches as the record with the most matches, 
						//then add it to the collection
						if(bPosMatch == true && bTosMatch ==true && bModMatch ==true && iMatch >= iMaxMatch)
						{
							objRelativeValue = new RelativeValues.RelativeValue();
							objRelativeValue.m_iMatches = iMatch;
							objRelativeValue.m_sPOS = sPos;
							objRelativeValue.m_sTOS = sTos;
							objRelativeValue.m_sMODS = sMod;
							objRelativeValue.m_dAmount = Conversion.ConvertStrToDouble(objDbReader.GetValue("RVUFEE").ToString());
							objRelativeValue.m_sSpecialty = objDbReader.GetValue("SPECIALTY").ToString();
							objRelativeValue.m_sUniqueSub = objDbReader.GetValue("UNIQUESUB").ToString();
							objRelativeValue.m_sIndicator = objDbReader.GetValue("INDICATOR").ToString();
							objRelativeValue.m_sDesc = objDbReader.GetValue("DESC").ToString();
							objRelativeValue.m_sSubCode = objDbReader.GetValue("SUBCODE").ToString();
							objRelativeValue.m_sByReport = objDbReader.GetValue("BYREPORT").ToString();
							objRelativeValue.m_sStarred = objDbReader.GetValue("STARRED").ToString();
							objRelativeValue.m_sQuestion = objDbReader.GetValue("QUESTION").ToString();
							objRelativeValue.m_sAssistSurg = objDbReader.GetValue("ASSISTSURG").ToString();
							objRelativeValue.m_sNotCovered = objDbReader.GetValue("NOTCOVERED").ToString();

							if(Conversion.IsNumeric(objDbReader.GetValue("FOLLUP").ToString()))
                                objRelativeValue.m_iFollup = Conversion.ConvertStrToInteger(objDbReader.GetValue("FOLLUP").ToString());
							else
                                objRelativeValue.m_sGlblCode = objDbReader.GetValue("FOLLUP").ToString();

                            //srajindersin - MITS 27672 - 03/20/2012
                            ////Add to collection
                            //if(bFirst || (sPrevPOS == sPos && sPrevTOS == sTos && sPrevMOD == sMod))
                            //{
                            //END srajindersin - MITS 27672 - 03/20/2012
								hstReturnValue.Add(iCnt,objRelativeValue);
								objRelativeValue = null;
                            //srajindersin - MITS 27672 - 03/20/2012
                            //}
                            //else
                            //{
                            //   hstReturnValue.Clear() ;
                            //   //abisht MITS 12005.Reset the value of iCnt. 
                            //   iCnt = -1; 
                            //}
                            //END srajindersin - MITS 27672 - 03/20/2012

                            bFirst = false ;
                            sPrevPOS = sPos ;
                            sPrevTOS = sTos ;
                            sPrevMOD = sMod ;
							iCnt++;
							iFinalMatch = iMatch;
						}//end if(iMatch = iMaxMatch && iMatch > 0)
					}
					// Added By Rahul. 03/21/2006.
					//If Spec code 99 exists then add it to the list by default.
					if(p_sStateCode == "FL")
					{
						int iMaxCount = hstReturnValue.Count;
						for(int i=0;i<iMaxCount;i++)
						{
							objRelativeValue = new RelativeValues.RelativeValue();
							objRelativeValue = (RelativeValues.RelativeValue)hstReturnValue[i];
							if(objRelativeValue.m_sSpecialty.Trim()=="99")
							{
								bFLSpec99Exists = true;
								break;
							}
						}
						if(!bFLSpec99Exists)
						{
							if(m_objCommon.GetDbReader(ref objDbReader1,sbSQL.ToString()))
							{
								bFirst = true;
								sPrevMOD = "";
								sPrevPOS = "";
								sPrevTOS = "";
								iFinalMatch = 0;
								while(objDbReader1.Read())
								{
									if(objDbReader1.GetValue("SPECIALTY").ToString()=="99")
									{
										iMatch = 0;
										bPosMatch = false;
										bTosMatch = false;
										bModMatch = false;
										sPos = objDbReader1.GetValue("POS").ToString();
										sTos = objDbReader1.GetValue("TOS").ToString();
										sMod = objDbReader1.GetValue("MODIFIER").ToString();
										
										if(sPos.Trim() == "")
										{
											iMatch = iMatch + 1;
											bPosMatch = true; 
										}
										else 
										{
											if(sPos == sCalcPos)
											{
												iMatch = iMatch + 1;
												bPosMatch = true; 
											}
										}

										if(sTos.Trim() == "")
										{
											iMatch = iMatch + 1;
											bTosMatch = true; 
										}
										else 
											if(sTos == sCalcTos)
										{
											iMatch = iMatch + 1;
											bTosMatch = true; 
										}

										if((p_sCalcMods.Trim()=="" && sMod.Trim()=="") || (p_sCalcMods.IndexOf("'" + sMod + "'",0)!= -1))
										{
							
											iMatch = iMatch + 1;
											bModMatch = true ;
										}
										else
										{
											if(sMod.Trim() == "")
											{
												bModMatch = true ;
											}
										}
										if(bPosMatch == true && bTosMatch ==true && bModMatch ==true && iMatch >= iFinalMatch)
										{
											objRelativeValue = new RelativeValues.RelativeValue();
											objRelativeValue.m_iMatches = iMatch;
											objRelativeValue.m_sPOS = sPos;
											objRelativeValue.m_sTOS = sTos;
											objRelativeValue.m_sMODS = sMod;
											objRelativeValue.m_dAmount = Conversion.ConvertStrToDouble(objDbReader1.GetValue("RVUFEE").ToString());
											objRelativeValue.m_sSpecialty = objDbReader1.GetValue("SPECIALTY").ToString();
											objRelativeValue.m_sUniqueSub = objDbReader1.GetValue("UNIQUESUB").ToString();
											objRelativeValue.m_sIndicator = objDbReader1.GetValue("INDICATOR").ToString();
											objRelativeValue.m_sDesc = objDbReader1.GetValue("DESC").ToString();
											objRelativeValue.m_sSubCode = objDbReader1.GetValue("SUBCODE").ToString();
											objRelativeValue.m_sByReport = objDbReader1.GetValue("BYREPORT").ToString();
											objRelativeValue.m_sStarred = objDbReader1.GetValue("STARRED").ToString();
											objRelativeValue.m_sQuestion = objDbReader1.GetValue("QUESTION").ToString();
											objRelativeValue.m_sAssistSurg = objDbReader1.GetValue("ASSISTSURG").ToString();
											objRelativeValue.m_sNotCovered = objDbReader1.GetValue("NOTCOVERED").ToString();

											if(Conversion.IsNumeric(objDbReader1.GetValue("FOLLUP").ToString()))
												objRelativeValue.m_iFollup = Conversion.ConvertStrToInteger(objDbReader1.GetValue("FOLLUP").ToString());
											else
												objRelativeValue.m_sGlblCode = objDbReader1.GetValue("FOLLUP").ToString();

                                            //srajindersin - MITS 27672 - 03/20/2012
											//Add to collection
                                            //if(bFirst || (sPrevPOS == sPos && sPrevTOS == sTos && sPrevMOD == sMod))
                                            //{
                                            //END srajindersin - MITS 27672 - 03/20/2012
												hstReturnValue.Add(iCnt,objRelativeValue);
												objRelativeValue = null;
                                            //srajindersin - MITS 27672 - 03/20/2012
                                            //}
                                            //else
                                            //{
                                            //    hstReturnValue.Clear() ;
                                            //}
                                            //END srajindersin - MITS 27672 - 03/20/2012

											bFirst = false ;
											sPrevPOS = sPos ;
											sPrevTOS = sTos ;
											sPrevMOD = sMod ;
											iCnt++;
											iFinalMatch = iMatch;
										}//end if(iMatch = iMaxMatch && iMatch > 0)

									}
								}
							}
						}
					}
					//Remove items from collection if they have fewer matches than the record(s) 
					//with the max number of matches
					int iCount = hstReturnValue.Count;
					for(iCnt=0; iCnt < iCount; iCnt++)
					{
						objRelativeValue = new RelativeValues.RelativeValue();
						objRelativeValue = (RelativeValues.RelativeValue)hstReturnValue[iCnt];
						if(objRelativeValue.m_iMatches != iMaxMatch)
							hstReturnValue.Remove(iCnt);
						
						objRelativeValue = null;
					}//for(iCnt=0; iCnt < hstReturnValue.Count; iCnt++)

					//return error message if none matched
					if(hstReturnValue.Count == 0)
                        p_sError = Globalization.GetString("Common.Constants.ErrTos", m_iClientId);
				}//end if(objDbReader != null)
				else
				{
					//leave here if no records and set error message
                    p_sError = Globalization.GetString("Common.Constants.ErrCpt", m_iClientId);
					return hstReturnValue;
				}

			}//end try
			catch(RMAppException p_objException)
			{
                throw new RMAppException(p_objException.Message, p_objException);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException("Error while getting Relative Valus.", p_objException);
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}
				objDbReader = null;
				sbSQL = null;
				objRelativeValue = null;
				if(objDbReader1!=null)
				{
					objDbReader1.Close() ;
					objDbReader1.Dispose() ;
				}
			}

			//Return the result to calling function
			return hstReturnValue;
		}//end function

		#endregion

		#region GetRelativeValuesWComp(p_objFeeSchedule,p_objRequest,p_sStudyId, p_sStateId, p_sCalcMods)
		/// Name			: GetRelativeValuesWComp
		/// Author			: Navneet Sota
		/// Date Created	: 1/10/2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Fills the Relative-Value object and returns the collection of RelativeValue objects
		/// </summary>
		/// <param name="p_objFeeSchedule">Fee Schedule object</param>
		/// <param name="p_objRequest">Request Object</param>
		/// <param name="p_sStudyId">Study-Id</param>
		/// <param name="p_sStateId">State-Id</param>
		/// <param name="p_sCalcMods">Calculation Mode</param>
		private Hashtable GetRelativeValuesWComp(FeeSchedule p_objFeeSchedule, Request p_objRequest, string p_sStudyId, 
			string p_sStateId, string p_sCalcMods)
		{
			//Return Variable
			Hashtable hstReturnValue = null;

			//string variables
			string sCalcPos = "";
			string sCalcTos = "";
			string sPos = "";
			string sTos = "";
			string sMod = "";

			//int variables
			int iMatch = 0;
			int iMaxMatch = 0;
			int iCnt = 0;
			bool bPosMatch = false;
			bool bTosMatch = false;
			bool bModMatch = false;
			//objects
			RelativeValues.RelativeValue objRelativeValue = null;
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			try
			{
				hstReturnValue =  new Hashtable();
				sbSQL = new StringBuilder();

				//Table[0]
                //srajindersin MITS 36730 dt 7/14/2014
				sbSQL.Append(" SELECT RLV,FOLLUP,SUBCODE,POS,TOS,MODIFIER,SPEC ");
				sbSQL.Append(" FROM WRKUVA WHERE STUDYID = '").Append(p_sStudyId).Append("'");
				sbSQL.Append(" AND STATEID = '").Append(p_sStateId).Append("'");
				sbSQL.Append(" AND CPT = '").Append(p_objRequest.CPT).Append("'");
            
				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()))
				{
					//fill short codes from g_objCalculate code values
					//these short codes are filled by CCalculate because are referred to in great number of places
                    sCalcTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode).ToString();
                    sCalcPos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.PlaceOfSerCode).ToString();
					//highest number of matches on the 3 criteria
					iMaxMatch = 0;
					
					while(objDbReader.Read())
					{
						iMatch = 0;
						sPos = objDbReader.GetValue("POS").ToString();
						sTos = objDbReader.GetValue("TOS").ToString();
						sMod = objDbReader.GetValue("MODIFIER").ToString();
						bPosMatch = false;
						bTosMatch = false;
						bModMatch = false;

						if(sPos.Trim() == "")
						{
							bPosMatch = true;
                            iMatch = iMatch + 1;//srajindersin MITS 36730 dt 7/14/2014
						}
						else 
						{
							if(sPos == sCalcPos)
							{
								iMatch = iMatch + 1;
								bPosMatch = true; 
							}
						}

						if(sTos.Trim() == "")
						{
							bTosMatch = true;
                            iMatch = iMatch + 1;//srajindersin MITS 36730 dt 7/14/2014
						}
						else 
							if(sTos == sCalcTos)
						    {
							    iMatch = iMatch + 1;
							    bTosMatch = true; 
						    }

						if((p_sCalcMods.Trim()=="" && sMod.Trim()=="") || (p_sCalcMods.IndexOf("'" + sMod + "'",0)!= -1))
						{
							
							iMatch = iMatch + 1;
							bModMatch = true ;
						}
						else
						{
							if(sMod.Trim() == "")
							{
								bModMatch = true ;
							}
						}

						if(iMaxMatch < iMatch)
							iMaxMatch = iMatch;

						//if this record has the same number of matches as the record with the most matches, 
						//then add it to the collection
						if(bPosMatch == true && bTosMatch ==true && bModMatch ==true && iMatch == iMaxMatch && iMatch > 0)
						{
							objRelativeValue = new RelativeValues.RelativeValue();
							objRelativeValue.m_iMatches = iMatch;
							objRelativeValue.m_sPOS = sPos;
							objRelativeValue.m_sTOS = sTos;
							objRelativeValue.m_sMODS = sMod;
							objRelativeValue.m_dAmount = Conversion.ConvertStrToDouble(objDbReader.GetValue("RLV").ToString())/100;
                            objRelativeValue.m_sSpecialty = objDbReader.GetValue("SPEC").ToString();//srajindersin MITS 36730 dt 7/14/2014
							objRelativeValue.m_sSubCode = objDbReader.GetValue("SUBCODE").ToString();

							if(Conversion.IsNumeric(objDbReader.GetValue("FOLLUP").ToString()))
								objRelativeValue.m_iFollup = 
									Conversion.ConvertStrToInteger(objDbReader.GetValue("FOLLUP").ToString());
							else
								objRelativeValue.m_sGlblCode = objDbReader.GetValue("FOLLUP").ToString();

							//Add to collection
							hstReturnValue.Add(iCnt,objRelativeValue);
							objRelativeValue = null;

							iCnt++;
						}//end if(iMatch = iMaxMatch && iMatch > 0)
					}

					//Remove items from collection if they have fewer matches than the record(s) 
					//with the max number of matches
					for(iCnt=0; iCnt < hstReturnValue.Count; iCnt++)
					{
						objRelativeValue = (RelativeValues.RelativeValue)hstReturnValue[iCnt];
						if(objRelativeValue.m_iMatches != iMaxMatch)
							hstReturnValue.Remove(iCnt);

						objRelativeValue = null;
					}//for(iCnt=0; iCnt < hstReturnValue.Count; iCnt++)
				}//end if(objDbReader != null)
			}//end try
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if (objDbReader != null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
				}
				objDbReader = null;
				sbSQL = null;
				objRelativeValue = null;
			}

			//Return the result to calling function
			return hstReturnValue;
		}//end function

		#endregion
        
		# endregion

        #region IDisposable Members

        public void Dispose()
        {           
            m_objCommon = null;
            if (m_objLocalCache != null)
                m_objLocalCache.Dispose();
            m_objLocalCache = null;
        }

        #endregion


		#region Destructor
		~CalculateWC()
		{
            Dispose();
		}
		# endregion

	}//End Class
}//End Namespace
