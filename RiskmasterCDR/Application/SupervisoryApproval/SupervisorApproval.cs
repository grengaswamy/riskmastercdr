﻿using System;
using System.Xml;
using System.IO;
using System.Collections;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using System.Text;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.Reserves;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Application.FundManagement; //smahajan6: Safeway: Payment Supervisory Approval
using System.Collections.Specialized;
using Riskmaster.Security;
using System.Globalization;
using System.Threading;
using Riskmaster.Application.VSSInterface;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Riskmaster.Application.SupervisoryApproval
{
	/**************************************************************
	 * $File		: SupervisorApproval.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 02/02/2005
	 * $Author		: Parag Sarin
	 * $Comment		: SupervisorApproval class has functions to List/Approve/Void/Deny funds transactions
	 * $Source		:  	
	**************************************************************/

	/// <summary>
	/// SupervisorApproval Class.
	/// </summary>
	public class SupervisorApproval
	{
		#region Variables Declaration
		/// <summary>
		/// Connection string for security database
		/// </summary>
		private string m_sSecureDSN="";
		/// <summary>
		/// Connection string for database
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName="";
		/// <summary>
		/// Enhanced Policy System installed or not
		/// </summary>
		private bool bUseEnhancedPolicySystem=false;
		/// <summary>
		/// Use Specific Supervisor or not
		/// </summary>
		private bool bUseSpecificSupervisor=false; 
		/// <summary>
        /// Use Supervisory Approval for Payments
        /// </summary>
        private bool bUseSupAppPayments = false;
        /// <summary>
        /// Allow Offset
        /// </summary>
        private bool bAllowOffset = false; //mcapps2 MITS 30236
        /// <summary>
		/// Use Queued Payments or not
		/// </summary>
		private bool bUseQueuedPayments=false;
        bool m_bSupervisoryApproval = false;
        bool m_bAccessGrpApprove = false;

		private bool bPlaceSupHoldPayDetailLimits = false;
		//Start: rsushilaggar 05-May-2010 MITS 20606,19970,20938
		private bool m_bUseSupApprovalSearch = false; 
		private bool m_bEnableVoidReason = false;
        private string m_sVoidReason = string.Empty;
        private bool m_bApplyOffSet = false; //mcapps2 MITS 30236
        private bool m_bEnableOffset = false; //mcapps2 MITS 30236
		private string m_sVoidReason_HTMLComments = string.Empty; 
		private bool m_bPutLSScollectionOnHold = false; 
		//End: rsushilaggar 
        //Added by Amitosh for R8 enhancement of OverRide Authority 
        private int m_iOverRideAmount = 0;
        private bool m_bApplyOverRide = false;
        //End Amitosh
		/// <summary>
		/// Type of database ORACLE,SQLSERVER etc.
		/// </summary>
		private long m_lDbMake=0;
		/// <summary>
		/// Manager Id
		/// </summary>
		private long m_lManagerId=0;
		/// <summary>
		/// User Id
		/// </summary>
		private long m_lUserId=0;
		/// <summary>
		/// Group Id
		/// </summary>
		private long m_lGroupId = 0;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = string.Empty;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = string.Empty;

		private DataModelFactory m_objDataModelFactory;
		//smahajan6: Safeway: Payment Supervisory Approval: Start
		/// <summary>
		/// Notify Supervisor Payments or not
		/// </summary>
		private bool m_bPmtNotfyImmSup = false;
		/// <summary>
		/// Private static variable to store Subordinate User List
		/// </summary>
		private static string m_sSubordinateUsersList = string.Empty;
		/// <summary>
		/// Private static variable to store Warning
		/// </summary>
		private static string m_sWarnings = string.Empty;
		/// <summary>
		/// Private variable to store DSN ID
		/// </summary>
		private int m_iDsnId = 0;
		/// <summary>
		/// Private variable to store GROUP ID
		/// </summary>
		private int m_iGroupId = 0;
		/// <summary>
		/// Private variable to store Reason
		/// </summary>
		private string m_sReason = string.Empty;
		//smahajan6: Safeway: Payment Supervisory Approval: End

		private string m_sFilterSql = string.Empty;                      
		//rsushilaggar: MCIC: vendor's Supervisory Approval: Start 05-May-2010 MITS 20606,19970
		private string sOrderBySql = string.Empty;

        /// <summary>
        /// ClientID for multi-tenancy, cloud
        /// </summary>
        private int m_iClientId = 0;

        public string m_slangCode = string.Empty;//vkumar258 ML Changes

		#endregion

		#region Properties Declaration
		/// <summary>
		/// User Id
		/// </summary>
		public long UserId
		{
			set
			{
				m_lUserId=value; 
			}
		}
		/// <summary>
		/// Group Id
		/// </summary>
		//public long GroupId
		//{
		//    set
		//    {
		//        m_lGroupId = value;
		//    }
		//}
		/// <summary>
		/// Manager Id
		/// </summary>
		public long ManagerId
		{
			set
			{
				m_lManagerId=value; 
			}
		}
		/// <summary>
		/// Connection string for security database
		/// </summary>
		public string SecureDSN
		{
			get
			{
				return m_sSecureDSN;
			}
			set
			{
				m_sSecureDSN=value;
			}
		}
		/// <summary>
		/// Connection string for database
		/// </summary>
		public string DSN
		{
			get
			{
				return m_sDSN;
			}
			set
			{
				m_sDSN=value;
			}
		}
		/// <summary>
		/// User name
		/// </summary>
		public string UserName
		{
			get
			{
				return m_sUserName;
			}
			set
			{
				m_sUserName=value;
			}
		}
		//smahajan6: Safeway: Payment Supervisory Approval: Start
		/// <summary>
		/// DSNID
		/// </summary>
		public int DSNID
		{
			get
			{
				return m_iDsnId;
			}
			set
			{
				m_iDsnId = value;
			}
		}
		/// <summary>
		/// Group Id
		/// </summary>
		public int GroupId
		{
			get
			{
				return m_iGroupId;
			}
			set
			{
				m_iGroupId = value;
			}
		}
		/// <summary>
		/// m_sFilterSql
		/// </summary>
		public string sFilterSql
		{
			get
			{
				return m_sFilterSql;
			}
			set
			{
				m_sFilterSql = value;
			}
		}
		/// <summary>
		/// Reason
		/// </summary>
		public string Reason
		{
			get
			{
				return m_sReason;
			}
			set
			{
				m_sReason = value;
			}
		}
		//smahajan6: Safeway: Payment Supervisory Approval: End
		//rsushilaggar: MCIC: vendor's Supervisory Approval: Start 05-May-2010 MITS 20606,19970
		/// <summary>
		///UseSupApprovalSearch
		/// </summary>
		public bool UseSupApprovalSearch
		{
			get
			{
				return m_bUseSupApprovalSearch;
			}
			set
			{
				m_bUseSupApprovalSearch = value;
			}
		}

        /// <summary>
        /// EnableOffset
        /// </summary>
        public bool EnableOffset //mcapps2 MITS 30236 Start
        {
            get
            {
                return m_bEnableOffset;
            }
            set
            {
                m_bEnableOffset = value;
            }
        }  //mcapps2 MITS 30236 End
		/// <summary>
		/// EnableVoidReason
		/// </summary>
		public bool EnableVoidReason
		{
			get
			{
				return m_bEnableVoidReason;
			}
			set
			{
				m_bEnableVoidReason = value;
			}
		}

		/// <summary>
		/// VoidReason
		/// </summary>
		public string VoidReason
		{
			get
			{
				return m_sVoidReason;
			}
			set
			{
				m_sVoidReason = value;
			}
		}

		/// <summary>
		/// VoidReason_HTMLComments
		/// </summary>
		public string VoidReason_HTMLComments
		{
			get
			{
				return m_sVoidReason_HTMLComments;
			}
			set
			{
				m_sVoidReason_HTMLComments = value;
			}
		}
		//rsushilaggar: End

		//Start: rsushilaggar Collections from LSS 06/02/2010 MITS 20938
		/// <summary>
		/// m_bPutLSScollectionOnHold
		/// </summary>
		public bool PutLSScollectionOnHold
		{
			get
			{
				return m_bPutLSScollectionOnHold;
			}
			set
			{
				m_bPutLSScollectionOnHold = value;
			}
		}

		//End: rsushilaggar 

        //Added by Amitosh for R8 enhancement of OverRide Authority
        public int OverRideAmount
        {
            get
            {
                return m_iOverRideAmount;
            }
            set
            {
                m_iOverRideAmount = value;
            }
        }

        public bool ApplyOverRide
        {
            get
            {
                return m_bApplyOverRide;
            }
            set
            {
                m_bApplyOverRide = value;
            }
        }
        //End Amitosh
        public bool ApplyOffSet  //mcapps2 MITS 30236 Start
        {
            get
            {
                return m_bApplyOffSet;
            }
            set
            {
                m_bApplyOffSet = value;
            }
        }  //mcapps2 MITS 30236 End
        //mcapps2 MITS 30236 Start
        public class OffsetStruc  
        {
            public int iClaimId;
            public int iUnitId;
            public int iClaimantEId;
            public int iReserveType;
            public int iRCRowId;
            public double dAmount;
        };
        //mcapps2 MITS 30236 End

		#endregion
		
		#region Constructor
		/// <summary>
		/// SupervisorApproval class constructor
		/// </summary>
		/// <param name="p_sDSN">Connection string to database</param>
		public SupervisorApproval(string p_sDSN, string p_sDsnName , string p_sUserName , string p_sPassword, int p_iClientId)
		{
			DbConnection objTempConn=null;
			try
			{
				// Get supervisory approval options
				objTempConn=DbFactory.GetDbConnection(p_sDSN);
				m_sDSN=p_sDSN;
				objTempConn.Open();
				m_lDbMake=(long)objTempConn.DatabaseType;
				objTempConn.Close();
				//ReadCurrentSystemValues();
				InitCheckOptions();
				objTempConn=null;

				// Initialize data model factory
                m_sUserName = p_sUserName;
                m_sPassword = p_sPassword;
                m_sDsnName = p_sDsnName;
                m_iClientId = p_iClientId; //ash - cloud
				m_objDataModelFactory = new DataModel.DataModelFactory(m_sDsnName , m_sUserName , m_sPassword,m_iClientId);
                setOffsetPermission();  //mcapps2 MITS 30236 
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("SupervisorApproval.Constructor.Error", m_iClientId),p_objException);
			}
			finally
			{
				if (objTempConn!=null)
				{
					objTempConn.Close();
					objTempConn=null;
				}
			}

		}

		public void Dispose()
		{
			try
			{
				if( m_objDataModelFactory != null )
				{
					m_objDataModelFactory.UnInitialize();
					m_objDataModelFactory = null ;	
				}
			}
			catch
			{
			}
		}

		#endregion

		#region Create Report
		/// Name		: CreateReport
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <param name="p_sValue">Value</param>
		/// <param name="p_sFieldName">Field name</param>
		/// <summary>
		/// Creates report based on passed xml document object
		/// </summary>
		/// <param name="p_sXml">Xml string containing report data</param>
		/// <param name="p_objMemory">Memory object containing report</param>
		/// <returns>Success 1 or Failure 0 in execution of the function</returns>
		public int CreateReport(XmlDocument p_objXml,out MemoryStream p_objMemory,bool PaymentFlag)
		{
			Report objReport=null;
			int iReturnValue=0;
			p_objMemory=null;
			try
			{
				//objReport=new Report(m_sDSN);
                objReport = new Report(m_sDSN,m_iClientId);
				//rsushilaggar 06/04/2010 MITS 20938
				objReport.IsPayment = PaymentFlag;
				iReturnValue=objReport.CreateReport(p_objXml.InnerXml,out p_objMemory);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("SupervisorApproval.CreateReport.Error", m_iClientId),p_objException);
			}
			finally
			{
				objReport=null;
			}
			return iReturnValue;
		}
		#endregion

		#region Void Checks
		/// Name		: VoidSuperChecks
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Nullify/Void checks
		/// </summary>
		/// <param name="p_sTransIds">Transaction ids separated by comma</param>
		/// <param name="p_sError">Information returned while executing the function</param>
		/// <returns>Success 1 or Failure 0 in execution of the function</returns>
		public int VoidSuperChecks(string p_sTransIds,out string p_sError)
		{
			string sPrevID = string.Empty;
			string sCurrentId = string.Empty; 
			string[] arrTransIds=null;
			CCacheFunctions objCacheFunctions=null;
			p_sError = string.Empty;
			int iReturnValue=0;
            Funds objFunds = null;
            bool bIsApprove = false;//7810
            ArrayList arrLimitExceed = null;//7810
            FundManager objFundsManager = null;//7810
            int iUserId = 0;  //7810
            int iGroupId = 0;//7810
            UserLogin objUserlogin = null;//7810

			try
			{
                objCacheFunctions = new CCacheFunctions(m_sDSN, m_iClientId);
				arrTransIds=p_sTransIds.Split(",".ToCharArray());
                arrLimitExceed = new ArrayList();
                objUserlogin = new UserLogin(m_sUserName, m_sDsnName, m_iClientId);
                iUserId = objUserlogin.UserId;
                iGroupId = objUserlogin.GroupId;
                objFundsManager = new FundManager(m_sDsnName, m_sUserName, m_sPassword, Conversion.ConvertObjToInt(iUserId, m_iClientId), iGroupId, m_iClientId);//7810

				int iRDStatus = (int)objCacheFunctions.GetCodeIDWithShort("RD","CHECK_STATUS");

				//Create "reversible" DM context transaction.
				DbTransaction objTrans = this.m_objDataModelFactory.Context.TransStart();
				objFunds = this.m_objDataModelFactory.GetDataModelObject("Funds",false) as Funds;
                for (int i = 0; i < arrTransIds.Length; i++)
                {
                    bIsApprove = true;//7810
                    sCurrentId = arrTransIds[i];
                    //smahajan6: Safeway: Payment Supervisory Approval - Approval Authority Validation
                    //strat 7810
                    // if (!sCurrentId.Trim().Equals(sPrevID) && ValidApprovalID(Conversion.ConvertStrToInteger(sCurrentId), arrLimitExceed)) 
                    if (!sCurrentId.Trim().Equals(sPrevID))
                    {

                        objFunds.MoveTo(Conversion.ConvertStrToInteger(sCurrentId));//sachin 7810
                        objFunds.IsHoldReasonUpdateRequired = true;//7810
                        arrLimitExceed = objFundsManager.ValidateLimits(Conversion.ConvertStrToInteger(sCurrentId));//7810
                        //if (objCacheFunctions.GetShortCode(objFunds.StatusCode) == "Q")
                        //{
                            //start 7810
                            //if (objFunds.HoldReasonList.Count > 0)
                            //{
                            // foreach (HoldReason objHldReason in objFunds.HoldReasonList)
                            // {

                            // if (!objHldReason.HoldReasonCode.Equals(Conversion.ConvertObjToInt(objFunds.Context.LocalCache.GetChildCodeIds(objFunds.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objFunds.Context.LocalCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), m_iClientId)))
                            // {
                            //  objHldReason.HoldStatusCode = (int)objCacheFunctions.GetCodeIDWithShort("R", "CHECK_STATUS");
                            //}
                            // else
                            //{
                            for (int j = 0; j < arrLimitExceed.Count; j++)
                            {
                                if (arrLimitExceed[j].ToString() == "ExcPayAuth")
                                    objFunds.IsExcPayAuth = true;
                                else if (arrLimitExceed[j].ToString() == "ExcPayDetailAuth")
                                    objFunds.IsExcPayDetailAuth = true;
                                else if (arrLimitExceed[j].ToString() == "ExcPayClmLmt")
                                    objFunds.IsExcPayClmLmt = true;
                            }


                            //JIRA 7810 start  fix RMA-13278
                            Entity objEntity = null;
                            objEntity = (this.m_objDataModelFactory.GetDataModelObject("Entity", false)) as Entity;
                            int intEntityApprovalStatusCode = 0;
                            intEntityApprovalStatusCode = objFunds.Context.LocalCache.GetCodeId("A", "ENTITY_APPRV_REJ");
                            foreach (FundsXPayee objFundsXPayee in objFunds.FundsXPayeeList)
                            {
                                objEntity.MoveTo(objFundsXPayee.PayeeEid);
                                if (objEntity.EntityApprovalStatusCode != objFunds.Context.LocalCache.GetCodeId("A", "ENTITY_APPRV_REJ"))
                                {
                                    intEntityApprovalStatusCode = objEntity.EntityApprovalStatusCode;
                                }
                                if (intEntityApprovalStatusCode == objFunds.Context.LocalCache.GetCodeId("R", "ENTITY_APPRV_REJ"))
                                    break;
                            }
                            //JIRA 7810 End  fix RMA-13278
                            if (objFunds.HoldReasonList.Count > 0)
                            {
                                foreach (HoldReason objHldReason in objFunds.HoldReasonList)
                                {
                                    if (objHldReason.HoldReasonCode.Equals(Conversion.ConvertObjToInt(objFunds.Context.LocalCache.GetChildCodeIds(objFunds.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objFunds.Context.LocalCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), m_iClientId)))
                                    //in case payee is approved from entity trans is void from approval screen then hold reason must release Payee not approved status
                                    {
                                        if (objHldReason.HoldStatusCode == objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS")
                                          && (intEntityApprovalStatusCode == 0 || intEntityApprovalStatusCode == objFunds.Context.LocalCache.GetCodeId("P", "ENTITY_APPRV_REJ")))
                                        {
                                            bIsApprove = false;
                                            objFunds.IsPayeeUnApproved = true;

                                        }
                                        else if (objHldReason.HoldStatusCode == objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS")
                                             && intEntityApprovalStatusCode == objFunds.Context.LocalCache.GetCodeId("R", "ENTITY_APPRV_REJ"))
                                        {
                                            bIsApprove = false;//7810
                                            objHldReason.HoldReasonCode = Conversion.ConvertObjToInt(objFunds.Context.LocalCache.GetChildCodeIds(objFunds.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objFunds.Context.LocalCache.GetCodeId("PAYEE_REJECT", "HOLD_REASON_PARENT")), m_iClientId);
                                        }
                                    }
                                    // }
                                }
                            }
                        //}
                        //end 7810
                        //objFunds.MoveTo(Conversion.ConvertStrToInteger(arrTransIds[i]));
                        if (ValidApprovalID(Conversion.ConvertStrToInteger(sCurrentId), arrLimitExceed, objFunds))//  7810
                        {
                            objFunds.Reason = m_sReason;//smahajan6: For updating Reason
                            objFunds.VoidFlag = true;
                            //Start: rsushilaggar void payment reason MITS 19970 05/21/2010
                            objFunds.VoidReason = m_sVoidReason;
                            objFunds.VoidReason_HTMLComments = m_sVoidReason_HTMLComments;
                            //End: rsushilaggar 
                            objFunds.ApproveUser = m_sUserName;
                            objFunds.DttmApproval = Conversion.ToDbDateTime(DateTime.Now);
                            objFunds.VoidDate = Conversion.ToDbDate(DateTime.Now);//Nitesh : RM/RMXGaps MITS 6269
                            //if (objCacheFunctions.GetShortCode(objFunds.StatusCode) == "Q") && (bIsApprove) && arrLimitExceed.Count==0) //7810  // queued payment - flip to Review Denied
                           // if ((objCacheFunctions.GetShortCode(objFunds.StatusCode) == "Q") && (bIsApprove) && arrLimitExceed.Count == 0)
                            if ((objCacheFunctions.GetShortCode(objFunds.StatusCode) == "Q") && arrLimitExceed.Count == 0)//JIRA 7810 fix RMA - 13276
                                objFunds.StatusCode = iRDStatus;
                            //JIRA 7810 start  fix RMA-13276
                            else if (arrLimitExceed.Count == 0)//JIRA 7810 fix RMA - 13276
                                objFunds.StatusCode = objCacheFunctions.GetCodeIDWithShort("R", "CHECK_STATUS");
                            //JIRA 7810 end  fix RMA-13276
                            //rsolanki2 : mits 22138 : script were not getting called when checks are voided from here. 

                            // objFunds.Save();  //Note implicit call to "UpdateReserves" here.//7810
                            //Start:Export payment void data to VSS
                            Claim objClaim = null;
                            objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(objFunds.ClaimId);
                            //if (objClaim.VssClaimInd)
                            //Use Utility Check Instead of Claim Check
                            if (objClaim.Context.InternalSettings.SysSettings.EnableVSS.Equals(-1))
                            {
                                VssExportAsynCall objVss = new VssExportAsynCall(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                                objVss.AsynchVssReserveExport(objFunds.ClaimId, objFunds.ClaimantEid, 0, objFunds.TransId, 0, "", "", "", "", "PaymentVoid");
                                objVss = null;
                            }
                            objClaim.Dispose();
                        }
                        //End:Export payment void data to VSS
                        objFunds.FiringScriptFlag = 2;
                        objFunds.Save();//7810
                    }
                    sPrevID = sCurrentId;
                }
				iReturnValue=1;

				//Iff No Errors... Go ahead and commit all of these changes.
				this.m_objDataModelFactory.Context.TransCommit();
			}
            //nehagoel:02062014 : RMA4689 - mits 35272
            catch (RMAppException p_objException)
            {
                //nehagoel:02062014 : RMA4689 - mits 35272  : script were not getting called when checks are approved from here. 
                if (objFunds != null)
                {
                    if (objFunds.Context.ScriptValidationErrors.Count > 0)
                    {
                        Log.Write(p_objException.ToString(), Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                        string sMessage = string.Empty;
                        foreach (string sValue in objFunds.Context.ScriptValidationErrors.Values)
                        {
                            //throw new RMAppException(sValue);
                            if (String.IsNullOrEmpty(sMessage))
                            {
                                sMessage = sValue + "^";
                            }
                            else
                            {
                                sMessage = sMessage + "^" + sValue;
                            }   

                        }
                        throw new RMAppException(sMessage);
                    }

                }
                //end

                throw p_objException;
            }
            //end nehagoel:02062014 : mits
			catch (Exception p_objException)
			{
				this.m_objDataModelFactory.Context.TransRollback();

                throw new RMAppException(Globalization.GetString("SupervisorApproval.VoidSuperChecks.Error", m_iClientId), p_objException);
			}
			finally
			{
				objCacheFunctions=null;
				arrTransIds=null;
			}
			return iReturnValue;
			
		}
		#endregion

		#region Deny Checks
		/// Name		: DenySuperChecks
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deny checks
		/// </summary>
		/// <param name="p_sTransIds">Transaction ids separated by comma</param>
		/// <returns>Success 1 or Failure 0 in execution of the function</returns>
		public int DenySuperChecks(string p_sTransIds)
		{
			int iStatusCode=0; 
			string sPrevID = string.Empty;
			string sCurrentId = string.Empty;
			string[] arrTransIds=null;
			CCacheFunctions objCacheFunctions=null;
			int iReturnValue=0;
            Funds objFunds = null;
            bool bIsApprove = false;//7810
            ArrayList arrLimitExceed = null;//7810
            FundManager objFundsManager = null;//7810
            int iUserId = 0;  //7810
            int iGroupId = 0;//7810
            UserLogin objUserlogin = null;//7810
			try
			{
                objCacheFunctions = new CCacheFunctions(m_sDSN, m_iClientId);
				iStatusCode=(int)objCacheFunctions.GetCodeIDWithShort("RD","CHECK_STATUS");
				arrTransIds=p_sTransIds.Split(",".ToCharArray());
                arrLimitExceed = new ArrayList();//7810
                DbTransaction objTrans = this.m_objDataModelFactory.Context.TransStart();
                objUserlogin = new UserLogin(m_sUserName, m_sDsnName, m_iClientId);
                iUserId = objUserlogin.UserId;
                iGroupId = objUserlogin.GroupId;
                objFundsManager = new FundManager(m_sDsnName, m_sUserName, m_sPassword, Conversion.ConvertObjToInt(iUserId, m_iClientId), iGroupId, m_iClientId);//7810

				objFunds = this.m_objDataModelFactory.GetDataModelObject("Funds",false) as Funds;

				for (int i=0;i<arrTransIds.Length;i++)
				{
                    bIsApprove = true;//JIRA 7810
					sCurrentId=arrTransIds[i];
					if (!sCurrentId.Trim().Equals(sPrevID))
					{
                        objFunds.MoveTo(Conversion.ConvertStrToInteger(sCurrentId));//sachin 7810
                        objFunds.IsHoldReasonUpdateRequired = true;//7810
                        arrLimitExceed = objFundsManager.ValidateLimits(Conversion.ConvertStrToInteger(sCurrentId));//7810
                        //start 7810
                        //if (objFunds.HoldReasonList.Count > 0)
                       // {
                            //foreach (HoldReason objHldReason in objFunds.HoldReasonList)
                           // {
                                //if (!objHldReason.HoldReasonCode.Equals(Conversion.ConvertObjToInt(objFunds.Context.LocalCache.GetChildCodeIds(objFunds.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objFunds.Context.LocalCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), m_iClientId)))
                                //{
                                    //objHldReason.HoldStatusCode = (int)objCacheFunctions.GetCodeIDWithShort("R", "CHECK_STATUS");
                               // }
                        for (int j = 0; j < arrLimitExceed.Count; j++)
                        {
                            if (arrLimitExceed[j].ToString() == "ExcPayAuth")
                                objFunds.IsExcPayAuth = true;
                            else if (arrLimitExceed[j].ToString() == "ExcPayDetailAuth")
                                objFunds.IsExcPayDetailAuth = true;
                            else if (arrLimitExceed[j].ToString() == "ExcPayClmLmt")
                                objFunds.IsExcPayClmLmt = true;
                        }
                        //JIRA 7810 start  fix RMA-13278
                        Entity objEntity = null;
                        objEntity = (this.m_objDataModelFactory.GetDataModelObject("Entity", false)) as Entity;
                        int intEntityApprovalStatusCode = 0;
                        intEntityApprovalStatusCode = objFunds.Context.LocalCache.GetCodeId("A", "ENTITY_APPRV_REJ");
                        foreach (FundsXPayee objFundsXPayee in objFunds.FundsXPayeeList)
                        {
                            objEntity.MoveTo(objFundsXPayee.PayeeEid);
                            if (objEntity.EntityApprovalStatusCode != objFunds.Context.LocalCache.GetCodeId("A", "ENTITY_APPRV_REJ"))
                            {
                                intEntityApprovalStatusCode = objEntity.EntityApprovalStatusCode;
                            }
                            if (intEntityApprovalStatusCode == objFunds.Context.LocalCache.GetCodeId("R", "ENTITY_APPRV_REJ"))
                                break;
                        }
                        //JIRA 7810 End  fix RMA-13278
                        if (objFunds.HoldReasonList.Count > 0)
                        {
                            foreach (HoldReason objHldReason in objFunds.HoldReasonList)
                            {
                                if (objHldReason.HoldReasonCode.Equals(Conversion.ConvertObjToInt(objFunds.Context.LocalCache.GetChildCodeIds(objFunds.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objFunds.Context.LocalCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), m_iClientId)))
                                {
                                    // else
                                    // {
                                    //in case payee is approved from entity trans is deny from approval screen then hold reason must release Payee not approved status
                                    if (objHldReason.HoldStatusCode == objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS")
                                          && (intEntityApprovalStatusCode == 0 || intEntityApprovalStatusCode == objFunds.Context.LocalCache.GetCodeId("P", "ENTITY_APPRV_REJ")))
                                    {
                                        bIsApprove = false;
                                        objFunds.IsPayeeUnApproved = true;

                                    }
                                    else if (objHldReason.HoldStatusCode == objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS")
                                                 && intEntityApprovalStatusCode == objFunds.Context.LocalCache.GetCodeId("R", "ENTITY_APPRV_REJ"))
                                    {
                                        bIsApprove = false;
                                        objHldReason.HoldReasonCode = Conversion.ConvertObjToInt(objFunds.Context.LocalCache.GetChildCodeIds(objFunds.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objFunds.Context.LocalCache.GetCodeId("PAYEE_REJECT", "HOLD_REASON_PARENT")), m_iClientId);
                                    }
                                    // }
                                }
                            }
                        }
                        //objFunds.MoveTo(Conversion.ConvertStrToInteger(arrTransIds[i]));
						//smahajan6: Safeway: Payment Supervisory Approval : Start
                        if (objFunds.AddedByUser == m_sUserName || (ValidApprovalID(Conversion.ConvertStrToInteger(sCurrentId), arrLimitExceed, objFunds)))//Approval Authority Validation
						{
							objFunds.Reason = m_sReason;//smahajan6: For updating Reason
                            objFunds.VoidFlag = true; //rsushilaggar Date 03/11/2011 MITS 24369
                            objFunds.VoidDate = DateTime.Now.ToString();
							objFunds.ApproveUser = m_sUserName;
							objFunds.DttmApproval = Conversion.ToDbDateTime(DateTime.Now);
                            //if (bIsApprove && arrLimitExceed.Count==0)
                            //if (arrLimitExceed.Count == 0)//JIRA 7810 fix RMA - 13276//RMA-14281
                            //{
                                objFunds.StatusCode = iStatusCode;
                                //}//RMA-14281
                           //nehagoel:02062014 : RMA4689 - mits 35272 : script were not getting called when checks are approved from here. 
                            
                                //nehagoel--end mits for scripts
							//objFunds.Save();//7810
                            //Start:Export payment void data to VSS
                            Claim objClaim = null;
                            objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(objFunds.ClaimId);
                            //if (objClaim.VssClaimInd)
                            //Use utility check instead of Claim check
                            if(objClaim.Context.InternalSettings.SysSettings.EnableVSS.Equals(-1))
                            {
                                VssExportAsynCall objVss = new VssExportAsynCall(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                                objVss.AsynchVssReserveExport(objFunds.ClaimId, objFunds.ClaimantEid, 0, objFunds.TransId, 0, "","","","","PaymentVoid");
                                objVss = null;
                            }
                            objClaim.Dispose();
                            //End:Export payment void data to VSS
						}
						//smahajan6: Safeway: Payment Supervisory Approval : End
                        objFunds.FiringScriptFlag = 2;
                        objFunds.Save();//7810
					}
					sPrevID = sCurrentId;
				}
				this.m_objDataModelFactory.Context.TransCommit();

				iReturnValue=1;
			}
            //added by amitosh for mits 24446 (03/30/2011)
            catch (RMAppException p_objException)
            {
               //nehagoel:02062014 : RMA4689 - mits 35272: script were not getting called when checks are approved from here. 
                if (objFunds != null)
                {
                    if (objFunds.Context.ScriptValidationErrors.Count > 0)
                    {
                        Log.Write(p_objException.ToString(), Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                        string sMessage = string.Empty;
                        foreach (string sValue in objFunds.Context.ScriptValidationErrors.Values)
                        {
                            if (String.IsNullOrEmpty(sMessage))
                            {
                                sMessage = sValue + "^";
                            }
                            else
                            {
                                sMessage = sMessage + "^" + sValue;
                            }                           
                        }
                        throw new RMAppException(sMessage);
                    }

                }
                //end
                throw p_objException;
            }
            //end amitosh
			catch (Exception p_objException)
			{
				this.m_objDataModelFactory.Context.TransRollback();

                throw new RMAppException(Globalization.GetString("SupervisorApproval.DenySuperChecks.Error", m_iClientId), p_objException);
			}
			finally
			{
				objCacheFunctions=null;
				arrTransIds=null;
			}
			return iReturnValue;
			
		}
		#endregion

		#region Approve Checks
		/// Name		: ApproveSuperChecks
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Approve checks
		/// </summary>
		/// <param name="p_sTransIds">Transaction ids separated by comma</param>
		/// <returns>Success 1 or Failure 0 in execution of the function</returns>
		public int ApproveSuperChecks(string p_sTransIds)
		{
			int iStatusCode=0; 
			string sPrevID = string.Empty;
			string sCurrentId = string.Empty;
			string[] arrTransIds=null;
			CCacheFunctions objCacheFunctions=null;
			int iReturnValue=0;
            bool bIsGroup = false;
            int iUserId = 0;  //mcapps2 MITS 30236
            int iGroupId = 0;  //mcapps2 MITS 30236  
            int iManagerGroupId = 0;  //mcapps2 MITS 30236
            int iManagerID = 0;  //mcapps2 MITS 30236
            UserLogin objUserlogin = null;  //mcapps2 MITS 30236
            Funds objFunds = null;
            bool bIsApprove = false;//7810
            ArrayList arrLimitExceed = null;//7810
            FundManager objFundsManager = null;//7810
            try
            {
                objCacheFunctions = new CCacheFunctions(m_sDSN, m_iClientId);
                iStatusCode = (int)objCacheFunctions.GetCodeIDWithShort("R", "CHECK_STATUS");
                  objUserlogin = new UserLogin(m_sUserName, m_sDsnName, m_iClientId);                            
                                iUserId = objUserlogin.UserId;
                                iGroupId = objUserlogin.GroupId;
                                objFundsManager = new FundManager(m_sDsnName, m_sUserName, m_sPassword, Conversion.ConvertObjToInt(iUserId, m_iClientId), iGroupId, m_iClientId);//7810

                arrLimitExceed = new ArrayList();//7810
                //Create "reversible" DM context transaction.
                DbTransaction objTrans = this.m_objDataModelFactory.Context.TransStart();
                objFunds = this.m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds;

                arrTransIds = p_sTransIds.Split(",".ToCharArray());

                for (int i = 0; i < arrTransIds.Length; i++)
                {
                    bIsApprove = true;//JIRA 7810
                    sCurrentId = arrTransIds[i];
                    //smahajan6: Safeway: Payment Supervisory Approval - Approval Authority Validation
                   // if (!sCurrentId.Trim().Equals(sPrevID)) && ValidApprovalID(Conversion.ConvertStrToInteger(sCurrentId)))//7810
                    if (!sCurrentId.Trim().Equals(sPrevID))//&& ValidApprovalID(Conversion.ConvertStrToInteger(sCurrentId)))//7810
                    {
                        //JIRA 7810 start 
                        objFunds.MoveTo(Conversion.ConvertStrToInteger(sCurrentId));
                        objFunds.IsHoldReasonUpdateRequired = true;//7810
                        arrLimitExceed = objFundsManager.ValidateLimits(Conversion.ConvertStrToInteger(sCurrentId));//7810
                        //start 7810
                        // if (objFunds.HoldReasonList.Count > 0)
                        // {
                        // foreach (HoldReason objHldReason in objFunds.HoldReasonList)
                        // {
                        // if (!objHldReason.HoldReasonCode.Equals(Conversion.ConvertObjToInt(objFunds.Context.LocalCache.GetChildCodeIds(objFunds.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objFunds.Context.LocalCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), m_iClientId)))
                        // {
                        for (int j = 0; j < arrLimitExceed.Count; j++)
                        {
                            if (arrLimitExceed[j].ToString() == "ExcPayAuth")
                                objFunds.IsExcPayAuth = true;
                            else if (arrLimitExceed[j].ToString() == "ExcPayDetailAuth")
                                objFunds.IsExcPayDetailAuth = true;
                            else if (arrLimitExceed[j].ToString() == "ExcPayClmLmt")
                                objFunds.IsExcPayClmLmt = true;
                        }
                        //JIRA 7810 start  fix RMA-13278
                        Entity objEntity = null;
                        objEntity = (this.m_objDataModelFactory.GetDataModelObject("Entity", false)) as Entity;
                        int intEntityApprovalStatusCode = 0;
                        int intPayeecount = 0;
                        intEntityApprovalStatusCode = objFunds.Context.LocalCache.GetCodeId("A", "ENTITY_APPRV_REJ");
                        foreach (FundsXPayee objFundsXPayee in objFunds.FundsXPayeeList)
                        {
                            objEntity.MoveTo(objFundsXPayee.PayeeEid);
                           
                            if (objEntity.EntityApprovalStatusCode != objFunds.Context.LocalCache.GetCodeId("A", "ENTITY_APPRV_REJ"))
                            {
                                intEntityApprovalStatusCode = objEntity.EntityApprovalStatusCode;
                            }
                            if (intEntityApprovalStatusCode == objFunds.Context.LocalCache.GetCodeId("R", "ENTITY_APPRV_REJ"))
                                break;
                        }
                        //JIRA 7810 End  fix RMA-13278
                        if (objFunds.HoldReasonList.Count > 0)
                        {
                            foreach (HoldReason objHldReason in objFunds.HoldReasonList)
                            {

                                if (objHldReason.HoldReasonCode.Equals(Conversion.ConvertObjToInt(objFunds.Context.LocalCache.GetChildCodeIds(objFunds.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objFunds.Context.LocalCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), m_iClientId)))
                                {
                                    if ((objHldReason.HoldStatusCode == objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS"))
                                       && (intEntityApprovalStatusCode == 0 || intEntityApprovalStatusCode == objFunds.Context.LocalCache.GetCodeId("P", "ENTITY_APPRV_REJ")))
                                    {
                                        bIsApprove = false;
                                        objFunds.IsPayeeUnApproved = true;

                                    }
                                    else if (objHldReason.HoldStatusCode == objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS")
                                   && intEntityApprovalStatusCode == objFunds.Context.LocalCache.GetCodeId("R", "ENTITY_APPRV_REJ"))
                                    {
                                        bIsApprove = false;//7810
                                        objHldReason.HoldReasonCode = Conversion.ConvertObjToInt(objFunds.Context.LocalCache.GetChildCodeIds(objFunds.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objFunds.Context.LocalCache.GetCodeId("PAYEE_REJECT", "HOLD_REASON_PARENT")), m_iClientId);
                                    }
                                    break;
                                }
                            }
                        }
                        //JIRA 7810 end 
                        if (ValidApprovalID(Conversion.ConvertStrToInteger(sCurrentId), arrLimitExceed, objFunds))//  7810
                        {
                            objFunds.Reason = m_sReason;//smahajan6: For updating Reason
                            objFunds.VoidFlag = false;
                            //SMISHRA54: MITS #24807, Void date should be null when approval is taking place.
                            objFunds.VoidDate = null;
                            objFunds.ApproveUser = m_sUserName;
                            objFunds.DttmApproval = Conversion.ToDbDateTime(DateTime.Now);
                            if ((bIsApprove) && arrLimitExceed.Count==0)//JIRA 7810
                        {
                            objFunds.StatusCode = iStatusCode;
                        }
                        //Added by Amitosh for R8 enhancement of OverRide Authority Check

                        if (m_bApplyOverRide && CheckForOverRideAuthority(arrTransIds[i],objFunds.ClaimId,ref bIsGroup))
                        {
                            SaveOverRideAuthority(objFunds.ClaimId, objFunds.AddedByUser, objFunds.Amount, objFunds.ApproverId,arrTransIds[i],bIsGroup);
                        }
                        //End Amitosh
                        //mcapps2 MITS 30236 Start
                        if (m_bApplyOffSet)
                        {
                            if ((objFunds.PaymentFlag == true) && (objFunds.Amount > 0) && (objFunds.OffSetFlag == false))
                            {
                                Claim objClaim = this.m_objDataModelFactory.GetDataModelObject("Claim", false) as Claim;
                                objClaim.MoveTo(objFunds.ClaimId);
                                ReserveCurrentList objResCurrList = objClaim.ReserveCurrentList;
                                objUserlogin = new UserLogin(m_sUserName, m_sDsnName, m_iClientId);
                                iManagerGroupId = objUserlogin.GroupId;
                                iUserId = objUserlogin.UserId;
                                iGroupId = objUserlogin.GroupId;
                                iManagerID = GetManagerID(iUserId);
                                ReserveFunds objReserveFunds = new ReserveFunds(DbFactory.GetDbConnection(m_sDSN).ConnectionString.ToString(), objUserlogin, m_iClientId);//rkaur27
                                //objReserveFunds.OffSetPayment(objFunds, m_iGroupId, iUserId, iManagerID, objUserlogin, m_sDsnName);
                                objReserveFunds.OffSetPayment(objFunds);
                            }
                        }
                                               //nehagoel:02062014 : RMA4689 - mits 35272: script were not getting called when checks are approved from here. 
                        //mcapps2 MITS 30236 End
                            //objFunds.Save();  //Note implicit call to "UpdateReserves" here.
                        }
                        objFunds.FiringScriptFlag = 2; 
                        objFunds.Save();
                    }
                    sPrevID = sCurrentId;
                }
                //Iff No Errors... Go ahead and commit all of these changes.
                this.m_objDataModelFactory.Context.TransCommit();

                iReturnValue = 1;
            }
                //added by amitosh for mits 24446 (03/30/2011)
            catch (RMAppException p_objException)
            {
               //nehagoel:02062014 : RMA4689 - mits 35272 : script were not getting called when checks are approved from here. 
                if (objFunds!=null)
                {
                    if (objFunds.Context.ScriptValidationErrors.Count>0)
                    {
                        Log.Write(p_objException.ToString(), Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                        string sMessage = string.Empty;
                        foreach (string sValue in objFunds.Context.ScriptValidationErrors.Values)
                        {
                            if (String.IsNullOrEmpty(sMessage))
                            {
                                sMessage = sValue + "^";
                            }
                            else
                            {
                                sMessage = sMessage + "^" + sValue;
                            }
                        }
                        throw new RMAppException(sMessage);
                    }
                    
                }
                //end
                throw p_objException;
            }
                //end amitosh

            catch (Exception p_objException)
            {
                this.m_objDataModelFactory.Context.TransRollback();

                throw new RMAppException(Globalization.GetString("SupervisorApproval.ApproveSuperChecks.Error", m_iClientId), p_objException);
            }
			finally
			{
                objCacheFunctions = null;
                arrTransIds = null;
                objUserlogin = null;
			}
			return iReturnValue;
		}
		#endregion

		#region Common Functions
		/// Name		: CloseReader
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Closes a passed DbReader
		/// </summary>
		/// <param name="p_objReader">DbReader</param>
		private void CloseReader(ref DbReader p_objReader)
		{
			if (p_objReader!=null)
			{
				if (!p_objReader.IsClosed)
				{
					p_objReader.Close();
				}
				p_objReader=null;
			}
		}
		#endregion

		#region Read System Values
		/// Name		: ReadCurrentSystemValues
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Reads Wizard's system level values
		/// </summary>
		private void ReadCurrentSystemValues()
		{
			SysSettings objSettings=null;
			try
			{
				objSettings=new SysSettings(m_sDSN,m_iClientId); //Ash - cloud
                bUseEnhancedPolicySystem = Conversion.ConvertLongToBool((long)objSettings.UseEnhPolFlag, m_iClientId);
				objSettings=null;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.ReadCurrentSystemValues.Error", m_iClientId), p_objException);
			}
			finally
			{
				objSettings=null;
			}
		}

        private void setOffsetPermission()   //mcapps2 MITS 30236 Start
        {
            UserLogin objUserlogin = null;
            int RMO_OFFSET = 47;
            int RMB_FUNDS_TRANSACT = 9650;

            try
            {

                objUserlogin = new UserLogin(m_sUserName, m_sDsnName, m_iClientId);

                if (objUserlogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_OFFSET))
                {
                    m_bEnableOffset = true;
                }
                else
                {
                    m_bEnableOffset = false;
                }
            }
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			finally
			{
                objUserlogin = null;
			}
        }  //mcapps2 MITS 30236 End



		/// Name		: InitCheckOptions
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Reads Check related options
		/// </summary>
		private void InitCheckOptions()
		{
			DbReader objRead=null;
			StringBuilder sbSQL=null;
			try
			{
				sbSQL=new StringBuilder();
                sbSQL.Append("SELECT SH_DIARY,PMT_NOTFY_IMM_SUP,QUEUED_PAYMENTS,USER_SPECIFIC_FLAG,USE_SUP_APPROVAL_SEARCH,ENABLE_VOID_REASON,LSS_COL_ON_HOLD, ALLOW_SUP_GRP_APPROVE,USE_SUP_APP_PAYMENTS FROM CHECK_OPTIONS");//smahajan6: Safeway: Payment Supervisory Approval
				objRead=DbFactory.GetDbReader(m_sDSN,sbSQL.ToString());
				if (objRead.Read())
				{
					bUseQueuedPayments=Conversion.ConvertObjToBool(
                        objRead.GetValue("QUEUED_PAYMENTS"), m_iClientId);
					bUseSpecificSupervisor=Conversion.ConvertObjToBool(
                        objRead.GetValue("USER_SPECIFIC_FLAG"), m_iClientId);
                    bUseSupAppPayments = Conversion.ConvertObjToBool(objRead.GetValue("USE_SUP_APP_PAYMENTS"), m_iClientId);
					m_bPmtNotfyImmSup = Conversion.ConvertObjToBool(
                        objRead.GetValue("PMT_NOTFY_IMM_SUP"), m_iClientId);//smahajan6: Safeway: Payment Supervisory Approval
					//rsushilaggar:  Vendor's Supervisory Approval 05-May-2010 MITS 20606
					m_bUseSupApprovalSearch = Conversion.ConvertObjToBool(
                        objRead.GetValue("USE_SUP_APPROVAL_SEARCH"), m_iClientId);
					//rsushilaggar:  Void Payment Reason MITS 19970 05/21/2010
					m_bEnableVoidReason = Conversion.ConvertObjToBool(
                        objRead.GetValue("ENABLE_VOID_REASON"), m_iClientId);
					//rsushilaggar:  Collections from LSS  06/02/2010 MITS 20938
					m_bPutLSScollectionOnHold = Conversion.ConvertObjToBool(
                        objRead.GetValue("LSS_COL_ON_HOLD"), m_iClientId);
                    //rsushilaggar MITS 26332 Date 11/28/2011
                    m_bAccessGrpApprove= Conversion.ConvertObjToBool(
                        objRead.GetValue("ALLOW_SUP_GRP_APPROVE"), m_iClientId);
                    m_bSupervisoryApproval = Conversion.ConvertObjToBool(
                        objRead.GetValue("USE_SUP_APP_PAYMENTS"), m_iClientId);
				}
				else
				{
					bUseQueuedPayments=false;
					bUseSpecificSupervisor=false;
					m_bPmtNotfyImmSup = false;//smahajan6: Safeway: Payment Supervisory Approval
				}
				CloseReader(ref objRead);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.InitCheckOptions.Error", m_iClientId), p_objException);
			}
			finally
			{
				CloseReader(ref objRead);
				sbSQL=null;
			}
		}
		#endregion

		#region Log Event to Diary
		/// Name		: LogEvent
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Logs an event denoted by a 'topic' and a 'message body' to the diary
		/// </summary>
		/// <param name="p_sDiaryUser">User name</param>
		/// <param name="p_sTopic">Topic</param>
		/// <param name="p_sRegarding">Regarding</param>
		/// <param name="p_sMsg">Message</param>
		/// <param name="p_sDate">Date</param>
		private void LogEvent(string p_sDiaryUser,string p_sTopic,string p_sRegarding,string p_sMsg,string p_sDate)
		{
			//Logs an event denoted by a 'topic' and a 'message body' to the diary.
			StringBuilder sbSQL=null;
			string sTempTopic = string.Empty;
			string sTempMsg = string.Empty;
			DbConnection objConn=null;
			string sTempRegarding = string.Empty;
			try
			{
				if (p_sTopic.Length > 50)
				{
					sTempTopic=p_sTopic.Substring(0,50);
				}
				else
				{
					sTempTopic=p_sTopic;
				}
				sTempTopic=Utilities.FormatSqlFieldValue(sTempTopic);
				if (p_sMsg.Length > 200)
				{
					sTempMsg=p_sMsg.Substring(0,200);
				}
				else
				{
					sTempMsg=p_sMsg;
				}
				sTempMsg=Utilities.FormatSqlFieldValue(sTempMsg);
				if (p_sRegarding.Length > 70)
				{
					sTempRegarding=p_sRegarding.Substring(0,70);
				}
				else
				{
					sTempRegarding=p_sRegarding;
				}
				sTempRegarding=Utilities.FormatSqlFieldValue(sTempRegarding);
				sbSQL=new StringBuilder();
				sbSQL.Append("INSERT INTO WPA_DIARY_ENTRY (ENTRY_ID,ENTRY_NAME,ENTRY_NOTES,CREATE_DATE,PRIORITY,");
				sbSQL.Append("  STATUS_OPEN,AUTO_CONFIRM,COMPLETE_TIME,ASSIGNED_USER,ASSIGNING_USER,ASSIGNED_GROUP,");
				sbSQL.Append("  IS_ATTACHED,REGARDING,COMPLETE_DATE,NOTIFY_FLAG,DIARY_VOID,ROUTE_FLAG,DIARY_DELETED,TE_TRACKED)");
                sbSQL.Append(" VALUES (" + Utilities.GetNextUID(m_sDSN, "WPA_DIARY_ENTRY", m_iClientId) + "," + sTempTopic + "," + sTempMsg);
				sbSQL.Append(",'"+ Conversion.ToDbDateTime(DateTime.Now)+"',1,1,0,'"+Conversion.GetTime(DateTime.Now.ToString())+"','"+p_sDiaryUser);
				sbSQL.Append("','" + p_sDiaryUser + "',' ',");
				sbSQL.Append("0,"+sTempRegarding+",'"+p_sDate + "',0,");
				sbSQL.Append("0,0,0,0)");
				objConn=DbFactory.GetDbConnection(m_sDSN);
				objConn.Open();
				objConn.ExecuteNonQuery(sbSQL.ToString());
				objConn.Close();
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.LogEvent.Error", m_iClientId), p_objException);
			}
			finally
			{
				if (objConn!=null)
				{
					objConn.Close();
					objConn=null;
				}
				sbSQL=null;
			}
		
		}
		#endregion

		#region List Funds Transactions
		/// Name		: GetTopApprovers
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets Top Approvers for LOB's
		/// </summary>
		/// <param name="p_lTop">LOB</param>
		/// <param name="p_lWC">LOB WC</param>
		/// <param name="p_lGC">LOB GC</param>
		/// <param name="p_lVA">LOB VA</param>
		/// <param name="p_lDI">LOB DI</param>
		/// <returns>Success - True or Failure- False</returns>
		private bool GetTopApprovers(ref long p_lTop, ref long p_lWC, ref long p_lGC, ref long p_lVA, ref long p_lDI) 
		{
			string sSQL = string.Empty;
			long lLobCode=0;
			DbReader objRead=null;
			bool bRetValue=false;
			try
			{
				sSQL = "SELECT LOB_CODE,USER_ID FROM LOB_SUP_APPROVAL";
				objRead=DbFactory.GetDbReader(m_sDSN,sSQL);
			
				while (objRead.Read())
				{
                    lLobCode = Conversion.ConvertObjToInt64(objRead.GetValue("LOB_CODE"), m_iClientId);
					switch(lLobCode)
					{
						case 242:
                            p_lVA = Conversion.ConvertObjToInt64(objRead.GetValue("USER_ID"), m_iClientId);
							if (p_lVA==m_lUserId)
							{
								bRetValue= true;
							}
							break;
						case 241:
                            p_lGC = Conversion.ConvertObjToInt64(objRead.GetValue("USER_ID"), m_iClientId);
							if (p_lGC==m_lUserId)
							{
								bRetValue= true;
							}
							break;
						case 243:
                            p_lWC = Conversion.ConvertObjToInt64(objRead.GetValue("USER_ID"), m_iClientId);
							if (p_lWC==m_lUserId)
							{
								bRetValue= true;
							}
							break;
						case 844:
                            p_lDI = Conversion.ConvertObjToInt64(objRead.GetValue("USER_ID"), m_iClientId);
							if (p_lDI==m_lUserId)
							{
								bRetValue= true;
							}
							break;
						default:
                            p_lTop = Conversion.ConvertObjToInt64(objRead.GetValue("USER_ID"), m_iClientId);
							if (p_lTop==m_lUserId)
							{
								bRetValue= true;
							}
							break;
					}//end switch
				}//end while
				CloseReader(ref objRead);
				
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.GetTopApprovers.Error", m_iClientId), p_objException);
			}
			finally
			{
				CloseReader(ref objRead);
			}
			return bRetValue;

		}
		/// Name		: GetMaxAmount
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get Max amount for a user to approve
		/// </summary>
		/// <param name="p_lUserId">User id</param>
		/// <param name="p_iLineOfBus">LOB</param>
		/// <returns>Maximum amount user can approve</returns>
		private double GetMaxAmount(long p_lUserId,int p_iLineOfBus)
		{
			double dMaxAmount=0;
			DbReader objRead=null;
			StringBuilder sbSQL=null;
			try
			{
				sbSQL=new StringBuilder();
				sbSQL.Append("SELECT MAX_AMOUNT FROM FUNDS_LIMITS WHERE FUNDS_LIMITS.USER_ID = " + p_lUserId.ToString() + " AND FUNDS_LIMITS.LINE_OF_BUS_CODE = " + p_iLineOfBus.ToString());
				objRead=DbFactory.GetDbReader(m_sDSN,sbSQL.ToString());
				if (objRead.Read())
				{
                    dMaxAmount = Conversion.ConvertObjToDouble(objRead.GetValue("MAX_AMOUNT"), m_iClientId);
				}
				else
				{
					dMaxAmount=-1;
				}
				CloseReader(ref objRead);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.GetMaxAmount.Error", m_iClientId), p_objException);
			}
			finally
			{
				CloseReader(ref objRead);
				sbSQL=null;
			}
			return dMaxAmount;
		}
		/// Name		: DownSupChain
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves the supervisor chain from user id (passed in) down in other words it gets all the users who have the user id (passed in) as a supervisor somewhere above them.
		/// </summary>
		/// <param name="p_lUserId">User id</param>
		/// <returns>Supervisor chain</returns>
		private string DownSupChain(long p_lUserId,string p_sShowAllItem)
		{
			//loops through user table and retrieves the supervisor chain from user id (passed in) down in other words it gets all the users who have the user id (passed in) as a supervisor somewhere above them.
			string sUserSQL = string.Empty;
			StringBuilder sbSQL=null;
			string sUsers = string.Empty;
			string sTransIDList = string.Empty; 
			int iApprovalDays=0; 
			long lTotalDays=0;
			string sTransDate = string.Empty;
			int iCount=0; 
			bool bLOBOrTopUser=false;
            bool bNoAssignedDate = false;
			long lTop=0; 
			long lWC=0; 
			long lGC=0; 
			long lVA=0; 
			long lDI=0; 
			StringBuilder sbLOBUSers=null; 
			StringBuilder sbTransIDSQL=null;
			string sLOBUSers = string.Empty;
			string sStartList = string.Empty;
			string sWhichLOB=string.Empty;
			DbReader objRead=null;
			DbReader objTempRead=null;
			string sSQL = string.Empty;
			DateTime datTemp;
			CCacheFunctions objCacheFunctions=null;
			//smahajan6: Safeway: Payment Supervisory Approval : Start
			string sAssignedDateTime = string.Empty;
			DateTime dStartDateTime;
			DateTime dEndDateTime;
			DateTime dCurrentDateTime = DateTime.Now;
			int iApprovalHours = 0;
			int iManagerId = 0;
			int iDaysCount = 0;
			int iHoursCount = 0;
            //Start: rsushilaggar 11/23/2011 MITS-26332
            bool bSupervisoryApproval = false;
            bool bAccessGrpApprove = false;
            bool bTempValue = false;
			//smahajan6: Safeway: Payment Supervisory Approval : End
			try
			{
				//Check to see who the top level users are
				bLOBOrTopUser = GetTopApprovers(ref lTop, ref lWC, ref lGC, ref lVA, ref lDI);
				//We get the number of days a payment is allowed to set waiting for a user to approve it.
                sSQL = "SELECT DAYS_FOR_APPROVAL,HOURS_FOR_APPROVAL,ALLOW_SUP_GRP_APPROVE,USE_SUP_APP_PAYMENTS FROM CHECK_OPTIONS";//smahajan6: Safeway: Payment Supervisory Approval
				objRead=DbFactory.GetDbReader(m_sDSN,sSQL);
				if(objRead.Read())
				{
					iApprovalDays=Common.Conversion.ConvertStrToInteger(
						Common.Conversion.ConvertObjToStr(objRead.GetValue("DAYS_FOR_APPROVAL")));
					iApprovalHours = Common.Conversion.ConvertStrToInteger(
						Common.Conversion.ConvertObjToStr(objRead.GetValue("HOURS_FOR_APPROVAL")));//smahajan6: Safeway: Payment Supervisory Approval
                    //Start: rsushilaggar 11/23/2011 MITS-26332
                    bSupervisoryApproval = Common.Conversion.ConvertObjToBool(objRead.GetValue("USE_SUP_APP_PAYMENTS"), m_iClientId);
                    bAccessGrpApprove = Common.Conversion.ConvertObjToBool(objRead.GetValue("ALLOW_SUP_GRP_APPROVE"), m_iClientId);
                    //End rsushilaggar
				}
				else
				{
					iApprovalDays=0;
					iApprovalHours = 0;//smahajan6: Safeway: Payment Supervisory Approval
				}
				CloseReader(ref objRead);
				sbSQL=new StringBuilder();
                objCacheFunctions = new CCacheFunctions(m_sDSN, m_iClientId);
                if (m_lUserId == 0)
                {
                    return string.Empty;
                }
                else
                {
                    //Get transactions assigned to current user
                    sbSQL.Append("SELECT TRANS_ID FROM FUNDS WHERE APPROVER_ID = " + m_lUserId.ToString());
                    if (bUseQueuedPayments)
                    {

                        sbSQL.Append(" AND ((FUNDS.STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString()
                            + " AND VOID_FLAG = 0) OR (FUNDS.STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS").ToString() + "AND VOID_FLAG <> 0))");
                    }
                    else
                    {
                        sbSQL.Append(" AND FUNDS.STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                        sbSQL.Append(" AND FUNDS.PAYMENT_FLAG <> 0 AND FUNDS.VOID_FLAG = 0");
                    }
                    sbSQL.Append(" AND FUNDS.PAYMENT_FLAG <> 0");
                    sbSQL.Append(" AND FUNDS.ADDED_BY_USER <> " + Utilities.FormatSqlFieldValue(m_sUserName));
                    objRead = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());

                    while (objRead.Read())
                    {
                        //smahajan6: Safeway: Payment Supervisory Approval : Start
                        sAssignedDateTime = "";
                        sSQL = " SELECT DTTM_APPROVAL_CHGD FROM PMT_APPROVAL_HIST WHERE APPROVER_ID = " + m_lUserId
                               + " AND TRANS_ID = " + objRead.GetValue("TRANS_ID").ToString() + " ORDER BY DTTM_APPROVAL_CHGD DESC";

                        using (DbReader objReader = DbFactory.GetDbReader(m_sDSN, sSQL.ToString()))
                        {
                            if (objReader.Read())
                            {
                                sAssignedDateTime = objReader.GetValue("DTTM_APPROVAL_CHGD").ToString();
                            }
                            else
                            {
                                bNoAssignedDate = true;
                            }
                            objReader.Close();
                        }
                        datTemp = ToDateTime(sAssignedDateTime);
                        dStartDateTime = datTemp;
                        dEndDateTime = dStartDateTime.AddDays(iApprovalDays).AddHours(iApprovalHours);
                        //smahajan6: Safeway: Payment Supervisory Approval : End
                        //smahajan6: Safeway: Payment Supervisory Approval - Time Validation Check

                        //Start rsusilaggar MITS 26332 11/23/2011
                        if (bSupervisoryApproval)
                        {
                            bTempValue = true;
                        }
                        else
                        {
                            bTempValue = (dEndDateTime >= dCurrentDateTime || dEndDateTime == dStartDateTime || (lTop == m_lUserId));
                        }
                        //if (dEndDateTime >= dCurrentDateTime || dEndDateTime == dStartDateTime || (lTop == m_lUserId))
                        //{
                        if (bTempValue)
                        {
                            if ((!sTransIDList.Trim().Equals("")))
                            {
                                if ((!sTransIDList.Substring(sTransIDList.Length - 1).Equals(",")))
                                {
                                    sTransIDList = sTransIDList + ",";
                                }
                            }
                            sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID")) + ",";
                        }

                    }
                    CloseReader(ref objRead);
                    if (p_sShowAllItem.Equals("True"))//vkumar258 RMA-12428
                    {
                        //}	Commented by Mohit
                        //The top level user is logged on we will get the checks that are assigned to the lob users and have been waiting for three days
                        if (bLOBOrTopUser && lTop == m_lUserId)
                        {
                            sLOBUSers = "";
                            sbLOBUSers = new StringBuilder();
                            if (lWC != 0)
                            {
                                sbLOBUSers.Append(lWC + ",");
                            }
                            if (lDI != 0)
                            {
                                sbLOBUSers.Append(lDI + ",");
                            }
                            if (lGC != 0)
                            {
                                sbLOBUSers.Append(lGC + ",");
                            }
                            if (lVA != 0)
                            {
                                //sbLOBUSers.Append(lVA+",");	Commented by Mohit
                                sbLOBUSers.Append(lVA);
                            }
                            sLOBUSers = sbLOBUSers.ToString();
                            if (sLOBUSers.Trim().Length > 0)
                            {
                                if (sLOBUSers.Substring(sLOBUSers.Length - 1).Equals(","))
                                {
                                    sLOBUSers = sLOBUSers.Substring(0, sLOBUSers.Length - 1);
                                }
                            }
                            sbSQL.Remove(0, sbSQL.Length);
                            /*************Start: Mohit Yadav: Safeway: Getting Error if specified TOP level supervisor 
                             **but none of LOB level supervisor. Added 'if' condition to check this sql error***********/
                            if (sLOBUSers != "")
                            {
                                sbSQL.Append("SELECT TRANS_ID, TRANS_DATE FROM FUNDS WHERE APPROVER_ID In (" + sLOBUSers + ")");
                                if (bUseQueuedPayments)
                                {

                                    sbSQL.Append(" AND ((FUNDS.STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString()
                                        + " AND VOID_FLAG = 0) OR (FUNDS.STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS").ToString() + " AND VOID_FLAG <> 0))");
                                }
                                else
                                {
                                    sbSQL.Append(" AND FUNDS.STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                                    sbSQL.Append(" AND FUNDS.PAYMENT_FLAG <> 0 AND FUNDS.VOID_FLAG = 0");
                                }
                                //rsushilaggar 06/02/2010 MITS 20938
                                if (!m_bPutLSScollectionOnHold)
                                    sbSQL.Append("AND FUNDS.PAYMENT_FLAG <> 0");
                                sbSQL.Append(" AND FUNDS.ADDED_BY_USER <> " + Utilities.FormatSqlFieldValue(m_sUserName));
                                objRead = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                                while (objRead.Read())
                                {
                                    sTransDate = Conversion.ConvertObjToStr(objRead.GetValue("TRANS_DATE"));
                                    datTemp = Conversion.ToDate(Conversion.ConvertObjToStr(sTransDate));
                                    if (iApprovalDays != 0)
                                    {
                                        datTemp = datTemp.AddDays(iApprovalDays);
                                    }
                                    else
                                    {
                                        datTemp = DateTime.Now;
                                    }
                                    if (datTemp <= DateTime.Now)
                                    {
                                        if ((!sTransIDList.Trim().Equals("")))
                                        {
                                            if ((!sTransIDList.Substring(sTransIDList.Length - 1).Equals(",")))
                                            {
                                                sTransIDList = sTransIDList + ",";
                                            }
                                        }
                                        //smahajan6: Safeway: Payment Supervisory Approval - commented
                                        sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID")) + ",";
                                    }
                                }//end while
                                CloseReader(ref objRead);
                            }
                            /*************End: Mohit Yadav: Safeway: Ending 'if' condition specified above**/
                            sLOBUSers = string.Empty;
                            sbLOBUSers = new StringBuilder();
                            if (lWC != 0)
                            {
                                sbLOBUSers.Append(lWC + ",");
                            }
                            if (lDI != 0)
                            {
                                sbLOBUSers.Append(lDI + ",");
                            }
                            if (lGC != 0)
                            {
                                sbLOBUSers.Append(lGC + ",");
                            }
                            if (lVA != 0)
                            {
                                sbLOBUSers.Append(lVA + ",");
                            }
                            if (lTop != 0)
                            {
                                //sbLOBUSers.Append(lTop + ",");	commented by Mohit
                                sbLOBUSers.Append(lTop);
                            }
                            sLOBUSers = sbLOBUSers.ToString();
                            if (sLOBUSers.Trim().Length > 0)
                            {
                                if (sLOBUSers.Substring(sLOBUSers.Length - 1).Equals(","))
                                {
                                    sLOBUSers = sLOBUSers.Substring(0, sLOBUSers.Length - 1);
                                }
                            }
                            sStartList = GetStartingUsers(sLOBUSers);
                            //smahajan6: Safeway: Payment Supervisory Approval : Sart
                            if (sStartList.Trim().Length > 0)
                            {
                                sStartList = sStartList + "," + sLOBUSers;
                            }
                            else
                            {
                                sStartList = sLOBUSers;
                            }
                            //smahajan6: Safeway: Payment Supervisory Approval : End
                        }
                        //An LOB user is logged on We will need to screen the transactions for LOB
                        else if (bLOBOrTopUser && lTop != m_lUserId)
                        {
                            sLOBUSers = "";
                            sbLOBUSers = new StringBuilder();
                            if (lWC != 0)
                            {
                                sbLOBUSers.Append(lWC + ",");
                                if (lWC == m_lUserId)
                                {
                                    sWhichLOB = "243";
                                }
                            }
                            if (lDI != 0)
                            {
                                sbLOBUSers.Append(lDI + ",");
                                if (lDI == m_lUserId)
                                {
                                    sWhichLOB = sWhichLOB + ",844";
                                }
                            }
                            if (lGC != 0)
                            {
                                sbLOBUSers.Append(lGC + ",");
                                if (lGC == m_lUserId)
                                {
                                    sWhichLOB = sWhichLOB + ",241";
                                }
                            }
                            if (lVA != 0)
                            {
                                sbLOBUSers.Append(lVA + ",");
                                if (lVA == m_lUserId)
                                {
                                    sWhichLOB = sWhichLOB + ",242";
                                }
                            }
                            if (lTop != 0)
                            {
                                //sbLOBUSers.Append(lTop + ",");	commented by Mohit
                                sbLOBUSers.Append(lTop);
                                if (lTop == m_lUserId)
                                {
                                    sWhichLOB = "-1";
                                }
                            }
                            sLOBUSers = sbLOBUSers.ToString();
                            if (sLOBUSers.Trim().Length > 0)
                            {
                                if (sLOBUSers.Substring(sLOBUSers.Length - 1).Equals(","))
                                {
                                    sLOBUSers = sLOBUSers.Substring(0, sLOBUSers.Length - 1);
                                }
                            }
                            sStartList = GetStartingUsers(sLOBUSers);
                        }//end if (bLOBOrTopUser && lTop != m_lUserId)
                        //We start down the superviory chain
                        if (sStartList.Trim().Length > 0)
                        {
                            if (sStartList.Trim().Substring(0, sStartList.Trim().Length - 1).Equals(","))
                            {
                                sStartList = sStartList.Substring(0, sLOBUSers.Length - 1);
                            }
                        }
                        //smahajan6: Safeway: Payment Supervisory Approval : Start
                        if (!bLOBOrTopUser)
                        {
                            sStartList = Conversion.ConvertObjToStr(m_lUserId);
                        }

                        sStartList = GetSubordinateUsers(sStartList);
                        //smahajan6: Safeway: Payment Supervisory Approval : End
                        if (sStartList.Trim().Equals(""))
                        {
                            //Get all users who have the currrent user as their manager
                            sUserSQL = "SELECT USER_ID, MANAGER_ID FROM USER_TABLE WHERE MANAGER_ID = " + p_lUserId.ToString();
                        }
                        else
                        {
                            //Get all users who have the users in the starting list as their manager
                            sUserSQL = "SELECT USER_ID, MANAGER_ID FROM USER_TABLE WHERE MANAGER_ID IN (" + sStartList + ")";
                        }

                        objRead = DbFactory.GetDbReader(m_sSecureDSN, sUserSQL);
                        bool bIsEOF = objRead.Read();
                        iCount = 1; //added by Mohit
                        do
                        {
                            lTotalDays = iApprovalDays * iCount;
                            //list of users who have lUserID as their manager
                            while (bIsEOF)
                            {
                                if (!sUsers.Trim().Equals(""))
                                {
                                    if (!sUsers.Substring(sUsers.Length - 1).Equals(","))
                                    {
                                        sUsers += ",";
                                    }
                                }
                                sUsers += Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")) + ",";
                                sbTransIDSQL = new StringBuilder();
                                sbTransIDSQL.Append("SELECT TRANS_ID, TRANS_DATE, CLAIM_ID FROM FUNDS WHERE APPROVER_ID = " + Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")));

                                if (bUseQueuedPayments)
                                {
                                    sbTransIDSQL.Append(" AND ((FUNDS.STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString()
                                        + " AND VOID_FLAG = 0) OR (FUNDS.STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS").ToString() + " AND VOID_FLAG <> 0))");
                                }
                                else
                                {
                                    sbTransIDSQL.Append(" AND FUNDS.STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                                    sbTransIDSQL.Append("   AND FUNDS.VOID_FLAG = 0");

                                }

                                sbTransIDSQL.Append("AND FUNDS.PAYMENT_FLAG <> 0");
                                sbTransIDSQL.Append(" AND FUNDS.ADDED_BY_USER <> " + Utilities.FormatSqlFieldValue(m_sUserName));
                                objTempRead = DbFactory.GetDbReader(m_sDSN, sbTransIDSQL.ToString());

                                while (objTempRead.Read())
                                {
                                    sTransDate = Conversion.ConvertObjToStr(objTempRead.GetValue("TRANS_DATE"));
                                    datTemp = Conversion.ToDate(Conversion.ConvertObjToStr(sTransDate));

                                    if (iApprovalDays != 0)
                                    {
                                        //Added by Mohit
                                        //MWC this will add in the step missed when going between the supervisory chain and the LOB users
                                        if (lTop == m_lUserId)
                                            lTotalDays = lTotalDays + iApprovalDays;
                                        //datTemp=datTemp.AddDays(iApprovalDays);
                                        //commented by Mohit
                                        datTemp = datTemp.AddDays(lTotalDays);
                                    }
                                    else
                                    {
                                        datTemp = DateTime.Now;
                                    }
                                    //smahajan6: Safeway: Payment Supervisory Approval : Start
                                    sAssignedDateTime = string.Empty;
                                    iDaysCount = iApprovalDays;
                                    iHoursCount = iApprovalHours;
                                    iManagerId = GetManagerID(Conversion.ConvertObjToInt(objRead.GetValue("USER_ID"), m_iClientId));

                                    sSQL = " SELECT DTTM_APPROVAL_CHGD FROM PMT_APPROVAL_HIST WHERE APPROVER_ID = " + Conversion.ConvertObjToStr(objRead.GetValue("USER_ID"))
                                           + " AND TRANS_ID = " + Conversion.ConvertObjToStr(objTempRead.GetValue("TRANS_ID")) + " ORDER BY DTTM_APPROVAL_CHGD DESC";

                                    using (DbReader objReader = DbFactory.GetDbReader(m_sDSN, sSQL.ToString()))
                                    {
                                        if (objReader.Read())
                                        {
                                            sAssignedDateTime = objReader.GetValue("DTTM_APPROVAL_CHGD").ToString();
                                        }
                                        objReader.Close();
                                    }
                                    if (sAssignedDateTime != "")
                                    {
                                        datTemp = ToDateTime(sAssignedDateTime);
                                    }
                                    while (iManagerId != 0 && iManagerId != Conversion.ConvertObjToInt(m_lUserId, m_iClientId))
                                    {
                                        iDaysCount = iDaysCount + iApprovalDays;
                                        iHoursCount = iHoursCount + iApprovalHours;
                                        iManagerId = GetManagerID(iManagerId);
                                    }

                                    if (iManagerId == 0 && bLOBOrTopUser && lTop == m_lUserId)
                                    {
                                        iDaysCount = iDaysCount + iApprovalDays;
                                        iHoursCount = iHoursCount + iApprovalHours;
                                    }

                                    dStartDateTime = datTemp.AddDays(iDaysCount).AddHours(iHoursCount);
                                    dEndDateTime = dStartDateTime.AddDays(iApprovalDays).AddHours(iApprovalHours);
                                    //if (dStartDateTime <= dCurrentDateTime && ((dEndDateTime >= dCurrentDateTime) || (lTop == m_lUserId)))
                                    //Start rsusilaggar MITS 26332 11/23/2011
                                    if (bSupervisoryApproval)
                                    {
                                        bTempValue = (dStartDateTime <= dCurrentDateTime);
                                    }
                                    else
                                    {
                                        bTempValue = (dStartDateTime <= dCurrentDateTime && ((dEndDateTime >= dCurrentDateTime) || (lTop == m_lUserId) || (dStartDateTime == dEndDateTime)));
                                    }
                                    //if (datTemp <= DateTime.Now) - modified Time Validation Check
                                    //smahajan6: Safeway: Payment Supervisory Approval : End
                                    if (bTempValue)
                                    {
                                        if (!string.IsNullOrEmpty(sWhichLOB))
                                        {
                                            if (sWhichLOB.IndexOf(GetLOBForClaim(Conversion.ConvertObjToInt64(
                                                objTempRead.GetValue("CLAIM_ID"), m_iClientId)).ToString()) > -1)
                                            {
                                                if ((!sTransIDList.Trim().Equals("")))
                                                {
                                                    if ((!sTransIDList.Substring(sTransIDList.Length - 1).Equals(",")))
                                                    {
                                                        sTransIDList = sTransIDList + ",";
                                                    }
                                                }
                                                sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objTempRead.GetValue("TRANS_ID")) + ",";
                                            }
                                        }
                                        else
                                        {
                                            if ((!sTransIDList.Trim().Equals("")))
                                            {
                                                if ((!sTransIDList.Substring(sTransIDList.Length - 1).Equals(",")))
                                                {
                                                    sTransIDList = sTransIDList + ",";
                                                }
                                            }
                                            sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objTempRead.GetValue("TRANS_ID")) + ",";
                                        }
                                    }
                                }//end while objTempRead
                                CloseReader(ref objTempRead);
                                bIsEOF = objRead.Read();
                            }//end while
                            CloseReader(ref objRead);
                            if (sUsers.Trim().Length > 0)
                            {
                                if (sUsers.Substring(sUsers.Length - 1).Equals(","))
                                {
                                    sUsers = sUsers.Substring(0, sUsers.Length - 1);
                                }
                            }
                            if (!sUsers.Trim().Equals(""))
                            {
                                sUserSQL = "SELECT USER_ID, MANAGER_ID FROM USER_TABLE WHERE MANAGER_ID IN (" + sUsers + ")";
                            }
                            else
                            {
                                sUserSQL = "SELECT USER_ID, MANAGER_ID FROM USER_TABLE WHERE MANAGER_ID = -1";
                            }
                            objRead = DbFactory.GetDbReader(m_sSecureDSN, sUserSQL);
                            sUsers = string.Empty;
                            iCount = iCount + 1;
                        } while (bIsEOF);
                        CloseReader(ref objRead);
                    }//added by Mohit
				 }
				if (sTransIDList.Trim().Length > 0) 
				{
					if (sTransIDList.Substring(sTransIDList.Length-1).Equals(","))
					{
						sTransIDList=sTransIDList.Substring(0,sTransIDList.Length-1);
					}
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.DownSupChain.Error", m_iClientId), p_objException);
			}
			finally
			{
				CloseReader(ref objRead);
				CloseReader(ref objTempRead);
				objCacheFunctions=null;
				sbSQL=null;
				sbLOBUSers=null;
			}
			return sTransIDList;
		}
		/// Name		: GetLOBForClaim
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets LOB for claim
		/// </summary>
		/// <param name="p_lClaimID">ClaimID</param>
		/// <returns>LOB</returns>
		private long GetLOBForClaim(long p_lClaimID)
		{
			long lLOBForClaim=0;
			DbReader objRead=null;
			StringBuilder sbSQL=null;
			try
			{
				sbSQL=new StringBuilder();
				sbSQL.Append("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_lClaimID.ToString());
				objRead=DbFactory.GetDbReader(m_sDSN,sbSQL.ToString());
				if (objRead.Read())
				{
                    lLOBForClaim = Conversion.ConvertObjToInt64(objRead.GetValue(0), m_iClientId);
				}
				CloseReader(ref objRead);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.GetLOBForClaim.Error", m_iClientId), p_objException);
			}
			finally
			{
				CloseReader(ref objRead);
				sbSQL=null;
			}
			return lLOBForClaim;
		}
		/// Name		: GetStartingUsers
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		
		/// <summary>
		/// Get User ids
		/// </summary>
		/// <param name="p_sUsersToInclude">Manager ids</param>
		/// <returns>String containing userids separated by comma</returns>
		private string GetStartingUsers(string p_sUsersToInclude)
		{
			StringBuilder sbSQL=null;
			StringBuilder sbTemp=null;
			DbReader objRead=null;
			string sReturnValue="";
			try
			{
				sbSQL=new StringBuilder();
				if (!p_sUsersToInclude.Trim().Equals(""))
				{
					sbSQL.Append("SELECT USER_ID FROM USER_TABLE WHERE (MANAGER_ID IS NULL OR MANAGER_ID = 0 OR MANAGER_ID IN (" + p_sUsersToInclude + ")) AND USER_ID NOT IN (" + p_sUsersToInclude + ")");
				}
				else
				{
					sbSQL.Append("SELECT USER_ID FROM USER_TABLE WHERE MANAGER_ID IS NULL OR MANAGER_ID = 0");
				}
			
				objRead=DbFactory.GetDbReader(m_sSecureDSN,sbSQL.ToString());
				sbTemp=new StringBuilder();
				while (objRead.Read())
				{
					if (!sbTemp.ToString().Trim().Equals(""))
					{
						if (!sbTemp.ToString().Substring(sbTemp.Length - 1).Equals(",")) //smahajan6: Safeway: Payment Supervisory Approval
						{
							sbTemp.Append(",");
						}
						sbTemp.Append(Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")));
					}
					//smahajan6: Safeway: Payment Supervisory Approval : Start - fixed issue in base
					else
					{
						sbTemp.Append(Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")));
					}
					//smahajan6: Safeway: Payment Supervisory Approval : End
				}
				sReturnValue=sbTemp.ToString();
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.GetStartingUsers.Error", m_iClientId), p_objException);
			}
			finally
			{
				CloseReader(ref objRead);
				sbSQL=null;
				sbTemp=null;
			}
			return sReturnValue;
		}
		/// Name		: ListFundsTransactions
		/// Author		: Saurav Mahajan
		/// Date Created: 11/02/2008		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Lists funds transactions with Warning
		/// </summary>
		/// <param name="p_objOutAllTran">All transactions XML</param>
		/// <param name="p_objOutDenyTran">All Deny only transactions XML</param>
		/// <param name="p_sOutErrorXml">Information returned as xml string</param>
		/// <param name="p_sOutWarningXml">Information returned as xml string</param>
		/// <returns>Success 1 or Failure 0 in execution of the function</returns>
        public int ListFundsTransactions(XmlDocument p_objXmlIn, out XmlDocument p_objOutAllTran, out XmlDocument p_objOutDenyTran, out string p_sOutErrorXml, out string p_sOutWarningXml , string p_sLangCode="1033")
		{
			try
			{
                m_slangCode = p_sLangCode;//vkumar258 ML Changes
				p_sOutWarningXml = m_sWarnings;
				if (!string.IsNullOrEmpty(p_sOutWarningXml))
				{
					p_sOutWarningXml = p_sOutWarningXml.Substring(0, p_sOutWarningXml.Length - 2);
					//p_sOutWarningXml = "<Warnings><Warning>A hold requiring supervisory approval has been placed because: "
									//+ "The amount of following payment(s) exceeded the users limit and requires manager's approval : " 
									//+ p_sOutWarningXml + "</Warning></Warnings>";
                    p_sOutWarningXml = "<Warnings><Warning>A hold requiring supervisory approval has been placed because: "
                                    + "The amount of following payment(s) "
                                    + p_sOutWarningXml + "</Warning></Warnings>";
					m_sWarnings = string.Empty;
				}
                return ListFundsTransactions(p_objXmlIn, out p_objOutAllTran, out p_objOutDenyTran, out p_sOutErrorXml , p_sLangCode);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.ListFundsTransactions.Error", m_iClientId), p_objException);
			}
		}



		/// Name		: ListFundsTransactions
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Lists funds transactions
		/// </summary>
		/// <param name="p_objOutAllTran">All transactions XML</param>
		/// <param name="p_objOutDenyTran">All Deny only transactions XML</param>
		/// <param name="p_sOutErrorXml">Information returned as xml string</param>
		/// <param name="p_objXmlIn">Create necessary transaction xml</param>
		/// <returns>Success 1 or Failure 0 in execution of the function</returns>
        public int ListFundsTransactions(XmlDocument p_objXmlIn, out XmlDocument p_objOutAllTran, out XmlDocument p_objOutDenyTran, out string p_sOutErrorXml, string p_sLangCode="1033")
		{
            
			string sTmp = string.Empty;
			string sName = string.Empty;
			string sFName = string.Empty;
			string sLName = string.Empty;
			//Start rsushilaggar 06/04/2010 MITS 20938
			string sClaimantFirstName = string.Empty;             
			string sClaimantLastName = string.Empty;              
			int iEntityApprovalStatus=0;                  
			DbReader objLSSInvoiceRead = null;          
			string sLSS_INVOICE_ID = string.Empty;
			//End rsushilaggar 06/04/2010 MITS 20938
			string sDitto = string.Empty;
            string sSubmittedTo = string.Empty;//sharishkumar Jira 6417
			long lTransId=0; 
			long lLastTransID=0; 
			double dMaxAmount=0; 
			long lGetUserID=0; 
			long lGetManagerID=0; 
			int iCheckCount=0;
			string sSupChain = string.Empty;
			// MITS 14836
			double dDetailAmount;
			long lResType;
			long lLastResType = 0;
			long dTotalOfResType=0;
			bool bDeleteTrans;
            string IsShowMyTrans = String.Empty;//ajohari2 RMA-13878(RMA 6404 and RMA 9875)
            bool bShowPayeeOnHold = false;//sachin Jira 14871
			bDeleteTrans = false;
			LobSettings objLobSettings = null;
			ColLobSettings objColLobSettings = null;
			StringBuilder sbSQL=null;
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			StringBuilder sbSQLTemp=null;
			CCacheFunctions objCacheFunctions=null;
			DbReader objRead=null;
			int iReturnValue=0;
			DbReader objTempRead=null;
			LocalCache objCache=null;
			bool bTempRead=false;
			bool bRead=false;
			p_sOutErrorXml = string.Empty;
			p_objOutAllTran=null;
			p_objOutDenyTran=null;
            SysSettings objSettings = null;
			try
			{

                m_slangCode = "1033";//msampathkuma RMA- 11146
				sDitto = Convert.ToChar(34).ToString();
                objCacheFunctions = new CCacheFunctions(m_sDSN, m_iClientId);
                objCache = new LocalCache(m_sDSN, m_iClientId);
                objSettings = new SysSettings(m_sDSN, m_iClientId);
                sbSQL = new StringBuilder();
                //ajohari2 RMA-13878(RMA 6404 and RMA 9875)
                if (p_objXmlIn.SelectSingleNode("//IsShowMyTrans") != null && !String.IsNullOrEmpty(p_objXmlIn.SelectSingleNode("//IsShowMyTrans").InnerText))
                {
                    IsShowMyTrans = p_objXmlIn.SelectSingleNode("//IsShowMyTrans").InnerText.Trim();
                }
                //ajohari2 RMA-13878(RMA 6404 and RMA 9875) END

                //sachin Jira 14871 starts
                if (p_objXmlIn.SelectSingleNode("//ShowPayeeOnHold") != null && !String.IsNullOrEmpty(p_objXmlIn.SelectSingleNode("//ShowPayeeOnHold").InnerText))
                {
                    if (p_objXmlIn.SelectSingleNode("//ShowPayeeOnHold").InnerText.ToLower() == "true")
                    {
                        bShowPayeeOnHold = true;
                    }
                }
                //sachin Jira 14871 ends

                //START rsushilaggar vendor's funds approva MITS 20606 05-May-2010
                sbSQL.Append("SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, FUNDS.LAST_NAME, FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT,FUNDS.COMMENTS,");
                sbSQL.Append("  FUNDS.ADDED_BY_USER, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE");
                
                if(objSettings.MultiCovgPerClm!=0)
                {
                sbSQL.Append(",RESERVE_CURRENT.RESERVE_TYPE_CODE");
                }
                else
                {
                    sbSQL.Append(",FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE");
                }
                sbSQL.Append(", FUNDS_TRANS_SPLIT.FROM_DATE, FUNDS_TRANS_SPLIT.TO_DATE,FUNDS.CLAIM_NUMBER,FUNDS.CLAIM_ID");
                sbSQL.Append(", ENTITY.LAST_NAME | ENTITY_LAST_NAME , ENTITY.FIRST_NAME | ENTITY_FIRST_NAME, ");


                //JIRA 7810 start  fix RMA-13278

               
                sbSQL.Append(" CASE");
                sbSQL.Append("   WHEN EXISTS (SELECT ENTITY.ENTITY_APPROVAL_STATUS FROM FUNDS_X_PAYEE ,ENTITY WHERE PAYEE_EID=ENTITY_ID");
                sbSQL.Append(" AND FUNDS_X_PAYEE.FUNDS_TRANS_ID=FUNDS.TRANS_ID AND ENTITY.ENTITY_APPROVAL_STATUS  IN (" + objCache.GetCodeId("R", "ENTITY_APPRV_REJ") + ")) THEN ");
                sbSQL.Append(objCache.GetCodeId("R", "ENTITY_APPRV_REJ"));    
                sbSQL.Append("   WHEN EXISTS (SELECT ENTITY.ENTITY_APPROVAL_STATUS FROM FUNDS_X_PAYEE ,ENTITY WHERE PAYEE_EID=ENTITY_ID");
                sbSQL.Append(" AND FUNDS_X_PAYEE.FUNDS_TRANS_ID=FUNDS.TRANS_ID AND ENTITY.ENTITY_APPROVAL_STATUS  IN (" + objCache.GetCodeId("P", "ENTITY_APPRV_REJ") + ",0)) THEN ");
                sbSQL.Append(objCache.GetCodeId("P", "ENTITY_APPRV_REJ"));             
	    
                sbSQL.Append(" ELSE ");
                sbSQL.Append(objCache.GetCodeId("A", "ENTITY_APPRV_REJ"));
               
                sbSQL.Append(" END");

                sbSQL.Append("  ENTITY_APPROVAL_STATUS , ");
                        //sbSQL.Append(" ENTITY2.ENTITY_APPROVAL_STATUS ,");
                //JIRA 7810 end  fix RMA-13278
                //JIRA 7810 Start Snehal
                if (m_lDbMake == (long)eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    sbSQL.Append("  ISNULL(  (SELECT STUFF((SELECT ', ' + CD.SHORT_CODE   FROM HOLD_REASON  INNER JOIN CODES ");
                    sbSQL.Append("   ON HOLD_REASON.HOLD_REASON_CODE = CODE_ID");
                    sbSQL.Append(" INNER JOIN  CODES CD ON CODES.RELATED_CODE_ID = CD.CODE_ID AND CD.TABLE_ID= " + objCache.GetTableId("HOLD_REASON_PARENT"));//RMA-14373 
                    sbSQL.Append(" WHERE ON_HOLD_ROW_ID = HR.ON_HOLD_ROW_ID");
                    sbSQL.Append("  AND HOLD_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                    sbSQL.Append("  AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                    sbSQL.Append("   FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE");
                    sbSQL.Append("  FROM HOLD_REASON  HR WHERE HOLD_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                    sbSQL.Append("  AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                    sbSQL.Append("  AND ON_HOLD_ROW_ID = FUNDS.TRANS_ID GROUP BY ON_HOLD_ROW_ID ) ,'') AS HOLD_REASON_CODE,");

                    sbSQL.Append(" ISNULL(  (SELECT STUFF((SELECT ', ' + CT.CODE_DESC   FROM HOLD_REASON  INNER JOIN CODES  C");
                    sbSQL.Append(" ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID INNER JOIN CODES_TEXT CT");
                    sbSQL.Append(" ON C.CODE_ID = CT.CODE_ID AND C.TABLE_ID= " + objCache.GetTableId("HOLD_REASON_CODE") + " AND CT.LANGUAGE_CODE =" + p_sLangCode + " WHERE ON_HOLD_ROW_ID = HR.ON_HOLD_ROW_ID");//RMA-14373 
                    sbSQL.Append(" AND HOLD_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("FUNDS"));
                    sbSQL.Append(" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE_DESC");
                    sbSQL.Append(" FROM HOLD_REASON  HR WHERE HOLD_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                    sbSQL.Append(" AND ON_HOLD_ROW_ID = FUNDS.TRANS_ID GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLD_REASON,");
                }
                else if (m_lDbMake == (long)eDatabaseType.DBMS_IS_ORACLE)
                {
                    sbSQL.Append(" nvl( (SELECT LISTAGG(CD.SHORT_CODE, ',') ");
                    sbSQL.Append(" WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID) AS SHORT_CODE");
                    sbSQL.Append(" FROM HOLD_REASON INNER JOIN CODES ON HOLD_REASON.HOLD_REASON_CODE = CODES.CODE_ID");
                    sbSQL.Append(" INNER JOIN  CODES CD ON CODES.RELATED_CODE_ID = CD.CODE_ID AND CD.TABLE_ID= " + objCache.GetTableId("HOLD_REASON_PARENT"));//RMA-14373 
                    sbSQL.Append(" WHERE HOLD_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                    sbSQL.Append(" AND ON_HOLD_ROW_ID = FUNDS.TRANS_ID GROUP BY ON_HOLD_ROW_ID),0)  AS HOLD_REASON_CODE,");

                    sbSQL.Append(" nvl( (SELECT LISTAGG(CT.CODE_DESC, ',') WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID )");
                    sbSQL.Append(" FROM HOLD_REASON INNER JOIN CODES  C ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID");
                    sbSQL.Append(" INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND C.TABLE_ID= " + objCache.GetTableId("HOLD_REASON_CODE") + " AND CT.LANGUAGE_CODE =" + p_sLangCode + " WHERE HOLD_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());//RMA-14373 
                    sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                    sbSQL.Append(" AND ON_HOLD_ROW_ID = FUNDS.TRANS_ID GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLD_REASON,");



                }
                //JIRA 7810 End
                sbSQL.Append("(SELECT LSS_INVOICE_ID FROM FUNDS_SUPP WHERE LSS_INVOICE_ID IS NOT NULL AND LSS_INVOICE_ID <> 0.0 AND FUNDS_SUPP.TRANS_ID=FUNDS.TRANS_ID) | LSS_INVOICE_ID");
                if (bUseSpecificSupervisor || bUseSupAppPayments)
				{
					sbSQL.Append(",FUNDS.APPROVER_ID");
				}
				sbSQL.Append(",FUNDS.PAYMENT_FLAG, FUNDS.COLLECTION_FLAG");
				sbSQL.Append(" FROM FUNDS ");
				sbSQL.Append("INNER JOIN  FUNDS_TRANS_SPLIT  ON FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ");
                if (objSettings.MultiCovgPerClm != 0)
                {
                    sbSQL.Append("INNER JOIN  RESERVE_CURRENT  ON FUNDS_TRANS_SPLIT.RC_ROW_ID = RESERVE_CURRENT.RC_ROW_ID ");
                }
             
				sbSQL.Append("LEFT OUTER JOIN  CLAIMANT  ON FUNDS.CLAIM_ID=CLAIMANT.CLAIM_ID AND CLAIMANT.PRIMARY_CLMNT_FLAG=-1  ");
				sbSQL.Append("LEFT OUTER JOIN ENTITY ON CLAIMANT.CLAIMANT_EID=ENTITY.ENTITY_ID ");
				sbSQL.Append("INNER JOIN ENTITY ENTITY2 ON ENTITY2.ENTITY_ID = FUNDS.PAYEE_EID  ");
                //ajohari2 RMA-13878(RMA 6404 and RMA 9875)
                if (bUseQueuedPayments || (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))
                {
				    if (bUseQueuedPayments)
				    {
					    sbSQL.Append(" WHERE ((FUNDS.STATUS_CODE =" + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString() + " AND FUNDS.VOID_FLAG = 0)");
					    sbSQL.Append(" OR (FUNDS.STATUS_CODE ="+objCacheFunctions.GetCodeIDWithShort("Q","CHECK_STATUS").ToString()+" AND FUNDS.VOID_FLAG <> 0))");
				    }
                    else
                    {
                        sbSQL.Append(" WHERE ((FUNDS.STATUS_CODE =" + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString() + " AND FUNDS.VOID_FLAG = 0))");
                    }
                }
                //ajohari2 RMA-13878(RMA 6404 and RMA 9875) END
				else
				{
					sbSQL.Append(" WHERE FUNDS.STATUS_CODE ="+objCacheFunctions.GetCodeIDWithShort("H","CHECK_STATUS").ToString());
					sbSQL.Append(" AND FUNDS.VOID_FLAG = 0");
				}

				//Start: rsushilaggar Collections from LSS 06/02/2010 MITS - 20938
				if (!PutLSScollectionOnHold)
				{
					sbSQL.Append(" AND FUNDS.PAYMENT_FLAG <> 0 "); 
				}
				//End: rsushilaggar
				sbSQLTemp=new StringBuilder();
                //ajohari2 RMA-13878(RMA 6404 and RMA 9875)
		        if (bUseQueuedPayments || (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))
				{
					sbSQLTemp.Append(sbSQL.ToString()+" AND FUNDS.ADDED_BY_USER = "+Utilities.FormatSqlFieldValue(m_sUserName));
					sbSQL.Append(" AND FUNDS.ADDED_BY_USER <> "+Utilities.FormatSqlFieldValue(m_sUserName));
				}
                else if (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true")
                {
                    sbSQL.Append(" AND FUNDS.ADDED_BY_USER = " + Utilities.FormatSqlFieldValue(m_sUserName));
                }
                //ajohari2 RMA-13878(RMA 6404 and RMA 9875) END
				else
				{
					sbSQL.Append(" AND FUNDS.ADDED_BY_USER <> "+Utilities.FormatSqlFieldValue(m_sUserName));
				}

                //MITS 24464 mcapps2 If we are not using the superisory chaing we don't need to get the user ids 
                if (!bUseSupAppPayments)
                {
                    sbSQL.Append(" AND FUNDS.TRANS_ID <> 0");
                }
                else
                {
                    //rsushilaggar MITS 26332 Date 11/29/2011
                    if (p_objXmlIn.SelectSingleNode("//ShowAllItems") != null)  //averma62 - MITS #31483 - Null check was missing for this node. 
                    {
                        if (p_objXmlIn.SelectSingleNode("//ShowAllItems").InnerText == "True")
                        {
                            sSupChain = ShowItemsToAllUser(m_lUserId);
                        }
                        else
                        {
                            sSupChain = DownSupChain(m_lUserId, p_objXmlIn.SelectSingleNode("//ShowAllItems").InnerText);
                        }
                    }
                    //sSupChain = DownSupChain(m_lUserId);

                    //Start: rsushilaggar 06/03/2010 MITS 20938
                    if (string.IsNullOrEmpty(sSupChain))
                    {
                        sSupChain = GetCollectionTransId(m_sUserName);
                    }
                    else
                    {
                        string list = GetCollectionTransId(m_sUserName);
                        sSupChain = sSupChain + (!string.IsNullOrEmpty(list) ? "," + list : "");
                    }
                    //end: rsushilaggar
                    if (!sSupChain.Trim().Equals("")) ////MITS 24464 mcapps2 we have ids 
                    {
                        sbSQLTemp.Append(" AND FUNDS.TRANS_ID <> 0");
                        sbSQL.Append(" AND FUNDS.TRANS_ID IN (" + sSupChain + ")");
                    }
                    if (sSupChain.Trim().Equals("")) //MITS 24464 mcapps2 Supervisory Approval is turned on and we don't have any trans ids, but we cannot display everything that is on hold so we will limit the approval to the user
                    {
                        sbSQL.Append(" AND FUNDS.APPROVER_ID = " + m_lUserId);
                    }
                    //else if (!bUseSupAppPayments) // //MITS 24464 mcapps2 we handle this above and don't need to do anything here
                    //{
                    //    sbSQL.Append(" AND FUNDS.TRANS_ID <> 0");
                    //}
                    //else if (bUseQueuedPayments)  //MITS 24464 mcapps2 I'm not sure why we have Trans_ID <> 0 and Trans_id = -1  If we have ids we will show them if not we don't show anything
                    //{
                    //    sbSQLTemp.Append(" AND FUNDS.TRANS_ID <> 0");
                    //    //Commenting  the changes for Mits 22041.
                    //    //if (bUseSpecificSupervisor) //MITS 22041 08/26/2010
                    //    //{
                    //    sbSQL.Append(" AND FUNDS.TRANS_ID = -1");
                    //    //}
                    //}
                    //else  MITS 24464 mcapps2  We do not just show Funds records because we didn't find any ids.  If use supervisory approval is turned on we will show only those records that qualify for approval by the logged on user
                    //{
                    //    //Commenting  the changes for Mits 22041.
                    //    //if (bUseSpecificSupervisor) //MITS 22041 08/26/2010

                    //    sbSQL.Append(" AND FUNDS.TRANS_ID = -1");
                    //}
                    //else//rsushilaggar LSS collectionions should not visible to every user
                    //{
                    //    sbSQL.Append(" AND FUNDS.ADDED_BY_USER <> 'LSSINF' ");
                    //}
                }    
				if(!string.IsNullOrEmpty(sFilterSql))
					sbSQL.Append(sFilterSql);
				if (!string.IsNullOrEmpty(sOrderBySql))
					sbSQL.Append(" ORDER BY " + sOrderBySql);
				else
					sbSQL.Append(" ORDER BY FUNDS.CTL_NUMBER,FUNDS.TRANS_ID");

                //Manish Multicurrency
                int transid = Conversion.ConvertStrToInteger(sSupChain);
                string culture = CommonFunctions.GetCulture(transid, CommonFunctions.NavFormType.Funds, m_sDSN);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
                if (bUseQueuedPayments || (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))//ajohari2 RMA-13878(RMA 6404 and RMA 9875)
				{

					if (!string.IsNullOrEmpty(sFilterSql))
						sbSQLTemp.Append(sFilterSql);

					if (!string.IsNullOrEmpty(sOrderBySql))
						sbSQLTemp.Append(" ORDER BY " + sOrderBySql);
					else
						sbSQLTemp.Append(" ORDER BY FUNDS.CTL_NUMBER,FUNDS.TRANS_ID");	
				}
				
				if (m_lDbMake==(long)eDatabaseType.DBMS_IS_ACCESS)
				{
					sbSQL.Replace("|", " AS ");
					sbSQLTemp.Replace("|", " AS ");
				}
				else
				{	
					sbSQL.Replace("|", " ");
					sbSQLTemp.Replace("|", " ");
				}
				objRead=DbFactory.GetDbReader(m_sDSN,sbSQL.ToString());
				bRead=objRead.Read();
				
                if (bUseQueuedPayments || (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))//ajohari2 RMA-13878(RMA 6404 and RMA 9875)
				{
					objTempRead=DbFactory.GetDbReader(m_sDSN,sbSQLTemp.ToString());
					bTempRead=objTempRead.Read();
				}

                if (bUseQueuedPayments || (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))//ajohari2 RMA-13878(RMA 6404 and RMA 9875)
				{
					if ((!bTempRead) && (!bRead))
					{
						CloseReader(ref objRead);
                        p_sOutErrorXml = p_sOutErrorXml + "<Errors><Error>" + CommonFunctions.FilterBusinessMessage(Globalization.GetString("SupervisorApproval.ListFundsTransactions.NoSupervisoryApproval", m_iClientId), p_sLangCode) + "</Error></Errors>";
						return 0;
					}
				
				}
				else
				{
					if ((!bRead))
					{
						CloseReader(ref objRead);
                        p_sOutErrorXml = p_sOutErrorXml + "<Errors><Error>" + CommonFunctions.FilterBusinessMessage(Globalization.GetString("SupervisorApproval.ListFundsTransactions.NoSupervisoryApproval", m_iClientId), p_sLangCode) + "</Error></Errors>";
						return 0;
					}
				}
				//The userID and UserManagersId is obtained
				if (bUseSpecificSupervisor)
				{
					lGetManagerID=0;
				}
				else
				{
					lGetManagerID=m_lManagerId;
				}
				lGetUserID=m_lUserId;
				iCheckCount=0;
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("FundsTransactions");
                //zmohammad MITs 33267 : Pay detail fix
                objColLobSettings = new ColLobSettings(m_sDSN, m_iClientId);
				objDOM.AppendChild(objElemParent);
				while (bRead)
				{
                    //zmohammad MITs 33267 : Pay detail fix
                    objLobSettings = objColLobSettings[(int)GetLOBForClaim(Common.Conversion.ConvertObjToInt64(objRead.GetValue("CLAIM_ID"), m_iClientId))];
                    dDetailAmount = -1;
                    //If there is a manager set then we get the max_amount from the database
					//If a manager is not set we set the varable 'dMaxAmount to -1
					if (lGetManagerID!=0)
					{
                        //zmohammad MITs 33267 : Pay detail fix
                        if (objLobSettings != null)//asingh263 mits 35854
                        {
                        if (objLobSettings.PayLmtFlag)
                        {
                            dMaxAmount = GetMaxAmount(m_lUserId, (int)GetLOBForClaim(
                                Common.Conversion.ConvertObjToInt64(objRead.GetValue("CLAIM_ID"), m_iClientId)));
                        }
                        else
                        {
                            dMaxAmount = -1;
                        }
                        if (objLobSettings.PayDetLmtFlag)
                        {
                            GetMaxPayDetailAmount((int)GetLOBForClaim(Common.Conversion.ConvertObjToInt64(objRead.GetValue("CLAIM_ID"), m_iClientId)),
                                Conversion.ConvertStrToInteger(objRead.GetValue("RESERVE_TYPE_CODE").ToString()), ref dDetailAmount);
                        } // if
                      }//asingh263 mits 35854	
					}
					else if (bUseSpecificSupervisor)
					{
                        //zmohammad MITs 33267 : Pay detail fix
                        if (objLobSettings != null)//asingh263 mits 35854
                       {
                        if (objLobSettings.PayLmtFlag)
                        {
                            dMaxAmount = GetMaxAmount(m_lUserId, (int)GetLOBForClaim(
                                Common.Conversion.ConvertObjToInt64(objRead.GetValue("CLAIM_ID"), m_iClientId)));
                        }
                        else
                        {
                            dMaxAmount = -1;
                        }
                        if (objLobSettings.PayDetLmtFlag)
                        {
                            GetMaxPayDetailAmount((int)GetLOBForClaim(Common.Conversion.ConvertObjToInt64(objRead.GetValue("CLAIM_ID"), m_iClientId)),
                                Conversion.ConvertStrToInteger(objRead.GetValue("RESERVE_TYPE_CODE").ToString()), ref dDetailAmount);
                        } // if
                       }//asingh263 mits 35854
					}
					else
					{
						dMaxAmount = -1;
					}

                   if (objLobSettings != null)//asingh263 mits 35854
                   {
                       if (bPlaceSupHoldPayDetailLimits)
                       {
                           if (objLobSettings.PayDetLmtFlag)
                           {
                               GetMaxPayDetailAmount((int)GetLOBForClaim(Common.Conversion.ConvertObjToInt64(objRead.GetValue("CLAIM_ID"), m_iClientId)),
                                   Conversion.ConvertStrToInteger(objRead.GetValue("RESERVE_TYPE_CODE").ToString()), ref dDetailAmount);
                           } // if
                       }//asingh263 mits 35854
					} // if

					//MITS 14430 check if they have Pay Details Limits enabled and entered for this LOB
					//If the amount of the check is less than the max_amount or if no manager exists then we skip this check
					//if ( (dMaxAmount >=Conversion.ConvertObjToDouble(objRead.GetValue("FTSAMT"),m_iClientId)
					//    )
					//    || (dMaxAmount == -1))
                    //start 7810
					//if ((dMaxAmount >= Conversion.ConvertObjToDouble(objRead.GetValue("FAMT"),m_iClientId)
						//)
						//|| (dMaxAmount == -1) || m_bPmtNotfyImmSup) //smahajan6: Safeway: Payment Supervisory Approval - for Notify Immediate Supervisor
					//{
                   if ((dMaxAmount >= Conversion.ConvertObjToDouble(objRead.GetValue("FAMT"), m_iClientId) && dMaxAmount >= Conversion.ConvertObjToDouble(objRead.GetValue("FAMT"), m_iClientId) )
                       
                       || (dMaxAmount == -1) || m_bPmtNotfyImmSup) //smahajan6: Safeway: Payment Supervisory Approval - for Notify Immediate Supervisor
					{
                     //end 7810

                        lTransId = Conversion.ConvertObjToInt64(objRead.GetValue("TRANS_ID"), m_iClientId);
						lResType = Conversion.ConvertStrToLong(objRead.GetValue("RESERVE_TYPE_CODE").ToString());
						if(lResType != lLastResType)
						{
							dTotalOfResType = 0;
						} // if

						if (lTransId!=lLastTransID)
						{
							if(bDeleteTrans)
							{
								foreach(XmlNode objTransactionNode in objDOM.SelectNodes("/FundsTransactions/Transaction/TransId"))
								{
									if(objTransactionNode.InnerText == lLastTransID.ToString())
									{
										objDOM.DocumentElement.RemoveChild(objTransactionNode.ParentNode);
									} // if

								} // foreach
							} // if
							dTotalOfResType = 0;
							bDeleteTrans = false;
							lLastResType = lResType;
							//skip duplicates (caused by inner join w/ splits table)
                            objCache = new LocalCache(m_sDSN, m_iClientId);
							objElemChild=objDOM.CreateElement("Transaction");
							//Added to know the queued payments
							if (bUseQueuedPayments)
								objElemChild.SetAttribute("UtilitySetting","Q");
							else
								objElemChild.SetAttribute("UtilitySetting","H");
							//Start: rsushilaggar Collections from LSS 06/02/2010 MITS 20938
                            if (Conversion.ConvertObjToBool(objRead.GetValue("PAYMENT_FLAG"), m_iClientId))
							{
								objElemChild.SetAttribute("PaymentType","P");
							}
							else
							{
								objElemChild.SetAttribute("PaymentType", "C");
							}
							//End: rsushilaggar
                            iEntityApprovalStatus = Common.Conversion.ConvertObjToInt(objRead.GetValue("ENTITY_APPROVAL_STATUS"), m_iClientId);
							if (iEntityApprovalStatus != (int)objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))
								objElemChild.SetAttribute("EntityApprovalStatus", "NOT-APPROVED");
							else
								objElemChild.SetAttribute("EntityApprovalStatus", "APPROVED");
							lLastTransID = lTransId;
							objElemTemp=objDOM.CreateElement("CtlNumber");
							objElemTemp.InnerText=Common.Conversion.ConvertObjToStr(objRead.GetValue("CTL_NUMBER"));
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("TransDate");
							objElemTemp.InnerText=Conversion.GetUIDate(
								Conversion.ConvertObjToStr(objRead.GetValue("TRANS_DATE")),m_slangCode,m_iClientId);//vkumar258 ML Changes
							objElemChild.AppendChild(objElemTemp);
                            //JIRA 7810 start  fix RMA-13278
                            string sPayeesName = string.Empty;

                            int iNoOfPayees = 0;
                            string sEntSQL = "SELECT ISNULL(ENTITY.FIRST_NAME,'') + ' ' + ENTITY.LAST_NAME AS PAYEES_NAME from ENTITY INNER JOIN FUNDS_X_PAYEE ON  ENTITY.ENTITY_ID =FUNDS_X_PAYEE.PAYEE_EID WHERE FUNDS_X_PAYEE.FUNDS_TRANS_ID=" + lTransId;
                            if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                            {
                                sEntSQL = sEntSQL.Replace("ISNULL", "nvl");
                                sEntSQL = sEntSQL.Replace("+", "||");

                            }
                            sName = string.Empty;
                            // akaushik5 Added for MITS 37343 Ends
                            using (DbReader objeReader = DbFactory.GetDbReader(DbFactory.GetDbConnection(m_sDSN).ConnectionString, sEntSQL))
                            
                            {
                                while (objeReader.Read())
                                {
                                    if (sPayeesName == string.Empty)
                                    {
                                        sPayeesName = objeReader.GetString("PAYEES_NAME");
                                    }
                                    else
                                    {
                                        sPayeesName = sPayeesName + "<br> " + objeReader.GetString("PAYEES_NAME");
                                    }

                                    // akaushik5 Added for MITS 37343 Starts
                                    sName = string.IsNullOrEmpty(sName) ? objeReader.GetString("PAYEES_NAME") : string.Format("{0} and {1}", sName, objeReader.GetString("PAYEES_NAME"));
                                    // akaushik5 Added for MITS 37343 Ends
                                    iNoOfPayees++;

                                }
                            }

							//sFName=Common.Conversion.ConvertObjToStr(objRead.GetValue("FIRST_NAME"));
							//sLName=Common.Conversion.ConvertObjToStr(objRead.GetValue("LAST_NAME"));
							//if (!string.IsNullOrEmpty(sFName))
							//{
								//sName=sFName+" "+sLName;
							//}
							//else
							//{
								//sName=sLName;
							//}
                            //JIRA 7810 End  fix RMA-13278
							sClaimantLastName = Common.Conversion.ConvertObjToStr(objRead.GetValue("ENTITY_LAST_NAME"));
							sClaimantFirstName = Common.Conversion.ConvertObjToStr(objRead.GetValue("ENTITY_FIRST_NAME"));
							objElemTemp=objDOM.CreateElement("PayeeName");
							objElemTemp.InnerText=sName;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp = objDOM.CreateElement("PrimaryClaimantName");
							objElemTemp.InnerText = !string.IsNullOrEmpty(sClaimantFirstName) ? sClaimantFirstName + " " + sClaimantLastName : sClaimantLastName;
							objElemChild.AppendChild(objElemTemp);
							//objElemTemp = objDOM.CreateElement("PrimaryClaimantFirstName");
							//objElemTemp.InnerText = sClaimantFirstName.Length > 0 ? sClaimantFirstName : "-";
							//objElemChild.AppendChild(objElemTemp);

							sLSS_INVOICE_ID = Common.Conversion.ConvertObjToStr(objRead.GetDouble("LSS_INVOICE_ID"));
							if(!sLSS_INVOICE_ID.Trim().Equals("0"))
							{
								objElemTemp = objDOM.CreateElement("LSSInvoiceLink");
								objElemTemp.InnerText = "home?pg=riskmaster/Custom/Signon/SingleSignon&paramLSSInvoiceID=" + sLSS_INVOICE_ID;
								objElemTemp.SetAttribute("RMLSSInvoiceID", sLSS_INVOICE_ID);
								objElemChild.AppendChild(objElemTemp);
							}
							if (!sLSS_INVOICE_ID.Trim().Equals("0"))
							{
								objElemTemp = objDOM.CreateElement("LSSID");
								objElemTemp.InnerText = sLSS_INVOICE_ID;
								objElemChild.AppendChild(objElemTemp);
							}
							else
							{
								objElemTemp = objDOM.CreateElement("LSSID");
								objElemTemp.InnerText = "-";
								objElemChild.AppendChild(objElemTemp);
							}
							
							objElemTemp=objDOM.CreateElement("ClaimNumber");
							objElemTemp.InnerText=Common.Conversion.ConvertObjToStr(objRead.GetValue("CLAIM_NUMBER"));
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("PaymentAmount");
                            //Deb
							//objElemTemp.InnerText=string.Format( "{0:C}" ,Conversion.ConvertObjToDouble(objRead.GetValue("FAMT"),m_iClientId));
                            objElemTemp.InnerText = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sDSN), Conversion.ConvertObjToDouble(objRead.GetValue("FAMT"),m_iClientId), m_sDSN,m_iClientId);
							objElemChild.AppendChild(objElemTemp);

							//Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS - 20606
							objElemTemp = objDOM.CreateElement("PaymentAmountNumber");
							objElemTemp.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("FAMT"));
							objElemChild.AppendChild(objElemTemp);
							//End: rsushilaggar 05-May-2010 Vendor's Funds approval

							objElemTemp=objDOM.CreateElement("User");
							objElemTemp.InnerText=Common.Conversion.ConvertObjToStr(objRead.GetValue("ADDED_BY_USER"));
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("TransactionType");
							objElemTemp.InnerText=objCache.GetCodeDesc(
								Common.Conversion.ConvertStrToInteger(
								Common.Conversion.ConvertObjToStr(
								objRead.GetValue("TRANS_TYPE_CODE"))));
							dTotalOfResType = dTotalOfResType + Conversion.ConvertStrToLong(objRead.GetValue("FTSAMT").ToString());
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("SplitAmount");
                            //Deb
							//objElemTemp.InnerText=string.Format( "{0:C}" ,Conversion.ConvertObjToDouble(objRead.GetValue("FTSAMT"),m_iClientId));
                            objElemTemp.InnerText = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sDSN), Conversion.ConvertObjToDouble(objRead.GetValue("FTSAMT"), m_iClientId), m_sDSN, m_iClientId);
							objElemChild.AppendChild(objElemTemp);

							//Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
							objElemTemp = objDOM.CreateElement("SplitAmountNumber");
							objElemTemp.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("FTSAMT"));
							objElemChild.AppendChild(objElemTemp);
							//End: rsushilaggar 05-May-2010 Vendor's Funds approval
							sTmp = Conversion.GetUIDate(
                                Conversion.ConvertObjToStr(objRead.GetValue("FROM_DATE")), m_slangCode, m_iClientId);//vkumar258 ML Changes
							sTmp = sTmp + " - " +Conversion.GetUIDate(
                                Conversion.ConvertObjToStr(objRead.GetValue("TO_DATE")), m_slangCode, m_iClientId);//vkumar258 ML Changes
							objElemTemp=objDOM.CreateElement("FromToDate");
							objElemTemp.InnerText=sTmp;
							objElemChild.AppendChild(objElemTemp);
							//Added by Shameem for Void Reason-02/02/2009
							objElemTemp = objDOM.CreateElement("VoidReason");
							objElemTemp.InnerText = string.Empty;
							objElemChild.AppendChild(objElemTemp);
							//End Shameem
							objElemTemp=objDOM.CreateElement("TransId");
							objElemTemp.InnerText=Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID"));
							objElemChild.AppendChild(objElemTemp);
							//Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
							iEntityApprovalStatus = Common.Conversion.ConvertObjToInt(objRead.GetValue("ENTITY_APPROVAL_STATUS"), m_iClientId);
							objElemTemp = objDOM.CreateElement("PayeeStatus");
							//rsushilaggar 10-Aug-2010 MITS-21715
							//if (iEntityApprovalStatus != (int)objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))
							//    objElemTemp.InnerText = "Pending";
							//else
							//    objElemTemp.InnerText = string.Empty;
                            if (iEntityApprovalStatus == 0)
                            {
                                iEntityApprovalStatus = (int)objCacheFunctions.GetCodeIDWithShort("P", "ENTITY_APPRV_REJ");
                            }
                            objElemTemp.InnerText = objCache.GetCodeDesc(iEntityApprovalStatus);
                            objElemChild.AppendChild(objElemTemp);
                            //End: rsushilaggar 05-May-2010 Vendor's Funds approval
                            //JIRA 7810 Start Snehal
                            if ((iEntityApprovalStatus.Equals(objCache.GetCodeId("A", "ENTITY_APPRV_REJ")))
                                  && (!string.IsNullOrEmpty(Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE"))))
                                  && ((Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE")).Contains("PAYEE_NOT_APPRV")) ||
                                       (Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE")).Contains("PAYEE_REJECT"))))
                            {
                                /*if (this.ApprvFundsIfPayeeNotApprvdPermission)
                                {
                                    objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                                    objElemTemp.InnerText = string.Empty;
                                    objElemChild.AppendChild(objElemTemp);
                                }
                                else
                                {
                                 */
                                    objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                                    objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE"));
                                    objElemChild.AppendChild(objElemTemp);
                                //}
                                if (Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE")).Trim().Equals("PAYEE_NOT_APPRV")
                                    || Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE")).Trim().Equals("PAYEE_REJECT"))
                                {
                                    objElemTemp = objDOM.CreateElement("HOLD_REASON");
                                    objElemTemp.InnerText = objCache.GetCodeDesc(objCache.GetCodeId("PAYE_APRV_PYM_UNAPRV", "HOLD_REASON_PARENT"));
                                    objElemChild.AppendChild(objElemTemp);
                                }
                                else
                                {
                                    objElemTemp = objDOM.CreateElement("HOLD_REASON");
                                    objElemTemp.InnerText = (Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON")).Replace(objCache.GetCodeDesc(objCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), "")).Trim().TrimEnd(',').TrimStart(',').Replace(", ,", ",");
                                    
                                    objElemChild.AppendChild(objElemTemp);
                                }
                            }
                            else
                            {
                                objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                                objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE"));
                                objElemChild.AppendChild(objElemTemp);

                                objElemTemp = objDOM.CreateElement("HOLD_REASON");
                                objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON"));
                                objElemChild.AppendChild(objElemTemp);

                            }
                           
                           
                            //JIRA 7810 Ends
                            //Added by csingh7 for MITS 20092 :Start
                            objElemTemp = objDOM.CreateElement("Comments");
                            if (Common.Conversion.ConvertObjToStr(objRead.GetValue("COMMENTS")) != null && Common.Conversion.ConvertObjToStr(objRead.GetValue("COMMENTS")) != "")
                                objElemTemp.InnerText = "Comments";// Common.Conversion.ConvertObjToStr(objRead.GetValue("COMMENTS"));
                            objElemChild.AppendChild(objElemTemp);
                            //Added by csingh7 for MITS 20092 :End

                            //sharishkumar Jira 6417 starts
                            if (bUseSpecificSupervisor || bUseSupAppPayments)
                            {
                                objElemTemp = objDOM.CreateElement("SubmittedTo");
                                sSubmittedTo = objCache.GetSystemLoginName(Common.Conversion.ConvertObjToInt(objRead.GetValue("APPROVER_ID"), m_iClientId), m_iDsnId, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), m_iClientId);
                                objElemTemp.InnerText = sSubmittedTo;
                                objElemChild.AppendChild(objElemTemp);
                            }
                            //sharishkumar Jira 6417 ends

							objDOM.FirstChild.AppendChild(objElemChild);
							objCache.Dispose();
							objCache=null;
							iCheckCount = iCheckCount + 1;
						}
						else
						{
							lLastResType = lResType;
							dTotalOfResType = dTotalOfResType + Conversion.ConvertStrToLong(objRead.GetValue("FTSAMT").ToString());
                            objCache = new LocalCache(m_sDSN, m_iClientId);
							objElemChild=objDOM.CreateElement("Transaction");
							//Added to know the queued payments
							if (bUseQueuedPayments)
								objElemChild.SetAttribute("UtilitySetting","Q");
							else
								objElemChild.SetAttribute("UtilitySetting","H");
							

							//Start: rsushilaggar Collections from LSS 06/02/2010 MITS 20938
                            if (Conversion.ConvertObjToBool(objRead.GetValue("PAYMENT_FLAG"), m_iClientId))
							{
								objElemChild.SetAttribute("PaymentType", "P");
							}
							else
							{
								objElemChild.SetAttribute("PaymentType", "C");
							}
							//End: rsushilaggar

                            iEntityApprovalStatus = Common.Conversion.ConvertObjToInt(objRead.GetValue("ENTITY_APPROVAL_STATUS"), m_iClientId);
							if (iEntityApprovalStatus != (int)objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))
								objElemChild.SetAttribute("EntityApprovalStatus", "NOT-APPROVED");
							else
								objElemChild.SetAttribute("EntityApprovalStatus", "APPROVED");
							
							lLastTransID = lTransId;
							objElemTemp=objDOM.CreateElement("CtlNumber");
							objElemTemp.InnerText=sDitto;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("TransDate");
							objElemTemp.InnerText=sDitto;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("PayeeName");
							objElemTemp.InnerText=sDitto;;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp = objDOM.CreateElement("PrimaryClaimantName");
							objElemTemp.InnerText = !string.IsNullOrEmpty(sClaimantFirstName) ? sClaimantFirstName + " " + sClaimantLastName : sClaimantLastName;
							objElemChild.AppendChild(objElemTemp);
							//objElemTemp = objDOM.CreateElement("PrimaryClaimantFirstName");
							//objElemTemp.InnerText = sClaimantFirstName.Length > 0 ? sClaimantFirstName : "-";
							//objElemChild.AppendChild(objElemTemp);

							sLSS_INVOICE_ID = Common.Conversion.ConvertObjToStr(objRead.GetDouble("LSS_INVOICE_ID"));
							if(!sLSS_INVOICE_ID.Trim().Equals("0"))
							{
								objElemTemp = objDOM.CreateElement("LSSInvoiceLink");
								objElemTemp.InnerText = "home?pg=riskmaster/Custom/Signon/SingleSignon&paramLSSInvoiceID=" + sLSS_INVOICE_ID;
								objElemTemp.SetAttribute("RMLSSInvoiceID", sLSS_INVOICE_ID);
								objElemChild.AppendChild(objElemTemp);
							}
							if (!sLSS_INVOICE_ID.Trim().Equals("0"))
							{
								objElemTemp = objDOM.CreateElement("LSSID");
								objElemTemp.InnerText = sLSS_INVOICE_ID;
								objElemChild.AppendChild(objElemTemp);
							}
							else
							{
								objElemTemp = objDOM.CreateElement("LSSID");
								objElemTemp.InnerText = "-";
								objElemChild.AppendChild(objElemTemp);
							}
							objElemTemp=objDOM.CreateElement("ClaimNumber");
							objElemTemp.InnerText=sDitto;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("PaymentAmount");
							objElemTemp.InnerText=sDitto;
							objElemChild.AppendChild(objElemTemp);
							//Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS - 20606
							objElemTemp = objDOM.CreateElement("PaymentAmountNumber");
							objElemTemp.InnerText = sDitto;
							objElemChild.AppendChild(objElemTemp);
							//End: rsushilaggar 05-May-2010 Vendor's Funds approval
							objElemTemp=objDOM.CreateElement("User");
							objElemTemp.InnerText=sDitto;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("TransactionType");
							objElemTemp.InnerText=objCache.GetCodeDesc(
								Common.Conversion.ConvertStrToInteger(
								Common.Conversion.ConvertObjToStr(
								objRead.GetValue("TRANS_TYPE_CODE"))));
						
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("SplitAmount");
                            //Deb
							//objElemTemp.InnerText=string.Format( "{0:C}" ,Conversion.ConvertObjToDouble(objRead.GetValue("FTSAMT"),m_iClientId));
                            objElemTemp.InnerText = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sDSN), Conversion.ConvertObjToDouble(objRead.GetValue("FTSAMT"), m_iClientId), m_sDSN, m_iClientId);
							objElemChild.AppendChild(objElemTemp);
							//Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
							objElemTemp = objDOM.CreateElement("SplitAmountNumber");
							objElemTemp.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("FTSAMT"));
							objElemChild.AppendChild(objElemTemp);
							//End: rsushilaggar 05-May-2010 Vendor's Funds approval
							sTmp = Conversion.GetUIDate(
                                Conversion.ConvertObjToStr(objRead.GetValue("FROM_DATE")), m_slangCode, m_iClientId);//vkumar258 ML Changes
							sTmp = sTmp + " - " +Conversion.GetUIDate(
                                Conversion.ConvertObjToStr(objRead.GetValue("TO_DATE")), m_slangCode, m_iClientId);//vkumar258 ML Changes
							objElemTemp=objDOM.CreateElement("FromToDate");
							objElemTemp.InnerText=sTmp;
							objElemChild.AppendChild(objElemTemp);
							//Added by Rahul Aggarwal for Void Reason-05/05/2010 MITS 20606
							objElemTemp = objDOM.CreateElement("VoidReason");
							objElemTemp.InnerText = string.Empty;
							objElemChild.AppendChild(objElemTemp);
							//End Rahul
							objElemTemp=objDOM.CreateElement("TransId");
							objElemTemp.InnerText=Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID"));
							objElemChild.AppendChild(objElemTemp);
							//Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
							iEntityApprovalStatus = Common.Conversion.ConvertObjToInt(objRead.GetValue("ENTITY_APPROVAL_STATUS"), m_iClientId);
							objElemTemp = objDOM.CreateElement("PayeeStatus");
							//rsushilaggar 10-Aug-2010 MITS-21715
							//if (iEntityApprovalStatus != (int)objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))
							//    objElemTemp.InnerText = "Pending";
							//else
							//    objElemTemp.InnerText = string.Empty;
                            if (iEntityApprovalStatus == 0)
                            {
                                iEntityApprovalStatus = (int)objCacheFunctions.GetCodeIDWithShort("P", "ENTITY_APPRV_REJ");
                            }
							objElemTemp.InnerText = objCache.GetCodeDesc(iEntityApprovalStatus);
							objElemChild.AppendChild(objElemTemp);
							//Added by csingh7 for MITS 20092 :End


                            //JIRA 7810 Start Snehal

                            if ((iEntityApprovalStatus.Equals(objCache.GetCodeId("A", "ENTITY_APPRV_REJ")))
                                  && (!string.IsNullOrEmpty(Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE"))))
                                  && (Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE")).Contains("PAYEE_NOT_APPRV")))
                            {
                               /* if (this.ApprvFundsIfPayeeNotApprvdPermission)
                                {
                                    objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                                    objElemTemp.InnerText = string.Empty;
                                    objElemChild.AppendChild(objElemTemp);
                                }
                                else
                                {*/
                                    objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                                    objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE"));
                                    objElemChild.AppendChild(objElemTemp);
                                //}
                                if (Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE")).Trim().Equals("PAYEE_NOT_APPRV"))
                                {
                                    objElemTemp = objDOM.CreateElement("HOLD_REASON");
                                    objElemTemp.InnerText = objCache.GetCodeDesc(objCache.GetCodeId("PAYE_APRV_PYM_UNAPRV", "HOLD_REASON_PARENT"));
                                    objElemChild.AppendChild(objElemTemp);
                                }
                                else
                                {
                                    objElemTemp = objDOM.CreateElement("HOLD_REASON");
                                    objElemTemp.InnerText = (Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON")).Replace(objCache.GetCodeDesc(objCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), "")).Trim().TrimEnd(',').TrimStart(',').Replace(", ,", ",");
                                    objElemChild.AppendChild(objElemTemp);
                                }
                            }
                            else
                            {
                                objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                                objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE"));
                                objElemChild.AppendChild(objElemTemp);

                                objElemTemp = objDOM.CreateElement("HOLD_REASON");
                                objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON"));
                                objElemChild.AppendChild(objElemTemp);

                            }
                            //JIRA 7810 end 

                            //Added by csingh7 for MITS 20092 :Start
                            objElemTemp = objDOM.CreateElement("Comments");
                            if (Common.Conversion.ConvertObjToStr(objRead.GetValue("COMMENTS")) != null && Common.Conversion.ConvertObjToStr(objRead.GetValue("COMMENTS")) != "")
                                objElemTemp.InnerText = "Comments";// Common.Conversion.ConvertObjToStr(objRead.GetValue("COMMENTS"));
                            objElemChild.AppendChild(objElemTemp);
                            //Added by csingh7 for MITS 20092 :End

              
                            //sharishkumar Jira 6417 starts
                            if (bUseSpecificSupervisor || bUseSupAppPayments)
                            {
                                objElemTemp = objDOM.CreateElement("SubmittedTo");
                                sSubmittedTo = objCache.GetSystemLoginName(Common.Conversion.ConvertObjToInt(objRead.GetValue("APPROVER_ID"), m_iClientId), m_iDsnId, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), m_iClientId);
                                objElemTemp.InnerText = sSubmittedTo;
                                objElemChild.AppendChild(objElemTemp);
                            }
                            //sharishkumar Jira 6417 ends

							objDOM.FirstChild.AppendChild(objElemChild);
							objCache.Dispose();
							objCache=null;
							iCheckCount = iCheckCount + 1;
						}
                        //if(dTotalOfResType > dDetailAmount && dDetailAmount != -1)
                        if (!m_bPmtNotfyImmSup)
                        {
                            bDeleteTrans = true;
                        } // if


					}
					bRead=objRead.Read();
				}//end while
				CloseReader(ref objRead);

				//MITS 14430 check to see if last trans needs to be removed
				if (bDeleteTrans)
				{
					foreach (XmlNode objTransactionNode in objDOM.SelectNodes("/FundsTransactions/Transaction/TransId"))
					{
						if (objTransactionNode.InnerText == lLastTransID.ToString())
						{
							objDOM.DocumentElement.RemoveChild(objTransactionNode.ParentNode);
						} // if

					} // foreach
				} // if

                //sachin Jira 14871 starts 
                if (!bShowPayeeOnHold)
                {
                    foreach (XmlNode objHoldReasonNode in objDOM.SelectNodes("/FundsTransactions/Transaction/HOLD_REASON_CODE"))
                    {
                        if ((!string.IsNullOrEmpty(objHoldReasonNode.InnerText)) && ((objHoldReasonNode.InnerText.Contains("PAYEE_NOT_APPRV")) || (objHoldReasonNode.InnerText.Contains("PAYEE_REJECT"))) && !(objHoldReasonNode.InnerText.Contains(",")) && !(iEntityApprovalStatus.Equals(objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))))//sachin Jira 14871/14872
                        {
                            objDOM.DocumentElement.RemoveChild(objHoldReasonNode.ParentNode);
                        } 
                    } 
                }
                //sachin Jira 14871 ends

				p_objOutAllTran=objDOM;
				
                if (bUseQueuedPayments || (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))//ajohari2 RMA-13878(RMA 6404 and RMA 9875)
				{
					objDOM =new XmlDocument();
					objElemParent = objDOM.CreateElement("FundsTransactions");
					objDOM.AppendChild(objElemParent);
					while (bTempRead)
					{
                        lTransId = Conversion.ConvertObjToInt64(objTempRead.GetValue("TRANS_ID"), m_iClientId);
						if (lTransId!=lLastTransID)
						{
							//skip duplicates (caused by inner join w/ splits table)
                            objCache = new LocalCache(m_sDSN, m_iClientId);
							objElemChild=objDOM.CreateElement("Transaction");
							//Added to know the queued payments
							if (bUseQueuedPayments)
								objElemChild.SetAttribute("UtilitySetting","Q");
							else
								objElemChild.SetAttribute("UtilitySetting","H");
							//Start: rsushilaggar Collections from LSS 06/02/2010 MITS 20938
                            if (Conversion.ConvertObjToBool(objTempRead.GetValue("PAYMENT_FLAG"), m_iClientId))
							{
								objElemChild.SetAttribute("PaymentType", "P");
							}
							else
							{
								objElemChild.SetAttribute("PaymentType", "C");
							}
							//End: rsushilaggar
                            iEntityApprovalStatus = Common.Conversion.ConvertObjToInt(objTempRead.GetValue("ENTITY_APPROVAL_STATUS"), m_iClientId);
							if (iEntityApprovalStatus != (int)objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))
								objElemChild.SetAttribute("EntityApprovalStatus", "NOT-APPROVED");
							else
								objElemChild.SetAttribute("EntityApprovalStatus", "APPROVED");
							lLastTransID = lTransId;
							objElemTemp=objDOM.CreateElement("CtlNumber");
							objElemTemp.InnerText=Common.Conversion.ConvertObjToStr(objTempRead.GetValue("CTL_NUMBER"));
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("TransDate");
							objElemTemp.InnerText=Conversion.GetUIDate(
								Conversion.ConvertObjToStr(objTempRead.GetValue("TRANS_DATE")),m_slangCode,m_iClientId);//vkumar258 ML Changes
							objElemChild.AppendChild(objElemTemp);
                            //JIRA 7810 start  fix RMA-13278
                            string sPayeesName = string.Empty;

                            int iNoOfPayees = 0;
                            string sEntSQL = "SELECT ISNULL(ENTITY.FIRST_NAME,'') + ' ' + ENTITY.LAST_NAME AS PAYEES_NAME from ENTITY INNER JOIN FUNDS_X_PAYEE ON  ENTITY.ENTITY_ID =FUNDS_X_PAYEE.PAYEE_EID WHERE FUNDS_X_PAYEE.FUNDS_TRANS_ID=" + lTransId;
                            if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                            {
                                sEntSQL = sEntSQL.Replace("ISNULL", "nvl");
                                sEntSQL = sEntSQL.Replace("+", "||");

                            }
                            sName = string.Empty;
                            // akaushik5 Added for MITS 37343 Ends
                            using (DbReader objeReader = DbFactory.GetDbReader(DbFactory.GetDbConnection(m_sDSN).ConnectionString, sEntSQL))
                            {
                                while (objeReader.Read())
                                {
                                    if (sPayeesName == string.Empty)
                                    {
                                        sPayeesName = objeReader.GetString("PAYEES_NAME");
                                    }
                                    else
                                    {
                                        sPayeesName = sPayeesName + "<br> " + objeReader.GetString("PAYEES_NAME");
                                    }

                                    // akaushik5 Added for MITS 37343 Starts
                                    sName = string.IsNullOrEmpty(sName) ? objeReader.GetString("PAYEES_NAME") : string.Format("{0} and {1}", sName, objeReader.GetString("PAYEES_NAME"));
                                    // akaushik5 Added for MITS 37343 Ends
                                    iNoOfPayees++;

                                }
                            }

							//sFName=Common.Conversion.ConvertObjToStr(objTempRead.GetValue("FIRST_NAME"));
							//sLName=Common.Conversion.ConvertObjToStr(objTempRead.GetValue("LAST_NAME"));
							//if (!string.IsNullOrEmpty(sFName))
							//{
								//sName=sFName+" "+sLName;
							//}
							//else
							//{
								//sName=sLName;
							//}
							
							sClaimantLastName = Common.Conversion.ConvertObjToStr(objTempRead.GetValue("ENTITY_LAST_NAME"));
							sClaimantFirstName = Common.Conversion.ConvertObjToStr(objTempRead.GetValue("ENTITY_FIRST_NAME"));
							
							objElemTemp=objDOM.CreateElement("PayeeName");
							objElemTemp.InnerText=sName;
							objElemChild.AppendChild(objElemTemp);
							//rsushilaggar 05/05/2010 MITS# :20606
							objElemTemp = objDOM.CreateElement("PrimaryClaimantName");
							objElemTemp.InnerText = sClaimantFirstName.Length > 0 ? sClaimantFirstName + " "+sClaimantLastName:sClaimantLastName;
							objElemChild.AppendChild(objElemTemp);

							sLSS_INVOICE_ID = Common.Conversion.ConvertObjToStr(objTempRead.GetDouble("LSS_INVOICE_ID"));
							if (!sLSS_INVOICE_ID.Trim().Equals("0"))
							{
								objElemTemp = objDOM.CreateElement("LSSInvoiceLink");
								objElemTemp.InnerText = "home?pg=riskmaster/Custom/Signon/SingleSignon&paramLSSInvoiceID=" + sLSS_INVOICE_ID;
								objElemTemp.SetAttribute("RMLSSInvoiceID", sLSS_INVOICE_ID);
								objElemChild.AppendChild(objElemTemp);
							}
							if (!sLSS_INVOICE_ID.Trim().Equals("0"))
							{
								objElemTemp = objDOM.CreateElement("LSSID");
								objElemTemp.InnerText = sLSS_INVOICE_ID;
								objElemChild.AppendChild(objElemTemp);
							}
							else
							{
								objElemTemp = objDOM.CreateElement("LSSID");
								objElemTemp.InnerText = "-";
								objElemChild.AppendChild(objElemTemp);
							}
							
							objElemTemp=objDOM.CreateElement("ClaimNumber");
							objElemTemp.InnerText=Common.Conversion.ConvertObjToStr(objTempRead.GetValue("CLAIM_NUMBER"));
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("PaymentAmount");
                            //Deb
							//objElemTemp.InnerText=string.Format( "{0:C}" ,Conversion.ConvertObjToDouble(objTempRead.GetValue("FAMT"),m_iClientId));
                            objElemTemp.InnerText = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sDSN), Conversion.ConvertObjToDouble(objTempRead.GetValue("FAMT"), m_iClientId), m_sDSN, m_iClientId);
							objElemChild.AppendChild(objElemTemp);
							//Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
							objElemTemp = objDOM.CreateElement("PaymentAmountNumber");
							objElemTemp.InnerText = Conversion.ConvertObjToStr(objTempRead.GetValue("FAMT"));
							objElemChild.AppendChild(objElemTemp);
							//End: rsushilaggar 05-May-2010 Vendor's Funds approval
							objElemTemp=objDOM.CreateElement("User");
							objElemTemp.InnerText=Common.Conversion.ConvertObjToStr(objTempRead.GetValue("ADDED_BY_USER"));
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("TransactionType");
							objElemTemp.InnerText=objCache.GetCodeDesc(
								Common.Conversion.ConvertStrToInteger(
								Common.Conversion.ConvertObjToStr(
								objTempRead.GetValue("TRANS_TYPE_CODE"))));
						
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("SplitAmount");
                            //Deb
							//objElemTemp.InnerText=string.Format( "{0:C}" ,Conversion.ConvertObjToDouble(objTempRead.GetValue("FTSAMT"),m_iClientId));
                            objElemTemp.InnerText = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sDSN), Conversion.ConvertObjToDouble(objTempRead.GetValue("FTSAMT"), m_iClientId), m_sDSN,m_iClientId);
							objElemChild.AppendChild(objElemTemp);
							//Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
							objElemTemp = objDOM.CreateElement("SplitAmountNumber");
							objElemTemp.InnerText = Conversion.ConvertObjToStr(objTempRead.GetValue("FTSAMT"));
							objElemChild.AppendChild(objElemTemp);
							//End: rsushilaggar 05-May-2010 Vendor's Funds approval
							sTmp = Conversion.GetUIDate(
                                Conversion.ConvertObjToStr(objTempRead.GetValue("FROM_DATE")), m_slangCode, m_iClientId);//vkumar258 ML Changes
							sTmp = sTmp + " - " +Conversion.GetUIDate(
                                Conversion.ConvertObjToStr(objTempRead.GetValue("TO_DATE")), m_slangCode, m_iClientId);//vkumar258 ML Changes
							objElemTemp=objDOM.CreateElement("FromToDate");
							objElemTemp.InnerText=sTmp;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("TransId");
							objElemTemp.InnerText=Common.Conversion.ConvertObjToStr(objTempRead.GetValue("TRANS_ID"));
							objElemChild.AppendChild(objElemTemp);
							//Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
							iEntityApprovalStatus = Common.Conversion.ConvertObjToInt(objTempRead.GetValue("ENTITY_APPROVAL_STATUS"), m_iClientId);
							objElemTemp = objDOM.CreateElement("PayeeStatus");
							//rsushilaggar 10-Aug-2010 MITS-21715
							//if (iEntityApprovalStatus != (int)objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))
							//    objElemTemp.InnerText = "Pending";
							//else
							//    objElemTemp.InnerText = string.Empty;
                            if (iEntityApprovalStatus == 0)
                            {
                                iEntityApprovalStatus = (int)objCacheFunctions.GetCodeIDWithShort("P", "ENTITY_APPRV_REJ");
                            }
                            objElemTemp.InnerText = objCache.GetCodeDesc(iEntityApprovalStatus);
                            objElemChild.AppendChild(objElemTemp);
                            //End: rsushilaggar 05-May-2010 Vendor's Funds approval


                            //JIRA 7810 Start Snehal
                            if ((iEntityApprovalStatus.Equals(objCache.GetCodeId("A", "ENTITY_APPRV_REJ")))
                                  && (!string.IsNullOrEmpty(Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE"))))
                                  && (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE")).Contains("PAYEE_NOT_APPRV")))
                            {
                              
                                objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                                objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE"));
                                objElemChild.AppendChild(objElemTemp);
                                
                                if (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE")).Trim().Equals("PAYEE_NOT_APPRV"))
                                {
                                    objElemTemp = objDOM.CreateElement("HOLD_REASON");
                                    objElemTemp.InnerText = objCache.GetCodeDesc(objCache.GetCodeId("PAYE_APRV_PYM_UNAPRV", "HOLD_REASON_PARENT"));
                                    objElemChild.AppendChild(objElemTemp);
                                }
                                else
                                {
                                    objElemTemp = objDOM.CreateElement("HOLD_REASON");
                                    objElemTemp.InnerText = (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON")).Replace(objCache.GetCodeDesc(objCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), "")).Trim().TrimEnd(',').TrimStart(',').Replace(", ,", ",");
                                    objElemChild.AppendChild(objElemTemp);
                                }
                            }
                            else
                            {
                                objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                                objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE"));
                                objElemChild.AppendChild(objElemTemp);

                                objElemTemp = objDOM.CreateElement("HOLD_REASON");
                                objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON"));
                                objElemChild.AppendChild(objElemTemp);

                            }
                           
                          //  objElemTemp = objDOM.CreateElement("HOLD_REASON");
                            //objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON"));
                           // objElemChild.AppendChild(objElemTemp);


                           // objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                           // objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE"));
                           // objElemChild.AppendChild(objElemTemp);

                            //JIRA 7810 End
                            //Added by csingh7 for MITS 20092 :Start
                            objElemTemp = objDOM.CreateElement("Comments");
                            if (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("COMMENTS")) != null && Common.Conversion.ConvertObjToStr(objTempRead.GetValue("COMMENTS")) != "")
                                objElemTemp.InnerText = "Comments";// Common.Conversion.ConvertObjToStr(objRead.GetValue("COMMENTS"));
                            objElemChild.AppendChild(objElemTemp);
                            //Added by csingh7 for MITS 20092 :End

                            //sharishkumar Jira 6417 starts
                            if (bUseSpecificSupervisor || bUseSupAppPayments)
                            {
                                objElemTemp = objDOM.CreateElement("SubmittedTo");
                                sSubmittedTo = objCache.GetSystemLoginName(Common.Conversion.ConvertObjToInt(objTempRead.GetValue("APPROVER_ID"), m_iClientId), m_iDsnId, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), m_iClientId);
                                objElemTemp.InnerText = sSubmittedTo;
                                objElemChild.AppendChild(objElemTemp);
                            }
                            //sharishkumar Jira 6417 ends

							objDOM.FirstChild.AppendChild(objElemChild);
							objCache.Dispose();
							objCache=null;
							iCheckCount = iCheckCount + 1;
						}
						else
						{
                            objCache = new LocalCache(m_sDSN, m_iClientId);
							objElemChild=objDOM.CreateElement("Transaction");
							//Added to know the queued payments
							if (bUseQueuedPayments)
								objElemChild.SetAttribute("UtilitySetting","Q");
							else
								objElemChild.SetAttribute("UtilitySetting","H");
							//Start: rsushilaggar Collections from LSS 06/02/2010 MITS 20938
                            if (Conversion.ConvertObjToBool(objTempRead.GetValue("PAYMENT_FLAG"), m_iClientId))
							{
								objElemChild.SetAttribute("PaymentType", "P");
							}
							else
							{
								objElemChild.SetAttribute("PaymentType", "C");
							}
							//End: rsushilaggar

                            iEntityApprovalStatus = Common.Conversion.ConvertObjToInt(objTempRead.GetValue("ENTITY_APPROVAL_STATUS"), m_iClientId);
							if (iEntityApprovalStatus != (int)objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))
								objElemChild.SetAttribute("EntityApprovalStatus", "NOT-APPROVED");
							else
								objElemChild.SetAttribute("EntityApprovalStatus", "APPROVED");
							lLastTransID = lTransId;
							objElemTemp=objDOM.CreateElement("CtlNumber");
							objElemTemp.InnerText=sDitto;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("TransDate");
							objElemTemp.InnerText=sDitto;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("PayeeName");
							objElemTemp.InnerText=sDitto;;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp = objDOM.CreateElement("PrimaryClaimantName");
							objElemTemp.InnerText = sClaimantFirstName.Length > 0 ? sClaimantFirstName+" " +sClaimantLastName: sClaimantLastName;
							objElemChild.AppendChild(objElemTemp);

							sLSS_INVOICE_ID = Common.Conversion.ConvertObjToStr(objTempRead.GetDouble("LSS_INVOICE_ID"));
							if (!sLSS_INVOICE_ID.Trim().Equals("0"))
							{
								objElemTemp = objDOM.CreateElement("LSSInvoiceLink");
								objElemTemp.InnerText = "home?pg=riskmaster/Custom/Signon/SingleSignon&paramLSSInvoiceID=" + sLSS_INVOICE_ID;
								objElemTemp.SetAttribute("RMLSSInvoiceID", sLSS_INVOICE_ID);
								objElemChild.AppendChild(objElemTemp);
							}
							if (!sLSS_INVOICE_ID.Trim().Equals("0"))
							{
								objElemTemp = objDOM.CreateElement("LSSID");
								objElemTemp.InnerText = sLSS_INVOICE_ID;
								objElemChild.AppendChild(objElemTemp);
							}
							else
							{
								objElemTemp = objDOM.CreateElement("LSSID");
								objElemTemp.InnerText = "-";
								objElemChild.AppendChild(objElemTemp);
							}
							objElemTemp=objDOM.CreateElement("ClaimNumber");
							objElemTemp.InnerText=sDitto;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("PaymentAmount");
							objElemTemp.InnerText=sDitto;
							objElemChild.AppendChild(objElemTemp);
							//Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
							objElemTemp = objDOM.CreateElement("PaymentAmountNumber");
							objElemTemp.InnerText = sDitto;
							objElemChild.AppendChild(objElemTemp);
							//End: rsushilaggar 05-May-2010 Vendor's Funds approval
							objElemTemp=objDOM.CreateElement("User");
							objElemTemp.InnerText=sDitto;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("TransactionType");
							objElemTemp.InnerText=objCache.GetCodeDesc(
								Common.Conversion.ConvertStrToInteger(
								Common.Conversion.ConvertObjToStr(
								objTempRead.GetValue("TRANS_TYPE_CODE"))));
						
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("SplitAmount");
                            //Deb
							//objElemTemp.InnerText=string.Format( "{0:C}" ,Common.Conversion.ConvertObjToStr(objTempRead.GetValue("FTSAMT")));
                            objElemTemp.InnerText = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sDSN), Conversion.ConvertObjToDouble(objTempRead.GetValue("FTSAMT"), m_iClientId), m_sDSN, m_iClientId);
							objElemChild.AppendChild(objElemTemp);
							//Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
							objElemTemp = objDOM.CreateElement("SplitAmountNumber");
							objElemTemp.InnerText = sDitto;
							objElemChild.AppendChild(objElemTemp);
							//End: rsushilaggar 05-May-2010 Vendor's Funds approval
							sTmp = Conversion.GetUIDate(
                                Conversion.ConvertObjToStr(objTempRead.GetValue("FROM_DATE")), m_slangCode, m_iClientId);
							sTmp = sTmp + " - " +Conversion.GetUIDate(
                                Conversion.ConvertObjToStr(objTempRead.GetValue("TO_DATE")), m_slangCode, m_iClientId);//vkumar258 ML Changes
							objElemTemp=objDOM.CreateElement("FromToDate");
							objElemTemp.InnerText=sTmp;
							objElemChild.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("TransId");
							objElemTemp.InnerText=Common.Conversion.ConvertObjToStr(objTempRead.GetValue("TRANS_ID"));
							objElemChild.AppendChild(objElemTemp);
							//Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
							iEntityApprovalStatus = Common.Conversion.ConvertObjToInt(objTempRead.GetValue("ENTITY_APPROVAL_STATUS"), m_iClientId);
							objElemTemp = objDOM.CreateElement("PayeeStatus");
							//rsushilaggar 10-Aug-2010 MITS-21715
							//if (iEntityApprovalStatus != (int)objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))
							//    objElemTemp.InnerText = "Pending";
							//else
							//    objElemTemp.InnerText = string.Empty;
                            if (iEntityApprovalStatus == 0)
                            {
                                iEntityApprovalStatus = (int)objCacheFunctions.GetCodeIDWithShort("P", "ENTITY_APPRV_REJ");
                            }
                            objElemTemp.InnerText = objCache.GetCodeDesc(iEntityApprovalStatus);
                            objElemChild.AppendChild(objElemTemp);
                            //End: rsushilaggar 05-May-2010 Vendor's Funds approval

                            //JIRA 7810 Start Snehal

                            if ((iEntityApprovalStatus.Equals(objCache.GetCodeId("A", "ENTITY_APPRV_REJ")))
                               && (!string.IsNullOrEmpty(Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE"))))
                               && (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE")).Contains("PAYEE_NOT_APPRV")))
                            {
                              
                                objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                                objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE"));
                                objElemChild.AppendChild(objElemTemp);

                                if (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE")).Trim().Equals("PAYEE_NOT_APPRV"))
                                {
                                    objElemTemp = objDOM.CreateElement("HOLD_REASON");
                                    objElemTemp.InnerText = objCache.GetCodeDesc(objCache.GetCodeId("PAYE_APRV_PYM_UNAPRV", "HOLD_REASON_PARENT"));
                                    objElemChild.AppendChild(objElemTemp);
                                }
                                else
                                {
                                    objElemTemp = objDOM.CreateElement("HOLD_REASON");
                                    objElemTemp.InnerText = (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON")).Replace(objCache.GetCodeDesc(objCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), "")).Trim().TrimEnd(',').TrimStart(',').Replace(", ,", ",");
                                    objElemChild.AppendChild(objElemTemp);
                                }
                            }
                            else
                            {
                                objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                                objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE"));
                                objElemChild.AppendChild(objElemTemp);

                                objElemTemp = objDOM.CreateElement("HOLD_REASON");
                                objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON"));
                                objElemChild.AppendChild(objElemTemp);

                            }
                           
                           // objElemTemp = objDOM.CreateElement("HOLD_REASON");
                            //objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON"));
                            //objElemChild.AppendChild(objElemTemp);


                            //objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                           // objElemTemp.InnerText = Common.Conversion.ConvertObjToStr(objRead.GetValue("HOLD_REASON_CODE"));
                            //objElemChild.AppendChild(objElemTemp);

                            //JIRA 7810 End
                            //Added by csingh7 for MITS 20092 :Start
                            objElemTemp = objDOM.CreateElement("Comments");
                            if (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("COMMENTS")) != null && Common.Conversion.ConvertObjToStr(objTempRead.GetValue("COMMENTS")) != "")
                                objElemTemp.InnerText = "Comments";// Common.Conversion.ConvertObjToStr(objRead.GetValue("COMMENTS"));
                            objElemChild.AppendChild(objElemTemp);
                            //Added by csingh7 for MITS 20092 :End

                            //sharishkumar Jira 6417 starts
                            if (bUseSpecificSupervisor || bUseSupAppPayments)
                            {
                                objElemTemp = objDOM.CreateElement("SubmittedTo");
                                sSubmittedTo = objCache.GetSystemLoginName(Common.Conversion.ConvertObjToInt(objTempRead.GetValue("APPROVER_ID"), m_iClientId), m_iDsnId, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), m_iClientId);
                                objElemTemp.InnerText = sSubmittedTo;
                                objElemChild.AppendChild(objElemTemp);
                            }
                            //sharishkumar Jira 6417 ends

							objDOM.FirstChild.AppendChild(objElemChild);
							objCache.Dispose();
							objCache=null;
							iCheckCount = iCheckCount + 1;
						}
						bTempRead=objTempRead.Read();
					}
					CloseReader(ref objTempRead);
					p_objOutDenyTran=objDOM;
				}
				iReturnValue=1;
             
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.ListFundsTransactions.Error", m_iClientId), p_objException);
			}
			finally
			{
				CloseReader(ref objRead);
				CloseReader(ref objTempRead);
				sbSQL=null;
				sbSQLTemp=null;
				objCacheFunctions=null;
				objDOM=null;
				objElemParent=null;
				objElemChild=null;
				objElemTemp=null;
				if (objCache!=null)
				{
					objCache.Dispose();
					objCache=null;
				}
			}
			return iReturnValue;
		}
		#endregion

        /// <summary>
        /// asharma326 for checking whether the user have Permission to approve whose Payee are not approved. JIRA 7810
        /// </summary>
        /*private bool ApprvFundsIfPayeeNotApprvdPermission
        {
            get
            {
                return this.m_objDataModelFactory.Context.RMUser.IsAllowedEx(10604);//10604 is for "Approve Payment with Unapproved Payee status"
            }
        }*/

		public bool GetMaxPayDetailAmount(int p_iLob, int p_iReserveTypeCode, ref double p_dblAmount)
		{
			DbReader objReader = null;

			bool bGetMaxPayDetailAmount = false;
			string sSQL = string.Empty;
			try
			{
				p_dblAmount = -1;

				sSQL = "SELECT LINE_OF_BUS_CODE, RESERVE_TYPE_CODE,MAX(MAX_AMOUNT) FROM FUNDS_DET_LIMITS "
					+ " WHERE (USER_ID = " + m_lUserId;

				if (m_lGroupId > 0)
					sSQL += " OR GROUP_ID = " + m_lGroupId;
                //zmohammad MITs 33267 : Pay detail fix
				sSQL += ") AND LINE_OF_BUS_CODE=" + p_iLob + " AND (RESERVE_TYPE_CODE=" + p_iReserveTypeCode
				    + " OR RESERVE_TYPE_CODE='0') GROUP BY LINE_OF_BUS_CODE,RESERVE_TYPE_CODE";

				objReader = DbFactory.GetDbReader(m_sDSN, sSQL);

				if (objReader != null)
				{
					if (objReader.Read())
					{
						p_dblAmount = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(2)));
						bGetMaxPayDetailAmount = true;
					}
				}
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FundsForm.GetMaxPayDetailAmount.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return (bGetMaxPayDetailAmount);
		}

		/// Name		: GetManagerID
		/// Author		: Saurav Mahajan
		/// Date Created: 11/12/2008
		/// <summary>
		/// Get the Manager Id
		/// </summary>
		/// <param name="iRMId">User Id</param>
		/// <returns>Manager Id</returns>
		private int GetManagerID(int iRMId)
		{
			int iManagerID = 0;
			string sSQL = string.Empty;
			DbReader objReader = null;

			try
			{
				sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.MANAGER_ID "
				    + " FROM USER_DETAILS_TABLE,USER_TABLE"
				    + " WHERE USER_DETAILS_TABLE.DSNID = " + m_iDsnId
				    + " AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID"
				    + " AND USER_TABLE.USER_ID = " + iRMId;
				objReader = DbFactory.GetDbReader(m_sSecureDSN, sSQL);
				if (objReader != null)
				{
					if (objReader.Read())
					{
						iManagerID = Conversion.ConvertObjToInt(objReader.GetValue("MANAGER_ID"), m_iClientId);
					}
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.GetManagerID.Error", m_iClientId), p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Dispose();
					objReader = null;
				}
			}
			return iManagerID;
		}

		/// Name		: ValidApprovalID
		/// Author		: Saurav Mahajan
		/// Date Created: 11/12/2008
		/// <summary>
		/// Get the Approver Id
		/// </summary>
		/// <param name="iTransID">Transaction Id</param>
		/// <returns>boolean Value</returns>
		private bool ValidApprovalID(int iTransID, ArrayList arrLimitExceed, Funds p_objFunds)
		{
			int iApproverID = 0;
			int iManagerId = 0;
			FundManager objFundsManager = null;
			//Funds objFunds = null;
			CCacheFunctions objCCacheFunctions = null;
			bool bReturn = false;
			try
			{
                objCCacheFunctions = new CCacheFunctions(m_sDSN, m_iClientId);
				iApproverID = Conversion.ConvertObjToInt(m_lUserId, m_iClientId);
				iManagerId = Conversion.ConvertObjToInt(m_lManagerId, m_iClientId);

				//rbhatia4 MITS 26333 : Oct 18 2011
                //Approval process needs to consider group id too
                GroupId = GetGroupID(Conversion.ConvertObjToInt(m_lUserId, m_iClientId));

                objFundsManager = new FundManager(m_sDsnName, m_sUserName, m_sPassword, Conversion.ConvertObjToInt(m_lUserId, m_iClientId), GroupId, m_sSecureDSN, m_iDsnId, iManagerId, m_iClientId);

				//objFunds = this.m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds;
				iApproverID = objFundsManager.GetSupervisorApprovalID(iTransID);

                if (iApproverID == Conversion.ConvertObjToInt(m_lUserId, m_iClientId))
				{
					bReturn = true;
				}
				else
				{
					//objFunds.MoveTo(iTransID);
                    p_objFunds.Reason = m_sReason;
					if (bUseQueuedPayments)
					{
                        p_objFunds.StatusCode = objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS");
                        p_objFunds.VoidFlag = true;
                        p_objFunds.VoidDate = Conversion.ToDbDate(DateTime.Now);
					}
					else
					{
                        p_objFunds.VoidFlag = false;
                        p_objFunds.VoidDate = null;
                        p_objFunds.StatusCode = objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS");
					}
                    p_objFunds.ApproveUser = m_sUserName;
                    p_objFunds.DttmApproval = Conversion.ToDbDateTime(DateTime.Now);
                    p_objFunds.ApproverId = iApproverID;
                    //objFunds.FiringScriptFlag = 2;                    //nehagoel:02062014 : RMA4689 - mits 35272  : script were not getting called when checks are approved from here. 
					//objFunds.Save();
                    //Start:Export payment void data to VSS
                    if (p_objFunds.VoidFlag)
                    {
                        Claim objClaim = null;
                        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(p_objFunds.ClaimId);
                        //if (objClaim.VssClaimInd)
                        // use utility check instead of claim check
                        if(objClaim.Context.InternalSettings.SysSettings.EnableVSS.Equals(-1))
                        {
                            VssExportAsynCall objVss = new VssExportAsynCall(m_sDsnName,m_sUserName,m_sPassword, m_iClientId);
                            objVss.AsynchVssReserveExport(p_objFunds.ClaimId, p_objFunds.ClaimantEid, 0, p_objFunds.TransId, 0, "", "", "", "", "PaymentVoid");
                            objVss = null;
                        }
                        objClaim.Dispose();
                    }
                    //End:Export payment void data to VSS
                    //start 7810
                    for (int j = 0; j < arrLimitExceed.Count; j++)
                    {
                        if (arrLimitExceed[j].ToString() == "ExcPayAuth")
                            m_sWarnings = m_sWarnings + p_objFunds.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(p_objFunds.Context.LocalCache.GetChildCodeIds(p_objFunds.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), p_objFunds.Context.LocalCache.GetCodeId("EXC_PAY_AUTH", "HOLD_REASON_PARENT")), p_objFunds.Context.ClientId)) + ",";
                        else if (arrLimitExceed[j].ToString() == "ExcPayDetailAuth")
                            m_sWarnings = m_sWarnings + p_objFunds.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(p_objFunds.Context.LocalCache.GetChildCodeIds(p_objFunds.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), p_objFunds.Context.LocalCache.GetCodeId("EXC_PAY_DETAIL_AUTH", "HOLD_REASON_PARENT")), p_objFunds.Context.ClientId)) + ",";
                        else if (arrLimitExceed[j].ToString() == "ExcPayClmLmt")
                            m_sWarnings = m_sWarnings + p_objFunds.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(p_objFunds.Context.LocalCache.GetChildCodeIds(p_objFunds.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), p_objFunds.Context.LocalCache.GetCodeId("EXC_PAY_CLM_LIM", "HOLD_REASON_PARENT")), p_objFunds.Context.ClientId)) + ",";
                    }
                    if (m_sWarnings != "")
                    {
                        m_sWarnings = m_sWarnings.Remove(m_sWarnings.Length - 1, 1);
                    }
                
                    //m_sWarnings = m_sWarnings + objFunds.CtlNumber + " , ";
                    m_sWarnings = m_sWarnings + " and requires manager's approval :" + p_objFunds.CtlNumber + " , ";
                    //End 7810
					bReturn = false;
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.ValidApprovalID.Error", m_iClientId), p_objException);
			}
			finally
			{
				objFundsManager = null;
				//objFunds = null;
				objCCacheFunctions = null ;
			}
			return bReturn;
		}
		/// Name		: GetSubordinateUsers
		/// Author		: Saurav Mahajan
		/// Date Created: 11/12/2008
		/// <summary>
		/// Get Subordinate User ids with Parent Id
		/// </summary>
		/// <param name="p_sUsersToInclude">Parent User Ids</param>
		/// <returns>String containing userids with Paret Id separated by comma</returns>
		private string GetSubordinateUsers(string p_sUsersToInclude)
		{
			string sUserList = string.Empty;
			try
			{
				m_sSubordinateUsersList = string.Empty;
				sUserList=GetSubordinateUsersList(p_sUsersToInclude);

				if (!string.IsNullOrEmpty(sUserList))
				{
					sUserList = sUserList + "," + p_sUsersToInclude;
				}
				else
				{
					sUserList = p_sUsersToInclude;
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.GetSubordinateUsers.Error", m_iClientId), p_objException);
			}
			return sUserList;
		}

		/// Name		: GetSubordinateUsersList
		/// Author		: Saurav Mahajan
		/// Date Created: 11/12/2008
		/// <summary>
		/// Get Subordinate User ids
		/// </summary>
		/// <param name="p_sUsersToInclude">User Ids</param>
		/// <returns>String containing userids separated by comma</returns>
		private string GetSubordinateUsersList(string p_sUsersToInclude)
		{
            string sUserListTemp = string.Empty;
            string sThisUserTemp = string.Empty;
			StringBuilder sbSQL = null;
			StringBuilder sbTemp = null;
			DbReader objRead = null;
			try
			{
				sbSQL = new StringBuilder();
				if (string.IsNullOrEmpty(p_sUsersToInclude))
				{
					return m_sSubordinateUsersList;
				}
				else
				{
					sbSQL.Append("SELECT USER_ID FROM USER_TABLE WHERE MANAGER_ID IN (" + p_sUsersToInclude + ") AND USER_ID NOT IN (" + p_sUsersToInclude + ")");

					objRead = DbFactory.GetDbReader(m_sSecureDSN, sbSQL.ToString());
					sbTemp = new StringBuilder();
					while (objRead.Read())
					{
						if (!sbTemp.ToString().Trim().Equals(""))
						{  
                            sThisUserTemp = Conversion.ConvertObjToStr(objRead.GetValue("USER_ID"));
                            sUserListTemp = m_sSubordinateUsersList;
                            if (sUserListTemp.IndexOf(sThisUserTemp) != -1)
                            {
                                sbTemp.Append("-1");
                            }
                            else
                            {
                                if (!sbTemp.ToString().Substring(sbTemp.Length - 1).Equals(","))
                                {
                                    sbTemp.Append(",");
                                }
                                sbTemp.Append(Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")));
                            }
						}
						else
						{
                            sThisUserTemp = Conversion.ConvertObjToStr(objRead.GetValue("USER_ID"));
                            sUserListTemp = m_sSubordinateUsersList;
                            if (sUserListTemp.IndexOf(sThisUserTemp) != -1)
                            {
                                sbTemp.Append("-1");
                            }
                            else
                            {
                                sbTemp.Append(Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")));
                            }
						}
					}
					if ((!string.IsNullOrEmpty(sbTemp.ToString())) && (sbTemp.ToString() != "-1"))
					{
						if (!string.IsNullOrEmpty(m_sSubordinateUsersList))
						{
							m_sSubordinateUsersList = m_sSubordinateUsersList + "," + sbTemp.ToString();
						}
						else
						{
							m_sSubordinateUsersList = sbTemp.ToString();
						}
					}
					GetSubordinateUsersList(sbTemp.ToString());
					CloseReader(ref objRead);
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.GetSubordinateUsersList.Error", m_iClientId), p_objException);
			}
			finally
			{
				CloseReader(ref objRead);
				sbSQL = null;
				sbTemp = null;
			}
			return m_sSubordinateUsersList;
		}

		/// Name		: ToDateTime
		/// Author		: Saurav Mahajan
		/// Date Created: 11/12/2008
		/// <summary>
		/// To Convert Database Date Time Format to DateTime Format 
		/// </summary>
		/// <param name="DbDateTime">Database Date Time Format</param>
		/// <returns>DateTime Format </returns>
		private DateTime ToDateTime(string DbDateTime)
		{
			DateTime dDateTime;
			try
			{
				if (DbDateTime.Length == 14)
				{
					dDateTime = Conversion.ToDate(DbDateTime.Substring(0, 8));
					dDateTime = dDateTime.AddHours(Conversion.ConvertStrToDouble(DbDateTime.Substring(8, 2)));
					dDateTime = dDateTime.AddMinutes(Conversion.ConvertStrToDouble(DbDateTime.Substring(10, 2)));
					dDateTime = dDateTime.AddSeconds(Conversion.ConvertStrToDouble(DbDateTime.Substring(12, 2)));
				}
				else
				{
					dDateTime = Conversion.ToDate(DbDateTime);
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.ToDateTime.Error", m_iClientId), p_objException);
			}
			return dDateTime;
		}
        private string ShowItemsToAllUser(long p_lUser_Id)
        {
            string sTransIDList = string.Empty;
            StringBuilder sSql = null;
            CCacheFunctions objCCacheFunctions = null;
            DbReader objRead=null;
            DbReader objTempRead=null;
            bool bLOBOrTopUser = false;
            long lTop = 0;
            long lWC = 0;
            long lGC = 0;
            long lVA = 0;
            long lDI = 0;
            double dMaxAmount = 0.0;
            string sQuery = string.Empty;
            int iGroupID = 0;
            int iLob = 0;
            try
            {
                objCCacheFunctions = new CCacheFunctions(m_sDSN, m_iClientId);
                sQuery = "Select GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " + p_lUser_Id;
                objTempRead = DbFactory.GetDbReader(m_sDSN, sQuery);
                if (objTempRead.Read())
                {
                    iGroupID = Common.Conversion.ConvertObjToInt(objTempRead.GetValue("GROUP_ID"), m_iClientId);
                }
                CloseReader(ref objTempRead);

                if (m_bSupervisoryApproval && iGroupID >0)
                {
                    sTransIDList = DownSupChain(p_lUser_Id, "True");
                    if (!string.IsNullOrEmpty(sTransIDList))
                    {
                        sTransIDList = sTransIDList + ",";
                    }
                    if (m_bAccessGrpApprove)
                    {
                        sSql = new StringBuilder();
                        sSql.Append("SELECT DISTINCT F.TRANS_ID,F.CLAIM_ID,F.AMOUNT FROM FUNDS F ");
                        sSql.Append(" INNER JOIN CLAIM C ON C.CLAIM_ID = F.CLAIM_ID ");
                        sSql.Append(" WHERE APPROVER_ID IN (SELECT USER_ID FROM USER_MEMBERSHIP WHERE GROUP_ID = (SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID ="+ p_lUser_Id + ")) AND " );
                        if (bUseQueuedPayments)
                        {
                            sSql.Append(" ((F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString()
                                + " AND F.VOID_FLAG = 0) OR (F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS").ToString() + "AND F.VOID_FLAG <> 0))");
                        }
                        else
                        {
                            sSql.Append(" F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                            sSql.Append(" AND F.PAYMENT_FLAG <> 0 AND F.VOID_FLAG = 0");
                        }
                        sSql.Append(" AND F.PAYMENT_FLAG <> 0");

                        objRead = DbFactory.GetDbReader(m_sDSN, sSql.ToString());
                        while (objRead.Read())
                        {
                            sSql = new StringBuilder();
                            sSql.Append("SELECT MAX_AMOUNT FROM FUNDS_LIMITS INNER JOIN CLAIM C ON C.LINE_OF_BUS_CODE = FUNDS_LIMITS.LINE_OF_BUS_CODE");
                            sSql.Append(" WHERE C.CLAIM_ID =" + Common.Conversion.ConvertObjToInt(objRead.GetValue("CLAIM_ID"), m_iClientId) + " AND (FUNDS_LIMITS.GROUP_ID = " + iGroupID + " OR FUNDS_LIMITS.USER_ID = " + p_lUser_Id + ")");
                            objTempRead = DbFactory.GetDbReader(m_sDSN, sSql.ToString());
                            if (objTempRead.Read())
                            {
                                dMaxAmount = Common.Conversion.ConvertObjToDouble(objTempRead.GetValue("MAX_AMOUNT"), m_iClientId);
                            }
                            else
                            {
                                sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID")) + ",";
                            }
                            CloseReader(ref objTempRead);
                            if (dMaxAmount >= Common.Conversion.ConvertObjToDouble(objRead.GetValue("AMOUNT"), m_iClientId))
                            {
                                sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID")) + ",";
                            }
                        }
                        CloseReader(ref objRead);

                    }
                }
                else
                {
                    //Check to see who the top level users are
                    bLOBOrTopUser = GetTopApprovers(ref lTop, ref lWC, ref lGC, ref lVA, ref lDI);


                    if (!bLOBOrTopUser)
                    {
                        sSql = new StringBuilder();
                        sSql.Append("SELECT DISTINCT F.TRANS_ID,F.CLAIM_ID,F.AMOUNT FROM FUNDS F WHERE");
                        //sSql.Append(" INNER JOIN CLAIM C ON C.CLAIM_ID = F.CLAIM_ID ");
                        //sSql.Append(" INNER JOIN FUNDS_LIMITS FL ON FL.LINE_OF_BUS_CODE = C.LINE_OF_BUS_CODE ");
                        //sSql.Append(" WHERE F.AMOUNT <= FL.MAX_AMOUNT AND FL.USER_ID = " + p_lUser_Id + "  AND ");
                        if (bUseQueuedPayments)
                        {
                            sSql.Append(" ((F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString()
                                + " AND F.VOID_FLAG = 0) OR (F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS").ToString() + "AND F.VOID_FLAG <> 0))");
                        }
                        else
                        {
                            sSql.Append(" F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                            sSql.Append(" AND F.PAYMENT_FLAG <> 0 AND F.VOID_FLAG = 0");
                        }
                        sSql.Append(" AND F.PAYMENT_FLAG <> 0");

                        objRead = DbFactory.GetDbReader(m_sDSN, sSql.ToString());
                        while (objRead.Read())
                        {
                            sSql = new StringBuilder();
                            sSql.Append("SELECT MAX_AMOUNT FROM FUNDS_LIMITS F INNER JOIN CLAIM C ON C.LINE_OF_BUS_CODE = F.LINE_OF_BUS_CODE WHERE C.CLAIM_ID =" + Common.Conversion.ConvertObjToInt(objRead.GetValue("CLAIM_ID"), m_iClientId) + " AND F.USER_ID =" + p_lUser_Id);
                            objTempRead = DbFactory.GetDbReader(m_sDSN, sSql.ToString());
                            if (objTempRead.Read())
                            {
                                if (Common.Conversion.ConvertObjToDouble(objRead.GetValue("AMOUNT"), m_iClientId) <= Common.Conversion.ConvertObjToDouble(objTempRead.GetValue("MAX_AMOUNT"), m_iClientId))
                                {
                                    sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID")) + ",";
                                }
                            }
                            else
                            {
                                sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID")) + ",";
                            }
                            CloseReader(ref objTempRead);
                        }


                        CloseReader(ref objRead);
                    }
                    else
                    {
                        if (lWC == m_lUserId)
                        {
                            sSql = new StringBuilder();
                            dMaxAmount = GetTopApproverMaxAmount(p_lUser_Id, objCCacheFunctions.GetCodeIDWithShort("WC", "LINE_OF_BUSINESS"));
                            iLob = objCCacheFunctions.GetCodeIDWithShort("WC", "LINE_OF_BUSINESS");
                            sSql.Append("SELECT TRANS_ID FROM FUNDS F ");
                            sSql.Append(" INNER JOIN CLAIM C ON C.CLAIM_ID = F.CLAIM_ID ");
                            sSql.Append(" WHERE F.AMOUNT <= " + dMaxAmount + " AND ");
                            sSql.Append(" C.LINE_OF_BUS_CODE =" + iLob + " AND ");

                            if (bUseQueuedPayments)
                            {
                                sSql.Append(" ((F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString()
                                    + " AND F.VOID_FLAG = 0) OR (F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS").ToString() + "AND F.VOID_FLAG <> 0))");
                            }
                            else
                            {
                                sSql.Append(" F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                                sSql.Append(" AND F.PAYMENT_FLAG <> 0 AND F.VOID_FLAG = 0");
                            }
                            sSql.Append(" AND F.PAYMENT_FLAG <> 0");

                            objRead = DbFactory.GetDbReader(m_sDSN, sSql.ToString());
                            while (objRead.Read())
                            {
                                sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID")) + ",";
                            }
                            CloseReader(ref objRead);
                        }
                        if (lGC == m_lUserId)
                        {
                            sSql = new StringBuilder();
                            dMaxAmount = GetTopApproverMaxAmount(p_lUser_Id, objCCacheFunctions.GetCodeIDWithShort("GC", "LINE_OF_BUSINESS"));
                            iLob = objCCacheFunctions.GetCodeIDWithShort("GC", "LINE_OF_BUSINESS");
                            sSql.Append("SELECT TRANS_ID FROM FUNDS F ");
                            sSql.Append(" INNER JOIN CLAIM C ON C.CLAIM_ID = F.CLAIM_ID ");
                            sSql.Append(" WHERE F.AMOUNT <= " + dMaxAmount + " AND ");
                            sSql.Append(" C.LINE_OF_BUS_CODE =" + iLob + " AND ");

                            if (bUseQueuedPayments)
                            {
                                sSql.Append(" ((F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString()
                                    + " AND F.VOID_FLAG = 0) OR (F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS").ToString() + "AND F.VOID_FLAG <> 0))");
                            }
                            else
                            {
                                sSql.Append(" F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                                sSql.Append(" AND F.PAYMENT_FLAG <> 0 AND F.VOID_FLAG = 0");
                            }
                            sSql.Append(" AND F.PAYMENT_FLAG <> 0");

                            objRead = DbFactory.GetDbReader(m_sDSN, sSql.ToString());
                            while (objRead.Read())
                            {
                                sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID")) + ",";
                            }
                            CloseReader(ref objRead);
                        }
                        if (lVA == m_lUserId)
                        {
                            sSql = new StringBuilder();
                            dMaxAmount = GetTopApproverMaxAmount(p_lUser_Id, objCCacheFunctions.GetCodeIDWithShort("VA", "LINE_OF_BUSINESS"));
                            iLob = objCCacheFunctions.GetCodeIDWithShort("VA", "LINE_OF_BUSINESS");
                            sSql.Append("SELECT TRANS_ID FROM FUNDS F ");
                            sSql.Append(" INNER JOIN CLAIM C ON C.CLAIM_ID = F.CLAIM_ID ");
                            sSql.Append(" WHERE F.AMOUNT <= " + dMaxAmount + " AND ");
                            sSql.Append(" C.LINE_OF_BUS_CODE =" + iLob + " AND ");

                            if (bUseQueuedPayments)
                            {
                                sSql.Append(" ((F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString()
                                    + " AND F.VOID_FLAG = 0) OR (F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS").ToString() + "AND F.VOID_FLAG <> 0))");
                            }
                            else
                            {
                                sSql.Append(" F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                                sSql.Append(" AND F.PAYMENT_FLAG <> 0 AND F.VOID_FLAG = 0");
                            }
                            sSql.Append(" AND F.PAYMENT_FLAG <> 0");

                            objRead = DbFactory.GetDbReader(m_sDSN, sSql.ToString());
                            while (objRead.Read())
                            {
                                sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID")) + ",";
                            }
                            CloseReader(ref objRead);
                        }
                        if (lDI == m_lUserId)
                        {
                            sSql = new StringBuilder();
                            dMaxAmount = GetTopApproverMaxAmount(p_lUser_Id, objCCacheFunctions.GetCodeIDWithShort("DI", "LINE_OF_BUSINESS"));
                            iLob = objCCacheFunctions.GetCodeIDWithShort("DI", "LINE_OF_BUSINESS");
                            sSql.Append("SELECT TRANS_ID FROM FUNDS F ");
                            sSql.Append(" INNER JOIN CLAIM C ON C.CLAIM_ID = F.CLAIM_ID ");
                            sSql.Append(" WHERE F.AMOUNT <= " + dMaxAmount + " AND ");
                            sSql.Append(" C.LINE_OF_BUS_CODE =" + iLob + " AND ");

                            if (bUseQueuedPayments)
                            {
                                sSql.Append(" ((F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString()
                                    + " AND F.VOID_FLAG = 0) OR (F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS").ToString() + "AND F.VOID_FLAG <> 0))");
                            }
                            else
                            {
                                sSql.Append(" F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                                sSql.Append(" AND F.PAYMENT_FLAG <> 0 AND F.VOID_FLAG = 0");
                            }
                            sSql.Append(" AND F.PAYMENT_FLAG <> 0");

                            objRead = DbFactory.GetDbReader(m_sDSN, sSql.ToString());
                            while (objRead.Read())
                            {
                                sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID")) + ",";
                            }
                            CloseReader(ref objRead);
                        }
                        if (lTop == m_lUserId)
                        {
                            sSql = new StringBuilder();
                            sSql.Append("SELECT TRANS_ID FROM FUNDS F WHERE ");
                            if (bUseQueuedPayments)
                            {
                                sSql.Append(" ((F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString()
                                    + " AND F.VOID_FLAG = 0) OR (F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS").ToString() + "AND F.VOID_FLAG <> 0))");
                            }
                            else
                            {
                                sSql.Append(" F.STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                                sSql.Append(" AND F.PAYMENT_FLAG <> 0 AND F.VOID_FLAG = 0");
                            }
                            sSql.Append(" AND F.PAYMENT_FLAG <> 0");

                            objRead = DbFactory.GetDbReader(m_sDSN, sSql.ToString());
                            while (objRead.Read())
                            {
                                sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objRead.GetValue("TRANS_ID")) + ",";
                            }
                            CloseReader(ref objRead);
                        }
                    }
                }
                

                if (sTransIDList.Trim().Length > 0)
                {
                    if (sTransIDList.Substring(sTransIDList.Length - 1).Equals(","))
                    {
                        sTransIDList = sTransIDList.Substring(0, sTransIDList.Length - 1);
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SupervisorApproval.DownSupChain.Error", m_iClientId), p_objException);
            }
            finally
            {
                CloseReader(ref objRead);
                objCCacheFunctions = null;
                sSql = null;
            }

            return sTransIDList;
        }

        private double GetTopApproverMaxAmount(long p_lUserId, int p_iLineOfBus)
        {
            double dMaxAmount = 0;
            DbReader objRead = null;
            StringBuilder sbSQL = null;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT PAYMENT_MAX FROM LOB_SUP_APPROVAL WHERE USER_ID = " + p_lUserId.ToString() + " AND LOB_CODE = " + p_iLineOfBus.ToString());
                objRead = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                if (objRead.Read())
                {
                    dMaxAmount = Conversion.ConvertObjToDouble(objRead.GetValue("PAYMENT_MAX"), m_iClientId);
                }
                else
                {
                    dMaxAmount = -1;
                }
                CloseReader(ref objRead);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SupervisorApproval.GetMaxAmount.Error", m_iClientId), p_objException);
            }
            finally
            {
                CloseReader(ref objRead);
                sbSQL = null;
            }
            return dMaxAmount;
        }
		public void BuildSearchFilter(XmlDocument p_objXmlIn)
		{
			string sPayeeLastName;
			string sPayeeFirstName;
			string sPriClaimantLastName;
			string sPriClaimantFirstName;
			//rsushilaggar Vendor's fund approval 05/05/2010 MITS 20606
			string sPayeeStatus;
			string sFromDate;
			string sToDate;

			string sPayeeLastNameFilterOperator;
			string sPayeeFirstNameFilterOperator;
			string sClaimantLastNameFilterOperator;
			string sClaimantFirstNameFilterOperator;
			//rsushilaggar Vendor's fund approval 05-May-2010 MITS 20606
			string sPayeeStatusFilterOperator;
			string sDateFilterOperator;
		   

			string sSQLPayeeLastNameFilterOperator = string.Empty;
			string sSQLPayeeFirstNameFilterOperator = string.Empty;
			string sSQLClaimantLastNameFilterOperator = string.Empty;
			string sSQLClaimantFirstNameFilterOperator = string.Empty;
			//rsushilaggar Vendor's fund approval 05-May-2010 MITS 20606
			string sSQLPayeeStatusFilterOperator = string.Empty;
			string sSQLDateFilterOperator = string.Empty;
			

			StringBuilder sSqlFilter = new StringBuilder();

			string sOrderBy = string.Empty;//Shameem
			string sOrderByDirection = string.Empty;//Shameem
			StringBuilder sSqlOrderBy = new StringBuilder();//Shameem
            LocalCache objCache = null;//  7810  fix JIRA 12718
			try
			{
				XmlNode node = p_objXmlIn.SelectSingleNode("//SearchParam");
                objCache = new LocalCache(m_sDSN, m_iClientId);  //7810  fix JIRA 12718
				if (node != null)
				{
					sPayeeLastName = p_objXmlIn.SelectSingleNode("//PayeeLastName").InnerText.Replace("'", "''").Replace("*", "%").Trim();
					sPayeeFirstName = p_objXmlIn.SelectSingleNode("//PayeeFirstName").InnerText.Replace("'", "''").Replace("*", "%").Trim();
					sPriClaimantLastName = p_objXmlIn.SelectSingleNode("//PrimaryClaimantLastName").InnerText.Replace("'", "''").Replace("*", "%").Trim();
					sPriClaimantFirstName = p_objXmlIn.SelectSingleNode("//PrimaryClaimantFirstName").InnerText.Replace("'", "''").Replace("*", "%").Trim();
					//Start : rsushilaggar Vendor's fund approval 05-May-2010 MITS 20606
					sPayeeStatus = p_objXmlIn.GetElementsByTagName("PayeeStatus") != null ? ((XmlElement)p_objXmlIn.GetElementsByTagName("PayeeStatus")[0]).GetAttribute("codeid") : "";
					
					sFromDate = p_objXmlIn.SelectSingleNode("//FromDate").InnerText;
					if (!string.IsNullOrEmpty(sFromDate))
					{
						DateTime d = Convert.ToDateTime(sFromDate);
						sFromDate = Common.Conversion.ToDbDate(d);
					}
					sToDate = p_objXmlIn.SelectSingleNode("//ToDate").InnerText;
					if (!string.IsNullOrEmpty(sToDate))
					{
						DateTime d = Convert.ToDateTime(sToDate);
						sToDate = Common.Conversion.ToDbDate(d);
					}
					//End: rsushilaggar
					sPayeeLastNameFilterOperator = p_objXmlIn.SelectSingleNode("//PayeeLastNameComparison").InnerText;
					sPayeeFirstNameFilterOperator = p_objXmlIn.SelectSingleNode("//PayeeFirstNameComparison").InnerText;
					sClaimantLastNameFilterOperator = p_objXmlIn.SelectSingleNode("//PrimaryClaimantLastNameComparison").InnerText;
					sClaimantFirstNameFilterOperator = p_objXmlIn.SelectSingleNode("//PrimaryClaimantFirstNameComparison").InnerText;
					//Start: rsushilaggar Vendor's fund approval 05-May-2010 MITS 20606
					sPayeeStatusFilterOperator = p_objXmlIn.SelectSingleNode("//PayeeStatusComparison") != null ? p_objXmlIn.SelectSingleNode("//PayeeStatusComparison").InnerText : "";
					sDateFilterOperator = p_objXmlIn.SelectSingleNode("//FromDateComparison") != null ? p_objXmlIn.SelectSingleNode("//FromDateComparison").InnerText : "";
					//End : rsushilaggar
					// sOrderBy = p_objXmlIn.SelectSingleNode("//OrderBy").InnerText;//Shameem
					// sOrderByDirection = p_objXmlIn.SelectSingleNode("//OrderByDirection").InnerText;//Shameem

					switch (sPayeeLastNameFilterOperator)
					{
						case "=":
							sSQLPayeeLastNameFilterOperator = " LIKE "; break;
						case "<>":
							sSQLPayeeLastNameFilterOperator = " NOT LIKE "; break;
						case ">":
							sSQLPayeeLastNameFilterOperator = " > "; break;
						case ">=":
							sSQLPayeeLastNameFilterOperator = " >= "; break;
						case "<":
							sSQLPayeeLastNameFilterOperator = " < "; break;
						case "<=":
							sSQLPayeeLastNameFilterOperator = " <= "; break;
					}

					switch (sPayeeFirstNameFilterOperator)
					{
						case "=":
							sSQLPayeeFirstNameFilterOperator = " LIKE "; break;
						case "<>":
							sSQLPayeeFirstNameFilterOperator = " NOT LIKE "; break;
						case ">":
							sSQLPayeeFirstNameFilterOperator = " > "; break;
						case ">=":
							sSQLPayeeFirstNameFilterOperator = " >= "; break;
						case "<":
							sSQLPayeeFirstNameFilterOperator = " < "; break;
						case "<=":
							sSQLPayeeFirstNameFilterOperator = " <= "; break;
					}

					switch (sClaimantLastNameFilterOperator)
					{
						case "=":
							sSQLClaimantLastNameFilterOperator = " LIKE "; break;
						case "<>":
							sSQLClaimantLastNameFilterOperator = " NOT LIKE "; break;
						case ">":
							sSQLClaimantLastNameFilterOperator = " > "; break;
						case ">=":
							sSQLClaimantLastNameFilterOperator = " >= "; break;
						case "<":
							sSQLClaimantLastNameFilterOperator = " < "; break;
						case "<=":
							sSQLClaimantLastNameFilterOperator = " <= "; break;
					}

					switch (sClaimantFirstNameFilterOperator)
					{
						case "=":
							sSQLClaimantFirstNameFilterOperator = " LIKE "; break;
						case "<>":
							sSQLClaimantFirstNameFilterOperator = " NOT LIKE "; break;
						case ">":
							sSQLClaimantFirstNameFilterOperator = " > "; break;
						case ">=":
							sSQLClaimantFirstNameFilterOperator = " >= "; break;
						case "<":
							sSQLClaimantFirstNameFilterOperator = " < "; break;
						case "<=":
							sSQLClaimantFirstNameFilterOperator = " <= "; break;
					}



					//start : rsushilaggar Vendor's fund approval 05-May-2010 MITS 20606
					switch (sPayeeStatusFilterOperator)
					{
						case "=":
							sSQLPayeeStatusFilterOperator = " LIKE "; break;
						case "<>":
							sSQLPayeeStatusFilterOperator = " NOT LIKE "; break;
					}

					switch (sDateFilterOperator)
					{
						case "=":
							sSQLDateFilterOperator = " LIKE "; break;
						case "<>":
							sSQLDateFilterOperator = " NOT LIKE "; break;
						case ">":
							sSQLDateFilterOperator = " > "; break;
						case ">=":
							sSQLDateFilterOperator = " >= "; break;
						case "<":
							sSQLDateFilterOperator = " < "; break;
						case "<=":
							sSQLDateFilterOperator = " <= "; break;
						case "between":
							sSQLDateFilterOperator = "BETWEEN"; break;
					}

					//end : rsushilaggar Vendor's fund approval

					if (!string.IsNullOrEmpty(sPayeeLastName))
					{
						sSqlFilter.Append(" AND FUNDS.LAST_NAME" + sSQLPayeeLastNameFilterOperator);
						sSqlFilter.Append("'" + sPayeeLastName + "'");
					}

					if (!string.IsNullOrEmpty(sPayeeFirstName))
					{
						sSqlFilter.Append(" AND FUNDS.FIRST_NAME" + sSQLPayeeFirstNameFilterOperator);
						sSqlFilter.Append("'" + sPayeeFirstName + "'");
					}

					if (!string.IsNullOrEmpty(sPriClaimantLastName))
					{
						sSqlFilter.Append(" AND ENTITY.LAST_NAME" + sSQLClaimantLastNameFilterOperator);
						sSqlFilter.Append("'" + sPriClaimantLastName + "'");

					}

					if (!string.IsNullOrEmpty(sPriClaimantFirstName))
					{
						sSqlFilter.Append(" AND ENTITY.FIRST_NAME" + sSQLClaimantFirstNameFilterOperator);
						sSqlFilter.Append("'" + sPriClaimantFirstName + "'");
					}
					//start : rsushilaggar Vendor's fund approval 05-May-2010 MITS 20606
					if (!string.IsNullOrEmpty(sPayeeStatus))
					{
						sSqlFilter.Append(" AND ENTITY2.ENTITY_APPROVAL_STATUS" + sSQLPayeeStatusFilterOperator);
						sSqlFilter.Append("'" + sPayeeStatus + "'");
                        //7810 start fix JIRA 12718
                        if (sPayeeStatus == objCache.GetCodeId("P", "ENTITY_APPRV_REJ").ToString())
                        {
                            sSqlFilter.Append(" OR ENTITY2.ENTITY_APPROVAL_STATUS" + sSQLPayeeStatusFilterOperator);
                            sSqlFilter.Append("'0'");
                        }
                        //7810 end fix JIRA 12718
					}

					if(!string.IsNullOrEmpty(sFromDate) || !string.IsNullOrEmpty(sToDate))
					{
						sSqlFilter.Append(" AND TRANS_DATE " + sSQLDateFilterOperator);
						if(sSQLDateFilterOperator == "BETWEEN")
						{
							sSqlFilter.Append(" '"+ sFromDate +"' AND '" + sToDate +"'");
						}
						else
						{
							sSqlFilter.Append("'"+ sFromDate +"'");
						}
					}
					//End : rsushilaggar Vendor's fund approval


					sFilterSql = sSqlFilter.ToString();

					if (!string.IsNullOrEmpty(sOrderBy) && !string.IsNullOrEmpty(sOrderByDirection) )
						sSqlOrderBy.Append(sOrderBy + " " + sOrderByDirection);

					sOrderBySql = sSqlOrderBy.ToString();
				}
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisoryApproval.BuildSearchFilter.Error", m_iClientId), p_objException);
			}
		}

		/// <summary>
		/// Get LSS Collections trans Id for the approver
		/// rsushilaggar 06/03/2010 MITS 20938
		/// </summary>
		/// <param name="m_sUserName"></param>
		/// <returns></returns>
		private string GetCollectionTransId(string m_sUserName)
		{
			string sTransIDList = string.Empty;
			string sSql = string.Empty;
			string sUserNameList = string.Empty;
			const string SLSSUSER = "LSSINF";
            CCacheFunctions objCacheFunctions = new CCacheFunctions(m_sDSN, m_iClientId);
            NameValueCollection nvColl = RMConfigurationManager.GetNameValueSectionSettings("LSSInterface", m_sDSN, m_iClientId);
			if (nvColl != null)
			{
				sUserNameList = nvColl["CollectionApprovalLogin"];
			}

			string[] sUserNameArray = sUserNameList.Split(',');
			bool sIsUserApprover = false;
			foreach (string sUser in sUserNameArray)
			{
				if (sUser == m_sUserName)
				{
					sIsUserApprover = true;
					break;
				}
			}

			if (string.IsNullOrEmpty(sUserNameList) || sIsUserApprover)
			{
				sSql = "SELECT TRANS_ID FROM FUNDS WHERE ADDED_BY_USER = '" + SLSSUSER + "' AND STATUS_CODE =" + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString();
				using (DbReader objReader = DbFactory.GetDbReader(m_sDSN, sSql))
				{
					while (objReader.Read())
					{
						if ((!sTransIDList.Trim().Equals("")))
						{
							if ((!sTransIDList.Substring(sTransIDList.Length - 1).Equals(",")))
							{
								sTransIDList = sTransIDList + ",";
							}
						}
						sTransIDList = sTransIDList + Common.Conversion.ConvertObjToStr(objReader.GetValue("TRANS_ID")) + ",";
					}
				}
			}

			if (sTransIDList.Trim().Length > 0)
			{
				if (sTransIDList.Substring(sTransIDList.Length - 1).Equals(","))
				{
					sTransIDList = sTransIDList.Substring(0, sTransIDList.Length - 1);
				}
			}
			return sTransIDList;
		}

		public XmlDocument GetPayeeCheckData(int iTabIndex, int iPageNumber)
		{
			XmlDocument objOutDoc = new XmlDocument();

			try
			{
				if (iTabIndex == 1)
					objOutDoc = FillFreezePayeeList(iPageNumber);

				if (iTabIndex == 2)
					objOutDoc = FillHoldCheckList(iPageNumber);

				if (iTabIndex == 3)
					objOutDoc = FillFreezeBatchList(iPageNumber);

			}
			catch (RMAppException p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.GetPayeeCheckData.Error", m_iClientId), p_objException);
			}
			catch (Exception p_objException)
			{
				throw p_objException;
			}

			return objOutDoc;
		}
		public XmlDocument FillFreezePayeeList(int iPageNumber)
		{
			XmlDocument objOutDoc = new XmlDocument();
			XmlElement objParentElem = null;
			XmlElement objChildNode = null;
			DbConnection objDbConn = null;
			StringBuilder sSQL;
			int TotalPages = 0;
			int StartRecord = 0;
			int EndRecord = 0;
			int i = 0;

			try
			{
				//Merging MITS 13327: Cases when a Payee is freezed by OFAC Check and someone unfreezes him/her from
				//Entity Screen.
				objDbConn = DbFactory.GetDbConnection(DbFactory.GetDbConnection(m_sDSN).ConnectionString);
				objDbConn.Open();
				sSQL = new StringBuilder();
                sSQL.Append("UPDATE PAYEE_CHECK_DETAIL SET REVIEWED = -1 WHERE ((REVIEWED IS NULL OR REVIEWED <> -1) AND (ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY WHERE ((FREEZE_PAYMENTS IS NULL OR FREEZE_PAYMENTS <> -1) OR (DELETED_FLAG = -1)))))");
				objDbConn.ExecuteNonQuery(sSQL.ToString());
				objDbConn.Close();


				//Fetch the total number of pages
				TotalPages = CalculatePage(ref iPageNumber, ref StartRecord, ref EndRecord, "PAYEE");
				//Fetch the list of payees on Freeze from PAYEE_CHECK_DETAIL table.
				objParentElem = objOutDoc.CreateElement("Payees");
				objParentElem.SetAttribute("PageNumber", iPageNumber.ToString());
				objParentElem.SetAttribute("TotalPages", TotalPages.ToString());

				sSQL.Remove(0, sSQL.Length);
                //avipinsrivas Start : Worked for Jira-340
                //sSQL.Append("SELECT D.TABLE_NAME, B.ENTITY_ID ,A.LAST_NAME, A.FIRST_NAME, A.TAX_ID, A.PHONE1, A.PHONE2, B.DTTM_FREEZE, A.BIRTH_DATE FROM ENTITY A , PAYEE_CHECK_DETAIL B, GLOSSARY C,GLOSSARY_TEXT D ");
                //sSQL.Append("WHERE A.ENTITY_ID = B.ENTITY_ID AND B.REVIEWED = 0 AND A.ENTITY_TABLE_ID = C.TABLE_ID AND C.TABLE_ID = D.TABLE_ID ORDER BY A.LAST_NAME");

                sSQL.Append(" SELECT D.TABLE_NAME, B.ENTITY_ID ,A.LAST_NAME, A.FIRST_NAME, A.TAX_ID, A.PHONE1, A.PHONE2, B.DTTM_FREEZE, A.BIRTH_DATE ");
                sSQL.Append(" FROM ENTITY A ");
                if (this.m_objDataModelFactory.Context.InternalSettings.SysSettings.UseEntityRole)
                    sSQL.Append(" INNER JOIN ENTITY_X_ROLES EXR ON EXR.ENTITY_ID = A.ENTITY_ID ");
                sSQL.Append(" INNER JOIN PAYEE_CHECK_DETAIL B ON A.ENTITY_ID = B.ENTITY_ID ");
                if (this.m_objDataModelFactory.Context.InternalSettings.SysSettings.UseEntityRole)
                    sSQL.Append(" INNER JOIN GLOSSARY C ON EXR.ENTITY_TABLE_ID = C.TABLE_ID ");
                else
                    sSQL.Append(" INNER JOIN GLOSSARY C ON A.ENTITY_TABLE_ID = C.TABLE_ID ");
                sSQL.Append(" INNER JOIN GLOSSARY_TEXT D ON C.TABLE_ID = D.TABLE_ID ");
                sSQL.Append(" WHERE B.REVIEWED = 0 ");
                sSQL.Append(" ORDER BY A.LAST_NAME ");
                //avipinsrivas End

				using (DbReader objReader = DbFactory.GetDbReader(DbFactory.GetDbConnection(m_sDSN).ConnectionString, sSQL.ToString()))
				{

					while (objReader.Read())
					{
						i++;
						if (i >= StartRecord && i <= EndRecord)
						{
							objChildNode = objOutDoc.CreateElement("Payee");

							objChildNode.SetAttribute("EntityId", objReader["ENTITY_ID"].ToString());
							objChildNode.SetAttribute("Dttm_Freeze", Riskmaster.Common.Conversion.ToDate(objReader["DTTM_FREEZE"].ToString()).ToString());
							objChildNode.SetAttribute("Last_Name", objReader["LAST_NAME"].ToString());
							objChildNode.SetAttribute("First_Name", objReader["FIRST_NAME"].ToString());
							objChildNode.SetAttribute("Type", objReader["TABLE_NAME"].ToString());
							objChildNode.SetAttribute("Office_Phone", objReader["PHONE1"].ToString());
							objChildNode.SetAttribute("Home_Phone", objReader["PHONE2"].ToString());
							objChildNode.SetAttribute("Tax_Id", objReader["TAX_ID"].ToString());
                            if (!string.IsNullOrEmpty(objReader["BIRTH_DATE"].ToString()))
                            {
                                objChildNode.SetAttribute("Birth_Date", Riskmaster.Common.Conversion.ToDate(objReader["BIRTH_DATE"].ToString()).ToString());
                            }
                            else
                            {
                                objChildNode.SetAttribute("Birth_Date", string.Empty);
                            }

							objParentElem.AppendChild(objChildNode);
						}

						if (i > EndRecord)
							break;
					}

					objOutDoc.AppendChild(objParentElem);

				}
			}
			catch (RMAppException p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.FillFreezePayeeList.Error", m_iClientId), p_objException);
			}
			catch (Exception p_objException)
			{
				throw p_objException;
			}

			return objOutDoc;
		}

		public XmlDocument FillHoldCheckList(int iPageNumber)
		{
			XmlDocument objOutDoc = new XmlDocument();
			XmlElement objParentElem = null;
			XmlElement objChildNode = null;
			DbConnection objDbConn = null;
			StringBuilder sSQL;
			int TotalPages = 0;
			int StartRecord = 0;
			int EndRecord = 0;
			int i = 0;
			int i_StatusCode = 0;

			try
			{
				//Merging MITS 13327: Cases When a Check (put on hold by Payee Check Utility) is Un-holded from Riskmaster 
				//Interface. We need to update the hold_check_detail table in that case.
				objDbConn = DbFactory.GetDbConnection(DbFactory.GetDbConnection(m_sDSN).ConnectionString);
				objDbConn.Open();
				sSQL = new StringBuilder();
				sSQL.Append("SELECT CODE_ID FROM CODES WHERE SHORT_CODE = 'H' AND TABLE_ID=(SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME LIKE 'CHECK_STATUS')");
				i_StatusCode = objDbConn.ExecuteInt(sSQL.ToString());
                sSQL.Remove(0, sSQL.Length);
                sSQL.Append("UPDATE HOLD_CHECK_DETAIL SET REVIEWED = -1 WHERE ((REVIEWED IS NULL OR REVIEWED <> -1) AND (TRANS_ID IN (SELECT TRANS_ID FROM FUNDS WHERE ((STATUS_CODE IS NULL OR STATUS_CODE <>"+ i_StatusCode.ToString() + ") OR (VOID_FLAG = -1)))))");
				objDbConn.ExecuteNonQuery(sSQL.ToString());

				objDbConn.Close();

				//Fetch the total number of pages
				TotalPages = CalculatePage(ref iPageNumber, ref StartRecord, ref EndRecord, "CHECKS");

				//Fetch the List of the Checks Put on Hold
				objParentElem = objOutDoc.CreateElement("Checks");
				objParentElem.SetAttribute("PageNumber", iPageNumber.ToString());
				objParentElem.SetAttribute("TotalPages", TotalPages.ToString());

				sSQL.Remove(0, sSQL.Length);
				sSQL.Append("SELECT A.TRANS_ID, A.CTL_NUMBER, A.CLAIM_NUMBER, A.DATE_OF_CHECK, A.TRANS_DATE, A.LAST_NAME, A.FIRST_NAME, A.AMOUNT,B.DTTM_HOLD  FROM FUNDS A, HOLD_CHECK_DETAIL B ");
                sSQL.Append("WHERE A.TRANS_ID = B.TRANS_ID AND B.REVIEWED = 0 ORDER BY A.CTL_NUMBER");

				using (DbReader objReader = DbFactory.GetDbReader(DbFactory.GetDbConnection(m_sDSN).ConnectionString, sSQL.ToString()))
				{

					while (objReader.Read())
					{
						i++;
						if (i >= StartRecord && i <= EndRecord)
						{
							objChildNode = objOutDoc.CreateElement("Check");

							objChildNode.SetAttribute("TransId", objReader["TRANS_ID"].ToString());
							objChildNode.SetAttribute("DateOfCheck", Riskmaster.Common.Conversion.ToDate(objReader["DATE_OF_CHECK"].ToString()).ToString());
							objChildNode.SetAttribute("Last_Name", objReader["LAST_NAME"].ToString());
							objChildNode.SetAttribute("First_Name", objReader["FIRST_NAME"].ToString());
							objChildNode.SetAttribute("CtlNumber", objReader["CTL_NUMBER"].ToString());
							objChildNode.SetAttribute("ClaimNumber", objReader["CLAIM_NUMBER"].ToString());
							objChildNode.SetAttribute("Amount", objReader["AMOUNT"].ToString());
							objChildNode.SetAttribute("TransDate", Riskmaster.Common.Conversion.ToDate(objReader["TRANS_DATE"].ToString()).ToString());
							objChildNode.SetAttribute("DttmHold", Riskmaster.Common.Conversion.ToDate(objReader["DTTM_HOLD"].ToString()).ToString());

							objParentElem.AppendChild(objChildNode);
						}

						if (i > EndRecord)
							break;
					}

					objOutDoc.AppendChild(objParentElem);

				}
			}
			catch (RMAppException p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.FillHoldCheckList.Error", m_iClientId), p_objException);
			}
			catch (Exception p_objException)
			{
				throw p_objException;
			}

			return objOutDoc;
		}

		public XmlDocument FillFreezeBatchList(int iPageNumber)
		{
			XmlDocument objOutDoc = new XmlDocument();
			XmlElement objParentElem = null;
			XmlElement objChildNode = null;
			DbConnection objDbConn = null;
			StringBuilder sSQL;
			int TotalPages = 0;
			int StartRecord = 0;
			int EndRecord = 0;
			int i = 0;


			try
			{
				//Merging MITS 13327: Cases When the Auto Batch is freezed by the PayeeCheck Utility but someone
				//Un Freezes it through Riskmaster.

				objDbConn = DbFactory.GetDbConnection(DbFactory.GetDbConnection(m_sDSN).ConnectionString);
				objDbConn.Open();
				sSQL = new StringBuilder();
				sSQL.Append("UPDATE FREEZE_BAT_DETAIL SET REVIEWED = -1 WHERE (REVIEWED IS NULL OR REVIEWED <> -1) AND AUTO_BATCH_ID IN (SELECT AUTO_BATCH_ID FROM FUNDS_AUTO_BATCH WHERE (FREEZE_BATCH_FLAG IS NULL OR FREEZE_BATCH_FLAG <> -1))");

				objDbConn.ExecuteNonQuery(sSQL.ToString());
				objDbConn.Close();

				//Fetch the total number of pages
				TotalPages = CalculatePage(ref iPageNumber, ref StartRecord, ref EndRecord, "AUTOCHECKS");

				//Fetch the List of the Auto Checks Put on Hold
				objParentElem = objOutDoc.CreateElement("AutoChecks");
				objParentElem.SetAttribute("PageNumber", iPageNumber.ToString());
				objParentElem.SetAttribute("TotalPages", TotalPages.ToString());

				sSQL.Remove(0, sSQL.Length);

				sSQL.Append("SELECT A.AUTO_BATCH_ID, MIN(A.PRINT_DATE) AS PRINT_DATE, MIN(A.LAST_NAME) AS LAST_NAME, MIN(A.FIRST_NAME) AS FIRST_NAME, MIN(A.AMOUNT) AS AMOUNT, MIN(B.DTTM_FREEZE) AS DATE_FREEZE ,MIN(A.CLAIM_NUMBER) AS CLAIMNUMBER ");
				sSQL.Append("FROM FUNDS_AUTO A ,FREEZE_BAT_DETAIL B ");
				sSQL.Append("WHERE REVIEWED = 0 AND A.AUTO_BATCH_ID = B.AUTO_BATCH_ID ");
                sSQL.Append("GROUP BY A.AUTO_BATCH_ID ORDER BY A.AUTO_BATCH_ID");

				using (DbReader objReader = DbFactory.GetDbReader(DbFactory.GetDbConnection(m_sDSN).ConnectionString, sSQL.ToString()))
				{

					while (objReader.Read())
					{
						i++;
						if (i >= StartRecord && i <= EndRecord)
						{
							objChildNode = objOutDoc.CreateElement("AutoCheck");

							objChildNode.SetAttribute("AutoBatchId", objReader["AUTO_BATCH_ID"].ToString());
							objChildNode.SetAttribute("PrintDate", Riskmaster.Common.Conversion.ToDate(objReader["PRINT_DATE"].ToString()).ToString());
							objChildNode.SetAttribute("Last_Name", objReader["LAST_NAME"].ToString());
							objChildNode.SetAttribute("First_Name", objReader["FIRST_NAME"].ToString());
							objChildNode.SetAttribute("ClaimNumber", objReader["CLAIMNUMBER"].ToString());
							objChildNode.SetAttribute("Amount", objReader["AMOUNT"].ToString());
							objChildNode.SetAttribute("DttmFreeze", Riskmaster.Common.Conversion.ToDate(objReader["DATE_FREEZE"].ToString()).ToString());

							objParentElem.AppendChild(objChildNode);
						}

						if (i > EndRecord)
							break;
					}

					objOutDoc.AppendChild(objParentElem);

				}
			}
			catch (RMAppException p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.FillFreezeBatchList.Error", m_iClientId), p_objException);
			}
			catch (Exception p_objException)
			{
				throw p_objException;
			}

			return objOutDoc;
		}

		public XmlDocument SaveAndReleasePayeeCheckData(int iTabIndex, int iPageNumber, string sPayeeList, string sCheckList, string sBatchList)
		{
			XmlDocument objOutDoc = new XmlDocument();
			DbConnection objDbConn = null;
			string sTempList = string.Empty;
			string sSQL = string.Empty;
			string sTemp = string.Empty;
			int StatusCode = 0;

			try
			{
				if (iTabIndex == 1)
				{
					sTempList = sPayeeList;

					if (sTempList.EndsWith(","))
						sTempList = sTempList.Substring(0, sTempList.Length - 1);
					if (sTempList.StartsWith(","))
						sTempList = sTempList.Substring(1);

					objDbConn = DbFactory.GetDbConnection(DbFactory.GetDbConnection(m_sDSN).ConnectionString);
					objDbConn.Open();
					sSQL = "UPDATE PAYEE_CHECK_DETAIL SET REVIEWED = -1 ,USER_UNFREEZE = '" + m_sUserName + "'  ,DTTM_UNFREEZE = '" + Conversion.ToDbDateTime(System.DateTime.Now) + "' WHERE ENTITY_ID in (" + sTempList + ")";
					objDbConn.ExecuteNonQuery(sSQL);
					sSQL = "UPDATE ENTITY SET FREEZE_PAYMENTS = 0 WHERE ENTITY_ID in (" + sTempList + ")";
					objDbConn.ExecuteNonQuery(sSQL);
					objDbConn.Close();
				}
				else if (iTabIndex == 2)
				{
					sTempList = sCheckList;

					if (sTempList.EndsWith(","))
						sTempList = sTempList.Substring(0, sTempList.Length - 1);
					if (sTempList.StartsWith(","))
						sTempList = sTempList.Substring(1);

					objDbConn = DbFactory.GetDbConnection(DbFactory.GetDbConnection(m_sDSN).ConnectionString);
					objDbConn.Open();
					sSQL = "UPDATE HOLD_CHECK_DETAIL SET REVIEWED = -1, DTTM_UNHOLD = '" + Conversion.ToDbDateTime(System.DateTime.Now) + "'  ,USER_UNHOLD = '" + m_sUserName + "' WHERE TRANS_ID in (" + sTempList + ")";
					objDbConn.ExecuteNonQuery(sSQL);

					//Loop for multiple selected Checks on Hold
					while (sTempList.Contains(","))
					{
						sTemp = sTempList.Substring(0, sTempList.IndexOf(","));
						sTempList = sTempList.Substring(sTempList.IndexOf(",") + 1, sTempList.Length - sTempList.IndexOf(",") - 1);

						sSQL = "SELECT OLD_STATUS_CODE FROM HOLD_CHECK_DETAIL WHERE TRANS_ID = " + sTemp;
						StatusCode = objDbConn.ExecuteInt(sSQL);

						sSQL = "UPDATE FUNDS SET STATUS_CODE = " + StatusCode + "WHERE TRANS_ID = " + sTemp;
						objDbConn.ExecuteNonQuery(sSQL);
					}
					//In Case only one Check is Selected
					if (!sTempList.Contains(","))
					{
						sSQL = "SELECT OLD_STATUS_CODE FROM HOLD_CHECK_DETAIL WHERE TRANS_ID = " + sTempList;
						StatusCode = objDbConn.ExecuteInt(sSQL);

						sSQL = "UPDATE FUNDS SET STATUS_CODE = " + StatusCode + "WHERE TRANS_ID = " + sTempList;
						objDbConn.ExecuteNonQuery(sSQL);
					}

					objDbConn.Close();
				}
				else if (iTabIndex == 3)
				{
					sTempList = sBatchList;

					if (sTempList.EndsWith(","))
						sTempList = sTempList.Substring(0, sTempList.Length - 1);
					if (sTempList.StartsWith(","))
						sTempList = sTempList.Substring(1);

					objDbConn = DbFactory.GetDbConnection(DbFactory.GetDbConnection(m_sDSN).ConnectionString);
					objDbConn.Open();
					sSQL = "UPDATE FREEZE_BAT_DETAIL SET REVIEWED = -1, DTTM_UNFREEZE = '" + Conversion.ToDbDateTime(System.DateTime.Now) + "'  ,USER_UNFREEZE = '" + m_sUserName + "' WHERE AUTO_BATCH_ID in (" + sTempList + ")";
					objDbConn.ExecuteNonQuery(sSQL);
					sSQL = "UPDATE FUNDS_AUTO_BATCH SET FREEZE_BATCH_FLAG = 0 WHERE AUTO_BATCH_ID IN (" + sTempList + ")";
					objDbConn.ExecuteNonQuery(sSQL);
					objDbConn.Close();
				}

			}
			catch (RMAppException p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.SaceAndReleasePayeeCheckData.Error", m_iClientId), p_objException);
			}
			catch (Exception p_objException)
			{
				throw p_objException;
			}

			return objOutDoc;
		}
		/// <summary>
        //RBHATIA4 MITS 26333 Start
        /// Get the Group Id
        /// </summary>
        /// <param name="iRMId">User Id</param>
        /// <returns>Manager Id</returns>
        protected int GetGroupID(int iManagerId)
        {
            int iThisGroupID = 0;
            string sSQL = string.Empty;
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " + iManagerId;
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iThisGroupID = Conversion.ConvertObjToInt(objReader.GetValue("GROUP_ID"), m_iClientId);
                    }
                }
                objReader.Close();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException("An Error Occured Getting The Group ID For The Manager", p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return iThisGroupID;
        }

		protected int CalculatePage(ref int PageNumber, ref int StartRecord, ref int EndRecord, string Type)
		{
			int TotalPages = 0;
			int TotalRecords = 0;
			int RecordsPerPage = 0; 
			//string sSQL = string.Empty;
			double PagesInDb = 0.0;
			DbConnection objDbConn = null;
            StringBuilder sbSQL;
            sbSQL = new StringBuilder();
            Riskmaster.Settings.SysSettings objSettings = new Riskmaster.Settings.SysSettings(DbFactory.GetDbConnection(m_sDSN).ConnectionString, m_iClientId);

			objDbConn = DbFactory.GetDbConnection(DbFactory.GetDbConnection(m_sDSN).ConnectionString);
			objDbConn.Open();

            if (Type == "PAYEE")
            {
                //avipinsrivas Start : Worked for Jira-340
                //sbSQL.Append("SELECT COUNT(*) FROM ENTITY A , PAYEE_CHECK_DETAIL B, GLOSSARY C ");
                //sbSQL.Append("WHERE A.ENTITY_ID = B.ENTITY_ID AND B.REVIEWED = 0 AND A.ENTITY_TABLE_ID = C.TABLE_ID ");
                //sSQL = "SELECT COUNT(*) AS COUNT FROM PAYEE_CHECK_DETAIL WHERE REVIEWED = 0;";

                sbSQL.Append(" SELECT COUNT(*) FROM ENTITY A ");
                if (this.m_objDataModelFactory.Context.InternalSettings.SysSettings.UseEntityRole)
                    sbSQL.Append(" INNER JOIN ENTITY_X_ROLES EXR ON EXR.ENTITY_ID = A.ENTITY_ID ");
                sbSQL.Append(" INNER JOIN PAYEE_CHECK_DETAIL B ON A.ENTITY_ID = B.ENTITY_ID ");
                if (this.m_objDataModelFactory.Context.InternalSettings.SysSettings.UseEntityRole)
                    sbSQL.Append(" INNER JOIN GLOSSARY C ON EXR.ENTITY_TABLE_ID = C.TABLE_ID ");
                else
                    sbSQL.Append(" INNER JOIN GLOSSARY C ON A.ENTITY_TABLE_ID = C.TABLE_ID ");
                sbSQL.Append(" WHERE B.REVIEWED = 0 ");
                //avipinsrivas End
            }
            else if (Type == "CHECKS")
            {
                sbSQL.Append("SELECT COUNT(*) FROM FUNDS A, HOLD_CHECK_DETAIL B ");
                sbSQL.Append("WHERE A.TRANS_ID = B.TRANS_ID AND B.REVIEWED = 0 ");
                //sSQL = "SELECT COUNT(*) AS COUNT FROM HOLD_CHECK_DETAIL WHERE REVIEWED = 0;";
            }
            else if (Type == "AUTOCHECKS")
            {
                sbSQL.Append("Select COUNT (*) FROM (SELECT A.AUTO_BATCH_ID ");
                sbSQL.Append("FROM FUNDS_AUTO A ,FREEZE_BAT_DETAIL B ");
                sbSQL.Append("WHERE REVIEWED = 0 AND A.AUTO_BATCH_ID = B.AUTO_BATCH_ID GROUP BY A.AUTO_BATCH_ID )a ");
              
                //sSQL = "SELECT COUNT(DISTINCT(AUTO_BATCH_ID)) AS COUNT FROM FREEZE_BAT_DETAIL WHERE REVIEWED = 0;";
            }

			TotalRecords = objDbConn.ExecuteInt(sbSQL.ToString());
            sbSQL.Length = 0;
			objDbConn.Close();

			RecordsPerPage = objSettings.ResultPayeeCheckReview;

			PagesInDb = (double)TotalRecords / RecordsPerPage;

			if ((PagesInDb - (int)PagesInDb) > 0)
				TotalPages = ((int)PagesInDb) + 1;
			else
				TotalPages = (int)PagesInDb;


			if (TotalPages < PageNumber)
				PageNumber = TotalPages;


			if (PageNumber == 1)
				StartRecord = 1;
			else
				StartRecord = ((PageNumber - 1) * RecordsPerPage) + 1;

			EndRecord = (PageNumber * RecordsPerPage);

			if (PageNumber == 0)
				PageNumber = 1;

			if (TotalPages == 0)
				TotalPages = 1;

			return TotalPages;
		}

        /// <summary>
        /// To OverRide the Apllied Limit Amount.For R8 enhancement of OverRide Authority check
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="p_iClaimId">Claim For which amount will be overriden</param>
        /// <param name="p_sOverRideUserName">user for which amount is overriden</param>
        /// <param name="p_itransAmount">Transaction Amount </param>
        /// <param name="p_iApproverId">Manager Id</param>
        /// <param name="p_stransId">Transaction Id</param>
        private void SaveOverRideAuthority(int p_iClaimId,string  p_sOverRideUserName,double p_itransAmount,int p_iApproverId,string p_stransId,bool bIsGroup)
        {
            string sSQL = string.Empty;
            StringBuilder sbSQL = null;
            int iManagerGroupId = 0;
            int iOverRideGroupId = 0;
            int iOverRideUserId = 0;
              DbConnection objConn = null;
              UserLogin objUserlogin = null;
              DbReader objReader = null;

              DbReader objDbReader = null;
              SysSettings objSettings = null;
              try
              {

                  objUserlogin = new UserLogin(p_iApproverId, m_iClientId);
                  objSettings = new SysSettings(m_sDSN, m_iClientId);
                  iManagerGroupId = objUserlogin.GroupId;

                  sbSQL = new StringBuilder();

                  sbSQL.Append("SELECT MAX(MAX_AMOUNT) MAX_AMT   FROM CL_RES_PAY_LMTS,CLAIM,FUNDS_TRANS_SPLIT");
                  if (objSettings.MultiCovgPerClm != 0)
                  {
                      sbSQL.Append(",RESERVE_CURRENT ");
                  }
                  sbSQL.Append(" WHERE (USER_ID = " + p_iApproverId);

                  if (iOverRideGroupId != 0)
                      sbSQL.Append(" OR GROUP_ID = " + iManagerGroupId);

                  sbSQL.Append(" ) AND CLAIM.CLAIM_ID = " + p_iClaimId);
                  sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = CL_RES_PAY_LMTS.LINE_OF_BUS_CODE");

                  sbSQL.Append(" AND FUNDS_TRANS_SPLIT.TRANS_ID =" + p_stransId);
                  if (objSettings.MultiCovgPerClm != 0)
                  {
                      sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RC_ROW_ID=RESERVE_CURRENT.RC_ROW_ID AND (RESERVE_CURRENT.RESERVE_TYPE_CODE = CL_RES_PAY_LMTS.RES_TYPE_CODE");
                  }
                  else
                  {
                      sbSQL.Append(" AND (FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = CL_RES_PAY_LMTS.RES_TYPE_CODE");
                  }


                  
                  sbSQL.Append("  OR CL_RES_PAY_LMTS.RES_TYPE_CODE = 0)");

                  objReader = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());

                  if (objReader.Read())
                  {
                      if (Conversion.ConvertObjToInt(objReader.GetValue("MAX_AMT"), m_iClientId) >= m_iOverRideAmount)
                      {
                          iOverRideUserId = objUserlogin.GetUserIdAndGroupId(p_sOverRideUserName, m_sDsnName, out iOverRideGroupId);
                          sSQL = "SELECT OVERRIDE_ROW_ID FROM CLAIM_X_OVERRIDE WHERE CLAIM_ID = " + p_iClaimId + " AND ( OVERRIDE_USER = " + iOverRideUserId + " OR OVERRIDE_GROUP = " + iOverRideGroupId + " ) AND OVERRIDE_APPROVER = " + p_iApproverId;

                          objDbReader = DbFactory.GetDbReader(m_sDSN, sSQL);
                          if (objDbReader.Read())
                          {
                              sSQL = " UPDATE CLAIM_X_OVERRIDE SET OVERRIDE_AMOUNT = " + m_iOverRideAmount + " WHERE OVERRIDE_ROW_ID = " + objDbReader.GetValue("OVERRIDE_ROW_ID");
                          }
                          else
                          {
                              if (bIsGroup)
                                  sSQL = "INSERT INTO CLAIM_X_OVERRIDE VALUES(" + Utilities.GetNextUID(m_sDSN, "CLAIM_X_OVERRIDE", m_iClientId) + "," + p_iClaimId + "," + m_iOverRideAmount + ",0," + iOverRideGroupId + "," + p_iApproverId + ",'" + Conversion.GetDate(System.DateTime.Now.Date.ToString()) + Conversion.GetTime(System.DateTime.Now.TimeOfDay.ToString()) + "','" + m_sUserName + "')";
                              else
                                  sSQL = "INSERT INTO CLAIM_X_OVERRIDE VALUES(" + Utilities.GetNextUID(m_sDSN, "CLAIM_X_OVERRIDE", m_iClientId) + "," + p_iClaimId + "," + m_iOverRideAmount + "," + iOverRideUserId + ",0," + p_iApproverId + ",'" + Conversion.GetDate(System.DateTime.Now.Date.ToString()) + Conversion.GetTime(System.DateTime.Now.TimeOfDay.ToString()) + "','" + m_sUserName + "')";
                          }

                          objConn = DbFactory.GetDbConnection(m_sDSN);
                          objConn.Open();

                          objConn.ExecuteNonQuery(sSQL);
                      }
                      else
                      {
                          throw new RMAppException(Globalization.GetString("SupervisorApproval.OverRidePermission.Error", m_iClientId));
                      }

                  }

              }
              catch (RMAppException e)
              {
                  throw e;
              }
              catch (Exception p_objException)
              {
                  throw new RMAppException(Globalization.GetString("SupervisorApproval.OverRideAuthority.Error", m_iClientId), p_objException);
              }
              finally
              {
                  if (objConn != null)
                  {
                      objConn.Close();
                      objConn.Dispose();
                  }
                  if (objReader != null)
                  {
                      objReader.Close();
                      objReader.Dispose();
                  }
                  if (objDbReader != null)
                  {
                      objDbReader.Close();
                      objDbReader.Dispose();
                  }
                  objUserlogin = null;
                  sbSQL = null;
              }



        }

            private bool CheckForOverRideAuthority( string p_stransId,int p_iClaimId,ref bool bIsGroup)
        {
           
            StringBuilder sbSQL = null;
             int iUserId = 0;
            int iGroupId = 0;         
            UserLogin objUserlogin = null;
            DbReader objReader = null;
            SysSettings objSettings = null;
         bool bReturnValue =  false;
            try
            {
                objUserlogin = new UserLogin(m_sUserName, m_sDsnName, m_iClientId);
                sbSQL = new StringBuilder();
                objSettings = new SysSettings(m_sDSN, m_iClientId);
                iUserId = objUserlogin.UserId;
              iGroupId = objUserlogin.GroupId;
                
                bIsGroup = false;

                sbSQL.Append("SELECT HAS_OVERRIDE_AUTHORITY,CL_RES_PAY_LMTS.USER_ID,CL_RES_PAY_LMTS.GROUP_ID  FROM CL_RES_PAY_LMTS,CLAIM,FUNDS_TRANS_SPLIT");
                if (objSettings.MultiCovgPerClm != 0)
                {
                    sbSQL.Append(",RESERVE_CURRENT ");
                }
                sbSQL.Append(" WHERE (USER_ID = " + m_lUserId);

                if (iGroupId != 0)
                    sbSQL.Append(" OR GROUP_ID = " + iGroupId);

                sbSQL.Append(" ) AND CLAIM.CLAIM_ID = " + p_iClaimId);
                sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = CL_RES_PAY_LMTS.LINE_OF_BUS_CODE");

                sbSQL.Append(" AND FUNDS_TRANS_SPLIT.TRANS_ID =" + p_stransId);
                if (objSettings.MultiCovgPerClm != 0)
                {
                    sbSQL.Append(" AND RESERVE_CURRENT.RC_ROW_ID=FUNDS_TRANS_SPLIT.RC_ROW_ID AND (RESERVE_CURRENT.RESERVE_TYPE_CODE = CL_RES_PAY_LMTS.RES_TYPE_CODE");
                }
                else
                {
                    sbSQL.Append(" AND (FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = CL_RES_PAY_LMTS.RES_TYPE_CODE");
                }

                sbSQL.Append("  OR CL_RES_PAY_LMTS.RES_TYPE_CODE = 0)");


                objReader = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());

                while (objReader.Read())
                {
                    if (Conversion.ConvertStrToInteger(objReader.GetValue("GROUP_ID").ToString()) > 0)
                    {
                        bReturnValue = Conversion.ConvertStrToBool(objReader.GetValue("HAS_OVERRIDE_AUTHORITY").ToString());
                        bIsGroup = true;
                        break;
                    }
                     bReturnValue = Conversion.ConvertStrToBool(objReader.GetValue("HAS_OVERRIDE_AUTHORITY").ToString());
                     }                  
                }

     
            catch (Exception p_objException)
            {
                throw p_objException;
                
            }
            finally
            {
              
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objUserlogin = null;
               
                sbSQL = null;
            }
            return bReturnValue;


        }

            //RMA-6404  achouhan3   Added for Angular Grid Implementation Starts
            /// Name		: ListFundsTransactions
            /// Author		: achouhan3 
            /// Date Created: 30/06/2015		
            ///************************************************************
            /// Amendment History
            ///************************************************************
            /// Date Amended   *   Amendment   *    Author
            ///************************************************************
            /// <summary>
            /// Lists funds transactions
            /// </summary>
            /// <param name="p_objOutAllTran">All transactions XML</param>
            /// <param name="p_objOutDenyTran">All Deny only transactions XML</param>
            /// <param name="p_sOutErrorXml">Information returned as xml string</param>
            /// <param name="p_objXmlIn">Create necessary transaction xml</param>
            /// <returns>Success 1 or Failure 0 in execution of the function</returns>
            public int GetFundsTransactions(XmlDocument p_objXmlIn, out XmlDocument p_objOutAllTran, out XmlDocument p_objOutDenyTran, out string p_sOutErrorXml, UserLogin p_objUserLogin
            , string p_sPageName, string p_sGridID, string p_sLangCode = "1033")
            {
                #region variable declaration , //rkotak: making code readable

                string sTmp = string.Empty;
                string sName = string.Empty;
                string sFName = string.Empty;
                string sLName = string.Empty;
                //Start rsushilaggar 06/04/2010 MITS 20938
                string sClaimantFirstName = string.Empty;
                string sClaimantLastName = string.Empty;
                int iEntityApprovalStatus;
                DbReader objLSSInvoiceRead = null;
                string sLSS_INVOICE_ID = string.Empty;
                //End rsushilaggar 06/04/2010 MITS 20938
                string sDitto = string.Empty;
                string sSubmittedTo = string.Empty;//sharishkumar Jira 6417
                long lTransId = 0;
                long lLastTransID = 0;
                double dMaxAmount = 0;
                long lGetUserID = 0;
                long lGetManagerID = 0;
                int iCheckCount = 0;
                string sSupChain = string.Empty;
                // MITS 14836
                double dDetailAmount;
                long lResType;
                long lLastResType = 0;
                long dTotalOfResType = 0;
                bool bDeleteTrans;

                bDeleteTrans = false;
                LobSettings objLobSettings = null;
                ColLobSettings objColLobSettings = null;
                StringBuilder sbSQL = null;
                XmlDocument objDOM = null;
                XmlElement objElemParent = null;
                XmlElement objElemChild = null;
                XmlElement objElemTemp = null;
                StringBuilder sbSQLTemp = null;
                CCacheFunctions objCacheFunctions = null;
                //DbReader objRead = null;
                DataSet objDs = null;
                DataTable objDt=null;
                int iReturnValue = 0;
                DbReader objTempRead = null;
                LocalCache objCache = null;
                bool bTempRead = false;
               // bool bRead = false;
                p_sOutErrorXml = string.Empty;
                p_objOutAllTran = null;
                p_objOutDenyTran = null;
                StringBuilder strJsonPayment = new StringBuilder();
                StringBuilder strJsonCollection = new StringBuilder();
                StringBuilder strJsonDenialCollection = new StringBuilder();
                string strPaymentType = String.Empty;
                StringBuilder strJson = new StringBuilder();
                int i = 0;
                int j = 0;
                string strJsonUserPref = String.Empty;
                StringBuilder strJsonAdditionalData = new StringBuilder();
                int iUserID = 0; int iDSNId = 0;
                string IsShowMyTrans = String.Empty; // RMA-9875    achouhan3   Added for ShowMyTrans funcationality
                List<long> lstUniqueTrans;
                Dictionary<int, double> dictMaxAmount = null;

                bool bShowPayeeOnHold = false;//ajohari2 RMA 6404 and RMA 9875

                //RMA-13782 achouhan3   Addded to implement permission govern column starts
                GridPreference objGridPreferences = null;
                SysSettings objPermissionSettings = null;
                GridColumnDefHelper objGridColDef;
                //RMA-13782 achouhan3   Addded to implement permission govern column starts
                bool isIterate = true;
                #endregion
                try
                {

                    #region Query preparation
                    string[] strGridName = p_sGridID.Split('|');
                    if (p_objUserLogin != null)
                    {
                        iUserID = p_objUserLogin.UserId;
                        iDSNId = p_objUserLogin.DatabaseId;
                        if (m_slangCode == "0")
                            m_slangCode = Convert.ToString(p_objUserLogin.objUser.NlsCode);
                        else
                            m_slangCode = p_sLangCode;
                    }
                    //RMA-9875    achouhan3   Added for ShowMyTrans funcationality
                    if (p_objXmlIn.SelectSingleNode("//ShowMyTrans") != null && !String.IsNullOrEmpty(p_objXmlIn.SelectSingleNode("//ShowMyTrans").InnerText))
                    {
                        IsShowMyTrans = p_objXmlIn.SelectSingleNode("//ShowMyTrans").InnerText.Trim();
                    }

                    //RMA-7810 achouhan3    Added to implement Arch enhancement for payee Statuss
                    if (p_objXmlIn.SelectSingleNode("//ShowPayeeOnHold") != null && !String.IsNullOrEmpty(p_objXmlIn.SelectSingleNode("//ShowPayeeOnHold").InnerText))
                    {
                        if (p_objXmlIn.SelectSingleNode("//ShowPayeeOnHold").InnerText.ToLower() == "true")
                        {
                            bShowPayeeOnHold = true;
                        }
                    }
                    //RMA-7810 achouhan3    Added to implement Arch enhancement for payee Statuss
                    sDitto = "\\\"";
                    objCacheFunctions = new CCacheFunctions(m_sDSN, m_iClientId);
                    objPermissionSettings = new SysSettings(m_sDSN, m_iClientId); //achouhan3   Addded to implement permission govern column starts
                    objCache = new LocalCache(m_sDSN, m_iClientId);//ajohari2 RMA 6404 and RMA 9875
                    sbSQL = new StringBuilder();
                    //START rsushilaggar vendor's funds approva MITS 20606 05-May-2010
                    sbSQL.Append("SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, FUNDS.LAST_NAME, FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT,FUNDS.COMMENTS,");
                    sbSQL.Append("  FUNDS.ADDED_BY_USER, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE");
                       
                      

                    if (objPermissionSettings.MultiCovgPerClm != 0)
                    {
                        sbSQL.Append(" ,RESERVE_CURRENT.RESERVE_TYPE_CODE, ");
                    }
                    else
                    {
                         sbSQL.Append(" ,FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, ");
                    }
                     sbSQL.Append("  FUNDS_TRANS_SPLIT.FROM_DATE, FUNDS_TRANS_SPLIT.TO_DATE,FUNDS.CLAIM_NUMBER,FUNDS.CLAIM_ID");
                    sbSQL.Append(", ENTITY.LAST_NAME | ENTITY_LAST_NAME , ENTITY.FIRST_NAME | ENTITY_FIRST_NAME, ");
                    //JIRA 7810 start  fix RMA-13278
                    sbSQL.Append(" CASE");
                    sbSQL.Append("   WHEN EXISTS (SELECT ENTITY.ENTITY_APPROVAL_STATUS FROM FUNDS_X_PAYEE ,ENTITY WHERE PAYEE_EID=ENTITY_ID");
                    sbSQL.Append(" AND FUNDS_X_PAYEE.FUNDS_TRANS_ID=FUNDS.TRANS_ID AND ENTITY.ENTITY_APPROVAL_STATUS  IN (" + objCache.GetCodeId("R", "ENTITY_APPRV_REJ") + ")) THEN ");
                    sbSQL.Append(objCache.GetCodeId("R", "ENTITY_APPRV_REJ"));
                    sbSQL.Append("   WHEN EXISTS (SELECT ENTITY.ENTITY_APPROVAL_STATUS FROM FUNDS_X_PAYEE ,ENTITY WHERE PAYEE_EID=ENTITY_ID");
                    sbSQL.Append(" AND FUNDS_X_PAYEE.FUNDS_TRANS_ID=FUNDS.TRANS_ID AND ENTITY.ENTITY_APPROVAL_STATUS  IN (" + objCache.GetCodeId("P", "ENTITY_APPRV_REJ") + ",0)) THEN ");
                    sbSQL.Append(objCache.GetCodeId("P", "ENTITY_APPRV_REJ"));

                    sbSQL.Append(" ELSE ");
                    sbSQL.Append(objCache.GetCodeId("A", "ENTITY_APPRV_REJ"));

                    sbSQL.Append(" END");

                    sbSQL.Append("  ENTITY_APPROVAL_STATUS , ");
                    //sbSQL.Append(" ENTITY2.ENTITY_APPROVAL_STATUS ,");
                    //JIRA 7810 end  fix RMA-13278
                    //JIRA 7810 Start Snehal
                    //JIRA 7810 Start Snehal
                    if (m_lDbMake == (long)eDatabaseType.DBMS_IS_SQLSRVR)
                    {
                        sbSQL.Append("  ISNULL(  (SELECT STUFF((SELECT ', ' + CD.SHORT_CODE   FROM HOLD_REASON  INNER JOIN CODES ");
                        sbSQL.Append("   ON HOLD_REASON.HOLD_REASON_CODE = CODE_ID");
                        sbSQL.Append(" INNER JOIN  CODES CD ON CODES.RELATED_CODE_ID = CD.CODE_ID AND CD.TABLE_ID= " + objCache.GetTableId("HOLD_REASON_PARENT"));//RMA-14373 
                        sbSQL.Append(" WHERE ON_HOLD_ROW_ID = HR.ON_HOLD_ROW_ID");
                        sbSQL.Append("  AND HOLD_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                        sbSQL.Append("  AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                        sbSQL.Append("   FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE");
                        sbSQL.Append("  FROM HOLD_REASON  HR WHERE HOLD_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                        sbSQL.Append("  AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                        sbSQL.Append("  AND ON_HOLD_ROW_ID = FUNDS.TRANS_ID GROUP BY ON_HOLD_ROW_ID ) ,'') AS HOLD_REASON_CODE,");

                        sbSQL.Append(" ISNULL(  (SELECT STUFF((SELECT ', ' + CT.CODE_DESC   FROM HOLD_REASON  INNER JOIN CODES  C");
                        sbSQL.Append(" ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID INNER JOIN CODES_TEXT CT");
                        sbSQL.Append(" ON C.CODE_ID = CT.CODE_ID AND C.TABLE_ID= " + objCache.GetTableId("HOLD_REASON_CODE") + " AND CT.LANGUAGE_CODE =" + p_sLangCode + " WHERE ON_HOLD_ROW_ID = HR.ON_HOLD_ROW_ID");//RMA-14373 
                        sbSQL.Append(" AND HOLD_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                        sbSQL.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("FUNDS"));
                        sbSQL.Append(" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE_DESC");
                        sbSQL.Append(" FROM HOLD_REASON  HR WHERE HOLD_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                        sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                        sbSQL.Append(" AND ON_HOLD_ROW_ID = FUNDS.TRANS_ID GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLD_REASON,");

                    }
                    else if (m_lDbMake == (long)eDatabaseType.DBMS_IS_ORACLE)
                    {
                        sbSQL.Append(" nvl( (SELECT LISTAGG(CD.SHORT_CODE, ',') ");
                        sbSQL.Append(" WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID) AS SHORT_CODE");
                        sbSQL.Append(" FROM HOLD_REASON INNER JOIN CODES ON HOLD_REASON.HOLD_REASON_CODE = CODES.CODE_ID");
                        sbSQL.Append(" INNER JOIN  CODES CD ON CODES.RELATED_CODE_ID = CD.CODE_ID AND CD.TABLE_ID= " + objCache.GetTableId("HOLD_REASON_PARENT"));//RMA-14373 
                        sbSQL.Append(" WHERE HOLD_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                        sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                        sbSQL.Append(" AND ON_HOLD_ROW_ID = FUNDS.TRANS_ID GROUP BY ON_HOLD_ROW_ID),0)  AS HOLD_REASON_CODE,");

                        sbSQL.Append(" nvl( (SELECT LISTAGG(CT.CODE_DESC, ',') WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID )");
                        sbSQL.Append(" FROM HOLD_REASON INNER JOIN CODES  C ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID");
                        sbSQL.Append(" INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND C.TABLE_ID= " + objCache.GetTableId("HOLD_REASON_CODE") + " AND CT.LANGUAGE_CODE =" + p_sLangCode + " WHERE HOLD_STATUS_CODE = " + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());//RMA-14373 
                        sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                        sbSQL.Append(" AND ON_HOLD_ROW_ID = FUNDS.TRANS_ID GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLD_REASON,");

                    }
                    //JIRA 7810 End
                    sbSQL.Append("(SELECT LSS_INVOICE_ID FROM FUNDS_SUPP WHERE LSS_INVOICE_ID IS NOT NULL AND LSS_INVOICE_ID <> 0.0 AND FUNDS_SUPP.TRANS_ID=FUNDS.TRANS_ID) | LSS_INVOICE_ID");
                    if (bUseSpecificSupervisor || bUseSupAppPayments)
                    {
                        sbSQL.Append(",FUNDS.APPROVER_ID");
                    }
                    sbSQL.Append(",FUNDS.PAYMENT_FLAG, FUNDS.COLLECTION_FLAG");
                    sbSQL.Append(" FROM FUNDS ");
                    sbSQL.Append("INNER JOIN  FUNDS_TRANS_SPLIT  ON FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ");
                    if (objPermissionSettings.MultiCovgPerClm != 0)
                    {
                        sbSQL.Append("INNER JOIN  RESERVE_CURRENT  ON FUNDS_TRANS_SPLIT.RC_ROW_ID = RESERVE_CURRENT.RC_ROW_ID ");
                    }
                    sbSQL.Append("LEFT OUTER JOIN  CLAIMANT  ON FUNDS.CLAIM_ID=CLAIMANT.CLAIM_ID AND CLAIMANT.PRIMARY_CLMNT_FLAG=-1  ");
                    sbSQL.Append("LEFT OUTER JOIN ENTITY ON CLAIMANT.CLAIMANT_EID=ENTITY.ENTITY_ID ");
                    sbSQL.Append("INNER JOIN ENTITY ENTITY2 ON ENTITY2.ENTITY_ID = FUNDS.PAYEE_EID  ");
                    //RMA-13730 Modified condition to get record in case of my pending Transaction Strtx
                    if (bUseQueuedPayments || (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))
                    {
                        if (bUseQueuedPayments)
                        {
                            sbSQL.Append(" WHERE ((FUNDS.STATUS_CODE =" + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString() + " AND FUNDS.VOID_FLAG = 0)");
                            sbSQL.Append(" OR (FUNDS.STATUS_CODE =" + objCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS").ToString() + " AND FUNDS.VOID_FLAG <> 0))");
                        }
                        else
                        {
                            sbSQL.Append(" WHERE ((FUNDS.STATUS_CODE =" + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString() + " AND FUNDS.VOID_FLAG = 0))");
                        }
                    }
                    //RMA-13730 Modified condition to get record in case of my pending Transaction Strtx
                    else
                    {
                        sbSQL.Append(" WHERE FUNDS.STATUS_CODE =" + objCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS").ToString());
                        sbSQL.Append(" AND FUNDS.VOID_FLAG = 0");
                    }

                    //Start: rsushilaggar Collections from LSS 06/02/2010 MITS - 20938
                    if (!PutLSScollectionOnHold)
                    {
                        sbSQL.Append(" AND FUNDS.PAYMENT_FLAG <> 0 ");
                    }
                    //End: rsushilaggar
                    sbSQLTemp = new StringBuilder();
                    //RMA-9875    achouhan3   Modified for ShowMyTrans funcationality Starts
                    if (bUseQueuedPayments || (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))
                    {
                        sbSQLTemp.Append(sbSQL.ToString() + " AND FUNDS.ADDED_BY_USER = " + Utilities.FormatSqlFieldValue(m_sUserName));
                        sbSQL.Append(" AND FUNDS.ADDED_BY_USER <> " + Utilities.FormatSqlFieldValue(m_sUserName));
                    }
                    else if (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true")
                    {
                        sbSQL.Append(" AND FUNDS.ADDED_BY_USER = " + Utilities.FormatSqlFieldValue(m_sUserName));
                    }
                    else
                        sbSQL.Append(" AND FUNDS.ADDED_BY_USER <> " + Utilities.FormatSqlFieldValue(m_sUserName));
                    //RMA-9875    achouhan3   Modified for ShowMyTrans funcationality Ends
                    //MITS 24464 mcapps2 If we are not using the superisory chaing we don't need to get the user ids 
                    if (!bUseSupAppPayments)
                    {
                        sbSQL.Append(" AND FUNDS.TRANS_ID <> 0");
                    }
                    else
                    {
                        //rsushilaggar MITS 26332 Date 11/29/2011
                        if (p_objXmlIn.SelectSingleNode("//ShowAllItems") != null && !String.IsNullOrEmpty(p_objXmlIn.SelectSingleNode("//ShowAllItems").InnerText))  //averma62 - MITS #31483 - Null check was missing for this node. 
                        {
                            if (p_objXmlIn.SelectSingleNode("//ShowAllItems").InnerText == "True")
                            {
                                sSupChain = ShowItemsToAllUser(m_lUserId);
                            }
                            else
                            {
                                sSupChain = DownSupChain(m_lUserId, p_objXmlIn.SelectSingleNode("//ShowAllItems").InnerText);
                            }
                        }
                        //sSupChain = DownSupChain(m_lUserId);

                        //Start: rsushilaggar 06/03/2010 MITS 20938
                        if (string.IsNullOrEmpty(sSupChain))
                        {
                            sSupChain = GetCollectionTransId(m_sUserName);
                        }
                        else
                        {
                            string list = GetCollectionTransId(m_sUserName);
                            sSupChain = sSupChain + (!string.IsNullOrEmpty(list) ? "," + list : "");
                        }

                        //end: rsushilaggar
                        if (!sSupChain.Trim().Equals("")) ////MITS 24464 mcapps2 we have ids 
                        {
                            sbSQLTemp.Append(" AND FUNDS.TRANS_ID <> 0");
                            sbSQL.Append(" AND FUNDS.TRANS_ID IN (" + sSupChain + ")");
                        }
                        if (sSupChain.Trim().Equals("")) //MITS 24464 mcapps2 Supervisory Approval is turned on and we don't have any trans ids, but we cannot display everything that is on hold so we will limit the approval to the user
                        {
                            sbSQL.Append(" AND FUNDS.APPROVER_ID = " + m_lUserId);
                        }
                       
                    }
                    if (!string.IsNullOrEmpty(sFilterSql))
                        sbSQL.Append(sFilterSql);
                    if (!string.IsNullOrEmpty(sOrderBySql))
                        sbSQL.Append(" ORDER BY " + sOrderBySql);
                    else
                        sbSQL.Append(" ORDER BY FUNDS.CTL_NUMBER,FUNDS.TRANS_ID");

                    //Manish Multicurrency
                    int transid = Conversion.ConvertStrToInteger(sSupChain);
                    string culture = CommonFunctions.GetCulture(transid, CommonFunctions.NavFormType.Funds, m_sDSN);
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
                    if (bUseQueuedPayments || (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))
                    {

                        if (!string.IsNullOrEmpty(sFilterSql))
                            sbSQLTemp.Append(sFilterSql);

                        if (!string.IsNullOrEmpty(sOrderBySql))
                            sbSQLTemp.Append(" ORDER BY " + sOrderBySql);
                        else
                            sbSQLTemp.Append(" ORDER BY FUNDS.CTL_NUMBER,FUNDS.TRANS_ID");
                    }

                    if (m_lDbMake == (long)eDatabaseType.DBMS_IS_ACCESS)
                    {
                        sbSQL.Replace("|", " AS ");
                        sbSQLTemp.Replace("|", " AS ");
                    }
                    else
                    {
                        sbSQL.Replace("|", " ");
                        sbSQLTemp.Replace("|", " ");
                    }
                    #endregion
                    //objRead = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                    objDs = DbFactory.GetDataSet(m_sDSN, sbSQL.ToString(),m_iClientId);
                    //bRead = objRead.Read();
                    if (bUseQueuedPayments || (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))
                    {
                        objTempRead = DbFactory.GetDbReader(m_sDSN, sbSQLTemp.ToString());
                        bTempRead = objTempRead.Read();
                    }
                    //The userID and UserManagersId is obtained
                    if (bUseSpecificSupervisor)
                    {
                        lGetManagerID = 0;
                    }
                    else
                    {
                        lGetManagerID = m_lManagerId;
                    }
                    lGetUserID = m_lUserId;
                    iCheckCount = 0;
                    objDOM = new XmlDocument();
                    objElemParent = objDOM.CreateElement("FundsTransactions");
                    //zmohammad MITs 33267 : Pay detail fix
                    objColLobSettings = new ColLobSettings(m_sDSN, m_iClientId);
                    objDOM.AppendChild(objElemParent);
                    objDt=objDs.Tables[0];
                    if (!m_bPmtNotfyImmSup)
                    {
                        bDeleteTrans = true;
                        if (objDt.Rows.Count > 0)
                        {
                            lLastTransID = Convert.ToInt64(objDt.Rows[objDt.Rows.Count - 1]["TRANS_ID"]);
                            DataRow[] rowLastTransId = objDt.Select("TRANS_ID='" + lLastTransID + "'");
                            foreach (DataRow row in rowLastTransId)
                            {
                                objDt.Rows.Remove(row);
                            }
                        }

                    } // if
                    foreach(DataRow dr in objDt.Rows)
                    {
                        //Check to remove duplicate row because of splits
                        //RMA-7810 achouhan3    Added to implement Arch enhancement for payee Statuss
                        if (!bShowPayeeOnHold)
                        {
                            //RMA-14872     achouhan3       Modified Contains to string compare
                            if ((!string.IsNullOrEmpty(Common.Conversion.ConvertObjToStr(dr["HOLD_REASON_CODE"])))
                                  && ((Common.Conversion.ConvertObjToStr(dr["HOLD_REASON_CODE"]).Trim().ToUpper() == "PAYEE_NOT_APPRV") ||
                                       (Common.Conversion.ConvertObjToStr(dr["HOLD_REASON_CODE"]).Trim().ToUpper() =="PAYEE_REJECT"))
									   && !(Conversion.ConvertObjToInt(dr["ENTITY_APPROVAL_STATUS"],m_iClientId).Equals(objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))))//sachin Jira 14871/14872
                            //RMA-14872     achouhan3       Modified Contains to string compare
                            {
                                isIterate = false;
                            }
                            else
                                isIterate = true;
                        }
                        else
                            isIterate = true;
                        //RMA-7810 achouhan3    Added to implement Arch enhancement for payee Status
                        if (isIterate)
                        {
                            strJson.Remove(0, strJson.Length);
                            if (Conversion.ConvertObjToBool(dr["PAYMENT_FLAG"], m_iClientId))
                            {
                                strPaymentType = "P";
                            }
                            else
                            {
                                strPaymentType = "C";
                            }

                            //zmohammad MITs 33267 : Pay detail fix
                            objLobSettings = objColLobSettings[(int)GetLOBForClaim(Common.Conversion.ConvertObjToInt64(dr["CLAIM_ID"], m_iClientId))];
                            dDetailAmount = -1;
                            //If there is a manager set then we get the max_amount from the database
                            //If a manager is not set we set the varable 'dMaxAmount to -1
                            if (lGetManagerID != 0)
                            {
                                //zmohammad MITs 33267 : Pay detail fix
                                if (objLobSettings != null)//asingh263 mits 35854
                                {
                                    if (objLobSettings.PayLmtFlag)
                                    {
                                        //dMaxAmount = GetMaxAmount(m_lUserId, (int)GetLOBForClaim(
                                        //    Common.Conversion.ConvertObjToInt64(dr["CLAIM_ID"), m_iClientId)));
                                        //created to optimize perfromance
                                        dMaxAmount = GetMaxAmountByUser(m_lUserId, (int)GetLOBForClaim(
                                           Common.Conversion.ConvertObjToInt64(dr["CLAIM_ID"], m_iClientId)), ref dictMaxAmount);
                                    }
                                    else
                                    {
                                        dMaxAmount = -1;
                                    }
                                    if (objLobSettings.PayDetLmtFlag)
                                    {
                                        GetMaxPayDetailAmount((int)GetLOBForClaim(Common.Conversion.ConvertObjToInt64(dr["CLAIM_ID"], m_iClientId)),
                                            Conversion.ConvertStrToInteger(dr["RESERVE_TYPE_CODE"].ToString()), ref dDetailAmount);
                                    } // if
                                }//asingh263 mits 35854	
                            }
                            else if (bUseSpecificSupervisor)
                            {
                                //zmohammad MITs 33267 : Pay detail fix
                                if (objLobSettings != null)//asingh263 mits 35854
                                {
                                    if (objLobSettings.PayLmtFlag)
                                    {
                                        //dMaxAmount = GetMaxAmount(m_lUserId, (int)GetLOBForClaim(
                                        //    Common.Conversion.ConvertObjToInt64(dr["CLAIM_ID"), m_iClientId)));
                                        dMaxAmount = GetMaxAmountByUser(m_lUserId, (int)GetLOBForClaim(
                                          Common.Conversion.ConvertObjToInt64(dr["CLAIM_ID"], m_iClientId)), ref dictMaxAmount);
                                    }
                                    else
                                    {
                                        dMaxAmount = -1;
                                    }
                                    if (objLobSettings.PayDetLmtFlag)
                                    {
                                        GetMaxPayDetailAmount((int)GetLOBForClaim(Common.Conversion.ConvertObjToInt64(dr["CLAIM_ID"], m_iClientId)),
                                            Conversion.ConvertStrToInteger(dr["RESERVE_TYPE_CODE"].ToString()), ref dDetailAmount);
                                    } // if
                                }//asingh263 mits 35854
                            }
                            else
                            {
                                dMaxAmount = -1;
                            }

                            if (objLobSettings != null)//asingh263 mits 35854
                            {
                                if (bPlaceSupHoldPayDetailLimits)
                                {
                                    if (objLobSettings.PayDetLmtFlag)
                                    {
                                        GetMaxPayDetailAmount((int)GetLOBForClaim(Common.Conversion.ConvertObjToInt64(dr["CLAIM_ID"], m_iClientId)),
                                            Conversion.ConvertStrToInteger(dr["RESERVE_TYPE_CODE"].ToString()), ref dDetailAmount);
                                    } // if
                                }//asingh263 mits 35854
                            } // if

                           
                            if ((dMaxAmount >= Conversion.ConvertObjToDouble(dr["FAMT"], m_iClientId) && dMaxAmount >= Conversion.ConvertObjToDouble(dr["FAMT"], m_iClientId))

                                || (dMaxAmount == -1) || m_bPmtNotfyImmSup) //smahajan6: Safeway: Payment Supervisory Approval - for Notify Immediate Supervisor
                            {
                                //end 7810

                                lTransId = Conversion.ConvertObjToInt64(dr["TRANS_ID"], m_iClientId);
                                lResType = Conversion.ConvertStrToLong(dr["RESERVE_TYPE_CODE"].ToString());
                                if (lResType != lLastResType)
                                {
                                    dTotalOfResType = 0;
                                } // if

                                //if (lTransId != lLastTransID)
                                //{
                                    dTotalOfResType = 0;
                                    bDeleteTrans = false;
                                    lLastResType = lResType;
                                    //skip duplicates (caused by inner join w/ splits table)
                                    objCache = new LocalCache(m_sDSN, m_iClientId);
                                    //objElemChild=objDOM.CreateElement("Transaction");
                                    //Added to know the queued payments
                                    if ((i > 0 && strPaymentType == "P") || (j > 0 && strPaymentType == "C"))
                                    {
                                        if (String.IsNullOrEmpty(strPaymentType) || strPaymentType == "P")
                                        {
                                            strJsonPayment.Append(",");
                                            i = i + 1;
                                        }
                                        else
                                        {
                                            strJsonCollection.Append(",");
                                            j = j + 1;
                                        }
                                    }
                                    else if ((i == 0 && strPaymentType == "P") || (j == 0 && strPaymentType == "C"))
                                    {
                                        if (String.IsNullOrEmpty(strPaymentType) || strPaymentType == "P")
                                        {
                                            strJsonPayment.Append("[");
                                            i = i + 1;
                                        }
                                        else
                                        {
                                            strJsonCollection.Append("[");
                                            j = j + 1;
                                        }
                                    }
                                    if (String.IsNullOrEmpty(strPaymentType) || strPaymentType == "P")
                                        strJsonPayment.Append("{ ");
                                    else
                                        strJsonCollection.Append("{ ");
                                    if (bUseQueuedPayments)
                                    {
                                        strJson.Append(Conversion.GetJsonNode("UtilitySetting", "Q", true));
                                    }
                                    else
                                    {
                                        strJson.Append(Conversion.GetJsonNode("UtilitySetting", "H", true));
                                    }

                                    iEntityApprovalStatus = Common.Conversion.ConvertObjToInt(dr["ENTITY_APPROVAL_STATUS"], m_iClientId);
                                    if (iEntityApprovalStatus != (int)objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))
                                    {
                                        //objElemChild.SetAttribute("EntityApprovalStatus", "NOT-APPROVED");
                                        strJson.Append(Conversion.GetJsonNode("EntityApprovalStatus", "NOT-APPROVED"));
                                    }
                                    else
                                        strJson.Append(Conversion.GetJsonNode("EntityApprovalStatus", "APPROVED"));
                                    //objElemChild.SetAttribute("EntityApprovalStatus", "APPROVED");
                                    lLastTransID = lTransId;
                                    strJson.Append(Conversion.GetJsonNode("CtlNumber", Common.Conversion.ConvertObjToStr(dr["CTL_NUMBER"])));

                                    strJson.Append(Conversion.GetJsonNode("TransDate", Conversion.GetUIDate(
                                        Conversion.ConvertObjToStr(dr["TRANS_DATE"]), m_slangCode, m_iClientId)));
                                    strJson.Append(Conversion.GetJsonNode("TransDate.DbValue", GridCommonFunctions.GetSortColumnValueForDateField(Convert.ToString(dr["TRANS_DATE"]))));//CUSTOM SORTING FEILD
                                   

                                    //JIRA 7810 start  fix RMA-13278
                                    string sPayeesName = string.Empty;

                                    int iNoOfPayees = 0;
                                    string sEntSQL = "SELECT ISNULL(ENTITY.FIRST_NAME,'') + ' ' + ENTITY.LAST_NAME AS PAYEES_NAME from ENTITY INNER JOIN FUNDS_X_PAYEE ON  ENTITY.ENTITY_ID =FUNDS_X_PAYEE.PAYEE_EID WHERE FUNDS_X_PAYEE.FUNDS_TRANS_ID=" + lTransId;
                                    if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                                    {
                                        sEntSQL = sEntSQL.Replace("ISNULL", "nvl");
                                        sEntSQL = sEntSQL.Replace("+", "||");

                                    }
                                    sName = string.Empty;
                                    // akaushik5 Added for MITS 37343 Ends
                                    using (DbReader objeReader = DbFactory.GetDbReader(DbFactory.GetDbConnection(m_sDSN).ConnectionString, sEntSQL))
                                    {
                                        while (objeReader.Read())
                                        {
                                            if (sPayeesName == string.Empty)
                                            {
                                                sPayeesName = objeReader.GetString("PAYEES_NAME");
                                            }
                                            else
                                            {
                                                sPayeesName = sPayeesName + "<br> " + objeReader.GetString("PAYEES_NAME");
                                            }

                                            // akaushik5 Added for MITS 37343 Starts
                                            sName = string.IsNullOrEmpty(sName) ? objeReader.GetString("PAYEES_NAME") : string.Format("{0} and {1}", sName, objeReader.GetString("PAYEES_NAME"));
                                            // akaushik5 Added for MITS 37343 Ends
                                            iNoOfPayees++;

                                        }
                                    }
                                    sClaimantLastName = Common.Conversion.ConvertObjToStr(dr["ENTITY_LAST_NAME"]);
                                    sClaimantFirstName = Common.Conversion.ConvertObjToStr(dr["ENTITY_FIRST_NAME"]);

                                    strJson.Append(Conversion.GetJsonNode("PayeeName", sName));
                                    strJson.Append(Conversion.GetJsonNode("PrimaryClaimantName", !string.IsNullOrEmpty(sClaimantFirstName) ? sClaimantFirstName + " " + sClaimantLastName : sClaimantLastName));

                                    sLSS_INVOICE_ID = Common.Conversion.ConvertObjToStr(dr["LSS_INVOICE_ID"]);
                                    if (!sLSS_INVOICE_ID.Trim().Equals("0"))
                                    {
                                        strJson.Append(Conversion.GetJsonNode("LSSInvoiceLink", "home?pg=riskmaster/Custom/Signon/SingleSignon&paramLSSInvoiceID=" + sLSS_INVOICE_ID));
                                        strJson.Append(Conversion.GetJsonNode("RMLSSInvoiceID", sLSS_INVOICE_ID));
                                    }
                                    if (!sLSS_INVOICE_ID.Trim().Equals("0"))
                                    {
                                        strJson.Append(Conversion.GetJsonNode("LSSID", sLSS_INVOICE_ID));
                                    }
                                    else
                                    {
                                        strJson.Append(Conversion.GetJsonNode("LSSID", "-"));
                                    }

                                    strJson.Append(Conversion.GetJsonNode("ClaimNumber", Common.Conversion.ConvertObjToStr(dr["CLAIM_NUMBER"])));
                                    strJson.Append(Conversion.GetJsonNode("PaymentAmount", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sDSN), Conversion.ConvertObjToDouble(dr["FAMT"], m_iClientId), m_sDSN, m_iClientId)));
                                    strJson.Append(Conversion.GetJsonNode("PaymentAmount.DbValue", Conversion.ConvertObjToDouble(dr["FAMT"], m_iClientId).ToString()));//CUSTOM SORTING FEILD

                                    //Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS - 20606
                                    strJson.Append(Conversion.GetJsonNode("PaymentAmountNumber", Conversion.ConvertObjToStr(dr["FAMT"])));
                                    strJson.Append(Conversion.GetJsonNode("PaymentAmountNumber.DbValue", Conversion.ConvertObjToDouble(dr["FAMT"], m_iClientId).ToString()));//CUSTOM SORTING FEILD
                                    //End: rsushilaggar 05-May-2010 Vendor's Funds approval


                                    strJson.Append(Conversion.GetJsonNode("User", Common.Conversion.ConvertObjToStr(dr["ADDED_BY_USER"])));

                                    strJson.Append(Conversion.GetJsonNode("TransactionType", objCache.GetCodeDesc(
                                        Common.Conversion.ConvertStrToInteger(
                                        Common.Conversion.ConvertObjToStr(
                                        dr["TRANS_TYPE_CODE"])))));

                                    dTotalOfResType = dTotalOfResType + Conversion.ConvertStrToLong(dr["FTSAMT"].ToString());
                                    strJson.Append(Conversion.GetJsonNode("SplitAmount", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sDSN), Conversion.ConvertObjToDouble(dr["FTSAMT"], m_iClientId), m_sDSN, m_iClientId)));
                                    strJson.Append(Conversion.GetJsonNode("SplitAmount.DbValue", Conversion.ConvertObjToDouble(dr["FTSAMT"], m_iClientId).ToString()));//CUSTOM SORTING FEILD
                                    //Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
                                    strJson.Append(Conversion.GetJsonNode("SplitAmountNumber", Conversion.ConvertObjToDouble(dr["FTSAMT"], m_iClientId).ToString()));
                                    strJson.Append(Conversion.GetJsonNode("SplitAmountNumber.DbValue", Conversion.ConvertObjToDouble(dr["FTSAMT"], m_iClientId).ToString()));//CUSTOM SORTING FEILD
                                    //End: rsushilaggar 05-May-2010 Vendor's Funds approval
                                    sTmp = Conversion.GetUIDate(
                                        Conversion.ConvertObjToStr(dr["FROM_DATE"]), m_slangCode, m_iClientId);//vkumar258 ML Changes
                                    sTmp = sTmp + " - " + Conversion.GetUIDate(
                                        Conversion.ConvertObjToStr(dr["TO_DATE"]), m_slangCode, m_iClientId);//vkumar258 ML Changes
                                    strJson.Append(Conversion.GetJsonNode("FromToDate", sTmp));
                                    strJson.Append(Conversion.GetJsonNode("FromToDate.DbValue", GridCommonFunctions.GetSortColumnValueForDateField(Convert.ToString(dr["FROM_DATE"]))));//CUSTOM SORTING FEILD
                                    //Added by Shameem for Void Reason-02/02/2009
                                    strJson.Append(Conversion.GetJsonNode("VoidReason", String.Empty));
                                    //End Shameem
                                    strJson.Append(Conversion.GetJsonNode("TransId", Common.Conversion.ConvertObjToStr(dr["TRANS_ID"])));
                                    //Start: rsushilaggar Collections from LSS 06/02/2010 MITS 20938
                                    if (Conversion.ConvertObjToBool(dr["PAYMENT_FLAG"], m_iClientId))
                                    {
                                        strJson.Append(Conversion.GetJsonNode("PaymentType", "P"));
                                        //objElemChild.SetAttribute("PaymentType","P");
                                    }
                                    else
                                    {
                                        strJson.Append(Conversion.GetJsonNode("PaymentType", "C"));
                                        //objElemChild.SetAttribute("PaymentType", "C");
                                    }
                                    //End: rsushilaggar
                                    //Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
                                    iEntityApprovalStatus = Common.Conversion.ConvertObjToInt(dr["ENTITY_APPROVAL_STATUS"], m_iClientId);
                                    //rsushilaggar 10-Aug-2010 MITS-21715
                                    //if (iEntityApprovalStatus != (int)objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))
                                    //    objElemTemp.InnerText = "Pending";
                                    //else
                                    //    objElemTemp.InnerText = string.Empty;
                                    if (iEntityApprovalStatus == 0)
                                    {
                                        iEntityApprovalStatus = (int)objCacheFunctions.GetCodeIDWithShort("P", "ENTITY_APPRV_REJ");
                                    }
                                    strJson.Append(Conversion.GetJsonNode("PayeeStatus", objCache.GetCodeDesc(iEntityApprovalStatus)));
                                    //End: rsushilaggar 05-May-2010 Vendor's Funds approval
                                    if ((iEntityApprovalStatus.Equals(objCache.GetCodeId("A", "ENTITY_APPRV_REJ")))
                                  && (!string.IsNullOrEmpty(Common.Conversion.ConvertObjToStr(dr["HOLD_REASON_CODE"])))
                                  && ((Common.Conversion.ConvertObjToStr(dr["HOLD_REASON_CODE"]).Contains("PAYEE_NOT_APPRV")) ||
                                       (Common.Conversion.ConvertObjToStr(dr["HOLD_REASON_CODE"]).Contains("PAYEE_REJECT"))))
                                    {
                                        /*if (this.ApprvFundsIfPayeeNotApprvdPermission)
                                        {
                                            objElemTemp = objDOM.CreateElement("HOLD_REASON_CODE");
                                            objElemTemp.InnerText = string.Empty;
                                            objElemChild.AppendChild(objElemTemp);
                                        }
                                        else
                                        {
                                         */
                                        strJson.Append(Conversion.GetJsonNode("HOLD_REASON_CODE", Common.Conversion.ConvertObjToStr(dr["HOLD_REASON_CODE"])));
                                        //}
                                        if (Common.Conversion.ConvertObjToStr(dr["HOLD_REASON_CODE"]).Trim().Equals("PAYEE_NOT_APPRV")
                                            || Common.Conversion.ConvertObjToStr(dr["HOLD_REASON_CODE"]).Trim().Equals("PAYEE_REJECT"))
                                        {
                                            strJson.Append(Conversion.GetJsonNode("HOLD_REASON", objCache.GetCodeDesc(objCache.GetCodeId("PAYE_APRV_PYM_UNAPRV", "HOLD_REASON_PARENT"))));
                                        }
                                        else
                                        {
                                            strJson.Append(Conversion.GetJsonNode("HOLD_REASON", (Common.Conversion.ConvertObjToStr(dr["HOLD_REASON"]).Replace(objCache.GetCodeDesc(objCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), "")).Trim().TrimEnd(',').TrimStart(',').Replace(", ,", ",")));
                                        }
                                    }
                                    else
                                    {
                                        strJson.Append(Conversion.GetJsonNode("HOLD_REASON_CODE", Common.Conversion.ConvertObjToStr(dr["HOLD_REASON_CODE"])));
                                        strJson.Append(Conversion.GetJsonNode("HOLD_REASON", Common.Conversion.ConvertObjToStr(dr["HOLD_REASON"])));

                                    }

                                    //JIRA 7810 Ends
                                    //Added by csingh7 for MITS 20092 :Start
                                    objElemTemp = objDOM.CreateElement("Comments");
                                    if (Common.Conversion.ConvertObjToStr(dr["COMMENTS"]) != null && Common.Conversion.ConvertObjToStr(dr["COMMENTS"]) != "")
                                        strJson.Append(Conversion.GetJsonNode("Comments", "Comments"));
                                    else
                                        strJson.Append(Conversion.GetJsonNode("Comments", String.Empty));
                                    //Added by csingh7 for MITS 20092 :End

                                    //sharishkumar Jira 6417 starts
                                    if (bUseSpecificSupervisor || bUseSupAppPayments)
                                    {
                                        sSubmittedTo = objCache.GetSystemLoginName(Common.Conversion.ConvertObjToInt(dr["APPROVER_ID"], m_iClientId), m_iDsnId, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), m_iClientId);
                                        strJson.Append(Conversion.GetJsonNode("SubmittedTo", sSubmittedTo));
                                    }
                                    //sharishkumar Jira 6417 ends
                                    strJson.Append(" }");
                                    if (strPaymentType == "P")
                                        strJsonPayment.Append(strJson.ToString());
                                    else
                                        strJsonCollection.Append(strJson.ToString());
                                    objCache.Dispose();
                                    objCache = null;
                                    iCheckCount = iCheckCount + 1;
                            }                            
                        }                       
                    }

                    if (i != 0)//if no records found
                        strJsonPayment.Append("]");
                    else //empty aray
                        strJsonPayment.Append("[]");
                    objElemChild = objDOM.CreateElement("PaymentsTransaction");
                    objDOM.FirstChild.AppendChild(objElemChild);

                    objElemTemp = objDOM.CreateElement("Data");
                    objElemTemp.InnerText = strJsonPayment.ToString();
                    objElemChild.AppendChild(objElemTemp);

                    #region Prepare Json for UserPref and add to XMl doc for PaymentsTransaction
                    strJsonUserPref = "";
                    strJsonUserPref = GetOrCreateUserPreference(iUserID, p_sPageName, iDSNId, strGridName[0].ToString());

                    //RMA-13782 achouhan3   Addded to implement permission govern column starts
                    objGridPreferences = Riskmaster.Common.GridCommonFunctions.GetListforUserPreference(strJsonUserPref);
                    if (!objPermissionSettings.UseEntityApproval && objGridPreferences != null && objGridPreferences.colDef != null)
                    {
                        objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "PAYEESTATUS").FirstOrDefault();
                        if (objGridColDef != null)
                        {
                            objGridColDef.visible = false;
                            objGridColDef.alwaysInvisibleOnColumnMenu = true;
                        }
                    }
                    else
                    {
                        objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "PAYEESTATUS").FirstOrDefault();
                        if (objGridColDef != null)
                        {
                            objGridColDef.alwaysInvisibleOnColumnMenu = false;
                        }
                    }
                    if (!objPermissionSettings.ShowLSSInvoice)
                    {
                        objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "LSSID").FirstOrDefault();
                        if (objGridColDef != null)
                        {
                            objGridColDef.visible = false;
                            objGridColDef.alwaysInvisibleOnColumnMenu = true;
                        }
                    }
                    else
                    {
                        objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "LSSID").FirstOrDefault();
                        if (objGridColDef != null)
                        {
                            objGridColDef.alwaysInvisibleOnColumnMenu = false;
                        }
                    }
                    strJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(objGridPreferences);
                    //RMA-13782 achouhan3   Addded to implement permission govern column Ends
                    XmlElement objUserPrefXmlElement = objDOM.CreateElement("UserPref");
                    objUserPrefXmlElement.InnerText = strJsonUserPref;
                    objElemChild.AppendChild(objUserPrefXmlElement);
                    #endregion

                    #region Prepare Json string for additional data and add to XMl doc
                    strJsonAdditionalData.Remove(0, strJsonAdditionalData.Length);
                    XmlElement objAdditionalDataXmlElement = objDOM.CreateElement("AdditionalData");
                    strJsonAdditionalData = new StringBuilder();
                    //dicGridData = new Dictionary<string, string>();
                    // lstDictData.Clear();
                    strJsonAdditionalData.Append("[{");
                    strJsonAdditionalData.Append(Conversion.GetJsonNode("TotalCount", i.ToString(), true));
                    //lstDictData.Add(dicGridData);
                    //strJsonResponse = Conversion.CreateJSONStringFromDictionary(lstDictData);
                    strJsonAdditionalData.Append("}]");
                    objAdditionalDataXmlElement.InnerText = strJsonAdditionalData.ToString();
                    objElemChild.AppendChild(objAdditionalDataXmlElement);
                    #endregion

                    if (j != 0)//if no records found
                        strJsonCollection.Append("]");
                    else //empty aray
                        strJsonCollection.Append("[]");
                    objElemChild = objDOM.CreateElement("CollectionTransaction");
                    objDOM.FirstChild.AppendChild(objElemChild);

                    objElemTemp = objDOM.CreateElement("Data");
                    objElemTemp.InnerText = strJsonCollection.ToString();
                    objElemChild.AppendChild(objElemTemp);


                    #region Prepare Json for UserPref and add to XMl doc for CollectionTransaction
                    strJsonUserPref = "";
                    objAdditionalDataXmlElement = null;
                    strJsonUserPref = GetOrCreateUserPreference(iUserID, p_sPageName, iDSNId, strGridName[1].ToString());
                    //RMA-13782 achouhan3   Addded to implement permission govern column starts
                    objGridPreferences = Riskmaster.Common.GridCommonFunctions.GetListforUserPreference(strJsonUserPref);
                    if (!objPermissionSettings.UseEntityApproval && objGridPreferences != null && objGridPreferences.colDef != null)
                    {
                        objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "PAYEESTATUS").FirstOrDefault();
                        if (objGridColDef != null)
                        {
                            objGridColDef.visible = false;
                            objGridColDef.alwaysInvisibleOnColumnMenu = true;
                        }
                    }
                    else
                    {
                        objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "PAYEESTATUS").FirstOrDefault();
                        if (objGridColDef != null)
                        {
                            objGridColDef.alwaysInvisibleOnColumnMenu = false;
                        }
                    }
                    if (!objPermissionSettings.ShowLSSInvoice)
                    {
                        objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "LSSID").FirstOrDefault();
                        if (objGridColDef != null)
                        {
                            objGridColDef.visible = false;
                            objGridColDef.alwaysInvisibleOnColumnMenu = true;
                        }
                    }
                    else
                    {
                        objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "LSSID").FirstOrDefault();
                        if (objGridColDef != null)
                        {
                            objGridColDef.alwaysInvisibleOnColumnMenu = false;
                        }
                    }
                    strJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(objGridPreferences);
                    //RMA-13782 achouhan3   Addded to implement permission govern column starts
                    objUserPrefXmlElement = objDOM.CreateElement("UserPref");
                    objUserPrefXmlElement.InnerText = strJsonUserPref;
                    objElemChild.AppendChild(objUserPrefXmlElement);
                    #endregion

                    #region Prepare Json string for additional data and add to XMl doc
                    strJsonAdditionalData.Remove(0, strJsonAdditionalData.Length);
                    objAdditionalDataXmlElement = null;
                    objAdditionalDataXmlElement = objDOM.CreateElement("AdditionalData");
                    strJsonAdditionalData = new StringBuilder();
                    //dicGridData = new Dictionary<string, string>();
                    // lstDictData.Clear();
                    strJsonAdditionalData.Append("[{");
                    strJsonAdditionalData.Append(Conversion.GetJsonNode("TotalCount", j.ToString(), true));
                    //lstDictData.Add(dicGridData);
                    //strJsonResponse = Conversion.CreateJSONStringFromDictionary(lstDictData);
                    strJsonAdditionalData.Append("}]");
                    objAdditionalDataXmlElement.InnerText = strJsonAdditionalData.ToString();
                    objElemChild.AppendChild(objAdditionalDataXmlElement);
                    #endregion
                    p_objOutAllTran = objDOM;
                    i = 0; //reassign Variable
                    isIterate = false;
                    if ((!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))
                        bShowPayeeOnHold = true;
                    if (bUseQueuedPayments || (!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))
                    {
                        objDOM = new XmlDocument();
                        strJson.Remove(0, strJson.Length);
                        objElemParent = objDOM.CreateElement("FundsTransactions");
                        objDOM.AppendChild(objElemParent);
                        while (bTempRead)
                        {

                            lTransId = Conversion.ConvertObjToInt64(objTempRead.GetValue("TRANS_ID"), m_iClientId);
                            
                            if (!bShowPayeeOnHold)
                            {
                                if ((!string.IsNullOrEmpty(Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE"))))
                                      && ((Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE")).Trim().ToUpper() == "PAYEE_NOT_APPRV") ||
                                           (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE")).Trim().ToUpper() == "PAYEE_REJECT")))
                                {
                                    isIterate = false;
                                }
                                else
                                    isIterate = true;
                            }
                            else
                                isIterate = true;
                            if (isIterate)
                            {
                                i = i + 1;
                                if (i > 1)
                                    strJsonDenialCollection.Append(",");
                                if (i == 1)
                                    strJsonDenialCollection.Append("[");

                                strJsonDenialCollection.Append("{ ");
                                //skip duplicates (caused by inner join w/ splits table)
                                objCache = new LocalCache(m_sDSN, m_iClientId);
                                //objElemChild=objDOM.CreateElement("Transaction");
                                //Added to know the queued paym ents
                                if (bUseQueuedPayments)
                                {
                                    strJson.Append(Conversion.GetJsonNode("UtilitySetting", "Q", true));
                                }
                                else
                                {
                                    strJson.Append(Conversion.GetJsonNode("UtilitySetting", "H", true));
                                }


                                //Start: rsushilaggar Collections from LSS 06/02/2010 MITS 20938
                                if (Conversion.ConvertObjToBool(objTempRead.GetValue("PAYMENT_FLAG"), m_iClientId))
                                {
                                    strPaymentType = "P";
                                    strJson.Append(Conversion.GetJsonNode("PaymentType", "P"));
                                    //objElemChild.SetAttribute("PaymentType","P");
                                }
                                else
                                {
                                    strPaymentType = "C";
                                    strJson.Append(Conversion.GetJsonNode("PaymentType", "C"));
                                    //objElemChild.SetAttribute("PaymentType", "C");
                                }
                                //End: rsushilaggar

                                iEntityApprovalStatus = Common.Conversion.ConvertObjToInt(objTempRead.GetValue("ENTITY_APPROVAL_STATUS"), m_iClientId);
                                if (iEntityApprovalStatus != (int)objCacheFunctions.GetCodeIDWithShort("A", "ENTITY_APPRV_REJ"))
                                    strJson.Append(Conversion.GetJsonNode("EntityApprovalStatus", "NOT-APPROVED"));
                                else
                                    strJson.Append(Conversion.GetJsonNode("EntityApprovalStatus", "APPROVED"));
                                lLastTransID = lTransId;
                                strJson.Append(Conversion.GetJsonNode("CtlNumber", Common.Conversion.ConvertObjToStr(objTempRead.GetValue("CTL_NUMBER"))));
                                strJson.Append(Conversion.GetJsonNode("TransDate", Conversion.GetUIDate(
                                    Conversion.ConvertObjToStr(objTempRead.GetValue("TRANS_DATE")), m_slangCode, m_iClientId)));
                                strJson.Append(Conversion.GetJsonNode("TransDate.DbValue", GridCommonFunctions.GetSortColumnValueForDateField(objTempRead.GetString("TRANS_DATE"))));//CUSTOM SORTING FEILD
                                //JIRA 7810 start  fix RMA-13278
                                string sPayeesName = string.Empty;

                                int iNoOfPayees = 0;
                                string sEntSQL = "SELECT ISNULL(ENTITY.FIRST_NAME,'') + ' ' + ENTITY.LAST_NAME AS PAYEES_NAME from ENTITY INNER JOIN FUNDS_X_PAYEE ON  ENTITY.ENTITY_ID =FUNDS_X_PAYEE.PAYEE_EID WHERE FUNDS_X_PAYEE.FUNDS_TRANS_ID=" + lTransId;
                                if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                                {
                                    sEntSQL = sEntSQL.Replace("ISNULL", "nvl");
                                    sEntSQL = sEntSQL.Replace("+", "||");

                                }
                                sName = string.Empty;
                                // akaushik5 Added for MITS 37343 Ends
                                using (DbReader objeReader = DbFactory.GetDbReader(DbFactory.GetDbConnection(m_sDSN).ConnectionString, sEntSQL))
                                {
                                    while (objeReader.Read())
                                    {
                                        if (sPayeesName == string.Empty)
                                        {
                                            sPayeesName = objeReader.GetString("PAYEES_NAME");
                                        }
                                        else
                                        {
                                            sPayeesName = sPayeesName + "<br> " + objeReader.GetString("PAYEES_NAME");
                                        }

                                        // akaushik5 Added for MITS 37343 Starts
                                        sName = string.IsNullOrEmpty(sName) ? objeReader.GetString("PAYEES_NAME") : string.Format("{0} and {1}", sName, objeReader.GetString("PAYEES_NAME"));
                                        // akaushik5 Added for MITS 37343 Ends
                                        iNoOfPayees++;

                                    }
                                }

                                sClaimantLastName = Common.Conversion.ConvertObjToStr(objTempRead.GetValue("ENTITY_LAST_NAME"));
                                sClaimantFirstName = Common.Conversion.ConvertObjToStr(objTempRead.GetValue("ENTITY_FIRST_NAME"));

                                strJson.Append(Conversion.GetJsonNode("PayeeName", sName));
                                //rsushilaggar 05/05/2010 MITS# :20606
                                strJson.Append(Conversion.GetJsonNode("PrimaryClaimantName", sClaimantFirstName.Length > 0 ? sClaimantFirstName + " " + sClaimantLastName : sClaimantLastName));

                                sLSS_INVOICE_ID = Common.Conversion.ConvertObjToStr(objTempRead.GetDouble("LSS_INVOICE_ID"));
                                if (!sLSS_INVOICE_ID.Trim().Equals("0"))
                                {
                                    strJson.Append(Conversion.GetJsonNode("LSSInvoiceLink", "home?pg=riskmaster/Custom/Signon/SingleSignon&paramLSSInvoiceID=" + sLSS_INVOICE_ID));
                                    strJson.Append(Conversion.GetJsonNode("RMLSSInvoiceID", sLSS_INVOICE_ID));

                                }
                                if (!sLSS_INVOICE_ID.Trim().Equals("0"))
                                {
                                    strJson.Append(Conversion.GetJsonNode("LSSID", sLSS_INVOICE_ID));
                                }
                                else
                                {
                                    strJson.Append(Conversion.GetJsonNode("LSSID", "-"));
                                }

                                strJson.Append(Conversion.GetJsonNode("ClaimNumber", Common.Conversion.ConvertObjToStr(objTempRead.GetValue("CLAIM_NUMBER"))));

                                strJson.Append(Conversion.GetJsonNode("PaymentAmount", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sDSN), Conversion.ConvertObjToDouble(objTempRead.GetValue("FAMT"), m_iClientId), m_sDSN, m_iClientId)));
                                strJson.Append(Conversion.GetJsonNode("PaymentAmount.DbValue", Conversion.ConvertObjToDouble(objTempRead.GetValue("FAMT"), m_iClientId).ToString()));//CUSTOM SORTING FEILD
                                //Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
                                strJson.Append(Conversion.GetJsonNode("PaymentAmountNumber", Conversion.ConvertObjToStr(objTempRead.GetValue("FAMT"))));
                                strJson.Append(Conversion.GetJsonNode("PaymentAmountNumber.DbValue", Conversion.ConvertObjToDouble(objTempRead.GetValue("FAMT"), m_iClientId).ToString()));//CUSTOM SORTING FEILD
                                //End: rsushilaggar 05-May-2010 Vendor's Funds approval
                                strJson.Append(Conversion.GetJsonNode("User", Common.Conversion.ConvertObjToStr(objTempRead.GetValue("ADDED_BY_USER"))));
                                strJson.Append(Conversion.GetJsonNode("TransactionType", objCache.GetCodeDesc(
                                    Common.Conversion.ConvertStrToInteger(
                                    Common.Conversion.ConvertObjToStr(
                                    objTempRead.GetValue("TRANS_TYPE_CODE"))))));

                                strJson.Append(Conversion.GetJsonNode("SplitAmount", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sDSN), Conversion.ConvertObjToDouble(objTempRead.GetValue("FTSAMT"), m_iClientId), m_sDSN, m_iClientId)));
                                strJson.Append(Conversion.GetJsonNode("SplitAmount.DbValue", Conversion.ConvertObjToDouble(objTempRead.GetValue("FTSAMT"), m_iClientId).ToString()));//CUSTOM SORTING FEILD
                                //Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
                                strJson.Append(Conversion.GetJsonNode("SplitAmountNumber", Conversion.ConvertObjToDouble(objTempRead.GetValue("FTSAMT"), m_iClientId).ToString()));
                                strJson.Append(Conversion.GetJsonNode("SplitAmountNumber.DbValue", Conversion.ConvertObjToDouble(objTempRead.GetValue("FTSAMT"), m_iClientId).ToString()));//CUSTOM SORTING FEILD
                                //End: rsushilaggar 05-May-2010 Vendor's Funds approval
                                sTmp = Conversion.GetUIDate(
                                    Conversion.ConvertObjToStr(objTempRead.GetValue("FROM_DATE")), m_slangCode, m_iClientId);//vkumar258 ML Changes
                                sTmp = sTmp + " - " + Conversion.GetUIDate(
                                    Conversion.ConvertObjToStr(objTempRead.GetValue("TO_DATE")), m_slangCode, m_iClientId);//vkumar258 ML Changes
                                strJson.Append(Conversion.GetJsonNode("FromToDate", sTmp));
                                strJson.Append(Conversion.GetJsonNode("FromToDate.DbValue", GridCommonFunctions.GetSortColumnValueForDateField(objTempRead.GetString("FROM_DATE"))));//CUSTOM SORTING FEILD
                                strJson.Append(Conversion.GetJsonNode("TransId", Common.Conversion.ConvertObjToStr(objTempRead.GetValue("TRANS_ID"))));
                                //Start: rsushilaggar 05-May-2010 Vendor's Funds approval MITS 20606
                                iEntityApprovalStatus = Common.Conversion.ConvertObjToInt(objTempRead.GetValue("ENTITY_APPROVAL_STATUS"), m_iClientId);
                                if (iEntityApprovalStatus == 0)
                                {
                                    iEntityApprovalStatus = (int)objCacheFunctions.GetCodeIDWithShort("P", "ENTITY_APPRV_REJ");
                                }
                                strJson.Append(Conversion.GetJsonNode("PayeeStatus", objCache.GetCodeDesc(iEntityApprovalStatus)));
                                //End: rsushilaggar 05-May-2010 Vendor's Funds approval
                                //JIRA 7810 Start Snehal
                                if ((iEntityApprovalStatus.Equals(objCache.GetCodeId("A", "ENTITY_APPRV_REJ")))
                                      && (!string.IsNullOrEmpty(Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE"))))
                                      && (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE")).Contains("PAYEE_NOT_APPRV")))
                                {
                                    strJson.Append(Conversion.GetJsonNode("HOLD_REASON_CODE", Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE"))));
                                  
                                    if (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE")).Trim().Equals("PAYEE_NOT_APPRV"))
                                    {
                                        strJson.Append(Conversion.GetJsonNode("HOLD_REASON", objCache.GetCodeDesc(objCache.GetCodeId("PAYE_APRV_PYM_UNAPRV", "HOLD_REASON_PARENT"))));
                                    }
                                    else
                                    {
                                        strJson.Append(Conversion.GetJsonNode("HOLD_REASON", (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON")).Replace(objCache.GetCodeDesc(objCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), "")).Trim().TrimEnd(',').TrimStart(',').Replace(", ,", ",")));
                                    }
                                }
                                else
                                {
                                    strJson.Append(Conversion.GetJsonNode("HOLD_REASON_CODE", Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON_CODE"))));
                                    strJson.Append(Conversion.GetJsonNode("HOLD_REASON", Common.Conversion.ConvertObjToStr(objTempRead.GetValue("HOLD_REASON"))));
                                  
                                }

                                //Added by csingh7 for MITS 20092 :Start
                                objElemTemp = objDOM.CreateElement("Comments");
                                if (Common.Conversion.ConvertObjToStr(objTempRead.GetValue("COMMENTS")) != null && Common.Conversion.ConvertObjToStr(objTempRead.GetValue("COMMENTS")) != "")
                                    strJson.Append(Conversion.GetJsonNode("Comments", "Comments"));
                                else
                                    strJson.Append(Conversion.GetJsonNode("Comments", String.Empty));

                                //Added by csingh7 for MITS 20092 :End

                                //sharishkumar Jira 6417 starts
                                if (bUseSpecificSupervisor || bUseSupAppPayments)
                                {

                                    sSubmittedTo = objCache.GetSystemLoginName(Common.Conversion.ConvertObjToInt(objTempRead.GetValue("APPROVER_ID"), m_iClientId), m_iDsnId, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), m_iClientId);
                                    strJson.Append(Conversion.GetJsonNode("SubmittedTo", sSubmittedTo));
                                }
                                //sharishkumar Jira 6417 ends
                                strJson.Append(" }");
                                strJsonDenialCollection.Append(strJson.ToString());
                                objCache.Dispose();
                                objCache = null;
                                iCheckCount = iCheckCount + 1;
                                strJson.Remove(0, strJson.Length);
                            }                           
                            bTempRead = objTempRead.Read();
                        }
                        if (i != 0)//if no records found
                            strJsonDenialCollection.Append("]");
                        else //empty aray
                            strJsonDenialCollection.Append("[]");
                        CloseReader(ref objTempRead);
                        objElemChild = objDOM.CreateElement("DenialTransaction");
                        objDOM.FirstChild.AppendChild(objElemChild);

                        objElemTemp = objDOM.CreateElement("Data");
                        objElemTemp.InnerText = strJsonDenialCollection.ToString();
                        objElemChild.AppendChild(objElemTemp);

                        #region Prepare Json for UserPref and add to XMl doc for DenialTransaction
                        strJsonUserPref = "";
                        var isUserPrefExists = false;//rma 14898
                        if(!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true")
                            strJsonUserPref = GetOrCreateUserPreference(iUserID, p_sPageName, iDSNId, strGridName[3].ToString(), out isUserPrefExists);//rma 14898
                        else
                            strJsonUserPref = GetOrCreateUserPreference(iUserID, p_sPageName, iDSNId, strGridName[2].ToString(), out isUserPrefExists);//rma 14898
                        //RMA-13782 achouhan3   Addded to implement permission govern column starts
                        objGridPreferences = Riskmaster.Common.GridCommonFunctions.GetListforUserPreference(strJsonUserPref);
                        if (!objPermissionSettings.UseEntityApproval && objGridPreferences != null && objGridPreferences.colDef != null)
                        {
                            objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "PAYEESTATUS").FirstOrDefault();
                            if (objGridColDef != null)
                            {
                                objGridColDef.visible = false;
                                objGridColDef.alwaysInvisibleOnColumnMenu = true;
                            }
                        }
                        else
                        {
                            objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "PAYEESTATUS").FirstOrDefault();
                            if (objGridColDef != null)
                            {
                                objGridColDef.alwaysInvisibleOnColumnMenu = false;
                            }
                        }
                        if (!objPermissionSettings.ShowLSSInvoice)
                        {
                            objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "LSSID").FirstOrDefault();
                            if (objGridColDef != null)
                            {
                                objGridColDef.visible = false;
                                objGridColDef.alwaysInvisibleOnColumnMenu = true;
                            }
                        }
                        else
                        {
                            objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "LSSID").FirstOrDefault();
                            if (objGridColDef != null)
                            {
                                objGridColDef.alwaysInvisibleOnColumnMenu = false;
                            }
                        }
                        //RMA - 14903 - - snehal - dispaly hold reason on queued payment grid.
                        //RMA-14825 achouhan3   Display Hold Reason on My Pending transaction
                       // objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "HOLD_REASON").FirstOrDefault();
                       // if ((!String.IsNullOrEmpty(IsShowMyTrans) && IsShowMyTrans.Trim().ToLower() == "true"))
                       // {
                           // if (objGridColDef != null && isUserPrefExists == false)//rma 14898
                           // {
                              //  objGridColDef.visible = true;
                                //objGridColDef.alwaysInvisibleOnColumnMenu = true;
                             //   objGridColDef.alwaysInvisibleOnColumnMenu = false; //rkotak:rma 14898
                          //  }
                      //  }
                       // else
                       // {
                           // if (objGridColDef != null)
                           // {
                               // objGridColDef.visible = false;
                                //objGridColDef.alwaysInvisibleOnColumnMenu = false;
                              //  objGridColDef.alwaysInvisibleOnColumnMenu = true;//rkotak: RMA- 14903, hold reason column is implemented for payment grid only
                           // }
                        //}
                        //RMA-14825 achouhan3   Display Hold Reason on My Pending transaction
                        //RMA - 14903 - - snehal - dispaly hold reason on queued payment grid.
                        strJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(objGridPreferences);
                        //RMA-13782 achouhan3   Addded to implement permission govern column starts

                        objUserPrefXmlElement = null;
                        objUserPrefXmlElement = objDOM.CreateElement("UserPref");
                        objUserPrefXmlElement.InnerText = strJsonUserPref;
                        objElemChild.AppendChild(objUserPrefXmlElement);
                        #endregion

                        #region Prepare Json string for additional data and add to XMl doc
                        strJsonAdditionalData.Remove(0, strJsonAdditionalData.Length);
                        objAdditionalDataXmlElement = null;
                        objAdditionalDataXmlElement = objDOM.CreateElement("AdditionalData");
                        strJsonAdditionalData = new StringBuilder();
                        //dicGridData = new Dictionary<string, string>();
                        // lstDictData.Clear();
                        strJsonAdditionalData.Append("[{");
                        strJsonAdditionalData.Append(Conversion.GetJsonNode("TotalCount", i.ToString(), true));
                        //lstDictData.Add(dicGridData);
                        //strJsonResponse = Conversion.CreateJSONStringFromDictionary(lstDictData);
                        strJsonAdditionalData.Append("}]");
                        objAdditionalDataXmlElement.InnerText = strJsonAdditionalData.ToString();
                        objElemChild.AppendChild(objAdditionalDataXmlElement);
                        #endregion

                        //Added to check whther to diplay grid or not when permission is not set
                        objElemTemp = objDOM.CreateElement("IsQueuePayment");
                        objElemTemp.InnerText = Convert.ToString(bUseQueuedPayments);
                        objElemChild.AppendChild(objElemTemp);

                        p_objOutDenyTran = objDOM;
                    }
                    iReturnValue = 1;

                }
                catch (RMAppException p_objException)
                {
                    throw p_objException;
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("SupervisorApproval.ListFundsTransactions.Error", m_iClientId), p_objException);
                }
                finally
                {
                    //CloseReader(ref objRead);
                    if (objDt != null)
                        objDt.Dispose();
                    if (objDs != null)
                        objDs.Dispose();
                    CloseReader(ref objTempRead);
                    sbSQL = null;
                    sbSQLTemp = null;
                    objCacheFunctions = null;
                    objDOM = null;
                    objElemParent = null;
                    objElemChild = null;
                    objElemTemp = null;
                    if (objCache != null)
                    {
                        objCache.Dispose();
                        objCache = null;
                    }
                    dictMaxAmount = null;
                }
                return iReturnValue;
            }

            /// <summary>
            /// this function will create user pref for the grid if it does not exist in the database
            /// </summary>
            /// 

            private string GetOrCreateUserPreference(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId )
            {
                string sJsonUserPref = string.Empty;
                bool IsUserPrefExist = false;
                
                try
                {
                    Riskmaster.Common.GridCommonFunctions.GetUserHeaderAndPreference(ref sJsonUserPref, p_iUserID, m_iClientId, p_sPageName, p_iDSNId.ToString(), p_sGridId);
                    if (String.IsNullOrEmpty(sJsonUserPref))
                        IsUserPrefExist = false;
                    else
                        IsUserPrefExist = true;

                    if (!IsUserPrefExist)
                    {
                        return CreateUserPreference(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
                    }
                }
                catch (PermissionViolationException objException)
                {
                    throw objException;
                }
                catch (RMAppException objException)
                {
                    throw objException;
                }
                catch (Exception objException)
                {
                    throw new RMAppException(Globalization.GetString("ReserveFunds.CreateUserPreference.Error", m_iClientId), objException);
                }
                finally
                {
                }
                return sJsonUserPref;
            }
            //rma 14898 starts
            private string GetOrCreateUserPreference(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId, out bool IsUserPrefExist)
            {
                string sJsonUserPref = string.Empty;
                //bool IsUserPrefExist = false;
                IsUserPrefExist = false;
                try
                {
                    Riskmaster.Common.GridCommonFunctions.GetUserHeaderAndPreference(ref sJsonUserPref, p_iUserID, m_iClientId, p_sPageName, p_iDSNId.ToString(), p_sGridId);
                    if (String.IsNullOrEmpty(sJsonUserPref))
                        IsUserPrefExist = false;
                    else
                        IsUserPrefExist = true;

                    if (!IsUserPrefExist)
                    {
                        return CreateUserPreference(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
                    }
                }
                catch (PermissionViolationException objException)
                {
                    throw objException;
                }
                catch (RMAppException objException)
                {
                    throw objException;
                }
                catch (Exception objException)
                {
                    throw new RMAppException(Globalization.GetString("ReserveFunds.CreateUserPreference.Error", m_iClientId), objException);
                }
                finally
                {
                }
                return sJsonUserPref;
            }
            //rma 14898 ends
            /// <summary>
            /// This function is used to Create default User Pref when it does not exist in database
            /// </summary>
            /// <param name="p_iUserID"></param>
            /// <param name="p_sPageName"></param>
            /// <param name="p_iDSNId"></param>
            /// <param name="p_sGridId"></param>
            /// <returns></returns>
            private string CreateUserPreference(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
            {
                string sJsonUserPref = string.Empty;
                try
                {
                    //if there are multiple grids on the page, each grid can have different user pref.     
                    sJsonUserPref = CreateUserPreferenceForGrid(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
                }
                catch (PermissionViolationException objException)
                {
                    throw objException;
                }
                catch (RMAppException objException)
                {
                    throw objException;
                }
                catch (Exception objException)
                {
                    throw new RMAppException(Globalization.GetString("ReserveFunds.CreateUserPreference.Error", m_iClientId), objException);
                }
                finally
                {
                }
                return sJsonUserPref;

            }

            private string CreateUserPreferenceForGrid(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
            {
                string sJsonUserPref = string.Empty;
                int iCarrierClaim;
                List<GridColumnDefHelper> lstColDef;
                GridPreference oGridPref;
                string sPageID = "0";
                int iIsMultiCurrencyOn = -1;
                string sLssCellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\" title=\"{{COL_FIELD}}\" ><a href=\"#\" onclick=\" navigateToLSS('paramLSSInvoiceID={{row.getProperty('LSSID')}}'); return false;\"> {{COL_FIELD}} </a> </div>"; //rma-8617                        
                string sHyperlinkCellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\" title=\"{{COL_FIELD}}\" ><a href=\"#\" onclick=\" return LookupComments('{{row.getProperty('TransId')}}');\"> Comments </a> </div>";            ////rma-10914
                Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

                try
                {
                    sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                    lstColDef = new List<GridColumnDefHelper>();

                    if (p_sGridId == "gridPaymentApproval" || p_sGridId == "gridCollectionApproval")
                    {
                        lstColDef.Add(new GridColumnDefHelper("UtilitySetting", "UtilitySetting", false, "", "", "", false, true));             ////rma-10914                   
                        lstColDef.Add(new GridColumnDefHelper("PAYMENT_FLAG", "PAYMENT_FLAG", false, "", "", "", false, true));
                        lstColDef.Add(new GridColumnDefHelper("EntityApprovalStatus", "EntityApprovalStatus", false, "", "", "", false, true));
                        lstColDef.Add(new GridColumnDefHelper("CtlNumber", getMLHeaderText("CtlNumber", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("TransDate", getMLHeaderText("TransDate", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("TransDate.DbValue", "TransDate.DbValue", false, "", "", "", false, true, GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));//rkotak:custom sorting,need to add sorting function in the arguments for this column because this is the default sorting column as well               
                        lstColDef.Add(new GridColumnDefHelper("PayeeName", getMLHeaderText("PayeeName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("PrimaryClaimantName", getMLHeaderText("PrimaryClaimantName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("LSSID", getMLHeaderText("LSSID", sPageID), true, sLssCellTemplate, "", "", false, false));
                        lstColDef.Add(new GridColumnDefHelper("ClaimNumber", getMLHeaderText("ClaimNumber", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("PaymentAmount", getMLHeaderText("PaymentAmount", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("PaymentAmount.DbValue", "PaymentAmount.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                        lstColDef.Add(new GridColumnDefHelper("PaymentAmountNumber", "PaymentAmountNumber", false, "", "", "", false, true, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("PaymentAmountNumber.DbValue", "PaymentAmountNumber.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                        // rkotak: multiple if conditions are intentional. Maintain strict order of columns
                        lstColDef.Add(new GridColumnDefHelper("User", getMLHeaderText("User", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("TransactionType", getMLHeaderText("TransactionType", sPageID)));

                        lstColDef.Add(new GridColumnDefHelper("SplitAmount", getMLHeaderText("SplitAmount", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("SplitAmount.DbValue", "SplitAmount.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                        lstColDef.Add(new GridColumnDefHelper("SplitAmountNumber", "SplitAmountNumber", false, "", "", "", false, true, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("SplitAmountNumber.DbValue", "SplitAmountNumber.DbValue", false, "", "", "", false, true));//rkotak:custom sorting


                        lstColDef.Add(new GridColumnDefHelper("FromToDate", getMLHeaderText("FromToDate", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("FromToDate.DbValue", "FromToDate.DbValue", false, "", "", "", false, true, GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));//rkotak:custom sorting,need to add sorting function in the arguments for this column because this is the default sorting column as well               
                        lstColDef.Add(new GridColumnDefHelper("VoidReason", "VoidReason", false, "", "", "", false, true));
                        lstColDef.Add(new GridColumnDefHelper("TransId", "TransId", false, "", "", "", false, true)); //we have not given ate sot function for this column because this col contains mix values of string and date ("No" or cleared date")
                        lstColDef.Add(new GridColumnDefHelper("PayeeStatus", getMLHeaderText("PayeeStatus", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("Comments", getMLHeaderText("Comments", sPageID), true, sHyperlinkCellTemplate, "", "", false));
                        lstColDef.Add(new GridColumnDefHelper("SubmittedTo", getMLHeaderText("SubmittedTo", sPageID)));
                        if (p_sGridId == "gridPaymentApproval")
                        {
                            lstColDef.Add(new GridColumnDefHelper("HOLD_REASON", getMLHeaderText("HOLD_REASON", sPageID)));
                            lstColDef.Add(new GridColumnDefHelper("HOLD_REASON_CODE", "HOLD_REASON_CODE", false, "", "", "", false, true));
                        }
                        else
                        {
                            lstColDef.Add(new GridColumnDefHelper("HOLD_REASON", getMLHeaderText("HOLD_REASON", sPageID), false, "", "", "", false, true));
                            lstColDef.Add(new GridColumnDefHelper("HOLD_REASON_CODE", "HOLD_REASON_CODE", false, "", "", "", false, true));
                        }

                        oGridPref = new GridPreference();
                        oGridPref.colDef = lstColDef;
                        oGridPref.SortColumn = new string[1] { "CtlNumber" }; // should match with sql query order by clause for default sorting
                        oGridPref.SortDirection = new string[1] { "asc" };
                        oGridPref.AdditionalUserPref = dicAdditionalUserPref;
                        sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
                    }
                    else
                    {
                        lstColDef.Add(new GridColumnDefHelper("UtilitySetting", "UtilitySetting", false, "", "", "", false, true));             ////rma-10914                   
                        lstColDef.Add(new GridColumnDefHelper("PAYMENT_FLAG", "PAYMENT_FLAG", false, "", "", "", false, true));
                        lstColDef.Add(new GridColumnDefHelper("EntityApprovalStatus", "EntityApprovalStatus", false, "", "", "", false, true));
                        lstColDef.Add(new GridColumnDefHelper("CtlNumber", getMLHeaderText("CtlNumber", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("TransDate", getMLHeaderText("TransDate", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("TransDate.DbValue", "TransDate.DbValue", false, "", "", "", false, true, GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));//rkotak:custom sorting,need to add sorting function in the arguments for this column because this is the default sorting column as well               
                        lstColDef.Add(new GridColumnDefHelper("PayeeName", getMLHeaderText("PayeeName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("PrimaryClaimantName", getMLHeaderText("PrimaryClaimantName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("LSSID", getMLHeaderText("LSSID", sPageID), true, "", "", "", false, false));
                        lstColDef.Add(new GridColumnDefHelper("ClaimNumber", getMLHeaderText("ClaimNumber", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("PaymentAmount", getMLHeaderText("PaymentAmount", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("PaymentAmount.DbValue", "PaymentAmount.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                        lstColDef.Add(new GridColumnDefHelper("PaymentAmountNumber", "PaymentAmountNumber", false, "", "", "", false, true));//rkotak:custom sorting
                        lstColDef.Add(new GridColumnDefHelper("PaymentAmountNumber.DbValue", "PaymentAmountNumber.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                        // rkotak: multiple if conditions are intentional. Maintain strict order of columns
                        lstColDef.Add(new GridColumnDefHelper("User", getMLHeaderText("User", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("TransactionType", getMLHeaderText("TransactionType", sPageID)));

                        lstColDef.Add(new GridColumnDefHelper("SplitAmount", getMLHeaderText("SplitAmount", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("SplitAmount.DbValue", "SplitAmount.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                        lstColDef.Add(new GridColumnDefHelper("SplitAmountNumber", "SplitAmountNumber", false, "", "", "", false, true, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("SplitAmountNumber.DbValue", "SplitAmountNumber.DbValue", false, "", "", "", false, true));//rkotak:custom sorting


                        lstColDef.Add(new GridColumnDefHelper("FromToDate", getMLHeaderText("FromToDate", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("FromToDate.DbValue", "FromToDate.DbValue", false, "", "", "", false, true, GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));//rkotak:custom sorting,need to add sorting function in the arguments for this column because this is the default sorting column as well               
                        lstColDef.Add(new GridColumnDefHelper("TransId", "TransId", false, "", "", "", false, true)); //we have not given ate sot function for this column because this col contains mix values of string and date ("No" or cleared date")
                        lstColDef.Add(new GridColumnDefHelper("PayeeStatus", getMLHeaderText("PayeeStatus", sPageID)));
                        //RMA-13891     achouhan3   My pending reserve not showing comment as link
                        lstColDef.Add(new GridColumnDefHelper("Comments", getMLHeaderText("Comments", sPageID), true, sHyperlinkCellTemplate, "", "", false));
                        lstColDef.Add(new GridColumnDefHelper("SubmittedTo", getMLHeaderText("SubmittedTo", sPageID)));
                                     //RMA-7810
                        //RMA - 14903 - - snehal - dispaly hold reason on queued payment grid.
                       // lstColDef.Add(new GridColumnDefHelper("HOLD_REASON", getMLHeaderText("HOLD_REASON", sPageID), false, "", "", "", false, true));
                       // lstColDef.Add(new GridColumnDefHelper("HOLD_REASON_CODE", "HOLD_REASON_CODE", false, "", "", "", false, true));
                        lstColDef.Add(new GridColumnDefHelper("HOLD_REASON", getMLHeaderText("HOLD_REASON", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("HOLD_REASON_CODE", "HOLD_REASON_CODE", false, "", "", "", false, true));
                        //RMA - 14903 - - snehal - dispaly hold reason on queued payment grid.
                        oGridPref = new GridPreference();
                        oGridPref.colDef = lstColDef;
                        oGridPref.SortColumn = new string[1] { "CtlNumber" }; // should match with sql query order by clause for default sorting
                        oGridPref.SortDirection = new string[1] { "asc"};
                        oGridPref.AdditionalUserPref = dicAdditionalUserPref;
                        sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
                    }


                }
                catch (PermissionViolationException objException)
                {
                    throw objException;
                }
                catch (RMAppException objException)
                {
                    throw objException;
                }
                catch (Exception objException)
                {
                    throw new RMAppException(Globalization.GetString("ReserveFunds.CreateUserPreference.Error", m_iClientId), objException);
                }
                finally
                {
                    lstColDef = null;
                    oGridPref = null;
                }
                return sJsonUserPref;
            }

            private string getMLHeaderText(string key, string sPageID)
            {
                string sBaseLangCode = "0";
                string sUserLangCode = "0";
                try
                {
                    sUserLangCode = m_slangCode.ToString();
                    if (sUserLangCode == "0" || String.IsNullOrEmpty(sUserLangCode))
                    {
                        //set base lang code as user lang code if it is not defined
                        sUserLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
                    }
                    return CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString(key, 0, sPageID, m_iClientId), sUserLangCode);
                }
                catch (PermissionViolationException objException)
                {
                    throw objException;
                }
                catch (RMAppException objException)
                {
                    throw objException;
                }
                catch (Exception objException)
                {
                    throw objException;
                }
            }

            /// <summary>
            /// This function is used to restore default user pref
            /// </summary>
            /// <param name="p_iUserID"></param>
            /// <param name="p_sPageName"></param>
            /// <param name="p_iDSNId"></param>
            /// <param name="p_sGridId"></param>
            /// <returns></returns>
            public string RestoreDefaultUserPref(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
            {
                //RMA-14678    achouhan3 Modified for appliying permission of LSS and Payee Status column
                string sJsonUserPref = "";
                SysSettings objPermissionSettings = null;
                GridPreference objGridPreferences = null;
                GridColumnDefHelper objGridColDef;
                try
                {
                    if (Riskmaster.Common.GridCommonFunctions.DeleteUserPreference(p_iUserID, m_iClientId, p_sPageName, p_iDSNId, p_sGridId))
                        sJsonUserPref = CreateUserPreference(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
                    objPermissionSettings = new SysSettings(m_sDSN, m_iClientId);
                    objGridPreferences = Riskmaster.Common.GridCommonFunctions.GetListforUserPreference(sJsonUserPref);
                    if (!objPermissionSettings.UseEntityApproval && objGridPreferences != null && objGridPreferences.colDef != null)
                    {
                        objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "PAYEESTATUS").FirstOrDefault();
                        if (objGridColDef != null)
                        {
                            objGridColDef.visible = false;
                            objGridColDef.alwaysInvisibleOnColumnMenu = true;
                        }
                    }
                    else
                    {
                        objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "PAYEESTATUS").FirstOrDefault();
                        if (objGridColDef != null)
                        {
                            objGridColDef.alwaysInvisibleOnColumnMenu = false;
                        }
                    }
                    if (!objPermissionSettings.ShowLSSInvoice)
                    {
                        objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "LSSID").FirstOrDefault();
                        if (objGridColDef != null)
                        {
                            objGridColDef.visible = false;
                            objGridColDef.alwaysInvisibleOnColumnMenu = true;
                        }
                    }
                    else
                    {
                        objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "LSSID").FirstOrDefault();
                        if (objGridColDef != null)
                        {
                            objGridColDef.alwaysInvisibleOnColumnMenu = false;
                        }
                    }
                    //RMA - 14903 - - snehal - dispaly hold reason on queued payment grid.
                    //RMA-14825 achouhan3   Display Hold Reason on My Pending transaction
                    //objGridColDef = objGridPreferences.colDef.Where(X => X.field.ToUpper() == "HOLD_REASON").FirstOrDefault();
                    //if (p_sGridId.ToLower() == "gridmypendingtransaction")
                   // {
                       // if (objGridColDef != null)
                       // {
                           // objGridColDef.visible = true;
                           // objGridColDef.alwaysInvisibleOnColumnMenu = true;
                        //}
                   // }
                   // else
                   // {
                       // if (objGridColDef != null)
                       // {
                          //  objGridColDef.visible = false;
                          //  objGridColDef.alwaysInvisibleOnColumnMenu = false;
                       // }
                  //  }
                    //RMA-14825 achouhan3   Display Hold Reason on My Pending transaction
                    sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(objGridPreferences);
                    //RMA-14678    achouhan3 Modified for appliying permission of LSS and Payee Status column
                }
                catch (PermissionViolationException objException)
                {
                    throw objException;
                }
                catch (RMAppException objException)
                {
                    throw objException;
                }
                catch (Exception objException)
                {
                    throw new RMAppException(Globalization.GetString("ReserveFunds.RestoreDefaultUserPref.Error", m_iClientId), objException);
                }
                finally
                {
                    objPermissionSettings = null;
                    objGridPreferences = null;
                    objGridColDef = null;
                }
                return sJsonUserPref;
            }
            //RMA-6404  achouhan3   Added for Angular Grid Implementation Ends


            /// <summary>
            /// Created By :    Achouhan3 
            /// Created Date :  17/Aug/2015
            /// Description:    Added to cache Max Amount corresponding to a user for all associated LOB
            /// </summary>
            /// <param name="p_lUserId"></param>
            /// <param name="iLobCode"></param>
            /// <param name="dictMaxAmount"></param>
            /// <returns></returns>
            private double GetMaxAmountByUser(long p_lUserId, int iLobCode, ref Dictionary<int, double> dictMaxAmount)
            {
                double dMaxAmount = 0;
                StringBuilder sbSQL = null;
                DataSet dsLob = null;
                Dictionary<object, object> dictTemp = new Dictionary<object, object>(); //RMA-13864 achouhan3   Added for datatype issue in case of oracle
                try
                {
                    if (dictMaxAmount == null)
                    {
                        sbSQL = new StringBuilder();
                        dictMaxAmount = new Dictionary<int, double>();
                        sbSQL.Append("SELECT LINE_OF_BUS_CODE, MAX_AMOUNT FROM FUNDS_LIMITS WHERE FUNDS_LIMITS.USER_ID = " + p_lUserId.ToString());
                        dsLob = DbFactory.GetDataSet(m_sDSN, sbSQL.ToString(), m_iClientId);
                        if (dsLob.Tables[0] != null && dsLob.Tables[0].Rows.Count > 0)
                            //RMA-13864 achouhan3   Added for datatype issue in case of oracle
                            dictTemp = dsLob.Tables[0].AsEnumerable().ToDictionary(row => row.Field<object>(0), row => row.Field<object>(1));
                        dictMaxAmount = dictTemp.ToDictionary(kvp =>Convert.ToInt32(kvp.Key), kvp => Convert.ToDouble(kvp.Value)); //RMA-13864 achouhan3   Added for datatype issue in case of oracle
                    }
                    if (dictMaxAmount.ContainsKey(iLobCode))
                        dMaxAmount = Conversion.ConvertObjToDouble(dictMaxAmount[iLobCode], m_iClientId);
                    else
                        dMaxAmount = -1;
                }
                catch (RMAppException p_objException)
                {
                    throw p_objException;
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("SupervisorApproval.GetMaxAmount.Error", m_iClientId), p_objException);
                }
                finally
                {
                    if (dsLob != null)
                        dsLob.Dispose();
                    sbSQL = null;
                    dictTemp = null;
                }
                return dMaxAmount;
            }

        //RMA-6404  achouhan3   Added to optimize performance on Angular Grid
        //RMA-13778 achohan3    Added to handle multiple error Starts
        public int GetFundsTransactions(XmlDocument p_objXmlIn, out XmlDocument p_objOutAllTran, out XmlDocument p_objOutDenyTran, out string p_sOutWarningXml, out string p_sOutErrorXml, UserLogin p_objUserLogin
            , string p_sPageName, string p_sGridID, string p_sLangCode = "1033")
        {
            try
            {
                m_slangCode = p_sLangCode;//vkumar258 ML Changes
                p_sOutWarningXml = m_sWarnings;
                if (!string.IsNullOrEmpty(p_sOutWarningXml))
                {
                    //achouhan3 Modified for ML Suport
                    string sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                    p_sOutWarningXml = p_sOutWarningXml.Substring(0, p_sOutWarningXml.Length - 2);
                   
                    p_sOutWarningXml = "<Warnings><Warning>" + getMLHeaderText("lblPaymentWarning", sPageID) + p_sOutWarningXml + "</Warning></Warnings>";
                    m_sWarnings = string.Empty;
                }
                return GetFundsTransactions(p_objXmlIn, out p_objOutAllTran, out p_objOutDenyTran, out p_sOutErrorXml, p_objUserLogin, p_sPageName, p_sGridID, p_sLangCode);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SupervisorApproval.ListFundsTransactions.Error", m_iClientId), p_objException);
            }
        }
        //RMA-13778 achohan3    Added to handle multiple error Ends
        
	}

}
