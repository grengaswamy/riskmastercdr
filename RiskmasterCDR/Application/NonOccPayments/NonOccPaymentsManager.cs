﻿using System;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;
using System.Collections;
using System.Collections.Generic;
using Riskmaster.Settings;
using Riskmaster.Security;
using Riskmaster.Application.FundManagement;
using Riskmaster.Common;

namespace Riskmaster.Application.NonOccPayments
{
	///************************************************************** 
	///* $File		: NonOccPaymentsManager.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 22-Sep-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to fetch the nonocc payments information for a Claim.
	/// </summary>
	public class NonOccPaymentsManager :IDisposable
	{
		#region Constructor
		/// <summary>
		/// Default constructor.
		/// </summary>
		public NonOccPaymentsManager()
		{
		}
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		public NonOccPaymentsManager(string p_sDsnName , string p_sUserName , string p_sPassword, int p_iClientId)
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
			this.Initialize();
		}
		#endregion

        #region Destructor

        ~NonOccPaymentsManager()
        {
            Dispose();
        }

        #endregion


        #region Constants
        public const int SysCmd_Calc=1;
		public const int SysCmd_DisplayPrinted=2;
		public const int SysCmd_DisplayFuture=3;
		public const int SysCmd_Continue=4;
		public const int SysCmd_DupePayments=5;
		public const int SysCmd_SavePayments=6;
		public const int SysCmd_EditPayment=7;
		public const int SysCmd_DeletePayment=8;
		public const int SysCmd_SaveEdit=9;
		#endregion

		#region Variable Declaration
        //igupta3        
        /// <summary>
        /// Private variable to store the instance of Warning List
        /// </summary>
        private ArrayList m_arrlstWarnings = null;
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Private variable to store the instance of Local Cache
		/// </summary>
		private LocalCache m_objCache = null;
        private double m_dDailyAmount = 0;
        private double m_dDailySuppAmount = 0;
        private string m_sBenefitStartDate = "";
        private string m_sBenefitThroughDate = "";
        private double m_dPension = 0;
        private double m_dSS = 0;
        private double m_dOther = 0;
        //Added by RKaur
        private double m_dOther1 = 0;
        private double m_dOther2 = 0;
        private double m_dOther3 = 0;
        private double m_dPostTax1 = 0;
        private double m_dPostTax2 = 0;
        ///////////////
        private int m_iDaysWorkingInMonth = 0;
        private int m_iDaysWorkingInWeek = 0;
        private int m_iOffsetCalc = 0;
        private double m_dPensionOffset = 0;
        private double m_dSSOffset = 0;
        private double m_dOiOffset = 0;
        //added by Ravneet Kaur
        private double m_dOthOffset1 = 0;
        private double m_dOthOffset2 = 0;
        private double m_dOthOffset3 = 0;
        private double m_dPstTaxDed1 = 0;
        private double m_dPstTaxDed2 = 0;
        ///////////////////////// 
        private double m_dFederalTax = 0;
        private double m_dSocialSecurityAmount = 0;
        private double m_dMedicareAmount = 0;
        private double m_dStateAmount = 0;
        private long m_lSuppSubAcc = 0;
        private long m_lSuppBankAcc = 0;
        private long m_lSuppTransType = 0;
        private long m_lSuppResType = 0;
        private long m_lClaimantPayeeTypeCode = 0;                //MITS 14206
        private long m_lOtherPayeeTypeCode = 0;                   //MITS 14206
        private bool m_bSuppRollUp = false;
        private double m_dNetPayment = 0;
        private double m_dGrossCalculatedPayment = 0;
        private double m_dGrossCalculatedSupplement = 0;
        private double m_dGrossTotalNetOffsets = 0;
        private int m_ilblDaysIncluded = 0;
        private double m_dSuppRate = 0;
        private double m_dWeeklyBenefit = 0;
        private int iCount = 0;
        private DateTime m_dBenCalcPayStart = DateTime.MinValue;
        private DateTime m_dBenCalcPayTo = DateTime.MinValue;
        private int m_iClientId = 0;
        #endregion

		#region Structure
		internal struct TaxRate
		{
			public double FED_TAX_RATE;
			public double SS_TAX_RATE;
			public double MEDICARE_TAX_RATE;
			public double STATE_TAX_RATE;
			public int iFedTaxEID; 
			public int iSSTaxEID;
			public int iMedicareTaxEID;
			public int iStateTaxEID;
			public int iFedTransTypeCodeID;
			public int iSSTransTypeCodeID;
			public int iMedicareTransTypeCodeID;
			public int iStateTransTypeCodeID;
			public bool bFed;
			public bool bSS;
			public bool bMed;
			public bool bState;
			public double dblTaxablePercent;
		}

		internal struct EntityData
		{
			//0=Claimant,1=fed,2=ss,3=med,4=state
			public int ClaimantEid;
			public int ClaimId;
			public string ClaimNumber;
			public int BankAccountId;
			public int SubBankAccountId;
			public string[] FN;
			public string[] MN;
			public string[] LN;
			public string[] Addr1;
			public string[] Addr2;
            public string[] Addr3;
            public string[] Addr4;
			public string[] City;
			public int[] StateId;
			public string[] Zip;
			public int[] PayeeType;
			public int[] ReserveTypeCode;
		}
    
		internal enum Entities:int
		{
			//0=Claimant,1=fed,2=ss,3=med,4=state
			Clmnt = 0,
			Fed = 1,
			SS = 2,
			Med = 3,
			State = 4
		}

		#endregion

		#region Public Functions

		public XmlDocument GetPrintedPayments(int p_iClaimId, int p_iPiEid,string p_sTaxArray)
		{
			XmlDocument objPrintedPayments=null;
			XmlElement objDocumentElement=null;
			XmlElement objRow=null;
            StringBuilder sSQL = new StringBuilder();
			DbReader objReader = null;
			int iClaimStatusCode=0;
			string[] arrTaxes=p_sTaxArray.Split(new char[] {','});
			int iBatchIdLocal=0;
			int iBatchIdCheck=0;
			string sPaymentNum=string.Empty;
			string sPaymentNumCheck=string.Empty;
            string sCTLNumber = string.Empty;
            string sCTLNumCheck = string.Empty;
			bool bZeroRecords=true;
            //Animesh Inserted For GHS Enhancment //pmittal5 Mits 14841
            //PJS, 05-06-08, MITS 12172 - Changes made same as MITS 11879
            //bool[] bCheckEnter = new bool[7] { true, true, true, true, true, true, true };
            bool[] bCheckEnter = new bool[12] { true, true, true, true, true, true, true, true, true, true, true, true };
            //Animesh Insertion Ends
            bool bNotEntered = true;
            //PJS, 05-06-08, MITS 12172 - Changes made same as MITS 11879
			int iTransTypeCode=0;
			double dblAmt=0;
			string sTax=string.Empty;
            string sAccountName = string.Empty;

            long lPensionOffTransTypeCodeID = 0;
            long lSSOffTransTypeCodeID = 0;
            long lOtherOffTransTypeCodeID = 0;
            //Added by RKaur
            long lOthOff1TransTypeCodeID = 0;
            long lOthOff2TransTypeCodeID = 0;
            long lOthOff3TransTypeCodeID = 0;
            long lPstTax1TransTypeCodeID = 0;
            long lPstTax2TransTypeCodeID = 0;
            ///////////////////////////////

            //Raman Bhatia: LTD changes: Get the Transaction Types for Offsets

            sSQL.Append("SELECT * FROM OFFSET_MAPPING");
            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
            //loop and get OFFSET trans type code
            while (objReader.Read())
            {
                if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "PENSION")
                    lPensionOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "SOCIAL_SECURITY")
                    lSSOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHER_INCOME")
                    lOtherOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                //Added by RKaur //pmittal5 Mits 14841
                if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET1")
                    lOthOff1TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET2")
                    lOthOff2TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET3")
                    lOthOff3TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "POST_TAX_DED1")
                    lPstTax1TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "POST_TAX_DED2")
                    lPstTax2TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
            }
            objReader.Close();
            sSQL.Remove(0, sSQL.Length);
            
            objPrintedPayments=new XmlDocument();
			objDocumentElement=objPrintedPayments.CreateElement("table");
			objDocumentElement.SetAttribute("syscmd",SysCmd_DisplayPrinted.ToString());
			objPrintedPayments.AppendChild(objDocumentElement);

			//header row
			objRow=objPrintedPayments.CreateElement("row");
			objDocumentElement.AppendChild(objRow);
			objRow.SetAttribute("class", "colheader");
			objRow.SetAttribute("batchid", "Batch #");
			objRow.SetAttribute("paymentnumber", "Payment #");
			objRow.SetAttribute("void", "Void");
			objRow.SetAttribute("ctl", "Control Number");
			objRow.SetAttribute("transtype", "Trans Type");
			objRow.SetAttribute("fromdate", "From Date");
			objRow.SetAttribute("todate", "To Date");
			objRow.SetAttribute("printdate", "Print Date");
			objRow.SetAttribute("gross", "Gross Payment");
            objRow.SetAttribute("supppayment", "Gross Supp Payment");
			objRow.SetAttribute("net", "Net Payment");
			objRow.SetAttribute("numofpayments", "Days Paid");
			objRow.SetAttribute("fed", "Federal Tax");
			objRow.SetAttribute("ss", "Social Security Tax");
			objRow.SetAttribute("med", "Medicare Tax");
			objRow.SetAttribute("state", "State Tax");
            objRow.SetAttribute("pension", "Pension Offset");
            objRow.SetAttribute("ssoffset", "SS Offset");
            objRow.SetAttribute("oi", "Other Offset");
            //Added by RKaur //pmittal5 Mits 14841
            objRow.SetAttribute("othoffset1", "Other Offset #1");
            objRow.SetAttribute("othoffset2", "Other Offset #2");
            objRow.SetAttribute("othoffset3", "Other Offset #3");
            objRow.SetAttribute("psttaxded1", "Post Tax Deduction #1");
            objRow.SetAttribute("psttaxded2", "Post Tax Deduction #2");
            /////////////////////////////
			objRow.SetAttribute("account", "Account Name");
            objRow.SetAttribute("supptranstypecode", "Supp Trans Type");
				
			//get claimant name & claim number
			sSQL.Append("SELECT ENTITY.FIRST_NAME, ENTITY.LAST_NAME, CLAIM.CLAIM_NUMBER");
			sSQL.Append(" FROM ENTITY INNER JOIN CLAIMANT");
            sSQL.Append(" ON ENTITY.ENTITY_ID = CLAIMANT.CLAIMANT_EID");
            sSQL.Append(" INNER JOIN CLAIM");
            sSQL.Append(" ON CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID");
			sSQL.Append(" WHERE CLAIMANT.PRIMARY_CLMNT_FLAG <> 0");
			sSQL.Append(" AND CLAIMANT.PRIMARY_CLMNT_FLAG IS NOT NULL");
			sSQL.Append(" AND CLAIMANT.CLAIM_ID = " + p_iClaimId);
			sSQL.Append(" AND CLAIM.CLAIM_ID = " + p_iClaimId);
			if(p_iPiEid>0)
			{
				sSQL.Append(" AND CLAIMANT.CLAIMANT_EID = " + p_iPiEid);
				sSQL.Append(" AND ENTITY.ENTITY_ID = " + p_iPiEid);
			}
				
			try
			{
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
				if (objReader.Read())
				{
					objDocumentElement.SetAttribute("claimant",Conversion.ConvertObjToStr(objReader.GetValue(0))+ " " + Conversion.ConvertObjToStr(objReader.GetValue(1)));
					objDocumentElement.SetAttribute("claimnumber",Conversion.ConvertObjToStr(objReader.GetValue(2)));
				}
                sSQL = sSQL.Remove(0, sSQL.Length );
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.GetPrintedPayments.Error",m_iClientId), p_objEx);//psharma206
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
			}
			// get claim status code id
			sSQL.Append("SELECT CODES.CODE_ID");
			sSQL.Append(" FROM CODES, GLOSSARY");
			sSQL.Append(" WHERE CODES.TABLE_ID = GLOSSARY.TABLE_ID");
			sSQL.Append(" AND CODES.SHORT_CODE = 'P'");
			sSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'CHECK_STATUS'");
				
			try
			{
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
				if (objReader.Read())
				{
                    iClaimStatusCode = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
				}
                sSQL = sSQL.Remove(0, sSQL.Length );
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.GetPrintedPayments.Error",m_iClientId), p_objEx);//psharma206
			}
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }

			//get Non Supplemental payments
            sSQL.Append("SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.FIRST_NAME, FUNDS.LAST_NAME, FUNDS.AMOUNT NET, FUNDS_TRANS_SPLIT.AMOUNT GROSS,  FUNDS_TRANS_SPLIT.FROM_DATE, FUNDS_TRANS_SPLIT.TO_DATE,");
            sSQL.Append(" FUNDS.BATCH_NUMBER, FUNDS.AUTO_CHECK_DETAIL, FUNDS.DATE_OF_CHECK, FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE, CODES_TEXT.CODE_DESC, FUNDS.ACCOUNT_ID,");
            sSQL.Append(" FUNDS.NUM_OF_PAID_DAYS, FUNDS.CTL_NUMBER, FUNDS.VOID_FLAG, FUNDS_TRANS_SPLIT.SUPP_PAYMENT_FLAG ");
			sSQL.Append(" FROM FUNDS INNER JOIN FUNDS_TRANS_SPLIT");
			sSQL.Append(" ON FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");
            sSQL.Append(" INNER JOIN CODES_TEXT");
            sSQL.Append(" ON FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = CODES_TEXT.CODE_ID");
			sSQL.Append(" WHERE CLAIM_ID = " + p_iClaimId);
			sSQL.Append(" AND FUNDS.PAYEE_EID NOT IN (" + arrTaxes[1] + ", " + arrTaxes[4] + ", " + arrTaxes[7] + ", " + arrTaxes[10] + ")");
            sSQL.Append(" AND (FUNDS.SUPP_PAYMENT_FLAG IS NULL OR FUNDS.SUPP_PAYMENT_FLAG = 0)");
			sSQL.Append(" AND FUNDS.STATUS_CODE = " + iClaimStatusCode);
            sSQL.Append(" ORDER BY FUNDS.BATCH_NUMBER, FUNDS.CTL_NUMBER, AUTO_CHECK_DETAIL, FUNDS.TRANS_ID, FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, FUNDS_TRANS_SPLIT.SUPP_PAYMENT_FLAG");
			
			try
			{
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
                while (objReader.Read())
                {
                    bZeroRecords = false;
                    //parent record in FUNDS is gross payment to recipient.  child records in FUNDS_TRANS are net payment to recipient and each tax payment
                    iBatchIdLocal = Conversion.ConvertObjToInt(objReader.GetValue("BATCH_NUMBER"), m_iClientId);
                    sPaymentNum = Conversion.ConvertObjToStr(objReader.GetValue("AUTO_CHECK_DETAIL"));
                    sCTLNumber = Conversion.ConvertObjToStr(objReader.GetValue("CTL_NUMBER"));
                    if ((sPaymentNum != sPaymentNumCheck) || (iBatchIdLocal != iBatchIdCheck) || (sCTLNumber != sCTLNumCheck))
                    {
                        //first paymentnumber is the payment to the recipient:  gross from FUNDS, net from FUNDS_TRANS_SPLIT
                        sPaymentNumCheck = sPaymentNum;
                        iBatchIdCheck = iBatchIdLocal;
                        sCTLNumCheck = sCTLNumber;

                        objRow = objPrintedPayments.CreateElement("row");
                        objDocumentElement.AppendChild(objRow);
                        objRow.SetAttribute("class", "rowlight");
                        objRow.SetAttribute("batchid", iBatchIdLocal.ToString());
                        objRow.SetAttribute("paymentnumber", sPaymentNum);
                        sAccountName = GetAccountName(Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), m_iClientId));
                        if (Conversion.ConvertObjToBool(objReader.GetValue("VOID_FLAG"), m_iClientId))
                            objRow.SetAttribute("void", "True");
                        else
                            objRow.SetAttribute("void", "False");

                        objRow.SetAttribute("ctl", Conversion.ConvertObjToStr(objReader.GetValue("CTL_NUMBER")));
                        objRow.SetAttribute("transtype", Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
                        objRow.SetAttribute("fromdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("FROM_DATE")), "d"));
                        objRow.SetAttribute("todate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("TO_DATE")), "d"));
                        objRow.SetAttribute("printdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("DATE_OF_CHECK")), "d"));
                        objRow.SetAttribute("gross", string.Format("{0:0.00}", Conversion.ConvertObjToDouble(objReader.GetValue("GROSS"), m_iClientId)));//FUNDS.AMOUNT gross payment
                        objRow.SetAttribute("net", string.Format("{0:0.00}", Conversion.ConvertObjToDouble(objReader.GetValue("NET"), m_iClientId)));//FUNDS_TRANS_SPLIT.AMOUNT net amount
                        objRow.SetAttribute("numofpayments", Conversion.ConvertObjToStr(objReader.GetValue("NUM_OF_PAID_DAYS")));
                        objRow.SetAttribute("account", sAccountName);
                        objRow.SetAttribute("fed", string.Format("{0:0.00}", 0));//note, these can be overwritten in Else block below
                        objRow.SetAttribute("ss", string.Format("{0:0.00}", 0));
                        objRow.SetAttribute("med", string.Format("{0:0.00}", 0));
                        objRow.SetAttribute("state", string.Format("{0:0.00}", 0));
                        objRow.SetAttribute("pension", "0.00");
                        objRow.SetAttribute("ssoffset", "0.00");
                        objRow.SetAttribute("oi", "0.00");
                        //Added by Ravneet Kaur
                        objRow.SetAttribute("othoffset1", "0.00");
                        objRow.SetAttribute("othoffset2", "0.00");
                        objRow.SetAttribute("othoffset3", "0.00");
                        objRow.SetAttribute("psttaxded1", "0.00");
                        objRow.SetAttribute("psttaxded2", "0.00");
                        /////////////////////////////////
                        objRow.SetAttribute("supptranstypecode", "");
                        objRow.SetAttribute("supppayment", "0.00");
                        //Animesh Modified for GHS Enhancement  //pmittal5 Mits 14841
                        //PJS, 05-06-08, MITS 12172 - Changes made same as MITS 11879
                        //bCheckEnter = new bool[7] { true, true, true, true, true, true, true };
                        bCheckEnter = new bool[12] { true, true, true, true, true, true, true, true, true, true, true, true };
                        //Animesh Insertion Ends
                        bNotEntered = true;
                        sTax = string.Empty;
                        //PJS, 05-06-08, MITS 12172 - Changes made same as MITS 11879
                    }
                    else
                    {
                        //other paymentnumbers are the tax payments (FUNDS_TRANS_SPLIT.AMOUNT)
                        iTransTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId);
                        dblAmt = Conversion.ConvertObjToDouble(objReader.GetValue("GROSS"), m_iClientId);
                        if (Conversion.ConvertObjToBool(objReader.GetValue("SUPP_PAYMENT_FLAG"), m_iClientId) == true)
                        {
                            objRow.SetAttribute("supppayment", dblAmt.ToString());
                            m_objCache = new LocalCache(m_sConnectionString, m_iClientId);
                            objRow.SetAttribute("supptranstypecode", m_objCache.GetCodeDesc(iTransTypeCode));
                        }

                        //PJS, 05-06-08, MITS 12172 - Changes made same as MITS 11879
                        if (iTransTypeCode == Conversion.ConvertStrToInteger(arrTaxes[2]))	//iFedTransTypeCodeID
                        {
                            if (bCheckEnter[0] && bNotEntered)
                            {
                                bCheckEnter[0] = false;
                                bNotEntered = false;
                                sTax = "fed";
                            }
                        }
                        if (iTransTypeCode == Conversion.ConvertStrToInteger(arrTaxes[8]))	//iSSTransTypeCodeID
                        {
                            if (bCheckEnter[1] && bNotEntered)
                            {
                                bCheckEnter[1] = false;
                                bNotEntered = false;
                                sTax = "ss";
                            }
                        }

                        if (iTransTypeCode == Conversion.ConvertStrToInteger(arrTaxes[5]))	//iMedicareTransTypeCodeID
                        {
                            if (bCheckEnter[2] && bNotEntered)
                            {
                                bCheckEnter[2] = false;
                                bNotEntered = false;
                                sTax = "med";
                            }
                        }

                        if (iTransTypeCode == Conversion.ConvertStrToInteger(arrTaxes[11]))	//iStateTransTypeCodeID
                        {
                            if (bCheckEnter[3] && bNotEntered)
                            {
                                bCheckEnter[3] = false;
                                bNotEntered = false;
                                sTax = "state";
                            }
                        }

                        // Commented right now, Might be there or changed

                        if (iTransTypeCode == lPensionOffTransTypeCodeID)
                        {
                            if (bCheckEnter[4] && bNotEntered)
                            {
                                bCheckEnter[4] = false;
                                bNotEntered = false;
                                sTax = "pension";
                            }
                        }

                        if (iTransTypeCode == lSSOffTransTypeCodeID)
                        {
                            if (bCheckEnter[5] && bNotEntered)
                            {
                                bCheckEnter[5] = false;
                                bNotEntered = false;
                                sTax = "ssoffset";
                            }
                        }

                        if (iTransTypeCode == lOtherOffTransTypeCodeID)
                        {
                            if (bCheckEnter[6] && bNotEntered)
                            {
                                bCheckEnter[6] = false;
                                bNotEntered = false;
                                sTax = "oi";
                            }
                        }
                        //Added by Ravneet Kaur
                        if (iTransTypeCode == lOthOff1TransTypeCodeID)
                        {
                            if (bCheckEnter[7] && bNotEntered)
                            {
                                bCheckEnter[7] = false;
                                bNotEntered = false;
                                sTax = "othoffset1";
                            }
                        }

                        if (iTransTypeCode == lOthOff2TransTypeCodeID)
                        {
                            if (bCheckEnter[8] && bNotEntered)
                            {
                                bCheckEnter[8] = false;
                                bNotEntered = false;
                                sTax = "othoffset2";
                            }
                        }
                        if (iTransTypeCode == lOthOff3TransTypeCodeID )
                        {
                            if (bCheckEnter[9] && bNotEntered)
                            {
                                bCheckEnter[9] = false;
                                bNotEntered = false;
                                sTax = "othoffset3";
                            }
                        }
                        if (iTransTypeCode == lPstTax1TransTypeCodeID)
                        {
                            if (bCheckEnter[10] && bNotEntered)
                            {
                                bCheckEnter[10] = false;
                                bNotEntered = false;
                                sTax = "psttaxded1";
                            }
                        }
                        if (iTransTypeCode == lPstTax2TransTypeCodeID)
                        {
                            if (bCheckEnter[11] && bNotEntered)
                            {
                                bCheckEnter[11] = false;
                                bNotEntered = false;
                                sTax = "psttaxded2";
                            }
                        }
                        ///////////////////


                        //Commented by Shivendu for MITS 12111
                        //objRow.SetAttribute("pension", "0.00");
                        //objRow.SetAttribute("ssoffset", "0.00");
                        //objRow.SetAttribute("oi", "0.00");

                        if (sTax.Trim() != string.Empty)
                            objRow.SetAttribute(sTax, string.Format("{0:0.00}", dblAmt));
                    }
                    //PJS, 05-06-08, MITS 12172 - Changes made same as MITS 11879
                    bNotEntered = true;
                    sTax = string.Empty;
                    //PJS, 05-06-08, MITS 12172 - Changes made same as MITS 11879
                 

                }
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.GetPrintedPayments.Error",m_iClientId), p_objEx);//psharma206
			}
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            sSQL = sSQL.Remove(0, sSQL.Length );
            //get Supplemental payments
            sSQL.Append("SELECT FUNDS.TRANS_ID, FUNDS.FIRST_NAME, FUNDS.LAST_NAME, FUNDS.AMOUNT NET, FUNDS_TRANS_SPLIT.AMOUNT GROSS,  FUNDS_TRANS_SPLIT.FROM_DATE, FUNDS_TRANS_SPLIT.TO_DATE,");
            sSQL.Append(" FUNDS.BATCH_NUMBER, FUNDS.AUTO_CHECK_DETAIL, FUNDS.DATE_OF_CHECK, FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE, CODES_TEXT.CODE_DESC, FUNDS.ACCOUNT_ID,");
            sSQL.Append(" FUNDS.NUM_OF_PAID_DAYS, FUNDS.CTL_NUMBER, FUNDS.VOID_FLAG, FUNDS_TRANS_SPLIT.SUPP_PAYMENT_FLAG ");
            sSQL.Append(" FROM FUNDS INNER JOIN FUNDS_TRANS_SPLIT");
            sSQL.Append(" ON FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");
            sSQL.Append(" INNER JOIN CODES_TEXT");
            sSQL.Append(" ON FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = CODES_TEXT.CODE_ID");
            sSQL.Append(" WHERE CLAIM_ID = " + p_iClaimId);
            sSQL.Append(" AND FUNDS.PAYEE_EID NOT IN (" + arrTaxes[1] + ", " + arrTaxes[4] + ", " + arrTaxes[7] + ", " + arrTaxes[10] + ")");
            sSQL.Append(" AND FUNDS.SUPP_PAYMENT_FLAG = -1");
            sSQL.Append(" AND FUNDS.STATUS_CODE = " + iClaimStatusCode);
            sSQL.Append(" ORDER BY FUNDS.BATCH_NUMBER, FUNDS.CTL_NUMBER, AUTO_CHECK_DETAIL, FUNDS.TRANS_ID, FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, FUNDS_TRANS_SPLIT.SUPP_PAYMENT_FLAG");

            try
            {
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
                while (objReader.Read())
                {
                    bZeroRecords = false;
                    //parent record in FUNDS is gross payment to recipient.  child records in FUNDS_TRANS are net payment to recipient and each tax payment
                    iBatchIdLocal = Conversion.ConvertObjToInt(objReader.GetValue("BATCH_NUMBER"), m_iClientId);
                    sPaymentNum = Conversion.ConvertObjToStr(objReader.GetValue("AUTO_CHECK_DETAIL"));
                    if ((sPaymentNum != sPaymentNumCheck) || (iBatchIdLocal != iBatchIdCheck))
                    {
                        //first paymentnumber is the payment to the recipient:  gross from FUNDS, net from FUNDS_TRANS_SPLIT
                        sPaymentNumCheck = sPaymentNum;
                        iBatchIdCheck = iBatchIdLocal;

                        objRow = objPrintedPayments.CreateElement("row");
                        objDocumentElement.AppendChild(objRow);
                        objRow.SetAttribute("class", "rowlight");
                        objRow.SetAttribute("batchid", iBatchIdLocal.ToString());
                        objRow.SetAttribute("paymentnumber", sPaymentNum);
                        sAccountName = GetAccountName(Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), m_iClientId));
                        if (Conversion.ConvertObjToBool(objReader.GetValue("VOID_FLAG"), m_iClientId))
                            objRow.SetAttribute("void", "True");
                        else
                            objRow.SetAttribute("void", "False");

                        objRow.SetAttribute("ctl", Conversion.ConvertObjToStr(objReader.GetValue("CTL_NUMBER")));
                        objRow.SetAttribute("transtype", Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
                        objRow.SetAttribute("fromdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("FROM_DATE")), "d"));
                        objRow.SetAttribute("todate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("TO_DATE")), "d"));
                        objRow.SetAttribute("printdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("DATE_OF_CHECK")), "d"));
                        objRow.SetAttribute("gross", string.Format("{0:0.00}", Conversion.ConvertObjToDouble(objReader.GetValue("GROSS"), m_iClientId)));//FUNDS.AMOUNT gross payment
                        objRow.SetAttribute("net", string.Format("{0:0.00}", Conversion.ConvertObjToDouble(objReader.GetValue("NET"), m_iClientId)));//FUNDS_TRANS_SPLIT.AMOUNT net amount
                        objRow.SetAttribute("numofpayments", Conversion.ConvertObjToStr(objReader.GetValue("NUM_OF_PAID_DAYS")));
                        objRow.SetAttribute("account", sAccountName);
                        objRow.SetAttribute("fed", string.Format("{0:0.00}", 0));//note, these can be overwritten in Else block below
                        objRow.SetAttribute("ss", string.Format("{0:0.00}", 0));
                        objRow.SetAttribute("med", string.Format("{0:0.00}", 0));
                        objRow.SetAttribute("state", string.Format("{0:0.00}", 0));
                        objRow.SetAttribute("pension", "0.00");
                        objRow.SetAttribute("ssoffset", "0.00");
                        objRow.SetAttribute("oi", "0.00");
                        //Added by Ravneet Kaur
                        objRow.SetAttribute("othoffset1", "0.00");
                        objRow.SetAttribute("othoffset2", "0.00");
                        objRow.SetAttribute("othoffset3", "0.00");
                        objRow.SetAttribute("psttaxded1", "0.00");
                        objRow.SetAttribute("psttaxded2", "0.00");
                        ///////////////////////////////
                        objRow.SetAttribute("supptranstypecode", "");
                        objRow.SetAttribute("supppayment", "0.00");
                        //Animesh Modified For GHS Enhancement  //pmittal5 Mits 14841
                        //PJS, 05-06-08, MITS 12172 - Changes made same as MITS 11879
                        //bCheckEnter = new bool[7] { true, true, true, true, true, true, true };
                        bCheckEnter = new bool[12] { true, true, true, true, true, true, true, true, true, true, true, true };
                        //Animesh Modification Ends
                        bNotEntered = true;
                        sTax = string.Empty;
                        //PJS, 05-06-08, MITS 12172 - Changes made same as MITS 11879
                    }
                    else
                    {
                        iTransTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId);
                        dblAmt = Conversion.ConvertObjToDouble(objReader.GetValue("GROSS"), m_iClientId);
                        if (Conversion.ConvertObjToBool(objReader.GetValue("SUPP_PAYMENT_FLAG"), m_iClientId) == true)
                        {
                            //objRow.SetAttribute("supppayment", dblAmt.ToString());
                            if (m_objCache == null)  //Mits 15804
                                m_objCache = new LocalCache(m_sConnectionString, m_iClientId); 
                            objRow.SetAttribute("supptranstypecode", m_objCache.GetCodeDesc(iTransTypeCode));
                        }
                        //PJS, 05-06-08, MITS 12172 - Changes made same as MITS 11879
                        if (iTransTypeCode == Conversion.ConvertStrToInteger(arrTaxes[2]))	//iFedTransTypeCodeID
                        {
                            if (bCheckEnter[0] && bNotEntered)
                            {
                                bCheckEnter[0] = false;
                                bNotEntered = false;
                                sTax = "fed";
                            }
                        }
                        if (iTransTypeCode == Conversion.ConvertStrToInteger(arrTaxes[8]))	//iSSTransTypeCodeID
                        {
                            if (bCheckEnter[1] && bNotEntered)
                            {
                                bCheckEnter[1] = false;
                                bNotEntered = false;
                                sTax = "ss";
                            }
                        }

                        if (iTransTypeCode == Conversion.ConvertStrToInteger(arrTaxes[5]))	//iMedicareTransTypeCodeID
                        {
                            if (bCheckEnter[2] && bNotEntered)
                            {
                                bCheckEnter[2] = false;
                                bNotEntered = false;
                                sTax = "med";
                            }
                        }

                        if (iTransTypeCode == Conversion.ConvertStrToInteger(arrTaxes[11]))	//iStateTransTypeCodeID
                        {
                            if (bCheckEnter[3] && bNotEntered)
                            {
                                bCheckEnter[3] = false;
                                bNotEntered = false;
                                sTax = "state";
                            }
                        }

                        // Commented right now, Might be there or changed

                        if (iTransTypeCode == lPensionOffTransTypeCodeID)
                        {
                            if (bCheckEnter[4] && bNotEntered)
                            {
                                bCheckEnter[4] = false;
                                bNotEntered = false;
                                sTax = "pension";
                            }
                        }

                        if (iTransTypeCode == lSSOffTransTypeCodeID)
                        {
                            if (bCheckEnter[5] && bNotEntered)
                            {
                                bCheckEnter[5] = false;
                                bNotEntered = false;
                                sTax = "ssoffset";
                            }
                        }

                        if (iTransTypeCode == lOtherOffTransTypeCodeID)
                        {
                            if (bCheckEnter[6] && bNotEntered)
                            {
                                bCheckEnter[6] = false;
                                bNotEntered = false;
                                sTax = "oi";
                            }
                        }
                        //Added by Ravneet Kaur
                        if (iTransTypeCode == lOthOff1TransTypeCodeID )
                        {
                            if (bCheckEnter[7] && bNotEntered)
                            {
                                bCheckEnter[7] = false;
                                bNotEntered = false;
                                sTax = "othoffset1";
                            }
                        }

                        if (iTransTypeCode == lOthOff2TransTypeCodeID )
                        {
                            if (bCheckEnter[8] && bNotEntered)
                            {
                                bCheckEnter[8] = false;
                                bNotEntered = false;
                                sTax = "othoffset2";
                            }
                        }
                        if (iTransTypeCode == lOthOff3TransTypeCodeID )
                        {
                            if (bCheckEnter[9] && bNotEntered)
                            {
                                bCheckEnter[9] = false;
                                bNotEntered = false;
                                sTax = "othoffset3";
                            }
                        }
                        if (iTransTypeCode == lPstTax1TransTypeCodeID)
                        {
                            if (bCheckEnter[10] && bNotEntered)
                            {
                                bCheckEnter[10] = false;
                                bNotEntered = false;
                                sTax = "psttaxded1";
                            }
                        }
                        if (iTransTypeCode == lPstTax2TransTypeCodeID)
                        {
                            if (bCheckEnter[11] && bNotEntered)
                            {
                                bCheckEnter[11] = false;
                                bNotEntered = false;
                                sTax = "psttaxded2";
                            }
                        }
                        ///////////////////

                        if (sTax.Trim() != string.Empty)
                            objRow.SetAttribute(sTax, string.Format("{0:0.00}", dblAmt));
                    }
                    //pmittal5 Mits 15804
                    bNotEntered = true;
                    sTax = string.Empty;                   
                }
                objDocumentElement.SetAttribute("zerorecords", bZeroRecords.ToString());
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.GetPrintedPayments.Error",m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
			return objPrintedPayments;
		}


		public XmlDocument GetFuturePayments(int p_iClaimId, string p_sTaxArray, bool p_bBatchRestrict, 
											 int p_iBatchId)
		{
            StringBuilder sSQL = new StringBuilder();
			string[] arrTaxes=p_sTaxArray.Split(new char[] {','});
			DbReader objReader=null;
			int iBatchIdLocal=0;
			int iBatchIdCheck=0;
			string sPaymentNum=string.Empty;
			string sPaymentNumCheck=string.Empty;
			bool bTemp=false;
			bool bZeroRecords=true;
            //Animesh Modified for GHS Enhancment  //pmittal5 Mits 14841
            //Start by Shivendu for MITS 11879
            //bool[] bCheckEnter = new bool[7] { true, true, true, true, true, true, true };
            bool[] bCheckEnter = new bool[12] { true, true, true, true, true, true, true, true, true, true, true, true};
            //Animesh Insertion Ends
            bool bNotEntered = true;
            //End by Shivendu for MITS 11879
			int iTransTypeCode=0;
			double dblAmt=0;
			string sTax=string.Empty;
            string sAccountName = string.Empty;

			XmlDocument objFuturePayments=null;
			XmlElement objDocumentElement=null;
			XmlElement objBatchRow=null;
			XmlElement objRow=null;
            long lPensionOffTransTypeCodeID = 0;
            long lSSOffTransTypeCodeID = 0;
            long lOtherOffTransTypeCodeID = 0;
            //Added by RKaur
            long lOthOff1TransTypeCodeID = 0;
            long lOthOff2TransTypeCodeID = 0;
            long lOthOff3TransTypeCodeID = 0;
            long lPstTax1TransTypeCodeID = 0;
            long lPstTax2TransTypeCodeID = 0;
            /////////////////////////////
            int iAutoSplitId = 0;
            int iTag = 0;    //pmittal5  MITS:12620  07/17/08
             

            //Raman Bhatia: LTD changes: Get the Transaction Types for Offsets

            sSQL.Append("SELECT * FROM OFFSET_MAPPING");
            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
				//loop and get OFFSET trans type code
            while (objReader.Read())
            {
                if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "PENSION")
                    lPensionOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                 if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "SOCIAL_SECURITY")
                     lSSOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                 if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHER_INCOME")
                     lOtherOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                 //Added by RKaur
                 if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET1")
                     lOthOff1TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                 if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET2")
                     lOthOff2TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                 if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET3")
                     lOthOff3TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                 if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "POST_TAX_DED1")
                     lPstTax1TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                 if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "POST_TAX_DED2")
                     lPstTax2TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
            }
            objReader.Close();
            sSQL.Remove(0, sSQL.Length);

			objFuturePayments=new XmlDocument();
			objDocumentElement=objFuturePayments.CreateElement("table");
            //pmittal5
            sSQL.Append("SELECT PAYMNT_FROZEN_FLAG FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId);
            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
            if (objReader.Read())
                objDocumentElement.SetAttribute("paymntfrozenflag", Conversion.ConvertObjToStr(objReader.GetBoolean("PAYMNT_FROZEN_FLAG")));
            objReader.Close();
            sSQL.Remove(0,sSQL.Length);

			objDocumentElement.SetAttribute("syscmd",SysCmd_DisplayFuture.ToString());
			objFuturePayments.AppendChild(objDocumentElement);

			//header row
			objRow=objFuturePayments.CreateElement("row");
			objDocumentElement.AppendChild(objRow);
			objRow.SetAttribute("class", "colheader");	
			objRow.SetAttribute("header", "1");
			objRow.SetAttribute("batchid", "Batch #");
			objRow.SetAttribute("paymentnumber", "Payment #");
			objRow.SetAttribute("transtype", "Trans Type");
			objRow.SetAttribute("fromdate", "From Date");
			objRow.SetAttribute("todate", "To Date");
			objRow.SetAttribute("printdate", "Print Date");
			objRow.SetAttribute("gross", "Gross Payment");
            objRow.SetAttribute("supppayment", "Gross Supp Payment");
			objRow.SetAttribute("net", "Net Payment");
			objRow.SetAttribute("numofpayments", "Days Paid");
			objRow.SetAttribute("fed", "Federal Tax");
			objRow.SetAttribute("ss", "Social Security Tax");
			objRow.SetAttribute("med", "Medicare Tax");
			objRow.SetAttribute("state", "State Tax");
            objRow.SetAttribute("pension", "Pension Offset");
            objRow.SetAttribute("ssoffset", "SS Offset");
            objRow.SetAttribute("oi", "Other Offset");
            //Added by RKaur
            objRow.SetAttribute("othoffset1", "Other Offset #1");
            objRow.SetAttribute("othoffset2", "Other Offset #2");
            objRow.SetAttribute("othoffset3", "Other Offset #3");
            objRow.SetAttribute("psttaxded1", "Post Tax Deduction #1");
            objRow.SetAttribute("psttaxded2", "Post Tax Deduction #2");
            ///////
			objRow.SetAttribute("account", "Account Name");

            sSQL.Append("SELECT FUNDS_AUTO.AUTO_TRANS_ID, FUNDS_AUTO.FIRST_NAME, FUNDS_AUTO.LAST_NAME, FUNDS_AUTO.AMOUNT NET,FUNDS_AUTO_SPLIT.AMOUNT GROSS,FUNDS_AUTO_SPLIT.FROM_DATE, FUNDS_AUTO_SPLIT.TO_DATE,FUNDS_AUTO.CLAIM_NUMBER, ");
			sSQL.Append(" FUNDS_AUTO.AUTO_BATCH_ID, FUNDS_AUTO.PAY_NUMBER, FUNDS_AUTO.PRINT_DATE, FUNDS_AUTO_SPLIT.TAG, FUNDS_AUTO_SPLIT.AUTO_SPLIT_ID, FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE, FUNDS_AUTO.ACCOUNT_ID, ");
            sSQL.Append(" FUNDS_AUTO.NUM_OF_PAID_DAYS,CODES_TEXT.CODE_DESC, FUNDS_AUTO_BATCH.START_DATE,FUNDS_AUTO_BATCH.END_DATE, FUNDS_AUTO_SPLIT.SUPP_PAYMENT_FLAG");
			sSQL.Append(" FROM FUNDS_AUTO INNER JOIN FUNDS_AUTO_BATCH");
            sSQL.Append(" ON FUNDS_AUTO.AUTO_BATCH_ID=FUNDS_AUTO_BATCH.AUTO_BATCH_ID");
            sSQL.Append(" INNER JOIN FUNDS_AUTO_SPLIT");
            sSQL.Append(" ON FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID");
            sSQL.Append(" INNER JOIN CODES_TEXT");
            sSQL.Append(" ON FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE = CODES_TEXT.CODE_ID");
			sSQL.Append(" WHERE FUNDS_AUTO.CLAIM_ID = " + p_iClaimId.ToString());
            sSQL.Append(" AND (FUNDS_AUTO.SUPP_PAYMENT_FLAG IS NULL OR FUNDS_AUTO.SUPP_PAYMENT_FLAG = 0)");
			sSQL.Append(" AND FUNDS_AUTO.PAYEE_EID NOT IN (" + arrTaxes[1] + ", " + arrTaxes[4] + ", " + arrTaxes[7] + ", " + arrTaxes[10] + ")");

			//if user calculated payments, restrict recordset to this new batch
			if (p_bBatchRestrict)
			{
				if (p_iBatchId!=0)
					sSQL.Append(" AND FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString());
			}

			sSQL.Append(" ORDER BY FUNDS_AUTO.AUTO_BATCH_ID, PAY_NUMBER, FUNDS_AUTO.AUTO_TRANS_ID, FUNDS_AUTO_SPLIT.AUTO_SPLIT_ID");
            int iCount = 0;
			try
			{
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
				//loop and get payments
				while (objReader.Read())
				{
                    iCount++;
                    if (bTemp==false)
					{
						bTemp=true;
						//get claimant, claim number, payments beginning, payments through
						objDocumentElement.SetAttribute("claimant",Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"))+ " " + Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")));
						objDocumentElement.SetAttribute("claimnumber",Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER")));
					}
					bZeroRecords=false;
                    iBatchIdLocal = Conversion.ConvertObjToInt(objReader.GetValue("AUTO_BATCH_ID"), m_iClientId);
					sPaymentNum = Conversion.ConvertObjToStr(objReader.GetValue("PAY_NUMBER"));
                    sAccountName = GetAccountName(Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), m_iClientId));
                    iAutoSplitId = Conversion.ConvertObjToInt(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId);
					if ((sPaymentNum != sPaymentNumCheck) || (iBatchIdLocal != iBatchIdCheck))
					{
                        // Part of this not in RM World
						if (p_bBatchRestrict && p_iBatchId!=0)
						{
							objDocumentElement.SetAttribute("batchrestrict", "true");
							objDocumentElement.SetAttribute("batchid", iBatchIdLocal.ToString());
							objDocumentElement.SetAttribute("bencalcpaystart", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("START_DATE")),"d"));
							objDocumentElement.SetAttribute("bencalcpayto",Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("END_DATE")),"d"));
							objDocumentElement.SetAttribute("claimid", p_iClaimId.ToString());
							objDocumentElement.SetAttribute("account",sAccountName);
							objDocumentElement.SetAttribute("transtype",Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
						}
						else
						{
							//xsl makes this row a batch separator
							if (iBatchIdLocal != iBatchIdCheck)
							{
								objBatchRow=objFuturePayments.CreateElement("row");
								objDocumentElement.AppendChild(objBatchRow);
								objBatchRow.SetAttribute("batchid", iBatchIdLocal.ToString());
								objBatchRow.SetAttribute("bencalcpaystart", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("START_DATE")),"d"));
								objBatchRow.SetAttribute("bencalcpayto",Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("END_DATE")),"d"));
								objBatchRow.SetAttribute("account",sAccountName);
								objBatchRow.SetAttribute("transtype",Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
							}
						}

						//first paymentnumber is the payment to the recipient:  gross from FUNDS_AUTO_SPLIT, net from FUNDS_AUTO
						sPaymentNumCheck = sPaymentNum;
						iBatchIdCheck = iBatchIdLocal;
						objRow=objFuturePayments.CreateElement("row");
						objDocumentElement.AppendChild(objRow);
						objRow.SetAttribute("class", "rowlight");
						objRow.SetAttribute("batchid", iBatchIdLocal.ToString());
						objRow.SetAttribute("paymentnumber", sPaymentNum);
						objRow.SetAttribute("fromdate",Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("FROM_DATE")),"d"));
						objRow.SetAttribute("todate",Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("TO_DATE")),"d"));
						objRow.SetAttribute("printdate",Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("PRINT_DATE")),"d"));
                        objRow.SetAttribute("gross", string.Format("{0:0.00}", Conversion.ConvertObjToDouble(objReader.GetValue("GROSS"), m_iClientId)));//FUNDS.AMOUNT gross payment
                        objRow.SetAttribute("net", string.Format("{0:0.00}", Conversion.ConvertObjToDouble(objReader.GetValue("NET"), m_iClientId)));//FUNDS_TRANS_SPLIT.AMOUNT net amount
						objRow.SetAttribute("numofpayments",Conversion.ConvertObjToStr(objReader.GetValue("NUM_OF_PAID_DAYS")));
						objRow.SetAttribute("fed",string.Format("{0:0.00}",0));//note, these can be overwritten in Else block below
						objRow.SetAttribute("ss",string.Format("{0:0.00}",0));
						objRow.SetAttribute("med",string.Format("{0:0.00}",0)); 
						objRow.SetAttribute("state",string.Format("{0:0.00}",0));
                        objRow.SetAttribute("pension", "0.00");
                        objRow.SetAttribute("ssoffset", "0.00");
                        objRow.SetAttribute("oi", "0.00");
                        //Added by Ravneet Kaur
                        objRow.SetAttribute("othoffset1", "0.00");
                        objRow.SetAttribute("othoffset2", "0.00");
                        objRow.SetAttribute("othoffset3", "0.00");
                        objRow.SetAttribute("psttaxded1", "0.00");
                        objRow.SetAttribute("psttaxded2", "0.00");
                        ///////////////////////////
                        objRow.SetAttribute("supppayment", "0.00");
                        //Animesh Modified For GHS Enhancment  //pmittal5 Mits 14841
                        //Start by Shivendu for MITS 11879
                        //bCheckEnter = new bool[7] { true, true, true, true, true, true, true };
                        bCheckEnter = new bool[12] { true, true, true, true, true, true, true, true, true, true, true, true };
                        //Animesh Modification ends
                        bNotEntered = true;
                        sTax = string.Empty;
                        //End by Shivendu for MITS 11879
                        
					}
					else
					{
                       //pmittal5  MITS:12620  07/17/08
                        iTag = Conversion.ConvertObjToInt(objReader.GetValue("TAG"), m_iClientId);
                        iTransTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId);
                        dblAmt = Conversion.ConvertObjToDouble(objReader.GetValue("GROSS"), m_iClientId);
                        if (Conversion.ConvertObjToBool(objReader.GetValue("SUPP_PAYMENT_FLAG"), m_iClientId) == true)
                       {
                           objRow.SetAttribute("supppayment", dblAmt.ToString());
                       }
                       /*
                       else
                       {
                           objRow.SetAttribute("supppayment", "0.00");
                       }
                        */
                        //if containing bool variables added by Shivendu for MITS 11879

                       //pmittal5  MITS:12620  07/17/08
                       //if (iTransTypeCode == Conversion.ConvertStrToInteger(arrTaxes[2]))	//iFedTransTypeCodeID
                        if(iTag == 1)
                       {
                           if (bCheckEnter[0] && bNotEntered)
                           {
                               bCheckEnter[0] = false;
                               bNotEntered = false;
                               sTax = "fed";
                           }
                       }
                        //pmittal5  MITS:12620  07/17/08
						//if (iTransTypeCode==Conversion.ConvertStrToInteger(arrTaxes[8]))	//iSSTransTypeCodeID
                        if(iTag == 2)
                        {
                            if (bCheckEnter[1] && bNotEntered)
                            {
                                bCheckEnter[1] = false;
                                bNotEntered = false;
                                sTax = "ss";
                            }
                        }

                        //pmittal5  MITS:12620  07/17/08
						//if (iTransTypeCode==Conversion.ConvertStrToInteger(arrTaxes[5]))	//iMedicareTransTypeCodeID
                        if(iTag == 3)
                        {
                            if (bCheckEnter[2] && bNotEntered)
                            {
                                bCheckEnter[2] = false;
                                bNotEntered = false;
                                sTax = "med";
                            }
                        }

                        //pmittal5  MITS:12620  07/17/08
						//if (iTransTypeCode==Conversion.ConvertStrToInteger(arrTaxes[11]))	//iStateTransTypeCodeID
                        if(iTag == 4)
                        {
                            if (bCheckEnter[3] && bNotEntered)
                            {
                                bCheckEnter[3] = false;
                                bNotEntered = false;
                                sTax = "state";
                            }
                        }
                            
                        // Commented right now, Might be there or changed

                        //pmittal5  MITS:12620  07/17/08
                        //if (iTransTypeCode == lPensionOffTransTypeCodeID)
                        if(iTag == 5)       //Pension offset
                        {
                            if (bCheckEnter[4] && bNotEntered)
                            {
                                bCheckEnter[4] = false;
                                bNotEntered = false;
                                sTax = "pension";
                            }
                        }

                        //pmittal5  MITS:12620  07/17/08
                        //if (iTransTypeCode == lSSOffTransTypeCodeID)
                        if(iTag == 6)    //SS Offset
                        {
                            if (bCheckEnter[5] && bNotEntered)
                            {
                                bCheckEnter[5] = false;
                                bNotEntered = false;
                                sTax = "ssoffset";
                            }
                        }

                        //pmittal5  MITS:12620  07/17/08
                        //if (iTransTypeCode == lOtherOffTransTypeCodeID)
                        if(iTag == 7)  //Other Offset
                        {
                            if (bCheckEnter[6] && bNotEntered)
                            {
                                bCheckEnter[6] = false;
                                bNotEntered = false;
                                sTax = "oi";
                            }
                        }

                        //Added by Ravneet Kaur
                        if (iTag == 9)
                        {
                            if (bCheckEnter[7] && bNotEntered)
                            {
                                bCheckEnter[7] = false;
                                bNotEntered = false;
                                sTax = "othoffset1";
                            }
                        }
                        if (iTag == 10)
                        {
                            if (bCheckEnter[8] && bNotEntered)
                            {
                                bCheckEnter[8] = false;
                                bNotEntered = false;
                                sTax = "othoffset2";
                            }
                        }
                        if (iTag == 11)
                        {
                            if (bCheckEnter[9] && bNotEntered)
                            {
                                bCheckEnter[9] = false;
                                bNotEntered = false;
                                sTax = "othoffset3";
                            }
                        }
                        if (iTag == 12)
                        {
                            if (bCheckEnter[10] && bNotEntered)
                            {
                                bCheckEnter[10] = false;
                                bNotEntered = false;
                                sTax = "psttaxded1";
                            }
                        }
                        if (iTag == 13)
                        {
                            if (bCheckEnter[11] && bNotEntered)
                            {
                                bCheckEnter[11] = false;
                                bNotEntered = false;
                                sTax = "psttaxded2";
                            }
                        }
                        ///////////////////
                            
                        //Commented by Shivendu for MITS 12111
                        //objRow.SetAttribute("pension", "0.00");
                        //objRow.SetAttribute("ssoffset", "0.00");
                        //objRow.SetAttribute("oi", "0.00");
                        
                        if (sTax.Trim()!=string.Empty)
							objRow.SetAttribute(sTax,string.Format("{0:0.00}",dblAmt));
					}
                    //Added by Shivendu for MITS 11879
                    bNotEntered = true;
                    sTax = string.Empty;
                    //End by Shivendu for MITS 11879
                    objRow.SetAttribute("AutoSplitId", iAutoSplitId.ToString());

				}
			}
			catch(RMAppException p_objEx)
			{
                iCount = iCount;
                throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.GetFuturePayments.Error",m_iClientId), p_objEx);
			}
			finally
			{

                iCount = iCount;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
			}
            sSQL = sSQL.Remove(0, sSQL.Length );
            //get Supplemental payments
            sSQL.Append("SELECT FUNDS_AUTO.AUTO_TRANS_ID, FUNDS_AUTO.FIRST_NAME, FUNDS_AUTO.LAST_NAME, FUNDS_AUTO.AMOUNT NET,FUNDS_AUTO_SPLIT.AMOUNT GROSS,FUNDS_AUTO_SPLIT.FROM_DATE, FUNDS_AUTO_SPLIT.TO_DATE,FUNDS_AUTO.CLAIM_NUMBER, ");
            sSQL.Append(" FUNDS_AUTO.AUTO_BATCH_ID, FUNDS_AUTO.PAY_NUMBER, FUNDS_AUTO.PRINT_DATE, FUNDS_AUTO_SPLIT.TAG, FUNDS_AUTO_SPLIT.AUTO_SPLIT_ID, FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE, FUNDS_AUTO.ACCOUNT_ID, ");
            sSQL.Append(" FUNDS_AUTO.NUM_OF_PAID_DAYS,CODES_TEXT.CODE_DESC,FUNDS_AUTO_BATCH.START_DATE,FUNDS_AUTO_BATCH.END_DATE, FUNDS_AUTO_SPLIT.SUPP_PAYMENT_FLAG");
            sSQL.Append(" FROM FUNDS_AUTO INNER JOIN FUNDS_AUTO_BATCH");
            sSQL.Append(" ON FUNDS_AUTO.AUTO_BATCH_ID=FUNDS_AUTO_BATCH.AUTO_BATCH_ID");
            sSQL.Append(" INNER JOIN FUNDS_AUTO_SPLIT");
            sSQL.Append(" ON FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID");
            sSQL.Append(" INNER JOIN CODES_TEXT");
            sSQL.Append(" ON FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE = CODES_TEXT.CODE_ID");
            sSQL.Append(" WHERE FUNDS_AUTO.CLAIM_ID = " + p_iClaimId.ToString());
            sSQL.Append(" AND FUNDS_AUTO.SUPP_PAYMENT_FLAG = -1");
            sSQL.Append(" AND FUNDS_AUTO.PAYEE_EID NOT IN (" + arrTaxes[1] + ", " + arrTaxes[4] + ", " + arrTaxes[7] + ", " + arrTaxes[10] + ")");

            //if user calculated payments, restrict recordset to this new batch
            if (p_bBatchRestrict)
            {
                if (p_iBatchId != 0)
                    sSQL.Append(" AND FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString());
            }

            sSQL.Append(" ORDER BY FUNDS_AUTO.AUTO_BATCH_ID, PAY_NUMBER, FUNDS_AUTO.AUTO_TRANS_ID, FUNDS_AUTO_SPLIT.AUTO_SPLIT_ID");

            try
            {
                bool bFirstSuppPaymnt = true; //pmittal5 06/11/09 Mits 16214
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
                //loop and get payments
                while (objReader.Read())
                {
                    if (bTemp == false)
                    {
                        bTemp = true;
                        //get claimant, claim number, payments beginning, payments through
                        objDocumentElement.SetAttribute("claimant", Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")) + " " + Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")));
                        objDocumentElement.SetAttribute("claimnumber", Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER")));
                    }
                    bZeroRecords = false;
                    iBatchIdLocal = Conversion.ConvertObjToInt(objReader.GetValue("AUTO_BATCH_ID"), m_iClientId);
                    sPaymentNum = Conversion.ConvertObjToStr(objReader.GetValue("PAY_NUMBER"));
                    sAccountName = GetAccountName(Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), m_iClientId));
                    iAutoSplitId = Conversion.ConvertObjToInt(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId); //Mits 15804
                    if ((sPaymentNum != sPaymentNumCheck) || (iBatchIdLocal != iBatchIdCheck))
                    {
                        // Part of this not in RM World
                        //Start - pmittal5 06/11/09 Mits 16214 - batch separator in case of Supplement payment
                        //if (p_bBatchRestrict && p_iBatchId != 0)
                        if (p_bBatchRestrict && p_iBatchId != 0 && bFirstSuppPaymnt)
                        {
                            objDocumentElement.SetAttribute("batchrestrict", "true");
                            objDocumentElement.SetAttribute("claimid", p_iClaimId.ToString());

                            //pmittal5 08/24/09 Mits 17676 - In case of only Supplement Payments, table attributes are not present already.
                            if (objDocumentElement.HasAttribute("batchid") && objDocumentElement.HasAttribute("bencalcpaystart") && objDocumentElement.HasAttribute("bencalcpayto") && objDocumentElement.HasAttribute("account") && objDocumentElement.HasAttribute("transtype"))
                            {
                                objBatchRow = objFuturePayments.CreateElement("row");
                                objDocumentElement.AppendChild(objBatchRow);
                                //objDocumentElement.SetAttribute("batchid", iBatchIdLocal.ToString());
                                //objDocumentElement.SetAttribute("bencalcpaystart", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("START_DATE")), "d"));
                                //objDocumentElement.SetAttribute("bencalcpayto", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("END_DATE")), "d"));
                                //objDocumentElement.SetAttribute("account", sAccountName);
                                //objDocumentElement.SetAttribute("transtype", Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
                                objBatchRow.SetAttribute("batchid", iBatchIdLocal.ToString());
                                objBatchRow.SetAttribute("bencalcpaystart", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("START_DATE")), "d"));
                                objBatchRow.SetAttribute("bencalcpayto", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("END_DATE")), "d"));
                                objBatchRow.SetAttribute("account", sAccountName);
                                objBatchRow.SetAttribute("transtype", Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
                                bFirstSuppPaymnt = false;
                            }
                            else //pmittal5 08/24/09 Mits 17676 
                            {
                                objDocumentElement.SetAttribute("batchid", iBatchIdLocal.ToString());
                                objDocumentElement.SetAttribute("bencalcpaystart", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("START_DATE")), "d"));
                                objDocumentElement.SetAttribute("bencalcpayto", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("END_DATE")), "d"));
                                objDocumentElement.SetAttribute("account", sAccountName);
                                objDocumentElement.SetAttribute("transtype", Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
                                bFirstSuppPaymnt = false;
                            }
                            //End - pmittal5
                        }
                        else
                        {
                            //xsl makes this row a batch separator
                            //pmittal5 06/11/09 Mits 16214 - batch separator in case of Supplement payment
                            //if (iBatchIdLocal != iBatchIdCheck) 
                            if (iBatchIdLocal != iBatchIdCheck || bFirstSuppPaymnt)
                            {
                                objBatchRow = objFuturePayments.CreateElement("row");
                                objDocumentElement.AppendChild(objBatchRow);
                                objBatchRow.SetAttribute("batchid", iBatchIdLocal.ToString());
                                objBatchRow.SetAttribute("bencalcpaystart", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("START_DATE")), "d"));
                                objBatchRow.SetAttribute("bencalcpayto", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("END_DATE")), "d"));
                                objBatchRow.SetAttribute("account", sAccountName);
                                objBatchRow.SetAttribute("transtype", Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
                                bFirstSuppPaymnt = false;
                            }
                        }

                        //first paymentnumber is the payment to the recipient:  gross from FUNDS_AUTO_SPLIT, net from FUNDS_AUTO
                        sPaymentNumCheck = sPaymentNum;
                        iBatchIdCheck = iBatchIdLocal;
                        objRow = objFuturePayments.CreateElement("row");
                        objDocumentElement.AppendChild(objRow);
                        objRow.SetAttribute("class", "rowlight");
                        objRow.SetAttribute("batchid", iBatchIdLocal.ToString());
                        objRow.SetAttribute("paymentnumber", sPaymentNum);
                        objRow.SetAttribute("fromdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("FROM_DATE")), "d"));
                        objRow.SetAttribute("todate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("TO_DATE")), "d"));
                        objRow.SetAttribute("printdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("PRINT_DATE")), "d"));
                        objRow.SetAttribute("gross", string.Format("{0:0.00}", Conversion.ConvertObjToDouble(objReader.GetValue("GROSS"), m_iClientId)));//FUNDS.AMOUNT gross payment
                        objRow.SetAttribute("net", string.Format("{0:0.00}", Conversion.ConvertObjToDouble(objReader.GetValue("NET"), m_iClientId)));//FUNDS_TRANS_SPLIT.AMOUNT net amount
                        objRow.SetAttribute("numofpayments", Conversion.ConvertObjToStr(objReader.GetValue("NUM_OF_PAID_DAYS")));
                        objRow.SetAttribute("fed", string.Format("{0:0.00}", 0));//note, these can be overwritten in Else block below
                        objRow.SetAttribute("ss", string.Format("{0:0.00}", 0));
                        objRow.SetAttribute("med", string.Format("{0:0.00}", 0));
                        objRow.SetAttribute("state", string.Format("{0:0.00}", 0));
                        //pmittal5 Mits 15804  04/25/09
                        objRow.SetAttribute("pension", "");
                        objRow.SetAttribute("ssoffset", "");
                        objRow.SetAttribute("oi", "");
                        //Added by Ravneet Kaur
                        objRow.SetAttribute("othoffset1", "");
                        objRow.SetAttribute("othoffset2", "");
                        objRow.SetAttribute("othoffset3", "");
                        objRow.SetAttribute("psttaxded1", "");
                        objRow.SetAttribute("psttaxded2", "");
                        ///////////////////////////
                        objRow.SetAttribute("supppayment", "");
                        //Start by Shivendu for MITS 11879
                        //bCheckEnter = new bool[7] { true, true, true, true, true, true, true };
                        bCheckEnter = new bool[12] { true, true, true, true, true, true, true, true, true, true, true, true };
                    
                        bNotEntered = true;
                        sTax = string.Empty;
                        //End by Shivendu for MITS 11879
                        
                    }
                    else
                    {
                        iTransTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId);
                        dblAmt = Conversion.ConvertObjToDouble(objReader.GetValue("GROSS"), m_iClientId);
                        //if containing bool variables added by Shivendu for MITS 11879
                       if (iTransTypeCode == Conversion.ConvertStrToInteger(arrTaxes[2]))	//iFedTransTypeCodeID
                       {
                           if (bCheckEnter[0] && bNotEntered)
                           {
                               bCheckEnter[0] = false;
                               bNotEntered = false;
                               sTax = "fed";
                           }
                       }
						if (iTransTypeCode==Conversion.ConvertStrToInteger(arrTaxes[8]))	//iSSTransTypeCodeID
                        {
                            if (bCheckEnter[1] && bNotEntered)
                            {
                                bCheckEnter[1] = false;
                                bNotEntered = false;
                                sTax = "ss";
                            }
                        }
                            
						if (iTransTypeCode==Conversion.ConvertStrToInteger(arrTaxes[5]))	//iMedicareTransTypeCodeID
                        {
                            if (bCheckEnter[2] && bNotEntered)
                            {
                                bCheckEnter[2] = false;
                                bNotEntered = false;
                                sTax = "med";
                            }
                        }
                            
						if (iTransTypeCode==Conversion.ConvertStrToInteger(arrTaxes[11]))	//iStateTransTypeCodeID
                        {
                            if (bCheckEnter[3] && bNotEntered)
                            {
                                bCheckEnter[3] = false;
                                bNotEntered = false;
                                sTax = "state";
                            }
                        }
                            
                        // Commented right now, Might be there or changed
                        
                        if (iTransTypeCode == lPensionOffTransTypeCodeID)
                        {
                            if (bCheckEnter[4] && bNotEntered)
                            {
                                bCheckEnter[4] = false;
                                bNotEntered = false;
                                sTax = "pension";
                            }
                        }
                            
                        if (iTransTypeCode == lSSOffTransTypeCodeID)
                        {
                            if (bCheckEnter[5] && bNotEntered)
                            {
                                bCheckEnter[5] = false;
                                bNotEntered = false;
                                sTax = "ssoffset";
                            }
                        }
                            
                        if (iTransTypeCode == lOtherOffTransTypeCodeID)
                        {
                            if (bCheckEnter[6] && bNotEntered)
                            {
                                bCheckEnter[6] = false;
                                bNotEntered = false;
                                sTax = "oi";
                            }
                        }
                        //Added by Ravneet Kaur
                        if (iTransTypeCode == lOthOff1TransTypeCodeID)
                        {
                            if (bCheckEnter[7] && bNotEntered)
                            {
                                bCheckEnter[7] = false;
                                bNotEntered = false;
                                sTax = "othoffset1";
                            }
                        }

                        if (iTransTypeCode == lOthOff2TransTypeCodeID)
                        {
                            if (bCheckEnter[8] && bNotEntered)
                            {
                                bCheckEnter[8] = false;
                                bNotEntered = false;
                                sTax = "othoffset2";
                            }
                        }
                        if (iTransTypeCode == lOthOff3TransTypeCodeID)
                        {
                            if (bCheckEnter[9] && bNotEntered)
                            {
                                bCheckEnter[9] = false;
                                bNotEntered = false;
                                sTax = "othoffset3";
                            }
                        }
                        if (iTransTypeCode == lPstTax1TransTypeCodeID)
                        {
                            if (bCheckEnter[10] && bNotEntered)
                            {
                                bCheckEnter[10] = false;
                                bNotEntered = false;
                                sTax = "psttaxded1";
                            }
                        }
                        if (iTransTypeCode == lPstTax2TransTypeCodeID)
                        {
                            if (bCheckEnter[11] && bNotEntered)
                            {
                                bCheckEnter[11] = false;
                                bNotEntered = false;
                                sTax = "psttaxded2";
                            }
                        }
                        ///////////////////     

                        //Commented by Shivendu for MITS 12111
                        //objRow.SetAttribute("pension", "0.00");
                        //objRow.SetAttribute("ssoffset", "0.00");
                        //objRow.SetAttribute("oi", "0.00");
                        
                        if (sTax.Trim()!=string.Empty)
							objRow.SetAttribute(sTax,string.Format("{0:0.00}",dblAmt));
					}
                    
                    //Added by Shivendu for MITS 11879
                    bNotEntered = true;
                    sTax = string.Empty;
                    //End by Shivendu for MITS 11879
                    objRow.SetAttribute("AutoSplitId", iAutoSplitId.ToString()); //Mits 15804
                    if(bZeroRecords == true)
                        objDocumentElement.SetAttribute("zerorecords", bZeroRecords.ToString());
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.GetFuturePayments.Error",m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
			return objFuturePayments;
		}

        /// <summary>
        /// Created By : Parijat--Mits 12126: New function added inorder to meet the Batch delete functionality which is different from payment delete
        /// </summary>
        /// <param name="p_sDeleteList"></param>
        /// <returns></returns>
        public bool DeleteBatchPayments(string p_sDeleteList)
        {
            string[] arrDelete = new string[] { "0", "0" };
            arrDelete = p_sDeleteList.Split(new char[] { ',' });
            string sSQL = string.Empty;
            //int result = 0;
            //int rs = 0;
            int iPNum=0;
			int iBatchIdLocal=0;
            //long lAutoBatchID = 0;
            DbConnection objConn=null;
            //StringBuilder sSQL = new StringBuilder();
            //DbReader objReader = null;
            try
			{ 
				objConn = DbFactory.GetDbConnection(m_sConnectionString);			
				objConn.Open();

                iPNum=Conversion.ConvertStrToInteger(arrDelete[0]);
			    iBatchIdLocal=Conversion.ConvertStrToInteger(arrDelete[1]);
                if (iBatchIdLocal != 0)
                {

                    sSQL = "DELETE FROM FUNDS_AUTO_SPLIT WHERE AUTO_TRANS_ID IN (SELECT AUTO_TRANS_ID FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + iBatchIdLocal.ToString() + ")";
                    objConn.ExecuteNonQuery(sSQL);

                    sSQL = "DELETE FROM FUNDS_AUTO WHERE AUTO_BATCH_ID = " + iBatchIdLocal.ToString();
                    objConn.ExecuteNonQuery(sSQL);

                    sSQL = "DELETE FROM FUNDS_AUTO_BATCH WHERE AUTO_BATCH_ID = " + iBatchIdLocal.ToString();
                    objConn.ExecuteNonQuery(sSQL);
                }
            
            
            }
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.DeletePayments.Error",m_iClientId), p_objEx);
			}
			finally
			{
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                //if (objReader != null)
                //{
                //    objReader.Close();
                //    objReader.Dispose();
                //}
			}
			return true;
        }

		public bool DeletePayments(string p_sDeleteList)
		{
			string[] arrDeleteList=p_sDeleteList.Split(new char[]{'|'});
			string[] arrDelete=new string[]{"0","0"};
			int iPNum=0;
			int iBatchIdLocal=0;
			DbConnection objConn=null;
			StringBuilder sSQL=new StringBuilder();
            DbReader objReader = null;
           

			try
			{ 
				objConn = DbFactory.GetDbConnection(m_sConnectionString);			
				objConn.Open();

				foreach(string sDelete in arrDeleteList)
				{
					arrDelete=sDelete.Split(new char[]{','});
                   
					iPNum=Conversion.ConvertStrToInteger(arrDelete[0]);
					iBatchIdLocal=Conversion.ConvertStrToInteger(arrDelete[1]);
                    if( (iPNum != 0) && (iBatchIdLocal != 0))
                    {
                       
                        // Changed from string to StringBuilder to improve performance
						sSQL.Append("DELETE FROM FUNDS_AUTO_SPLIT");
						sSQL.Append(" WHERE FUNDS_AUTO_SPLIT.AUTO_TRANS_ID IN");
						sSQL.Append(" (SELECT FUNDS_AUTO.AUTO_TRANS_ID");
						sSQL.Append(" FROM FUNDS_AUTO");
						sSQL.Append(" WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + iBatchIdLocal.ToString());
                        sSQL.Append(" AND FUNDS_AUTO.PAY_NUMBER = " + iPNum.ToString() + ")");
                        
                        //Parijat: MITS 12126--Inorder to make it compatible to oracle database also where one nonquerry is fired at a time
                        objConn.ExecuteNonQuery(sSQL.ToString());
                        sSQL.Remove(0, sSQL.Length);

						sSQL.Append(" DELETE FROM FUNDS_AUTO");
						sSQL.Append(" WHERE FUNDS_AUTO.AUTO_BATCH_ID =" + iBatchIdLocal.ToString());
						sSQL = sSQL.Append(" AND FUNDS_AUTO.PAY_NUMBER = " + iPNum.ToString());

                        //Parijat: MITS 12126--Inorder to make it compatible to oracle database also where one nonquerry is fired at a time
                        objConn.ExecuteNonQuery(sSQL.ToString());
                        sSQL.Remove(0, sSQL.Length);

                        // Added as this is in RMWorld
                        sSQL.Append(" UPDATE FUNDS_AUTO_BATCH SET TOTAL_PAYMENTS = TOTAL_PAYMENTS - 1");
                        sSQL.Append(" WHERE FUNDS_AUTO_BATCH.AUTO_BATCH_ID =" + iBatchIdLocal.ToString());

                        //Parijat: MITS 12126--Inorder to make it compatible to oracle database also where one nonquerry is fired at a time
                        objConn.ExecuteNonQuery(sSQL.ToString());
                        sSQL.Remove(0, sSQL.Length);

                       
                        // Commented - So that all the Queries are fired at one time
						//objConn.ExecuteNonQuery(sSQL.ToString());
                        
                    }
				}
                //Parijat:Commented - So that all the Queries are fired one at a time as required by oracle
                //objConn.ExecuteNonQuery(sSQL.ToString());

                //Check to see if this was the last payment if so delete the entry from funds_auto_batch
                sSQL.Remove(0 , sSQL.Length);
                sSQL.Append("SELECT * FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + iBatchIdLocal.ToString());
                sSQL.Append(" AND FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID");
                
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
                if (!objReader.Read())
                {
                    sSQL.Remove(0, sSQL.Length);
                    sSQL.Append("DELETE FROM FUNDS_AUTO_BATCH WHERE AUTO_BATCH_ID = " + iBatchIdLocal.ToString());
                    objConn.ExecuteNonQuery(sSQL.ToString());
                }
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.DeletePayments.Error",m_iClientId), p_objEx);
			}
			finally
			{
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
			}
			return true;
		}

        //Created by : pmittal5 for Mits 14253 on 01/19/09 - Deleting entry from FUNDS_AUTO_BATCH if all Payments in batch are Negative
        private bool DeleteAutoBatchPayment(int p_iBatchId,DbTransaction objDBTransaction,DbConnection objDBConnection)
        {
            StringBuilder sSQL = new StringBuilder();
            DbReader objReader = null;
            try
            {
                sSQL.Append("SELECT * FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString());
                sSQL.Append(" AND FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID");
                objReader = objDBConnection.ExecuteReader(sSQL.ToString(), objDBTransaction);
                if (!objReader.Read())
                {
                    objReader.Close();
                    sSQL.Remove(0, sSQL.Length);
                    sSQL.Append("DELETE FROM FUNDS_AUTO_BATCH WHERE AUTO_BATCH_ID = " + p_iBatchId.ToString());
                    objDBConnection.ExecuteNonQuery(sSQL.ToString(), objDBTransaction);
                }
                sSQL.Remove(0, sSQL.Length);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.DeleteAutoBatchPayment.Error",m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return true;
        }//End - pmittal5


        //Created by : pmittal5 for Mits 14253 on 01/19/09 - Updating TOTAL_PAYMENTS in FUNDS_AUTO_BATCH 
        private bool UpdateAutoBatchPayment(int p_iBatchId, DbTransaction objDBTransaction, DbConnection objDBConnection)
        {
            StringBuilder sSQL = new StringBuilder();
            DbReader objReader = null;
            try
            {
                sSQL.Append(" UPDATE FUNDS_AUTO_BATCH SET TOTAL_PAYMENTS = TOTAL_PAYMENTS - 1");
                sSQL.Append(" WHERE FUNDS_AUTO_BATCH.AUTO_BATCH_ID =" + p_iBatchId.ToString());

                objDBConnection.ExecuteNonQuery(sSQL.ToString(), objDBTransaction);
                sSQL.Remove(0, sSQL.Length);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.UpdateAutoBatchPayment.Error",m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return true;
        }//End - pmittal5

		public XmlDocument EditPayment(int p_iBatchId, int p_iPaymentNum, int iAutoSplitId)
		{
			XmlDocument objEditPayment=null;
			XmlElement objDocumentElement=null;
			XmlElement objRow=null;
			StringBuilder sSQL=new StringBuilder();
			DbReader objReader = null;
			bool bZeroRecords=true;
			int iTag=0;
			double dblNet=0;
			double dblGross=0;
            String sCheckMemo = "";

			objEditPayment=new XmlDocument();
			objDocumentElement=objEditPayment.CreateElement("table");
			objDocumentElement.SetAttribute("syscmd",SysCmd_EditPayment.ToString());
			objDocumentElement.SetAttribute("batchid",p_iBatchId.ToString());
			objDocumentElement.SetAttribute("paymentnum",p_iPaymentNum.ToString());
            objDocumentElement.SetAttribute("autosplitid", iAutoSplitId.ToString());
			objEditPayment.AppendChild(objDocumentElement);

            //pmittal5 Mits 15804 04/24/09 - Modified Query to correct Separate Supplement Payment scenario
            // Changed from String to StringBuilder to improve Performance
            // NET AND GROSS Changed
            sSQL.Append("SELECT FIRST_NAME, LAST_NAME, CLAIM_NUMBER,FUNDS_AUTO.AMOUNT NET,FUNDS_AUTO_SPLIT.AMOUNT GROSS,FUNDS_AUTO_SPLIT.FROM_DATE,FUNDS_AUTO_SPLIT.TO_DATE,FUNDS_AUTO.PRINT_DATE,FUNDS_AUTO_SPLIT.TAG,CHECK_MEMO");
            sSQL.Append(" FROM FUNDS_AUTO INNER JOIN FUNDS_AUTO_SPLIT");
            sSQL.Append(" ON FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID");
            //sSQL.Append(" WHERE FUNDS_AUTO_SPLIT.AMOUNT > 0");
            //sSQL.Append(" AND FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString());
            //sSQL.Append(" AND FUNDS_AUTO.PAY_NUMBER = " + p_iPaymentNum.ToString());
            sSQL.Append(" WHERE FUNDS_AUTO.AUTO_TRANS_ID = (SELECT AUTO_TRANS_ID FROM FUNDS_AUTO_SPLIT WHERE AUTO_SPLIT_ID = " + iAutoSplitId.ToString() + " )");
            sSQL.Append(" ORDER BY TAG");
           
            try
			{
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
				while (objReader.Read())
				{
					bZeroRecords=false;
					//	tag:  0=claimant,1=fed,2=ss,3=medicare,4=state
					objRow=objEditPayment.CreateElement("row");
					objDocumentElement.AppendChild(objRow);
					objRow.SetAttribute("class", "rowlight");
                    iTag = Conversion.ConvertObjToInt(objReader.GetValue("TAG"), m_iClientId);
                    dblNet = Conversion.ConvertObjToDouble(objReader.GetValue("NET"), m_iClientId);
                    dblGross = Conversion.ConvertObjToDouble(objReader.GetValue("GROSS"), m_iClientId);
                    sCheckMemo = Conversion.ConvertObjToStr(objReader.GetValue("CHECK_MEMO"));

					switch (iTag)
					{
						case 0:
                            objDocumentElement.SetAttribute("claimant", Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")) + " " + Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")));
                            objDocumentElement.SetAttribute("claimnumber", Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER")));

							//gross, fromdate, todate,check memo are editable
							objDocumentElement.SetAttribute("gross", string.Format("{0:0.00}",dblGross));
											
							//from/to date is same for all records in batch
							objDocumentElement.SetAttribute("fromdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("FROM_DATE")),"d"));
							objDocumentElement.SetAttribute("todate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("TO_DATE")),"d"));
                            objDocumentElement.SetAttribute("printdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("PRINT_DATE")), "d"));
                            objDocumentElement.SetAttribute("checkmemo", sCheckMemo);
								
			                //Setting other attribute nodes as blank
                            objDocumentElement.SetAttribute("supppayment", "");
                            objDocumentElement.SetAttribute("pensionoffset", "");
                            objDocumentElement.SetAttribute("ssoffset", "");
                            objDocumentElement.SetAttribute("oioffset", "");
                            //Added by Ravneet Kuar //pmittal5 Mits 14841
                            objDocumentElement.SetAttribute("othoffset1", "");
                            objDocumentElement.SetAttribute("othoffset2", "");
                            objDocumentElement.SetAttribute("othoffset3", "");
                            objDocumentElement.SetAttribute("psttaxded1", "");
                            objDocumentElement.SetAttribute("psttaxded2", "");
                            //////////////////////
                            objDocumentElement.SetAttribute("totaloffsets", "");
                            //rest of rows are read-only
                            objRow.SetAttribute("title", "Net Payment:");
                            objRow.SetAttribute("id", "Net");
							objRow.SetAttribute("value", string.Format("{0:0.00}",dblNet));
							break;
						case 1:	//fed
							objRow.SetAttribute("title", "Federal Tax");
                            objRow.SetAttribute("id", "FederalTax");
							//objRow.SetAttribute("value", string.Format("{0:0.00}",dblGross));	//gross & net are same for tax payments
                            objRow.SetAttribute("value", string.Format("{0:0.00}", (dblGross * -1)));   //Mits 15804
							break;
						case 2:	//ss
							objRow.SetAttribute("title", "Social Security Tax");
                            objRow.SetAttribute("id", "SocialSecurityTax");
                            objRow.SetAttribute("value", string.Format("{0:0.00}", (dblGross * -1)));	//Mits 15804
							break;
						case 3:	//med
							objRow.SetAttribute("title", "Medicare Tax");
                            objRow.SetAttribute("id", "MedicareTax");
                            objRow.SetAttribute("value", string.Format("{0:0.00}", (dblGross * -1)));	//Mits 15804
							break;
						case 4:	//state
							objRow.SetAttribute("title", "State Tax");
                            objRow.SetAttribute("id", "StateTax");
                            objRow.SetAttribute("value", string.Format("{0:0.00}", (dblGross * -1)));	//Mits 15804
							break;
					}
				}
                XmlNodeList objList = objDocumentElement.SelectNodes("//row");
                if(objDocumentElement.SelectSingleNode("//row[@id='FederalTax']")==null)
                {
                    objRow = objEditPayment.CreateElement("row");
                    objDocumentElement.AppendChild(objRow);
                    objRow.SetAttribute("class", "rowlight");
                    objRow.SetAttribute("title", "Federal Tax");
                    objRow.SetAttribute("id", "FederalTax");
                    objRow.SetAttribute("value", "0.00");

                }
                if (objDocumentElement.SelectSingleNode("//row[@id='SocialSecurityTax']") == null)
                {
                    objRow = objEditPayment.CreateElement("row");
                    objDocumentElement.AppendChild(objRow);
                    objRow.SetAttribute("class", "rowlight");
                    objRow.SetAttribute("title", "Social Security Tax");
                    objRow.SetAttribute("id", "SocialSecurityTax");
                    objRow.SetAttribute("value", "0.00");

                }
                if (objDocumentElement.SelectSingleNode("//row[@id='MedicareTax']") == null)
                {
                    objRow = objEditPayment.CreateElement("row");
                    objDocumentElement.AppendChild(objRow);
                    objRow.SetAttribute("class", "rowlight");
                    objRow.SetAttribute("title", "Medicare Tax");
                    objRow.SetAttribute("id", "MedicareTax");
                    objRow.SetAttribute("value", "0.00");

                }
                if (objDocumentElement.SelectSingleNode("//row[@id='StateTax']") == null)
                {
                    objRow = objEditPayment.CreateElement("row");
                    objDocumentElement.AppendChild(objRow);
                    objRow.SetAttribute("class", "rowlight");
                    objRow.SetAttribute("title", "State Tax");
                    objRow.SetAttribute("id", "StateTax");
                    objRow.SetAttribute("value", "0.00");

                }
                double dGrossTotalOffsets = 0;
                foreach (XmlElement objElem in objList)
                {
                    dGrossTotalOffsets = dGrossTotalOffsets +  Conversion.ConvertStrToDouble(objElem.GetAttribute("value"));
                }
                //ravneet  //pmittal5 Mits 14841 - Post tax deductions
                objReader.Close();
                sSQL.Remove(0, sSQL.Length);
                sSQL.Append("SELECT FUNDS_AUTO_SPLIT.AMOUNT GROSS");
                sSQL.Append(" FROM FUNDS_AUTO INNER JOIN FUNDS_AUTO_SPLIT");
                sSQL.Append(" ON FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID");
                sSQL.Append(" WHERE ");
                //pmittal5 Mits 15804 04/24/09 - Modified Query to correct Separate Supplement Payment scenario
                //sSQL.Append(" FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString());
                //sSQL.Append(" AND FUNDS_AUTO.PAY_NUMBER = " + p_iPaymentNum.ToString());
                sSQL.Append(" FUNDS_AUTO.AUTO_TRANS_ID = (SELECT AUTO_TRANS_ID FROM FUNDS_AUTO_SPLIT WHERE AUTO_SPLIT_ID = " + iAutoSplitId.ToString() + " )");
                sSQL.Append(" AND FUNDS_AUTO_SPLIT.TAG IN (12,13)");
                sSQL.Append(" ORDER BY TAG");
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
                while (objReader.Read())
                {
                    dGrossTotalOffsets = dGrossTotalOffsets + Math.Abs(Conversion.ConvertObjToInt64(objReader.GetValue(0), m_iClientId));
                }

                //End
                objDocumentElement.SetAttribute("grosstotalnetoffsets", dGrossTotalOffsets.ToString());

				if (bZeroRecords)
				{
					throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.EditPayment.PaymentMissing",m_iClientId), p_iBatchId.ToString(),p_iPaymentNum.ToString()));
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.EditPayment.Error",m_iClientId), p_objEx);
			}
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
			return objEditPayment;
		}


		public XmlDocument SaveEdit(string p_sFrom, string p_sTo, double p_dblGross, int p_iClaimId, string p_sClaimNumber ,
									int p_iBatchId, int p_iPaymentNum, bool p_bReCalc, long lAutoSplitId, double dblSuppPayment
                                    , double dPensionOffset, double dSSOffset, double dOiOffset, double dOthOffset1, double dOthOffset2, double dOthOffset3, double dPstTaxDed1, double dPstTaxDed2, bool bAmountChange, string p_sPrintDate, string p_sCheckMemo)
		{
			XmlDocument objTable=null;
			XmlElement objDocumentElement=null;
            string sShortCode = "";
            string sCodeDesc = "";

			objTable=new XmlDocument();
			objDocumentElement=objTable.CreateElement("table");
			objDocumentElement.SetAttribute("syscmd",SysCmd_EditPayment.ToString());
			objTable.AppendChild(objDocumentElement);
            DbReader objReader = null;

			//check for required parameters
			if (p_sFrom.Trim()=="")
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.SaveEdit.ParameterMissing",m_iClientId), "From Date"));

			if (p_sTo.Trim()=="")
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.SaveEdit.ParameterMissing",m_iClientId), "To Date"));

			if (p_dblGross==0)
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.SaveEdit.ParameterMissing",m_iClientId), "Gross"));

			Claim objClaim=null;
			int iLOB=0;
			LobSettings objLobSettings = null;

			objClaim=m_objDataModelFactory.GetDataModelObject("Claim",false) as Claim;

			iLOB = objClaim.Context.LocalCache.GetCodeId("DI", "LINE_OF_BUSINESS");
			objLobSettings = objClaim.Context.InternalSettings.ColLobSettings[iLOB];

			objClaim.LineOfBusCode = iLOB;
			objClaim.NavType = Riskmaster.DataModel.ClaimNavType.ClaimNavLOB;

            if (p_iClaimId != 0)
            {
                objClaim.MoveTo(p_iClaimId);
            }
            else
            {
                string sSQL = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + p_sClaimNumber + "'";

                try
                {
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        p_iClaimId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId);
                        objClaim.MoveTo(p_iClaimId);
                    }
                }
                catch(Exception e)
                {

                }
                finally
                {
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                }
            }

            m_objCache = new LocalCache(m_sConnectionString, m_iClientId);

			//Raman Bhatia..LTD updations..writing a new function to update the Dom in case of LTD
            // 03/02/2007 Raman Bhatia: Shifting completely to new methods
            m_objCache.GetCodeInfo(objClaim.DisabilityPlan.PrefPaySchCode, ref sShortCode, ref sCodeDesc);
            //if (sShortCode.ToLower() == "pm")
            //Changed by Ravneet Kaur For GHS Enhancment    
            //FillNonOccPaymentDomAsPerRMWorld(objClaim, ref objTable, p_sFrom, p_sTo, p_dblGross, p_iClaimId, p_iBatchId, p_iPaymentNum, p_bReCalc, lAutoSplitId ,dblSuppPayment ,dPensionOffset , dSSOffset , dOiOffset ,bAmountChange ,p_sPrintDate  , p_sCheckMemo , ref p_objErrOut);
            FillNonOccPaymentDomAsPerRMWorld(objClaim, ref objTable, p_sFrom, p_sTo, p_dblGross, p_iClaimId, p_iBatchId, p_iPaymentNum, p_bReCalc, lAutoSplitId, dblSuppPayment, dPensionOffset, dSSOffset, dOiOffset, dOthOffset1 , dOthOffset2 , dOthOffset3 , dPstTaxDed1, dPstTaxDed2, bAmountChange, p_sPrintDate, p_sCheckMemo);
            //else 
            //   FillNonOccPaymentDom(objClaim,ref objTable,p_sFrom,p_sTo,p_dblGross,p_iClaimId,p_iBatchId,p_iPaymentNum,p_bReCalc,ref p_objErrOut);

            if (objClaim != null)
               objClaim.Dispose();

            return objTable;
		}


		//Modified the parameter list for GHS enhancment.
        public XmlDocument CalculatePayments(int p_iClaimId, int p_iPiEid,int p_iSysCmd, int p_iTransTypeCode,
											 int p_iAccountId, string sBenCalcPayStart , string sBenCalcPayEnd ,double dDailyAmount , double dDailySuppAmount ,string sBenefitStartDate ,string sBenefitThroughDate,double dPension , double dSS , double dOther ,int iDaysWorkingInMonth , int iDaysWorkingInWeek ,int iOffsetCalc , double dPensionOffset , double dSSOffset , double dOiOffset ,double dFederalTax , double dSocialSecurityAmount ,double dMedicareAmount ,double dStateAmount ,double dNetPayment , double dGrossCalculatedPayment ,double dGrossCalculatedSupplement ,double dGrossTotalNetOffsets ,int ilblDaysIncluded ,double dSuppRate , 
                                             double dWeeklyBenefit,double dOther1, double dOther2, double dOther3, double dPostTax1, double dPostTax2,double OtherOffset1, double OtherOffset2, double OtherOffset3, double PostTaxDed1, double PostTaxDed2)
		{
			bool bDupePayments=false;
			XmlDocument objTable=null;
			XmlElement objDocumentElement=null;


			objTable=new XmlDocument();
			objDocumentElement=objTable.CreateElement("table");
			objTable.AppendChild(objDocumentElement);

			//m_dDailyAmount = Math.Round(dDailyAmount , 2);
            m_dDailyAmount = dDailyAmount;
           // m_dDailySuppAmount = Math.Round(dDailySuppAmount , 2);
            m_dDailySuppAmount = dDailySuppAmount;
            //m_dPension = Math.Round(dPension ,2);
            m_dPension = dPension;
            //m_dSS = Math.Round(dSS ,2);
            m_dSS = dSS;
            m_dOther = dOther ;

            //Added by RKaur for GHS Enhancement //pmittal5 Mits 14841
            m_dOther1  = dOther1 ;
            m_dOther2  = dOther2 ;
            m_dOther3  = dOther3 ;
            m_dPostTax1   = dPostTax1 ;
            m_dPostTax2   = dPostTax2 ;
            ///////////////////////

            m_iDaysWorkingInMonth = iDaysWorkingInMonth;
            m_iDaysWorkingInWeek = iDaysWorkingInWeek;
            m_iOffsetCalc = iOffsetCalc;
            m_dPensionOffset = dPensionOffset;
            m_dSSOffset = dSSOffset;
            m_dOiOffset = dOiOffset;

            //Added by Ravneet Kaur //pmittal5 Mits 14841
            m_dOthOffset1 = OtherOffset1;
            m_dOthOffset2 = OtherOffset2 ;
            m_dOthOffset3 = OtherOffset3 ;
            m_dPstTaxDed1 = PostTaxDed1 ;
            m_dPstTaxDed2 = PostTaxDed2 ;
            ////////////////////////

            //m_dFederalTax = Math.Round(dFederalTax,2);
            m_dFederalTax = dFederalTax;
            //m_dMedicareAmount = Math.Round(dMedicareAmount, 2);
            m_dMedicareAmount = dMedicareAmount;
            //m_dSocialSecurityAmount = Math.Round(dSocialSecurityAmount,2);
            m_dSocialSecurityAmount = dSocialSecurityAmount;
            //m_dStateAmount = Math.Round(dStateAmount,2);
            m_dStateAmount = dStateAmount;
            //m_dNetPayment = Math.Round(dNetPayment, 2);
            m_dNetPayment = dNetPayment;
           // m_dGrossCalculatedPayment = Math.Round(dGrossCalculatedPayment,2);
            m_dGrossCalculatedPayment = dGrossCalculatedPayment;
            //m_dGrossCalculatedSupplement = Math.Round(dGrossCalculatedSupplement, 2);
            m_dGrossCalculatedSupplement = dGrossCalculatedSupplement;
            //m_dGrossTotalNetOffsets = Math.Round(dGrossTotalNetOffsets,2);
            m_dGrossTotalNetOffsets = dGrossTotalNetOffsets;
            m_ilblDaysIncluded = ilblDaysIncluded;
            //m_dSuppRate = Math.Round(dSuppRate, 2);
            m_dSuppRate = dSuppRate;
           // m_dWeeklyBenefit = Math.Round(dWeeklyBenefit, 2);
            m_dWeeklyBenefit = dWeeklyBenefit;

            if (p_iSysCmd==SysCmd_Calc)
				bDupePayments=DupePayments(ref objTable,p_iClaimId, p_iPiEid);

			if (!bDupePayments)
			{
				if (p_iSysCmd==SysCmd_SavePayments)
					objDocumentElement.SetAttribute("syscmd",SysCmd_SavePayments.ToString());
				else
					objDocumentElement.SetAttribute("syscmd",SysCmd_Calc.ToString());

				Claim objClaim=null;
				int iLOB=0;
				LobSettings objLobSettings = null;

				objClaim=m_objDataModelFactory.GetDataModelObject("Claim",false) as Claim;

				iLOB = objClaim.Context.LocalCache.GetCodeId("DI", "LINE_OF_BUSINESS");
				objLobSettings = objClaim.Context.InternalSettings.ColLobSettings[iLOB];

				objClaim.LineOfBusCode = iLOB;
				objClaim.NavType = Riskmaster.DataModel.ClaimNavType.ClaimNavLOB;

				objClaim.MoveTo(p_iClaimId);

                m_objCache = new LocalCache(m_sConnectionString, m_iClientId);

                FillNonOccPaymentsDom(objClaim, ref objTable, p_iTransTypeCode, p_iAccountId, p_iSysCmd, sBenCalcPayStart, sBenCalcPayEnd, sBenefitStartDate, sBenefitThroughDate);

                if (objClaim != null)
                    objClaim.Dispose();

			}

			return objTable;

		}


		public XmlDocument GetCollectionScreenInformation(int p_iClaimId)
		{
			XmlDocument objReturnXML=null;
			objReturnXML=new XmlDocument();
			Claim objClaim=null;
			DisabilityClass objDisClass=null;
			bool[] bTaxes=new bool[]{false,false,false,false};
			TaxRate dtpTaxes=new TaxRate();

			InitCollectionScreen(p_iClaimId,ref objReturnXML, ref objClaim, ref objDisClass, ref bTaxes, ref dtpTaxes, true);

			return objReturnXML;
		}


		public void SaveAllCollections(int p_iClaimId,XmlDocument p_objXmlIn)
		{
			XmlDocument objReturnXML=null;
			objReturnXML=new XmlDocument();
			Claim objClaim=null;
			DisabilityClass objDisClass=null;
			bool[] bTaxes=new bool[]{false,false,false,false};
			TaxRate dtpTaxes=new TaxRate();

			int iBankAccount=Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//BankAccount").InnerText);
			double dblNetAmount=Conversion.ConvertStrToDouble(p_objXmlIn.SelectSingleNode("//NetAmount").InnerText);
			int iTransTypeCode=Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//TransactionType").InnerText);
			bool bFed=Conversion.ConvertStrToBool(p_objXmlIn.SelectSingleNode("//Fed").InnerText);
			bool bSS=Conversion.ConvertStrToBool(p_objXmlIn.SelectSingleNode("//SS").InnerText);
			bool bMed=Conversion.ConvertStrToBool(p_objXmlIn.SelectSingleNode("//Med").InnerText);
			bool bState=Conversion.ConvertStrToBool(p_objXmlIn.SelectSingleNode("//State").InnerText);
			double dblFedTax=Conversion.ConvertStrToDouble(p_objXmlIn.SelectSingleNode("//FedAmount").InnerText);
			double dblSSTax=Conversion.ConvertStrToDouble(p_objXmlIn.SelectSingleNode("//SSAmount").InnerText);
			double dblMedTax=Conversion.ConvertStrToDouble(p_objXmlIn.SelectSingleNode("//MedAmount").InnerText);
			double dblStateTax=Conversion.ConvertStrToDouble(p_objXmlIn.SelectSingleNode("//StateAmount").InnerText);

			InitCollectionScreen(p_iClaimId,ref objReturnXML, ref objClaim, ref objDisClass, ref bTaxes, ref dtpTaxes, false);

			int iBankAccountID=0;
			int iSubBankAccountID=0;

			//Get Account IDs Check to see if using sub accounts
			if (iBankAccount > 0 )
			{
				if (objClaim.Context.InternalSettings.SysSettings.UseFundsSubAcc)
				{
					iSubBankAccountID=iBankAccount;
					iBankAccountID=GetParentAccountID(iBankAccount,objClaim.Context.DbConn,null);
				}
				else
				{
					iBankAccountID=iBankAccount;
					iSubBankAccountID=0;
				}
			}

			double dblAmount=0;
			int iPayeeEid=0;
			int iTaxPayment=0;
			
			dblAmount = dblNetAmount;
            iPayeeEid = objClaim.PrimaryClaimant.ClaimantEid;
			iTransTypeCode=iTransTypeCode;
            iTaxPayment = 0;
			SaveThisCollection(	dblAmount, iPayeeEid, iTransTypeCode, iTaxPayment,
								objClaim,iBankAccountID,objDisClass,p_objXmlIn,dtpTaxes,
								iSubBankAccountID);

			if (bFed)
			{
				dblAmount=dblFedTax;
				iPayeeEid=dtpTaxes.iFedTaxEID;
				iTransTypeCode=dtpTaxes.iFedTransTypeCodeID;
				iTaxPayment = 1;
				SaveThisCollection(	dblAmount, iPayeeEid, iTransTypeCode, iTaxPayment,
									objClaim,iBankAccountID,objDisClass,p_objXmlIn,dtpTaxes,
									iSubBankAccountID);
			}
            
			if (bSS)
			{
				dblAmount=dblSSTax;
				iPayeeEid=dtpTaxes.iSSTaxEID;
				iTransTypeCode=dtpTaxes.iSSTransTypeCodeID;
				iTaxPayment = 1;
				SaveThisCollection(	dblAmount, iPayeeEid, iTransTypeCode, iTaxPayment,
									objClaim,iBankAccountID,objDisClass,p_objXmlIn,dtpTaxes,
									iSubBankAccountID);
			}

			if (bMed)
			{
				dblAmount=dblMedTax;
				iPayeeEid=dtpTaxes.iMedicareTaxEID;
				iTransTypeCode=dtpTaxes.iMedicareTransTypeCodeID;
				iTaxPayment = 1;
				SaveThisCollection(	dblAmount, iPayeeEid, iTransTypeCode, iTaxPayment,
									objClaim,iBankAccountID,objDisClass,p_objXmlIn,dtpTaxes,
									iSubBankAccountID);
			}

			if (bState)
			{
				dblAmount=dblStateTax;
				iPayeeEid=dtpTaxes.iStateTaxEID;
				iTransTypeCode=dtpTaxes.iStateTransTypeCodeID;
				iTaxPayment = 1;
				SaveThisCollection(	dblAmount, iPayeeEid, iTransTypeCode, iTaxPayment,
									objClaim,iBankAccountID,objDisClass,p_objXmlIn,dtpTaxes,
									iSubBankAccountID);
			}
   		}
   

		#endregion

		#region Private Functions
		/// <summary>
		/// Initialize objects
		/// </summary>
		private void Initialize()
		{
			try
			{
				m_objDataModelFactory = new DataModelFactory( m_sDsnName , m_sUserName , m_sPassword ,m_iClientId);	//psharma206
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;				
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.Initialize.Error",m_iClientId) , p_objEx );				
			}			
		}		


		//Raman 07/29/2009: No longer used
        /*
        private void FillNonOccPaymentDom(Claim objClaim, ref XmlDocument p_objTable, string p_sFrom, 
										  string p_sTo, double p_dblGross, int p_iClaimId, int p_iBatchId, 
										  int p_iPaymentNum, bool p_bReCalc, string p_sCheckMemo , ref BusinessAdaptorErrors p_objErrOut)
		{
			DisabilityPlan objPlan=null;
			DisabilityClass objDisClass=null;
			PiEmployee objEmp=null;
			int iPiEid=0;
			string sSQL=string.Empty;
			DbReader objReader=null;
			int iPrefPaySchCode=0;
			string sBenCalcPayStart=string.Empty;
			string sBenCalcPayTo=string.Empty;
			string sPayPeriodStart=string.Empty;
			int iBiWeek = 0;
			int iPayPeriod = 0;
			string sShortCode=string.Empty;
			string sCodeDesc=string.Empty;
			string sWorkDaysArray=string.Empty;
			bool[] bTaxes=new bool[]{false,false,false,false};
			int iTaxFlags=0;
			TaxRate dtpTaxes=new TaxRate();
			bool bError=false;
			double dblNewGross=p_dblGross;
			double dblHourlyRate=0;
			double dblWeeklyRate=0;
			int iWorkDaysCount=0;
			double dblDailyBenefit=0;
			double dblWeeklyBenefit=0;
			DateTime dFrom=DateTime.Now;
			DateTime dTo=DateTime.Now;
			int iCount=0;
			double dblTemp=0;
			TimeSpan ts=new TimeSpan(1);
			DateTime dNewFrom=DateTime.Now;
			DateTime dNewTo=DateTime.Now;
			DateTime dTemp=DateTime.Now;
			DateTime dFirstToDate=DateTime.Now;

			dNewFrom=Conversion.ToDate(p_sFrom);
			dNewTo=Conversion.ToDate(p_sTo);

			objPlan=objClaim.DisabilityPlan;
			iPiEid=objClaim.PrimaryClaimant.ClaimantEid;

			foreach(DisabilityClass objClass in objPlan.ClassList)
			{
				if (objClass.ClassRowId==objClaim.ClassRowId) 
				{
					objDisClass=objClass;
					break;
				}
			}
			if (objDisClass == null)
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.DisClassNotFound"), objClaim.ClaimId.ToString(),objClaim.DisabilityPlan.PlanId.ToString()));

			objEmp=objClaim.PrimaryPiEmployee;

			//get benefit start date, benefit through, pay period from the batch
			//note:  user could have edited these fields in plan or in non-occ window after calculating the payments, so grab values from the batch
			//payment_interval = Plan.PrefPaySchCode,start_date = Claim.BenCalcPayStart (or default), end_date = Claim.BenCalcPayTo (or default), pay_period_start=Plan.StartOfPayPeriod (or default)
			sSQL = "SELECT PAYMENT_INTERVAL,START_DATE,END_DATE,PAY_PERIOD_START FROM FUNDS_AUTO_BATCH WHERE AUTO_BATCH_ID = " + p_iBatchId.ToString();
			try
			{
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
				if (objReader.Read())
				{
					iPrefPaySchCode = Conversion.ConvertObjToInt(objReader.GetValue(0));
					sBenCalcPayStart = Conversion.ConvertObjToStr(objReader.GetValue(1));
					sBenCalcPayTo = Conversion.ConvertObjToStr(objReader.GetValue(2));
					sPayPeriodStart = Conversion.ConvertObjToStr(objReader.GetValue(3));
				}
				else
					throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.PayBatchNotFound"), p_iBatchId.ToString()));
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.Error"), p_objEx);
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
			}

			//pay period is 7 or 14 days.
			//if 14 days, compute if dBenCalcPayStart is in first or second week of pay period
			iBiWeek = 1;
			iPayPeriod = 7;

			m_objCache.GetCodeInfo(objPlan.PrefPaySchCode,ref sShortCode, ref sCodeDesc);
		
			switch (sShortCode.Trim().ToUpper())
			{
				case "PB":
					//if datediff is even, are in first week; odd, are in second week
					//note:  dPayPeriodStart can be in past or future, does not matter,
					//only used to compute iBiWeek (whether dBenCAlcPayStart is in first or second week of pay period)
					int iCounter = GetWeeks(Conversion.ToDate(sBenCalcPayStart),Conversion.ToDate(sPayPeriodStart));
					if (iCounter % 2 != 0) 
						iBiWeek = 2;
					iPayPeriod = 14;
					break;
				case "PW":
					//weekly
					break;
				default:
					throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.InvalidPrefPaySchCode"));
			}
			
			//days worked
			if (objDisClass.Day7WorkWeek)
				sWorkDaysArray = "-1,-1,-1,-1,-1,-1,-1";
			else if (objDisClass.Day5WorkWeek)
				sWorkDaysArray = "0,-1,-1,-1,-1,-1,0";
			else	//objclass.ACTUAL_WORK_WEEK or not set
			{
				sWorkDaysArray=	Conversion.ConvertBoolToInt(objEmp.WorkMonFlag).ToString() + "," +
					Conversion.ConvertBoolToInt(objEmp.WorkTueFlag).ToString() + "," + 
					Conversion.ConvertBoolToInt(objEmp.WorkWedFlag).ToString() + "," +
					Conversion.ConvertBoolToInt(objEmp.WorkThuFlag).ToString() + "," +
					Conversion.ConvertBoolToInt(objEmp.WorkFriFlag).ToString() + "," +
					Conversion.ConvertBoolToInt(objEmp.WorkSatFlag).ToString() + "," +
					Conversion.ConvertBoolToInt(objEmp.WorkSunFlag).ToString();
			}

			//fillnonoccdom() should prevent payment calculation if no work days in week
			if (sWorkDaysArray.Equals("0,0,0,0,0,0,0"))
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.ZeroWorkDays"));

			string[] arrWorkDays = sWorkDaysArray.Split(new char[]{','});

			//taxes
			m_objCache.GetStateInfo(objClaim.PrimaryClaimant.ClaimantEntity.StateId,ref sShortCode, ref sCodeDesc);
            //MITS 12902 : Umesh
			//GetTaxes(objClaim.PrimaryClaimant.ClaimantEntity.StateId, ref bTaxes, ref dtpTaxes);
            GetTaxes(objClaim, ref bTaxes, ref dtpTaxes);

			iTaxFlags = objClaim.TaxFlags;
			if (iTaxFlags==0)	//mwc only set flags if they have not been previously saved.  1 means has been saved
			{
				dtpTaxes.bFed = objDisClass.WithholdFedItax;
				dtpTaxes.bSS = objDisClass.WithholdFica;
				dtpTaxes.bMed = objDisClass.WithholdMedicare;
				dtpTaxes.bState = objDisClass.WithholdState;		
			}
			else
			{
				//objClaim.taxflags is bitmask: fed = 2,ss = 4,medicare = 8,state = 16
				dtpTaxes.bFed = ((2 | iTaxFlags)==iTaxFlags);
				dtpTaxes.bSS = ((4 | iTaxFlags)==iTaxFlags);
				dtpTaxes.bMed = ((8 | iTaxFlags)==iTaxFlags);
				dtpTaxes.bState = ((16 | iTaxFlags)==iTaxFlags);
			}
			dtpTaxes.dblTaxablePercent = objDisClass.TaxablePercent/100;

			//1.  verify that FromDate/ToDate are valid (fromdate cannot precede beginning of pay period, dTo cannot exceed end)
			
			//find first "To" date, the last day of the week of the pay period--start at payments calculation start date, add date until get to same day of week as pay period start date + 6
			dTemp=Conversion.ToDate(sBenCalcPayStart);
			//DateTime dPayPeriodStart=Conversion.ToDate(sPayPeriodStart);
			iCount=(int)dTemp.DayOfWeek;

			while (iCount!=((int)(Conversion.ToDate(sPayPeriodStart)).AddDays(6).DayOfWeek))
			{
				dTemp=dTemp.AddDays(1);
				iCount=(int)dTemp.DayOfWeek;
			}
			dFirstToDate=dTemp;

			if (iPayPeriod==14 && iBiWeek==1)
				//in first week of bi-weekly payments, so add 7 days
				dFirstToDate =dFirstToDate.AddDays(7);

			dTo = dFirstToDate;
			dFrom =Conversion.ToDate(sBenCalcPayStart);
			int iTemp=1;
			bool bTemp = false;
			//find the pay period I am in (indicated by payment number)
			//--payment numbers are always sequential starting with "1" at first
			//--user can delete a payment from a batch, but after deleting can never add a payment to fill the missing payment number.
			//--the only way to add payments is to add an entire batch
			while(dFrom <= Conversion.ToDate(sBenCalcPayTo))
			{
				if (iTemp == p_iPaymentNum)
				{
					bTemp=true;
					break;
				}
				iTemp += 1;
				dTo = dTo.AddDays(iPayPeriod);
				dFrom=dTo.AddDays(-(iPayPeriod - 1));	//beginning of pay period
				ts=Conversion.ToDate(sBenCalcPayTo).Subtract(dTo);
				if (ts.Days < 0)
					dTo=Conversion.ToDate(sBenCalcPayTo);
			}

			if (!bTemp)
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.OrigPayPeriodNotFound"), p_iBatchId.ToString(),p_iPaymentNum.ToString()));
    
			//add error message(s) if either date is bad
			ts=dNewTo.Subtract(dNewFrom);
			if (ts.Days<0)
			{
				bError = true;
				p_objErrOut.Add("NonOccPaymentsManagerError",Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.FromDateMustBeLess"),BusinessAdaptorErrorType.Error);
			}

			ts=dNewFrom.Subtract(dFrom);
			if (ts.Days<0 || ts.Days>iPayPeriod)
			{
				bError = true;
				p_objErrOut.Add("NonOccPaymentsManagerError",String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.FromDateError"), Conversion.GetDBDateFormat(Conversion.ToDbDate(dFrom),"d"),Conversion.GetDBDateFormat(Conversion.ToDbDate(dTo),"d")),BusinessAdaptorErrorType.Error);
			}

			ts=dNewTo.Subtract(dTo);
			if (ts.Days<0 || ts.Days<(iPayPeriod*-1))
			{
				bError = true;
				p_objErrOut.Add("NonOccPaymentsManagerError",String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.ToDateError"), Conversion.GetDBDateFormat(Conversion.ToDbDate(dTo),"d"),Conversion.GetDBDateFormat(Conversion.ToDbDate(dFrom),"d")),BusinessAdaptorErrorType.Error);
			}

			//2
			if (p_bReCalc && !bError)
			{
				//if bRecalc, recompute workdays count & dblNewGross based upon fromdate/todate
				dblNewGross = 0;
				if (objEmp.PiRowId==0)
					throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.EmployeeNotFound"),objClaim.ClaimId.ToString()));
        
				//weekly benefit
				dblHourlyRate = objEmp.HourlyRate;
				if (objClaim.ClaimAWW == null)
					dblWeeklyRate=objEmp.WeeklyRate;
				else
				{
					dblWeeklyRate=objClaim.ClaimAWW.Aww;
					if (dblWeeklyRate==0)
						dblWeeklyRate=objEmp.WeeklyRate;
				}

				if (objDisClass.BenePrctgFlag)	//Percentage of Weekly Wage
				{
					dblTemp = objDisClass.BenePerAmt;
					if (dblTemp == 0)
						dblTemp = 100;   //0 means leave at 100%
					dblWeeklyBenefit = objEmp.WeeklyRate * (dblTemp / 100);
				}
				else if (objDisClass.BeneFlatAmtFlag)
				{
					dblWeeklyBenefit = objDisClass.BeneFlatAmt;
				}
				else if (objDisClass.BeneTdFlag)  //Table Driven (based on Weekly Rate or Hourly Rate)
				{
					dblWeeklyBenefit = dblWeeklyRate;	//dblWeeklyBenefit overridden in For loop if match is found
					foreach(DisClassTd objDisClassTd in objDisClass.TableList)
					{
						if (dblWeeklyRate <= objDisClassTd.WagesTo)
						{
							if (dblWeeklyRate >= objDisClassTd.WagesFrom)
							{
								dblWeeklyBenefit = objDisClassTd.WeeklyBenefit;
								break;
							}
						}
					}
				}

				// apply weekly cap
				if (objDisClass.WeeklyBeneCap > 0)
				{
					if (dblWeeklyBenefit > objDisClass.WeeklyBeneCap)
						dblWeeklyBenefit = objDisClass.WeeklyBeneCap;
				}

				//get daily benefit: weekly benefit/number of days worked per week
				iWorkDaysCount = 0;
				for(int iCounter=0;iCounter<arrWorkDays.Length;iCounter++)
				{
					if (arrWorkDays[iCounter]!="0")
						iWorkDaysCount=iWorkDaysCount+1;
				}
        
				dblDailyBenefit = dblWeeklyBenefit / iWorkDaysCount;
        
				//recompute the gross
				dFrom=dNewFrom;
				iWorkDaysCount = 0;
				while(dFrom <= dNewTo)
				{
					iCount=(int)dFrom.DayOfWeek;
					if(arrWorkDays[iCount]!="0")
					{
						dblNewGross = dblNewGross + dblDailyBenefit;
						iWorkDaysCount = iWorkDaysCount + 1	;
					}
					dFrom=dFrom.AddDays(1);
				}
			}
			else if (!p_bReCalc && !bError)
			{
				//get new workdays count based on new from/to dates
				dFrom=dNewFrom;
				iWorkDaysCount = 0;
				while(dFrom <= dNewTo)
				{
					iCount=(int)dFrom.DayOfWeek;
					if(arrWorkDays[iCount]!="0")
						iWorkDaysCount = iWorkDaysCount + 1	;
					dFrom=dFrom.AddDays(1);
				}
			}//if bRecalc

			//3.  recalculate taxes based on new gross
			double dblTaxablePercent=0;
			double dblFed=0;
			double dblSS=0;
			double dblMed=0;
			double dblState=0;
			double dblNet=0;
			dblTaxablePercent = dtpTaxes.dblTaxablePercent;
			if (dblTaxablePercent == 0) 
				dblTaxablePercent = 1;
			if (dtpTaxes.bFed)
				dblFed=Conversion.ConvertStrToDouble(string.Format("{0:0.00}",(dblNewGross * dblTaxablePercent) * dtpTaxes.FED_TAX_RATE));
			if (dtpTaxes.bSS)
				dblSS=Conversion.ConvertStrToDouble(string.Format("{0:0.00}",(dblNewGross * dblTaxablePercent) * dtpTaxes.SS_TAX_RATE));
			if (dtpTaxes.bMed)
				dblMed=Conversion.ConvertStrToDouble(string.Format("{0:0.00}",(dblNewGross * dblTaxablePercent) * dtpTaxes.MEDICARE_TAX_RATE));
			if (dtpTaxes.bState && dtpTaxes.STATE_TAX_RATE>0)
				dblState=Conversion.ConvertStrToDouble(string.Format("{0:0.00}",(dblNewGross * dblTaxablePercent) * dtpTaxes.STATE_TAX_RATE));
			dblNet = dblNewGross - (dblFed + dblSS + dblMed + dblState);

			string sNewFrom=Conversion.ToDbDate(dNewFrom);
			string sNewTo=Conversion.ToDbDate(dNewTo);

			//4. update payment records in database (user is either recalculating or saving, no other action)
			if (!bError && !p_bReCalc)
			{
				DbConnection objDbConnection=null;
				DbTransaction objDbTransaction=null;

				try
				{
					//begin the transaction
					//must open a separate database for transaction or get connection leak
					objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
					objDbConnection.Open();  
					objDbTransaction = objDbConnection.BeginTransaction(); 

					//for claimant payee eid: FUNDS_AUTO_SPLIT.TAG = 0, FUNDS_AUTO.AMOUNT holds net, FUNDS_AUTO_SPLIT.AMOUNT holds gross
					sSQL = FundsAutoSplitUpdateSQL(iPiEid, p_iBatchId, p_iPaymentNum, 0, dblNewGross, sNewFrom, sNewTo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

                    sSQL = FundsAutoUpdateSQL(iPiEid, p_iBatchId, p_iPaymentNum, 0, dblNet, iWorkDaysCount, p_sCheckMemo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

					//for claimant payee eid: FUNDS_AUTO_SPLIT.TAG=[TAX], FUNDS_AUTO.AMOUNT holds net, FUNDS_AUTO_SPLIT.AMOUNT holds tax * -1
					sSQL = FundsAutoSplitUpdateSQL(iPiEid, p_iBatchId, p_iPaymentNum, 1, dblFed * -1, sNewFrom, sNewTo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

                    sSQL = FundsAutoUpdateSQL(iPiEid, p_iBatchId, p_iPaymentNum, 1, dblNet, iWorkDaysCount, p_sCheckMemo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

					sSQL = FundsAutoSplitUpdateSQL(iPiEid, p_iBatchId, p_iPaymentNum, 2, dblSS * -1,sNewFrom, sNewTo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

                    sSQL = FundsAutoUpdateSQL(iPiEid, p_iBatchId, p_iPaymentNum, 2, dblNet, iWorkDaysCount, p_sCheckMemo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

					sSQL = FundsAutoSplitUpdateSQL(iPiEid, p_iBatchId, p_iPaymentNum, 3, dblMed * -1, sNewFrom, sNewTo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

                    sSQL = FundsAutoUpdateSQL(iPiEid, p_iBatchId, p_iPaymentNum, 3, dblNet, iWorkDaysCount, p_sCheckMemo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

					sSQL = FundsAutoSplitUpdateSQL(iPiEid, p_iBatchId, p_iPaymentNum, 4, dblState * -1, sNewFrom, sNewTo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

					sSQL = FundsAutoUpdateSQL(iPiEid, p_iBatchId, p_iPaymentNum, 4, dblNet, iWorkDaysCount , p_sCheckMemo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);
        
					//for tax payee eid: FUNDS_AUTO.AMOUNT and FUNDS_AUTO_SPLIT.AMOUNT hold tax
					sSQL = FundsAutoSplitUpdateSQL(dtpTaxes.iFedTaxEID, p_iBatchId, p_iPaymentNum, 1, dblFed, sNewFrom, sNewTo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

                    sSQL = FundsAutoUpdateSQL(dtpTaxes.iFedTaxEID, p_iBatchId, p_iPaymentNum, 1, dblFed, iWorkDaysCount, p_sCheckMemo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

					sSQL = FundsAutoSplitUpdateSQL(dtpTaxes.iSSTaxEID, p_iBatchId, p_iPaymentNum, 2, dblSS, sNewFrom, sNewTo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

                    sSQL = FundsAutoUpdateSQL(dtpTaxes.iSSTaxEID, p_iBatchId, p_iPaymentNum, 2, dblSS, iWorkDaysCount, p_sCheckMemo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

					sSQL = FundsAutoSplitUpdateSQL(dtpTaxes.iMedicareTaxEID, p_iBatchId, p_iPaymentNum, 3, dblMed, sNewFrom, sNewTo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

                    sSQL = FundsAutoUpdateSQL(dtpTaxes.iMedicareTaxEID, p_iBatchId, p_iPaymentNum, 3, dblMed, iWorkDaysCount, p_sCheckMemo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

					sSQL = FundsAutoSplitUpdateSQL(dtpTaxes.iStateTaxEID, p_iBatchId, p_iPaymentNum, 4, dblState, sNewFrom, sNewTo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

					sSQL = FundsAutoUpdateSQL(dtpTaxes.iStateTaxEID, p_iBatchId, p_iPaymentNum, 4, dblState, iWorkDaysCount , p_sCheckMemo);
					objDbConnection.ExecuteNonQuery(sSQL,objDbTransaction);

					objDbTransaction.Commit();
				}
				catch(InitializationException p_objInitializationException)
				{
					throw p_objInitializationException;
				}
				catch(InvalidValueException p_objInvalidValueException)
				{
					if(objDbTransaction!=null)
					{
						objDbTransaction.Rollback(); 
					}
					throw p_objInvalidValueException;
				}
				catch(RMAppException p_objRMAppException)
				{
					if(objDbTransaction!=null)
					{
						objDbTransaction.Rollback(); 
					}

					throw p_objRMAppException;
				}
				catch(Exception p_objException)
				{
					if(objDbTransaction!=null)
					{
						objDbTransaction.Rollback(); 
					}
					throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.Error"),p_objException);
				}
				finally
				{
					if(objDbTransaction!=null)
					{
						objDbTransaction.Dispose();
					}
				
					if(objDbConnection!=null)
					{
						objDbConnection.Close();
                        objDbConnection.Dispose();
					}
				}
			}

			//5. fill dom with new data
			//note, create same dom as EditPayment() sub in nonocc.asp

			XmlElement objDocumentElement=null;
			XmlElement objRow=null;

			objDocumentElement=p_objTable.DocumentElement;
			objDocumentElement.SetAttribute("batchid", p_iBatchId.ToString());
			objDocumentElement.SetAttribute("paymentnum", p_iPaymentNum.ToString());
			objDocumentElement.SetAttribute("claimant", objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName());
			objDocumentElement.SetAttribute("claimnumber", objClaim.ClaimNumber);
			objDocumentElement.SetAttribute("gross", string.Format("{0:0.00}",dblNewGross));
			objDocumentElement.SetAttribute("fromdate", Conversion.GetDBDateFormat(sNewFrom,"d"));
			objDocumentElement.SetAttribute("todate", Conversion.GetDBDateFormat(sNewTo,"d"));
    
			objRow=p_objTable.CreateElement("row");
			objDocumentElement.AppendChild(objRow);
			objRow.SetAttribute("class", "rowlight");
			objRow.SetAttribute("title", "Net Payment:");
			objRow.SetAttribute("value", string.Format("{0:0.00}",dblNet));
    
			objRow=p_objTable.CreateElement("row");
			objDocumentElement.AppendChild(objRow);
			objRow.SetAttribute("class", "rowlight");
			objRow.SetAttribute("title", "Federal Tax");
			objRow.SetAttribute("value", string.Format("{0:0.00}",dblFed));

			objRow=p_objTable.CreateElement("row");
			objDocumentElement.AppendChild(objRow);
			objRow.SetAttribute("class", "rowlight");
			objRow.SetAttribute("title", "Social Security Tax");
			objRow.SetAttribute("value", string.Format("{0:0.00}",dblSS));

			objRow=p_objTable.CreateElement("row");
			objDocumentElement.AppendChild(objRow);
			objRow.SetAttribute("class", "rowlight");
			objRow.SetAttribute("title", "Medicare Tax");
			objRow.SetAttribute("value", string.Format("{0:0.00}",dblMed));
			
			objRow=p_objTable.CreateElement("row");
			objDocumentElement.AppendChild(objRow);
			objRow.SetAttribute("class", "rowlight");
			objRow.SetAttribute("title", "State Tax");
			objRow.SetAttribute("value", string.Format("{0:0.00}",dblState));
		}

         */
          
        //Modified the Function defination for GHS Enhancment //pmittal5 Mits 14841
        private void FillNonOccPaymentDomAsPerRMWorld(Claim objClaim, ref XmlDocument p_objTable, string p_sFrom,
                                          string p_sTo, double p_dblGross, int p_iClaimId, int p_iBatchId,
                                          int p_iPaymentNum, bool p_bReCalc,long lAutoSplitId ,double dblSuppPayment ,double dPensionOffset , double dSSOffset , double dOtherOffset ,
                                          double OtherOffset1, double OtherOffset2, double OtherOffset3, double PostTaxDed1, double PostTaxDed2,bool bAmountChange ,string p_sPrintDate , string p_sCheckMemo)
        {
            DisabilityPlan objPlan = null;
            DisabilityClass objDisClass = null;
            PiEmployee objEmp = null;
            int iPiEid = 0;
            string sSQL = string.Empty;
            DbReader objReader = null;
            int iPrefPaySchCode = 0;
            string sBenCalcPayStart = string.Empty;
            string sBenCalcPayTo = string.Empty;
            string sPayPeriodStart = string.Empty;
            int iBiWeek = 0;
            int iPayPeriod = 0;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            string sWorkDaysArray = string.Empty;
            bool[] bTaxes = new bool[] { false, false, false, false };
            int iTaxFlags = 0;
            TaxRate dtpTaxes = new TaxRate();
            bool bError = false;
            double dblNewGross = p_dblGross;
            double dblHourlyRate = 0;
            double dblWeeklyRate = 0;
            int iWorkDaysCount = 0;
            double dblDailyBenefit = 0;
            double dblWeeklyBenefit = 0;
            DateTime dFrom = DateTime.Now;
            DateTime dTo = DateTime.Now;
            int iCount = 0;
            double dblTemp = 0;
            TimeSpan ts = new TimeSpan(1);
            DateTime dNewFrom = DateTime.Now;
            DateTime dNewTo = DateTime.Now;
            DateTime dTemp = DateTime.Now;
            DateTime dFirstToDate = DateTime.Now;
            int iPrefSched = 0;

            double dDailyAmount = 0;
            double dDailySuppAmount = 0;
            double dGrossPayment = 0;
            double dGrossSupp = 0;
            double dFedTaxAmount = 0;
            double dSSTaxAmount = 0;
            double dMedicareTaxAmount = 0;
            double dStateTaxAmount = 0;
            double dTaxTotal = 0;
            double dGrossNetOffsets = 0;
            double dTotalOffset = 0.0;               // MITS 14186
            const double dOneCent = 0.01;            //MITS 14186
            string sBenefitStartDate = "";
            int iBenefitDays = 0;
            int[] iWorkWeek = new int[7];
            long p_lAutoSplitId = 0;
            long lThisBatchID = 0;
            long lClaimantEID = 0;
            string sAutoTransID = "";
            string sCheckMemo = "";
            string slblDailyAmount = "";
            string slblDailySuppAmount = "";
            string slblClaimID = "";
            long lStateID = 0;
            string sOldFromDate = "";
            string sOldToDate = "";
            bool[] p_bTaxes = new bool[4];
            TaxRate p_dtpTaxes = new TaxRate();
            DbReader objTempDbReader = null;
           
            
            objPlan = objClaim.DisabilityPlan;
            iPiEid = objClaim.PrimaryClaimant.ClaimantEid;
            
            foreach (DisabilityClass objClass in objPlan.ClassList)
            {
                if (objClass.ClassRowId == objClaim.ClassRowId)
                {
                    objDisClass = objClass;
                    break;
                }
            }
            if (objDisClass == null)
                throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.DisClassNotFound",m_iClientId), objClaim.ClaimId.ToString(), objClaim.DisabilityPlan.PlanId.ToString()));

            // Pull the preferred schedule selected on the disability plan

            if (m_objCache.GetShortCode(objClaim.DisabilityPlan.PrefPaySchCode).ToLower() == "pw")
            {
                iPrefSched = 1;
            }
            if (m_objCache.GetShortCode(objClaim.DisabilityPlan.PrefPaySchCode).ToLower() == "pb")
            {
                iPrefSched = 2;
            }
            if (m_objCache.GetShortCode(objClaim.DisabilityPlan.PrefPaySchCode).ToLower() == "pm")
            {
                iPrefSched = 3;
            }
            
            objEmp = objClaim.PrimaryPiEmployee;
            GetWorkWeek(objDisClass , objClaim , ref iWorkWeek);

            //pmittal5 MITS 12620 07/16/08
            lStateID = objClaim.PrimaryPiEmployee.PiEntity.StateId;
            //MITS 12902
            //GetTaxes(lStateID, ref p_bTaxes, ref p_dtpTaxes);
            GetTaxes(objClaim, ref p_bTaxes, ref p_dtpTaxes);
            p_dtpTaxes.dblTaxablePercent = objDisClass.TaxablePercent / 100;
            
            iBenefitDays = GetBenefitDays(Conversion.ToDate(p_sFrom),Conversion.ToDate(p_sTo), iPrefSched, iWorkWeek);

            GetBatchInfo(objDisClass, lAutoSplitId, ref lThisBatchID, ref lClaimantEID, ref sAutoTransID, ref sCheckMemo,  ref slblDailyAmount,  ref slblDailySuppAmount,ref slblClaimID, ref lStateID, ref sPayPeriodStart , ref sOldFromDate , ref sOldToDate);
        
            if(objDisClass.ActualMonth)
            {
                GetDailyAmount(sPayPeriodStart, ref slblDailyAmount, ref slblDailySuppAmount, p_sFrom);
            }
            
    
            dDailyAmount = Conversion.ConvertStrToDouble(slblDailyAmount);
            //kdb 05/05/2005
            dDailySuppAmount = Conversion.ConvertStrToDouble(slblDailySuppAmount);

            if (bAmountChange)
            {
                dGrossPayment = Math.Round(p_dblGross, 2, MidpointRounding.AwayFromZero);   //MITS 14186

                if (dblSuppPayment > 0)
                    dGrossSupp = Math.Round(dblSuppPayment, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                else
                    dGrossSupp = 0;
            }
            else
            {
                if (!IsSuppPayment(sAutoTransID, objDisClass))
                    dGrossPayment = Math.Round(dDailyAmount * iBenefitDays, 2, MidpointRounding.AwayFromZero);  //MITS 14186

                else
                    dGrossPayment = Math.Round(dDailySuppAmount * iBenefitDays, 2, MidpointRounding.AwayFromZero);  //MITS 14186


                if (dblSuppPayment > 0)
                    dGrossSupp = Math.Round(dDailySuppAmount * iBenefitDays, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                else
                    dGrossSupp = 0;
            }
       
    
            //kdb 05/02/2005  Set up Offsets
            dPensionOffset = Math.Round(dPensionOffset, 2, MidpointRounding.AwayFromZero);  //MITS 14186

            dSSOffset = Math.Round(dSSOffset, 2, MidpointRounding.AwayFromZero);   //MITS 14186

            dOtherOffset = Math.Round(dOtherOffset, 2, MidpointRounding.AwayFromZero);  //MITS 14186

            dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset), 2, MidpointRounding.AwayFromZero);
            dGrossNetOffsets = Math.Round((dGrossPayment + dGrossSupp - dTotalOffset), 2, MidpointRounding.AwayFromZero);
            //MITS 14186  
            //As discussed with Mike Hamann , if  dPayment - dTotalOffset = 0.01
            //deduct 1 cent from the offset having maximum value and make net payment = 0

            //Animesh Inserted For GHS Enhancment  //pmittal5 Mits 14841
            // 3 more pre tax offsets need to be deducted 
            dGrossNetOffsets = Math.Round((dGrossNetOffsets - OtherOffset1 - OtherOffset2 - OtherOffset3), 2, MidpointRounding.AwayFromZero);
            //Animesh Insertion ends


            if (dGrossNetOffsets < 0 && dGrossNetOffsets >= -0.01)
            {

                double[] dArrayofOffsets = new double[] { dPensionOffset, dSSOffset, dOtherOffset, OtherOffset1, OtherOffset2, OtherOffset3 };
                int iIndex = GetMaxOffsetIndex(dArrayofOffsets);

                //deducting 1 Cent from offset having maximum value
                dArrayofOffsets[iIndex] = Math.Round((dArrayofOffsets[iIndex] - dOneCent), 2, MidpointRounding.AwayFromZero);

                dPensionOffset = dArrayofOffsets[0];
                dSSOffset = dArrayofOffsets[1];
                dOtherOffset = dArrayofOffsets[2];
                OtherOffset1 = dArrayofOffsets[3];
                OtherOffset2 = dArrayofOffsets[4];
                OtherOffset3 = dArrayofOffsets[5];
                dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset + OtherOffset1 + OtherOffset2 + OtherOffset3), 2, MidpointRounding.AwayFromZero);
                dGrossNetOffsets = 0.00;


            }
            //Pawan  MITS:12620  06/27/08
            iTaxFlags = objClaim.TaxFlags;
            if (iTaxFlags == 0)
            {
                p_dtpTaxes.bFed = objDisClass.WithholdFedItax;
                p_dtpTaxes.bSS = objDisClass.WithholdFica;
                p_dtpTaxes.bMed = objDisClass.WithholdMedicare;
                p_dtpTaxes.bState = objDisClass.WithholdState;
            }
            else
            {
                p_dtpTaxes.bFed = ((2 | iTaxFlags) == iTaxFlags);
                p_dtpTaxes.bSS = ((4 | iTaxFlags) == iTaxFlags);
                p_dtpTaxes.bMed = ((8 | iTaxFlags) == iTaxFlags);
                p_dtpTaxes.bState = ((16 | iTaxFlags) == iTaxFlags);
            }
            //End 

            //MITS 21531 08/19/2010 - mcapps2 Start
            UpdateTaxFlagsForExistingSplits(ref p_dtpTaxes, p_iBatchId, p_iPaymentNum);
            //MITS 21531 08/19/2010 - mcapps2 End

            //Pawan  MITS:12620  06/27/08
            //if (p_bTaxes[0])
            if(p_dtpTaxes.bFed)
            {
               if (p_dtpTaxes.dblTaxablePercent > 0)
                   dFedTaxAmount = Math.Round(dGrossNetOffsets * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.FED_TAX_RATE, 2, MidpointRounding.AwayFromZero); //MITS 14186
               else
                   dFedTaxAmount = Math.Round(dGrossNetOffsets * p_dtpTaxes.FED_TAX_RATE, 2, MidpointRounding.AwayFromZero); //MITS 14186
            }
            else
               dFedTaxAmount = 0;

            //Pawan  MITS:12620  06/27/08
            //if (p_bTaxes[1])
            if(p_dtpTaxes.bSS)
            {
              if (p_dtpTaxes.dblTaxablePercent > 0)
                  dSSTaxAmount = Math.Round(dGrossNetOffsets * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.SS_TAX_RATE, 2, MidpointRounding.AwayFromZero); //MITS 14186
              else
                  dSSTaxAmount = Math.Round(dGrossNetOffsets * p_dtpTaxes.SS_TAX_RATE, 2, MidpointRounding.AwayFromZero);//MITS 14186
            }
            else
              dSSTaxAmount = 0;

            //Pawan  MITS:12620  06/27/08
            //if (p_bTaxes[2])
            if(p_dtpTaxes.bMed)
            {
             if (p_dtpTaxes.dblTaxablePercent > 0)
                 dMedicareTaxAmount = Math.Round(dGrossNetOffsets * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.MEDICARE_TAX_RATE, 2, MidpointRounding.AwayFromZero);  //MITS 14186
             else
                 dMedicareTaxAmount = Math.Round(dGrossNetOffsets * p_dtpTaxes.MEDICARE_TAX_RATE, 2, MidpointRounding.AwayFromZero);  //MITS 14186
            }
            else
                dMedicareTaxAmount = 0;

            //Pawan  MITS:12620  06/27/08
            //if (p_bTaxes[3])
            if(p_dtpTaxes.bState)
            {
                if (p_dtpTaxes.dblTaxablePercent > 0)
                    dStateTaxAmount = Math.Round(dGrossNetOffsets * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.STATE_TAX_RATE, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                else
                    dStateTaxAmount = Math.Round(dGrossNetOffsets * p_dtpTaxes.STATE_TAX_RATE, 2, MidpointRounding.AwayFromZero);  //MITS 14186
            }
            else
                dStateTaxAmount = 0;

            dTaxTotal = Math.Round((dFedTaxAmount + dSSTaxAmount + dMedicareTaxAmount + dStateTaxAmount), 2, MidpointRounding.AwayFromZero); //MITS 14186 
            double dNetAmount = Math.Round((dGrossNetOffsets - dTaxTotal), 2, MidpointRounding.AwayFromZero);

            //Animesh Inserted For GHS Enhancement  //pmittal5 Mits 14841
            //Now we need to deduct to post tax offsets 
            dNetAmount = Math.Round(dNetAmount - (PostTaxDed1 + PostTaxDed2), 2, MidpointRounding.AwayFromZero); 
            //Animesh Insertion ends
     
            //finding payment number and updating it
            sSQL = "SELECT PAY_NUMBER FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString() + " AND FUNDS_AUTO.AUTO_TRANS_ID = " + sAutoTransID;
            
            //execute sql
            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    p_iPaymentNum = Conversion.ConvertObjToInt(objReader.GetValue("PAY_NUMBER"), m_iClientId);
                }
            }
            if (objReader != null)
                objReader.Close();

            //5. fill dom with new data
            //note, create same dom as EditPayment() sub in nonocc.asp

            XmlElement objDocumentElement = null;
            XmlElement objRow = null;

            objDocumentElement = p_objTable.DocumentElement;
            objDocumentElement.SetAttribute("batchid", p_iBatchId.ToString());
            objDocumentElement.SetAttribute("paymentnum", p_iPaymentNum.ToString());
            objDocumentElement.SetAttribute("claimant", objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName());
            objDocumentElement.SetAttribute("claimnumber", objClaim.ClaimNumber);
            objDocumentElement.SetAttribute("gross", string.Format("{0:0.00}", dGrossPayment));
            objDocumentElement.SetAttribute("supppayment", string.Format("{0:0.00}", dGrossSupp));
            objDocumentElement.SetAttribute("fromdate", Conversion.GetDBDateFormat(p_sFrom, "d"));
            objDocumentElement.SetAttribute("todate", Conversion.GetDBDateFormat(p_sTo, "d"));
            objDocumentElement.SetAttribute("pensionoffset", string.Format("{0:0.00}", dPensionOffset));
            objDocumentElement.SetAttribute("ssoffset", string.Format("{0:0.00}", dSSOffset));
            objDocumentElement.SetAttribute("oioffset", string.Format("{0:0.00}", dOtherOffset));
            
            //Added by Ravneet Kaur for GHS
            objDocumentElement.SetAttribute("othoffset1", string.Format("{0:0.00}", OtherOffset1 ));
            objDocumentElement.SetAttribute("othoffset2", string.Format("{0:0.00}", OtherOffset2 ));
            objDocumentElement.SetAttribute("othoffset3", string.Format("{0:0.00}", OtherOffset3 ));
            objDocumentElement.SetAttribute("psttaxded1", string.Format("{0:0.00}", PostTaxDed1));
            objDocumentElement.SetAttribute("psttaxded2", string.Format("{0:0.00}", PostTaxDed2));
            /////////////////////////////////

            objDocumentElement.SetAttribute("grosstotalnetoffsets", string.Format("{0:0.00}", dGrossNetOffsets));
            objDocumentElement.SetAttribute("autosplitid", lAutoSplitId.ToString());
            objDocumentElement.SetAttribute("printdate", Conversion.GetDBDateFormat(p_sPrintDate, "d"));
            objDocumentElement.SetAttribute("amountchange", bAmountChange.ToString());
            objDocumentElement.SetAttribute("checkmemo", p_sCheckMemo);
            
            objRow = p_objTable.CreateElement("row");
            objDocumentElement.AppendChild(objRow);
            objRow.SetAttribute("class", "rowlight");
            objRow.SetAttribute("title", "Net Payment:");
            objRow.SetAttribute("id", "Net");   //pmittal5 Mits 14253
            objRow.SetAttribute("value", string.Format("{0:0.00}", dNetAmount));

            objRow = p_objTable.CreateElement("row");
            objDocumentElement.AppendChild(objRow);
            objRow.SetAttribute("class", "rowlight");
            objRow.SetAttribute("title", "Federal Tax");
            objRow.SetAttribute("id", "FederalTax");  //pmittal5 Mits 14253
            objRow.SetAttribute("value", string.Format("{0:0.00}", dFedTaxAmount));

            objRow = p_objTable.CreateElement("row");
            objDocumentElement.AppendChild(objRow);
            objRow.SetAttribute("class", "rowlight");
            objRow.SetAttribute("title", "Social Security Tax");
            objRow.SetAttribute("id", "SocialSecurityTax");  //pmittal5 Mits 14253
            objRow.SetAttribute("value", string.Format("{0:0.00}", dSSTaxAmount));

            objRow = p_objTable.CreateElement("row");
            objDocumentElement.AppendChild(objRow);
            objRow.SetAttribute("class", "rowlight");
            objRow.SetAttribute("title", "Medicare Tax");
            objRow.SetAttribute("id", "MedicareTax");  //pmittal5 Mits 14253
            objRow.SetAttribute("value", string.Format("{0:0.00}", dMedicareTaxAmount));

            objRow = p_objTable.CreateElement("row");
            objDocumentElement.AppendChild(objRow);
            objRow.SetAttribute("class", "rowlight");
            objRow.SetAttribute("title", "State Tax");
            objRow.SetAttribute("id", "StateTax");  //pmittal5 Mits 14253
            objRow.SetAttribute("value", string.Format("{0:0.00}", dStateTaxAmount));

            //Save the new values in the database if p_bReCalc is false
            if (!p_bReCalc)
            {
                DbConnection objDbConnection = null;
                DbTransaction objDbTransaction = null;
                DbReader objMainReader = null;
                bool bSuppSplitFlag = false;
                //bool bSupp = false;
                int iSupp = 0;
                string sFASSQL = string.Empty;
                long lPensionOffTransTypeCodeID = 0;
                long lSSOffTransTypeCodeID = 0;
                long lOtherOffTransTypeCodeID = 0;
                //Added by RKaur for GHS 
                long lOthOff1TransTypeCodeID = 0;
                long lOthOff2TransTypeCodeID = 0;
                long lOthOff3TransTypeCodeID = 0;
                long lPstTax1TransTypeCodeID = 0;
                long lPstTax2TransTypeCodeID = 0;
                //End

                try
                {
                    //Raman Bhatia: Get the Transaction Types for Offsets
                    //MITS 14253 Umesh : Net amount should not be negative
                    if (dNetAmount<0)
                        throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.SaveEdit.Negative NetAmountError",m_iClientId), "Net Amount"));

                    //MITS 14253 End
                    sSQL = "SELECT * FROM OFFSET_MAPPING";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
                    //loop and get OFFSET trans type code
                    while (objReader.Read())
                    {
                        if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "PENSION")
                            lPensionOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                        if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "SOCIAL_SECURITY")
                            lSSOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                        if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHER_INCOME")
                            lOtherOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                        //Added by RKaur
                        if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET1")
                            lOthOff1TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                        if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET2")
                            lOthOff2TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                        if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET3")
                            lOthOff3TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                        if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "POST_TAX_DED1")
                            lPstTax1TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                        if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "POST_TAX_DED2")
                            lPstTax2TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                        ///////////////////////////

                    }
                    objReader.Close();

                    //begin the transaction
                    //must open a separate database for transaction or get connection leak
                    objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                    objDbConnection.Open();
                    objDbTransaction = objDbConnection.BeginTransaction();

                    if (dblSuppPayment > 0)
                        bSuppSplitFlag = true;
                    else
                        bSuppSplitFlag = false;

                    string sDTTM = Conversion.ToDbDateTime(DateTime.Now);

                    sSQL = "SELECT SUPP_PAYMENT_FLAG FROM FUNDS_AUTO WHERE";
                    sSQL = sSQL + " FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString();
                    sSQL = sSQL + " AND FUNDS_AUTO.AUTO_TRANS_ID = " + sAutoTransID;
                    sSQL = sSQL + " AND FUNDS_AUTO.PAY_NUMBER = (SELECT PAY_NUMBER FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString();
                    sSQL = sSQL + " AND FUNDS_AUTO.AUTO_TRANS_ID = " + sAutoTransID + ")";

                    //execute sql
                    objReader = objDbConnection.ExecuteReader(sSQL.ToString(), objDbTransaction);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            //pmittal5 Mits 15804 04/24/09 - SUPP_PAYMENT_FLAG can also be -1
                            //bSupp = Conversion.ConvertObjToBool(objReader.GetValue("SUPP_PAYMENT_FLAG"));
                            iSupp = Conversion.ConvertObjToInt(objReader.GetValue("SUPP_PAYMENT_FLAG"), m_iClientId);
                        }
                    }
                    if (objReader != null)
                        objReader.Close();
                    
                    //pmittal5 Mits 15804 04/30/09 - Handling Third Party Payments in case of Separate supplement flag
                    if (bSuppSplitFlag)
                    {
                        sSQL = "SELECT * FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString() + " AND FUNDS_AUTO.PAY_NUMBER = (SELECT PAY_NUMBER FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString() + " AND FUNDS_AUTO.AUTO_TRANS_ID = " + sAutoTransID + ")";
                        sSQL = sSQL + " ORDER BY AUTO_TRANS_ID";
                    }
                    else
                    { 
                        if(iSupp != 0)  //Supplement Payment
                        {
                            sSQL = "SELECT * FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString() + " AND FUNDS_AUTO.PAY_NUMBER = (SELECT PAY_NUMBER FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString() + " AND FUNDS_AUTO.AUTO_TRANS_ID = " + sAutoTransID + ")";
                            sSQL = sSQL + " AND FUNDS_AUTO.AUTO_TRANS_ID >= " + sAutoTransID;
                            sSQL = sSQL + " ORDER BY AUTO_TRANS_ID";
                        }
                        else           //Non-Supplement Payment
                        {
                            string sSuppAutoTransId = string.Empty;
                            objTempDbReader = DbFactory.ExecuteReader(m_sConnectionString, "SELECT AUTO_TRANS_ID FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString() + " AND FUNDS_AUTO.PAY_NUMBER = (SELECT PAY_NUMBER FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString() + " AND FUNDS_AUTO.AUTO_TRANS_ID = " + sAutoTransID + ") AND SUPP_PAYMENT_FLAG <> 0");
                            if (objTempDbReader != null && objTempDbReader.Read())
                            {
                                sSuppAutoTransId = Common.Conversion.ConvertObjToStr(objTempDbReader.GetValue("AUTO_TRANS_ID"));
                                objTempDbReader.Close();
                                objTempDbReader.Dispose();
                            }
                            sSQL = "SELECT * FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString() + " AND FUNDS_AUTO.PAY_NUMBER = (SELECT PAY_NUMBER FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString() + " AND FUNDS_AUTO.AUTO_TRANS_ID = " + sAutoTransID + ")";
                            if(sSuppAutoTransId != "")
                                sSQL = sSQL + " AND FUNDS_AUTO.AUTO_TRANS_ID < " + sSuppAutoTransId.ToString();
                            sSQL = sSQL + " ORDER BY AUTO_TRANS_ID";                          
                        }
                    }//End - pmittal5
                    //execute sql
                    objMainReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objMainReader != null)
                    {
                        while (objMainReader.Read())
                        {
                            //pmittal5 Mits 14699 03/17/09 - Third party taxes were not getting updated
                            sAutoTransID = Conversion.ConvertObjToStr(objMainReader.GetValue("AUTO_TRANS_ID"));
                            if (lClaimantEID == Conversion.ConvertObjToInt64(objMainReader.GetValue("PAYEE_EID"), m_iClientId))
                            {
                                sFASSQL = FundsAutoUpdateSQLAsPerRMWorld(dNetAmount, iBenefitDays, p_sCheckMemo, p_sPrintDate, Conversion.ConvertStrToLong(sAutoTransID), iSupp); //Mits 15804 - Changed bSupp to iSupp
                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);

                                if (!bSuppSplitFlag)
                                {
                                    sSQL = "SELECT * FROM FUNDS_AUTO_SPLIT WHERE";
                                    sSQL = sSQL + " AUTO_TRANS_ID = " + sAutoTransID;
                                    sSQL = sSQL + " AND (SUPP_PAYMENT_FLAG = " + iSupp + " OR SUPP_PAYMENT_FLAG IS NULL)"; //Mits 15804 - Changed bSupp to iSupp
                                    sSQL = sSQL + " ORDER BY AUTO_SPLIT_ID";
                                }
                                else
                                {
                                    sSQL = "SELECT * FROM FUNDS_AUTO_SPLIT WHERE";
                                    sSQL = sSQL + " AUTO_TRANS_ID = " + sAutoTransID;
                                    sSQL = sSQL + " ORDER BY AUTO_SPLIT_ID";
                                }

                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                if (objReader != null)
                                {
                                    while (objReader.Read())
                                    {
                                        if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "0")  //And lAnyVarToLong(vDB_GetData(rs_2, "SUPP_PAYMENT_FLAG")) = 0 Then
                                        {
                                            sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dGrossPayment, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                            objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);

                                            //KDB 05/02/2005 Add in supp
                                        }
                                        else if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "8")
                                        {
                                            if (dGrossSupp > 0)
                                            {
                                                //sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(Math.Round(dGrossSupp , 2), p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID")));
                                                sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dGrossSupp, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                            else
                                            {
                                                sFASSQL = "DELETE FROM FUNDS_AUTO_SPLIT";
                                                sFASSQL = sFASSQL + " WHERE AUTO_SPLIT_ID = " + Conversion.ConvertObjToStr(objReader.GetValue("AUTO_SPLIT_ID"));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                        }
                                        //pmittal5  MITS:12620  07/11/08
                                        //else if (Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE")) == p_dtpTaxes.iFedTransTypeCodeID.ToString())
                                        else if(Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "1")
                                        {
                                            sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dFedTaxAmount * -1, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                            objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                        }
                                        //pmittal5  MITS:12620  07/11/08
                                        //else if (Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE")) == p_dtpTaxes.iSSTransTypeCodeID.ToString())
                                        else if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "2")
                                        {
                                            sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dSSTaxAmount * -1, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                            objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                        }
                                        //pmittal5  MITS:12620  07/11/08
                                        //else if (Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE")) == p_dtpTaxes.iMedicareTransTypeCodeID.ToString())
                                        else if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "3")
                                        {
                                            sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dMedicareTaxAmount * -1, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                            objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                        }
                                        //pmittal5  MITS:12620  07/11/08
                                        //else if (Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE")) == p_dtpTaxes.iStateTransTypeCodeID.ToString())
                                        else if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "4")
                                        {
                                            sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dStateTaxAmount * -1, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                            objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                        }
                                        //pmittal5  MITS:12620  07/11/08
                                        //else if (Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE")) == lPensionOffTransTypeCodeID.ToString())
                                        else if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "5")
                                        {
                                            if (dPensionOffset > 0)
                                            {
                                                sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dPensionOffset * -1, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                            else
                                            {
                                                sFASSQL = "DELETE FROM FUNDS_AUTO_SPLIT";
                                                sFASSQL = sFASSQL + " WHERE AUTO_SPLIT_ID = " + Conversion.ConvertObjToStr(objReader.GetValue("AUTO_SPLIT_ID"));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                        }
                                        //pmittal5  MITS:12620  07/11/08
                                        //else if (Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE")) == lSSOffTransTypeCodeID.ToString())
                                        else if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "6")
                                        {
                                            if (dSSOffset > 0)
                                            {
                                                sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dSSOffset * -1, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                            else
                                            {
                                                sFASSQL = "DELETE FROM FUNDS_AUTO_SPLIT";
                                                sFASSQL = sFASSQL + " WHERE AUTO_SPLIT_ID = " + Conversion.ConvertObjToStr(objReader.GetValue("AUTO_SPLIT_ID"));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                        }
                                        //pmittal5  MITS:12620  07/11/08
                                        //else if (Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE")) == lOtherOffTransTypeCodeID.ToString())
                                        else if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "7")
                                        {
                                            if (dOtherOffset > 0)
                                            {
                                                sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dOtherOffset * -1, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                            else
                                            {
                                                sFASSQL = "DELETE FROM FUNDS_AUTO_SPLIT";
                                                sFASSQL = sFASSQL + " WHERE AUTO_SPLIT_ID = " + Conversion.ConvertObjToStr(objReader.GetValue("AUTO_SPLIT_ID"));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                        }
                                        //***********************
                                        //Added by RKaur
                                        else if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "9")
                                        {
                                            if (OtherOffset1  > 0)
                                            {
                                                sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(OtherOffset1 * -1, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                            else
                                            {
                                                sFASSQL = "DELETE FROM FUNDS_AUTO_SPLIT";
                                                sFASSQL = sFASSQL + " WHERE AUTO_SPLIT_ID = " + Conversion.ConvertObjToStr(objReader.GetValue("AUTO_SPLIT_ID"));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                        }
                                        else if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "10")
                                        {
                                            if (OtherOffset2 > 0)
                                            {
                                                sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(OtherOffset2 * -1, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                            else
                                            {
                                                sFASSQL = "DELETE FROM FUNDS_AUTO_SPLIT";
                                                sFASSQL = sFASSQL + " WHERE AUTO_SPLIT_ID = " + Conversion.ConvertObjToStr(objReader.GetValue("AUTO_SPLIT_ID"));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                        }
                                        else if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "11")
                                        {
                                            if (OtherOffset3  > 0)
                                            {
                                                sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(OtherOffset3 * -1, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                            else
                                            {
                                                sFASSQL = "DELETE FROM FUNDS_AUTO_SPLIT";
                                                sFASSQL = sFASSQL + " WHERE AUTO_SPLIT_ID = " + Conversion.ConvertObjToStr(objReader.GetValue("AUTO_SPLIT_ID"));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                        }
                                        else if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "12")
                                        {
                                            if (PostTaxDed1  > 0)
                                            {
                                                sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(PostTaxDed1 * -1, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                            else
                                            {
                                                sFASSQL = "DELETE FROM FUNDS_AUTO_SPLIT";
                                                sFASSQL = sFASSQL + " WHERE AUTO_SPLIT_ID = " + Conversion.ConvertObjToStr(objReader.GetValue("AUTO_SPLIT_ID"));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                        }
                                        else if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "13")
                                        {
                                            if (PostTaxDed2  > 0)
                                            {
                                                sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(PostTaxDed2 * -1, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                            else
                                            {
                                                sFASSQL = "DELETE FROM FUNDS_AUTO_SPLIT";
                                                sFASSQL = sFASSQL + " WHERE AUTO_SPLIT_ID = " + Conversion.ConvertObjToStr(objReader.GetValue("AUTO_SPLIT_ID"));
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                            }
                                        }
                                        ///////////////////////
                                        //***********************

                                    }//end while objReader
                                }//end if objReader
                                if (objReader != null)
                                    objReader.Close();
                            }//end id

                            else if (p_dtpTaxes.iFedTaxEID == Conversion.ConvertObjToInt64(objMainReader.GetValue("PAYEE_EID"), m_iClientId))
                            {
                                //pmittal5  MITS:12620  07/11/08
                                //sFASSQL = FundsAutoUpdateSQLAsPerRMWorld(dFedTaxAmount, iBenefitDays, p_sCheckMemo, p_sPrintDate, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID")), 0);
                                sFASSQL = FundsAutoUpdateSQLAsPerRMWorld(dFedTaxAmount, iBenefitDays, p_sCheckMemo, p_sPrintDate, Conversion.ConvertObjToInt64(objMainReader.GetValue("AUTO_TRANS_ID"), m_iClientId), 0);    
                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);

                                    sSQL = "SELECT * FROM FUNDS_AUTO_SPLIT WHERE";
                                    sSQL = sSQL + " AUTO_TRANS_ID = " + sAutoTransID;
                                    sSQL = sSQL + " ORDER BY AUTO_SPLIT_ID";

                                    //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                    objReader = objDbConnection.ExecuteReader(sSQL, objDbTransaction);
                                    {
                                        if (objReader != null)
                                        {
                                            while (objReader.Read())
                                            {
                                                //pmittal5 MITS:12620 07/11/08
                                                //if (Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE")) == p_dtpTaxes.iFedTransTypeCodeID.ToString())
                                                if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "1")
                                                {
                                                    //sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dFedTaxAmount, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID")));
                                                    long lAutoSplit = Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId);
                                                    objReader.Close();
                                                    sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dFedTaxAmount, p_sFrom, p_sTo, lAutoSplit);
                                                    objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                                    break;
                                                }//end if
                                                //End - pmittal5
                                            }//end while
                                        }//end if
                                    }
                                    if (objReader != null)
                                        objReader.Close();
                            }//end else

                            else if (p_dtpTaxes.iSSTaxEID == Conversion.ConvertObjToInt64(objMainReader.GetValue("PAYEE_EID"), m_iClientId))
                            {
                                    //pmittal5  MITS:12620  07/11/08
                                    //sFASSQL = FundsAutoUpdateSQLAsPerRMWorld(dSSTaxAmount, iBenefitDays, p_sCheckMemo, p_sPrintDate, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID")), 0);
                                sFASSQL = FundsAutoUpdateSQLAsPerRMWorld(dSSTaxAmount, iBenefitDays, p_sCheckMemo, p_sPrintDate, Conversion.ConvertObjToInt64(objMainReader.GetValue("AUTO_TRANS_ID"), m_iClientId), 0);
                                    objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);

                                    sSQL = "SELECT * FROM FUNDS_AUTO_SPLIT WHERE";
                                    sSQL = sSQL + " AUTO_TRANS_ID = " + sAutoTransID;
                                    sSQL = sSQL + " ORDER BY AUTO_SPLIT_ID";

                                    //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                    objReader = objDbConnection.ExecuteReader(sSQL, objDbTransaction);
                                    if (objReader != null)
                                    {
                                        while (objReader.Read())
                                        {
                                            //pmittal5 MITS:12620 07/11/08
                                            //if (Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE")) == p_dtpTaxes.iSSTransTypeCodeID.ToString())
                                            if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "2")
                                            {
                                                //sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dSSTaxAmount, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID")));
                                                long lAutoSplit = Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId);
                                                objReader.Close();
                                                sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dSSTaxAmount, p_sFrom, p_sTo, lAutoSplit);
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                                break;
                                            }//end if
                                        }//end while
                                    }//end if
                                    if (objReader != null)
                                        objReader.Close();
                            }//end else

                            else if (p_dtpTaxes.iMedicareTaxEID == Conversion.ConvertObjToInt64(objMainReader.GetValue("PAYEE_EID"), m_iClientId))
                            {
                                    //pmittal5  MITS:12620  07/11/08
                                    //sFASSQL = FundsAutoUpdateSQLAsPerRMWorld(dMedicareTaxAmount, iBenefitDays, p_sCheckMemo, p_sPrintDate, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID")), 0);
                                sFASSQL = FundsAutoUpdateSQLAsPerRMWorld(dMedicareTaxAmount, iBenefitDays, p_sCheckMemo, p_sPrintDate, Conversion.ConvertObjToInt64(objMainReader.GetValue("AUTO_TRANS_ID"), m_iClientId), 0);
                                    objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);

                                    sSQL = "SELECT * FROM FUNDS_AUTO_SPLIT WHERE";
                                    sSQL = sSQL + " AUTO_TRANS_ID = " + sAutoTransID;
                                    sSQL = sSQL + " ORDER BY AUTO_SPLIT_ID";

                                    //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                    objReader = objDbConnection.ExecuteReader(sSQL, objDbTransaction);
                                    if (objReader != null)
                                    {
                                        while (objReader.Read())
                                        {
                                            //pmittal5 MITS:12620 07/11/08
                                            //if (Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE")) == p_dtpTaxes.iMedicareTransTypeCodeID.ToString())
                                            if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "3")
                                            {
                                                //sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dMedicareTaxAmount, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID")));
                                                long lAutoSplit = Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId);
                                                objReader.Close();
                                                sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dMedicareTaxAmount, p_sFrom, p_sTo, lAutoSplit);
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                                break;
                                            }//end if
                                        }//end while
                                    }//end if
                                    if (objReader != null)
                                        objReader.Close();
                            }//end else

                            else if (p_dtpTaxes.iStateTaxEID == Conversion.ConvertObjToInt64(objMainReader.GetValue("PAYEE_EID"), m_iClientId))
                            {
                                    //pmittal5  MITS:12620  07/11/08
                                    //sFASSQL = FundsAutoUpdateSQLAsPerRMWorld(dStateTaxAmount, iBenefitDays, p_sCheckMemo, p_sPrintDate, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID")), 0);
                                sFASSQL = FundsAutoUpdateSQLAsPerRMWorld(dStateTaxAmount, iBenefitDays, p_sCheckMemo, p_sPrintDate, Conversion.ConvertObjToInt64(objMainReader.GetValue("AUTO_TRANS_ID"), m_iClientId), 0);
                                    objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);

                                    sSQL = "SELECT * FROM FUNDS_AUTO_SPLIT WHERE";
                                    sSQL = sSQL + " AUTO_TRANS_ID = " + sAutoTransID;
                                    sSQL = sSQL + " ORDER BY AUTO_SPLIT_ID";

                                    //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                    objReader = objDbConnection.ExecuteReader(sSQL, objDbTransaction);
                                    if (objReader != null)
                                    {
                                        while (objReader.Read())
                                        {
                                            //pmittal5 MITS:12620 07/11/08
                                            //if (Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE")) == p_dtpTaxes.iStateTransTypeCodeID.ToString())
                                            if (Conversion.ConvertObjToStr(objReader.GetValue("TAG")) == "4")
                                            {
                                                //sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dStateTaxAmount, p_sFrom, p_sTo, Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID")));
                                                long lAutoSplit = Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_SPLIT_ID"), m_iClientId);
                                                objReader.Close();
                                                sFASSQL = FundsAutoSplitUpdateSQLAsPerRMWorld(dStateTaxAmount, p_sFrom, p_sTo, lAutoSplit);
                                                objDbConnection.ExecuteNonQuery(sFASSQL, objDbTransaction);
                                                break;
                                            }//end if
                                        }//end while
                                    }//end if
                                    if (objReader != null)
                                        objReader.Close();
                            }//end else

                           // }//end while objReader
                        }//end mainreader while
                    }//end mainreader if

                    //committing transaction
                    objDbTransaction.Commit();
                }

                catch (InitializationException p_objInitializationException)
                {
                    if (objDbTransaction != null)
                        objDbTransaction.Rollback();
                    throw p_objInitializationException;
                    
                }
                catch (InvalidValueException p_objInvalidValueException)
                {
                    if (objDbTransaction != null)
                    {
                        objDbTransaction.Rollback();
                    }
                    throw p_objInvalidValueException;
                }
                catch (RMAppException p_objRMAppException)
                {
                    if (objDbTransaction != null)
                    {
                        objDbTransaction.Rollback();
                    }

                    throw p_objRMAppException;
                }
                catch (Exception p_objException)
                {
                    if (objDbTransaction != null)
                    {
                        objDbTransaction.Rollback();
                    }
                    throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.Error",m_iClientId), p_objException);
                }
                finally
                {
                    if (objDbTransaction != null)
                    {
                        objDbTransaction.Dispose();
                        objDbTransaction = null;
                    }

                    if (objDbConnection != null)
                    {
                        objDbConnection.Close();
                        objDbConnection = null;
                    }
                    if (objMainReader != null)
                    {
                        objMainReader.Close();
                        objMainReader.Dispose();
                    }
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                    if (objTempDbReader != null)
                    {
                        objTempDbReader.Close();
                        objTempDbReader.Dispose();
                    }
                }
            }
        }


        private void UpdateTaxFlagsForExistingSplits(ref TaxRate p_dtpTaxes, int iBatchId, int iPaymentNum)
        {
            //MITS 21531 08/19/2010 - mcapps2 Start
            string sSQL = string.Empty;
            string sThisCTLNumber = string.Empty;
            DbConnection objDbConnection = null;
            DbTransaction objDbTransaction = null;

            objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
            objDbConnection.Open();
            
            p_dtpTaxes.bFed = false;
            p_dtpTaxes.bSS = false;
            p_dtpTaxes.bMed = false;
            p_dtpTaxes.bState = false;


            sSQL = "SELECT CTL_NUMBER FROM FUNDS_AUTO WHERE AUTO_BATCH_ID = " + iBatchId + " AND PAY_NUMBER = " + iPaymentNum;

            //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
            using (DbReader objReader = objDbConnection.ExecuteReader(sSQL, objDbTransaction))
            {
                while (objReader.Read())
                {
                    sThisCTLNumber = Conversion.ConvertObjToStr(objReader.GetValue("CTL_NUMBER")).Trim().ToUpper();
                    switch (sThisCTLNumber.Substring(sThisCTLNumber.Length -3))
                    {
                        case "T-1": //Federal Tax
                    
                            p_dtpTaxes.bFed = true;
                            break;

                        case "T-2":  //Social Security Tax
                    
                            p_dtpTaxes.bSS = true;
                            break;

                        case "T-3": //Medicare Tax
                    
                            p_dtpTaxes.bMed = true;
                            break;

                        case "T-4": //State Tax
                    
                            p_dtpTaxes.bState = true;
                            break;
                    }
                }
            }
            //MITS 21531 08/19/2010 - mcapps2 End
        }

		private void FillNonOccPaymentsDom(	Claim objClaim,ref XmlDocument p_objTable, int p_iTransTypeCode,
											int p_iAccountId, int p_iSysCmd, string sBenCalcPayStart , string sBenCalcPayTo ,string sBenefitsStartDate ,string sBenefitThroughDate)
		{
			XmlElement objDocumentElement=null;
			DisabilityClass objDisClass=null;
			string sShortCode=string.Empty;
			string sCodeDesc=string.Empty;
			string sPrdType=string.Empty;
			string sEligDate=string.Empty;
			//string sBenefitStartDate=string.Empty;
			double dblHourlyRate=0;
			double dblWeeklyRate=0;
			double dblTemp=0;
			double dblWeeklyBenefit=0;
			bool[] bTaxes=new bool[]{false,false,false,false};
			int iTaxFlags=0;
			TaxRate dtpTaxes=new TaxRate();
			string sWorkDaysArray=string.Empty;
			int iBatchId=0;
			DateTime dTemp=DateTime.Now;	

			objDocumentElement=p_objTable.DocumentElement;

			//check for required objects and fields
			if (objClaim.DisabilityPlan == null)
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccDomFailed.DisabilityPlanNotFound",m_iClientId), objClaim.ClaimId.ToString()));
    
			PiEmployee objEmp=null;
			objEmp=objClaim.PrimaryPiEmployee as PiEmployee;

			if (objEmp.PiRowId==0)
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccDomFailed.EmployeeNotFound",m_iClientId), objClaim.ClaimId.ToString()));

			if (objEmp.DateHired=="")
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccDomFailed.DateHired",m_iClientId), objClaim.PrimaryPiEmployee.EmployeeNumber));

			if(p_iTransTypeCode==0)
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentsDom.ParameterMissing",m_iClientId), "Transaction Type"));

			if(p_iAccountId==0)
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentsDom.ParameterMissing",m_iClientId), "Account"));

			foreach(DisabilityClass objClass in objClaim.DisabilityPlan.ClassList)
			{
				if (objClass.ClassRowId==objClaim.ClassRowId) 
				{
					objDisClass=objClass;
					break;
				}
			}
			if (objDisClass == null)
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccDomFailed.DisabilityPlanNotFound",m_iClientId), objClaim.ClaimId.ToString()));

			//Eligibility Date		
			dTemp=Conversion.ToDate(objEmp.DateHired);

			m_objCache.GetCodeInfo(objDisClass.BeneFromDtType,ref sShortCode,ref sCodeDesc); //Immediate : 1st Of Next Month

			if (sShortCode.ToUpper()!="DH")	//DH means eligible from date hired  + ELIG_BENE_PRD
			{
				dTemp=dTemp.AddMonths(1);
				dTemp=Conversion.ToDate(string.Format("{0:0000}",dTemp.Year) + string.Format("{0:00}",dTemp.Month) + "01");
			}
			m_objCache.GetCodeInfo(objDisClass.BenePrdType,ref sShortCode, ref sCodeDesc); //Days : Weeks : Months
			sPrdType=sShortCode.ToLower();
			if (sPrdType=="w")
				sPrdType="ww";

			sEligDate=AddPeriodToDate(Conversion.ToDbDate(dTemp),sPrdType,objDisClass.EligBenePrd);																					

			if (sEligDate.CompareTo(objClaim.DisabilFromDate)>0)
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccDomFailed.NotEligForBenefits",m_iClientId),Conversion.GetDBDateFormat(sEligDate,"d")));

			//Benefit Start Date & period
            GetBenefitPeriod(objDisClass, ref sBenefitsStartDate, ref sBenCalcPayStart, ref sBenCalcPayTo, objClaim);

            m_sBenefitStartDate = sBenefitsStartDate;
            m_sBenefitThroughDate = sBenefitThroughDate;
            //weekly benefit
			dblHourlyRate = objClaim.PrimaryPiEmployee.HourlyRate;
			if (objClaim.ClaimAWW == null)
				dblWeeklyRate=objClaim.PrimaryPiEmployee.WeeklyRate;
			else
			{
				dblWeeklyRate=objClaim.ClaimAWW.Aww;
				if (dblWeeklyRate==0)
					dblWeeklyRate=objClaim.PrimaryPiEmployee.WeeklyRate;
			}

			if (objDisClass.BenePrctgFlag)	//Percentage of Weekly Wage
			{
				dblTemp = objDisClass.BenePerAmt;
				if (dblTemp == 0)
					dblTemp = 100;   //0 means leave at 100%
				dblWeeklyBenefit = objClaim.PrimaryPiEmployee.WeeklyRate * (dblTemp / 100);
			}
			else if (objDisClass.BeneFlatAmtFlag)
			{
				dblWeeklyBenefit = objDisClass.BeneFlatAmt;
			}
			else if (objDisClass.BeneTdFlag)  //Table Driven (based on Weekly Rate or Hourly Rate)
			{
				dblWeeklyBenefit = dblWeeklyRate;	//dblWeeklyBenefit overridden in For loop if match is found
				foreach(DisClassTd objDisClassTd in objDisClass.TableList)
				{
					if (dblWeeklyRate <= objDisClassTd.WagesTo)
					{
						if (dblWeeklyRate >= objDisClassTd.WagesFrom)
						{
							dblWeeklyBenefit = objDisClassTd.WeeklyBenefit;
							break;
						}
					}
				}
			}

			// apply weekly cap
			if (objDisClass.WeeklyBeneCap > 0)
			{
				if (dblWeeklyBenefit > objDisClass.WeeklyBeneCap)
					dblWeeklyBenefit = objDisClass.WeeklyBeneCap;
			}

			//Tax Deduction checkboxes
			m_objCache.GetStateInfo(objClaim.PrimaryClaimant.ClaimantEntity.StateId, ref sShortCode, ref sCodeDesc);
	        //MITS 12902 : Umesh
			//GetTaxes(objClaim.PrimaryClaimant.ClaimantEntity.StateId, ref bTaxes, ref dtpTaxes);
            GetTaxes(objClaim, ref bTaxes, ref dtpTaxes);
	   			
			iTaxFlags = objClaim.TaxFlags;

			if (iTaxFlags==0)	//mwc only set flags if they have not been previously saved.  1 means has been saved
			{
				dtpTaxes.bFed = objDisClass.WithholdFedItax;
				dtpTaxes.bSS = objDisClass.WithholdFica;
				dtpTaxes.bMed = objDisClass.WithholdMedicare;
				dtpTaxes.bState = objDisClass.WithholdState;		
			}
			else
			{
				//objClaim.taxflags is bitmask: fed = 2,ss = 4,medicare = 8,state = 16
				dtpTaxes.bFed = ((2 | iTaxFlags)==iTaxFlags);
				dtpTaxes.bSS = ((4 | iTaxFlags)==iTaxFlags);
				dtpTaxes.bMed = ((8 | iTaxFlags)==iTaxFlags);
				dtpTaxes.bState = ((16 | iTaxFlags)==iTaxFlags);
			}
			dtpTaxes.dblTaxablePercent = objDisClass.TaxablePercent/100;

			//days worked
			if (objDisClass.Day7WorkWeek)
				sWorkDaysArray = "-1,-1,-1,-1,-1,-1,-1";
			else if (objDisClass.Day5WorkWeek)
				sWorkDaysArray = "0,-1,-1,-1,-1,-1,0";
			else	//objclass.ACTUAL_WORK_WEEK or not set
			{
                sWorkDaysArray = Conversion.ConvertBoolToInt(objEmp.WorkMonFlag, m_iClientId).ToString() + "," +
                    Conversion.ConvertBoolToInt(objEmp.WorkTueFlag, m_iClientId).ToString() + "," +
                    Conversion.ConvertBoolToInt(objEmp.WorkWedFlag, m_iClientId).ToString() + "," +
                    Conversion.ConvertBoolToInt(objEmp.WorkThuFlag, m_iClientId).ToString() + "," +
                    Conversion.ConvertBoolToInt(objEmp.WorkFriFlag, m_iClientId).ToString() + "," +
                    Conversion.ConvertBoolToInt(objEmp.WorkSatFlag, m_iClientId).ToString() + "," +
                    Conversion.ConvertBoolToInt(objEmp.WorkSunFlag, m_iClientId).ToString();
			}

			//fillnonoccdom() should prevent payment calculation if no work days in week
			if (sWorkDaysArray.Equals("0,0,0,0,0,0,0"))
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentsDom.ZeroWorkDays",m_iClientId));
            

			//LTD updations..Raman Bhatia 
            //Writing a new CalculateNonOccPaymentsAsPerRMWorld() function to handle LTD case..
            //Ultimately we need to move completely to the new function

            //Raman Bhatia
            //03/02/2007 Now Lets shift completely to new methods
            
            m_objCache.GetCodeInfo(objClaim.DisabilityPlan.PrefPaySchCode, ref sShortCode, ref sCodeDesc);
            //if (sShortCode.ToLower() == "pm")
            //{
                if (p_iSysCmd == SysCmd_SavePayments)
                    iBatchId = CalculateNonOccPaymentsAsPerRMWorld(objClaim, objClaim.DisabilityPlan, objDisClass, sWorkDaysArray, dblWeeklyBenefit, p_iTransTypeCode, p_iAccountId, sBenefitsStartDate, sBenCalcPayStart, sBenCalcPayTo, true, ref p_objTable, ref dtpTaxes);
                else
                    iBatchId = CalculateNonOccPaymentsAsPerRMWorld(objClaim, objClaim.DisabilityPlan, objDisClass, sWorkDaysArray, dblWeeklyBenefit, p_iTransTypeCode, p_iAccountId, sBenefitsStartDate, sBenCalcPayStart, sBenCalcPayTo, false, ref p_objTable, ref dtpTaxes);
            /*}
            //else
            //{
                //compute payments and fill m_xmlDom with them
              //  if (p_iSysCmd == SysCmd_SavePayments)
                //    iBatchId = CalculateNonOccPayments(objClaim, objClaim.DisabilityPlan, objDisClass, sWorkDaysArray, dblWeeklyBenefit, p_iTransTypeCode, p_iAccountId, sBenefitStartDate, sBenCalcPayStart, sBenCalcPayTo, true, ref p_objTable, ref dtpTaxes);
               // else
                //    iBatchId = CalculateNonOccPayments(objClaim, objClaim.DisabilityPlan, objDisClass, sWorkDaysArray, dblWeeklyBenefit, p_iTransTypeCode, p_iAccountId, sBenefitStartDate, sBenCalcPayStart, sBenCalcPayTo, false, ref p_objTable, ref dtpTaxes);
            }
             */ 
            //MITS 13347 : Umesh
              if (objClaim.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                 p_iAccountId = GetParentAccountID(p_iAccountId);
             //MITS 13347 :End
			objDocumentElement.SetAttribute("batchid", iBatchId.ToString());
			objDocumentElement.SetAttribute("bencalcpaystart", Conversion.GetDBDateFormat(sBenCalcPayStart,"d"));
			objDocumentElement.SetAttribute("bencalcpayto",Conversion.GetDBDateFormat(sBenCalcPayTo,"d"));
			objDocumentElement.SetAttribute("claimid", objClaim.ClaimId.ToString());
			objDocumentElement.SetAttribute("transtypecode", p_iTransTypeCode.ToString());
			objDocumentElement.SetAttribute("accountid", p_iAccountId.ToString());
			objDocumentElement.SetAttribute("claimnumber", objClaim.ClaimNumber);
			objDocumentElement.SetAttribute("claimant",objClaim.PrimaryClaimant.ClaimantEntity.FirstName + " " + objClaim.PrimaryClaimant.ClaimantEntity.LastName);

			m_objCache.GetCodeInfo(p_iTransTypeCode, ref sShortCode, ref sCodeDesc);
			objDocumentElement.SetAttribute("transtype", sCodeDesc);
			objDocumentElement.SetAttribute("account", GetAccountName(p_iAccountId));
		}

		
		private int GetWeeks(DateTime p_dStDate, DateTime p_dEdDate)
		{

			TimeSpan ts= p_dEdDate - p_dStDate;
			int iDays=0;

			if( ts.Days < 7)
			{
				if(p_dStDate.DayOfWeek > p_dEdDate.DayOfWeek)
					return 1; //It is accross the week 

				else
					return 0; // same week
			}
			else
			{
				iDays = ts.Days -7 +(int) p_dStDate.DayOfWeek ;
				int i=0;
				int k=0;

				for(i=1;k<iDays ;i++)
				{
					k+=7;
				}

				if(i>1 && p_dEdDate.DayOfWeek != DayOfWeek.Sunday ) i-=1; 
				return i;
			} 
		}

        //MITS 12902 : Umesh
		private void GetTaxes(Claim p_objClaim, ref bool[] p_bTaxes, ref TaxRate p_dtpTaxes)
		{
			string sSQL=string.Empty;
            int iStateId = 0;
			DbReader objReader=null;

            if (p_objClaim != null)
                iStateId = p_objClaim.PrimaryClaimant.ClaimantEntity.StateId;

            sSQL = "SELECT TAX_NAME,TAX_PERCENTAGE,TAX_EID,TRANS_TYPE_CODE_ID FROM TAX_MAPPING WHERE STATE_ID = -1";
			
			try
			{
				objReader=DbFactory.GetDbReader(m_sConnectionString,sSQL);

				while(objReader.Read())
				{
					switch(Conversion.ConvertObjToStr(objReader.GetValue("TAX_NAME")).Trim().ToUpper())
					{
                            // OBJEmployee to be fethed
						case "FEDERAL":
							p_bTaxes[0] = true;
                            if (p_objClaim.PrimaryPiEmployee.CustomFedTaxPer > 0)
                            {
                                p_dtpTaxes.FED_TAX_RATE = (p_objClaim.PrimaryPiEmployee.CustomFedTaxPer) / 100;
                            }
                            else
                            {
                                p_dtpTaxes.FED_TAX_RATE = Conversion.ConvertObjToDouble(objReader.GetValue("TAX_PERCENTAGE"), m_iClientId) / 100;
                            }
                            p_dtpTaxes.iFedTaxEID = Conversion.ConvertObjToInt(objReader.GetValue("TAX_EID"), m_iClientId);
                            p_dtpTaxes.iFedTransTypeCodeID = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
							break;
						case "SOCIAL_SECURITY":
							p_bTaxes[1] = true;
                            if (p_objClaim.PrimaryPiEmployee.CustomSSTaxPer > 0)
                            {
                                p_dtpTaxes.SS_TAX_RATE = (p_objClaim.PrimaryPiEmployee.CustomSSTaxPer) / 100;
                            }
                            else
                            {
                                p_dtpTaxes.SS_TAX_RATE = Conversion.ConvertObjToDouble(objReader.GetValue("TAX_PERCENTAGE"), m_iClientId) / 100;
                            }
                            p_dtpTaxes.iSSTaxEID = Conversion.ConvertObjToInt(objReader.GetValue("TAX_EID"), m_iClientId);
                            p_dtpTaxes.iSSTransTypeCodeID = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
							break;
						case "MEDICARE":
							p_bTaxes[2] = true;
                            if (p_objClaim.PrimaryPiEmployee.CustomMedTaxPer > 0)
                            {
                                p_dtpTaxes.MEDICARE_TAX_RATE = Math.Round((p_objClaim.PrimaryPiEmployee.CustomMedTaxPer) / 100, 4, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                p_dtpTaxes.MEDICARE_TAX_RATE = Math.Round(Conversion.ConvertObjToDouble(objReader.GetValue("TAX_PERCENTAGE"), m_iClientId) / 100, 4, MidpointRounding.AwayFromZero);
                            }
                            p_dtpTaxes.iMedicareTaxEID = Conversion.ConvertObjToInt(objReader.GetValue("TAX_EID"), m_iClientId);
                            p_dtpTaxes.iMedicareTransTypeCodeID = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
							break;
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.GetTaxes.Error",m_iClientId), p_objEx);
			}
			finally
			{
				objReader.Close();
				objReader.Dispose();
			}
			if (iStateId>0)
			{
                sSQL = "SELECT TAX_NAME,TAX_PERCENTAGE,TAX_EID,TRANS_TYPE_CODE_ID FROM TAX_MAPPING WHERE STATE_ID = " + iStateId.ToString();
				try
				{
					objReader=DbFactory.GetDbReader(m_sConnectionString,sSQL);
					if (objReader.Read())
					{
						p_bTaxes[3] = true;
                        if (p_objClaim.PrimaryPiEmployee.CustomSTTaxPer > 0)
                        {
                            p_dtpTaxes.STATE_TAX_RATE = (p_objClaim.PrimaryPiEmployee.CustomSTTaxPer) / 100;
                        }
                        else
                        {
                            p_dtpTaxes.STATE_TAX_RATE = Conversion.ConvertObjToDouble(objReader.GetValue("TAX_PERCENTAGE"), m_iClientId) / 100;
                        }
                        p_dtpTaxes.iStateTaxEID = Conversion.ConvertObjToInt(objReader.GetValue("TAX_EID"), m_iClientId);
                        p_dtpTaxes.iStateTransTypeCodeID = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
					}
				}
				catch(RMAppException p_objEx)
				{
					throw p_objEx ;
				}
				catch(Exception p_objEx)
				{
					throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.GetTaxes.Error",m_iClientId), p_objEx);
				}
				finally
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}


		private string FundsAutoSplitUpdateSQL(int p_iPiEid,int p_iBatchId,int p_iPaymentNum,int p_iTag,double p_dblAmount,string p_sFrom,string p_sTo)
		{
            StringBuilder sSQL = new StringBuilder();
			string sDTTM=Conversion.ToDbDateTime(DateTime.Now);

			sSQL.Append("UPDATE FUNDS_AUTO_SPLIT SET AMOUNT = " + string.Format("{0:0.00}",p_dblAmount));
			sSQL.Append(",DTTM_RCD_LAST_UPD='" + sDTTM + "'");
			sSQL.Append(",UPDATED_BY_USER='" + m_sUserName + "'");
			sSQL.Append(",FROM_DATE='" + p_sFrom + "'");
			sSQL.Append(",TO_DATE='" + p_sTo + "'");
			sSQL.Append(" FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT");
			sSQL.Append(" WHERE FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID");
			sSQL.Append(" AND FUNDS_AUTO_SPLIT.TAG=" + p_iTag.ToString());
			sSQL.Append(" AND FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString());
			sSQL.Append(" AND FUNDS_AUTO.PAY_NUMBER = " + p_iPaymentNum.ToString());
            sSQL.Append(" AND FUNDS_AUTO.PAYEE_EID = " + p_iPiEid.ToString());

			return sSQL.ToString();
		}

        private string FundsAutoSplitUpdateSQLAsPerRMWorld(double p_dblAmount, string p_sFrom, string p_sTo , long p_lAutoSplitId)
        {
            StringBuilder sSQL = new StringBuilder();
            string sDTTM = Conversion.ToDbDateTime(DateTime.Now);

            sSQL.Append("UPDATE FUNDS_AUTO_SPLIT SET AMOUNT = " + p_dblAmount.ToString());
            sSQL.Append(", FROM_DATE = " + p_sFrom);
            sSQL.Append(", TO_DATE = " + p_sTo);
            sSQL.Append(", DTTM_RCD_LAST_UPD = " + sDTTM);
            sSQL.Append(", UPDATED_BY_USER = '" + m_sUserName + "'");
            sSQL.Append(" WHERE AUTO_SPLIT_ID = " + p_lAutoSplitId.ToString());

                       
            return sSQL.ToString();




        }


		private string FundsAutoUpdateSQL(int p_iPiEid,long p_iBatchId,long p_iPaymentNum, int p_iTag,double p_dblAmount,int p_iWorkDaysCount,string p_sCheckMemo)
		{
            StringBuilder sSQL = new StringBuilder();
			string sDTTM=Conversion.ToDbDateTime(DateTime.Now);

			sSQL.Append("UPDATE FUNDS_AUTO SET AMOUNT = " + string.Format("{0:0.00}",p_dblAmount));
			sSQL.Append(",DTTM_RCD_LAST_UPD='" + sDTTM + "'");
			sSQL.Append(",UPDATED_BY_USER='" + m_sUserName + "'");
			sSQL.Append(",NUM_OF_PAID_DAYS=" + p_iWorkDaysCount.ToString());
            sSQL.Append(",CHECK_MEMO=" + p_sCheckMemo);
			sSQL.Append(" FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT");
			sSQL.Append(" WHERE FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID");
			sSQL.Append(" AND FUNDS_AUTO_SPLIT.TAG=" + p_iTag.ToString());
			sSQL.Append(" AND FUNDS_AUTO.AUTO_BATCH_ID = " + p_iBatchId.ToString());
			sSQL.Append(" AND FUNDS_AUTO.PAY_NUMBER = " + p_iPaymentNum.ToString());
			sSQL.Append(" AND FUNDS_AUTO.PAYEE_EID = " + p_iPiEid.ToString());

			return sSQL.ToString();
		}

        private string FundsAutoUpdateSQLAsPerRMWorld(double p_dblAmount, int p_iWorkDaysCount, string p_sCheckMemo , string p_sPrintDate , long p_lAutoTransId , int p_iSupp)
        {
            StringBuilder sSQL = new StringBuilder();
            string sDTTM = Conversion.ToDbDateTime(DateTime.Now);

            //PJS 04-30-08 :MITS 12171 Corrected the query below 
            sSQL.Append("UPDATE FUNDS_AUTO SET AMOUNT = " + string.Format("{0:0.00}", p_dblAmount));
            sSQL.Append(",DTTM_RCD_LAST_UPD='" + sDTTM + "'");
            sSQL.Append(",UPDATED_BY_USER='" + m_sUserName + "'");
            sSQL.Append(",NUM_OF_PAID_DAYS=" + p_iWorkDaysCount.ToString());
            sSQL.Append(", CHECK_MEMO = '" + p_sCheckMemo + "'");
            sSQL.Append(", PRINT_DATE = '" + p_sPrintDate + "'");
            sSQL.Append(" WHERE AUTO_TRANS_ID = " + p_lAutoTransId.ToString());
            sSQL.Append(" AND (SUPP_PAYMENT_FLAG = " + p_iSupp.ToString() + " OR SUPP_PAYMENT_FLAG IS NULL)");
           
            return sSQL.ToString();
        }


		private bool DupePayments(ref XmlDocument p_objTable,int p_iClaimId, int p_iPiEid)
		{
			XmlElement objDocumentElement=null;
			XmlElement objRow=null;
			DbReader objReader=null;
			StringBuilder sSQL=new StringBuilder();
			bool bDupePayments=false;
			int iCounter=0;
			int iBatchIdLocal=0;
			int iBatchIdCheck=0;
			string sFN=string.Empty;
			string sLN=string.Empty;

			objDocumentElement=p_objTable.DocumentElement;

			sSQL.Append("SELECT FUNDS_AUTO.AUTO_BATCH_ID,START_DATE,END_DATE,FIRST_NAME,LAST_NAME,CLAIM_NUMBER,TOTAL_PAYMENTS,VOUCHER_FLAG");
			sSQL.Append(" FROM FUNDS_AUTO INNER JOIN FUNDS_AUTO_BATCH");
            sSQL.Append(" ON FUNDS_AUTO_BATCH.AUTO_BATCH_ID = FUNDS_AUTO.AUTO_BATCH_ID");
			sSQL.Append(" WHERE FUNDS_AUTO.CLAIM_ID = " + p_iClaimId.ToString());
			sSQL.Append(" AND FUNDS_AUTO.PAYEE_EID = " + p_iPiEid.ToString());
			sSQL.Append(" AND FUNDS_AUTO_BATCH.DISABILITY_FLAG <> 0");
            sSQL.Append(" ORDER BY FUNDS_AUTO.AUTO_TRANS_ID");

			try
			{
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
				while (objReader.Read())
				{
					if (iCounter==0)
					{
						iCounter=1;
						bDupePayments=true;
						objDocumentElement.SetAttribute("syscmd", SysCmd_DupePayments.ToString());
						//	header row
						objRow=p_objTable.CreateElement("row");
						objDocumentElement.AppendChild(objRow);
						objRow.SetAttribute("class", "colheader");
						objRow.SetAttribute("startdate", "Start Date");
						objRow.SetAttribute("enddate", "End Date");
						objRow.SetAttribute("payee", "Payee Name");
						objRow.SetAttribute("claimnumber", "Claim Number");
						objRow.SetAttribute("numofpayments", "Number Of Payments");
						objRow.SetAttribute("voucher", "Voucher");
						objRow.SetAttribute("batch", "Batch Number");
					}

                    iBatchIdLocal = Conversion.ConvertObjToInt(objReader.GetValue("AUTO_BATCH_ID"), m_iClientId);
					if (iBatchIdLocal != iBatchIdCheck)
					{
						iBatchIdCheck = iBatchIdLocal;
						objRow=p_objTable.CreateElement("row");
						objDocumentElement.AppendChild(objRow);
						objRow.SetAttribute("class", "rowlight");
						objRow.SetAttribute("batch", iBatchIdLocal.ToString());

						objRow.SetAttribute("startdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("START_DATE")),"d"));
						objRow.SetAttribute("enddate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("END_DATE")),"d"));
						sFN=Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Trim();
						sLN=Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")).Trim();
						if (sFN!=string.Empty)
							objRow.SetAttribute("payee", sFN + " " + sLN);
						else
							objRow.SetAttribute("payee", sLN);
						objRow.SetAttribute("claimnumber",Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER")));
						objRow.SetAttribute("numofpayments",Conversion.ConvertObjToStr(objReader.GetValue("TOTAL_PAYMENTS")));
                        if (!Conversion.ConvertObjToBool(objReader.GetValue("VOUCHER_FLAG"), m_iClientId))
							objRow.SetAttribute("voucher", "No");
						else
							objRow.SetAttribute("voucher","Yes");
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.DupePayments.Error",m_iClientId), p_objEx);
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
			}

			return bDupePayments;
		}


		private string AddPeriodToDate(string p_sBaseDate,string p_sPrdType,int p_iPrd)
		{
			switch(p_sPrdType.ToLower())
			{
				case "yyyy":
					return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddYears(p_iPrd));
				case "q":
					return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddMonths((p_iPrd-1)*3));
				case "m":
					return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddMonths(p_iPrd));
				case "y":
					return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddYears(p_iPrd));
				case "d":
					return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddDays(p_iPrd));
					//case "w":
					//	break;
				case "ww":
                    //abisht MITS 10780 commented the code.
                    //Days added were wrong.Was adding one week less than what was desired.
                    //return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddDays((p_iPrd-1)*7));
					return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddDays((p_iPrd*7)-1));
					//case "h":
					//	break;
					//case "n":
					//	break;
					//case "s":
					//	break;
			}
			return "";
		}


		private void GetBenefitPeriod(DisabilityClass p_objDisClass, ref string p_sBenefitStartDate, ref string p_sBenCalcPayStart, ref string p_sBenCalcPayTo, Claim objClaim)
		{
			bool bFoundDisType = false;
			int iBenDisType=0;
			string sShortCode=string.Empty;
			string sCodeDesc=string.Empty;
			DisClassWait objDisClassWait=null;
			string sTempDate=string.Empty;

			//Benefit Start Date
			if (p_objDisClass.WaitList.Count > 0) //alternate waiting periods exist
			{
				iBenDisType=objClaim.DisTypeCode;
				foreach(DisClassWait objClassWait in p_objDisClass.WaitList)
				{
					if (iBenDisType==objClassWait.DisTypeCode)
					{
						objDisClassWait=objClassWait;
						bFoundDisType=true;	//found an alternate waiting period that matches claim's benefit disability type
						break;
					}
				}
			}

            //if (bFoundDisType)	//found an objClassWait (alternate waiting period) that matches claim's Benefit Disabilty Type
            //{
            //    m_objCache.GetCodeInfo(objDisClassWait.DisPrdType,ref sShortCode,ref sCodeDesc); //Days: Weeks
            //    sShortCode = sShortCode.ToLower();
            //    if (sShortCode == "w")
            //        sShortCode = "ww";
            //    p_sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate,sShortCode, objDisClassWait.DisWaitPrd);
            //}
            //else	//alternate waiting periods do not exist, or did not find an objClassWait (alternate waiting period) that matches claim's Benefit Disabilty Type
            //{
            //    m_objCache.GetCodeInfo(p_objDisClass.DisPrdType,ref sShortCode,ref sCodeDesc); //Days: Weeks
            //    sShortCode = sShortCode.ToLower();
            //    if (sShortCode == "w")
            //        sShortCode = "ww";
            //    p_sBenefitStartDate =AddPeriodToDate(objClaim.DisabilFromDate,sShortCode, p_objDisClass.DisWaitPrd);
            //}

           // MITS 13132 :Umesh
            if (p_sBenCalcPayStart == "")
            {
                if (objClaim.BenCalcPayStart.Trim() != "")
                    p_sBenCalcPayStart = objClaim.BenCalcPayStart;
                else if (objClaim.BenefitsStart.Trim() != "")
                    p_sBenCalcPayStart = objClaim.BenefitsStart;
                else
                    p_sBenCalcPayStart = p_sBenefitStartDate;
            }

            // MITS 13132 :Umesh
            if (p_sBenCalcPayTo =="")
            {
                if (objClaim.BenCalcPayTo.Trim() != "")
                    p_sBenCalcPayTo = objClaim.BenCalcPayTo;
                else if (objClaim.BenefitsThrough.Trim() != "")
                    p_sBenCalcPayTo = objClaim.BenefitsThrough;
                else
                {
                    //use plan class max duration of disability (if any) added to benefit start date
                    m_objCache.GetCodeInfo(p_objDisClass.FromDisPrdType, ref sShortCode, ref sCodeDesc); //Days: Weeks
                    if (sShortCode.ToUpper() == "B")
                        sTempDate = p_sBenefitStartDate;
                    else
                        sTempDate = objClaim.DisabilFromDate;

                    //add days/months/weeks?
                    m_objCache.GetCodeInfo(p_objDisClass.DisMaxPrdType, ref sShortCode, ref sCodeDesc); //Days: Weeks
                    sShortCode = sShortCode.ToLower();
                    if (sShortCode == "w")
                        sShortCode = "ww";

                    //objClass.DIS_MAX_PRD is # of days/months/weeks
                    sTempDate = AddPeriodToDate(sTempDate, sShortCode, p_objDisClass.DisMaxPrd);
                    //abisht MITS 10780
                    string sTempBenCalcPayTo = Conversion.GetDate(p_sBenCalcPayTo);

                    if (sTempBenCalcPayTo != "" && sTempDate != "")
                    {
                        if (int.Parse(sTempBenCalcPayTo.ToString()) > int.Parse(sTempDate.ToString()))
                            p_sBenCalcPayTo = sTempDate;
                        else
                            p_sBenCalcPayTo = Conversion.GetDate(p_sBenCalcPayTo);
                    }
                    else
                    {
                        p_sBenCalcPayTo = sTempDate;
                    }
                }
			}
		}


		private string GetAccountName(int p_iAccountId)
		{
			string sSQL=string.Empty;
			DbReader objReader=null;
			string sAccountName=string.Empty;

			sSQL = "SELECT ACCOUNT_NAME FROM ACCOUNT WHERE ACCOUNT_ID =" + p_iAccountId.ToString();

			try
			{
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
				if (objReader.Read())
					sAccountName=Conversion.ConvertObjToStr(objReader.GetValue("ACCOUNT_NAME"));
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.GetAccountName.Error",m_iClientId), p_iAccountId.ToString()), p_objEx);
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
			}
			return sAccountName;
		}


		private int CalculateNonOccPayments(Claim objClaim,DisabilityPlan objPlan,DisabilityClass objDisClass, 
											string p_sWorkDaysArray, double p_dblWeeklyBenefit,
											int p_iTransTypeCode,int p_iAccountId,string p_sBenefitStartDate,
											string p_sBenCalcPayStart,string p_sBenCalcPayTo, bool p_bSave,
											ref XmlDocument p_objTable,ref TaxRate p_dtpTaxes)
		{
			int iPiEid=0; 
			int iPrintDay=0;
			string sShortCode=string.Empty;
			string sCodeDesc=string.Empty;
			int iBiWeek = 0;
			int iPayPeriod = 0;
			XmlElement objDocumentElement=null;
			XmlElement objRow=null;
			double dblDailyBenefit=0;
			int iBatchId=0;
			int iWorkDaysCount=0;
			int iFundsAutoTransID=0;
			int iFundsAutoSplitId=0;
			DateTime dBenCalcPayStart=DateTime.Now;
			DateTime dBenCalcPayTo=DateTime.Now;
			DateTime dPayPeriodStart=DateTime.Now;
            DateTime dTemp = DateTime.Now;
			DateTime dFirstToDate=DateTime.Now;
			int iPrintDayDiff=0;
			DateTime dFirstPrintDate=DateTime.Now;
			int iCount;
			DateTime dPrintDate=DateTime.Now;
			double dblGross = 0;
			double dblTaxablePercent=0;
			DateTime dTo = DateTime.Now;
			DateTime dFrom = DateTime.Now;
			int iPaymentNumber = 1;
			int iTotalPayments=0;

			DbConnection objDbConnection=null;
			DbTransaction objDbTransaction=null;
            StringBuilder sSQL = new StringBuilder();
            string sReturn = string.Empty;
			EntityData objEntityData=new EntityData();

			objDocumentElement=p_objTable.DocumentElement;
			iPiEid = objClaim.PrimaryClaimant.ClaimantEid;

			//day of week for printing paychecks, default is friday
			iPrintDay=(int)DayOfWeek.Friday;
			if(objPlan.PrefDayCode!=0)
			{
				m_objCache.GetCodeInfo(objPlan.PrefDayCode, ref sShortCode,ref sCodeDesc);
				switch (sShortCode.ToUpper())
				{
					case "D0":
					case "D5":
						//iPrintDay = DayOfWeek.Friday   default is set above
						break;
					case "D1":
						iPrintDay=(int)DayOfWeek.Monday;
						break;
					case "D2":
						iPrintDay=(int)DayOfWeek.Tuesday;
						break;
					case "D3":
						iPrintDay=(int)DayOfWeek.Wednesday;
						break;
					case "D4":
						iPrintDay=(int)DayOfWeek.Thursday;
						break;
					case "D6":
						iPrintDay=(int)DayOfWeek.Saturday;
						break;
					case "D7":
						iPrintDay=(int)DayOfWeek.Sunday;
						break;
				}
			}

			//get daily benefit: weekly benefit/number of days worked per week
			string[] arrWorkDays = p_sWorkDaysArray.Split(new char[]{','});

			//get daily benefit: weekly benefit/number of days worked per week
			for(int iCounter=0;iCounter<arrWorkDays.Length;iCounter++)
			{
				if (arrWorkDays[iCounter]!="0")
					iWorkDaysCount=iWorkDaysCount+1;
			}

			dblDailyBenefit = p_dblWeeklyBenefit / iWorkDaysCount;
    
			//get date for payment start, payment end
			dBenCalcPayStart = Conversion.ToDate(p_sBenCalcPayStart);
			dBenCalcPayTo = Conversion.ToDate(p_sBenCalcPayTo);

			//get date that pay period starts
			if(objPlan.StartPayPeriod.Trim()!="")
			{
				dPayPeriodStart=Conversion.ToDate(objPlan.StartPayPeriod);
			}
			else
			{
				//if no Plan.StartofPayPeriod then assume Sun-Sat pay period.  if biweekly this means assume are in first week
				dTemp=dBenCalcPayStart;
				iCount=(int)dTemp.DayOfWeek;
				
				while(iCount!=(int)DayOfWeek.Sunday)
				{
					dTemp=dTemp.AddDays(1);
					iCount=(int)dTemp.DayOfWeek;
				}
				dPayPeriodStart = dTemp;
			}

			//pay period is 7 or 14 days.
			//if 14 days, compute if dBenCalcPayStart is in first or second week of pay period
			iBiWeek = 1;
			iPayPeriod = 7;

			if (objPlan.PrefPaySchCode!=0)
			{
				m_objCache.GetCodeInfo(objPlan.PrefPaySchCode,ref sShortCode, ref sCodeDesc);
		
				switch (sShortCode.Trim().ToUpper())
				{
					case "PB":
						//if datediff is even, are in first week; odd, are in second week
						//note:  dPayPeriodStart can be in past or future, does not matter,
						//only used to compute iBiWeek (whether dBenCAlcPayStart is in first or second week of pay period)
						int iCounter = GetWeeks(dBenCalcPayStart,dPayPeriodStart);
						if (iCounter % 2 != 0) 
							iBiWeek = 2;
						iPayPeriod = 14;
						break;
					case "PW":
						//weekly
						break;
					default:
						throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentsDom.InvalidPrefPaySchCode",m_iClientId));
				}
			}

			//find first "To" date, the last day of the week of the pay period--start at payments calculation start date, add date until get to same day of week as pay period start date + 6
			dTemp=dBenCalcPayStart;
			iCount=(int)dTemp.DayOfWeek;

			while (iCount!=((int)dPayPeriodStart.AddDays(6).DayOfWeek))
			{
				dTemp=dTemp.AddDays(1);
				iCount=(int)dTemp.DayOfWeek;
			}
			dFirstToDate=dTemp;

			if (iPayPeriod==14 && iBiWeek==1)
			{
				//in first week of bi-weekly payments, so add 7 days
				dFirstToDate =dFirstToDate.AddDays(7);
			}

			//find first check print date
			//PrintBeforePayEnd means print date is before or = dFirstToDate; othewise is after or =
			int iTemp=0;
			if (objPlan.PrintBeforeEnd)
				iTemp=-1;
			else
				iTemp=1;

			dTemp=dFirstToDate;
			iCount=(int)dTemp.DayOfWeek;
			iPrintDayDiff = 0;
			while(iCount!=iPrintDay)
			{
				iPrintDayDiff = iPrintDayDiff + iTemp;
				dTemp=dTemp.AddDays(iTemp);
				iCount=(int)dTemp.DayOfWeek;
			}
			dFirstPrintDate = dTemp;
    
			//first print date cannot be earlier than today
			TimeSpan ts=DateTime.Now.Subtract(dFirstPrintDate);
			if (ts.Days > 0)
			{
				dTemp=DateTime.Now;
				while(dFirstPrintDate<dTemp)
					dFirstPrintDate=dFirstPrintDate.AddDays(iPayPeriod);
			}
    
			//set the header row for the table
			objRow=p_objTable.CreateElement("row");
			objDocumentElement.AppendChild(objRow);
			objRow.SetAttribute("header", "1");
			objRow.SetAttribute("class", "colheader");
			objRow.SetAttribute("paymentnumber", "Payment Number");
			objRow.SetAttribute("fromdate", "From Date");
			objRow.SetAttribute("todate", "To Date");
			objRow.SetAttribute("printdate", "Print Date");
			objRow.SetAttribute("gross", "Gross");
			objRow.SetAttribute("net", "Net");
			objRow.SetAttribute("dayspaid", "Days Paid");
			objRow.SetAttribute("fed", "Fed Tax");
			objRow.SetAttribute("ss", "SS Tax");
			objRow.SetAttribute("med", "Medicare Tax");
			objRow.SetAttribute("state", "State Tax");

			//for each day worked, add dblDailyBenefit to dblGross.  at each "To" date add paycheck row to table
			dPrintDate = dFirstPrintDate;
			dblTaxablePercent = p_dtpTaxes.dblTaxablePercent;
			if (dblTaxablePercent == 0)
				dblTaxablePercent = 1;
			iWorkDaysCount = 0;
			dTo = dFirstToDate;
			dFrom = dBenCalcPayStart;

			//initialize arrays in the structure
			objEntityData.FN=new string[5]{"","","","",""};
			objEntityData.MN=new string[5]{"","","","",""};
			objEntityData.LN=new string[5]{"","","","",""};
			objEntityData.Addr1=new string[5]{"","","","",""};
			objEntityData.Addr2=new string[5]{"","","","",""};
            objEntityData.Addr3 = new string[5] { "", "", "", "", "" };
            objEntityData.Addr4 = new string[5] { "", "", "", "", "" };
			objEntityData.City=new string[5]{"","","","",""};
			objEntityData.StateId=new int[5]{0,0,0,0,0};
			objEntityData.Zip=new string[5]{"","","","",""};
			objEntityData.PayeeType=new int[5]{0,0,0,0,0};
			objEntityData.ReserveTypeCode=new int[5]{0,0,0,0,0};

			try
			{
				if( p_bSave)
				{
					//begin the transaction
					//must open a separate database for transaction or get connection leak
					objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
					objDbConnection.Open();  
					objDbTransaction = objDbConnection.BeginTransaction(); 

					//get data needed for saving each payment
					//entity and other data and other needed for saving each payment
					FillEntities(objClaim,p_iAccountId, p_iTransTypeCode,ref objEntityData,ref p_dtpTaxes,objDbConnection,objDbTransaction);
    
					//iTotalPayments needed for saving each payment
					//note:  need to do the same Do-Loop twice.
					while(dFrom <= dBenCalcPayStart)
					{
						dTo = dTo.AddDays(iPayPeriod);
						dFrom=dTo.AddDays(-(iPayPeriod - 1));	//beginning of pay period
						ts=dBenCalcPayTo.Subtract(dTo);
						if (ts.Days < 0)
							dTo=dBenCalcPayTo;
						//payment number counter
						iPaymentNumber = iPaymentNumber + 1;
					}
		
					iTotalPayments = iPaymentNumber - 1;

					//save the batch entry.  because they can be edited, store the current payment period start date, payment preference code, BenCalcPayFrom,BenCalcPayTo, BenefitsFrom (sBenefitStartDate), & BenefitsThrough (Claim.BenefitsThrough or if that's empty BenCalcPayTo)
                    iBatchId = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_BATCH", objDbTransaction, m_iClientId);
					string sPayPeriodStart=Conversion.ToDbDate(dPayPeriodStart);
					string sBenefitsThrough=objClaim.BenefitsThrough;
					if (sPayPeriodStart.Trim()=="")
						sBenefitsThrough=p_sBenCalcPayTo; //note: same algorithm in GetBenefitPeriod()/FillNonOccDom()
        
					sSQL.Append("INSERT INTO FUNDS_AUTO_BATCH (AUTO_BATCH_ID,TOTAL_PAYMENTS,CURRENT_PAYMENT,PAYMENT_INTERVAL,START_DATE,END_DATE,BEN_START_DATE,BEN_END_DATE,PAY_PERIOD_START,GROSS_TO_DATE,PAYMENTS_TO_DATE,DISABILITY_FLAG,DAILY_AMOUNT)");
					sSQL.Append(" VALUES (" + iBatchId.ToString() + "," + iTotalPayments.ToString() + ",1," + objPlan.PrefPaySchCode.ToString() + ",'" + Conversion.ToDbDate(dBenCalcPayStart) + "','" + Conversion.ToDbDate(dBenCalcPayTo) + "','" + p_sBenefitStartDate + "','" + sBenefitsThrough + "','" + sPayPeriodStart + "',0,0,-1, " + dblDailyBenefit.ToString() + ")");

					//execute sql
					objDbConnection.ExecuteNonQuery(sSQL.ToString(),objDbTransaction);
        
					//reset vars to do the same Do-Loop again below
					dTo = dFirstToDate;
					dFrom = dBenCalcPayStart;
					iPaymentNumber = 1;   //payment number
				}

				double dblFed=0;
				double dblSS=0;
				double dblMed=0;
				double dblState=0;
				double dblNet=0;
                DateTime dtPritdateHS;
				while(dFrom<=dBenCalcPayTo)
				{
					// compute the gross/days count for this pay period (only add a daily benefit to the gross if the day is a workday)
					// dFirst = dCheckDate  'first and last days in the pay period for which a person recieves a benefit.  this is saved as "to date" and "from date" in datbase
					// dLast = dCheckDate
					dTemp=dFrom;
					while (dTemp<=dTo)
					{
						iCount = (int)dTemp.DayOfWeek;
						if (arrWorkDays[iCount]!="0")
						{
							dblGross = dblGross + dblDailyBenefit;
							iWorkDaysCount = iWorkDaysCount + 1;
							//If dFirst = dCheckDate Then dFirst = d
							//dLast = d
						}
						dTemp = dTemp.AddDays(1);
					}
                    //Neha Funds Utility Enhancement
                   
                    dtPritdateHS = AdjustPrintDate(objClaim.AdjustPrintDatesFlag, dPrintDate);
                    //Neha Funds Utility Enhancement

					//if beginning of the pay period precedes beginning of ben calc period, move the from date up (usually the case in first week of payment)
					ts=dFrom.Subtract(dBenCalcPayStart);
					if(ts.Days<0)
						dFrom=dBenCalcPayStart;

					//add values to the paycheck
					objRow = p_objTable.CreateElement("row");
					objDocumentElement.AppendChild(objRow);
					objRow.SetAttribute("class", "rowlight");
					objRow.SetAttribute("gross", string.Format("{0:0.00}",dblGross));

					if (p_dtpTaxes.bFed)
						dblFed=Conversion.ConvertStrToDouble(string.Format("{0:0.00}",(dblGross * dblTaxablePercent) * p_dtpTaxes.FED_TAX_RATE));
					if (p_dtpTaxes.bSS)
						dblSS=Conversion.ConvertStrToDouble(string.Format("{0:0.00}",(dblGross * dblTaxablePercent) * p_dtpTaxes.SS_TAX_RATE));
					if (p_dtpTaxes.bMed)
						dblMed=Conversion.ConvertStrToDouble(string.Format("{0:0.00}",(dblGross * dblTaxablePercent) * p_dtpTaxes.MEDICARE_TAX_RATE));
					if (p_dtpTaxes.bState && p_dtpTaxes.STATE_TAX_RATE>0)
						dblState=Conversion.ConvertStrToDouble(string.Format("{0:0.00}",(dblGross * dblTaxablePercent) * p_dtpTaxes.STATE_TAX_RATE));
					dblNet = dblGross - (dblFed + dblSS + dblMed + dblState);

					objRow.SetAttribute("fed", string.Format("{0:0.00}",dblFed * -1));
					objRow.SetAttribute("ss", string.Format("{0:0.00}",dblSS * -1));
					objRow.SetAttribute("med", string.Format("{0:0.00}",dblMed * -1));
					objRow.SetAttribute("state", string.Format("{0:0.00}",dblState * -1));
					objRow.SetAttribute("net", string.Format("{0:0.00}",dblNet));
                    objRow.SetAttribute("printdate", dtPritdateHS.ToShortDateString());
					objRow.SetAttribute("fromdate", dFrom.ToShortDateString());

					//if end of pay period exceeds end of ben calc period, move the "to" date back (usually the case in last week of payment)
					ts=dBenCalcPayTo.Subtract(dTo);
					if(ts.Days<0)
						objRow.SetAttribute("todate", dBenCalcPayTo.ToShortDateString());
					else
						objRow.SetAttribute("todate", dTo.ToShortDateString());

					objRow.SetAttribute("paymentnumber", iPaymentNumber.ToString());
					objRow.SetAttribute("dayspaid", iWorkDaysCount.ToString());

					//save paycheck to database
					if(p_bSave && dblGross>0)
					{
						//note:  order of creation of lFundsAutoTransId is important
						//note: lGetNextUID() uses m_objApp.DBConnection2 (g_DBConn2), so is outside of transaction

						//one:  get lFundsAutoTransId for dblNet, and save gross & (taxes * -1) associated with dblNet to split table
                        iFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
						sReturn = FundsAutoSQL(iPiEid, iBatchId, iFundsAutoTransID, objPlan.PrefPaySchCode, Entities.Clmnt, dblNet, iPaymentNumber, dPrintDate, iWorkDaysCount, objDisClass.UseVouchers,objEntityData);
                        objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);

                        iFundsAutoSplitId = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                        sReturn = GetSplitSQL(iFundsAutoSplitId, iFundsAutoTransID, p_iTransTypeCode, dblGross, dFrom, dTo, Entities.Clmnt, objEntityData);
                        objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);
            
						//Save Third Party Splits
						if (p_dtpTaxes.iFedTaxEID!=0 && p_dtpTaxes.bFed && dblFed > 0)
						{
                            iFundsAutoSplitId = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                            sReturn = GetSplitSQL(iFundsAutoSplitId, iFundsAutoTransID, p_dtpTaxes.iFedTransTypeCodeID, dblFed * -1, dFrom, dTo, Entities.Fed, objEntityData);
                            objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);
						}

						if (p_dtpTaxes.iSSTaxEID!=0 && p_dtpTaxes.bSS && dblSS > 0)
						{
                            iFundsAutoSplitId = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                            sReturn = GetSplitSQL(iFundsAutoSplitId, iFundsAutoTransID, p_dtpTaxes.iSSTransTypeCodeID, dblSS * -1, dFrom, dTo, Entities.SS, objEntityData);
                            objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);
						}

						if (p_dtpTaxes.iMedicareTaxEID!=0 && p_dtpTaxes.bMed && dblMed > 0)
						{
                            iFundsAutoSplitId = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                            sReturn = GetSplitSQL(iFundsAutoSplitId, iFundsAutoTransID, p_dtpTaxes.iMedicareTransTypeCodeID, dblMed * -1, dFrom, dTo, Entities.Med, objEntityData);
                            objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);
						}

						if (p_dtpTaxes.iStateTaxEID!=0 && p_dtpTaxes.bState && dblState > 0)
						{
                            iFundsAutoSplitId = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                            sReturn = GetSplitSQL(iFundsAutoSplitId, iFundsAutoTransID, p_dtpTaxes.iStateTransTypeCodeID, dblState * -1, dFrom, dTo, Entities.State, objEntityData);
                            objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);
						}
            
						//two:  get separate FundsAutoTransId for each tax payment
						//Save Split Payments to Funds_auto and Funds_auto_split tables
						if (p_dtpTaxes.iFedTaxEID!=0 && p_dtpTaxes.bFed && dblFed > 0)
						{
                            iFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                            sReturn = FundsAutoSQL(p_dtpTaxes.iFedTaxEID, iBatchId, iFundsAutoTransID, objPlan.PrefPaySchCode, Entities.Fed, dblFed, iPaymentNumber, dPrintDate, iWorkDaysCount, false, objEntityData);
                            objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);

                            iFundsAutoSplitId = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                            sReturn = GetSplitSQL(iFundsAutoSplitId, iFundsAutoTransID, p_dtpTaxes.iFedTransTypeCodeID, dblFed, dFrom, dTo, Entities.Fed, objEntityData);
                            objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);
						}
						if (p_dtpTaxes.iSSTaxEID!=0 && p_dtpTaxes.bSS && dblSS > 0)
						{
                            iFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                            sReturn = FundsAutoSQL(p_dtpTaxes.iSSTaxEID, iBatchId, iFundsAutoTransID, objPlan.PrefPaySchCode, Entities.SS, dblSS, iPaymentNumber, dPrintDate, iWorkDaysCount, false, objEntityData);
                            objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);

                            iFundsAutoSplitId = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                            sReturn = GetSplitSQL(iFundsAutoSplitId, iFundsAutoTransID, p_dtpTaxes.iSSTransTypeCodeID, dblSS, dFrom, dTo, Entities.SS, objEntityData);
                            objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);
						}
						if (p_dtpTaxes.iMedicareTaxEID!=0 && p_dtpTaxes.bMed && dblMed > 0)
						{
                            iFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                            sReturn = FundsAutoSQL(p_dtpTaxes.iMedicareTaxEID, iBatchId, iFundsAutoTransID, objPlan.PrefPaySchCode, Entities.Med, dblMed, iPaymentNumber, dPrintDate, iWorkDaysCount, false, objEntityData);
                            objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);

                            iFundsAutoSplitId = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                            sReturn = GetSplitSQL(iFundsAutoSplitId, iFundsAutoTransID, p_dtpTaxes.iMedicareTransTypeCodeID, dblMed, dFrom, dTo, Entities.Med, objEntityData);
                            objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);
						}
						if (p_dtpTaxes.iStateTaxEID!=0 && p_dtpTaxes.bState && dblState > 0)
						{
                            iFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                            sReturn = FundsAutoSQL(p_dtpTaxes.iStateTaxEID, iBatchId, iFundsAutoTransID, objPlan.PrefPaySchCode, Entities.State, dblState, iPaymentNumber, dPrintDate, iWorkDaysCount, false, objEntityData);
                            objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);

                            iFundsAutoSplitId = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                            sReturn = GetSplitSQL(iFundsAutoSplitId, iFundsAutoTransID, p_dtpTaxes.iStateTransTypeCodeID, dblState, dFrom, dTo, Entities.State, objEntityData);
                            objDbConnection.ExecuteNonQuery(sReturn, objDbTransaction);
						}
                	}
					
					//move to the next pay period (note:  this loop is used above to compute iTotalPayments.  if change here must change above and vice-versa)
					dTo = dTo.AddDays(iPayPeriod);
					dFrom=dTo.AddDays(-(iPayPeriod - 1));	//beginning of pay period
					ts=dBenCalcPayTo.Subtract(dTo);
					if (ts.Days < 0)
						dTo=dBenCalcPayTo;

					//print date could have been reset to be >= Date(), so only increment when past that point					
					if (dPrintDate < dTo.AddDays(iPrintDayDiff))
						dPrintDate = dPrintDate.AddDays(iPayPeriod);

					//reset cumulative vars to 0
					dblGross = 0; 
					dblFed = 0; 
					dblSS = 0; 
					dblMed = 0; 
					dblState = 0;
					iWorkDaysCount = 0;
					        
					//payment number counter
					iPaymentNumber = iPaymentNumber + 1;
				}

				if(objDbTransaction!=null)
				{
					objDbTransaction.Commit();
				}
			}
			catch(InitializationException p_objInitializationException)
			{
				throw p_objInitializationException;
			}
			catch(InvalidValueException p_objInvalidValueException)
			{
				if(objDbTransaction!=null)
				{
					objDbTransaction.Rollback(); 
				}
				throw p_objInvalidValueException;
			}
			catch(RMAppException p_objRMAppException)
			{
				if(objDbTransaction!=null)
				{
					objDbTransaction.Rollback(); 
				}

				throw p_objRMAppException;
			}
			catch(Exception p_objException)
			{
				if(objDbTransaction!=null)
				{
					objDbTransaction.Rollback(); 
				}
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.CalculateNonOccPayments.Error",m_iClientId), p_objException);  
			}
			finally
			{
				if(objDbTransaction!=null)
				{
					objDbTransaction.Dispose();
					objDbTransaction = null;
				}
				
				if(objDbConnection!=null)
				{
					objDbConnection.Close();
                    objDbConnection.Dispose();
				}
			}
			return iBatchId;
		}

        /// <summary> R8 Funds utility enhancement
        /// Neha: Changes the print date sa per Holiset set up in schedule dates
        /// </summary>
        /// <param name="bAdjustPrintDatesFlag"></param>
        /// <param name="p_dtToDate"></param>
        /// <returns>date time</returns>
        private DateTime AdjustPrintDate(bool bAdjustPrintDatesFlag, DateTime p_dtToDate)
        {
            string sSQL = string.Empty;
            bool bAutoCheck = false;
            string sScheduleToDay = string.Empty;
            string sAdjustPrintDatesFlag = string.Empty;
            ArrayList sHolidayList = new ArrayList();
            bool bIsHoliday = false;
            DateTime dtPrintDate;
            dtPrintDate = p_dtToDate;
            bAutoCheck = CommonFunctions.IsScheduleDatesOn("SCH_DATE_NONOCC_PMT", m_sConnectionString, m_iClientId);
                   
               
            if (bAutoCheck)
            {
                if (bAdjustPrintDatesFlag)
                {
                    sScheduleToDay = CommonFunctions.GetWeekendSchedule(m_sConnectionString, m_iClientId);
                   sSQL = "SELECT SCHEDULE_DATE FROM SCHEDULED_DATES";
                   using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            sHolidayList.Add(objReader.GetString("SCHEDULE_DATE"));
                        }
                       
                    }
                   dtPrintDate = CheckHolidayWeekend(dtPrintDate, sScheduleToDay, sHolidayList);
                }
            }
            return dtPrintDate;
        }

        /// <summary>
        /// neha R8 Funds utilty enhancement loops through all days till 
        /// </summary>
        /// <param name="p_date"></param>
        /// <param name="sScheduleToDay"></param>
        /// <param name="sHolidayList"></param>
        /// <returns></returns>
        private DateTime CheckHolidayWeekend(DateTime p_date, string sScheduleToDay, ArrayList sHolidayList)
        {
            bool bIsHoliday = false;
            foreach (string  sScheduledate in sHolidayList)
            {
                if (String.Compare(p_date.ToString("yyyyMMdd"), sScheduledate) == 0)
                {
                    bIsHoliday = true;
                }
            }
            
            if (p_date.DayOfWeek == DayOfWeek.Sunday)
            {
                switch (sScheduleToDay)
                {
                    case "Monday":
                        p_date = p_date.AddDays(1);
                        break;
                    case "Thursday":
                        p_date = p_date.AddDays(-3);
                        break;

                    case "Saturday":
                        p_date = p_date.AddDays(-1);
                        break;
                    default://and friday will be default case as well
                        p_date = p_date.AddDays(-2);
                        break;
                }
                p_date = CheckHolidayWeekend(p_date, sScheduleToDay, sHolidayList);
            }
            else if (p_date.DayOfWeek == DayOfWeek.Saturday && sScheduleToDay != "Saturday")
            {
                switch (sScheduleToDay)
                {
                    case "Monday":
                        p_date = p_date.AddDays(2);
                        break;
                    case "Thursday":
                        p_date = p_date.AddDays(-2);
                        break;
                    default://and friday will be default case as well
                        p_date = p_date.AddDays(-1);
                        break;
                }
                p_date = CheckHolidayWeekend(p_date, sScheduleToDay, sHolidayList);
            }
            else if (bIsHoliday)
            {
                p_date = p_date.AddDays(-1);
                p_date = CheckHolidayWeekend(p_date, "", sHolidayList);
            }

            return p_date;

        }

        private int CalculateNonOccPaymentsAsPerRMWorld(Claim objClaim, DisabilityPlan objPlan, DisabilityClass objDisClass,
                                            string p_sWorkDaysArray, double p_dblWeeklyBenefit,
                                            int p_iTransTypeCode, int p_iAccountId, string p_sBenefitStartDate,
                                            string p_sBenCalcPayStart, string p_sBenCalcPayTo, bool p_bSave,
                                            ref XmlDocument p_objTable, ref TaxRate p_dtpTaxes)
        {
            DbTransaction objDbTransaction = null;
            DbConnection objDbConnection = null;
            long lSuppSubAcc = 0;
            long lSuppBankAcc = 0;
            long lSuppTransType = 0;
            long lSuppResType = 0;
            int iBatchId = 0;
            int iOrdinal = 0;
            int [] iDaysOfWeek = new int[7];
            DateTime dTempPaymentStart = DateTime.MinValue;
            DateTime dTempPaymentEnd = DateTime.MinValue;
            DateTime dStartDate = DateTime.MinValue;
            DateTime dEndDate = DateTime.MinValue;
            int iDaysToEnd = 0;
            int iNumberOfPayments = 0;
            DateTime dThisDate = DateTime.MinValue;
            int iRemainder = 0;
            int iNumberOfWeeks = 0;
            double dFinalPaymentAmount = 0;
            int i = 0;
            int j = 0;
            int iPaymentLoop = 0;
            double dPayment = 0;
            double dSupp = 0;
            string sPaymentStart = "";
            string sPaymentEnd = "";
            string sShortCode = "";
            string sCodeDesc = "";
            int iDays = 0;
            int rs = 0;
            string sSQL = "";
            long lFundsAutoTransID = 0;
            long lFundsAutoSplitID = 0;
            int iLastPayment = 0;
            int result = 0;
            DateTime dPrintDate = DateTime.MinValue;
            XmlElement objRow = null;
            long lPayeeEid = 0;
            double dAmount = 0;
            bool bThirdParty = false;
            int iNumOfPays = 0;
            double dFedTax = 0;
            double dSSTax = 0;
            double dMedTax = 0;
            double dStateTax = 0;
            double dPensionOffset = 0;
            double dSSOffset = 0;
            double dOtherOffset = 0;
            //Added by Ravneet Kaur for adding offset and post tax deduction
            double dOtherOffset_1 = 0;
            double dOtherOffset_2 = 0;
            double dOtherOffset_3 = 0;
            double dPstTaxDed_1 = 0;
            double dPstTaxDed_2 = 0;
            ////////////////////////////////////
            //Animesh inserted For GHS enhancment  //pmittal5 Mits 14841
            double dtempNetPayment = 0;
            bool blnPaymentDeleted=false;
            //Animesh Insertion ends

            double dTotalOffset = 0;
            double dPaymentOffsetDif = 0;
            double dTempPensionOffset = 0;
            double dTempSSOffset = 0;
            double dTempOtherOffset = 0;
            //Added by RKaur for GHS 
            double dTempOffset1 = 0;
            double dTempOffset2 = 0;
            double dTempOffset3 = 0;
            double dTempPstTD1 = 0; 
            double dTempPstTD2 = 0;
            //////

            double dTempOffset = 0;
            double dPension = 0;
            double dSS = 0;
            double dOther = 0;
            double dCalcFactor = 0;  //MITS 14186
            const double dOneCent = 0.01;

            //Added by Ravneet Kaur
            double dOther1 = 0;
            double dOther2 = 0;
            double dOther3 = 0;
            double dPostTax1 = 0;
            double dPostTax2 = 0;
            /////////////////

            int iPos = 0;
            DateTime dToDate = DateTime.MinValue;
            double dTotalTax = 0;
            int iCount = 0;
            DateTime dThisPrintDate = DateTime.MinValue;
            int iNumOfDays = 0;
            int iDaysInFirstPayPeriod = 0;
            DateTime dCurrentPrintDate = DateTime.MinValue;
            DateTime dNewDate1 = DateTime.MinValue;
            DateTime dNewDate2 = DateTime.MinValue;
            DateTime dThisPayPeriodStart = DateTime.MinValue;
            DateTime dThisPayPeriodEnd = DateTime.MinValue;
            XmlElement objDocumentElement = null;
            bool bSuppPaymentFlag = false;
            long lTransType = 0;
            double dDailyAmountTotal = 0;
            string sAppend = "";
            int iPrefSched = 0;
            int iTypeOfMonthlyPay = 0;
            int iPrefDate = 0;
            int ilblDaysIncluded = 0;
            bSuppPaymentFlag = false;
            bool bSuppRollUp = false;
            DbReader objReader = null;
            long lPensionOffTransTypeCodeID = 0;
            long lSSOffTransTypeCodeID = 0;
            long lOtherOffTransTypeCodeID = 0;
            //Added by RKaur for GHS 
            long lOthOff1TransTypeCodeID = 0;
            long lOthOff2TransTypeCodeID = 0;
            long lOthOff3TransTypeCodeID = 0;
            long lPstTax1TransTypeCodeID = 0;
            long lPstTax2TransTypeCodeID = 0;
            //////////////////////////

            DateTime dPaymentStart = DateTime.MinValue;
            DateTime dPaymentEnd = DateTime.MinValue;
            DateTime dPrintDateHS = DateTime.MinValue;


            try
            {
                // Pull the preferred schedule selected on the disability plan

                if (m_objCache.GetShortCode(objClaim.DisabilityPlan.PrefPaySchCode).ToLower() == "pw")
                {
                    iPrefSched = 1;
                }
                if (m_objCache.GetShortCode(objClaim.DisabilityPlan.PrefPaySchCode).ToLower() == "pb")
                {
                    iPrefSched = 2;
                }
                if (m_objCache.GetShortCode(objClaim.DisabilityPlan.PrefPaySchCode).ToLower() == "pm")
                {
                    iPrefSched = 3;
                }
                if (m_objCache.GetShortCode(objClaim.DisabilityPlan.TypeofMonthlyPay).ToLower() == "cdt")
                {
                    iTypeOfMonthlyPay = 1;
                }
                else
                    iTypeOfMonthlyPay = 0;

                if (objClaim.DisabilityPlan.PrefDate != 0)
                    iPrefDate = objClaim.DisabilityPlan.PrefDate;
                else
                    iPrefDate = 1;

                //Raman Bhatia: LTD changes: Get the Transaction Types for Offsets

                sSQL = "SELECT * FROM OFFSET_MAPPING";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
                //loop and get OFFSET trans type code
                while (objReader.Read())
                {
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "PENSION")
                        lPensionOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "SOCIAL_SECURITY")
                        lSSOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHER_INCOME")
                        lOtherOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                    //Added by RKaur for GHS 
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET1")
                        lOthOff1TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET2")
                        lOthOff2TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET3")
                        lOthOff3TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "POST_TAX_DED1")
                        lPstTax1TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "POST_TAX_DED2")
                        lPstTax2TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId);
                }
                objReader.Close();

                if (GetSuppInfo(objClaim, objPlan, objDisClass, ref p_dtpTaxes, p_iAccountId, p_iTransTypeCode, ref lSuppSubAcc, ref lSuppBankAcc, ref lSuppTransType, ref lSuppResType, ref bSuppRollUp))
                {

                    objDocumentElement = p_objTable.DocumentElement;
                    //header row
                    objRow = p_objTable.CreateElement("row");
                    objDocumentElement.AppendChild(objRow);
                    objRow.SetAttribute("class", "colheader");
                    objRow.SetAttribute("batchid", "Batch #");
                    objRow.SetAttribute("paymentnumber", "Payment #");
                    objRow.SetAttribute("void", "Void");
                    objRow.SetAttribute("ctl", "Control Number");
                    objRow.SetAttribute("transtype", "Trans Type");
                    objRow.SetAttribute("fromdate", "From Date");
                    objRow.SetAttribute("todate", "To Date");
                    objRow.SetAttribute("printdate", "Print Date");
                    objRow.SetAttribute("gross", "Gross Payment");
                    objRow.SetAttribute("supppayment", "Gross Supp Payment");
                    objRow.SetAttribute("net", "Net Payment");
                    objRow.SetAttribute("dayspaid", "Days Paid");
                    objRow.SetAttribute("fed", "Federal Tax");
                    objRow.SetAttribute("ss", "Social Security Tax");
                    objRow.SetAttribute("med", "Medicare Tax");
                    objRow.SetAttribute("state", "State Tax");
                    objRow.SetAttribute("pension", "Pension Offset");
                    objRow.SetAttribute("ssoffset", "SS Offset");
                    objRow.SetAttribute("oi", "Other Offset");
                    //Added by RKaur for GHS  //pmittal5 Mits 14841
                    objRow.SetAttribute("othoffset1", "Other Offset #1");
                    objRow.SetAttribute("othoffset2", "Other Offset #2");
                    objRow.SetAttribute("othoffset3", "Other Offset #3");
                    objRow.SetAttribute("psttaxded1", "Post Tax Deduction #1");
                    objRow.SetAttribute("psttaxded2", "Post Tax Deduction #2");
                    //End
                    objRow.SetAttribute("account", "Account Name");
                    objRow.SetAttribute("supptranstypecode", "Supp Trans Type");
                    
                    dStartDate = Conversion.ToDate(p_sBenCalcPayStart);
                    dEndDate = Conversion.ToDate(p_sBenCalcPayTo);
                    m_dBenCalcPayStart = dStartDate;
                    m_dBenCalcPayTo = dEndDate;
                    GetPayPeriod(objClaim, iPrefSched, ref dThisPayPeriodStart, ref dThisPayPeriodEnd);
                    dPrintDate = GetNextPrintDate(objClaim, objPlan, objDisClass, p_sBenCalcPayStart, p_sBenCalcPayTo, iPrefSched,iTypeOfMonthlyPay,iPrefDate, false);
                    dCurrentPrintDate = GetNextPrintDate(objClaim, objPlan, objDisClass, p_sBenCalcPayStart, p_sBenCalcPayTo, iPrefSched,iTypeOfMonthlyPay,iPrefDate, true);
                    m_lSuppSubAcc = lSuppSubAcc; //Mits 15804
                    m_lSuppBankAcc = lSuppBankAcc;
                    m_lSuppTransType = lSuppTransType;
                    ilblDaysIncluded = m_ilblDaysIncluded;
                    m_lSuppResType = lSuppResType;
                    m_bSuppRollUp = bSuppRollUp;
                    m_lClaimantPayeeTypeCode = m_objCache.GetCodeId("C", "PAYEE_TYPE");   //MITS 14206
                    m_lOtherPayeeTypeCode = m_objCache.GetCodeId("O", "PAYEE_TYPE");     // MITS 14206

                    //Updating Pension values
                    dPension = m_dPension;
                    dSS = m_dSS;
                    dOther = m_dOther;
                    //updating the GHS Offsets  //pmittal5 Mits 14841
                    dOther1 = m_dOther1;
                    dOther2 = m_dOther2;
                    dOther3 = m_dOther3;
                    dPostTax1  = m_dPostTax1 ;
                    dPostTax2  = m_dPostTax2 ;

                    if(m_dWeeklyBenefit != 0 && m_dGrossCalculatedPayment !=0)
                    {
                        dCalcFactor = m_dGrossCalculatedPayment/m_dWeeklyBenefit;
                    }
                    else
                    {
                        if (m_dSuppRate != 0)
                            dCalcFactor = m_dGrossCalculatedSupplement / m_dSuppRate;
                    }
                    GetWorkWeek(objDisClass , objClaim , ref iDaysOfWeek);
                    
                    //Updating p_dtpTaxes according to Tax Values
                    if (m_dFederalTax > 0)
                        p_dtpTaxes.bFed = true;
                    if (m_dSocialSecurityAmount > 0)
                        p_dtpTaxes.bSS = true;
                    if (m_dMedicareAmount > 0)
                        p_dtpTaxes.bMed = true;
                    if (m_dStateAmount > 0)
                        p_dtpTaxes.bState = true;
                    
                    //begin the transaction
                    //must open a separate database for transaction or get connection leak
                    objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                    objDbConnection.Open();
                    objDbTransaction = objDbConnection.BeginTransaction();

                    DateTime dLastPrintDate = DateTime.MinValue;
                    if ((dPrintDate < dCurrentPrintDate) && (dPrintDate < DateTime.Now.Date))
                    {
                        if (objPlan.PrefPaySchCode == 1737)
                        {
                            dLastPrintDate = dCurrentPrintDate.AddDays(-14);
                        }
                        else if (iPrefSched == 1)
                        {
                            dLastPrintDate = dCurrentPrintDate.AddDays(-7);
                        }
                        else if (iPrefSched == 3)
                        {
                            dLastPrintDate = dCurrentPrintDate.AddDays(-28);
                        }

                        if (DateTime.Now.Date < dLastPrintDate)
                            dPrintDate = dLastPrintDate;
                        else
                            dPrintDate = dCurrentPrintDate;
                    }
                    //R8 enhancement Neha 
                    //Neha Funds Utility Enhancement

                    dPrintDateHS = AdjustPrintDate(objClaim.AdjustPrintDatesFlag, dPrintDate);
                    //Neha Funds Utility Enhancement

                    //
                    //Get the number of days to the end of the Payment Period
                    TimeSpan tDifference = TimeSpan.MinValue;
                    if (dEndDate > dStartDate)
                    {
                        tDifference = dEndDate.Subtract(dStartDate);
                        iDaysToEnd = Convert.ToInt32(tDifference.TotalDays) + 1; //May place limit on days and payments here***
                    }
                    else
                    {
                        tDifference = dStartDate.Subtract(dEndDate);
                        iDaysToEnd = Convert.ToInt32(tDifference.TotalDays) + 1;
                    }
                    if (dThisPayPeriodEnd > dStartDate)
                    {
                        tDifference = dThisPayPeriodEnd.Subtract(dStartDate);
                        iDaysInFirstPayPeriod = Convert.ToInt32(tDifference.TotalDays) + 1; //May place limit on days and payments here***
                    }
                    else
                    {
                        tDifference = dStartDate.Subtract(dThisPayPeriodEnd);
                        iDaysInFirstPayPeriod = Convert.ToInt32(tDifference.TotalDays) + 1;
                    }

                    //Check to see if this Plan is set to Weekly or Bi-Weekly and calculate the number of Payments involved, Number of weeks and number of days remaining for the last payment
                    if (objPlan.PrefPaySchCode == 1737)
                    {
                        iNumberOfPayments = (iDaysToEnd - iDaysInFirstPayPeriod) / 14;
                        //iRemainder = (iDaysToEnd - Val(lblDaysIncluded)) Mod 14
                        Math.DivRem(iDaysToEnd - iDaysInFirstPayPeriod, 14, out iRemainder);
                        iNumberOfWeeks = iNumberOfPayments * 2;
                        iDays = 14;
                    }
                    else if (iPrefSched == 1)
                    {
                        if (objDisClass.Day7WorkWeek)
                        {
                            for (j = 0; j < 7; j++)
                                iDaysOfWeek[j] = -1;
                        }
                        else if (objDisClass.Day5WorkWeek)
                        {
                            for (j = 0; j < 7 ; j++)
                            {
                                //abisht MITS 10780 commented the code
                                //Array begins with 0 and ends at 6 with 0 - Sunday and 6 - Saturday
                                //if (j == 1 || j == 7)    
                                if (j == 0 || j == 6)
                                    iDaysOfWeek[j] = 0;
                                else
                                    iDaysOfWeek[j] = -1;
                            }
                        }
                        else if (objDisClass.ActualWorkWeek)
                        {
                            iDaysOfWeek[0] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkSunFlag, m_iClientId);
                            iDaysOfWeek[1] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkMonFlag, m_iClientId);
                            iDaysOfWeek[2] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkTueFlag, m_iClientId);
                            iDaysOfWeek[2] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkTueFlag, m_iClientId);
                            iDaysOfWeek[3] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkWedFlag, m_iClientId);
                            iDaysOfWeek[4] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkThuFlag, m_iClientId);
                            iDaysOfWeek[5] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkFriFlag, m_iClientId);
                            iDaysOfWeek[6] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkSatFlag, m_iClientId);

                        }
                        else
                        {
                            iDaysOfWeek[0] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkSunFlag, m_iClientId);
                            iDaysOfWeek[1] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkMonFlag, m_iClientId);
                            iDaysOfWeek[2] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkTueFlag, m_iClientId);
                            iDaysOfWeek[3] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkWedFlag, m_iClientId);
                            iDaysOfWeek[4] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkThuFlag, m_iClientId);
                            iDaysOfWeek[5] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkFriFlag, m_iClientId);
                            iDaysOfWeek[6] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkSatFlag, m_iClientId);

                        }

                        ilblDaysIncluded = GetDaysIncluded(objClaim, Conversion.ToDate(p_sBenCalcPayStart), Conversion.ToDate(p_sBenCalcPayTo), iDaysOfWeek, true, false, dThisPayPeriodStart, dThisPayPeriodEnd);
//Code Updated By Pawan for Mits 10780
//iNumberOfPayments = (iDaysToEnd - ilblDaysIncluded) / 7;
                        iNumberOfPayments = (iDaysToEnd - iDaysInFirstPayPeriod) / 7;
                        Math.DivRem(iDaysToEnd - iDaysInFirstPayPeriod, 7, out iRemainder);
                        iNumberOfWeeks = iNumberOfPayments;
                        iDays = 7;
                    }

                    else if (iPrefSched == 3)
                    {
                        dNewDate1 = dThisPayPeriodEnd.AddDays(1);
                        //MITS : 13134  :Umesh 
                        //dNewDate2 = dNewDate1.AddDays(-1).AddMonths(1);
                        dNewDate2 = dNewDate1.AddMonths(1).AddDays(-1);
                        iNumberOfPayments = 0;
                        iRemainder = iDaysToEnd - iDaysInFirstPayPeriod;
                        while (dNewDate2 < Conversion.ToDate(p_sBenCalcPayTo))
                        {
                            iDays = ((int)dNewDate2.Subtract(dNewDate1).TotalDays) + 1;  //pay period days
                            dNewDate1 = dNewDate2.AddDays(1);
                            dNewDate2 = dNewDate1.AddMonths(1).AddDays(-1);
                            iRemainder = iRemainder - iDays;
                            iNumberOfPayments = iNumberOfPayments + 1;
                        };
                    }
                    //If iRemainder is > 0 then we have a partial final payment.
                    //Used to tell how many payments we have for Funds_auto_batch
                    if (iRemainder <= 0)
                        iLastPayment = 0;
                    else
                        iLastPayment = 1;

                    //Calculate the Number of Payments
                    iNumberOfPayments = iNumberOfPayments + iLastPayment + 1;

                    //kdb 04/14/2005 If a supplement is included, double the number of payments

                    //Commented - pmittal5 Mits 15804 04/24/09
                    //if ((objDisClass.SuppPercent > 0) && (!bSuppRollUp))  
                    //    iNumberOfPayments = iNumberOfPayments * 2;

                    if (p_bSave)
                    {
                        //Save Fund_Auto_Batch Record
                        iBatchId = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_BATCH", objDbTransaction, m_iClientId);

                        if (objPlan.StartPayPeriod == "")
                        {
                            sSQL = "INSERT INTO FUNDS_AUTO_BATCH (AUTO_BATCH_ID, TOTAL_PAYMENTS, CURRENT_PAYMENT, PAYMENT_INTERVAL, START_DATE, END_DATE, GROSS_TO_DATE, PAYMENTS_TO_DATE, DISABILITY_FLAG, DAILY_AMOUNT, DAILY_SUPP_AMOUNT, BEN_START_DATE, BEN_END_DATE) VALUES (" + iBatchId + ", " + iNumberOfPayments + ", 1, " + objPlan.PrefPaySchCode + ", '" + Conversion.ToDbDate(dStartDate) + "', '" + Conversion.ToDbDate(dEndDate) + "',  0, 0, -1, " + m_dDailyAmount  + ", " + m_dDailySuppAmount  + ", '" + Conversion.GetDate(m_sBenefitStartDate) + "', '" + Conversion.GetDate(m_sBenefitThroughDate) + "')";
                        }
                        else
                            sSQL = "INSERT INTO FUNDS_AUTO_BATCH (AUTO_BATCH_ID, TOTAL_PAYMENTS, CURRENT_PAYMENT, PAYMENT_INTERVAL, START_DATE, END_DATE, GROSS_TO_DATE, PAYMENTS_TO_DATE, DISABILITY_FLAG, DAILY_AMOUNT, DAILY_SUPP_AMOUNT, PAY_PERIOD_START, BEN_START_DATE, BEN_END_DATE) VALUES (" + iBatchId + ", " + iNumberOfPayments + ", 1, " + objPlan.PrefPaySchCode + ", '" + Conversion.ToDbDate(dStartDate) + "', '" + Conversion.ToDbDate(dEndDate) + "',  0, 0, -1, " + m_dDailyAmount + ", " + m_dDailySuppAmount + ", " + objPlan.StartPayPeriod + ", '" + p_sBenefitStartDate + "', '" + Conversion.GetDate(m_sBenefitThroughDate) + "')";

                        //execute sql
                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);


                        //kdb 04/14/2005 If a supplement is included, cut number of payments in half to
                        //equal original amount

                        //Commented - pmittal5 Mits 15804 04/24/09
                        //if (objDisClass.SuppPercent > 0 && (!bSuppRollUp))
                        //    iNumberOfPayments = iNumberOfPayments / 2;

                    }
                    for (i = 1; i <= iNumberOfPayments; i++)
                    {
                        if (i == 1)  //First Payment is usually a partial Payment
                        {
                            //Calculate the payment amounts and dates
                            iNumOfPays = ilblDaysIncluded;

                            if (ilblDaysIncluded > 1)
                            {
                                if (iNumberOfPayments == 1)
                                    dToDate = dEndDate;
                                else
                                    dToDate = dThisPayPeriodEnd;
                            }
                            else if (p_sBenCalcPayStart == p_sBenCalcPayTo)
                            {
                                dToDate = dEndDate;
                            }
                            else
                                dToDate = dThisPayPeriodEnd;


                            //Calculate Offsets kdb 04/25/2005
                            //Pension Offset
                            if (m_dPension > 0)
                            {
                                if (PartialPay(iNumOfPays, iPrefSched, objDisClass))
                                {
                                    if (m_iOffsetCalc == 1 || m_iOffsetCalc == 2)
                                    {
                                        if (objDisClass.FullpayOnlyFlag)
                                            dPensionOffset = 0;
                                        //else
                                        else if (objDisClass.ProrateOffFlag)  //pmittal5 11/13/08 Mits 12620
                                            //dPensionOffset = m_dPension * iNumOfPays;
                                            dPensionOffset = Math.Round(m_dPensionOffset * dCalcFactor, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                                        //pmittal5 11/13/08 Mits 12620
                                        else
                                        {
                                            if (iPrefSched == 2)
                                            {
                                                dPensionOffset = m_dPensionOffset * 2;
                                            }
                                            else
                                            {
                                                dPensionOffset = m_dPensionOffset;
                                            }
                                        }//End - pmittal5
                                    }
                                    else
                                        dPensionOffset = 0;
                                }
                                //else if (objDisClass.ProrateOffFlag)
                                //    dPensionOffset = m_dPension * iNumOfPays;
                                //else
                                //    dPensionOffset = m_dPensionOffset;
                                else
                                {
                                    if (iPrefSched == 2)
                                    {
                                        dPensionOffset = m_dPensionOffset * 2;
                                    }
                                    else if (iPrefSched == 3 && objDisClass.ProrateOffFlag)
                                    {
                                        //dPensionOffset = m_dPension * iNumOfPays;
                                        dPensionOffset = Math.Round(m_dPensionOffset * dCalcFactor, 2, MidpointRounding.AwayFromZero);        //MITS 14186
                                    }
                                    else
                                    {
                                        dPensionOffset = m_dPensionOffset;
                                    }
                                }
                            }
                            //SS Offset
                            if (m_dSS > 0)
                            {
                                if (PartialPay(iNumOfPays, iPrefSched, objDisClass))
                                {
                                    if (m_iOffsetCalc == 1 || m_iOffsetCalc == 2)
                                    {
                                        if (objDisClass.FullpayOnlyFlag)
                                            dSSOffset = 0;
                                        //else
                                        else if (objDisClass.ProrateOffFlag)  //pmittal5 11/13/08 Mits 12620
                                            //dSSOffset = m_dSS * iNumOfPays;
                                            dSSOffset = Math.Round(m_dSSOffset * dCalcFactor, 2, MidpointRounding.AwayFromZero); //MITS 14186
                                        //pmittal5 11/13/08 Mits 12620
                                        else
                                        {
                                            if (iPrefSched == 2)
                                            {
                                                dSSOffset = m_dSSOffset * 2;
                                            }
                                            else
                                            {
                                                dSSOffset = m_dSSOffset;
                                            }
                                        }//End - pmittal5
                                    }
                                    else
                                        dSSOffset = 0;
                                }
                                //else if (objDisClass.ProrateOffFlag)
                                //    dSSOffset = m_dSS * iNumOfPays;
                                //else
                                //    dSSOffset = m_dSSOffset;
                                else
                                {
                                    if (iPrefSched == 2)
                                    {
                                        dSSOffset = m_dSSOffset * 2;
                                    }
                                    else if (iPrefSched == 3 && objDisClass.ProrateOffFlag)
                                    {
                                        //dSSOffset = m_dSS * iNumOfPays;
                                        dSSOffset = Math.Round(m_dSSOffset * dCalcFactor, 2, MidpointRounding.AwayFromZero); //MITS 14186
                                    }
                                    else
                                    {
                                        dSSOffset = m_dSSOffset;
                                    }
                                }
                            }
                            //Other Offset
                            if (m_dOther > 0)
                            {
                                if (PartialPay(iNumOfPays, iPrefSched, objDisClass))
                                {
                                    if (m_iOffsetCalc == 1 || m_iOffsetCalc == 2)
                                    {
                                        if (objDisClass.FullpayOnlyFlag)
                                            dOtherOffset = 0;
                                        //else
                                        else if (objDisClass.ProrateOffFlag)  //pmittal5 11/13/08 Mits 12620
                                            //dOtherOffset = m_dOther * iNumOfPays;
                                            dOtherOffset = Math.Round(m_dOiOffset * dCalcFactor, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                                        //pmittal5 11/13/08 Mits 12620
                                        else
                                        {
                                            if (iPrefSched == 2)
                                            {
                                                dOtherOffset = m_dOiOffset * 2;
                                            }
                                            else
                                            {
                                                dOtherOffset = m_dOiOffset;
                                            }
                                        }//End - pmittal5
                                    }
                                    else
                                        dOtherOffset = 0;
                                }
                                //else if (objDisClass.ProrateOffFlag)
                                //    dOtherOffset = m_dOther * iNumOfPays;
                                else
                                {
                                    if (iPrefSched == 2)
                                    { 
                                        dOtherOffset = m_dOiOffset * 2; 
                                    }
                                    else if (iPrefSched == 3 && objDisClass.ProrateOffFlag)
                                    {
                                       // dOtherOffset = m_dOther * iNumOfPays;
                                        dOtherOffset = Math.Round(m_dOiOffset * dCalcFactor, 2, MidpointRounding.AwayFromZero); //MITS 14186
                                    }
                                    else
                                    {
                                        dOtherOffset = m_dOiOffset;
                                    }
                                }
                            }

                            //********************************************
                            //Animesh Inserted with modified logice for PS2  //pmittal5 Mits 14841
                            if (m_dOther1 > 0)
                            {
                                if (PartialPay(iNumOfPays, iPrefSched, objDisClass))
                                {
                                    if (m_iOffsetCalc == 1 || m_iOffsetCalc == 2)
                                    {
                                        if (objDisClass.FullpayOnlyFlag)
                                            dOtherOffset_1 = 0;
                                        else if (objDisClass.ProrateOffFlag)
                                            //dOtherOffset_1 = m_dOther1 * iNumOfPays;
                                            dOtherOffset_1 = Math.Round(m_dOthOffset1 * dCalcFactor, 2, MidpointRounding.AwayFromZero); //MITS 14186
                                        else
                                        {
                                            if (iPrefSched == 2)
                                            {
                                                dOtherOffset_1 = m_dOthOffset1 * 2;
                                            }
                                            else
                                            {
                                                dOtherOffset_1 = m_dOthOffset1;
                                            }
                                        }
                                    }
                                    else
                                        dOtherOffset_1 = 0;
                                }
                                else
                                {
                                    if (iPrefSched == 2)
                                    {
                                        dOtherOffset_1 = m_dOthOffset1 * 2;
                                    }
                                    else if (iPrefSched == 3 && objDisClass.ProrateOffFlag)
                                    {
                                        //dOtherOffset_1 = m_dOther1 * iNumOfPays;
                                        dOtherOffset_1 = Math.Round(m_dOthOffset1 * dCalcFactor, 2, MidpointRounding.AwayFromZero); //MITS 14186
                                    }
                                    else
                                    {
                                        dOtherOffset_1 = m_dOthOffset1;
                                    }
                                }
                            }
                            if (m_dOther2 > 0)
                            {
                                if (PartialPay(iNumOfPays, iPrefSched, objDisClass))
                                {
                                    if (m_iOffsetCalc == 1 || m_iOffsetCalc == 2)
                                    {
                                        if (objDisClass.FullpayOnlyFlag)
                                            dOtherOffset_2 = 0;
                                        else if (objDisClass.ProrateOffFlag)
                                            //dOtherOffset_2 = m_dOther2 * iNumOfPays;
                                            dOtherOffset_2 = Math.Round(m_dOthOffset2 * dCalcFactor, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                                        else
                                        {
                                            if (iPrefSched == 2)
                                            {
                                                dOtherOffset_2 = m_dOthOffset2 * 2;
                                            }
                                            else
                                            {
                                                dOtherOffset_2 = m_dOthOffset2;
                                            }
                                        }
                                    }
                                    else
                                        dOtherOffset_2 = 0;
                                }
                                else
                                {
                                    if (iPrefSched == 2)
                                    {
                                        dOtherOffset_2 = m_dOthOffset2 * 2;
                                    }
                                    else if (iPrefSched == 3 && objDisClass.ProrateOffFlag)
                                    {
                                        //dOtherOffset_2 = m_dOther2 * iNumOfPays;
                                        dOtherOffset_2 = Math.Round(m_dOthOffset2 * dCalcFactor, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                                    }
                                    else
                                    {
                                        dOtherOffset_2 = m_dOthOffset2;
                                    }
                                }
                            }
                            if (m_dOther3 > 0)
                            {
                                if (PartialPay(iNumOfPays, iPrefSched, objDisClass))
                                {
                                    if (m_iOffsetCalc == 1 || m_iOffsetCalc == 2)
                                    {
                                        if (objDisClass.FullpayOnlyFlag)
                                            dOtherOffset_3 = 0;
                                        else if (objDisClass.ProrateOffFlag)
                                            //dOtherOffset_3 = m_dOther3 * iNumOfPays;
                                            dOtherOffset_3 = Math.Round(m_dOthOffset3 * dCalcFactor, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                                        else
                                        {
                                            if (iPrefSched == 2)
                                            {
                                                dOtherOffset_3 = m_dOthOffset3 * 2;
                                            }
                                            else
                                            {
                                                dOtherOffset_3 = m_dOthOffset3;
                                            }
                                        }
                                    }
                                    else
                                        dOtherOffset_3 = 0;
                                }
                                else
                                {
                                    if (iPrefSched == 2)
                                    {
                                        dOtherOffset_3 = m_dOthOffset3 * 2;
                                    }
                                    else if (iPrefSched == 3 && objDisClass.ProrateOffFlag)
                                    {
                                        //dOtherOffset_3 = m_dOther3 * iNumOfPays;
                                        dOtherOffset_3 = Math.Round(m_dOthOffset3 * dCalcFactor, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                                    }
                                    else
                                    {
                                        dOtherOffset_3 = m_dOthOffset3;
                                    }
                                }
                            }
                            if (m_dPostTax1 > 0)
                            {
                                dPstTaxDed_1 = m_dPstTaxDed1;  //pmittal5 Mits 14841
                            }
                            if (m_dPostTax2 > 0)
                            {
                                dPstTaxDed_2 = m_dPstTaxDed2;  //pmittal5 Mits 14841
                            }
                            //Animesh Insertion ends
                            //*********************************************


                            dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset), 2, MidpointRounding.AwayFromZero);
                            //Animesh Inserted For GHS  //pmittal5 Mits 14841
                            //3 more offsets need to be added 
                            dTotalOffset = Math.Round(dTotalOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3, 2, MidpointRounding.AwayFromZero);
                            //Animesh Insertion ends

                            //Process supp and core separate payments first
                            //if ((objDisClass.SuppPercent > 0) && (bSuppRollUp == false))
                            if ((m_dSuppRate != 0) && (bSuppRollUp == false)) //Mits 15804
                            {
                                for (j = 1; j <= 2; j++) //Loop twice, first to process Core, second to process Supplement
                                {
                                    //Subtract offsets from payment
                                    if (bSuppPaymentFlag == false)
                                    {
                                        //Set up temporary offset variables to be used for
                                        //Supplement offset amounts
                                        dTempPensionOffset = 0;
                                        dTempSSOffset = 0;
                                        dTempOtherOffset = 0;
                                        //Added by RKaur
                                        dTempOffset1 = 0;
                                        dTempOffset2 = 0;
                                        dTempOffset3 = 0;
                                        dTempPstTD1 = 0; 
                                        dTempPstTD2 = 0;
                                        ////////////

                                        dPayment = Math.Round(m_dDailyAmount * iNumOfPays, 2, MidpointRounding.AwayFromZero);
                                        dPaymentOffsetDif = Math.Round((dPayment - dTotalOffset), 2, MidpointRounding.AwayFromZero);
                                        if (dPaymentOffsetDif <= 0)
                                        {
                                            dTempOffset = dPayment - 1;
                                            //get offsets amounts here
                                            if (dPensionOffset < dTempOffset)
                                                dTempOffset = Math.Round((dTempOffset - dPensionOffset), 2, MidpointRounding.AwayFromZero);
                                            else if (dPensionOffset == dTempOffset)
                                                dTempOffset = 0;
                                            else
                                            {
                                                dTempPensionOffset = Math.Round((dPensionOffset - dTempOffset), 2, MidpointRounding.AwayFromZero);
                                                dPensionOffset = dTempOffset;
                                                dTempSSOffset = dSSOffset;
                                                dTempOtherOffset = dOtherOffset;
                                                //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                dTempOffset1=  dOtherOffset_1;
                                                dTempOffset2 = dOtherOffset_2;
                                                dTempOffset3 = dOtherOffset_3;
                                                dTempPstTD1 = dPstTaxDed_1; 
                                                dTempPstTD2 = dPstTaxDed_2; 
                                                //Animesh Insertion ends
                                                dSSOffset = 0;
                                                dOtherOffset = 0;
                                                //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                dOtherOffset_1 = 0;
                                                dOtherOffset_2 = 0;
                                                dOtherOffset_3 = 0;
                                                dPstTaxDed_1 = 0;  
                                                dPstTaxDed_2 = 0;
                                                //Animesh Insertion ends
                                                goto EndOffsetCalc;
                                            }
                                            if (dSSOffset < dTempOffset)
                                                dTempOffset = Math.Round((dTempOffset - dSSOffset), 2, MidpointRounding.AwayFromZero);
                                            else if (dSSOffset == dTempOffset)
                                                dTempOffset = 0;
                                            else
                                            {
                                                dTempSSOffset = Math.Round((dSSOffset - dTempOffset), 2, MidpointRounding.AwayFromZero);
                                                dSSOffset = dTempOffset;
                                                dTempOtherOffset = dOtherOffset;
                                                //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                dTempOffset1 = dOtherOffset_1;
                                                dTempOffset2 = dOtherOffset_2;
                                                dTempOffset3 = dOtherOffset_3;
                                                dTempPstTD1 = dPstTaxDed_1;  
                                                dTempPstTD2 = dPstTaxDed_2;
                                                //Animesh Insertion ends
                                                dOtherOffset = 0;
                                                //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                dOtherOffset_1 = 0;
                                                dOtherOffset_2 = 0;
                                                dOtherOffset_3 = 0;
                                                dPstTaxDed_1 = 0; 
                                                dPstTaxDed_2 = 0;
                                                //Animesh Insertion ends
                                                goto EndOffsetCalc;
                                            }
                                            if (dOtherOffset < dTempOffset)
                                                dTempOffset = Math.Round((dTempOffset - dOtherOffset), 2, MidpointRounding.AwayFromZero);
                                            else if (dOtherOffset == dTempOffset)
                                                dTempOffset = 0;
                                            else
                                            {
                                                dTempOtherOffset = Math.Round((dOtherOffset - dTempOffset), 2, MidpointRounding.AwayFromZero);
                                                dOtherOffset = dTempOffset;
                                                //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                dTempOffset1 = dOtherOffset_1;
                                                dTempOffset2 = dOtherOffset_2;
                                                dTempOffset3 = dOtherOffset_3;
                                                dTempPstTD1 = dPstTaxDed_1; 
                                                dTempPstTD2 = dPstTaxDed_2;
                                                //Animesh Insertion ends
                                                //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                dOtherOffset_1 = 0;
                                                dOtherOffset_2 = 0;
                                                dOtherOffset_3 = 0;
                                                dPstTaxDed_1 = 0;
                                                dPstTaxDed_2 = 0;
                                                goto EndOffsetCalc;
                                                //Animesh Insertion ends
                                            }
                                            //**********************************
                                            //Animesh Inserted for GHS Enhancement  //pmittal5 Mits 14841
                                            if (dOtherOffset_1 < dTempOffset)
                                                dTempOffset = dTempOffset - dOtherOffset_1;
                                            else if (dOtherOffset_1 == dTempOffset)
                                                dTempOffset = 0;
                                            else
                                            {
                                                dTempOffset1 = dOtherOffset_1 - dTempOffset;
                                                dOtherOffset_1 = dTempOffset;
                                                //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                dTempOffset2 = dOtherOffset_2;
                                                dTempOffset3 = dOtherOffset_3;
                                                dTempPstTD1 = dPstTaxDed_1;  
                                                dTempPstTD2 = dPstTaxDed_2;
                                                //Animesh Insertion ends
                                                dOtherOffset_2 = 0;
                                                dOtherOffset_3 = 0;
                                                dPstTaxDed_1 = 0;  
                                                dPstTaxDed_2 = 0;
                                                goto EndOffsetCalc;
                                                //Animesh Insertion ends
                                            }
                                            if (dOtherOffset_2 < dTempOffset)
                                                dTempOffset = dTempOffset - dOtherOffset_2;
                                            else if (dOtherOffset_2 == dTempOffset)
                                                dTempOffset = 0;
                                            else
                                            {
                                                dTempOffset2 = dOtherOffset_2 - dTempOffset;
                                                dOtherOffset_2 = dTempOffset;
                                                //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                dTempOffset3 = dOtherOffset_3;
                                                dTempPstTD1 = dPstTaxDed_1; 
                                                dTempPstTD2 = dPstTaxDed_2;
                                                //Animesh Insertion ends
                                                dOtherOffset_3 = 0;
                                                dPstTaxDed_1 = 0; 
                                                dPstTaxDed_2 = 0;
                                                goto EndOffsetCalc;
                                                //Animesh Insertion ends
                                            }
                                            if (dOtherOffset_3 < dTempOffset)
                                                dTempOffset = dTempOffset - dOtherOffset_3;
                                            else if (dOtherOffset_3 == dTempOffset)
                                                dTempOffset = 0;
                                            else
                                            {
                                                dTempOffset3 = dOtherOffset_3 - dTempOffset;
                                                dOtherOffset_3 = dTempOffset;
                                                //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                dTempPstTD1 = dPstTaxDed_1; 
                                                dTempPstTD2 = dPstTaxDed_2;
                                                //Animesh Insertion ends
                                                dPstTaxDed_1 = 0; 
                                                dPstTaxDed_2 = 0;
                                                goto EndOffsetCalc;
                                                //Animesh Insertion ends
                                            }
                                            if (dPstTaxDed_1 < dTempOffset)    //pmittal5 Mits 14841 - Post Tax deductions should not change
                                                dTempOffset = dTempOffset - dPstTaxDed_1;
                                            else if (dPstTaxDed_1 == dTempOffset)
                                                dTempOffset = 0;
                                            else
                                            {
                                                dTempPstTD1 = dPstTaxDed_1 - dTempOffset;
                                                dPstTaxDed_1 = dTempOffset;
                                                //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                dTempPstTD2 = dPstTaxDed_2;
                                                //Animesh Insertion ends
                                                dPstTaxDed_2 = 0;
                                                goto EndOffsetCalc;
                                                //Animesh Insertion ends
                                            }
                                            if (dPstTaxDed_2 < dTempOffset)
                                                dTempOffset = dTempOffset - dPstTaxDed_2;
                                            else if (dPstTaxDed_2 == dTempOffset)
                                                dTempOffset = 0;
                                            else
                                            {
                                                dTempPstTD2 = dPstTaxDed_2 - dTempOffset;
                                                dPstTaxDed_2 = dTempOffset;
                                            }
                                            //**********************************
                                            

                                        EndOffsetCalc:
                                            dPayment = 1;
                                        }
                                        else
                                            dPayment = dPaymentOffsetDif;

                                        dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset), 2, MidpointRounding.AwayFromZero);
                                        //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                        dTotalOffset = Math.Round(dTotalOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3, 2, MidpointRounding.AwayFromZero);
                                        //Animesh Insertion Ends
                                    }
                                    else if (bSuppPaymentFlag)
                                    {
                                        //Animesh Inserted  //pmittal5 Mits 14841
                                        blnPaymentDeleted = false;  
                                        //Animesh Inserttion ends
                                        dPayment = Math.Round(m_dDailySuppAmount * iNumOfPays, 2, MidpointRounding.AwayFromZero);
                                        dPensionOffset = dTempPensionOffset;
                                        dSSOffset = dTempSSOffset;
                                        dOtherOffset = dTempOtherOffset;
                                        //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                        dOtherOffset_1 = dTempOffset1;
                                        dOtherOffset_2 = dTempOffset2;
                                        dOtherOffset_3 = dTempOffset3;
                                        dPstTaxDed_1 = dTempPstTD1; 
                                        dPstTaxDed_2 = dTempPstTD2;
                                        //Animesh Insertion ends
                                        dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset), 2, MidpointRounding.AwayFromZero);
                                        //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                        dTotalOffset = Math.Round((dTotalOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3), 2, MidpointRounding.AwayFromZero);
                                        //Animesh Insertion Ends

                                        dPayment = Math.Round((dPayment - dTotalOffset), 2, MidpointRounding.AwayFromZero);
                                        //pmittal5 Mits 14253 01/15/09 - Allow Zero Payments
                                        //if (dPayment <= 0)
                                        //{
                                        //    DeletePayments("0," + iBatchId.ToString());
                                        //    return 0;
                                        //}
                                        //MITS 14186  
                                        //As discussed with Mike Hamann , if  dPayment - dTotalOffset = 0.01
                                        //deduct 1 cent from the offset having maximum value and make  net payment = 0

                                        if (dPayment < 0 && dPayment >= -0.01)
                                        {
                                            
                                            double[] dArrayofOffsets = new double[] { dPensionOffset, dSSOffset, dOtherOffset, dOtherOffset_1, dOtherOffset_2, dOtherOffset_3 };
                                            int iIndex = GetMaxOffsetIndex(dArrayofOffsets);

                                            //deducting 1 Cent from offset having maximum value
                                            dArrayofOffsets[iIndex] = Math.Round((dArrayofOffsets[iIndex] - dOneCent), 2, MidpointRounding.AwayFromZero);

                                            dPensionOffset = dArrayofOffsets[0];
                                            dSSOffset = dArrayofOffsets[1];
                                            dOtherOffset = dArrayofOffsets[2];
                                            dOtherOffset_1 = dArrayofOffsets[3];
                                            dOtherOffset_2 = dArrayofOffsets[4];
                                            dOtherOffset_3 = dArrayofOffsets[5];
                                            dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3), 2, MidpointRounding.AwayFromZero);
                                            dPayment = 0.00;
                                            

                                        }
                                        if (dPayment < 0)
                                        {
                                            if (p_bSave)
                                            {
                                                UpdateAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                                blnPaymentDeleted = true;  
                                            }
                                            continue;
                                        }
                                        //End - pmittal5
                                    }
                                    lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);

                                    //Figure the tax which needs to be subtracted from the amount when saved to Funds_auto
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax > 0)
                                    if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax >= 0)
                                    {
                                        if (p_dtpTaxes.dblTaxablePercent > 0)
                                            dFedTax = Math.Round(dPayment * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.FED_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                        else
                                            dFedTax = Math.Round(dPayment * p_dtpTaxes.FED_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                        dFedTax = 0;

                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount > 0)
                                    if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount >= 0)
                                    {
                                        if (p_dtpTaxes.dblTaxablePercent > 0)
                                            dSSTax = Math.Round(dPayment * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.SS_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                        else
                                            dSSTax = Math.Round(dPayment * p_dtpTaxes.SS_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                        dSSTax = 0;

                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount > 0)
                                    if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount >= 0)
                                    {
                                        if (p_dtpTaxes.dblTaxablePercent > 0)
                                            dMedTax = Math.Round(dPayment * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.MEDICARE_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                        else
                                            dMedTax = Math.Round(dPayment * p_dtpTaxes.MEDICARE_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                        dMedTax = 0;

                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount > 0)
                                    if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount >= 0)
                                    {
                                        if (p_dtpTaxes.dblTaxablePercent > 0)
                                            dStateTax = Math.Round(dPayment * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.STATE_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                        else
                                            dStateTax = Math.Round(dPayment * p_dtpTaxes.STATE_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                        dStateTax = 0;

                                    dTotalTax = Math.Round((dFedTax + dSSTax + dMedTax + dStateTax), 2, MidpointRounding.AwayFromZero);

                                    //kdb 04/28/2005 Set up sAppend for the control number
                                    if (bSuppPaymentFlag)
                                        sAppend = "S";
                                    else
                                        sAppend = "";

                                    //Logic Modified to include zero dollar payment -- Animesh  //pmittal5 Mits 14841
                                    //Animesh Inserted To check the Negative payment won't get saved after Post tax deduction 
                                    //if (dPayment - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2) <= 0)
                                    //{
                                    //    DeletePayments("0," + iBatchId.ToString());
                                    //    return 0;
                                    //}
                                    if (Math.Round(dPayment - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2), 2, MidpointRounding.AwayFromZero) < 0)  //pmittal5 Mits 14841
                                    {
                                        if (p_bSave && blnPaymentDeleted==false)
                                            UpdateAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                        if (bSuppPaymentFlag == false) //pmittal5 Mits 14841
                                            bSuppPaymentFlag = true;
                                        continue;
                                    }
                                    //Animesh Insertion Ends

                                    //Save the payment to Funds_auto table
                                    if (p_bSave)
                                    {
                                        //Animesh Modified For GHS Enhancement as post tax deduction need to be deducted //pmittal5 Mits 14841
                                        //sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, objClaim.PrimaryClaimant.ClaimantEid, (dPayment - dTotalTax), 0, i, false, false, iNumberOfPayments, dPrintDate, sAppend, iNumOfPays, bSuppPaymentFlag);
                                        sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, objClaim.PrimaryClaimant.ClaimantEid, Math.Round((dPayment - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2)), 2, MidpointRounding.AwayFromZero), 0, i, false, false, iNumberOfPayments, dPrintDateHS, sAppend, iNumOfPays, bSuppPaymentFlag);
                                        //Animesh Modification Ends
                                        //execute sql
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                    }

                                    if (bSuppPaymentFlag == true)
                                        lTransType = m_lSuppTransType;
                                    else
                                        lTransType = p_iTransTypeCode;

                                    if (p_bSave)
                                    {
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lTransType, Math.Round((dPayment + dTotalOffset), 2, MidpointRounding.AwayFromZero), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "0", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                        //Save Third Party Splits to Funds_auto_split table
                                        //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                        //if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax > 0)
                                        if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax >= 0)
                                        {
                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iFedTransTypeCodeID, (dFedTax * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "1", bSuppPaymentFlag);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                        //if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount > 0)
                                        if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount >= 0)
                                        {
                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iSSTransTypeCodeID, (dSSTax * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "2", bSuppPaymentFlag);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                        //if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount > 0)
                                        if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount >= 0)
                                        {
                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iMedicareTransTypeCodeID, (dMedTax * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "3", bSuppPaymentFlag);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                        //if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount > 0)
                                        if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount >= 0)
                                        {
                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iStateTransTypeCodeID, (dStateTax * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "4", bSuppPaymentFlag);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }


                                        //KDB 04/25/2005 Save Offsets splits to FUNDS_AUTO_SPLIT table
                                        if (dPensionOffset > 0)
                                        {
                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPensionOffTransTypeCodeID, (dPensionOffset * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "5", bSuppPaymentFlag);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        if (dSSOffset > 0)
                                        {
                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lSSOffTransTypeCodeID, (dSSOffset * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "6", bSuppPaymentFlag);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        if (dOtherOffset > 0)
                                        {
                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOtherOffTransTypeCodeID, (dOtherOffset * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "7", bSuppPaymentFlag);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }

                                        //Added by Ravneet Kaur //pmittal5 Mits 14841
                                        if (dOtherOffset_1  > 0)
                                        {
                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOthOff1TransTypeCodeID , (dOtherOffset_1  * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "9", bSuppPaymentFlag);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        if (dOtherOffset_2 > 0)
                                        {
                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOthOff2TransTypeCodeID, (dOtherOffset_2 * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "10", bSuppPaymentFlag);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        if (dOtherOffset_3 > 0)
                                        {
                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOthOff3TransTypeCodeID, (dOtherOffset_3 * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "11", bSuppPaymentFlag);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        if (dPstTaxDed_1 > 0)
                                        {
                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPstTax1TransTypeCodeID , (dPstTaxDed_1 * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "12", bSuppPaymentFlag);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        if (dPstTaxDed_2 > 0)
                                        {
                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPstTax2TransTypeCodeID, (dPstTaxDed_2 * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "13", bSuppPaymentFlag);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        //Ends

                                        //Save Split Payments to Funds_auto and Funds_auto_split tables
                                        //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                        //if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax > 0)
                                        if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax >= 0)
                                        {
                                            lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                            sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iFedTaxEID, dFedTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, sAppend + "T-1", iNumOfPays, false);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iFedTransTypeCodeID, dFedTax, Conversion.ToDate(p_sBenCalcPayStart), dToDate, "1", false);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                        //if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount > 0)
                                        if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount >= 0)
                                        {
                                            lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                            sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iSSTaxEID, dSSTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, sAppend + "T-2", iNumOfPays, false);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iSSTransTypeCodeID, dSSTax, Conversion.ToDate(p_sBenCalcPayStart), dToDate, "2", false);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                        //if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount > 0)
                                        if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount >= 0)
                                        {
                                            lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                            sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iMedicareTaxEID, dMedTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, sAppend + "T-3", iNumOfPays, false);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iMedicareTransTypeCodeID, dMedTax, Conversion.ToDate(p_sBenCalcPayStart), dToDate, "3", false);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                        //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                        //if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount > 0)
                                        if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount >= 0)
                                        {
                                            lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                            sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iStateTaxEID, dStateTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, sAppend + "T-4", iNumOfPays, false);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                            sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iStateTransTypeCodeID, dStateTax, Conversion.ToDate(p_sBenCalcPayStart), dToDate, "4", false);
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                        }
                                    }

                                    bSuppPaymentFlag = true; //kdb 04/15/2005  Flip the flag for marking payments as Supp

                                    //Added by RKaur on 04/09/2009 //pmittal5 Mits 14841
                                    dtempNetPayment = Math.Round(dPayment - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2), 2, MidpointRounding.AwayFromZero);

                                    //if (dtempNetPayment < 0)  //pmittal5 Mits 14841
                                    //    continue;
                                    ///Appending data to output XML
                                    objRow = p_objTable.CreateElement("row");
                                    objDocumentElement.AppendChild(objRow);
                                    objRow.SetAttribute("class", "rowlight");
                                    objRow.SetAttribute("gross", string.Format("{0:0.00}", dPayment + dTotalOffset));
                                    objRow.SetAttribute("supppayment", "");
                                    objRow.SetAttribute("paymentnumber", i.ToString());
                                    objRow.SetAttribute("fromdate", Conversion.ToDate(p_sBenCalcPayStart).ToShortDateString());
                                    objRow.SetAttribute("todate", dToDate.ToShortDateString());
                                    objRow.SetAttribute("printdate", dPrintDateHS.ToShortDateString());

                                    //Added by RKaur on 04/09/2009 //pmittal5 Mits 14841
                                    objRow.SetAttribute("net", string.Format("{0:0.00}", dtempNetPayment));

                                    //objRow.SetAttribute("net", string.Format("{0:0.00}", dPayment - dTotalTax));
                                    //Animesh Modified for GHS Enhancment
                                    //dtempNetPayment = Math.Round(dPayment - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2),2);
                                    //if (dtempNetPayment > 0)
                                    //{
                                    //    objRow.SetAttribute("net", string.Format("{0:0.00}", dtempNetPayment ));
                                    //}
                                    //else
                                    //{
                                    //    objRow.SetAttribute("net", string.Format("{0:0.00}", 0));
                                    //}
                                    //Animesh Modification ends
                                    objRow.SetAttribute("fed", string.Format("{0:0.00}", dFedTax * -1));
                                    objRow.SetAttribute("ss", string.Format("{0:0.00}", dSSTax * -1));
                                    objRow.SetAttribute("med", string.Format("{0:0.00}", dMedTax * -1));
                                    objRow.SetAttribute("state", string.Format("{0:0.00}", dStateTax * -1));
                                    objRow.SetAttribute("pension", string.Format("{0:0.00}", dPensionOffset * -1));
                                    objRow.SetAttribute("ssoffset", string.Format("{0:0.00}", dSSOffset * -1));
                                    objRow.SetAttribute("oi", string.Format("{0:0.00}", dOtherOffset * -1));
                                    //Added by Ravneet Kaur //pmittal5 Mits 14841
                                    objRow.SetAttribute("othoffset1", string.Format("{0:0.00}", dOtherOffset_1 * -1));
                                    objRow.SetAttribute("othoffset2", string.Format("{0:0.00}", dOtherOffset_2 * -1));
                                    objRow.SetAttribute("othoffset3", string.Format("{0:0.00}", dOtherOffset_3 * -1));
                                    objRow.SetAttribute("psttaxded1", string.Format("{0:0.00}", dPstTaxDed_1 * -1));
                                    objRow.SetAttribute("psttaxded2", string.Format("{0:0.00}", dPstTaxDed_2 * -1));
                                    //Ends
                                    //abisht MITS 10780 commented the code.
                                    //Wrong formatting to decimal.
                                    //Formatting it to string now
                                    //objRow.SetAttribute("dayspaid", string.Format("{0:0.00}", iNumOfPays));
                                    objRow.SetAttribute("dayspaid", iNumOfPays.ToString());
                                }


                                //******END FIRST SUPPLEMENT + CORE PAYMENT******
                            }
                            else
                            {
                                //pmittal5 Mits 14253 01/19/09 - Skip Negative Payments
                                if (iNumberOfPayments == 1 && m_dNetPayment < 0)
                                {
                                    if (p_bSave)
                                        DeleteAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                    continue;
                                }
                                else if (m_dNetPayment < 0)
                                {
                                    if (p_bSave)
                                        UpdateAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                    continue;
                                }//End - pmittal5

                                //MITS 14186  
                                //As discussed with Mike Hamann , if  dPayment - dTotalOffset = 0.01
                                //deduct 1 cent from the offset having maximum value and make  net payment = 0

                                if (Math.Round((m_dGrossCalculatedPayment + m_dGrossCalculatedSupplement - dTotalOffset), 2, MidpointRounding.AwayFromZero) < 0 && Math.Round((m_dGrossCalculatedPayment + m_dGrossCalculatedSupplement - dTotalOffset), 2, MidpointRounding.AwayFromZero) >= -0.01)
                                {
                                    double[] dArrayofOffsets = new double[] { dPensionOffset, dSSOffset, dOtherOffset,dOtherOffset_1,dOtherOffset_2,dOtherOffset_3 };
                                    int iIndex = GetMaxOffsetIndex(dArrayofOffsets);

                                    //deducting 1 Cent from offset having maximum value
                                    dArrayofOffsets[iIndex] = Math.Round((dArrayofOffsets[iIndex] - dOneCent), 2, MidpointRounding.AwayFromZero);

                                    dPensionOffset = dArrayofOffsets[0];
                                    dSSOffset = dArrayofOffsets[1];
                                    dOtherOffset = dArrayofOffsets[2];
                                    dOtherOffset_1 = dArrayofOffsets[3];
                                    dOtherOffset_2 = dArrayofOffsets[4];
                                    dOtherOffset_3 = dArrayofOffsets[5];
                                    dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3), 2, MidpointRounding.AwayFromZero);
                                }

                                if (p_bSave)
                                {
                                    //Get payment amounts from the screen
                                    lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                    sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, objClaim.PrimaryClaimant.ClaimantEid, m_dNetPayment, 0, i, false, false, iNumberOfPayments, dPrintDateHS, "", ilblDaysIncluded, false);
                                    objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                    lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                    sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_iTransTypeCode, m_dGrossCalculatedPayment, Conversion.ToDate(p_sBenCalcPayStart), dToDate, "0", false);
                                    objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                    //kdb 04/28/2005 Save the supplement as a split if it exists
                                    if (m_dGrossCalculatedSupplement > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        lTransType = m_lSuppTransType;
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lTransType, m_dGrossCalculatedSupplement, Conversion.ToDate(p_sBenCalcPayStart), dToDate, "8", true);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                    }

                                    //Get the Tax amounts from the screen because they have already been calculated
                                    //Save Third Party Splits
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax > 0)
                                    if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax >= 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iFedTransTypeCodeID, (m_dFederalTax * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "1", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount > 0)
                                    if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount >= 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iSSTransTypeCodeID, (m_dSocialSecurityAmount * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "2", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount > 0)
                                    if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount >= 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iMedicareTransTypeCodeID, (m_dMedicareAmount * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "3", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount > 0)
                                    if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount >= 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iStateTransTypeCodeID, (m_dStateAmount * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "4", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }

                                    //KDB 04/25/2005 Save Offsets splits to FUNDS_AUTO_SPLIT table
                                    if (dPensionOffset > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPensionOffTransTypeCodeID, (dPensionOffset * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "5", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    if (dSSOffset > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lSSOffTransTypeCodeID, (dSSOffset * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "6", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    if (dOtherOffset > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOtherOffTransTypeCodeID, (dOtherOffset * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "7", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }

                                    //Animesh Inserted For GHS Enhancment //pmittal5 Mits 14841
                                    if (dOtherOffset_1 > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOthOff1TransTypeCodeID, (dOtherOffset_1 * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "9", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    if (dOtherOffset_2 > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOthOff2TransTypeCodeID, (dOtherOffset_2 * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "10", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    if (dOtherOffset_3 > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOthOff3TransTypeCodeID, (dOtherOffset_3 * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "11", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    if (dPstTaxDed_1 > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPstTax1TransTypeCodeID, (dPstTaxDed_1 * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "12", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    if (dPstTaxDed_2 > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPstTax2TransTypeCodeID, (dPstTaxDed_2 * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "13", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //Animesh Insertion Ends

                                    //Save Split Payments to Funds_auto and Funds_auto_split tables
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax > 0)
                                    if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax >= 0)
                                    {
                                        lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                        sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iFedTaxEID, m_dFederalTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, "T-1", ilblDaysIncluded, false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iFedTransTypeCodeID, m_dFederalTax, Conversion.ToDate(p_sBenCalcPayStart), dToDate, "1", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount > 0)
                                    if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount >= 0)
                                    {
                                        lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                        sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iSSTaxEID, m_dSocialSecurityAmount, -1, i, true, false, iNumberOfPayments, dPrintDateHS, "T-2", ilblDaysIncluded, false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iSSTransTypeCodeID, m_dSocialSecurityAmount, Conversion.ToDate(p_sBenCalcPayStart), dToDate, "2", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount > 0)
                                    if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount >= 0)
                                    {
                                        lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                        sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iMedicareTaxEID, m_dMedicareAmount, -1, i, true, false, iNumberOfPayments, dPrintDateHS, "T-3", ilblDaysIncluded, false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iMedicareTransTypeCodeID, m_dMedicareAmount, Conversion.ToDate(p_sBenCalcPayStart), dToDate, "3", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount > 0)
                                    if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount >= 0)
                                    {
                                        lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                        sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iStateTaxEID, m_dStateAmount, -1, i, true, false, iNumberOfPayments, dPrintDateHS, "T-4", ilblDaysIncluded, false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iStateTransTypeCodeID, m_dStateAmount, Conversion.ToDate(p_sBenCalcPayStart), dToDate, "4", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }

                                }
                                ///Appending data to output XML
                                objRow = p_objTable.CreateElement("row");
                                objDocumentElement.AppendChild(objRow);
                                objRow.SetAttribute("class", "rowlight");
                                objRow.SetAttribute("gross", string.Format("{0:0.00}", m_dGrossCalculatedPayment));
                                objRow.SetAttribute("paymentnumber", i.ToString());
                                objRow.SetAttribute("fromdate", Conversion.ToDate(p_sBenCalcPayStart).ToShortDateString());
                                objRow.SetAttribute("todate", dToDate.ToShortDateString());
                                objRow.SetAttribute("printdate", dPrintDateHS.ToShortDateString());
                                objRow.SetAttribute("supppayment", string.Format("{0:0.00}", m_dGrossCalculatedSupplement));
                                objRow.SetAttribute("net", string.Format("{0:0.00}", m_dNetPayment));
                                objRow.SetAttribute("fed", string.Format("{0:0.00}", m_dFederalTax*-1));
                                objRow.SetAttribute("ss", string.Format("{0:0.00}", m_dSocialSecurityAmount * -1));
                                objRow.SetAttribute("med", string.Format("{0:0.00}", m_dMedicareAmount * -1));
                                objRow.SetAttribute("state", string.Format("{0:0.00}", m_dStateAmount * -1));
                                objRow.SetAttribute("pension", string.Format("{0:0.00}", dPensionOffset * -1));
                                objRow.SetAttribute("ssoffset", string.Format("{0:0.00}", dSSOffset * -1));
                                objRow.SetAttribute("oi", string.Format("{0:0.00}", dOtherOffset * -1));
                                //Added by Ravneet Kaur  //pmittal5 Mits 14841
                                objRow.SetAttribute("othoffset1", string.Format("{0:0.00}", dOtherOffset_1  * -1));
                                objRow.SetAttribute("othoffset2", string.Format("{0:0.00}", dOtherOffset_2  * -1));
                                objRow.SetAttribute("othoffset3", string.Format("{0:0.00}", dOtherOffset_3  * -1));
                                objRow.SetAttribute("psttaxded1", string.Format("{0:0.00}", dPstTaxDed_1 * -1));
                                objRow.SetAttribute("psttaxded2", string.Format("{0:0.00}", dPstTaxDed_2 * -1));

                                //abisht MITS 10780 commented the code
                                //Wrong formatting to decimal.
                                //Formatting it to string now
                                //objRow.SetAttribute("dayspaid", string.Format("{0:0.00}", iNumOfPays));
                                objRow.SetAttribute("dayspaid", iNumOfPays.ToString());
                            }

                            

                        }

                            //*********END FIRST PAYMENT**************
                        else if (i < iNumberOfPayments)
                        {
                            //These Payments are full pay period payments
                            iCount++;
                            bSuppPaymentFlag = false; //kdb 04/28/2005

                            //kdb 04/28/2005  Check for supp payment and if supp is rolled up
                            //if ((objDisClass.SuppPercent > 0) && (bSuppRollUp == false))
                            if ((m_dSuppRate != 0) && (bSuppRollUp == false)) //Mits 15804
                                iPaymentLoop = 2;
                            else
                                iPaymentLoop = 1;


                            for (j = 1; j <= iPaymentLoop; j++) //Loop twice, first to process Core, second to process Supplement
                            {
                                //Calculate the payment amounts and dates
                                iNumOfPays = 0;
                                if (iPrefSched == 3)  //monthly kdb 04/05/2005
                                {
                                    dPaymentStart = dToDate.AddDays(1).AddMonths(i - 2);
                                    dPaymentEnd = dPaymentStart.AddMonths(1).AddDays(-1);
                                    iNumOfPays = Convert.ToInt32(dPaymentEnd.Subtract(dPaymentStart).TotalDays) + 1;
                                    if (objDisClass.Day30WorkMonth == false) //kdb 04/12/2005  Actual Month
                                    {
                                        m_dDailyAmount = m_dWeeklyBenefit / DaysInMonth(dPaymentStart);
                                        dPension = m_dPensionOffset / DaysInMonth(dPaymentStart);
                                        dSS = m_dSSOffset / DaysInMonth(dPaymentStart);
                                        dOther = m_dOiOffset / DaysInMonth(dPaymentStart);
                                        //Added by RKaur //pmittal5 Mits 14841
                                        dOther1 = m_dOthOffset1  / DaysInMonth(dPaymentStart);
                                        dOther2 = m_dOthOffset2  / DaysInMonth(dPaymentStart);
                                        dOther3 = m_dOthOffset3  / DaysInMonth(dPaymentStart);
                                        dPostTax1 = m_dPstTaxDed1 ;
                                        dPostTax2 = m_dPstTaxDed2 ;
                                        ///////////////////////
                                        if (m_dDailySuppAmount > 0)
                                            m_dDailySuppAmount = m_dSuppRate / DaysInMonth(dPaymentStart);
                                    }
                                }
                                else if (iPrefSched == 1 || iPrefSched == 2) //weekly/bi-weekly kdb 04/05/2005
                                {
                                    dPaymentStart = dToDate.AddDays(1).AddDays((i - 2) * iDays);
                                    dPaymentEnd = dPaymentStart.AddDays(iDays - 1);
                                    iNumOfPays = GetDaysIncluded(objClaim, dPaymentStart, dPaymentEnd, iDaysOfWeek, false, false, dThisPayPeriodStart, dThisPayPeriodEnd);
                                }

                                //kdb 04/28/2005 Get Offsets
                                if (bSuppPaymentFlag == false)
                                {
                                    dPensionOffset = GetOffsetAmt(dPension, m_dPensionOffset, iNumOfPays, iPrefSched);
                                    dSSOffset = GetOffsetAmt(dSS, m_dSSOffset, iNumOfPays, iPrefSched);
                                    dOtherOffset = GetOffsetAmt(dOther, m_dOiOffset, iNumOfPays, iPrefSched);
                                    //Added by Ravneet Kaur //pmittal5 Mits 14841
                                    dOtherOffset_1 = GetOffsetAmt(dOther1, m_dOthOffset1 , iNumOfPays , iPrefSched);
                                    dOtherOffset_2 = GetOffsetAmt(dOther2, m_dOthOffset2, iNumOfPays , iPrefSched);
                                    dOtherOffset_3 = GetOffsetAmt(dOther3, m_dOthOffset3, iNumOfPays, iPrefSched);
                                    dPstTaxDed_1 = m_dPstTaxDed1;
                                    dPstTaxDed_2 = m_dPstTaxDed2;
                                    //End 
                                    //Changed by RKaur //pmittal5 Mits 14841
                                    //dTotalOffset = dPensionOffset + dSSOffset + dOtherOffset;
                                    dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3), 2, MidpointRounding.AwayFromZero);
                                    //End

                                    dTempPensionOffset = 0;
                                    dTempSSOffset = 0;
                                    dTempOtherOffset = 0;
                                    //Added by Ravneet Kaur  //pmittal5 Mits 14841
                                    dTempOffset1 = 0;
                                    dTempOffset2 = 0;
                                    dTempOffset3 = 0;
                                    dTempPstTD1 = 0; 
                                    dTempPstTD2 = 0;
                                    //End

                                }

                                //Subtract offsets from payment
                                if (bSuppRollUp == true)
                                {
                                    dPayment = Math.Round((m_dDailySuppAmount * iNumOfPays), 2, MidpointRounding.AwayFromZero) + Math.Round((m_dDailyAmount * iNumOfPays), 2, MidpointRounding.AwayFromZero);

                                    //MITS 14186  
                                    //As discussed with Mike Hamann , if  dPayment - dTotalOffset = -0.01
                                    //deduct 1 cent from the offset having maximum value and make  net payment = 0

                                    if (Math.Round((dPayment - dTotalOffset), 2, MidpointRounding.AwayFromZero) < 0 && Math.Round((dPayment - dTotalOffset), 2, MidpointRounding.AwayFromZero) >= -0.01)
                                    {

                                        double[] dArrayofOffsets = new double[] { dPensionOffset, dSSOffset, dOtherOffset, dOtherOffset_1, dOtherOffset_2, dOtherOffset_3 };
                                        int iIndex = GetMaxOffsetIndex(dArrayofOffsets);

                                        //deducting 1 Cent from offset having maximum value
                                        dArrayofOffsets[iIndex] = Math.Round((dArrayofOffsets[iIndex] - dOneCent), 2, MidpointRounding.AwayFromZero);

                                        dPensionOffset = dArrayofOffsets[0];
                                        dSSOffset = dArrayofOffsets[1];
                                        dOtherOffset = dArrayofOffsets[2];
                                        dOtherOffset_1 = dArrayofOffsets[3];
                                        dOtherOffset_2 = dArrayofOffsets[4];
                                        dOtherOffset_3 = dArrayofOffsets[5];
                                        dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3), 2, MidpointRounding.AwayFromZero);
                                        
                                    }
                                    //pmittal5 Mits 14253 01/15/09 - Allow Zero Payments
                                    //if ((dPayment - dTotalOffset) > 0)
                                    if (Math.Round((dPayment - dTotalOffset), 2, MidpointRounding.AwayFromZero) >= 0)  //MITS 14186  
                                        dPayment = Math.Round((dPayment - dTotalOffset), 2, MidpointRounding.AwayFromZero);
                                    else
                                    {
                                        //DeletePayments("0," + iBatchId.ToString());
                                        //return 0;
                                        if (p_bSave)
                                            UpdateAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                        continue;
                                    }
                                    //End - pmittal5
                                }
                                else if (bSuppPaymentFlag == false)
                                {
                                    dPayment = Math.Round(m_dDailyAmount * iNumOfPays, 2, MidpointRounding.AwayFromZero);
                                    dPaymentOffsetDif = Math.Round((dPayment - dTotalOffset), 2, MidpointRounding.AwayFromZero);
                                    if (dPaymentOffsetDif <= 0)
                                    {
                                        dTempOffset = dPayment - 1;
                                        //Calculate individual offset amounts
                                        if (dPensionOffset < dTempOffset)
                                            dTempOffset = Math.Round((dTempOffset - dPensionOffset), 2, MidpointRounding.AwayFromZero);
                                        else if (dPensionOffset == dTempOffset)
                                            dTempOffset = 0;
                                        else
                                        {
                                            dTempPensionOffset = Math.Round((dPensionOffset - dTempOffset), 2, MidpointRounding.AwayFromZero);
                                            dPensionOffset = dTempOffset;
                                            dTempSSOffset = dSSOffset;
                                            dTempOtherOffset = dOtherOffset;
                                            //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                            dTempOffset1 = dOtherOffset_1;
                                            dTempOffset2 = dOtherOffset_2;
                                            dTempOffset3 = dOtherOffset_3;
                                            dTempPstTD1 = dPstTaxDed_1; 
                                            dTempPstTD2 = dPstTaxDed_2;
                                            //Animesh Insertion ends
                                            dSSOffset = 0;
                                            dOtherOffset = 0;
                                            //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                            dOtherOffset_1 = 0;
                                            dOtherOffset_2 = 0;
                                            dOtherOffset_3 = 0;
                                            dPstTaxDed_1 = 0; 
                                            dPstTaxDed_2 = 0;
                                            //Animesh Insertion ends
                                            goto EndOffsetCalc2;
                                        }

                                        if (dSSOffset < dTempOffset)
                                            dTempOffset = Math.Round((dTempOffset - dSSOffset), 2, MidpointRounding.AwayFromZero);
                                        else if (dSSOffset == dTempOffset)
                                            dTempOffset = 0;
                                        else
                                        {
                                            dTempSSOffset = Math.Round((dSSOffset - dTempOffset), 2, MidpointRounding.AwayFromZero);
                                            dSSOffset = dTempOffset;
                                            dTempOtherOffset = dOtherOffset;
                                            //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                            dTempOffset1 = dOtherOffset_1; 
                                            dTempOffset2 = dOtherOffset_2;
                                            dTempOffset3 = dOtherOffset_3;
                                            dTempPstTD1 = dPstTaxDed_1; 
                                            dTempPstTD2 = dPstTaxDed_2;
                                            //Animesh Insertion ends
                                            dOtherOffset = 0;
                                            //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                            dOtherOffset_1 = 0;
                                            dOtherOffset_2 = 0;
                                            dOtherOffset_3 = 0;
                                            dPstTaxDed_1 = 0; 
                                            dPstTaxDed_2 = 0;
                                            //Animesh Insertion ends
                                            goto EndOffsetCalc2;
                                        }
                                        if (dOtherOffset < dTempOffset)
                                            dTempOffset = Math.Round((dTempOffset - dOtherOffset), 2, MidpointRounding.AwayFromZero);
                                        else if (dOtherOffset == dTempOffset)
                                            dTempOffset = 0;
                                        else
                                        {
                                            dTempOtherOffset = Math.Round((dOtherOffset - dTempOffset), 2, MidpointRounding.AwayFromZero);
                                            dOtherOffset = dTempOffset;
                                            //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                            dTempOffset1 = dOtherOffset_1;
                                            dTempOffset2 = dOtherOffset_2;
                                            dTempOffset3 = dOtherOffset_3;
                                            dTempPstTD1 = dPstTaxDed_1; 
                                            dTempPstTD2 = dPstTaxDed_2;
                                            //Animesh Insertion ends
                                            //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                            dOtherOffset_1 = 0;
                                            dOtherOffset_2 = 0;
                                            dOtherOffset_3 = 0;
                                            dPstTaxDed_1 = 0;  
                                            dPstTaxDed_2 = 0;
                                            goto EndOffsetCalc2;
                                            //Animesh Insertion ends
                                        }
                                        //**********************************
                                        //Animesh Inserted for GHS Enhancement //pmittal5 Mits 14841
                                        if (dOtherOffset_1 < dTempOffset)
                                            dTempOffset = dTempOffset - dOtherOffset_1;
                                        else if (dOtherOffset_1 == dTempOffset)
                                            dTempOffset = 0;
                                        else
                                        {
                                            dTempOffset1 = dOtherOffset_1 - dTempOffset;
                                            dOtherOffset_1 = dTempOffset;
                                            //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                            dTempOffset2 = dOtherOffset_2;
                                            dTempOffset3 = dOtherOffset_3;
                                            dTempPstTD1 = dPstTaxDed_1; 
                                            dTempPstTD2 = dPstTaxDed_2;
                                            //Animesh Insertion ends
                                            dOtherOffset_2 = 0;
                                            dOtherOffset_3 = 0;
                                            dPstTaxDed_1 = 0;
                                            dPstTaxDed_2 = 0;
                                            goto EndOffsetCalc2;
                                            //Animesh Insertion ends
                                        }
                                        if (dOtherOffset_2 < dTempOffset)
                                            dTempOffset = dTempOffset - dOtherOffset_2;
                                        else if (dOtherOffset_2 == dTempOffset)
                                            dTempOffset = 0;
                                        else
                                        {
                                            dTempOffset2 = dOtherOffset_2 - dTempOffset;
                                            dOtherOffset_2 = dTempOffset;
                                            //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                            dTempOffset3 = dOtherOffset_3;
                                            dTempPstTD1 = dPstTaxDed_1;
                                            dTempPstTD2 = dPstTaxDed_2;
                                            //Animesh Insertion ends
                                            dOtherOffset_3 = 0;
                                            dPstTaxDed_1 = 0; 
                                            dPstTaxDed_2 = 0;
                                            goto EndOffsetCalc2;
                                            //Animesh Insertion ends
                                        }
                                        if (dOtherOffset_3 < dTempOffset)
                                            dTempOffset = dTempOffset - dOtherOffset_3;
                                        else if (dOtherOffset_3 == dTempOffset)
                                            dTempOffset = 0;
                                        else
                                        {
                                            dTempOffset3 = dOtherOffset_3 - dTempOffset;
                                            dOtherOffset_3 = dTempOffset;
                                            //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                            dTempPstTD1 = dPstTaxDed_1; 
                                            dTempPstTD2 = dPstTaxDed_2;
                                            //Animesh Insertion ends
                                            dPstTaxDed_1 = 0; 
                                            dPstTaxDed_2 = 0;
                                            goto EndOffsetCalc2;
                                            //Animesh Insertion ends
                                        }
                                    if (dPstTaxDed_1 < dTempOffset)   //pmittal5 Mits 14841 - Post Tax deductions should not change
                                            dTempOffset = dTempOffset - dPstTaxDed_1;
                                        else if (dPstTaxDed_1 == dTempOffset)
                                            dTempOffset = 0;
                                        else
                                        {
                                            dTempPstTD1 = dPstTaxDed_1 - dTempOffset;
                                            dPstTaxDed_1 = dTempOffset;
                                            //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                            dTempPstTD2 = dPstTaxDed_2; 
                                            //Animesh Insertion ends
                                            dPstTaxDed_2 = 0;
                                            goto EndOffsetCalc2;
                                            //Animesh Insertion ends
                                        }
                                        if (dPstTaxDed_2 < dTempOffset)
                                            dTempOffset = dTempOffset - dPstTaxDed_2;
                                        else if (dPstTaxDed_2 == dTempOffset)
                                            dTempOffset = 0;
                                        else
                                        {
                                            dTempPstTD2 = dPstTaxDed_2 - dTempOffset;
                                            dPstTaxDed_2 = dTempOffset;
                                        }
                                    //**********************************
                                    EndOffsetCalc2:
                                        dPayment = 1;
                                    dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset), 2, MidpointRounding.AwayFromZero);
                                        //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                    dTotalOffset = Math.Round(dTotalOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3, 2, MidpointRounding.AwayFromZero);
                                        //Animesh Insertion ends
                                    }
                                    else
                                        dPayment = dPaymentOffsetDif;
                                }
                                else if (bSuppPaymentFlag == true)
                                {
                                    //Animesh Inserted  //pmittal5 Mits 14841
                                    blnPaymentDeleted = false; 
                                    //Animesh Insertion Ends
                                    dPayment = Math.Round(m_dDailySuppAmount * iNumOfPays, 2, MidpointRounding.AwayFromZero);
                                    dPensionOffset = dTempPensionOffset;
                                    dSSOffset = dTempSSOffset;
                                    dOtherOffset = dTempOtherOffset;

                                    //Animesh Inserted For GHS Enhancment //pmittal5 Mits 14841
                                    dOtherOffset_1 = dTempOffset1;
                                    dOtherOffset_2 = dTempOffset2;
                                    dOtherOffset_3 = dTempOffset3;
                                    dPstTaxDed_1 = dTempPstTD1; 
                                    dPstTaxDed_2 = dTempPstTD2;
                                    //Animesh Insertion Ends

                                    dTotalOffset = Math.Round(dPensionOffset + dSSOffset + dOtherOffset, 2, MidpointRounding.AwayFromZero);
                                    //Animesh Modified for GHS Enhancement  //pmittal5 Mits 14841
                                    dTotalOffset = Math.Round(dTotalOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3, 2, MidpointRounding.AwayFromZero);
                                    //Animesh Insertion ends
                                    dPayment = Math.Round((dPayment - dTotalOffset), 2, MidpointRounding.AwayFromZero);
                                    //pmittal5 Mits 14253 01/15/09 - Allow Zero Payments
                                    //if (dPayment <= 0)
                                    //{
                                    //    DeletePayments("0," + iBatchId.ToString());
                                    //    return 0;
                                    //}




                                    //MITS 14186  
                                    //As discussed with Mike Hamann , if  dPayment - dTotalOffset = 0.01
                                    //deduct 1 cent from the offset having maximum value and make  net payment = 0

                                    if (dPayment < 0 && dPayment >= -0.01)
                                    {

                                        double[] dArrayofOffsets = new double[] { dPensionOffset, dSSOffset, dOtherOffset, dOtherOffset_1, dOtherOffset_2, dOtherOffset_3 };
                                        int iIndex = GetMaxOffsetIndex(dArrayofOffsets);

                                        //deducting 1 Cent from offset having maximum value
                                        dArrayofOffsets[iIndex] = Math.Round((dArrayofOffsets[iIndex] - dOneCent), 2, MidpointRounding.AwayFromZero);

                                        dPensionOffset = dArrayofOffsets[0];
                                        dSSOffset = dArrayofOffsets[1];
                                        dOtherOffset = dArrayofOffsets[2];
                                        dOtherOffset_1 = dArrayofOffsets[3];
                                        dOtherOffset_2 = dArrayofOffsets[4];
                                        dOtherOffset_3 = dArrayofOffsets[5];
                                        dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3), 2, MidpointRounding.AwayFromZero);
                                        dPayment = Math.Round((dPayment - dTotalOffset),0, MidpointRounding.AwayFromZero);


                                    }
                                    if (dPayment < 0)
                                    {
                                        if (p_bSave)
                                        {
                                            UpdateAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                            blnPaymentDeleted = true;  
                                        }
                                        continue;
                                    }
                                    //End - pmittal5
                                }


                                dSupp = Math.Round(m_dDailySuppAmount * iNumOfPays, 2, MidpointRounding.AwayFromZero);

                                lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
//Mits 10780change done by pawan commented lines below updated code with new lines of code as below for getting the print date correctly
//				if (objPlan.PrintBeforeEnd)
//                                {
//                                    if (dPrintDate < dPaymentStart)
//                                    {
//                                        if (objPlan.PrefPaySchCode == 1737)
//                                            dPrintDate = dPrintDate.AddDays(14);
//                                        else if (iPrefSched == 1)
//                                            dPrintDate = dPrintDate.AddDays(7);
//                                        else if (iPrefSched == 3)  //kdb 04/04/2005
//                                        {
//                                            m_objCache.GetCodeInfo(objPlan.PrefDayCode, ref sShortCode, ref sCodeDesc);
//                                            dPrintDate = GetOrdinalDate(dPrintDate.Year, dPrintDate.Month + 1, Conversion.ConvertStrToInteger(m_objCache.GetShortCode(objPlan.PrefDayOfMCode)), GetDayNumber(sCodeDesc));
//                                        }
//                                    }
//                                }
//                                else if (dPrintDate < dPaymentEnd)
//                                {
//                                    if (objPlan.PrefPaySchCode == 1737)
//                                        dPrintDate = dPrintDate.AddDays(14);
//                                    else if (iPrefSched == 1)
//                                        dPrintDate = dPrintDate.AddDays(7);
//                                    else if (iPrefSched == 3)  //kdb 04/04/2005
//                                    {
//                                        m_objCache.GetCodeInfo(objPlan.PrefDayCode, ref sShortCode, ref sCodeDesc);
//                                        dPrintDate = GetOrdinalDate(dPrintDate.Year, dPrintDate.Month + 1, Conversion.ConvertStrToInteger(m_objCache.GetShortCode(objPlan.PrefDayOfMCode)), GetDayNumber(sCodeDesc));
//                                    }
//                                }

                                //MITS 13135 : Umesh 09/04/08
                                iOrdinal = 0;
                                dTempPaymentStart = dPaymentStart;
                                if (iPrefSched == 3)
                                {
                                    
                                    m_objCache.GetCodeInfo(objPlan.PrefDayOfMCode, ref sShortCode, ref sCodeDesc);
                                    iOrdinal = Conversion.ConvertStrToInteger(sShortCode);
                                    m_objCache.GetCodeInfo(objPlan.PrefDayCode, ref sShortCode, ref sCodeDesc);

                                }
                                //MITS 13135 : End
                                if (objPlan.PrintBeforeEnd)
                                {
                                    while (dPrintDate < dPaymentStart)
                                    {
                                        if (iPrefSched == 2)
                                        {
                                            dPrintDate = dPrintDate.AddDays(14);
                                        }
                                        else if (iPrefSched == 3)
                                        {
                                            
                                            //dPrintDate = dPrintDate.AddDays(28);
                                            //MITS 13135 : Umesh 09/04/08
                                            //dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            //dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                            if (iTypeOfMonthlyPay != 1) //in case of day based
                                            {
                                                dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            }
                                            else
                                            {
                                                if (iPrefDate <= dTempPaymentStart.AddMonths(1).AddDays(-1).Day)
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, iPrefDate);
                                                else
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, dTempPaymentStart.AddMonths(1).AddDays(-1).Day);
                                            }
                                            dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                        }
                                        else
                                        {
                                            dPrintDate = dPrintDate.AddDays(7);
                                        }
                                        
                                    }
                                    while (dPrintDate < DateTime.Now.Date)
                                    {
                                        if (iPrefSched == 2)
                                        {
                                            dPrintDate = dPrintDate.AddDays(14);
                                        }
                                        else if (iPrefSched == 3)
                                        {
                                           // dPrintDate = dPrintDate.AddDays(28);
                                            //MITS 13135 : Umesh 09/04/08
                                            //dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            //dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                            if (iTypeOfMonthlyPay != 1) //in case of day based
                                            {
                                                dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            }
                                            else
                                            {
                                                if (iPrefDate <= dTempPaymentStart.AddMonths(1).AddDays(-1).Day)
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, iPrefDate);
                                                else
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, dTempPaymentStart.AddMonths(1).AddDays(-1).Day);
                                            }
                                            dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                        }
                                        else
                                        {
                                            dPrintDate = dPrintDate.AddDays(7);
                                        }

                                    }

                                }
                                else
                                {
                                    while (dPrintDate < dPaymentEnd)
                                    {
                                        if (iPrefSched == 2)
                                        {
                                            dPrintDate = dPrintDate.AddDays(14);
                                        }
                                        else if (iPrefSched == 3)
                                        {
                                            //dPrintDate = dPrintDate.AddDays(28);
                                            //MITS 13135 : Umesh 09/04/08
                                            //dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            //dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                            if (iTypeOfMonthlyPay != 1) //in case of day based
                                            {
                                                dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            }
                                            else
                                            {
                                                if (iPrefDate <= dTempPaymentStart.AddMonths(1).AddDays(-1).Day)
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, iPrefDate);
                                                else
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, dTempPaymentStart.AddMonths(1).AddDays(-1).Day);
                                            }
                                            dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                        }
                                        else
                                        {
                                            dPrintDate = dPrintDate.AddDays(7);
                                        }
                                        
                                    }
                                    while (dPrintDate < DateTime.Now.Date)
                                    {
                                        if (iPrefSched == 2)
                                        {
                                            dPrintDate = dPrintDate.AddDays(14);
                                        }
                                        else if (iPrefSched == 3)
                                        {
                                            //dPrintDate = dPrintDate.AddDays(28);
                                            //MITS 13135 : Umesh 09/04/08
                                            //dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            //dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                            if (iTypeOfMonthlyPay != 1) //in case of day based
                                            {
                                                dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            }
                                            else
                                            {
                                                if (iPrefDate <= dTempPaymentStart.AddMonths(1).AddDays(-1).Day)
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, iPrefDate);
                                                else
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, dTempPaymentStart.AddMonths(1).AddDays(-1).Day);
                                            }
                                            dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                        }
                                        else
                                        {
                                            dPrintDate = dPrintDate.AddDays(7);
                                        }

                                    }
                                    //pmittal5 Mits 14889 - Print Date should not fall in same month as that of Payment Date
                                    if (iPrefSched == 3 && iTypeOfMonthlyPay == 1)
                                    {
                                        if (dPrintDate.Month == dPaymentEnd.Month)
                                        {
                                            dPrintDate = dPrintDate.AddMonths(1);
                                            if (iPrefDate <= dTempPaymentStart.AddMonths(2).AddDays(-1).Day)
                                                dPrintDate = new DateTime(dPrintDate.Year, dPrintDate.Month, iPrefDate);
                                            else
                                                dPrintDate = new DateTime(dPrintDate.Year, dPrintDate.Month, dTempPaymentStart.AddMonths(2).AddDays(-1).Day);
                                        }
                                    }
                                    //End - pmittal5
                                }
                                //Neha R8 enhancement
                                dPrintDateHS = AdjustPrintDate(objClaim.AdjustPrintDatesFlag, dPrintDate);
                                //enha end

                                //Figure the tax which needs to be subtracted from the amount when saved to Funds_auto
                                //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                //if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax > 0)
                                if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax >= 0)
                                {
                                    if (p_dtpTaxes.dblTaxablePercent > 0)
                                        dFedTax = Math.Round(dPayment * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.FED_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                    else
                                        dFedTax = Math.Round(dPayment * p_dtpTaxes.FED_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                }
                                else
                                    dFedTax = 0;
                                //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                //if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount > 0)
                                if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount >= 0)
                                {
                                    if (p_dtpTaxes.dblTaxablePercent > 0)
                                        dSSTax = Math.Round(dPayment * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.SS_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                    else
                                        dSSTax = Math.Round(dPayment * p_dtpTaxes.SS_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                }
                                else
                                    dSSTax = 0;
                                //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                //if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount > 0)
                                if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount >= 0)
                                {
                                    if (p_dtpTaxes.dblTaxablePercent > 0)
                                        dMedTax = Math.Round(dPayment * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.MEDICARE_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                    else
                                        dMedTax = Math.Round(dPayment * p_dtpTaxes.MEDICARE_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                }
                                else
                                    dMedTax = 0;
                                //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                //if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount > 0)
                                if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount >= 0)
                                {
                                    if (p_dtpTaxes.dblTaxablePercent > 0)
                                        dStateTax =Math.Round( dPayment * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.STATE_TAX_RATE,2, MidpointRounding.AwayFromZero);
                                    else
                                        dStateTax =Math.Round( dPayment * p_dtpTaxes.STATE_TAX_RATE ,2, MidpointRounding.AwayFromZero);
                                }
                                else
                                    dStateTax = 0;

                                dTotalTax = Math.Round((dFedTax + dSSTax + dMedTax + dStateTax), 2, MidpointRounding.AwayFromZero); //MITS 14186 

                                //kdb 04/28/2005 Set up sAppend for the control number
                                if (bSuppPaymentFlag)
                                    sAppend = "S";
                                else
                                    sAppend = "";

                                //modified the logic so -- Animesh 
                                //Animesh Inserted To check the Negative payment won't get saved after Post tax deduction 
                                //if (dPayment - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2) < 0)
                                //{
                                //    DeletePayments("0," + iBatchId.ToString());
                                //    return 0;
                                //}
                                if (Math.Round(dPayment - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2),2, MidpointRounding.AwayFromZero) < 0)  //pmittal5 Mits 14841
                                {
                                    if (p_bSave && blnPaymentDeleted==false)
                                        UpdateAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                    if (bSuppPaymentFlag == false) //pmittal5 Mits 14841
                                        bSuppPaymentFlag = true;
                                    continue;
                                }
                                //Animesh Insertion Ends


                                //Save the payment to Funds_auto table
                                if (p_bSave)
                                {
                                    //Animesh Modified For GHS Enhancment  //pmittal5 Mits 14841
                                    //sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, objClaim.PrimaryClaimant.ClaimantEid, (dPayment - dTotalTax), 0, i, false, false, iNumberOfPayments, dPrintDate, sAppend, iNumOfPays, bSuppPaymentFlag);
                                    sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, objClaim.PrimaryClaimant.ClaimantEid, Math.Round((dPayment - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2)),2, MidpointRounding.AwayFromZero), 0, i, false, false, iNumberOfPayments, dPrintDateHS, sAppend, iNumOfPays, bSuppPaymentFlag);
                                    //Animesh Modification Ends
                                    //execute sql
                                    objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                    lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);


                                    if (bSuppPaymentFlag == true)
                                        lTransType = m_lSuppTransType;
                                    else
                                        lTransType = p_iTransTypeCode;


                                    if (bSuppRollUp == true)
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lTransType,Math.Round(( dPayment - dSupp + dTotalOffset),2, MidpointRounding.AwayFromZero), dPaymentStart, dPaymentEnd, "0", false);
                                    else
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lTransType,Math.Round(( dPayment + dTotalOffset),2, MidpointRounding.AwayFromZero), dPaymentStart, dPaymentEnd, "0", bSuppPaymentFlag);

                                    objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                    //kdb 04/28/2005 Save the supplement as a split if it exists
                                    if (bSuppRollUp == true && dSupp > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        lTransType = m_lSuppTransType;
                                        //pmittal5 Mits 14253 01/14/09
                                        //sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lTransType, dSupp, Conversion.ToDate(p_sBenCalcPayStart), dToDate, "8", true);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lTransType, dSupp, dPaymentStart, dPaymentEnd, "8", true);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }

                                    //Save Third Party Splits to Funds_auto_split table
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax > 0)
                                    if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax >= 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iFedTransTypeCodeID, (dFedTax * -1), dPaymentStart, dPaymentEnd, "1", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount > 0)
                                    if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount >= 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iSSTransTypeCodeID, (dSSTax * -1), dPaymentStart, dPaymentEnd, "2", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount > 0)
                                    if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount >= 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iMedicareTransTypeCodeID, (dMedTax * -1), dPaymentStart, dPaymentEnd, "3", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount > 0)
                                    if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount >= 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iStateTransTypeCodeID, (dStateTax * -1), dPaymentStart, dPaymentEnd, "4", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }


                                    //KDB 04/25/2005 Save Offsets splits to FUNDS_AUTO_SPLIT table
                                    if (dPensionOffset > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        //pmittal5 Mits 14253 01/14/09
                                        //sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPensionOffTransTypeCodeID, (dPensionOffset * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "5", bSuppPaymentFlag);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPensionOffTransTypeCodeID, (dPensionOffset * -1), dPaymentStart, dPaymentEnd, "5", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    if (dSSOffset > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        //pmittal5 Mits 14253 01/14/09
                                        //sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lSSOffTransTypeCodeID, (dSSOffset * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "6", bSuppPaymentFlag);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lSSOffTransTypeCodeID, (dSSOffset * -1), dPaymentStart, dPaymentEnd, "6", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    if (dOtherOffset > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        //pmittal5 Mits 14253 01/14/09
                                        //sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOtherOffTransTypeCodeID, (dOtherOffset * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "7", bSuppPaymentFlag);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOtherOffTransTypeCodeID, (dOtherOffset * -1), dPaymentStart, dPaymentEnd, "7", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }

                                    //*********************************************
                                    if (dOtherOffset_1 > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOthOff1TransTypeCodeID, (dOtherOffset_1 * -1), dPaymentStart, dPaymentEnd, "9", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    if (dOtherOffset_2 > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOthOff2TransTypeCodeID, (dOtherOffset_2 * -1), dPaymentStart, dPaymentEnd, "10", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    if (dOtherOffset_3 > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOthOff3TransTypeCodeID, (dOtherOffset_3 * -1), dPaymentStart, dPaymentEnd, "11", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    if (dPstTaxDed_1 > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPstTax1TransTypeCodeID, (dPstTaxDed_1 * -1), dPaymentStart, dPaymentEnd, "12", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    if (dPstTaxDed_2 > 0)
                                    {
                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPstTax2TransTypeCodeID, (dPstTaxDed_2 * -1), dPaymentStart, dPaymentEnd, "13", bSuppPaymentFlag);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }

                                    ///////////////////////

                                    //*********************************************

                                    //Save Split Payments to Funds_auto and Funds_auto_split tables
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax > 0)
                                    if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax >= 0)
                                    {
                                        lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                        sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iFedTaxEID, dFedTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, sAppend + "T-1", iNumOfPays, false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iFedTransTypeCodeID, dFedTax, dPaymentStart, dPaymentEnd, "1", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount > 0)
                                    if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount >= 0)
                                    {
                                        lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                        sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iSSTaxEID, dSSTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, sAppend + "T-2", iNumOfPays, false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iSSTransTypeCodeID, dSSTax, dPaymentStart, dPaymentEnd, "2", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount > 0)
                                    if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount >= 0)
                                    {
                                        lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                        sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iMedicareTaxEID, dMedTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, sAppend + "T-3", iNumOfPays, false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iMedicareTransTypeCodeID, dMedTax, dPaymentStart, dPaymentEnd, "3", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                    //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                    //if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount > 0)
                                    if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount >= 0)
                                    {
                                        lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                        sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iStateTaxEID, dStateTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, sAppend + "T-4", iNumOfPays, false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                        lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                        sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iStateTransTypeCodeID, dStateTax, dPaymentStart, dPaymentEnd, "4", false);
                                        objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                    }
                                } // closing p_bSave

                                if (bSuppPaymentFlag == false)
                                    bSuppPaymentFlag = true; //kdb 04/15/2005  Flip the flag for marking payments as Supp


                                double dGrossPayment = 0;
                                if (bSuppRollUp == true)
                                    dGrossPayment =Math.Round(( dPayment - dSupp + dTotalOffset),2, MidpointRounding.AwayFromZero);
                                else
                                    dGrossPayment = Math.Round((dPayment + dTotalOffset),2, MidpointRounding.AwayFromZero);

                                //Added by RKaur on 04/09/2009 //pmittal5 Mits 14841
                                dtempNetPayment = Math.Round(dPayment - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2), 2, MidpointRounding.AwayFromZero);
                                
                                objRow = p_objTable.CreateElement("row");
                                objDocumentElement.AppendChild(objRow);
                                objRow.SetAttribute("class", "rowlight");
                                objRow.SetAttribute("gross", string.Format("{0:0.00}", dGrossPayment));
                                objRow.SetAttribute("paymentnumber", i.ToString());
                                if (bSuppRollUp)  //pmittal5 Mits 15804 04/24/09 - Dont Show SuppPayment when Separate Supp Payment flag is ON
                                    objRow.SetAttribute("supppayment", string.Format("{0:0.00}", dSupp));
                                else
                                    objRow.SetAttribute("supppayment", "");
                                objRow.SetAttribute("fromdate", dPaymentStart.ToShortDateString());
                                objRow.SetAttribute("todate", dPaymentEnd.ToShortDateString());
                                objRow.SetAttribute("printdate", dPrintDateHS.ToShortDateString());

                                //Added by RKaur on 04/09/2009 //pmittal5 Mits 14841
                                objRow.SetAttribute("net", string.Format("{0:0.00}", dtempNetPayment));

                                //Animesh Inserted For GHS Enhancment 
                                //objRow.SetAttribute("net", string.Format("{0:0.00}", dPayment - dTotalTax));
                                //dtempNetPayment = dPayment - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2);
                                //if (dtempNetPayment > 0)
                                //{
                                //    objRow.SetAttribute("net", string.Format("{0:0.00}", dtempNetPayment ));
                                //}
                                //else 
                                //{
                                //    objRow.SetAttribute("net", string.Format("{0:0.00}", 0));
                                //}
                                
                                //Animesh Insertion Ends
                                objRow.SetAttribute("fed", string.Format("{0:0.00}", dFedTax * -1));
                                objRow.SetAttribute("ss", string.Format("{0:0.00}", dSSTax * -1));
                                objRow.SetAttribute("med", string.Format("{0:0.00}", dMedTax * -1));
                                objRow.SetAttribute("state", string.Format("{0:0.00}", dStateTax * -1));
                                objRow.SetAttribute("pension", string.Format("{0:0.00}", dPensionOffset * -1));
                                objRow.SetAttribute("ssoffset", string.Format("{0:0.00}", dSSOffset * -1));
                                objRow.SetAttribute("oi", string.Format("{0:0.00}", dOtherOffset * -1));
                                //Added by Ravneet Kaur //pmittal5 Mits 14841
                                objRow.SetAttribute("othoffset1", string.Format("{0:0.00}", dOtherOffset_1 * -1));
                                objRow.SetAttribute("othoffset2", string.Format("{0:0.00}", dOtherOffset_2 * -1));
                                objRow.SetAttribute("othoffset3", string.Format("{0:0.00}", dOtherOffset_3 * -1));
                                objRow.SetAttribute("psttaxded1", string.Format("{0:0.00}", dPstTaxDed_1 * -1));
                                objRow.SetAttribute("psttaxded2", string.Format("{0:0.00}", dPstTaxDed_2 * -1));
                                //******************************
                                objRow.SetAttribute("dayspaid", iNumOfPays.ToString());

                            } //closing for 

                        }//closing "all loop payments"
                        else //Last payment can be full or partial pay period
                        {
                            bSuppPaymentFlag = false;
            
                            //kdb 04/15/2005
                            //if((objDisClass.SuppPercent > 0) && (!bSuppRollUp)) 
                            if ((m_dSuppRate != 0) && (!bSuppRollUp))  //Mits 15804
                                iPaymentLoop = 2;
                            else
                                iPaymentLoop = 1;
                            

                            //Calculate the payment amount
                            if(iPrefSched == 1 || iPrefSched == 2) //weekly/bi-weekly
                            {
                                if(iRemainder == 0 && objPlan.PrefPaySchCode == 1737) //This signals that the last payment is a full two week benefit period
                                {
                                    iNumOfPays = 14;
                                    dPaymentStart = dToDate.AddDays(1).AddDays((i - 2) * iDays);
                                    dPaymentEnd = dPaymentStart.AddDays(iDays - 1);
                                }
                                else if(iRemainder == 0 && iPrefSched == 1) //This signals that the last payment is a full week benefit period
                                {
                                    iNumOfPays = 7;
                                    dPaymentStart = dToDate.AddDays(1).AddDays((i - 2) * iDays);
                                    dPaymentEnd = dPaymentStart.AddDays(iDays - 1);
                                }
                                else
                                {
                                    iNumOfPays = iRemainder;
                                    dPaymentStart = dToDate.AddDays(1).AddDays((i - 2) * iDays);
                                    dPaymentEnd = Conversion.ToDate(p_sBenCalcPayTo);
                                }
                                
                            }
                            else if(iPrefSched==3)
                            {
                                if(iRemainder == 0)
                                {
                                    if(Conversion.ToDate(p_sBenCalcPayTo) > dPaymentEnd)
                                        iNumOfPays = Convert.ToInt32((Conversion.ToDate(p_sBenCalcPayTo).Subtract(dPaymentEnd.AddDays(1))).TotalDays);
                                    else
                                        iNumOfPays = Convert.ToInt32((dPaymentEnd.AddDays(1).Subtract(Conversion.ToDate(p_sBenCalcPayTo))).TotalDays);
                                }
                                else
                                    iNumOfPays = iRemainder;
                                
                                dPaymentStart = dToDate.AddDays(1).AddMonths((i - 2));
                                dPaymentEnd = Conversion.ToDate(p_sBenCalcPayTo);
                                //pmittal5 05/05/09 - Calculating Total days in month to determine if it is a Full or Partial Pay period
                                m_iDaysWorkingInMonth = DaysInMonth(dPaymentStart);

                                if (objDisClass.Day30WorkMonth == false) //kdb 04/12/2005  Actual Month
                                {
                                    dPension = m_dPensionOffset / DaysInMonth(dPaymentStart);
                                    dSS = m_dSSOffset / DaysInMonth(dPaymentStart);
                                    dOther = m_dOiOffset / DaysInMonth(dPaymentStart);
                                    //Added by RKaur //pmittal5 Mits 14841
                                    dOther1 = m_dOthOffset1 / DaysInMonth(dPaymentStart);
                                    dOther2 = m_dOthOffset2 / DaysInMonth(dPaymentStart);
                                    dOther3 = m_dOthOffset3 / DaysInMonth(dPaymentStart);
                                    dPostTax1 = m_dPstTaxDed1 ;
                                    dPostTax2 = m_dPstTaxDed2;
                                    //////////////////////////////

                                }                                
                            }
                            if(iPrefSched == 1 || iPrefSched == 2) //weekly/bi-weekly
                                iNumOfDays = GetWorkDays(iNumOfPays , dToDate.AddDays(1) , iDaysOfWeek);
                            else if(iPrefSched == 3)
                                iNumOfDays = iNumOfPays;

                            //pmittal5 Mits 14186 02/23/09 - m_dDailyAmount calculation should be done before dCalcFactor. So copied the below lines
                            //kdb 04/12/2005 Actual Month
                            if (iPrefSched == 3 && objDisClass.Day30WorkMonth == false)
                            {
                                m_dDailyAmount = m_dWeeklyBenefit / DaysInMonth(dPaymentStart);
                                if (m_dDailySuppAmount > 0)
                                    m_dDailySuppAmount = m_dSuppRate / DaysInMonth(dPaymentStart);

                            }//End - pmittal5 Mits 14186

                            if (m_dWeeklyBenefit != 0 && m_dDailyAmount != 0)
                            {
                                dCalcFactor = Math.Round((m_dDailyAmount * iNumOfDays), 2, MidpointRounding.AwayFromZero) / m_dWeeklyBenefit;
                            }
                            else
                            {
                                if (m_dSuppRate != 0)
                                    dCalcFactor = Math.Round(m_dDailySuppAmount * iNumOfDays, 2, MidpointRounding.AwayFromZero) / m_dSuppRate;
                            }
                            //Calculate Offsets kdb 04/25/2005
                            //Pension Offset
                            if (dPension > 0)
                            {
                                if (PartialPay(iNumOfDays, iPrefSched, objDisClass))
                                {
                                    if (m_iOffsetCalc == 1 || m_iOffsetCalc == 2)
                                    {
                                        if (objDisClass.FullpayOnlyFlag)
                                            dPensionOffset = 0;
                                        else if (objDisClass.ProrateOffFlag)  //pmittal5 11/13/08 Mits 12620
                                            // dPensionOffset = dPension * iNumOfDays;
                                            dPensionOffset =Math.Round( m_dPensionOffset * dCalcFactor,2, MidpointRounding.AwayFromZero);
                                        //pmittal5 11/13/08 Mits 12620
                                        else
                                        {
                                            if (iPrefSched == 2)
                                            {
                                                dPensionOffset = m_dPensionOffset * 2;
                                            }
                                            else
                                            {
                                                dPensionOffset = m_dPensionOffset;
                                            }
                                        }
                                        //End- pmittal5
                                    }
                                    else
                                        dPensionOffset = 0;
                                }
                                else //last pay is whole
                                //pmittal5 11/13/08 Mits 12620
                                //if (objDisClass.ProrateOffFlag)
                                //    dPensionOffset = dPension * iNumOfDays;
                                {
                                    if (iPrefSched == 2)
                                    {
                                        dPensionOffset = m_dPensionOffset * 2;
                                    }
                                    else if (iPrefSched == 3 && objDisClass.ProrateOffFlag)
                                    {
                                        //dPensionOffset = dPension * iNumOfDays;
                                        dPensionOffset = Math.Round(m_dPensionOffset * dCalcFactor, 2, MidpointRounding.AwayFromZero); //MITS 14186
                                    }
                                    else
                                    {
                                        dPensionOffset = m_dPensionOffset;
                                    }
                                }
                                //End- pmittal5
                            } 
                            //SS Offset
                            if(dSS > 0)
                            {
                                if(PartialPay(iNumOfDays , iPrefSched , objDisClass))
                                {
                                    if(m_iOffsetCalc == 1 || m_iOffsetCalc == 2)
                                    {
                                        if(objDisClass.FullpayOnlyFlag)
                                            dSSOffset = 0;
                                        else if (objDisClass.ProrateOffFlag)  //pmittal5 11/13/08 Mits 12620
                                            //dSSOffset = dSS * iNumOfDays;
                                            dSSOffset = Math.Round(m_dSSOffset * dCalcFactor, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                                        //pmittal5 11/13/08 Mits 12620
                                        else
                                        {
                                            if (iPrefSched == 2)
                                            {
                                                dSSOffset = m_dSSOffset * 2;
                                            }
                                            else
                                            {
                                                dSSOffset = m_dSSOffset;
                                            }
                                        }
                                        //End- pmittal5
                                    }
                                    else
                                        dSSOffset = 0;
                                }
                                else //last pay is whole
                                //pmittal5 11/13/08 Mits 12620
                                    //if(objDisClass.ProrateOffFlag)
                                    //    dSSOffset = dSS * iNumOfDays;
                                {
                                    if (iPrefSched == 2)
                                    {
                                        dSSOffset = m_dSSOffset * 2;
                                    }
                                    else if (iPrefSched == 3 && objDisClass.ProrateOffFlag)
                                    {
                                        //dSSOffset = dSS * iNumOfDays;
                                        dSSOffset = Math.Round(m_dSSOffset * dCalcFactor, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                                    }
                                    else
                                    {
                                        dSSOffset = m_dSSOffset;
                                    }
                                }
                                //End- pmittal5
                            } 
                            //Other Income Offset
                            if(dOther > 0)
                            {
                                if(PartialPay(iNumOfDays , iPrefSched , objDisClass))
                                {
                                    if(m_iOffsetCalc == 1 || m_iOffsetCalc == 2)
                                    {
                                        if(objDisClass.FullpayOnlyFlag)
                                            dOtherOffset = 0;
                                        else if (objDisClass.ProrateOffFlag)  //pmittal5 11/13/08 Mits 12620
                                            //dOtherOffset = dOther * iNumOfDays;
                                            dOtherOffset = Math.Round(m_dOiOffset * dCalcFactor, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                                        //pmittal5 11/13/08 Mits 12620
                                        else
                                        {
                                            if (iPrefSched == 2)
                                            {
                                                dOtherOffset = m_dOiOffset * 2;
                                            }
                                            else
                                            {
                                                dOtherOffset = m_dOiOffset;
                                            }
                                        }
                                        //End- pmittal5
                                    }
                                    else
                                        dOtherOffset = 0;
                                }
                                else //last pay is whole
                                    //pmittal5 11/13/08 Mits 12620
                                    //if(objDisClass.ProrateOffFlag)
                                    //    dOtherOffset = dOther * iNumOfDays;
                                    {
                                        if (iPrefSched == 2)
                                        {
                                            dOtherOffset = m_dOiOffset * 2;
                                        }
                                        else if (iPrefSched == 3 && objDisClass.ProrateOffFlag)
                                        {
                                           // dOtherOffset = dOther * iNumOfDays;
                                            dOtherOffset = Math.Round(m_dOiOffset * dCalcFactor, 2, MidpointRounding.AwayFromZero);  //MITS 14186
                                        }
                                        else
                                        {
                                            dOtherOffset = m_dOiOffset;
                                        }
                                    }
                            }
                            //*************************************************************
                            //Animesh Inserted for GHS enhancment with PS2 modified logic  //pmittal5 Mits 14841
                            if (dOther1 > 0)
                            {
                                if (PartialPay(iNumOfDays, iPrefSched, objDisClass))
                                {
                                    if (m_iOffsetCalc == 1 || m_iOffsetCalc == 2)
                                    {
                                        if (objDisClass.FullpayOnlyFlag)
                                            dOtherOffset_1 = 0;
                                        else if (objDisClass.ProrateOffFlag)
                                            //dOtherOffset_1 = dOther1 * iNumOfDays;
                                            dOtherOffset_1 = Math.Round(m_dOthOffset1 * dCalcFactor, 2, MidpointRounding.AwayFromZero); //MITS 14186
                                        else
                                        {
                                            if (iPrefSched == 2)
                                            {
                                                dOtherOffset_1 = m_dOthOffset1 * 2;
                                            }
                                            else
                                            {
                                                dOtherOffset_1 = m_dOthOffset1;
                                            }
                                        }
                                    }
                                    else
                                        dOtherOffset_1 = 0;
                                }
                                else //last pay is whole
                                {
                                    if (iPrefSched == 2)
                                    {
                                        dOtherOffset_1 = m_dOthOffset1 * 2;
                                    }
                                    else if (iPrefSched == 3 && objDisClass.ProrateOffFlag)
                                    {
                                        //dOtherOffset_1 = dOther1 * iNumOfDays;
                                        dOtherOffset_1 = Math.Round(m_dOthOffset1 * dCalcFactor, 2, MidpointRounding.AwayFromZero); //MITS 14186
                                    }
                                    else
                                    {
                                        dOtherOffset_1 = m_dOthOffset1;
                                    }
                                }
                            }
                            if (dOther2 > 0)
                            {
                                if (PartialPay(iNumOfDays, iPrefSched, objDisClass))
                                {
                                    if (m_iOffsetCalc == 1 || m_iOffsetCalc == 2)
                                    {
                                        if (objDisClass.FullpayOnlyFlag)
                                            dOtherOffset_2 = 0;
                                        else if (objDisClass.ProrateOffFlag)
                                            //dOtherOffset_2 = dOther2 * iNumOfDays;
                                            dOtherOffset_2 = Math.Round(m_dOthOffset2 * dCalcFactor, 2, MidpointRounding.AwayFromZero); //MITS 14186
                                        else
                                        {
                                            if (iPrefSched == 2)
                                            {
                                                dOtherOffset_2 = m_dOthOffset2 * 2;
                                            }
                                            else
                                            {
                                                dOtherOffset_2 = m_dOthOffset2;
                                            }
                                        }
                                    }
                                    else
                                        dOtherOffset_2 = 0;
                                }
                                else //last pay is whole
                                {
                                    if (iPrefSched == 2)
                                    {
                                        dOtherOffset_2 = m_dOthOffset2 * 2;
                                    }
                                    else if (iPrefSched == 3 && objDisClass.ProrateOffFlag)
                                    {
                                        //dOtherOffset_2 = dOther2 * iNumOfDays;
                                        dOtherOffset_2 = Math.Round(m_dOthOffset2 * dCalcFactor, 2, MidpointRounding.AwayFromZero); //MITS 14186
                                    }
                                    else
                                    {
                                        dOtherOffset_2 = m_dOthOffset2;
                                    }
                                }
                            }
                            if (dOther3 > 0)
                            {
                                if (PartialPay(iNumOfDays, iPrefSched, objDisClass))
                                {
                                    if (m_iOffsetCalc == 1 || m_iOffsetCalc == 2)
                                    {
                                        if (objDisClass.FullpayOnlyFlag)
                                            dOtherOffset_3 = 0;
                                        else if (objDisClass.ProrateOffFlag)
                                            //dOtherOffset_3 = dOther3 * iNumOfDays;
                                            dOtherOffset_3 = Math.Round(m_dOthOffset3 * dCalcFactor, 2, MidpointRounding.AwayFromZero); //MITS 14186
                                        else
                                        {
                                            if (iPrefSched == 2)
                                            {
                                                dOtherOffset_3 = m_dOthOffset3 * 2;
                                            }
                                            else
                                            {
                                                dOtherOffset_3 = m_dOthOffset3;
                                            }
                                        }
                                    }
                                    else
                                        dOtherOffset_3 = 0;
                                }
                                else //last pay is whole
                                {
                                    if (iPrefSched == 2)
                                    {
                                        dOtherOffset_3 = m_dOthOffset3 * 2;
                                    }
                                    else if (iPrefSched == 3 && objDisClass.ProrateOffFlag)
                                    {
                                        //dOtherOffset_3 = dOther3 * iNumOfDays;
                                        dOtherOffset_3 = Math.Round(m_dOthOffset3 * dCalcFactor, 2, MidpointRounding.AwayFromZero); //MITS 14186
                                    }
                                    else
                                    {
                                        dOtherOffset_3 = m_dOthOffset3;
                                    }
                                }
                            }
                            if (dPostTax1 > 0)
                            {
                                dPstTaxDed_1 = m_dPstTaxDed1;  //pmittal5 Mits 14841
                            }
                            if (dPostTax2 > 0)
                            {
                                dPstTaxDed_2 = m_dPstTaxDed2;  //pmittal5 Mits 14841
                            } 
                            //Animesh Insertion ends
                            //*************************************************************


                            dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset), 2, MidpointRounding.AwayFromZero);
                            //Animesh inserted the condition for GHS    //pmittal5 Mits 14841                          
                            dTotalOffset = Math.Round(dTotalOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3, 2, MidpointRounding.AwayFromZero);
                            //Animesh modification Ends

                            //kdb 05/11/2005 Moved print date calc here
 
//Mits 10780change done by pawan commented lines below updated code with new lines of code as below for getting the print date correctly
//				if (objPlan.PrintBeforeEnd)
//                                {
//                                    if (dPrintDate < dPaymentStart)
//                                    {
//                                        if (objPlan.PrefPaySchCode == 1737)
//                                            dPrintDate = dPrintDate.AddDays(14);
//                                        else if (iPrefSched == 1)
//                                            dPrintDate = dPrintDate.AddDays(7);
//                                        else if (iPrefSched == 3)  //kdb 04/04/2005
//                                        {
//                                            m_objCache.GetCodeInfo(objPlan.PrefDayCode, ref sShortCode, ref sCodeDesc);
//                                            dPrintDate = GetOrdinalDate(dPrintDate.Year, dPrintDate.Month + 1, Conversion.ConvertStrToInteger(m_objCache.GetShortCode(objPlan.PrefDayOfMCode)), GetDayNumber(sCodeDesc));
//                                        }
//                                    }
//                                }
//                                else if (dPrintDate < dPaymentEnd)
//                                {
//                                    if (objPlan.PrefPaySchCode == 1737)
//                                        dPrintDate = dPrintDate.AddDays(14);
//                                    else if (iPrefSched == 1)
//                                        dPrintDate = dPrintDate.AddDays(7);
//                                    else if (iPrefSched == 3)  //kdb 04/04/2005
//                                    {
//                                        m_objCache.GetCodeInfo(objPlan.PrefDayCode, ref sShortCode, ref sCodeDesc);
//                                        dPrintDate = GetOrdinalDate(dPrintDate.Year, dPrintDate.Month + 1, Conversion.ConvertStrToInteger(m_objCache.GetShortCode(objPlan.PrefDayOfMCode)), GetDayNumber(sCodeDesc));
//                                    }
//                                }
                            //MITS 13135 : Umesh 09/04/08
                            iOrdinal = 0;
                            dTempPaymentStart = dPaymentStart;
                            if (iPrefSched == 3)
                            {

                                m_objCache.GetCodeInfo(objPlan.PrefDayOfMCode, ref sShortCode, ref sCodeDesc);
                                iOrdinal = Conversion.ConvertStrToInteger(sShortCode);
                                m_objCache.GetCodeInfo(objPlan.PrefDayCode, ref sShortCode, ref sCodeDesc);

                            }
                            //MITS 13135 : End

                            //Pawan  MITS 12620  06/30/08
                            if (iPrefSched == 2 && dPrintDate < dPaymentStart.AddDays(14))
                            {
                                dPrintDate = dPrintDate.AddDays(14);
                            }
                            //MITS 13135 : Umesh 09/04/08
                            //else if (iPrefSched == 3 && dPrintDate < dPaymentStart.AddMonths(1).AddDays(-1))
                            //{
                            //    dPrintDate = dPrintDate.AddDays(28);
                            //}
                            else if (iPrefSched == 1 && dPrintDate < dPaymentStart.AddDays(7))
                            {
                                dPrintDate = dPrintDate.AddDays(7);
                            }
                            //End
                                
                            if (objPlan.PrintBeforeEnd)
                                {
                                    while (dPrintDate < dPaymentStart)
                                    {
                                        if (iPrefSched == 2)
                                        {
                                            dPrintDate = dPrintDate.AddDays(14);
                                        }
                                        else if (iPrefSched == 3)
                                        {
                                           // dPrintDate = dPrintDate.AddDays(28);
                                            //MITS 13135 : Umesh 09/04/08
                                            //dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            //dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                            if (iTypeOfMonthlyPay != 1) //in case of day based
                                            {
                                                dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            }
                                            else
                                            {
                                                if (iPrefDate <= dTempPaymentStart.AddMonths(1).AddDays(-1).Day)
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, iPrefDate);
                                                else
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, dTempPaymentStart.AddMonths(1).AddDays(-1).Day);
                                            }
                                            dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                        }
                                        else
                                        {
                                            dPrintDate = dPrintDate.AddDays(7);
                                        }
                                        
                                    }
                                    while (dPrintDate < DateTime.Now.Date)
                                    {
                                        if (iPrefSched == 2)
                                        {
                                            dPrintDate = dPrintDate.AddDays(14);
                                        }
                                        else if (iPrefSched == 3)
                                        {
                                            //dPrintDate = dPrintDate.AddDays(28);
                                            //MITS 13135 : Umesh 09/04/08
                                            //dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            //dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                            if (iTypeOfMonthlyPay != 1) //in case of day based
                                            {
                                                dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            }
                                            else
                                            {
                                                if (iPrefDate <= dTempPaymentStart.AddMonths(1).AddDays(-1).Day)
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, iPrefDate);
                                                else
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, dTempPaymentStart.AddMonths(1).AddDays(-1).Day);
                                            }
                                            dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                        }
                                        else
                                        {
                                            dPrintDate = dPrintDate.AddDays(7);
                                        }
                                    }
                                }
                                else 
                                {
                                    //MITS 13135 : Umesh 09/04/08
                                    //if (iPrefSched == 1)
                                    //    dTempPaymentEnd = dPaymentStart.AddDays(7);
                                    //else if (iPrefSched == 2)
                                    //    dTempPaymentEnd = dPaymentStart.AddDays(14);
                                    
                                   //pmittal5 MITS 12620 11/14/08
                                    if (iPrefSched == 1 || iPrefSched == 2)
                                        dTempPaymentEnd = dPaymentEnd;
                                   //End - pmittal5
                                   if(iPrefSched ==3)
                                        dTempPaymentEnd = dPaymentStart.AddMonths(1).AddDays(-1);
                                
                                //while (dPrintDate < dPaymentEnd)
                                    while (dPrintDate < dTempPaymentEnd)
                                    {
                                        if (iPrefSched == 2)
                                        {
                                            dPrintDate = dPrintDate.AddDays(14);
                                        }
                                        else if (iPrefSched == 3)
                                        {
                                           // dPrintDate = dPrintDate.AddDays(28);
                                            //MITS 13135 : Umesh 09/04/08
                                            //dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            //dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                            if (iTypeOfMonthlyPay != 1) //in case of day based
                                            {
                                                dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            }
                                            else
                                            {
                                                if (iPrefDate <= dTempPaymentStart.AddMonths(1).AddDays(-1).Day)
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, iPrefDate);
                                                else
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, dTempPaymentStart.AddMonths(1).AddDays(-1).Day);
                                            }
                                            dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                        }
                                        else
                                        {
                                            dPrintDate = dPrintDate.AddDays(7);
                                        }
                                    }
                                    while (dPrintDate < DateTime.Now.Date)
                                    {
                                        if (iPrefSched == 2)
                                        {
                                            dPrintDate = dPrintDate.AddDays(14);
                                        }
                                        else if (iPrefSched == 3)
                                        {
                                           // dPrintDate = dPrintDate.AddDays(28);
                                            //MITS 13135 : Umesh 09/04/08
                                            //dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            //dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                            if (iTypeOfMonthlyPay != 1) //in case of day based
                                            {
                                                dPrintDate = GetOrdinalDate(dTempPaymentStart.Year, dTempPaymentStart.Month, iOrdinal, GetDayNumber(sCodeDesc));
                                            }
                                            else
                                            {
                                                if (iPrefDate <= dTempPaymentStart.AddMonths(1).AddDays(-1).Day)
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, iPrefDate);
                                                else
                                                    dPrintDate = new DateTime(dTempPaymentStart.Year, dTempPaymentStart.Month, dTempPaymentStart.AddMonths(1).AddDays(-1).Day);
                                            }
                                            dTempPaymentStart = dTempPaymentStart.AddMonths(1);
                                        }
                                        else
                                        {
                                            dPrintDate = dPrintDate.AddDays(7);
                                        }
                                    }
                                    //pmittal5 Mits 14889 - Print Date should not fall in same month as that of Payment Date in case of Date based Print Date
                                    if (iPrefSched == 3 && iTypeOfMonthlyPay == 1)
                                    {
                                        if (dPrintDate.Month == dTempPaymentEnd.Month)
                                        {
                                            dPrintDate = dPrintDate.AddMonths(1);
                                            if (iPrefDate <= dTempPaymentStart.AddMonths(2).AddDays(-1).Day)
                                                dPrintDate = new DateTime(dPrintDate.Year, dPrintDate.Month, iPrefDate);
                                            else
                                                dPrintDate = new DateTime(dPrintDate.Year, dPrintDate.Month, dTempPaymentStart.AddMonths(2).AddDays(-1).Day);
                                        }
                                    }
                                    //End - pmittal5
                                }
                                //else
                                //{
                                //    //abisht MITS 10780
                                //    //This portion of code should only execute when pref Sched = 3
                                //    if (iPrefSched == 3)
                                //    {
                                //        m_objCache.GetCodeInfo(objPlan.PrefDayCode, ref sShortCode, ref sCodeDesc);
                                //        dPrintDate = GetOrdinalDate(dPrintDate.Year, dPrintDate.Month + 1, Conversion.ConvertStrToInteger(m_objCache.GetShortCode(objPlan.PrefDayOfMCode)), GetDayNumber(sCodeDesc));
                                //    }
                                //}

                                for (j = 1; j <= iPaymentLoop; j++) //kdb 04/15/2005  Run twice for supplement payments
                                {

                                    if (iNumOfDays > 0)  //MWC 05/08/2003 Only create payment if ther are benefit days to pay.
                                    {
                                        if (iPrefSched == 1 || iPrefSched == 2) //weekly/bi-weekly
                                            dThisDate = dToDate.AddDays(1).AddDays((i - 2) * iDays);
                                        else if (iPrefSched == 3)  //monthly kdb 04/07/2005
                                            dThisDate = dPaymentStart;

                                   //pmittal5 Mits 14186 02/24/09 - Commented below lines and placed them before dCalcFactor calculation
                                        //kdb 04/12/2005 Actual Month
                                        //if (iPrefSched == 3 && objDisClass.Day30WorkMonth == false)
                                        //{
                                        //    m_dDailyAmount = m_dWeeklyBenefit / DaysInMonth(dPaymentStart);
                                        //    if (m_dDailySuppAmount > 0)
                                        //        m_dDailySuppAmount = m_dSuppRate / DaysInMonth(dPaymentStart);

                                        //}
                                   //End - pmittal5 Mits 14186   

                                        //Subtract offsets from payment
                                        if (bSuppRollUp == true)
                                        {
                                            dFinalPaymentAmount = Math.Round((m_dDailySuppAmount * iNumOfDays), 2, MidpointRounding.AwayFromZero) + Math.Round((m_dDailyAmount * iNumOfDays), 2, MidpointRounding.AwayFromZero);
                                            //pmittal5 Mits 14253 01/15/09 - Allow Zero Payments
                                            //if ((dFinalPaymentAmount - dTotalOffset) > 0)


                                            //MITS 14186  
                                            //As discussed with Mike Hamann , if  dPayment - dTotalOffset = 0.01
                                            //deduct 1 cent from the offset having maximum value and make  net payment = 0

                                            if (Math.Round((dFinalPaymentAmount - dTotalOffset), 2, MidpointRounding.AwayFromZero) < 0 && Math.Round((dFinalPaymentAmount - dTotalOffset), 2, MidpointRounding.AwayFromZero) >= -0.01)
                                            {

                                                double[] dArrayofOffsets = new double[] { dPensionOffset, dSSOffset, dOtherOffset,dOtherOffset_1,dOtherOffset_2,dOtherOffset_3 };
                                                int iIndex = GetMaxOffsetIndex(dArrayofOffsets);

                                                //deducting 1 Cent from offset having maximum value
                                                dArrayofOffsets[iIndex] = Math.Round((dArrayofOffsets[iIndex] - dOneCent), 2, MidpointRounding.AwayFromZero);

                                                dPensionOffset = dArrayofOffsets[0];
                                                dSSOffset = dArrayofOffsets[1];
                                                dOtherOffset = dArrayofOffsets[2];
                                                dOtherOffset_1 = dArrayofOffsets[3];
                                                dOtherOffset_2 = dArrayofOffsets[4];
                                                dOtherOffset_3 = dArrayofOffsets[5];
                                                dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3), 2, MidpointRounding.AwayFromZero);
                                            }

                                            if (Math.Round((dFinalPaymentAmount - dTotalOffset), 2, MidpointRounding.AwayFromZero) >= 0)   //MITS 14186
                                                dFinalPaymentAmount = Math.Round((dFinalPaymentAmount - dTotalOffset), 2, MidpointRounding.AwayFromZero);
                                            else
                                            {
                                                //DeletePayments("0," + iBatchId.ToString());
                                                //return 0;

                                                if (p_bSave)
                                                {
                                                    UpdateAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                                    DeleteAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                                }
                                                continue;
                                            }
                                            //End - pmittal5
                                        }
                                        else if (bSuppPaymentFlag == false)
                                        {
                                            dFinalPaymentAmount = Math.Round(m_dDailyAmount * iNumOfDays, 2, MidpointRounding.AwayFromZero);
                                            dPaymentOffsetDif = Math.Round((dFinalPaymentAmount - dTotalOffset), 2, MidpointRounding.AwayFromZero);
                                            //kdb 04/28/2005 Get Offsets
                                            dTempPensionOffset = 0;
                                            dTempSSOffset = 0;
                                            dTempOtherOffset = 0;
                                            //Added by RKaur //pmittal5 Mits 14841
                                            dTempOffset1 = 0;
                                            dTempOffset2 = 0;
                                            dTempOffset3 = 0;
                                            dTempPstTD1 = 0; 
                                            dTempPstTD2 = 0;
                                            ////////////

                                            if (dPaymentOffsetDif <= 0)
                                            {
                                                dTempOffset = dFinalPaymentAmount - 1;
                                                //Calculate individual offset amounts
                                                if (dPensionOffset < dTempOffset)
                                                    dTempOffset = Math.Round((dTempOffset - dPensionOffset), 2, MidpointRounding.AwayFromZero);
                                                else if (dPensionOffset == dTempOffset)
                                                    dTempOffset = 0;
                                                else
                                                {
                                                    dTempPensionOffset = Math.Round((dPensionOffset - dTempOffset), 2, MidpointRounding.AwayFromZero);
                                                    dPensionOffset = dTempOffset;
                                                    dTempSSOffset = dSSOffset;
                                                    dTempOtherOffset = dOtherOffset;
                                                    //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                    dTempOffset1 = dOtherOffset_1;
                                                    dTempOffset2 = dOtherOffset_2;
                                                    dTempOffset3 = dOtherOffset_3;
                                                    dTempPstTD1 = dPstTaxDed_1; 
                                                    dTempPstTD2 = dPstTaxDed_2;
                                                    //Animesh Insertion ends
                                                    dSSOffset = 0;
                                                    dOtherOffset = 0;
                                                    //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                    dOtherOffset_1 = 0;
                                                    dOtherOffset_2 = 0;
                                                    dOtherOffset_3 = 0;
                                                    dPstTaxDed_1 = 0;
                                                    dPstTaxDed_2 = 0;
                                                    goto EndOffsetCalc3;
                                                }

                                                if (dSSOffset < dTempOffset)
                                                    dTempOffset = Math.Round((dTempOffset - dSSOffset), 2, MidpointRounding.AwayFromZero);
                                                else if (dSSOffset == dTempOffset)
                                                    dTempOffset = 0;
                                                else
                                                {
                                                    dTempSSOffset = Math.Round((dSSOffset - dTempOffset), 2, MidpointRounding.AwayFromZero);
                                                    dSSOffset = dTempOffset;
                                                    dTempOtherOffset = dOtherOffset;
                                                    //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                    dTempOffset1 = dOtherOffset_1;
                                                    dTempOffset2 = dOtherOffset_2;
                                                    dTempOffset3 = dOtherOffset_3;
                                                    dTempPstTD1 = dPstTaxDed_1; 
                                                    dTempPstTD2 = dPstTaxDed_2;
                                                    //Animesh Insertion ends
                                                    dOtherOffset = 0;
                                                    //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                    dOtherOffset_1 = 0;
                                                    dOtherOffset_2 = 0;
                                                    dOtherOffset_3 = 0;
                                                    dPstTaxDed_1 = 0; 
                                                    dPstTaxDed_2 = 0;
                                                    //Animesh Insertion ends
                                                    goto EndOffsetCalc3;
                                                }

                                                if (dOtherOffset < dTempOffset)
                                                    dTempOffset = Math.Round((dTempOffset - dOtherOffset), 2, MidpointRounding.AwayFromZero);
                                                else if (dOtherOffset == dTempOffset)
                                                    dTempOffset = 0;
                                                else
                                                {
                                                    dTempOtherOffset = Math.Round((dOtherOffset - dTempOffset), 2, MidpointRounding.AwayFromZero);
                                                    dOtherOffset = dTempOffset;
                                                    //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                    dTempOffset1 = dOtherOffset_1; 
                                                    dTempOffset2 = dOtherOffset_2;
                                                    dTempOffset3 = dOtherOffset_3;
                                                    dTempPstTD1 = dPstTaxDed_1; 
                                                    dTempPstTD2 = dPstTaxDed_2;
                                                    //Animesh Insertion ends
                                                    //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                    dOtherOffset_1 = 0;
                                                    dOtherOffset_2 = 0;
                                                    dOtherOffset_3 = 0;
                                                    dPstTaxDed_1 = 0; 
                                                    dPstTaxDed_2 = 0;
                                                    goto EndOffsetCalc3;
                                                    //Animesh Insertion ends
                                                }
                                                //**********************************
                                                //Animesh Inserted for GHS Enhancement //pmittal5 Mits 14841
                                                if (dOtherOffset_1 < dTempOffset)
                                                    dTempOffset = dTempOffset - dOtherOffset_1;
                                                else if (dOtherOffset_1 == dTempOffset)
                                                    dTempOffset = 0;
                                                else
                                                {
                                                    dTempOffset1 = dOtherOffset_1 - dTempOffset;
                                                    dOtherOffset_1 = dTempOffset;
                                                    //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                    dTempOffset2 = dOtherOffset_2;
                                                    dTempOffset3 = dOtherOffset_3;
                                                    dTempPstTD1 = dPstTaxDed_1; 
                                                    dTempPstTD2 = dPstTaxDed_2;
                                                    //Animesh Insertion ends
                                                    dOtherOffset_2 = 0;
                                                    dOtherOffset_3 = 0;
                                                    dPstTaxDed_1 = 0; 
                                                    dPstTaxDed_2 = 0;
                                                    goto EndOffsetCalc3;
                                                    //Animesh Insertion ends
                                                }
                                                if (dOtherOffset_2 < dTempOffset)
                                                    dTempOffset = dTempOffset - dOtherOffset_2;
                                                else if (dOtherOffset_2 == dTempOffset)
                                                    dTempOffset = 0;
                                                else
                                                {
                                                    dTempOffset2 = dOtherOffset_2 - dTempOffset;
                                                    dOtherOffset_2 = dTempOffset;
                                                    //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                    dTempOffset3 = dOtherOffset_3;
                                                    dTempPstTD1 = dPstTaxDed_1; 
                                                    dTempPstTD2 = dPstTaxDed_2;
                                                    //Animesh Insertion ends
                                                    dOtherOffset_3 = 0;
                                                    dPstTaxDed_1 = 0; 
                                                    dPstTaxDed_2 = 0;
                                                    goto EndOffsetCalc3;
                                                    //Animesh Insertion ends
                                                }
                                                if (dOtherOffset_3 < dTempOffset)
                                                    dTempOffset = dTempOffset - dOtherOffset_3;
                                                else if (dOtherOffset_3 == dTempOffset)
                                                    dTempOffset = 0;
                                                else
                                                {
                                                    dTempOffset3 = dOtherOffset_3 - dTempOffset;
                                                    dOtherOffset_3 = dTempOffset;
                                                    //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                    dTempPstTD1 = dPstTaxDed_1;
                                                    dTempPstTD2 = dPstTaxDed_2;
                                                    //Animesh Insertion ends
                                                    dPstTaxDed_1 = 0; 
                                                    dPstTaxDed_2 = 0;
                                                    goto EndOffsetCalc3;
                                                    //Animesh Insertion ends
                                                }
                                                if (dPstTaxDed_1 < dTempOffset)
                                                    dTempOffset = dTempOffset - dPstTaxDed_1;
                                                else if (dPstTaxDed_1 == dTempOffset)
                                                    dTempOffset = 0;
                                                else
                                                {
                                                    dTempPstTD1 = dPstTaxDed_1 - dTempOffset;
                                                    dPstTaxDed_1 = dTempOffset;
                                                    //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                                                    dTempPstTD2 = dPstTaxDed_2;
                                                    //Animesh Insertion ends
                                                    dPstTaxDed_2 = 0;
                                                    goto EndOffsetCalc3;
                                                    //Animesh Insertion ends
                                                }
                                                if (dPstTaxDed_2 < dTempOffset)
                                                    dTempOffset = dTempOffset - dPstTaxDed_2;
                                                else if (dPstTaxDed_2 == dTempOffset)
                                                    dTempOffset = 0;
                                                else
                                                {
                                                    dTempPstTD2 = dPstTaxDed_2 - dTempOffset;
                                                    dPstTaxDed_2 = dTempOffset;
                                                }
                                            //**********************************
                                            //
                                            EndOffsetCalc3:
                                                dFinalPaymentAmount = 1;
                                            }
                                            else
                                                dFinalPaymentAmount = dPaymentOffsetDif;

                                            dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset), 2, MidpointRounding.AwayFromZero);
                                            //Animesh Inserted for GHS Enhancement  //pmittal5 Mits 14841
                                            dTotalOffset = Math.Round(dTotalOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3, 2, MidpointRounding.AwayFromZero); 
                                            //Animesh Insertion ends 
                                        }
                                        else if (bSuppPaymentFlag == true)
                                        {
                                            dFinalPaymentAmount = Math.Round(m_dDailySuppAmount * iNumOfDays, 2, MidpointRounding.AwayFromZero);
                                            dPensionOffset = dTempPensionOffset;
                                            dSSOffset = dTempSSOffset;
                                            dOtherOffset = dTempOtherOffset;
                                            //Animesh Inserted for GHS Enhancement //pmittal5 Mits 14841
                                            dOtherOffset_1 = dTempOffset1;
                                            dOtherOffset_2 = dTempOffset2;
                                            dOtherOffset_3 = dTempOffset3;
                                            dPstTaxDed_1 = dTempPstTD1; 
                                            dPstTaxDed_2 = dTempPstTD2;
                                            //Animesh Insertion Ends 
                                            dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset), 2, MidpointRounding.AwayFromZero);
                                            //Animesh Inserted for GHS enhancment  //pmittal5 Mits 14841
                                            dTotalOffset = Math.Round(dTotalOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3, 2, MidpointRounding.AwayFromZero);
                                            //Animesh Insertion Ends 
                                            dFinalPaymentAmount = Math.Round((dFinalPaymentAmount - dTotalOffset), 2, MidpointRounding.AwayFromZero);
                                            //pmittal5 Mits 14253 01/15/09 - Allow Zero Payments
                                            //if (dFinalPaymentAmount <= 0)
                                            //{
                                            //    DeletePayments("0," + iBatchId.ToString());
                                            //    return 0;
                                            //}


                                            //MITS 14186  
                                            //As discussed with Mike Hamann , if  dPayment - dTotalOffset = 0.01
                                            //deduct 1 cent from the offset having maximum value and make  net payment = 0

                                            if (dFinalPaymentAmount < 0 && dFinalPaymentAmount >= -0.01)
                                            {

                                                double[] dArrayofOffsets = new double[] { dPensionOffset, dSSOffset, dOtherOffset, dOtherOffset_1, dOtherOffset_2,  dOtherOffset_3 };
                                                int iIndex = GetMaxOffsetIndex(dArrayofOffsets);

                                                //deducting 1 Cent from offset having maximum value
                                                dArrayofOffsets[iIndex] = Math.Round((dArrayofOffsets[iIndex] - dOneCent), 2, MidpointRounding.AwayFromZero);

                                                dPensionOffset = dArrayofOffsets[0];
                                                dSSOffset = dArrayofOffsets[1];
                                                dOtherOffset = dArrayofOffsets[2];
                                                dOtherOffset_1 = dArrayofOffsets[3];
                                                dOtherOffset_2 = dArrayofOffsets[4];
                                                dOtherOffset_3 = dArrayofOffsets[5];
                                                dTotalOffset = Math.Round((dPensionOffset + dSSOffset + dOtherOffset + dOtherOffset_1 + dOtherOffset_2 + dOtherOffset_3), 2, MidpointRounding.AwayFromZero);
                                                dFinalPaymentAmount = 0.00;


                                            }
                                            if (dFinalPaymentAmount < 0)
                                            {
                                                if (p_bSave)
                                                {
                                                    UpdateAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                                    DeleteAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                                }
                                                continue;
                                            }
                                            //End - pmital5
                                        }


                                        //dFinalPaymentAmount = Math.Round(dFinalPaymentAmount, 2);
                                        //dSupp = Math.Round(m_dDailySuppAmount * iNumOfDays, 2);
                                        dSupp = Math.Round(m_dDailySuppAmount * iNumOfDays, 2, MidpointRounding.AwayFromZero);
                                        lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);

                                        //Figure the tax which needs to be subtracted from the amount when saved to Funds_auto
                                        //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                        //if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax > 0)
                                        if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax >= 0)
                                        {
                                            if (p_dtpTaxes.dblTaxablePercent > 0)
                                                dFedTax = Math.Round(dFinalPaymentAmount * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.FED_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                            else
                                                dFedTax = Math.Round(dFinalPaymentAmount * p_dtpTaxes.FED_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                            dFedTax = 0;
                                        //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                        //if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount > 0)
                                        if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount >= 0)
                                        {
                                            if (p_dtpTaxes.dblTaxablePercent > 0)
                                                dSSTax = Math.Round(dFinalPaymentAmount * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.SS_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                            else
                                                dSSTax = Math.Round(dFinalPaymentAmount * p_dtpTaxes.SS_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                            dSSTax = 0;
                                        //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                        //if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount > 0)
                                        if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount >= 0)
                                        {
                                            if (p_dtpTaxes.dblTaxablePercent > 0)
                                                dMedTax = Math.Round(dFinalPaymentAmount * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.MEDICARE_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                            else
                                                dMedTax = Math.Round(dFinalPaymentAmount * p_dtpTaxes.MEDICARE_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                            dMedTax = 0;
                                        //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                        //if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount > 0)
                                        if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount >= 0)
                                        {
                                            if (p_dtpTaxes.dblTaxablePercent > 0)
                                                dStateTax = Math.Round(dFinalPaymentAmount * p_dtpTaxes.dblTaxablePercent * p_dtpTaxes.STATE_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                            else
                                                dStateTax = Math.Round(dFinalPaymentAmount * p_dtpTaxes.STATE_TAX_RATE, 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                            dStateTax = 0;

                                        dTotalTax = Math.Round((dFedTax + dSSTax + dMedTax + dStateTax),2);

                                        //pmittal5 Mits 14841
                                        if (Math.Round(dFinalPaymentAmount - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2), 2, MidpointRounding.AwayFromZero) < 0) 
                                        {
                                            if (p_bSave)
                                            {
                                                UpdateAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                                DeleteAutoBatchPayment(iBatchId, objDbTransaction, objDbConnection);
                                            }
                                            if (bSuppPaymentFlag == false) //pmittal5 Mits 14841
                                                bSuppPaymentFlag = true;
                                            continue;
                                        }

                                        //kdb 04/28/2005 Set up sAppend for the control number
                                        if (bSuppPaymentFlag)
                                            sAppend = "S";
                                        else
                                            sAppend = "";
                                        dPrintDateHS = AdjustPrintDate(objClaim.AdjustPrintDatesFlag, dPrintDate);//neha R8 Funds utilty enhancement
                                        //Save the payment to Funds_auto table
                                        if (p_bSave)
                                        {
                                            
                                            //Animesh Modified the condition for GHS 
                                            //sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, objClaim.PrimaryClaimant.ClaimantEid, (dFinalPaymentAmount - dTotalTax), 0, i, false, false, iNumberOfPayments, dPrintDate, sAppend, iNumOfDays, bSuppPaymentFlag);
                                            sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, objClaim.PrimaryClaimant.ClaimantEid, Math.Round((dFinalPaymentAmount - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2)), 2, MidpointRounding.AwayFromZero), 0, i, false, false, iNumberOfPayments, dPrintDateHS, sAppend, iNumOfDays, bSuppPaymentFlag);
                                            //Animesh Modification Ends
                                            //execute sql
                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                            lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);


                                            if (bSuppPaymentFlag == true)
                                                lTransType = m_lSuppTransType;
                                            else
                                                lTransType = p_iTransTypeCode;


                                            if (bSuppRollUp == true)
                                                //pmittal5 Mits 14253 01/14/09
                                                //sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lTransType, dFinalPaymentAmount - dSupp + dTotalOffset, dPaymentStart, dEndDate, "0", false);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lTransType, Math.Round((dFinalPaymentAmount - dSupp + dTotalOffset), 2, MidpointRounding.AwayFromZero), dThisDate, dEndDate, "0", false);
                                            else
                                                //pmittal5 Mits 14253 01/14/09
                                                //sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lTransType, dFinalPaymentAmount + dTotalOffset, dPaymentStart, dEndDate, "0", bSuppPaymentFlag);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lTransType, Math.Round((dFinalPaymentAmount + dTotalOffset), 2, MidpointRounding.AwayFromZero), dThisDate, dEndDate, "0", bSuppPaymentFlag);

                                            objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                            //kdb 04/28/2005 Save the supplement as a split if it exists
                                            if (bSuppRollUp == true && dSupp > 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                lTransType = m_lSuppTransType;
                                                //pmittal5 Mits 14253 01/14/09
                                                //sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lTransType, dSupp, Conversion.ToDate(p_sBenCalcPayStart), dEndDate, "8", true);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lTransType, dSupp, dThisDate, dEndDate, "8", true);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }

                                            //Save Third Party Splits to Funds_auto_split table
                                            //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                            //if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax > 0)
                                            if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax >= 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iFedTransTypeCodeID, (dFedTax * -1), dThisDate, dEndDate, "1", bSuppPaymentFlag);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                            //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                            //if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount > 0)
                                            if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount >= 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iSSTransTypeCodeID, (dSSTax * -1), dThisDate, dEndDate, "2", bSuppPaymentFlag);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                            //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                            //if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount > 0)
                                            if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount >= 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iMedicareTransTypeCodeID, (dMedTax * -1), dThisDate, dEndDate, "3", bSuppPaymentFlag);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                            //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                            //if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount > 0)
                                            if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount >= 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iStateTransTypeCodeID, (dStateTax * -1), dThisDate, dEndDate, "4", bSuppPaymentFlag);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }


                                            //KDB 04/25/2005 Save Offsets splits to FUNDS_AUTO_SPLIT table
                                            if (dPensionOffset > 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                //pmittal5 Mits 14253 01/14/09
                                                //sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPensionOffTransTypeCodeID, (dPensionOffset * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "5", bSuppPaymentFlag);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPensionOffTransTypeCodeID, (dPensionOffset * -1), dThisDate, dEndDate, "5", bSuppPaymentFlag);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                            if (dSSOffset > 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                //pmittal5 Mits 14253 01/14/09
                                                //sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lSSOffTransTypeCodeID, (dSSOffset * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "6", bSuppPaymentFlag);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lSSOffTransTypeCodeID, (dSSOffset * -1), dThisDate, dEndDate, "6", bSuppPaymentFlag);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                            if (dOtherOffset > 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                //pmittal5 Mits 14253 01/14/09 
                                                //sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOtherOffTransTypeCodeID, (dOtherOffset * -1), Conversion.ToDate(p_sBenCalcPayStart), dToDate, "7", bSuppPaymentFlag);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOtherOffTransTypeCodeID, (dOtherOffset * -1), dThisDate, dEndDate, "7", bSuppPaymentFlag);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }

                                            //Added by Ravneet Kaur //pmittal5 Mits 14841
                                            if (dOtherOffset_1  > 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOthOff1TransTypeCodeID, (dOtherOffset_1 * -1), dThisDate, dEndDate, "9", bSuppPaymentFlag);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                            if (dOtherOffset_2  > 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOthOff2TransTypeCodeID, (dOtherOffset_2 * -1), dThisDate, dEndDate, "10", bSuppPaymentFlag);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                            if (dOtherOffset_3  > 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lOthOff3TransTypeCodeID, (dOtherOffset_3 * -1), dThisDate, dEndDate, "11", bSuppPaymentFlag);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                            if (dPstTaxDed_1 > 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPstTax1TransTypeCodeID, (dPstTaxDed_1 * -1), dThisDate, dEndDate, "12", bSuppPaymentFlag);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                            if (dPstTaxDed_2 > 0)
                                            {
                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, lPstTax2TransTypeCodeID, (dPstTaxDed_2 * -1), dThisDate, dEndDate, "13", bSuppPaymentFlag);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                           ///////////////////////


                                            //Save Split Payments to Funds_auto and Funds_auto_split tables
                                            //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                            //if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax > 0)
                                            if (p_dtpTaxes.iFedTaxEID != 0 && p_dtpTaxes.bFed && m_dFederalTax >= 0)
                                            {
                                                lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                                sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iFedTaxEID, dFedTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, sAppend + "T-1", iNumOfDays, false);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iFedTransTypeCodeID, dFedTax, dThisDate, dEndDate, "1", false);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                            //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                            //if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount > 0)
                                            if (p_dtpTaxes.iSSTaxEID != 0 && p_dtpTaxes.bSS && m_dSocialSecurityAmount >= 0)
                                            {
                                                lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                                sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iSSTaxEID, dSSTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, sAppend + "T-2", iNumOfDays, false);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iSSTransTypeCodeID, dSSTax, dThisDate, dEndDate, "2", false);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                            //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                            //if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount > 0)
                                            if (p_dtpTaxes.iMedicareTaxEID != 0 && p_dtpTaxes.bMed && m_dMedicareAmount >= 0)
                                            {
                                                lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                                sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iMedicareTaxEID, dMedTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, sAppend + "T-3", iNumOfDays, false);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iMedicareTransTypeCodeID, dMedTax, dThisDate, dEndDate, "3", false);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                            //pmittal5 Mits 14253 01/19/09 - Allow Zero Payments
                                            //if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount > 0)
                                            if (p_dtpTaxes.iStateTaxEID != 0 && p_dtpTaxes.bState && m_dStateAmount >= 0)
                                            {
                                                lFundsAutoTransID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO", objDbTransaction, m_iClientId);
                                                sSQL = GetDisCalcSQL(objClaim, objPlan, objDisClass, p_iAccountId, iBatchId, lFundsAutoTransID, p_dtpTaxes.iStateTaxEID, dStateTax, -1, i, true, false, iNumberOfPayments, dPrintDateHS, sAppend + "T-4", iNumOfDays, false);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);

                                                lFundsAutoSplitID = Utilities.GetNextUID(objDbConnection, "FUNDS_AUTO_SPLIT", objDbTransaction, m_iClientId);
                                                sSQL = GetRMWorldSplitSQL(objClaim, objPlan, objDisClass, lFundsAutoSplitID, lFundsAutoTransID, p_dtpTaxes.iStateTransTypeCodeID, dStateTax, dThisDate, dEndDate, "4", false);
                                                objDbConnection.ExecuteNonQuery(sSQL.ToString(), objDbTransaction);
                                            }
                                        } // closing p_bSave

                                        double dGrossPayment = 0;
                                        if (bSuppRollUp == true)
                                            dGrossPayment = Math.Round((dFinalPaymentAmount - dSupp + dTotalOffset), 2, MidpointRounding.AwayFromZero);
                                        else
                                            dGrossPayment =Math.Round(( dFinalPaymentAmount + dTotalOffset),2);
                                        //Added by RKaur on 04/09/2009  //pmittal5 Mits 14841
                                        dtempNetPayment = Math.Round(dFinalPaymentAmount - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2), 2, MidpointRounding.AwayFromZero);

                                        objRow = p_objTable.CreateElement("row");
                                        objDocumentElement.AppendChild(objRow);
                                        objRow.SetAttribute("class", "rowlight");
                                        objRow.SetAttribute("gross", string.Format("{0:0.00}", dGrossPayment));
                                        objRow.SetAttribute("paymentnumber", i.ToString());
                                        if (bSuppRollUp)  //pmittal5 Mits 15804 04/24/09 - Dont Show SuppPayment when Separate Supp Payment flag is ON
                                            objRow.SetAttribute("supppayment", string.Format("{0:0.00}", dSupp));
                                        else
                                            objRow.SetAttribute("supppayment", "");
                                        objRow.SetAttribute("fromdate", dThisDate.ToShortDateString());
                                        objRow.SetAttribute("todate", dEndDate.ToShortDateString());
                                        objRow.SetAttribute("printdate", dPrintDateHS.ToShortDateString());


                                        //Added by RKaur on 04/09/2009 //pmittal5 Mits 14841
                                        objRow.SetAttribute("net", string.Format("{0:0.00}", dtempNetPayment));

                                        //Animesh commented for GHS enhancment 
                                        //objRow.SetAttribute("net", string.Format("{0:0.00}", dFinalPaymentAmount - dTotalTax));
                                        //Animesh Inserted for GHS enhancment 
                                        //dtempNetPayment = dFinalPaymentAmount - dTotalTax - (dPstTaxDed_1 + dPstTaxDed_2);
                                        //if (dtempNetPayment > 0)
                                        //{
                                        //    objRow.SetAttribute("net", string.Format("{0:0.00}", dtempNetPayment));
                                        //}
                                        //else
                                        //{
                                        //    objRow.SetAttribute("net", string.Format("{0:0.00}", 0));
                                        //}
                                        //Animesh Insertion ends
                                        objRow.SetAttribute("fed", string.Format("{0:0.00}", dFedTax * -1));
                                        objRow.SetAttribute("ss", string.Format("{0:0.00}", dSSTax * -1));
                                        objRow.SetAttribute("med", string.Format("{0:0.00}", dMedTax * -1));
                                        objRow.SetAttribute("state", string.Format("{0:0.00}", dStateTax * -1));
                                        objRow.SetAttribute("pension", string.Format("{0:0.00}", dPensionOffset * -1));
                                        objRow.SetAttribute("ssoffset", string.Format("{0:0.00}", dSSOffset * -1));
                                        objRow.SetAttribute("oi", string.Format("{0:0.00}", dOtherOffset * -1));
                                        //Added by Ravneet Kaur //pmittal5 Mits 14841
                                        objRow.SetAttribute("othoffset1", string.Format("{0:0.00}", dOtherOffset_1  * -1));
                                        objRow.SetAttribute("othoffset2", string.Format("{0:0.00}", dOtherOffset_2  * -1));
                                        objRow.SetAttribute("othoffset3", string.Format("{0:0.00}", dOtherOffset_3  * -1));
                                        objRow.SetAttribute("psttaxded1", string.Format("{0:0.00}", dPstTaxDed_1 * -1));
                                        objRow.SetAttribute("psttaxded2", string.Format("{0:0.00}", dPstTaxDed_2 * -1));
                                        /////////////////////

                                        objRow.SetAttribute("dayspaid", iNumOfDays.ToString());
                                    }
                                    
                                    if (bSuppPaymentFlag == false)
                                        bSuppPaymentFlag = true; //kdb 04/15/2005  Flip the flag for marking payments as Supp
                                }///closing for
                                
                        }///closing Last Payment

                
                    }//// closing parent "for"

                    //pmittal5 Mits 14253 01/20/09
                    if(p_objTable.SelectSingleNode("table").ChildNodes.Count == 1)
                        objDocumentElement.SetAttribute("zerorecords", "True");
                    //End - pmittal5

                    ///Everything went fine..we must commit the transaction now.
                    if (p_bSave)
                        objDbTransaction.Commit();
            
                }//// closing GetSuppInfo() call
               
            } /// closing try
            





            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }

                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }
                throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.CalculateNonOccPayments.Error",m_iClientId), p_objException);
            }
            finally
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Dispose();
                    objDbTransaction = null;
                }

                if (objDbConnection != null)
                {
                    objDbConnection.Close();
                    objDbConnection.Dispose();
                }

                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return iBatchId;
        }

        private bool GetSuppInfo(Claim objClaim, DisabilityPlan objPlan, DisabilityClass objClass, ref TaxRate p_dtpTaxes, int p_iAccountId, int p_iTransTypeCode, ref long lSuppSubAcc, ref long lSuppBankAcc, ref long lSuppTransType , ref long lSuppResType , ref bool bSuppRollUp)
        {
            int i = 0;
            int iClassIndex = 0;
            DisClassTd objThisTableRow = null;
            long lTmp = 0;
            bool bGetSuppInfo = true;
            bSuppRollUp = true;

            if (objClaim.ClaimId != 0)
            {
                //kdb 04/27/2005 check for the separate payment flag for supp
                if (objClass.SuppSepPayFlag)
                {
                    bSuppRollUp = false;
                    //get bank/sub account info
                    if (objClaim.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                    {
                        if (objClass.SuppSubAccId == 0)
                        {
                            lSuppSubAcc = p_iAccountId;
                            lSuppBankAcc = GetParentAccountID(p_iAccountId);
                        }
                        else
                        {
                            lSuppSubAcc = objClass.SuppSubAccId;
                            lSuppBankAcc = GetParentAccountID(objClass.SuppSubAccId);
                        }
                    }
                    else
                    {
                        //if (objClass.SuppSubAccId == 0)  //Mits 15804
                        if(objClass.SuppBankAccId == 0)
                        {
                            lSuppSubAcc = 0;
                            lSuppBankAcc = p_iAccountId;
                        }
                        else
                        {
                            lSuppSubAcc = 0;
                            //lSuppBankAcc = objClass.SuppSubAccId;
                            lSuppBankAcc = objClass.SuppBankAccId;   //Mits 15804
                        }
                    }
                }
                else
                {
                    bSuppRollUp = true;
                    if (objClaim.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                    {
                        lSuppSubAcc = p_iAccountId;
                        lSuppBankAcc = GetParentAccountID(p_iAccountId);
                    }
                    else
                    {
                        lSuppSubAcc = 0;
                        lSuppBankAcc = p_iAccountId;
                    }
                }

              lSuppTransType = objClass.SuppTrTypeCode;
              if (lSuppTransType != 0)
                {
                   lTmp = m_objCache.GetRelatedCodeId(Convert.ToInt32(lSuppTransType));
                   if (IsReserveValid(objClaim, lTmp, 844, objClaim.ClaimTypeCode,m_iClientId))
                     lSuppResType = lTmp;

                   else
                   {
                     throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.FillNonOccDomFailed.ReserveTypeNotValid",m_iClientId)));
                   }
                }
                else
                {
                    lSuppTransType = p_iTransTypeCode;
                }

            }
            return bGetSuppInfo;
        }
		
        /// <summary>
		/// This method returns the parent account id for the selected sub account.
		/// </summary>
		/// <param name="p_iAccountId">Sub Account Id</param>		
		/// <returns>Parent Account Id</returns>
		private int GetParentAccountID(int p_iAccountId)
		{
			string			sSQL=string.Empty;
			DbReader		objReader=null;
			int				iAccountId=0;			
			try
			{
				sSQL = "SELECT ACCOUNT_ID FROM BANK_ACC_SUB WHERE SUB_ROW_ID=" + p_iAccountId;
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
				if (objReader.Read())
                    iAccountId = Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), m_iClientId);				
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(String.Format(Globalization.GetString("BankAccReconciliation.GetParentAccountID.Error",m_iClientId), p_iAccountId.ToString()), p_objEx);
			}
			finally
			{
				if(objReader != null)
				{
                    objReader.Close();
                    objReader.Dispose();
					
				}
				
			}
			return iAccountId;
		}
                
                private void FillEntities(	Claim objClaim,int p_iAccountId,int p_iTransTypeCode, 
									ref EntityData p_objEntityData,ref TaxRate p_dtpTaxes,
									DbConnection objDbConnection, DbTransaction objDbTransaction)
		{
			//dtpE ordinal positions: 0=Claimant,1=fed,2=ss,3=med,4=state
			p_objEntityData.ClaimId = objClaim.ClaimId;
			p_objEntityData.ClaimNumber = objClaim.ClaimNumber;
			p_objEntityData.ClaimantEid = objClaim.PrimaryClaimant.ClaimantEid;
    
			//bank account data needed for saving each payment
			if (objClaim.Context.InternalSettings.SysSettings.UseFundsSubAcc)
			{
				//if using sub accounts, lAccountId is the subaccount id, the bank account is the parent
				p_objEntityData.BankAccountId = GetParentAccountID(p_iAccountId,objDbConnection,objDbTransaction);
				p_objEntityData.SubBankAccountId = p_iAccountId;
			}
			else
			{
				//if not using sub accounts, lAccountId is the bank account, lSubAccountId is 0
				p_objEntityData.BankAccountId = p_iAccountId;
				p_objEntityData.SubBankAccountId = 0;
			}
			
			//payee type for Claimant
			int iPayeeType = objClaim.Context.InternalSettings.CacheFunctions.GetCodeIDWithShort("C", "PAYEE_TYPE");
			p_objEntityData.FN[(int)Entities.Clmnt] = objClaim.PrimaryClaimant.ClaimantEntity.FirstName;
			p_objEntityData.MN[(int)Entities.Clmnt] = objClaim.PrimaryClaimant.ClaimantEntity.MiddleName;
			p_objEntityData.LN[(int)Entities.Clmnt] = objClaim.PrimaryClaimant.ClaimantEntity.LastName;
			p_objEntityData.Addr1[(int)Entities.Clmnt] = objClaim.PrimaryClaimant.ClaimantEntity.Addr1;
			p_objEntityData.Addr2[(int)Entities.Clmnt] = objClaim.PrimaryClaimant.ClaimantEntity.Addr2;
            p_objEntityData.Addr3[(int)Entities.Clmnt] = objClaim.PrimaryClaimant.ClaimantEntity.Addr3;
            p_objEntityData.Addr4[(int)Entities.Clmnt] = objClaim.PrimaryClaimant.ClaimantEntity.Addr4;
			p_objEntityData.City[(int)Entities.Clmnt] = objClaim.PrimaryClaimant.ClaimantEntity.City;
			p_objEntityData.StateId[(int)Entities.Clmnt] = objClaim.PrimaryClaimant.ClaimantEntity.StateId;
			p_objEntityData.Zip[(int)Entities.Clmnt] = objClaim.PrimaryClaimant.ClaimantEntity.ZipCode;
			p_objEntityData.PayeeType[(int)Entities.Clmnt] = iPayeeType;
			p_objEntityData.ReserveTypeCode[(int)Entities.Clmnt] = objClaim.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(p_iTransTypeCode);
	    
			//payee type for all tax payments
			iPayeeType = objClaim.Context.InternalSettings.CacheFunctions.GetCodeIDWithShort("O", "PAYEE_TYPE");
  
			if(p_dtpTaxes.iFedTaxEID!=0 & p_dtpTaxes.bFed)
				FillTaxEntity(p_dtpTaxes.iFedTaxEID, p_dtpTaxes.iFedTransTypeCodeID, iPayeeType, Entities.Fed, objClaim, ref p_objEntityData,objDbConnection,objDbTransaction);
			if(p_dtpTaxes.iSSTaxEID!=0 & p_dtpTaxes.bSS)
				FillTaxEntity(p_dtpTaxes.iSSTaxEID, p_dtpTaxes.iSSTransTypeCodeID, iPayeeType, Entities.SS, objClaim, ref p_objEntityData,objDbConnection,objDbTransaction);
			if(p_dtpTaxes.iMedicareTaxEID!=0 & p_dtpTaxes.bMed)
				FillTaxEntity(p_dtpTaxes.iMedicareTaxEID, p_dtpTaxes.iMedicareTransTypeCodeID, iPayeeType, Entities.Med, objClaim, ref p_objEntityData,objDbConnection,objDbTransaction);
			if(p_dtpTaxes.iStateTaxEID!=0 & p_dtpTaxes.bState)
				FillTaxEntity(p_dtpTaxes.iStateTaxEID, p_dtpTaxes.iStateTransTypeCodeID, iPayeeType, Entities.State, objClaim, ref p_objEntityData,objDbConnection,objDbTransaction);

			//fix any single quotes
			for (int i=(int)Entities.Clmnt;i<=(int)Entities.State;i++)
			{
				p_objEntityData.FN[i] = p_objEntityData.FN[i].Replace("'", "''");
				p_objEntityData.MN[i] = p_objEntityData.MN[i].Replace("'", "''");
				p_objEntityData.LN[i] = p_objEntityData.LN[i].Replace("'", "''");
				p_objEntityData.Addr1[i] = p_objEntityData.Addr1[i].Replace("'", "''");
				p_objEntityData.Addr2[i] = p_objEntityData.Addr2[i].Replace("'", "''");
                p_objEntityData.Addr3[i] = p_objEntityData.Addr3[i].Replace("'", "''");
                p_objEntityData.Addr4[i] = p_objEntityData.Addr4[i].Replace("'", "''");
				p_objEntityData.City[i] = p_objEntityData.City[i].Replace("'", "''");
			}
		}


		private void FillTaxEntity(	int p_iPayeeId, int p_iTransTypeCode, int p_iPayeeType, Entities p_iEntity,
									Claim objClaim,ref EntityData p_objEntityData, DbConnection objDbConection,
									DbTransaction objDbTransaction)
		{
			string sSQL=string.Empty;
			DbReader objReader=null;

			p_objEntityData.ReserveTypeCode[(int)p_iEntity] = objClaim.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(p_iTransTypeCode);

            sSQL = "SELECT FIRST_NAME,MIDDLE_NAME,LAST_NAME,ADDR1,ADDR2,ADDR3,ADDR4,CITY,STATE_ID,ZIP_CODE FROM ENTITY WHERE ENTITY_ID = " + p_iPayeeId.ToString();

			try
			{
				objReader =objDbConection.ExecuteReader(sSQL,objDbTransaction);

				if(objReader.Read())
				{
					p_objEntityData.FN[(int)p_iEntity] = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
					p_objEntityData.MN[(int)p_iEntity] = Conversion.ConvertObjToStr(objReader.GetValue("MIDDLE_NAME"));
					p_objEntityData.LN[(int)p_iEntity] = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
					p_objEntityData.Addr1[(int)p_iEntity] = Conversion.ConvertObjToStr(objReader.GetValue("ADDR1"));
					p_objEntityData.Addr2[(int)p_iEntity] = Conversion.ConvertObjToStr(objReader.GetValue("ADDR2"));
                    p_objEntityData.Addr3[(int)p_iEntity] = Conversion.ConvertObjToStr(objReader.GetValue("ADDR3"));
                    p_objEntityData.Addr4[(int)p_iEntity] = Conversion.ConvertObjToStr(objReader.GetValue("ADDR4"));
					p_objEntityData.City[(int)p_iEntity] = Conversion.ConvertObjToStr(objReader.GetValue("CITY"));
                    p_objEntityData.StateId[(int)p_iEntity] = Conversion.ConvertObjToInt(objReader.GetValue("STATE_ID"), m_iClientId);
					p_objEntityData.Zip[(int)p_iEntity] = Conversion.ConvertObjToStr(objReader.GetValue("ZIP_CODE"));
					p_objEntityData.PayeeType[(int)p_iEntity] = p_iPayeeType;
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.FillTaxEntity.Error",m_iClientId), p_objEx);
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
			}
		}

        private bool IsReserveValid(Claim objClaim , long lReserveId , int p_iLOB , long lClaimType, int p_iClientId)
        {
            bool bIsReserveValid = false;
            ColLobSettings objColLobSettings = null;
            LobSettings.CColReserves objColReserves = null;

            objColLobSettings = new ColLobSettings(m_sConnectionString, p_iClientId);	
            objColReserves = objColLobSettings[p_iLOB].Reserves ;

            foreach (LobSettings.CReserve objReserve in objColReserves)
            {
                if (lReserveId == objReserve.ReserveTypeCode)
                {
                    //pmahli MITS 11124 1/24/2008
                    //'And' Condition changed to 'Or' as previously it was not going inside the code for both the condition
                    //whether reserve by claim type is true or it is false
                    if (objColLobSettings[p_iLOB].ResByClmType == false || objReserve.ClaimTypeCode == lClaimType)
                        bIsReserveValid = true;
                }
            }
            
            return bIsReserveValid;
        
        }

		private int GetParentAccountID(int p_iAccountId,DbConnection objDbConnection,DbTransaction objDbTransaction)
		{
			string sSQL=string.Empty;
			DbReader objReader=null;
			int iAccountId=0;

			sSQL = "SELECT ACCOUNT_ID FROM BANK_ACC_SUB WHERE SUB_ROW_ID=" + p_iAccountId.ToString();

			try
			{
				objReader = objDbConnection.ExecuteReader(sSQL,objDbTransaction);
				if (objReader.Read())
                    iAccountId = Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), m_iClientId);
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(String.Format(Globalization.GetString("NonOccPaymentsManager.GetParentAccountID.Error",m_iClientId), p_iAccountId.ToString()), p_objEx);
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
			}
			return iAccountId;
		}


		private string FundsAutoSQL(int p_iPayeeEid, int p_iBatchId,int p_iFundsAutoTransID, 
									int p_iWeekCode,Entities p_iEntity, double p_dblAmount, 
									int p_iPaymentNumber,DateTime p_dPrintDate, int p_iWorkDaysCount, 
									bool p_bUseVouchers, EntityData p_dtpE)
		{
			int iTaxPaymentFlag = 0;
			int iThirdPartyFlag = 0;
			string sAppend = string.Empty;
			string sAmount=string.Empty;
			string sPrintDate=string.Empty;
			StringBuilder sSQL=new StringBuilder();
			int iVoucherFlag=0;
			string sDTTM=Conversion.ToDbDateTime(DateTime.Now);

			if (p_iEntity==Entities.Clmnt)	//payment to Claimant, Tax & ThirdParty flags are false
			{
                iVoucherFlag = Conversion.ConvertBoolToInt(p_bUseVouchers, m_iClientId);   //Plan.Class.bUseVouchers
				iTaxPaymentFlag = 0;
				iThirdPartyFlag = 0;
				sAppend = "";
			}
			else
			{
				iVoucherFlag = -1;   //Plan.Class.bUseVouchers
				iTaxPaymentFlag = -1;
				iThirdPartyFlag = -1;
				sAppend = "T-" + p_iEntity.ToString();
			}

			sPrintDate = Conversion.ToDbDate(p_dPrintDate);
			sAmount = string.Format("{0:0.00}",p_dblAmount);

			sSQL.Append("INSERT INTO FUNDS_AUTO (AUTO_BATCH_ID, AUTO_TRANS_ID, CLAIM_ID, CLAIMANT_EID, ");
            sSQL.Append("FIRST_NAME, LAST_NAME, ADDR1, ADDR2, ADDR3, ADDR4, CITY, STATE_ID, ZIP_CODE, ");
			sSQL.Append("PAYEE_EID, PAYEE_TYPE_CODE, CTL_NUMBER, AMOUNT, ACCOUNT_ID, PRINT_DATE, ");
			sSQL.Append("DTTM_RCD_ADDED, DTTM_RCD_LAST_UPD, ADDED_BY_USER, UPDATED_BY_USER, ");
			sSQL.Append("THIRD_PARTY_FLAG, PRECHECK_FLAG, CHECK_BATCH_NUM, CLAIM_NUMBER, PAY_NUMBER, ");
			sSQL.Append("END_PAY_NUMBER, STATUS_CODE, SUB_ACC_ID, MIDDLE_NAME, VOUCHER_FLAG, ");
			sSQL.Append("NUM_OF_PAID_DAYS, WEEKS_PAID_CODE, TAX_PAYMENT_FLAG, UNIT_ID) ");
			sSQL.Append("VALUES ");
			sSQL.Append("(" + p_iBatchId.ToString() + ", " + p_iFundsAutoTransID.ToString() + ", " + p_dtpE.ClaimId.ToString() + ", ");
			sSQL.Append(p_dtpE.ClaimantEid.ToString() + ", '" + p_dtpE.FN[(int)p_iEntity] + "', '" + p_dtpE.LN[(int)p_iEntity]);
            sSQL.Append("', '" + p_dtpE.Addr1[(int)p_iEntity] + "', '" + p_dtpE.Addr2[(int)p_iEntity] + "', '" + p_dtpE.Addr3[(int)p_iEntity] + "', '" + p_dtpE.Addr4[(int)p_iEntity] + "', '" + p_dtpE.City[(int)p_iEntity] + "', " + p_dtpE.StateId[(int)p_iEntity].ToString());
			sSQL.Append(", '" + p_dtpE.Zip[(int)p_iEntity] + "', " + p_iPayeeEid.ToString() + ", " + p_dtpE.PayeeType[(int)p_iEntity].ToString() + ", 'DIS-" + string.Format("{0000000}",p_iBatchId) + sAppend + "', ");
			sSQL.Append(sAmount + ", " + p_dtpE.BankAccountId.ToString() + ", '" + sPrintDate + "', '" + sDTTM + "', '");
			sSQL.Append(sDTTM + "', '" + m_sUserName + "', '" + m_sUserName + "', ");
			sSQL.Append(iThirdPartyFlag.ToString() + ", 0, 0, '" + p_dtpE.ClaimNumber + "', " + p_iPaymentNumber.ToString() + ", 0, ");
			sSQL.Append("0, " + p_dtpE.SubBankAccountId.ToString() + ", '" + p_dtpE.MN[(int)p_iEntity] + "', " + iVoucherFlag.ToString() + ", ");
			sSQL.Append(" " + p_iWorkDaysCount.ToString() + ", " + p_iWeekCode.ToString() + ", " + iTaxPaymentFlag.ToString() + ",0)");

			return sSQL.ToString();
		}
   

		private string GetSplitSQL(	int p_iAutoSplitId,int p_iFundsAutoTransID,int p_iTransTypeCode,
									double p_dblAmount, DateTime p_dFrom, DateTime p_dTo, Entities p_iEntity,
									EntityData p_dtpE)
		{
            StringBuilder sSQL = new StringBuilder();
			string sDTTM=Conversion.ToDbDateTime(DateTime.Now);
			string sFromDate=string.Empty;
			string sToDate=string.Empty;
			string sAmount=string.Empty;
			string sTag=string.Empty;

			sFromDate = Conversion.ToDbDate(p_dFrom);
			sToDate = Conversion.ToDbDate(p_dTo);
			sAmount = string.Format("{0:0.00}",p_dblAmount);
			sTag = ((int)p_iEntity).ToString();

			sSQL.Append("INSERT INTO FUNDS_AUTO_SPLIT (AUTO_SPLIT_ID, AUTO_TRANS_ID, TRANS_TYPE_CODE, RESERVE_TYPE_CODE, ");
			sSQL.Append("AMOUNT, GL_ACCOUNT_CODE, FROM_DATE, TO_DATE, DTTM_RCD_ADDED, DTTM_RCD_LAST_UPD, ");
			sSQL.Append("ADDED_BY_USER, UPDATED_BY_USER, TAG) ");
			sSQL.Append("VALUES ");
			sSQL.Append("(" + p_iAutoSplitId.ToString() + ", " + p_iFundsAutoTransID.ToString() + ", " + p_iTransTypeCode.ToString() + ", ");
			sSQL.Append(p_dtpE.ReserveTypeCode[(int)p_iEntity] + ", " + sAmount + ", 0, " + sFromDate + ", " + sToDate + ", ");
			sSQL.Append(sDTTM + ", " + sDTTM + ", '" + m_sUserName + "', '" + m_sUserName + "', ");
			sSQL.Append(sTag + ")");

			return sSQL.ToString();
		}
	

		private string AppendTransactionTypeCombo(Claim p_objClaim,ref XmlDocument p_objReturnXML)
		{
			StringBuilder sSQL=new StringBuilder();
			XmlCDataSection objCData=null;
			DbReader objReader=null;
			XmlElement xmlOption=null;
			XmlAttribute xmlOptionAttrib=null;
			XmlElement objNew=null;
			string sReserveType=string.Empty;
			XmlElement objDocumentElement=p_objReturnXML.DocumentElement;
			int iCodeId=0;
			string sShortCode=string.Empty;
			string sCodeDesc=string.Empty;
			DbConnection objConn=null;
			string sDBType="";
          
            string sShortCodeStatus = string.Empty;
            string sReserveStatus = string.Empty;//skhare7 27920
            object oReserveStatus = null;//skhare7
			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				sDBType = objConn.DatabaseType.ToString();
				objConn.Dispose();
				if (sDBType==Constants.DB_ORACLE)
				{
					sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE || '-' || CODES_TEXT.CODE_DESC");
				}
				else
				{
					sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE + ' - ' + CODES_TEXT.CODE_DESC");
				}
				sSQL.Append(" FROM CODES INNER JOIN CODES_TEXT");
				sSQL.Append(" ON CODES.CODE_ID = CODES_TEXT.CODE_ID");
                sSQL.Append(" INNER JOIN GLOSSARY");
				sSQL.Append(" ON CODES.TABLE_ID = GLOSSARY.TABLE_ID");
                sSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'TRANS_TYPES'");
				sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)");
				sSQL.Append(" AND CODES.RELATED_CODE_ID IN (SELECT SYS_LOB_RESERVES.RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES,CODES WHERE SYS_LOB_RESERVES.LINE_OF_BUS_CODE = 844 AND SYS_LOB_RESERVES.RESERVE_TYPE_CODE = CODES.CODE_ID AND CODES.RELATED_CODE_ID = 1066)");
				sSQL.Append(" AND CODES.CODE_ID NOT IN (SELECT TRANS_TYPE_CODE_ID FROM TAX_MAPPING)");

				objReader=DbFactory.GetDbReader(m_sConnectionString,sSQL.ToString());
				objNew = p_objReturnXML.CreateElement("TransactionType");

				objNew.SetAttribute("codeid","");

				//create an empty node
				xmlOption = p_objReturnXML.CreateElement("option");
				xmlOption.InnerText="";
				xmlOptionAttrib = p_objReturnXML.CreateAttribute("value");
				xmlOptionAttrib.Value = "0";
				xmlOption.Attributes.Append(xmlOptionAttrib);
				xmlOptionAttrib = p_objReturnXML.CreateAttribute("reservetype");
				xmlOptionAttrib.Value = "0";
				xmlOption.Attributes.Append(xmlOptionAttrib);
				objNew.AppendChild(xmlOption);

				if (objReader!=null)
				{
					//Loop through and create all the option values for the combobox control
					while(objReader.Read())
					{
						xmlOption = p_objReturnXML.CreateElement("option");
						objCData = p_objReturnXML.CreateCDataSection(Conversion.ConvertObjToStr(objReader[1]));
						xmlOption.AppendChild(objCData);
						xmlOptionAttrib = p_objReturnXML.CreateAttribute("value");
                        xmlOptionAttrib.Value = Conversion.ConvertObjToInt(objReader[0], m_iClientId).ToString();
						xmlOption.Attributes.Append(xmlOptionAttrib);
						objNew.AppendChild(xmlOption);
					
						//get reserve type for each transaction.  reservetype label set in nonocc.js setReserveType() onchange event added in above line
                        iCodeId = p_objClaim.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(Conversion.ConvertObjToInt(objReader[0], m_iClientId));
						p_objClaim.Context.InternalSettings.CacheFunctions.GetCodeInfo(iCodeId,ref sShortCode, ref sCodeDesc);

						xmlOptionAttrib = p_objReturnXML.CreateAttribute("reservetype");
                        xmlOptionAttrib.Value = sCodeDesc;
						//sReserveType=sReserveType + sCodeDesc + ",";
						xmlOption.Attributes.Append(xmlOptionAttrib);//skhare 27920
                        oReserveStatus = DbFactory.ExecuteScalar(this.m_sConnectionString, "SELECT RES_STATUS_CODE FROM RESERVE_CURRENT WHERE CLAIM_ID=" +p_objClaim.ClaimId + " AND RESERVE_TYPE_CODE=" + iCodeId);
                        if (oReserveStatus != null && oReserveStatus != string.Empty)
                        {
                            oReserveStatus = Conversion.ConvertObjToStr(oReserveStatus);
                            sShortCodeStatus = p_objClaim.Context.InternalSettings.CacheFunctions.GetShortCode(Conversion.ConvertObjToInt(oReserveStatus, m_iClientId));

                            sReserveType = sReserveType + sCodeDesc + "_" + sShortCodeStatus + ",";
                        }

					}//end foreach
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.AppendTransactionTypeCombo.Error",m_iClientId), p_objEx);
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if(oReserveStatus!=null)
                oReserveStatus = null;
			}

			objDocumentElement.AppendChild(objNew);

			sReserveType="0"+ "," + sReserveType.Trim(new char[]{','});

			return sReserveType;
		}


		private void AppendBankAccountList(Claim p_objClaim,ref XmlDocument p_objReturnXML)
		{
			//Variable declarations
			ArrayList arrAcctList = new ArrayList();
			XmlElement objNew=null;
			XmlCDataSection objCData=null;
			XmlElement objDocumentElement=p_objReturnXML.DocumentElement;

			//Retrieve the security credential information to pass to the FundManager object
			string sDSNName = m_sDsnName;
			string sUserName = m_sUserName;
			string sPassword = m_sPassword;
			int iUserId = m_objDataModelFactory.Context.RMUser.UserId;
			int iGroupId = m_objDataModelFactory.Context.RMUser.GroupId;

            using (FundManager objFundMgr = new FundManager(sDSNName, sUserName, sPassword, iUserId, iGroupId, m_iClientId))
            {
                arrAcctList = objFundMgr.GetAccountsEx(true, true, 0);
            }

			//Create the necessary SysExData to be used in ref binding
			objNew = p_objReturnXML.CreateElement("AccountList");

			if (p_objClaim.Context.InternalSettings.SysSettings.UseFundsSubAcc)
				objNew.SetAttribute("codeid",p_objClaim.DisabilityPlan.SubAccRowId.ToString());
			else
				objNew.SetAttribute("codeid",p_objClaim.DisabilityPlan.BankAccId.ToString());
			
			//Loop through and create all the option values for the combobox control
			foreach (FundManager.AccountDetail item in arrAcctList)
			{
				XmlElement xmlOption = p_objReturnXML.CreateElement("option");
				objCData = p_objReturnXML.CreateCDataSection(item.AccountName);
				xmlOption.AppendChild(objCData);
				XmlAttribute xmlOptionAttrib = p_objReturnXML.CreateAttribute("value");
				xmlOptionAttrib.Value = item.AccountId.ToString();
				xmlOption.Attributes.Append(xmlOptionAttrib);
				objNew.AppendChild(xmlOption);
			}//end foreach

			//Add the XML to the SysExData 
			objDocumentElement.AppendChild(objNew);

			//Clean up
			arrAcctList = null;
			objNew = null;
			objCData = null;
		}


		private void InitCollectionScreen(	int p_iClaimId,ref XmlDocument objReturnXML, ref Claim objClaim,
											ref DisabilityClass objDisClass,ref bool[] bTaxes, 
											ref TaxRate dtpTaxes, bool bFillCombo)
		{
			XmlElement objDocumentElement=null;
			XmlElement objElem=null;
			objDocumentElement=objReturnXML.CreateElement("Collection");
			objReturnXML.AppendChild(objDocumentElement);
			
			int iLOB=0;
			LobSettings objLobSettings = null;

			objClaim=m_objDataModelFactory.GetDataModelObject("Claim",false) as Claim;

			iLOB = objClaim.Context.LocalCache.GetCodeId("DI", "LINE_OF_BUSINESS");
			objLobSettings = objClaim.Context.InternalSettings.ColLobSettings[iLOB];

			objClaim.LineOfBusCode = iLOB;
			objClaim.NavType = Riskmaster.DataModel.ClaimNavType.ClaimNavLOB;

			objClaim.MoveTo(p_iClaimId);

			foreach(DisabilityClass objClass in objClaim.DisabilityPlan.ClassList)
			{
				if (objClass.ClassRowId==objClaim.ClassRowId) 
				{
					objDisClass=objClass;
					break;
				}
			}

			objElem=objReturnXML.CreateElement("ClaimId");
			objElem.InnerText=objClaim.ClaimId.ToString();
			objDocumentElement.AppendChild(objElem);

			objElem=objReturnXML.CreateElement("ClaimNumber");
			objElem.InnerText=objClaim.ClaimNumber;
			objDocumentElement.AppendChild(objElem);

			if (bFillCombo)
			{
				AppendBankAccountList(objClaim, ref objReturnXML);
				
				objElem=objReturnXML.CreateElement("ReserveTypeArray");
				objElem.InnerText=AppendTransactionTypeCombo(objClaim,ref objReturnXML);
				objDocumentElement.AppendChild(objElem);
			}
            //MITS 12902 : Umesh
			//GetTaxes(objClaim.PrimaryClaimant.ClaimantEntity.StateId, ref bTaxes, ref dtpTaxes);
            GetTaxes(objClaim, ref bTaxes, ref dtpTaxes);
			
			int iTaxFlags = objClaim.TaxFlags;
			if (iTaxFlags==0)	//mwc only set flags if they have not been previously saved.  1 means has been saved
			{
				dtpTaxes.bFed = objDisClass.WithholdFedItax;
				dtpTaxes.bSS = objDisClass.WithholdFica;
				dtpTaxes.bMed = objDisClass.WithholdMedicare;
				dtpTaxes.bState = objDisClass.WithholdState;		
			}
			else
			{
				//objClaim.taxflags is bitmask: fed = 2,ss = 4,medicare = 8,state = 16
				dtpTaxes.bFed = ((2 | iTaxFlags)==iTaxFlags);
				dtpTaxes.bSS = ((4 | iTaxFlags)==iTaxFlags);
				dtpTaxes.bMed = ((8 | iTaxFlags)==iTaxFlags);
				dtpTaxes.bState = ((16 | iTaxFlags)==iTaxFlags);
			}
			//Rahul: Commenting line below as per RMWorld. In RMWorld in STDCollection screen
			//			dblTaxablePercentis not set.
			//dtpTaxes.dblTaxablePercent = objDisClass.TaxablePercent/100;

			objElem=objReturnXML.CreateElement("TaxablePercent");
			objElem.InnerText=dtpTaxes.dblTaxablePercent.ToString();
			objDocumentElement.AppendChild(objElem);
			objElem=objReturnXML.CreateElement("FED_TAX_RATE");
			objElem.InnerText=dtpTaxes.FED_TAX_RATE.ToString();
			objDocumentElement.AppendChild(objElem);
			objElem=objReturnXML.CreateElement("SS_TAX_RATE");
			objElem.InnerText=dtpTaxes.SS_TAX_RATE.ToString();
			objDocumentElement.AppendChild(objElem);
			objElem=objReturnXML.CreateElement("MEDICARE_TAX_RATE");
			objElem.InnerText=dtpTaxes.MEDICARE_TAX_RATE.ToString();
			objDocumentElement.AppendChild(objElem);
			objElem=objReturnXML.CreateElement("STATE_TAX_RATE");
			objElem.InnerText=dtpTaxes.STATE_TAX_RATE.ToString();
			objDocumentElement.AppendChild(objElem);
		}


		private void SaveThisCollection(double dblAmount, int iPayeeEid, int iTransTypeCode, int iTaxPayment, 
										Claim objClaim, int iBankAccountID, DisabilityClass objDisClass,XmlDocument p_objXmlIn,
										TaxRate dtpTaxes,int iSubBankAccountID)
		{

			int iClaimantEid=objClaim.PrimaryClaimant.ClaimantEid;
			bool bFed=Conversion.ConvertStrToBool(p_objXmlIn.SelectSingleNode("//Fed").InnerText);
			bool bSS=Conversion.ConvertStrToBool(p_objXmlIn.SelectSingleNode("//SS").InnerText);
			bool bMed=Conversion.ConvertStrToBool(p_objXmlIn.SelectSingleNode("//Med").InnerText);
			bool bState=Conversion.ConvertStrToBool(p_objXmlIn.SelectSingleNode("//State").InnerText);
			double dblFedTax=Conversion.ConvertStrToDouble(p_objXmlIn.SelectSingleNode("//FedAmount").InnerText);
			double dblSSTax=Conversion.ConvertStrToDouble(p_objXmlIn.SelectSingleNode("//SSAmount").InnerText);
			double dblMedTax=Conversion.ConvertStrToDouble(p_objXmlIn.SelectSingleNode("//MedAmount").InnerText);
			double dblStateTax=Conversion.ConvertStrToDouble(p_objXmlIn.SelectSingleNode("//StateAmount").InnerText);
			int iFedTransTypeCodeID=dtpTaxes.iFedTransTypeCodeID;
			int iSSTransTypeCodeID=dtpTaxes.iSSTransTypeCodeID;
			int iMedicareTransTypeCodeID=dtpTaxes.iMedicareTransTypeCodeID;
			int iStateTransTypeCodeID=dtpTaxes.iStateTransTypeCodeID;
			double dblGross=Conversion.ConvertStrToDouble(p_objXmlIn.SelectSingleNode("//Amount").InnerText);
			string sFrom=p_objXmlIn.SelectSingleNode("//StartDate").InnerText;
			string sTo=p_objXmlIn.SelectSingleNode("//EndDate").InnerText;
			
			int iPayeeType=0;
			string sSQL=string.Empty;
			DbReader objReader=null;
			string sFirstName=string.Empty;
			string sLastName=string.Empty;
			string sMiddleName=string.Empty;
			string sAddr1=string.Empty;
			string sAddr2=string.Empty;
            string sAddr3 = string.Empty;
            string sAddr4 = string.Empty;
			string sCity=string.Empty;
			int iStateID=0;
			string sZipCode=string.Empty;
			double dblThisAmount=dblAmount;
			Funds objFunds=null;
			FundsTransSplit objTS=null;

			if (iTaxPayment != 0)
				iPayeeType = objClaim.Context.InternalSettings.CacheFunctions.GetCodeIDWithShort("O", "PAYEE_TYPE");
			else
				iPayeeType = objClaim.Context.InternalSettings.CacheFunctions.GetCodeIDWithShort("C", "PAYEE_TYPE");

            sSQL = "SELECT FIRST_NAME,LAST_NAME,MIDDLE_NAME,ADDR1,ADDR2,ADDR3,ADDR4,CITY,ZIP_CODE,STATE_ID FROM ENTITY WHERE ENTITY_ID = " + iPayeeEid;
			try
			{
				objReader=DbFactory.GetDbReader(m_sConnectionString,sSQL);
				if (objReader.Read())
				{
					sFirstName=Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Replace("'","''");
					sLastName=Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")).Replace("'","''");
					sMiddleName=Conversion.ConvertObjToStr(objReader.GetValue("MIDDLE_NAME")).Replace("'","''");
					sAddr1=Conversion.ConvertObjToStr(objReader.GetValue("ADDR1")).Replace("'","''");
					sAddr2=Conversion.ConvertObjToStr(objReader.GetValue("ADDR2")).Replace("'","''");
                    sAddr3 = Conversion.ConvertObjToStr(objReader.GetValue("ADDR3")).Replace("'", "''");
                    sAddr4 = Conversion.ConvertObjToStr(objReader.GetValue("ADDR4")).Replace("'", "''");
					sCity=Conversion.ConvertObjToStr(objReader.GetValue("CITY")).Replace("'","''");
					sZipCode=Conversion.ConvertObjToStr(objReader.GetValue("ZIP_CODE"));
                    iStateID = Conversion.ConvertObjToInt(objReader.GetValue("STATE_ID"), m_iClientId);
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.SaveThisCollection.Error",m_iClientId), p_objEx);
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
			}
			
			objFunds=m_objDataModelFactory.GetDataModelObject("Funds",false) as Funds;

			objFunds.AccountId = iBankAccountID;
			objFunds.Addr1 = sAddr1;
			objFunds.Addr2 = sAddr2;
            objFunds.Addr3 = sAddr3;
            objFunds.Addr4 = sAddr4;
			objFunds.Amount = dblAmount;
			objFunds.CheckMemo = "";
			objFunds.City = sCity;
			objFunds.ClaimantEid = iClaimantEid;
			objFunds.ClaimId = objClaim.ClaimId;
			objFunds.ClaimNumber = objClaim.ClaimNumber;
			objFunds.CollectionFlag = true;
			objFunds.DateOfCheck = Conversion.ToDbDate(DateTime.Now);
			objFunds.FirstName = sFirstName;
			objFunds.LastName = sLastName;
			objFunds.PayeeEid = iPayeeEid;
			objFunds.PayeeTypeCode = iPayeeType;
			objFunds.PaymentFlag = false;
			objFunds.StateId = iStateID;
			objFunds.StatusCode = 479;
			objFunds.SubAccountId = iSubBankAccountID;
			if (iTaxPayment==0)
				objFunds.TaxPaymentFlag = false ;
			else
				objFunds.TaxPaymentFlag = true ;
			objFunds.TransDate = Conversion.ToDbDate(DateTime.Now);
			objFunds.UnitId = 0;
			objFunds.VoidFlag = false;
			objFunds.ZipCode = sZipCode;
			if (iTaxPayment!=0)
				objFunds.VoucherFlag=true;
			else
				objFunds.VoucherFlag=objDisClass.UseVouchers;

			if (iTaxPayment == 0)
			{
				objTS=objFunds.TransSplitList.AddNew();
				dblThisAmount=dblGross;

				objTS.TransTypeCode = iTransTypeCode;
				objTS.Amount = dblThisAmount;
				objTS.SumAmount = (dblThisAmount * -1);
				objTS.InvoiceAmount = dblThisAmount;
				objTS.GlAccountCode = 0;
				objTS.FromDate = sFrom;
				objTS.ToDate = sTo;
				
				if (bFed)
				{
					objTS=objFunds.TransSplitList.AddNew();
					dblThisAmount=dblFedTax;

					objTS.TransTypeCode = iFedTransTypeCodeID;
					objTS.Amount = (dblThisAmount * -1);
					objTS.SumAmount = dblThisAmount;
					objTS.InvoiceAmount = (dblThisAmount * -1);
					objTS.GlAccountCode = 0;
					objTS.FromDate = sFrom;
					objTS.ToDate = sTo;
				}
				if (bSS)
				{
					objTS=objFunds.TransSplitList.AddNew();
					dblThisAmount=dblSSTax;

					objTS.TransTypeCode = iSSTransTypeCodeID;
					objTS.Amount = (dblThisAmount * -1);
					objTS.SumAmount = dblThisAmount;
					objTS.InvoiceAmount = (dblThisAmount * -1);
					objTS.GlAccountCode = 0;
					objTS.FromDate = sFrom;
					objTS.ToDate = sTo;
				}
				if (bMed)
				{
					objTS=objFunds.TransSplitList.AddNew();
					dblThisAmount=dblMedTax;

					objTS.TransTypeCode = iMedicareTransTypeCodeID;
					objTS.Amount = (dblThisAmount * -1);
					objTS.SumAmount = dblThisAmount;
					objTS.InvoiceAmount = (dblThisAmount * -1);
					objTS.GlAccountCode = 0;
					objTS.FromDate = sFrom;
					objTS.ToDate = sTo;
				}
				if (bState)
				{
					objTS=objFunds.TransSplitList.AddNew();
					dblThisAmount=dblStateTax;

					objTS.TransTypeCode = iStateTransTypeCodeID;
					objTS.Amount = (dblThisAmount * -1);
					objTS.SumAmount = dblThisAmount;
					objTS.InvoiceAmount = (dblThisAmount * -1);
					objTS.GlAccountCode = 0;
					objTS.FromDate = sFrom;
					objTS.ToDate = sTo;
				}
			}
			else
			{
				objTS=objFunds.TransSplitList.AddNew();

				objTS.TransTypeCode = iTransTypeCode;
				objTS.Amount = dblAmount;
				objTS.SumAmount = (dblAmount * -1);
				objTS.InvoiceAmount = dblAmount;
				objTS.GlAccountCode = 0;
				objTS.FromDate = sFrom;
				objTS.ToDate = sTo;
			}

			objFunds.Save();
		}
        private DateTime GetNextPrintDate(Claim objClaim , DisabilityPlan objPlan , DisabilityClass objClass , string p_sBenCalcPayStart , string p_sBenCalcPayTo , int iPrefSched ,int p_iTypeOfMonthlyPay,int p_iPrefDate, bool bGetCurrentPrintDate)
        {
          
    
            DateTime dThisPayStartDate = DateTime.MinValue;
            DateTime dThisPayEndDate = DateTime.MinValue;
            DateTime dCheckDate = DateTime.MinValue;
            DateTime dThisPrintDate = DateTime.MinValue;
            DateTime dPrintDate = DateTime.MinValue;
            DateTime dGetNextPrintDate = DateTime.MinValue;
            string sShortCode = "";
            string sCodeDesc = "";
            int iTmp = 0;
            int i = 0;
            bool bFoundDate = false;
            DateTime dThisDate = DateTime.MinValue;
            int iOrdinal = 0;
            int iWeekDay = 0;
            DateTime dOrdinalDate = DateTime.MinValue;
    
            if(bGetCurrentPrintDate)
               dThisDate = DateTime.Now.Date;
            else
               dThisDate = Conversion.ToDate(p_sBenCalcPayStart);

            bFoundDate = false;
        
            //check for a Pay Plan Start Date on the Plan
            if(objPlan.StartPayPeriod != "")
                dThisPayStartDate = Conversion.ToDate(objPlan.StartPayPeriod);
           
            if(dThisPayStartDate != DateTime.MinValue)
            {
                if(objPlan.PrefPaySchCode == 1737) // Bi-weekly
                {
                    iTmp = 14;
                    dThisPayEndDate = dThisPayStartDate.AddDays(iTmp - 1);
                }
                else if(iPrefSched == 1)
                {
                    iTmp = 7;
                    dThisPayEndDate = dThisPayStartDate.AddDays(iTmp - 1);
                }
                else if(iPrefSched == 3)
                    dThisPayEndDate = dThisPayStartDate.AddMonths(1).AddDays(-1);
            
                while(!bFoundDate)
                {
                    if(dThisPayStartDate <= dThisDate)
                    {
                        if(dThisPayStartDate <= dThisDate && dThisPayEndDate >= dThisDate)
                            bFoundDate = true;
                        else if(iPrefSched == 3) //monthly kdb 04/05/2005
                        {
                            dThisPayStartDate = dThisPayEndDate.AddDays(1);
                            dThisPayEndDate = dThisPayStartDate.AddMonths(1).AddDays(-1);
                        }
                        else if(iPrefSched == 1 || iPrefSched == 2) //weekly/bi-weekly
                        {
                            dThisPayStartDate = dThisPayStartDate.AddDays(iTmp);
                            dThisPayEndDate = dThisPayEndDate.AddDays(iTmp);
                        }
                   }
                
                   else if(dThisPayStartDate < dThisDate && dThisPayEndDate > dThisDate)
                            bFoundDate = true;
                   else if(iPrefSched == 3) //monthly kdb 04/05/2005
                   {
                            dThisPayStartDate = dThisPayStartDate.AddMonths(-1);
                            dThisPayEndDate = dThisPayStartDate.AddMonths(1).AddDays(-1);
                   }
                   else if(iPrefSched == 1 || iPrefSched == 2) //weekly/bi-weekly
                   {
                            dThisPayStartDate = dThisPayStartDate.AddDays(iTmp * -1);
                            dThisPayEndDate = dThisPayEndDate.AddDays(iTmp * -1);
                   }
                };
            }
            else
            {
                //'dCheckDate = DateTime.Now.Date;
                //if (dCheckDate < Conversion.ToDate(p_sBenCalcPayStart))
                    dCheckDate = Conversion.ToDate(p_sBenCalcPayStart);

                
                for (i = 1; i <= 7; i++)
                {
                    //abisht MITS 10780
                    //Wrong comparision was made to monday instead of sunday
                    //Changed that to sunday.
                    //if (((int)dCheckDate.DayOfWeek) != 1)
                    if (dCheckDate.DayOfWeek != DayOfWeek.Sunday)
                    {
                        dCheckDate = dCheckDate.AddDays(-1);
                    }
                    else
                    {
                        dThisPayStartDate = dCheckDate;
                        continue;
                    }
                }
                if (objPlan.PrefPaySchCode == 1737)
                {
                    iTmp = 13;
                    //Pawan  MITS:12620  06/30/08
                    //dThisPayEndDate = dThisPayStartDate.AddDays(iTmp - 1);
                    dThisPayEndDate = dThisPayStartDate.AddDays(iTmp);
                }
                else if (iPrefSched == 1)
                {
                    iTmp = 6;
                    //Pawan  MITS:12620  06/30/08
                    //dThisPayEndDate = dThisPayStartDate.AddDays(iTmp - 1);
                    dThisPayEndDate = dThisPayStartDate.AddDays(iTmp);              
                }
                else
                {
                    iTmp = DaysInMonth(dThisPayStartDate);
                    DateTime dDate = new DateTime(dThisPayStartDate.Year, dThisPayStartDate.Month, iTmp);
                    dThisPayEndDate = dDate;
                }
            }
           
       
            int iDay = 0;
            dThisPrintDate = dThisPayStartDate;
            if (iPrefSched == 3) // kdb 04/05/2005 Monthly
            {
                if (p_iTypeOfMonthlyPay != 1) //in case of day based
                {
                m_objCache.GetCodeInfo(objPlan.PrefDayOfMCode, ref sShortCode, ref sCodeDesc);
                iOrdinal = Conversion.ConvertStrToInteger(sShortCode);
                m_objCache.GetCodeInfo(objPlan.PrefDayCode, ref sShortCode, ref sCodeDesc);
                dOrdinalDate = GetOrdinalDate(dThisPrintDate.Year, dThisPrintDate.Month, iOrdinal, GetDayNumber(sCodeDesc));
                dPrintDate = dOrdinalDate;
                }
                else // in case of calender date based.
                {
                    if(p_iPrefDate <= dPrintDate.AddMonths(1).AddDays(-1).Day)
                        dPrintDate = new DateTime(dPrintDate.Year, dPrintDate.Month, p_iPrefDate);
                    else
                        dPrintDate = new DateTime(dPrintDate.Year, dPrintDate.Month, dPrintDate.AddMonths(1).AddDays(-1).Day);

                }
            }
            //Pawan  MITS:12620  06/30/08
            //else if (iPrefSched == 1 || iPrefSched == 2)
            else if (iPrefSched == 1)
            {
                m_objCache.GetCodeInfo(objPlan.PrefDayCode, ref sShortCode, ref sCodeDesc);
                for (i = 1; i <= 7; i++)
                {
                    if (dThisPrintDate.DayOfWeek.ToString().ToLower() == sCodeDesc.ToLower())
                    {
                        dPrintDate = dThisPrintDate;
                        continue;
                    }
                    else
                        //abisht MITS 10780
                        //dThisPrintDate.AddDays(-1);
                        dThisPrintDate = dThisPrintDate.AddDays(1);
                }
                dPrintDate = dThisPrintDate;
            }
            //Pawan  MITS:12620  06/30/08
            else if (iPrefSched == 2)
            {
                dThisPrintDate = dThisPayEndDate;
                m_objCache.GetCodeInfo(objPlan.PrefDayCode, ref sShortCode, ref sCodeDesc);
                for (i = 1; i <= 7; i++)
                {
                    if (dThisPrintDate.DayOfWeek.ToString().ToLower() == sCodeDesc.ToLower())
                    {
                        dPrintDate = dThisPrintDate;
                        continue;
                    }
                    else
                    {
                        if (objPlan.PrintBeforeEnd)
                            dThisPrintDate = dThisPrintDate.AddDays(-1);
                        else
                            dThisPrintDate = dThisPrintDate.AddDays(1);
                    }
                }
                dPrintDate = dThisPrintDate;
            }//End

//Mits 10780 change done by pawan, Updated few lines below arraged it correctly
            dThisPrintDate = dThisPayStartDate;
            if (objPlan.PrintBeforeEnd)
            {
                while (dPrintDate < dThisPayStartDate)
                {
                    if (iPrefSched == 2)
                    {
                        dPrintDate = dPrintDate.AddDays(14);
                    }
                    else if (iPrefSched == 3)
                    {
                        //dPrintDate = dPrintDate.AddDays(28);
                        //MITS 13135 : Umesh 09/04/08
                        if (p_iTypeOfMonthlyPay != 1) //in case of day based
                        {
                        dPrintDate = GetOrdinalDate(dThisPrintDate.Year, dThisPrintDate.Month, iOrdinal, GetDayNumber(sCodeDesc));
                        }
                        else
                        {
                            if (p_iPrefDate <= dThisPrintDate.AddMonths(1).AddDays(-1).Day)
                                dPrintDate = new DateTime(dThisPrintDate.Year, dThisPrintDate.Month, p_iPrefDate);
                            else
                                dPrintDate = new DateTime(dThisPrintDate.Year, dThisPrintDate.Month, dThisPrintDate.AddMonths(1).AddDays(-1).Day);
                        }
                        dThisPrintDate = dThisPrintDate.AddMonths(1);
                    }

                    else
                    {
                        dPrintDate = dPrintDate.AddDays(7);
                    }

                }
                while (dPrintDate < DateTime.Now.Date)
                {
                    if (iPrefSched == 2)
                    {
                        dPrintDate = dPrintDate.AddDays(14);
                    }
                    else if (iPrefSched == 3)
                    {
                        //dPrintDate = dPrintDate.AddDays(28);
                        //MITS 13135 : Umesh 09/04/08
                        //dPrintDate = GetOrdinalDate(dThisPayStartDate.Year, dThisPayStartDate.Month, iOrdinal, GetDayNumber(sCodeDesc));
                        //dThisPayStartDate = dThisPayStartDate.AddMonths(1);

                        if (p_iTypeOfMonthlyPay != 1) //in case of day based
                        {
                        dPrintDate = GetOrdinalDate(dThisPayStartDate.Year, dThisPayStartDate.Month, iOrdinal, GetDayNumber(sCodeDesc));
                        }
                        else
                        {
                            if (p_iPrefDate <= dThisPayStartDate.AddMonths(1).AddDays(-1).Day)
                                dPrintDate = new DateTime(dThisPayStartDate.Year, dThisPayStartDate.Month, p_iPrefDate);
                            else
                                dPrintDate = new DateTime(dThisPayStartDate.Year, dThisPayStartDate.Month, dThisPayStartDate.AddMonths(1).AddDays(-1).Day);
                        }
                        dThisPayStartDate = dThisPayStartDate.AddMonths(1);
                    }
                    else
                    {
                        dPrintDate = dPrintDate.AddDays(7);
                    }
                }

            }
            else
            {
                while (dPrintDate < dThisPayEndDate)
                {
                    if (iPrefSched == 2)
                    {
                        dPrintDate = dPrintDate.AddDays(14);
                    }
                    else if (iPrefSched == 3)
                    {
                        //dPrintDate = dPrintDate.AddDays(28);
                        //MITS 13135 : Umesh 09/04/08
                        //dPrintDate = GetOrdinalDate(dThisPayStartDate.Year, dThisPayStartDate.Month, iOrdinal, GetDayNumber(sCodeDesc));
                        //dThisPayStartDate = dThisPayStartDate.AddMonths(1);
                        if (p_iTypeOfMonthlyPay != 1) //in case of day based
                        {
                        dPrintDate = GetOrdinalDate(dThisPayStartDate.Year, dThisPayStartDate.Month, iOrdinal, GetDayNumber(sCodeDesc));
                        }
                        else
                        {
                            if (p_iPrefDate <= dThisPayStartDate.AddMonths(1).AddDays(-1).Day)
                                dPrintDate = new DateTime(dThisPayStartDate.Year, dThisPayStartDate.Month, p_iPrefDate);
                            else
                                dPrintDate = new DateTime(dThisPayStartDate.Year, dThisPayStartDate.Month, dThisPayStartDate.AddMonths(1).AddDays(-1).Day);
                        }
                        dThisPayStartDate = dThisPayStartDate.AddMonths(1);
                    }
                    else
                    {
                        dPrintDate = dPrintDate.AddDays(7);
                    }

                }
                while (dPrintDate < DateTime.Now.Date)
                {
                    if (iPrefSched == 2)
                    {
                        dPrintDate = dPrintDate.AddDays(14);
                    }
                    else if (iPrefSched == 3)
                    {
                        //dPrintDate = dPrintDate.AddDays(28);
                        //MITS 13135 : Umesh 09/04/08
                        //dPrintDate = GetOrdinalDate(dThisPayStartDate.Year, dThisPayStartDate.Month, iOrdinal, GetDayNumber(sCodeDesc));
                        //dThisPayStartDate = dThisPayStartDate.AddMonths(1);
                        if (p_iTypeOfMonthlyPay != 1) //in case of day based
                        {
                        dPrintDate = GetOrdinalDate(dThisPayStartDate.Year, dThisPayStartDate.Month, iOrdinal, GetDayNumber(sCodeDesc));
                        }
                        else
                        {
                            if (p_iPrefDate <= dThisPayStartDate.AddMonths(1).AddDays(-1).Day)
                                dPrintDate = new DateTime(dThisPayStartDate.Year, dThisPayStartDate.Month, p_iPrefDate);
                            else
                                dPrintDate = new DateTime(dThisPayStartDate.Year, dThisPayStartDate.Month, dThisPayStartDate.AddMonths(1).AddDays(-1).Day);
                        }
                        dThisPayStartDate = dThisPayStartDate.AddMonths(1);
                    }
                    else
                    {
                        dPrintDate = dPrintDate.AddDays(7);
                    }
                }
                //pmittal5 Mits 14889 - Print Date should not fall in same month as that of Payment Date
                if (iPrefSched == 3 && p_iTypeOfMonthlyPay == 1)
                {
                    if (dPrintDate.Month == dThisPayEndDate.Month)
                    {
                        dPrintDate = dPrintDate.AddMonths(1);
                        if (p_iPrefDate <= dThisPayStartDate.AddMonths(1).AddDays(-1).Day)
                            dPrintDate = new DateTime(dPrintDate.Year, dPrintDate.Month, p_iPrefDate);
                        else
                            dPrintDate = new DateTime(dPrintDate.Year, dPrintDate.Month, dThisPayStartDate.AddMonths(1).AddDays(-1).Day);
                    }
                }
                //End - pmittal5
            } 
            //pmittal5 Mits 14220 04/20/09
            if (iPrefSched == 3)
            {
                while (dPrintDate < Conversion.ToDate(p_sBenCalcPayStart))
                {
                    //dPrintDate = GetOrdinalDate(dThisPayStartDate.Year, dThisPayStartDate.Month, iOrdinal, GetDayNumber(sCodeDesc));
                    //dThisPayStartDate = dThisPayStartDate.AddMonths(1);
                    if (p_iTypeOfMonthlyPay != 1) //in case of day based
                    {
                    dPrintDate = GetOrdinalDate(dThisPayStartDate.Year, dThisPayStartDate.Month, iOrdinal, GetDayNumber(sCodeDesc));
                    }
                    else
                    {
                        if (p_iPrefDate <= dThisPayStartDate.AddMonths(1).AddDays(-1).Day)
                            dPrintDate = new DateTime(dThisPayStartDate.Year, dThisPayStartDate.Month, p_iPrefDate);
                        else
                            dPrintDate = new DateTime(dThisPayStartDate.Year, dThisPayStartDate.Month, dThisPayStartDate.AddMonths(1).AddDays(-1).Day);
                    }
                    dThisPayStartDate = dThisPayStartDate.AddMonths(1);
                }
             }//End - pmittal5
            dGetNextPrintDate = dPrintDate;

            return dGetNextPrintDate;

        }
        private int DaysInMonth(DateTime p_dDate)
        {
            int iDaysInMonth = 0;
            int iResult = 0;
            //kdb 04/05/2005  Return number of days in a month
            switch(p_dDate.Month)
            {
                case 1:case 3:case 5:case 7:case 8:case 10:case 12:
                    iDaysInMonth = 31;
                    break;
                case 4:case 6:case 9:case 11: 
                    iDaysInMonth = 30;
                    break;
                case 2: Math.DivRem(p_dDate.Year, 4, out iResult);
                    if (iResult == 0)
                        iDaysInMonth = 29;
                    else
                        iDaysInMonth = 28;
                        break;
            }
            return iDaysInMonth;
        }
        private int GetDayNumber(string p_sDay)
        {
            int iGetDayNumber = 0;
            switch(p_sDay.ToLower())
            {
                case "monday": iGetDayNumber = 2;
                               break; 
                case "tuesday": iGetDayNumber = 3;
                               break; 
                case "wednesday": iGetDayNumber = 4;
                               break; 
                case "thursday": iGetDayNumber = 5;
                               break; 
                case "friday": iGetDayNumber = 6;
                               break; 
                case "saturday": iGetDayNumber = 7;
                               break; 
                case "sunday": iGetDayNumber = 1;
                               break; 
            }
            return iGetDayNumber;
        }
        //kdb 04/05/2005 Finds the ordinal occurrence of the given weekday in
        //specified month/year. Sunday = 1,...
        private DateTime GetOrdinalDate(int iYr, int iMn, int iOrd, int iWDay)
        {
            DateTime dtGetOrdinalDate = DateTime.MinValue;
            DateTime dDateParam1 = DateTime.MinValue;
            if(iMn <=12)
                dDateParam1 = new DateTime(iYr, iMn, 1);
            else
                dDateParam1 = new DateTime(iYr + 1, iMn - 12, 1);
            int iResult = 0;
            Math.DivRem((iWDay - ((int)dDateParam1.DayOfWeek + 1) + 7), 7, out iResult);
            dtGetOrdinalDate = dDateParam1.AddDays(iResult + (iOrd - 1) * 7);
            if (dtGetOrdinalDate.Month > iMn)
                dtGetOrdinalDate = dtGetOrdinalDate.AddDays(-7);
            return dtGetOrdinalDate;
        }
        public bool GetPayPeriod(Claim objClaim , int p_iPrefSched, ref DateTime dThisPayPeriodStart, ref DateTime dThisPayPeriodEnd)
        {
            DateTime dBeneStart = DateTime.MinValue;
            DateTime dPlanPayStart = DateTime.MinValue;
            DateTime dPlanPayEnd = DateTime.MinValue;
            int iTmp = 0;
            int iLastDayOfMonth = 0;
            bool bFoundDate = false;
            DateTime dCheckDate = DateTime.MinValue;
            bool bGetPayPeriod = false;
            int i;

            bFoundDate = false;

            if (objClaim.DisabilityPlan.StartPayPeriod != "")
                dPlanPayStart = Conversion.ToDate(objClaim.DisabilityPlan.StartPayPeriod);

            //pmittal5 Mits 14126 12/30/08 - Start Date should always be the one currently present on the screen
            //if (objClaim.BenCalcPayStart != "")
            //    dBeneStart = Conversion.ToDate(objClaim.BenCalcPayStart);
            //else
                dBeneStart = m_dBenCalcPayStart;

            if (dPlanPayStart != DateTime.MinValue && dBeneStart != DateTime.MinValue)
            {
                if (objClaim.DisabilityPlan.PrefPaySchCode == 1737)
                {
                    iTmp = 14;
                    dPlanPayEnd = dPlanPayStart.AddDays(iTmp - 1);
                }
                else if (p_iPrefSched == 1)
                {
                    iTmp = 7;
                    dPlanPayEnd = dPlanPayStart.AddDays(iTmp - 1);
                }
                else
                {
                    dPlanPayEnd = dPlanPayStart.AddMonths(1).AddDays(-1);
                }


                while (bFoundDate == false)
                {
                    if (dPlanPayStart <= dBeneStart)
                    {
                        if (dPlanPayStart <= dBeneStart && dPlanPayEnd >= dBeneStart)
                        {
                            dThisPayPeriodStart = dPlanPayStart;
                            dThisPayPeriodEnd = dPlanPayEnd;
                            bFoundDate = true;
                            bGetPayPeriod = true;
                        }
                        else if (p_iPrefSched == 3) //monthly kdb 04/05/2005
                        {
                            dPlanPayStart = dPlanPayEnd.AddDays(1);
                            dPlanPayEnd = dPlanPayStart.AddMonths(1).AddDays(-1);
                        }
                        else //weekly/bi-weekly
                        {
                            dPlanPayStart = dPlanPayStart.AddDays(iTmp);
                            dPlanPayEnd = dPlanPayEnd.AddDays(iTmp);
                        }
                    }
                    else
                        if ((dPlanPayStart < dBeneStart) && (dPlanPayEnd > dBeneStart))
                        {
                            dThisPayPeriodStart = dPlanPayStart;
                            dThisPayPeriodEnd = dPlanPayEnd;
                            bFoundDate = true;
                        }
                        else if (p_iPrefSched == 3) //monthly kdb 04/05/2005
                        {
                            dPlanPayStart = dPlanPayStart.AddMonths(-1);
                            dPlanPayEnd = dPlanPayStart.AddMonths(1).AddDays(-1);
                        }
                        else //weekly/bi-weekly
                        {
                            dPlanPayStart = dPlanPayStart.AddDays(iTmp * -1);
                            dPlanPayEnd = dPlanPayEnd.AddDays(iTmp * -1);
                        }
                }
            }
            else if (dBeneStart != DateTime.MinValue)
            {
                if (p_iPrefSched == 1 || p_iPrefSched == 2)
                {
                    dCheckDate = dBeneStart;
                    for (i = 1; i <= 7; i++)
                    {
                        //abisht MITS 10780
                        //if ((int)dCheckDate.DayOfWeek != 1)
                        //Wrong comparision was made to monday instead of sunday
                        //Changed that to sunday.
                        if(dCheckDate.DayOfWeek != DayOfWeek.Sunday)
                        {
                            dCheckDate = dCheckDate.AddDays(-1);
                        }
                        else
                        {
                            dPlanPayStart = dCheckDate;
                            continue;
                        }
                    }
                }
                if (objClaim.DisabilityPlan.PrefPaySchCode == 1737)
                {
                    iTmp = 13;
                    dPlanPayEnd = dPlanPayStart.AddDays(iTmp); //MITS 5650 kdb 5/02/2005 took out tmp - 1
                }
                else if (p_iPrefSched == 1)
                {
                    iTmp = 6;
                    dPlanPayEnd = dPlanPayStart.AddDays(iTmp); //MITS 5650 kdb 5/02/2005 took out tmp-1
                }
                else if (p_iPrefSched == 3) //kdb 03/07/2005 Monthly
                {
                    string sTempMonth = "00";
                    if (dBeneStart.Month < 10)
                        sTempMonth = "0" + dBeneStart.Month.ToString();
                    else
                        sTempMonth = dBeneStart.Month.ToString();
                    dCheckDate = Conversion.ToDate(dBeneStart.Year.ToString() + sTempMonth + "01");
                    dPlanPayStart = dCheckDate;
                    dPlanPayEnd = dPlanPayStart.AddMonths(1).AddDays(-1);
                }
            }

            if (dPlanPayStart != DateTime.MinValue && dPlanPayEnd != DateTime.MinValue)
            {
                dThisPayPeriodStart = dPlanPayStart;
                dThisPayPeriodEnd = dPlanPayEnd;
                bGetPayPeriod = true;
            }
            else
                bGetPayPeriod = false;

            return bGetPayPeriod;

        }
        //KDB 04/06/2005  Calculate the days included in the month pay
        int GetDaysIncludedMonth(DateTime p_dStartDate , DateTime p_dEndDate)
        {
            int iDays = 0;
            int iNumberOfDays = 0;
            int iGetDaysIncludedMonth = 0;

            if(p_dStartDate == DateTime.MinValue)
            {
                iGetDaysIncludedMonth = 0;
                return iGetDaysIncludedMonth;
            }
            if(p_dEndDate == DateTime.MinValue)
            {
                iGetDaysIncludedMonth = 0;
                return iGetDaysIncludedMonth;
            }
        
            if(p_dEndDate.Month == p_dStartDate.Month)
            {
                TimeSpan d = p_dEndDate - p_dStartDate;
                iGetDaysIncludedMonth = d.Days + 1;
            }
            else
            {
                string  sMonth = p_dStartDate.Month.ToString();
                //int iMonth = p_dStartDate.Month + 1;
                int iMonth = 0;
                int iYear = p_dStartDate.Year;
                if (p_dStartDate.Month == 12)
                {
                    iMonth = 1;
                    iYear = iYear + 1;
                }
                else
                {
                    iMonth = p_dStartDate.Month + 1;
                }

                if (iMonth >= 1 && iMonth <= 9)
                    sMonth = "0" + iMonth.ToString();
                iDays = Conversion.ToDate(p_dStartDate.Year.ToString() + sMonth + "01").AddDays(-1).Day;
                iGetDaysIncludedMonth = (iDays - p_dStartDate.Day) + 1 + p_dEndDate.Day;
            }
            return iGetDaysIncludedMonth;
        }
        int GetDaysIncluded(Claim objClaim ,DateTime sStartDate, DateTime sEndDate, int[] iWorkWeek, bool bPartialWeek , bool bLastPay , DateTime dThisPayPeriodStart , DateTime dThisPayPeriodEnd)
        {
            int iGetDaysIncluded = 0;
            int i = 0;
            int iNumberOfDays = 0;
            DateTime dCheckDate = DateTime.MinValue;
            int iCount = 0;
            int iDaysToWeekend = 0;
            
            if(sStartDate == DateTime.MinValue)
            {
                iGetDaysIncluded = 0;
                return iGetDaysIncluded;
            }
            if(sEndDate == DateTime.MinValue)
            {
                iGetDaysIncluded = 0;
                return iGetDaysIncluded;
            }
            else
                iNumberOfDays = 1;
            
            if(bPartialWeek)
            {
                if(objClaim.DisabilityPlan.PrefPaySchCode == 1737)
                {
                    if(iNumberOfDays != 0)
                    {
                        iDaysToWeekend = GetPartialPayDays(sStartDate , sEndDate ,dThisPayPeriodStart , dThisPayPeriodEnd , bLastPay);
                        iNumberOfDays = iDaysToWeekend;
                    }
                }
                else
                {
                     if(iNumberOfDays != 0)
                    {
                        iDaysToWeekend = GetPartialPayDays(sStartDate , sEndDate , dThisPayPeriodStart , dThisPayPeriodEnd , bLastPay);
                        iNumberOfDays = iDaysToWeekend;
                    }
                }

            }
            else if(objClaim.DisabilityPlan.PrefPaySchCode == 1737)
                iNumberOfDays = 14;
            else
                iNumberOfDays = 7;
            
            if(iNumberOfDays != 0)
            {
                dCheckDate = sStartDate;
                i = 0;
                for(iCount = 1 ; iCount<=iNumberOfDays;iCount++)
                {
                    if (iWorkWeek[(int)dCheckDate.DayOfWeek] == 1 || iWorkWeek[(int)dCheckDate.DayOfWeek] == -1)
                        i = i + 1; 
                    if(iNumberOfDays > 1)
                        dCheckDate = dCheckDate.AddDays(1);
                }
            }
            else
            {
              dCheckDate = sStartDate;
              if (iWorkWeek[(int)dCheckDate.DayOfWeek] == 1 || iWorkWeek[(int)dCheckDate.DayOfWeek] == -1)
                i = 1;
              else
                i = 0;
            }
    
            iGetDaysIncluded = i;
            return iGetDaysIncluded;
        }
        int GetPartialPayDays(DateTime sStartDate, DateTime sEndDate, DateTime dThisPayPeriodStart , DateTime dThisPayPeriodEnd , bool bLastPay)
        {
            int i = 0;
            int iEnd = 0;
            int iStart = 0;
            string sTestDate = "";
            int iGetPartialPayDays = 0;

            if (!bLastPay)
            {
                if (sStartDate != DateTime.MinValue && dThisPayPeriodStart != DateTime.MinValue)
                    if (dThisPayPeriodStart > sStartDate)
                        iStart = (dThisPayPeriodStart.Subtract(sStartDate)).Days + 1;
                    else
                        iStart = (sStartDate.Subtract(dThisPayPeriodStart)).Days + 1;
                else
                    iStart = 1;
                if (sEndDate != DateTime.MinValue && dThisPayPeriodEnd != DateTime.MinValue)
                    if (sEndDate < dThisPayPeriodEnd)
                        iEnd = (sEndDate.Subtract(sStartDate)).Days + 1;
                    else
                        iEnd = (dThisPayPeriodEnd.Subtract(sStartDate)).Days + 1;
                else
                    iEnd = 1;
                i = iEnd;
            }
            else if (sEndDate != DateTime.MinValue && dThisPayPeriodEnd != DateTime.MinValue)
                if (sEndDate > dThisPayPeriodEnd)
                    iEnd = (sEndDate.Subtract(dThisPayPeriodEnd)).Days + 1;
                else
                    iStart = (dThisPayPeriodEnd.Subtract(sEndDate)).Days + 1;
            else
                iEnd = 1;

            iGetPartialPayDays = i;

            return iGetPartialPayDays;
        }
        //kdb 04/28/2005  Return whether or not the pay is partial
        bool PartialPay(int iDaysInCurrent, int iPrefSched , DisabilityClass objDisabilityClass)
        {
            int iFullPayDays = 0;

            if (iPrefSched == 3)
            {
                if (objDisabilityClass.Day30WorkMonth)
                    iFullPayDays = 30;
                else
                    iFullPayDays = m_iDaysWorkingInMonth;
            }
            else if (iPrefSched == 2)
                iFullPayDays = m_iDaysWorkingInWeek * 2;
            else if (iPrefSched == 1)
                iFullPayDays = m_iDaysWorkingInWeek;

            if (iDaysInCurrent < iFullPayDays)
                return true;
            else
                return false;

        }

        //kdb 04/14/2005 Modified to add bSuppPayment flag to note if payment is the Supplement payment
    private string GetDisCalcSQL(Claim objClaim , DisabilityPlan objPlan , DisabilityClass objDisClass , int p_iAccountId ,long iBatchId , long lFundsAutoTransID , long lPayeeEid ,double dAmount,int bThirdParty ,int iPayNumber,bool bTaxPayment,bool bSplit,int iNumberOfPayments,DateTime dPrintDate,string sAppend,int iNumberOfDays, bool bSuppPayment) 
    {
        string sFirstName="";
        string sLastName = "";
        string sMiddleName = "";
        string sAddr1 = "";
        string sAddr2 = "";
        string sAddr3 = "";
        string sAddr4 = "";
        string sCity = "";
        long lStateID = 0;
        string sZipCode = ""; ;
        long lSubBankAccountID=0;
        long lBankAccountID=0 ;
        int rs=0;
        string sSQL="" ;
        long lPayeeType=0;
        string sDTTMDate="";
        bool bUseThisVoucher=false;
        long lVoucherFlag=0;
        long lThisTaxFlag=0;
        long lSuppPayFlag=0; 
        DbReader objReader = null;
    
        sSQL = "";
        if(objDisClass.UseVouchers)
            lVoucherFlag = -1;
        else 
            lVoucherFlag = 0;
        
        if(bTaxPayment)
            lThisTaxFlag = -1;
        else
            lThisTaxFlag = 0;
        
        bUseThisVoucher = objDisClass.UseVouchers;
        sDTTMDate = DateTime.Now.Date.ToString("yyyyMMdd") + DateTime.Now.ToLocalTime().ToString("HHmmss");
        
        if (bTaxPayment)
        {   //MITS 14206
            //lPayeeType = m_objCache.GetCodeId("O" , "PAYEE_TYPE");
            lPayeeType = m_lOtherPayeeTypeCode;
        }
        else
        {
            //MITS 14206
            //lPayeeType = m_objCache.GetCodeId("C", "PAYEE_TYPE");
            lPayeeType = m_lClaimantPayeeTypeCode;
        }
    
        if(!bTaxPayment)
        {
            sFirstName = objClaim.PrimaryClaimant.ClaimantEntity.FirstName;
            sLastName = objClaim.PrimaryClaimant.ClaimantEntity.LastName;
            sMiddleName = objClaim.PrimaryClaimant.ClaimantEntity.MiddleName;
            sAddr1 = objClaim.PrimaryClaimant.ClaimantEntity.Addr1;
            sAddr2 = objClaim.PrimaryClaimant.ClaimantEntity.Addr2;
            sAddr3 = objClaim.PrimaryClaimant.ClaimantEntity.Addr3;
            sAddr4 = objClaim.PrimaryClaimant.ClaimantEntity.Addr4;
            sCity = objClaim.PrimaryClaimant.ClaimantEntity.City;
            lStateID = objClaim.PrimaryClaimant.ClaimantEntity.StateId;
            sZipCode = objClaim.PrimaryClaimant.ClaimantEntity.ZipCode;
        }
        else
        {
            sSQL = "SELECT * FROM ENTITY WHERE ENTITY_ID = " + lPayeeEid.ToString();
            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
            while (objReader.Read())
            {
                sFirstName = objReader.GetValue("FIRST_NAME").ToString();
                sLastName = objReader.GetValue("LAST_NAME").ToString();
                sMiddleName = objReader.GetValue("MIDDLE_NAME").ToString();
                sAddr1 = objReader.GetValue("ADDR1").ToString();
                sAddr2 = objReader.GetValue("ADDR2").ToString();
                sAddr3 = objReader.GetValue("ADDR3").ToString();
                sAddr4 = objReader.GetValue("ADDR4").ToString();
                sCity = objReader.GetValue("CITY").ToString();
                lStateID = Conversion.ConvertObjToInt64(objReader.GetValue("STATE_ID"), m_iClientId);
                sZipCode = objReader.GetValue("ZIP_CODE").ToString();
                bUseThisVoucher = true;
                lVoucherFlag = -1;
                lThisTaxFlag = -1;
            }
            objReader.Close();
            objReader.Dispose();
        }
    
        sFirstName = sFirstName.Replace("'", "''");
        sLastName = sLastName.Replace("'", "''");
        sMiddleName = sMiddleName.Replace("'", "''");
        sAddr1 = sAddr1.Replace("'", "''");
        sAddr2 = sAddr2.Replace("'", "''");
        sAddr3 = sAddr3.Replace("'", "''");
        sAddr4 = sAddr4.Replace("'", "''");
        sCity = sCity.Replace("'", "''");
        
        //Get Account IDs Check to see if using sub accounts
   
        if(objClaim.Context.InternalSettings.SysSettings.UseFundsSubAcc)
        {
            lSubBankAccountID = p_iAccountId;
            lBankAccountID =  GetParentAccountID(p_iAccountId);
        }
        else
        {
            lBankAccountID = p_iAccountId;
            lSubBankAccountID = 0;
        }
        
        if(bSuppPayment)
        {
            lSubBankAccountID = m_lSuppSubAcc;
            lBankAccountID = m_lSuppBankAcc;
            lSuppPayFlag = -1;
        }
        else
            lSuppPayFlag = 0;

        string sValidatedZipCode = "";
        if (sZipCode.Length >= 5)
            sValidatedZipCode = sZipCode.Substring(0, 5);
        
        sSQL = "INSERT INTO FUNDS_AUTO (AUTO_BATCH_ID, AUTO_TRANS_ID, CLAIM_ID, CLAIMANT_EID, ";
        sSQL = sSQL + "FIRST_NAME, LAST_NAME, ADDR1, ADDR2, ADDR3, ADDR4, CITY, STATE_ID, ZIP_CODE, ";
        sSQL = sSQL + "PAYEE_EID, PAYEE_TYPE_CODE, CTL_NUMBER, AMOUNT, ACCOUNT_ID, PRINT_DATE, ";
        sSQL = sSQL + "DTTM_RCD_ADDED, DTTM_RCD_LAST_UPD, ADDED_BY_USER, UPDATED_BY_USER, ";
        sSQL = sSQL + "THIRD_PARTY_FLAG, PRECHECK_FLAG, CHECK_BATCH_NUM, CLAIM_NUMBER, PAY_NUMBER, ";
        sSQL = sSQL + "END_PAY_NUMBER, STATUS_CODE, SUB_ACC_ID, MIDDLE_NAME, VOUCHER_FLAG, ";
        sSQL = sSQL + "NUM_OF_PAID_DAYS, WEEKS_PAID_CODE, TAX_PAYMENT_FLAG, UNIT_ID, SUPP_PAYMENT_FLAG) ";
        sSQL = sSQL + "VALUES ";
        sSQL = sSQL + "(" + iBatchId.ToString() + ", " + lFundsAutoTransID.ToString() + ", " + objClaim.ClaimId.ToString() + ", ";
        sSQL = sSQL + objClaim.PrimaryClaimant.ClaimantEid.ToString() + ", '" + sFirstName + "', '" + sLastName;
        sSQL = sSQL + "', '" + sAddr1 + "', '" + sAddr2 + "', '" + sAddr3 + "', '" + sAddr4 + "', '" + sCity + "', " + lStateID.ToString();
        sSQL = sSQL + ", '" + sValidatedZipCode + "', " + lPayeeEid.ToString() + ", " + lPayeeType.ToString() + ", 'DIS-" + iBatchId.ToString("0000000") + sAppend + "', ";
        sSQL = sSQL + dAmount.ToString() + ", " + lBankAccountID.ToString() + ", '" + Conversion.ToDbDate(dPrintDate) + "', '" + sDTTMDate + "', '";
        sSQL = sSQL + sDTTMDate + "', '" + m_sUserName + "', '" + m_sUserName + "', ";
        sSQL = sSQL + bThirdParty.ToString() + ", 0, 0, '" + objClaim.ClaimNumber + "', " + iPayNumber.ToString() + ", 0, ";
        sSQL = sSQL + "0, " + lSubBankAccountID.ToString() + ", '" + sMiddleName + "', " + (Conversion.ConvertBoolToInt(bUseThisVoucher, m_iClientId)).ToString() + ", ";
        sSQL = sSQL + iNumberOfDays.ToString() + ", " + objPlan.PrefPaySchCode.ToString() + ", " + lThisTaxFlag.ToString() + ", 0, " + lSuppPayFlag.ToString() + ")";

        return sSQL;

    }

        private string GetRMWorldSplitSQL(Claim objClaim, DisabilityPlan objPlan, DisabilityClass objDisClass, long lAutoSplitId , long lFundsAutoTransID , long lTransTypeCode , double dAmount , DateTime dFromDate , DateTime dToDate , string sTag , bool bSuppSplit )
        {
            string sSQL;
            string sDTTMDate; 
            long lReserveTypeCode = 0;
            long lSuppFlag = 0;
    
            sSQL = "";
    
            lReserveTypeCode = m_objCache.GetRelatedCodeId((int) lTransTypeCode);

            sDTTMDate = DateTime.Now.Date.ToString("yyyyMMdd") + DateTime.Now.ToLocalTime().ToString("HHmmss");
        
            //kdb 05/04/2005
            if(bSuppSplit)
                lSuppFlag = -1;
            else
                lSuppFlag = 0;
            
    
            sSQL = "INSERT INTO FUNDS_AUTO_SPLIT (AUTO_SPLIT_ID, AUTO_TRANS_ID, TRANS_TYPE_CODE, RESERVE_TYPE_CODE, ";
            sSQL = sSQL + "AMOUNT, GL_ACCOUNT_CODE, FROM_DATE, TO_DATE, DTTM_RCD_ADDED, DTTM_RCD_LAST_UPD, ";
            sSQL = sSQL + "ADDED_BY_USER, UPDATED_BY_USER, TAG, SUPP_PAYMENT_FLAG) ";
            sSQL = sSQL + "VALUES ";
            sSQL = sSQL + "(" + lAutoSplitId.ToString() + ", " + lFundsAutoTransID.ToString() + ", " + lTransTypeCode.ToString() + ", ";
            sSQL = sSQL + lReserveTypeCode.ToString() + ", " + dAmount.ToString() + ", 0, " + Conversion.ToDbDate(dFromDate) + ", " + Conversion.ToDbDate(dToDate) + ", ";
            sSQL = sSQL + sDTTMDate + ", " + sDTTMDate + ", '" + m_sUserName + "', '" + m_sUserName + "', ";
            sSQL = sSQL + sTag + ", " + lSuppFlag.ToString() + ")";


            return sSQL;

        }

        //kdb 04/28/2005  Calculate & return the Offset Amount
        double GetOffsetAmt(double dOffsetRate ,double dOffsetAmt,int iPays , int iPrefSched)
        {
            double dGetOffsetAmt = 0;
            double dGrossPayment = 0;
            double dGrossSupplement = 0;
            double dCalcFactor = 0;

            //MITS 14186
            dGrossPayment = Math.Round(m_dDailyAmount * iPays, 2, MidpointRounding.AwayFromZero);
            dGrossSupplement = Math.Round(m_dDailySuppAmount * iPays, 2, MidpointRounding.AwayFromZero);

            if (m_dWeeklyBenefit != 0 && dGrossPayment != 0)
            {
                dCalcFactor = dGrossPayment / m_dWeeklyBenefit;
            }
            else
            {
                if (m_dSuppRate != 0)
                    dCalcFactor = dGrossSupplement / m_dSuppRate;
            }
            if(dOffsetRate > 0)
            {
                if(m_iOffsetCalc == 2 || m_iOffsetCalc == 4)
                    //dGetOffsetAmt = dOffsetRate * iPays;
                    dGetOffsetAmt = Math.Round(dOffsetAmt * dCalcFactor, 2, MidpointRounding.AwayFromZero);
                else
                {
                    dGetOffsetAmt = dOffsetAmt;
                    if(iPrefSched == 2)
                        dGetOffsetAmt = dGetOffsetAmt * 2;
                }
            }
            return dGetOffsetAmt;   
        }

        int GetWorkDays(int iNumberOfDays, DateTime dStartDate , int[] iWorkWeek) 
        {

            int i = 0;
            int iCount = 0;
            DateTime dCheckDate = DateTime.MinValue;

            if(iNumberOfDays != 0)
            {
                dCheckDate = dStartDate;
                i = 0;
                for(iCount = 1; iCount<=iNumberOfDays;iCount++)
                {
                    if (iWorkWeek[(int)dCheckDate.DayOfWeek] == 1 || iWorkWeek[(int)dCheckDate.DayOfWeek] == -1)
                        i = i + 1;

                    if(iNumberOfDays > 1)
                        dCheckDate = dCheckDate.AddDays(1);
                    
                }
            }
            else
            {
                dCheckDate = dStartDate;
                if (iWorkWeek[(int)dCheckDate.DayOfWeek] == 1 || iWorkWeek[(int)dCheckDate.DayOfWeek] == -1)
                    i = 1;
                else 
                    i = 0;
            }
            return i;
    
        }

        private void GetWorkWeek(DisabilityClass objClass , Claim objClaim , ref int[] iDaysOfWeek)
		{
            if(objClass.Day7WorkWeek)
			{
				for(int i=0;i<7;i++)
				{
					iDaysOfWeek[i] = -1;
				}
			}
			else
			{
				if(objClass.Day5WorkWeek)
				{
					for(int i=0;i<7;i++)
					{
                        //if (i == 0 && i == 6) Commented and changed by pawan
						if(i==0 || i==6)
							iDaysOfWeek[i] = 0;
						else
							iDaysOfWeek[i] = -1;
					}
				}
				else
				{
					if(objClass.ActualWorkWeek)
					{
						if(objClaim.PrimaryPiEmployee.WorkSunFlag)
							iDaysOfWeek[0]=-1;
						else
							iDaysOfWeek[0]=0;
						if(objClaim.PrimaryPiEmployee.WorkMonFlag)
							iDaysOfWeek[1]=-1;
						else
							iDaysOfWeek[1]=0;
						if(objClaim.PrimaryPiEmployee.WorkTueFlag)
							iDaysOfWeek[2]=-1;
						else
							iDaysOfWeek[2]=0;
						if(objClaim.PrimaryPiEmployee.WorkWedFlag)
							iDaysOfWeek[3]=-1;
						else
							iDaysOfWeek[3]=0;
						if(objClaim.PrimaryPiEmployee.WorkThuFlag)
							iDaysOfWeek[4]=-1;
						else
							iDaysOfWeek[4]=0;
						if(objClaim.PrimaryPiEmployee.WorkFriFlag)
							iDaysOfWeek[5]=-1;
						else
							iDaysOfWeek[5]=0;
						if(objClaim.PrimaryPiEmployee.WorkSatFlag)
							iDaysOfWeek[6]=-1;
						else
							iDaysOfWeek[6]=0;
					}
					else
					{
						if(objClaim.PrimaryPiEmployee.WorkSunFlag)
							iDaysOfWeek[0]=-1;
						else
							iDaysOfWeek[0]=0;
						if(objClaim.PrimaryPiEmployee.WorkMonFlag)
							iDaysOfWeek[1]=-1;
						else
							iDaysOfWeek[1]=0;
						if(objClaim.PrimaryPiEmployee.WorkTueFlag)
							iDaysOfWeek[2]=-1;
						else
							iDaysOfWeek[2]=0;
						if(objClaim.PrimaryPiEmployee.WorkWedFlag)
							iDaysOfWeek[3]=-1;
						else
							iDaysOfWeek[3]=0;
						if(objClaim.PrimaryPiEmployee.WorkThuFlag)
							iDaysOfWeek[4]=-1;
						else
							iDaysOfWeek[4]=0;
						if(objClaim.PrimaryPiEmployee.WorkFriFlag)
							iDaysOfWeek[5]=-1;
						else
							iDaysOfWeek[5]=0;
						if(objClaim.PrimaryPiEmployee.WorkSatFlag)
							iDaysOfWeek[6]=-1;
						else
							iDaysOfWeek[6]=0;
					}
				}
			}
		}

        void GetBatchInfo(DisabilityClass objDisClass, long p_lAutoSplitId, ref long lThisBatchID, ref long lClaimantEID, ref string sAutoTransID, ref string sCheckMemo, ref string slblDailyAmount, ref string slblDailySuppAmount, ref string slblClaimID, ref long lStateID, ref string sPayPeriodStart , ref string sOldFromDate , ref string sOldToDate)
       {

          string sSQL = "";
          int rs = 0;
          DbReader objReader = null;
 
          try
          {
            sSQL = "SELECT AUTO_BATCH_ID, CLAIMANT_EID, FUNDS_AUTO.AUTO_TRANS_ID, FUNDS_AUTO.CHECK_MEMO ,FUNDS_AUTO_SPLIT.FROM_DATE , FUNDS_AUTO_SPLIT.TO_DATE FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT WHERE AUTO_SPLIT_ID = " + p_lAutoSplitId.ToString();
            sSQL = sSQL + " AND FUNDS_AUTO_SPLIT.AUTO_TRANS_ID = FUNDS_AUTO.AUTO_TRANS_ID";
        
            objReader = objDisClass.Context.DbConnLookup.ExecuteReader(sSQL);
            if(objReader.Read())
            {
                lThisBatchID = Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_BATCH_ID"), m_iClientId);
                lClaimantEID = Conversion.ConvertObjToInt64(objReader.GetValue("CLAIMANT_EID"), m_iClientId);
                sAutoTransID = Conversion.ConvertObjToStr(objReader.GetValue("AUTO_TRANS_ID"));
                sCheckMemo = Conversion.ConvertObjToStr(objReader.GetValue("CHECK_MEMO"));
                sOldFromDate = Conversion.ConvertObjToStr(objReader.GetValue("FROM_DATE"));
                sOldToDate = Conversion.ConvertObjToStr(objReader.GetValue("TO_DATE"));

            }
            if(objReader!=null)
                objReader.Close();
            //MITS 14022 : Umesh
            sSQL = "SELECT DAILY_AMOUNT, DAILY_SUPP_AMOUNT, CLAIM_ID, STATE_ID, PAY_PERIOD_START, START_DATE FROM FUNDS_AUTO_BATCH, FUNDS_AUTO ";
            sSQL = sSQL + "WHERE FUNDS_AUTO_BATCH.AUTO_BATCH_ID = " + lThisBatchID.ToString();
            sSQL = sSQL + " AND FUNDS_AUTO_BATCH.AUTO_BATCH_ID = FUNDS_AUTO.AUTO_BATCH_ID";
            sSQL = sSQL + " AND FUNDS_AUTO.AUTO_TRANS_ID = " + sAutoTransID ;//MWC MITS 5661
            
            objReader = objDisClass.Context.DbConnLookup.ExecuteReader(sSQL);
            if(objReader.Read())
            {
                slblDailyAmount = Conversion.ConvertObjToStr(objReader.GetValue("DAILY_AMOUNT"));
                slblDailySuppAmount = Conversion.ConvertObjToStr(objReader.GetValue("DAILY_SUPP_AMOUNT"));
                slblClaimID = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_ID"));
                lStateID = Conversion.ConvertObjToInt64(objReader.GetValue("STATE_ID"), m_iClientId);
                //MITS 14022 : Umesh
                //sPayPeriodStart = Conversion.ConvertObjToStr(objReader.GetValue("PAY_PERIOD_START"));
                sPayPeriodStart = Conversion.ConvertObjToStr(objReader.GetValue("START_DATE"));
            }
            if (objReader != null)
            {
                objReader.Close();
                objReader.Dispose();
            }
          }
          catch (InitializationException p_objInitializationException)
          {
              throw p_objInitializationException;
          }
          catch (InvalidValueException p_objInvalidValueException)
          {
              throw p_objInvalidValueException;
          }
          
          catch (Exception p_objException)
          {
              throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.Error",m_iClientId), p_objException);
          }
  
    
       }
        int GetBenefitDays(DateTime dBenefitStart, DateTime dBenefitEnd, int iPrefSched, int[] iWorkWeek)
        {

            int iNumberOfDays = 0;
            int i = 0;
            int iCount = 0;
            DateTime dCheckDate = DateTime.MinValue;
            int iGetBenefitDays = 0;

            dCheckDate = dBenefitStart;
            iNumberOfDays = ((int) dBenefitEnd.Subtract(dBenefitStart).TotalDays) + 1;

            i = 0;

            if (iPrefSched == 3) //Monthly kdb 05/02/2005
                iGetBenefitDays = iNumberOfDays;
            else
            {
                for (iCount = 1; iCount <= iNumberOfDays; iCount++)
                {
                    if (iWorkWeek[(int)dCheckDate.DayOfWeek] == 1 || iWorkWeek[(int)dCheckDate.DayOfWeek] == -1)
                    {
                        i = i + 1;
                        dCheckDate = dCheckDate.AddDays(1);
                    }
                    else
                        dCheckDate = dCheckDate.AddDays(1);
                }

                iGetBenefitDays = i;
            }

            return iGetBenefitDays;
        }
        //kdb 05/11/2005 For monthly pay schedules - the daily amount could differ from month to month
        //if the Actual Month option is used.
        private void GetDailyAmount(string sPayPeriodStart, ref string slblDailyAmount, ref string slblDailySuppAmount, string p_sFrom)
        {
            int iDaysInMonth = 0;
            double dBenefit = 0;
            double dSuppBenefit = 0;
            DateTime dDate = DateTime.MinValue;
            DateTime dTemp = DateTime.MinValue;

            dDate = Conversion.ToDate(sPayPeriodStart);

            dTemp = new DateTime(dDate.Year, dDate.Month, 1);
            iDaysInMonth = DaysInMonth(dTemp);
            dBenefit = Conversion.ConvertStrToDouble(slblDailyAmount) * iDaysInMonth;
            if (Conversion.ConvertStrToDouble(slblDailySuppAmount) != 0)
                dSuppBenefit = Conversion.ConvertStrToDouble(slblDailySuppAmount) * iDaysInMonth;

            dTemp = new DateTime(Conversion.ToDate(p_sFrom).Year, Conversion.ToDate(p_sFrom).Month, 1);
            iDaysInMonth = DaysInMonth(dTemp);
            slblDailyAmount = (dBenefit / iDaysInMonth).ToString();
            if (Conversion.ConvertStrToDouble(slblDailySuppAmount) != 0)
                slblDailySuppAmount = (dSuppBenefit / iDaysInMonth).ToString();
        }
        
        private bool IsSuppPayment(string slblAutoTransID , DisabilityClass objDisClass) 
        {
            string sSQL = "";
            long lBatchID = 0;
            long lThisAutoTransID = 0;
            DbReader objReader = null;
            bool bSuppPayment = false;

            try
            {
                lThisAutoTransID = Conversion.ConvertStrToLong(slblAutoTransID);

                sSQL = "SELECT AUTO_BATCH_ID FROM FUNDS_AUTO WHERE AUTO_TRANS_ID = " + lThisAutoTransID.ToString();
                objReader = DbFactory.GetDbReader(m_sConnectionString , sSQL);
                if (objReader.Read())
                {
                    lBatchID = Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_BATCH_ID"), m_iClientId);
                }
                if (objReader != null)
                    objReader.Close();

                sSQL = "SELECT SUPP_PAYMENT_FLAG FROM FUNDS_AUTO WHERE";
                sSQL = sSQL + " FUNDS_AUTO.AUTO_BATCH_ID = " + lBatchID.ToString();
                sSQL = sSQL + " AND FUNDS_AUTO.AUTO_TRANS_ID = " + lThisAutoTransID.ToString();
                sSQL = sSQL + " AND FUNDS_AUTO.PAY_NUMBER = (SELECT PAY_NUMBER FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + lBatchID.ToString();
                sSQL = sSQL + " AND FUNDS_AUTO.AUTO_TRANS_ID = " + lThisAutoTransID.ToString() + ")";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    bSuppPayment = Conversion.ConvertObjToBool(objReader.GetValue("SUPP_PAYMENT_FLAG"), m_iClientId);
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.Error",m_iClientId), p_objException);
            } 

            return bSuppPayment;
    
        }
        private bool IsSuppPayment(string slblAutoTransID, DisabilityClass objDisClass , DbTransaction objDbTransaction , DbConnection objDbConnection)
        {
            string sSQL = "";
            long lBatchID = 0;
            long lThisAutoTransID = 0;
            DbReader objReader = null;
            bool bSuppPayment = false;

            try
            {
                lThisAutoTransID = Conversion.ConvertStrToLong(slblAutoTransID);

                sSQL = "SELECT AUTO_BATCH_ID FROM FUNDS_AUTO WHERE AUTO_TRANS_ID = " + lThisAutoTransID.ToString();
                objReader = objDbConnection.ExecuteReader(sSQL, objDbTransaction);
                if (objReader.Read())
                {
                    lBatchID = Conversion.ConvertObjToInt64(objReader.GetValue("AUTO_BATCH_ID"), m_iClientId);
                }
                if (objReader != null)
                    objReader.Close();

                sSQL = "SELECT SUPP_PAYMENT_FLAG FROM FUNDS_AUTO WHERE";
                sSQL = sSQL + " FUNDS_AUTO.AUTO_BATCH_ID = " + lBatchID.ToString();
                sSQL = sSQL + " AND FUNDS_AUTO.AUTO_TRANS_ID = " + lThisAutoTransID.ToString();
                sSQL = sSQL + " AND FUNDS_AUTO.PAY_NUMBER = (SELECT PAY_NUMBER FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_BATCH_ID = " + lBatchID.ToString();
                sSQL = sSQL + " AND FUNDS_AUTO.AUTO_TRANS_ID = " + lThisAutoTransID.ToString() + ")";
                objReader = objDbConnection.ExecuteReader(sSQL, objDbTransaction);
                if (objReader.Read())
                {
                    bSuppPayment = Conversion.ConvertObjToBool(objReader.GetValue("SUPP_PAYMENT_FLAG"), m_iClientId);
                }
                if (objReader != null)
                    objReader.Close();
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.FillNonOccPaymentDom.Error",m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }

            return bSuppPayment;

        }
        public XmlDocument ManualPaymentDetails(int iAutoSplitID)
        {
            XmlDocument objDocument = null;
            XmlElement objRoot = null;
            XmlElement objTempElement = null;
            DbReader objReader = null;
            StringBuilder sSQL = new StringBuilder();
            int iAutoTransID = 0;
            int iAccountID = 0;
            //int iCheckNum = 0; 
            long lCheckNum = 0;       //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
            string sAccountName = String.Empty;

            try
            {
                objDocument = new XmlDocument();
                objRoot = objDocument.CreateElement("ProcessManualPayment");
                objDocument.AppendChild(objRoot);

                sSQL.Append("SELECT AUTO_TRANS_ID, ACCOUNT_ID FROM FUNDS_AUTO WHERE AUTO_TRANS_ID = (SELECT AUTO_TRANS_ID FROM FUNDS_AUTO_SPLIT WHERE AUTO_SPLIT_ID = ");
                sSQL.Append(iAutoSplitID.ToString() + ")");

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
                if (objReader.Read())
                {
                    iAutoTransID = Conversion.ConvertObjToInt(objReader.GetValue("AUTO_TRANS_ID"), m_iClientId);
                    iAccountID = Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), m_iClientId);
                }
                else
                {
                    iAutoTransID = 0;
                    iAccountID = 0;
                }

                objReader.Close();
                sSQL.Remove(0, sSQL.Length);

                //pmittal5 Mits 14919 03/25/09 - If Use Master Bank Account option is checked
                SysSettings objSysSettings = new SysSettings(m_sConnectionString,m_iClientId);//psharma206
                bool bMastAcctFound = false;
                if (objSysSettings.UseMastBankAcct && (!objSysSettings.UseFundsSubAcc))
                {
                    sSQL.Append("SELECT MAST_BANK_ACCT_ID FROM ACCOUNT WHERE ACCOUNT_ID = " + iAccountID.ToString());
                    objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL.ToString());
                    sSQL.Remove(0, sSQL.Length);
                    if (objReader.Read())
                    {
                        int iMastAcctId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                        if (iMastAcctId != 0)
                        {
                            sSQL.Append("SELECT NEXT_CHECK_NUMBER FROM ACCOUNT WHERE ACCOUNT_ID = " + iMastAcctId.ToString());
                            bMastAcctFound = true;
                        }
                    }
                    objReader.Close();
                } 
                if (bMastAcctFound == false)
                {//End - pmittal5 
                    sSQL.Append("SELECT NEXT_CHECK_NUMBER FROM ACCOUNT WHERE ACCOUNT_ID = " + iAccountID.ToString());
                }

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
                
                if (objReader.Read())
                    //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
                    //iCheckNum = Conversion.ConvertObjToInt(objReader.GetValue("NEXT_CHECK_NUMBER"));
                    lCheckNum = Conversion.ConvertObjToInt64(objReader.GetValue("NEXT_CHECK_NUMBER"), m_iClientId);
                else
                    lCheckNum = 0;
                sSQL.Remove(0, sSQL.Length);

                sAccountName = GetAccountName(iAccountID);

                objTempElement = objRoot.OwnerDocument.CreateElement("AutoTransID");
                objTempElement.InnerText = iAutoTransID.ToString();
                objRoot.AppendChild(objTempElement);

                objTempElement = objRoot.OwnerDocument.CreateElement("AccountID");
                objTempElement.InnerText = iAccountID.ToString();
                objRoot.AppendChild(objTempElement);

                objTempElement = objRoot.OwnerDocument.CreateElement("CheckNumber");
                objTempElement.InnerText = lCheckNum.ToString();
                objRoot.AppendChild(objTempElement);

                objTempElement = objRoot.OwnerDocument.CreateElement("AccountName");
                objTempElement.InnerText = sAccountName.ToString();
                objRoot.AppendChild(objTempElement);

                objTempElement = objRoot.OwnerDocument.CreateElement("CheckDate");
                objTempElement.InnerText = DateTime.Now.ToShortDateString();
                objRoot.AppendChild(objTempElement);

                objDocument.AppendChild(objRoot);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.GetPrintedPayments.Error",m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }

            return objDocument;
        }

        //MITS 14186 : Umesh
        //To get index of offset having maximum value.
        private int GetMaxOffsetIndex(double[] p_dArrayofOffsets)
        {
            int iIndex =0;
            double dMaxvalue = p_dArrayofOffsets[0];
            for (int i = 1; i <= p_dArrayofOffsets.Length - 1; i++)
            {
                if (p_dArrayofOffsets[i] > dMaxvalue)
                {
                    dMaxvalue = p_dArrayofOffsets[i];
                    iIndex = i;
                }
            }

            return iIndex;
        }
        //Abhishek MITS 11072 changed check number to long from int
        //pmittal5 Mits 17636 09/11/09 
        //public bool ProcessManualPayment(int iAutoTransID, string sCheckDate, long lCheckNumber)
        public bool ProcessManualPayment(int iAutoTransID, string sCheckDate, long lCheckNumber, int iAccountID, ref int rTransId)
        {
            DbReader objReader = null;
            DbReader objReaderFunds = null;
            Funds objFunds = null;
            Funds objMyFunds = null;
            FundsTransSplit objFundsTransSplit = null;
            FundsAutoSplit objFundsAutoSplit = null;
            FundsAuto objFundsAuto = null;
            FundsAutoBatch objFundsAutoBatch = null;
            string sDttm = String.Empty;
            string sSql = String.Empty;
            string sTemp = String.Empty;
            string sUniqueCtlNum = String.Empty;
            int iAutoBatchID = 0;
            int iClaimID = 0;
            int iClaimantEID = 0;
            int iUnitID = 0;
            int iTransID = 0;
            int iPayNum = 0;
            int iCurrentPayment = 0;
            int iPreviousAutoTransId = 0;
            double dGrossAmount = 0;
            int iPaymentsToDate = 0;
            DbConnection objConn = null;
            //DbTransaction objTrans = null;
            bool bReturn = false;
            long lTransNum = 0;            //pmittal5 Mits 17636
            bool bValueCalculated = false; //pmittal5 Mits 17636
            try
            {
                //MITS 14277 : Umesh

                sSql = "SELECT AUTO_BATCH_ID , PAY_NUMBER FROM FUNDS_AUTO WHERE AUTO_TRANS_ID = " + iAutoTransID;

                objReaderFunds = DbFactory.GetDbReader(m_sConnectionString, sSql);

                if (objReaderFunds.Read())
                {
                    iAutoBatchID = Conversion.ConvertObjToInt(objReaderFunds.GetValue("AUTO_BATCH_ID"), m_iClientId);

                    iPayNum = Conversion.ConvertObjToInt(objReaderFunds.GetValue("PAY_NUMBER"), m_iClientId);

                }

                objReaderFunds.Close();
                //MITS 14277 : End
                //objTrans = this.m_objDataModelFactory.Context.TransStart();
                
                sDttm = Conversion.ToDbDate(DateTime.Now);
                //sSql = "SELECT * FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT WHERE FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID AND FUNDS_AUTO.AUTO_TRANS_ID = " + iAutoTransID;
                //pmittal5 Mits 17636 09/11/09 - Added ORDER BY on Auto_trans_id 
                sSql = "SELECT * FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT WHERE FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID AND FUNDS_AUTO.AUTO_BATCH_ID = " + iAutoBatchID + "AND FUNDS_AUTO.PAY_NUMBER =" + iPayNum + " ORDER BY FUNDS_AUTO.AUTO_TRANS_ID";

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);

                //if (objReader.Read())
                while (objReader.Read())
                {
                    iAutoBatchID = Conversion.ConvertObjToInt(objReader.GetValue("AUTO_BATCH_ID"), m_iClientId);
                    iAutoTransID = Conversion.ConvertObjToInt(objReader.GetValue("AUTO_TRANS_ID"), m_iClientId);
                    lTransNum = lCheckNumber; //pmittal5 Mits 17636
                    if (iAutoTransID != iPreviousAutoTransId)
                    {
                        iPreviousAutoTransId = iAutoTransID;
                    objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                    objFunds.ClaimId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId);
                    objFunds.ClaimantEid = Conversion.ConvertObjToInt(objReader.GetValue("CLAIMANT_EID"), m_iClientId);

                    iClaimID = objFunds.ClaimId;
                    iClaimantEID = objFunds.ClaimantEid;

                    objFunds.UnitId = Conversion.ConvertObjToInt(objReader.GetValue("UNIT_ID"), m_iClientId);
                    iUnitID = objFunds.UnitId;

                    objFunds.FirstName = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                    objFunds.LastName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")); ;
                    objFunds.Addr1 = Conversion.ConvertObjToStr(objReader.GetValue("ADDR1")); ;
                    objFunds.Addr2 = Conversion.ConvertObjToStr(objReader.GetValue("ADDR2"));
                    objFunds.Addr3 = Conversion.ConvertObjToStr(objReader.GetValue("ADDR3")); ;
                    objFunds.Addr4 = Conversion.ConvertObjToStr(objReader.GetValue("ADDR4"));
                    objFunds.City = Conversion.ConvertObjToStr(objReader.GetValue("CITY"));
                    objFunds.StateId = Conversion.ConvertObjToInt(objReader.GetValue("STATE_ID"), m_iClientId);
                    objFunds.ZipCode = Conversion.ConvertObjToStr(objReader.GetValue("ZIP_CODE"));
                    objFunds.CheckMemo = Conversion.ConvertObjToStr(objReader.GetValue("CHECK_MEMO"));
                    objFunds.PayeeEid = Conversion.ConvertObjToInt(objReader.GetValue("PAYEE_EID"), m_iClientId);
                    objFunds.PayeeTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("PAYEE_TYPE_CODE"), m_iClientId);
                    objFunds.EnclosureFlag = Conversion.ConvertObjToBool(objReader.GetValue("ENCLOSURE_FLAG"), m_iClientId);
                    objFunds.SettlementFlag = Conversion.ConvertObjToBool(objReader.GetValue("SETTLEMENT_FLAG"), m_iClientId);
                    objFunds.VoucherFlag = Conversion.ConvertObjToBool(objReader.GetValue("VOUCHER_FLAG"), m_iClientId);
                    objFunds.WeeksPaidCode = Conversion.ConvertObjToInt(objReader.GetValue("WEEKS_PAID_CODE"), m_iClientId);

                    objFunds.TaxPaymentFlag = Conversion.ConvertObjToBool(objReader.GetValue("TAX_PAYMENT_FLAG"), m_iClientId);
                    objFunds.SuppPaymentFlag = Conversion.ConvertObjToBool(objReader.GetValue("SUPP_PAYMENT_FLAG"), m_iClientId);
                    objFunds.NumOfPaidDays = Conversion.ConvertObjToInt(objReader.GetValue("NUM_OF_PAID_DAYS"), m_iClientId);

                    iPayNum = Conversion.ConvertObjToInt(objReader.GetValue("PAY_NUMBER"), m_iClientId);
                    sTemp = Conversion.ConvertObjToStr(objReader.GetValue("CTL_NUMBER")) + iPayNum.ToString();
                    sTemp = sTemp + "-MP";
                        //sUniqueCtlNum = GetUniqueCtlNum(sTemp);
                        //objFunds.CtlNumber = sUniqueCtlNum;

                        // MITS 14277 : Umesh

                        bool bUnique = false;
                        string sRet = string.Empty;
                        int iCount = 0;
                        while (!bUnique)
                        {
                            sRet = this.m_objDataModelFactory.Context.DbConnLookup.ExecuteString(String.Format("SELECT CTL_NUMBER FROM FUNDS WHERE CTL_NUMBER='{0}'", sTemp));


                            if (sRet != "")
                            {
                                iCount = iCount + 1;
                                sTemp = sTemp + "-D-" + iCount.ToString();
                                bUnique = false;
                            }
                            else
                            {
                                bUnique = true;
                            }


                        }


                        sUniqueCtlNum = sTemp;
                    objFunds.CtlNumber = sUniqueCtlNum;

                    objFunds.Amount = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                    objFunds.AccountId = Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), m_iClientId);
                    objFunds.SubAccountId = Conversion.ConvertObjToInt(objReader.GetValue("SUB_ACC_ID"), m_iClientId);

                    //if print date is greater than todays date then set it back
                    sTemp = Conversion.ConvertObjToStr(objReader.GetValue("PRINT_DATE"));
                    if (sTemp.CompareTo(Conversion.ToDbDate(DateTime.Now)) > 0)
                        sTemp = Conversion.ToDbDate(DateTime.Now);
                    objFunds.TransDate = sTemp;

                    objFunds.DateOfCheck = Conversion.ConvertObjToStr(objReader.GetValue("PRINT_DATE"));
                    objFunds.DttmRcdAdded = Conversion.ConvertObjToStr(objReader.GetValue("DTTM_RCD_ADDED"));
                    objFunds.AddedByUser = Conversion.ConvertObjToStr(objReader.GetValue("ADDED_BY_USER"));
                    objFunds.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now);
                    objFunds.UpdatedByUser = Conversion.ConvertObjToStr(objReader.GetValue("UPDATED_BY_USER"));
                    objFunds.AutoCheckFlag = true;
                    objFunds.PrecheckFlag = true;
                    objFunds.VoidFlag = false;
                    objFunds.ClearedFlag = false;
                    objFunds.PaymentFlag = true;
                    objFunds.CollectionFlag = false;
                    objFunds.BatchNumber = 0;
                    m_objCache = new LocalCache(m_sConnectionString, m_iClientId);
                    objFunds.StatusCode = m_objCache.GetCodeId("R", "CHECK_STATUS");
                    objFunds.ClaimNumber = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER"));

                    objFunds.AutoCheckDetail = "Payment Number " + iPayNum;

                    objFunds.DateOfCheck = sCheckDate;
                    //objFunds.TransNumber = iCheckNumber;
		    //MITS 11072 Abhishek 
                        //MITS 14277 : Umesh
                        //because for tax payment ..there will be no check. It will be only Voucher. So no check numebr is allocated.

                    //pmittal5 Mits 17636 09/11/09 -- In case of Separate Supplement Payment, Bank Accounts can be different for Gross and Supplement Payment
                    //If Payments AccountId is not same as that on screen, it is a Supplement Payment. Check Number generated will be different
                    if (objFunds.TaxPaymentFlag != true)
                    {
                        if(objFunds.AccountId == iAccountID)
                            objFunds.TransNumber = lCheckNumber;
                        else if (objFunds.AccountId != iAccountID)
                        {
                            objConn = DbFactory.GetDbConnection(m_sConnectionString);
                            objConn.Open();
                            sSql = "SELECT NEXT_CHECK_NUMBER FROM ACCOUNT WHERE ACCOUNT_ID = " + objFunds.AccountId;
                            lTransNum = Conversion.ConvertObjToInt64(objConn.ExecuteScalar(sSql), m_iClientId);
                            objFunds.TransNumber = lTransNum;
                            objConn.Close();
                            objConn.Dispose();
                        }
                    }
                    //End - pmittal5
                    objFunds.StatusCode = 1054;
                    objFunds.UpdatedByUser = m_objDataModelFactory.Context.RMUser.LoginName;
                    objFunds.DttmRcdLastUpd = sDttm;

                    objFunds.Save();

                    iTransID = objFunds.TransId;
                    rTransId = iTransID;

                    sSql = "SELECT * FROM FUNDS_AUTO_SPLIT WHERE AUTO_TRANS_ID = " + iAutoTransID;
                    objReaderFunds = DbFactory.GetDbReader(m_sConnectionString, sSql);

                    while (objReaderFunds.Read())
                    {
                        objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit", false);

                        objFundsTransSplit.TransId = objFunds.TransId;

                        objFundsTransSplit.TransTypeCode = Conversion.ConvertObjToInt(objReaderFunds.GetValue("TRANS_TYPE_CODE"), m_iClientId);
                        objFundsTransSplit.ReserveTypeCode = Conversion.ConvertObjToInt(objReaderFunds.GetValue("RESERVE_TYPE_CODE"), m_iClientId);
                        objFundsTransSplit.Amount = Conversion.ConvertObjToDouble(objReaderFunds.GetValue("AMOUNT"), m_iClientId);
                        objFundsTransSplit.GlAccountCode = Conversion.ConvertObjToInt(objReaderFunds.GetValue("GL_ACCOUNT_CODE"), m_iClientId);
                        objFundsTransSplit.InvoicedBy = Conversion.ConvertObjToStr(objReaderFunds.GetValue("INVOICED_BY"));
                        objFundsTransSplit.InvoiceAmount = Conversion.ConvertObjToDouble(objReaderFunds.GetValue("INVOICE_AMOUNT"), m_iClientId);
                        objFundsTransSplit.InvoiceNumber = Conversion.ConvertObjToStr(objReaderFunds.GetValue("INVOICE_NUMBER"));
                        objFundsTransSplit.PoNumber = Conversion.ConvertObjToStr(objReaderFunds.GetValue("PO_NUMBER"));
                        objFundsTransSplit.DttmRcdAdded = Conversion.ConvertObjToStr(objReaderFunds.GetValue("DTTM_RCD_ADDED"));
                        objFundsTransSplit.AddedByUser = Conversion.ConvertObjToStr(objReaderFunds.GetValue("ADDED_BY_USER"));
                        objFundsTransSplit.DttmRcdLastUpd = Conversion.ConvertObjToStr(objReaderFunds.GetValue("DTTM_RCD_LAST_UPD"));
                        objFundsTransSplit.UpdatedByUser = Conversion.ConvertObjToStr(objReaderFunds.GetValue("UPDATED_BY_USER"));
                        objFundsTransSplit.FromDate = Conversion.ConvertObjToStr(objReaderFunds.GetValue("FROM_DATE"));
                        objFundsTransSplit.ToDate = Conversion.ConvertObjToStr(objReaderFunds.GetValue("TO_DATE"));
                        objFundsTransSplit.SumAmount = Conversion.ConvertObjToDouble(objReaderFunds.GetValue("AMOUNT"), m_iClientId);
                        objFundsTransSplit.SuppPaymentFlag = Conversion.ConvertObjToBool(objReaderFunds.GetValue("SUPP_PAYMENT_FLAG"), m_iClientId);

                        objFundsTransSplit.Save();

                    }

                    objReaderFunds.Close();
                    //pmittal5 09/10/09 Mits 17636 - In case of Separate Supplement Payments, below code is executed twice for same Batch_Number
                    if (objFunds.TaxPaymentFlag != true)
                    {
                        sSql = "SELECT * FROM FUNDS_AUTO_BATCH WHERE AUTO_BATCH_ID = " + iAutoBatchID.ToString();
                        objReaderFunds = DbFactory.GetDbReader(m_sConnectionString, sSql);

                        if (objReaderFunds.Read())
                        {
                            if (!bValueCalculated) //pmittal5 09/10/09 Mits 17636 
                                iCurrentPayment = Conversion.ConvertObjToInt(objReaderFunds.GetValue("PAYMENTS_TO_DATE"), m_iClientId) + 1;
                            dGrossAmount = Conversion.ConvertObjToInt(objReaderFunds.GetValue("GROSS_TO_DATE"), m_iClientId) +
                                           Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                            if (!bValueCalculated)
                                iPaymentsToDate = Conversion.ConvertObjToInt(objReaderFunds.GetValue("PAYMENTS_TO_DATE"), m_iClientId) + 1;
                            bValueCalculated = true; //pmittal5 09/10/09 Mits 17636 
                        }

                        objReaderFunds.Close();
                   }
                   //objReader.Close();

                    sSql = "DELETE FROM FUNDS_AUTO_SPLIT WHERE AUTO_TRANS_ID = " + iAutoTransID.ToString();
                    
                    objConn = DbFactory.GetDbConnection(m_sConnectionString);			
				    objConn.Open();
				    objConn.ExecuteNonQuery(sSql);
                    objConn.Close();
                    //objConn.Dispose();

                    objFundsAuto = (FundsAuto)m_objDataModelFactory.GetDataModelObject("FundsAuto", false);
                    objFundsAuto.MoveTo(iAutoTransID);
                    objFundsAuto.Delete();
                    if (objFunds.TaxPaymentFlag != true)
                    {
                        objFundsAutoBatch = (FundsAutoBatch)m_objDataModelFactory.GetDataModelObject("FundsAutoBatch", false);
                        objFundsAutoBatch.MoveTo(iAutoBatchID);
                        objFundsAutoBatch.PaymentsToDate = iPaymentsToDate;
                        objFundsAutoBatch.GrossToDate = dGrossAmount;
                        objFundsAutoBatch.CurrentPayment = iCurrentPayment + 1;
                        objFundsAutoBatch.Save();

                        //pmittal5 Mits 17636 09/11/09 - Check Number in case of Separate Supplement Payment will be different for Gross and Supplement
                        //Replaced lCheckNumber with lTransNum, since lCheckNumber is the value present on screen, while lTransNum is the actual value for the Check Number
                        //pmittal5 Mits 14919 03/25/09 - Update NEXT_CHECK_NUMBER for Account
                        if (lTransNum != 0)
                        {
                            objConn.Open();
                            if (lTransNum == lCheckNumber)
                                lCheckNumber = lCheckNumber + 1;
                            lTransNum = lTransNum + 1;
                            sSql = "UPDATE ACCOUNT SET NEXT_CHECK_NUMBER = " + lTransNum + " WHERE ACCOUNT_ID = " + objFunds.AccountId;
                            objConn.ExecuteNonQuery(sSql);

                            //If Use Master Bank Account option is checked
                            SysSettings objSysSettings = new SysSettings(m_sConnectionString,m_iClientId);//psharma206
                            if (objSysSettings.UseMastBankAcct && (!objSysSettings.UseFundsSubAcc))
                            {
                                sSql = "SELECT MAST_BANK_ACCT_ID FROM ACCOUNT WHERE ACCOUNT_ID = " + objFunds.AccountId;
                                int iMastAcctId = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSql), m_iClientId);
                                if (iMastAcctId != 0)
                                {
                                    sSql = "UPDATE ACCOUNT SET NEXT_CHECK_NUMBER = " + lTransNum + " WHERE ACCOUNT_ID = " + iMastAcctId;
                                    objConn.ExecuteNonQuery(sSql);
                                }
                            }
                            objConn.Close();
                        }
					}
                    objConn.Dispose();
                    //End - pmittal5

                    //Iff No Errors... Go ahead and commit all of these changes.
                   // this.m_objDataModelFactory.Context.TransCommit();

                  //  bReturn = true;

                }
                

                }
                objReader.Close();
                this.m_objDataModelFactory.Context.TransCommit();

                bReturn = true;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                this.m_objDataModelFactory.Context.TransRollback();
                bReturn = false;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objReaderFunds != null)
                {
                    objReaderFunds.Close();
                    objReaderFunds.Dispose();
                }

                if(objFunds!=null)
                    objFunds.Dispose();

                if (objMyFunds != null)
                    objMyFunds.Dispose();

                if(objFundsTransSplit != null)
                    objFundsTransSplit = null;

                if(objFundsAutoSplit != null)
                    objFundsAutoSplit = null;

                if(objFundsAuto != null)
                    objFundsAuto = null;
                
                if(objFundsAutoBatch != null)
                    objFundsAutoBatch = null;

                //if (objTrans != null)
                //    objTrans.Dispose();
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
            }
            return bReturn;

        }

        //igupta3 Mits : 28566 changes starts
        public bool CreateWorkLossRestrictions(int TransId)
        {
            Claim objClaim = null;
            Funds objFunds = null;
            FundsTransSplit objFundsTransSplit = null;
            int iLOB = 0; //Line of Business
            bool bAutoCrtWrkLossRest = false; //flag to check whether this module is on/off 
            SysSettings oSettings = null;
            string sWrkLossCode = string.Empty;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            PiXWorkLoss objPiXWorkLoss = null;
            PiXRestrict objPiXRestrict = null;
            bool bWorkLossDelete = false;
            bool bRestrictionsDelete = false;
            bool bCastToTypeSuccess = false;
            string sSql = String.Empty;
            DbReader objReaderFunds = null;

            //igupta3
            double dNetAMount = 0;
            string sTranTypeCode = String.Empty;
            int iSplitId = 0;


            try
            {
                //added by raman for R6 Auto Create Work Loss/Restrictions 
                //Parameters fetching for Creating Auto work loss and restriction screens and saving the screens
                oSettings = new SysSettings(m_sConnectionString,m_iClientId);
                bAutoCrtWrkLossRest = oSettings.AutoCrtWrkLossRest;
                if (!bAutoCrtWrkLossRest)
                    return false;
                
                    //Raman R7 Prf Imp : Feb 2 2011                
                objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                if (objFunds.IsNew)
                {
                    objFunds.MoveTo(TransId);
                }

                iLOB = objFunds.LineOfBusCode;                

                if (bAutoCrtWrkLossRest && iLOB != 0)
                {
                    //Trans_id and split_row_id is present in WORK_LOSS_MAPPING table, but split_row_is
                    //is in deleted status i.e. the transaction has been deleted;all such records will be 
                    //deleted:smishra25 start
                    string sSQL = "SELECT * FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND SPLIT_ROW_ID NOT IN (SELECT SPLIT_ROW_ID FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID = " + objFunds.TransId.ToString() + " )";
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        objPiXWorkLoss = (PiXWorkLoss)m_objDataModelFactory.GetDataModelObject("PiXWorkLoss", false);
                        objPiXRestrict = (PiXRestrict)m_objDataModelFactory.GetDataModelObject("PiXRestrict", false);
                        long lWorkLossRowId = 0;
                        long lRestrictRowId = 0;
                        while (objReader.Read())
                        {
                            lWorkLossRowId = 0;
                            lRestrictRowId = 0;
                            if (objReader.GetValue("PI_X_WL_ROW_ID") != null)
                            {
                                bCastToTypeSuccess = false;
                                lWorkLossRowId = Conversion.CastToType<long>(objReader.GetValue("PI_X_WL_ROW_ID").ToString(), out bCastToTypeSuccess);
                                if (!bCastToTypeSuccess)
                                {
                                    lWorkLossRowId = 0;
                                }
                            }
                            if (objReader.GetValue("PI_RESTRICT_ROW_ID") != null)
                            {
                                bCastToTypeSuccess = false;
                                lRestrictRowId = Conversion.CastToType<long>(objReader.GetValue("PI_RESTRICT_ROW_ID").ToString(), out bCastToTypeSuccess);
                                if (!bCastToTypeSuccess)
                                {
                                    lRestrictRowId = 0;
                                }
                            }
                            if (lWorkLossRowId != 0)
                            {
                                string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_WORK_LOSS WHERE PI_WL_ROW_ID = " + lWorkLossRowId).ToString();
                                if (string.Compare(sRecordCount, "1") == 0)
                                {
                                    objPiXWorkLoss.MoveTo((int)lWorkLossRowId);
                                    objPiXWorkLoss.Delete();
                                }
                                string sSQLWL = "DELETE FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND PI_X_WL_ROW_ID =" + lWorkLossRowId;
                                objFunds.Context.DbConn.ExecuteScalar(sSQLWL);
                            }
                            else if (lRestrictRowId != 0)
                            {
                                string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_RESTRICT WHERE PI_RESTRICT_ROW_ID = " + lRestrictRowId).ToString();
                                if (string.Compare(sRecordCount, "1") == 0)
                                {
                                    objPiXRestrict.MoveTo((int)lRestrictRowId);
                                    objPiXRestrict.Delete();
                                }
                                string sSQLRes = "DELETE FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND PI_RESTRICT_ROW_ID =" + lRestrictRowId;
                                objFunds.Context.DbConn.ExecuteScalar(sSQLRes);
                            }
                        }
                    }

                    //getting split related data
                    sSql = "SELECT * FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID = " + TransId;
                    objReaderFunds = DbFactory.GetDbReader(m_sConnectionString, sSql);

                    while (objReaderFunds.Read())
                    {
                        objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit", false);

                        objFundsTransSplit.TransId = objFunds.TransId;

                        objFundsTransSplit.TransTypeCode = Conversion.ConvertObjToInt(objReaderFunds.GetValue("TRANS_TYPE_CODE"), m_iClientId);
                        objFundsTransSplit.ReserveTypeCode = Conversion.ConvertObjToInt(objReaderFunds.GetValue("RESERVE_TYPE_CODE"), m_iClientId);
                        objFundsTransSplit.Amount = Conversion.ConvertObjToDouble(objReaderFunds.GetValue("AMOUNT"), m_iClientId);
                        objFundsTransSplit.GlAccountCode = Conversion.ConvertObjToInt(objReaderFunds.GetValue("GL_ACCOUNT_CODE"), m_iClientId);
                        objFundsTransSplit.InvoicedBy = Conversion.ConvertObjToStr(objReaderFunds.GetValue("INVOICED_BY"));
                        objFundsTransSplit.InvoiceAmount = Conversion.ConvertObjToDouble(objReaderFunds.GetValue("INVOICE_AMOUNT"), m_iClientId);
                        objFundsTransSplit.InvoiceNumber = Conversion.ConvertObjToStr(objReaderFunds.GetValue("INVOICE_NUMBER"));
                        objFundsTransSplit.PoNumber = Conversion.ConvertObjToStr(objReaderFunds.GetValue("PO_NUMBER"));
                        objFundsTransSplit.DttmRcdAdded = Conversion.ConvertObjToStr(objReaderFunds.GetValue("DTTM_RCD_ADDED"));
                        objFundsTransSplit.AddedByUser = Conversion.ConvertObjToStr(objReaderFunds.GetValue("ADDED_BY_USER"));
                        objFundsTransSplit.DttmRcdLastUpd = Conversion.ConvertObjToStr(objReaderFunds.GetValue("DTTM_RCD_LAST_UPD"));
                        objFundsTransSplit.UpdatedByUser = Conversion.ConvertObjToStr(objReaderFunds.GetValue("UPDATED_BY_USER"));
                        objFundsTransSplit.FromDate = Conversion.ConvertObjToStr(objReaderFunds.GetValue("FROM_DATE"));
                        objFundsTransSplit.ToDate = Conversion.ConvertObjToStr(objReaderFunds.GetValue("TO_DATE"));
                        objFundsTransSplit.SumAmount = Conversion.ConvertObjToDouble(objReaderFunds.GetValue("AMOUNT"), m_iClientId);
                        objFundsTransSplit.SuppPaymentFlag = Conversion.ConvertObjToBool(objReaderFunds.GetValue("SUPP_PAYMENT_FLAG"), m_iClientId);
                        objFundsTransSplit.SplitRowId = Conversion.ConvertObjToInt(objReaderFunds.GetValue("SPLIT_ROW_ID"), m_iClientId); 

                        //igupta3
                        if (objFundsTransSplit.TransTypeCode != 0)
                        {
                            dNetAMount = objFunds.Amount;
                            iSplitId = objFundsTransSplit.SplitRowId;
                            sTranTypeCode = objFundsTransSplit.TransTypeCode.ToString();
                        }

                    }

                    objReaderFunds.Close();

                    //Trans_id and split_row_id is present in WORK_LOSS_MAPPING table, but split_row_is
                    //is in deleted status i.e. the transaction has been deleted;all such records will be 
                    //deleted:smishra25 end
                    //foreach (FundsTransSplit objTempFundsTransSplit in objFunds.TransSplitList)
                    //{
                    bWorkLossDelete = true;
                    bRestrictionsDelete = true;
                    string sSQL1 = "SELECT WRKLOS_RSTRCT_CODE FROM LT_RST_DAYS_MAP WHERE LINE_OF_BUS_CODE =" + iLOB + " AND TRANS_TYPE_CODE = " + sTranTypeCode;
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL1))
                    {
                        while (objReader.Read())//smishra25 MITS 18193,18212: replacing if statement with while;there can be two rows for same trans type
                        {
                            sWrkLossCode = objReader.GetString("WRKLOS_RSTRCT_CODE");
                            sFromDate = objFundsTransSplit.FromDate;
                            sToDate = objFundsTransSplit.ToDate;
                            //Added by Shivendu for MITS 18875
                            if (!string.IsNullOrEmpty(sFromDate) && !string.IsNullOrEmpty(sToDate))
                            {
                                saveWorkLossRestrictions(objFunds.ClaimId, objFunds.ClaimantEid, sFromDate, sToDate, sWrkLossCode, objFunds, iSplitId, dNetAMount);
                            }
                            if (sWrkLossCode.ToLower() == "work loss")
                            {
                                bWorkLossDelete = false;
                            }
                            else
                            {
                                bRestrictionsDelete = false;
                            }
                        }
                    }
                    //If this split_row_id is present in WORK_LOSS_MAPPING table,but the mapping has changed
                    //or transaction type has changed, the existing records should get deleted: smishra25 start
                    if (bRestrictionsDelete || bWorkLossDelete)
                    {
                        string sSQL2 = "SELECT * FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND SPLIT_ROW_ID = " + iSplitId.ToString();
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL2))
                        {

                            long lWorkLossRowId = 0;
                            long lRestrictRowId = 0;
                            while (objReader.Read())
                            {
                                lWorkLossRowId = 0;
                                lRestrictRowId = 0;
                                if (objReader.GetValue("PI_X_WL_ROW_ID") != null)
                                {
                                    bCastToTypeSuccess = false;
                                    lWorkLossRowId = Conversion.CastToType<long>(objReader.GetValue("PI_X_WL_ROW_ID").ToString(), out bCastToTypeSuccess);
                                    if (!bCastToTypeSuccess)
                                    {
                                        lWorkLossRowId = 0;
                                    }
                                }
                                if (objReader.GetValue("PI_RESTRICT_ROW_ID") != null)
                                {
                                    bCastToTypeSuccess = false;
                                    lRestrictRowId = Conversion.CastToType<long>(objReader.GetValue("PI_RESTRICT_ROW_ID").ToString(), out bCastToTypeSuccess);
                                    if (!bCastToTypeSuccess)
                                    {
                                        lRestrictRowId = 0;
                                    }
                                }
                                if (bWorkLossDelete)
                                {
                                    if (lWorkLossRowId != 0)
                                    {
                                        string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_WORK_LOSS WHERE PI_WL_ROW_ID = " + lWorkLossRowId).ToString();
                                        if (string.Compare(sRecordCount, "1") == 0)
                                        {
                                            objPiXWorkLoss.MoveTo((int)lWorkLossRowId);
                                            objPiXWorkLoss.Delete();
                                        }
                                        string sSQLWL = "DELETE FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND PI_X_WL_ROW_ID =" + lWorkLossRowId;
                                        objFunds.Context.DbConn.ExecuteScalar(sSQLWL);
                                    }
                                }
                                if (bRestrictionsDelete)
                                {
                                    if (lRestrictRowId != 0)
                                    {
                                        string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_RESTRICT WHERE PI_RESTRICT_ROW_ID = " + lRestrictRowId).ToString();
                                        if (string.Compare(sRecordCount, "1") == 0)
                                        {
                                            objPiXRestrict.MoveTo((int)lRestrictRowId);
                                            objPiXRestrict.Delete();
                                        }
                                        string sSQLRes = "DELETE FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND PI_RESTRICT_ROW_ID =" + lRestrictRowId;
                                        objFunds.Context.DbConn.ExecuteScalar(sSQLRes);
                                    }
                                }
                            }
                        }
                    }



                    //If this split_row_id is present in WORK_LOSS_MAPPING table,but the mapping has changed
                    //or transaction type has changed, the existing records should get deleted: smishra25 end
                    //}
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                //if (objClaim != null)
                //{
                //    objClaim.Dispose();
                //    objClaim = null;
                //}
                if (oSettings != null)
                {
                    oSettings = null;
                }
                if (objFunds != null)
                {
                    objFunds.Dispose();
                    objFunds = null;
                }
                if (objPiXWorkLoss != null)
                    objPiXWorkLoss.Dispose();
                if (objPiXRestrict != null)
                    objPiXRestrict.Dispose();
            }
            //end of R6 Auto Create Work Loss/Restrictions(Utilities Checked and Workloss/Restricton Screen auto created
            return true;
        }


        private void saveWorkLossRestrictions(int p_iClaimId, int p_iClaimantEid, string p_sfromdate, string p_stodate, string p_sWorkLossRestDesc, Funds objFunds, int SplitId, double NetAMount)
        {
            Claim objClaim = null;
            objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
            DateTime fromdate;
            DateTime todate;
            //Start:Added by Nitin Goel, MITS#23187,11/30/2010
            String sLastWorkDay;
            String sReturnToWork;
            DateTime dLastWorkDay;
            DateTime dReturnToWork;
            List<string> lstWLRowid;
            //End:Added by Nitin Goel, MITS#23187,11/30/2010
            int iClaimantEID = 0;
            int iPiRowID = 0;
            int iEventID = 0;
            int iDeptAssignEID = 0;
            PiXWorkLoss objPiXWorkLoss = null;
            PiXRestrict objPiXRestrict = null;
            //Claim objClaim = null;
            int iWorkLoss = 0;
            int iRestDays = 0;
            string sSQL = "";
            string[] sDaysOfWeek = new string[7];
            string sWorkFlags = string.Empty;
            bool bExistingWorkLoss = false;
            bool bExistingRestriction = false;
            bool bCastToTypeSuccess = false;
            long lWorkLossRowId = 0;
            long lRestrictRowId = 0;
            try
            {
                //Start:Added by Nitin Goel, MITS#23187,11/30/2010
                sLastWorkDay = string.Empty;
                sReturnToWork = string.Empty;
                lstWLRowid = new List<string>();
                //End:Added by Nitin Goel, MITS#23187,11/30/2010
                objPiXWorkLoss = (PiXWorkLoss)m_objDataModelFactory.GetDataModelObject("PiXWorkLoss", false);
                objPiXRestrict = (PiXRestrict)m_objDataModelFactory.GetDataModelObject("PiXRestrict", false);
                //Raman R7 Prf Imp : Feb 2 2011
                //objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (objClaim.IsNew)
                {
                    objClaim.MoveTo(p_iClaimId);
                }

                //Delete all the records if the payment is void:smishra25 start
                if (objFunds.VoidFlag == true)
                {

                    string sSQLVoid = "SELECT * FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString();
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLVoid))
                    {
                        //long lWorkLossRowId = 0;
                        //long lRestrictRowId = 0;
                        while (objReader.Read())
                        {
                            lWorkLossRowId = 0;
                            lRestrictRowId = 0;
                            if (objReader.GetValue("PI_X_WL_ROW_ID") != null)
                            {
                                bCastToTypeSuccess = false;
                                lWorkLossRowId = Conversion.CastToType<long>(objReader.GetValue("PI_X_WL_ROW_ID").ToString(), out bCastToTypeSuccess);
                                if (!bCastToTypeSuccess)
                                {
                                    lWorkLossRowId = 0;
                                }

                            }
                            if (objReader.GetValue("PI_RESTRICT_ROW_ID") != null)
                            {
                                bCastToTypeSuccess = false;
                                lRestrictRowId = Conversion.CastToType<long>(objReader.GetValue("PI_RESTRICT_ROW_ID").ToString(), out bCastToTypeSuccess);
                                if (!bCastToTypeSuccess)
                                {
                                    lRestrictRowId = 0;
                                }

                            }
                            if (lWorkLossRowId != 0)
                            {
                                string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_WORK_LOSS WHERE PI_WL_ROW_ID = " + lWorkLossRowId).ToString();
                                if (string.Compare(sRecordCount, "1") == 0)
                                {
                                    objPiXWorkLoss.MoveTo((int)lWorkLossRowId);
                                    objPiXWorkLoss.Delete();
                                }

                            }
                            else if (lRestrictRowId != 0)
                            {
                                string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_RESTRICT WHERE PI_RESTRICT_ROW_ID = " + lRestrictRowId).ToString();
                                if (string.Compare(sRecordCount, "1") == 0)
                                {
                                    objPiXRestrict.MoveTo((int)lRestrictRowId);
                                    objPiXRestrict.Delete();
                                }

                            }
                        }
                    }
                    string sSQLDel = "DELETE FROM WORK_LOSS_MAPPING WHERE  TRANS_ID =" + objFunds.TransId.ToString();
                    objFunds.Context.DbConn.ExecuteScalar(sSQLDel);

                }
                //Delete all the records if the payment is void:smishra25 End
                else
                {                    

                    PiEmployee objEmployee = objClaim.PrimaryPiEmployee as PiEmployee;

                    if (objEmployee != null)
                        sWorkFlags = objEmployee.WorkSunFlag + ","
                            + objEmployee.WorkMonFlag + ","
                            + objEmployee.WorkTueFlag + ","
                            + objEmployee.WorkWedFlag + ","
                            + objEmployee.WorkThuFlag + ","
                            + objEmployee.WorkFriFlag + ","
                            + objEmployee.WorkSatFlag;
                    sDaysOfWeek = sWorkFlags.Split(",".ToCharArray());
                    // Changed by Amitosh for Mits 22484 (02/16/2011)
                    ////fromdate1 = Riskmaster.Common.Conversion.ToDate(p_sfromdate).AddDays(-1);
                    ////todate1 = Riskmaster.Common.Conversion.ToDate(p_stodate).AddDays(1);
                    fromdate = Riskmaster.Common.Conversion.ToDate(p_sfromdate);
                    todate = Riskmaster.Common.Conversion.ToDate(p_stodate);
                    // end Amitosh
                    //Start:Added by Nitin Goel, MITS#23187,11/30/2010
                    sLastWorkDay = fromdate.AddDays(-1).ToString("yyyyMMdd");
                    sReturnToWork = todate.AddDays(1).ToString("yyyyMMdd");
                    ////calculate worklost days 
                    //iWorkLoss = GetWorkLossDay(dFromDate, dToDate, sDaysOfWeek);                
                    //End:Added by Nitin Goel, MITS#23187,11/30/2010
                    //calculate worklost days 
                    iWorkLoss = GetWorkLossDay(fromdate, todate, sDaysOfWeek);
                    //calculate restricted codes
                    //iRestDays = GetWorkDay(fromdate1, todate1, sDaysOfWeek);
                    iRestDays = GetWorkDay(Riskmaster.Common.Conversion.ToDate(p_sfromdate), Riskmaster.Common.Conversion.ToDate(p_stodate), sDaysOfWeek);

                    objPiXWorkLoss.PiWlRowId = -1;
                    objPiXRestrict.PiRestrictRowId = -1;

                    iClaimantEID = p_iClaimantEid;
                    iEventID = objClaim.EventId;
                    //Check if the split_row_id entry is already present in the WORK_LOSS_MAPPING
                    //table. smishra25: start
                    string sSQLGetExistingID = "SELECT * FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND SPLIT_ROW_ID =" + SplitId.ToString();

                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLGetExistingID))
                    {
                        //long lWorkLossRowId = 0;
                        //long lRestrictRowId = 0;
                        while (objReader.Read())
                        {
                            lWorkLossRowId = 0;
                            lRestrictRowId = 0;
                            if (p_sWorkLossRestDesc.ToLower() == "work loss")
                            {
                                if (objReader.GetValue("PI_X_WL_ROW_ID") != null)
                                {
                                    bCastToTypeSuccess = false;
                                    lWorkLossRowId = Conversion.CastToType<long>(objReader.GetValue("PI_X_WL_ROW_ID").ToString(), out bCastToTypeSuccess);
                                    if (!bCastToTypeSuccess)
                                    {
                                        lWorkLossRowId = 0;
                                    }

                                }
                                if (lWorkLossRowId != 0)
                                {
                                    bExistingWorkLoss = true;
                                    string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_WORK_LOSS WHERE PI_WL_ROW_ID = " + lWorkLossRowId).ToString();
                                    if (string.Compare(sRecordCount, "1") == 0)
                                    {
                                        objPiXWorkLoss.MoveTo((int)lWorkLossRowId);
                                    }

                                }
                            }
                            else
                            {
                                if (objReader.GetValue("PI_RESTRICT_ROW_ID") != null)
                                {
                                    bCastToTypeSuccess = false;
                                    lRestrictRowId = Conversion.CastToType<long>(objReader.GetValue("PI_RESTRICT_ROW_ID").ToString(), out bCastToTypeSuccess);
                                    if (!bCastToTypeSuccess)
                                    {
                                        lRestrictRowId = 0;
                                    }

                                }

                                if (lRestrictRowId != 0)
                                {
                                    bExistingRestriction = true;
                                    string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_RESTRICT WHERE PI_RESTRICT_ROW_ID = " + lRestrictRowId).ToString();
                                    if (string.Compare(sRecordCount, "1") == 0)
                                    {
                                        objPiXRestrict.MoveTo((int)lRestrictRowId);
                                    }

                                }
                            }
                        }
                    }
                    //Check if the split_row_id entry is already present in the WORK_LOSS_MAPPING
                    //table. smishra25: End

                    sSQL = "select pi_row_id ,dept_assigned_eid from person_involved where pi_eid =" + iClaimantEID + "and event_id=" + iEventID;
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {

                        while (objReader.Read())
                        {
                            iPiRowID = objReader.GetInt32("pi_row_id");
                            iDeptAssignEID = objReader.GetInt32("dept_assigned_eid");


                            if (p_sWorkLossRestDesc.ToLower() == "work loss")
                            {

                                if (!bExistingWorkLoss)
                                {
                                    // Changed by Amitosh for Mits 22484 (02/16/2011)
                                    //if (ValidateWorkLoss(objEmployee, objPiXWorkLoss, p_sfromdate, p_stodate, objTempFundsTransSplit.SumAmount))
                                    if (ValidateWorkLoss(objEmployee, objPiXWorkLoss, ref sLastWorkDay, ref sReturnToWork, NetAMount, ref lstWLRowid))
                                    {
                                        objPiXWorkLoss.PiRowId = iPiRowID;

                                        // Changed by Amitosh for Mits 22484 (02/16/2011)
                                        //  objPiXWorkLoss.DateLastWorked = p_sfromdate;
                                        //    objPiXWorkLoss.DateReturned = p_stodate;
                                        objPiXWorkLoss.DateLastWorked = sLastWorkDay;
                                        objPiXWorkLoss.DateReturned = sReturnToWork;
                                        dLastWorkDay = Riskmaster.Common.Conversion.ToDate(sLastWorkDay);
                                        dReturnToWork = Riskmaster.Common.Conversion.ToDate(sReturnToWork);
                                        iWorkLoss = GetWorkLossDay(dLastWorkDay, dReturnToWork, sDaysOfWeek);
                                        //End:Added by Nitin Goel, MITS#23187,11/30/2010

                                        objPiXWorkLoss.Duration = iWorkLoss;
                                        objPiXWorkLoss.StateDuration = iWorkLoss;
                                        objPiXWorkLoss.Save();

                                        //Insert the record in WORK_LOSS_MAPPING table:smishra25 start
                                        objFunds.Context.DbConn.ExecuteScalar("INSERT INTO WORK_LOSS_MAPPING(TRANS_ID,SPLIT_ROW_ID,PI_X_WL_ROW_ID,PI_RESTRICT_ROW_ID) VALUES(" + objFunds.TransId.ToString() + "," + SplitId.ToString() + "," + objPiXWorkLoss.PiWlRowId.ToString() + ",0)");
                                        //Insert the record in WORK_LOSS_MAPPING table:smishra25 End
                                        //Start:Added by Nitin Goel, MITS#23187,11/30/2010
                                        if (lstWLRowid.Count > 0)
                                        {
                                            UpdateExtraWorkLossRecord(lstWLRowid, lWorkLossRowId);
                                        }
                                        //End:Added by Nitin Goel, MITS#23187,11/30/2010
                                    }

                                }
                               
                            }

                            else
                            {

                                if (!bExistingRestriction)
                                {
                                    if (ValidateRestrictions(objEmployee, objPiXRestrict, p_sfromdate, p_stodate, NetAMount))
                                    {

                                        objPiXRestrict.PiRowId = iPiRowID;
                                        objPiXRestrict.DateFirstRestrct = p_sfromdate;
                                        objPiXRestrict.DateLastRestrct = p_stodate;
                                        objPiXRestrict.Duration = iRestDays;
                                        objPiXRestrict.Save();

                                        //Insert the record in WORK_LOSS_MAPPING table:smishra25 start
                                        objFunds.Context.DbConn.ExecuteScalar("INSERT INTO WORK_LOSS_MAPPING(TRANS_ID,SPLIT_ROW_ID,PI_RESTRICT_ROW_ID,PI_X_WL_ROW_ID) VALUES(" + objFunds.TransId.ToString() + "," + SplitId.ToString() + "," + objPiXRestrict.PiRestrictRowId.ToString() + ",0)");
                                        //Insert the record in WORK_LOSS_MAPPING table:smishra25 End
                                    }
                                }
                               
                            }
                        }
                    }
                }
            }



            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }

            finally
            {

                if (objPiXWorkLoss != null)
                    objPiXWorkLoss.Dispose();
                if (objPiXRestrict != null)
                    objPiXRestrict.Dispose();
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    //objClaim = null;
                }
            }


        }


        private int GetWorkLossDay(DateTime sLastWorkDate, DateTime sReturnedToWorkDate, string[] sDaysOfWeek)
        {
            DateTime objLastWorkedDate = System.DateTime.Today;
            DateTime objReturnedToWorkDate = System.DateTime.Today;
            TimeSpan objDays;
            int iDayOfWeekStart = 0;
            int iDayOfWeekEnd = 0;
            int iNewDays = 0;
            int iDays = 0;
            string sParent = string.Empty;

            objLastWorkedDate = sLastWorkDate.AddDays(1);
            objReturnedToWorkDate = sReturnedToWorkDate;
            objDays = sReturnedToWorkDate.Subtract(objLastWorkedDate);
            iDays = objDays.Days;
            iDayOfWeekStart = (int)objLastWorkedDate.DayOfWeek;
            iDayOfWeekEnd = (int)objReturnedToWorkDate.DayOfWeek;

            for (int i = 1; i <= iDays; i++)
            {
                if (sDaysOfWeek[(int)objLastWorkedDate.AddDays(i - 1).DayOfWeek] == "True")
                    iNewDays = iNewDays + 1;
            }

            return iNewDays;

        }

        private int GetWorkDay(DateTime dDate1, DateTime dDate2, string[] sDaysOfWeek)
        {
            int iDays = 0;
            int iDayOfWeekStart = 0;
            int iDayOfWeekEnd = 0;
            int iNewDays = 0;
            string sParent = string.Empty;
            TimeSpan objDays;

            objDays = dDate2.Subtract(dDate1);
            iDays = objDays.Days + 1;
            iDayOfWeekStart = (int)dDate1.DayOfWeek;
            iDayOfWeekEnd = (int)dDate2.DayOfWeek;
            for (int i = 1; i <= iDays; i++)
            {
                if (sDaysOfWeek[(int)dDate1.AddDays(i - 1).DayOfWeek] == "True")
                    iNewDays = iNewDays + 1;
            }

            return iNewDays;
        }

        public bool ValidateWorkLoss(PiEmployee objEmployee, PiXWorkLoss objPiXWorkLoss, ref string p_sDateLastWorked, ref string p_sDateReturned, double p_SumAmount, ref List<string> lstWLRowid)
        {
            //DateTime dtReturnDate;
            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            string sWarningMessage = string.Empty;
            //Start:Added by nitin goel,12/14/2010,mits 23187
            DateTime dtListLastWorked;
            DateTime dtListReturnDate;
            DateTime dtLastWorked;
            DateTime dtReturnDate;
            bool bValFlag = false;

            if (p_sDateLastWorked != "")
            {
                if (p_sDateLastWorked.CompareTo(sToday) > 0)
                {
                    sWarningMessage = "Since Last Worked Date > Today's Date, a Work Loss record will not be created/updated for the transaction with amount " + p_SumAmount.ToString();
                    m_arrlstWarnings.Add("Since Last Worked Date > Today's Date, a Work Loss record will not be created/updated for the transaction with amount " + p_SumAmount.ToString());
                    // Errors.Add("WarningScriptError", sWarningMessage, BusinessAdaptorErrorType.Warning);
                    return false;
                }

                if (p_sDateReturned != "")
                {
                    if (p_sDateLastWorked.CompareTo(p_sDateReturned) > 0)
                    {
                        sWarningMessage = "Since Last Worked Date > Date Returned, a Work Loss record will not be created/updated for the transaction with amount " + p_SumAmount.ToString();
                        m_arrlstWarnings.Add("Since Last Worked Date > Date Returned, a Work Loss record will not be created/updated for the transaction with amount " + p_SumAmount.ToString());
                        // Errors.Add("WarningScriptError", sWarningMessage, BusinessAdaptorErrorType.Warning);

                        return false;
                    }
                }
            }

            int iWorkLossId = 0;
            int iWorkLossIdNextTemp = 0;
            int iWorkLossIdNext = 0;
            PiXWorkLossList objPiWorkLossList = objEmployee.PiXWorkLossList;
            dtLastWorked = Conversion.ToDate(p_sDateLastWorked);
            dtReturnDate = Conversion.ToDate(p_sDateReturned);

            if (objEmployee.PiXWorkLossList.Count > 0)
            {
                foreach (PiXWorkLoss objWorkLossforID in objPiWorkLossList)
                {

                    iWorkLossId = objWorkLossforID.PiWlRowId;
                    if (objPiXWorkLoss.PiWlRowId <= 0)
                        break;
                    else if (iWorkLossId < objPiXWorkLoss.PiWlRowId)
                        break;

                }
                objPiWorkLossList = objEmployee.PiXWorkLossList;

                //if (objPiXWorkLoss.PiWlRowId > 0)  commented for MITS 25713: rsushilaggar
                if (iWorkLossId > 0)  // added : rsushilaggar MITS 25713
                {
                    foreach (PiXWorkLoss objWorkLossforID in objPiWorkLossList)
                    {
                        iWorkLossIdNextTemp = objWorkLossforID.PiWlRowId;
                        dtListLastWorked = Conversion.ToDate(objPiWorkLossList[iWorkLossIdNextTemp].DateLastWorked);
                        dtListReturnDate = Conversion.ToDate(objPiWorkLossList[iWorkLossIdNextTemp].DateReturned);

                        if ((dtLastWorked.CompareTo(dtListReturnDate) < 0) && (dtReturnDate.CompareTo(dtListLastWorked) > 0))
                        {
                            if ((dtLastWorked.CompareTo(dtListLastWorked) == 0) && (dtReturnDate.CompareTo(dtListReturnDate) == 0))
                            {
                                bValFlag = true;
                                break;
                            }
                            else if ((dtLastWorked.CompareTo(dtListLastWorked) > 0) && (dtReturnDate.CompareTo(dtListReturnDate) < 0))
                            {
                                bValFlag = true;
                                break;
                            }
                            if (dtLastWorked.CompareTo(dtListLastWorked) > 0)
                            {
                                p_sDateLastWorked = dtListLastWorked.ToString("yyyyMMdd");
                            }
                            if (dtReturnDate.CompareTo(dtListReturnDate) < 0)
                            {
                                p_sDateReturned = dtListReturnDate.ToString("yyyyMMdd");
                            }
                            lstWLRowid.Add(objWorkLossforID.PiWlRowId.ToString());
                        }
                    }
                }

            }

            // Return true if there were no validation errors
            if (bValFlag == true)
            {
                m_arrlstWarnings.Add("A Work Loss record already exists with in this time frame.");
                return false;

            }
            return true;
        }


        public void UpdateExtraWorkLossRecord(List<string> lstWLRowid, long lWorkLossRowId)
        {
            string[] arrWLRowid = lstWLRowid.ToArray();
            string sWLRowid = string.Join(",", arrWLRowid);
            try
            {
                using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    objConn.Open();
                    objConn.ExecuteNonQuery("DELETE FROM PI_X_WORK_LOSS WHERE PI_WL_ROW_ID IN(" + sWLRowid + ")");
                    objConn.ExecuteNonQuery("UPDATE WORK_LOSS_MAPPING SET PI_X_WL_ROW_ID =" + lWorkLossRowId + " WHERE PI_X_WL_ROW_ID IN(" + sWLRowid + ")");
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
        }

        public bool ValidateRestrictions(PiEmployee objEmployee, PiXRestrict objPiXRestrict, string p_DateFirstRestrct, string p_DateLastRestrct, double p_SumAmount)
        {
            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            string sWarningMessage = string.Empty;
            // Perform data validation
            DateTime dtFirstRestrict;
            DateTime dtLastRestrict;

            if (p_DateFirstRestrct.CompareTo(sToday) > 0)
            {
                sWarningMessage = "Since First Restricted Date > Today's Date, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString();
                m_arrlstWarnings.Add("Since First Restricted Date > Today's Date, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString());
                //Errors.Add("WarningScriptError", sWarningMessage, BusinessAdaptorErrorType.Warning);
                return false;
            }

            if (p_DateLastRestrct.Length > 0)
            {
                if (p_DateFirstRestrct.CompareTo(p_DateLastRestrct) > 0)
                {
                    sWarningMessage = "Since First Restricted Date >  Last Restricted Date, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString();
                    m_arrlstWarnings.Add("Since First Restricted Date >  Last Restricted Date, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString());
                    // Errors.Add("WarningScriptError", sWarningMessage, BusinessAdaptorErrorType.Warning);
                    return false;
                }
            }


            int iRestrictId = 0;
            int iRestrictIdNextTemp = 0;
            int iRestrictIdNext = 0;
            PiXRestrictList objPiRestrictList = objEmployee.PiXRestrictList;

            if (objEmployee.PiXRestrictList.Count > 0)
            {
                foreach (PiXRestrict objRestrict in objPiRestrictList)
                {
                    iRestrictId = objRestrict.PiRestrictRowId;
                    if (objPiXRestrict.PiRestrictRowId <= 0)
                        break;
                    else if (iRestrictId < objPiXRestrict.PiRestrictRowId)
                        break;

                }
                objPiRestrictList = objEmployee.PiXRestrictList;
                if (objPiXRestrict.PiRestrictRowId > 0)
                {
                    foreach (PiXRestrict objRestrict in objPiRestrictList)
                    {
                        iRestrictIdNextTemp = objRestrict.PiRestrictRowId;
                        if (iRestrictIdNextTemp > objPiXRestrict.PiRestrictRowId)
                        {
                            iRestrictIdNext = iRestrictIdNextTemp;
                        }
                        if (iRestrictIdNextTemp == objPiXRestrict.PiRestrictRowId)
                        {
                            if (iRestrictIdNext == 0)
                            {
                                iRestrictIdNext = iRestrictIdNextTemp;
                            }
                            break;
                        }

                    }
                }
                if (objPiXRestrict.PiRestrictRowId != iRestrictId)
                {
                    dtFirstRestrict = Conversion.ToDate(p_DateFirstRestrct);
                    dtLastRestrict = Conversion.ToDate(objPiRestrictList[iRestrictId].DateLastRestrct);
                    if (dtFirstRestrict.CompareTo(dtLastRestrict) <= 0)
                    {
                        sWarningMessage = "You have entered overlapping Restricted dates, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString();
                        m_arrlstWarnings.Add("You have entered overlapping Restricted dates, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString());
                        // Errors.Add("WarningScriptError", sWarningMessage, BusinessAdaptorErrorType.Warning);
                        return false;
                    }
                }
                if (objPiXRestrict.PiRestrictRowId != iRestrictIdNext)
                {
                    if (objPiXRestrict.PiRestrictRowId > 0)
                    {
                        objPiRestrictList = objEmployee.PiXRestrictList;
                        dtFirstRestrict = Conversion.ToDate(objPiRestrictList[iRestrictIdNext].DateFirstRestrct);
                        dtLastRestrict = Conversion.ToDate(p_DateLastRestrct);
                        if (dtFirstRestrict.CompareTo(dtLastRestrict) <= 0)
                        {
                            sWarningMessage = "You have entered overlapping Restricted dates, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString();
                            m_arrlstWarnings.Add("You have entered overlapping Restricted dates, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString());
                            //  Errors.Add("WarningScriptError", sWarningMessage, BusinessAdaptorErrorType.Warning);
                            return false;
                        }
                    }
                }

            }
            // Return true if there were no validation errors
            return true;
        }

        //igupta3 Mits : 28566 changes ends

        public string GetUniqueCtlNum(string sUniqueCtlNum)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            int iCount = 0;
            bool bUnique = false;
            string sUnique = sUniqueCtlNum;

            try
            {
                while (!bUnique)
                {
                    sSQL = "SELECT CTL_NUMBER FROM FUNDS WHERE CTL_NUMBER = '" + sUnique + "'";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader.Read())
                    {
                        iCount = iCount + 1;
                        sUnique = sUniqueCtlNum + "-D-" + iCount.ToString();
                        bUnique = false;
                    }
                    else
                    {
                        bUnique = true;
                    }

                    objReader.Close();
                }

            }
            catch (Exception p_objEx)
            {
                //TODO    throw new RMAppException(Globalization.GetString("NonOccPaymentsManager.GetPrintedPayments.Error"), p_objEx);
            }
            finally
            {
                if (objReader != null)
                    objReader.Close();
            }

            return sUnique;
        }

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
                m_objDataModelFactory.Dispose();

            if (m_objCache != null)
                m_objCache.Dispose();
        }

   	#endregion
	}
}
