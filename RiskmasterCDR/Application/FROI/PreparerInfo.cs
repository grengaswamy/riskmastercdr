﻿
using System;
using System.Xml ;

using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Application.ExtenderLib ;

namespace Riskmaster.Application.FROI
{
	/**************************************************************
	 * $File		: PreparerInfo.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 02/02/2005
	 * $Author		: Vaibhav Kaushik
	 * $Comment		: Class exposes various methods for Preparer Information. 
	 * $Source		:  	
	**************************************************************/
	public class PreparerInfo
	{
		#region Constants
		/// <summary>
		/// Constant for SkipPreparerPrompt Flag node in configuration file.
		/// </summary>
		internal const string PREPARER_SKIP_PROMPT_FLAG = "FROI_SkipPreparerPrompt" ;
		/// <summary>
		/// Constant for PreparerName node in configuration file.
		/// </summary>
		internal const string PREPARER_NAME = "FROI_PreparerName" ;
		/// <summary>
		/// Constant for PreparerTitle node in configuration file.
		/// </summary>
		internal const string PREPARER_TITLE = "FROI_PreparerTitle" ;
		/// <summary>
		/// Constant for PreparerPhone node in configuration file.
		/// </summary>
		internal const string PREPARER_PHONE = "FROI_PreparerPhone" ;
		#endregion 

		#region Variable Declaration 
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of data model factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;		
		/// <summary>
		/// Private variable to store User Id
		/// </summary>
		private int m_iUserId = 0 ;
		/// <summary>
		/// Private variable for Preparer Source
		/// </summary>
		private PreparerSource m_enumPreparerSource = PreparerSource.None ;
		/// <summary>
		/// Private variable for store Preparer Name
		/// </summary>
		private string m_sName = "" ;
		/// <summary>
		/// Private variable for Preparer Title
		/// </summary>
		private string m_sTitle = "" ;
		/// <summary>
		/// Private variable for Preparer Phone
		/// </summary>
		private string m_sPhone = "" ;
		/// <summary>
		/// Private variable for Skipping Preparer Prompt.
		/// </summary>
		private bool m_bSkipPrompt = false ;
		/// <summary>
		/// Private variable for Preparer information Saving Required.
		/// </summary>
		private bool m_bSaveRequired = false ;
		/// <summary>
		/// Enumeration for Preparer Source.
		/// </summary>
		private enum PreparerSource : int
		{
			None = 2,
			ConfigFile = 4,
			UserXML = 8,
			SystemXML = 12,
		}

		#endregion 

		#region Constructor
		/// Name		: PreparerInfo
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	Constructor, initializes the variables to the default value
		///	It also Load the Preparer Info XML.
		/// </summary>
		/// <param name="p_sDsnName">DsnName</param>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_iUserId">User Id</param>
		public PreparerInfo( string p_sDsnName , string p_sUserName , string p_sPassword , int p_iUserId)
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			m_iUserId = p_iUserId ;
			m_objDataModelFactory = new DataModel.DataModelFactory( m_sDsnName , m_sUserName , m_sPassword );	
			m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
			if( ! this.LoadPreparerInfoXMLFromDb() )
			{
				this.LoadPreparerInfoFromConfig();
				m_bSaveRequired = true ;
			}
		}		

		/// Name		: LoadPreparerInfoXMLFromDb
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Load the Preparer Info XML from database.
		/// </summary>
		/// <returns>Return True if succeeded</returns>
		private bool LoadPreparerInfoXMLFromDb()
		{
			DbReader objReader = null ;
			XmlDocument objXmlDocument = null ;
			XmlElement objFROIOptionsNode = null ;

			int iUserId = 0 ;
			string sPrefXml = "" ;
			string sSQL = "" ;
			bool bReturnValue = false ;
			bool bLoadSuccess = false ; 

			try
			{
				sSQL = " SELECT USER_ID, PREF_XML FROM USER_PREF_XML WHERE USER_ID IN (" + m_iUserId + ",-1) ORDER BY USER_ID DESC" ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );

				if( objReader.Read())
				{
					iUserId = Conversion.ConvertObjToInt( objReader.GetValue( "USER_ID" ), m_iClientId );
					sPrefXml = objReader.GetString( "PREF_XML" );
					
					// We could be getting a "default system setting" (if USER_ID==-1), 
					// figure out who's preferences we're looking at...
					if( iUserId == -1 )
						m_enumPreparerSource = PreparerSource.SystemXML ;
					else
						m_enumPreparerSource = PreparerSource.UserXML ;

				
					objXmlDocument = new XmlDocument();
					try
					{
						objXmlDocument.LoadXml( sPrefXml );
						bLoadSuccess = true ;
					}
					catch
					{
						bLoadSuccess = false ;
					}
					
					if( bLoadSuccess )
					{						
						objFROIOptionsNode = ( XmlElement ) objXmlDocument.SelectSingleNode( "//FROIOptions" );
						if( objFROIOptionsNode != null )
						{
							m_sPhone = objFROIOptionsNode.GetAttribute( "phone" );
							m_sTitle = objFROIOptionsNode.GetAttribute( "title" );
							m_sName = objFROIOptionsNode.GetAttribute( "name" ); 
							bReturnValue = true ;
						}
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PreparerInfo.LoadPreparerInfoXMLFromDb.Error") , p_objEx );				
			}
			finally
			{
				if( objReader != null )
					objReader.Dispose();

				objXmlDocument = null ;
				objFROIOptionsNode = null ;				
			}
			return( bReturnValue );
		}

		/// Name		: LoadPreparerInfoFromConfig
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Load the Preparer Info XML from Configuration File.
		/// </summary>
		private void LoadPreparerInfoFromConfig()
		{
			RMConfigurator objConfig = null;
			string sTemp = "" ;
			
			try
			{
				objConfig = new RMConfigurator();

				m_sName = objConfig.GetValue( PREPARER_NAME ) ;
				m_sTitle = objConfig.GetValue( PREPARER_TITLE );
				m_sPhone = objConfig.GetValue( PREPARER_PHONE);
				sTemp = objConfig.GetValue( PREPARER_SKIP_PROMPT_FLAG );
			
				if( sTemp != null )
					m_bSkipPrompt = sTemp.ToLower() == "true" ;
				else
					m_bSkipPrompt = false ;
				
				// If Configuration file does not have the Preparer Name information then set it to blank.
				if( m_sName == null )
					m_sName = "" ;
				// If Configuration file does not have the Preparer Title information then set it to blank.
				if( m_sTitle == null )
					m_sTitle = "" ;
				// If Configuration file does not have the Preparer Phone information then set it to blank.
				if( m_sPhone == null )
					m_sTitle = "" ;
				
				// Set Config File as the Preparer source .
				m_enumPreparerSource = PreparerSource.ConfigFile ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PreparerInfo.LoadPreparerInfoFromConfig.Error") , p_objEx );				
			}
			finally
			{
				objConfig = null ;		
			}			
		}

		
		#endregion 

		#region Dispose
		/// Name		: Dispose
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Dispose the objects.
		/// It also save the Preparer info if required.
		/// </summary>
		public void Dispose()
		{
			try
			{
				if( m_bSaveRequired )
					this.SavePreparerInfoXML();

				if( m_objDataModelFactory != null )
				{
					m_objDataModelFactory.UnInitialize();
					m_objDataModelFactory = null ;	
				}
			}
			catch
			{
				// Avoid raising any exception here.				
			}
		}
		#endregion 

		#region Save Preparer Info.
		/// Name		: Save
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Save the Preparer info. 
		/// </summary>
		/// <param name="p_sPhone">Preparer Phone</param>
		/// <param name="p_sTitle">Preparer Title</param>
		/// <param name="p_sName">Preparer Name</param>
		public void Save( string p_sPhone , string p_sTitle , string p_sName)
		{
			try
			{
				m_sPhone = p_sPhone ;
				m_sTitle = p_sTitle ;
				m_sName = p_sName ;
				this.SavePreparerInfoXML();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PreparerInfo.Save.Error") , p_objEx );				
			}
		}

		/// Name		: SavePreparerInfoXML
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Save the Preparer info if required.
		/// </summary>
		private void SavePreparerInfoXML()
		{
			DbReader objReader = null ;
			DbConnection objConn = null ;
			XmlDocument objXmlDocument = null ;
			XmlElement objFROIOptionsNode = null ;
			XmlElement objRootNode = null ;

			string sPrefXml = "" ;
			string sSQL = "" ;
			bool bUpdate = false ;

			try
			{
				objXmlDocument = new XmlDocument();

				sSQL = " SELECT * FROM USER_PREF_XML WHERE USER_ID =" + m_iUserId ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader.Read())
				{
					sPrefXml = objReader.GetString( "PREF_XML" );
					bUpdate = true ;
				}
				
				if( bUpdate )
				{				
					try
					{
						objXmlDocument.LoadXml( sPrefXml.Trim() );
						objRootNode = objXmlDocument.DocumentElement ;
					}
					catch
					{
						objXmlDocument = new XmlDocument();
						objRootNode = objXmlDocument.CreateElement( "setting" );
						objXmlDocument.AppendChild( objRootNode );
					}
					 
					objFROIOptionsNode = ( XmlElement ) objXmlDocument.SelectSingleNode( "//FROIOptions" );
					if( objFROIOptionsNode == null )
					{
						objFROIOptionsNode = objXmlDocument.CreateElement( "FROIOptions" );
						objRootNode.AppendChild( objFROIOptionsNode );
					}					
				}
				else
				{	
					objXmlDocument = new XmlDocument();
					objRootNode = objXmlDocument.CreateElement( "setting" );
					objXmlDocument.AppendChild( objRootNode );
					objFROIOptionsNode = objXmlDocument.CreateElement( "FROIOptions" );
					objRootNode.AppendChild( objFROIOptionsNode );					
				}

				objFROIOptionsNode.SetAttribute( "phone" , m_sPhone );
				objFROIOptionsNode.SetAttribute( "title" , m_sTitle );
				objFROIOptionsNode.SetAttribute( "name" , m_sName ); 
				objFROIOptionsNode.SetAttribute( "skipprompt" , m_bSkipPrompt.ToString() );

				sPrefXml = objXmlDocument.OuterXml ;

				if( bUpdate )
					sSQL = " UPDATE USER_PREF_XML SET PREF_XML = '" + sPrefXml + "' WHERE USER_ID =" + m_iUserId ;
				else
					sSQL = " INSERT INTO USER_PREF_XML( PREF_XML , USER_ID ) VALUES( '" + sPrefXml + "' , " 
							+ m_iUserId + " ) " ;

				objConn = DbFactory.GetDbConnection( m_sConnectionString );
				objConn.Open();				
				objConn.ExecuteNonQuery( sSQL );
				objConn.Close();
				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PreparerInfo.SavePreparerInfoXML.Error") , p_objEx );				
			}
			finally
			{
				if( objReader != null )
					objReader.Dispose();
				
				if( objConn != null )
					objConn.Dispose();

				objXmlDocument = null ;
				objFROIOptionsNode = null ;	
				objRootNode = null ;
			}			
		}


		#endregion 
			
		#region Skip Preparer Prompt

		/// Name		: SkipJurisPreparerPrompt
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Return true if Juris Preparer Prompt need to skip.
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <param name="p_iSource">Return Source of the Preparer Info</param>
		/// <param name="p_sName">Return Preparer Name</param>
		/// <param name="p_sTitle">Return Preparer Title</param>
		/// <param name="p_sPhone">Return Preparer Phone</param>
		/// <returns>True/False</returns>
		public bool SkipJurisPreparerPrompt( int p_iClaimId , ref int p_iSource , ref string p_sName , 
			ref string p_sTitle , ref string p_sPhone )
		{
			bool bReturnValue = false ;

			try
			{
				bReturnValue = this.SkipPreparerPrompt( p_iClaimId , true ) ;
				p_iSource = (int) m_enumPreparerSource ;
				p_sName = m_sName ;
				p_sTitle = m_sTitle ;
				p_sPhone = m_sPhone ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PreparerInfo.SkipJurisPreparerPrompt.Error") , p_objEx );				
			}
			return( bReturnValue );
		}

		/// Name		: SkipFroiPreparerPrompt
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Return true if FROI Preparer Prompt need to skip.
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <param name="p_iSource">Return Source of the Preparer Info</param>
		/// <param name="p_sName">Return Preparer Name</param>
		/// <param name="p_sTitle">Return Preparer Title</param>
		/// <param name="p_sPhone">Return Preparer Phone</param>
		/// <returns>True/False</returns>
		public bool SkipFroiPreparerPrompt( int p_iClaimId , ref int p_iSource , ref string p_sName , 
			ref string p_sTitle , ref string p_sPhone )
		{
			bool bReturnValue = false ;

			try
			{
				bReturnValue = this.SkipPreparerPrompt( p_iClaimId , false ) ;
				p_iSource = (int) m_enumPreparerSource ;
				p_sName = m_sName ;
				p_sTitle = m_sTitle ;
				p_sPhone = m_sPhone ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PreparerInfo.SkipFroiPreparerPrompt.Error") , p_objEx );				
			}
			return( bReturnValue );
		}

		/// Name		: SkipPreparerPrompt
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Return true if Preparer Prompt need to skip.
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <param name="p_bIsJuris">Is Juris Function Called Flag?</param>
		/// <returns>True/False</returns>
		private bool SkipPreparerPrompt( int p_iClaimId , bool p_bIsJuris )
		{
			Populater objPopulater = null ;

			int iPreparerSourceSetting = 0 ;
			bool bReturnValue = true ;

			try
			{
				objPopulater = new Populater( m_sUserName , m_sPassword , m_sDsnName );
				
				if( p_bIsJuris )
					iPreparerSourceSetting = objPopulater.GetJURISPreparerSource( p_iClaimId );
				else
					iPreparerSourceSetting = objPopulater.GetFROIPreparerSource( p_iClaimId );

				if( iPreparerSourceSetting == 1 )
					if( ! m_bSkipPrompt )
						if( m_enumPreparerSource == PreparerSource.UserXML || m_enumPreparerSource == PreparerSource.ConfigFile )
							bReturnValue = false ;									
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PreparerInfo.SkipPreparerPrompt.Error") , p_objEx );				
			}
			finally
			{
				if( objPopulater != null )
				{
					objPopulater.Dispose();
					objPopulater = null ;
				}
			}
			return( bReturnValue );
		}
			
		
		#endregion 
	}
}
