using System;
using Riskmaster.Application.ReportInterfaces;
using System.Collections;
using System.Xml;
namespace Riskmaster.Application.OSHALib
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004
	/// Purpose :  This interface has to be implemented by each type of report.	
	/// </summary>
	public interface IOSHAReport
	{
		string UserFileName
		{
			get;
			set;
		}
		DateTime StartTime
		{
			get;
			set;
		}
		DateTime BeginDate
		{
			get;
			set;
		}
		DateTime EndDate
		{
			get;
			set;
		}
		bool EventBasedFlag
		{
			get;
			set;
		}
		bool PrimaryLocationFlag
		{
			get;
			set;
		}
		bool PrintOSHADescFlag
		{
			get;
			set;
		}
		bool AllEntitiesFlag
		{
			get;
			set;
		}
		int UseReportLevel
		{
			get;
			set;
		}
		int ReportLevel
		{
			get;
			set;
		}
		int ReportOn
		{
			get;
			set;
		}
		string YearOfReport
		{
			get;
			set;
		}
		bool Enforce180DayRule
		{
			get;
			set;
		}
		DateTime AsOfDate
		{
			get;
			set;
		}
		int ItemSortOrder
		{
			get;
			set;
		}
        //MITS 37193 begins
        int ColumnESource
        {
            get;
            set;
        }
        //MITS 37193 Ends

		int ColumnFSource
		{
			get;
			set;
		}
		string EstablishmentNamePrefix
		{
			get;
			set;
		}
		bool ByOSHAEstablishmentFlag
		{
			get;
			set;
		}
		int PITypeEmployee
		{
			get;
		}         
		ColError Errors
		{
			get;
		}
		EntityItem Company
		{
			get;
		}
		IWorkerNotifications Notify
		{
			get;
			set;
		}
		InstanceUtilities Context
		{
			get;
		}
		
		ArrayList Items
		{
			get;
		}
		XmlDocument CriteriaXmlDom
		{
			get;
			set;
		}
    
		ArrayList SelectedEntities
		{
			get;
			set;
		}
		bool PrintSoftErrLog
		{
			get;
			set;
		}
		
		void LoadEntities();
		void Init();
		string Dsn
		{
			get;
			set;

		}
		string sUseReportLevel
		{
			get;
		}

		string sReportLevel
		{
			get;
		}
	}
}
