using System;

namespace Riskmaster.Application.OSHALib
{
	
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004
	/// Purpose :  This class is framed on publish-subscription model, basically will be raising the events
	///			   when the reader state has to be disposed off. Creator objects will subscribe to these events.
	///            When the state of the reader in creator object goes to false, that the corresponding event handler will close reader.
	/// </summary>
	internal class StateInformerForDbReaders
	{
		#region Member Variables
		/// <summary>
		/// Flag that identify whether data for body part exist
		/// </summary>
		internal bool m_bCanReaderReadBodyParts=true;
		/// <summary>
		/// Flag that identify whether data for work loss exist
		/// </summary>
		internal bool m_bCanReaderReadWorkLoss=true;
		/// <summary>
		/// Flag that identify whether data for restricted days exist
		/// </summary>
		internal bool m_bCanReaderReadRestrictedDays=true;
		/// <summary>
		/// Flag that identify whether data for Physician exist
		/// </summary>
		internal bool m_bCanReaderReadLoadPhysician=true;
		/// <summary>
		/// Flag that identify whether data for Hospital exist
		/// </summary>
		internal bool m_bCanReaderReadLoadHospital=true;
		/// <summary>
		/// Flag that identify whether data for Treatment exist
		/// </summary>
		internal bool m_bCanReaderReadLoadTreatments=true;
		/// <summary>
		/// Flag that identify whether data for Injury exist
		/// </summary>
		internal bool m_bCanReaderReadLoadInjuries=true;
		/// <summary>
		/// Delegate that will identify which Data Reader needs to be closed
		/// </summary>
		internal delegate void Notification();
		/// <summary>
		/// Event determining that Data Reader opened for Body part data has to be closed
		/// </summary>
		internal event Notification CloseReaderForBodyPart;
		/// <summary>
		/// Event determining that Data Reader opened for Work Loss data has to be closed
		/// </summary>
		internal event Notification CloseReaderForWorkLoss;
		/// <summary>
		/// Event determining that Data Reader opened for Restricted days data has to be closed
		/// </summary>
		internal event Notification CloseReaderRestrictedDays;
		/// <summary>
		/// Event determining that Data Reader opened for Physician data has to be closed
		/// </summary>
		internal event Notification CloseReaderLoadPhysician;
		/// <summary>
		/// Event determining that Data Reader opened for Hospital data has to be closed
		/// </summary>
		internal event Notification CloseReaderLoadHospital;
		/// <summary>
		/// Event determining that Data Reader opened for Treatment data has to be closed
		/// </summary>
		internal event Notification CloseReaderLoadTreatments;
		/// <summary>
		/// Event determining that Data Reader opened for Injury data has to be closed
		/// </summary>
		internal event Notification CloseReaderLoadInjuries;
		#endregion		

		#region Properties
		/// <summary>
		/// Property accesses the flag that identify whether data for body part exist.
		/// It raises the event to close the data reader opened for reading body part in case the flag is false.
		/// </summary>
		internal bool CanReaderReadBodyParts
		{
			get
			{
				return m_bCanReaderReadBodyParts;
			}
			set
			{
                m_bCanReaderReadBodyParts=value;
				if(m_bCanReaderReadBodyParts==false)
				{
					
					if(CloseReaderForBodyPart!=null)
					{
                       CloseReaderForBodyPart();
					}
				}
			}
		}
		/// <summary>
		/// Property accesses the flag that identify whether data for Work Loss exist.
		/// It raises the event to close the data reader opened for reading work loss data in case the flag is false.
		/// </summary>
		internal bool CanReaderReadWorkLoss
		{
			get
			{
				return m_bCanReaderReadWorkLoss;
			}
			set
			{
				m_bCanReaderReadWorkLoss=value;
				if(m_bCanReaderReadWorkLoss==false)
				{
					
					if(CloseReaderForWorkLoss!=null)
					{
						CloseReaderForWorkLoss();
					}
				}
			}
		}
		/// <summary>
		/// Property accesses the flag that identify whether data for restricted days exist.
		/// It raises the event to close the data reader opened for reading restricted days data in case the flag is false.
		/// </summary>
		internal bool CanReaderReadRestrictedDays
		{
			get
			{
				return m_bCanReaderReadRestrictedDays;
			}
			set
			{
				m_bCanReaderReadRestrictedDays=value;
				if(m_bCanReaderReadRestrictedDays==false)
				{
					
					if(CloseReaderRestrictedDays!=null)
					{
						CloseReaderRestrictedDays();
					}
				}
			}
		}
		/// <summary>
		/// Property accesses the flag that identify whether data for Physician exist.
		/// It raises the event to close the data reader opened for reading Physician data in case the flag is false.
		/// </summary>
		internal bool CanReaderReadLoadPhysician
		{
			get
			{
				return m_bCanReaderReadLoadPhysician;
			}
			set
			{
				m_bCanReaderReadLoadPhysician=value;
				if(m_bCanReaderReadLoadPhysician==false)
				{
					
					if(CloseReaderLoadPhysician!=null)
					{
						CloseReaderLoadPhysician();
					}
				}
			}
		}
		/// <summary>
		/// Property accesses the flag that identify whether data for Hospital exist.
		/// It raises the event to close the data reader opened for reading Hospital data in case the flag is false.
		/// </summary>
		internal bool CanReaderReadLoadHospital
		{
			get
			{
				return m_bCanReaderReadLoadHospital;
			}
			set
			{
				m_bCanReaderReadLoadHospital=value;
				if(m_bCanReaderReadLoadHospital==false)
				{
					
					if(CloseReaderLoadHospital!=null)
					{
						CloseReaderLoadHospital();
					}
				}
			}
		}
		/// <summary>
		/// Property accesses the flag that identify whether data for Treatment exist.
		/// It raises the event to close the data reader opened for reading Treatment data in case the flag is false.
		/// </summary>
		internal bool CanReaderReadLoadTreatments
		{
			get
			{
				return m_bCanReaderReadLoadTreatments;
			}
			set
			{
				m_bCanReaderReadLoadTreatments=value;
				if(m_bCanReaderReadLoadTreatments==false)
				{
					if(CloseReaderLoadTreatments!=null)
					{
						CloseReaderLoadTreatments();
					}
				}
			}
		}
		/// <summary>
		/// Property accesses the flag that identify whether data for Injury exist.
		/// It raises the event to close the data reader opened for reading Injury data in case the flag is false.
		/// </summary>
		internal bool CanReaderReadLoadInjuries
		{
			get
			{
				return m_bCanReaderReadLoadInjuries;
			}
			set
			{
				m_bCanReaderReadLoadInjuries=value;
				if(m_bCanReaderReadLoadInjuries==false)
				{
					if(CloseReaderLoadInjuries!=null)
					{
						CloseReaderLoadInjuries();
					}
				}
			}
		}
		#endregion

		#region Constructor
		/// <summary>
		/// Riskmaster.Application.OSHALib.StateInformerForDbReaders is the default constructor.
		/// </summary>
		internal StateInformerForDbReaders()
		{
			
		}
		#endregion

	}
}
