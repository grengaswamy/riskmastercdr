using System;
using Riskmaster.Common;
using System.IO;
using C1.C1Zip;
using Riskmaster.ExceptionTypes;


namespace Riskmaster.Application.OSHALib
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004
	/// Purpose :  This class is used for compression and decompression of the images using component one technique.
	/// </summary>	
	internal class ZipUtil
	{
		#region Member Variables
		/// <summary>
		/// Contains the binary data for the image that has been converted to memory stream. 
		/// </summary>
		private byte[] m_arrByte;
		#endregion

		#region Constructor
		/// <summary>
		/// Riskmaster.Application.OSHALib.ZipUtil is the default constructor.
		/// </summary>
		internal ZipUtil()
		{	
		}
		#endregion

		#region Methods

		/// <summary>
		/// This function will decode the binary string.
		/// </summary>
		/// <param name="p_sSrc">String to be decoded.</param>
		/// <param name="p_arrImage">This array will contain the decoded bytes.</param>
		protected virtual void DecodeBinaryString(ref string p_sSrc,ref byte[] p_arrImage)
		{
			short sHex9 =0;
            short sHexA =0;
            short sHex0 =0;
			int iITmp=0;
			int iJTmp=0;
			string sSTmp="";
			short sC1=0;
            short sC2=0;
			int iLen=0; 
			try
			{
				if(p_sSrc!=null && p_sSrc!="")
				{
					sHex9=Conversion.GetAscii("9");
					sHexA=Conversion.GetAscii("A");
					sHex0=Conversion.GetAscii("0");
					iLen=p_sSrc.Length;
					sSTmp=sSTmp.PadRight((iLen/2),' ');
					while(iITmp<iLen)
					{
						sC2=Conversion.GetAscii(p_sSrc.Substring(iITmp,1));
					
						if(sC2 > sHex9)
						{
							sC2 = (short)(sC2 - sHexA + 10);
						}
						else
						{
							sC2 = (short)(sC2 - sHex0);
						}
						++iITmp;


						sC1=Conversion.GetAscii(p_sSrc.Substring(iITmp,1));
					
						if(sC1 > sHex9)
						{
							sC1 = (short)(sC1 - sHexA + 10);
						}
						else
						{
							sC1 = (short)(sC1 - sHex0);
						}
						++iITmp;

                       p_arrImage[iJTmp]=(byte)((sC2 * 16) + sC1);
    
                         ++iJTmp;
					}
				}				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ZipUtil.DecodeBinaryString.Error"),p_objException);
			}
		}
		/// <summary>
		/// This function will encode the byte array into a string.
		/// </summary>
		/// <param name="p_arrImage">Array of bytes to be encoded</param>		
		/// <returns>String containing the encoded binary data</returns>
		protected virtual string EncodeBinaryString(byte[] p_arrImage)
		{
			int iTemp=0;
			int jTemp=0;
			int kTemp=0;
			int lTemp=0;
			string sLookUp="";
			string sTemp="";
			
			try
			{
                sLookUp = "000102030405060708090A0B0C0D0E0F"
				 + "101112131415161718191A1B1C1D1E1F"
				 + "202122232425262728292A2B2C2D2E2F"
				 + "303132333435363738393A3B3C3D3E3F"
				 + "404142434445464748494A4B4C4D4E4F"
				 + "505152535455565758595A5B5C5D5E5F"
				 + "606162636465666768696A6B6C6D6E6F"
				 + "707172737475767778797A7B7C7D7E7F"
				 + "808182838485868788898A8B8C8D8E8F"
				 + "909192939495969798999A9B9C9D9E9F"
				 + "A0A1A2A3A4A5A6A7A8A9AAABACADAEAF"
				 + "B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF"
				 + "C0C1C2C3C4C5C6C7C8C9CACBCCCDCECF"
				 + "D0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF"
				 + "E0E1E2E3E4E5E6E7E8E9EAEBECEDEEEF"
				 + "F0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF";
			
				iTemp=p_arrImage.GetLowerBound(0);
				kTemp=p_arrImage.GetUpperBound(0);
				while(iTemp<=kTemp)
				{
                 lTemp=2*p_arrImage[iTemp];
				 sTemp=sTemp+sLookUp.Substring(lTemp,2);
                 jTemp=jTemp+2;
				 iTemp=iTemp+1;
				}
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ZipUtil.EncodeBinaryString.Error"),p_objException);
			}
			return sTemp;
		}
		
		/// <summary>
		/// This function will decompress the string into memory stream.
		/// </summary>
		/// <param name="p_sStr">String to be decompressed.</param>
		/// <returns>Memory Stream after decompression</returns>
		internal MemoryStream UnZIPBufferToMemoryStream(ref string p_sStr)
		{
			MemoryStream objMsSrc=null;
			C1ZStreamReader objSr=null;
			BinaryWriter objBw=null;
			BinaryReader objBr=null;
			MemoryStream objMsDest=null;
			try
			{
				if(p_sStr.Length>0 && p_sStr!=null)
				{
					byte[] arrImage=new byte[p_sStr.Length/2];
					DecodeBinaryString(ref p_sStr,ref arrImage);	
					objMsDest=new MemoryStream();
					objMsSrc=new MemoryStream(arrImage);
					objSr = new C1ZStreamReader(objMsSrc);
					objBw =new BinaryWriter(objMsDest);
					objBr=new BinaryReader(objSr);
					int iCount =0;
					byte[] arrData=new byte[1024];
					while(true)
					{
						iCount = objBr.Read(arrData, 0, 1024);
						if(iCount <= 0)
						{
							break;
						}
						objBw.Write(arrData,0,iCount);
					}
					objBw.Flush();
					objSr.Close();
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ZipUtil.UnZIPBufferToMemoryStream.Error"),p_objException);
			}
			finally
			{
				if(objMsSrc!=null)
				{
					objMsSrc.Close();
				}
				if(objBr!=null)
				{
					objBr.Close();
				}
				if(objSr!=null)
				{
					objSr.Close();
				}				
				p_sStr=null;							
			}
			return objMsDest;
		}
		/// <summary>
		/// This function will firstly compress the file and then encode the file using EncodeBinaryString() function.
		/// </summary>
		/// <param name="p_sFilePath">Path of the file to be compressed and encoded</param>
		/// <returns>Encoded Binary String</returns>
		internal string ZipFile(string p_sFilePath)
		{
			
		    FileStream objFsSrc=null;
		    MemoryStream objMsDest=new MemoryStream();
			string sRetVal="";
			BinaryReader objBr=null;
			BinaryWriter objBw =null;
			C1ZStreamWriter objSw=null;
			
			try
			{ 
				if(p_sFilePath.Length>0)
				{
					objFsSrc=new FileStream(p_sFilePath,FileMode.Open,FileAccess.Read);
					objBr = new BinaryReader(objFsSrc);
					byte[] arrPhoto = objBr.ReadBytes((int)objFsSrc.Length);
					objSw = new C1ZStreamWriter(objMsDest);
					objBw = new BinaryWriter(objSw);
					objBw.Write(arrPhoto);
					objBw.Flush();
					m_arrByte=objMsDest.ToArray();
					sRetVal=EncodeBinaryString(m_arrByte);
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ZipUtil.ZipFile.Error"),p_objException);
			}
			finally
			{
				if(objMsDest!=null)
				{
					objMsDest.Close();
				}
				if(objFsSrc!=null)
				{
					objFsSrc.Close();
				}
				if(objBr!=null)
				{
					objBr.Close();
				}
				if(objBw != null)
					objBw .Close();
				if(objSw != null)
					objSw.Close();

			}
			return sRetVal;
		}

		#endregion
	}
}
