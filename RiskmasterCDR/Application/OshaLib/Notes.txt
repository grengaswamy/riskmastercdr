1)Size of output pdf's(paper size) may vary a bit, hence they are subject to change depending upon the required size.

2)Soft error log will be printed before the actual reports starts on.

3)In osha sharps log report,data gets printed in tabular form.If data does not fit into one page it will be printed on to next page.If a portion of data comes on to border,table will have its end completed onto next page with that data.

4)Number of pages in Osha sharps log is not taken into consideration(while printing it PDF) as total number of entities on one page are not fixed.

5)The reports which are for before the year 2003(i.e 2002 and before) marks the check boxes with Tick mark instead of X(In VB Com version).

6)This C# component uses C1.Zip for compression, hence existing images can not be decompressed through this component as they were not compressed through this component.

7)In VB version of this component, all the XML(Osha300.xml, Osha301.xml) files were put into resource file present in the component itself, in C# version of the component, XML files will be supplied by the client of this component.


To Do

1) Check for informix...
2) Check for Update progress....
3) check for data i.e like data on previous reports is same as the current one...
4) check for "no data" tags....
5) check for exceptional data...
 