using System;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.Application.ReportInterfaces;
using System.Collections;

namespace Riskmaster.Application.OSHALib
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004	
	///</summary>
	
	/// <summary>
	/// This class contains helper functions.
	/// </summary>
	public class InstanceUtilities
	{
		#region Member Variables
		/// <summary>
		/// The connection string used by the database layer for connection with the database.
		/// </summary>
		private string m_sConnectString;
        private int m_iClientId;
		#endregion

		#region Properties
		/// <summary>
		///This property provides access to the connection string field of this class.
		/// </summary>		
		internal string Dsn
		{
			get
			{
				return m_sConnectString;
			}
			set
			{
             m_sConnectString=value;
			}
		}
		#endregion

		#region Constructor
		/// <summary>
		/// This is the constructor with connection string as the parameter.
		/// </summary>
		/// <param name="p_sDsn">Connection string used by the database layer for connection with the database.</param>
		internal InstanceUtilities(string p_sDsn,int p_iClientId)
		{
			m_sConnectString=p_sDsn;
            m_iClientId = p_iClientId;

		}
		#endregion

		#region Methods
		/// <summary>
		/// This method would return the state code.
		/// </summary>
		/// <param name="p_iStateID">Id of the state for which state code has to be fetched</param>
		/// <returns>State Code</returns>
		internal  string GetStateCode(long p_iStateID) 
		{
			return GetSingleString("STATE_ID", "STATES", "STATE_ROW_ID=" + p_iStateID);
		}
	

		/// <summary>
		/// This method would return the state name.
		/// </summary>
		/// <param name="p_iStateID">Id of the state for which state name has to be fetched</param>
		/// <returns>State Name</returns>
		internal  string GetStateName(int p_iStateID) 
		{
			return GetSingleString("STATE_NAME", "STATES", "STATE_ROW_ID=" + p_iStateID);
		}

		/// <summary>
		/// This method would return the short code.
		/// </summary>
		/// <param name="p_iCodeID">Id for which short code has to be fetched</param>
		/// <returns>Short Code</returns>
		internal  string GetShortCode(long p_iCodeID) 
		{
			return GetSingleString("SHORT_CODE", "CODES", "CODE_ID=" + p_iCodeID);
		}

		/// <summary>
		/// This method would return the code description.
		/// </summary>
		/// <param name="p_iCodeID">Id for which description has to be fetched.</param>
		/// <returns>Code Description</returns>
		internal  string GetCodeDesc(int p_iCodeID) 
		{
			return GetSingleString("CODE_DESC", "CODES_TEXT", "CODE_ID=" + p_iCodeID);
		}

		/// <summary>
		/// This method would return the table name.
		/// </summary>
		/// <param name="p_iCodeID">Table id for which table name has to be fetched.</param>
		/// <returns>Table Name</returns>
		internal  string GetTableName(int p_iCodeID) 
		{
			return GetSingleString("SYSTEM_TABLE_NAME", "GLOSSARY", "TABLE_ID=" + p_iCodeID);
		}

		/// <summary>
		/// This method would return the id corresponding to a table.
		/// </summary>
		/// <param name="p_sTableName">Name of the table for which table id has to be fetched.</param>
		/// <returns>Table Id</returns>
		internal  int GetTableID(string p_sTableName) 
		{
			return GetSingleInt("TABLE_ID", "GLOSSARY", "SYSTEM_TABLE_NAME = " + this.SqlTextArg(p_sTableName));
		}
		/// <summary>
		/// This method would process the arguments of type string for use directly in SQL statements.		
		/// </summary>
		/// <param name="p_sArg">Argument to be formatted.</param>
		/// <returns>Formatted SQL sub query</returns>
		internal string SqlTextArg(string p_sArg)
		{
			string sRetVal="";
			try
			{
				if(p_sArg==null || p_sArg=="")
				{
					sRetVal="NULL";
				}
				else
				{ 
                     sRetVal = "'" + p_sArg.Replace("'","''") + "'";
				}
				
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("InstanceUtilities.SqlTextArg.Error",m_iClientId),p_objException);
			}	
			return sRetVal;
		}
		/// <summary>
		/// This method would return the code id.
		/// </summary>
		/// <param name="p_sCode">Short Code</param>
		/// <param name="p_iTableId">Id of the table.</param>
		/// <returns>Code Id</returns>
		internal  int GetCodeIDWithShort(string p_sCode, int p_iTableId) 
		{
			return GetSingleInt("CODE_ID","CODES", "TABLE_ID=" + p_iTableId + " AND SHORT_CODE=" + this.SqlTextArg(p_sCode));
		}

		/// <summary>
		/// This method would return the abbreviation of the entity.
		/// </summary>
		/// <param name="p_iEntityId">Entity id for which abbreviation has to be fetched.</param>
		/// <returns>Abbreviation</returns>
		internal  string GetOrgAbbreviation(int p_iEntityId) 
		{
			return GetSingleString("ABBREVIATION", "ENTITY", "ENTITY_ID=" + p_iEntityId);
		}

		/// <summary>
		/// This method would return the name of the entity.
		/// </summary>
		/// <param name="p_iEntityId">Entity id for which name has to be fetched.</param>
		/// <returns>Entity Name</returns>
		internal  string GetOrgName(int p_iEntityId) 
		{
			return GetSingleString("LAST_NAME", "ENTITY", "ENTITY_ID=" + p_iEntityId);
		}

		/// <summary>
		/// This method would return the value of a field, depending upon the criteria, table name 
		///  and the field name specified as the parameters.
		/// </summary>
		/// <param name="p_sFieldName">Field to be fetched.</param>
		/// <param name="p_sTableName">Name of the table.</param>
		/// <param name="p_sCriteria">Criteria for selecting the value.</param>
		/// <returns>Value of the field</returns>
		internal  string GetSingleString(string p_sFieldName, string p_sTableName, string p_sCriteria)
		{
			string sGetSingleString="";
			DbReader objReader=null;
			try
			{	
				if(p_sFieldName=="" || p_sTableName=="")
					throw new RMAppException(Globalization.GetString("InstaceUtilities.GetSingleString.MissingTableColumnName",m_iClientId));
				objReader=DbFactory.GetDbReader(m_sConnectString,"SELECT " + p_sFieldName + 
					" FROM " + p_sTableName + 
					(p_sCriteria.Length==0 ? "" : " WHERE " + p_sCriteria));
				if (objReader != null)
					if(objReader.Read())
					{
						if(objReader.GetValue(0) is System.DBNull)
						{
							sGetSingleString="";
						}
						else
						{
							sGetSingleString=(string)objReader.GetValue(0);
						}
					}
				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("InstaceUtilities.GetSingleString.GeneralError",m_iClientId),p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return sGetSingleString;
		}

		/// <summary>
		/// This method would return the value of a field, depending upon the criteria, table name 
		///  and the field name specified as the parameters.
		/// </summary>
		/// <param name="p_sFieldName">Field to be fetched.</param>
		/// <param name="p_sTableName">Name of the table.</param>
		/// <param name="p_sCriteria">Criteria for selecting the value.</param>
		/// <returns>Value of the field</returns>
		internal  int GetSingleInt(string p_sFieldName, string p_sTableName, string p_sCriteria)
		{
			int iGetSingleLong=0;
			string sSQL="";
			
			DbReader objReader=null;
			try
			{
				sSQL="SELECT " + p_sFieldName + " FROM " + p_sTableName + (p_sCriteria.Length==0 ? "" : " WHERE " + p_sCriteria);
				if (p_sFieldName == "DEPT_ASSIGNED_EID" )
				{
					sSQL = sSQL + " AND DEPT_ASSIGNED_EID > 0";
				}
				objReader=DbFactory.GetDbReader(m_sConnectString,sSQL);
				if (objReader != null)
					if(objReader.Read())
					{
						if(objReader.GetValue(0) is System.DBNull)
					 {
						 iGetSingleLong=0;
					 }
					 else
					 {
						iGetSingleLong=Convert.ToInt32(objReader.GetValue(0));
					 }
						
					}
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("InstaceUtilities.GetSingleInt.GeneralError",m_iClientId),p_objException);
			}
			if(objReader!=null)
			{
				objReader.Close();
				objReader.Dispose();
			}
			return iGetSingleLong;
		}

        /// <summary>
        /// This method would return the value of a field, depending upon the criteria, table name 
        ///  and the field name specified as the parameters.
        /// </summary>
        /// <param name="p_sFieldName">Field to be fetched.</param>
        /// <param name="p_sTableName">Name of the table.</param>
        /// <param name="p_sCriteria">Criteria for selecting the value.</param>
        /// <returns>Value of the field</returns>
        internal ArrayList GetMultipleInt(string p_sFieldName, string p_sTableName, string p_sCriteria)
        {
            ArrayList iGetSingleLong = new ArrayList();
            string sSQL = "";
            
            DbReader objReader = null;
            try
            {
                sSQL = "SELECT " + p_sFieldName + " FROM " + p_sTableName + (p_sCriteria.Length == 0 ? "" : " WHERE " + p_sCriteria);
                if (p_sFieldName == "DEPT_ASSIGNED_EID")
                {
                    sSQL = sSQL + " AND DEPT_ASSIGNED_EID > 0";
                }
                objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                if (objReader != null)
                    while(objReader.Read())
                    {
                        if (objReader.GetValue(0) is System.DBNull)
                        {
                            iGetSingleLong.Add(0);
                        }
                        else
                        {
                            iGetSingleLong.Add(Convert.ToInt32(objReader.GetValue(0)));
                        }
                    }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InstaceUtilities.GetSingleInt.GeneralError",m_iClientId), p_objException);
            }
            if (objReader != null)
            {
                objReader.Close();
                objReader.Dispose();
            }
            return iGetSingleLong;
        }

		#endregion
	}

	#region Enumerations
	/// <summary>
	/// This enumeration describes the sorting order of the osha report entities.
	/// </summary>
	internal enum SortOrder:int
	{
		SORT_EVENTDATE,
		SORT_EVENTNUMBER,
	    SORT_PERSONNAME
    }
    //MITS 37193
    /// <summary>
    /// This enumeration identifies what value would be shown for the Column E in the report.
    /// </summary>
    internal enum ColumnESourceEnum : int
    {
        SRC_E_PRIMARY_LOCATION,
        SRC_E_LOCATION_DESCRIPTION,
        SRC_E_DEPARTMENT_ADDRESS
    }

    /// <summary>
	/// This enumeration identifies what value would be shown for the Column F in the report.
	/// </summary>
	internal enum ColumnFSourceEnum:int
	{
		SRC_DEFAULT,
		SRC_EVENTDESC,
	    SRC_OSHAHOWACC,
	    SRC_OSHAACCDESC
	}
	#endregion

	/// <summary>
	/// This class contains static variables and functions that are used as helper functions across this namespace.
	/// </summary>
	internal class Declarations
	{   
		#region Constants
		/// <summary>
		/// A constant value that identify the field corresponding 
		/// to Organization hierarchy as Client		
		/// </summary>
		internal const int CLIENT_EID = 1005;
		/// <summary>
		/// A constant value that identify the field corresponding 
		/// to Organization hierarchy as Company		
		/// </summary>
		internal const int COMPANY_EID = 1006;
		/// <summary>
		/// A constant value that identify the field corresponding 
		/// to Organization hierarchy as Operation		
		/// </summary>
		internal const int OPERATION_EID = 1007;
		/// <summary>
		/// A constant value that identify the field corresponding 
		/// to Organization hierarchy as Region		
		/// </summary>
		internal const int REGION_EID = 1008;
		/// <summary>
		/// A constant value that identify the field corresponding 
		/// to Organization hierarchy as Division
		/// </summary>
		internal const int DIVISION_EID = 1009;
		/// <summary>
		/// A constant value that identify the field corresponding 
		/// to Organization hierarchy as Location
		/// </summary>
		internal const int LOCATION_EID = 1010;
		/// <summary>
		/// A constant value that identify the field corresponding 
		/// to Organization hierarchy as Facility
		/// </summary>
		internal const int FACILITY_EID = 1011;
		/// <summary>
		/// A constant value that identify the field corresponding 
		/// to Organization hierarchy as Department
		/// </summary>
		internal const int DEPARTMENT_EID = 1012;

		/// <summary>
		/// A constant string identifying the date on which the rule for Work loss has changed.
		/// </summary>
		internal const string WORK_LOSS_RULE_CHANGE_DATE = "20011231";
		#endregion

		#region Methods
		/// <summary>
		/// This method would validate that the string parameter, passed to it ,is a valid date or not.
		/// </summary>
		/// <param name="p_sDate">Date string that needs to be validated.</param>
		/// <returns>True/False</returns>
		internal static bool IsDate(string p_sDate)
		{
			try
			{
				DateTime.Parse(p_sDate);
				return true;
			}
			catch
			{
				return false;
			}
		}
		/// <summary>
		/// This method would validate that the string parameter, passed to it, is a valid time or not.
		/// </summary>
		/// <param name="p_sTime">Time string that needs to be validated.</param>
		/// <returns>True/False</returns>
		internal static bool IsTime(string p_sTime)
		{
			bool bRetVal=false;
			string sGetTime="";
			try
			{
				
				if(p_sTime!=null && p_sTime!="")
				{
					p_sTime=p_sTime.Trim();
					sGetTime=Conversion.GetTime(p_sTime);
					if((sGetTime=="")||(sGetTime=="000000" && p_sTime!="12:00 AM"))
					{
						bRetVal=false;
					}
					else
					{
						bRetVal=true;

					}
				}
			}
			catch
			{
				bRetVal=false;
			}
			return bRetVal;
		
		}
		/// <summary>
		/// This method would return number of days between the two dates passed to it as parameter.
		/// </summary>
		/// <param name="p_sDate1">Date string</param>
		/// <param name="p_sDate2">Date string</param>
		/// <returns>Number of days</returns>
		internal static int DaysDiff(string p_sDate1,string p_sDate2,int p_iClientId)
		{
			int iRetVal=0;
			try
			{
				iRetVal=((TimeSpan)Conversion.ToDate(p_sDate1).Subtract(Conversion.ToDate(p_sDate2))).Days;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Declarations.DaysDiff.GeneralError",p_iClientId),p_objException);
			}
			return iRetVal;
		}
		/// <summary>
		/// This method would return the description of the Report Level.
		/// </summary>
		/// <param name="p_iReportLevel">Report Level.</param>
		/// <returns>Description of the Report Level.</returns>
		internal static string OrgLevelToEIDColumn(int p_iReportLevel,int p_iClientId)
		{
			string sRetVal="";
			try
			{
				switch(p_iReportLevel)
				{
					case CLIENT_EID:
					{
                        sRetVal="CLIENT_EID";
						break;
					}
					case COMPANY_EID :
					{
						sRetVal="COMPANY_EID";
						break;
					}
					case OPERATION_EID :
					{
						sRetVal="OPERATION_EID";
						break;
					}
					case REGION_EID :
					{
						sRetVal="REGION_EID";
						break;
					}
					case DIVISION_EID :
					{
						sRetVal="DIVISION_EID";
						break;
					}
					case LOCATION_EID :
					{
						sRetVal="LOCATION_EID";
						break;
					}
					case FACILITY_EID :
					{
						sRetVal="FACILITY_EID";
						break;
					}
					case DEPARTMENT_EID :
					{
						sRetVal="DEPARTMENT_EID";
						break;
					}
				}
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.OrgLevelToEIDColumn.GeneralError", p_iClientId), p_objException);
			}
			return sRetVal;
		}
		/// <summary>
		/// This method would read the value for the "checked" attribute of the XML Element passed to it.
		/// </summary>
		/// <param name="p_objElement">XML Element</param>
		/// <returns>True/Flase</returns>
		internal static bool IsChecked(XmlElement p_objElement,int p_iClientId)
		{
			try
			{
              return p_objElement.Attributes.GetNamedItem("checked")!=null;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.IsChecked.GeneralError", p_iClientId), p_objException);
			}
		}
		/// <summary>
		/// The method would return the value for the attribute, passed to it, 
		/// as fetched from the XML Document passed to it.
		/// </summary>	
		/// <param name="p_sName">Attribute Name</param>
		/// <param name="p_objXML">XML Document</param>
		/// <returns>Value of the attribute</returns>
		internal static string GetXMLValue(string p_sName, XmlDocument p_objXML, int p_iClientId) 
		{
			string sRetVal="";																				 
			XmlElement objNode=null;																											 
			try
			{																								  
				objNode = (XmlElement)p_objXML.SelectSingleNode("//control[@name='" + p_sName + "']");
				if(objNode!=null)
				{
					switch(objNode.GetAttribute("type"))
					{																										  
					
						case "radio":
							objNode=null;
							objNode = (XmlElement)p_objXML.SelectSingleNode("//control[@name='" + p_sName + "' and @checked]");				
							if(objNode!=null)
							{
								sRetVal =Declarations.GetStrValFromXmlAttribute(objNode.GetAttribute("value"),p_iClientId);
							}
							break;
						case "date":		
							sRetVal = Declarations.GetStrValFromXmlAttribute(objNode.GetAttribute("value"),p_iClientId);
							sRetVal=Conversion.GetDate(sRetVal);
							break;
						case "combobox":
							sRetVal = Declarations.GetStrValFromXmlAttribute(objNode.GetAttribute("codeid"),p_iClientId);
							break;
						case "multiclaimnumberlookup":
							foreach(XmlElement objChild in objNode.ChildNodes)
                                Common.Utilities.AddDelimited(ref sRetVal, Declarations.GetStrValFromXmlAttribute(objChild.GetAttribute("value").ToString(),p_iClientId), null, p_iClientId);
							break;
						case "multieventnumberlookup":
							foreach(XmlElement objChild in objNode.ChildNodes)
                                Common.Utilities.AddDelimited(ref sRetVal, Declarations.GetStrValFromXmlAttribute(objChild.GetAttribute("value").ToString(),p_iClientId), null, p_iClientId);
							break;
			
						case "multiorgh":
							foreach(XmlElement objChild in objNode.ChildNodes)
                                Common.Utilities.AddDelimited(ref sRetVal, Declarations.GetStrValFromXmlAttribute(objChild.GetAttribute("value").ToString(),p_iClientId), null, p_iClientId);
							break;
						case "numeric":
							sRetVal = Declarations.GetStrValFromXmlAttribute(objNode.GetAttribute("value"),p_iClientId);
							break;
						case "checkbox":
							objNode = null;
							objNode = (XmlElement)p_objXML.SelectSingleNode("//control[@name='" + p_sName + "' and @checked]");
							sRetVal = (objNode!=null?"-1":"0");
							break;
					}
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.GetXMLValue.GeneralError", p_iClientId), p_objException);
			}
			finally
			{
				objNode=null;				
			}
			return sRetVal;
		}
		/// <summary>
		/// This method would return an integer value by converting the string, passed to it as parameter,
		/// to an integer. It will return 0 if the string is null or blank.
		/// </summary>
		/// <param name="p_sAttribute">String value</param>
		/// <returns>Integer value after conversion.</returns>
		internal static int GetIntValFromString(string p_sAttribute,int p_iClientId)
		{   int iRetVal=0;
			try
			{
				if(p_sAttribute==null || p_sAttribute=="")
				{
					iRetVal=0;
				}
				else
				{
					iRetVal=Convert.ToInt32(p_sAttribute);
				}
             
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.GetIntValFromString.Error", p_iClientId), p_objException);
			}
			return iRetVal;
		}

		/// <summary>
		/// This method would return a Boolean value by converting the string, passed to it as parameter,
		/// to a Boolean. It will return false if the passed string is 0,null or blank.
		/// </summary>
		/// <param name="p_sAttribute">String Value</param>
		/// <returns>True/False</returns>
		internal static bool GetBoolValFromXmlAttribute(string p_sAttribute,int p_iClientId)
		{
			bool bRetVal=false;
			try
			{
                //RMA-9778 Bigin
				if(p_sAttribute==null || p_sAttribute=="")
				{
                    if (p_sAttribute == null)
                        bRetVal = false;
                    else
                        bRetVal = true;
				}
                //RMA-9778 End
				else
				{
					if(p_sAttribute.Trim()=="-1")
					{
						return true;
					}
					if(p_sAttribute.Trim()=="0")
					{
						return false;
					}
                  bRetVal=Convert.ToBoolean(p_sAttribute);
				}
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.GetBoolValFromXmlAttribute.Error", p_iClientId), p_objException);
			}
			return bRetVal;
		}
		/// <summary>
		/// This method would return blank if the passed string is null else it will return the same string value.
		/// </summary>
		/// <param name="p_sAttribute">String Value</param>
		/// <returns>String Value</returns>
		internal static string GetStrValFromXmlAttribute(string p_sAttribute,int p_iClientId)
		{
			string sRetVal="";
			try
			{
				if(p_sAttribute==null || p_sAttribute=="")
				{
					 sRetVal=""; 
				}
				else
				{
                  sRetVal=p_sAttribute;
				}
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.GetStrValFromXmlAttribute.Error", p_iClientId), p_objException);
			}
			return sRetVal;
		}
		/// <summary>
		/// This method would return a string value by converting the integer, passed to it as parameter,
		/// to a string. It will return blank if the integer is 0.
		/// </summary>
		/// <param name="p_iVal">Integer value</param>
		/// <returns>String value</returns>
		internal static string GetStrValFromInt(int p_iVal,int p_iClientId)
		{ string sRetVal="";
			try
			{
				if(p_iVal==0)
				{
					return sRetVal;
				}
				else
				{
					sRetVal=p_iVal.ToString();
				}
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.GetStrValFromInt.Error", p_iClientId), p_objException);
			}
			return sRetVal;
		}
		/// <summary>
		/// This method would return a date after adding specified number of days/months/years.
		/// </summary>
		/// <param name="p_saddFormat">Interval type (d/m/yyyy)</param>				
		/// <param name="p_iInterval">Interval Value</param>				
		/// <param name="p_datDate">Date to which specified interval is added</param>				
		/// <returns>Date</returns>
		internal static DateTime DateAdd(string p_saddFormat,int p_iInterval,DateTime p_datDate,int p_iClientId)
		{
			DateTime datRetVal=p_datDate;
			try
			{
				
				switch(p_saddFormat.ToLower())
				{
					case "d":
                          datRetVal=p_datDate.AddDays(p_iInterval);
						break;
					case "m":
						datRetVal=p_datDate.AddMonths(p_iInterval);
						break;
					case "yyyy":
						 datRetVal=p_datDate.AddYears(p_iInterval);
						break;
				}
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.DateAdd.Error", p_iClientId), p_objException);
			}
			return datRetVal;
		}
		/// <summary>
		/// This method would give the number of days between the two dates that will be passed as parameters.
		/// It will compare the two dates and subtract the lesser date from the greater one.
		/// </summary>
		/// <param name="p_datDate1">Date</param>
		/// <param name="p_datDate2">Date</param>
		/// <returns>Number of days</returns>
		internal static int DateDiffInAbsoluteDays(DateTime p_datDate1,DateTime p_datDate2,int p_iClientId)
		{
			int iRetVal=0;
			try
			{
				if(p_datDate1.CompareTo(p_datDate2) < 0)
				{
					iRetVal=((TimeSpan)p_datDate2.Subtract(p_datDate1)).Days;
				}
				else
				{
                   iRetVal=((TimeSpan)p_datDate1.Subtract(p_datDate2)).Days;
				}

			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.DateDiffInAbsoluteDays.Error", p_iClientId), p_objException);
			}
			return iRetVal;

		}
		/// <summary>
		/// This method will remove the date and time from the string passed to it as parameter.
		/// </summary>
		/// <param name="p_sStr">String</param>
		/// <returns>String after removing date and timestamp</returns>
		internal static string RemoveDateTimeStamp(string p_sStr,int p_iClientId)
		{
			string sRetVal="";
			int iPos=0;
			try
			{
				if(p_sStr!=null && p_sStr!="")
				{
					iPos =p_sStr.IndexOf(" ",0);
					if(iPos>0)
					{
						if(IsDate(p_sStr.Substring(0,iPos).Trim()))
						{
							p_sStr=p_sStr.Substring(iPos).Trim();

                        }
                    }
                    if (p_sStr.Length >= 8)
                    {
                        //JIRA 1288
                        if ((p_sStr.Length >= 11) && (p_sStr.Substring(0, 11).Trim().ToUpper().EndsWith("AM") || p_sStr.Substring(0, 11).Trim().ToUpper().EndsWith("PM")))
                        {
                            if (IsTime(p_sStr.Substring(0, 11).Trim()))
                            {
                                if (p_sStr.Trim().Length > 11)
                                {
                                    p_sStr = p_sStr.Substring(11).Trim();
                                }
                                else
                                {
                                    p_sStr = "";
                                }

                            }
                        }
                        else
                        {
                            if (IsTime(p_sStr.Substring(0, 8).Trim()))
                            {
                                if (p_sStr.Trim().Length > 8)
                                {
                                    p_sStr = p_sStr.Substring(8).Trim();
                                }
                                else
                                {
                                    p_sStr = "";
                                }

                            }
                        }
                    }
                    sRetVal = p_sStr;
				}
				else
				{
					sRetVal="";
				}
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.RemoveDateTimeStamp.Error", p_iClientId), p_objException);
			}
			return sRetVal;
		}		
		/// <summary>
		/// This method would convert the Date Time passed to it into MM/DD/YYYY format.
		/// </summary>		
		/// <param name="p_datDate">Date Time value</param>
		/// <returns>Date Time converted to MM/DD/YYYY format</returns>
        internal static string ConvertDateTimeToMmDdYyyy(DateTime p_datDate, int p_iClientId)
		{
			string sRetVal="";
			try
			{
				if(p_datDate==DateTime.MinValue)
				{
					sRetVal="";
				}
				else
				{
					sRetVal=p_datDate.Month.ToString() + "/" + p_datDate.Day.ToString() + "/" + p_datDate.Year.ToString();
				}
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.ConvertDateTimeToMmDdYyyy.Error", p_iClientId), p_objException);
			}
			return sRetVal;
		}
		/// <summary>
		/// This function converts one time format to another format.
		/// </summary>		
		/// <example>Input time string->130000.Output time string->1:00 PM</example>
		/// <param name="p_sVal">String time whose format to be changed.</param>
		/// <returns>Formatted time</returns>
		internal static string GetPmAmTimeFromDbTimeFormat(string p_sVal)
		{
			string sHours =""; 
			string sMins ="";
			try
			{
				if(p_sVal=="" || p_sVal==null)
					return "";
				if(p_sVal.Length>=4)
				{
					p_sVal=p_sVal.Substring(0,2) + ":" +p_sVal.Substring(2,2);
				
					string[] arrTimeDetails=p_sVal.Split(':');
					sHours=arrTimeDetails[0];
					sMins=arrTimeDetails[1];
					if((Convert.ToInt32(sHours))>12)
					{
						return (((Convert.ToInt32(sHours))-12) + ":" + sMins + " " + "PM");
					}
					else
					{
						if((Convert.ToInt32(sHours))==0)
							return "12:" + sMins + " " + "AM";
						else 
						{
							if((Convert.ToInt32(sHours))==12)
							{
								return p_sVal +" " + "PM";
							}
							else
							{ 
								return p_sVal +" " + "AM";

							}
						}

					}
				}
			}
			catch
			{
				p_sVal=""; //all attempt failed,return blank string..
			}			
			return p_sVal;
		}
		/// <summary>
		/// This method would check if the process of report generation has been aborted.
		/// If yes then raise error and stop further processing else proceed further.
		/// </summary>
		/// <param name="p_objNotify">Object that will notify if the report generation has to be aborted.</param>
		internal static void CheckAbort(IWorkerNotifications p_objNotify,int p_iClientId)
		{
		if(p_objNotify!=null)
		 {
			 if(p_objNotify.CheckAbort()!=0)
			 {
                 throw new AbortRequestedException(Globalization.GetString("Declarations.CheckAbort.Abort", p_iClientId));		
				 
			 }
		 }
			
		
			
		}
		/// <summary>
		/// This method will update the current status of report generation.
		/// </summary>
		/// <param name="p_objNotify">Object to which the current status of the report will be updated.</param>
		/// <param name="p_sStage">Current stage in the report generation process.</param>
		/// <param name="p_iTimeElapsed">Time since the report generation has started</param>
		/// <param name="p_iTimeLeft">0/1</param>
		/// <param name="p_iPercentComplete">0</param>
		internal static void UpdateProgress(IWorkerNotifications p_objNotify,string p_sStage,int p_iTimeElapsed,int p_iTimeLeft,int p_iPercentComplete)
		{
			if(p_objNotify!=null)
			{
				p_objNotify.UpdateProgress(p_sStage,p_iTimeElapsed,p_iTimeLeft,p_iPercentComplete);
			}
		}
		/// <summary>
		/// This method would return the difference in seconds between the current date time and the date time passed.
		/// </summary>
		/// <param name="p_datStart">Datetime</param>								
		/// <returns>Number of seconds</returns>
        internal static int DateDiff(DateTime p_datStart, int p_iClientId)
		{
			try
			{
				return ((TimeSpan)(DateTime.Now.Subtract(p_datStart))).Seconds;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.DateDiff.Error", p_iClientId), p_objException);
			}
		}

		#endregion
		/// <summary>
		/// Converts long value to string.
		/// </summary>
		/// <param name="p_iVal">Long value to be converted to string</param>
		/// <returns>string value of long value</returns>
		internal static string GetStrValFromLong(long p_iVal,int p_iClientId)
		{
			string sRetVal="";
			try
			{
				if(p_iVal==0)
				{
					return sRetVal;
				}
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.GetStrValFromLong.Error", p_iClientId), p_objException);
			}
			return p_iVal.ToString();
		}
		/// <summary>
		/// This method would return an integer value by converting the string, passed to it as parameter,
		/// to an integer. It will return 0 if the string is null or blank.
		/// </summary>
		/// <param name="p_sAttribute">String value</param>
		/// <returns>long value after conversion.</returns>
		internal static long GetLongValFromString(string p_sAttribute,int p_iClientId)
		{
			long iRetVal=0;
			try
			{
				if(p_sAttribute==null || p_sAttribute=="")
				{
					return iRetVal;
				}
			 
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.GetLongValFromString.Error", p_iClientId), p_objException);
			}
			return Convert.ToInt64(p_sAttribute);
		}

	}
}
