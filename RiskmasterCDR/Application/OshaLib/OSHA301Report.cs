using System;
using System.Collections;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.OSHALib
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004
	/// Purpose :  This class inherits CriteriaEntity class.
	///			   It implements the methods/properties declared in IOSHAReport interface. 
	/// </summary>
	
	internal class OSHA301Report :CriteriaEntity,IOSHAReport
	{		
		#region Member Variables
		/// <summary>
		/// Event Id.
		/// </summary>
		private int m_iFilterEventId ;
		/// <summary>
		/// Claim Id
		/// </summary>
		private int m_iFilterClaimId;
		#endregion
		
		#region Properties
		/// <summary>
		/// This property will access the value of Event Id.
		/// </summary>
		internal int FilterEventId
		{
			get
			{
				return m_iFilterEventId;
			}
			set
			{
				m_iFilterEventId=value;
			}
		}
		/// <summary>
		/// This property will access the value of Claim Id.
		/// </summary>
		internal int FilterClaimId
		{
			get
			{
				return m_iFilterClaimId;
			}
			set
			{
				m_iFilterClaimId=value;
			}
		}

		/// <summary>
		/// This property accesses the XML Document fetched/loaded for the determining OSHA Report criteria.
		/// </summary>
		public XmlDocument CriteriaXmlDom
		{
			get
			{
				XmlNodeList objControlNodes=null;
				bool bUseYear=false;
				XmlElement objEntityElt=null;
				XmlElement objEntityTmpElt=null;
				XmlDocument objCriteriaXML=null;
				try
				{
					objCriteriaXML=m_objCriteriaXML;
					objControlNodes=objCriteriaXML.GetElementsByTagName("control");
					foreach(XmlElement objTmpElt in objControlNodes)
					{
						if(objTmpElt.Attributes.GetNamedItem("checked")!=null)
						{
							objTmpElt.Attributes.RemoveNamedItem("checked");
						}
						if(objTmpElt.Attributes.GetNamedItem("checked")!=null)
						{
							objTmpElt.Attributes.RemoveNamedItem("codeid");
						}
					}
					objControlNodes = objCriteriaXML.GetElementsByTagName("control");
					if(objControlNodes!=null)
					{
						foreach(XmlElement objTmpElt in objControlNodes)
						{
							switch(objTmpElt.GetAttribute("name").ToLower())
							{
								case "reportlevel" :
								{
									objTmpElt.SetAttribute("codeid",Declarations.GetStrValFromInt(base.ReportLevel,m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + base.ReportLevel + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
								case "usereportlevel" :
								{
									objTmpElt.SetAttribute("codeid",Declarations.GetStrValFromInt(base.UseReportLevel,m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + base.UseReportLevel + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
								case "begindate" :
								{
									if(!bUseYear)
									{
										objTmpElt.SetAttribute("value",Conversion.ToDbDate(base.BeginDate));
									}
									break;
									
								}
								case "enddate" :
								{
									if(!bUseYear)
									{
										objTmpElt.SetAttribute("value",Conversion.ToDbDate(base.EndDate));
									}
									break;
								}
								case "allentitiesflag" :
								{
									if(base.AllEntitiesFlag)
									{
										objEntityElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='allentitiesflag' and @value='true']");
									}
									else
									{
										objEntityElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='allentitiesflag' and @value='false']");
									}
									if(objEntityElt!=null)
										objEntityElt.SetAttribute("checked", "");
									break;
								}
								case "selectedentities" :
								{
									foreach(string vTmp in m_arrlstEntities)
									{
										objEntityElt = objCriteriaXML.CreateElement("option");
										objEntityElt.SetAttribute("value",vTmp);
										objEntityElt.InnerText = m_objInstance.GetOrgAbbreviation(Declarations.GetIntValFromString(vTmp,m_iClientId)) + " - " + m_objInstance.GetOrgName(Declarations.GetIntValFromString(vTmp,m_iClientId));
										objTmpElt.AppendChild(objEntityElt);
									}
									break;
								}
								case "yearofreport" :
								{
									objTmpElt.SetAttribute("codeid", base.YearOfReport);
									break;
								}
								case "eventbasedflag" :
								{
									if(base.EventBasedFlag)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "printsofterrlog" :
								{
									if(base.PrintSoftErrLog)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "printoshadescflag" :
								{
									if(m_bPrintOSHADescFlag)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "primarylocationflag" :
								{
									if(m_bPrimaryLocationFlag)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "preparername" :
								{
									
									objTmpElt.SetAttribute("value",base.Preparer.Name);
									break;
								}
								case "preparertitle" :
								{
									objTmpElt.SetAttribute("value",base.Preparer.Title);
									break;
								}
								case "preparerphone" :
								{
									objTmpElt.SetAttribute("value",base.Preparer.Phone);
									break;
								}
								case "enforce180dayrule" :
								{
									if(base.Enforce180DayRule)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "asofdate" :
								{
									objTmpElt.SetAttribute("value",Conversion.ToDbDate(base.AsOfDate));
									break;
								}
								case "establishmentnameprefix" :
								{
									objTmpElt.SetAttribute("codeid",base.EstablishmentNamePrefix); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + base.EstablishmentNamePrefix + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
								case "sortorder" :
								{
									objTmpElt.SetAttribute("codeid",Declarations.GetStrValFromInt(base.ItemSortOrder,m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + base.ItemSortOrder + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
                                    //MITS 37193 Begin
                                case "columnesource":
                                {
                                    objTmpElt.SetAttribute("codeid", Declarations.GetStrValFromInt(base.ColumnESource,m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
                                    objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + base.ColumnESource + "']");
                                    if (objEntityElt != null)
                                    {
                                        objEntityElt.SetAttribute("selected", "");
                                    }
                                    break;
                                }
                                //MITS 37193 End
								case "columnfsource" :
								{
									objTmpElt.SetAttribute("codeid",Declarations.GetStrValFromInt(base.ColumnFSource,m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + base.ColumnFSource + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
								case "filtereventid" :
								{
									objTmpElt.SetAttribute("value",this.FilterEventId.ToString());
									break;
								}
								case "filterclaimid" :
								{
									objTmpElt.SetAttribute("value",this.FilterClaimId.ToString());
									break;
								}
								case "byoshaestablishmentflag":
								{
									if(base.ByOSHAEstablishmentFlag)
									{
										objEntityElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='byoshaestablishmentflag' and @value='true']");
									}
									else
									{
										objEntityElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='byoshaestablishmentflag' and @value='false']");
									}
									if(objEntityElt!=null)
										objEntityElt.SetAttribute("checked","");
									break;
								}
							}
						}
					}
       

					objControlNodes = objCriteriaXML.GetElementsByTagName("internal");
					foreach(XmlElement objTmpElt in objControlNodes)
					{
						switch(objTmpElt.GetAttribute("name"))
						{
							case "reporttype" :
								objTmpElt.SetAttribute("value","1");
								objEntityTmpElt=(XmlElement)objCriteriaXML.SelectSingleNode("//report");
								objEntityTmpElt.SetAttribute("type","1");
								break;
						}
					}

				}
				catch(RMAppException p_objException)
				{
					throw p_objException;
				}
				catch(Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("OSHA301Report.CriteriaXmlDom.ErrorGet",m_iClientId),p_objException);
				}
				finally
				{
					objControlNodes=null;					
					objEntityElt=null;
					objEntityTmpElt=null;					
				}
				return objCriteriaXML;
			}
			set
			{
				#region for setting criteria...
				XmlDocument objPrevXml=null;
				XmlNodeList objControlNodes=null;
				bool bUseYear=false;
				XmlElement objElt=null;
				XmlDocument objCriteriaXML=null;
				try
				{
					objPrevXml=m_objCriteriaXML;
					objCriteriaXML=value;
					objElt = ((XmlElement)objCriteriaXML.SelectSingleNode("//control[@name = 'useyear']"));
					if(objElt!=null)
					{
						bUseYear=objElt.InnerText.ToLower()=="true" ? true:false;
					}
					objControlNodes = objCriteriaXML.GetElementsByTagName("control");
					if(objControlNodes!=null)
					{
						foreach(XmlElement objTmpElt in objControlNodes)
						{
							switch(objTmpElt.GetAttribute("name").ToLower())
							{
								case "reportlevel" :
								{
									if(objTmpElt.GetAttribute("codeid")!="")//Enhanced functionality,not present in VB Com.
									{
										base.ReportLevel=Declarations.GetIntValFromString(objTmpElt.GetAttribute("codeid"),m_iClientId) ;//any numeric combo box is set as a "codeid" in RequestToXML()
									}
									break;
									
								}
								case "usereportlevel" :
								{
									if(objTmpElt.GetAttribute("codeid")!="")//Enhanced functionality,not present in VB Com.
									{
										base.UseReportLevel = Declarations.GetIntValFromString(objTmpElt.GetAttribute("codeid"),m_iClientId) ;//any numeric combo box is set as a "codeid" in RequestToXML()
									}
									break;
								}
								case "begindate" :
								{
									if(!bUseYear)
									{
										base.BeginDate =Conversion.ToDate(Conversion.GetDate(objTmpElt.GetAttribute("value")));
									}
									break;
								}
								case "enddate" :
								{
									if(!bUseYear)
									{
										base.EndDate = Conversion.ToDate(Conversion.GetDate(objTmpElt.GetAttribute("value")));
									}
									break;
								}
								case "allentitiesflag" :
								{
									objElt = ((XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='allentitiesflag' and @checked]"));
									if(objElt!=null)
									{
										base.AllEntitiesFlag =Declarations.GetBoolValFromXmlAttribute(objElt.GetAttribute("value"),m_iClientId);
										objElt=null;
									}
									break;
								}
								case "selectedentities" :
								{
									m_arrlstEntities = new ArrayList();
									foreach(XmlElement objTemp in objTmpElt.ChildNodes)
									{
										base.SelectedEntities.Add(Declarations.GetStrValFromXmlAttribute(objTemp.GetAttribute("value"),m_iClientId));
									}
									break;
								}
								case "yearofreport" :
								{
									if(bUseYear)
									{
										base.YearOfReport =Declarations.GetStrValFromXmlAttribute(objTmpElt.GetAttribute("codeid"),m_iClientId);
									}
									break;
								}
								case "eventbasedflag" :
								{
									base.EventBasedFlag = Declarations.IsChecked(objTmpElt,m_iClientId);
									break;
								}
								case "printsofterrlog" :
								{
									base.PrintSoftErrLog = Declarations.IsChecked(objTmpElt,m_iClientId);
									break;
								}
								case "printoshadescflag" :
								{
									m_bPrintOSHADescFlag = Declarations.IsChecked(objTmpElt,m_iClientId);
									break;
								}
								case "primarylocationflag" :
								{
									m_bPrimaryLocationFlag = Declarations.IsChecked(objTmpElt,m_iClientId);
									break;
								}
								case "preparername" :
								{
									base.Preparer.Name =Declarations.GetStrValFromXmlAttribute(objTmpElt.GetAttribute("value"),m_iClientId);
									break;
								}
								case "preparertitle" :
								{
									base.Preparer.Title = Declarations.GetStrValFromXmlAttribute(objTmpElt.GetAttribute("value"),m_iClientId);
									break;
								}
								case "preparerphone" :
								{
									base.Preparer.Phone = Declarations.GetStrValFromXmlAttribute(objTmpElt.GetAttribute("value"),m_iClientId);
									break;
								}
								case "enforce180dayrule" :
								{
									base.Enforce180DayRule =Declarations.GetBoolValFromXmlAttribute(Declarations.GetXMLValue("enforce180dayrule", objCriteriaXML,m_iClientId),m_iClientId);
									break;
								}
								case "asofdate" :
								{
									base.AsOfDate =Conversion.ToDate(Declarations.GetXMLValue("asofdate", objCriteriaXML,m_iClientId));
									break;
								}
								case "establishmentnameprefix" :
								{
									base.EstablishmentNamePrefix = Declarations.GetXMLValue("establishmentnameprefix", objCriteriaXML,m_iClientId);
									break;
								}
								case "sortorder" :
								{
									base.ItemSortOrder =Declarations.GetIntValFromString(Declarations.GetXMLValue("sortorder", objCriteriaXML,m_iClientId),m_iClientId);
									break;
								}
                                    //MITS 37193 Begin
                                case "columnesource":
                                {
                                    base.ColumnESource = Declarations.GetIntValFromString(Declarations.GetXMLValue("columnesource", objCriteriaXML,m_iClientId),m_iClientId);
                                    break;
                                }
                                //MITS 37193 End
								case "columnfsource" :
								{
									base.ColumnFSource =Declarations.GetIntValFromString(Declarations.GetXMLValue("columnfsource", objCriteriaXML,m_iClientId),m_iClientId);
									break;
								}
								case "filtereventid" :
								{
									this.FilterEventId =Declarations.GetIntValFromString(objTmpElt.GetAttribute("value"),m_iClientId);
									break;
								}
								case "filterclaimid" :
								{
									this.FilterClaimId = Declarations.GetIntValFromString(objTmpElt.GetAttribute("value"),m_iClientId);
									break;
								}
								case "byoshaestablishmentflag":
								{
									objElt =((XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='byoshaestablishmentflag' and @checked]"));
									if(objElt!=null)
									{
										base.ByOSHAEstablishmentFlag =Declarations.GetBoolValFromXmlAttribute(objElt.GetAttribute("value"),m_iClientId);
									}
									break;
								}
							}
						}
					}
					this.ValidateCriteria();
					m_objCriteriaXML=value;
							
				}
				catch(RMAppException p_objException)
				{
					throw p_objException;
				}
				catch(Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("OSHA301Report.CriteriaXmlDom.ErrorSet",m_iClientId),p_objException);
				}
				finally
				{
					objPrevXml=null;
					objControlNodes=null;					
					objElt=null;
					objCriteriaXML=null;
				}
				#endregion
			}
		}
		
		#endregion

		#region Constructor
		/// <summary>
		/// This is default constructor.
		/// </summary>
		/// <param name="p_sDsn">Database connection string</param>
		internal OSHA301Report(string p_sDsn,int p_iClientId)//:base(p_sDsn)
		{
			m_sDsn=p_sDsn;
            base.ClientId = p_iClientId;
			base.m_objInstance=new InstanceUtilities(p_sDsn,base.ClientId);
			base.m_objColErrors = new ColError();
			base.m_objCompany = new EntityItem();
			base.m_objPreparer=new Preparer();
            
		}
		#endregion

		#region Methods
		/// <summary>
		/// This method will fetch the value of Person Involved Type of Employee. 
		/// </summary>
		public void Init()
		{
			try
			{
				m_iPITypeEmployee=m_objInstance.GetSingleInt("CODE_ID", "CODES", "SHORT_CODE='E' AND TABLE_ID=" + m_objInstance.GetTableID("PERSON_INV_TYPE"));
				if(m_iPITypeEmployee == 0)
				{
					throw new RMAppException(Globalization.GetString("OSHA301Report.Init.NoPIFound",m_iClientId));
				}
       
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("OSHA301Report.Init.Error",m_iClientId),p_objException);
			}
		}
		
		/// <summary>
		/// This method would validate the report criteria(s) of the base class depending upon the value of Claim Id and Filter Id.
		/// </summary>
		/// <returns>True/False</returns>
		protected override bool ValidateCriteria()
		{
			bool bRetVal=false;
			try
			{
				if(this.FilterClaimId!=0 || this.FilterEventId!=0)
				{
					base.ReportLevel = 1012;
					bRetVal=true;
					return bRetVal;//OSHA301
				}
                base.ValidateCriteria();
				bRetVal=true;
			}
			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("OSHA301Report.ValidateCriteria.Error",m_iClientId),p_objException);
			}
			return bRetVal;
		}
		/// <summary>
		/// This method would invoke the methods for validating the report criteria(s)
		/// and fetching the data to be shown in OSHA Report.
		/// </summary>
		protected virtual void ValidateAndLoadReport()
		{
			try
			{
				m_bPrintOSHADescFlag = Convert.ToBoolean(m_objInstance.GetSingleInt("PRINT_OSHA_DESC","SYS_PARMS",""));
				this.ValidateCriteria();
				RptOSHA objRptOsha=new RptOSHA(m_sDsn,m_iClientId);
				base.m_arrlstOshaItems=objRptOsha.FetchOshaData(this,m_sDsn,false);
				if(m_arrlstOshaItems.Count==0)
				{
					base.UserFileName="";
					return;
				}
				if(Convert.ToBoolean(this.FilterClaimId) || Convert.ToBoolean(this.FilterEventId))
				{
					this.FilterOshaData();
				}

			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("OSHA301Report.ValidateAndLoadReport.Error",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method will allow printing the OSHA 301 Report for a particular claim or specific to an event.
		/// </summary>
		protected virtual void FilterOshaData()
		{
			int iItemsCount=0;
			ArrayList arrlstIndex=new ArrayList();
			try
			{  
				iItemsCount=m_arrlstOshaItems.Count;
				for(int iTemp=0;iTemp<iItemsCount;iTemp++)
				{
					if(Convert.ToBoolean(this.FilterClaimId))
					{
						if(m_objInstance.GetSingleInt("CLAIM_ID", "CLAIM, PERSON_INVOLVED", "CLAIM.CLAIM_ID =" + this.FilterClaimId + " AND CLAIM.EVENT_ID=PERSON_INVOLVED.EVENT_ID AND PERSON_INVOLVED.PI_ROW_ID =" + ((OSHAItem)m_arrlstOshaItems[iTemp]).PiRowId + " AND PERSON_INVOLVED.PI_TYPE_CODE = 237")==0)
						{
							arrlstIndex.Add(iTemp);
						}
					}
					else
					{
						if(Convert.ToBoolean(this.FilterEventId))
						{
							if((base.m_objInstance.GetSingleInt("EVENT_ID", "PERSON_INVOLVED", "PERSON_INVOLVED.PI_ROW_ID =" + ((OSHAItem)m_arrlstOshaItems[iTemp]).PiRowId + " AND PERSON_INVOLVED.EVENT_ID =" + this.FilterEventId)==0))
							{
								arrlstIndex.Add(iTemp);
							}
						}
					}
				}

				int iItemIndex=0;
				string sTemp="";
				for(int iTemp=0;iTemp<iItemsCount;iTemp++)
				{
					sTemp=Convert.ToString(arrlstIndex.BinarySearch(iTemp));
					if(sTemp.IndexOf("-")==0)
					{
						iItemIndex=iTemp;
							break;
					}

				}
				arrlstIndex.Clear();
				arrlstIndex.Add(m_arrlstOshaItems[iItemIndex]);
				m_arrlstOshaItems=arrlstIndex;	
				arrlstIndex=null;					
				}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("OSHA301Report.FilterOshaData.Error",m_iClientId),p_objException);
			}
			finally
			{
				arrlstIndex=null;
			}
		}

		/// <summary>
		/// This method would invoke the method that would validate the report criteria(s)
		/// and fetch the data to be displayed in the OSHA Report. 
		/// </summary>
		public void LoadEntities()
		{
			try
			{
				this.ValidateAndLoadReport();
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("OSHA301Report.LoadEntities.Error",m_iClientId),p_objException);
			}
		}
		#endregion
	}
	}
		
	

