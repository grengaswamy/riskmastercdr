using System;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using DataDynamics.ActiveReports;

namespace Riskmaster.Application.OSHALib
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004	
	/// Purpose :  This is the class corresponding to the OSHA301 Report (Active Report).
	///			   It contains methods related to the report layout, report fields initialization and populating them with data.
	///</summary>
	internal class OSHA301_2004 : CustomActiveReport
	{
		#region Member Variables
		/// <summary>
		/// Keeps track of report in process.
		/// </summary>
		protected int m_iReportInProcess;
		/// <summary>
		/// Keeps track of number of items to be printed.
		/// </summary>
		protected int m_iItemCount;
		#endregion

		#region Constructor
		/// <summary>
		/// Default constructor.
		/// </summary>
		internal OSHA301_2004(int p_iClientId):base(p_iClientId)
		{
			InitializeReport();
            base.m_iClientId = p_iClientId;
		}
		#endregion

		#region Methods
		/// <summary>
		/// This method would add and initialize the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void OSHA301_2004_DataInitialize(object p_objsender, System.EventArgs p_objeArgs)
		{
			string sTmp="";
			try
			{
				sTmp = sTmp + "This Injury and Illness Incident Report is one of the" + "\n"
					+ "first forms you must fill out when a recordable work-" + "\n"
					+ "related injury or illness has occurred.  Together with" + "\n"
					+ "the Log of Work-Related Injuries and Illnesses and the" + "\n"
					+ "accompanying Summary, these forms help the" + "\n"
					+ "employer and OSHA develop a picture of the extent" + "\n"
					+ "and severity of work-related incidents." + "\n"
					+ "    Within 7 calendar days after you receive" + "\n"
					+ "information that a recordable work-related injury or" + "\n"
					+ "illness has occurred, you must fill out this form or an" + "\n"
					+ "equivalent.  Some state workers' compensation," + "\n"
					+ "insurance, or other reports may be acceptable" + "\n"
					+ "substitutes.  To be considered an equivalent form," + "\n"
					+ "any substitue must contain all the information" + "\n"
					+ "asked for on this form." + "\n"
					+ "    According to Public Law 91-596 and 29 CFR" + "\n"
					+ "1904, OSHA's recordkeepling rule, you must keep" + "\n"
					+ "this form on file for 5 years following the year to" + "\n"
					+ "which it pertains." + "\n"
					+ "    If you need additional copies of this form, you" + "\n"
					+ "may photocopy and use as many as you need.";
		
				this.Label48.Text = sTmp;
		
				this.Fields.Add("Config");
				this.Fields.Add("PreparerName");
				this.Fields.Add("PreparerTitle");
				this.Fields.Add("PreparerPhone");
				this.Fields.Add("PreparerDate");
		
				this.Fields.Add("EmployeeName");
				this.Fields.Add("EmployeeStreet");
				this.Fields.Add("EmployeeCity");
				this.Fields.Add("EmployeeState");
				this.Fields.Add("EmployeeZip");
				this.Fields.Add("EmployeeDateofBirth");
				this.Fields.Add("EmployeeDateHired");
				this.Fields.Add("EmployeeSexMale");
				this.Fields.Add("EmployeeSexFemale");
				this.Fields.Add("PhysicianName");
				this.Fields.Add("FacilityName");
				this.Fields.Add("FacilityStreet");
				this.Fields.Add("FacilityCity");
				this.Fields.Add("FacilityState");
				this.Fields.Add("FacilityZip");
				this.Fields.Add("EmergencyRoomNo");
				this.Fields.Add("EmergencyRoomYes");
				this.Fields.Add("OvernightNo");
				this.Fields.Add("OvernightYes");
				this.Fields.Add("EventNumber");
				this.Fields.Add("DateOfEvent");
				this.Fields.Add("EmployeeBeginTime");
				this.Fields.Add("TimeOfEvent");
				this.Fields.Add("NoTimeOfEvent");
				this.Fields.Add("ActivityWhenInjured");
				this.Fields.Add("HowEventOccurred");
				this.Fields.Add("InjuryDesc");
				this.Fields.Add("IllnessDesc");
				this.Fields.Add("ObjectSubstanceThatInjured");
				this.Fields.Add("DateOfDeath");
				this.Fields.Add("Detail");
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OSHA301_2004.OSHA301_2004_DataInitialize.Error", m_iClientId), p_objException);
			}
		}

		/// <summary>
		/// This method would fill data in the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void OSHA301_2004_FetchData(object p_objsender, FetchEventArgs p_objeArgs)
		{
			OSHA301Report obj301=null; 			
			string sReportConfig="";
			OSHAItem objItem=null;		
			try
			{
				Declarations.CheckAbort(m_objNotify,m_iClientId);
				if(this.m_iReportInProcess == 0)
				{
					this.m_iReportInProcess=1;
				}

				while(this.m_iReportInProcess>0)
				{
					if(this.m_iReportInProcess>m_arrlstRpts.Count)
					{
						p_objeArgs.EOF=true;
						return;
					}
					else
					{
						p_objeArgs.EOF=false;
					}
					obj301=(OSHA301Report)m_arrlstRpts[this.m_iReportInProcess-1];
					if(obj301.Items.Count==0)
					{
						this.m_iReportInProcess=this.m_iReportInProcess+1;
						obj301=null;
					}
					else
					{
						break;
					}
				}
				//Pump the data out....
				if(obj301!=null)
				{
					if(obj301.AsOfDate!=DateTime.MinValue)
					{
					 
						sReportConfig = sReportConfig + "As of Date:    " + Declarations.ConvertDateTimeToMmDdYyyy(obj301.AsOfDate,m_iClientId);
					}
					else
					{
						sReportConfig = sReportConfig + "As of Date:    ";
					}
					sReportConfig = sReportConfig + "\n" + "Event Range:    " + Declarations.ConvertDateTimeToMmDdYyyy(obj301.BeginDate,m_iClientId) + " To " + Declarations.ConvertDateTimeToMmDdYyyy(obj301.EndDate,m_iClientId);
					sReportConfig = sReportConfig + "\n" + "180 Day Rule:  " + obj301.Enforce180DayRule;
					sReportConfig = sReportConfig + "\n" + "Event Based:   " + obj301.EventBasedFlag;
					sReportConfig = sReportConfig + "\n" + "Run Date:      " + Declarations.ConvertDateTimeToMmDdYyyy(DateTime.Today,m_iClientId);
					this.Fields["Config"].Value = sReportConfig;
					if(obj301.Preparer!=null)
					{
						this.Fields["PreparerName"].Value = obj301.Preparer.Name;
						this.Fields["PreparerTitle"].Value = obj301.Preparer.Title;
						this.Fields["PreparerPhone"].Value = obj301.Preparer.Phone;
						//check for the format...!!!!
						this.Fields["PreparerDate"].Value =Declarations.ConvertDateTimeToMmDdYyyy(DateTime.Today,m_iClientId);
					}
					//get the items one by one from each report...
					if(this.m_iItemCount==0)
					{
						this.m_iItemCount=1;
					}
					objItem=((OSHAItem)obj301.Items[this.m_iItemCount-1]);
					if(Convert.ToBoolean(objItem.PrivacyCase))
					{
						this.Fields["EmployeeName"].Value = "Privacy Case";
						this.Fields["EmployeeStreet"].Value = "Privacy Case";
                        this.Fields["EmployeeCity"].Value = "Privacy Case";
                        this.Fields["EmployeeState"].Value = "Privacy Case";
                        this.Fields["EmployeeZip"].Value = "Privacy Case";
                        this.Fields["EmployeeDateofBirth"].Value = "Privacy Case";
                        this.Fields["EmployeeDateHired"].Value = "Privacy Case";

                        this.Fields["EmployeeSexFemale"].Value = false;
                        this.Fields["EmployeeSexMale"].Value = false;

                        this.Fields["DateOfEvent"].Value = "Privacy Case";
                        this.Fields["EmployeeBeginTime"].Value = "Privacy Case";

                        this.Fields["TimeOfEvent"].Value = "Privacy Case";
                        this.Fields["NoTimeOfEvent"].Value = false;


                        this.Fields["HowEventOccurred"].Value = "Privacy Case";
                        this.Fields["InjuryDesc"].Value = "Privacy Case";

                        this.Fields["ObjectSubstanceThatInjured"].Value = "Privacy Case";

					}
					else
					{
						this.Fields["EmployeeName"].Value = objItem.EmployeeFullName;
						this.Fields["EmployeeStreet"].Value = objItem.EmployeeAddress;
                        this.Fields["EmployeeCity"].Value = objItem.EmployeeCity;
                        this.Fields["EmployeeState"].Value = objItem.EmployeeState;
                        this.Fields["EmployeeZip"].Value = objItem.EmployeeZipCode;
                        this.Fields["EmployeeDateofBirth"].Value = Declarations.ConvertDateTimeToMmDdYyyy(objItem.EmployeeDateOfBirth,m_iClientId);
                        this.Fields["EmployeeDateHired"].Value = Declarations.ConvertDateTimeToMmDdYyyy(objItem.DateOfHire,m_iClientId);

                        switch (objItem.EmployeeSex)
                        {
                            case "F":
                                this.Fields["EmployeeSexFemale"].Value = true;
                                this.Fields["EmployeeSexMale"].Value = false;
                                break;
                            case "M":
                                this.Fields["EmployeeSexFemale"].Value = false;
                                this.Fields["EmployeeSexMale"].Value = true;
                                break;
                            default:
                                this.Fields["EmployeeSexFemale"].Value = false;
                                this.Fields["EmployeeSexMale"].Value = false;
                                break;
                        }

                        this.Fields["DateOfEvent"].Value = Declarations.ConvertDateTimeToMmDdYyyy(objItem.DateOfEvent,m_iClientId);
                        this.Fields["EmployeeBeginTime"].Value = objItem.EmployeeStartTime;

                        if (objItem.TimeOfEvent == "")
                        {
                            this.Fields["TimeOfEvent"].Value = "";
                            this.Fields["NoTimeOfEvent"].Value = true;
                        }
                        else
                        {
                            this.Fields["TimeOfEvent"].Value = objItem.TimeOfEvent;
                            this.Fields["NoTimeOfEvent"].Value = false;
                        }

                        this.Fields["HowEventOccurred"].Value = objItem.HowEventOccurred;
                        if (objItem.DisabilityCode.ToUpper() == "INJ")
                            this.Fields["InjuryDesc"].Value = objItem.InjuryDesc;
                        else if (objItem.DisabilityCode.ToUpper() == "ILL")
                            this.Fields["InjuryDesc"].Value = objItem.IllnessDesc;

                        this.Fields["ObjectSubstanceThatInjured"].Value = objItem.ObjectSubstanceThatInjured;
					}            
 
					this.Fields["PhysicianName"].Value = objItem.HealthCareProf;
					this.Fields["FacilityName"].Value = objItem.HealthCareEntityFacility;
					this.Fields["FacilityStreet"].Value = objItem.HealthCareEntityAddress;
					this.Fields["FacilityCity"].Value = objItem.HealthCareEntityCity;
					this.Fields["FacilityState"].Value = objItem.HealthCareEntityState;
					this.Fields["FacilityZip"].Value = objItem.HealthCareEntityZipCode;
				
					switch(objItem.EmergencyRoomTreatment)
					{
						case false :
							this.Fields["EmergencyRoomNo"].Value= true;
							this.Fields["EmergencyRoomYes"].Value = false;
							break;
						case true:
							this.Fields["EmergencyRoomNo"].Value = false;
							this.Fields["EmergencyRoomYes"].Value = true;
							break;
						default:
							this.Fields["EmergencyRoomNo"].Value = false;
							this.Fields["EmergencyRoomYes"].Value = false;
							break;
					}
					switch(objItem.HospitalizedOverNight)
					{
						case false :
							this.Fields["OvernightNo"].Value= true;
							this.Fields["OvernightYes"].Value = false;
							break;
						case true:
							this.Fields["OvernightNo"].Value = false;
							this.Fields["OvernightYes"].Value = true;
							break;
						default:
							this.Fields["OvernightNo"].Value = false;
							this.Fields["OvernightYes"].Value = false;
							break;
					}
					this.Fields["EventNumber"].Value = objItem.CaseNumber;
					this.Fields["DateOfEvent"].Value=Declarations.ConvertDateTimeToMmDdYyyy(objItem.DateOfEvent,m_iClientId);
					this.Fields["EmployeeBeginTime"].Value = objItem.EmployeeStartTime;
					if(objItem.TimeOfEvent=="")
					{
						this.Fields["TimeOfEvent"].Value = "";
						this.Fields["NoTimeOfEvent"].Value = true;
					}
					else
					{
						this.Fields["TimeOfEvent"].Value = objItem.TimeOfEvent;
						this.Fields["NoTimeOfEvent"].Value = false;
					}
					this.Fields["ActivityWhenInjured"].Value = objItem.ActivityWhenInjured;
					this.Fields["HowEventOccurred"].Value = objItem.HowEventOccurred;
					if( objItem.DisabilityCode.ToUpper() == "INJ" )
						this.Fields["InjuryDesc"].Value = objItem.InjuryDesc;
					else if( objItem.DisabilityCode.ToUpper() == "ILL" )
						this.Fields["InjuryDesc"].Value = objItem.IllnessDesc;
					this.Fields["ObjectSubstanceThatInjured"].Value = objItem.ObjectSubstanceThatInjured;
					this.Fields["DateOfDeath"].Value=Declarations.ConvertDateTimeToMmDdYyyy(objItem.DateOfDeath,m_iClientId);

                    //Raman:MITS 14844
                    if (!objItem.EmployeeOshaRecordable)
                    {
                        ClearFields();
                    }

					objItem=null;
					this.m_iItemCount=this.m_iItemCount+1;
					if(this.m_iItemCount > obj301.Items.Count)
					{
						obj301 = null;
						this.m_iItemCount = 1;
						this.m_iReportInProcess = this.m_iReportInProcess + 1;
					}
                    
                    
				}
			}						
			catch(RMAppException p_objException)
			{
				p_objeArgs.EOF=true;
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				p_objeArgs.EOF=true;
				throw new RMAppException(Globalization.GetString("OSHA301_2004.OSHA301_2004_FetchData.Error",m_iClientId),p_objException);
			}
			finally
			{
			}
		}

		/// <summary>
		/// This method would do the page settings before start printing the report 
		/// and invoke base class method to print the soft error log.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void OSHA301_2004_ReportStart(object p_objsender, System.EventArgs p_objeArgs)
		{
			try
			{
                //MITS 10630 the user for cws may not have any defalut printer
                this.Document.Printer.PrinterName = string.Empty;
                this.PageSettings.PaperHeight=14f;
				this.PageSettings.PaperWidth=(float)8.5;
				this.PageSettings.Orientation=DataDynamics.ActiveReports.Document.PageOrientation.Landscape;
				base.SoftErrorLogPrint();
				//print soft error...!!!!!!
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OSHA301_2004.OSHA301_2004_ReportStart.Error", m_iClientId), p_objException);
			}
		}
		/// <summary>
		/// This method would clear the fields of the report.
		/// </summary>
		private void ClearFields()
		{
			try
			{
				this.Fields["Config"].Value="";
				this.Fields["PreparerName"].Value="";
				this.Fields["PreparerTitle"].Value="";
				this.Fields["PreparerPhone"].Value="";
				this.Fields["PreparerDate"].Value="";
		
				this.Fields["EmployeeName"].Value="";
				this.Fields["EmployeeStreet"].Value="";
				this.Fields["EmployeeCity"].Value="";
				this.Fields["EmployeeState"].Value="";
				this.Fields["EmployeeZip"].Value="";
				this.Fields["EmployeeDateofBirth"].Value="";
				this.Fields["EmployeeDateHired"].Value="";
				this.Fields["EmployeeSexMale"].Value="";
				this.Fields["EmployeeSexFemale"].Value="";
				this.Fields["PhysicianName"].Value="";
				this.Fields["FacilityName"].Value="";
				this.Fields["FacilityStreet"].Value="";
				this.Fields["FacilityCity"].Value="";
				this.Fields["FacilityState"].Value="";
				this.Fields["FacilityZip"].Value="";
				this.Fields["EmergencyRoomNo"].Value="";
				this.Fields["EmergencyRoomYes"].Value="";
				this.Fields["OvernightNo"].Value="";
				this.Fields["OvernightYes"].Value="";
				this.Fields["EventNumber"].Value="";
				this.Fields["DateOfEvent"].Value="";
				this.Fields["EmployeeBeginTime"].Value="";
				this.Fields["TimeOfEvent"].Value="";
				this.Fields["NoTimeOfEvent"].Value="";
				this.Fields["ActivityWhenInjured"].Value="";
				this.Fields["HowEventOccurred"].Value="";
				this.Fields["InjuryDesc"].Value="";
				this.Fields["IllnessDesc"].Value="";
				this.Fields["ObjectSubstanceThatInjured"].Value="";
				this.Fields["DateOfDeath"].Value="";
				this.Fields["Detail"].Value="";
                this.Fields["EmployeeSexFemale"].Value = false;
                this.Fields["EmployeeSexMale"].Value = false;
                this.Fields["EmergencyRoomNo"].Value = false;
                this.Fields["EmergencyRoomYes"].Value = false;
                this.Fields["OvernightNo"].Value = false;
                this.Fields["OvernightYes"].Value = false;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OSHA301_2004.ClearFields.Error", m_iClientId), p_objException);
			}
		}
		#endregion

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.Label Label01 = null;
		private DataDynamics.ActiveReports.Label Label07 = null;
		private DataDynamics.ActiveReports.Label Label48 = null;
		private DataDynamics.ActiveReports.Line Line119 = null;
		private DataDynamics.ActiveReports.Label Label05 = null;
		private DataDynamics.ActiveReports.Shape Shape11 = null;
		private DataDynamics.ActiveReports.Label Label02 = null;
		private DataDynamics.ActiveReports.Label Label04 = null;
		private DataDynamics.ActiveReports.TextBox Field2 = null;
		private DataDynamics.ActiveReports.Label Label08 = null;
		private DataDynamics.ActiveReports.Label Label32 = null;
		private DataDynamics.ActiveReports.Line Line130 = null;
		private DataDynamics.ActiveReports.Line Line131 = null;
		private DataDynamics.ActiveReports.Label Label06 = null;
		private DataDynamics.ActiveReports.Label Label28 = null;
		private DataDynamics.ActiveReports.Label Label29 = null;
		private DataDynamics.ActiveReports.Label Label30 = null;
		private DataDynamics.ActiveReports.Label Label31 = null;
		private DataDynamics.ActiveReports.Label Label33 = null;
		private DataDynamics.ActiveReports.Label Label34 = null;
		private DataDynamics.ActiveReports.Label Label35 = null;
		private DataDynamics.ActiveReports.Label Label36 = null;
		private DataDynamics.ActiveReports.Label Label37 = null;
		private DataDynamics.ActiveReports.Label Label38 = null;
		private DataDynamics.ActiveReports.Label Label39 = null;
		private DataDynamics.ActiveReports.CheckBox chkTime = null;
		private DataDynamics.ActiveReports.Line Line151 = null;
		private DataDynamics.ActiveReports.Line Line152 = null;
		private DataDynamics.ActiveReports.Line Line153 = null;
		private DataDynamics.ActiveReports.Line Line154 = null;
		private DataDynamics.ActiveReports.Label Label40 = null;
		private DataDynamics.ActiveReports.Label Label41 = null;
		private DataDynamics.ActiveReports.Label Label42 = null;
		private DataDynamics.ActiveReports.Line Line155 = null;
		private DataDynamics.ActiveReports.Label Label43 = null;
		private DataDynamics.ActiveReports.Label Label44 = null;
		private DataDynamics.ActiveReports.Label Label45 = null;
		private DataDynamics.ActiveReports.Label Label46 = null;
		private DataDynamics.ActiveReports.Label Label47 = null;
		private DataDynamics.ActiveReports.TextBox EventNumber = null;
		private DataDynamics.ActiveReports.TextBox TimeOfEvent = null;
		private DataDynamics.ActiveReports.TextBox DateOfEvent = null;
		private DataDynamics.ActiveReports.TextBox EmployeeBeginTime = null;
		private DataDynamics.ActiveReports.TextBox ActivityWhenInjured = null;
		private DataDynamics.ActiveReports.TextBox HowEventOccurred = null;
		private DataDynamics.ActiveReports.TextBox InjuryDesc = null;
		private DataDynamics.ActiveReports.TextBox ObjectSubstanceThatInjured = null;
		private DataDynamics.ActiveReports.TextBox DateOfDeath = null;
		private DataDynamics.ActiveReports.Line Line144 = null;
		private DataDynamics.ActiveReports.Label Label03 = null;
		private DataDynamics.ActiveReports.Picture Image1 = null;
		private DataDynamics.ActiveReports.Shape Shape13 = null;
		private DataDynamics.ActiveReports.Label lblPreparerName = null;
		private DataDynamics.ActiveReports.Label lblPreparerTitle = null;
		private DataDynamics.ActiveReports.Label lblPreparerPhone = null;
		private DataDynamics.ActiveReports.Label lblPreparerDate = null;
		private DataDynamics.ActiveReports.Line Line145 = null;
		private DataDynamics.ActiveReports.Line Line146 = null;
		private DataDynamics.ActiveReports.Line Line147 = null;
		private DataDynamics.ActiveReports.Line Line148 = null;
		private DataDynamics.ActiveReports.TextBox PreparerName = null;
		private DataDynamics.ActiveReports.TextBox PreparerTitle = null;
		private DataDynamics.ActiveReports.TextBox PreparerPhone = null;
		private DataDynamics.ActiveReports.TextBox PreparerDate = null;
		private DataDynamics.ActiveReports.Label Label09 = null;
		private DataDynamics.ActiveReports.Label Label10 = null;
		private DataDynamics.ActiveReports.Label Label11 = null;
		private DataDynamics.ActiveReports.Label Label12 = null;
		private DataDynamics.ActiveReports.Label Label13 = null;
		private DataDynamics.ActiveReports.Label Label14 = null;
		private DataDynamics.ActiveReports.Label Label15 = null;
		private DataDynamics.ActiveReports.Label Label16 = null;
		private DataDynamics.ActiveReports.Label Label17 = null;
		private DataDynamics.ActiveReports.CheckBox chkboxMale = null;
		private DataDynamics.ActiveReports.CheckBox chkFemale = null;
		private DataDynamics.ActiveReports.Label Label18 = null;
		private DataDynamics.ActiveReports.Label Label19 = null;
		private DataDynamics.ActiveReports.Label Label20 = null;
		private DataDynamics.ActiveReports.Label Label21 = null;
		private DataDynamics.ActiveReports.Label Label22 = null;
		private DataDynamics.ActiveReports.Label Label23 = null;
		private DataDynamics.ActiveReports.Label Label24 = null;
		private DataDynamics.ActiveReports.Label Label25 = null;
		private DataDynamics.ActiveReports.Label Label26 = null;
		private DataDynamics.ActiveReports.CheckBox chkEmergencyYes = null;
		private DataDynamics.ActiveReports.CheckBox chkEmergencyNo = null;
		private DataDynamics.ActiveReports.Label Label27 = null;
		private DataDynamics.ActiveReports.CheckBox chkHospitalizedYes = null;
		private DataDynamics.ActiveReports.CheckBox chkHospitializedNo = null;
		private DataDynamics.ActiveReports.Line Line132 = null;
		private DataDynamics.ActiveReports.Line Line133 = null;
		private DataDynamics.ActiveReports.Line Line134 = null;
		private DataDynamics.ActiveReports.Line Line135 = null;
		private DataDynamics.ActiveReports.Line Line136 = null;
		private DataDynamics.ActiveReports.Line Line137 = null;
		private DataDynamics.ActiveReports.Line Line138 = null;
		private DataDynamics.ActiveReports.Line Line139 = null;
		private DataDynamics.ActiveReports.Line Line140 = null;
		private DataDynamics.ActiveReports.Line Line141 = null;
		private DataDynamics.ActiveReports.Line Line142 = null;
		private DataDynamics.ActiveReports.Line Line143 = null;
		private DataDynamics.ActiveReports.Line Line149 = null;
		private DataDynamics.ActiveReports.Line Line150 = null;
		private DataDynamics.ActiveReports.TextBox EmployeeName = null;
		private DataDynamics.ActiveReports.TextBox EmployeeStreet = null;
		private DataDynamics.ActiveReports.TextBox EmployeeCity = null;
		private DataDynamics.ActiveReports.TextBox EmployeeState = null;
		private DataDynamics.ActiveReports.TextBox EmployeeZip = null;
		private DataDynamics.ActiveReports.TextBox EmployeeDateOfBirth = null;
		private DataDynamics.ActiveReports.TextBox EmployeeDateHired = null;
		private DataDynamics.ActiveReports.TextBox FacilityName = null;
		private DataDynamics.ActiveReports.TextBox FacilityStreet = null;
		private DataDynamics.ActiveReports.TextBox FacilityCity = null;
		private DataDynamics.ActiveReports.TextBox FacilityState = null;
		private DataDynamics.ActiveReports.TextBox FacilityZip = null;
		private DataDynamics.ActiveReports.TextBox PhysicianName = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		internal void InitializeReport()
		{
			try
			{
				this.LoadLayout(this.GetType(), "Riskmaster.Application.OSHALib.OSHA301_2004.rpx");
				this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
				this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
				this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
				this.Label01 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[0]));
				this.Label07 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[1]));
				this.Label48 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[2]));
				this.Line119 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[3]));
				this.Label05 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[4]));
				this.Shape11 = ((DataDynamics.ActiveReports.Shape)(this.Detail.Controls[5]));
				this.Label02 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[6]));
				this.Label04 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[7]));
				this.Field2 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[8]));
				this.Label08 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[9]));
				this.Label32 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[10]));
				this.Line130 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[11]));
				this.Line131 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[12]));
				this.Label06 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[13]));
				this.Label28 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[14]));
				this.Label29 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[15]));
				this.Label30 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[16]));
				this.Label31 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[17]));
				this.Label33 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[18]));
				this.Label34 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[19]));
				this.Label35 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[20]));
				this.Label36 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[21]));
				this.Label37 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[22]));
				this.Label38 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[23]));
				this.Label39 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[24]));
				this.chkTime = ((DataDynamics.ActiveReports.CheckBox)(this.Detail.Controls[25]));
				this.Line151 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[26]));
				this.Line152 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[27]));
				this.Line153 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[28]));
				this.Line154 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[29]));
				this.Label40 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[30]));
				this.Label41 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[31]));
				this.Label42 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[32]));
				this.Line155 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[33]));
				this.Label43 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[34]));
				this.Label44 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[35]));
				this.Label45 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[36]));
				this.Label46 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[37]));
				this.Label47 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[38]));
				this.EventNumber = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[39]));
				this.TimeOfEvent = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[40]));
				this.DateOfEvent = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[41]));
				this.EmployeeBeginTime = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[42]));
				this.ActivityWhenInjured = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[43]));
				this.HowEventOccurred = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[44]));
				this.InjuryDesc = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[45]));
				this.ObjectSubstanceThatInjured = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[46]));
				this.DateOfDeath = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[47]));
				this.Line144 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[48]));
				this.Label03 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[49]));
				this.Image1 = ((DataDynamics.ActiveReports.Picture)(this.Detail.Controls[50]));
				this.Shape13 = ((DataDynamics.ActiveReports.Shape)(this.Detail.Controls[51]));
				this.lblPreparerName = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[52]));
				this.lblPreparerTitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[53]));
				this.lblPreparerPhone = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[54]));
				this.lblPreparerDate = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[55]));
				this.Line145 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[56]));
				this.Line146 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[57]));
				this.Line147 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[58]));
				this.Line148 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[59]));
				this.PreparerName = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[60]));
				this.PreparerTitle = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[61]));
				this.PreparerPhone = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[62]));
				this.PreparerDate = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[63]));
				this.Label09 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[64]));
				this.Label10 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[65]));
				this.Label11 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[66]));
				this.Label12 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[67]));
				this.Label13 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[68]));
				this.Label14 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[69]));
				this.Label15 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[70]));
				this.Label16 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[71]));
				this.Label17 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[72]));
				this.chkboxMale = ((DataDynamics.ActiveReports.CheckBox)(this.Detail.Controls[73]));
				this.chkFemale = ((DataDynamics.ActiveReports.CheckBox)(this.Detail.Controls[74]));
				this.Label18 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[75]));
				this.Label19 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[76]));
				this.Label20 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[77]));
				this.Label21 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[78]));
				this.Label22 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[79]));
				this.Label23 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[80]));
				this.Label24 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[81]));
				this.Label25 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[82]));
				this.Label26 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[83]));
				this.chkEmergencyYes = ((DataDynamics.ActiveReports.CheckBox)(this.Detail.Controls[84]));
				this.chkEmergencyNo = ((DataDynamics.ActiveReports.CheckBox)(this.Detail.Controls[85]));
				this.Label27 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[86]));
				this.chkHospitalizedYes = ((DataDynamics.ActiveReports.CheckBox)(this.Detail.Controls[87]));
				this.chkHospitializedNo = ((DataDynamics.ActiveReports.CheckBox)(this.Detail.Controls[88]));
				this.Line132 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[89]));
				this.Line133 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[90]));
				this.Line134 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[91]));
				this.Line135 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[92]));
				this.Line136 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[93]));
				this.Line137 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[94]));
				this.Line138 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[95]));
				this.Line139 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[96]));
				this.Line140 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[97]));
				this.Line141 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[98]));
				this.Line142 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[99]));
				this.Line143 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[100]));
				this.Line149 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[101]));
				this.Line150 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[102]));
				this.EmployeeName = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[103]));
				this.EmployeeStreet = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[104]));
				this.EmployeeCity = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[105]));
				this.EmployeeState = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[106]));
				this.EmployeeZip = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[107]));
				this.EmployeeDateOfBirth = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[108]));
				this.EmployeeDateHired = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[109]));
				this.FacilityName = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[110]));
				this.FacilityStreet = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[111]));
				this.FacilityCity = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[112]));
				this.FacilityState = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[113]));
				this.FacilityZip = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[114]));
				this.PhysicianName = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[115]));
				// Attach Report Events
				this.DataInitialize += new System.EventHandler(this.OSHA301_2004_DataInitialize);
				this.FetchData += new FetchEventHandler(this.OSHA301_2004_FetchData);
				this.ReportStart += new System.EventHandler(this.OSHA301_2004_ReportStart);
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OSHA301_2004.InitializeReport.Error", m_iClientId), p_objException);
			}
		}

		#endregion
	}
}
