using System;
using System.Collections;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Drawing;

namespace Riskmaster.Application.OSHALib
	
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004
	/// Purpose :  This class inherits CriteriaEntity class.
	///			   It implements the methods/properties declared in IOSHAReport interface. 
	/// </summary>
	internal class OSHA300AReport:CriteriaEntity,IOSHAReport
	{
		#region Member Variables
		/// <summary>
		/// Average Number of Employees
		/// </summary>
		private int m_iAvgNumberofEmployees ;
		/// <summary>
		/// Total hours worked by all employees
		/// </summary>
        private int m_iTotalHoursWorked ;
		/// <summary>
		/// Image of the Company's Executive Signature
		/// </summary>
		private Bitmap m_objimgSignature=null;
		/// <summary>
		/// This array will contain all the totals to be displayed on the report.
		/// </summary>
        private int[] m_arrTotalsOnPage=new int[20];
        //private int m_iClientId;
		#endregion
		
		#region Properties
		/// <summary>
		/// This property would access the image of the Company's executive signature.
		/// </summary>
		internal Bitmap Signature
		{
			get
			{
				return m_objimgSignature;
			}
		}
		/// <summary>
		/// This property would access the average number of employees.
		/// </summary>
		internal int ExposureAvgEmployeeCount
		{
			get
			{
				return m_iAvgNumberofEmployees;
			}
			set
			{
               m_iAvgNumberofEmployees=value;
			}
		}
		/// <summary>
		/// This property would access the total hours worked by all employees.
		/// </summary>
		internal int ExposureTotalHoursWorked
		{
			get
			{
				return m_iTotalHoursWorked;
			}
			set
			{
				m_iTotalHoursWorked=value;
			}
		}
		/// <summary>
		/// This property would access the XML Document fetched/loaded for the determining OSHA Report criteria.
		/// </summary>
		public XmlDocument CriteriaXmlDom
		{
			get
			{
				XmlNodeList objControlNodes=null;
				bool bUseYear=false;
				XmlElement objEntityElt=null;
				XmlElement objEntityTmpElt=null;
				XmlDocument objCriteriaXML=null;
				try
				{
					objCriteriaXML=m_objCriteriaXML;
					objControlNodes=objCriteriaXML.GetElementsByTagName("control");
					foreach(XmlElement objTmpElt in objControlNodes)
					{
						if(objTmpElt.Attributes.GetNamedItem("checked")!=null)
						{
							objTmpElt.Attributes.RemoveNamedItem("checked");
						}
						if(objTmpElt.Attributes.GetNamedItem("checked")!=null)
						{
							objTmpElt.Attributes.RemoveNamedItem("codeid");
						}
					}
					objControlNodes = objCriteriaXML.GetElementsByTagName("control");
					if(objControlNodes!=null)
					{
						foreach(XmlElement objTmpElt in objControlNodes)
						{
							switch(objTmpElt.GetAttribute("name").ToLower())
							{
								case "reportlevel" :
								{
                                    objTmpElt.SetAttribute("codeid", Declarations.GetStrValFromInt(base.ReportLevel, m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + base.ReportLevel + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
								case "usereportlevel" :
								{
                                    objTmpElt.SetAttribute("codeid", Declarations.GetStrValFromInt(base.UseReportLevel, m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + base.UseReportLevel + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
								case "begindate" :
								{
									if(!bUseYear)
									{
										objTmpElt.SetAttribute("value",Conversion.ToDbDate(base.BeginDate));
									}
									break;
									
								}
								case "enddate" :
								{
									if(!bUseYear)
									{
										objTmpElt.SetAttribute("value",Conversion.ToDbDate(base.EndDate));
									}
									break;
								}
								case "allentitiesflag" :
								{
									if(base.AllEntitiesFlag)
									{
										objEntityElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='allentitiesflag' and @value='true']");
									}
									else
									{
										objEntityElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='allentitiesflag' and @value='false']");
									}
									if(objEntityElt!=null)
										objEntityElt.SetAttribute("checked", "");
									break;
								}
								case "selectedentities" :
								{
									foreach(string vTmp in m_arrlstEntities)
									{
										objEntityElt = objCriteriaXML.CreateElement("option");
										objEntityElt.SetAttribute("value",vTmp);
										objEntityElt.InnerText = m_objInstance.GetOrgAbbreviation(Declarations.GetIntValFromString(vTmp,m_iClientId)) + " - " + m_objInstance.GetOrgName(Declarations.GetIntValFromString(vTmp,m_iClientId));
										objTmpElt.AppendChild(objEntityElt);
									}
									break;
								}
								case "yearofreport" :
								{
									objTmpElt.SetAttribute("codeid", base.YearOfReport);
									break;
								}
								case "eventbasedflag" :
								{
									if(base.EventBasedFlag)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "printsofterrlog" :
								{
									if(base.PrintSoftErrLog)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "printoshadescflag" :
								{
									if(base.PrintOSHADescFlag)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "primarylocationflag" :
								{
									if(base.PrimaryLocationFlag)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "preparername" :
								{
									
									objTmpElt.SetAttribute("value",base.Preparer.Name);
									break;
								}
								case "preparertitle" :
								{
									objTmpElt.SetAttribute("value",base.Preparer.Title);
									break;
								}
								case "preparerphone" :
								{
									objTmpElt.SetAttribute("value",base.Preparer.Phone);
									break;
								}
								case "preparersignaturedate" :
								{
									objTmpElt.SetAttribute("value",Conversion.ToDbDate(base.Preparer.SignatureDate));
									break;
								}
								case "enforce180dayrule" :
								{
									if(base.Enforce180DayRule)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "asofdate" :
								{
									objTmpElt.SetAttribute("value",Conversion.ToDbDate(base.AsOfDate));
									break;
								}
								case "establishmentnameprefix" :
								{
									objTmpElt.SetAttribute("codeid",base.EstablishmentNamePrefix); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + base.EstablishmentNamePrefix + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
								case "sortorder" :
								{
                                    objTmpElt.SetAttribute("codeid", Declarations.GetStrValFromInt(base.ItemSortOrder, m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + base.ItemSortOrder + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
                                //MITS 37193 begin
                                case "columnesource":
                                    {
                                        objTmpElt.SetAttribute("codeid", Declarations.GetStrValFromInt(base.ColumnESource, m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
                                        objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + base.ColumnESource + "']");
                                        if (objEntityElt != null)
                                        {
                                            objEntityElt.SetAttribute("selected", "");
                                        }
                                        break;
                                    }
                                //MITS 37193 End
								case "columnfsource" :
								{
                                    objTmpElt.SetAttribute("codeid", Declarations.GetStrValFromInt(base.ColumnFSource, m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + base.ColumnFSource + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
								case "byoshaestablishmentflag":
								{
									if(base.ByOSHAEstablishmentFlag)
									{
										objEntityElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='byoshaestablishmentflag' and @value='true']");
									}
									else
									{
										objEntityElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='byoshaestablishmentflag' and @value='false']");
									}
									if(objEntityElt!=null)
										objEntityElt.SetAttribute("checked","");
									break;
								}
							}
						}
					}
       

					objControlNodes = objCriteriaXML.GetElementsByTagName("internal");
					foreach(XmlElement objTmpElt in objControlNodes)
					{
						switch(objTmpElt.GetAttribute("name"))
						{
							case "reporttype" :
								objTmpElt.SetAttribute("value","1");
								objEntityTmpElt=(XmlElement)objCriteriaXML.SelectSingleNode("//report");
								objEntityTmpElt.SetAttribute("type","1");
								break;
						}
					}

				}
				catch(RMAppException p_objException)
				{
					throw p_objException;
				}
				catch(Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("OSHA300AReport.CriteriaXmlDom.Error",m_iClientId),p_objException);
				}
				finally
				{
					objControlNodes=null;					
					objEntityElt=null;
					objEntityTmpElt=null;					
				}
				return objCriteriaXML;
			}
			set
			{
				#region for setting criteria...
				XmlDocument objPrevXml=null;
				XmlNodeList objControlNodes=null;
				bool bUseYear=false;
				XmlElement objElt=null;
				XmlDocument objCriteriaXML=null;
				try
				{
					objPrevXml=m_objCriteriaXML;
					objCriteriaXML=value;
					objElt = ((XmlElement)objCriteriaXML.SelectSingleNode("//control[@name = 'useyear']"));
					if(objElt!=null)
					{
						bUseYear=objElt.InnerText.ToLower()=="true" ? true:false;
					}
					objControlNodes = objCriteriaXML.GetElementsByTagName("control");
					if(objControlNodes!=null)
					{
						foreach(XmlElement objTmpElt in objControlNodes)
						{
							switch(objTmpElt.GetAttribute("name").ToLower())
							{
								case "reportlevel" :
								{
									if(objTmpElt.GetAttribute("codeid")!=null)//Enhanced functionality,not present in VB Com.
									{
										base.ReportLevel=Declarations.GetIntValFromString(objTmpElt.GetAttribute("codeid"),m_iClientId) ;//any numeric combo box is set as a "codeid" in RequestToXML()
									}
									break;
									
								}
								case "usereportlevel" :
								{
									if(objTmpElt.GetAttribute("codeid")!=null)//Enhanced functionality,not present in VB Com.
									{
										base.UseReportLevel = Declarations.GetIntValFromString(objTmpElt.GetAttribute("codeid"),m_iClientId) ;//any numeric combo box is set as a "codeid" in RequestToXML()
									}
									break;
								}
								case "begindate" :
								{
									if(!bUseYear)
									{
										base.BeginDate =Conversion.ToDate(Conversion.GetDate(objTmpElt.GetAttribute("value")));
									}
									break;
								}
								case "enddate" :
								{
									if(!bUseYear)
									{
										base.EndDate = Conversion.ToDate(Conversion.GetDate(objTmpElt.GetAttribute("value")));
									}
									break;
								}
								case "allentitiesflag" :
								{
									objElt = ((XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='allentitiesflag' and @checked]"));
									if(objElt!=null)
									{
										base.AllEntitiesFlag =Declarations.GetBoolValFromXmlAttribute(objElt.GetAttribute("value"),m_iClientId);
										objElt=null;
									}
									break;
								}
								case "selectedentities" :
								{
									m_arrlstEntities = new ArrayList();
									foreach(XmlElement objTemp in objTmpElt.ChildNodes)
									{
										base.SelectedEntities.Add(Declarations.GetStrValFromXmlAttribute(objTemp.GetAttribute("value"),m_iClientId));
									}
									break;
								}
								case "yearofreport" :
								{
									if(bUseYear)
									{
										base.YearOfReport =Declarations.GetStrValFromXmlAttribute(objTmpElt.GetAttribute("codeid"),m_iClientId);
									}
									break;
								}
								case "eventbasedflag" :
								{
									base.EventBasedFlag = Declarations.IsChecked(objTmpElt,m_iClientId);
									break;
								}
								case "printsofterrlog" :
								{
									base.PrintSoftErrLog = Declarations.IsChecked(objTmpElt,m_iClientId);
									break;
								}
								case "printoshadescflag" :
								{
									base.PrintOSHADescFlag = Declarations.IsChecked(objTmpElt,m_iClientId);
									break;
								}
								case "primarylocationflag" :
								{
									base.PrimaryLocationFlag = Declarations.IsChecked(objTmpElt,m_iClientId);
									break;
								}
								case "preparername" :
								{
									base.Preparer.Name =Declarations.GetStrValFromXmlAttribute(objTmpElt.GetAttribute("value"),m_iClientId);
									break;
								}
								case "preparertitle" :
								{
									base.Preparer.Title = Declarations.GetStrValFromXmlAttribute(objTmpElt.GetAttribute("value"),m_iClientId);
									break;
								}
								case "preparerphone" :
								{
									base.Preparer.Phone = Declarations.GetStrValFromXmlAttribute(objTmpElt.GetAttribute("value"),m_iClientId);
									break;
								}
								case "preparersignaturedate" :
								{
									base.Preparer.SignatureDate = Conversion.ToDate(Conversion.GetDate(objTmpElt.GetAttribute("value")));
									break;
								}
								case "enforce180dayrule" :
								{
									base.Enforce180DayRule =Declarations.GetBoolValFromXmlAttribute(Declarations.GetXMLValue("enforce180dayrule", objCriteriaXML,m_iClientId),m_iClientId);
									break;
								}
								case "asofdate" :
								{
									base.AsOfDate =Conversion.ToDate(Declarations.GetXMLValue("asofdate", objCriteriaXML,m_iClientId));
									break;
								}
								case "establishmentnameprefix" :
								{
									base.EstablishmentNamePrefix = Declarations.GetXMLValue("establishmentnameprefix", objCriteriaXML,m_iClientId);
									break;
								}
								case "sortorder" :
								{
                                    base.ItemSortOrder = Declarations.GetIntValFromString(Declarations.GetXMLValue("sortorder", objCriteriaXML, m_iClientId), m_iClientId);
									break;
								}
                                    //MITS 37193 Begin
                                case "columnesource":
                                    {
                                        base.ColumnESource = Declarations.GetIntValFromString(Declarations.GetXMLValue("columnesource", objCriteriaXML, m_iClientId), m_iClientId);
                                        break;
                                    }
                                //MITS 37193 End
								case "columnfsource" :
								{
                                    base.ColumnFSource = Declarations.GetIntValFromString(Declarations.GetXMLValue("columnfsource", objCriteriaXML, m_iClientId), m_iClientId);
									break;
								}
								
								case "byoshaestablishmentflag":
								{
									objElt =((XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='byoshaestablishmentflag' and @checked]"));
									if(objElt!=null)
									{
										base.ByOSHAEstablishmentFlag =Declarations.GetBoolValFromXmlAttribute(objElt.GetAttribute("value"),m_iClientId);
									}
									break;
								}
							}
						}
					}
					base.ValidateCriteria();
					m_objCriteriaXML=value;
							
				}
				catch(RMAppException p_objException)
				{
					throw p_objException;
				}
				catch(Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("OSHA300AReport.CriteriaXmlDom.ErrorSet",m_iClientId),p_objException);
				}
				finally
				{
					objPrevXml=null;
					objControlNodes=null;					
					objElt=null;
					objCriteriaXML=null;
				}
				#endregion
			}
		}
		
		#endregion

		#region Constructor
		/// <summary>
		/// Riskmaster.Application.OSHALib.OSHA300AReport is the constructor.
		/// </summary>
		/// <param name="p_sDsn">Connection String</param>
		internal OSHA300AReport(string p_sDsn,int p_iClientId)
		{
			m_sDsn=p_sDsn;
            base.ClientId = p_iClientId;
            base.m_objInstance=new InstanceUtilities(p_sDsn,base.ClientId);
			base.m_objColErrors = new ColError();
			base.m_objCompany = new EntityItem();
			base.m_objPreparer=new Preparer();
            
			
		}
		#endregion

		#region Methods
		/// <summary>
		/// This method will fetch the value of Person Involved Type of Employee. 
		/// </summary>
		public void Init()
		{
			try
			{
				m_iPITypeEmployee=m_objInstance.GetSingleInt("CODE_ID","CODES","SHORT_CODE='E' AND TABLE_ID=" + m_objInstance.GetTableID("PERSON_INV_TYPE"));
				if(m_iPITypeEmployee == 0)
				{
					throw new RMAppException(Globalization.GetString("OSHA300AReport.Init.EmployeeNotFound",m_iClientId));
				}       
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("OSHA300AReport.Init.Error",m_iClientId),p_objException);
			}
		}		
		/// <summary>
		/// This method would invoke the method that would validate the report criteria(s)
		/// and fetch the data to be displayed in the OSHA Report. 
		/// </summary>
		public void LoadEntities()
		{
			try
			{
				this.ValidateAndLoadReport();
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("OSHA300AReport.LoadEntities.Error",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method would invoke the methods for validating the report criteria(s)
		/// and fetching the data to be shown in OSHA Report.
		/// </summary>
		protected virtual void ValidateAndLoadReport()
		{
			try
			{
				base.ValidateCriteria();
				RptOSHA objRptOsha=new RptOSHA(m_sDsn,m_iClientId);
                objRptOsha.LoadReportOnCompanyData(base.Company,this);
				base.m_arrlstOshaItems=objRptOsha.FetchOshaData(this,m_sDsn,false);
				this.ResetPageTotals();
				foreach(OSHAItem objItem in m_arrlstOshaItems)
				{
					this.AddItemToTotals(objItem);
				}
                objRptOsha.LoadExposureInfo3(this);
				m_objimgSignature=objRptOsha.FetchSignature();
			}				
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OSHA300AReport.ValidateAndLoadReport.Error", m_iClientId), p_objException);
			}
		}
		/// <summary>
		/// This method would reset the various total values shown in the report.
		/// </summary>
		protected virtual void ResetPageTotals()
		{
			try
			{
				for(int iTemp=0;iTemp<19;iTemp++)
				{
					m_arrTotalsOnPage[iTemp]=0;
				}
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OSHA300AReport.ResetPageTotals.Error", m_iClientId), p_objException);
			}
		}
		/// <summary>
		/// This method would calculate the values for the various total shown in the report.
		/// </summary>
		/// <param name="p_objItem">An OSHA Report item</param>
		protected virtual void AddItemToTotals(OSHAItem p_objItem)
		{
			try
			{
				if(p_objItem.ChkBoxDeath.ToUpper()=="YES")
				{
					m_arrTotalsOnPage[7] = m_arrTotalsOnPage[7] + 1;
				}
				else
				{
					if(p_objItem.DaysWorkLoss>0)
					{
						 m_arrTotalsOnPage[8] = m_arrTotalsOnPage[8] + 1;
					}
					else
					{
						if(p_objItem.DaysWorkLoss ==0 && p_objItem.DaysRestriction > 0)
						{
							m_arrTotalsOnPage[9] = m_arrTotalsOnPage[9] + 1;
						}
						else
						{
							if((p_objItem.DaysRestriction + p_objItem.DaysWorkLoss) == 0)
							{
								 m_arrTotalsOnPage[10] = m_arrTotalsOnPage[10] + 1;
							}
						}
																					
															
					}
				}
               m_arrTotalsOnPage[11] = m_arrTotalsOnPage[11] + p_objItem.DaysRestriction;
               m_arrTotalsOnPage[12] = m_arrTotalsOnPage[12] + p_objItem.DaysWorkLoss;
    
				if(p_objItem.RecordAsInjury == true)
				{
					m_arrTotalsOnPage[13] = m_arrTotalsOnPage[13] + 1;
				}
				if(p_objItem.RecordAsSkinDisor == true)
				{
					m_arrTotalsOnPage[15] = m_arrTotalsOnPage[15] + 1;
				}
				if(p_objItem.RecordAsRespCond == true)
				{
					m_arrTotalsOnPage[16] = m_arrTotalsOnPage[16] + 1;
				}
				if(p_objItem.RecordAsPoison == true)
				{
					m_arrTotalsOnPage[17] = m_arrTotalsOnPage[17] + 1;
				}
                //Hearing Loss is new for OSHA in 2004 
				if(p_objItem.RecordAsHearLoss == true)
				{
					m_arrTotalsOnPage[18] = m_arrTotalsOnPage[18] + 1;
				}
				if(p_objItem.RecordAsOther == true)
				{
					m_arrTotalsOnPage[19] = m_arrTotalsOnPage[19] + 1;
				}
            
           }
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OSHA300AReport.AddItemToTotals.Error", m_iClientId), p_objException);
			}
		}
		/// <summary>
		/// This method will be used to display a specific total on the report.
		/// </summary>
		/// <param name="p_iPos">Value identifies the type of total to be displayed.</param>
		/// <returns>Total value</returns>
		internal int TotalsOnPage(int p_iPos)
		{
			try
			{
				return m_arrTotalsOnPage[p_iPos];
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OSHA300AReport.TotalsOnPage.Error", m_iClientId), p_objException);
			}
		}
		#endregion
	}
}
