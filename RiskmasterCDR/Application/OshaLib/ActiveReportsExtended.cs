using System;
using DataDynamics.ActiveReports.Document;
using System.Collections;
using Riskmaster.Application.ReportInterfaces;
using System.Drawing;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using DataDynamics.ActiveReports;

namespace Riskmaster.Application.OSHALib
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004	
	/// </summary>
	
	/// <summary>
	/// This is the ultimate base class for all reports; this basically is the result of optimization of code for all reports.
	/// Also, this class can absorb future report changes if any.This class inherits the ActiveReport class in namespace DataDynamics.ActiveReports. 
	/// </summary>
	internal class CustomActiveReport:ActiveReport3
	{ 
		#region Member Variables
		/// <summary>
		/// Collection of reports.
		/// </summary>
		protected  ArrayList m_arrlstRpts=null; 
		/// <summary>
		/// Notification object.
		/// </summary>
		protected  IWorkerNotifications m_objNotify=null;
        protected int m_iClientId;
		#endregion

		#region Properties
		/// <summary>
		/// Property to access reports collection.
		/// </summary>
		public virtual  ArrayList ReportCollection
		{
			set 
			{
				m_arrlstRpts=value;
			}
		}
		/// <summary>
		/// Property to access notification object.
		/// </summary>
		public virtual  IWorkerNotifications Notify
		{
			set 
			{
				m_objNotify=value;
			}
		}		
		#endregion

		#region Constructor
		/// <summary>
		/// This is the default constructor.
		/// </summary>
//		internal CustomActiveReport()
//		{
//		}
		public CustomActiveReport(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Methods
		/// <summary>
		/// This method will print soft error log.
		/// </summary>
		protected virtual void SoftErrorLogPrint()
		{
			IOSHAReport objRpt=null;
			float fHeight=8.7f;
			float fWidth=14.1f;
			float fCurrentY=.685f;
			string sTemp="";
			Page objPage=null;
			Page objMain=null;
			SizeF structMeasureText;
			float fLeft=0f;
			Error objErr=null;
			bool bErrorFlag=false;
			
			try
			{
				if(m_arrlstRpts.Count>0)
				{
					objRpt=(IOSHAReport)m_arrlstRpts[0];
				}
				if(objRpt!=null)
				{
					if(objRpt.PrintSoftErrLog)
					{ 
						objPage=new Page();
						objPage.Height=fHeight;
						objPage.Width=fWidth;
						
						objPage.Orientation=PageOrientation.Landscape;
						this.Document.Pages.Add(objPage);
						this.Document.Pages[this.Document.Pages.Count-1].Font=new System.Drawing.Font("Arial",18.0f,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point);
						sTemp="OSHA Report - Problem Log";
						structMeasureText=objPage.MeasureText(sTemp);
						fLeft=((float)(fWidth-structMeasureText.Width))/((float)2.0);
						this.Document.Pages[this.Document.Pages.Count-1].DrawText(sTemp,fLeft,fCurrentY,structMeasureText.Width,structMeasureText.Height);
						fCurrentY=fCurrentY+structMeasureText.Height;
                       
						sTemp=Declarations.ConvertDateTimeToMmDdYyyy(objRpt.BeginDate,m_iClientId) + " " + "To" +" " +Declarations.ConvertDateTimeToMmDdYyyy(objRpt.EndDate,m_iClientId);
						structMeasureText=objPage.MeasureText(sTemp);
						fLeft=((float)(fWidth-structMeasureText.Width))/((float)2.0);
						this.Document.Pages[this.Document.Pages.Count-1].DrawText(sTemp,fLeft,fCurrentY,structMeasureText.Width,structMeasureText.Height);
						fCurrentY=fCurrentY+structMeasureText.Height;
						
						this.Document.Pages[this.Document.Pages.Count-1].Font=new System.Drawing.Font("Arial",10f,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point);
						
						for(int iTemp=0;iTemp<m_arrlstRpts.Count;iTemp++)
						{
							objRpt=(IOSHAReport)m_arrlstRpts[iTemp];
							if(objRpt.Errors!=null)
							{
								for(int iErrCount=0;iErrCount<objRpt.Errors.count;iErrCount++)
								{
									bErrorFlag=true;
									objErr=(Error)objRpt.Errors.m_arrlstError[iErrCount];
									sTemp=objErr.Description;
									structMeasureText=objPage.MeasureText(sTemp);
									this.Document.Pages[this.Document.Pages.Count-1].DrawText(sTemp,1.0f,fCurrentY,structMeasureText.Width,structMeasureText.Height);
									fCurrentY=fCurrentY+structMeasureText.Height;
									if((fCurrentY)>(fHeight-1.7f))
									{
										fCurrentY=.685f;
										objMain=new Page();
										objMain.Height=fHeight;
										objMain.Width=fWidth;
										objMain.Orientation=PageOrientation.Landscape;
										this.Document.Pages.Add(objMain);
										this.Document.Pages[this.Document.Pages.Count-1].Font=new System.Drawing.Font("Arial",10f,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point);
                     				}
								}
							}
						}
						if(!bErrorFlag)
						{
							sTemp="No problems encountered.";
							structMeasureText=objPage.MeasureText(sTemp);
							fLeft=((float)(fWidth-structMeasureText.Width))/((float)2.0);
							this.Document.Pages[this.Document.Pages.Count-1].DrawText(sTemp,fLeft,fCurrentY,structMeasureText.Width,structMeasureText.Height);
							fCurrentY=fCurrentY+structMeasureText.Height;
						}
						sTemp="End of OSHA Report - Problem Log";
						structMeasureText=objPage.MeasureText(sTemp);
						fLeft=((float)(fWidth-structMeasureText.Width))/((float)2.0);
						this.Document.Pages[this.Document.Pages.Count-1].DrawText(sTemp,fLeft,fCurrentY,structMeasureText.Width,structMeasureText.Height);                  
					}
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CustomActiveReport.SoftErrorLogPrint.Error",m_iClientId),p_objException);
			}			
			finally
			{
				objRpt = null;
				objPage = null;
				objMain = null;
			}
		}

		#endregion
	}
	/// <summary>
	/// This is the base class for all Osha300 reports.
	/// It has been framed taking into consideration the future changes to the reports(Osha300).
	/// </summary>
	internal class CommonOsha300 : CustomActiveReport 
	{
		#region Member Variables
		/// <summary>
		/// Keeps track of number of reports.
		/// </summary>
		protected int m_iReport;
		/// <summary>
		/// Keeps track of printed items.
		/// </summary>
		protected int m_iPrintedReportLineItem;
		/// <summary>
		/// Keeps track of number of pages
		/// </summary>
		protected int m_iPrintedPage;
		/// <summary>
		/// This will hold the year of the report.
		/// </summary>
		protected int m_iYearOfReport;
        /// <summary>
        /// This array will contain all the totals to be displayed on the report.
        /// </summary>
		int[] m_arrTotalsOnPage=new int[20];
		/// <summary>
		/// This will hold the value of total restricted days.
		/// </summary>
		int m_iTotalDaysRestriction=0; 
		/// <summary>
		/// This will hold the value of total work loss.
		/// </summary>
		int m_iTotalDaysWorkLoss=0;
        protected int m_iClientId=0;
		#endregion

		#region Constructor
		/// <summary>
		/// This is the constructor with year of report as parameter.
		/// </summary>
		/// <param name="p_iYearOfReport">Year Of Report</param>
        internal CommonOsha300(int p_iYearOfReport, int p_iClientId)
            : base(p_iClientId)
		{
			m_iYearOfReport=p_iYearOfReport;
            m_iClientId = p_iClientId;
		}
		
        //internal CommonOsha300()
        //{
           
        //}
		#endregion

		#region Methods
		/// <summary>
		/// This method will initialize the fields of the OSHA report to be printed.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		protected void DataInitializeBase(ref object p_objsender, ref System.EventArgs p_objeArgs)
		{
			string sTmp="";
			string sTemp="";
			try
			{
				Fields.Add("Year");
				Fields.Add("EstablishmentCity");
				Fields.Add("EstablishmentName");
				Fields.Add("EstablishmentState");
				Fields.Add("Config");
				for(int iTemp=1;iTemp<=13;iTemp++)
				{
										 
					sTemp=("0"+iTemp.ToString());		 
					sTmp=sTemp.Substring(sTemp.Length-2,2);
					Fields.Add("L" + sTmp + "A");
					Fields.Add("L" + sTmp + "B");
					Fields.Add("L" + sTmp + "C");
					Fields.Add("L" + sTmp + "D");
					Fields.Add("L" + sTmp + "E");
					Fields.Add("L" + sTmp + "F");
					Fields.Add("L" + sTmp + "G");
					Fields.Add("L" + sTmp + "H");
					Fields.Add("L" + sTmp + "I");
					Fields.Add("L" + sTmp + "J");
					Fields.Add("L" + sTmp + "WorkLoss");
					Fields.Add("L" + sTmp + "L");
					Fields.Add("L" + sTmp + "M1");
					Fields.Add("L" + sTmp + "M2");
					Fields.Add("L" + sTmp + "M3");
					Fields.Add("L" + sTmp + "M4");
					if(this.m_iYearOfReport>2003)  //check for different reports
					{
						Fields.Add("L" + sTmp + "HearingLoss");
					}
					Fields.Add("L" + sTmp + "AllOtherIllnesses");
				}
				Fields.Add("TotalG");
				Fields.Add("TotalH");
				Fields.Add("TotalI");
				Fields.Add("TotalJ");
				Fields.Add("TotalWorkLoss");
				Fields.Add("TotalRestricted");
				Fields.Add("TotalInjuries");
				Fields.Add("TotalSkinDisorder");
				Fields.Add("TotalResp");
				Fields.Add("TotalPoisonings");
				if(this.m_iYearOfReport>2003)  //check for different reports
				{
					Fields.Add("TotalHearingLoss");
				}
				Fields.Add("TotalAllOtherIllnesses");
				Fields.Add("PageNumber");
				Fields.Add("PageOfPages");
				Fields.Add("Detail");
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CommonOsha300.DataInitializeBase.Error",m_iClientId),p_objException);
			}	
			
		}
		/// <summary>
		/// This method will fetch the data for populating the fields of the OSHA report to be printed.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		protected void FetchDataBase(ref object p_objsender,ref FetchEventArgs p_objeArgs)		{
		{
			p_objeArgs.EOF=true;
			OSHA300Report obj300=null;
			int iIndex=0;
			string sTemp="";
			string sTmp="";
			string sReportConfig="";
			OSHAItem objItem=null;
			int iTmp=0;
			   
			try
			{
				if(m_objNotify!=null)
				{
					//TODO Notification logic would be finalized after SMWorker is developed.
					Declarations.CheckAbort(m_objNotify,m_iClientId);
				
				}
				if(this.m_iReport == 0)
				{
					
					this.m_iReport = 1;
					this.m_iPrintedReportLineItem = 0;
					this.m_iPrintedPage = 0;
						
				}
				else
				{
					obj300 = (OSHA300Report)m_arrlstRpts[this.m_iReport-1];
					if(this.m_iPrintedReportLineItem == obj300.Items.Count) 
					{
				
						this.m_iReport = this.m_iReport + 1;
						this.m_iPrintedReportLineItem = 0;
						this.m_iPrintedPage = 0;
						    
					}
				}
          

				if(this.m_iReport > m_arrlstRpts.Count)
				{
					return;
				}
				else
				{
					p_objeArgs.EOF=false;
				}
    
				this.ClearFields();
				obj300=(OSHA300Report)m_arrlstRpts[this.m_iReport-1];
				if(obj300!=null)
				{
					if(obj300.YearOfReport.Length >= 4)
					{
						Fields["Year"].Value = obj300.YearOfReport.Substring(2,2);
					}
					if(obj300.Company!=null)
					{
						if(this.m_iYearOfReport>2003)
						{

							Fields["EstablishmentCity"].Value = obj300.Company.City;
							Fields["EstablishmentName"].Value = obj300.Company.Name;
							Fields["EstablishmentState"].Value = obj300.Company.State;
						}
						else
						{
							if(obj300.Company.City!="")
							{
								Fields["EstablishmentCity"].Value = obj300.Company.City;
							}
							else
							{
								Fields["EstablishmentCity"].Value = "(No Data)";
							}
							if(obj300.Company.Name!="")
							{
								Fields["EstablishmentName"].Value = obj300.Company.Name;
							}
							else
							{
								Fields["EstablishmentName"].Value = "(No Data)";
							}
							if(obj300.Company.State!="")
							{
								Fields["EstablishmentState"].Value = obj300.Company.State;
							}
							else
							{
								Fields["EstablishmentState"].Value = obj300.Company.State;
							}
						}
					}
					if(this.m_iYearOfReport>2003) //check for different reports
					{
						if(obj300.AsOfDate!=DateTime.MinValue)
						{
					 
							sReportConfig = sReportConfig + "As of Date:    " + Declarations.ConvertDateTimeToMmDdYyyy(obj300.AsOfDate,m_iClientId);
						}
						else
						{
							sReportConfig = sReportConfig + "As of Date:    ";
						}
						sReportConfig = sReportConfig + "\n" + "Event Range:    " + Declarations.ConvertDateTimeToMmDdYyyy(obj300.BeginDate,m_iClientId) + " To " + Declarations.ConvertDateTimeToMmDdYyyy(obj300.EndDate,m_iClientId);
						sReportConfig = sReportConfig + "\n" + "180 Day Rule:  " + obj300.Enforce180DayRule;
						sReportConfig = sReportConfig + "\n" + "Event Based:   " + obj300.EventBasedFlag;
						sReportConfig = sReportConfig + "\n" + "Run Date:      " + Declarations.ConvertDateTimeToMmDdYyyy(DateTime.Today,m_iClientId);
         
						Fields["Config"].Value = sReportConfig;
					}
					else
					{
						if(obj300.BeginDate !=DateTime.MinValue && obj300.EndDate!=DateTime.MinValue)
						{

							if(Declarations.DateDiffInAbsoluteDays(obj300.BeginDate,obj300.EndDate,m_iClientId)!=364)
							{
								sReportConfig = sReportConfig + "\n" + "Events From:  " + Declarations.ConvertDateTimeToMmDdYyyy(obj300.BeginDate,m_iClientId);
								sReportConfig = sReportConfig + "\n" + "Events To:    " + Declarations.ConvertDateTimeToMmDdYyyy(obj300.EndDate,m_iClientId);
							}

						}
						
						if(obj300.Enforce180DayRule)
						{
							sReportConfig = sReportConfig + "\n" + "Using 180 Day Rule";
						}
						if(obj300.AsOfDate!=DateTime.MinValue)
						{
							sReportConfig = sReportConfig + "\n"+ "AS OF: " + Declarations.ConvertDateTimeToMmDdYyyy(obj300.AsOfDate,m_iClientId);
						}
						if(obj300.EventBasedFlag)
						{
							sReportConfig = sReportConfig + "\n" + "Event Based";
						}
						else
						{
							sReportConfig = sReportConfig + "\n" + "Person Involved Based";
						}
						sReportConfig = sReportConfig + "\n" + "Run Date: " + Declarations.ConvertDateTimeToMmDdYyyy(DateTime.Today,m_iClientId);
						Fields["Config"].Value = sReportConfig;

					}
					if(obj300.Items.Count>0)
					{
						iTmp=0;
						for(iIndex=(this.m_iPrintedReportLineItem + 1);iIndex<=obj300.Items.Count;iIndex++)
						{
							objItem=(OSHAItem)obj300.Items[iIndex-1];
							iTmp=iTmp+1;
							sTemp=("0"+iTmp.ToString());		 
							sTmp=sTemp.Substring(sTemp.Length-2,2);
							Fields["L" + sTmp + "A"].Value = objItem.CaseNumber;
							if(Convert.ToBoolean(objItem.PrivacyCase) == true)
							{
								Fields["L" + sTmp + "B"].Value = "Privacy Case";
							}
							else
							{
								Fields["L" + sTmp + "B"].Value = objItem.EmployeeFullName;
							}
							Fields["L" + sTmp + "C"].Value = objItem.Occupation     ;
							if(objItem.DateOfEvent!=DateTime.MinValue)
							{
								Fields["L" + sTmp + "D"].Value = objItem.DateOfEvent.Month.ToString() + "/" + objItem.DateOfEvent.Day.ToString();
							}
							else
							{
								Fields["L" + sTmp + "D"].Value="";
							}
							Fields["L" + sTmp + "E"].Value = objItem.EventLocation;
                            // akaushik5 Changed for MITS 38542 Starts
                            //Fields["L" + sTmp + "F"].Value = objItem.InjuryDesc;
                            Fields["L" + sTmp + "F"].Value = !string.IsNullOrEmpty(objItem.DisabilityCode) && objItem.DisabilityCode.ToUpper().Equals("ILL") ?
                                objItem.IllnessDesc : objItem.InjuryDesc;
                            // akaushik5 Changed for MITS 38542 Ends
     
							if(objItem.ChkBoxDeath.ToUpper() == "YES")
							{
								
								Fields["L" + sTmp + "G"].Value = true;
						
								m_arrTotalsOnPage[7] = m_arrTotalsOnPage[7] + 1;
							}
							else
							{
								if(objItem.DaysWorkLoss > 0)
								{
									
									Fields["L" + sTmp + "H"].Value = true;
									
									m_arrTotalsOnPage[8] = m_arrTotalsOnPage[8] + 1;
								}
								else
								{
									if((objItem.DaysWorkLoss == 0) && (objItem.DaysRestriction) > 0)
									{ 
										
										Fields["L" + sTmp + "I"].Value = true;
										
										m_arrTotalsOnPage[9] = m_arrTotalsOnPage[9] + 1;
									}
									else
										if((objItem.DaysRestriction + objItem.DaysWorkLoss) == 0)
									{
										
										Fields["L" + sTmp + "J"].Value = true;
										
										m_arrTotalsOnPage[10] = m_arrTotalsOnPage[10] + 1;
									}
						 
								}
							}
							Fields["L" + sTmp + "WorkLoss"].Value = objItem.DaysWorkLoss;
							m_iTotalDaysWorkLoss = m_iTotalDaysWorkLoss + objItem.DaysWorkLoss;
							Fields["L" + sTmp + "L"].Value = objItem.DaysRestriction;
							m_iTotalDaysRestriction = m_iTotalDaysRestriction + objItem.DaysRestriction;
							Fields["L" + sTmp + "M1"].Value = objItem.RecordAsInjury;
							m_arrTotalsOnPage[13] = m_arrTotalsOnPage[13] + Abs(objItem.RecordAsInjury);
							Fields["L" + sTmp + "M2"].Value = objItem.RecordAsSkinDisor;
							m_arrTotalsOnPage[15] = m_arrTotalsOnPage[15] + Abs(objItem.RecordAsSkinDisor);
							Fields["L" + sTmp + "M3"].Value = objItem.RecordAsRespCond;
							m_arrTotalsOnPage[16] = m_arrTotalsOnPage[16] + Abs(objItem.RecordAsRespCond);
							Fields["L" + sTmp + "M4"].Value = objItem.RecordAsPoison;
							m_arrTotalsOnPage[17] = m_arrTotalsOnPage[17] + Abs(objItem.RecordAsPoison);
							if(this.m_iYearOfReport>2003)
							{
								Fields["L" + sTmp + "HearingLoss"].Value = objItem.RecordAsHearLoss;
								m_arrTotalsOnPage[18] = m_arrTotalsOnPage[18] + Abs(objItem.RecordAsHearLoss);
							}
							Fields["L" + sTmp + "AllOtherIllnesses"].Value = objItem.RecordAsOther;
							m_arrTotalsOnPage[19] = m_arrTotalsOnPage[19] + Abs(objItem.RecordAsOther);
							this.m_iPrintedReportLineItem = this.m_iPrintedReportLineItem + 1;
							if((this.m_iPrintedReportLineItem%13 )== 0) 
							{
								break;
							}

						}//for loop..

					}//if loop..

	
					this.m_iPrintedPage =this.m_iPrintedPage  + 1;
					Fields["PageNumber"].Value = this.m_iPrintedPage;
					Fields["PageOfPages"].Value = obj300.OfPages;
			    
					Fields["TotalG"].Value = m_arrTotalsOnPage[7];
					Fields["TotalH"].Value = m_arrTotalsOnPage[8];
					Fields["TotalI"].Value = m_arrTotalsOnPage[9];
					Fields["TotalJ"].Value = m_arrTotalsOnPage[10];
					Fields["TotalWorkLoss"].Value = m_iTotalDaysWorkLoss;
					Fields["TotalRestricted"].Value = m_iTotalDaysRestriction;
					Fields["TotalInjuries"].Value = m_arrTotalsOnPage[13];
					Fields["TotalSkinDisorder"].Value = m_arrTotalsOnPage[15];
					Fields["TotalResp"].Value = m_arrTotalsOnPage[16];
					Fields["TotalPoisonings"].Value = m_arrTotalsOnPage[17];
					if(this.m_iYearOfReport>2003)
					{
						Fields["TotalHearingLoss"].Value = m_arrTotalsOnPage[18];
						
					}
					Fields["TotalAllOtherIllnesses"].Value = m_arrTotalsOnPage[19];
					Fields["Detail"].Value = "";
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CommonOsha300.FetchDataBase.Error",m_iClientId),p_objException);
			}	
			finally
			{
				obj300=null;
				objItem=null;
			}
		}
		}

		/// <summary>
		/// This method will do the page setting for the OSHA Report to be printed.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event</param>
		protected void ReportStartBase(ref object p_objsender, ref System.EventArgs p_objeArgs)
		{
			try
			{
                //MITS 10630 the user for cws may not have any defalut printer
                this.Document.Printer.PrinterName = string.Empty;
				this.PageSettings.PaperHeight=14;
				this.PageSettings.PaperWidth=(float)8.5;
				this.PageSettings.Orientation=DataDynamics.ActiveReports.Document.PageOrientation.Landscape;
				base.SoftErrorLogPrint();
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CommonOsha300.ReportStartBase.Error",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method will reset all the field values in the OSHA300 Report.
		/// </summary>
		private void ClearFields()
		{
			string sTemp="";
			string sTmp="";
			try
			{
				for(int iTemp=1;iTemp<=13;iTemp++)
				{
					sTemp=("0"+iTemp.ToString());		 
					sTmp=sTemp.Substring(sTemp.Length-2,2);
					Fields["L" + sTmp + "A"].Value = "";
					Fields["L" + sTmp + "B"].Value = "";
					Fields["L" + sTmp + "C"].Value = "";
					Fields["L" + sTmp + "D"].Value = "";
					Fields["L" + sTmp + "E"].Value = "";
					Fields["L" + sTmp + "F"].Value = "";
					Fields["L" + sTmp + "G"].Value = false;
					Fields["L" + sTmp + "H"].Value = false;
					Fields["L" + sTmp + "I"].Value = false;
					Fields["L" + sTmp + "J"].Value = false;
					Fields["L" + sTmp + "WorkLoss"].Value = "";
					Fields["L" + sTmp + "L"].Value = "";
					Fields["L" + sTmp + "M1"].Value = false;
					Fields["L" + sTmp + "M2"].Value = false;
					Fields["L" + sTmp + "M3"].Value = false;
					Fields["L" + sTmp + "M4"].Value = false;
					if(this.m_iYearOfReport>2003)
					{
						Fields["L" + sTmp + "HearingLoss"].Value = false;
					}
					Fields["L" + sTmp + "AllOtherIllnesses"].Value = false;
				}
                for (int iTmp = 0; iTmp <= 19; iTmp++) //msingh69 - MITS 37692
				{
					m_arrTotalsOnPage[iTmp]=0;
				}
				m_iTotalDaysWorkLoss = 0;
				m_iTotalDaysRestriction = 0;
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CommonOsha300.ClearFields.Error",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method will return 1 for true input value, 0 for false input value.
		/// </summary>
		/// <param name="p_bVal">input bool value</param>
		/// <returns>0 or 1 depending upon the input</returns>
		internal int Abs(bool p_bVal)
		{
			int iRetVal=0;
			if(p_bVal!=false)
			{
				iRetVal=1;
			}
			return iRetVal;
		}
		#endregion
		
	}
	/// <summary>
	/// This is the base class for all Osha300A reports.
	/// It has been framed taking into consideration the future changes to the reports (Osha300A).
	/// </summary>
	internal class CommonOsha300A : CustomActiveReport
	{		
		#region Member Variable
		/// <summary>
		/// This will contain year of report.
		/// </summary>
		protected int m_iYearOfReport;
		/// <summary>
		/// Keeps track of number of pages.
		/// </summary>
		protected int m_iPage;
        protected int m_iClientId;
		#endregion

		#region Constructor

        internal CommonOsha300A(int p_iClientId)
            : base(p_iClientId)
        {
            m_iClientId = p_iClientId;
        }
				/// <summary>
		/// This is the constructor with year of report as parameter.
		/// </summary>
		/// <param name="p_iYearOfReport">Year of report.</param>
        internal CommonOsha300A(int p_iYearOfReport, int p_iClientId)
            : base(p_iClientId)
		{
			m_iYearOfReport=p_iYearOfReport;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Methods
		/// <summary>
		/// This method will initialize the OSHA300A report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		protected void DataInitializeBase(ref object p_objsender, ref System.EventArgs p_objeArgs)
		{
			try
			{
				Fields.Add("ColumnG");
				Fields.Add("ColumnH");
				Fields.Add("ColumnI");
				Fields.Add("ColumnJ");
				Fields.Add("WorkLoss");
				Fields.Add("ColumnL");
				Fields.Add("ColumnM1");
				Fields.Add("ColumnM2");
				Fields.Add("ColumnM3");
				Fields.Add("ColumnM4");
				Fields.Add("Config");
				if(this.m_iYearOfReport>2003)
				{
					Fields.Add("HearingLoss");
					
					Fields.Add("NAICS1");
					Fields.Add("NAICS2");
					Fields.Add("NAICS3");
					Fields.Add("NAICS4");
					Fields.Add("NAICS5");
					Fields.Add("NAICS6");
					Fields.Add("SIC1");
					Fields.Add("SIC2");
					Fields.Add("SIC3");
					Fields.Add("SIC4");
				}
				else
				{
					Fields.Add("SIC2003");
				}
				Fields.Add("AllOtherIllnesses");
				
				Fields.Add("EmployeeCount");
				Fields.Add("EmployeeHours");
				Fields.Add("EstablishmentBusiness");
				Fields.Add("EstablishmentCity");
				Fields.Add("EstablishmentName");
				Fields.Add("EstablishmentState");
				Fields.Add("EstablishmentStreet");
				Fields.Add("EstablishmentZip");
				Fields.Add("ExecDate");
				Fields.Add("ExecTitle");
				Fields.Add("ExecPhone");
				Fields.Add("Year");
				Fields.Add("Pagenumber");
				Fields.Add("ExecSign");
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CommonOsha300A.DataInitializeBase.Error",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method will fetch data to populate the fields in OSHA300A report. 
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		protected void FetchDataBase(ref object p_objsender,ref FetchEventArgs p_objeArgs)
		{
			OSHA300AReport objOsha300A=null;
			string sReportConfig="";
			try
			{
				Declarations.CheckAbort(m_objNotify,m_iClientId);
				this.m_iPage=this.m_iPage+1;
				if(this.m_iPage>m_arrlstRpts.Count)
				{
					p_objeArgs.EOF=true;
					return;
				}
				else
				{
					p_objeArgs.EOF=false;
				}
				this.ClearFields();
				objOsha300A=(OSHA300AReport)m_arrlstRpts[this.m_iPage-1];
				Declarations.CheckAbort(m_objNotify,m_iClientId);
				if(objOsha300A!=null)
				{
					if(objOsha300A.Signature!=null)
					{
						Fields["ExecSign"].Value=objOsha300A.Signature;
					}
					Fields["ColumnG"].Value = objOsha300A.TotalsOnPage(7);
					Fields["ColumnH"].Value = objOsha300A.TotalsOnPage(8);
					Fields["ColumnI"].Value = objOsha300A.TotalsOnPage(9);
					Fields["ColumnJ"].Value = objOsha300A.TotalsOnPage(10);
					Fields["WorkLoss"].Value = objOsha300A.TotalsOnPage(12);
					Fields["ColumnL"].Value = objOsha300A.TotalsOnPage(11);
					Fields["ColumnM1"].Value = objOsha300A.TotalsOnPage(13);
					Fields["ColumnM2"].Value = objOsha300A.TotalsOnPage(15);
					Fields["ColumnM3"].Value = objOsha300A.TotalsOnPage(16);
					Fields["ColumnM4"].Value = objOsha300A.TotalsOnPage(17);
					Fields["AllOtherIllnesses"].Value = objOsha300A.TotalsOnPage(19);
					if(objOsha300A.ExposureAvgEmployeeCount >0 || objOsha300A.ExposureTotalHoursWorked>0)
					{
						Fields["EmployeeCount"].Value = objOsha300A.ExposureAvgEmployeeCount;
						Fields["EmployeeHours"].Value = objOsha300A.ExposureTotalHoursWorked;
					}
					else
					{
						Fields["EmployeeCount"].Value = "";
						Fields["EmployeeHours"].Value = "";
					}
				    Fields["ExecDate"].Value = Declarations.ConvertDateTimeToMmDdYyyy(objOsha300A.Preparer.SignatureDate,m_iClientId);
					Fields["ExecTitle"].Value = objOsha300A.Preparer.Title;
					Fields["ExecPhone"].Value = objOsha300A.Preparer.Phone;
					if(this.m_iYearOfReport>2003)
					{
						sReportConfig = sReportConfig + "As of Date:    " + Declarations.ConvertDateTimeToMmDdYyyy(objOsha300A.AsOfDate,m_iClientId);
						sReportConfig = sReportConfig + "\n" + "Event Range:    " + Declarations.ConvertDateTimeToMmDdYyyy(objOsha300A.BeginDate,m_iClientId) + " To " + Declarations.ConvertDateTimeToMmDdYyyy(objOsha300A.EndDate,m_iClientId);
						sReportConfig = sReportConfig + "\n" + "180 Day Rule:  " + objOsha300A.Enforce180DayRule;
						sReportConfig = sReportConfig + "\n" + "Event Based:   " + objOsha300A.EventBasedFlag;
						sReportConfig = sReportConfig + "\n" + "Run Date:      " + Declarations.ConvertDateTimeToMmDdYyyy(DateTime.Today,m_iClientId);
						Fields["Config"].Value = sReportConfig;
						Fields["HearingLoss"].Value = objOsha300A.TotalsOnPage(18);
						if (objOsha300A.Company.NaicsText!="")
						{
							if(objOsha300A.Company.NaicsText.Length>=1)
								Fields["NAICS1"].Value =objOsha300A.Company.NaicsText.Substring(0,1);

							if(objOsha300A.Company.NaicsText.Length>=2)
								Fields["NAICS2"].Value = objOsha300A.Company.NaicsText.Substring(1,1);

							if(objOsha300A.Company.NaicsText.Length>=3)
								Fields["NAICS3"].Value = objOsha300A.Company.NaicsText.Substring(2,1);
					
							if(objOsha300A.Company.NaicsText.Length>=4)
								Fields["NAICS4"].Value = objOsha300A.Company.NaicsText.Substring(3,1);
						
							if(objOsha300A.Company.NaicsText.Length>=5)
								Fields["NAICS5"].Value = objOsha300A.Company.NaicsText.Substring(4,1);
						
							if(objOsha300A.Company.NaicsText.Length>=6)
								Fields["NAICS6"].Value = objOsha300A.Company.NaicsText.Substring(5,1);
						
						}
					}
					else
					{
						if(objOsha300A.BeginDate !=DateTime.MinValue && objOsha300A.EndDate!=DateTime.MinValue)
						{
							if(Declarations.DateDiffInAbsoluteDays(objOsha300A.BeginDate,objOsha300A.EndDate,m_iClientId)!=364)
							{
								sReportConfig = sReportConfig + "\n" + "Events From:  " + Declarations.ConvertDateTimeToMmDdYyyy(objOsha300A.BeginDate,m_iClientId);
								sReportConfig = sReportConfig + "\n" + "Events To:    " + Declarations.ConvertDateTimeToMmDdYyyy(objOsha300A.EndDate,m_iClientId);
							}

						}
						
						if(objOsha300A.Enforce180DayRule)
						{
							sReportConfig = sReportConfig + "\n" + "Using 180 Day Rule";
						}
						if(objOsha300A.AsOfDate!=DateTime.MinValue)
						{
							sReportConfig = sReportConfig + "\n"+ "AS OF: " + Declarations.ConvertDateTimeToMmDdYyyy(objOsha300A.AsOfDate,m_iClientId);
						}
						if(objOsha300A.EventBasedFlag)
						{
							sReportConfig = sReportConfig + "\n" + "Event Based";
						}
						else
						{
							sReportConfig = sReportConfig + "\n" + "Person Involved Based";
						}
						sReportConfig = sReportConfig + "\n" + "Run Date: " + Declarations.ConvertDateTimeToMmDdYyyy(DateTime.Today,m_iClientId);
						Fields["Config"].Value = sReportConfig;

					}
					if(objOsha300A.YearOfReport!="")
					{
						if(objOsha300A.YearOfReport.Length >= 4)
						{
							Fields["Year"].Value = objOsha300A.YearOfReport.Substring(2,2);
						}
					}
					else
					{
						Fields["Year"].Value="";
					}

					if(objOsha300A.Company!=null)
					{
						if(this.m_iYearOfReport>2003)
						{
							Fields["EstablishmentBusiness"].Value = objOsha300A.Company.NatureOfBusiness;
							Fields["EstablishmentCity"].Value = objOsha300A.Company.City;
							Fields["EstablishmentName"].Value = objOsha300A.Company.Name;
							Fields["EstablishmentState"].Value = objOsha300A.Company.State;
							Fields["EstablishmentStreet"].Value = objOsha300A.Company.Address;
							Fields["EstablishmentZip"].Value = objOsha300A.Company.ZipCode;
						}
						else
						{
							Fields["EstablishmentName"].Value = objOsha300A.Company.Name;
                            Fields["EstablishmentBusiness"].Value = objOsha300A.Company.NatureOfBusiness;
							if(objOsha300A.Company.Address!="")
							{
									Fields["EstablishmentStreet"].Value = objOsha300A.Company.Address;
							}
							else
							{
									Fields["EstablishmentStreet"].Value = "(No Data)";
							}
							if(objOsha300A.Company.City!="")
							{
                               Fields["EstablishmentCity"].Value = objOsha300A.Company.City;
							}
							else
							{
								Fields["EstablishmentCity"].Value = "(No Data)";
							}
							if(objOsha300A.Company.State!="")
							{
									Fields["EstablishmentState"].Value = objOsha300A.Company.State;
							}
							else
							{
									Fields["EstablishmentState"].Value = "(No Data)";
							}
							if(objOsha300A.Company.ZipCode!="")
							{ 
								Fields["EstablishmentZip"].Value = objOsha300A.Company.ZipCode;
							}
							else
							{
								Fields["EstablishmentZip"].Value = "(No Data)";
							}

						}
						if (objOsha300A.Company.SicCode!="")
						{
							if(this.m_iYearOfReport>2003)
							{
								if(objOsha300A.Company.SicCode.Length>=1)
									Fields["SIC1"].Value = objOsha300A.Company.SicCode.Substring(0,1);
								if(objOsha300A.Company.SicCode.Length>=2)
									Fields["SIC2"].Value = objOsha300A.Company.SicCode.Substring(1,1);
						
								if(objOsha300A.Company.SicCode.Length>=3)
									Fields["SIC3"].Value = objOsha300A.Company.SicCode.Substring(2,1);
						
								if(objOsha300A.Company.SicCode.Length>=4)
									Fields["SIC4"].Value = objOsha300A.Company.SicCode.Substring(3,1);
							}
							else
							{
								if(objOsha300A.Company.SicCode!="")
								{
									Fields["SIC2003"].Value = objOsha300A.Company.SicCode;
								}
								else
								{
									Fields["SIC2003"].Value = "";
								}
							}
						}
					}
				}
				Fields["Pagenumber"].Value = " ";
				p_objeArgs.EOF=false;
			}			
			catch(RMAppException p_objException)
			{
				p_objeArgs.EOF=true;
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				p_objeArgs.EOF=true;
				throw new RMAppException(Globalization.GetString("CommonOsha300A.FetchDataBase.Error",m_iClientId),p_objException);
			}
			finally
			{
				objOsha300A = null;
			}
		}
		/// <summary>
		/// This method will do the setting of page for OSHA300A report. 
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		protected void ReportStartBase(ref object p_objsender, ref System.EventArgs p_objeArgs)
		{
			try
			{
                //MITS 10630 the user for cws may not have any defalut printer
                this.Document.Printer.PrinterName = string.Empty;
				this.PageSettings.PaperHeight=14;
				this.PageSettings.PaperWidth=(float)8.5;
				this.PageSettings.Orientation=DataDynamics.ActiveReports.Document.PageOrientation.Landscape;
				base.SoftErrorLogPrint();
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CommonOsha300A.ReportStartBase.Error",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method will clear all the data fields of the OSHA300A report.
		/// </summary>
		private void ClearFields()
		{
			try
			{
				Fields["ColumnG"].Value="";
				Fields["ColumnH"].Value="";
				Fields["ColumnI"].Value="";
				Fields["ColumnJ"].Value="";
				Fields["WorkLoss"].Value="";
				Fields["ColumnL"].Value="";
				Fields["ColumnM1"].Value="";
				Fields["ColumnM2"].Value="";
				Fields["ColumnM3"].Value="";
				Fields["ColumnM4"].Value="";
				Fields["Config"].Value="";
				if(this.m_iYearOfReport>2003)
				{
					Fields["HearingLoss"].Value="";
					
					Fields["NAICS1"].Value="";
					Fields["NAICS2"].Value="";
					Fields["NAICS3"].Value="";
					Fields["NAICS4"].Value="";
					Fields["NAICS5"].Value="";
					Fields["NAICS6"].Value="";
					Fields["SIC1"].Value="";
					Fields["SIC2"].Value="";
					Fields["SIC3"].Value="";
					Fields["SIC4"].Value="";
				}
				else
				{
					Fields["SIC2003"].Value="";
				}
				Fields["AllOtherIllnesses"].Value="";
				if(this.m_iYearOfReport>2003)
				{
					
				}
				Fields["EmployeeCount"].Value="";
				Fields["EmployeeHours"].Value="";
				Fields["EstablishmentBusiness"].Value="";
				Fields["EstablishmentCity"].Value="";
				Fields["EstablishmentName"].Value="";
				Fields["EstablishmentState"].Value="";
				Fields["EstablishmentStreet"].Value="";
				Fields["EstablishmentZip"].Value="";
				Fields["ExecDate"].Value="";
				Fields["ExecTitle"].Value="";
				Fields["ExecPhone"].Value="";
				Fields["Year"].Value="";
				Fields["Pagenumber"].Value="";
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CommonOsha300A.ClearFields.Error",m_iClientId),p_objException);
			}		
			
		}
		
		#endregion
	}
}
