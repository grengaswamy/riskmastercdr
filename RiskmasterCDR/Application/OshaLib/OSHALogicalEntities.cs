using System;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Collections;
using Riskmaster.Application.ReportInterfaces;

namespace Riskmaster.Application.OSHALib
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004	
	/// </summary>
	
	/// <summary>
	/// This is the basic entity on which reports are to be generated.	
	/// </summary>
	internal class OSHAItem
	{
		#region Member Variables
		/// <summary>
		/// This field will hold the Event Id.
		/// </summary>
		private int m_iEventId=0;
		/// <summary>
		/// This field will hold the Case Number.
		/// </summary>
		private string m_sCaseNumber="";
		/// <summary>
		/// This field will hold the Occupation.
		/// </summary>
		private string m_sOccupation="";
		/// <summary>
		/// This field will hold the Date Of Event.
		/// </summary>
		private DateTime m_datDateOfEvent;
		/// <summary>
		/// This field will hold the Time Of Event.
		/// </summary>
		private string m_sTimeOfEvent;
		/// <summary>
		/// This field will hold the Event Location.
		/// </summary>
		private string m_sEventLocation="";
		/// <summary>
		/// This field will hold the Injury Description.
		/// </summary>
		private string m_sInjuryDesc="";
		/// <summary>
		/// This field will hold Record As Injury status.
		/// </summary>
		private bool m_bRecordAsInjury;		
		/// <summary>
		/// This field will hold the Record As Skin Disorder status.
		/// </summary>
		private bool m_bRecordAsSkinDisor;
		/// <summary>
		/// This field will hold the Record As Respiratory Conditions status.
		/// </summary>
		private bool m_bRecordAsRespCond;
		/// <summary>
		/// This field will hold the Record As Hear Loss status.
		/// </summary>
		private bool m_bRecordAsHearLoss;
		/// <summary>
		///  This field will hold the Record As Other Injury status.
		/// </summary>
		private bool m_bRecordAsOther;
		/// <summary>
		/// This field will hold the Death status.
		/// </summary>
		private string m_sChkBoxDeath="";
		/// <summary>
		/// This field will hold the Restriction status.
		/// </summary>
		private string m_sChkBoxRestriction="NO";
		/// <summary>
		/// This field will hold the Work Loss status.
		/// </summary>
		private string m_sChkBoxWorkLoss="NO";
		/// <summary>
		/// This field will hold the Other Record status.
		/// </summary>
		private string m_sChkBoxOtherRecord="";
		/// <summary>
		/// This field will hold the number of days of work loss.
		/// </summary>
		private int m_iDaysWorkLoss;
		/// <summary>
		/// This field will hold the number of days of restriction.
		/// </summary>
		private int m_iDaysRestriction;
		/// <summary>
		/// This field will hold the Record As Poison status.
		/// </summary>
		private bool m_bRecordAsPoison;
		/// <summary>
		/// This field will hold the Privacy Case status.
		/// </summary>
		private int m_iPrivacyCase;
		/// <summary>
		/// This field will hold the Date Of Death.
		/// </summary>
		private DateTime m_datDateOfDeath;
		/// <summary>
		/// This field will hold the Date Of Hire.
		/// </summary>
		private DateTime m_datDateOfHire;
		/// <summary>
		/// This field will hold the Employee Start Time.
		/// </summary>
		private string m_sEmployeeStartTime="";
		/// <summary>
		/// This field will hold the Employee Address.
		/// </summary>
		private string m_sEmployeeAddress="";
		/// <summary>
		/// This field will hold the Employee City.
		/// </summary>
		private string m_sEmployeeCity="";
		/// <summary>
		/// This field will hold the Employee State.
		/// </summary>
		private string m_sEmployeeState="";
		/// <summary>
		/// This field will hold the Employee Zip Code.
		/// </summary>
		private string m_sEmployeeZipCode="";
		/// <summary>
		/// This field will hold the Employee Date Of Birth.
		/// </summary>
		private DateTime m_datEmployeeDateOfBirth;
		/// <summary>
		/// This field will hold the Employee Sex.
		/// </summary>
		private string m_sEmployeeSex;//do not initialize..
		/// <summary>
		/// This field will hold the Employee Full Name.
		/// </summary>
		private string m_sEmployeeFullName="";
		/// <summary>
		/// This field will hold the Sort On data.
		/// </summary>
		private string m_sSortOn="";
		/// <summary>
		/// This field will hold the Employee Occupation.
		/// </summary>
		private string m_sEmployeeOccupation="";
		/// <summary>
		/// This field will hold the Activity When Injured.
		/// </summary>
		private string m_sActivityWhenInjured="";
		/// <summary>
		/// This field will hold the Disability Code.
		/// </summary>
		private string m_sDisabilityCode="";
		/// <summary>
		/// This field will hold the PI Row Id.
		/// </summary>
		private string m_sPIRowId="";
		/// <summary>
		/// This field will hold the Illness Description.
		/// </summary>
		private string m_sIllnessDesc="";
		/// <summary>
		/// This field will hold the Object Substance That Injured.
		/// </summary>
		private string m_sObjectSubstanceThatInjured="";
		/// <summary>
		/// This field will hold the Sharps Object Id.
		/// </summary>
		private int m_iSharpsObjectId;
		/// <summary>
		/// This field will hold the Sharps Make Id.
		/// </summary>
		private int m_iSharpsMakeId;
		/// <summary>
		/// This field will hold the How Event Occurred.
		/// </summary>
		private string m_sHowEventOccurred="";
		/// <summary>
		/// This field will hold the HealthCare Entity Facility.
		/// </summary>
		private string m_sHealthCareEntityFacility="";
		/// <summary>
		/// This field will hold the HealthCare Entity Address.
		/// </summary>
		private string m_sHealthCareEntityAddress="";
		/// <summary>
		/// This field will hold the HealthCare Entity City.
		/// </summary>
		private string m_sHealthCareEntityCity="";
		/// <summary>
		/// This field will hold the HealthCare Entity State.
		/// </summary>
		private string m_sHealthCareEntityState="";
		/// <summary>
		/// This field will hold the HealthCare Entity Zip Code.
		/// </summary>
		private string m_sHealthCareEntityZipCode="";
		/// <summary>
		/// This field will hold the Days Of Week data.
		/// </summary>
		private bool[] m_arrDaysOfWeek;
		/// <summary>
		/// This field will hold the HealthCare Prof data.
		/// </summary>
		private string m_sHealthCareProf="";
		/// <summary>
		/// This field will hold the Emergency Room Treatment status.
		/// </summary>
		private bool m_bEmergencyRoomTreatment;//keep it as it is ,do not initialize..
		/// <summary>
		///This field will hold the Hospitalized Overnight status.
		/// </summary>
		private bool m_bHospitalizedOverNight;////keep it as it is ,do not initialize..
        //MITS 14844:Raman Bhatia
        private bool m_bIsEmployeeOshaRecordable = true;
		#endregion

		#region Properties
		/// <summary>
		/// This property provides access to the case number.
		/// </summary>
		internal string CaseNumber
		{
			get
			{
				return m_sCaseNumber;
			}
			set
			{
               	if (value == null)
					m_sCaseNumber = "";			
				else
					m_sCaseNumber = value;			

			}
		}
		/// <summary>
		/// This property provides access to Employee full name.
		/// </summary>
		internal string EmployeeFullName
		{
			get
			{
				return m_sEmployeeFullName;
			}
			set
			{
                if (value != null)
					m_sEmployeeFullName = value;			
				else
					m_sEmployeeFullName = "";			
			}
		}
		/// <summary>
		/// This property provides access to Occupation.
		/// </summary>
		internal string Occupation
		{
			get
			{
				return  m_sOccupation;
			}
			set
			{
				if (value != null)
					m_sOccupation = value;
				else
					m_sOccupation = "";
			}
		}
		/// <summary>
		/// This property provides access to Date of Event.
		/// </summary>
		internal DateTime DateOfEvent
		{
			get
			{
				return m_datDateOfEvent;
			}
			set
			{
				m_datDateOfEvent = value;
			}
		}
		/// <summary>
		/// This property provides access to Time of Event.
		/// </summary>
		internal string TimeOfEvent
		{
			get
			{
				return m_sTimeOfEvent;
			}
			set
			{
				if (value != null)
					m_sTimeOfEvent = value;
				else
					m_sTimeOfEvent = "";
			}
		}
		/// <summary>
		/// This property provides access to Event Location.
		/// </summary>
		internal string EventLocation
		{
			get
			{
				return m_sEventLocation;
			}
			set
			{
				if (value != null)
					m_sEventLocation = value;
				else
					m_sEventLocation = "";
			}
		}
		/// <summary>
		/// This property provides access to Injury Description.
		/// </summary>
		internal string InjuryDesc
		{
			get
			{
				return m_sInjuryDesc;
			}
			set
			{
				if (value != null)
					m_sInjuryDesc = value;
				else
					m_sInjuryDesc = "";

			}
		}
		/// <summary>
		/// This property provides access to value of Record As Injury flag.
		/// </summary>
		internal bool RecordAsInjury
		{
			get
			{
				return m_bRecordAsInjury;
			}
			set
			{
				m_bRecordAsInjury = value;
			}
		}		
		/// <summary>
		/// This property provides access to value of Record As Skin Disorder flag.
		/// </summary>
		internal bool RecordAsSkinDisor
		{
			get
			{
				return m_bRecordAsSkinDisor;
			}
			set
			{
				m_bRecordAsSkinDisor = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Record As Respiratory Conditions flag.
		/// </summary>
		internal bool RecordAsRespCond
		{
			get
			{
				return m_bRecordAsRespCond;
			}
			set
			{
				m_bRecordAsRespCond = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Record As Hearing Loss flag.
		/// </summary>
		internal bool RecordAsHearLoss
		{
			get
			{
				return m_bRecordAsHearLoss;
			}
			set
			{
				m_bRecordAsHearLoss = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Record As Other Injury flag.
		/// </summary>
		internal bool RecordAsOther
		{
			get
			{
				return m_bRecordAsOther;
			}
			set
			{
				m_bRecordAsOther = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Death flag.
		/// </summary>
		internal string ChkBoxDeath
		{
			get
			{
				return m_sChkBoxDeath;	
			}
			set
			{
				if (value != null)
					m_sChkBoxDeath = value;
				else
					m_sChkBoxDeath = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Restriction flag.
		/// </summary>
		internal string ChkBoxRestriction
		{
			get
			{
				return m_sChkBoxRestriction;
			}
			set
			{
				if (value != null)
					m_sChkBoxRestriction = value;
				else
					m_sChkBoxRestriction = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Work Loss flag.
		/// </summary>
		internal string ChkBoxWorkLoss
		{
			get
			{
				return m_sChkBoxWorkLoss;
			}
			set
			{
				if (value != null)
					m_sChkBoxWorkLoss = value;
				else
					m_sChkBoxWorkLoss = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Other Record flag.
		/// </summary>
		internal string ChkBoxOtherRecord
		{
			get
			{
				return m_sChkBoxOtherRecord;
			}
			set
			{
				if (value != null)
					m_sChkBoxOtherRecord = value;
				else
					m_sChkBoxOtherRecord = "";
			}
		}
		/// <summary>
		/// This property provides access to value of number of days of work loss.
		/// </summary>
		internal int DaysWorkLoss
		{
			get
			{
				return m_iDaysWorkLoss;
			}
			set
			{
				m_iDaysWorkLoss = value;
			}
		}
		/// <summary>
		/// This property provides access to value of number of days of restriction.
		/// </summary>
		internal int DaysRestriction
		{
			get
			{
				return m_iDaysRestriction;
			}
			set
			{
				m_iDaysRestriction = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Record As Poison flag.
		/// </summary>
		internal bool RecordAsPoison
		{
			get
			{
				return m_bRecordAsPoison;
			}
			set
			{
				m_bRecordAsPoison = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Privacy Case status.
		/// </summary>
		internal int PrivacyCase
		{
			get
			{
				return m_iPrivacyCase;
			}
			set
			{
				m_iPrivacyCase = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Date of Death.
		/// </summary>
		internal DateTime DateOfDeath
		{
			get
			{
				return m_datDateOfDeath;
			}
			set
			{
				m_datDateOfDeath = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Date of Hire.
		/// </summary>
		internal DateTime DateOfHire
		{
			get
			{
				return m_datDateOfHire;
			}
			set
			{
				m_datDateOfHire = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Employee Start Time.
		/// </summary>
		internal string EmployeeStartTime
		{
			get
			{
				return m_sEmployeeStartTime;
			}
			set
			{
				if (value != null)
					m_sEmployeeStartTime = value;
				else
					m_sEmployeeStartTime = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Employee Address.
		/// </summary>
		internal string EmployeeAddress
		{
			get
			{
				return m_sEmployeeAddress;
			}
			set
			{
				if (value != null)
					m_sEmployeeAddress = value;
				else
					m_sEmployeeAddress = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Employee City.
		/// </summary>
		internal string EmployeeCity
		{
			get
			{
				return m_sEmployeeCity;
			}
			set
			{
				if (value != null)
					m_sEmployeeCity = value;
				else
					m_sEmployeeCity = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Employee State.
		/// </summary>
		internal string EmployeeState
		{
			get
			{
				return m_sEmployeeState;
			}
			set
			{
				if (value != null)
					m_sEmployeeState = value;
				else
					m_sEmployeeState = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Employee Zip Code.
		/// </summary>
		internal string EmployeeZipCode
		{
			get
			{
				return m_sEmployeeZipCode;
			}
			set
			{
				if (value != null)
					m_sEmployeeZipCode = value;
				else
					m_sEmployeeZipCode = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Employee Date of Birth.
		/// </summary>
		internal DateTime EmployeeDateOfBirth
		{
			get
			{
				return m_datEmployeeDateOfBirth;
			}
			set
			{
				m_datEmployeeDateOfBirth = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Sex of Employee.
		/// </summary>
		internal string EmployeeSex
		{
			get
			{
				return m_sEmployeeSex;
			}
			set
			{
				if (value != null)
					m_sEmployeeSex = value;
				else
					m_sEmployeeSex = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Employee Occupation.
		/// </summary>
		internal string EmployeeOccupation
		{
			get
			{
				return m_sEmployeeOccupation;
			}
			set
			{
				if (value != null)
					m_sEmployeeOccupation = value;
				else
					m_sEmployeeOccupation ="";
			}
		}
		/// <summary>
		/// This property provides access to value of Activity When Injured.
		/// </summary>
		internal string ActivityWhenInjured
		{
			get
			{
				return m_sActivityWhenInjured;
			}
			set
			{
				if (value != null)
					m_sActivityWhenInjured = value;
				else
					m_sActivityWhenInjured = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Disability Code.
		/// </summary>
		internal string DisabilityCode
		{
			get
			{
				return m_sDisabilityCode;
			}
			set
			{
				if (value != null)
					m_sDisabilityCode = value;
				else
					m_sDisabilityCode = "";
			}
		}
		/// <summary>
		/// This property provides access to value of PI Row Id.
		/// </summary>
		internal string PiRowId
		{
			get
			{
				return m_sPIRowId;
			}
			set
			{
				if (value != null)
					m_sPIRowId = value;
				else
					m_sPIRowId = "";
			}
		}
		/// <summary>
		/// This property provides access to value of illness description.
		/// </summary>
		internal string IllnessDesc
		{
			get
			{
				return m_sIllnessDesc;
			}
			set
			{
				if (value != null)
					m_sIllnessDesc = value;
				else
					m_sIllnessDesc = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Object Substance That Injured.
		/// </summary>
		internal string ObjectSubstanceThatInjured
		{
			get
			{
				return m_sObjectSubstanceThatInjured;
			}
			set
			{
				if (value != null)
					m_sObjectSubstanceThatInjured = value;
				else
					m_sObjectSubstanceThatInjured = "";
			}
		}
		/// <summary>
		/// This property provides access to value of How Event Occurred.
		/// </summary>
		internal string HowEventOccurred
		{
			get
			{
				return m_sHowEventOccurred;
			}
			set
			{
				if (value != null)
					m_sHowEventOccurred = value;
				else
					m_sHowEventOccurred = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Sharps Object Id.
		/// </summary>
		internal int SharpsObjectId
		{
			get
			{
				return m_iSharpsObjectId;
			}
			set
			{
				m_iSharpsObjectId = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Sharps Make Id.
		/// </summary>
		internal int SharpsMakeId
		{
			get
			{
				return m_iSharpsMakeId;
			}
			set
			{
				m_iSharpsMakeId = value;
			}
		}
		/// <summary>
		/// This property provides access to value of HealthCare Entity Facility.
		/// </summary>
		internal string HealthCareEntityFacility
		{
			get
			{
				return m_sHealthCareEntityFacility;
			}
			set
			{
				if (value != null)
					m_sHealthCareEntityFacility = value;
				else
					m_sHealthCareEntityFacility = "";
			}
		}
		/// <summary>
		/// This property provides access to value of HealthCare Entity Address.
		/// </summary>
		internal string HealthCareEntityAddress
		{
			get
			{
				return m_sHealthCareEntityAddress;
			}
			set
			{
				if (value != null)
					m_sHealthCareEntityAddress = value;
				else
					m_sHealthCareEntityAddress = "";
			}
		}
		/// <summary>
		/// This property provides access to value of HealthCare Entity City.
		/// </summary>
		internal string HealthCareEntityCity
		{
			get
			{
				return m_sHealthCareEntityCity;
			}
			set
			{
				if (value != null)
					m_sHealthCareEntityCity = value;
				else
					m_sHealthCareEntityCity = "";
			}
		}
		/// <summary>
		/// This property provides access to value of HealthCare Entity State.
		/// </summary>
		internal string HealthCareEntityState
		{
			get
			{
				return m_sHealthCareEntityState;
			}
			set
			{
				if (value != null)
					m_sHealthCareEntityState = value;
				else
					m_sHealthCareEntityState = "";
			}
		}
		/// <summary>
		/// This property provides access to value of HealthCare Entity Zip Code.
		/// </summary>
		internal string HealthCareEntityZipCode
		{
			get
			{
				return m_sHealthCareEntityZipCode;
			}
			set
			{
				if (value != null)
					m_sHealthCareEntityZipCode = value;
				else
					m_sHealthCareEntityZipCode = "";
			}
		}
		/// <summary>
		/// This property provides access to value of array of Days of Week.
		/// </summary>
		internal bool[] DaysOfWeek
		{
			get
			{
				return m_arrDaysOfWeek;
			}
			set
			{
				m_arrDaysOfWeek = value;
			}
		}
		/// <summary>
		/// This property provides access to value of HealthCare Prof.
		/// </summary>
		internal string HealthCareProf
		{
			get
			{
				return m_sHealthCareProf;
			}
			set
			{
				if (value != null)
					m_sHealthCareProf = value;
				else
					m_sHealthCareProf = "";
			}
		}
		/// <summary>
		/// This property provides access to value of Emergency Room Treatment.
		/// </summary>
		internal bool EmergencyRoomTreatment
		{
			get
			{
				return m_bEmergencyRoomTreatment;
			}
			set
			{
				m_bEmergencyRoomTreatment = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Hospitalized Overnight.
		/// </summary>
		internal bool HospitalizedOverNight
		{
			get
			{
				return m_bHospitalizedOverNight;
			}
			set
			{
				m_bHospitalizedOverNight = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Event Id.
		/// </summary>
		internal int EventId
		{
			get
			{
				return m_iEventId;
			}
			set
			{
				m_iEventId = value;
			}
		}
		/// <summary>
		/// This property provides access to value of Sort On data.
		/// </summary>
		public string SortOn
		{
			get
			{
				return m_sSortOn;
			}
			set
			{
				if (value != null)
					m_sSortOn = value;
				else
					m_sSortOn = "";
			}
		}
        public bool EmployeeOshaRecordable
        {
            get
            {
                return m_bIsEmployeeOshaRecordable;
            }
            set
            {
                m_bIsEmployeeOshaRecordable = value;
            }
        }



		#endregion

	}
	/// <summary>
	/// This class will hold criteria information for reports.
	/// </summary>
	internal class CriteriaEntity
	{
		#region Member Variables
		/// <summary>
		/// This field will hold the value of Year Of Report.
		/// </summary>
		protected string m_sYearOfReport;
		/// <summary>
		/// This field will hold the value of Beginning date of Report.
		/// </summary>
		protected DateTime m_datBeginDate;
		/// <summary>
		/// This field will hold the value of ending date of Report.
		/// </summary>
		protected DateTime m_datEndDate;
		/// <summary>
		/// This field will hold the value of Report Level.
		/// </summary>
		protected int m_iReportLevel;
		/// <summary>
		/// This field will hold the value of Report Level description.
		/// </summary>
		protected string m_sReportLevel;
		/// <summary>
		/// This field will hold the value of Use Reports at Level.
		/// </summary>
		protected int m_iUseReportLevel ;
		/// <summary>
		/// This field will hold the value of Use Reports at Level description.
		/// </summary>
		protected string m_sUseReportLevel ;
		/// <summary>
		///  This field will hold the value of Report On entity.
		/// </summary>
		protected int m_iReportOn ;
		/// <summary>
		///  This field will hold the value of User Filename.
		/// </summary>
		protected string m_sUserFileName;
		/// <summary>
		///  This field will hold the value of All Entities flag.
		/// </summary>
		protected bool m_bAllEntitiesFlag ;
		/// <summary>
		/// This field will hold the value of Event Based flag.
		/// </summary>
		protected bool m_bEventBasedFlag ;
		/// <summary>
		/// This field will hold the value of Print OSHA Description flag.
		/// </summary>
		protected bool m_bPrintOSHADescFlag ;
		/// <summary>
		/// This field will hold the array list of selected entities.
		/// </summary>
		protected ArrayList m_arrlstEntities ;
		/// <summary>
		/// This field will hold the information about the company.
		/// </summary>
		protected EntityItem m_objCompany ;
		/// <summary>
		/// This field will hold the value of xml for report criteria.
		/// </summary>
		protected XmlDocument m_objCriteriaXML; 
		/// <summary>
		/// This field will hold the value of start date time.
		/// </summary>
		protected DateTime m_datStartTime ;
		/// <summary>
		/// This field will hold the value of Primary Location Flag.
		/// </summary>
		protected bool m_bPrimaryLocationFlag;
		/// <summary>
		/// This field will hold the value of Enforce 180 day rule Flag.
		/// </summary>
		protected bool m_bEnforce180DayRule ;
		/// <summary>
		/// This field will hold the value of As Of Date.
		/// </summary>
		protected DateTime m_datAsOfDate ;
		/// <summary>
		/// This field will hold the value of Sort Order.
		/// </summary>
		protected int m_iItemSortOrder ;
		/// <summary>
		/// This field will hold the value of Prefix for Establishment Name.
		/// </summary>
		protected string m_sEstablishmentNamePrefix ;
        //MITS 37193
        /// <summary>
        /// This field will hold the value of Column E Data source.
        /// </summary>
        protected int m_iColumnESource;
        /// <summary>
		/// This field will hold the value of Column F Data source.
		/// </summary>
		protected int m_iColumnFSource ;
        //MITS 37469 Begin                        
        /// <summary>
        /// This field will hod the value of Use Employee Names flag.
        /// </summary>
        protected bool m_bUseEmployeeNames;
        //MITS 37469 End
        /// <summary>                                 
		/// This field will hold the value of Print soft error log flag.
		/// </summary>
		protected bool m_bPrintSoftErrLog;
		/// <summary>
		/// This field will hold the value of By OSHA establishment flag.
		/// </summary>
		protected bool m_bByOSHAEstablishmentFlag ;
		/// <summary>
		/// This field will hold an instance of InstanceUtilities.
		/// </summary>
		protected InstanceUtilities m_objInstance ;
		/// <summary>
		/// This field will hold an instance of IWorkerNotifications.
		/// </summary>
		protected IWorkerNotifications m_objNotify ;
		/// <summary>
		/// This field will hold value of Person Involved Type.
		/// </summary>
		protected int m_iPITypeEmployee ;
		/// <summary>
		/// Array list of OSHA Report Item
		/// </summary>
		protected ArrayList m_arrlstOshaItems ;
		/// <summary>
		/// This field will hold an error object.
		/// </summary>
		protected ColError m_objColErrors;
		/// <summary>
		/// This field will hold a Preparer class object.
		/// </summary>
		protected Preparer m_objPreparer ;
		/// <summary>
		/// This field will hold database connection string.
		/// </summary>
		protected string m_sDsn="";

        protected int m_iClientId;
		#endregion

		#region Properties
		/// <summary>
		/// This property would access the value of database connection string.
		/// </summary>
		public virtual string Dsn
		{
			get
			{
				return m_sDsn;
			}
			set
			{
				if (value != null)
					m_sDsn=value;
				else
					m_sDsn="";
				m_objInstance.Dsn=m_sDsn;
			}
	
		}
		/// <summary>
		/// This property would access the value of All Entities flag.
		/// </summary>
		public virtual bool AllEntitiesFlag
		{
			get
			{
				return m_bAllEntitiesFlag;
			}
			set
			{
				m_bAllEntitiesFlag = value;
			}
		}
		/// <summary>
		/// This property would access the value of OSHA Establishment flag.
		/// </summary>
		public virtual bool ByOSHAEstablishmentFlag
		{
			get
			{
				return m_bByOSHAEstablishmentFlag;
			}
			set
			{
				if(value)
				{
					this.EstablishmentNamePrefix = "";
					this.ReportLevel = 1012;
					this.UseReportLevel = 1012;
				}
				m_bByOSHAEstablishmentFlag = value;
			}
		}

		/// <summary>
		/// This property would access the value of report Beginning date.
		/// </summary>
		public virtual DateTime BeginDate
		{
			get
			{
				return m_datBeginDate;
			}
			set
			{
				m_datBeginDate = value;
				m_sYearOfReport=Declarations.GetStrValFromInt(value.Year,m_iClientId);
			}
		}
	
		/// <summary>
		/// This property would access the value of Year of report.
		/// </summary>
		public virtual string YearOfReport
		{
			get
			{
				return m_sYearOfReport;
			}
			set
			{
				if((value=="") || (value == null))
				{
					return;
				}
				if(Declarations.GetIntValFromString(value,m_iClientId)==0)
				{
					return;
				}
				m_datBeginDate=Conversion.ToDate(value.Trim() + "0101");
				m_datEndDate=Conversion.ToDate(value.Trim()+"1231");
				m_sYearOfReport=value;
			}
		}
		/// <summary>
		/// This property would access the value of report Ending date.
		/// </summary>
		public virtual DateTime EndDate
		{
			get
			{
				return m_datEndDate;
			}
			set
			{
				try
				{
					if(m_datBeginDate.Year==value.Year)
					{
						m_datEndDate=value;
					}
					else 
					{
						throw new RMAppException(Globalization.GetString("CriteriaEntity.EndDate.InvalidDates",m_iClientId));
					}
				}
				catch(RMAppException p_objException)
				{
					throw p_objException;
				}
				catch(Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("CriteriaEntity.EndDate.ErrorSet",m_iClientId),p_objException);
				}	
				 
			}
		}
		/// <summary>
		/// This property would access the value of Event Based Flag.
		/// </summary>
		public virtual bool EventBasedFlag
		{
			get
			{
				return m_bEventBasedFlag;
			}
			set
			{
				m_bEventBasedFlag = value;
			}
		}
		/// <summary>
		/// This property would access the instance of IWorkerNotifications.
		/// </summary>
		public virtual IWorkerNotifications Notify
		{
			get
			{
				return m_objNotify;
			}
			set
			{
				m_objNotify=value;
			}
		}
		/// <summary>
		/// This property would access the value of Person Involved Type.
		/// </summary>
		public virtual int PITypeEmployee
		{
			get
			{
				return m_iPITypeEmployee;
			}
		}
		/// <summary>
		/// This property would access the value of Primary Location Flag.
		/// </summary>
		public virtual bool PrimaryLocationFlag
		{
			get
			{
				return m_bPrimaryLocationFlag;
			}
			set
			{
				m_bPrimaryLocationFlag = value;
			}
		}
		/// <summary>
		/// This property would access the value of Print OSHA Description Flag.
		/// </summary>
		public virtual bool PrintOSHADescFlag
		{
			get
			{
				return m_bPrintOSHADescFlag;
			}
			set
			{
				m_bPrintOSHADescFlag = value;
			}
		}
		/// <summary>
		/// This property would access the value of As Of Date.
		/// </summary>
		public virtual DateTime AsOfDate
		{
			get
			{
				return m_datAsOfDate;
			}
			set
			{
				m_datAsOfDate = value;
			}
		}
		/// <summary>
		/// This property would access the value of Report On entity.
		/// </summary>
		public virtual int ReportOn
		{
			get
			{
				return m_iReportOn;
			}
			set
			{
				m_iReportOn = value;
			}
		}
		/// <summary>
		/// This property would access the value of array list of selected entities.
		/// </summary>
		public virtual ArrayList SelectedEntities
		{
			get
			{
				return m_arrlstEntities;
			}
			set
			{
				m_arrlstEntities=value;
			}
		}
		/// <summary>
		/// This property would access the value of start date time.
		/// </summary>
		public virtual DateTime StartTime
		{
			get
			{
				return m_datStartTime;
			}
			set
			{
				m_datStartTime = value;
			}
		}
		/// <summary>
		/// This property would access the value of report level.
		/// </summary>
		public virtual int ReportLevel
		{
			get
			{
				return m_iReportLevel;
			}
			set
			{
				value=m_bByOSHAEstablishmentFlag ? 1012:value;
				m_iReportLevel=value;
				m_sReportLevel=Declarations.OrgLevelToEIDColumn(value,m_iClientId);
			}
		}

        public virtual int ClientId
        {
            get
            {
                return m_iClientId;
            }
            set
            {
                m_iClientId = value;
            }
        }
		/// <summary>
		/// This property would access the value of Use Reports at Level.
		/// </summary>
		public virtual int UseReportLevel
		{
			get
			{
				return m_iUseReportLevel;
			}
			set
			{
				value=m_bByOSHAEstablishmentFlag ? 1012:value;
				m_iUseReportLevel=value;
				m_sUseReportLevel=Declarations.OrgLevelToEIDColumn(value,m_iClientId);
			}
		}
		/// <summary>
		/// This property would access the value of Use Reports at Level Description.
		/// </summary>
		public virtual string sUseReportLevel
		{
			get
			{
				return m_sUseReportLevel;
			}
		}
		/// <summary>
		/// This property would access the value of Report Level Description.
		/// </summary>
		public virtual string sReportLevel
		{
			get
			{
				return m_sReportLevel;
			}
		}
		/// <summary>
		/// This property would access the value of Enforce 180 day rule flag.
		/// </summary>
		public virtual bool Enforce180DayRule
		{
			get
			{
				return m_bEnforce180DayRule;
			}
			set
			{
				m_bEnforce180DayRule = value;
			}
		}

		/// <summary>
		/// This property would access the value of Sort Order.
		/// </summary>
		public virtual int ItemSortOrder
		{
			get
			{
				return m_iItemSortOrder;
			}
			set
			{
				m_iItemSortOrder = value;
			}
		}
        
        //MITS 37193
        /// <summary>
        /// This property would access the value of Column E Data source.
        /// </summary>
        public virtual int ColumnESource
        {
            get
            {
                return m_iColumnESource;
            }
            set
            {
                m_iColumnESource = value;
            }
        }

		/// <summary>
		/// This property would access the value of Column F Data source.
		/// </summary>
		public virtual int ColumnFSource
		{
			get
			{
				return m_iColumnFSource;
			}
			set
			{
				m_iColumnFSource = value;
			}
		}
        //MITS 37469 Begin                                        
        /// <summary>
        /// This property would access value of Use Employee Names flag.
        /// </summary>
        public virtual bool UseEmployeeNames
        {
            get
            {
                return m_bUseEmployeeNames;
            }
            set
            {
                m_bUseEmployeeNames = value;
            }
        }
        //MITS 37469 End
        /// <summary>                                              
		/// This property would access the value of Prefix for Establishment Name.
		/// </summary>
		public virtual string EstablishmentNamePrefix
		{
			get
			{
				return m_sEstablishmentNamePrefix;
			}
			set
			{
				value=m_bByOSHAEstablishmentFlag ? "0" : value;
				if (value != null)
					m_sEstablishmentNamePrefix = value;
				else
					m_sEstablishmentNamePrefix = "";
			}
		}
		/// <summary>
		/// This property would access the value of User Filename.
		/// </summary>
		public virtual string UserFileName
		{
			get
			{
				return m_sUserFileName;
			}
			set
			{
				if (value != null)
					m_sUserFileName = value;
				else
					m_sUserFileName = "";
			}
		}
		
		/// <summary>
		/// This property would access the object holding the information about the company.
		/// </summary>
		public virtual EntityItem Company
		{
			get
			{
				return m_objCompany;
			}
		}
		/// <summary>
		/// This property would access the array list of OSHA Report Item.
		/// </summary>
		public virtual ArrayList Items
		{
			get
			{
				return m_arrlstOshaItems;
			}
		}
		/// <summary>
		/// This property would access the error object.
		/// </summary>
		public virtual ColError Errors
		{
			get
			{
				return m_objColErrors;
			}
		}

		/// <summary>
		/// This property would access an instance of InstanceUtilities.
		/// </summary>
		public virtual InstanceUtilities Context
		{
			get
			{

				return m_objInstance;
			}
		}
		/// <summary>
		/// This property would access value of Print soft error log flag.
		/// </summary>
		public virtual bool PrintSoftErrLog
		{
			get
			{
				return m_bPrintSoftErrLog;
			}
			set
			{
				m_bPrintSoftErrLog=value;
			}
		}
		/// <summary>
		/// This property would access a Preparer class object.
		/// </summary>
		public virtual Preparer Preparer
		{
			get
			{
				return m_objPreparer;
			}
		}
		#endregion

		#region Methods
		/// <summary>
		/// This method would validate the report criteria(s)/parameter(s).
		/// </summary>
		/// <returns>True/False</returns>
		protected virtual bool ValidateCriteria()
		{
			bool bRetVal=false;
			try
			{
				
				if(this.YearOfReport == "")
				{
					if(this.BeginDate==DateTime.MinValue)
					{
						throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.BeginDate",m_iClientId));
					}
					if(this.EndDate==DateTime.MinValue)
					{
						
						throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.EndDate",m_iClientId));
					}
					if((this.BeginDate.Year!=this.EndDate.Year))
					{
						throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.DateRange",m_iClientId));
					}
					if(Declarations.GetIntValFromString(Conversion.ToDbDate(this.EndDate),m_iClientId)-Declarations.GetIntValFromString(Conversion.ToDbDate(this.BeginDate),m_iClientId)<=0)
					{
						throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.InvalidDateRange",m_iClientId));
					}
        
					if(this.BeginDate.Year < 2002)
					{
						throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.OshaRule",m_iClientId));
					
					}
				}
				else
				{
					if(Declarations.GetIntValFromString(this.YearOfReport,m_iClientId) < 2002)
					{
						throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.OshaRule",m_iClientId));

					}
				}
				if((this.ReportLevel <1005) || (this.ReportLevel > 1012))
				{
                    throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.InvalidReportlevel", m_iClientId));

				}
				if((this.UseReportLevel <1005) || (this.UseReportLevel > 1012))
				{
                    throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.InvalidUseReportlevel", m_iClientId));

				}
				if(this.UseReportLevel > this.ReportLevel)
				{
                    throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.InvalidLevels", m_iClientId));
				}
				if((!this.AllEntitiesFlag) && this.SelectedEntities.Count==0)
				{
                    throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.NoEntity", m_iClientId));
				}
				if(this.AsOfDate != DateTime.MinValue)
				{
					//Only check if not doing full year report.
					if (!(this.BeginDate ==Conversion.ToDate(this.YearOfReport + "0101") && this.EndDate == Conversion.ToDate(this.YearOfReport + "1231")))
					{
						if(this.AsOfDate.CompareTo(this.EndDate)< 0) 
						{
                            throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.InvalidAsOfDate", m_iClientId));
						}
					}
    
				}
				//Check that Establishment Prefix is "above" Use Reports at level.
				// In Vb OshaLib, this check was missing for Osha301 and Osha sharps log.
				if((Declarations.GetIntValFromString(this.EstablishmentNamePrefix,m_iClientId)) > this.UseReportLevel)
				{
                    throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.InvalidEstablishmentPrefix", m_iClientId));
					
				
				}
				bRetVal=true;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("CriteriaEntity.ValidateCriteria.ErrorValidate", m_iClientId), p_objException);
			}		
			return bRetVal;
		}

#endregion
	}
	/// <summary>
	/// This entity represents the error data.
	/// </summary>
	internal class Error
	{   
		#region Member Variables
		/// <summary>
		/// Error number
		/// </summary>
		private int m_iNumber;
		/// <summary>
		/// Description of the error
		/// </summary>
		private string m_sDescription="";
		/// <summary>
		/// Source of the error
		/// </summary>
		private string m_sSource="";
		/// <summary>
		/// Line on which error has occurred
		/// </summary>
		private int m_iLine;
		#endregion

		#region Properties
		/// <summary>
		/// This property provides access to the number of the error. 
		/// </summary>
		internal int Number
		{
			get
			{
				return m_iNumber;
			}
			set
			{
				m_iNumber = value;
			}
		}
		/// <summary>
		/// This property provides access to the description of the error. 
		/// </summary>
		internal string Description
		{
			get
			{
				return m_sDescription;
			}
			set
			{
				if (value != null)
					m_sDescription = value;
				else
					m_sDescription = "";
			}
		}
		/// <summary>
		/// This property provides access to the source of the error. 
		/// </summary>
		internal string Source
		{
			get
			{
				return m_sSource;
			}
			set
			{
				if (value != null)
					m_sSource = value;
				else
					m_sSource = "";
			}
		}
		/// <summary>
		/// This property provides access to the line number of the error. 
		/// </summary>
		internal int Line
		{
			get
			{
				return m_iLine;
			}
			set
			{
				m_iLine = value;
			}
		}

		#endregion

		#region Constructor
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		internal Error()
		{
			
		}
		#endregion
	}
	/// <summary>
	/// This class will hold the basic information about the entity.
	/// </summary>
	public class EntityItem
	{    
		#region Member Variables
		/// <summary>
		/// Name of the entity
		/// </summary>
		private string m_sName = "";
		/// <summary>
		/// Address data of the entity
		/// </summary>
		private string m_sAddress = "";
		/// <summary>
		/// City data of the entity
		/// </summary>
		private string m_sCity = "";
		/// <summary>
		/// State data of the entity
		/// </summary>
		private string m_sState = "";
		/// <summary>
		///  Zip Code data of the entity
		/// </summary>
		private string m_sZipCode = "";
		/// <summary>
		/// SIC Code data of the entity
		/// </summary>
		private string m_sSICCode = "";
		/// <summary>
		/// Nature Of Business data of the entity
		/// </summary>
		private string m_sNatureOfBusiness = "";
		/// <summary>
		/// NAICS Text data of the entity
		/// </summary>
		private string m_sNAICSText = "";
		/// <summary>
		/// Entity ID of the entity
		/// </summary>
		private int m_iEntityID;
		#endregion

		#region Properties
		/// <summary>
		/// This property provides access to the name data of the entity.
		/// </summary>
		internal string Name
		{
			get
			{
				return m_sName;
			}
			set
			{
				if (value != null)
					m_sName = value;
				else
					m_sName = "";
			}
		}
		/// <summary>
		/// This property provides access to the address data of the entity.
		/// </summary>
		internal string Address
		{
			get
			{
				return m_sAddress;
			}
			set
			{
				if (value != null)
					m_sAddress = value;
				else
					m_sAddress = "";
			}
		}
		/// <summary>
		/// This property provides access to the city data of the entity.
		/// </summary>
		internal string City
		{
			get
			{
				return m_sCity;
			}
			set
			{
				if (value != null)
					m_sCity = value;
				else
					m_sCity = "";
			}
		}
		/// <summary>
		/// This property provides access to the state data of the entity.
		/// </summary>
		internal string State
		{
			get
			{
				return m_sState;
			}
			set
			{
				if (value != null)
					m_sState = value;
				else
					m_sState = "";
			}
		}
		/// <summary>
		/// This property provides access to the zip code data of the entity.
		/// </summary>
		internal string ZipCode
		{
			get
			{
				return m_sZipCode;
			}
			set
			{
				if (value != null)
					m_sZipCode = value;
				else
					m_sZipCode = "";
			}
		}
		/// <summary>
		/// This property provides access to the SIC code data of the entity.
		/// </summary>
		internal string SicCode
		{
			get
			{
				return m_sSICCode;
			}
			set
			{
				if (value != null)
					m_sSICCode = value;
				else
					m_sSICCode = "";
			}
		}
		/// <summary>
		/// This property provides access to the Nature Of Business data of the entity.
		/// </summary>
		internal string NatureOfBusiness
		{
			get
			{
				return m_sNatureOfBusiness;
			}
			set
			{
				if (value != null)
					m_sNatureOfBusiness = value;
				else
					m_sNatureOfBusiness = "";
			}
		}
		/// <summary>
		/// This property provides access to the NAICS Text data of the entity.
		/// </summary>
		internal string NaicsText
		{
			get
			{
				return m_sNAICSText;
			}
			set
			{
				if (value != null)
					m_sNAICSText = value;
				else
					m_sNAICSText = "";
			}
		}
		/// <summary>
		/// This property provides access to the Entity ID of the entity.
		/// </summary>
		internal int EntityID
		{
			get
			{
				return m_iEntityID;
			}
			set
			{
				m_iEntityID = value;
			}
		}
		#endregion

	}
	/// <summary>
	/// This class will contain exposure information.
	/// </summary>
	internal class Exposure
	{
		#region Member Variables
		/// <summary>
		/// Row Id of the exposure entity.
		/// </summary>
		protected int m_iRowID;
		/// <summary>
		/// Number of the employees
		/// </summary>
		protected int m_iEmployees;
		/// <summary>
		/// Number of the Hours
		/// </summary>
		protected int m_iHours;
		/// <summary>
		/// Start date of the exposure entity
		/// </summary>
		protected DateTime m_datStartDate;
		/// <summary>
		/// End date of the exposure entity
		/// </summary>
		protected DateTime m_datEndDate;
		/// <summary>
		/// Day Count of the exposure entity
		/// </summary>
		protected int m_iDayCount;
		#endregion

		#region Properties
		/// <summary>
		/// This property provides access to Row Id of the exposure entity. 
		/// </summary>
		internal int RowID
		{
			get
			{
				return m_iRowID;
			}
			set
			{
				m_iRowID = value;
			}
		}
		/// <summary>
		/// This property provides access to number of employees field of the exposure entity. 
		/// </summary>
		internal int Employees
		{
			get
			{
				return m_iEmployees;
			}
			set
			{
				m_iEmployees = value;
			}
		}
		/// <summary>
		/// This property provides access to number of hours field of the exposure entity. 
		/// </summary>
		internal int Hours
		{
			get
			{
				return m_iHours;
			}
			set
			{
				m_iHours = value;
			}
		}
		/// <summary>
		/// This property provides access to start date field of the exposure entity. 
		/// </summary>
		internal DateTime StartDate
		{
			get
			{
				return m_datStartDate;
			}
			set
			{
				m_datStartDate = value;
			}
		}
		/// <summary>
		///  This property provides access to end date field of the exposure entity. 
		/// </summary>
		internal DateTime EndDate
		{
			get
			{
				return m_datEndDate;
			}
			set
			{
				m_datEndDate = value;
			}
		}
		/// <summary>
		/// This property provides access to day count field of the exposure entity. 
		/// </summary>
		internal int DayCount
		{
			get
			{
				return m_iDayCount;
			}
			set
			{
				m_iDayCount = value;
			}
		}
		#endregion

		#region Constructor
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		internal Exposure()
		{
			
		}
		#endregion
	}
	/// <summary>
	/// This class will contain the Preparer information.
	/// </summary>
	internal class Preparer
	{		
		#region Member Variables
		/// <summary>
		/// This field will hold the name of the Preparer.
		/// </summary>
		private string m_sName;
		/// <summary>
		/// This field will hold the title of the Preparer.
		/// </summary>
		private string m_sTitle;
		/// <summary>
		/// This field will hold the phone number of the Preparer.
		/// </summary>
		private string m_sPhone;
		/// <summary>
		/// This field will hold the signature date of the Preparer. 
		/// </summary>
		private DateTime m_datSignatureDate;	
		#endregion

		#region Properties
		/// <summary>
		///Property to access preparer's name.
		/// </summary>
		internal string Name
		{
			get
			{
				return m_sName;
			}
			set
			{
				if (value != null)
					m_sName=value;
				else
					m_sName="";
			}
		}

		/// <summary>
		///Property to access preparer's title.
		/// </summary>
		internal string Title
		{
			get
			{
				return m_sTitle;
			}
			set
			{
				if (value != null)
					m_sTitle=value;
				else
					m_sTitle="";
			}
		}

		/// <summary>
		///Property to access preparer's phone number.
		/// </summary>
		internal string Phone
		{
			get
			{
				return m_sPhone;
			}
			set
			{
				if (value != null)
					m_sPhone=value;
				else
					m_sPhone="";
			}
		}

		/// <summary>
		///Property to access preparer's signature date.
		/// </summary>
		internal DateTime SignatureDate
		{
			get
			{
				return m_datSignatureDate;
			}
			set
			{
				m_datSignatureDate=value;
			}
		}
		#endregion

		#region Constructor
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		internal Preparer()
		{
			
		}
		#endregion
			
	}
	/// <summary>
	/// This class will contain selected entities for report.
	/// </summary>
	internal class ReportItem
	{		
		#region Member Variables
		/// <summary>
		/// This will hold the selected entities for report.
		/// </summary>
		private ArrayList m_arrlstSelectedEntities=null;
		/// <summary>
		/// This will hold the entity id on which report to be generated.
		/// </summary>
		private int m_iReportOnEID;
		#endregion
		
		#region Properties
		/// <summary>
		///Property to access Report On EID field
		/// </summary>
		internal int ReportOnEID
		{
			get
			{
				return m_iReportOnEID;
			}
			set
			{
				m_iReportOnEID=value;
			}
		}
		/// <summary>
		/// This property provides access to selected entities collection.
		/// </summary>
		internal ArrayList SelectedEntities
		{
			get
			{
				return m_arrlstSelectedEntities;
			}
			set
			{
				m_arrlstSelectedEntities=value;
			}
		}
		#endregion

		#region Constructor
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		internal ReportItem()
		{
			m_arrlstSelectedEntities=new ArrayList();
		}
		#endregion
	}
}
