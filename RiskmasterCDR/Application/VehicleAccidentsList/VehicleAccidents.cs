using System;
using System.Xml ;
using System.Text ;
using System.Collections ;
using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.ExceptionTypes ;


namespace Riskmaster.Application.VehicleAccidentsList
{
	/// <summary>
	///Author  :   Rahul Sharma
	///Dated   :   20th Sept. 2005
	///Purpose :   This class performs the operations related to Vehicle Accidents Listing.
	/// </summary>
	public class VehicleAccidents
	{
        private int m_iClientId = 0;
        public VehicleAccidents(int p_iClientId)
		{
            m_iClientId = p_iClientId;
			//
			// TODO: Add constructor logic here
			//
		}
		#region Get Accidents List
		/// Name		: GetAccidentsList
		/// Author		: Rahul Sharma
		/// Date Created: 09/20/2005
		///************************************************************
		///					Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 
		///************************************************************
		/// <summary>
		///		Gets the list of the Vehicles Accident for a Vehicle Class
		/// </summary>
		/// <param name="sUnitId">Unit Id of Vehicle Class</param>
		/// <param name="strConnection">Connection String</param>
		/// <returns>XML String:
		///		<AccidentsList>
		///			<ClaimId></ClaimId>
		///			<ClaimNumber></ClaimNumber>
		///			<AccidentDate></AccidentDate>
		///		</AccidentsList>
		public string GetAccidentsList(string sUnitId,string strConnection,string sLangCode)
		{
			DbReader objReader=null;
			string strOutXml=string.Empty;
			string sSQL=string.Empty;

			try
			{
				strOutXml="<AccidentsList>";
				sSQL="SELECT VEHICLE_X_ACC_DATE.ACCIDENT_DATE, VEHICLE_X_ACC_DATE.CLAIM_ID, CLAIM.CLAIM_NUMBER FROM VEHICLE_X_ACC_DATE, CLAIM WHERE VEHICLE_X_ACC_DATE.UNIT_ID=" + sUnitId + " AND CLAIM.CLAIM_ID=VEHICLE_X_ACC_DATE.CLAIM_ID";
				objReader=DbFactory.GetDbReader(strConnection,sSQL);
				if(objReader!=null)
				{
					while(objReader.Read())
					{
						strOutXml=strOutXml + "<Record>";

                        //srajindersin - Pentesting - 27682
                        //strOutXml = strOutXml + "<ClaimId>" + objReader.GetValue("CLAIM_ID").ToString() + "</ClaimId>";
                        //strOutXml = strOutXml + "<ClaimNumber>" + objReader.GetString("CLAIM_NUMBER").ToString() + "</ClaimNumber>";
                        //strOutXml = strOutXml + "<AccidentDate>" + Conversion.GetDBDateFormat(objReader.GetValue("ACCIDENT_DATE").ToString(), "d") + "</AccidentDate>";

                        strOutXml = strOutXml + "<ClaimId>" + CommonFunctions.HTMLCustomEncode(objReader.GetValue("CLAIM_ID").ToString()) + "</ClaimId>";
                        strOutXml = strOutXml + "<ClaimNumber>" + CommonFunctions.HTMLCustomEncode(objReader.GetString("CLAIM_NUMBER").ToString()) + "</ClaimNumber>";
                        strOutXml = strOutXml + "<AccidentDate>" + Conversion.GetUIDate(CommonFunctions.HTMLCustomEncode(objReader.GetValue("ACCIDENT_DATE").ToString()), sLangCode, m_iClientId) + "</AccidentDate>";
                        //END srajindersin - Pentesting - 27682
						
                        strOutXml=strOutXml + "</Record>";
					}
				}
				strOutXml=strOutXml + "</AccidentsList>";
				
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("VehicleAccidents.AccidentsList.Error", m_iClientId), p_objEx);
			}
			finally
			{
				//Modified by Shivendu
                if (objReader != null)
                    objReader.Dispose();
			}
			return strOutXml;
		}
		#endregion
	}
}
