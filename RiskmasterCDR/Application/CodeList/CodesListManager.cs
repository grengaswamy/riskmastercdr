﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 03/14/2014 | 35039   | pgupta93   | Changes req for search code description on lookup
 * 09/20/2014 | rma-857 | ngupta36  | Incurred Handling - Collection Allowed or Not Allowed
 **********************************************************************************************/
using System;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Models;
using Riskmaster.Security;
using Riskmaster.Settings;
//using Riskmaster.DataModel;

namespace Riskmaster.Application.CodesList
{
    ///************************************************************** 
    ///* $File		: CodesListManager.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 10-May-2005
    ///* $Author	: Nikhil Kr. Garg 
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to fetch the list of codes for different Entities and Tables.
    ///	It caters to two functionalities: "getcode" and "quicklookup"
    /// </summary>
    public class CodesListManager
    {        
        #region "Constructor"
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CodesListManager()
        {
        }


        /// <summary>
        /// Overloaded constructor, which sets the connection string.
        /// </summary>
        /// <param name="p_sConnectionstring">Database connection string</param>        
        public CodesListManager(string p_sConnectionString,int p_iClientId)
        {
            m_sConnectionString = p_sConnectionString;
            m_iClientId = p_iClientId; //Ash - cloud
            MAX_CODES_PER_PAGE = GetMaxRecordsonPage();

            m_bIsOracle = DbFactory.IsOracleDatabase(m_sConnectionString);
        }
        #endregion

        #region "Constants"
        /// <summary>
        /// Max no of Codes displayed on one page. Used for Paging on the screen
        /// </summary>
        public int MAX_CODES_PER_PAGE = 1;
        #endregion

        #region "Variables"
        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_sConnectionString = string.Empty;
        private int m_iClientId = 0;
        private bool m_bIsSecurityDatabase = false;
        private bool m_bIsOracle = false;
        private bool m_bIsOrgSec = false; //pmittal5 confidential record
        private int m_userid = 0; //pmittal5 confidential record
        private int iPolSystemId = 0;//rupal for policy system interafce
        private bool m_bIsMobileAdjuster = false; //ijha Mobile Adjsuter
        //Aman ML Changes
        private string m_sLanguageCode = string.Empty; 
        /// <summary>
        /// Private variable for DB ID need for generating lang code
        /// </summary>
        private string m_DBId = "";
        ///// <summary>
        ///// Private variable for User ID need for generating lang code
        ///// </summary>
        //private int m_UserId = 0;
        /// <summary>
        /// Private variable for identifying that the code lookup table name includes CODES table 
        /// </summary>
        private bool m_CodesTable = false;
        /// <summary>
        /// Code Table or not
        /// </summary>
        public bool CodesTable
        {
            get
            {
                return m_CodesTable;
            }
            set
            {
                m_CodesTable = value;
            }
        }
        /// <summary>
        /// Passed DBID can be used to set up NLSCode
        /// </summary>
        public string DBID
        {
            get
            {
                return m_DBId;
            }
            set
            {
                m_DBId = value;
            }
        }
        /// <summary>
        /// Passed User ID that can be used to set up NLSCode
        /// abisht MITS 10932
        /// </summary>
        //public int USERID
        //{
        //    get
        //    {
        //        return m_UserId;
        //    }
        //    set
        //    {
        //        m_UserId = value;
        //    }
        //}		
        public string LanguageCode
        {
            get
            {
                return m_sLanguageCode;
            }
            set
            {
                m_sLanguageCode = value;
            }
        }
        //Aman ML chanegs
        //pmittal5 confidential record
        public Boolean OrgSec
        {
            get
            {
                return m_bIsOrgSec;
            }
            set
            {
                m_bIsOrgSec = value;
            }
        }
        public int userid
        {
            get
            {
                return m_userid;
            }
            set
            {
                m_userid = value;
            }
        }
        //End
        // ijha: Mobile adjuster
        public Boolean bIsMobileAdjuster
        {
            get
            {
                return m_bIsMobileAdjuster;
            }
            set
            {
                m_bIsMobileAdjuster = value;
            }
        }
        //End 
        private string useMultiLingual = string.Empty;
        #endregion

        #region "Public Functions"

        /// Name		: GetCodes
        /// Author		: Nikhil Kumar Garg
        /// Date Created: 11-May-2005
        ///*************************************************************************
        /// Amendment History
        ///*************************************************************************
        /// Date Amended   *   Amendment   *								Author
        ///*************************************************************************
        /// <summary>
        /// Gets the codes list for the entity according to page number.
        /// </summary>
        /// <param name="p_lDeptEId">Department Id</param>
        /// <param name="p_sTableName">Table Name for the Codes</param>
        /// <param name="p_sLOB">Line Of Business</param>
        /// <param name="p_sTypeLimits">Limits for Claim_Types or Event_Types</param>
        /// <param name="p_sFormName">FormName on which the code is required</param>
        /// <param name="p_sTriggerDate">Date for filtering</param>
        /// <param name="p_sSessionDsnId">DSN Id stored in the session</param>
        /// <param name="p_sSessionLOB">LOB stored in the session</param>
        /// <param name="p_sSessionClaimId">Claim Id stored in the session</param>
        /// <param name="p_lRecordCount">Record Count</param>
        /// <param name="p_lPageNumber">Current Page Number</param>
        /// <param name="p_sInsuredEid">Insured ID</param>
        /// <returns>XML Document containing the codes list</returns>
        //Start:Commented by Nitin Goel,02/25/2010,MITS#18231
        //public CodeListType GetCodesNew(long p_lDeptEId, string p_sTableName, string p_sLOB,
        //                            string p_sTypeLimits, string p_sFormName, string p_sTriggerDate, string p_sEventDate,
        //                            string p_sSessionDsnId, string p_sSessionLOB, string p_sSessionClaimId, long p_lRecordCount,
        //                            long p_lPageNumber, string p_sFilter, string p_sTitle, string p_sInsuredEid, string p_sEventId, string sParentCodeID, string p_sCodeFilter)//--stara Mits 16667 //Tushar added sCodefilter for VACo MITS 18231
		//p_sJurisdiction MITS # 36929
        public CodeListType GetCodesNew(long p_lDeptEId, string p_sTableName, string p_sLOB, string p_sJurisdiction,
        //public CodeListType GetCodesNew(long p_lDeptEId, string p_sTableName, string p_sLOB,
                                    string p_sTypeLimits, string p_sFormName, string p_sTriggerDate, string p_sEventDate,
                                    string p_sSessionDsnId, string p_sSessionLOB, string p_sSessionClaimId, long p_lRecordCount,
                                    long p_lPageNumber, string p_sFilter, string p_sTitle, string p_sInsuredEid, string p_sEventId,
                                    string sParentCodeID, string p_sShowCheckBox, string p_sPolicyId, string p_sCovTypecode, string p_sTransId, string p_sClaimantEid,
                                    string p_sIsFirstFinal, string p_sPolUnitRowId, string p_sRcRowId, string p_sPolicyLOB, string p_sCovgSeqNum, 
                                    string sRsvStatusParent, string p_sTransSeqNum, string p_sSortColumn, string p_sSortOrder, int iLangCode, 
                                    string p_sFieldName, string p_sCoverageKey, string p_sLossTypeCode)//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17//--stara Mits 16667 //Tushar added sCodefilter for VACo MITS 18231
        //End:Nitin Goel,02/25/2010,MITS#18231 //rsushilaggar Handled  paging for checkbox functionality  MITS 21600
        //rupal: added p_sIsFirstFinal parameter to identify first & final payment, added p_PolCobgRowID to fetch reserve type based on this in case of First & final Payment
        //smishra54: MITS 33996-Added p_sTransSeqNum as new input parameter.
        //avipinsrivas: MITS 36081-Added p_sCoverageKey as new input parameter.
        //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
        {
            int iMonthNow = 0;				//current month
            int iDay = 0;						//current day
            int iLOB;                       //Line of Business :Umesh
            string sToday = string.Empty;		//string for Date
            long lFacEId = 0;					//Facility Id
            long lLocEId = 0;					//Location Id
            long lDivEId = 0;					//Division Id
            long lRegEId = 0;					//Region Id
            long lOprEId = 0;					//Operation Id
            long lComEId = 0;					//Company Id
            long lCliEId = 0;					//Client Id
            long lClaimantEid = 0;
            StringBuilder sSQL = null;		//SQL for searching for the code
            string sLOBSQL = string.Empty;	//SQL string for LOB
            string sLimitsSQL = string.Empty;	//SQL string for Claim/Event Type
            string sAdjDTTFilter = string.Empty;			//umesh
            string[] sarrSplit;				//used for parsing the limits
            string[] sarrSplit1;			//used for parsing the limits
            string[] sarrFilter;           //Umesh
            XmlDocument objXmlDocument = null;      // Umesh
            //DbReader objRdr=null;//Umesh
            CodeListType objCodeList = null;
            ColLobSettings objColLobSettings = null;
            //rsharma220 MITS 33923
            //SysSettings objSettings = new SysSettings(m_sConnectionString);
            // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
            int iSessionClaimId = 0;
            bool bSuccess = false;
            lClaimantEid = Conversion.CastToType<int>(p_sClaimantEid,out bSuccess);
            iSessionClaimId = Conversion.CastToType<int>(p_sSessionClaimId, out bSuccess);
            // npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim

            //PJS MITS 15220
            string sInsParentEid = string.Empty;  
            //PJS MITS 15220

            //rupal:start, for first & final payment
            long lPolCvgRoWId = 0;
            bool bConverted = false;
            bool bIsFirstFinal = false;
            bIsFirstFinal = Conversion.CastToType<bool>(p_sIsFirstFinal, out bConverted);
            SysSettings objSysSetting = null;// new SysSettings(m_sConnectionString);
            //rupal:end,  for first & final payment
            LocalCache objCache = null;
            DbConnection objConn = null;
            bool success;       //Ankit Start : Worked on MITS - 34657
            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objSysSetting = new SysSettings(m_sConnectionString,m_iClientId); //Ash - cloud
                objConn.Open();
                //Aman ML Change
                //string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
                //if (m_sLanguageCode.Equals(string.Empty))
                //{
                //    if (!m_DBId.Equals(string.Empty))
                        m_sLanguageCode = GetNLSCode(int.Parse(m_DBId)).ToString();

                //}
                //Aman ML Change
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                if (p_lPageNumber == 0)
                    p_lPageNumber = 1;

                iMonthNow = DateTime.Now.Month;
                iDay = DateTime.Now.Day;

                sToday = DateTime.Now.Year.ToString();
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);//asharma326 JIRA 857 Starts
                iLOB = Conversion.ConvertStrToInteger(p_sSessionLOB);//asharma326 JIRA 857 Starts
                //for the date string
                if (iMonthNow < 10)
                    sToday = sToday + "0" + iMonthNow.ToString();
                else
                    sToday = sToday + iMonthNow.ToString();

                if (iDay < 10)
                    sToday = sToday + "0" + iDay.ToString();
                else
                    sToday = sToday + iDay.ToString();
                //If page is child of event , Dept_Id would be Dept_Id of Event
                //MITS 17449 : Umesh
                if (p_lDeptEId == 0 && p_sEventId != "" && p_sEventId != "0")
                {   
                    int iEventId = Conversion.ConvertStrToInteger(p_sEventId);
                    
                    using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString,
                                  "SELECT DEPT_EID FROM EVENT WHERE EVENT_ID =" + iEventId))
                    {

                        if (objRdr != null)
                        {
                            if (objRdr.Read())
                            {
                                long lDeptEId = Conversion.ConvertObjToInt64(objRdr["DEPT_EID"], m_iClientId);
                                if (lDeptEId > 0)
                                    p_lDeptEId = lDeptEId;
                               
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(p_sTransId))
                {
                    using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, "SELECT CLAIMANT_EID FROM FUNDS WHERE TRANS_ID =" + p_sTransId))
                    {
                        if (objRdr != null)
                        {
                            if (objRdr.Read())
                            {
                                lClaimantEid = Conversion.ConvertObjToInt64(objRdr["CLAIMANT_EID"], m_iClientId);
                            }
                        }
                    }
                }
                //MITS 17449 : End
                //if DepartmentId is passed then get the information about the Facility, Location etc. also.
                if (p_lDeptEId > 0)
                    GetOrgEid(p_lDeptEId, ref lFacEId, ref lLocEId, ref lDivEId, ref lRegEId, ref lOprEId,
                        ref lComEId, ref lCliEId,objConn);

                if (p_sTableName.Equals(string.Empty))
                    return null;

                sSQL = new StringBuilder();

                //PJS MITS 15220 
                sInsParentEid = GetParentEid(p_sInsuredEid);  
                //PJS MITS 15220
                
                //create SQL depending on the table name passed
                if (p_sTableName.ToLower().Equals("states"))
                {
                    sSQL.Append(@" SELECT STATES.STATE_ROW_ID, STATES.STATE_ID, STATES.STATE_NAME	
								FROM STATES WHERE DELETED_FLAG !=-1 AND STATE_ROW_ID > 0 ORDER BY STATES.STATE_ID");
                }
                //avipinsrivas start : Worked for Jira-340
                else if (p_sTableName.ToLower().Equals(Globalization.BUSINESS_ENTITY_ROLE.ToLower()) || p_sTableName.ToLower().Equals(Globalization.PEOPLE_ENTITY_ROLE.ToLower()))
                {
                    sSQL.Append("SELECT GT.TABLE_ID, GT.TABLE_NAME ");
                    sSQL.Append("FROM GLOSSARY G ");
                    sSQL.Append("INNER JOIN GLOSSARY_TEXT GT ON G.TABLE_ID = GT.TABLE_ID ");
                    if (p_sTableName.ToLower().Equals(Globalization.BUSINESS_ENTITY_ROLE.ToLower()))
                        sSQL.Append("WHERE G.GLOSSARY_TYPE_CODE = 4 ");
                    else if (p_sTableName.ToLower().Equals(Globalization.PEOPLE_ENTITY_ROLE.ToLower()))
                    {
                        sSQL.Append("WHERE G.GLOSSARY_TYPE_CODE = 7 ");
                        sSQL.Append("AND G.SYSTEM_TABLE_NAME NOT IN ('EMPLOYEES', 'PHYSICIANS', 'MEDICAL_STAFF', 'PATIENTS', 'WITNESS') ");     //avipinsrivas Start : Worked for 7551 (Issue of 4634 - Epic 340)
                    }
                    sSQL.Append("ORDER BY GT.TABLE_NAME ");
                }
                //avipinsrivas End
				    //Start:added by Nitin goel, MITS 30910, 08/22/2013
                    //Changed by Nikhil.Code review comments
               // else if (p_sTableName.ToLower().Equals("coverage_group"))
                else if (String.Compare(p_sTableName,"coverage_group",true) == 0)
                {
                    string sDateOfClaim = string.Empty;
                    string sDateOfEvent = string.Empty;
                    string sDateOfPolicy = string.Empty;
                    //Start -  Multilingual changes

                    int iLangcode = 0;
                    iLangcode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                    //end -  Multilingual changes
                    //Changed by Nikhil.Code review comments
                   // if (iSessionClaimId > 0 && !(string.IsNullOrEmpty(p_sSessionClaimId) || p_sSessionClaimId.ToLower().Trim() == "undefined"))
                    if (iSessionClaimId > 0 && !(string.IsNullOrEmpty(p_sSessionClaimId) || String.Compare(p_sSessionClaimId.Trim(),"undefined",true) == 0))
                        //Changed by Nikhil.Retrofit Changes NI
                   // GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy);
                        GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy, objConn, objSysSetting,p_sSessionLOB);
                    if (!(string.Compare(p_sPolicyId, "0") == 0) && !(string.IsNullOrEmpty(p_sPolicyId)))
                        //Changed by Nikhil.Code review comments
                    //sDateOfPolicy=DbFactory.ExecuteAsType<string>(m_sConnectionString,"SELECT EFFECTIVE_DATE FROM POLICY WHERE POLICY_ID = "+ p_sPolicyId);
                        sDateOfPolicy = DbFactory.ExecuteAsType<string>(m_sConnectionString,String.Format("SELECT EFFECTIVE_DATE FROM POLICY WHERE POLICY_ID = {0}", p_sPolicyId));

                    //Start: Added by Sumit Agarwal for NI on 11/10/2014 to get the values for sDateOfEvent and sDateOfClaim (if they are blank)
                    if (string.IsNullOrEmpty(sDateOfEvent))
                        sDateOfEvent = DbFactory.ExecuteAsType<string>(m_sConnectionString, String.Format("SELECT E.DATE_OF_EVENT FROM EVENT E INNER JOIN CLAIM C ON E.EVENT_ID = C.EVENT_ID AND C.CLAIM_ID =  {0}", p_sSessionClaimId));
                    if (string.IsNullOrEmpty(sDateOfClaim))
                        sDateOfClaim = DbFactory.ExecuteAsType<string>(m_sConnectionString, String.Format("SELECT DATE_OF_CLAIM FROM CLAIM WHERE CLAIM_ID = {0}", p_sSessionClaimId));
                    //End: Added by Sumit Agarwal for NI on 11/10/2014 to get the values for sDateOfEvent and sDateOfClaim (if they are blank)

                    sSQL.Length = 0;
                    //start -  Multilingual changes
                    //sSQL.Append(@" SELECT CG.COVERAGE_GROUP_ID, CG.COV_GROUP, CG.COV_GROUP_DESC");
                    //sSQL.Append(" FROM COVERAGE_GROUP CG WHERE CG.DELETED_FLAG !=-1 AND CG.COVERAGE_GROUP_ID > 0");
                    sSQL.Append(@" SELECT CG.COVERAGE_GROUP_ID, CG.COV_GROUP, CGT.GROUP_DESCRIPTION");
                    sSQL.AppendFormat(" FROM COVERAGE_GROUP CG INNER JOIN COVERAGE_GROUP_TEXT CGT ON CG.COVERAGE_GROUP_ID = CGT.COVERAGE_GROUP_ID WHERE CGT.LANGUAGE_CODE = {0} AND CG.DELETED_FLAG !=-1 AND CG.COVERAGE_GROUP_ID > 0 ", iLangcode);
                    //end -  Multilingual changes
                   //start:Added to filter Coverage group based on States value
                    if (!string.IsNullOrEmpty(p_sFilter))
                        sSQL.Append(" AND " + p_sFilter);
                    //end:Added to filter Coverage group based on States value
                    sSQL.Append(" AND ((CG.TRIGGER_DATE_FIELD IS NULL) ");
                                        
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CG.EFF_START_DATE<='" + sToday + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sToday + "') ");
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CG.EFF_START_DATE<='" + sToday + "' AND CG.EFF_END_DATE>='" + sToday + "') ");

                    if (!string.IsNullOrEmpty(sDateOfEvent))
                    {
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CG.EFF_START_DATE<='" + sDateOfEvent + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sDateOfEvent + "') ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CG.EFF_START_DATE<='" + sDateOfEvent + "' AND CG.EFF_END_DATE>='" + sDateOfEvent + "') ");

                    }
                    if (!string.IsNullOrEmpty(sDateOfClaim))
                    {
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CG.EFF_START_DATE<='" + sDateOfClaim + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sDateOfClaim + "') ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CG.EFF_START_DATE<='" + sDateOfClaim + "' AND CG.EFF_END_DATE>='" + sDateOfClaim + "') ");
                    }
                    if (!string.IsNullOrEmpty(sDateOfPolicy))
                    {
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CG.EFF_START_DATE<='" + sDateOfPolicy + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CG.EFF_START_DATE<='" + sDateOfPolicy + "' AND CG.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                    }
                    
                    sSQL.Append(") ORDER BY CG.COV_GROUP");
                }
                //end:added by Nitin goel, MITS 30910, 08/22/2013
                else if (p_sTableName.ToLower().Equals("cust_rate"))
                {
                    sSQL.Append(@" SELECT RATE_TABLE_ID, EFFECTIVE_DATE, EXPIRATION_DATE, TABLE_NAME FROM CUST_RATE
								WHERE CUST_RATE.CUSTOMER_EID = " + p_sLOB);
                }
                else if (p_sTableName.ToLower().Equals("event_type") || p_sTableName.ToLower().Equals("claim_type"))
                {
                    this.CodesTable = true; //Aman ML Change
                    if (Conversion.ConvertStrToLong(p_sLOB) > 0)
                        // JP 12.12.2005    sLOBSQL = " AND CODES.LINE_OF_BUS_CODE = " + p_sLOB ;
                        sLOBSQL = " AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) "; // JP 12.12.2005   If CODES.LINE_OF_BUS_CODE = 0 then always show - regardless of claim's lob
                    else
                        sLOBSQL = string.Empty;

                    if (p_sTypeLimits.Trim().Length > 0)
                    {
                        sarrSplit = p_sTypeLimits.Split('|');
                        for (int i = 0; i < sarrSplit.Length; i++)
                        {
                            sarrSplit1 = sarrSplit[i].Split(',');
                            if (Conversion.ConvertStrToBool(sarrSplit1[1]) == false)
                                sLimitsSQL = sLimitsSQL + "," + sarrSplit1[0];
                        }
                        if (sLimitsSQL.Trim().Length > 0)
                            sLimitsSQL = " AND CODES.CODE_ID NOT IN (" + sLimitsSQL.Substring(2) + ") ";
                    }
                    else
                        sLimitsSQL = string.Empty;


                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC, C2.SHORT_CODE");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID");
                    sSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sTableName.ToUpper() + "'");
                    sSQL.Append(" AND GLOSSARY.TABLE_ID=CODES.TABLE_ID");
                    sSQL.Append(" AND CODES.CODE_ID=CODES_TEXT.CODE_ID");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=1033");
                    //pmittal5 Mits 14480 05/14/09 - Allow deleted "Event Types" also in User Privileges setup
                    if (!(p_sTableName.ToLower().Equals("event_type") && p_sFormName.ToLower().Equals("userprivileges")))  
                        sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)");
                    sSQL.Append(sLOBSQL);
                    sSQL.Append(sLimitsSQL);

                    if (p_lDeptEId > 0)
                    {
                        sSQL.Append("	AND (CODES.ORG_GROUP_EID=" + p_lDeptEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lFacEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lLocEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lDivEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lRegEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lOprEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lComEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lCliEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID IS NULL ");
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                    }
                    else
                    {
                        //Abhishek MITS 11352, for claim_type with associated org types should also
                        //be displayed in claim type search
                        //pmittal5  MITS:12318  06/13/08  - All Codes of Query Fields should be displayed in case of "Search"
                        //if (!p_sTableName.ToLower().Equals("claim_type"))
                        //if (!p_sTableName.ToLower().Equals("claim_type") && p_sTitle != "Search")
                        // RMA 6448 - Claim Type/Claim Status or Event Type codes which have org hierarchy filter set in Table maintenance are not coming on User Privileges screen
                        if (p_sTitle != "Search" && p_sFormName != "TableField" && p_sFormName != "search" && ! p_sFormName.ToLower().Equals("userprivileges"))//rsushilaggar MITS 31339 Date 02/15/52013
                        {
                            sSQL.Append("	AND (CODES.ORG_GROUP_EID IS NULL ");
                            sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                        }
                    }
                    //zalam 08/27/2008 Mits:-11708
					//EvaluateTriggerDates(sSQL,p_sFormName, sToday, p_sTriggerDate,p_sEventDate);
                    //pmittal5 Mits 14480 05/14/09 - Allow deleted "Event Types" also in User Privileges setup
                    if (!(p_sTableName.ToLower().Equals("event_type") && p_sFormName.ToLower().Equals("userprivileges")))
                        EvaluateTriggerDates(sSQL, p_sFormName, sToday, p_sTriggerDate, p_sEventDate, p_sSessionClaimId, objConn, objSysSetting, p_sLOB);

                    //Mgaba2: R8:SuperVisory Approval
                    //Claim Types to be filtered on LOB basis
                    if (p_sFilter != "")
                        sSQL.Append(" AND " + p_sFilter);

                    if(Conversion.ConvertStrToBool(objSysSetting.MultiCovgPerClm.ToString()) && !string.IsNullOrEmpty(p_sPolicyLOB) && string.Equals(p_sTableName, "CLAIM_TYPE", StringComparison.InvariantCultureIgnoreCase) && (string.Equals(p_sFormName, "claimgc", StringComparison.InvariantCultureIgnoreCase) || string.Equals(p_sFormName, "claimwc", StringComparison.InvariantCultureIgnoreCase)))
                    {
                        objCache = new LocalCache(m_sConnectionString,m_iClientId);
                        sSQL.Append(" AND CODES.CODE_ID IN (SELECT CODE2 FROM CODE_X_CODE WHERE CODE1 = " + p_sPolicyLOB +  " AND REL_TYPE_CODE = " + objCache.GetCodeId("POLTOCT", "CODE_REL_TYPE").ToString() + " AND DELETED_FLAG =0)");
                    }
                    sSQL.Append(" ORDER BY CODES.SHORT_CODE");
                }
               
                else if (p_sTableName.ToLower().Equals("account"))
                {
                    sSQL.Append(" SELECT ACCOUNT_ID, NULL, ACCOUNT_NAME ");
                    sSQL.Append(" FROM ACCOUNT");
                    sSQL.Append(" ORDER BY ACCOUNT_ID ");
                }
                else if (p_sTableName.ToLower().Equals("rm_sys_users"))
                {
                    //m_sConnectionString=SecurityDatabase.GetSecurityDsn(0);
                    m_bIsSecurityDatabase = true;
                    sSQL.Append(" SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME");
                    sSQL.Append(" FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID");
                    sSQL.Append(" AND USER_DETAILS_TABLE.DSNID = " + p_sSessionDsnId);
                    sSQL.Append(" ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                }
                else if (p_sTableName.ToLower().Equals("user_groups"))
                {
                    sSQL.Append(" SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS ");
                    sSQL.Append(" ORDER BY GROUP_ID ");
                }            
                else if (p_sTableName.ToLower().Equals("trans_types"))
                {
                    this.CodesTable = true; //Aman ML Change
                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,C2.SHORT_CODE");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID");
                    sSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" + p_sTableName.ToUpper() + "' ");
                    //asharma326 JIRA 857 Starts
                    // npadhy RMA-6193 - In case the Funds is not associated with any claim, the LOB will be 0.
                    // In that case objColLobSettings[iLOB].PreventCollectionAllReserve crashes. This this check shall only be done
                    // when funds record is associated with claim
                    if ((!string.IsNullOrEmpty(p_sFilter)) && p_sFilter == "Split_Trans_Type")
                    {
                        if (iLOB > 0)
                        {
                            if (objColLobSettings[iLOB].PreventCollectionAllReserve == -1)
                            {
                                //only recovery reserve types
                                sSQL.Append(" AND C2.CODE_ID IN (SELECT CODE_ID FROM CODES WHERE CODES.RELATED_CODE_ID IN (SELECT CODE_ID FROM CODES_TEXT WHERE SHORT_CODE = 'R')) ");
                            }
                            else if (objColLobSettings[iLOB].PreventCollectionPerReserve == -1)
                            {
                                //sSQL.Append(" AND CODES.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sSessionLOB + ")");
                                sSQL.Append(" AND C2.CODE_ID NOT IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_NOT_ALLOWED WHERE LINE_OF_BUS_CODE = " + p_sSessionLOB + ")");
                            }
                        }
                        p_sFilter = "";
                    }
                    
                    //asharma326 JIRA 857 Ends
                    if (p_sSessionLOB.Equals(string.Empty) || p_sSessionLOB.Equals("0"))
                        sSQL.Append(" ");
                    else
                    {
                        string sTableID = string.Empty;
                        string sPolSql = string.Empty;
                        string sClaimChkSql = string.Empty;
                        string sGetReserveExternalSql = string.Empty;
                        string sPolicySystemCode = string.Empty;
                        int iIncludeClaimType = Int32.MinValue;

                        // if carrier claim is on,  for all cases 
                        if (objSysSetting.MultiCovgPerClm != 0)
                        {
                            if (objCache == null)
                                objCache = new LocalCache(m_sConnectionString,m_iClientId);
                            sTableID = objCache.GetTableId("RESERVE_TYPE").ToString();
                            
                            //rsharma220 MITS 33854 Start
                            if (!string.IsNullOrEmpty(p_sPolicyId))
                            {
                                sPolSql = "SELECT POLICY.POLICY_SYSTEM_ID, POLICY_X_WEB.POLICY_SYSTEM_CODE FROM POLICY, POLICY_X_WEB";
                                sPolSql=sPolSql+" WHERE POLICY_ID=" + p_sPolicyId+ " AND POLICY_X_WEB.POLICY_SYSTEM_ID=POLICY.POLICY_SYSTEM_ID";

                                using (DbReader objRdr = objConn.ExecuteReader(sPolSql))
                                {
                                    if (objRdr != null)
                                    {
                                        if (objRdr.Read())
                                        {
                                            iPolSystemId = Conversion.ConvertObjToInt(objRdr["POLICY_SYSTEM_ID"], m_iClientId);
                                            sPolicySystemCode = Conversion.ConvertObjToStr(objRdr["POLICY_SYSTEM_CODE"]);
                                            objRdr.Close();
                                        }
                                    }
                                }

                                //check if we have external policy, i.e. policy system id >0
                                if (iPolSystemId > 0)
                                {
                                    sClaimChkSql = "SELECT INCLUDE_CLAIM_TYPE FROM PS_MAP_TABLES WHERE RMX_TABLE_ID = "+sTableID +" AND POLICY_SYSTEM_TYPE_ID = "+sPolicySystemCode.Trim();
                                    //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, sClaimChkSql))
                                    //{
                                    //    if (objRdr != null)
                                    //    {
                                    //        if (objRdr.Read())
                                    //        {
                                    //            iIncludeClaimType = Conversion.ConvertObjToInt(objRdr["INCLUDE_CLAIM_TYPE"], m_iClientId);
                                    //            objRdr.Close();
                                    //        }
                                    //    }
                                    //}
                                    iIncludeClaimType = objConn.ExecuteInt(sClaimChkSql);
                                }
                            }
                            //rsharma220 MITS 33854 End
                        }
                        
                        
                        // 06/18/2007 MITS : 9499   by Umesh
                        objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                        iLOB = Conversion.ConvertStrToInteger(p_sSessionLOB);
                        // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                        //iClaimId = Conversion.ConvertStrToInteger(p_sSessionClaimId);
                        if (objColLobSettings[iLOB].ResByClmType == true && iSessionClaimId > 0 && (!string.IsNullOrEmpty(p_sSessionClaimId)))
                        {
                        //    using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString,
                        //      "SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iSessionClaimId))
                        //// npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                        //    {
                        //        if (objRdr.Read())
                        //        {
                                    int iClaimTypeCode = 0;
                                    iClaimTypeCode = objConn.ExecuteInt("SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iSessionClaimId);
                                //    sSQL.Append(" AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE=" + p_sSessionLOB + " AND CLAIM_TYPE_CODE =" + iClaimTypeCode + ")");
                            // akaushik5 Added for MITS 38314 Starts
                            if (!iClaimTypeCode.Equals(default(int)))
                            {
                                sSQL.AppendFormat(" AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE= {0} AND CLAIM_TYPE_CODE = {1})", p_sSessionLOB, iClaimTypeCode);
                            }
                            // akaushik5 Added for MITS 38314 Ends
                                //}

                                //objRdr.Close();
                            //}

                        } // END MITS 9499
                        else
                        {
                            ////JIRA-857 Start
                            //if (iLOB != null && (iLOB > 0) && (p_sFilter == "Split_Trans_Type"))
                            //{
                            //    if (objColLobSettings[iLOB].PreventCollectionAllReserve == -1)
                            //    {
                            //        //only recovery reserve types
                            //        sSQL.Append(" AND C2.CODE_ID IN (SELECT CODE_ID FROM CODES WHERE CODES.RELATED_CODE_ID IN (SELECT CODE_ID FROM CODES_TEXT WHERE SHORT_CODE = 'R')) ");
                            //    }
                            //    if (objColLobSettings[iLOB].PreventCollectionPerReserve == -1)
                            //    {
                            //        sSQL.Append(" AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sSessionLOB + ")");
                            //        sSQL.Append(" AND C2.CODE_ID NOT IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_NOT_ALLOWED WHERE LINE_OF_BUS_CODE = " + p_sSessionLOB + ")");
                            //    }
                            //    p_sFilter = "";
                            //}
                            //else
                            ////JIRA-857 Ends
                            //{
                                sSQL.Append(" AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sSessionLOB + ")");
                            //}
                        }
                        //if icnlude clasim type is true, get reserves based on mapping, required for point upload
                        if (objColLobSettings[iLOB].ResByClmType == false && iIncludeClaimType == 1 && iSessionClaimId > 0 && (!string.IsNullOrEmpty(p_sSessionClaimId)))
                        {
                            using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString,
                             "SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iSessionClaimId))
                            // npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            {
                                if (objRdr.Read())
                                {
                                    int iClaimTypeCode = 0;
                                    iClaimTypeCode = objRdr.GetInt("CLAIM_TYPE_CODE");
                                    sSQL.Append(" AND C2.CODE_ID IN (SELECT RMX_CODE_ID FROM POLICY_CODE_MAPPING WHERE POLICY_SYSTEM_ID = " + iPolSystemId.ToString() + " AND CLAIM_TYPE_CODE = " + iClaimTypeCode + " AND RMX_TABLE_ID = " + sTableID + ")");
                                }
                            }
                        }

                        //rupal:mits 27617
                        if (objSysSetting.UseResFilter == false && objSysSetting.MultiCovgPerClm != 0 && bIsFirstFinal == false) //if carrier claim is on and first&final=false
                        {
                            if (p_sPolUnitRowId != string.Empty)
                            {
                                //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                                sSQL.Append(" AND C2.CODE_ID IN (SELECT DISTINCT(RESERVE_TYPE_CODE) ");
                                sSQL.Append(" FROM RESERVE_CURRENT ");
                                sSQL.Append(" INNER JOIN COVERAGE_X_LOSS ON COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = RESERVE_CURRENT.POLCVG_LOSS_ROW_ID ");
                                sSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON COVERAGE_X_LOSS.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID ");
                                sSQL.Append(" WHERE POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID =" + p_sPolUnitRowId);
                                sSQL.Append(" AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId);
                                sSQL.Append(" AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =" + p_sCovTypecode);
                                sSQL.Append(" AND COVERAGE_X_LOSS.LOSS_CODE = " + p_sLossTypeCode);
                                sSQL.Append(" AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");
                                //Ankit End
                            }
                        }
                        //rupal:end, 
                    }
                    sSQL.Append(" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ");
                    sSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ");
                    if (p_sLOB.Trim() != "")
                    {
                        sSQL.Append(" AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) ");
                    }
                    
                    //rupal:start for first & final payment   
                    if (objSysSetting.UseResFilter == false && bIsFirstFinal && objSysSetting.MultiCovgPerClm != 0)
                    {
                        //in case of first & final payment,
                        //we donn't want certain trans type to be populated if any kind of payment is made for the trans type's parent type (reserve type)
                        //rupal:start,policy system interface
                        string sPolSql = "SELECT POLICY_SYSTEM_ID FROM POLICY WHERE POLICY_ID=" + p_sPolicyId;

                        //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, sPolSql))
                        //{
                        //    if (objRdr != null)
                        //    {
                        //        if (objRdr.Read())
                        //        {
                        //            iPolSystemId = Conversion.ConvertObjToInt(objRdr["POLICY_SYSTEM_ID"], m_iClientId);
                        //            objRdr.Close();
                        //        }
                        //    }
                        //}
                        iPolSystemId = objConn.ExecuteInt(sPolSql);
                        //iPolSystemId = Conversion.ConvertObjToInt(objRdr["POLICY_SYSTEM_ID"], m_iClientId);

                        string sSQLCovg = string.Empty;
                        if (iPolSystemId > 0)
                        {
                            // Start RMA-11452 - rkaur27
                            //sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_sPolUnitRowId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode + " and CVG_SEQUENCE_NO='" + p_sCovgSeqNum + "' and TRANS_SEQ_NO='" + p_sTransSeqNum + "'";
                            sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_sPolUnitRowId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;
                            if (!string.IsNullOrEmpty(p_sCovgSeqNum))
                                sSQLCovg = string.Concat(sSQLCovg, " AND CVG_SEQUENCE_NO = '" + p_sCovgSeqNum + "'");//rkotak:rma 11750 raised because single quotes were mising in the changed query (ripple effect of rma-11452)
                            if (!string.IsNullOrEmpty(p_sTransSeqNum))
                                sSQLCovg = string.Concat(sSQLCovg, " AND TRANS_SEQ_NO = '" + p_sTransSeqNum + "'");//rkotak:rma 11750 raised because single quotes were mising in the changed query (ripple effect of rma-11452)
                            if (!string.IsNullOrEmpty(p_sCoverageKey))
                                sSQLCovg = string.Concat(sSQLCovg, " AND COVERAGE_KEY = '" + p_sCoverageKey + "'");//rkotak:rma 11750 raised because single quotes were mising in the changed query (ripple effect of rma-11452)
                            ////Ankit Start : Worked on MITS - 34297
                            //if (string.IsNullOrEmpty(p_sCoverageKey))
                            //    sSQLCovg = string.Concat(sSQLCovg, " AND COVERAGE_KEY IS NULL ");
                            //else
                            //    sSQLCovg = string.Concat(sSQLCovg, " AND COVERAGE_KEY='", p_sCoverageKey, "'");
                            ////Ankit End
                            //END : RMA-11452
                        }
                        else
                        {
                            //string sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_ID=" + p_sPolicyId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;
                            sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_sPolUnitRowId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;
                        }
                        //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, sSQLCovg))
                        //{
                        //    if (objRdr != null)
                        //    {
                        //        if (objRdr.Read())
                        //        {
                        //            lPolCvgRoWId = Conversion.ConvertObjToInt64(objRdr["POLCVG_ROW_ID"]);
                        //        }
                        //    }
                        //}

                        lPolCvgRoWId = objConn.ExecuteInt(sSQLCovg);
                        //rupal:end,policy system interface
                        sSQL.Append(" AND C2.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE FROM RESERVE_CURRENT ");
                        sSQL.Append(" INNER JOIN COVERAGE_X_LOSS ON COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = RESERVE_CURRENT.POLCVG_LOSS_ROW_ID ");
                        sSQL.Append(" WHERE CLAIM_ID = " + p_sSessionClaimId + " AND CLAIMANT_EID = " + p_sClaimantEid);
                        sSQL.Append("AND COVERAGE_X_LOSS.LOSS_CODE = " + p_sLossTypeCode + " AND COVERAGE_X_LOSS.POLCVG_ROW_ID = " + lPolCvgRoWId + ")");
                    }
                    //rupal:end for firt & final payment

                    //Mits 13681 :Asif Start
                    //below code added for filtering transtype while making payments.this will show only  
                    //those codes in payment which are allowed for that perticular department
                    if (p_lDeptEId > 0)
                    {
                        sSQL.Append("	AND (CODES.ORG_GROUP_EID=" + p_lDeptEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lFacEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lLocEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lDivEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lRegEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lOprEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lComEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lCliEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID IS NULL ");
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                    }

                    //trans_type is also dependent upon currentdate,event date, claim date and policy transaction date, policy effective date
                    // for current mits transaction types need to be filtered on system date
                    EvaluateTriggerDates(sSQL, p_sFormName, sToday, p_sTriggerDate, p_sEventDate, p_sSessionClaimId, objConn, objSysSetting, p_sLOB);
                    //MGaba2-MITS 11517:Start
                    if (sSQL.ToString().Substring(sSQL.Length - 2, 1) == ")")
                    {
                        sSQL.Remove(sSQL.Length - 2, 1);
                    }
                    //Adding trigger for event,claim and policy date in case of trans_type
                    //Raman Bhatia 05/10/2010 : MITS 20703
                    // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                    //if (p_sSessionClaimId == "0" || p_sSessionClaimId.Trim() == "")
                    if (iSessionClaimId <= 0 || string.IsNullOrEmpty(p_sSessionClaimId) || p_sSessionClaimId.ToLower().Trim() == "undefined")
                    // npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                    {
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT')");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM')");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE'))");
                    }
                    else
                    {
                        String sDateOfClaim = string.Empty;
                        String sDateOfEvent = string.Empty;
                        String sDateOfPolicy = string.Empty;
                        GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy, objConn, objSysSetting,p_sLOB);

                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + sDateOfEvent + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfEvent + "') ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + sDateOfEvent + "' AND CODES.EFF_END_DATE>='" + sDateOfEvent + "') ");

                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + sDateOfClaim + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfClaim + "') ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + sDateOfClaim + "' AND CODES.EFF_END_DATE>='" + sDateOfClaim + "') ");

                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "'))");

                    }
                    //MGaba2-MITS 11517:End
                    if (string.IsNullOrEmpty(sParentCodeID) && !(string.IsNullOrEmpty(p_sRcRowId)))
                    {
                        sParentCodeID = GetResTypeCodeId(p_sRcRowId);
                    }

                    //if (p_sTableName.ToLower().Equals("trans_types") && (p_sFormName.Equals("split", StringComparison.InvariantCultureIgnoreCase) || p_sFormName.Equals("thirdpartypayments", StringComparison.InvariantCultureIgnoreCase) || p_sFormName.Equals("futurepayments", StringComparison.InvariantCultureIgnoreCase)))
                    //{
                    //    if (sRsvStatusParent == "C")
                    //    {
                    //        if ((sParentCodeID != "") && (sParentCodeID != null) && (sParentCodeID != "0"))
                    //            sSQL.Append(" AND CODES.RELATED_CODE_ID= " + sParentCodeID);//--stara Mits 16667 05/18/09
                    //    }
                    //}
                    //else
                    //{
                        if ((sParentCodeID != "") && (sParentCodeID != null) && (sParentCodeID != "0"))
                            sSQL.Append(" AND CODES.RELATED_CODE_ID= " + sParentCodeID);//--stara Mits 16667 05/18/09
                    //start:Added by nitin goel,NI, MITS 30910
                    //Changed by Nikhil.Code review changes
                   // if (p_sFilter != "")
                        if (!String.IsNullOrEmpty(p_sFilter))
                    {
                        //Deb: Pen Testing
                        if (p_sFilter.ToUpper().IndexOf("IN") > -1)
                        {
                            int index = p_sFilter.LastIndexOf("IN");
                            string sTemp = p_sFilter.Substring(index + 2);
                            //Deb MITS 27684
                            if (!(sTemp.IndexOf("(") > -1 && sTemp.IndexOf(")") > -1))
                                p_sFilter = p_sFilter.Replace(sTemp, "(" + sTemp + ")");
                            sSQL.Append(" AND " + p_sFilter);
                        }
                        //Deb : Pen Testing
                        else
                            sSQL.Append(" AND " + p_sFilter);
                    }
                    //End:Added by Nitin goel
                    sSQL.Append(" ORDER BY CODES.SHORT_CODE");
                }
                    //added by rkaur7
                else if (p_sTableName.ToLower().Equals("reserve_type"))
                {
                    //asharma326 JIRA 870 starts
                    bool bExcludeRecovery = false;
                    if ((!string.IsNullOrEmpty(p_sFilter)) && (p_sFilter.Equals("LOBRSVMLTCD") || (p_sFilter.Equals("LOBINCRMLTCD"))))
                    {
                        bExcludeRecovery = true;
                        p_sFilter = "";
                    }
                    this.CodesTable = true; //Akman ML Change
                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,CODES.SHORT_CODE");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES ");
                    //asharma326 JIRA 870 
                    if (bExcludeRecovery)
                        sSQL.Append(" , CODES CODES_MASTER_RES_TYPE ");

                    sSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" + p_sTableName.ToUpper() + "' ");
                    //Asharma326 JIRA 857 Start
                    // npadhy RMA-6193 - In case the Funds is not associated with any claim, the LOB will be 0.
                    // In that case objColLobSettings[iLOB].PreventCollectionAllReserve crashes. This this check shall only be done
                    // when funds record is associated with claim
                    if ((!string.IsNullOrEmpty(p_sFilter)) && p_sFilter == "Split_Trans_Type")
                    {
                        if (iLOB > 0)
                        {
                            //objColLobSettings = new ColLobSettings(m_sConnectionString);
                            //iLOB = Conversion.ConvertStrToInteger(p_sSessionLOB);
                            if (objColLobSettings[iLOB].PreventCollectionAllReserve == -1)
                            {
                                //only recovery reserve types
                                sSQL.Append(" AND CODES.CODE_ID IN (SELECT CODE_ID FROM CODES WHERE CODES.RELATED_CODE_ID IN (SELECT CODE_ID FROM CODES_TEXT WHERE SHORT_CODE = 'R')) ");
                            }
                            else if (objColLobSettings[iLOB].PreventCollectionPerReserve == -1)
                            {
                                sSQL.Append(" AND CODES.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sSessionLOB + ")");
                                sSQL.Append(" AND CODES.CODE_ID NOT IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_NOT_ALLOWED WHERE LINE_OF_BUS_CODE = " + p_sSessionLOB + ")");
                            }
                        }
                        p_sFilter = "";
                    }

                    //Asharma326 JIRA 857 Ends
                    if (p_sSessionLOB.Equals(string.Empty) || p_sSessionLOB.Equals("0"))
                        sSQL.Append(" ");
                    else
                    {
                        string sTableID = string.Empty;
                        string sPolSql = string.Empty;
                        string sClaimChkSql = string.Empty;
                        string sGetReserveExternalSql = string.Empty;
                        int iIncludeClaimType = Int32.MinValue;
                        string sPolicySystemCd = string.Empty;

                        // if carrier claim is on,  for all cases 
                        if (objSysSetting.MultiCovgPerClm != 0)
                        {
                            if (objCache == null)
                                objCache = new LocalCache(m_sConnectionString,m_iClientId);
                            sTableID = objCache.GetTableId("RESERVE_TYPE").ToString();

                            sPolSql = "SELECT POLICY.POLICY_SYSTEM_ID, POLICY_X_WEB.POLICY_SYSTEM_CODE FROM POLICY, POLICY_X_WEB";
                            sPolSql = sPolSql + " WHERE POLICY_ID=" + p_sPolicyId + " AND POLICY_X_WEB.POLICY_SYSTEM_ID=POLICY.POLICY_SYSTEM_ID";

                            using (DbReader objRdr = objConn.ExecuteReader(sPolSql))
                            {
                                if (objRdr != null)
                                {
                                    if (objRdr.Read())
                                    {
                                        iPolSystemId = Conversion.ConvertObjToInt(objRdr["POLICY_SYSTEM_ID"], m_iClientId);
                                        sPolicySystemCd = Conversion.ConvertObjToStr(objRdr["POLICY_SYSTEM_CODE"]);
                                        objRdr.Close();
                                    }
                                }
                            }

                            //check if we have external policy, i.e. policy system id >0
                            if (iPolSystemId > 0)
                            {
                                sClaimChkSql = "SELECT INCLUDE_CLAIM_TYPE FROM PS_MAP_TABLES WHERE RMX_TABLE_ID = " + sTableID+" AND POLICY_SYSTEM_TYPE_ID ="+sPolicySystemCd;
                                //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, sClaimChkSql))
                                //{
                                //    if (objRdr != null)
                                //    {
                                //        if (objRdr.Read())
                                //        {
                                //            iIncludeClaimType = Conversion.ConvertObjToInt(objRdr["INCLUDE_CLAIM_TYPE"], m_iClientId);
                                //            objRdr.Close();
                                //        }
                                //    }
                                //}
                                iIncludeClaimType = objConn.ExecuteInt(sClaimChkSql);
                            }
                        }
                        
                        
                        // 06/18/2007 MITS : 9499   by Umesh
                        objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                        iLOB = Conversion.ConvertStrToInteger(p_sSessionLOB);
                        
                        // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                        //iClaimId = Conversion.ConvertStrToInteger(p_sSessionClaimId);
                        if (objColLobSettings[iLOB].ResByClmType == true && iSessionClaimId > 0 && (!string.IsNullOrEmpty(p_sSessionClaimId)))
                        {
                            //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString,
                            //  "SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iSessionClaimId))
                            //// npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            //{
                            //    if (objRdr.Read())
                            //    {
                                    int iClaimTypeCode = 0;
                                    iClaimTypeCode = objConn.ExecuteInt("SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iSessionClaimId);
                                    sSQL.Append(" AND CODES.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE=" + p_sSessionLOB + " AND CLAIM_TYPE_CODE =" + iClaimTypeCode + ")");
                            //    }

                            //    objRdr.Close();
                            //}

                        } // END MITS 9499
                        else
                        {
                            sSQL.Append(" AND CODES.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sSessionLOB + ")");
                            /*
                            if (!(string.Compare(p_sPolicyId, "0") == 0) && !(string.IsNullOrEmpty(p_sPolicyId)))
                            {
                                //rupal:start, for first & final payment
                                //if condition added by rupal
                                //following clause in SQL should be added only if payment is not first & final payment
                                //because in case of first & final payment, we may not have any reserves in reserve_current                                
                                //if (objSysSetting.MultiCovgPerClm == 0 && bIsFirstFinal == false) //if carrier claim is off
                                //rupal:mits 27617
                                if (objSysSetting.MultiCovgPerClm != 0 && bIsFirstFinal == false) //if carrier claim is on and first&final=false
                                {
                                    //sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(RESERVE_TYPE_CODE) FROM POLICY_X_CVG_TYPE,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND POLICY_X_CVG_TYPE.POLICY_ID =" + p_sPolicyId);
                                    //sSQL.Append(" AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + "AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =" + p_sCovTypecode + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");
            
                                    sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(RESERVE_TYPE_CODE) FROM POLICY_X_CVG_TYPE,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID =" + p_sPolUnitRowId);
                                    sSQL.Append(" AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + "AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =" + p_sCovTypecode + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");
                                }
                                //rupal:end, for first & final payment
                               
                            }
                             * */
                        }

                        if (objColLobSettings[iLOB].ResByClmType == false && iIncludeClaimType == 1 && iSessionClaimId > 0 && (!string.IsNullOrEmpty(p_sSessionClaimId)))
                        {
                            //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString,
                            // "SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iSessionClaimId))
                            //{
                            //    if (objRdr.Read())
                            //    {
                                    int iClaimTypeCode = 0;
                                    iClaimTypeCode = objConn.ExecuteInt("SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iSessionClaimId);
                                    sSQL.Append(" AND CODES.CODE_ID IN (SELECT RMX_CODE_ID FROM POLICY_CODE_MAPPING WHERE POLICY_SYSTEM_ID = " + iPolSystemId.ToString() + " AND CLAIM_TYPE_CODE = " + iClaimTypeCode + " AND RMX_TABLE_ID = " + sTableID + ")");
                            //    }
                            //}
                        }
                        //rupal:start
                        //mits 27617
                        if (!(string.Compare(p_sPolicyId, "0") == 0) && !(string.IsNullOrEmpty(p_sPolicyId)))
                        {
                            //rupal:start, for first & final payment
                            //if condition added by rupal
                            //following clause in SQL should be added only if payment is not first & final payment
                            //because in case of first & final payment, we may not have any reserves in reserve_current                                
                            //if (objSysSetting.MultiCovgPerClm == 0 && bIsFirstFinal == false) //if carrier claim is off
                            //rupal:mits 27617
                            if (objSysSetting.MultiCovgPerClm != 0 && bIsFirstFinal == false) //if carrier claim is on and first&final=false
                            {
                                //sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(RESERVE_TYPE_CODE) FROM POLICY_X_CVG_TYPE,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND POLICY_X_CVG_TYPE.POLICY_ID =" + p_sPolicyId);
                                //sSQL.Append(" AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + "AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =" + p_sCovTypecode + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");
                                //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                                sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(RESERVE_TYPE_CODE) ");
                                sSQL.Append(" FROM RESERVE_CURRENT ");
                                sSQL.Append(" INNER JOIN COVERAGE_X_LOSS ON COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = RESERVE_CURRENT.POLCVG_LOSS_ROW_ID ");
                                sSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON COVERAGE_X_LOSS.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID ");
                                sSQL.Append(" WHERE POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID =" + p_sPolUnitRowId);
                                sSQL.Append(" AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId);
                                sSQL.Append(" AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =" + p_sCovTypecode);
                                sSQL.Append(" AND COVERAGE_X_LOSS.LOSS_CODE = " + p_sLossTypeCode);
                                sSQL.Append(" AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");
                                //Ankit End
                            }
                            //rupal:end, for first & final payment
                        }
                        //rupal:end
                    }

                  
                    //rupal:start for first & final payment                   
                   //in case of first & final payment,
                   //we donn't want certain reservetype to be populated if any kind of payment is made for that reserve type                    
                    if (objSysSetting.MultiCovgPerClm != 0) //zero for false
                    {
                        if (bIsFirstFinal)
                        {
                            string sPolSql = "SELECT POLICY_SYSTEM_ID FROM POLICY WHERE POLICY_ID=" + p_sPolicyId;

                            //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, sPolSql))
                            //{
                            //    if (objRdr != null)
                            //    {
                            //        if (objRdr.Read())
                            //        {
                            //            iPolSystemId = Conversion.ConvertObjToInt(objRdr["POLICY_SYSTEM_ID"], m_iClientId);
                            //            objRdr.Close();
                            //        }
                            //    }
                            //}
                            iPolSystemId = objConn.ExecuteInt(sPolSql);
                            string sSQLCovg = string.Empty;
                            if (iPolSystemId > 0)
                            {
                                //string sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_ID=" + p_sPolicyId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;
                                //Start : RMA-11452 - rkaur27 
                                //sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_sPolUnitRowId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode + " and CVG_SEQUENCE_NO='" + p_sCovgSeqNum + "' and TRANS_SEQ_NO='" + p_sTransSeqNum + "'";
                                sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_sPolUnitRowId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;
                                if (!string.IsNullOrEmpty(p_sCovgSeqNum))
                                    sSQLCovg = string.Concat(sSQLCovg, " AND CVG_SEQUENCE_NO = '" + p_sCovgSeqNum + "'");//rkotak:rma 11750 raised because single quotes were mising in the changed query (ripple effect of rma-11452)
                                if (!string.IsNullOrEmpty(p_sTransSeqNum))
                                    sSQLCovg = string.Concat(sSQLCovg, " AND TRANS_SEQ_NO = '" + p_sTransSeqNum + "'");//rkotak:rma 11750 raised because single quotes were mising in the changed query (ripple effect of rma-11452)
                                if (!string.IsNullOrEmpty(p_sCoverageKey))
                                    sSQLCovg = string.Concat(sSQLCovg, " AND COVERAGE_KEY = '" + p_sCoverageKey + "'");//rkotak:rma 11750 raised because single quotes were mising in the changed query (ripple effect of rma-11452)

                                 ////Ankit Start : Worked on MITS - 34297
                                //if (string.IsNullOrEmpty(p_sCoverageKey))
                                //    sSQLCovg = string.Concat(sSQLCovg, " AND COVERAGE_KEY IS NULL ");
                                //else
                                //    sSQLCovg = string.Concat(sSQLCovg, " AND COVERAGE_KEY='", p_sCoverageKey, "'");
                                ////Ankit End
                                //End: RMA-11452
                            }
                            else
                            {                               
                                //string sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_ID=" + p_sPolicyId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;
                                sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_sPolUnitRowId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;                             
                            }
                            //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, sSQLCovg))
                            //{
                            //    if (objRdr != null)
                            //    {
                            //        if (objRdr.Read())
                            //        {
                            //            lPolCvgRoWId = Conversion.ConvertObjToInt64(objRdr["POLCVG_ROW_ID"]);
                            //        }
                            //    }
                            //}
                            lPolCvgRoWId = objConn.ExecuteInt(sSQLCovg);
                            sSQL.Append(" AND CODES.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE FROM RESERVE_CURRENT ");
                            sSQL.Append(" INNER JOIN COVERAGE_X_LOSS ON COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = RESERVE_CURRENT.POLCVG_LOSS_ROW_ID ");
                            sSQL.Append(" WHERE CLAIM_ID = " + p_sSessionClaimId + " AND CLAIMANT_EID = " + p_sClaimantEid);
                            sSQL.Append("AND COVERAGE_X_LOSS.LOSS_CODE = " + p_sLossTypeCode + " AND COVERAGE_X_LOSS.POLCVG_ROW_ID = " + lPolCvgRoWId + ")");
                        }
                    }

                    //rupal:end for firt & final payment
                    

                    sSQL.Append(" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ");
                    sSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ");
                    //asharma326 JIRA 870 
                    if (bExcludeRecovery)
                        sSQL.Append(" AND CODES.RELATED_CODE_ID = CODES_MASTER_RES_TYPE.CODE_ID AND CODES_MASTER_RES_TYPE.SHORT_CODE <> 'R' ");
                    //JIRA-857 Starts
                    if (!string.IsNullOrEmpty(p_sFilter.Trim()) && (p_sFilter == "LOBSetUp_lstRsvColl"))
                    {
                        sSQL.Append(" AND RELATED_CODE_ID != (SELECT CODES.CODE_ID FROM CODES_TEXT INNER JOIN CODES ON CODES.CODE_ID = CODES_TEXT.CODE_ID WHERE CODES_TEXT.SHORT_CODE = 'R' AND CODES.TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'MASTER_RESERVE'))");
                        p_sFilter = "";
                    }
                    //JIRA-857 Ends
                    //start - Changed by Nikhil on 11/28/2014
                   // if (p_sLOB.Trim() != "")
                    if (Conversion.ConvertStrToLong(p_sLOB) > 0)
                    //End - Changed by Nikhil on 11/28/2014
                    {
                        sSQL.Append(" AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) ");
                    }
                    //Mits 13681 :Asif Start
                    //below code added for filtering transtype while making payments.this will show only  
                    //those codes in payment which are allowed for that perticular department
                    if (p_lDeptEId > 0)
                    {
                        sSQL.Append("	AND (CODES.ORG_GROUP_EID=" + p_lDeptEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lFacEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lLocEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lDivEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lRegEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lOprEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lComEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lCliEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID IS NULL ");
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                    }

                    //trans_type is also dependent upon currentdate,event date, claim date and policy transaction date, policy effective date
                    // for current mits transaction types need to be filtered on system date
                    EvaluateTriggerDates(sSQL, p_sFormName, sToday, p_sTriggerDate, p_sEventDate, p_sSessionClaimId, objConn, objSysSetting,p_sLOB);
                    //MGaba2-MITS 11517:Start
                    if (sSQL.ToString().Substring(sSQL.Length - 2, 1) == ")")
                    {
                        sSQL.Remove(sSQL.Length - 2, 1);
                    }
                    //Adding trigger for event,claim and policy date in case of trans_type
                    // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                    if (iSessionClaimId <= 0 || string.IsNullOrEmpty(p_sSessionClaimId))
                    // npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                    {
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT')");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM')");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE'))");
                    }
                    else
                    {
                        String sDateOfClaim = string.Empty;
                        String sDateOfEvent = string.Empty;
                        String sDateOfPolicy = string.Empty;
                        GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy, objConn, objSysSetting, p_sLOB);

                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + sDateOfEvent + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfEvent + "') ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + sDateOfEvent + "' AND CODES.EFF_END_DATE>='" + sDateOfEvent + "') ");

                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + sDateOfClaim + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfClaim + "') ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + sDateOfClaim + "' AND CODES.EFF_END_DATE>='" + sDateOfClaim + "') ");

                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "'))");

                    }
                    //MGaba2-MITS 11517:End
                    //if ((sParentCodeID != "") && (sParentCodeID != null) && (sParentCodeID != "0"))
                    //    sSQL.Append(" AND CODES.RELATED_CODE_ID= " + sParentCodeID);//--stara Mits 16667 05/18/09
                    //Start:Added by Nitin Goel, MITS 30910,01/25/2013 ,For National Intersate.
                   //Changed by Nikhil.Code review changes
                    //if (p_sFilter != "")
                    if (!String.IsNullOrEmpty(p_sFilter))
                    {
                        //Deb: Pen Testing
                        if (p_sFilter.ToUpper().IndexOf("IN") > -1)
                        {
                            int index = p_sFilter.LastIndexOf("IN");
                            string sTemp = p_sFilter.Substring(index + 2);
                            //Deb MITS 27684
                            if (!(sTemp.IndexOf("(") > -1 && sTemp.IndexOf(")") > -1))
                                p_sFilter = p_sFilter.Replace(sTemp, "(" + sTemp + ")");
                            sSQL.Append(" AND " + p_sFilter);
                        }
                        //Deb : Pen Testing
                        else
                            sSQL.Append(" AND " + p_sFilter);
                    }
                    //End:added by Nitin goel
                    sSQL.Append(" ORDER BY CODES.SHORT_CODE");
                }
                    //end rkaur7
               
                else if ((p_sTableName.Length > 8) && (p_sTableName.ToLower().Substring(0, 9).Equals("glossary_")))
                {
                    sSQL.Append(" SELECT GLOSSARY.TABLE_ID, GLOSSARY.SYSTEM_TABLE_NAME, GLOSSARY_TEXT.TABLE_NAME, NULL");
                    sSQL.Append(" FROM GLOSSARY, GLOSSARY_TEXT");
                    sSQL.Append(" WHERE GLOSSARY.TABLE_ID=GLOSSARY_TEXT.TABLE_ID");
                    //sSQL.Append(" AND GLOSSARY_TEXT.LANGUAGE_CODE=1033"); //Aman ML Change
                    sSQL.Append(" AND GLOSSARY.GLOSSARY_TYPE_CODE=" + p_sTableName.Substring(9));
                }
                else if (p_sTableName.ToLower().Equals("entity")) //25/12/2007 Abhishek, MITS 11097 - START
                {
                    // ijha: Mobile adjuster : 09/14/2011 
                    if (bIsMobileAdjuster)
                    {
                        sSQL.Append("SELECT TABLE_ID, SYSTEM_TABLE_NAME FROM GLOSSARY WHERE ");
                        sSQL.Append("GLOSSARY_TYPE_CODE =7 ORDER BY SYSTEM_TABLE_NAME");
                    }
                    else
                    {
                        //JIRA RMA-4690: neha goel:05202014 added if else so that PI_SUPP works both for Entity type table and Code tables MITS # 35595
                        if (string.Compare(p_sFormName, "TableField", true) == 0)
                        {
                            sSQL.Append("SELECT TABLE_ID, SYSTEM_TABLE_NAME FROM GLOSSARY WHERE GLOSSARY_TYPE_CODE =7 ");
                            sSQL.Append("ORDER BY SYSTEM_TABLE_NAME");
                        }
                        //JIRA RMA-4690: neha goel:05202014 added if else so that PI_SUPP works both for Entity type table and Code tables MITS # 35595
                        else
                        {
                            sSQL.Append("SELECT TABLE_ID, SYSTEM_TABLE_NAME FROM GLOSSARY WHERE GLOSSARY_TYPE_CODE =4 ");
                            sSQL.Append("or GLOSSARY_TYPE_CODE =5 or GLOSSARY_TYPE_CODE =7 ORDER BY SYSTEM_TABLE_NAME");
                        }
                    }
                } //25/12/2007 Abhishek, MITS 11097 - END
                else
                {
                    this.CodesTable = true;
                    if (Conversion.ConvertStrToLong(p_sLOB) > 0)
                        sLOBSQL = " AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) ";
                    else
                        sLOBSQL = string.Empty;

                   if (!string.IsNullOrEmpty(p_sPolicyId) && !string.Equals(p_sPolicyId,"0"))
                    {
                     
                    //rupal:start,policy system interafce
                    string sSQLPolicy = "SELECT POLICY_SYSTEM_ID FROM POLICY WHERE POLICY_ID=" + p_sPolicyId;
                    
                    if (string.Compare(p_sTableName, "COVERAGE_TYPE", true) == 0)
                    {
                        //using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLPolicy))
                        //{
                        //    if (objReader.Read())
                        //    {
                        //        iPolSystemId = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReader.GetValue(0)), out bSuccess);
                        //        objReader.Close();
                        //    }
                        //}
                        iPolSystemId = objConn.ExecuteInt(sSQLPolicy);
                        //tanwar2 - Policy Download from Staging - start
                        //if (iPolSystemId > 0 && objSysSetting.MultiCovgPerClm != 0 && string.Compare(p_sTableName, "COVERAGE_TYPE", true) == 0)
                        if ((iPolSystemId > 0) && objSysSetting.MultiCovgPerClm != 0 && string.Compare(p_sTableName, "COVERAGE_TYPE", true) == 0)
                        //tanwar2 - Policy Download from staging - end
                        {
                                //sSQL.Append(" SELECT DISTINCT A.COVERAGE_TYPE_CODE CODE_ID,CODES.SHORT_CODE,");
                                //sSQL.Append(" CASE(CNT)");
                                //sSQL.Append(" WHEN 1 THEN A.CODE_DESC ");
                                //sSQL.Append(" ELSE");
                                //if (DbFactory.IsOracleDatabase(m_sConnectionString))
                                //{
                                //    sSQL.Append(" CASE(NVL(COVERAGE_TEXT,''))");
                                //}


                                sSQL.Append(" SELECT DISTINCT A.COVERAGE_TYPE_CODE CODE_ID,CODES.SHORT_CODE,");

                               
                                sSQL.Append(" COVERAGE_TEXT CODE_DESC, C2.SHORT_CODE,CVG_SEQUENCE_NO, TRANS_SEQ_NO, COVERAGE_KEY");     //Ankit Start : Worked on MITS - 34297
                               
                            sSQL.Append(" FROM ");
                            sSQL.Append(" ( ");
                            sSQL.Append(" SELECT C.POLCVG_ROW_ID, CODE_DESC, COVERAGE_TEXT, SHORT_CODE,C.COVERAGE_TYPE_CODE ,CVG_SEQUENCE_NO,B.CNT,TRANS_SEQ_NO, COVERAGE_KEY FROM ");      //Ankit Start : Worked on MITS - 34297
                            sSQL.Append(" ( ");
                            sSQL.Append(" SELECT COVERAGE_TYPE_CODE,COUNT(*) CNT FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_sPolUnitRowId + " GROUP BY COVERAGE_TYPE_CODE");
                            sSQL.Append(" )B,");
                            sSQL.Append(" (");
                            sSQL.Append(" SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID, CODE_DESC, COVERAGE_TEXT, SHORT_CODE,COVERAGE_TYPE_CODE ,CVG_SEQUENCE_NO, TRANS_SEQ_NO, COVERAGE_KEY");       //Ankit Start : Worked on MITS - 34297
                            sSQL.Append(" FROM POLICY_X_CVG_TYPE");
                            if (!bIsFirstFinal)
                            {
                                sSQL.Append(" ,RESERVE_CURRENT");
                            }
                            sSQL.Append(" ,CODES_TEXT");
                            sSQL.Append(" WHERE");
                        }
                        else
                        {
                            sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,C2.SHORT_CODE");
                            sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID");
                            sSQL.Append(" WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME) = '" + p_sTableName.ToUpper() + "' ");
                        }
                    }
                    else
                    {
                        sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,C2.SHORT_CODE");
                        sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID");
                        sSQL.Append(" WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME) = '" + p_sTableName.ToUpper() + "' ");
                    }
                    }
                   else
                   {
                       sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,C2.SHORT_CODE");
                       sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID");
                       sSQL.Append(" WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME) = '" + p_sTableName.ToUpper() + "' ");
					   //added by swati for AIC Gap 7 MITS # 36929
                        string sTempSql = string.Empty;
                        int iRecCount = 0;
                        if (!string.IsNullOrEmpty(p_sJurisdiction) && (string.Compare(p_sJurisdiction, "0") != 0) && string.Equals(p_sTableName, "AIA_CODE_12", StringComparison.InvariantCultureIgnoreCase) && (string.Equals(p_sFormName, "claimgc", StringComparison.InvariantCultureIgnoreCase) || string.Equals(p_sFormName, "claimwc", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            sTempSql = "SELECT CODE2 FROM CODE_X_CODE WHERE CODE1 = " + p_sJurisdiction + " AND REL_TYPE_CODE = " + objCache.GetCodeId("JDTOAIA12", "CODE_REL_TYPE").ToString() + " AND DELETED_FLAG = 0";
                            iRecCount = objConn.ExecuteInt(sTempSql);
                            if (iRecCount > 0)
                            {
                                sSQL.Append(" AND CODES.CODE_ID IN (SELECT CODE2 FROM CODE_X_CODE WHERE CODE1 = " + p_sJurisdiction + " AND REL_TYPE_CODE = " + objCache.GetCodeId("JDTOAIA12", "CODE_REL_TYPE").ToString() + " AND DELETED_FLAG = 0)");
                            }
                        }
                        else if (!string.IsNullOrEmpty(p_sJurisdiction) && (string.Compare(p_sJurisdiction, "0") != 0) && string.Equals(p_sTableName, "AIA_CODE_34", StringComparison.InvariantCultureIgnoreCase) && (string.Equals(p_sFormName, "claimgc", StringComparison.InvariantCultureIgnoreCase) || string.Equals(p_sFormName, "claimwc", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            sTempSql = "SELECT CODE2 FROM CODE_X_CODE WHERE CODE1 = " + p_sJurisdiction + " AND REL_TYPE_CODE = " + objCache.GetCodeId("JDTOAIA34", "CODE_REL_TYPE").ToString() + " AND DELETED_FLAG = 0";
                            iRecCount = objConn.ExecuteInt(sTempSql);
                            if (iRecCount > 0)
                            {
                                sSQL.Append(" AND CODES.CODE_ID IN (SELECT CODE2 FROM CODE_X_CODE WHERE CODE1 = " + p_sJurisdiction + " AND REL_TYPE_CODE = " + objCache.GetCodeId("JDTOAIA34", "CODE_REL_TYPE").ToString() + " AND DELETED_FLAG = 0)");
                            }
                        }
                        else if (!string.IsNullOrEmpty(p_sJurisdiction) && (string.Compare(p_sJurisdiction, "0") != 0) && string.Equals(p_sTableName, "AIA_CODE_56", StringComparison.InvariantCultureIgnoreCase) && (string.Equals(p_sFormName, "claimgc", StringComparison.InvariantCultureIgnoreCase) || string.Equals(p_sFormName, "claimwc", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            sTempSql = "SELECT CODE2 FROM CODE_X_CODE WHERE CODE1 = " + p_sJurisdiction + " AND REL_TYPE_CODE = " + objCache.GetCodeId("JDTOAIA56", "CODE_REL_TYPE").ToString() + " AND DELETED_FLAG = 0";
                            iRecCount = objConn.ExecuteInt(sTempSql);
                            if (iRecCount > 0)
                            {
                                sSQL.Append(" AND CODES.CODE_ID IN (SELECT CODE2 FROM CODE_X_CODE WHERE CODE1 = " + p_sJurisdiction + " AND REL_TYPE_CODE = " + objCache.GetCodeId("JDTOAIA56", "CODE_REL_TYPE").ToString() + " AND DELETED_FLAG = 0)");
                            }
                        }
                        //change end here by swati
                   }
                    //Parijat:11486 Just to fetch the code values when code table is in lower case in case of oracle.
                    // 01/08/07 REM UMESH
                    if (string.Compare(p_sTableName, "COVERAGE_TYPE", true) == 0)
                    {
					if (!p_sFieldName.Contains("supp")) //igupta3 for suppliment fields no policy checks req: MITS 36575 
                        {
                        //srajindersin MITS 34896 dt 1/9/2014
                        if (!(iSessionClaimId == 0 && lClaimantEid == 0))
                        {
                            //rupal:start for first & final payment
                            //if IsFirstFinal is set to true, then we assume that reserves are not already created, 
                            //we will create the reserve on the fly. therefore coverage type can not be found in reserve current table
                            // if IsFirstFinal is true then coverage type will be fetched directly from policy
                            if (bIsFirstFinal)
                            {
                                if (iPolSystemId > 0 && objSysSetting.MultiCovgPerClm != 0)
                                {
                                    sSQL.Append(" POLICY_UNIT_ROW_ID =" + p_sPolUnitRowId + " AND CODES_TEXT.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE");                                    
                                }
                                else
                                {
                                    sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(COVERAGE_TYPE_CODE) FROM POLICY_X_CVG_TYPE WHERE ");
                                    //sSQL.Append(" POLICY_X_CVG_TYPE.POLICY_ID =" + p_sPolicyId + ")");
                                    sSQL.Append(" POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID =" + p_sPolUnitRowId + ")");
                                }
                            }
                            //rupal :end for first & final payment
                            else
                            {
                                //RUPAL:START, POLICY SYSTEM INTERFACE
                                //tanwar2 - Policy Download from staging - start
                                //if (iPolSystemId > 0 && objSysSetting.MultiCovgPerClm != 0)
                                if ((iPolSystemId > 0) && objSysSetting.MultiCovgPerClm != 0)
                                //tanwar2 - Policy Download from staging - end
                                {
                                    sSQL.Append(" POLICY_UNIT_ROW_ID =" + p_sPolUnitRowId + " AND CODES_TEXT.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE");
                                    sSQL.Append(" AND POLICY_X_CVG_TYPE.POLCVG_ROW_ID = RESERVE_CURRENT.POLCVG_ROW_ID AND RESERVE_CURRENT.CLAIM_ID = " + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID = " + lClaimantEid);                                    
                                }
                                //RUPAL:END, POLICY SYSTEM INTERFACE 
                                else
                                {                                    
                                    //commented by rupal as Reserve_Current Table does not have Policy_ID as per new changes
                                    //sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(COVERAGE_TYPE_CODE) FROM POLICY_X_CVG_TYPE,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID");
                                    //sSQL.Append(" AND RESERVE_CURRENT.POLICY_ID =" + p_sPolicyId + " AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");

                                    //rupal
                                    sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(COVERAGE_TYPE_CODE) FROM POLICY_X_CVG_TYPE,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID");
                                    //Raman : Sep 14 2011: Unit changes for R8
                                    //sSQL.Append(" AND POLICY_X_CVG_TYPE.POLICY_ID =" + p_sPolicyId + " AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");

                                    //Indu Adding for supplemental issue
                                    if (p_sPolUnitRowId != string.Empty)
                                    {
                                        sSQL.Append(" AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID =" + p_sPolUnitRowId + " AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");
                                    }
                                    else
                                        sSQL.Append(")");
                                    
									}
                                }
                            }
                           
                        }
                        //RUPAL:START,POLICY SYSTEM INTERFACE
                        //tanwar2 - Policy Download from staging - start
                        //if (iPolSystemId > 0 && objSysSetting.MultiCovgPerClm != 0)
                        if ((iPolSystemId > 0) && objSysSetting.MultiCovgPerClm != 0)
                        //tanwar2 - Policy Download from staging - end
                        {
                            sSQL.Append(" ) C WHERE B.COVERAGE_TYPE_CODE = C.COVERAGE_TYPE_CODE");
                            sSQL.Append(" )A ");
                            sSQL.Append(" ,CODES_TEXT, GLOSSARY");
                            sSQL.Append(" ,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID");
                            sSQL.Append(" WHERE ");
                            sSQL.Append(" A.COVERAGE_TYPE_CODE=CODES.CODE_ID AND A.COVERAGE_TYPE_CODE=CODES_TEXT.CODE_ID");
                            sSQL.Append(" AND UPPER(GLOSSARY.SYSTEM_TABLE_NAME) ='" + p_sTableName + "'");                            
                        }
                        //RUPAL:END,POLICY SYSTEM INTERFACE
                    }  //rupal:end,policy system interafce
                    if ((p_sFormName == "adjusterdatedtext") && (p_sTableName == "ADJ_TEXT_TYPE"))
                    {
                        objXmlDocument = new XmlDocument();
                        string strContent = RMSessionManager.GetCustomContent(m_iClientId);
                        if (! string.IsNullOrEmpty(strContent))
                            {
                                objXmlDocument.LoadXml(strContent);
                                //objRdr.Dispose();
                                XmlNode objSearch = objXmlDocument.SelectSingleNode("//RMAdminSettings/SpecialSettings/text");
                                if (objSearch != null && objSearch.InnerText != "")
                                {
                                    sAdjDTTFilter = objSearch.InnerText;
                                    sarrFilter = sAdjDTTFilter.Split(',');
                                    if (sarrFilter.Length > 0)
                                    {
                                        sSQL.Append(" AND CODES.SHORT_CODE IN (");
                                        for (int f = 0; f < sarrFilter.Length; f++)
                                        {
                                            sSQL.Append(String.Format("'{0}',", sarrFilter[f].Trim().ToUpper()));
                                        }
                                        sSQL.Remove(sSQL.Length - 1, 1);
                                        sSQL.Append(")");
                                    }
                                }
                            }
                    }
                    else if ((p_sTableName.ToUpper().Equals("DISTRIBUTION_TYPE")) && (!string.IsNullOrEmpty(p_sFilter)) && p_sFilter.ToLower() == "distributiontype_filter")
                    {
                        //asharma326 for jira 10762
                        sSQL.Append(" AND CODES.SHORT_CODE NOT IN ( 'MAL' ) ");
                        p_sFilter = "";
                    }
                    //REM END
                    sSQL.Append(" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ");
                    sSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ");
                    if (sLOBSQL != string.Empty)
                        sSQL.Append(sLOBSQL);
                    if (p_lDeptEId > 0)
                    {
                        sSQL.Append(" AND (CODES.ORG_GROUP_EID=" + p_lDeptEId);
                        sSQL.Append(" OR CODES.ORG_GROUP_EID=" + lFacEId);
                        sSQL.Append(" OR CODES.ORG_GROUP_EID=" + lLocEId);
                        sSQL.Append(" OR CODES.ORG_GROUP_EID=" + lDivEId);
                        sSQL.Append(" OR CODES.ORG_GROUP_EID=" + lRegEId);
                        sSQL.Append(" OR CODES.ORG_GROUP_EID=" + lOprEId);
                        sSQL.Append(" OR CODES.ORG_GROUP_EID=" + lComEId);
                        sSQL.Append(" OR CODES.ORG_GROUP_EID=" + lCliEId);
						
				        sSQL.Append(" OR CODES.ORG_GROUP_EID IS NULL ");
						
                        //PJS MITS 15220 
                        if (!String.IsNullOrEmpty(p_sInsuredEid) && p_sFormName == "exposure")
                        {
                            if (!String.IsNullOrEmpty(sInsParentEid))
                            {
                                sSQL.Append(" OR CODES.ORG_GROUP_EID IN ( ");
                                sSQL.Append(sInsParentEid);
                                sSQL.Append(")");
                            }
                        }
                        //PJS MITS 15220

						sSQL.Append(" OR CODES.ORG_GROUP_EID=0 ) ");
					}
                    else
                    {
                        //pmittal5  MITS:12318  06/13/08  - All Codes of Query Fields should be displayed in case of "Search"
                        if (p_sTitle != "Search" && p_sTitle != "ExpRateParms" && p_sFormName.ToLower() != "userprivileges")    // Added by csingh7 MITS 15220, MITS 37576, jIRA 6596:aaggarwal29    // Added by csingh7 MITS 15220
                        {
                            sSQL.Append("	AND (CODES.ORG_GROUP_EID IS NULL ");
                            //PJS MITS 15220 
                            if (!String.IsNullOrEmpty(p_sInsuredEid) && p_sFormName == "exposure")
                            {
                                if (!String.IsNullOrEmpty(sInsParentEid))
                                {
                                    sSQL.Append("	OR CODES.ORG_GROUP_EID IN ( ");
                                    sSQL.Append(sInsParentEid);
                                    sSQL.Append(")");
                                }
                            }
                            //PJS MITS 15220
                            sSQL.Append(" OR CODES.ORG_GROUP_EID=0) ");
                        }
                    }

                    switch (p_sFormName.ToLower())
                    {
                        case "":
                            sSQL.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                            break;
                            //sgoel6 Medicare 05/13/2009
                        case "lineofbuscodeselfinsured":
                            sSQL.Append(" AND CODES.SHORT_CODE NOT IN ('DI') ");
                            sSQL.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                            break;
                        case "event":
                            sSQL.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        //pmittal5  MITS:11758  05/30/08
                        case "medwatch":
                        case "fallinfo":
                            sSQL.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            if (iSessionClaimId > 0 && !(string.IsNullOrEmpty(p_sSessionClaimId)))
                            // npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            {
                                String sDateOfClaim = string.Empty;
                                String sDateOfEvent = string.Empty;
                                String sDateOfPolicy = string.Empty;
                                GetEventDate(p_sSessionClaimId, ref sDateOfEvent); 

                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + sDateOfEvent + "' AND CODES.EFF_END_DATE IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sDateOfEvent + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + sDateOfEvent + "' AND CODES.EFF_END_DATE>='" + sDateOfEvent + "') ");

                            }
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "claimgc":
                        case "claimwc":
                        case "claimva":
                        case "claimdi":
                        case "claimpc":
                        //Shruti for 11708
                        case "claimant":
                            sSQL.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            if (p_sEventDate.Length > 0)
                            {
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sEventDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sEventDate + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sEventDate + "' AND CODES.EFF_END_DATE>='" + p_sEventDate + "') ");
                            }
                            //zalam 08/27/2008 Mits:-11708 Start
                            // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            if (iSessionClaimId > 0 && !(string.IsNullOrEmpty(p_sSessionClaimId)))
                            // npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            {
                                String sDateOfClaim = string.Empty;
                                String sDateOfEvent = string.Empty;
                                String sDateOfPolicy = string.Empty;
                                GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy, objConn, objSysSetting, p_sLOB);

                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");

                            }
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "policy":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "funds":
                        //"split.aspx" added 
                        case "split":
							sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            //MGaba2-MITS 11517:Start
							/*sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
							sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
							sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");*/
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
							sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            //Adding trigger for event,claim and policy date in case of GL Accounts
                            // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            if (iSessionClaimId <= 0 || string.IsNullOrEmpty(p_sSessionClaimId))
                            // npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            {
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT')");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM')");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE')");
                            }
                            else
                            {
                                String sDateOfClaim = string.Empty;
                                String sDateOfEvent = string.Empty;
                                String sDateOfPolicy = string.Empty;
                                GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy, objConn, objSysSetting, p_sLOB);

                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + sDateOfEvent + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfEvent + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + sDateOfEvent + "' AND CODES.EFF_END_DATE>='" + sDateOfEvent + "') ");

                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + sDateOfClaim + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfClaim + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + sDateOfClaim + "' AND CODES.EFF_END_DATE>='" + sDateOfClaim + "') ");

                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");

                            }
                            


							/*sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
							sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
							sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");*/
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            
                            //MGaba2-MITS 11517:End
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "employee":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        //rsushilaggar MITS 20309 06/25/2010
                        case "search":
                            //rsharma220 MITS 33923 Start
                            //rsharma220 MITS 34524 : added line of business condition
                            if (objSysSetting.MultiCovgPerClm == -1 && (p_sTableName.ToLower().Equals("line_of_business")))
                            {
                                sSQL.Append(" AND ( CODES.CODE_ID= 241 OR CODES.CODE_ID= 243 )"); 
                            }
                            //rsharma220 MITS 33923 End
                            break;
                        //rsushilaggar MITS 20309 06/25/2010
                        //MITS 15185 : Umesh
                        default:
                            sSQL.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                            break;
                        //MITS 15185 : Umesh

                    }
                    //Custom Filter clause. MCIC Changes 01/24/08
                    if (p_sFilter != "")
                    {
                        //Deb: Pen Testing
                        if (p_sFilter.ToUpper().IndexOf("IN") > -1)
                        {
                            int index = p_sFilter.LastIndexOf("IN");
                            string sTemp = p_sFilter.Substring(index + 2);
                            //Deb MITS 27684
                            if (!(sTemp.IndexOf("(") > -1 && sTemp.IndexOf(")") > -1))
                            p_sFilter = p_sFilter.Replace(sTemp, "(" + sTemp + ")");
                            sSQL.Append(" AND " + p_sFilter);
                        }
                        //Deb : Pen Testing
                        else
                           sSQL.Append(" AND " + p_sFilter);
                    }
                    //Deb Multi Currency
                    if (p_sTableName == "CURRENCY_TYPE" && objSysSetting.UseMultiCurrency == 0)
                    {
                        sSQL.Append(" AND " + "CODES.CODE_ID=" + objSysSetting.BaseCurrencyType);
                    }
                    //Deb Multi Currency
                    //Start:modified by nitin goel for NIS,02/08/2013, cases for loss codes.
                    //if (p_sTableName == "LOSS_CODES" && objSysSetting.MultiCovgPerClm != 0)
                    if (p_sTableName == "LOSS_CODES" && objSysSetting.MultiCovgPerClm != 0 && !string.IsNullOrEmpty(p_sPolicyId))
                    {
                        //Ankit Start : Worked on MITS - 34657
                        //p_sPolicyLOB = Convert.ToString(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT POLICY_LOB_CODE FROM POLICY WHERE POLICY_ID=" + p_sPolicyId));
                        //string sLTemp = "SELECT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + p_sCovTypecode;
                        using (DbReader objReader = GetPolicyInfo(p_sPolicyId,objConn))
                        {
                            if (objReader.Read())
                            {
                                p_sPolicyLOB = Convert.ToString(objReader.GetValue("POLICY_LOB_CODE"));
                                iPolSystemId = Conversion.CastToType<int>(objReader.GetValue("POLICY_SYSTEM_ID").ToString(), out success);
                                objReader.Close();
                            }
                        }
                        //Ankit End
                        string sLTemp = string.Empty;
                        if (bIsFirstFinal && (objSysSetting.MultiCovgPerClm != 0))
                        {
                              //spahariya MITS 31978 to avoid dups loss code for multiple reserve type
                               // sLTemp = "SELECT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + p_sCovTypecode;
                                sLTemp = "SELECT DISTINCT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + p_sCovTypecode;
                            sLTemp = string.Concat(sLTemp, " AND POLICY_SYSTEM_ID = " + iPolSystemId);                  //Ankit Start : Worked on MITS - 34657
                            sSQL.Append(" AND " + "CODES.CODE_ID IN (" + sLTemp + ") ");
                        }
                        else
                        {
                            if (objSysSetting.MultiCovgPerClm != 0) 
                            {
                                if (!(string.Compare(p_sSessionClaimId, "0", true) == 0 && string.Compare(lClaimantEid.ToString(), "0", true) == 0))
                                {
                                    //Ankit Start : Worked on MITS - 34657
                                    //sLTemp = "SELECT LOSS_CODE FROM COVERAGE_X_LOSS,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_LOSS_ROW_ID= COVERAGE_X_LOSS.CVG_LOSS_ROW_ID AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid;
                                    sLTemp = "SELECT LOSS_CODE ";
                                    sLTemp = string.Concat(sLTemp, " FROM COVERAGE_X_LOSS CXL ");
                                    sLTemp = string.Concat(sLTemp, " INNER JOIN RESERVE_CURRENT RC ON RC.POLCVG_LOSS_ROW_ID= CXL.CVG_LOSS_ROW_ID ");
                                    if (objSysSetting.UsePolicyInterface)
                                    {
                                        sLTemp = string.Concat(sLTemp, " INNER JOIN POLICY_X_CVG_TYPE PXCT ON PXCT.POLCVG_ROW_ID = CXL.POLCVG_ROW_ID ");
                                        sLTemp = string.Concat(sLTemp, " INNER JOIN POLICY_X_UNIT PXU ON PXU.POLICY_UNIT_ROW_ID = PXCT.POLICY_UNIT_ROW_ID ");
                                        sLTemp = string.Concat(sLTemp, " INNER JOIN POLICY P ON P.POLICY_ID = PXU.POLICY_ID AND P.POLICY_SYSTEM_ID = " + iPolSystemId);
                                    }
                                    sLTemp = string.Concat(sLTemp, " WHERE RC.CLAIM_ID  = " + p_sSessionClaimId);
                                    sLTemp = string.Concat(sLTemp, " AND RC.CLAIMANT_EID = " + lClaimantEid);
                                    //Ankit End
                                    sSQL.Append(" AND " + "CODES.CODE_ID IN (" + sLTemp + ") ");
                                }
                            }
                        }
                    }
                    if (p_sTableName == "DISABILITY_CATEGORY" && objSysSetting.MultiCovgPerClm != 0)
                    {
                        p_sPolicyLOB = Convert.ToString(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT POLICY_LOB_CODE FROM POLICY WHERE POLICY_ID=" + p_sPolicyId));
                        //string sLTemp = "SELECT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + p_sCovTypecode;

                        string sLTemp = string.Empty;
                        if (!bIsFirstFinal && (objSysSetting.MultiCovgPerClm != 0))
                            {
                                if (!(string.Compare(p_sSessionClaimId, "0", true) == 0 && string.Compare(lClaimantEid.ToString(), "0", true) == 0))
                                {
                                    sLTemp = "SELECT RELATED_CODE_ID FROM COVERAGE_X_LOSS,RESERVE_CURRENT,CODES WHERE RESERVE_CURRENT.POLCVG_LOSS_ROW_ID= COVERAGE_X_LOSS.CVG_LOSS_ROW_ID AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + "  AND CODES.CODE_ID = COVERAGE_X_LOSS.LOSS_CODE";
                                    sSQL.Append(" AND " + "CODES.CODE_ID IN (" + sLTemp + ") ");
                                }
                            }
                        




                    }
                    if (p_sTableName == "DISABILITY_TYPE" && objSysSetting.MultiCovgPerClm != 0) //dbisht6 mits 35554
                    {
                        string sCvgType = string.Empty;
                        if (p_sFormName == "ReserveListingBOB" || p_sFormName == "Reservecurrent") //bsharma33 : RMA-3887 Reserve supplemental
                        {
                            sCvgType = GetCoverageType(p_sCovTypecode).ToString();

                        }
                        else
                            sCvgType = p_sCovTypecode;
                        //Ankit Start : Worked on MITS - 34657
                        //p_sPolicyLOB = Convert.ToString(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT POLICY_LOB_CODE FROM POLICY WHERE POLICY_ID=" + p_sPolicyId));
                        //string sLTemp = "SELECT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + p_sCovTypecode;
                        using (DbReader objReader = GetPolicyInfo(p_sPolicyId,objConn))
                        {
                            if (objReader.Read())
                            {
                                p_sPolicyLOB = Convert.ToString(objReader.GetValue("POLICY_LOB_CODE"));
                                iPolSystemId = Conversion.CastToType<int>(objReader.GetValue("POLICY_SYSTEM_ID").ToString(), out success);
                                objReader.Close();
                            }
                        }
                        //Ankit End

                        string sLTemp = string.Empty;
                        if ((bIsFirstFinal || p_sFormName == "ReserveListingBOB" || p_sFormName == "Reservecurrent") && (objSysSetting.MultiCovgPerClm != 0)) //bsharma33 : RMA-3887 Reserve supplemental
                        {
                            //spahariya MITS 31978 to avoid dups loss code for multiple reserve type
                            //sLTemp = "SELECT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + sCvgType;
                            sLTemp = "SELECT DISTINCT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + sCvgType;
                            sLTemp = string.Concat(sLTemp, " AND POLICY_SYSTEM_ID = " + iPolSystemId);                  //Ankit Start : Worked on MITS - 34657
                            sSQL.Append(" AND " + "CODES.CODE_ID IN (" + sLTemp + ") ");
                        }
                        else
                        {
                            if (objSysSetting.MultiCovgPerClm != 0)
                            {
                                if (!(string.Compare(p_sSessionClaimId, "0", true) == 0 && string.Compare(lClaimantEid.ToString(), "0", true) == 0))
                                {
                                    //Ankit Start : Worked on MITS - 34657
                                    //sLTemp = "SELECT LOSS_CODE FROM COVERAGE_X_LOSS,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_LOSS_ROW_ID= COVERAGE_X_LOSS.CVG_LOSS_ROW_ID AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid;
                                    sLTemp = "SELECT LOSS_CODE ";
                                    sLTemp = string.Concat(sLTemp, " FROM COVERAGE_X_LOSS CXL ");
                                    sLTemp = string.Concat(sLTemp, " INNER JOIN RESERVE_CURRENT RC ON RC.POLCVG_LOSS_ROW_ID= CXL.CVG_LOSS_ROW_ID ");
                                    sLTemp = string.Concat(sLTemp, " INNER JOIN POLICY_X_CVG_TYPE PXCT ON PXCT.POLCVG_ROW_ID = CXL.POLCVG_ROW_ID ");
                                    sLTemp = string.Concat(sLTemp, " INNER JOIN POLICY_X_UNIT PXU ON PXU.POLICY_UNIT_ROW_ID = PXCT.POLICY_UNIT_ROW_ID ");
                                    sLTemp = string.Concat(sLTemp, " INNER JOIN POLICY P ON P.POLICY_ID = PXU.POLICY_ID AND P.POLICY_SYSTEM_ID = " + iPolSystemId);
                                    sLTemp = string.Concat(sLTemp, " WHERE RC.CLAIM_ID  = " + p_sSessionClaimId);
                                    sLTemp = string.Concat(sLTemp, " AND RC.CLAIMANT_EID = " + lClaimantEid);
                                    //Ankit End
                                    sSQL.Append(" AND " + "CODES.CODE_ID IN (" + sLTemp + ") ");
                                }
                            }
                        }
                    }
                    //Changed by Gagan Bhatnagar for MITS 9314 : Start
                    //if (p_sFormName.IndexOf("admintracking") >= 0)
                    //{
                    //    sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                    //    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    //    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                    //    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                    //    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                    //}
                    //Changed by Gagan Bhatnagar for MITS 9314 : End
                    
                    //Added ExtraFilter for Adjuster and Enhanced Notes when BES enabled:Start for MITS 16650
                    if(m_bIsOrgSec)
                    {
                        GetExtraFilter(p_sTableName, sSQL);
                    }
                    //End
                   

                    sSQL.Append(" ORDER BY CODES.SHORT_CODE");
                }

                //get the codes Xml
                //objXML=GetCodesXML(sSQL.ToString(),p_lRecordCount,p_lPageNumber,p_sTableName,p_sTriggerDate,p_sFormName);  
                objCodeList = new CodeListType();
                objCodeList.Codes = new System.Collections.Generic.List<CodeType>();
                //Aman ML Change                
                if(this.CodesTable)
                {
                    //Ashish Ahuja : Mits 32888 Temporary Condition Check of Multilingual
                    if (isMultiLingual())
                    {
                        sSQL = PrepareMultilingualQuery(sSQL.ToString(), p_sTableName);
                    }
                    
                }
                objCodeList = GetCodesList(iSessionClaimId, sSQL.ToString(), p_lRecordCount, p_lPageNumber, p_sTableName, p_sTriggerDate, p_sFormName, p_sShowCheckBox, p_sSortColumn, p_sSortOrder,objConn,objCache); //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17//rsushilaggar Handled  paging for checkbox functionality  MITS 21600
            }
            catch (RMAppException p_objExp)
            {
                throw (p_objExp);
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.GetCodes.Error", m_iClientId), p_objExp);
            }
            finally
            {
                objSysSetting = null;
                if (objCache != null)
                    objCache.Dispose();
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
            }
            return objCodeList;
        }
        private StringBuilder PrepareMultilingualQuery(string p_sSQL, string p_sTableName)
        {                        
            StringBuilder sReturn = new StringBuilder();
            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
            int iIndex = p_sSQL.IndexOf("ORDER BY");
            int iOrderIndex = -1;
            string sOrderByCmd = p_sSQL.Substring(iIndex);
            string sOrderByQuery = string.Empty;
            string[] arrOrderByQueries = null;
            p_sSQL = p_sSQL.Replace(sOrderByCmd, "");
            string sOrderBy = string.Empty;
            if (!string.IsNullOrEmpty(sOrderByCmd))
            {
                arrOrderByQueries = sOrderByCmd.Split(',');
                int i = 1;
                foreach (string sQuery in arrOrderByQueries)
                {                    
                    sOrderByQuery = sQuery.Replace("ORDER BY", "");
                    sOrderByQuery = sOrderByQuery.Trim();
                    iOrderIndex = p_sSQL.IndexOf(sOrderByQuery, 0);
                    sOrderByQuery = p_sSQL.Substring(iOrderIndex, sOrderByQuery.Length);
                    p_sSQL = p_sSQL.Remove(iOrderIndex, sOrderByQuery.Length);
                    p_sSQL = p_sSQL.Insert(iOrderIndex, sOrderByQuery + " CODE" + i);
                    sOrderBy = sOrderBy + sQuery.Replace(sOrderByQuery, " CODE" + i) + ",";
                    i++;
                }
            }
            sReturn.Append(p_sSQL.Replace("1033", m_sLanguageCode.ToString()) + " UNION " + p_sSQL.Replace("1033",sBaseLangCode) + @" AND CODES.CODE_ID NOT IN (SELECT CODES.CODE_ID FROM CODES INNER JOIN CODES_TEXT
                                          ON CODES.CODE_ID = CODES_TEXT.CODE_ID WHERE CODES.TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + p_sTableName + "') AND CODES_TEXT.LANGUAGE_CODE = " + m_sLanguageCode.ToString() + ")");
            if (!string.IsNullOrEmpty(sOrderBy))
            {
               // sOrderByCmd = sOrderByCmd.Replace(sOrderByQuery, " CODE1");
                string sComma = sOrderBy.Substring(sOrderBy.Length - 1, 1);               
                if(!string.IsNullOrEmpty(sComma))
                {
                    sOrderBy = sOrderBy.Substring(0, sOrderBy.Length - 1);
                }
                sReturn.Append(sOrderBy);
            }
           
            return sReturn;

        }
        /// Name		: GetNLSCode
        /// Author		: Amandeep Kaur
        /// Date Created: 10/31/2012
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Returns the NLS code from security database
        /// </summary>
        /// <returns>integer</returns>
        private int GetNLSCode(int p_iDatabaseId)
        {
            int iReturnValue = 1033;
            StringBuilder sbSQL = null;
            DbReader objReader = null;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT USER_TABLE.NLS_CODE ");
                sbSQL.Append("FROM USER_TABLE,USER_DETAILS_TABLE  ");
                sbSQL.Append("WHERE USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.DSNID= ");
                sbSQL.Append(p_iDatabaseId);
                //abisht MITS 10932
                if (m_userid != 0)
                {
                    sbSQL.Append(" AND USER_TABLE.USER_ID= ");
                    sbSQL.Append(m_userid);
                }


                objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), sbSQL.ToString());
                if (objReader.Read())
                {
                    iReturnValue = Conversion.ConvertStrToInteger(objReader.GetValue("NLS_CODE").ToString());
                    objReader.Close();
                }
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.GetNLSCode.Err", m_iClientId), objException);
            }
            finally
            {
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();
            }
            return iReturnValue;
        }
        //Start: MITS 16650
        private void GetExtraFilter(string p_sTableName, StringBuilder sSQL)
        {
            string sField = string.Empty;
            switch (p_sTableName)
            {
                case "ADJ_TEXT_TYPE" :
                    sField = "ORG_SECURITY.GROUP_ADJ_TEXT_TYPES";
                    break;
                case "NOTE_TYPE_CODE" :
                    sField = "ORG_SECURITY.GROUP_ENH_NOTES_TYPES";
                    break;
                default:                    
                    break;
            }
            if (!string.IsNullOrEmpty(sField))
            {
                StringBuilder besSql = new StringBuilder();
                besSql.AppendFormat("select {0} from ORG_SECURITY INNER JOIN ORG_SECURITY_USER ON ", sField); 
                besSql.Append("ORG_SECURITY.GROUP_ID = ORG_SECURITY_USER.GROUP_ID WHERE USER_ID IN (" + m_userid + ")");
                DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, besSql.ToString());
                while (objReader.Read())
                {
                    string sReturn=string.Empty;
                    sReturn = objReader.GetValue(0).ToString().Trim(); ;
                    if (!string.IsNullOrEmpty(sReturn))
                    {
                        string substr = sReturn.Substring(0, 1);
                        if (substr == "-")
                        {
                            sReturn = sReturn.Substring(1);
                            sSQL.Append(" AND CODES.CODE_ID NOT IN" + "(" + sReturn + ")");
                        }
                        else
                        {
                            sSQL.Append(" AND CODES.CODE_ID IN" + "(" + sReturn + ")");
                        }
                    }
                }
            }
        }
        //End: MITS 16650
        public XmlDocument GetCodes(long p_lDeptEId, string p_sTableName, string p_sLOB,
                                    string p_sTypeLimits, string p_sFormName, string p_sTriggerDate, string p_sEventDate,
                                    string p_sSessionDsnId, string p_sSessionLOB, string p_sSessionClaimId, long p_lRecordCount,
                                    long p_lPageNumber, string p_sFilter, string p_sSortColumn, string p_sSortOrder)//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
        {
            int iMonthNow = 0;				//current month
            int iDay = 0;						//current day
            int iLOB;                       //Line of Business :Umesh
            int iClaimId;                   //Claim Id :Umesh
            string sToday = string.Empty;		//string for Date
            long lFacEId = 0;					//Facility Id
            long lLocEId = 0;					//Location Id
            long lDivEId = 0;					//Division Id
            long lRegEId = 0;					//Region Id
            long lOprEId = 0;					//Operation Id
            long lComEId = 0;					//Company Id
            long lCliEId = 0;					//Client Id
            StringBuilder sSQL = null;		//SQL for searching for the code
            string sLOBSQL = string.Empty;	//SQL string for LOB
            string sLimitsSQL = string.Empty;	//SQL string for Claim/Event Type
            string sAdjDTTFilter = string.Empty;			//umesh
            string[] sarrSplit;				//used for parsing the limits
            string[] sarrSplit1;			//used for parsing the limits
            string[] sarrFilter;           //Umesh
            XmlDocument objXML = null;		//Document to return 
            XmlDocument objXmlDocument = null;      // Umesh
            //DbReader objRdr=null;//Umesh
            CodeListType objCodeList = null;
            ColLobSettings objColLobSettings = null;
            DbConnection objConn = null;
            //Added by Nikhil.Retrofit Changes NI
            SysSettings objSysSetting = null;


            try
            {
                //Added by Nikhil.Retrofit Changes NI
                objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                if (p_lPageNumber == 0)
                    p_lPageNumber = 1;

                iMonthNow = DateTime.Now.Month;
                iDay = DateTime.Now.Day;

                sToday = DateTime.Now.Year.ToString();

                //for the date string
                if (iMonthNow < 10)
                    sToday = sToday + "0" + iMonthNow.ToString();
                else
                    sToday = sToday + iMonthNow.ToString();

                if (iDay < 10)
                    sToday = sToday + "0" + iDay.ToString();
                else
                    sToday = sToday + iDay.ToString();

                //if DepartmentId is passed then get the information about the Facility, Location etc. also.
                if (p_lDeptEId > 0)
                    GetOrgEid(p_lDeptEId, ref lFacEId, ref lLocEId, ref lDivEId, ref lRegEId, ref lOprEId,
                        ref lComEId, ref lCliEId, objConn
                        );

                if (p_sTableName.Equals(string.Empty))
                    return null;

                sSQL = new StringBuilder();

                //create SQL depending on the table name passed
                if (p_sTableName.ToLower().Equals("states"))
                {
                    sSQL.Append(@" SELECT STATES.STATE_ROW_ID, STATES.STATE_ID, STATES.STATE_NAME	
								FROM STATES WHERE STATE_ROW_ID > 0 ORDER BY STATES.STATE_ID");
                }
                //Start:added by Nitin goel, MITS 30910, 08/22/2013
                //Changed by Nikhil.Code review changes
                    //else if (p_sTableName.ToLower().Equals("coverage_group"))
                else if (String.Compare(p_sTableName,"coverage_group",true) == 0)
                {
                    string sDateOfClaim = string.Empty;
                    string sDateOfEvent = string.Empty;
                    string sDateOfPolicy = string.Empty;
                    int iSessionClaimId = 0;
                    bool bSuccess = false;
                    //Start -  Multilingual changes

                    int iLangcode = 0;
                    iLangcode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                    //end -  Multilingual changes
                    iSessionClaimId = Conversion.CastToType<int>(p_sSessionClaimId, out bSuccess);
                    //Changed by Nikhil. Code review changes
                    //if (iSessionClaimId > 0 && !(string.IsNullOrEmpty(p_sSessionClaimId) || p_sSessionClaimId.ToLower().Trim() == "undefined"))
                     if (iSessionClaimId > 0 && !(string.IsNullOrEmpty(p_sSessionClaimId) || String.Compare(p_sSessionClaimId.Trim(),"undefined",true) == 0))
                         //Changed by Nikhil.Retrofit changes NI
                         //GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy);
                         GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy, objConn, objSysSetting, p_sSessionLOB);
                    if (!(string.Compare(p_sTriggerDate, "0") == 0) && !(string.IsNullOrEmpty(p_sTriggerDate)))
                        //Changed By Nikhil.Code review changes
                        //sDateOfPolicy = DbFactory.ExecuteAsType<string>(m_sConnectionString, "SELECT EFFECTIVE_DATE FROM POLICY WHERE POLICY_ID = " + p_sTriggerDate);
                    sDateOfPolicy = DbFactory.ExecuteAsType<string>(m_sConnectionString, String.Format("SELECT EFFECTIVE_DATE FROM POLICY WHERE POLICY_ID ={0} " , p_sTriggerDate));

                    //Start: Added by Sumit Agarwal for NI on 11/10/2014 to get the values for sDateOfEvent and sDateOfClaim (if they are blank)
                    if (string.IsNullOrEmpty(sDateOfEvent))
                        sDateOfEvent = DbFactory.ExecuteAsType<string>(m_sConnectionString, String.Format("SELECT E.DATE_OF_EVENT FROM EVENT E INNER JOIN CLAIM C ON E.EVENT_ID = C.EVENT_ID AND C.CLAIM_ID =  {0}", p_sSessionClaimId));
                    if (string.IsNullOrEmpty(sDateOfClaim))
                        sDateOfClaim = DbFactory.ExecuteAsType<string>(m_sConnectionString, String.Format("SELECT DATE_OF_CLAIM FROM CLAIM WHERE CLAIM_ID = {0}", p_sSessionClaimId));
                    //End: Added by Sumit Agarwal for NI on 11/10/2014 to get the values for sDateOfEvent and sDateOfClaim (if they are blank)

                    sSQL.Length = 0;
                    //start -  Multilingual changes
                    //sSQL.Append(@" SELECT CG.COVERAGE_GROUP_ID, CG.COV_GROUP, CG.COV_GROUP_DESC");
                    //sSQL.Append(" FROM COVERAGE_GROUP CG WHERE CG.DELETED_FLAG !=-1 AND CG.COVERAGE_GROUP_ID > 0");
                    sSQL.Append(@" SELECT CG.COVERAGE_GROUP_ID, CG.COV_GROUP, CGT.GROUP_DESCRIPTION");
                    sSQL.AppendFormat(" FROM COVERAGE_GROUP CG INNER JOIN COVERAGE_GROUP_TEXT CGT ON CG.COVERAGE_GROUP_ID = CGT.COVERAGE_GROUP_ID WHERE CGT.LANGUAGE_CODE = {0} AND CG.DELETED_FLAG !=-1 AND CG.COVERAGE_GROUP_ID > 0 ", iLangcode);
                    //end -  Multilingual changes
                    //start:Added to filter Coverage group based on States value
                    if (!string.IsNullOrEmpty(p_sFilter))
                        sSQL.Append(" AND " + p_sFilter);
                    //end:Added to filter Coverage group based on States value
                    sSQL.Append(" AND ((CG.TRIGGER_DATE_FIELD IS NULL) ");

                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CG.EFF_START_DATE<='" + sToday + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sToday + "') ");
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CG.EFF_START_DATE<='" + sToday + "' AND CG.EFF_END_DATE>='" + sToday + "') ");

                    if (!string.IsNullOrEmpty(sDateOfEvent))
                    {
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CG.EFF_START_DATE<='" + sDateOfEvent + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sDateOfEvent + "') ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CG.EFF_START_DATE<='" + sDateOfEvent + "' AND CG.EFF_END_DATE>='" + sDateOfEvent + "') ");

                    }
                    if (!string.IsNullOrEmpty(sDateOfClaim))
                    {
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CG.EFF_START_DATE<='" + sDateOfClaim + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sDateOfClaim + "') ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CG.EFF_START_DATE<='" + sDateOfClaim + "' AND CG.EFF_END_DATE>='" + sDateOfClaim + "') ");
                    }
                    if (!string.IsNullOrEmpty(sDateOfPolicy))
                    {
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CG.EFF_START_DATE<='" + sDateOfPolicy + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CG.EFF_START_DATE<='" + sDateOfPolicy + "' AND CG.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                    }

                    sSQL.Append(") ORDER BY CG.COV_GROUP");
                }
                //end:added by Nitin goel, MITS 30910, 08/22/2013
                else if (p_sTableName.ToLower().Equals("cust_rate"))
                {
                    sSQL.Append(@" SELECT RATE_TABLE_ID, EFFECTIVE_DATE, EXPIRATION_DATE, TABLE_NAME FROM CUST_RATE
								WHERE CUST_RATE.CUSTOMER_EID = " + p_sLOB);
                }
                else if (p_sTableName.ToLower().Equals("event_type") || p_sTableName.ToLower().Equals("claim_type"))
                {
                    if (Conversion.ConvertStrToLong(p_sLOB) > 0)
                        // JP 12.12.2005    sLOBSQL = " AND CODES.LINE_OF_BUS_CODE = " + p_sLOB ;
                        sLOBSQL = " AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) "; // JP 12.12.2005   If CODES.LINE_OF_BUS_CODE = 0 then always show - regardless of claim's lob
                    else
                        sLOBSQL = string.Empty;

                    if (p_sTypeLimits.Trim().Length > 0)
                    {
                        sarrSplit = p_sTypeLimits.Split('|');
                        for (int i = 0; i < sarrSplit.Length; i++)
                        {
                            sarrSplit1 = sarrSplit[i].Split(',');
                            if (Conversion.ConvertStrToBool(sarrSplit1[1]) == false)
                                sLimitsSQL = sLimitsSQL + "," + sarrSplit1[0];
                        }
                        if (sLimitsSQL.Trim().Length > 0)
                            sLimitsSQL = " AND CODES.CODE_ID NOT IN (" + sLimitsSQL.Substring(2) + ") ";
                    }
                    else
                        sLimitsSQL = string.Empty;


                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC, C2.SHORT_CODE");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID");
                    sSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sTableName.ToUpper() + "'");
                    sSQL.Append(" AND GLOSSARY.TABLE_ID=CODES.TABLE_ID");
                    sSQL.Append(" AND CODES.CODE_ID=CODES_TEXT.CODE_ID");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=1033");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)");
                    sSQL.Append(sLOBSQL);
                    sSQL.Append(sLimitsSQL);

                    if (p_lDeptEId > 0)
                    {
                        sSQL.Append("	AND (CODES.ORG_GROUP_EID=" + p_lDeptEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lFacEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lLocEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lDivEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lRegEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lOprEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lComEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lCliEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID IS NULL ");
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                    }
                    else
                    {
                        //Abhishek MITS 11352, for claim_type with associated org types should also
                        //be displayed in claim type search
                        if (!p_sTableName.ToLower().Equals("claim_type"))
                        {
                            sSQL.Append("	AND (CODES.ORG_GROUP_EID IS NULL ");
                            sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                        }
                    }

                    EvaluateTriggerDates(sSQL, p_sFormName, sToday, p_sTriggerDate, p_sEventDate, p_sSessionClaimId, objConn, null, p_sLOB);

                    sSQL.Append(" ORDER BY CODES.SHORT_CODE");
                }

                else if (p_sTableName.ToLower().Equals("account"))
                {
                    sSQL.Append(" SELECT ACCOUNT_ID, NULL, ACCOUNT_NAME ");
                    sSQL.Append(" FROM ACCOUNT");
                    sSQL.Append(" ORDER BY ACCOUNT_ID ");
                }
                else if (p_sTableName.ToLower().Equals("rm_sys_users"))
                {
                    //m_sConnectionString=SecurityDatabase.GetSecurityDsn(0);
                    m_bIsSecurityDatabase = true;
                    sSQL.Append(" SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME");
                    sSQL.Append(" FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID");
                    sSQL.Append(" AND USER_DETAILS_TABLE.DSNID = " + p_sSessionDsnId);
                    sSQL.Append(" ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                }
                else if (p_sTableName.ToLower().Equals("user_groups"))
                {
                    sSQL.Append(" SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS ");
                    sSQL.Append(" ORDER BY GROUP_ID ");
                }
                else if (p_sTableName.ToLower().Equals("trans_types"))
                {
                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,C2.SHORT_CODE");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID");
                    sSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" + p_sTableName.ToUpper() + "' ");
                    if (p_sSessionLOB.Equals(string.Empty) || p_sSessionLOB.Equals("0"))
                        sSQL.Append(" ");
                    else
                    {// 06/18/2007 MITS : 9499   by Umesh
                        objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                        iLOB = Conversion.ConvertStrToInteger(p_sSessionLOB);
                        iClaimId = Conversion.ConvertStrToInteger(p_sSessionClaimId);
                        if (objColLobSettings[iLOB].ResByClmType == true && iClaimId != 0 && p_sSessionClaimId != "")
                        {
                            //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString,
                            //  "SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimId))
                            //{
                            //    if (objRdr.Read())
                            //    {
                                    int iClaimTypeCode = 0;
                                    iClaimTypeCode = objConn.ExecuteInt("SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimId);
                                    sSQL.Append(" AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE=" + p_sSessionLOB + " AND CLAIM_TYPE_CODE =" + iClaimTypeCode + ")");
                            //    }

                            //    objRdr.Close();
                            //}

                        } // END MITS 9499
                        else

                            sSQL.Append(" AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sSessionLOB + ")");
                    }
                    sSQL.Append(" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ");
                    sSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ");
                    if (p_sLOB.Trim() != "")
                    {
                        sSQL.Append(" AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) ");
                    }
                    //trans_type is also dependent upon currentdate,event date, claim date and policy transaction date, policy effective date
                    // for current mits transaction types need to be filtered on system date
                    EvaluateTriggerDates(sSQL, p_sFormName, sToday, p_sTriggerDate, p_sEventDate, p_sSessionClaimId, objConn, null, p_sLOB);
                    //start:Added by nitin goel,NI, MITS 30910
                    //Changed by Nikhil .Code review changes
                   // if (p_sFilter != "")
                    if (!String.IsNullOrEmpty(p_sFilter))
                    {
                        //Deb: Pen Testing
                        if (p_sFilter.ToUpper().IndexOf("IN") > -1)
                        {
                            int index = p_sFilter.LastIndexOf("IN");
                            string sTemp = p_sFilter.Substring(index + 2);
                            //Deb MITS 27684
                            if (!(sTemp.IndexOf("(") > -1 && sTemp.IndexOf(")") > -1))
                            p_sFilter = p_sFilter.Replace(sTemp, "(" + sTemp + ")");
                            sSQL.Append(" AND " + p_sFilter);
                        }
                        //Deb : Pen Testing
                        else
                           sSQL.Append(" AND " + p_sFilter);
                    }
                    //End:Added by Nitin goel
                    sSQL.Append(" ORDER BY CODES.SHORT_CODE");
                }
                else if ((p_sTableName.Length > 8) && (p_sTableName.ToLower().Substring(0, 9).Equals("glossary_")))
                {
                    sSQL.Append(" SELECT GLOSSARY.TABLE_ID, GLOSSARY.SYSTEM_TABLE_NAME, GLOSSARY_TEXT.TABLE_NAME, NULL");
                    sSQL.Append(" FROM GLOSSARY, GLOSSARY_TEXT");
                    sSQL.Append(" WHERE GLOSSARY.TABLE_ID=GLOSSARY_TEXT.TABLE_ID");
                    //sSQL.Append(" AND GLOSSARY_TEXT.LANGUAGE_CODE=1033"); //Aman ML Change
                    sSQL.Append(" AND GLOSSARY.GLOSSARY_TYPE_CODE=" + p_sTableName.Substring(9));
                }
                else if (p_sTableName.ToLower().Equals("entity")) //25/12/2007 Abhishek, MITS 11097 - START
                {
                    if (bIsMobileAdjuster)
                    {
                        sSQL.Append("SELECT TABLE_ID, SYSTEM_TABLE_NAME FROM GLOSSARY WHERE ");
                        sSQL.Append("GLOSSARY_TYPE_CODE =7 ORDER BY SYSTEM_TABLE_NAME");
                    }
                    else
                    {
                        sSQL.Append("SELECT TABLE_ID, SYSTEM_TABLE_NAME FROM GLOSSARY WHERE GLOSSARY_TYPE_CODE =4 ");
                        sSQL.Append("or GLOSSARY_TYPE_CODE =5 or GLOSSARY_TYPE_CODE =7 ORDER BY SYSTEM_TABLE_NAME");
                    }
                } //25/12/2007 Abhishek, MITS 11097 - END
                else
                {
                    if (Conversion.ConvertStrToLong(p_sLOB) > 0)
                        sLOBSQL = " AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) ";
                    else
                        sLOBSQL = string.Empty;

                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,C2.SHORT_CODE");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID");
                    sSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" + p_sTableName.ToUpper() + "' ");
                    // 01/08/07 REM UMESH
                    if ((p_sFormName == "adjusterdatedtext") && (p_sTableName == "ADJ_TEXT_TYPE"))
                    {
                        objXmlDocument = new XmlDocument();
                        string strContent = RMSessionManager.GetCustomContent(m_iClientId);
                        
                            if (! string.IsNullOrEmpty(strContent))
                            {
                                objXmlDocument.LoadXml(strContent);
                                //objRdr.Dispose();
                                XmlNode objSearch = objXmlDocument.SelectSingleNode("//RMAdminSettings/SpecialSettings/text");
                                if (objSearch != null && objSearch.InnerText != "")
                                {
                                    sAdjDTTFilter = objSearch.InnerText;
                                    sarrFilter = sAdjDTTFilter.Split(',');
                                    if (sarrFilter.Length > 0)
                                    {
                                        sSQL.Append("AND CODES.SHORT_CODE IN (");
                                        for (int f = 0; f < sarrFilter.Length; f++)
                                        {
                                            sSQL.Append("'" + sarrFilter[f].Trim().ToUpper() + "',");
                                        }
                                        sSQL.Remove(sSQL.Length - 1, 1);
                                        sSQL.Append(")");
                                    }
                                }
                            }
                    }


                    //REM END
                    sSQL.Append(" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ");
                    sSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ");
                    if (sLOBSQL != string.Empty)
                        sSQL.Append(sLOBSQL);
                    if (p_lDeptEId > 0)
                    {
                        sSQL.Append("	AND (CODES.ORG_GROUP_EID=" + p_lDeptEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lFacEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lLocEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lDivEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lRegEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lOprEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lComEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lCliEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID IS NULL ");
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                    }
                    else
                    {
                        sSQL.Append("	AND (CODES.ORG_GROUP_EID IS NULL ");
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                    }
                    switch (p_sFormName.ToLower())
                    {
                        case "":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                            break;
                        case "event":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "claimgc":
                        case "claimwc":
                        case "claimva":
                        case "claimdi":
                        //Shruti for 11708
                        case "claimant":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            if (p_sEventDate.Length > 0)
                            {
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sEventDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sEventDate + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sEventDate + "' AND CODES.EFF_END_DATE>='" + p_sEventDate + "') ");
                            }
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "policy":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "funds":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "employee":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        //MITS 15185 : Umesh
                        default:
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                            break;
                        //MITS 15185 : Umesh



                    }
                    //Custom Filter clause. MCIC Changes 01/24/08
                    if (p_sFilter != "")
                        sSQL.Append(" AND " + p_sFilter);
                    //Changed by Gagan Bhatnagar for MITS 9314 : Start
                    //if (p_sFormName.IndexOf("admintracking") >= 0)
                    //{
                    //    sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                    //    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    //    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                    //    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                    //    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                    //}
                    //Changed by Gagan Bhatnagar for MITS 9314 : End

                    sSQL.Append(" ORDER BY CODES.SHORT_CODE");
                }

                //get the codes Xml
                objXML = GetCodesXML(sSQL.ToString(), p_lRecordCount, p_lPageNumber, p_sTableName, p_sTriggerDate, p_sFormName, p_sSortColumn, p_sSortOrder);//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                //objCodeList = new CodeListType();
                //objCodeList.Codes = new System.Collections.Generic.List<CodeType>();
                //objCodeList = GetCodesList(sSQL.ToString(), p_lRecordCount, p_lPageNumber, p_sTableName, p_sTriggerDate, p_sFormName);
            }
            catch (RMAppException p_objExp)
            {
                throw (p_objExp);
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.GetCodes.Error", m_iClientId), p_objExp);
            }

            return objXML;
        }
        //MITS 9499 : Umesh
        public XmlDocument GetCodes(long p_lDeptEId, string p_sTableName, string p_sLOB,
                                    string p_sTypeLimits, string p_sFormName, string p_sTriggerDate,
                                    string p_sSessionDsnId, string p_sSessionLOB, long p_lRecordCount,
                                    long p_lPageNumber, string p_sSortColumn, string p_sSortOrder)
        {
            XmlDocument objXML = null;		//Document to return
            CodeListType retCodesList = new CodeListType();
            try
            {
                objXML = GetCodes(p_lDeptEId, p_sTableName, p_sLOB, p_sTypeLimits, p_sFormName, p_sTriggerDate, string.Empty, p_sSessionDsnId, p_sSessionLOB, "0", p_lRecordCount, p_lPageNumber, "", p_sSortColumn, p_sSortOrder);//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
            }
            catch (RMAppException p_objExp)
            {
                throw (p_objExp);
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.GetCodes.Error", m_iClientId), p_objExp);
            }
            return objXML;
        }
        //MITS 9499 : Umesh
        //p_sJurisdiction MITS # 36929
        public CodeListType GetCodesNew(long p_lDeptEId, string p_sTableName, string p_sLOB, string p_sJurisdiction,
        //public CodeListType GetCodesNew(long p_lDeptEId, string p_sTableName, string p_sLOB,
                                    string p_sTypeLimits, string p_sFormName, string p_sTriggerDate,
                                    string p_sSessionDsnId, string p_sSessionLOB, long p_lRecordCount,
                                    long p_lPageNumber, string p_sParentCodeId, string p_sFilter, string p_sSortColumn, string p_sSortOrder, int iLangCode)//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
        {
            XmlDocument objXML = null;		//Document to return
            CodeListType retCodesList = new CodeListType();
            try
            {
                //rupal: commented for first & final payment
                //retCodesList = GetCodesNew(p_lDeptEId, p_sTableName, p_sLOB, p_sTypeLimits, p_sFormName, p_sTriggerDate, string.Empty, p_sSessionDsnId, p_sSessionLOB, "0", p_lRecordCount, p_lPageNumber, p_sFilter, "", "", "", p_sParentCodeId, "", string.Empty, string.Empty, string.Empty, string.Empty); //rsushilaggar Handled  paging for checkbox functionality  MITS 21600
                //rupal : above line chaged for first & final payment

                retCodesList = GetCodesNew(p_lDeptEId, p_sTableName, p_sLOB,p_sJurisdiction, p_sTypeLimits, p_sFormName, p_sTriggerDate, string.Empty, p_sSessionDsnId, p_sSessionLOB, "0", p_lRecordCount, p_lPageNumber, p_sFilter, "", "", "", p_sParentCodeId, "", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, p_sSortColumn, p_sSortOrder, iLangCode, string.Empty, string.Empty, string.Empty);//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17 //rsushilaggar Handled  paging for checkbox functionality  MITS 21600
            }
            catch (RMAppException p_objExp)
            {
                throw (p_objExp);
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.GetCodes.Error", m_iClientId), p_objExp);
            }
            return retCodesList;
        }

        /// Name		: QuickLookup
        /// Author		: Nikhil Kumar Garg
        /// Date Created: 13-May-2005
        ///*************************************************************************
        /// Amendment History
        ///*************************************************************************
        /// Date Amended   *   Amendment   *								Author
        ///*************************************************************************
        /// <summary>
        /// Gets the codes list for the quick lookup window by searching on the text
        /// typed by the user.
        /// </summary>
        /// <param name="p_sIndex">Number/Id</param>
        /// <param name="p_sLookupString">string to search</param>
        /// <param name="p_sLookupType">Type/Code table to search</param>
        /// <param name="p_sLOB">Line Of Business</param>
        /// <param name="p_sSessionDsnId">DSN Id stored in the session</param>
        /// <param name="p_sTypeLimits">Limits for Claim_Types or Event_Types</param>
        /// <param name="p_sFilter">Additional Filter Clause for Custom Code Lookup</param>
        /// <returns>XML Document containing the codes list</returns>
        public XmlDocument QuickLookup(string p_sIndex, string p_sLookupString, string p_sLookupType,
                                       string p_sLOB, string p_sTypeLimits, string p_sSessionDsnId, bool p_bDescriptionSearch, string p_sFilter, string p_sSessionLOB, long p_lDeptEId, string p_sFormName, string p_sTriggerDate, string p_sEventDate, string p_sTitle, string p_sSessionClaimId, string p_sOrgLevel)
        {
            XmlDocument objQuickLookupXml = null;		//xml to return
            string sLOBSQL = string.Empty;			//SQL string for LOB
            string sLimitsSQL = string.Empty;			//SQL string for Claim/Event Type
            string[] sarrSplit;						//used for parsing the limits
            string[] sarrSplit1;					//used for parsing the limits
            bool bOracle = m_bIsOracle;				//Whether case sensitive search is required for Oracle
            StringBuilder sSQL = new StringBuilder();	//SQL for searching for the code
            string sConnectionString = "";
            bool bIsSecurityDB = false;
            DbConnection objConn = null;
            //PJS MITS 11571-03-11-2008 : used for filtering 
            string sToday = string.Empty;		//string for Date
            int iMonthNow = 0;				    //current month
            int iDay = 0;						//current day
            long lFacEId = 0;					//Facility Id
            long lLocEId = 0;					//Location Id
            long lDivEId = 0;					//Division Id
            long lRegEId = 0;					//Region Id
            long lOprEId = 0;					//Operation Id
            long lComEId = 0;					//Company Id
            long lCliEId = 0;					//Client Id
            //Added by Nikhil.Retrofit changes NI
            SysSettings objSysSetting = null;
            LocalCache objCache = null;     //avipinsrivas Start : Worked for Jira-340
            try
            {
                //Added by Nikhil.Retrofit Changes
                objSysSetting =  new SysSettings(m_sConnectionString,m_iClientId);
                objCache = new LocalCache(m_sConnectionString,m_iClientId);     //avipinsrivas Start : Worked for Jira-340
                sConnectionString = m_sConnectionString;
                bIsSecurityDB = false;
                objConn = DbFactory.GetDbConnection(sConnectionString);
                objConn.Open();
                //format string to search and append %
                p_sLookupString = p_sLookupString.Trim();
                p_sLookupString = p_sLookupString.Replace("'", "''");
                p_sLookupString = p_sLookupString.Replace("%", "");
                if (p_bDescriptionSearch)
                    p_sLookupString = "%" + p_sLookupString + "%";
                else
                    p_sLookupString = p_sLookupString + "%";

                //PJS MITS 11571-03-11-2008 : calculating dates & department for effective date filtering 
                iMonthNow = DateTime.Now.Month;
                iDay = DateTime.Now.Day;

                sToday = DateTime.Now.Year.ToString();

                //for the date string
                if (iMonthNow < 10)
                    sToday = sToday + "0" + iMonthNow.ToString();
                else
                    sToday = sToday + iMonthNow.ToString();

                if (iDay < 10)
                    sToday = sToday + "0" + iDay.ToString();
                else
                    sToday = sToday + iDay.ToString();
                //if DepartmentId is passed then get the information about the Facility, Location etc. also.
                if (p_lDeptEId > 0)
                    GetOrgEid(p_lDeptEId, ref lFacEId, ref lLocEId, ref lDivEId, ref lRegEId, ref lOprEId,
                        ref lComEId, ref lCliEId,objConn);
                //PJS MITS 11571-03-11-2008 
                //find out whether we need case insensitive search for Oracle
                //BSB 05.12.2006 We need to know if we're running on Oracle - not just if a sysparm is set.
                //Parijat: We need to know if its oracle then the value of sysparm therefore '&='not just '|='
                bOracle &= IsCaseInSensitveSearch();

                //BSB 05.12.2006 Fix for Oracle comparison to empty string in where clause 
                // fails to yeild any query results
                if (bOracle && p_sIndex == "")
                    p_sIndex = " ";

                //BSB 05.18.2006 Fix for Oracle case insensitive comparison.
                // Leaving p_sLookupString mixed case failed to yeild any quick lookup query results.
                if (bOracle)
                    p_sLookupString = p_sLookupString.ToUpper();

                //create sql string for searching
                if (Conversion.ConvertStrToLong(p_sLOB) > 0)
                    sLOBSQL = " AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) ";
                else
                    sLOBSQL = string.Empty;
                if (p_sLookupType.ToUpper().Equals("EMPLOYEE.LASTNAME"))
                {
                    if (bOracle)
                        sSQL.Append("SELECT EMPLOYEE.EMPLOYEE_EID, EMPLOYEE.EMPLOYEE_NUMBER, ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM EMPLOYEE,ENTITY WHERE EMPLOYEE.EMPLOYEE_EID=ENTITY.ENTITY_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND UPPER(ENTITY.LAST_NAME) LIKE '" + p_sLookupString + "' ORDER BY EMPLOYEE.EMPLOYEE_NUMBER");
                    else
                        sSQL.Append("SELECT EMPLOYEE.EMPLOYEE_EID, EMPLOYEE.EMPLOYEE_NUMBER, ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM EMPLOYEE,ENTITY WHERE EMPLOYEE.EMPLOYEE_EID=ENTITY.ENTITY_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND ENTITY.LAST_NAME LIKE '" + p_sLookupString + "' ORDER BY EMPLOYEE.EMPLOYEE_NUMBER");
                }
                else if (p_sLookupType.ToUpper().Equals("EMPLOYEES"))
                {
                    if (bOracle)
                        sSQL.Append("SELECT EMPLOYEE.EMPLOYEE_EID, EMPLOYEE.EMPLOYEE_NUMBER, ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM EMPLOYEE,ENTITY WHERE EMPLOYEE.EMPLOYEE_EID=ENTITY.ENTITY_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND UPPER(EMPLOYEE.EMPLOYEE_NUMBER) LIKE '" + p_sLookupString + "' AND UPPER(EMPLOYEE.EMPLOYEE_NUMBER) >= '" + p_sIndex.ToUpper() + "' ORDER BY EMPLOYEE.EMPLOYEE_NUMBER");
                    else
                        sSQL.Append("SELECT EMPLOYEE.EMPLOYEE_EID, EMPLOYEE.EMPLOYEE_NUMBER, ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM EMPLOYEE,ENTITY WHERE EMPLOYEE.EMPLOYEE_EID=ENTITY.ENTITY_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND EMPLOYEE.EMPLOYEE_NUMBER LIKE '" + p_sLookupString + "' AND EMPLOYEE.EMPLOYEE_NUMBER >= '" + p_sIndex + "' ORDER BY EMPLOYEE.EMPLOYEE_NUMBER");
                }
                else if (p_sLookupType.ToUpper().Equals("CODE.ORGH"))
                {
                    //if (bOracle)
                    //    sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID=1012 AND UPPER(ABBREVIATION) LIKE '" + p_sLookupString + "' AND UPPER(ABBREVIATION) >= '" + p_sIndex.ToUpper() + "' ORDER BY ABBREVIATION");
                    //else
                    //    sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID=1012 AND ABBREVIATION LIKE '" + p_sLookupString + "' AND ABBREVIATION >= '" + p_sIndex + "' ORDER BY ABBREVIATION");
                    //abansal23: on 05/12/2009 for MITS 14847
                    if (bOracle)
                        sSQL.Append("SELECT A.*,B.ABBREVIATION,B.LAST_NAME FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE B.ENTITY_TABLE_ID=1012 AND UPPER(B.ABBREVIATION) LIKE '" + p_sLookupString + "' AND UPPER(B.ABBREVIATION) >= '" + p_sIndex + "'");
                    else
                        sSQL.Append("SELECT A.*,B.ABBREVIATION,B.LAST_NAME FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE B.ENTITY_TABLE_ID=1012 AND B.ABBREVIATION LIKE '" + p_sLookupString + "' AND B.ABBREVIATION >= '" + p_sIndex + "'");
                    //pmittal5 confidential record
                    if (m_bIsOrgSec)
                        sSQL.Append(" AND B.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID=" + m_userid + ")");
                    sSQL.Append(" ORDER BY B.ABBREVIATION");
                    //End - pmittal5

                }
                else if (p_sLookupType.ToUpper().Equals("CODE.STATES"))
                {
                    if (bOracle)
                        sSQL.Append("SELECT STATE_ROW_ID, STATE_ID, STATE_NAME FROM STATES WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND UPPER(STATE_ID) LIKE '" + p_sLookupString + "' AND UPPER(STATE_ID) >= '" + p_sIndex.ToUpper() + "'");
                    else
                        sSQL.Append("SELECT STATE_ROW_ID, STATE_ID, STATE_NAME FROM STATES WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND STATE_ID LIKE '" + p_sLookupString + "' AND STATE_ID >= '" + p_sIndex + "'");
                }
                //Start:added by Nitin goel, MITS 30910, 08/22/2013
                    //Changed  by Nikhil.Code review changes
              //  else if (p_sLookupType.ToUpper().Equals("CODE.COVERAGE_GROUP"))
                 else if (String.Compare(p_sLookupType,"CODE.COVERAGE_GROUP",true) == 0)
                {
                    string sDateOfClaim = string.Empty;
                    string sDateOfEvent = string.Empty;
                    string sDateOfPolicy = string.Empty;
                    bool bSuceess =false;
                    //Start -  Multilingual changes
                    int iSessionClaimId = 0;
                    int iLangcode = 0;
                    iSessionClaimId = Conversion.CastToType<int>(p_sSessionClaimId, out bSuceess);
                    //Changed  by Nikhil.Code review changes
                    //if (iSessionClaimId > 0 && !(string.IsNullOrEmpty(p_sSessionClaimId) || p_sSessionClaimId.ToLower().Trim() == "undefined"))
                    if (iSessionClaimId > 0 && !(string.IsNullOrEmpty(p_sSessionClaimId) || String.Compare(p_sSessionClaimId.Trim(),"undefined",true) == 0))
                        //Changed by Nikhil.Retrofit changes NI
                    //    GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy);
                        GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy,objConn,objSysSetting,p_sLOB);
                    if (!(string.Compare(p_sTriggerDate, "0") == 0) && !(string.IsNullOrEmpty(p_sTriggerDate)))
                        //Changed  by Nikhil.Code review changes
                        //sDateOfPolicy = DbFactory.ExecuteAsType<string>(m_sConnectionString, "SELECT EFFECTIVE_DATE FROM POLICY WHERE POLICY_ID = " + p_sTriggerDate);
                       sDateOfPolicy = DbFactory.ExecuteAsType<string>(m_sConnectionString,String.Format("SELECT EFFECTIVE_DATE FROM POLICY WHERE POLICY_ID ={0}", p_sTriggerDate));

                    //Start: Added by Sumit Agarwal for NI on 11/10/2014 to get the values for sDateOfEvent and sDateOfClaim (if they are blank)
                    if (string.IsNullOrEmpty(sDateOfEvent))
                        sDateOfEvent = DbFactory.ExecuteAsType<string>(m_sConnectionString, String.Format("SELECT E.DATE_OF_EVENT FROM EVENT E INNER JOIN CLAIM C ON E.EVENT_ID = C.EVENT_ID AND C.CLAIM_ID =  {0}", p_sSessionClaimId));
                    if (string.IsNullOrEmpty(sDateOfClaim))
                        sDateOfClaim = DbFactory.ExecuteAsType<string>(m_sConnectionString, String.Format("SELECT DATE_OF_CLAIM FROM CLAIM WHERE CLAIM_ID = {0}", p_sSessionClaimId));
                    //End: Added by Sumit Agarwal for NI on 11/10/2014 to get the values for sDateOfEvent and sDateOfClaim (if they are blank)

                    sSQL.Length = 0;
                    iLangcode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                    //start -  Multilingual changes
                    //sSQL.Append(@" SELECT CG.COVERAGE_GROUP_ID, CG.COV_GROUP, CG.COV_GROUP_DESC");
                    //sSQL.Append(" FROM COVERAGE_GROUP CG WHERE CG.DELETED_FLAG !=-1 AND CG.COVERAGE_GROUP_ID > 0 ");
                    sSQL.Append(@" SELECT CG.COVERAGE_GROUP_ID, CG.COV_GROUP, CGT.GROUP_DESCRIPTION");
                    sSQL.AppendFormat(" FROM COVERAGE_GROUP CG INNER JOIN COVERAGE_GROUP_TEXT CGT ON CG.COVERAGE_GROUP_ID = CGT.COVERAGE_GROUP_ID WHERE CGT.LANGUAGE_CODE = {0} AND CG.DELETED_FLAG !=-1 AND CG.COVERAGE_GROUP_ID > 0 ",iLangcode);
                    //end -  Multilingual changes
                    sSQL.Append(" AND UPPER(CG.COV_GROUP) LIKE UPPER('" + p_sLookupString + "') AND UPPER(CG.COV_GROUP) >= UPPER('" + p_sIndex + "')");
                    //start:Added to filter Coverage group based on States value
                    if (!string.IsNullOrEmpty(p_sFilter))
                        sSQL.Append(" AND " + p_sFilter);
                    //end:Added to filter Coverage group based on States value
                    sSQL.Append(" AND ((CG.TRIGGER_DATE_FIELD IS NULL) ");
                   
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CG.EFF_START_DATE<='" + sToday + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sToday + "') ");
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CG.EFF_START_DATE<='" + sToday + "' AND CG.EFF_END_DATE>='" + sToday + "') ");

                    if (!string.IsNullOrEmpty(sDateOfEvent))
                    {
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CG.EFF_START_DATE<='" + sDateOfEvent + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sDateOfEvent + "') ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CG.EFF_START_DATE<='" + sDateOfEvent + "' AND CG.EFF_END_DATE>='" + sDateOfEvent + "') ");

                    }
                    if (!string.IsNullOrEmpty(sDateOfClaim))
                    {
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CG.EFF_START_DATE<='" + sDateOfClaim + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sDateOfClaim + "') ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CG.EFF_START_DATE<='" + sDateOfClaim + "' AND CG.EFF_END_DATE>='" + sDateOfClaim + "') ");
                    }
                    if (!string.IsNullOrEmpty(sDateOfPolicy))
                    {
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CG.EFF_START_DATE<='" + sDateOfPolicy + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CG.EFF_START_DATE<='" + sDateOfPolicy + "' AND CG.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                    }

                    sSQL.Append(") ORDER BY CG.COV_GROUP");
                }
                //end:added by Nitin goel, MITS 30910, 08/22/2013
                else if (p_sLookupType.ToUpper().Equals("CODE.TRANS_TYPES"))
                {
                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,C2.SHORT_CODE");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID");
                    sSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" + p_sLookupType.Substring(5).ToUpper() + "' ");
                    if (p_sLOB.Equals(string.Empty) || p_sLOB.Equals("0"))
                        sSQL.Append(" ");
                    else
                        sSQL.Append(" AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sLOB + ")");
                    sSQL.Append(" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ");
                    sSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ");
                    sSQL.Append(" AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ");
                    sSQL.Append(" AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' ");
                    //TR2566- BRS transaction type lookup gives problem.. 
                    if (p_sLOB != "")
                        sSQL.Append(" AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) ");
                    if (p_lDeptEId > 0)
                    {
                        sSQL.Append("	AND (CODES.ORG_GROUP_EID=" + p_lDeptEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lFacEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lLocEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lDivEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lRegEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lOprEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lComEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lCliEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID IS NULL ");
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                    }
                    //trans_type is also dependent upon currentdate,event date, claim date and policy transaction date, policy effective date
                    // for current mits transaction types need to be filtered on system date
                    EvaluateTriggerDates(sSQL, p_sFormName, sToday, p_sTriggerDate, p_sEventDate, p_sSessionClaimId, objConn, null, p_sLOB);
                    sSQL.Append(" ORDER BY CODES.SHORT_CODE");

                }
                //added by rkaur7
                else if (p_sLookupType.ToUpper().Equals("CODE.RESERVE_TYPE"))
                {
                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,CODES.SHORT_CODE");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES ");
                    sSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" + p_sLookupType.Substring(5).ToUpper() + "' ");
                    if (p_sLOB.Equals(string.Empty) || p_sLOB.Equals("0"))
                        sSQL.Append(" ");
                    else
                    {// 06/18/2007 MITS : 9499   by Umesh
                        ColLobSettings objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                        int iLOB = Conversion.ConvertStrToInteger(p_sLOB);
                        int iClaimId = Conversion.ConvertStrToInteger(p_sSessionClaimId);
                        if (objColLobSettings[iLOB].ResByClmType == true && iClaimId != 0 && p_sSessionClaimId != "")
                        {
                            //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString,
                            //  "SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimId))
                            //{
                            //    if (objRdr.Read())
                            //    {
                            int iClaimTypeCode = 0;
                            iClaimTypeCode = objConn.ExecuteInt("SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimId);
                            sSQL.Append(" AND CODES.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE=" + p_sLOB + " AND CLAIM_TYPE_CODE =" + iClaimTypeCode + ")");
                            //    }

                            //    objRdr.Close();
                            //}

                        } // END MITS 9499
                        else

                            sSQL.Append(" AND CODES.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sLOB + ")");
                    }
                    //else
                    //  sSQL.Append(" AND CODES.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sLOB + ")");
                    sSQL.Append(" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ");
                    sSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ");
                    sSQL.Append(" AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ");
                    sSQL.Append(" AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' ");
                    //TR2566- BRS transaction type lookup gives problem.. 
                    if (p_sLOB != "")
                        sSQL.Append(" AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) ");
                    if (p_lDeptEId > 0)
                    {
                        sSQL.Append("	AND (CODES.ORG_GROUP_EID=" + p_lDeptEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lFacEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lLocEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lDivEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lRegEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lOprEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lComEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lCliEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID IS NULL ");
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                    }
                    //trans_type is also dependent upon currentdate,event date, claim date and policy transaction date, policy effective date
                    // for current mits transaction types need to be filtered on system date
                    EvaluateTriggerDates(sSQL, p_sFormName, sToday, p_sTriggerDate, p_sEventDate, p_sSessionClaimId, objConn, null, p_sLOB);
                    sSQL.Append(" ORDER BY CODES.SHORT_CODE");

                }

                //end rkaur7
                else if (p_sLookupType.ToUpper().Equals("CODE.RM_SYS_USERS"))
                {
                    sConnectionString = SecurityDatabase.GetSecurityDsn(m_iClientId);
                    bIsSecurityDB = true;

                    if (bOracle)
                    {
                        sSQL.Append("SELECT USER_TABLE.USER_ID,USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                        sSQL.Append(" FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID");
                        sSQL.Append(" AND USER_DETAILS_TABLE.DSNID = " + p_sSessionDsnId);
                        /*	sSQL.Append(" AND (UPPER(USER_DETAILS_TABLE.LOGIN_NAME) LIKE '" + p_sLookupString + "' OR UPPER(USER_DETAILS_TABLE.LOGIN_NAME) LIKE '" + p_sLookupString.Replace("%", "*") + "')");
                            sSQL.Append(" AND UPPER(USER_DETAILS_TABLE.LOGIN_NAME) >= '" + p_sIndex.ToUpper() + "'");
                            sSQL.Append(" ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                        */
                        sSQL.Append(" AND (USER_DETAILS_TABLE.LOGIN_NAME LIKE '" + p_sLookupString + "' OR USER_DETAILS_TABLE.LOGIN_NAME LIKE '" + p_sLookupString.Replace("%", "*") + "')");
                        //sSQL.Append(" AND UPPER(USER_DETAILS_TABLE.LOGIN_NAME) >= '" + p_sIndex.ToUpper() + "'");
                        sSQL.Append(" ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                    }
                    else
                    {
                        sSQL.Append("SELECT USER_TABLE.USER_ID,USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                        sSQL.Append(" FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID");
                        sSQL.Append(" AND USER_DETAILS_TABLE.DSNID = " + p_sSessionDsnId);
                        sSQL.Append(" AND (USER_DETAILS_TABLE.LOGIN_NAME LIKE '" + p_sLookupString + "' OR USER_DETAILS_TABLE.LOGIN_NAME LIKE '" + p_sLookupString.Replace("%", "*") + "')");
                        sSQL.Append(" AND USER_DETAILS_TABLE.LOGIN_NAME >= '" + p_sIndex + "'");
                        sSQL.Append(" ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                    }
                }
                else if (p_sLookupType.ToUpper().Equals("CODE.CLAIM_TYPE"))
                {
                    if (p_sTypeLimits.Trim().Length > 0)
                    {
                        sarrSplit = p_sTypeLimits.Split('|');
                        for (int i = 0; i < sarrSplit.Length; i++)
                        {
                            sarrSplit1 = sarrSplit[i].Split(',');
                            if (Conversion.ConvertStrToBool(sarrSplit1[1]) == false && p_sLOB == sarrSplit1[3])
                                sLimitsSQL = sLimitsSQL + "," + sarrSplit1[0];
                        }
                        if (sLimitsSQL.Trim().Length > 0)
                            sLimitsSQL = " AND CODES.CODE_ID NOT IN (" + sLimitsSQL.Substring(2) + ") ";
                    }
                    else
                        sLimitsSQL = string.Empty;
                    //MITS 11246 :Umesh
                    if (p_bDescriptionSearch)
                    {
                        if (bOracle)
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE || ' ' ||CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ORDER BY CODES.SHORT_CODE");
                        else
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE + ' ' +CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ORDER BY CODES.SHORT_CODE");
                    }
                    else
                    {
                        if (bOracle)
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ORDER BY CODES.SHORT_CODE");
                        else
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ORDER BY CODES.SHORT_CODE");

                    }
                }
                else if (p_sLookupType.ToUpper().Equals("CODE.EVENT_TYPE"))
                {
                    if (p_sTypeLimits.Trim().Length > 0)
                    {
                        sarrSplit = p_sTypeLimits.Split('|');
                        for (int i = 0; i < sarrSplit.Length; i++)
                        {
                            sarrSplit1 = sarrSplit[i].Split(',');
                            if (Conversion.ConvertStrToBool(sarrSplit1[1]) == false)
                                sLimitsSQL = sLimitsSQL + "," + sarrSplit1[0];
                        }
                        if (sLimitsSQL.Trim().Length > 0)
                            sLimitsSQL = " AND CODES.CODE_ID NOT IN (" + sLimitsSQL.Substring(2) + ") ";
                    }
                    else
                        sLimitsSQL = string.Empty;
                    //MITS 11246 :Umesh
                    //Mcic changes  removed "ORDER BY CODES.SHORT_CODE");" from end of the queries by Pankaj
                    if (p_bDescriptionSearch)
                    {
                        if (bOracle)
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE || ' ' ||CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ORDER BY CODES.SHORT_CODE");
                        else
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE + ' ' +CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ORDER BY CODES.SHORT_CODE");
                    }
                    else
                    {
                        if (bOracle)
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ORDER BY CODES.SHORT_CODE");
                        else
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ORDER BY CODES.SHORT_CODE");

                    }

                }
                else if ((p_sLookupType.Length > 4) && (p_sLookupType.ToUpper().Substring(0, 5).Equals("CODE.")))
                {    //MITS 11246 : Umesh
                    //if (p_bDescriptionSearch)
                    //{
                    //    if (bOracle)
                    //        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE || ' ' ||CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ORDER BY CODES.SHORT_CODE");
                    //    else
                    //        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE + ' ' +CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "' ORDER BY CODES.SHORT_CODE");
                    //}
                    //else 
                    //{
                    //    if (bOracle)
                    //        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE ) LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ORDER BY CODES.SHORT_CODE");
                    //    else
                    //        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "' ORDER BY CODES.SHORT_CODE");

                    //}

                    //Additional Custom Filter Clause. //MCIC Changes 01/24/08 Pankaj, queries above were also changed to take care of the stuff.
                    if (p_bDescriptionSearch)
                    {
                        if (bOracle)
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE || ' ' ||CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "'");
                        else
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE + ' ' +CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                    }
                    else
                    {
                        if (bOracle)
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE ) LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "'");
                        else
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "'");

                    }
                    //PJS MITS 11571,03-11-2008 - Adding code for filtering codes     
                    if (sLOBSQL != string.Empty)
                        sSQL.Append(sLOBSQL);
                    if (p_lDeptEId > 0)
                    {
                        sSQL.Append("	AND (CODES.ORG_GROUP_EID=" + p_lDeptEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lFacEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lLocEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lDivEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lRegEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lOprEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lComEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lCliEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID IS NULL ");
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                    }
                    else
                    {
                        sSQL.Append("	AND (CODES.ORG_GROUP_EID IS NULL ");
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                    }
                    switch (p_sFormName.ToLower())
                    {
                        case "":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                            break;
                        case "lineofbuscodeselfinsured":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                            break;
                        case "event":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "claimgc":
                        case "claimwc":
                        case "claimva":
                        case "claimdi":
                        //Shruti for 11708
                        case "claimant":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            if (p_sEventDate.Length > 0)
                            {
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sEventDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sEventDate + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sEventDate + "' AND CODES.EFF_END_DATE>='" + p_sEventDate + "') ");
                            }
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "policy":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "funds":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "employee":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                    }

                    //PJS MITS 11571,03-11-2008, Adding code above

                    if (p_sFilter != "")
                        sSQL.Append(" AND " + p_sFilter);
                    sSQL.Append(" ORDER BY CODES.SHORT_CODE");

                }
                else if ((p_sLookupType.Length > 5) && (p_sLookupType.ToUpper().Substring(0, 6).Equals("ENTITY")))
                {
                    if (p_sLookupType.IndexOf(".") > 0)
                    {
                        //avipinsrivas Start : Worked for Jira-340
                        string sSystemTableName = p_sLookupType.Substring(7);
                        bool bOracleCaseInSensitive = m_bIsOracle & ((objSysSetting.OracleCaseIns == 1 || objSysSetting.OracleCaseIns == -1) ? true : false);
                        //Specific Search for Entity Type
                        //if (bOracle)
                        //    sSQL.Append("SELECT ENTITY_ID, LAST_NAME, FIRST_NAME FROM ENTITY,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(7).ToUpper() + "' AND GLOSSARY.TABLE_ID=ENTITY.ENTITY_TABLE_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND UPPER(LAST_NAME) LIKE '" + p_sLookupString + "' AND UPPER(LAST_NAME) >= '" + p_sIndex.ToUpper() + "' ORDER BY LAST_NAME");
                        //else
                        //    sSQL.Append("SELECT ENTITY_ID, LAST_NAME, FIRST_NAME FROM ENTITY,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(7) + "' AND GLOSSARY.TABLE_ID=ENTITY.ENTITY_TABLE_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND LAST_NAME LIKE '" + p_sLookupString + "' AND LAST_NAME >= '" + p_sIndex + "' ORDER BY LAST_NAME");
                        sSQL.Append(" SELECT E.ENTITY_ID, E.LAST_NAME, E.FIRST_NAME ");
                        sSQL.Append(" FROM ENTITY E ");
                        if (!(objSysSetting.UseEntityRole && !objCache.FromOrgHierarchy(sSystemTableName)))     //avipinsrivas Start : Worked for 15191 (For Epic 7767 and Story 13197)
                            sSQL.Append(" INNER JOIN GLOSSARY G ON E.ENTITY_TABLE_ID = G.TABLE_ID ");
                        if(bOracleCaseInSensitive)
                            sSQL.Append(" WHERE UPPER(G.SYSTEM_TABLE_NAME)='" + sSystemTableName.ToUpper() + "' ");
                        else
                            sSQL.Append(" WHERE G.SYSTEM_TABLE_NAME='" + sSystemTableName + "' ");
                        sSQL.Append(" AND (E.DELETED_FLAG = 0 OR E.DELETED_FLAG IS NULL) ");
                        if (bOracleCaseInSensitive)
                        {
                            sSQL.Append(" AND UPPER(E.LAST_NAME) LIKE '" + p_sLookupString + "' ");
                            sSQL.Append(" AND UPPER(E.LAST_NAME) >= '" + p_sIndex.ToUpper() + "' ");
                        }
                        else
                        {
                            sSQL.Append(" AND E.LAST_NAME LIKE '" + p_sLookupString + "' ");
                            sSQL.Append(" AND E.LAST_NAME >= '" + p_sIndex + "' ");
                        }
                        sSQL.Append(" ORDER BY E.LAST_NAME ");
                        sSQL.Append("");
                        //avipinsrivas End
                    }
                    else
                    {
                        // All Entities search		
                        if (bOracle)
                            sSQL.Append("SELECT ENTITY_ID, LAST_NAME, FIRST_NAME FROM ENTITY WHERE (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND UPPER(LAST_NAME) LIKE '" + p_sLookupString + "' AND UPPER(LAST_NAME) >= '" + p_sIndex.ToUpper() + "' ORDER BY LAST_NAME");
                        else
                            sSQL.Append("SELECT ENTITY_ID, LAST_NAME, FIRST_NAME FROM ENTITY WHERE (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND LAST_NAME LIKE '" + p_sLookupString + "' AND LAST_NAME >= '" + p_sIndex + "' ORDER BY LAST_NAME");
                    }
                }
                else if (p_sLookupType.ToUpper().Equals("VEHICLE"))
                {
                    if (bOracle)
                        sSQL.Append("SELECT UNIT_ID, VIN, VEHICLE_MAKE, VEHICLE_MODEL, VEHICLE_YEAR FROM VEHICLE WHERE UPPER(VIN) LIKE '" + p_sLookupString + "' AND UPPER(VIN) >= '" + p_sIndex.ToUpper() + "' ORDER BY VIN");
                    else
                        sSQL.Append("SELECT UNIT_ID, VIN, VEHICLE_MAKE, VEHICLE_MODEL, VEHICLE_YEAR FROM VEHICLE WHERE VIN LIKE '" + p_sLookupString + "' AND VIN >= '" + p_sIndex + "' ORDER BY VIN");
                }
                else if (p_sLookupType.ToUpper().Equals("CLAIM"))
                {
                    if (bOracle)
                        sSQL.Append("SELECT CLAIM_ID, CLAIM_NUMBER, EVENT_NUMBER FROM CLAIM WHERE UPPER(CLAIM_NUMBER) LIKE '" + p_sLookupString + "' AND UPPER(CLAIM_NUMBER) >= '" + p_sIndex.ToUpper() + "' ORDER BY CLAIM_NUMBER");
                    else
                        sSQL.Append("SELECT CLAIM_ID, CLAIM_NUMBER, EVENT_NUMBER FROM CLAIM WHERE CLAIM_NUMBER LIKE '" + p_sLookupString + "' AND CLAIM_NUMBER >= '" + p_sIndex + "' ORDER BY CLAIM_NUMBER");
                }
                else if (p_sLookupType.ToUpper().Equals("EVENT"))
                {
                    if (bOracle)
                        sSQL.Append("SELECT EVENT_ID, EVENT_NUMBER, CODES_TEXT.CODE_DESC FROM EVENT,CODES_TEXT WHERE CODES_TEXT.CODE_ID=EVENT.EVENT_TYPE_CODE AND UPPER(EVENT_NUMBER) LIKE '" + p_sLookupString + "' AND UPPER(EVENT_NUMBER) >= '" + p_sIndex.ToUpper() + "' ORDER BY EVENT_NUMBER");
                    else
                        sSQL.Append("SELECT EVENT_ID, EVENT_NUMBER, CODES_TEXT.CODE_DESC FROM EVENT,CODES_TEXT WHERE CODES_TEXT.CODE_ID=EVENT.EVENT_TYPE_CODE AND EVENT_NUMBER LIKE '" + p_sLookupString + "' AND EVENT_NUMBER >= '" + p_sIndex + "' ORDER BY EVENT_NUMBER");
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("CodesListManager.QuickLookup.UnknownType", m_iClientId));
                }

                //get the codes xml after searching
                objQuickLookupXml = GetQuickLookupXML(sSQL.ToString(), sConnectionString, bIsSecurityDB);
            }
            catch (RMAppException p_objExp)
            {
                throw (p_objExp);
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.QuickLookup.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
            }
            return objQuickLookupXml;
        }

        //Ashish Ahuja : Mits 32888
        bool isMultiLingual()
        {
            if (string.IsNullOrEmpty(useMultiLingual))
            {
                //useMultiLingual = System.Configuration.ConfigurationSettings.AppSettings["UseMultiLingual"].ToString();
                useMultiLingual = RMConfigurationManager.GetAppSetting("UseMultiLingual").ToString();
            }
            if (useMultiLingual == "false")
            {
                return (false);
            }
            else
            {
                return (true);            
            }

        }


        /// Name		: QuickLookup
        /// Author		: Nikhil Kumar Garg
        /// Date Created: 13-May-2005
        ///*************************************************************************
        /// Amendment History
        ///*************************************************************************
        /// Date Amended   *   Amendment   *								Author
        ///*************************************************************************
        /// <summary>
        /// Gets the codes list for the quick lookup window by searching on the text
        /// typed by the user.
        /// </summary>
        /// <param name="p_sIndex">Number/Id</param>
        /// <param name="p_sLookupString">string to search</param>
        /// <param name="p_sLookupType">Type/Code table to search</param>
        /// <param name="p_sLOB">Line Of Business</param>
        /// <param name="p_sSessionDsnId">DSN Id stored in the session</param>
        /// <param name="p_sTypeLimits">Limits for Claim_Types or Event_Types</param>
        /// <param name="p_sFilter">Additional Filter Clause for Custom Code Lookup</param>
        /// <returns>XML Document containing the codes list</returns>
        public CodeListType QuickLookupData(string p_sIndex, string p_sLookupString, string p_sLookupType,
                                       string p_sLOB, string p_sJurisdiction, string p_sTypeLimits, string p_sSessionDsnId, bool p_bDescriptionSearch, string p_sFilter, string p_sSessionLOB, 
                                        long p_lDeptEId, string p_sFormName, string p_sTriggerDate, string p_sEventDate, string p_sTitle, string p_sSessionClaimId,
                                        string p_sOrgLevel, string p_sInsuredEid, string p_sEventId, string sParentCodeID, Int32 iCurrentPageNumber, string p_sPolicyId,
                                        string p_sCovTypecode, string p_sTransId, string p_sClaimantEid, string p_sIsFirstFinal, string sPolUnitRowId,string p_sPolicyLOB,
                                        string p_sCovgSeqNum, string p_sRsvStatusParent, string p_sTransSeqNum, int iLangCode, 
                                        string p_sFieldName, string p_sCoverageKey, string p_sLossTypeCode)//stara Mits 16667 
            //rupal: added p_sIsFFPmt for r8 first * final pmt enh
            
        {
            //XmlDocument objQuickLookupXml = null;		//xml to return
            string sLOBSQL = string.Empty;			//SQL string for LOB
            string sLimitsSQL = string.Empty;			//SQL string for Claim/Event Type
            string[] sarrSplit;						//used for parsing the limits
            string[] sarrSplit1;					//used for parsing the limits
            bool bOracleCaseInSensitive = m_bIsOracle;		//Whether case sensitive search is required for Oracle
            StringBuilder sSQL = new StringBuilder();	//SQL for searching for the code
            string sConnectionString = "";
            bool bIsSecurityDB = false;
            DbConnection objConn = null;
            //PJS MITS 11571-03-11-2008 : used for filtering 
            string sToday = string.Empty;		//string for Date
            int iMonthNow = 0;				    //current month
            int iDay = 0;						//current day
            long lFacEId = 0;					//Facility Id
            long lLocEId = 0;					//Location Id
            long lDivEId = 0;					//Division Id
            long lRegEId = 0;					//Region Id
            long lOprEId = 0;					//Operation Id
            long lComEId = 0;					//Company Id
            long lCliEId = 0;					//Client Id
            long lClaimantEid = 0;
            //PJS MITS 15220
            string sInsParentEid = string.Empty;
            //PJS MITS 15220
            string sLookupCode = string.Empty;
            string sLookupDesc = string.Empty;
            // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
            int iSessionClaimId = 0;
            bool bSuccess = false;
            lClaimantEid = Conversion.CastToType<int>(p_sClaimantEid, out bSuccess);
            iSessionClaimId = Conversion.CastToType<int>(p_sSessionClaimId, out bSuccess);
            // npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim

            CodeListType objCodeList = null;
            //rupal:start, for first & final payment
            long lPolCvgRoWId = 0;
            bool bConverted = false;
            bool bIsFirstFinal = false;
            bIsFirstFinal = Conversion.CastToType<bool>(p_sIsFirstFinal, out bConverted);
            SysSettings objSysSetting = new SysSettings(m_sConnectionString, m_iClientId); //Ash - cloud
            //rupal:end,  for first & final payment
            LocalCache objCache = null;
            string sLookupTemp = string.Empty;
            bool success;       //Ankit Start : Worked on MITS - 34657
            ColLobSettings objColLobSettings = null;
            try
            {
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objCache = new LocalCache(m_sConnectionString, m_iClientId);        //avipinsrivas Start : Worked for Jira-340
                objConn.Open();
                //Aman ML Change
                //string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
                //if (m_sLanguageCode.Equals(string.Empty))
                //{
                //    if (!m_DBId.Equals(string.Empty))
                m_sLanguageCode = iLangCode.ToString();// GetNLSCode(int.Parse(m_DBId)).ToString();

                //}
                //Aman ML Change
                sConnectionString = m_sConnectionString;
                bIsSecurityDB = false;

                //format string to search and append %
                p_sLookupString = Riskmaster.Common.CommonFunctions.HTMLCustomDecode(p_sLookupString);
                //rsushilaggar MITS 22195 Date 12/03/12
                if (p_sLookupString.Contains("\",\""))
                {
                    sLookupTemp = p_sLookupString;
                    //string[] temp = p_sLookupString.Split("\",\"");
                    sLookupCode = p_sLookupString.Substring(1, p_sLookupString.IndexOf("\",\"")).Replace("\"", "");
                    sLookupDesc = p_sLookupString.Substring(p_sLookupString.IndexOf("\",\"") + 3).Replace("\"", "");
                    p_sLookupString = sLookupCode;

                    p_sLookupString = p_sLookupString.Trim();
                    p_sLookupString = p_sLookupString.Replace("'", "''");
                    p_sLookupString = p_sLookupString.Replace("%", "");

                    sLookupDesc = sLookupDesc.Trim();
                    sLookupDesc = sLookupDesc.Replace("'", "''");
                    sLookupDesc = sLookupDesc.Replace("%", "");

                    p_sLookupString = "%" + p_sLookupString + "%";
                    sLookupDesc = "%" + sLookupDesc + "%";
                }
                else
                {
                    p_sLookupString = p_sLookupString.Trim();
                    p_sLookupString = p_sLookupString.Replace("'", "''");
                    p_sLookupString = p_sLookupString.Replace("%", "");

                    if (p_bDescriptionSearch)
                    {
                        p_sLookupString = "%" + p_sLookupString + "%";
                    }
                    else
                    {
                        p_sLookupString = p_sLookupString + "%";
                        //Ashish Ahuja Mits 33867 start
                        if (p_sLookupString.StartsWith("*"))
                        {
                            p_sLookupString = p_sLookupString.Remove(0,1);
                            p_sLookupString = "%" + p_sLookupString;
                        }
                        //Ashish Ahuja Mits 33867 end
                    }
                 
                }
                //PJS MITS 11571-03-11-2008 : calculating dates & department for effective date filtering 
                iMonthNow = DateTime.Now.Month;
                iDay = DateTime.Now.Day;

                sToday = DateTime.Now.Year.ToString();

                //for the date string
                if (iMonthNow < 10)
                    sToday = sToday + "0" + iMonthNow.ToString();
                else
                    sToday = sToday + iMonthNow.ToString();

                if (iDay < 10)
                    sToday = sToday + "0" + iDay.ToString();
                else
                    sToday = sToday + iDay.ToString();
                //If page is child of event , Dept_Id would be Dept_Id of Event
                //MITS 17449 : Umesh
                if (p_sEventId != "" && p_sEventId != "0" && "piemployee" == p_sFormName)
                {
                    int iEventId = Conversion.ConvertStrToInteger(p_sEventId);

                    //using (DbReader objRdr = )
                    //{

                    //    if (objRdr != null)
                    //    {
                    //        if (objRdr.Read())
                    //        {
                    long lDeptEId = objConn.ExecuteInt("SELECT DEPT_EID FROM EVENT WHERE EVENT_ID =" + iEventId); //Conversion.ConvertObjToInt64(objRdr["DEPT_EID"]);
                                if (lDeptEId > 0)
                                    p_lDeptEId = lDeptEId;

                    //        }
                    //    }
                    //}
                }
                if (!string.IsNullOrEmpty(p_sTransId) && !string.Equals(p_sTransId.Trim(),"0"))
                {
                    //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, ))
                    //{
                    //    if (objRdr != null)
                    //    {
                    //        if (objRdr.Read())
                    //        {
                    lClaimantEid = objConn.ExecuteInt("SELECT CLAIMANT_EID FROM FUNDS WHERE TRANS_ID =" + p_sTransId);
                    //        }
                    //    }
                    //}
                }
                //MITS 17449 :End
                //if DepartmentId is passed then get the information about the Facility, Location etc. also.
                if (p_lDeptEId > 0)
                    GetOrgEid(p_lDeptEId, ref lFacEId, ref lLocEId, ref lDivEId, ref lRegEId, ref lOprEId,
                        ref lComEId, ref lCliEId,objConn);


                //PJS MITS 15220 
                sInsParentEid = GetParentEid(p_sInsuredEid);

                //PJS MITS 15220

                //BSB 05.12.2006 Fix for Oracle comparison to empty string in where clause 
                // fails to yeild any query results
                if (m_bIsOracle && p_sIndex == "")
                    p_sIndex = " ";

                //PJS MITS 11571-03-11-2008 
                //find out whether we need case insensitive search for Oracle
                //BSB 05.12.2006 We need to know if we're running on Oracle - not just if a sysparm is set.
                //Parijat: We need to know if its oracle then the value of sysparm therefore '&='not just '|='
                //bOracleCaseInSensitive = m_bIsOracle & IsCaseInSensitveSearch();
                bOracleCaseInSensitive = m_bIsOracle & ((objSysSetting.OracleCaseIns == 1 || objSysSetting.OracleCaseIns == -1) ? true : false);
                //BSB 05.18.2006 Fix for Oracle case insensitive comparison.
                // Leaving p_sLookupString mixed case failed to yeild any quick lookup query results.
                if (bOracleCaseInSensitive)
                {
                    p_sLookupString = p_sLookupString.ToUpper();
                    sLookupDesc = sLookupDesc.ToUpper();
                }

                //create sql string for searching
                if (Conversion.ConvertStrToLong(p_sLOB) > 0)
                    sLOBSQL = " AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) ";
                else
                    sLOBSQL = string.Empty;
                if (p_sLookupType.ToUpper().Equals("EMPLOYEE.LASTNAME"))
                {
                    if (bOracleCaseInSensitive)
                        sSQL.Append("SELECT EMPLOYEE.EMPLOYEE_EID, EMPLOYEE.EMPLOYEE_NUMBER, ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM EMPLOYEE,ENTITY WHERE EMPLOYEE.EMPLOYEE_EID=ENTITY.ENTITY_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND UPPER(ENTITY.LAST_NAME) LIKE '" + p_sLookupString + "' ORDER BY EMPLOYEE.EMPLOYEE_NUMBER");
                    else
                        sSQL.Append("SELECT EMPLOYEE.EMPLOYEE_EID, EMPLOYEE.EMPLOYEE_NUMBER, ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM EMPLOYEE,ENTITY WHERE EMPLOYEE.EMPLOYEE_EID=ENTITY.ENTITY_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND ENTITY.LAST_NAME LIKE '" + p_sLookupString + "' ORDER BY EMPLOYEE.EMPLOYEE_NUMBER");
                }
                else if (p_sLookupType.ToUpper().Equals("EMPLOYEES") || p_sLookupType.ToUpper().Equals("EMPLOYEE"))
                {
                    if (bOracleCaseInSensitive)
                        sSQL.Append("SELECT EMPLOYEE.EMPLOYEE_EID, EMPLOYEE.EMPLOYEE_NUMBER, ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM EMPLOYEE,ENTITY WHERE EMPLOYEE.EMPLOYEE_EID=ENTITY.ENTITY_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND UPPER(EMPLOYEE.EMPLOYEE_NUMBER) LIKE '" + p_sLookupString + "' AND UPPER(EMPLOYEE.EMPLOYEE_NUMBER) >= '" + p_sIndex.ToUpper() + "' ORDER BY EMPLOYEE.EMPLOYEE_NUMBER");
                    else
                        sSQL.Append("SELECT EMPLOYEE.EMPLOYEE_EID, EMPLOYEE.EMPLOYEE_NUMBER, ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM EMPLOYEE,ENTITY WHERE EMPLOYEE.EMPLOYEE_EID=ENTITY.ENTITY_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND EMPLOYEE.EMPLOYEE_NUMBER LIKE '" + p_sLookupString + "' AND EMPLOYEE.EMPLOYEE_NUMBER >= '" + p_sIndex + "' ORDER BY EMPLOYEE.EMPLOYEE_NUMBER");
                }
                else if (p_sLookupType.ToUpper().Equals("CODE.ORGH"))
                {
                    //pmittal5  MITS:12369  08/28/08
                    //if (bOracle)
                    //    sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID=1012 AND UPPER(ABBREVIATION) LIKE '" + p_sLookupString + "' AND UPPER(ABBREVIATION) >= '" + p_sIndex.ToUpper() + "' ORDER BY ABBREVIATION");
                    //else
                    //    sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID=1012 AND ABBREVIATION LIKE '" + p_sLookupString + "' AND ABBREVIATION >= '" + p_sIndex + "' ORDER BY ABBREVIATION");
                    //Ashish Ahuja Mits 33867 start : removed LTRIM and RTRIM function from query 
                    if (p_sOrgLevel != null && p_sOrgLevel != "" && p_sOrgLevel.ToUpper() != "ALL")//MITS 18733:Changing If condition for "ALL" case to work properly
                    {
                        int iEntTableId = GetEntityTableId(p_sOrgLevel);
                        if (iEntTableId != 1012) //MITS 18733:As discussed with Amit, adding If clause to allow execution of prior query except for department.So,Uncommenting the query for MITS:12369
                        {
                            //avipinsrivas Start : Worked for Jira-340
                            //asharma326 MITS 
                            //var EntityQuery = "";
                            //if (iEntTableId == 0)
                            //    EntityQuery = " IN (1005,1006,1007,1008,1009,1010,1011,1012) ";
                            //else
                            //{
                            //    EntityQuery = " = " + iEntTableId;
                            //}
                            //if (bOracleCaseInSensitive)
                                
                            //    //sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND ENTITY_TABLE_ID= " + iEntTableId + " AND (LTRIM(RTRIM(UPPER(ABBREVIATION))) LIKE '" + p_sLookupString + "' OR LTRIM(RTRIM(UPPER(LAST_NAME))) LIKE '" + p_sLookupString + "') AND UPPER(ABBREVIATION) >= '" + p_sIndex.ToUpper() + "' ORDER BY ABBREVIATION");
                            //    sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND ENTITY_TABLE_ID  " + EntityQuery + " AND (UPPER(ABBREVIATION) LIKE '" + p_sLookupString + "' OR UPPER(LAST_NAME) LIKE '" + p_sLookupString + "') AND UPPER(ABBREVIATION) >= '" + p_sIndex.ToUpper() + "' ORDER BY ABBREVIATION");
                            //else
                            //    //sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND ENTITY_TABLE_ID=" + iEntTableId + " AND (LTRIM(RTRIM(ABBREVIATION)) LIKE '" + p_sLookupString + "' OR LTRIM(RTRIM(LAST_NAME)) LIKE '" + p_sLookupString + "') AND ABBREVIATION >= '" + p_sIndex + "' ORDER BY ABBREVIATION");
                            //    sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND ENTITY_TABLE_ID " + EntityQuery + " AND (ABBREVIATION LIKE '" + p_sLookupString + "' OR LAST_NAME LIKE '" + p_sLookupString + "') AND ABBREVIATION >= '" + p_sIndex + "' ORDER BY ABBREVIATION");
                            ////sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND ENTITY_TABLE_ID=" + iEntTableId + " AND ABBREVIATION LIKE '" + p_sLookupString + "' AND ABBREVIATION >= '" + p_sIndex + "' ORDER BY ABBREVIATION");
                            ////(LTRIM(RTRIM(G.ABBREVIATION)) LIKE '1N%' OR LTRIM(RTRIM(G.LAST_NAME)) LIKE '1N%')

                            sSQL.Append(" SELECT E.ENTITY_ID,E.ABBREVIATION,E.LAST_NAME ");
                            sSQL.Append(" FROM ENTITY E ");
                            sSQL.Append(" WHERE (E.DELETED_FLAG = 0 OR E.DELETED_FLAG IS NULL) ");
                            if (int.Equals(iEntTableId, 0))
                                sSQL.Append(" AND E.ENTITY_TABLE_ID IN (1005,1006,1007,1008,1009,1010,1011,1012) ");
                            else
                            {
                                if (!(objSysSetting.UseEntityRole && !objCache.FromOrgHierarchy(iEntTableId)))
                                    sSQL.Append(" AND E.ENTITY_TABLE_ID = " + iEntTableId + "");
                            }
                            if (bOracleCaseInSensitive)
                            {
                                sSQL.Append(" AND (UPPER(E.ABBREVIATION) LIKE '" + p_sLookupString + "' OR UPPER(E.LAST_NAME) LIKE '" + p_sLookupString + "') ");
                                sSQL.Append(" AND UPPER(E.ABBREVIATION) >= '" + p_sIndex.ToUpper() + "' ");
                            }
                            else
                            {
                                sSQL.Append(" AND (E.ABBREVIATION LIKE '" + p_sLookupString + "' OR E.LAST_NAME LIKE '" + p_sLookupString + "') ");
                                sSQL.Append(" AND E.ABBREVIATION >= '" + p_sIndex + "' ");
                            }
                            sSQL.Append(" ORDER BY ABBREVIATION ");
                            //avipinsrivas End
                        }
                        else
{
                            //asingh263:MITS 25317 starts
                            //abansal23: on 05/12/2009 for MITS 14847
                            // akaushik5 Changed for MITS 36567/JIRA-494  Starts
                            //if (bOracleCaseInSensitive)
                            //    //sSQL.Append("SELECT A.*,B.ABBREVIATION,B.LAST_NAME FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE (B.DELETED_FLAG = 0 OR B.DELETED_FLAG IS NULL) AND B.ENTITY_TABLE_ID= " + iEntTableId + " AND (LTRIM(RTRIM(UPPER(B.ABBREVIATION))) LIKE '" + p_sLookupString + "' OR LTRIM(RTRIM(UPPER(B.LAST_NAME))) LIKE '" + p_sLookupString + "') AND UPPER(B.ABBREVIATION) >= '" + p_sIndex + "' ORDER BY B.ABBREVIATION");
                            //    sSQL.Append("SELECT A.*,B.ABBREVIATION,B.LAST_NAME FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE (B.DELETED_FLAG = 0 OR B.DELETED_FLAG IS NULL) AND B.ENTITY_TABLE_ID= " + iEntTableId + "   AND ( NULLIF(B.EFF_END_DATE,'NULL')  >  " + DateTime.Now.ToString("yyyyMMdd") + " OR (B.EFF_END_DATE IS NULL)) AND (UPPER(B.ABBREVIATION) LIKE '" + p_sLookupString + "' OR UPPER(B.LAST_NAME) LIKE '" + p_sLookupString + "') AND UPPER(B.ABBREVIATION) >= '" + p_sIndex + "' ORDER BY B.ABBREVIATION");
                            //else
                            //sSQL.Append("SELECT A.*,B.ABBREVIATION,B.LAST_NAME FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE (B.DELETED_FLAG = 0 OR B.DELETED_FLAG IS NULL) AND B.ENTITY_TABLE_ID= " + iEntTableId + " AND (LTRIM(RTRIM(B.ABBREVIATION)) LIKE '" + p_sLookupString + "' OR LTRIM(RTRIM(B.LAST_NAME)) LIKE '" + p_sLookupString + "') AND B.ABBREVIATION >= '" + p_sIndex + "' ORDER BY B.ABBREVIATION");
                            //sSQL.Append("SELECT A.*,B.ABBREVIATION,B.LAST_NAME FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE (B.DELETED_FLAG = 0 OR B.DELETED_FLAG IS NULL) AND B.ENTITY_TABLE_ID= " + iEntTableId + " AND ( NULLIF(B.EFF_END_DATE,'NULL')  >  " + DateTime.Now.ToString("yyyyMMdd") + " OR (B.EFF_END_DATE IS NULL)) AND (B.ABBREVIATION LIKE '" + p_sLookupString + "' OR B.LAST_NAME LIKE '" + p_sLookupString + "') AND B.ABBREVIATION >= '" + p_sIndex + "' ORDER BY B.ABBREVIATION");
                            sSQL.Append("SELECT A.*,D.ABBREVIATION,D.LAST_NAME FROM ORG_HIERARCHY A INNER JOIN ENTITY D ON A.DEPARTMENT_EID = D.ENTITY_ID");
                            bool filterRecords = this.GetOrgPreferenceFilter();
                            if (filterRecords)
                            {
                                sSQL.AppendFormat(" INNER JOIN ENTITY F ON A.FACILITY_EID = F.ENTITY_ID");
                                sSQL.AppendFormat(" INNER JOIN ENTITY L ON A.LOCATION_EID = L.ENTITY_ID");
                                sSQL.AppendFormat(" INNER JOIN ENTITY DIV ON A.LOCATION_EID = DIV.ENTITY_ID");
                                sSQL.AppendFormat(" INNER JOIN ENTITY R ON A.LOCATION_EID = R.ENTITY_ID");
                                sSQL.AppendFormat(" INNER JOIN ENTITY O ON A.LOCATION_EID = O.ENTITY_ID");
                                sSQL.AppendFormat(" INNER JOIN ENTITY CO ON A.LOCATION_EID = CO.ENTITY_ID");
                                sSQL.AppendFormat(" INNER JOIN ENTITY CL ON A.LOCATION_EID = CL.ENTITY_ID");
                            }
                            sSQL.AppendFormat(" WHERE (D.DELETED_FLAG = 0 OR D.DELETED_FLAG IS NULL) AND D.ENTITY_TABLE_ID= {0}", iEntTableId);
                            if (filterRecords)
                            {
                                this.GetTriggerCriteria(p_sFormName, p_sTriggerDate, sSQL, "D", sToday);
                                this.GetTriggerCriteria(p_sFormName, p_sTriggerDate, sSQL, "F", sToday);
                                this.GetTriggerCriteria(p_sFormName, p_sTriggerDate, sSQL, "L", sToday);
                                this.GetTriggerCriteria(p_sFormName, p_sTriggerDate, sSQL, "DIV", sToday);
                                this.GetTriggerCriteria(p_sFormName, p_sTriggerDate, sSQL, "R", sToday);
                                this.GetTriggerCriteria(p_sFormName, p_sTriggerDate, sSQL, "O", sToday);
                                this.GetTriggerCriteria(p_sFormName, p_sTriggerDate, sSQL, "CO", sToday);
                                this.GetTriggerCriteria(p_sFormName, p_sTriggerDate, sSQL, "CL", sToday);
                            }
                            if (bOracleCaseInSensitive)
                            {
                                sSQL.AppendFormat(" AND (UPPER(D.ABBREVIATION) LIKE '{0}' OR UPPER(D.LAST_NAME) LIKE '{0}') AND UPPER(D.ABBREVIATION) >= '{1}' ORDER BY D.ABBREVIATION", p_sLookupString, p_sIndex);
                            }
                            else
                            {
                                sSQL.AppendFormat(" AND (D.ABBREVIATION LIKE '{0}' OR D.LAST_NAME LIKE '{0}') AND D.ABBREVIATION >= '{1}' ORDER BY D.ABBREVIATION", p_sLookupString, p_sIndex);
                            }
                            // akaushik5 Changed for MITS 36567/JIRA-494 Ends
                            //asingh263:MITS 25317 ends 
                        }
                        //Ashish Ahuja Mits 33867 end : removed LTRIM and RTRIM function from query 
                    }
                    else
                    {
                        if (p_sOrgLevel.ToUpper() != "DEPARTMENT") //MITS 18733:As discussed with Amit, adding If clause to allow execution of prior query except for department.So,Uncommenting the query for MITS:12369
                        {
                            if (bOracleCaseInSensitive)
                                //sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND ENTITY_TABLE_ID BETWEEN 1005 AND 1012 AND (LTRIM(RTRIM(UPPER(ABBREVIATION))) LIKE '" + p_sLookupString + "' OR LTRIM(RTRIM(UPPER(LAST_NAME))) LIKE '" + p_sLookupString + "') AND UPPER(ABBREVIATION) >= '" + p_sIndex.ToUpper() + "' ORDER BY ABBREVIATION");
                                sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND ENTITY_TABLE_ID BETWEEN 1005 AND 1012 AND (UPPER(ABBREVIATION) LIKE '" + p_sLookupString + "' OR UPPER(LAST_NAME) LIKE '" + p_sLookupString + "') AND UPPER(ABBREVIATION) >= '" + p_sIndex.ToUpper() + "' ORDER BY ABBREVIATION");
                            else
                                //sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND ENTITY_TABLE_ID BETWEEN 1005 AND 1012 AND (LTRIM(RTRIM(ABBREVIATION)) LIKE '" + p_sLookupString + "' OR LTRIM(RTRIM(LAST_NAME)) LIKE '" + p_sLookupString + "') AND ABBREVIATION >= '" + p_sIndex + "' ORDER BY ABBREVIATION");
                                sSQL.Append("SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND ENTITY_TABLE_ID BETWEEN 1005 AND 1012 AND (ABBREVIATION LIKE '" + p_sLookupString + "' OR LAST_NAME LIKE '" + p_sLookupString + "') AND ABBREVIATION >= '" + p_sIndex + "' ORDER BY ABBREVIATION");

                        }
                        else
                        {
                            //abansal23: on 05/12/2009 for MITS 14847
                            if (bOracleCaseInSensitive)
                                //sSQL.Append("SELECT A.*,B.ABBREVIATION,B.LAST_NAME FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE (B.DELETED_FLAG = 0 OR B.DELETED_FLAG IS NULL) AND B.ENTITY_TABLE_ID BETWEEN 1005 AND 1012 AND (LTRIM(RTRIM(UPPER(B.ABBREVIATION))) LIKE '" + p_sLookupString + "' OR LTRIM(RTRIM(UPPER(B.LAST_NAME))) LIKE '" + p_sLookupString + "') AND UPPER(B.ABBREVIATION) >= '" + p_sIndex + "' AND (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL)  ORDER BY B.ABBREVIATION");
                                sSQL.Append("SELECT A.*,B.ABBREVIATION,B.LAST_NAME FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE (B.DELETED_FLAG = 0 OR B.DELETED_FLAG IS NULL) AND B.ENTITY_TABLE_ID BETWEEN 1005 AND 1012 AND (UPPER(B.ABBREVIATION) LIKE '" + p_sLookupString + "' OR UPPER(B.LAST_NAME) LIKE '" + p_sLookupString + "') AND UPPER(B.ABBREVIATION) >= '" + p_sIndex + "' AND (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL)  ORDER BY B.ABBREVIATION");
                            else
                                //sSQL.Append("SELECT A.*,B.ABBREVIATION,B.LAST_NAME FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE (B.DELETED_FLAG = 0 OR B.DELETED_FLAG IS NULL) AND B.ENTITY_TABLE_ID BETWEEN 1005 AND 1012 AND (LTRIM(RTRIM(B.ABBREVIATION)) LIKE '" + p_sLookupString + "' OR LTRIM(RTRIM(B.LAST_NAME)) LIKE '" + p_sLookupString + "') AND B.ABBREVIATION >= '" + p_sIndex + "' AND (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) ORDER BY B.ABBREVIATION");
                                sSQL.Append("SELECT A.*,B.ABBREVIATION,B.LAST_NAME FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE (B.DELETED_FLAG = 0 OR B.DELETED_FLAG IS NULL) AND B.ENTITY_TABLE_ID BETWEEN 1005 AND 1012 AND (B.ABBREVIATION LIKE '" + p_sLookupString + "' OR B.LAST_NAME LIKE '" + p_sLookupString + "') AND B.ABBREVIATION >= '" + p_sIndex + "' AND (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) ORDER BY B.ABBREVIATION");
                        }
                    }
                    //End - pmittal5
                    //Ashish Ahuja Mits 33867 end : removed LTRIM and RTRIM function from query 
                }
                else if (p_sLookupType.ToUpper().Equals("CODE.STATES"))
                {
                    if (bOracleCaseInSensitive)
                    {
                        if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                            sSQL.Append("SELECT STATE_ROW_ID, STATE_ID, STATE_NAME FROM STATES WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND UPPER(STATE_ID) LIKE '" + p_sLookupString + "' AND UPPER(STATE_ID) >= '" + p_sIndex.ToUpper() + "'");
                        //MITS:35039 START
                        else
                            sSQL.Append("SELECT STATE_ROW_ID, STATE_ID, STATE_NAME FROM STATES WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND ((UPPER(STATE_ID) LIKE '" + p_sLookupString + "' AND UPPER(STATE_ID) >= '" + p_sIndex.ToUpper() + "') OR (UPPER(STATE_NAME) LIKE '%" + p_sLookupString + "'))");
                        //MITS:35039 END
                    }
                    else
                    {
                        if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                            sSQL.Append("SELECT STATE_ROW_ID, STATE_ID, STATE_NAME FROM STATES WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND STATE_ID LIKE '" + p_sLookupString + "' AND STATE_ID >= '" + p_sIndex + "'");
                        //MITS:35039 START
                        else
                            sSQL.Append("SELECT STATE_ROW_ID, STATE_ID, STATE_NAME FROM STATES WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND ((STATE_ID LIKE '" + p_sLookupString + "' AND STATE_ID >= '" + p_sIndex + "' ) OR (STATE_NAME like '%" + p_sLookupString + "'))");
                        //MITS:35039 END
                    }
                }
   else if (p_sLookupType.ToUpper().Equals("CODE.COVERAGE_GROUP"))
                {
                    string sDateOfClaim = string.Empty;
                    string sDateOfEvent = string.Empty;
                    string sDateOfPolicy = string.Empty;
                    //Start -  Multilingual changes

                    int iLangcode = 0;
                    iLangcode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                    //end -  Multilingual changes      
                    if (iSessionClaimId > 0 && !(string.IsNullOrEmpty(p_sSessionClaimId) || p_sSessionClaimId.ToLower().Trim() == "undefined"))
                        GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy, objConn, objSysSetting, p_sLOB);
                    if (!(string.Compare(p_sPolicyId, "0") == 0) && !(string.IsNullOrEmpty(p_sPolicyId)))
                        sDateOfPolicy = DbFactory.ExecuteAsType<string>(m_sConnectionString, "SELECT EFFECTIVE_DATE FROM POLICY WHERE POLICY_ID = " + p_sPolicyId);

                    //Start: Added by Sumit Agarwal for NI on 11/10/2014 to get the values for sDateOfEvent and sDateOfClaim (if they are blank)
                    if (string.IsNullOrEmpty(sDateOfEvent))
                        sDateOfEvent = DbFactory.ExecuteAsType<string>(m_sConnectionString, String.Format("SELECT E.DATE_OF_EVENT FROM EVENT E INNER JOIN CLAIM C ON E.EVENT_ID = C.EVENT_ID AND C.CLAIM_ID =  {0}", p_sSessionClaimId));
                    if (string.IsNullOrEmpty(sDateOfClaim))
                        sDateOfClaim = DbFactory.ExecuteAsType<string>(m_sConnectionString, String.Format("SELECT DATE_OF_CLAIM FROM CLAIM WHERE CLAIM_ID = {0}", p_sSessionClaimId));
                    //End: Added by Sumit Agarwal for NI on 11/10/2014 to get the values for sDateOfEvent and sDateOfClaim (if they are blank)

                    sSQL.Length = 0;
                    //start -  Multilingual changes
                    //sSQL.Append(@" SELECT CG.COVERAGE_GROUP_ID, CG.COV_GROUP, CG.COV_GROUP_DESC");
                    //sSQL.Append(" FROM COVERAGE_GROUP CG WHERE CG.DELETED_FLAG !=-1 AND CG.COVERAGE_GROUP_ID > 0");
                    sSQL.Append(@" SELECT CG.COVERAGE_GROUP_ID, CG.COV_GROUP, CGT.GROUP_DESCRIPTION");
                    sSQL.AppendFormat(" FROM COVERAGE_GROUP CG INNER JOIN COVERAGE_GROUP_TEXT CGT ON CG.COVERAGE_GROUP_ID = CGT.COVERAGE_GROUP_ID WHERE CGT.LANGUAGE_CODE = {0} AND CG.DELETED_FLAG !=-1 AND CG.COVERAGE_GROUP_ID > 0 ", iLangcode);
                    //end -  Multilingual changes
                    sSQL.Append(" AND UPPER(CG.COV_GROUP) LIKE UPPER('" + p_sLookupString + "') AND UPPER(CG.COV_GROUP) >= UPPER('" + p_sIndex + "')");
                    //start:Added to filter Coverage group based on States value
                    if (!string.IsNullOrEmpty(p_sFilter))
                        sSQL.Append(" AND " + p_sFilter);
                    //end:Added to filter Coverage group based on States value
                    sSQL.Append(" AND ((CG.TRIGGER_DATE_FIELD IS NULL) ");

                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CG.EFF_START_DATE<='" + sToday + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sToday + "') ");
                    sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CG.EFF_START_DATE<='" + sToday + "' AND CG.EFF_END_DATE>='" + sToday + "') ");

                    if (!string.IsNullOrEmpty(sDateOfEvent))
                    {
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CG.EFF_START_DATE<='" + sDateOfEvent + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sDateOfEvent + "') ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CG.EFF_START_DATE<='" + sDateOfEvent + "' AND CG.EFF_END_DATE>='" + sDateOfEvent + "') ");

                    }
                    if (!string.IsNullOrEmpty(sDateOfClaim))
                    {
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CG.EFF_START_DATE<='" + sDateOfClaim + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sDateOfClaim + "') ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CG.EFF_START_DATE<='" + sDateOfClaim + "' AND CG.EFF_END_DATE>='" + sDateOfClaim + "') ");
                    }
                    if (!string.IsNullOrEmpty(sDateOfPolicy))
                    {
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CG.EFF_START_DATE<='" + sDateOfPolicy + "' AND (CG.EFF_END_DATE IS NULL OR CG.EFF_END_DATE='')) ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CG.EFF_START_DATE IS NULL OR CG.EFF_START_DATE='') AND CG.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                        sSQL.Append(" OR (CG.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CG.EFF_START_DATE<='" + sDateOfPolicy + "' AND CG.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                    }

                    sSQL.Append(") ORDER BY CG.COV_GROUP");
                }
                //end:added by Nitin goel, MITS 30910, 08/22/2013
                else if (p_sLookupType.ToUpper().Equals("CODE.TRANS_TYPES"))
                {
                    this.CodesTable = true; //Aman ML Change
                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,C2.SHORT_CODE");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID");
                    sSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" + p_sLookupType.Substring(5).ToUpper() + "' ");
                    //asharma326 JIRA 857 Starts
                    if ((!string.IsNullOrEmpty(p_sFilter)) && p_sFilter == "Split_Trans_Type")
                    {
                        int iLOB = Conversion.ConvertStrToInteger(p_sLOB);
                        // akaushik5 Added for RMA-7067 Starts
                        if (iLOB > 0)
                        // akaushik5 Added for RMA-7067 Ends
                        {
                            if (objColLobSettings[iLOB].PreventCollectionAllReserve == -1)
                            {
                                //only recovery reserve types
                                sSQL.Append(" AND C2.CODE_ID IN (SELECT CODE_ID FROM CODES WHERE CODES.RELATED_CODE_ID IN (SELECT CODE_ID FROM CODES_TEXT WHERE SHORT_CODE = 'R')) ");
                            }
                            else if (objColLobSettings[iLOB].PreventCollectionPerReserve == -1)
                            {
                                //sSQL.Append(" AND CODES.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sSessionLOB + ")");
                                sSQL.Append(" AND C2.CODE_ID NOT IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_NOT_ALLOWED WHERE LINE_OF_BUS_CODE = " + p_sSessionLOB + ")");
                            }
                        }
                        p_sFilter = "";
                    }
                    //asharma326 JIRA 857 Ends
                    if (p_sLOB.Equals(string.Empty) || p_sLOB.Equals("0"))
                    {
                        sSQL.Append(" ");
                    }
                    else
                    {
                        // akaushik5 Added for MITS 38314 Starts
                        int iLOB = Conversion.ConvertStrToInteger(p_sLOB);
                        int iClaimTypeCode = 0;
                        if (objColLobSettings[iLOB].ResByClmType == true && iSessionClaimId > 0 && (!string.IsNullOrEmpty(p_sSessionClaimId)))
                        {
                            iClaimTypeCode = objConn.ExecuteInt("SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iSessionClaimId);
                        }

                        if (!iClaimTypeCode.Equals(default(int)))
                        {
                            sSQL.AppendFormat(" AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE= {0} AND CLAIM_TYPE_CODE = {1})", p_sSessionLOB, iClaimTypeCode);
                        }
                        else
                        {
                            // akaushik5 Added for MITS 38314 Ends
                            sSQL.Append(" AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sLOB + ")");
                        }
                    }
                    //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                    if (objSysSetting.UseResFilter == false && objSysSetting.MultiCovgPerClm != 0 && bIsFirstFinal == false) //if carrier claim is on and first&final=false
                    {
                        if (sPolUnitRowId != string.Empty)
                        {
                            sSQL.Append(" AND C2.CODE_ID IN (SELECT DISTINCT(RESERVE_TYPE_CODE) ");
                            sSQL.Append(" FROM POLICY_X_CVG_TYPE ");
                            sSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID ");
                            sSQL.Append(" INNER JOIN COVERAGE_X_LOSS ON COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = RESERVE_CURRENT.POLCVG_LOSS_ROW_ID ");
                            sSQL.Append(" WHERE POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID =" + sPolUnitRowId);
                            sSQL.Append(" AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId);
                            sSQL.Append(" AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =" + p_sCovTypecode);
                            sSQL.Append(" AND COVERAGE_X_LOSS.LOSS_CODE = " + p_sLossTypeCode);
                            sSQL.Append(" AND RESERVE_CURRENT.CLAIMANT_EID =" + p_sClaimantEid + ")");
                         }
                    }
                    //Ankit End
                    sSQL.Append(" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ");
                    sSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ");
                    sSQL.Append(" AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ");
                    //rsushilaggar MITS 22195 Date 12/03/12
                    if (sLookupTemp.Contains("\",\""))
                    {
                        sSQL.Append(" AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + sLookupDesc + "')");
                        
                        sSQL.Append(" OR (UPPER(CODES.SHORT_CODE) LIKE '" + sLookupDesc + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "'))");
                    }
                    else
                    {
                        if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                            sSQL.Append(" AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "'");
                        //MITS:35039 START
                        else
                            sSQL.Append(" AND (UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' OR UPPER(CODES_TEXT.CODE_DESC) LIKE '%" + p_sLookupString + "')");
                        //MITS:35039 END
                    }
                    //TR2566- BRS transaction type lookup gives problem.. 
                    if (p_sLOB != "")
                        sSQL.Append(" AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) ");
                    //sSQL.Append(" ORDER BY CODES.SHORT_CODE");
                    if ((sParentCodeID != "") && (sParentCodeID != "0") && (sParentCodeID != null))// stara Mits 16667
                        sSQL.Append(" AND CODES.RELATED_CODE_ID = " + sParentCodeID);


                    //if ((p_sLookupType.ToUpper().Equals("CODE.TRANS_TYPES")) && (p_sFormName.Equals("split", StringComparison.InvariantCultureIgnoreCase) || p_sFormName.Equals("thirdpartypayments", StringComparison.InvariantCultureIgnoreCase) || p_sFormName.Equals("futurepayments", StringComparison.InvariantCultureIgnoreCase)))
                    //{
                    //    if (p_sRsvStatusParent == "C")
                    //    {
                    //        if ((sParentCodeID != "") && (sParentCodeID != null) && (sParentCodeID != "0"))
                    //            sSQL.Append(" AND CODES.RELATED_CODE_ID= " + sParentCodeID);//--stara Mits 16667 05/18/09
                    //    }
                    //}
                    //else
                    //{
                        if ((sParentCodeID != "") && (sParentCodeID != null) && (sParentCodeID != "0"))
                            sSQL.Append(" AND CODES.RELATED_CODE_ID= " + sParentCodeID);//--stara Mits 16667 05/18/09

                    //}




                    //rupal:start for first & final payment   
                    if (objSysSetting.UseResFilter == false && bIsFirstFinal && objSysSetting.MultiCovgPerClm != 0)
                    {
                        //in case of first & final payment,
                        //we donn't want certain trans type to be populated if any kind of payment is made for the trans type's parent type (reserve type)
                        //rupal:start,policy system interface
                        string sPolSql = "SELECT POLICY_SYSTEM_ID FROM POLICY WHERE POLICY_ID=" + p_sPolicyId;

                        //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, sPolSql))
                        //{
                        //    if (objRdr != null)
                        //    {
                        //        if (objRdr.Read())
                        //        {
                        //            iPolSystemId = Conversion.ConvertObjToInt(objRdr["POLICY_SYSTEM_ID"], m_iClientId);
                        //            objRdr.Close();
                        //        }
                        //    }
                        //}
                        iPolSystemId = objConn.ExecuteInt(sPolSql);
                        string sSQLCovg = string.Empty;
                        if (iPolSystemId > 0)
                        {
                            //string sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_ID=" + p_sPolicyId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;
                            //Start:RMA-11452 - rkaur27
                           //sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + sPolUnitRowId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode + " and CVG_SEQUENCE_NO='" + p_sCovgSeqNum + "' and TRANS_SEQ_NO='" + p_sTransSeqNum + "'";
                            sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + sPolUnitRowId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;
                            if (!string.IsNullOrEmpty(p_sCovgSeqNum))
                                sSQLCovg = string.Concat(sSQLCovg, " AND CVG_SEQUENCE_NO = '" + p_sCovgSeqNum + "'");//rkotak:rma 11750 raised because single quotes were mising in the changed query (ripple effect of rma-11452)
                            if (!string.IsNullOrEmpty(p_sTransSeqNum))
                                sSQLCovg = string.Concat(sSQLCovg, " AND TRANS_SEQ_NO = '" + p_sTransSeqNum + "'");//rkotak:rma 11750 raised because single quotes were mising in the changed query (ripple effect of rma-11452)
                            if (!string.IsNullOrEmpty(p_sCoverageKey))
                                sSQLCovg = string.Concat(sSQLCovg, " AND COVERAGE_KEY = '" + p_sCoverageKey + "'");//rkotak:rma 11750 raised because single quotes were mising in the changed query (ripple effect of rma-11452)

                            ////Ankit Start : Worked on MITS - 34297
                            //if (string.IsNullOrEmpty(p_sCoverageKey))
                            //    sSQLCovg = string.Concat(sSQLCovg, " AND COVERAGE_KEY IS NULL ");
                            //else
                            //    sSQLCovg = string.Concat(sSQLCovg, " AND COVERAGE_KEY='", p_sCoverageKey, "'");
                            ////Ankit End
                            //End : RMA-11452
                        }
                        else
                        {
                            //string sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_ID=" + p_sPolicyId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;
                            sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + sPolUnitRowId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;

                        }
                        //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, sSQLCovg))
                        //{
                        //    if (objRdr != null)
                        //    {
                        //        if (objRdr.Read())
                        //        {
                        //            lPolCvgRoWId = Conversion.ConvertObjToInt64(objRdr["POLCVG_ROW_ID"]);
                        //        }
                        //    }
                        //}
                        lPolCvgRoWId = objConn.ExecuteInt(sSQLCovg);
                        //rupal:end,policy system interface
                        sSQL.Append(" AND C2.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE FROM RESERVE_CURRENT ");
                        sSQL.Append(" INNER JOIN COVERAGE_X_LOSS ON COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = RESERVE_CURRENT.POLCVG_LOSS_ROW_ID ");
                        sSQL.Append(" WHERE CLAIM_ID = " + p_sSessionClaimId + " AND CLAIMANT_EID = " + p_sClaimantEid);
                        sSQL.Append("AND COVERAGE_X_LOSS.LOSS_CODE = " + p_sLossTypeCode + " AND COVERAGE_X_LOSS.POLCVG_ROW_ID = " + lPolCvgRoWId + ")");
                    }
                    //rupal:end for firt & final payment

                }
                //added by rkaur7
                else if (p_sLookupType.ToUpper().Equals("CODE.RESERVE_TYPE"))
                {
                    this.CodesTable = true; //Aman ML Change
                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES ");
                    sSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" + p_sLookupType.Substring(5).ToUpper() + "' ");
                    //asharma326 JIRA 857 Starts
                    if ((!string.IsNullOrEmpty(p_sFilter)) && p_sFilter == "Split_Trans_Type")
                    {

                        int iLOB = Conversion.ConvertStrToInteger(p_sLOB);
                        // akaushik5 Added for RMA- 7067 Starts
                        if (iLOB > 0)
                        {
                            // akaushik5 Added for RMA- 7067 Ends
                            if (objColLobSettings[iLOB].PreventCollectionAllReserve == -1)
                            {
                                //only recovery reserve types
                                sSQL.Append(" AND CODES.CODE_ID IN (SELECT CODE_ID FROM CODES WHERE CODES.RELATED_CODE_ID IN (SELECT CODE_ID FROM CODES_TEXT WHERE SHORT_CODE = 'R')) ");
                            }
                            else if (objColLobSettings[iLOB].PreventCollectionPerReserve == -1)
                            {
                                sSQL.Append(" AND CODES.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sSessionLOB + ")");
                                sSQL.Append(" AND CODES.CODE_ID NOT IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_NOT_ALLOWED WHERE LINE_OF_BUS_CODE = " + p_sSessionLOB + ")");
                            }
                        }
                        p_sFilter = "";
                    }
                    //asharma326 JIRA 857 Ends
                    if (p_sLOB.Equals(string.Empty) || p_sLOB.Equals("0"))
                        sSQL.Append(" ");
                    else
                    {// 06/18/2007 MITS : 9499   by Umesh
                        //ColLobSettings objColLobSettings = new ColLobSettings(m_sConnectionString);
                        int iLOB = Conversion.ConvertStrToInteger(p_sLOB);

                        // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                        //int iClaimId = Conversion.ConvertStrToInteger(p_sSessionClaimId);
                        //zmohammad JIRa 6284 : Fix for Policy lookups.
                        if (objColLobSettings[iLOB].ResByClmType == true && iSessionClaimId > 0 && (!string.IsNullOrEmpty(p_sSessionClaimId)))
                        {
                            //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString,
                            //  "SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iSessionClaimId))
                            //// npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            //{
                            //    if (objRdr.Read())
                            //    {
                                    int iClaimTypeCode = 0;
                                    iClaimTypeCode = objConn.ExecuteInt("SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iSessionClaimId);
                                    sSQL.Append(" AND CODES.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE=" + p_sLOB + " AND CLAIM_TYPE_CODE =" + iClaimTypeCode + ")");

                                    if (objSysSetting.MultiCovgPerClm != 0 && bIsFirstFinal == false)
                                    {
                                        sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(RESERVE_TYPE_CODE) ");
                                        sSQL.Append(" FROM RESERVE_CURRENT ");
                                        sSQL.Append(" INNER JOIN COVERAGE_X_LOSS ON COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = RESERVE_CURRENT.POLCVG_LOSS_ROW_ID ");
                                        sSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON COVERAGE_X_LOSS.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID ");
                                        sSQL.Append(" WHERE POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID =" + sPolUnitRowId);
                                        sSQL.Append(" AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId);
                                        sSQL.Append(" AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =" + p_sCovTypecode);
                                        sSQL.Append(" AND COVERAGE_X_LOSS.LOSS_CODE = " + p_sLossTypeCode);
                                        sSQL.Append(" AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");
                                    
                                    }

                            //    }

                            //    objRdr.Close();
                            //}

                        } // END MITS 9499
                        else
                        {
                            sSQL.Append(" AND CODES.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sLOB + ")");
                            if (!(string.Compare(p_sPolicyId, "0") == 0) && !string.IsNullOrEmpty(p_sPolicyId))
                            {
                                //if condition added by rupal
                                //following clause in SQL should be added only if payment is not first & final payment
                                //because in case of first & final payment, we may not have any reserves in reserve_current                                
                                if (objSysSetting.MultiCovgPerClm != 0 && bIsFirstFinal == false) //if carrier claim is on and first and final payment is off
                                {
                                    //rupal:first & final pmt , 4 july 2010
                                    //RESERVE_CURRENT table does ot contain Policy_ID as per new changes
                                    //sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(RESERVE_TYPE_CODE) FROM POLICY_X_CVG_TYPE,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND RESERVE_CURRENT.POLICY_ID =" + p_sPolicyId);
                                    //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                                    sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(RESERVE_TYPE_CODE) ");
                                    sSQL.Append(" FROM RESERVE_CURRENT ");
                                    sSQL.Append(" INNER JOIN COVERAGE_X_LOSS ON COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = RESERVE_CURRENT.POLCVG_LOSS_ROW_ID ");
                                    sSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON COVERAGE_X_LOSS.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID ");
                                    sSQL.Append(" WHERE POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID =" + sPolUnitRowId);
                                    sSQL.Append(" AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId);
                                    sSQL.Append(" AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =" + p_sCovTypecode);
                                    sSQL.Append(" AND COVERAGE_X_LOSS.LOSS_CODE = " + p_sLossTypeCode);
                                    sSQL.Append(" AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");
                                    //Ankit End
                                }
                            }
                        }
                    }

                    //rupal:start for first & final payment                   
                    //in case of first & final payment,
                    //we don't want certain reservetype to be populated if any kind of payment is made for that reserve type                    
                    if (objSysSetting.MultiCovgPerClm != 0) //zero for false
                    {
                        if (bIsFirstFinal)
                        {
                            //rupal:start,policy system interface
                            string sPolSql = "SELECT POLICY_SYSTEM_ID FROM POLICY WHERE POLICY_ID=" + p_sPolicyId;

                            //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, sPolSql))
                            //{
                            //    if (objRdr != null)
                            //    {
                            //        if (objRdr.Read())
                            //        {
                            //            iPolSystemId = Conversion.ConvertObjToInt(objRdr["POLICY_SYSTEM_ID"], m_iClientId);
                            //            objRdr.Close();
                            //        }
                            //    }
                            //}
                            iPolSystemId = objConn.ExecuteInt(sPolSql);
                            string sSQLCovg = string.Empty;
                            if (iPolSystemId > 0)
                            {
                                //Start : RMA-11452 - rkaur27
                                //sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + sPolUnitRowId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode + " and CVG_SEQUENCE_NO='" + p_sCovgSeqNum + "' and TRANS_SEQ_NO='" + p_sTransSeqNum + "'";
                                sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + sPolUnitRowId + " and COVERAGE_TYPE_CODE = ";
                                if (!string.IsNullOrEmpty(p_sCovgSeqNum))
                                    sSQLCovg = string.Concat(sSQLCovg, " AND CVG_SEQUENCE_NO = '" + p_sCovgSeqNum + "'");//rkotak:rma 11750 raised because single quotes were mising in the changed query (ripple effect of rma-11452)
                                if (!string.IsNullOrEmpty(p_sTransSeqNum))
                                    sSQLCovg = string.Concat(sSQLCovg, " AND TRANS_SEQ_NO = '" + p_sTransSeqNum + "'");//rkotak:rma 11750 raised because single quotes were mising in the changed query (ripple effect of rma-11452)
                                if (!string.IsNullOrEmpty(p_sCoverageKey))
                                    sSQLCovg = string.Concat(sSQLCovg, " AND COVERAGE_KEY = '" + p_sCoverageKey + "'");//rkotak:rma 11750 raised because single quotes were mising in the changed query (ripple effect of rma-11452)

                                ////Ankit Start : Worked on MITS - 34297
                                //if (string.IsNullOrEmpty(p_sCoverageKey))
                                //    sSQLCovg = string.Concat(sSQLCovg, " AND COVERAGE_KEY IS NULL ");
                                //else
                                //    sSQLCovg = string.Concat(sSQLCovg, " AND COVERAGE_KEY='", p_sCoverageKey, "'");
                                ////Ankit End
                                //End : RMA-11452
                            }
                            else
                            {
                                //string sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_ID=" + p_sPolicyId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;
                                sSQLCovg = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + sPolUnitRowId + " and COVERAGE_TYPE_CODE = " + p_sCovTypecode;
                            }                           
                            
                            //using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, sSQLCovg))
                            //{
                            //    if (objRdr != null)
                            //    {
                            //        if (objRdr.Read())
                            //        {
                            //            lPolCvgRoWId = Conversion.ConvertObjToInt64(objRdr["POLCVG_ROW_ID"]);
                            //        }
                            //    }
                            //}
                            lPolCvgRoWId = objConn.ExecuteInt(sSQLCovg);
                            //rupal:end,policy system interface
                            sSQL.Append(" AND CODES.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE FROM RESERVE_CURRENT ");
                            sSQL.Append(" INNER JOIN COVERAGE_X_LOSS ON COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = RESERVE_CURRENT.POLCVG_LOSS_ROW_ID ");
                            sSQL.Append(" WHERE CLAIM_ID = " + p_sSessionClaimId + " AND CLAIMANT_EID = " + p_sClaimantEid);
                            sSQL.Append("AND COVERAGE_X_LOSS.LOSS_CODE = " + p_sLossTypeCode + " AND COVERAGE_X_LOSS.POLCVG_ROW_ID = " + lPolCvgRoWId + ")");
                        }
                    }

                    //rupal:end for firt & final payment


                    // else
                    // sSQL.Append(" AND CODES.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" + p_sLOB + ")");
                    sSQL.Append(" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ");
                    sSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ");
                    sSQL.Append(" AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ");
                    //rsushilaggar MITS 22195 Date 03/01/12
                    if (sLookupTemp.Contains("\",\""))
                    {
                        sSQL.Append(" AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + sLookupDesc + "')");
                        //sSQL.Append(" OR ( UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "')");
                        sSQL.Append(" OR (UPPER(CODES.SHORT_CODE) LIKE '" + sLookupDesc + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "'))");
                    }
                    else
                    {
                        if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                            sSQL.Append(" AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' ");
                        //MITS:35039 START 
                        else
                            sSQL.Append(" AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "') OR (UPPER(CODES_TEXT.CODE_DESC) LIKE '%" + p_sLookupString + "'))");
                        //MITS:35039 END
                    }
                    //TR2566- BRS transaction type lookup gives problem.. 
                    if (p_sLOB != "")
                        sSQL.Append(" AND (CODES.LINE_OF_BUS_CODE = " + p_sLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) ");

                }
                //end rkaur7                    
                else if (p_sLookupType.ToUpper().Equals("CODE.RM_SYS_USERS"))
                {
                    sConnectionString = SecurityDatabase.GetSecurityDsn(m_iClientId);
                    bIsSecurityDB = true;

                    if (bOracleCaseInSensitive)
                    {
                        sSQL.Append("SELECT USER_TABLE.USER_ID,USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                        sSQL.Append(" FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID");
                        sSQL.Append(" AND USER_DETAILS_TABLE.DSNID = " + p_sSessionDsnId);
                        /*	sSQL.Append(" AND (UPPER(USER_DETAILS_TABLE.LOGIN_NAME) LIKE '" + p_sLookupString + "' OR UPPER(USER_DETAILS_TABLE.LOGIN_NAME) LIKE '" + p_sLookupString.Replace("%", "*") + "')");
                            sSQL.Append(" AND UPPER(USER_DETAILS_TABLE.LOGIN_NAME) >= '" + p_sIndex.ToUpper() + "'");
                            sSQL.Append(" ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                        */
                        sSQL.Append(" AND (USER_DETAILS_TABLE.LOGIN_NAME LIKE '" + p_sLookupString + "' OR USER_DETAILS_TABLE.LOGIN_NAME LIKE '" + p_sLookupString.Replace("%", "*") + "')");
                        //sSQL.Append(" AND UPPER(USER_DETAILS_TABLE.LOGIN_NAME) >= '" + p_sIndex.ToUpper() + "'");
                        sSQL.Append(" ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                    }
                    else
                    {
                        sSQL.Append("SELECT USER_TABLE.USER_ID,USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                        sSQL.Append(" FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID");
                        sSQL.Append(" AND USER_DETAILS_TABLE.DSNID = " + p_sSessionDsnId);
                        sSQL.Append(" AND (USER_DETAILS_TABLE.LOGIN_NAME LIKE '" + p_sLookupString + "' OR USER_DETAILS_TABLE.LOGIN_NAME LIKE '" + p_sLookupString.Replace("%", "*") + "')");
                        sSQL.Append(" AND USER_DETAILS_TABLE.LOGIN_NAME >= '" + p_sIndex + "'");
                        sSQL.Append(" ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                    }
                }
                else if (p_sLookupType.ToUpper().Equals("CODE.CLAIM_TYPE"))
                {
                    this.CodesTable = true; //Aman ML Change
                    if (p_sTypeLimits.Trim().Length > 0)
                    {
                        sarrSplit = p_sTypeLimits.Split('|');
                        for (int i = 0; i < sarrSplit.Length; i++)
                        {
                            sarrSplit1 = sarrSplit[i].Split(',');
                            if (Conversion.ConvertStrToBool(sarrSplit1[1]) == false && p_sLOB == sarrSplit1[3])
                                sLimitsSQL = sLimitsSQL + "," + sarrSplit1[0];
                        }
                        if (sLimitsSQL.Trim().Length > 0)
                            sLimitsSQL = " AND CODES.CODE_ID NOT IN (" + sLimitsSQL.Substring(2) + ") ";
                    }
                    else
                        sLimitsSQL = string.Empty;
                    //MITS 11246 :Umesh
                    if (p_bDescriptionSearch)
                    {
                        if (bOracleCaseInSensitive)
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE || ' ' ||CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ");
                        else if(m_bIsOracle)
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE || ' ' || CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ");
                        else
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE + ' ' +CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ");
                    }
                    else
                    {
                        //rsushilaggar MITS 22195 Date 03/01/12
                        if (sLookupTemp.Contains("\",\""))
                        {
                            if (bOracleCaseInSensitive)
                                sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + sLookupDesc + "' ) OR (UPPER(CODES.SHORT_CODE) LIKE '" + sLookupDesc + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' ) ) " + sLOBSQL + sLimitsSQL + " AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ");
                            else
                                sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((CODES.SHORT_CODE LIKE '" + p_sLookupString + "' AND CODES_TEXT.CODE_DESC LIKE '" + sLookupDesc + "' ) OR (CODES.SHORT_CODE LIKE '" + sLookupDesc + "' AND CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' ) )  " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ");
                        }
                        else
                        {
                            if (bOracleCaseInSensitive)
                            {
                                if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                                    sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ");
                                //MITS:35039 START
                                else
                                    sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ) OR (UPPER(CODES_TEXT.CODE_DESC) LIKE '%" + p_sLookupString + "')) ");
                                //MITS:35039 END
                            }
                            else
                            {
                                if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                                    sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ");
                                //MITS:35039 START
                                else
                                    sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ) OR (CODES_TEXT.CODE_DESC LIKE '%" + p_sLookupString + "'))");
                                //MITS:35039 END
                            }
                        }

                    }
                 if(Conversion.ConvertStrToBool(objSysSetting.MultiCovgPerClm.ToString()) && !string.IsNullOrEmpty(p_sPolicyLOB)&& string.Equals(p_sLookupType, "CODE.CLAIM_TYPE", StringComparison.InvariantCultureIgnoreCase) && (string.Equals(p_sFormName, "claimgc", StringComparison.InvariantCultureIgnoreCase) || string.Equals(p_sFormName, "claimwc", StringComparison.InvariantCultureIgnoreCase)))
                    {
                        //objCache = new LocalCache(m_sConnectionString,m_iClientId);       //avipinsrivas Start : Worked for Jira-340
                        sSQL.Append(" AND CODES.CODE_ID IN (SELECT CODE2 FROM CODE_X_CODE WHERE CODE1 = " + p_sPolicyLOB  + " AND REL_TYPE_CODE = " + objCache.GetCodeId("POLTOCT", "CODE_REL_TYPE").ToString() + " AND DELETED_FLAG =0)");
                    }
                }
                else if (p_sLookupType.ToUpper().Equals("CODE.EVENT_TYPE"))
                {
                    this.CodesTable = true; //Aman ML Change
                    if (p_sTypeLimits.Trim().Length > 0)
                    {
                        sarrSplit = p_sTypeLimits.Split('|');
                        for (int i = 0; i < sarrSplit.Length; i++)
                        {
                            sarrSplit1 = sarrSplit[i].Split(',');
                            if (Conversion.ConvertStrToBool(sarrSplit1[1]) == false)
                                sLimitsSQL = sLimitsSQL + "," + sarrSplit1[0];
                        }
                        if (sLimitsSQL.Trim().Length > 0)
                            sLimitsSQL = " AND CODES.CODE_ID NOT IN (" + sLimitsSQL.Substring(2) + ") ";
                    }
                    else
                        sLimitsSQL = string.Empty;
                    //MITS 11246 :Umesh
                    //Mcic changes  removed "ORDER BY CODES.SHORT_CODE");" from end of the queries by Pankaj
                    if (p_bDescriptionSearch)
                    {
                        if (bOracleCaseInSensitive)
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE || ' ' ||CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ");
                        else if(m_bIsOracle)
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE || ' ' || CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ");
                        else
                            sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE + ' ' +CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ");
                    }
                    else
                    {
                        //rsushilaggar MITS 22195 Date 03/01/12
                        if (sLookupTemp.Contains("\",\""))
                        {
                            if (bOracleCaseInSensitive)
                                sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + sLookupDesc + "' ) OR (UPPER(CODES.SHORT_CODE) LIKE '" + sLookupDesc + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' ) )  " + sLOBSQL + sLimitsSQL + " AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ");
                            else
                                sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((CODES.SHORT_CODE LIKE '" + p_sLookupString + "' AND CODES_TEXT.CODE_DESC LIKE '" + sLookupDesc + "' ) OR (CODES.SHORT_CODE LIKE '" + sLookupDesc + "' AND CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' ) )  " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ");
                        }
                        else
                        {
                            if (bOracleCaseInSensitive)
                            {
                                if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                                    sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ");
                                //MITS:35039 START
                                else
                                    sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ) OR (UPPER(CODES_TEXT.CODE_DESC) LIKE '%" + p_sLookupString + "')) ");
                                //MITS:35039 END
                            }
                            else
                            {
                                if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                                    sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND CODES.SHORT_CODE >= '" + p_sIndex + "' ");
                                //MITS:35039 START
                                else
                                    sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + sLimitsSQL + " AND ((CODES.SHORT_CODE >= '" + p_sIndex + "')) OR (CODES_TEXT.CODE_DESC LIKE '%" + p_sLookupString + "'))) ");
                                //MITS:35039 END
                            }
                        }
                    }

                }
                    //Start:Modified by Nitin goel, Condition for Loss codes for NI, 02/08/2013
                //else if (p_sLookupType.ToUpper().Equals("CODE.LOSS_CODES"))
                else if (p_sLookupType.ToUpper().Equals("CODE.LOSS_CODES") && !string.IsNullOrEmpty(p_sPolicyId))
                {
                    //Ankit Start : Worked on MITS - 34657
                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES");
                    sSQL.Append(" WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME) = '" + p_sLookupType.Substring(5).ToUpper() + "' ");
                    sSQL.Append(" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ");
                    sSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ");
                    if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                        sSQL.Append(" AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "'  AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                    //MITS:35039 START
                    else
                        sSQL.Append(" AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "'  AND CODES.SHORT_CODE >= '" + p_sIndex + "') OR (UPPER(CODES_TEXT.CODE_DESC) LIKE '%" + p_sLookupString + "'))");
                    //MITS:35039 END
                    if (sLOBSQL != string.Empty)
                        sSQL.Append(sLOBSQL);

                    //p_sPolicyLOB = Convert.ToString(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT POLICY_LOB_CODE FROM POLICY WHERE POLICY_ID=" + p_sPolicyId));
                    //string sLTemp = "SELECT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + p_sCovTypecode;
                    using (DbReader objReader = GetPolicyInfo(p_sPolicyId,objConn ))
                    {
                        if (objReader.Read())
                        {
                            p_sPolicyLOB = Convert.ToString(objReader.GetValue("POLICY_LOB_CODE"));
                            iPolSystemId = Conversion.CastToType<int>(objReader.GetValue("POLICY_SYSTEM_ID").ToString(), out success);
                            objReader.Close();
                        }
                    }
                    //Ankit End
                    string sLTemp = string.Empty;
                    if (bIsFirstFinal && (objSysSetting.MultiCovgPerClm != 0))
                    {
                        //spahariya MITS 31978 to avoid dups loss code for multiple reserve type
                        //sLTemp = "SELECT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + p_sCovTypecode;
                        sLTemp = "SELECT DISTINCT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + p_sCovTypecode;
                        sLTemp = string.Concat(sLTemp, " AND POLICY_SYSTEM_ID = " + iPolSystemId);                  //Ankit Start : Worked on MITS - 34657
                        sSQL.Append(" AND " + "CODES.CODE_ID IN (" + sLTemp + ") ");
                    }
                    else
                    {
                        if (objSysSetting.MultiCovgPerClm != 0)
                        {
                            if (!(string.Compare(p_sSessionClaimId, "0", true) == 0 && string.Compare(lClaimantEid.ToString(), "0", true) == 0))
                            {
                                //Ankit Start : Worked on MITS - 34657
                                //sLTemp = "SELECT LOSS_CODE FROM COVERAGE_X_LOSS,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_LOSS_ROW_ID= COVERAGE_X_LOSS.CVG_LOSS_ROW_ID AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid;
                                sLTemp = "SELECT LOSS_CODE ";
                                sLTemp = string.Concat(sLTemp, " FROM COVERAGE_X_LOSS CXL ");
                                sLTemp = string.Concat(sLTemp, " INNER JOIN RESERVE_CURRENT RC ON RC.POLCVG_LOSS_ROW_ID= CXL.CVG_LOSS_ROW_ID ");
                                sLTemp = string.Concat(sLTemp, " INNER JOIN POLICY_X_CVG_TYPE PXCT ON PXCT.POLCVG_ROW_ID = CXL.POLCVG_ROW_ID ");
                                sLTemp = string.Concat(sLTemp, " INNER JOIN POLICY_X_UNIT PXU ON PXU.POLICY_UNIT_ROW_ID = PXCT.POLICY_UNIT_ROW_ID ");
                                sLTemp = string.Concat(sLTemp, " INNER JOIN POLICY P ON P.POLICY_ID = PXU.POLICY_ID AND P.POLICY_SYSTEM_ID = " + iPolSystemId);
                                sLTemp = string.Concat(sLTemp, " WHERE RC.CLAIM_ID  = " + p_sSessionClaimId);
                                sLTemp = string.Concat(sLTemp, " AND RC.CLAIMANT_EID = " + lClaimantEid);
                                //Ankit End
                                sSQL.Append(" AND " + "CODES.CODE_ID IN (" + sLTemp + ") ");
                            }
                        }
                    }

                }
                else if (p_sLookupType.ToUpper().Equals("CODE.DISABILITY_CATEGORY"))
                {
                    //Ankit Start : Worked on MITS - 34657
                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES");
                    sSQL.Append(" WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME) = '" + p_sLookupType.Substring(5).ToUpper() + "' ");
                    sSQL.Append(" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ");
                    sSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ");
                    if (objSysSetting.SearchCodeDescription == 0)  //MITS:35039
                        sSQL.Append(" AND CODES.SHORT_CODE LIKE '" + p_sLookupString + "'  AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                    //MITS:35039 START
                    else
                        sSQL.Append(" AND ((CODES.SHORT_CODE LIKE '" + p_sLookupString + "'  AND CODES.SHORT_CODE >= '" + p_sIndex + "') OR (CODES_TEXT.CODE_DESC LIKE '%" + p_sLookupString + "'))");
                    //MITS:35039 END
                    if (sLOBSQL != string.Empty)
                        sSQL.Append(sLOBSQL);
                    //Ankit End
                    //Bharani - MITS : 36171 - Change 1/2 - Start
                    //p_sPolicyLOB = Convert.ToString(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT POLICY_LOB_CODE FROM POLICY WHERE POLICY_ID=" + p_sPolicyId));
                    if (!string.IsNullOrEmpty(p_sPolicyId))
                        p_sPolicyLOB = Convert.ToString(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT POLICY_LOB_CODE FROM POLICY WHERE POLICY_ID=" + p_sPolicyId));
                    else
                        p_sPolicyId = string.Empty;
                    //Bharani - MITS : 36171 - Change 1/2 - End
                    //string sLTemp = "SELECT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + p_sCovTypecode;

                    string sLTemp = string.Empty;
                    if (!bIsFirstFinal && (objSysSetting.MultiCovgPerClm != 0))
                    {
                        //if (!(string.Compare(p_sSessionClaimId, "0", true) == 0 && string.Compare(lClaimantEid.ToString(), "0", true) == 0))//Bharani - MITS : 36171 - Change 2/2
                        if (!(string.Compare(p_sSessionClaimId, "0", true) == 0 && string.Compare(lClaimantEid.ToString(), "0", true) == 0) && !string.IsNullOrEmpty(p_sSessionClaimId))//Bharani - MITS : 36171 - Change 2/2
                        {
                            sLTemp = "SELECT RELATED_CODE_ID FROM COVERAGE_X_LOSS,RESERVE_CURRENT,CODES WHERE RESERVE_CURRENT.POLCVG_LOSS_ROW_ID = COVERAGE_X_LOSS.CVG_LOSS_ROW_ID AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + "  AND CODES.CODE_ID = COVERAGE_X_LOSS.LOSS_CODE";
                            sSQL.Append(" AND " + "CODES.CODE_ID IN (" + sLTemp + ") ");
                        }
                    }
                }

                //Start:Modified by Nitin goel, Condition for disability code for NI, 02/08/2013
                //else if( p_sLookupType.ToUpper().Equals("CODE.DISABILITY_TYPE_CODE" ))
                //else if (p_sLookupType.ToUpper().Equals("CODE.DISABILITY_TYPE_CODE") && !string.IsNullOrEmpty(p_sPolicyId))
                //RMA-9663 caggarwal4 Changed DISABILITY_TYPE_CODE to DISABILITY_TYPE
                else if (p_sLookupType.ToUpper().Equals("CODE.DISABILITY_TYPE") && !string.IsNullOrEmpty(p_sPolicyId))
                {
                    //Ankit Start : Worked on MITS - 34657
                    sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC");
                    sSQL.Append(" FROM CODES_TEXT,GLOSSARY,CODES");
                    sSQL.Append(" WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME) = '" + p_sLookupType.Substring(5).ToUpper() + "' ");
                    sSQL.Append(" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ");
                    sSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) ");
                    if (objSysSetting.SearchCodeDescription == 0)   //MITS:35039
                        sSQL.Append(" AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "'  AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                    //MITS:35039 START
                    else
                        sSQL.Append(" AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "'  AND CODES.SHORT_CODE >= '" + p_sIndex + "') OR (UPPER(CODES_TEXT.CODE_DESC) LIKE '%" + p_sLookupString + "'))");
                    //MITS:35039 END
                    if (sLOBSQL != string.Empty)
                        sSQL.Append(sLOBSQL);

                    //p_sPolicyLOB = Convert.ToString(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT POLICY_LOB_CODE FROM POLICY WHERE POLICY_ID=" + p_sPolicyId));
                    //string sLTemp = "SELECT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + p_sCovTypecode;
                    using (DbReader objReader = GetPolicyInfo(p_sPolicyId,objConn))
                    {
                        if (objReader.Read())
                        {
                            p_sPolicyLOB = Convert.ToString(objReader.GetValue("POLICY_LOB_CODE"));
                            iPolSystemId = Conversion.CastToType<int>(objReader.GetValue("POLICY_SYSTEM_ID").ToString(), out success);
                            objReader.Close();
                        }
                    }
                    //Ankit End
                    string sCvgType = string.Empty;
                    if (p_sFormName == "ReserveListingBOB")
                    {
                        sCvgType = GetCoverageType(p_sCovTypecode).ToString();

                    }
                    else                    
                        sCvgType = p_sCovTypecode;
                    ////asingh263 10/30/2013 starts
                    //this.CodesTable = true; 
                    //sSQL.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE");
                    //sSQL.Append(" FROM GLOSSARY,CODES ");
                    //sSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" + p_sLookupType.Substring(5).ToUpper() + "' ");
                    //sSQL.Append(" AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ");
                    
                    //if (sLookupTemp.Contains("\",\""))
                    //{
                    //    sSQL.Append(" AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "')");
                    //    sSQL.Append(" OR (UPPER(CODES.SHORT_CODE) LIKE '" + sLookupDesc + "')");
                    //}
                    //else
                    //{
                    //    sSQL.Append(" AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' ");
                    //}
                    ////asingh263 10/30/2013 ends
                    string sLTemp = string.Empty;
                    //Start:Modified by Nitin goel, Condition for disability code for NI, 02/08/2013
                    //if ((bIsFirstFinal || p_sFormName == "ReserveListingBOB") && (objSysSetting.MultiCovgPerClm != 0))
                    if ((bIsFirstFinal || p_sFormName == "ReserveListingBOB") && (objSysSetting.MultiCovgPerClm != 0) && !string.IsNullOrEmpty(p_sPolicyLOB) && !string.IsNullOrEmpty(sCvgType))
                    {
                        //spahariya MITS 31978 to avoid dups loss code for multiple reserve type
                        //sLTemp = "SELECT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + sCvgType;
                        sLTemp = "SELECT DISTINCT LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_LOB=" + p_sPolicyLOB + " AND CVG_TYPE_CODE=" + sCvgType;
                        sLTemp = string.Concat(sLTemp, " AND POLICY_SYSTEM_ID = " + iPolSystemId);                  //Ankit Start : Worked on MITS - 34657
                        sSQL.Append(" AND " + "CODES.CODE_ID IN (" + sLTemp + ") ");
                    }
                    else
                    {
                        if (objSysSetting.MultiCovgPerClm != 0)
                        {
                            if (!(string.Compare(p_sSessionClaimId, "0", true) == 0 && string.Compare(lClaimantEid.ToString(), "0", true) == 0))
                            {
                                //Ankit Start : Worked on MITS - 34657
                                //sLTemp = "SELECT LOSS_CODE FROM COVERAGE_X_LOSS,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_LOSS_ROW_ID= COVERAGE_X_LOSS.CVG_LOSS_ROW_ID AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid;
                                sLTemp = "SELECT LOSS_CODE ";
                                sLTemp = string.Concat(sLTemp, " FROM COVERAGE_X_LOSS CXL ");
                                sLTemp = string.Concat(sLTemp, " INNER JOIN RESERVE_CURRENT RC ON RC.POLCVG_LOSS_ROW_ID= CXL.CVG_LOSS_ROW_ID ");
                                sLTemp = string.Concat(sLTemp, " INNER JOIN POLICY_X_CVG_TYPE PXCT ON PXCT.POLCVG_ROW_ID = CXL.POLCVG_ROW_ID ");
                                sLTemp = string.Concat(sLTemp, " INNER JOIN POLICY_X_UNIT PXU ON PXU.POLICY_UNIT_ROW_ID = PXCT.POLICY_UNIT_ROW_ID ");
                                sLTemp = string.Concat(sLTemp, " INNER JOIN POLICY P ON P.POLICY_ID = PXU.POLICY_ID AND P.POLICY_SYSTEM_ID = " + iPolSystemId);
                                sLTemp = string.Concat(sLTemp, " WHERE RC.CLAIM_ID  = " + p_sSessionClaimId);
                                sLTemp = string.Concat(sLTemp, " AND RC.CLAIMANT_EID = " + lClaimantEid);
                                //Ankit End
                                sSQL.Append(" AND " + "CODES.CODE_ID IN (" + sLTemp + ") ");
                            }
                        }
                    }
                }
                else if ((p_sLookupType.Length > 4) && (p_sLookupType.ToUpper().Substring(0, 5).Equals("CODE.")))
                {
                    this.CodesTable = true; //Aman ML Change
                    //MITS 11246 : Umesh
                    //if (p_bDescriptionSearch)
                    //{
                    //    if (bOracle)
                    //        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE || ' ' ||CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ORDER BY CODES.SHORT_CODE");
                    //    else
                    //        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE + ' ' +CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "' ORDER BY CODES.SHORT_CODE");
                    //}
                    //else 
                    //{
                    //    if (bOracle)
                    //        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE ) LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "' ORDER BY CODES.SHORT_CODE");
                    //    else
                    //        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "' ORDER BY CODES.SHORT_CODE");

                    //}

                    //Additional Custom Filter Clause. //MCIC Changes 01/24/08 Pankaj, queries above were also changed to take care of the stuff.

                    #endregion

                    //rupal:start,policy system interafce
                    //changes for MITS 32588 start: policyId will be 0, if accessing from policy tracking screen, skip below query.
                    string sSQLPolicy = string.Empty;
                     if (!string.IsNullOrEmpty(p_sPolicyId) && !string.Equals(p_sPolicyId,"0"))
                     sSQLPolicy = "SELECT POLICY_SYSTEM_ID FROM POLICY WHERE POLICY_ID=" + p_sPolicyId;
                     //changes for MITS 32588 end
                    if (p_sLookupType.ToUpper().Equals("CODE.COVERAGE_TYPE"))
                    {
                        
                        #region get policy system if
                        //MITS 32588 start: added NullAndEmpty check before executing query: sSQLPolicy
                        if (!string.IsNullOrEmpty(sSQLPolicy))
                        {
                            //using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLPolicy))
                            //{
                            //    if (objReader.Read())
                            //    {
                            //        iPolSystemId = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReader.GetValue(0)), out bSuccess);
                            //        objReader.Close();
                            //    }
                            //}
                            iPolSystemId = objConn.ExecuteInt(sSQLPolicy);
                        }
                        #endregion
                        
                        #region if (iPolSystemId > 0 && objSysSetting.MultiCovgPerClm != 0 && string.Compare(p_sLookupType, "COVERAGE_TYPE", true) == 0)

                        if (iPolSystemId > 0 && objSysSetting.MultiCovgPerClm != 0 && p_sLookupType.ToUpper().Equals("CODE.COVERAGE_TYPE"))
                        {
                            //sSQL.Append(" SELECT DISTINCT A.COVERAGE_TYPE_CODE CODE_ID,CODES.SHORT_CODE,");
                            //sSQL.Append(" CASE(CNT)");
                            //sSQL.Append(" WHEN 1 THEN A.CODE_DESC ");
                            //sSQL.Append(" ELSE");
                            //if (DbFactory.IsOracleDatabase(m_sConnectionString))
                            //{
                            //    sSQL.Append(" CASE(NVL(COVERAGE_TEXT,''))");
                            //}
                            //else
                            //{
                            //    sSQL.Append(" CASE(ISNULL(COVERAGE_TEXT,''))");
                            //}

                            sSQL.Append(" SELECT DISTINCT A.COVERAGE_TYPE_CODE CODE_ID,CODES.SHORT_CODE,");
                            sSQL.Append("  COVERAGE_TEXT  ");
                            sSQL.Append("  CODE_DESC,");
                            //tanwar2 - JIRA 6129 - start
                            //sSQL.Append(" CVG_SEQUENCE_NO");
                            sSQL.Append(" CVG_SEQUENCE_NO, ");
                            //tanwar2 - JIRA 6129 - end
                            sSQL.Append(" TRANS_SEQ_NO, COVERAGE_KEY");     //Ankit Start : Worked on MITS - 34297
                            sSQL.Append(" FROM ");
                            sSQL.Append(" ( ");
                            sSQL.Append(" SELECT C.POLCVG_ROW_ID, CODE_DESC, COVERAGE_TEXT, SHORT_CODE,C.COVERAGE_TYPE_CODE ,CVG_SEQUENCE_NO,B.CNT, TRANS_SEQ_NO, COVERAGE_KEY FROM ");     //Ankit Start : Worked on MITS - 34297
                            sSQL.Append(" ( ");
                            sSQL.Append(" SELECT COVERAGE_TYPE_CODE,COUNT(*) CNT FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + sPolUnitRowId + " GROUP BY COVERAGE_TYPE_CODE");
                            sSQL.Append(" )B,");
                            sSQL.Append(" (");
                            sSQL.Append(" SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID, CODE_DESC, COVERAGE_TEXT, SHORT_CODE,COVERAGE_TYPE_CODE ,CVG_SEQUENCE_NO, TRANS_SEQ_NO, COVERAGE_KEY");       //Ankit Start : Worked on MITS - 34297
                            sSQL.Append(" FROM POLICY_X_CVG_TYPE");
                            if (!bIsFirstFinal)
                            {
                                sSQL.Append(" ,RESERVE_CURRENT");
                            }
                            sSQL.Append(" ,CODES_TEXT");
                            sSQL.Append(" WHERE");
                        }
                        else if (p_bDescriptionSearch)
                        {
                            if (bOracleCaseInSensitive)
                                sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE || ' ' ||CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "'");
                            else if (m_bIsOracle)
                                sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE || ' ' || CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                            else
                                sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE + ' ' +CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                        }
                        else
                        {
                            //rsushilaggar MITS 22195 Date 03/01/12
                            if (sLookupTemp.Contains("\",\""))
                            {
                                if (bOracleCaseInSensitive)
                                    sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + sLookupDesc + "' ) OR (UPPER(CODES.SHORT_CODE) LIKE '" + sLookupDesc + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' ) )  " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "'");
                                else
                                    sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((CODES.SHORT_CODE LIKE '" + p_sLookupString + "' AND CODES_TEXT.CODE_DESC LIKE '" + sLookupDesc + "' ) OR (CODES.SHORT_CODE LIKE '" + sLookupDesc + "' AND CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' ) )  " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                            }
                            else
                            {
                                if (bOracleCaseInSensitive)
                                {
                                    if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                                        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "'");
                                    //MITS:35039 START
                                    else
                                        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "') OR (UPPER(CODES_TEXT.CODE_DESC) LIKE '%" + p_sLookupString + "'))");
                                    //MITS:35039 END
                                }
                                else
                                {
                                    if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                                        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                                    //MITS:35039 START
                                    else
                                        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "') OR (CODES_TEXT.CODE_DESC LIKE '%" + p_sLookupString + "'))");
                                    //MITS:35039 END
                                }
                            }

                        }
                        #endregion


                        
                    }

                    else
                    {
                        if (p_bDescriptionSearch)
                        {
                            if (bOracleCaseInSensitive)
                                sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE || ' ' ||CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "'");
                            else if (m_bIsOracle)
                                sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE || ' ' || CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                            else
                                sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE + ' ' +CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                        }
                        else
                        {
                            //rsushilaggar MITS 22195 Date 03/01/12
                            if (sLookupTemp.Contains("\",\""))
                            {
                                if (bOracleCaseInSensitive)
                                    sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + sLookupDesc + "' ) OR (UPPER(CODES.SHORT_CODE) LIKE '" + sLookupDesc + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' ) )  " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "'");
                                else
                                    sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((CODES.SHORT_CODE LIKE '" + p_sLookupString + "' AND CODES_TEXT.CODE_DESC LIKE '" + sLookupDesc + "' ) OR (CODES.SHORT_CODE LIKE '" + sLookupDesc + "' AND CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' ) )  " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                            }
                            else
                            {
                                if (bOracleCaseInSensitive)
                                {
                                    if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                                        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "'");
                                    //MITS:35039 START
                                    else
                                        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(5).ToUpper() + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "') OR (UPPER(CODES_TEXT.CODE_DESC) LIKE '%" + p_sLookupString + "'))");
                                    //MITS:35039 END
                                }
                                else
                                {
                                    if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                                        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                                    //MITS:35039 START
                                    else
                                        sSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(5) + "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ((CODES.SHORT_CODE LIKE '" + p_sLookupString + "' " + sLOBSQL + "AND CODES.SHORT_CODE >= '" + p_sIndex + "') OR (CODES_TEXT.CODE_DESC LIKE '%" + p_sLookupString + "'))");
                                    //MITS:35039 END
                                }
                            }

                        }
						//added by swati for AIC Gap 7 MITS # 36929                        
                        if (!string.IsNullOrEmpty(p_sJurisdiction) && (string.Compare(p_sJurisdiction, "0") != 0) && string.Equals(p_sLookupType, "CODE.AIA_CODE_12", StringComparison.InvariantCultureIgnoreCase) && (string.Equals(p_sFormName, "claimgc", StringComparison.InvariantCultureIgnoreCase) || string.Equals(p_sFormName, "claimwc", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            sSQL.Append(" AND CODES.CODE_ID IN (SELECT CODE2 FROM CODE_X_CODE WHERE CODE1 = " + p_sJurisdiction + " AND REL_TYPE_CODE = " + objCache.GetCodeId("JDTOAIA12", "CODE_REL_TYPE").ToString() + " AND DELETED_FLAG =0)");
                        }
                        else if (!string.IsNullOrEmpty(p_sJurisdiction) && (string.Compare(p_sJurisdiction, "0") != 0) && string.Equals(p_sLookupType, "CODE.AIA_CODE_34", StringComparison.InvariantCultureIgnoreCase) && (string.Equals(p_sFormName, "claimgc", StringComparison.InvariantCultureIgnoreCase) || string.Equals(p_sFormName, "claimwc", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            sSQL.Append(" AND CODES.CODE_ID IN (SELECT CODE2 FROM CODE_X_CODE WHERE CODE1 = " + p_sJurisdiction + " AND REL_TYPE_CODE = " + objCache.GetCodeId("JDTOAIA34", "CODE_REL_TYPE").ToString() + " AND DELETED_FLAG =0)");
                        }
                        else if (!string.IsNullOrEmpty(p_sJurisdiction) && (string.Compare(p_sJurisdiction, "0") != 0) && string.Equals(p_sLookupType, "CODE.AIA_CODE_56", StringComparison.InvariantCultureIgnoreCase) && (string.Equals(p_sFormName, "claimgc", StringComparison.InvariantCultureIgnoreCase) || string.Equals(p_sFormName, "claimwc", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            sSQL.Append(" AND CODES.CODE_ID IN (SELECT CODE2 FROM CODE_X_CODE WHERE CODE1 = " + p_sJurisdiction + " AND REL_TYPE_CODE = " + objCache.GetCodeId("JDTOAIA56", "CODE_REL_TYPE").ToString() + " AND DELETED_FLAG =0)");
                        }                        
                        //change end here by swati
                        //asharma326 for jira 10762
                        else if ((p_sLookupType.ToUpper().Equals("CODE.DISTRIBUTION_TYPE")) && (!string.IsNullOrEmpty(p_sFilter)) && p_sFilter.ToLower() == "distributiontype_filter")
                        {
                            sSQL.Append(" AND CODES.SHORT_CODE NOT IN ( 'MAL' )");
                            p_sFilter = "";
                        }
                    }

                    if (p_sFilter != "")
                        sSQL.Append(" AND " + p_sFilter);
                    
                    #region if (p_sLookupType.ToUpper().Equals("CODE.COVERAGE_TYPE"))
                    if (p_sLookupType.ToUpper().Equals("CODE.COVERAGE_TYPE"))
                    {
					if (!p_sFieldName.Contains("supp")) //igupta3 for suppliment fields no policy checks req: MITS 36575 
                        {
                        if (!(string.Compare(p_sSessionClaimId, "0", true) == 0 && string.Compare(lClaimantEid.ToString(), "0", true) == 0) && !string.IsNullOrEmpty(p_sSessionClaimId))
                        {
                            //rupal:start for first & final payment
                            //if IsFirstFinal is set to true, then we assume that reserves are not already created, 
                            //we will create the reserve on the fly. therefore coverage type can not be found in reserve current table
                            // if IsFirstFinal is true then coverage type will be fetched directly from policy
                            if (bIsFirstFinal)
                            {
                                if (iPolSystemId > 0 && objSysSetting.MultiCovgPerClm != 0)
                                {
                                    sSQL.Append(" POLICY_UNIT_ROW_ID =" + sPolUnitRowId + " AND CODES_TEXT.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE");
                                }
                                else
                                {
                                    sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(COVERAGE_TYPE_CODE) FROM POLICY_X_CVG_TYPE WHERE ");
                                    //sSQL.Append(" POLICY_X_CVG_TYPE.POLICY_ID =" + p_sPolicyId + ")");
                                    sSQL.Append(" POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID =" + sPolUnitRowId + ")");
                                }
                            }
                            //rupal :end for first & final payment
                            else
                            {
                                //commented by rupal as Reserve_Current Table does not have Policy_ID as per new changes
                                //sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(COVERAGE_TYPE_CODE) FROM POLICY_X_CVG_TYPE,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID");
                                //sSQL.Append(" AND RESERVE_CURRENT.POLICY_ID =" + p_sPolicyId + " AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");

                                //rupal
                                //RUPAL:START, POLICY SYSTEM INTERFACE
                                if (iPolSystemId > 0 && objSysSetting.MultiCovgPerClm != 0)
                                {
                                    sSQL.Append(" POLICY_UNIT_ROW_ID =" + sPolUnitRowId + " AND CODES_TEXT.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE");
                                    sSQL.Append(" AND POLICY_X_CVG_TYPE.POLCVG_ROW_ID = RESERVE_CURRENT.POLCVG_ROW_ID AND RESERVE_CURRENT.CLAIM_ID = " + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID = " + lClaimantEid);
                                }
                                //RUPAL:END, POLICY SYSTEM INTERFACE 
                                else
                                {
                                    sSQL.Append(" AND CODES.CODE_ID IN (SELECT DISTINCT(COVERAGE_TYPE_CODE) FROM POLICY_X_CVG_TYPE,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID");
                                    //sSQL.Append(" AND POLICY_X_CVG_TYPE.POLICY_ID =" + p_sPolicyId + " AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");
                                    sSQL.Append(" AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID =" + sPolUnitRowId + " AND RESERVE_CURRENT.CLAIM_ID =" + p_sSessionClaimId + " AND RESERVE_CURRENT.CLAIMANT_EID =" + lClaimantEid + ")");
									}
                                }
                            }
                        }
                        //RUPAL:START,POLICY SYSTEM INTERFACE
                        if (iPolSystemId > 0 && objSysSetting.MultiCovgPerClm != 0)
                        {
                            sSQL.Append(" ) C WHERE B.COVERAGE_TYPE_CODE = C.COVERAGE_TYPE_CODE");
                            sSQL.Append(" )A ");
                            sSQL.Append(" ,CODES_TEXT, GLOSSARY, CODES");                            
                            sSQL.Append(" WHERE ");
                            sSQL.Append(" A.COVERAGE_TYPE_CODE=CODES.CODE_ID AND A.COVERAGE_TYPE_CODE=CODES_TEXT.CODE_ID");
                            //sSQL.Append(" AND UPPER(GLOSSARY.SYSTEM_TABLE_NAME) ='" + p_sLookupType + "'");

                            //+ 
                            if (p_bDescriptionSearch)
                            {                              
                                if (bOracleCaseInSensitive)
                                    sSQL.Append(" AND UPPER(GLOSSARY.SYSTEM_TABLE_NAME) ='" + p_sLookupType.Substring(5).ToUpper() + "' AND UPPER(CODES.SHORT_CODE || ' ' || A.CODE_DESC) LIKE '" + p_sLookupString + "' AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "'");
                                else if (m_bIsOracle)
                                    sSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME ='" + p_sLookupType.Substring(5) + "' AND CODES.SHORT_CODE || ' ' || A.CODE_DESC LIKE '" + p_sLookupString + "' AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                                else
                                    sSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME ='" + p_sLookupType.Substring(5) + "' AND CODES.SHORT_CODE + ' ' + A.CODE_DESC LIKE '" + p_sLookupString + "' AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                            }
                            else
                            {
                                //rsushilaggar MITS 22195 Date 03/01/12
                                if (sLookupTemp.Contains("\",\""))
                                {
                                    if (bOracleCaseInSensitive)
                                        sSQL.Append(" AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + sLookupDesc + "' ) OR (UPPER(CODES.SHORT_CODE) LIKE '" + sLookupDesc + "' AND UPPER(CODES_TEXT.CODE_DESC) LIKE '" + p_sLookupString + "' ) )  AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "'");
                                    else
                                        sSQL.Append(" AND ((CODES.SHORT_CODE LIKE '" + p_sLookupString + "' AND CODES_TEXT.CODE_DESC LIKE '" + sLookupDesc + "' ) OR (CODES.SHORT_CODE LIKE '" + sLookupDesc + "' AND CODES_TEXT.CODE_DESC LIKE '" + p_sLookupString + "' ) )  AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                                }
                                else
                                {
                                    if (bOracleCaseInSensitive)
                                    {
                                        if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                                            sSQL.Append(" AND UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "'");
                                        //MITS:35039 START
                                        else
                                            sSQL.Append(" AND ((UPPER(CODES.SHORT_CODE) LIKE '" + p_sLookupString + "' AND UPPER(CODES.SHORT_CODE) >= '" + p_sIndex.ToUpper() + "') OR (UPPER(CODES_TEXT.CODE_DESC) LIKE '%" + p_sLookupString + "'))");
                                        //MITS:35039 END
                                    }
                                    else
                                    {
                                        if (objSysSetting.SearchCodeDescription == 0)//MITS:35039
                                            sSQL.Append(" AND CODES.SHORT_CODE LIKE '" + p_sLookupString + "'  AND CODES.SHORT_CODE >= '" + p_sIndex + "'");
                                        //MITS:35039 START
                                        else
                                            sSQL.Append(" AND ((CODES.SHORT_CODE LIKE '" + p_sLookupString + "'  AND CODES.SHORT_CODE >= '" + p_sIndex + "') OR (CODES_TEXT.CODE_DESC LIKE '%" + p_sLookupString + "'))");
                                        //MITS:35039 END
                                    }
                                }
                            }
                            sSQL.Append(" AND GLOSSARY.TABLE_ID=CODES.TABLE_ID ");
                            sSQL.Append(sLOBSQL);                            
                        }
                        //RUPAL:END,POLICY SYSTEM INTERFACE
                    }
                    #endregion
                }
                //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                else if (p_sLookupType.ToUpper().Equals("ENTITY.ADJUSTERS") && !string.IsNullOrEmpty(sParentCodeID))
                {
                    sSQL.Append("SELECT CLAIM_ADJUSTER.ADJUSTER_EID ADJ_ROW_ID, LAST_NAME, FIRST_NAME ");
                    sSQL.Append(" FROM CLAIM_ADJUSTER");
                    sSQL.Append(" INNER JOIN ENTITY ON ENTITY.ENTITY_ID = CLAIM_ADJUSTER.ADJUSTER_EID");
                    sSQL.Append(" WHERE CLAIM_ID = " + sParentCodeID);

                    if (bOracleCaseInSensitive)
                    {
                        sSQL.Append(" AND UPPER(LAST_NAME) LIKE '" + p_sLookupString + "'");
                    }
                    else
                    {
                        sSQL.Append(" AND LAST_NAME LIKE '" + p_sLookupString + "'");
                    }

                }
                //Ankit End
                else if ((p_sLookupType.Length > 5) && (p_sLookupType.ToUpper().Substring(0, 6).Equals("ENTITY")))
                {
                    if (p_sLookupType.IndexOf(".") > 0)
                    {
                        //avipinsrivas Start : Worked for Jira-340
                        string sSystemTableName = p_sLookupType.Substring(7);

                        //avipinsrivas Start : Worked for 7480 (Issue of 4634 - Epic 340)
                        bool bFromEntityPeople = (objSysSetting.UseEntityRole && (int.Equals(objCache.GetGlossaryType(objCache.GetTableId(sSystemTableName)), 4) || (int.Equals(objCache.GetGlossaryType(objCache.GetTableId(sSystemTableName)), 7))));
                        bool bGlobalSearch = (objSysSetting.UseEntityRole && Enum.IsDefined(typeof(Globalization.EntityGlossaryTableNames), sSystemTableName.ToUpper()));
                        //avipinsrivas End
                        //Specific Search for Entity Type
                        //if (bOracleCaseInSensitive)
                        //    sSQL.Append("SELECT ENTITY_ID, LAST_NAME, FIRST_NAME FROM ENTITY,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" + p_sLookupType.Substring(7).ToUpper() + "' AND GLOSSARY.TABLE_ID=ENTITY.ENTITY_TABLE_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND UPPER(LAST_NAME) LIKE '" + p_sLookupString + "' AND UPPER(LAST_NAME) >= '" + p_sIndex.ToUpper() + "' ORDER BY LAST_NAME");
                        //else
                        //    sSQL.Append("SELECT ENTITY_ID, LAST_NAME, FIRST_NAME FROM ENTITY,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + p_sLookupType.Substring(7) + "' AND GLOSSARY.TABLE_ID=ENTITY.ENTITY_TABLE_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND LAST_NAME LIKE '" + p_sLookupString + "' AND LAST_NAME >= '" + p_sIndex + "' ORDER BY LAST_NAME");

                        sSQL.Append(" SELECT DISTINCT E.ENTITY_ID, E.LAST_NAME, E.FIRST_NAME ");    //avipinsrivas Start : Worked for 7480 (Issue of 4634 - Epic 340)
                        sSQL.Append(" FROM ENTITY E ");
                        if (!bFromEntityPeople)
                            sSQL.Append(" INNER JOIN GLOSSARY G ON E.ENTITY_TABLE_ID = G.TABLE_ID ");

                        //avipinsrivas Start : Worked for 7480 (Issue of 4634 - Epic 340)
                        sSQL.Append(" WHERE ");
                        if (!bGlobalSearch)
                        {
                            if (bOracleCaseInSensitive)
                                sSQL.Append(" UPPER(G.SYSTEM_TABLE_NAME)='" + sSystemTableName.ToUpper() + "' AND ");
                            else
                                sSQL.Append(" G.SYSTEM_TABLE_NAME='" + sSystemTableName + "' AND ");
                        }
                        sSQL.Append(" (E.DELETED_FLAG = 0 OR E.DELETED_FLAG IS NULL) ");
                        //avipinsrivas End

                        if (bOracleCaseInSensitive)
                        {
                            sSQL.Append(" AND UPPER(E.LAST_NAME) LIKE '" + p_sLookupString + "' ");
                            sSQL.Append(" AND UPPER(E.LAST_NAME) >= '" + p_sIndex.ToUpper() + "' ");
                        }
                        else
                        {
                            sSQL.Append(" AND E.LAST_NAME LIKE '" + p_sLookupString + "' ");
                            sSQL.Append(" AND E.LAST_NAME >= '" + p_sIndex + "' ");
                        }
                        sSQL.Append(" ORDER BY E.LAST_NAME ");
                        //avipinsrivas End
                    }
                    else
                    {
                        //avipinsrivas Start : Worked for Jira-340
                        // All Entities search		
                        //if (bOracleCaseInSensitive)//Parijat :MITS 18834 :start
                        //    sSQL.Append("SELECT ENTITY_ID, LAST_NAME, FIRST_NAME ,ADDR1 ,CITY ,EMAIL_ADDRESS FROM ENTITY WHERE (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND UPPER(LAST_NAME) LIKE '" + p_sLookupString + "' AND UPPER(LAST_NAME) >= '" + p_sIndex.ToUpper() + "' ORDER BY LAST_NAME,FIRST_NAME,ADDR1,CITY,EMAIL_ADDRESS");
                        //else
                        //    sSQL.Append("SELECT ENTITY_ID, LAST_NAME, FIRST_NAME ,ADDR1 ,CITY ,EMAIL_ADDRESS FROM ENTITY WHERE (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND LAST_NAME LIKE '" + p_sLookupString + "' AND LAST_NAME >= '" + p_sIndex + "' ORDER BY LAST_NAME,FIRST_NAME,ADDR1,CITY,EMAIL_ADDRESS");

                        sSQL.Append(" SELECT E.ENTITY_ID, E.LAST_NAME, E.FIRST_NAME, E.ADDR1, E.CITY, E.EMAIL_ADDRESS ");
                        sSQL.Append(" FROM ENTITY E ");
   
                        sSQL.Append(" WHERE (E.DELETED_FLAG = 0 OR E.DELETED_FLAG IS NULL) ");
                        if (bOracleCaseInSensitive)
                        {
                            sSQL.Append(" AND UPPER(E.LAST_NAME) LIKE '" + p_sLookupString + "' ");
                            sSQL.Append(" AND UPPER(E.LAST_NAME) >= '" + p_sIndex.ToUpper() + "' ");
                        }
                        else
                        {
                            sSQL.Append(" AND E.LAST_NAME LIKE '" + p_sLookupString + "' ");
                            sSQL.Append(" AND E.LAST_NAME >= '" + p_sIndex + "' ");
                        }
                        sSQL.Append(" ORDER BY LAST_NAME,FIRST_NAME,ADDR1,CITY,EMAIL_ADDRESS ");
                        //avipinsrivas End
                    }//Parijat :MITS 18834 :end
                }
                else if (p_sLookupType.ToUpper().Equals("VEHICLE"))
                {
                    if (bOracleCaseInSensitive)
                        sSQL.Append("SELECT UNIT_ID, VIN, VEHICLE_MAKE, VEHICLE_MODEL, VEHICLE_YEAR FROM VEHICLE WHERE UPPER(VIN) LIKE '" + p_sLookupString + "' AND UPPER(VIN) >= '" + p_sIndex.ToUpper() + "' ORDER BY VIN");
                    else
                        sSQL.Append("SELECT UNIT_ID, VIN, VEHICLE_MAKE, VEHICLE_MODEL, VEHICLE_YEAR FROM VEHICLE WHERE VIN LIKE '" + p_sLookupString + "' AND VIN >= '" + p_sIndex + "' ORDER BY VIN");
                }
                else if (p_sLookupType.ToUpper().Equals("CLAIM"))
                {
                    if (bOracleCaseInSensitive)
                        sSQL.Append("SELECT CLAIM_ID, CLAIM_NUMBER, EVENT_NUMBER FROM CLAIM WHERE UPPER(CLAIM_NUMBER) LIKE '" + p_sLookupString + "' AND UPPER(CLAIM_NUMBER) >= '" + p_sIndex.ToUpper() + "' ORDER BY CLAIM_NUMBER");
                    else
                        sSQL.Append("SELECT CLAIM_ID, CLAIM_NUMBER, EVENT_NUMBER FROM CLAIM WHERE CLAIM_NUMBER LIKE '" + p_sLookupString + "' AND CLAIM_NUMBER >= '" + p_sIndex + "' ORDER BY CLAIM_NUMBER");
                }
                else if (p_sLookupType.ToUpper().Equals("EVENT"))
                {
                    if (bOracleCaseInSensitive)
                        sSQL.Append("SELECT EVENT_ID, EVENT_NUMBER, CODES_TEXT.CODE_DESC FROM EVENT,CODES_TEXT WHERE CODES_TEXT.CODE_ID=EVENT.EVENT_TYPE_CODE AND UPPER(EVENT_NUMBER) LIKE '" + p_sLookupString + "' AND UPPER(EVENT_NUMBER) >= '" + p_sIndex.ToUpper() + "' ORDER BY EVENT_NUMBER");
                    else
                        sSQL.Append("SELECT EVENT_ID, EVENT_NUMBER, CODES_TEXT.CODE_DESC FROM EVENT,CODES_TEXT WHERE CODES_TEXT.CODE_ID=EVENT.EVENT_TYPE_CODE AND EVENT_NUMBER LIKE '" + p_sLookupString + "' AND EVENT_NUMBER >= '" + p_sIndex + "' ORDER BY EVENT_NUMBER");
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("CodesListManager.QuickLookup.UnknownType", m_iClientId));
                }

                //Shruti for 12115
                //PJS MITS 11571,03-11-2008 - Adding code for filtering codes     
                //Start:Commented by Nitin goel, MITS 30910 NI changes.09/02/2013 and added Coverage_group also in the list
                //if ((p_sLookupType.Length > 4) && (p_sLookupType.ToUpper().Substring(0, 5).Equals("CODE."))
                //    && p_sLookupType.ToUpper() != "CODE.ORGH" && p_sLookupType.ToUpper() != "CODE.STATES"
                //    && p_sLookupType.ToUpper() != "CODE.RM_SYS_USERS")
                    if ((p_sLookupType.Length > 4) && (p_sLookupType.ToUpper().Substring(0, 5).Equals("CODE."))
                    && p_sLookupType.ToUpper() != "CODE.ORGH" && p_sLookupType.ToUpper() != "CODE.STATES"
                    && p_sLookupType.ToUpper() != "CODE.RM_SYS_USERS" && p_sLookupType.ToUpper() != "CODE.COVERAGE_GROUP")
                    //end:Commented by Nitin goel, MITS 30910 NI changes.09/02/2013
                {
                    if (sLOBSQL != string.Empty)
                        sSQL.Append(sLOBSQL);
                    if (p_lDeptEId > 0)
                    {
                        sSQL.Append("	AND (CODES.ORG_GROUP_EID=" + p_lDeptEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lFacEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lLocEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lDivEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lRegEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lOprEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lComEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=" + lCliEId);
                        sSQL.Append("	OR CODES.ORG_GROUP_EID IS NULL ");
                        //PJS MITS 15220 
                        if (!String.IsNullOrEmpty(p_sInsuredEid) && p_sFormName == "exposure")
                        {
                            if (!String.IsNullOrEmpty(sInsParentEid))
                            {
                                sSQL.Append("	OR CODES.ORG_GROUP_EID IN ( ");
                                sSQL.Append(sInsParentEid);
                                sSQL.Append(")");
                            }
                        }
                        //PJS MITS 15220
                        sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                    }
                    else
                    {
                        //pmittal5  MITS:12318  06/13/08
                        if (p_sTitle != "Search" && p_sTitle != "ExpRateParms" && p_sFormName.ToLower() != "userprivileges")    // Added by csingh7 MITS 15220, MITS 37576, JIRA 6596:aaggarwal29    
                        {
                            sSQL.Append("	AND (CODES.ORG_GROUP_EID IS NULL ");
                            //PJS MITS 15220 
                            if (!String.IsNullOrEmpty(p_sInsuredEid) && p_sFormName == "exposure")
                            {
                                if (!String.IsNullOrEmpty(sInsParentEid))
                                {
                                    sSQL.Append("	OR CODES.ORG_GROUP_EID IN ( ");
                                    sSQL.Append(sInsParentEid);
                                    sSQL.Append(")");
                                }
                            }
                            //PJS MITS 15220
                            sSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                        }
                    }
                    switch (p_sFormName.ToLower())
                    {
                        case "":
                            // apeykov - RMA-1723 This issue was fixed 14.3 branch (MITS 36614) and merged - begin
                            if (p_sTitle != "Search")
                            {
                                sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                                break;
                            }
                            // apeykov - RMA-1723 - end 
                            break;
                        //sgoel6 Medicare 05/13/2009
                        case "lineofbuscodeselfinsured":
                            sSQL.Append("	AND CODES.SHORT_CODE NOT IN ('DI') ");
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                            break;
                        case "event":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "claimgc":
                        case "claimwc":
                        case "claimva":
                        case "claimdi":
                        case "claimpc":
                        //Shruti for 11708
                        case "claimant":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            if (p_sEventDate.Length > 0)
                            {
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sEventDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sEventDate + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sEventDate + "' AND CODES.EFF_END_DATE>='" + p_sEventDate + "') ");
                            }
                            //zalam 08/27/2008 Mits:-11708 Start
                            // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            if (iSessionClaimId > 0 && (!string.IsNullOrEmpty(p_sSessionClaimId)))
                            // npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            {
                                String sDateOfClaim = string.Empty;
                                String sDateOfEvent = string.Empty;
                                String sDateOfPolicy = string.Empty;
                                GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy, objConn, objSysSetting, p_sLOB);
                                if (!string.IsNullOrEmpty(sDateOfPolicy))
                                {
                                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                                }
                            }
                            //zalam 08/27/2008 Mits:-11708 End
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "policy":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                        case "funds":
                        case "split":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");

                            //pmittal5  MITS:12289  06/19/08  -Adding trigger for event,claim and policy date in case of GL Accounts    
                            //sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')");

                            // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            if (iSessionClaimId <= 0 || string.IsNullOrEmpty(p_sSessionClaimId))//MITS 14661 Issue 4 03/25/2009 Added (p_sSessionClaimId == "") option
                            // npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                            {
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT')");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM')");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE'))");
                            }
                            else
                            {
                                String sDateOfClaim = string.Empty;
                                String sDateOfEvent = string.Empty;
                                String sDateOfPolicy = string.Empty;
                                GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy, objConn, objSysSetting, p_sLOB);

                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + sDateOfEvent + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfEvent + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + sDateOfEvent + "' AND CODES.EFF_END_DATE>='" + sDateOfEvent + "') ");

                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + sDateOfClaim + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfClaim + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + sDateOfClaim + "' AND CODES.EFF_END_DATE>='" + sDateOfClaim + "') ");

                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "'))");
                            }
                            //End - pmittal5
                            break;
                        case "employee":
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                            break;
                            //rsushilaggar mits 23969
                        default:
                            sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                            break;
                            //End rsushilaggar
                    }

                    //PJS MITS 11571,03-11-2008, Adding code above

                    if (p_sFilter != "")
                        sSQL.Append(" AND " + p_sFilter);
                    //Deb Multi Currency
                    if (p_sLookupType.Substring(5).ToUpper() == "CURRENCY_TYPE" && objSysSetting.UseMultiCurrency == 0)
                    {
                        sSQL.Append(" AND " + "CODES.CODE_ID=" + objSysSetting.BaseCurrencyType);
                    }
                    //Deb Multi Currency
                    //Added ExtraFilter for Adjuster and Enhanced Notes when BES enabled:Start for MITS 16650
                    if (m_bIsOrgSec)
                    {
                        GetExtraFilter(p_sLookupType.Substring(5).ToUpper(), sSQL);
                    }
                    //End
                    sSQL.Append(" ORDER BY CODES.SHORT_CODE");
                }

                //sgoel6 Medicare 05/13/2009
                if (p_sFormName.ToLower() == "lineofbuscodeselfinsured" && p_sLookupString.ToLower() == "all%")
                {
                    CodeType tmpcode = new CodeType();
                    CodeListType retCodes = null;
                    retCodes = new CodeListType();
                    retCodes.Codes = new System.Collections.Generic.List<CodeType>();

                    string sCodeId = "-3";
                    string sShortCode = "All";
                    string sCodeDesc = "All";
                    string sParentCode = "\" \"";

                    sCodeDesc = sCodeDesc.Replace((char)34, ' ');
                    sParentCode = sParentCode.Replace((char)34, ' ');

                    tmpcode.Id = Conversion.ConvertStrToInteger(sCodeId);
                    tmpcode.Desc = sCodeDesc + " " + sShortCode;
                    tmpcode.ParentCode = sParentCode;
                    tmpcode.parentDesc = "";
                    tmpcode.ShortCode = sShortCode;
                    retCodes.Codes.Add(tmpcode);
                    objCodeList = retCodes;
                }
                else
                {
                    objCodeList = new CodeListType();
                    objCodeList.Codes = new System.Collections.Generic.List<CodeType>();
                    //Aman ML Change                
                    if (this.CodesTable)
                    {
                        //p_sLookupType.ToUpper().Equals("CODE.EVENT_TYPE")
                        //Ashish Ahuja : Mits 32888 Temporary Condition Check of Multilingual
                        if (isMultiLingual())
                        {   
                            sSQL = PrepareMultilingualQuery(sSQL.ToString(), p_sLookupType.Substring(p_sLookupType.IndexOf(".") + 1));
                        }
                    }
                    //Aman ML Change 
                    objCodeList = GetQuickLookupData(sSQL.ToString(), sConnectionString, bIsSecurityDB, iCurrentPageNumber,objConn);
                }
                //get the codes xml after searching
                //objQuickLookupXml = GetQuickLookupXML(sSQL.ToString(), sConnectionString, bIsSecurityDB);
            }
            catch (RMAppException p_objExp)
            {
                throw (p_objExp);
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.QuickLookup.Error", m_iClientId), p_objExp);
            }
            finally
            {
                objSysSetting = null;
                if (objCache != null)
                    objCache.Dispose();
            }
            return objCodeList;
        }


        #region "Private Functions"

        // akaushik5 Added for MITS 36567/ JIRA-494 Starts
        /// <summary>
        /// Gets the trigger criteria.
        /// </summary>
        /// <param name="p_sFormName">Name of the P_S form.</param>
        /// <param name="p_sTriggerDate">The P_S trigger date.</param>
        /// <param name="sSQL">The s SQL.</param>
        /// <param name="tableAlias">The table alias.</param>
        /// <param name="sToday">The s today.</param>
        private void GetTriggerCriteria(string p_sFormName, string p_sTriggerDate, StringBuilder sSQL, string tableAlias, string sToday)
        {
            sSQL.AppendFormat(" AND (({0}.TRIGGER_DATE_FIELD IS NULL) ", tableAlias);
            
            switch (p_sFormName.ToLower())
            {
                case "event":
                        sSQL.AppendFormat("OR ({0}.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (({0}.EFF_START_DATE IS NULL AND {0}.EFF_END_DATE IS NULL) ", tableAlias);
                        sSQL.AppendFormat("OR ({1}.EFF_START_DATE<='{0}' AND {1}.EFF_END_DATE IS NULL)", p_sTriggerDate, tableAlias);
                        sSQL.AppendFormat("OR ({1}.EFF_START_DATE IS NULL AND {1}.EFF_END_DATE>='{0}') ", p_sTriggerDate, tableAlias);
                        sSQL.AppendFormat("OR ({1}.EFF_START_DATE<='{0}' AND {1}.EFF_END_DATE>='{0}')))", p_sTriggerDate, tableAlias);
                    break;
                case "claimgc":
                case "claimwc":
                case "claimva":
                case "claimdi":
                case "claimpc":
                        sSQL.AppendFormat(" OR ({0}.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (({0}.EFF_START_DATE IS NULL AND {0}.EFF_END_DATE IS NULL)", tableAlias);
                        sSQL.AppendFormat("OR ({1}.EFF_START_DATE<='{0}' AND {1}.EFF_END_DATE IS NULL)", p_sTriggerDate, tableAlias);
                        sSQL.AppendFormat("OR ({1}.EFF_START_DATE IS NULL AND {1}.EFF_END_DATE>='{0}') ", p_sTriggerDate, tableAlias);
                        sSQL.AppendFormat("OR ({1}.EFF_START_DATE<='{0}' AND {1}.EFF_END_DATE>='{0}')))", p_sTriggerDate, tableAlias);
                    break;
                case "policy":
                case "policyenh":
                        sSQL.AppendFormat(" OR ({0}.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE,SYSTEM_DATE' AND (({0}.EFF_START_DATE IS NULL AND {0}.EFF_END_DATE IS NULL) ", tableAlias);
                        sSQL.AppendFormat("OR ({1}.EFF_START_DATE<='{0}' AND {1}.EFF_END_DATE IS NULL)", p_sTriggerDate, tableAlias);
                        sSQL.AppendFormat("OR ({1}.EFF_START_DATE IS NULL AND {1}.EFF_END_DATE>='{0}') ", p_sTriggerDate, tableAlias);
                        sSQL.AppendFormat("OR ({1}.EFF_START_DATE<='{0}' AND {1}.EFF_END_DATE>='{0}')))", p_sTriggerDate, tableAlias);
                    break;
                default:
                    sSQL.AppendFormat(" OR ({0}.TRIGGER_DATE_FIELD <> 'SYSTEM_DATE')", tableAlias);
                    break;
            }

            sSQL.AppendFormat(" OR ({0}.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (({0}.EFF_START_DATE IS NULL AND {0}.EFF_END_DATE IS NULL) ", tableAlias);
            sSQL.AppendFormat("OR ({1}.EFF_START_DATE<='{0}' AND {1}.EFF_END_DATE IS NULL)", sToday, tableAlias);
            sSQL.AppendFormat("OR ({1}.EFF_START_DATE IS NULL AND {1}.EFF_END_DATE>='{0}') ", sToday, tableAlias);
            sSQL.AppendFormat("OR ({1}.EFF_START_DATE<='{0}' AND {1}.EFF_END_DATE>='{0}'))))", sToday, tableAlias);
        }
        // akaushik5 Added for MITS 36567/ JIRA-494 Ends

        //Ankit Start : Worked on MITS - 34657
        private DbReader GetPolicyInfo(string sPolicyId,DbConnection objconn)
        {
            System.Collections.Generic.Dictionary<string, string> dictParams = null;
            try
            {
                //dictParams = new System.Collections.Generic.Dictionary<string, string>();
                //dictParams.Add("POLICYID", sPolicyId);
                if (objconn == null) { objconn = DbFactory.GetDbConnection(m_sConnectionString); objconn.Open(); }
                return objconn.ExecuteReader(string.Format("SELECT POLICY_LOB_CODE,POLICY_SYSTEM_ID FROM POLICY WHERE POLICY_ID={0}", sPolicyId));
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                dictParams = null;
            }
        }
        //Ankit End
        private int GetCoverageType(string sPolXCvgId)
        {
            int iReturn = 0;
            int iPolXCvgId=0;
            try
            {
                iPolXCvgId = Conversion.ConvertStrToInteger( sPolXCvgId.Split('#')[0]);
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT COVERAGE_TYPE_CODE FROM POLICY_X_CVG_TYPE WHERE POLCVG_ROW_ID =" + iPolXCvgId))
                {
                    if (objReader.Read())
                    {
                        iReturn = Conversion.ConvertObjToInt(objReader.GetValue("COVERAGE_TYPE_CODE"), m_iClientId);
                    }

                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return iReturn;
        }
        /// <summary>
        /// Gets the maximum number of records displayed 
        /// </summary>
        /// <returns></returns>
        private int GetMaxRecordsonPage()
        {
           // StringBuilder sbSQL = null;
            int iReturn = 0;
            SysSettings objSettings = null;
           // DbReader objReader = null;
            try
            {
                //sbSQL = new StringBuilder();
                //sbSQL.Append("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE");
                //sbSQL.Append(" WHERE PARM_NAME = 'CODES_USER_LIMIT' ");
                //objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);
                //if (objReader.Read())
                if (objSettings.CodeUserLimit > 0)
                {
                    //iReturn = Conversion.ConvertObjToInt(objReader.GetValue("STR_PARM_VALUE"), m_iClientId);
                    iReturn = objSettings.CodeUserLimit;
                }
                else
                {
                    iReturn = 100;  // JP 12.15.2005  Was returning 0 which was bad. Caused divide by zero error.
                }
                //if (!objReader.IsClosed)
                //{
                //    objReader.Close();
                //}
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                //sbSQL = null;
                //if (!objReader.IsClosed)
                //{
                //    objReader.Close();
                //}
                //objReader.Dispose();
                objSettings = null;
            }
            return iReturn;
        }
        /// Name		: GetQuickLookupXML
        /// Author		: Nikhil Kumar Garg
        /// Date Created: 13-May-2005
        ///*************************************************************************
        /// Amendment History
        ///*************************************************************************
        /// Date Amended   *   Amendment   *								Author
        ///*************************************************************************
        /// <summary>
        /// Searches for the codes and returns the xml
        /// </summary>
        /// <param name="p_sSQL">SQL Search String</param>
        /// <returns>XML Document containing the codes list</returns>
        private XmlDocument GetQuickLookupXML(string p_sSQL, string sConnectionString, bool bIsSecurityDB)
        {
            DbReader objReader = null;			//reader object for searching
            XmlDocument objCodesXml = null;		//Xml to return
            XmlElement objElement = null;			//used for creating the Xml for the codes
            XmlElement objChildElement = null;	//used for creating the Xml for the codes
            string sText = string.Empty;			//used for creating the Xml for the codes
            LocalCache objCache = null;
            int iCodeID = 0;

            try
            {
                //if search sql is not empty then fire ther query and get the search results
                if (!(p_sSQL.Equals(string.Empty)))
                {
                    objReader = DbFactory.GetDbReader(sConnectionString, p_sSQL);

                    objCodesXml = new XmlDocument();
                    objElement = objCodesXml.CreateElement("codes");
                    objCodesXml.AppendChild(objElement);

                    if (!bIsSecurityDB) objCache = new LocalCache(sConnectionString,m_iClientId);

                    //create xml for the search results
                    while (objReader.Read())
                    {
                        objElement = objCodesXml.CreateElement("code");

                        iCodeID = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                        objChildElement = objCodesXml.CreateElement("id");
                        objChildElement.InnerText = iCodeID.ToString();
                        objElement.AppendChild(objChildElement);

                        objChildElement = objCodesXml.CreateElement("text");
                        //create innertext by combining all the columns
                        sText = string.Empty;
                        string sBirthDate = string.Empty;
                        for (int i = 1; i < objReader.FieldCount; i++)
                        {
                            if (objReader.GetName(i).ToUpper().Equals("BIRTH_DATE"))
                            {
                                sBirthDate = Conversion.ConvertObjToStr(objReader.GetValue(i));
                                sText = sText + " " + sBirthDate.Substring(5, 2) + "/" + sBirthDate.Substring(sBirthDate.Length - 2, 2) + "/" + sBirthDate.Substring(0, 4);
                            }
                            else
                            {
                                sText = sText + Conversion.ConvertObjToStr(objReader.GetValue(i)) + " ";
                            }
                        }
                        objChildElement.InnerText = sText;
                        objElement.AppendChild(objChildElement);

                        // For the parent
                        if (!bIsSecurityDB)
                        {
                            objChildElement = objCodesXml.CreateElement("parenttext");
                            objChildElement.InnerText = objCache.GetShortCode(objCache.GetRelatedCodeId(iCodeID)) + " " + objCache.GetCodeDesc(objCache.GetRelatedCodeId(iCodeID));
                            objElement.AppendChild(objChildElement);
                        }
                        objCodesXml.DocumentElement.AppendChild(objElement);
                    }
                }
            }
            catch (DataModelException p_objExp)
            {
                throw (p_objExp);
            }
            catch (RMAppException p_objExp)
            {
                throw (p_objExp);
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.QuickLookup.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Clear(m_iClientId);
                    objCache.Dispose();
                }
            }
            return objCodesXml;
        }
        /// Name		: GetQuickLookupData
        /// Author		: Parag Sarin
        /// Date Created: 07-May-2008
        ///*************************************************************************
        /// Amendment History
        ///*************************************************************************
        /// Date Amended   *   Amendment   *								Author
        ///*************************************************************************
        /// <summary>
        /// Searches for the codes and returns the xml
        /// </summary>
        /// <param name="p_sSQL">SQL Search String</param>
        /// <returns>XML Document containing the codes list</returns>
        private CodeListType GetQuickLookupData(string p_sSQL, string sConnectionString, bool bIsSecurityDB, Int32 p_iPageNumber,DbConnection objConn)
        {
            DbReader objReader = null;			//reader object for searching
            string sText = string.Empty;			//used for creating the Xml for the codes
            LocalCache objCache = null;
            int iCodeID = 0;
            CodeListType retCodes = null;

            int iRecordCount = 0;
            //string sSQLCount = "SELECT COUNT(*) ";
            bool bInSuccess = false;
            int iRecordsPerPage = 500;
            string sRecordsPerPage = string.Empty;
            //int p_iPageNumber = 1;
            int iStartIndex = 0;
            int iEndIndex = 0;
            int iTotalNumberOfPages = 0;
            int iLoopCounter = 0;
            //int iIndex = 0;
            DataTable dt = null;
			bool bAccess = true;
            bool bSuccess;      //avipinsrivas Start : Worked for Jira-340
            SysSettings objSyssettings = null;//RMA-6871
            try
            {
                if (bIsSecurityDB)
                {
                    objConn.Close();
                    objConn.Dispose();
                    objConn = DbFactory.GetDbConnection(sConnectionString);
                }
                retCodes = new CodeListType();
                //if search sql is not empty then fire ther query and get the search results
                if (!(p_sSQL.Equals(string.Empty)))
                {
                    retCodes.Codes = new System.Collections.Generic.List<CodeType>();
                    //Aman ML Change
                    //Ashish Ahuja : Mits 32888 Temporary Condition Check of Multilingual
                    //if (isMultiLingual())
                    //{
                    //    if (this.CodesTable)
                    //    {
                    //        iIndex = p_sSQL.IndexOf("UNION");
                    //    }
                    //    else
                    //    {
                    //        iIndex = p_sSQL.IndexOf("ORDER BY");
                    //    }
                    //}
                    //else
                    //{
                    //    iIndex = p_sSQL.IndexOf("ORDER BY");
                    //}
                    //Aman ML Change                
                    //if (iIndex == -1)
                    //{
                    //    sSQLCount = sSQLCount + p_sSQL.Substring(p_sSQL.IndexOf(" FROM "));
                    //}
                    //else
                    //{                                           
                    //    sSQLCount = sSQLCount + p_sSQL.Substring(p_sSQL.IndexOf(" FROM "), (iIndex  - p_sSQL.IndexOf(" FROM ")));
                    //}
                    //iRecordCount = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(sConnectionString, sSQLCount).ToString(), out bInSuccess);
                    //iRecordCount = objConn.ExecuteInt(sSQLCount);
                    //Aman ML Change
                    //Ashish Ahuja : Mits 32888 Temporary Condition Check of Multilingual
                    //if (isMultiLingual())
                    //{
                    //    if (this.CodesTable)
                    //    {
                    //        sSQLCount = "SELECT COUNT(*) ";
                    //        int iStart = p_sSQL.IndexOf("FROM", p_sSQL.IndexOf("UNION"));
                    //        iIndex = p_sSQL.IndexOf("ORDER BY");
                    //        if (iIndex == -1)
                    //            sSQLCount = sSQLCount + p_sSQL.Substring(iStart);
                    //        else
                    //            sSQLCount = sSQLCount + p_sSQL.Substring(iStart, (iIndex - iStart));
                    //        iRecordCount = iRecordCount + Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(sConnectionString, sSQLCount).ToString(), out bInSuccess);
                    //    }
                    //}
                    //Aman ML Change
                    //objReader = DbFactory.GetDbReader(sConnectionString, p_sSQL);
                    objReader = objConn.ExecuteReader(p_sSQL);
                    dt = new DataTable();
                    dt.Load(objReader);
                    iRecordCount = dt.Rows.Count;
                    
                    
                    if (!bIsSecurityDB)
                    {
                        objCache = new LocalCache(sConnectionString,m_iClientId);
                        //akaur9 11/03/2011 gets the user limit from the main DB not from the security DB so putiing the stmt the if block MITS 26518
                        objCache.GetSystemInfo("QCKLKPUP_USER_LIMIT", ref sRecordsPerPage);
                    }
                    
                    iRecordsPerPage = Conversion.CastToType<Int32>(sRecordsPerPage, out bInSuccess);
                    CodeType code = null;

                    //create xml for the search results
                    if (p_iPageNumber > 0)
                    {
                        if (iRecordCount > iRecordsPerPage)
                        { //MGaba2:MITS 18913:Deleting record was disabling links
                            iTotalNumberOfPages = (int)Math.Ceiling((double)iRecordCount / iRecordsPerPage);
                        }
                        else
                        {
                            iTotalNumberOfPages = 1;
                        }
                        iStartIndex = (p_iPageNumber - 1) * iRecordsPerPage;
                        iEndIndex = iStartIndex + (iRecordsPerPage - 1);
                        if (iEndIndex > (iRecordCount - 1))
                        {
                            if (p_iPageNumber != 1 && (iRecordCount == iStartIndex))
                            {
                                p_iPageNumber -= 1;
                                iStartIndex = (p_iPageNumber - 1) * iRecordsPerPage;
                                iEndIndex = iStartIndex + (iRecordsPerPage - 1);
                            }
                            else
                                iEndIndex = iRecordCount - 1;
                        }
                    }
                    else
                    {
                        iStartIndex = 0;
                        iEndIndex = iRecordCount - 1;
                    }

                    retCodes.PageCount = iTotalNumberOfPages.ToString();
                    retCodes.ThisPage = p_iPageNumber.ToString();
                    //create xml for the search results
                    //while (objReader.Read())
                    //{
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (iLoopCounter >= iStartIndex && iLoopCounter <= iEndIndex)
                        {
                            code = new CodeType();


                            iCodeID = Conversion.ConvertObjToInt(dr[0], m_iClientId);
                            code.Id = iCodeID;

                            //create innertext by combining all the columns
                            sText = string.Empty;
                            string sHoldValue = string.Empty;//Parijat : MITS 18834
                            string sReaderValue = string.Empty; //Prashant MITS 35072
                            //Prashant MITS 35072 starts- Commented 
                            //for (int i = 0; i < dt.Columns.Count; i++)
                            //{
                            foreach (DataColumn col in dt.Columns)
                            {
                                //Prashant MITS 35072 starts- Commented the endif block and replacing it with a switch 
                               // if (objReader.GetName(i).ToUpper().Equals("BIRTH_DATE"))
                               // {
                                   // sHoldValue = Conversion.ConvertObjToStr(objReader.GetValue(i));
                               //     sText = sText + " " + sHoldValue.Substring(5, 2) + "/" + sHoldValue.Substring(sHoldValue.Length - 2, 2) + "/" + sHoldValue.Substring(0, 4);
                               // }
                               // //Parijat : MITS 18834 -Start
                               // else if (objReader.GetName(i).ToUpper().Equals("ADDR1"))
                               // {
                               //     sHoldValue = Conversion.ConvertObjToStr(objReader.GetValue(i));
                               //     if (sHoldValue != string.Empty && sHoldValue != null)
                               //         sText = sText + ", <B>(Addr:) </B>" + sHoldValue + " ";
                               // }
                               // else if (objReader.GetName(i).ToUpper().Equals("CITY"))
                               // {
                               //     sHoldValue = Conversion.ConvertObjToStr(objReader.GetValue(i));
                               //     if (sHoldValue != string.Empty && sHoldValue != null)
                               //         sText = sText + ", <B>(City:) </B>" + sHoldValue + " ";
                               // }
                               // else if (objReader.GetName(i).ToUpper().Equals("EMAIL_ADDRESS"))
                               // {
                               //     sHoldValue = Conversion.ConvertObjToStr(objReader.GetValue(i));
                               //     if (sHoldValue != string.Empty && sHoldValue != null)
                               //         sText = sText + ", <B>(Email Add:) </B>" + sHoldValue + " ";
                               // }//Parijat : MITS 18834 -End
                               // else if (objReader.GetName(i).ToUpper().Equals("ABBREVIATION"))   //Milli : MITS 16660: On Tab out "ABBR NAME"  appers but on saving it would be "ABBR - NAME" so bringing consistency                             
                               // {

                               //     sText = sText + objReader.GetString("ABBREVIATION") + "-";//Milli : MITS 16660
                               // }
                               //     //RUPAL:start,policy system interface
                               // if (objReader.GetName(i).ToUpper().Equals("CVG_SEQUENCE_NO"))
                               // {
                               //     sHoldValue = Conversion.ConvertObjToStr(objReader.GetValue(i));
                               //     code.CovgSeqNum = sHoldValue;
                               // }//rupal:end
                               // //smishra54: MITS 33996
                               // if (objReader.GetName(i).ToUpper().Equals("TRANS_SEQ_NO"))
                               // {
                               //     sHoldValue = Conversion.ConvertObjToStr(objReader.GetValue(i));
                               //     code.TransSeqNum = sHoldValue;
                               // }
                               // //smishra54
                               //else
                               // {
                               //     sText = sText + Conversion.ConvertObjToStr(objReader.GetValue(i)) + " ";
                               // }
                                //sReaderValue = objReader.GetName(i).ToUpper().ToString();
                                if (col.Ordinal == 0) continue;
                                sReaderValue = col.ColumnName.ToUpper().ToString();
                                switch (sReaderValue)
                                {
                                    case "BIRTH_DATE":
                                        sHoldValue = Conversion.ConvertObjToStr(dr[sReaderValue]);
                                        sText = sText + " " + sHoldValue.Substring(5, 2) + "/" + sHoldValue.Substring(sHoldValue.Length - 2, 2) + "/" + sHoldValue.Substring(0, 4);
                                        break;
                                    case "ADDR1":
                                        sHoldValue = Conversion.ConvertObjToStr(dr[sReaderValue]);
                                        if (sHoldValue != string.Empty && sHoldValue != null)
                                            sText = sText + ", <B>(Addr:) </B>" + sHoldValue + " ";
                                        break;
                                    case "CITY":
                                        sHoldValue = Conversion.ConvertObjToStr(dr[sReaderValue]);
                                        if (sHoldValue != string.Empty && sHoldValue != null)
                                            sText = sText + ", <B>(City:) </B>" + sHoldValue + " ";
                                        break;
                                    case "EMAIL_ADDRESS":
                                        sHoldValue = Conversion.ConvertObjToStr(dr[sReaderValue]);
                                        if (sHoldValue != string.Empty && sHoldValue != null)
                                            sText = sText + ", <B>(Email Add:) </B>" + sHoldValue + " ";
                                        break;
                                    case "ABBREVIATION":
                                        sText = sText + Conversion.ConvertObjToStr(dr[sReaderValue]) + "-";
                                        break;
                                    case "CVG_SEQUENCE_NO":
                                        sHoldValue = Conversion.ConvertObjToStr(dr[sReaderValue]);
                                        code.CovgSeqNum = sHoldValue;
                                        break;
                                    case "TRANS_SEQ_NO":
                                        sHoldValue = Conversion.ConvertObjToStr(dr[sReaderValue]);
                                        code.TransSeqNum = sHoldValue;
                                        break;
                                    //Ankit Start : Worked on MITS - 34297
                                    case "COVERAGE_KEY":
                                        sHoldValue = Conversion.ConvertObjToStr(dr[sReaderValue]);
                                        code.CoverageKey = sHoldValue;
                                        break;
                                    //Ankit End
                                    //avipinsrivas Start : Worked for Jira-340
                                    case "ER_ROW_ID":
                                        if (dr[sReaderValue] != null)
                                            code.ErRowID = Conversion.CastToType<int>(dr[sReaderValue].ToString(), out bSuccess);
                                        break;
                                   
                                   default:
                                        sText = sText + Conversion.ConvertObjToStr(dr[sReaderValue]) + " ";
                                        break;
                                }
                                //Prashant MITS 35072 ends
                            }
                            code.Desc = sText;


                            // For the parent
                            if (!bIsSecurityDB)
                            {
                                code.parentText = objCache.GetShortCode(objCache.GetRelatedCodeId(iCodeID)) + " " + objCache.GetCodeDesc(objCache.GetRelatedCodeId(iCodeID));
                            }
                            retCodes.Codes.Add(code);
                        }
                        else
                        {
                            if (iLoopCounter > iEndIndex)
                            {
                                break;
                            }
                        }
                        iLoopCounter++;
                    }
                    //dt = null;
                }
                objSyssettings = new SysSettings(m_sConnectionString, m_iClientId);//RMA-6871
                retCodes.IsEntityRoleEnabled = objSyssettings.UseEntityRole;
            }
            catch (DataModelException p_objExp)
            {
                throw (p_objExp);
            }
            catch (RMAppException p_objExp)
            {
                throw (p_objExp);
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.QuickLookup.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Clear(m_iClientId);
                    objCache.Dispose();
                }
                objSyssettings = null;//RMA-6871
                if (dt != null)
                {
                    dt.Clear();
                    dt.Dispose();
                }
            }
            return retCodes;
        }

        /// Name		: GetCodesXML
        /// Author		: Nikhil Kumar Garg
        /// Date Created: 11-May-2005
        ///*************************************************************************
        /// Amendment History
        ///*************************************************************************
        /// Date Amended   *   Amendment   *								Author
        ///*************************************************************************
        /// <summary>
        /// Searches for the codes and returns the xml
        /// </summary>
        /// <param name="p_sSQL">SQL Search String</param>
        /// <param name="p_lRecordCount">Number of Records</param>
        /// <param name="p_lPageNumber">Current Page number</param>
        /// <param name="p_sTableName">Table Name for the codes</param>
        /// <param name="p_sTriggerDate">Date for searching</param>
        /// <param name="p_sFormName">Form name from which code window is called</param>
        /// <returns>XML Document containing the codes list</returns>
        private XmlDocument GetCodesXML(string p_sSQL, long p_lRecordCount, long p_lPageNumber,
                                        string p_sTableName, string p_sTriggerDate, string p_sFormName, string p_sSortColumn, string p_sSortOrder)//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
        {
            int iStart = 0;						//for starting record
            int iLength = 0;						//used for creating the sql string
            string sCountSQL = string.Empty;		//sql for getting the count of records
            DbConnection objConnection = null;	//connection object
            DbCommand objCommand = null;			//command object
            long lRecordCount = 0;				//Records count passed from the page
            long lPageCount = 0;					//total pages
            long lStartAt = 0;					//starting position for the record to return
            DbReader objReader = null;			//reader containing the codes
            long lCounter = 0;					//used for iterating in the data reader
            XmlDocument objCodesXml = null;		//document to send back 
            XmlElement objElement = null;			//used fotr creating the xml document
            LocalCache objCache = null;

            try
            {
                objCache = new LocalCache(m_sConnectionString,m_iClientId);

                objCodesXml = new XmlDocument();
                objElement = objCodesXml.CreateElement("codes");
                objCodesXml.AppendChild(objElement);
                if (p_sTableName.Equals("RM_SYS_USERS"))
                {
                    objCodesXml.DocumentElement.SetAttribute("displayname", "System Users");
                }
                else
                {
                    objCodesXml.DocumentElement.SetAttribute("displayname", objCache.GetUserTableName(objCache.GetTableId(p_sTableName)));
                }

                if (p_lRecordCount == 0)
                {
                    //if user hit next/previous/first/last link record count was passed in
                    iStart = p_sSQL.IndexOf("FROM");
                    iLength = p_sSQL.IndexOf("ORDER BY") - iStart;

                    //get count of records
                    if (iLength > 0)
                        sCountSQL = "SELECT COUNT(*) " + p_sSQL.Substring(iStart, iLength);
                    else
                        sCountSQL = "SELECT COUNT(*) " + p_sSQL.Substring(iStart);

                    if (m_bIsSecurityDatabase == true)
                        objConnection = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
                    else
                        objConnection = DbFactory.GetDbConnection(m_sConnectionString);
                    objConnection.Open();

                    objCommand = objConnection.CreateCommand();
                    objCommand.CommandText = sCountSQL;
                    lRecordCount = Conversion.ConvertObjToInt64(objCommand.ExecuteScalar(), m_iClientId);
                }
                if (lRecordCount == 0)
                    return objCodesXml;

                //get the codes
                if (m_bIsSecurityDatabase == true)
                    objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), p_sSQL);
                else
                    objReader = DbFactory.GetDbReader(m_sConnectionString, p_sSQL);

                lPageCount = lRecordCount / MAX_CODES_PER_PAGE;

                if ((lRecordCount % MAX_CODES_PER_PAGE) != 0)
                    lPageCount += 1;

                if (p_lPageNumber == 1)
                    lStartAt = 1;
                else
                    lStartAt = ((p_lPageNumber - 1) * MAX_CODES_PER_PAGE) + 1;

                //move to the current page
                lCounter = 1;

                if (lStartAt > 1)
                {
                    while (lCounter < lStartAt)
                    {
                        lCounter += 1;
                        objReader.Read();
                    }
                }

                lCounter = 0;

                //for reading values from the DataReader for the Codes
                string sCodeId = string.Empty;
                string sCodeDesc = string.Empty;
                string sFirstName = string.Empty;
                string sShortCode = string.Empty;
                string sEfDate = string.Empty;
                string sExDate = string.Empty;
                string sParentCode = string.Empty;

                switch (p_sTableName.ToLower())
                {
                    case "rm_sys_users":
                        while (objReader.Read())
                        {
                            //akaur9 09/14/2011 All codes must be displayed for Mobile Adjuster
                            if (!bIsMobileAdjuster && lCounter >= MAX_CODES_PER_PAGE)
                                break;

                            sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0));		//user id
                            sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(1));	//last name
                            sFirstName = Conversion.ConvertObjToStr(objReader.GetValue(2));
                            sShortCode = Conversion.ConvertObjToStr(objReader.GetValue(3));	//login name

                            if (!(sFirstName.Equals(string.Empty)))
                                sCodeDesc = sFirstName + " " + sCodeDesc;

                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');

                            objElement = objCodesXml.CreateElement("code");
                            objElement.InnerText = sCodeDesc;
                            objElement.SetAttribute("id", sCodeId);
                            objElement.SetAttribute("shortcode", sShortCode);
                            objCodesXml.DocumentElement.AppendChild(objElement);
                            lCounter += 1;
                        }
                        break;

                    case "user_groups":
                        while (objReader.Read())
                        {
                            //akaur9 09/14/2011 All codes must be displayed for Mobile Adjuster
                            if (!bIsMobileAdjuster && lCounter >= MAX_CODES_PER_PAGE)
                                break;

                            sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0));		//user id
                            sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(1));	//last name

                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');

                            objElement = objCodesXml.CreateElement("code");
                            objElement.InnerText = sCodeDesc;
                            objElement.SetAttribute("id", sCodeId);
                            objElement.SetAttribute("shortcode", sCodeDesc);
                            objCodesXml.DocumentElement.AppendChild(objElement);
                            lCounter += 1;
                        }
                        break;

                    case "cust_rate":
                        while (objReader.Read())
                        {
                            //akaur9 09/14/2011 All codes must be displayed for Mobile Adjuster
                            if (!bIsMobileAdjuster && lCounter >= MAX_CODES_PER_PAGE)
                                break;

                            sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            sShortCode = Conversion.ConvertObjToStr(objReader.GetValue(3));
                            sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(3));
                            sEfDate = Conversion.ConvertObjToStr(objReader.GetValue(1));
                            sExDate = Conversion.ConvertObjToStr(objReader.GetValue(2));

                            sCodeDesc = sCodeDesc.Replace((char)34, ' ') + " - Effective Date " + Conversion.GetDBDateFormat(sEfDate, "short date") + " Expiration Date " + Conversion.GetDBDateFormat(sExDate, "short date");

                            objElement = objCodesXml.CreateElement("code");
                            objElement.InnerText = sCodeDesc;
                            objElement.SetAttribute("id", sCodeId);
                            objElement.SetAttribute("shortcode", sShortCode);
                            objCodesXml.DocumentElement.AppendChild(objElement);
                            lCounter += 1;
                        }
                        break;
                    case "entity": //25/12/2007 Abhishek, MITS 11097 - START
                        while (objReader.Read())
                        {
                            //akaur9 09/14/2011 All codes must be displayed for Mobile Adjuster
                            if (!bIsMobileAdjuster && lCounter >= MAX_CODES_PER_PAGE)
                                break;
                             
                            sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(1));
                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');

                            objElement = objCodesXml.CreateElement("code");
                            objElement.InnerText = sCodeDesc;
                            objElement.SetAttribute("id", sCodeId);
                            objCodesXml.DocumentElement.AppendChild(objElement);
                            lCounter += 1;
                        }
                        break; //25/12/2007 Abhishek, MITS 11097 - End
                    case "states":
                        while (objReader.Read())
                        {
                            //akaur9 09/14/2011 All codes must be displayed for Mobile Adjuster
                            if (!bIsMobileAdjuster && lCounter >= MAX_CODES_PER_PAGE)
                                break;

                            sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            sShortCode = Conversion.ConvertObjToStr(objReader.GetValue(1));
                            sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(2));

                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');

                            objElement = objCodesXml.CreateElement("code");
                            objElement.InnerText = sCodeDesc;
                            objElement.SetAttribute("id", sCodeId);
                            objElement.SetAttribute("shortcode", sShortCode);
                            objCodesXml.DocumentElement.AppendChild(objElement);
                            lCounter += 1;
                        }
                        break;
                    //Start:added by Nitin Goel,08/21/2013, mits 30910
                    case "coverage_group":
                        while (objReader.Read())
                        {
                            //akaur9 09/14/2011 All codes must be displayed for Mobile Adjuster
                            if (!bIsMobileAdjuster && lCounter >= MAX_CODES_PER_PAGE)
                                break;

                            sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            sShortCode = Conversion.ConvertObjToStr(objReader.GetValue(1));
                            sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(2));

                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');

                            objElement = objCodesXml.CreateElement("code");
                            objElement.InnerText = sCodeDesc;
                            objElement.SetAttribute("id", sCodeId);
                            objElement.SetAttribute("shortcode", sShortCode);
                            objCodesXml.DocumentElement.AppendChild(objElement);
                            lCounter += 1;
                        }
                        break;
                    //end:added by Nitin Goel,08/21/2013, mits 30910

                    default:
                        while (objReader.Read())
                        {
                            //akaur9 09/14/2011 All codes must be displayed for Mobile Adjuster
                            if (!bIsMobileAdjuster && lCounter >= MAX_CODES_PER_PAGE)
                                break;

                            sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            sShortCode = Conversion.ConvertObjToStr(objReader.GetValue(1));
                            sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(2));
                            sParentCode = Conversion.ConvertObjToStr(objReader.GetValue(3));

                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');
                            sParentCode = sParentCode.Replace((char)34, ' ');

                            objElement = objCodesXml.CreateElement("code");
                            objElement.InnerText = sCodeDesc;
                            objElement.SetAttribute("id", sCodeId);
                            objElement.SetAttribute("shortcode", sShortCode);
                            objElement.SetAttribute("parentcode", sParentCode);
                            objElement.SetAttribute("parentdesc", objCache.GetCodeDesc(objCache.GetRelatedCodeId(Conversion.ConvertStrToInteger(sCodeId))));

                            objCodesXml.DocumentElement.AppendChild(objElement);
                            lCounter += 1;
                        }
                        break;
                }

                objCodesXml.DocumentElement.SetAttribute("recordcount", lRecordCount.ToString());
                objCodesXml.DocumentElement.SetAttribute("pagecount", lPageCount.ToString());
                objCodesXml.DocumentElement.SetAttribute("tablename", p_sTableName);
                objCodesXml.DocumentElement.SetAttribute("triggerdate", p_sTriggerDate);
                objCodesXml.DocumentElement.SetAttribute("FormName", p_sFormName);

                //note:  getcode.xsl will add pagination links if m_lPageCount != 1

                if (lPageCount > 1)
                {
                    if (p_lPageNumber == 1)
                        objCodesXml.DocumentElement.RemoveAttribute("previouspage");
                    else
                        objCodesXml.DocumentElement.SetAttribute("previouspage", Convert.ToString(p_lPageNumber - 1));

                    if (p_lPageNumber > 1)
                        objCodesXml.DocumentElement.SetAttribute("firstpage", "1");
                    else
                        objCodesXml.DocumentElement.RemoveAttribute("firstpage");

                    if (p_lPageNumber == lPageCount)
                        objCodesXml.DocumentElement.RemoveAttribute("nextpage");
                    else
                        objCodesXml.DocumentElement.SetAttribute("nextpage", Convert.ToString(p_lPageNumber + 1));

                    objCodesXml.DocumentElement.SetAttribute("thispage", p_lPageNumber.ToString());
                    objCodesXml.DocumentElement.SetAttribute("pagecount", lPageCount.ToString());

                    if (p_lPageNumber < lPageCount)
                        objCodesXml.DocumentElement.SetAttribute("lastpage", lPageCount.ToString());
                    else
                        objCodesXml.DocumentElement.RemoveAttribute("lastpage");
                }
            }
            catch (DataModelException p_objExp)
            {
                throw (p_objExp);
            }
            catch (RMAppException p_objExp)
            {
                throw (p_objExp);
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.GetCodesXML.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objConnection != null)
                {
                    objConnection.Close();
                    objConnection.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Clear(m_iClientId);
                    objCache.Dispose();
                }
            }
            return objCodesXml;
        }

        /// Name		: GetCodesList
        /// Author		: Parag Sarin
        /// Date Created: 10-April-2008
        ///*************************************************************************
        /// Amendment History
        ///*************************************************************************
        /// Date Amended   *   Amendment   *								Author
        ///*************************************************************************
        /// <summary>
        /// Searches for the codes and returns the xml
        /// </summary>
        /// <param name="p_sSQL">SQL Search String</param>
        /// <param name="p_lRecordCount">Number of Records</param>
        /// <param name="p_lPageNumber">Current Page number</param>
        /// <param name="p_sTableName">Table Name for the codes</param>
        /// <param name="p_sTriggerDate">Date for searching</param>
        /// <param name="p_sFormName">Form name from which code window is called</param>
        /// <param name="p_sSortColumn">Column name on which the result will be sorted.</param>
        /// <param name="p_sSortOrder">Order in which result will be sorted</param>
        /// <returns>XML Document containing the codes list</returns>
        private CodeListType GetCodesList(int iClaimid, string p_sSQL, long p_lRecordCount, long p_lPageNumber,
                                        string p_sTableName, string p_sTriggerDate, string p_sFormName, string p_sShowCheckBox, string p_sSortColumn, string p_sSortOrder,DbConnection p_ObjConn,LocalCache p_ObjCache)//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17//rsushilaggar Handled  paging for checkbox functionality  MITS 21600
        {
            int iStart = 0;						//for starting record
            int iLength = 0;						//used for creating the sql string
            string sCountSQL = string.Empty;		//sql for getting the count of records
            //DbConnection objConnection = null;	//connection object
            DbCommand objCommand = null;			//command object
            long lRecordCount = 0;				//Records count passed from the page
            long lPageCount = 0;					//total pages
            long lStartAt = 0;					//starting position for the record to return
            DbReader objReader = null;			//reader containing the codes
            long lCounter = 0;					//used for iterating in the data reader
            XmlDocument objCodesXml = null;		//document to send back 
            XmlElement objElement = null;			//used fotr creating the xml document
            //LocalCache objCache = null;
            CodeListType retCodes = null;
            //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
            System.Data.DataTable dtCodes = null;
            System.Data.DataColumn column = null;
            System.Data.DataColumn columnDesc = null;
            //End changes MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
            //tanwar2 - Policy Download from staging - start
            SysSettings oSettings = null;
            //tanwar2 - Policy Download from staging - end
            try
            {
                //tanwar2 - Policy Download from staging - start
                oSettings = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud
                //tanwar2 - Policy Download from staging - end
                //objCache =  new LocalCache(m_sConnectionString);
                if (p_ObjCache == null)
                {
                    p_ObjCache = new LocalCache(m_sConnectionString,m_iClientId);
                }
                retCodes = new CodeListType();
                retCodes.Codes = new System.Collections.Generic.List<CodeType>();
                //objCodesXml = new XmlDocument();
                //objElement = objCodesXml.CreateElement("codes");
                //objCodesXml.AppendChild(objElement);
                if (p_sTableName.Equals("RM_SYS_USERS"))
                {
                    //objCodesXml.DocumentElement.SetAttribute("displayname", "System Users");
                    retCodes.Displayname = "System Users";
                }
                //avipinsrivas start : Worked for Jira-340
                else if (p_sTableName.ToLower().Equals(Globalization.BUSINESS_ENTITY_ROLE.ToLower()))
                    retCodes.Displayname = Globalization.DN_BUSINESS_ENTITY_ROLE;
                else if (p_sTableName.ToLower().Equals(Globalization.PEOPLE_ENTITY_ROLE.ToLower()))
                    retCodes.Displayname = Globalization.DN_PEOPLE_ENTITY_ROLE;
                //avipinsrivas End
                else if (p_sTableName.Equals("USER_GROUPS"))//psharma206 jira 6031
                {
                    retCodes.Displayname = "User Groups";
                }

                else
                {
                    //objCodesXml.DocumentElement.SetAttribute("displayname", objCache.GetUserTableName(objCache.GetTableId(p_sTableName)));
                    retCodes.Displayname = p_ObjCache.GetUserTableName(p_ObjCache.GetTableId(p_sTableName));
                }

                if (p_lRecordCount == 0)
                {
                    //if user hit next/previous/first/last link record count was passed in
                    iStart = p_sSQL.IndexOf("FROM");
                    //Aman ML Change
                    //if (p_sTableName.ToLower().Equals("event_type") || p_sTableName.ToLower().Equals("claim_type") || p_sTableName.ToLower().Equals("trans_types") || p_sTableName.ToLower().Equals("reserve_type") || this.CodesTable)
                    //Ashish Ahuja : Mits 32888 Temporary Condition Check of Multilingual
                    if(isMultiLingual())
                    {
                    if(this.CodesTable)
                    {
                        iLength = p_sSQL.IndexOf("UNION") - iStart;
                    }
                    else
                    {
                        iLength = p_sSQL.IndexOf("ORDER BY") - iStart;
                    }
                    }
                    else
                    {
                        iLength = p_sSQL.IndexOf("ORDER BY") - iStart;
                    }
                    //iLength = p_sSQL.IndexOf("ORDER BY") - iStart;

                    //get count of records
                    if (iLength > 0)
                        sCountSQL = "SELECT COUNT(*) " + p_sSQL.Substring(iStart, iLength);
                    else
                        sCountSQL = "SELECT COUNT(*) " + p_sSQL.Substring(iStart);

                    if (m_bIsSecurityDatabase == true)
                    {
                        if (p_ObjConn != null)
                        {
                            p_ObjConn.Close();
                            p_ObjConn = null;
                        }
                        p_ObjConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
                        p_ObjConn.Open();
                    }
                    //else
                    //    objConnection = DbFactory.GetDbConnection(m_sConnectionString);
                    //objConnection.Open();

                    objCommand = p_ObjConn.CreateCommand();
                    objCommand.CommandText = sCountSQL;
                    lRecordCount = Conversion.ConvertObjToInt64(objCommand.ExecuteScalar(), m_iClientId);

                    //Aman ML Change
                    //if (p_sTableName.ToLower().Equals("event_type") || p_sTableName.ToLower().Equals("claim_type") || p_sTableName.ToLower().Equals("trans_types") || p_sTableName.ToLower().Equals("reserve_type") || this.CodesTable)
                    //Ashish Ahuja : Mits 32888 Temporary Condition Check of Multilingual
                    if (isMultiLingual())
                    {
                        if (this.CodesTable)
                        {
                            iStart = p_sSQL.IndexOf("FROM", p_sSQL.IndexOf("UNION"));
                            iLength = p_sSQL.IndexOf("ORDER BY") - iStart;
                            if (iLength > 0)
                                sCountSQL = "SELECT COUNT(*) " + p_sSQL.Substring(iStart, iLength);
                            else
                                sCountSQL = "SELECT COUNT(*) " + p_sSQL.Substring(iStart);
                            objCommand = p_ObjConn.CreateCommand();
                            objCommand.CommandText = sCountSQL;
                            lRecordCount = lRecordCount + Conversion.ConvertObjToInt64(objCommand.ExecuteScalar(), m_iClientId);
                        }
                    }
                    //Aman ML Change
                }
                if (lRecordCount == 0)
                    return retCodes;
                //rsushilaggar MITS 21600 Date 09/28/2010
                // paging would not work with the check box functionality enabled
                if (p_sShowCheckBox.ToLower().Equals("true"))
                {
                    MAX_CODES_PER_PAGE = Convert.ToInt32( lRecordCount);
                }
                //get the codes
                //if (m_bIsSecurityDatabase == true)
                   // objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(0), p_sSQL);
                    objReader = p_ObjConn.ExecuteReader(p_sSQL);
                //else
                //    objReader = DbFactory.GetDbReader(m_sConnectionString, p_sSQL);
                //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                dtCodes = new System.Data.DataTable();
                column = new System.Data.DataColumn();
                column.DataType = System.Type.GetType("System.Int32");
                column.ColumnName = "AutoID";
                dtCodes.Columns.Add(column);

                columnDesc = new System.Data.DataColumn();
                columnDesc.DataType = System.Type.GetType("System.String");
                columnDesc.ColumnName = "Description";
                dtCodes.Columns.Add(columnDesc);
                dtCodes.Load(objReader);

                switch (p_sSortColumn)
                {
                    case "1":
                        //avipinsrivas start : Worked for Jira-340
                        if (p_sTableName.ToLower().Equals(Globalization.BUSINESS_ENTITY_ROLE.ToLower()) || p_sTableName.ToLower().Equals(Globalization.PEOPLE_ENTITY_ROLE.ToLower()))
                            p_sSortColumn = string.Empty;
                        //avipinsrivas End
                        else if (p_sSQL.Contains("STATE_NAME"))
                        {
                            p_sSortColumn = "STATE_NAME";
                        }
                        else if (p_sSQL.Contains("USER_GROUPS"))  //psharma206 jira 6031
                        {
                            p_sSortColumn = "GROUP_NAME";
                        
                        }
                        //JIRA RMA-4690: neha goel:05202014 added if else so that PI_SUPP works both for Entity type table and Code tables MITS # 35595
                        else if (p_sTableName.ToLower().Equals("entity"))
                        {
                            p_sSortColumn = "Description";
                        }
                        //JIRA RMA-4690: neha goel:05202014 added if else so that PI_SUPP works both for Entity type table and Code tables MITS # 35595
                        else
                        {
                            //MITS 29790 - kkaur8 -03142013 - Start
                            //p_sSortColumn = "Short_Code";
                            //rsharma220 MITS 29790
                            p_sSortColumn = "SHORT_CODE";
                            //MITS 29790 - End
                        }
                        break;
                    case "2": p_sSortColumn = "Description";
                        break;
                    case "3":
                        //MITS 29790 - kkaur8 -03142013 - Start
                        //p_sSortColumn = "Short_Code1";
						//rsharma220 MITS 29790
                        p_sSortColumn = "SHORT_CODE1";
                        //MITS 29790 - End
                        break;
                        //mbahl3  Mits 34651 for mobile adjuster  
                    case "4":
                        if (bIsMobileAdjuster)
                        {
                            p_sSortColumn = "SYSTEM_TABLE_NAME";
                        }
                        break;               
                }

                lPageCount = lRecordCount / MAX_CODES_PER_PAGE;

                if ((lRecordCount % MAX_CODES_PER_PAGE) != 0)
                    lPageCount += 1;

                if (p_lPageNumber == 1)
                    lStartAt = 1;
                else
                    lStartAt = ((p_lPageNumber - 1) * MAX_CODES_PER_PAGE) + 1;

                //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                //move to the current page
                //lCounter = 1;

                //if (lStartAt > 1)
                //{
                //    while (lCounter < lStartAt)
                //    {
                //        lCounter += 1;
                //        objReader.Read();
                //    }
                //}

                lCounter = 0;

                //for reading values from the DataReader for the Codes
                string sCodeId = string.Empty;
                string sCodeDesc = string.Empty;
                string sFirstName = string.Empty;
                string sShortCode = string.Empty;
                string sEfDate = string.Empty;
                string sExDate = string.Empty;
                string sParentCode = string.Empty;
                string sSql = string.Empty;//skhare7 27920
                object oReserveStatus = null;
                string sShortCodeStatus = string.Empty;
                string sCovgSeqNum=string.Empty;//rupal:policy system interface
                string sTransSeqNum = string.Empty;//smishra54: MITS 33996
                string sCoverageKey = string.Empty;     //Ankit Start : Worked on MITS - 34297
                switch (p_sTableName.ToLower())
                {
                    //avipinsrivas start : Worked for Jira-340
                    case Globalization.BUSINESS_ENTITY_ROLE: case Globalization.PEOPLE_ENTITY_ROLE:
                        bool bSucess;
                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            sCodeDesc = dtCodes.Rows[i]["TABLE_NAME"].ToString();
                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');
                            dtCodes.Rows[i][1] = sCodeDesc;
                        }
                        SortingAndPaging(ref dtCodes, lStartAt, p_sSortColumn, p_sSortOrder);

                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            CodeType tmpcode = new CodeType();
                            sCodeId = dtCodes.Rows[i]["TABLE_ID"].ToString();
                            sCodeDesc = dtCodes.Rows[i]["TABLE_NAME"].ToString();
                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');

                            tmpcode.Id = Conversion.CastToType<int>(sCodeId, out bSucess);
                            tmpcode.Desc = sCodeDesc;
                            tmpcode.ShortCode = string.Empty;
                            retCodes.Codes.Add(tmpcode);
                        }
                        break;
                    //avipinsrivas End
                    case "rm_sys_users":
                        //while (objReader.Read()) //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17


                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {

                            sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][3]);	//last name
                            sFirstName = Conversion.ConvertObjToStr(dtCodes.Rows[i][4]);

                            if (!(sFirstName.Equals(string.Empty)))
                                sCodeDesc = sFirstName + " " + sCodeDesc;

                            dtCodes.Rows[i][1] = sCodeDesc;
                        }
                        SortingAndPaging(ref dtCodes, lStartAt, p_sSortColumn, p_sSortOrder);

                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            CodeType tmpcode = new CodeType();
                            //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                            //if (!bIsMobileAdjuster && lCounter >= MAX_CODES_PER_PAGE)
                            //    break;

                            sCodeId = Conversion.ConvertObjToStr(dtCodes.Rows[i][2]);		//user id
                            sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][3]);	//last name
                            sFirstName = Conversion.ConvertObjToStr(dtCodes.Rows[i][4]);
                            sShortCode = Conversion.ConvertObjToStr(dtCodes.Rows[i][5]);	//login name

                            if (!(sFirstName.Equals(string.Empty)))
                                sCodeDesc = sFirstName + " " + sCodeDesc;

                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');

                            tmpcode.Id = Conversion.ConvertStrToInteger(sCodeId);
                            tmpcode.Desc = sCodeDesc;
                            tmpcode.ShortCode = sShortCode;
                            retCodes.Codes.Add(tmpcode);
                            //lCounter += 1;   //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                        }
                        break;

                    case "user_groups":
                        //while (objReader.Read()) //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][3]);	//last name
                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');
                            dtCodes.Rows[i][1] = sCodeDesc;
                        }
                        SortingAndPaging(ref dtCodes, lStartAt, p_sSortColumn, p_sSortOrder);

                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            CodeType tmpcode = new CodeType();
                            //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                            //if (lCounter >= MAX_CODES_PER_PAGE)
                            // break;
                            //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                            //sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0));		//user id
                            //sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(1));	//last name

                            sCodeId = Conversion.ConvertObjToStr(dtCodes.Rows[i][2]);		//user id
                            sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][3]);	//last name

                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');

                            tmpcode.Id = Conversion.ConvertStrToInteger(sCodeId);
                            tmpcode.Desc = sCodeDesc;
                            tmpcode.ShortCode = sShortCode;
                            retCodes.Codes.Add(tmpcode);
                            //lCounter += 1; //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                        }
                        break;

                    case "cust_rate":
                        //while (objReader.Read()) //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][5]);
                            sEfDate = Conversion.ConvertObjToStr(dtCodes.Rows[i][3]);
                            sExDate = Conversion.ConvertObjToStr(dtCodes.Rows[i][4]);

                            sCodeDesc = sCodeDesc.Replace((char)34, ' ') + " - Effective Date " + Conversion.GetDBDateFormat(sEfDate, "short date") + " Expiration Date " + Conversion.GetDBDateFormat(sExDate, "short date");
                            dtCodes.Rows[i][1] = sCodeDesc;

                        }
                        SortingAndPaging(ref dtCodes, lStartAt, p_sSortColumn, p_sSortOrder);

                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            CodeType tmpcode = new CodeType();
                            //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                            //if (lCounter >= MAX_CODES_PER_PAGE)
                            //  break;
                            //commented by bsharma33 MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                            //sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            //sShortCode = Conversion.ConvertObjToStr(objReader.GetValue(3));
                            //sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(3));
                            //sEfDate = Conversion.ConvertObjToStr(objReader.GetValue(1));
                            //sExDate = Conversion.ConvertObjToStr(objReader.GetValue(2));


                            sCodeId = Conversion.ConvertObjToStr(dtCodes.Rows[i][2]);
                            sShortCode = Conversion.ConvertObjToStr(dtCodes.Rows[i][5]);
                            sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][5]);
                            sEfDate = Conversion.ConvertObjToStr(dtCodes.Rows[i][3]);
                            sExDate = Conversion.ConvertObjToStr(dtCodes.Rows[i][4]);

                            sCodeDesc = sCodeDesc.Replace((char)34, ' ') + " - Effective Date " + Conversion.GetDBDateFormat(sEfDate, "short date") + " Expiration Date " + Conversion.GetDBDateFormat(sExDate, "short date");

                            tmpcode.Id = Conversion.ConvertStrToInteger(sCodeId);
                            tmpcode.Desc = sCodeDesc;
                            tmpcode.ShortCode = sShortCode;
                            retCodes.Codes.Add(tmpcode);
                            //lCounter += 1; //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                        }
                        break;
                    case "entity": //25/12/2007 Abhishek, MITS 11097 - START
                        //while (objReader.Read()) //commented by bsharma33 MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][3]);
                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');
                            dtCodes.Rows[i][1] = sCodeDesc;
                        }
                        SortingAndPaging(ref dtCodes, lStartAt, p_sSortColumn, p_sSortOrder);

                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            CodeType tmpcode = new CodeType();
                            //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                            //if (lCounter >= MAX_CODES_PER_PAGE)
                            //  break;
                            //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                            //sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            //sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(1));

                            sCodeId = Conversion.ConvertObjToStr(dtCodes.Rows[i][2]);
                            sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][3]);
                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');

                            tmpcode.Id = Conversion.ConvertStrToInteger(sCodeId);
                            tmpcode.Desc = sCodeDesc;
                            tmpcode.ShortCode = sShortCode;
                            retCodes.Codes.Add(tmpcode);
                            //lCounter += 1; //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                        }
                        break; //25/12/2007 Abhishek, MITS 11097 - End
                    case "states":
                        // while (objReader.Read()) //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][4]);
                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');
                            dtCodes.Rows[i][1] = sCodeDesc;
                        }
                        SortingAndPaging(ref dtCodes, lStartAt, p_sSortColumn, p_sSortOrder);
                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            CodeType tmpcode = new CodeType();
                            //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                            // if (lCounter >= MAX_CODES_PER_PAGE)
                            //   break;
                            //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                            //sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            //sShortCode = Conversion.ConvertObjToStr(objReader.GetValue(1));
                            //sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(2));

                            sCodeId = Conversion.ConvertObjToStr(dtCodes.Rows[i][2]);
                            sShortCode = Conversion.ConvertObjToStr(dtCodes.Rows[i][3]);
                            sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][4]);
                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');
                            sParentCode = sParentCode.Replace((char)34, ' ');

                            tmpcode.Id = Conversion.ConvertStrToInteger(sCodeId);
                            tmpcode.Desc = sCodeDesc;
                            tmpcode.ParentCode = sParentCode;
                            tmpcode.parentDesc = p_ObjCache.GetCodeDesc(p_ObjCache.GetRelatedCodeId(Conversion.ConvertStrToInteger(sCodeId)));

                            oReserveStatus = DbFactory.ExecuteScalar(this.m_sConnectionString, "SELECT RES_STATUS_CODE FROM RESERVE_CURRENT WHERE CLAIM_ID=" + iClaimid + " AND RESERVE_TYPE_CODE=" + p_ObjCache.GetRelatedCodeId(Conversion.ConvertStrToInteger(sCodeId)));
                            if (oReserveStatus != null && oReserveStatus != string.Empty)
                            {
                                oReserveStatus = Conversion.ConvertObjToStr(oReserveStatus);
                                sShortCodeStatus = p_ObjCache.GetShortCode(Conversion.ConvertObjToInt(oReserveStatus, m_iClientId));
                                //sReserveType = sReserveType + sCodeDesc + "_" + sShortCodeStatus + ",";
                            }
                            tmpcode.ShortCode = sShortCode;
                            tmpcode.resstatus = sShortCodeStatus;
                            retCodes.Codes.Add(tmpcode);
                            //lCounter += 1; //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                        }
                        break;
//Start:added by Nitin Goel,08/21/2013, mits 30910
                    case "coverage_group":
                       
                        if (!bIsMobileAdjuster && lCounter >= MAX_CODES_PER_PAGE)
                           break;
                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            CodeType tmpcode = new CodeType();

                            sCodeId = Conversion.ConvertObjToStr(dtCodes.Rows[i][2]);
                            sShortCode = Conversion.ConvertObjToStr(dtCodes.Rows[i][3]);
                            sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][4]);

                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');

                            tmpcode.Id = Conversion.ConvertStrToInteger(sCodeId);
                            tmpcode.Desc = sCodeDesc;
                            tmpcode.ShortCode = sShortCode;
                            retCodes.Codes.Add(tmpcode);
                        //    lCounter += 1;
                        }
                        break;
                    //end:added by Nitin Goel,08/21/2013, mits 30910

                    default:
                        //sgoel6 Medicare 05/13/2009
                        if (p_sFormName == "LineOfBusCodeSelfInsured" && lCounter == 0)
                        {
                            CodeType tmpcode = new CodeType();
                            sCodeId = "-3";
                            sShortCode = "All";
                            sCodeDesc = "All";
                            sParentCode = "\" \"";

                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');
                            sParentCode = sParentCode.Replace((char)34, ' ');

                            tmpcode.Id = Conversion.ConvertStrToInteger(sCodeId);
                            tmpcode.Desc = sCodeDesc;
                            tmpcode.ParentCode = sParentCode;
                            tmpcode.parentDesc = p_ObjCache.GetCodeDesc(p_ObjCache.GetRelatedCodeId(Conversion.ConvertStrToInteger(sCodeId)));
                            tmpcode.ShortCode = sShortCode;
                            retCodes.Codes.Add(tmpcode);
                               //lCounter += 1; //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                        }
                        //while (objReader.Read()) //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                       for (int i = 0; i < dtCodes.Rows.Count; i++)
                       {
                           sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][4]);
                           sCodeDesc = sCodeDesc.Replace((char)34, ' ');
                           dtCodes.Rows[i][1] = sCodeDesc;
                       }
                       SortingAndPaging( ref dtCodes, lStartAt, p_sSortColumn, p_sSortOrder);
                        for (int i = 0; i < dtCodes.Rows.Count; i++)
                        {
                            CodeType tmpcode = new CodeType();
                            //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                            //if (lCounter >= MAX_CODES_PER_PAGE)
                            //  break;

                            //sCodeId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            //sShortCode = Conversion.ConvertObjToStr(objReader.GetValue(1));
                            //sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue(2));
                            //sParentCode = Conversion.ConvertObjToStr(objReader.GetValue(3));

                            sCodeId = Conversion.ConvertObjToStr(dtCodes.Rows[i][2]);
                            sShortCode = Conversion.ConvertObjToStr(dtCodes.Rows[i][3]);
                            sCodeDesc = Conversion.ConvertObjToStr(dtCodes.Rows[i][4]);
                            sParentCode = Conversion.ConvertObjToStr(dtCodes.Rows[i][5]);
                            sCodeDesc = sCodeDesc.Replace((char)34, ' ');
                            sParentCode = sParentCode.Replace((char)34, ' ');

                            tmpcode.Id = Conversion.ConvertStrToInteger(sCodeId);
                            tmpcode.Desc = sCodeDesc;
                            tmpcode.ParentCode = sParentCode;
                            tmpcode.parentDesc = p_ObjCache.GetCodeDesc(p_ObjCache.GetRelatedCodeId(Conversion.ConvertStrToInteger(sCodeId)));
                            tmpcode.ShortCode = sShortCode;
                            //mbahl3 Mits 34733
						    if (dtCodes.Columns.Count > 6)
                            {
								//tanwar2 - Policy Download from staging - start
                            	//if (p_sTableName.ToLower() == "coverage_type" && iPolSystemId > 0)
                                if (p_sTableName.ToLower() == "coverage_type" && (iPolSystemId > 0))
                            	//tanwar2 - Policy Download from staging - end
                            	{
                                    tmpcode.CovgSeqNum = Conversion.ConvertObjToStr(dtCodes.Rows[i][6]);
									
								}
                            }
                            if (dtCodes.Columns.Count > 7)
                            {
                                tmpcode.TransSeqNum = Conversion.ConvertObjToStr(dtCodes.Rows[i][7]);
                            }
                            //mbahl3 Mits 34733
                            //Ankit Start : Worked on MITS - 34297
                            if (dtCodes.Columns.Count > 8)
                            {
                                tmpcode.CoverageKey = Conversion.ConvertObjToStr(dtCodes.Rows[i][8]);
                            }
                            //Ankit End
                            retCodes.Codes.Add(tmpcode);
                            //lCounter += 1; //commented by bsharma33 MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                        }
                        break;
                }
                retCodes.RecordCount = lRecordCount.ToString();
                retCodes.PageCount = lPageCount.ToString();
                retCodes.TableName = p_sTableName;
                retCodes.TriggerDate = p_sTriggerDate;
                retCodes.FormName = p_sFormName;
                //note:  getcode.xsl will add pagination links if m_lPageCount != 1

                if (lPageCount > 1)
                {
                    if (p_lPageNumber == 1)
                        retCodes.PreviousPage = "-1";
                    else
                        retCodes.PreviousPage = Convert.ToString(p_lPageNumber - 1);
                    if (p_lPageNumber > 1)
                        retCodes.FirstPage = "1";
                    else
                        retCodes.FirstPage = "-1";

                    if (p_lPageNumber == lPageCount)
                        retCodes.NextPage = "-1";
                    else
                        retCodes.NextPage = Convert.ToString(p_lPageNumber + 1);

                    retCodes.ThisPage = p_lPageNumber.ToString();
                    retCodes.PageCount = lPageCount.ToString();

                    if (p_lPageNumber < lPageCount)
                        retCodes.LastPage = lPageCount.ToString();
                    else
                        retCodes.LastPage = "-1";
                }
            }
            catch (DataModelException p_objExp)
            {
                throw (p_objExp);
            }
            catch (RMAppException p_objExp)
            {
                throw (p_objExp);
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.GetCodesXML.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (p_ObjConn != null)
                {
                    p_ObjConn.Close();
                    p_ObjConn.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (p_ObjCache != null)
                {
                    p_ObjCache.Clear(m_iClientId);
                    p_ObjCache.Dispose();
                }
            }
            return retCodes;
        }
        //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
        private void SortingAndPaging(ref System.Data.DataTable dtCodes, long lStartAt, string p_sSortColumn, string p_sSortOrder)
        {
            if (string.IsNullOrEmpty(p_sSortColumn) == false)
            {
                dtCodes.DefaultView.Sort = p_sSortColumn + " " + p_sSortOrder;
                dtCodes = dtCodes.DefaultView.ToTable();
            }
            for (int i = 0; i < dtCodes.Rows.Count; i++)
            {
                dtCodes.Rows[i][0] = i + 1;
            }

            dtCodes.DefaultView.RowFilter = "AutoID >=" + Convert.ToString(lStartAt) + " and AutoID <=" + Convert.ToString(lStartAt + MAX_CODES_PER_PAGE - 1);
            dtCodes = dtCodes.DefaultView.ToTable();
        }
        /// Name		: GetOrgEid
        /// Author		: Nikhil Kumar Garg
        /// Date Created: 12-May-2005
        ///*************************************************************************
        /// Amendment History
        ///*************************************************************************
        /// Date Amended   *   Amendment   *								Author
        ///*************************************************************************
        /// <summary>
        /// Gets the Lcoation,Division... etc. for a Department
        /// </summary>
        /// <param name="p_lDeptEId">Department Id</param>
        /// <param name="p_lFacEId">Facility Id</param>
        /// <param name="p_lLocEId">Location Id</param>
        /// <param name="p_lDivEId">Division Id</param>
        /// <param name="p_lRegEId">Region Id</param>
        /// <param name="p_lOprEId">Operation Id</param>
        /// <param name="p_lComEId">Company Id</param>
        /// <param name="p_lCliEId">Client Id</param>
        /// <returns>Null. All values are returned in ref variables</returns>
        private void GetOrgEid(long p_lDeptEId, ref long p_lFacEId, ref long p_lLocEId,
                               ref long p_lDivEId, ref long p_lRegEId, ref long p_lOprEId,
                               ref long p_lComEId, ref long p_lCliEId,DbConnection objConn)
        {
            string sSQL = string.Empty;	//sql string
            DbReader objReader = null;	//Data reader object

            sSQL = "SELECT FACILITY_EID, LOCATION_EID, DIVISION_EID, REGION_EID, OPERATION_EID, COMPANY_EID, CLIENT_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=" + p_lDeptEId;

            try
            {
                //Fetch the Details for this Department
                objReader = objConn.ExecuteReader(sSQL); //DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                {
                    p_lFacEId = Conversion.ConvertObjToInt64(objReader.GetValue("FACILITY_EID"), m_iClientId);
                    p_lLocEId = Conversion.ConvertObjToInt64(objReader.GetValue("LOCATION_EID"), m_iClientId);
                    p_lDivEId = Conversion.ConvertObjToInt64(objReader.GetValue("DIVISION_EID"), m_iClientId);
                    p_lRegEId = Conversion.ConvertObjToInt64(objReader.GetValue("REGION_EID"), m_iClientId);
                    p_lOprEId = Conversion.ConvertObjToInt64(objReader.GetValue("OPERATION_EID"), m_iClientId);
                    p_lComEId = Conversion.ConvertObjToInt64(objReader.GetValue("COMPANY_EID"), m_iClientId);
                    p_lCliEId = Conversion.ConvertObjToInt64(objReader.GetValue("CLIENT_EID"), m_iClientId);
                }

                objReader.Close();
            }
            catch (DataModelException p_objExp)
            {
                throw p_objExp;
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.GetOrgEid.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objReader != null) objReader.Dispose();
            }
        }


        /// Name		: GetClaimEventAndPolicyDate
        /// Author		: Mona Gaba
        /// Date Created: 04/16/2008
        ///*************************************************************************
        /// Amendment History
        ///*************************************************************************
        /// Date Amended   *   Amendment   *								Author
        ///*************************************************************************
        /// <summary>
        /// MITS 11517-Gets the Claim,Event and policy date corresponding to a claim-used for filtering
        /// </summary>
        /// <param name="p_sClaimId">Claim Id</param>
        /// <param name="p_sDateOfClaim">Claim Date</param>
        /// <param name="p_sDateOfEvent">Event Date</param>
        /// <param name="p_sDateOfPolicy">Policy Date</param>
        /// <returns>All values are returned in ref variables</returns>
        /// MITS 11517:Start
        private void GetClaimEventAndPolicyDate(string p_sClaimId, ref string p_sDateOfClaim, ref string p_sDateOfEvent, ref string p_sDateOfPolicy, DbConnection objConn, SysSettings objSys, string p_sLOB)
        {
            string sSQL = string.Empty;	//sql string
            DbReader objReader = null;	//Data reader object
            bool bEnhPolicySystem = false;

            //Start - 07/23/2010: Sumit - Added CLAIM.LINE_OF_BUS_CODE to fetch LOB which is used to check if Enh policy is active for this fetched LOB.
            //int iLOB = 0;
            bool bSuccess=false;
            //sSQL = "SELECT CLAIM.DATE_OF_CLAIM,EVENT.DATE_OF_EVENT FROM CLAIM,EVENT WHERE CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + p_sClaimId;
            //sSQL = "SELECT CLAIM.DATE_OF_CLAIM,CLAIM.LINE_OF_BUS_CODE,EVENT.DATE_OF_EVENT FROM CLAIM INNER JOIN EVENT ON CLAIM.EVENT_ID=EVENT.EVENT_ID WHERE CLAIM.CLAIM_ID=" + p_sClaimId;

            try
            {
                //Fetch the Details for this Department
                //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                //objReader = objConn.ExecuteReader(sSQL);
                //if (objReader.Read())
                //{
                //    p_sDateOfClaim = objReader.GetString("DATE_OF_CLAIM");
                //    p_sDateOfEvent = objReader.GetString("DATE_OF_EVENT");
                //    iLOB = objReader.GetInt32("LINE_OF_BUS_CODE");
                //}

                //objReader.Close();

                //sSQL = "SELECT USE_ENH_POL_FLAG FROM SYS_PARMS";
                //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                //if (objReader.Read())
                //{
                //    bEnhPolicySystem = Conversion.ConvertObjToBool(objReader.GetValue("USE_ENH_POL_FLAG"));
                //}
                //objReader.Close();



                //ColLobSettings objColLobSettings = new ColLobSettings(m_sConnectionString);
               
                //End: Sumit
                //SysSettings 
                if(objSys==null)
                    objSys = new SysSettings(m_sConnectionString, m_iClientId); //Ash - cloud

                if (objSys.MultiCovgPerClm == 0)
                {
                    int iEnhPol = objConn.ExecuteInt("SELECT USE_ENH_POL_FLAG FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE=" + p_sLOB);
                    if (iEnhPol == -1)
                    {
                        bEnhPolicySystem = true;
                    }
                    if (bEnhPolicySystem)
                    {
                        sSQL = "SELECT CLAIM.DATE_OF_CLAIM,CLAIM.LINE_OF_BUS_CODE,EVENT.DATE_OF_EVENT FROM CLAIM INNER JOIN EVENT ON CLAIM.EVENT_ID=EVENT.EVENT_ID WHERE CLAIM.CLAIM_ID=" + p_sClaimId;
                        using (objReader = objConn.ExecuteReader(sSQL))
                        {
                            if (objReader.Read())
                            {
                                p_sDateOfClaim = objReader.GetString("DATE_OF_CLAIM");
                                p_sDateOfEvent = objReader.GetString("DATE_OF_EVENT");
                                //iLOB = objReader.GetInt32("LINE_OF_BUS_CODE");
                            }
                            objReader.Close();
                        }
                        int iPolicyId = 0;
                        sSQL = "SELECT PRIMARY_POLICY_ID FROM CLAIM WHERE CLAIM_ID =" + p_sClaimId;
                        iPolicyId = objConn.ExecuteInt(sSQL);
                        sSQL = "SELECT EFFECTIVE_DATE FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER = (SELECT MAX(POLICY_X_TERM_ENH.TERM_NUMBER) FROM POLICY_X_TERM_ENH,POLICY_ENH,CLAIM WHERE POLICY_X_TERM_ENH.POLICY_ID = POLICY_ENH.POLICY_ID AND CLAIM.PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID AND CLAIM.CLAIM_ID =" + p_sClaimId + ")";
                        object oDate = objConn.ExecuteScalar(sSQL);
                        p_sDateOfPolicy = Conversion.ConvertObjToStr(oDate);
                    }
                    else
                    {
                        sSQL = "SELECT C.DATE_OF_CLAIM,C.LINE_OF_BUS_CODE,E.DATE_OF_EVENT,P.EFFECTIVE_DATE FROM POLICY P INNER JOIN CLAIM C ON C.PRIMARY_POLICY_ID = P.POLICY_ID INNER JOIN EVENT E ON C.EVENT_ID=E.EVENT_ID WHERE C.CLAIM_ID =" + p_sClaimId;
                        using (objReader = objConn.ExecuteReader(sSQL))
                        {
                            if (objReader.Read())
                            {
                                p_sDateOfClaim = objReader.GetString("DATE_OF_CLAIM");
                                p_sDateOfEvent = objReader.GetString("DATE_OF_EVENT");
                                //iLOB = objReader.GetInt32("LINE_OF_BUS_CODE");
                                p_sDateOfPolicy = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
                            }
                            objReader.Close();
                        }
                    }

                    objSys = null;


                    //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    //if (objReader.Read())
                    //{
                    //    p_sDateOfPolicy = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
                    //}
                    //objReader.Close();
                }
            }
            catch (DataModelException p_objExp)
            {
                throw p_objExp;
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.GetClaimEventAndPolicyDate.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objReader != null) objReader.Dispose();
            }
        }
        private void GetEventDate(string p_sEventId, ref string p_sDateOfEvent)
        {
            string sSQL = string.Empty;	//sql string
            DbReader objReader = null;	//Data reader object



            sSQL = "SELECT DATE_OF_EVENT FROM EVENT WHERE EVENT_ID=" + p_sEventId;

            try
            {
                //Fetch the Details for this Department
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                {
                    p_sDateOfEvent = Conversion.ConvertObjToStr(objReader.GetValue("DATE_OF_EVENT"));
                }

                objReader.Close();

               
            }
            catch (DataModelException p_objExp)
            {
                throw p_objExp;
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.GetClaimEventAndPolicyDate.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objReader != null) objReader.Dispose();
            }
        }
        /// Name		: IsCaseInSensitveSearch
        /// Author		: Sumeet Rathod
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Finds the case sensitiveness for Oracle database
        /// </summary>
        /// <returns>boolean value indicating case sensitiveness</returns>
        private bool IsCaseInSensitveSearch()
        {
            int iResult;
            string sSQL = "";
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT ORACLE_CASE_INS FROM SYS_PARMS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iResult = Common.Conversion.ConvertStrToInteger(objReader["ORACLE_CASE_INS"].ToString());
                        objReader.Close();
                        if (iResult == 1 || iResult == -1)
                            return true;
                    }
                }
            }
            //Riskmaster.Db is throwing following exception so need to catch the same
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.IsCaseInSensitveSearch.DataError", m_iClientId), p_objExp);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return false;
        }

        //zalam 08/27/2008 Mits:-11708
        //private void EvaluateTriggerDates(StringBuilder sSQL, string p_sFormName, string sToday, string p_sTriggerDate, string p_sEventDate)
        private void EvaluateTriggerDates(StringBuilder sSQL, string p_sFormName, string sToday, string p_sTriggerDate, string p_sEventDate, string p_sSessionClaimId, DbConnection objConn, SysSettings objSysSetting,string p_sLOB)
        {
            // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
            int iSessionClaimId = 0;
            bool bSuccess = false;

            iSessionClaimId = Conversion.CastToType<int>(p_sSessionClaimId, out bSuccess);
            // npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim

            switch (p_sFormName.ToLower())
            {
                case "":
                    sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                    break;
                case "event":
                    sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                    break;
                case "claimgc":
                case "claimwc":
                case "claimva":
                case "claimdi":
                case "claimpc":
                    sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                    if (p_sEventDate.Length > 0)
                    {
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sEventDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sEventDate + "') ");
                        sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + p_sEventDate + "' AND CODES.EFF_END_DATE>='" + p_sEventDate + "') ");
                    }
                    //zalam 08/27/2008 Mits:-11708 Start
                    // npadhy Start MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                    if (iSessionClaimId > 0 && (!string.IsNullOrEmpty(p_sSessionClaimId)))
                    // npadhy End MITS 21650 Claim type and Claim status Code list appearing blank while trying to create GC/VA claim
                    {
                        String sDateOfClaim = string.Empty;
                        String sDateOfEvent = string.Empty;
                        String sDateOfPolicy = string.Empty;
                        GetClaimEventAndPolicyDate(p_sSessionClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy, objConn, objSysSetting, p_sLOB);
                        if (!string.IsNullOrEmpty(sDateOfPolicy))
                        {
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");

                        }
                    }
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                    break;
                case "policy":
                    sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                    break;
                case "funds":
                case "split":
                    //PJS (MITS 15672): Filtering for Trans Type
                    sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" + p_sTriggerDate + "' AND CODES.EFF_END_DATE>='" + p_sTriggerDate + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append("	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                    break;
                //rsushilaggar MITS 20309 06/25/2010
                case "search":
                    break;
                //rsushilaggar MITS 20309 06/25/2010
                //MITS 15185 : Umesh
                default:
                    sSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                    sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");
                    break;
                //MITS 15185 : Umesh
             }
        }


        private string GetResTypeCodeId(string iRcRowId)
        {
            //int iPolCvgRowId = 0;
            //iClaimantEid = 0;
            int iDestReserveTypeCode = 0;
            string sSQL = "SELECT RESERVE_TYPE_CODE FROM RESERVE_CURRENT WHERE RC_ROW_ID =" + iRcRowId;

            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {
                if (objReader.Read())
                {
                    //iPolCvgRowId = objReader.GetInt("POLCVG_ROW_ID");
                    //iClaimantEid = objReader.GetInt("CLAIMANT_EID");
                    iDestReserveTypeCode = objReader.GetInt("RESERVE_TYPE_CODE");
                }
            }
            return iDestReserveTypeCode.ToString();
        }
        #endregion
        #region Get Entity TableId
        /// Name		: GetEntityTableId
        /// Author		: Priya Mittal
        /// Date Created: 08/28/08	
        /// MITS        : 12369
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Fetches the Table Id of the Org Hierarchy
        /// </summary>
        /// <param name="p_sOrgLevel">Level</param>
        /// <returns>Table Id depending on given parameter</returns>
        private int GetEntityTableId(string p_sOrgLevel)
        {
            DbReader objRdr = null;
            string sSql = "";
            int tableId = 0;

            switch (p_sOrgLevel.ToUpper())
            {
                case "CLIENT":
                    return 1005;
                case "COMPANY":
                    return 1006;
                case "OPERATION":
                    return 1007;
                case "REGION":
                    return 1008;
                case "DIVISION":
                    return 1009;
                case "LOCATION":
                    return 1010;
                case "FACILITY":
                    return 1011;
                case "DEPARTMENT":
                    return 1012;
                default:
                    sSql = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + p_sOrgLevel + "'";
                    using (objRdr = DbFactory.GetDbReader(m_sConnectionString, sSql))
                    {
                        while (objRdr.Read())
                        {
                            tableId = Convert.ToInt32((objRdr.GetValue(0).ToString()));
                        }     
                    }
                    return tableId;
            }
        }
        #endregion
        #region Get ParentEids
        /// Name		: GetParentEid
        /// Author		: Prashant J Singh
        /// Date Created: 05/18/09	
        /// MITS        : 15220
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Fetches the Table Id of the Org Hierarchy
        /// </summary>
        /// <param name="sInsEid">List of Insured</param>
        /// <returns>Entity Ids of parents of insureds</returns>

        private string GetParentEid( string sInsEid)
        {
            StringBuilder sParentEid = new StringBuilder();
            string sTempIns = string.Empty;
            string sParent = string.Empty;
            string sRet = string.Empty; 

            string sSQL = string.Empty;	//sql string
            DbReader objReader = null;	//Data reader object

            string[] sInsList = sInsEid.Split(' ');

            
            try
            {
                foreach (string sInsId in sInsList)
                {
                    sTempIns = sInsId;

                    if (sTempIns != "0" && !String.IsNullOrEmpty(sTempIns))
                    {
                        sParentEid.Append(sTempIns);
                        sParentEid.Append(" ,");
                    }

                    while (sTempIns != "0" && !String.IsNullOrEmpty(sTempIns))
                    {
                        sSQL.Remove(0, sSQL.Length);    
                        sSQL = "SELECT PARENT_EID FROM ENTITY WHERE ENTITY_ID =" + sTempIns;

                        //Fetch the Details for this Department
                        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                        if (objReader.Read())
                        {
                            sParent = Conversion.ConvertObjToStr(objReader.GetValue("PARENT_EID"));
                            if (sParent != "0" && !String.IsNullOrEmpty(sParent))
                            {
                                sParentEid.Append(sParent);
                                sTempIns = sParent;
                                sParentEid.Append(" ,");
                            }
                            else if(sParent =="0")
                                sTempIns = sParent;
                        }

                        objReader.Close();
                    }
                }
                sRet = sParentEid.ToString().TrimEnd(','); 
            }
            catch (DataModelException p_objExp)
            {
                throw p_objExp;
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.GetParentEid.Error",m_iClientId), p_objExp);
            }
            finally
            {
                if (objReader != null) objReader.Dispose();
            }

            return sRet;
        }

        /// <summary>
        /// Gets the org preference filter.
        /// </summary>
        /// <returns>
        /// Filter by effective date.
        /// </returns>
        private bool GetOrgPreferenceFilter()
        {
            string sSql = string.Empty;
            string sXml = string.Empty;
            DbReader objReader = null;
            bool returnValue = false;
            
            sSql = string.Format("SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID= {0}",  userid);
            using (objReader = DbFactory.GetDbReader(m_sConnectionString, sSql))
            {
                if (objReader.Read())
                {
                    sXml = Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                }
                objReader.Close();
            }

            if (sXml != string.Empty)
            {
                XElement root = XElement.Parse(sXml);
                returnValue = root != null && root.Element("OrgViewNet") != null && root.Element("OrgViewNet").Attribute("filter") != null ? Conversion.ConvertStrToBool(root.Element("OrgViewNet").Attribute("filter").Value) : false;
            }
            return returnValue;
        }
        #endregion
    }
}
