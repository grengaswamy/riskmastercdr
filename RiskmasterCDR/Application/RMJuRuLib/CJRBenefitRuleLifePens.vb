Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CJRBenefitRuleLifePens
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRBenefitRuleLifePens"

    'local variable to hold collection
    Private mCol As Collection

    Private Function GetFieldSQL() As String
        Dim sSQL As String
        GetFieldSQL = sSQL
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", BEGIN_DATE"
        sSQL = sSQL & ", END_DATE"
        sSQL = sSQL & ", MAX_AWW"
        sSQL = sSQL & ", MIN_AWW"
        sSQL = sSQL & ", MIN_DIS_PERCENT"
        sSQL = sSQL & ", PD_MULTIPLIER"
        GetFieldSQL = sSQL
    End Function

    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim TableRowID As Integer
        Try

            LoadData = 0

            sSQL = GetFieldSQL()
            sSQL = sSQL & " FROM WCP_LIFEPEN_LIMITS"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID

            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            'While objReader.Read()
            '    TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            '    Add(objReader.GetString( "BEGIN_DATE"), False, objReader.GetInt32( "DELETED_FLAG"), objReader.GetString( "EFFECTIVE_DATE"), objReader.GetString( "END_DATE"), objReader.GetInt32( "JURIS_ROW_ID"), objReader.GetDouble( "MAX_AWW"), objReader.GetDouble( "MIN_AWW"), objReader.GetDouble( "MIN_DIS_PERCENT"), objReader.GetDouble( "PD_MULTIPLIER"), TableRowID, "k" & CStr(TableRowID))
            '    
            'End While
            'End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                Add(objReader.GetString("BEGIN_DATE"), False, objReader.GetInt32("DELETED_FLAG"), objReader.GetString("EFFECTIVE_DATE"), objReader.GetString("END_DATE"), objReader.GetInt32("JURIS_ROW_ID"), objReader.GetDouble("MAX_AWW"), objReader.GetDouble("MIN_AWW"), objReader.GetDouble("MIN_DIS_PERCENT"), objReader.GetDouble("PD_MULTIPLIER"), TableRowID, "k" & CStr(TableRowID))
            End While


            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function Add(ByRef BeginDateDTG As String, ByRef DataHasChanged As Boolean, ByRef DeletedFlag As Short, ByRef EffectiveDateDTG As String, ByRef EndDateDTG As String, ByRef JurisRowID As Short, ByRef MaxAWW As Double, ByRef MinAWW As Double, ByRef MinDisability As Double, ByRef PDMultipler As Double, ByRef TableRowID As Integer, ByRef sKey As String) As CJRBenefitRuleLifePen
        'create a new object
        Dim objNewMember As CJRBenefitRuleLifePen
        objNewMember = New CJRBenefitRuleLifePen
        'set the properties passed into the method
        objNewMember.TableRowID = TableRowID
        objNewMember.MinAWW = MinAWW
        objNewMember.MaxAWW = MaxAWW
        objNewMember.MinDisability = MinDisability
        objNewMember.JurisRowID = JurisRowID
        objNewMember.EndDateDTG = EndDateDTG
        objNewMember.EffectiveDateDTG = EffectiveDateDTG
        objNewMember.DeletedFlag = DeletedFlag
        objNewMember.DataHasChanged = DataHasChanged
        objNewMember.BeginDateDTG = BeginDateDTG
        objNewMember.PDMultipler = PDMultipler

        mCol.Add(objNewMember, sKey)
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing
    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleLifePen
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()
    End Sub
End Class

