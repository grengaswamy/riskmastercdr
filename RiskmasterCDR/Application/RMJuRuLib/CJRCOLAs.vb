Option Strict Off
Option Explicit On
Public Class CJRCOLAs
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRCOLAS"
    Private Const sTableName As String = "WCP_COLA"

    'Class properties--local copy
    Private mCol As Collection
    Private Function GetFieldNames() As String
        Dim sSQL As String
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID, JURIS_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",DELETED_FLAG, EFFECTIVE_DATE"
        sSQL = sSQL & ",ANNIVERSARY_CODE"
        sSQL = sSQL & ",BEGININGRANGE_DATE"
        sSQL = sSQL & ",END_DATE"
        sSQL = sSQL & ",ENDINGRAGE_DATE,MAX_AMT,MULTIPLER"
        sSQL = sSQL & ",PERCENTAGE"
        sSQL = sSQL & ",PERMITTED"
        sSQL = sSQL & ",REFERENCE"

        GetFieldNames = sSQL


    End Function

    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim lTableRowID As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0



            sSQL = ""
            sSQL = sSQL & GetFieldNames()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                lTableRowID = objReader.GetInt32("TABLE_ROW_ID")
                Add(objReader.GetInt32("ANNIVERSARY_CODE"), objReader.GetString("BEGININGRANGE_DATE"), objReader.GetInt32("DELETED_FLAG"), objReader.GetString("EFFECTIVE_DATE"), objReader.GetString("END_DATE"), objReader.GetString("ENDINGRAGE_DATE"), objReader.GetInt32("JURIS_ROW_ID"), objReader.GetDouble("MAX_AMT"), objReader.GetDouble("MULTIPLER"), objReader.GetDouble("PERCENTAGE"), objReader.GetInt32("PERMITTED"), objReader.GetInt32("TABLE_ROW_ID"), objReader.GetString("REFERENCE"), "k" & lTableRowID)



            End While


            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Add(ByRef AnniversaryCode As Integer, ByRef BeginningDateRangeDTG As String, ByRef DeletedFlag As Integer, ByRef EffectiveDateDTG As String, ByRef EndDateDTG As String, ByRef EndingDateRangeDTG As String, ByRef JurisRowID As Integer, ByRef MaxAmount As Double, ByRef Multipler As Double, ByRef Percentage As Double, ByRef Permitted As Integer, ByRef TableRowID As Integer, ByRef Reference As String, ByRef sKey As String) As CJRCOLA
        Try
            Dim objNewMember As CJRCOLA
            objNewMember = New CJRCOLA

            'set the properties passed into the method
            With objNewMember
                .AnniversaryCode = AnniversaryCode
                .BeginningDateRangeDTG = BeginningDateRangeDTG
                .DeletedFlag = DeletedFlag
                .EffectiveDateDTG = EffectiveDateDTG
                .EndDateDTG = EndDateDTG
                .EndingDateRangeDTG = EndingDateRangeDTG
                .JurisRowID = JurisRowID
                .MaxAmount = MaxAmount
                .Multipler = Multipler
                .Percentage = Percentage
                .Permitted = Permitted
                .TableRowID = TableRowID
                .Reference = Reference
            End With
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function
    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRCOLA
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

