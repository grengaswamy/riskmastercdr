Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleNSPPDCAWeeks
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRBenefitRuleNSPPDCAWeeks"
    Const sTableName As String = "WCP_NSPPD_WEEKS"

    'Class properties, local variable(s)
    Private mCol As Collection
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJRBenefitRuleNSPPDCAWeek
        Try

            LoadData = 0
            'ClearObject

            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " ORDER BY BEGIN_DATE,BEGIN_PERCENT"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRBenefitRuleNSPPDCAWeek
                With objRecord
                    .BaseWeeks = objReader.GetDouble("BASE_WEEKS")
                    .BeginningDateDTG = objReader.GetString("BEGIN_DATE")
                    .BeginPercent = objReader.GetDouble("BEGIN_PERCENT")
                    .EndingDateDTG = objReader.GetString("END_DATE")
                    .EndPercent = objReader.GetDouble("END_PERCENT")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .PDDistrator = objReader.GetDouble("PD_DISTRACTOR")
                    .PDMultiplier = objReader.GetDouble("PD_MULTIPLIER")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.BaseWeeks, .BeginningDateDTG, .BeginPercent, .EndingDateDTG, .EndPercent, .JurisRowID, .PDDistrator, .PDMultiplier, .TableRowID, "k" & .TableRowID)

                End With


            End While

            LoadData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Add(ByRef BaseWeeks As Double, ByRef BeginningDateDTG As String, ByRef BeginPercent As Double, ByRef EndingDateDTG As String, ByRef EndPercent As Double, ByRef JurisRowID As Integer, ByRef PDDistrator As Double, ByRef PDMultiplier As Double, ByRef TableRowID As Integer, ByRef sKey As String) As CJRBenefitRuleNSPPDCAWeek
        'create a new object
        Dim objNewMember As CJRBenefitRuleNSPPDCAWeek
        Try
            objNewMember = New CJRBenefitRuleNSPPDCAWeek
            'set the properties passed into the method
            objNewMember.JurisRowID = JurisRowID
            objNewMember.TableRowID = TableRowID
            objNewMember.BeginningDateDTG = BeginningDateDTG
            objNewMember.EndingDateDTG = EndingDateDTG
            objNewMember.BeginPercent = BeginPercent
            objNewMember.EndPercent = EndPercent
            objNewMember.BaseWeeks = BaseWeeks
            objNewMember.PDMultiplier = PDMultiplier
            objNewMember.PDDistrator = PDDistrator

            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleNSPPDCAWeek
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", BEGIN_DATE, END_DATE"
        sSQL = sSQL & ", BEGIN_PERCENT, END_PERCENT"
        sSQL = sSQL & ", BASE_WEEKS"
        sSQL = sSQL & ", PD_MULTIPLIER"
        sSQL = sSQL & ", PD_DISTRACTOR"
        sSQL = sSQL & " FROM " & sTableName
        GetBaseSQL = sSQL



    End Function
End Class

