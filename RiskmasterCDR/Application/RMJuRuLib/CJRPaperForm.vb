Option Strict Off
Option Explicit On
Public Class CJRPaperForm
    Const sClassName As String = "CJRPaperForm"

    'Class properties, local copy
    Private m_JurisRowID As Integer
    Private m_FormName As String
    Private m_FormID As Integer
    Private m_FormCategoryName As String
    Private m_FormTitle As String
    Private m_DataHasChanged As Boolean
    Private Function GetSQLFields() As String
        Dim sSQL As String
        GetSQLFields = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        'JTODD22 11/01/2004 Not in table--sSQL = sSQL & " WCP_FORMS.TABLE_ROW_ID"
        sSQL = sSQL & " DTTM_RCD_ADDED"
        sSQL = sSQL & ", ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD"
        sSQL = sSQL & ", UPDATED_BY_USER"
        sSQL = sSQL & ", WCP_FORMS.STATE_ROW_ID"
        sSQL = sSQL & ", FORM_CAT_DESC"
        sSQL = sSQL & ", FORM_ID"
        sSQL = sSQL & ", FORM_TITLE"
        sSQL = sSQL & ", FORM_NAME"
        GetSQLFields = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lFormID As Integer) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0


            sSQL = ""
            sSQL = sSQL & GetSQLFields()
            sSQL = sSQL & " FROM WCP_FORMS, WCP_FORMS_CAT_LKUP"
            sSQL = sSQL & " WHERE WCP_FORMS.FORM_CATEGORY = WCP_FORMS_CAT_LKUP.FORM_CAT"
            sSQL = sSQL & " AND WCP_FORMS.FORM_ID = " & lFormID
            sSQL = sSQL & " AND PRIMARY_FORM_FLAG <> 0"
            sSQL = sSQL & " ORDER BY FORM_CAT_DESC, FORM_ID, FORM_TITLE, FORM_NAME"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_FormCategoryName = Trim(objReader.GetString("FORM_CAT_DESC"))
                m_FormID = objReader.GetInt32("FORM_ID")
                m_FormName = Trim(objReader.GetString("FORM_NAME"))
                m_FormTitle = Trim(objReader.GetString("FORM_TITLE"))
            End If

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sRoutineName As String = "SaveData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = ""
            sSQL = sSQL & GetSQLFields()
            sSQL = sSQL & " FROM WCP_FORMS, WCP_FORMS_CAT_LKUP"
            sSQL = sSQL & " WHERE WCP_FORMS.FORM_CATEGORY = WCP_FORMS_CAT_LKUP.FORM_CAT"
            sSQL = sSQL & " AND WCP_FORMS.TABLE_ROW_ID = " & m_FormID
            sSQL = sSQL & " AND PRIMARY_FORM_FLAG <> 0"
            sSQL = sSQL & " ORDER BY FORM_CAT_DESC, FORM_ID, FORM_TITLE, FORM_NAME"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_FormCategoryName = Trim(objReader.GetString("FORM_CAT_DESC"))
                m_FormID = objReader.GetInt32("FORM_ID")
                m_FormName = Trim(objReader.GetString("FORM_NAME"))
                m_FormTitle = Trim(objReader.GetString("FORM_TITLE"))
                m_JurisRowID = CInt(objReader.GetString("STATE_ROW_ID"))
            End If

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sRoutineName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property FormTitle() As String
        Get
            FormTitle = m_FormTitle
        End Get
        Set(ByVal Value As String)
            m_FormTitle = Value
        End Set
    End Property
    Public Property FormCategoryName() As String
        Get
            FormCategoryName = m_FormCategoryName
        End Get
        Set(ByVal Value As String)
            m_FormCategoryName = Value
        End Set
    End Property
    Public Property FormID() As Integer
        Get
            FormID = m_FormID
        End Get
        Set(ByVal Value As Integer)
            m_FormID = Value
        End Set
    End Property
    Public Property FormName() As String
        Get
            FormName = m_FormName
        End Get
        Set(ByVal Value As String)
            m_FormName = Value
        End Set
    End Property

    Public Property JurisRowID() As Short
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Short)
            m_JurisRowID = Value
        End Set
    End Property
End Class

