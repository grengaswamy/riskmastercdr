Option Strict Off
Option Explicit On
Public Class CJREDIStdReleases
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJREDIStdReleases"
    Const sTableName2 As String = "EDI_STDFILING_LKUP"
    Const sTableName3 As String = "WCP_STDFILING_LKUP"

    'local variable to hold collection
    Private mCol As Collection
    Private mColJurisdictionsImplemented As New Collection

    Private Function GetFieldSQL(ByRef lJurisRowID As Integer) As String
        Dim sSQL As String

        GetFieldSQL = sSQL
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", SHORT_CODE, RELEASE_NAME"
        sSQL = sSQL & ", RELEASE_ROW_ID"
        If lJurisRowID > 0 Then
            sSQL = sSQL & " FROM " & sTableName3
        Else
            sSQL = sSQL & " FROM " & sTableName2
        End If
        GetFieldSQL = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRelease As CJREDIStdRelease
        Try

            LoadData = 0

            g_sErrProcedure = sClassName & "." & sFunctionName
            g_sErrDescription = ""



            sSQL = GetFieldSQL(lJurisRowID)
            If lJurisRowID > 0 Then
                sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            End If
            sSQL = sSQL & " ORDER BY RELEASE_NAME"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRelease = New CJREDIStdRelease
                With objRelease
                    .ReleaseDesc = objReader.GetString("RELEASE_NAME")
                    .ShortCode = objReader.GetString("SHORT_CODE")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .ReleaseRowID = objReader.GetInt32("RELEASE_ROW_ID")
                    Add(.TableRowID, .ReleaseDesc, .ShortCode, .ReleaseRowID, "k" & CStr(.TableRowID))
                End With
                'UPGRADE_NOTE: Object objRelease may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRelease = Nothing

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & " " & Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Public Function Add(ByRef TableRowID As Integer, ByRef ReleaseDesc As String, ByRef ShortCode As String, ByRef ReleaseRowID As Integer, Optional ByRef sKey As String = "") As CJREDIStdRelease
        Dim objNewMember As CJREDIStdRelease
        Try
            objNewMember = New CJREDIStdRelease

            'set the properties passed into the method
            objNewMember.TableRowID = TableRowID
            objNewMember.ReleaseDesc = ReleaseDesc
            objNewMember.ShortCode = ShortCode
            objNewMember.ReleaseRowID = ReleaseRowID

            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJREDIStdRelease
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function

    Public ReadOnly Property JurisdictionsImplemented(ByVal objUser As Riskmaster.Security.UserLogin) As Collection
        Get
            'This is a hack to speed up the initial load of Juris Rules application.
            Const sFunctionName As String = "JurisdictionsImplemented"
            Dim objReader As DbReader
            Dim sSQL As String
            Try

                'UPGRADE_NOTE: Object mColJurisdictionsImplemented may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                mColJurisdictionsImplemented = Nothing


                sSQL = "SELECT DISTINCT JURIS_ROW_ID FROM WCP_STDFILING_LKUP"
                'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                'If Not g_DBObject.DB_EOF(iRdset) Then
                '	While objReader.Read()
                '		mColJurisdictionsImplemented.Add(objReader.GetInt32( "JURIS_ROW_ID"))
                '		
                '	End While
                '         End If

                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

                While objReader.Read()
                    mColJurisdictionsImplemented.Add(objReader.GetInt32("JURIS_ROW_ID"))
                End While



                JurisdictionsImplemented = mColJurisdictionsImplemented

            Catch ex As Exception
                With Err()
                    g_lErrNum = Err.Number
                    g_sErrSrc = .Source
                    g_sErrDescription = g_sErrDescription & " " & Err.Description
                End With
                SafeCloseRecordset(objReader)

                g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
                g_lErrLine = Erl()
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
            Finally
                SafeDropRecordset(objReader)
            End Try
        End Get
    End Property
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)








    End Sub
    Public Sub New()
        MyBase.New()
        mCol = New Collection
    End Sub
    Protected Overrides Sub Finalize()
        mCol = Nothing
        MyBase.Finalize()


    End Sub
End Class

