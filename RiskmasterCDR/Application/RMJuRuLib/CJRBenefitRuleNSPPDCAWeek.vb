Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleNSPPDCAWeek
    Const sClassName As String = "CJRBenefitRuleNSPPDCAWeek"
    Const sTableName As String = "WCP_NSPPD_WEEKS"

    Private m_DataHasChanged As Boolean
    Private m_NoCodeID As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_YesCodeID As Integer
    'Class properties, local variable(s)
    Private m_BaseWeeks As Double
    Private m_BeginningDateDTG As String
    Private m_EndingDateDTG As String
    Private m_JurisRowID As Integer
    Private m_TableRowID As Integer
    Private m_BeginPercent As Double
    Private m_EndPercent As Double
    Private m_PayLumpSumInReEmpCode As Integer
    Private m_PayPeriodCode As Integer
    Private m_PDMultiplier As Double
    Private m_PDDistrator As Double
    Private m_Warning As String
    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try
            AssignData = 0
            ClearObject()

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_BeginningDateDTG = objReader.GetString("BEGIN_DATE")
                m_EndingDateDTG = objReader.GetString("END_DATE")
                m_BeginPercent = objReader.GetDouble("BEGIN_PERCENT")
                m_EndPercent = objReader.GetDouble("END_PERCENT")
                m_BaseWeeks = objReader.GetDouble("BASE_WEEKS")
                m_PDMultiplier = objReader.GetDouble("PD_MULTIPLIER")
                m_PDDistrator = objReader.GetDouble("PD_DISTRACTOR")
            End If
            SafeCloseRecordset(objReader)

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID


            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : SaveData
    ' DateTime  : 7/13/2004 09:59
    ' Author    : jtodd22
    ' Purpose   :
    ' Notes     : Do not pass TableRowID, it is part of class (m_TableRowID)
    '---------------------------------------------------------------------------------------
    '
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0

            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("BEGIN_DATE").Value = m_BeginningDateDTG
                objWriter.Fields("END_DATE").Value = m_EndingDateDTG
                objWriter.Fields("BEGIN_PERCENT").Value = m_BeginPercent
                objWriter.Fields("END_PERCENT").Value = m_EndPercent
                objWriter.Fields("BASE_WEEKS").Value = m_BaseWeeks
                objWriter.Fields("PD_MULTIPLIER").Value = m_PDMultiplier
                objWriter.Fields("PD_DISTRACTOR").Value = m_PDDistrator
                objWriter.Execute()

            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))

                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("BEGIN_DATE", m_BeginningDateDTG)
                objWriter.Fields.Add("END_DATE", m_EndingDateDTG)
                objWriter.Fields.Add("BEGIN_PERCENT", m_BeginPercent)
                objWriter.Fields.Add("END_PERCENT", m_EndPercent)
                objWriter.Fields.Add("BASE_WEEKS", m_BaseWeeks)
                objWriter.Fields.Add("PD_MULTIPLIER", m_PDMultiplier)
                objWriter.Fields.Add("PD_DISTRACTOR", m_PDDistrator)
                objWriter.Execute()

            End If
            m_DataHasChanged = False
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try


    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String, ByRef dPercentage As Double) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(BEGIN_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND BEGIN_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetBaseSQL()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND BEGIN_DATE = (" & sSQL2 & ")"
            sSQL = sSQL & " AND BEGIN_PERCENT >= " & dPercentage
            sSQL = sSQL & " AND END_PERCENT <= " & dPercentage

            LoadDataByEventDate = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadDataByTableRowID = 0

            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID
            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function ValidateData() As Integer



    End Function
    Public Property BaseWeeks() As Double
        Get
            BaseWeeks = m_BaseWeeks
        End Get
        Set(ByVal Value As Double)
            m_BaseWeeks = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property BeginningDateDTG() As String
        Get
            BeginningDateDTG = m_BeginningDateDTG
        End Get
        Set(ByVal Value As String)
            m_BeginningDateDTG = Value
        End Set
    End Property

    Public Property EndingDateDTG() As String
        Get
            EndingDateDTG = m_EndingDateDTG
        End Get
        Set(ByVal Value As String)
            m_EndingDateDTG = Value
        End Set
    End Property
    Public Property PDDistrator() As Double
        Get
            PDDistrator = m_PDDistrator
        End Get
        Set(ByVal Value As Double)
            m_PDDistrator = Value
        End Set
    End Property
    Public Property PDMultiplier() As Double
        Get
            PDMultiplier = m_PDMultiplier
        End Get
        Set(ByVal Value As Double)
            m_PDMultiplier = Value
        End Set
    End Property
    Public Property EndPercent() As Double
        Get
            EndPercent = m_EndPercent
        End Get
        Set(ByVal Value As Double)
            m_EndPercent = Value
        End Set
    End Property
    Public Property BeginPercent() As Double
        Get
            BeginPercent = m_BeginPercent
        End Get
        Set(ByVal Value As Double)
            m_BeginPercent = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property
    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
    Private Function ClearObject() As Integer
        m_BaseWeeks = 0
        m_BeginningDateDTG = ""
        m_BeginPercent = 0
        m_DataHasChanged = False
        m_EndingDateDTG = ""
        m_EndPercent = 0
        m_JurisRowID = 0
        m_TableRowID = 0
        m_PDMultiplier = 0
        m_PDDistrator = 0




    End Function
    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", BEGIN_DATE, END_DATE"
        sSQL = sSQL & ", BEGIN_PERCENT, END_PERCENT"
        sSQL = sSQL & ", BASE_WEEKS"
        sSQL = sSQL & ", PD_MULTIPLIER"
        sSQL = sSQL & ", PD_DISTRACTOR"
        GetBaseSQL = sSQL



    End Function
End Class

