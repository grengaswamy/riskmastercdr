Option Strict Off
Option Explicit On
Public Class CJRSpendableIncomes
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRSpendableIncomes"

    'Class properties--local copy
    Private mCol As Collection
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sEffectiveDateDTG As String) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJRSpendableIncome
        Try

            LoadData = 0



            sSQL = GetFieldSelect(lJurisRowID, sEffectiveDateDTG)

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRSpendableIncome
                With objRecord
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .BaseAmount = objReader.GetDouble("BASE_AMOUNT")
                    .TaxStatusCode = objReader.GetInt32("TAX_STATUS_CODE")
                    .Exemption00 = objReader.GetDouble("EXEMP_A")
                    .Exemption01 = objReader.GetDouble("EXEMP_B")
                    .Exemption02 = objReader.GetDouble("EXEMP_C")
                    .Exemption03 = objReader.GetDouble("EXEMP_D")
                    .Exemption04 = objReader.GetDouble("EXEMP_E")
                    .Exemption05 = objReader.GetDouble("EXEMP_F")
                    .Exemption06 = objReader.GetDouble("EXEMP_G")
                    .Exemption07 = objReader.GetDouble("EXEMP_H")
                    .Exemption08 = objReader.GetDouble("EXEMP_I")
                    .Exemption09 = objReader.GetDouble("EXEMP_J")
                    .Exemption10orMore = objReader.GetDouble("EXEMP_K")
                    .Exemption11 = objReader.GetDouble("EXEMP_L")
                    .TaxStatusText = objReader.GetString("SHORT_CODE")
                    .UseDiscount00Code = objReader.GetInt32("USE_DISCNT_A_CODE")

                    Add2(.TableRowID, .JurisRowID, .EffectiveDateDTG, .DeletedFlag, .BaseAmount, .TaxStatusCode, .TaxStatusText, .Exemption00, .Exemption01, .Exemption02, .Exemption03, .Exemption04, .Exemption05, .Exemption06, .Exemption07, .Exemption08, .Exemption09, .Exemption10orMore, .Exemption11, .DataHasChanged, .UseDiscount00Code, "k" & .TableRowID)

                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = CStr(g_sErrSrc & g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|")
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            DBDisconnect()

        End Try

    End Function
    Public Function LoadEffectiveDates(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJRSpendableIncome
        Try

            LoadEffectiveDates = 0

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " DISTINCT EFFECTIVE_DATE"
            sSQL = sSQL & " FROM WCP_SPENDABLE"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " ORDER BY EFFECTIVE_DATE ASC"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRSpendableIncome
                With objRecord
                    .TableRowID = 0
                    .JurisRowID = lJurisRowID
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .DeletedFlag = 0
                    .BaseAmount = 0
                    .TaxStatusCode = 0
                    .Exemption00 = 0
                    .Exemption01 = 0
                    .Exemption02 = 0
                    .Exemption03 = 0
                    .Exemption04 = 0
                    .Exemption05 = 0
                    .Exemption06 = 0
                    .Exemption07 = 0
                    .Exemption08 = 0
                    .Exemption09 = 0
                    .Exemption10orMore = 0
                    .Exemption11 = 0
                    .TaxStatusText = ""

                    Add(.TableRowID, .JurisRowID, .EffectiveDateDTG, .DeletedFlag, .BaseAmount, .TaxStatusCode, .TaxStatusText, .Exemption00, .Exemption01, .Exemption02, .Exemption03, .Exemption04, .Exemption05, .Exemption06, .Exemption07, .Exemption08, .Exemption09, .Exemption10orMore, .Exemption11, .DataHasChanged, "k" & .TableRowID)

                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing

            End While

            LoadEffectiveDates = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadEffectiveDates = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = CStr(g_sErrSrc & g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadEffectiveDates|")
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Public Function Add(ByRef TableRowID As Integer, ByRef JurisRowID As Short, ByRef EffectiveDateDTG As String, ByRef DeletedFlag As Short, ByRef BaseAmount As Double, ByRef TaxStatusCode As Integer, ByRef TaxStatusText As String, ByRef Exemption00 As Double, ByRef Exemption01 As Double, ByRef Exemption02 As Double, ByRef Exemption03 As Double, ByRef Exemption04 As Double, ByRef Exemption05 As Double, ByRef Exemption06 As Double, ByRef Exemption07 As Double, ByRef Exemption08 As Double, ByRef Exemption09 As Double, ByRef Exemption10orMore As Double, ByRef Exemption11 As Double, ByRef DataHasChanged As Boolean, ByRef sKey As String) As CJRSpendableIncome

        Try

            'create a new object
            Dim objNewMember As CJRSpendableIncome
            objNewMember = New CJRSpendableIncome

            'set the properties passed into the method
            objNewMember.TableRowID = TableRowID
            objNewMember.JurisRowID = JurisRowID
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.BaseAmount = BaseAmount
            objNewMember.TaxStatusCode = TaxStatusCode
            objNewMember.TaxStatusText = TaxStatusText
            objNewMember.Exemption00 = Exemption00
            objNewMember.Exemption01 = Exemption01
            objNewMember.Exemption02 = Exemption02
            objNewMember.Exemption03 = Exemption03
            objNewMember.Exemption04 = Exemption04
            objNewMember.Exemption05 = Exemption05
            objNewMember.Exemption06 = Exemption06
            objNewMember.Exemption07 = Exemption07
            objNewMember.Exemption08 = Exemption08
            objNewMember.Exemption09 = Exemption09
            objNewMember.Exemption10orMore = Exemption10orMore
            objNewMember.Exemption11 = Exemption11
            objNewMember.DataHasChanged = DataHasChanged
            'jtodd22 There are no keys in this collection
            mCol.Add(objNewMember)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = CStr(g_sErrSrc & g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|")
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function Add2(ByRef TableRowID As Integer, ByRef JurisRowID As Short, ByRef EffectiveDateDTG As String, ByRef DeletedFlag As Short, ByRef BaseAmount As Double, ByRef TaxStatusCode As Integer, ByRef TaxStatusText As String, ByRef Exemption00 As Double, ByRef Exemption01 As Double, ByRef Exemption02 As Double, ByRef Exemption03 As Double, ByRef Exemption04 As Double, ByRef Exemption05 As Double, ByRef Exemption06 As Double, ByRef Exemption07 As Double, ByRef Exemption08 As Double, ByRef Exemption09 As Double, ByRef Exemption10orMore As Double, ByRef Exemption11 As Double, ByRef DataHasChanged As Boolean, ByRef UseDiscount00Code As Double, ByRef sKey As String) As CJRSpendableIncome

        Try

            'create a new object
            Dim objNewMember As CJRSpendableIncome
            objNewMember = New CJRSpendableIncome

            'set the properties passed into the method
            objNewMember.TableRowID = TableRowID
            objNewMember.JurisRowID = JurisRowID
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.BaseAmount = BaseAmount
            objNewMember.TaxStatusCode = TaxStatusCode
            objNewMember.TaxStatusText = TaxStatusText
            objNewMember.Exemption00 = Exemption00
            objNewMember.Exemption01 = Exemption01
            objNewMember.Exemption02 = Exemption02
            objNewMember.Exemption03 = Exemption03
            objNewMember.Exemption04 = Exemption04
            objNewMember.Exemption05 = Exemption05
            objNewMember.Exemption06 = Exemption06
            objNewMember.Exemption07 = Exemption07
            objNewMember.Exemption08 = Exemption08
            objNewMember.Exemption09 = Exemption09
            objNewMember.Exemption10orMore = Exemption10orMore
            objNewMember.Exemption11 = Exemption11
            objNewMember.DataHasChanged = DataHasChanged
            objNewMember.UseDiscount00Code = UseDiscount00Code
            'jtodd22 There are no keys in this collection
            mCol.Add(objNewMember)

            'return the object created
            Add2 = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = CStr(g_sErrSrc & g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|")
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveCollection(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveCollection"

        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim objConn As DbConnection
        Dim objTrans As DbTransaction
        Dim lIndex As Integer
        Dim lTest As Integer
        Dim sSQL As String

        Try
            objConn = DbFactory.GetDbConnection(g_ConnectionString)
            objConn.Open()
            SaveCollection = 0
            sSQL = GetSQLFieldList_SaveCol()
            sSQL = sSQL & " FROM WCP_SPENDABLE"
            sSQL = sSQL & " WHERE 0 = -1"

            If objConn.State <> ConnectionState.Open Then
                g_sErrSrc = "RMJuRuLib|"
                g_sErrDescription = "Failed to connect to database."
                g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
                g_lErrLine = Erl()
                g_lErrNum = 70001
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                Err.Raise(g_lErrNum, sClassName & "." & sFunctionName, g_sErrDescription)
                Exit Function
            End If
            objTrans = objConn.BeginTransaction()
            With mCol
                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                For lIndex = 1 To .Count()
                    lTest = lGetNextUID("WCP_SPENDABLE")
                    objWriter.Fields.Add("TABLE_ROW_ID", lTest)
                    objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                    objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                    objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                    objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                    objWriter.Fields.Add("EFFECTIVE_DATE", .Item(lIndex).EffectiveDateDTG)
                    objWriter.Fields.Add("JURIS_ROW_ID", .Item(lIndex).JurisRowID)
                    objWriter.Fields.Add("DELETED_FLAG", .Item(lIndex).DeletedFlag)
                    objWriter.Fields.Add("BASE_AMOUNT", .Item(lIndex).BaseAmount)
                    objWriter.Fields.Add("TAX_STATUS_CODE", .Item(lIndex).TaxStatusCode)
                    objWriter.Fields.Add("EXEMP_A", .Item(lIndex).Exemption00)
                    objWriter.Fields.Add("USE_DISCNT_A_CODE", .Item(lIndex).UseDiscount00Code)
                    objWriter.Fields.Add("EXEMP_B", .Item(lIndex).Exemption01)
                    objWriter.Fields.Add("EXEMP_C", .Item(lIndex).Exemption02)
                    objWriter.Fields.Add("EXEMP_D", .Item(lIndex).Exemption03)
                    objWriter.Fields.Add("EXEMP_E", .Item(lIndex).Exemption04)
                    objWriter.Fields.Add("EXEMP_F", .Item(lIndex).Exemption05)
                    objWriter.Fields.Add("EXEMP_G", .Item(lIndex).Exemption06)
                    objWriter.Fields.Add("EXEMP_H", .Item(lIndex).Exemption07)
                    objWriter.Fields.Add("EXEMP_I", .Item(lIndex).Exemption08)
                    objWriter.Fields.Add("EXEMP_J", .Item(lIndex).Exemption09)
                    objWriter.Fields.Add("EXEMP_K", .Item(lIndex).Exemption10orMore)
                    objWriter.Fields.Add("EXEMP_L", .Item(lIndex).Exemption11)
                    objWriter.Execute()
                Next lIndex
            End With 'mCol
            objTrans.Commit()
            SaveCollection = -1
        Catch ex As Exception
            objTrans.Rollback()
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveCollection = Err.Number
            SafeCloseRecordset(objReader)
            objTrans.Rollback()
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
            objTrans = Nothing
            objTrans.Dispose()
            objConn = Nothing
            objConn.Dispose()
        End Try
    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRSpendableIncome
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function GetFieldSelect(ByRef lJurisRowID As Integer, ByRef sEffectiveDateDTG As String) As String
        Dim sSQL As String
        GetFieldSelect = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",WCP_SPENDABLE.DELETED_FLAG"
        sSQL = sSQL & ",BASE_AMOUNT"
        sSQL = sSQL & ",CODES.SHORT_CODE"
        sSQL = sSQL & ",TAX_STATUS_CODE"
        sSQL = sSQL & ",EXEMP_A"
        sSQL = sSQL & ",USE_DISCNT_A_CODE"
        sSQL = sSQL & ",EXEMP_B"
        sSQL = sSQL & ",EXEMP_C"
        sSQL = sSQL & ",EXEMP_D"
        sSQL = sSQL & ",EXEMP_E"
        sSQL = sSQL & ",EXEMP_F"
        sSQL = sSQL & ",EXEMP_G"
        sSQL = sSQL & ",EXEMP_H"
        sSQL = sSQL & ",EXEMP_I"
        sSQL = sSQL & ",EXEMP_J"
        sSQL = sSQL & ",EXEMP_K"
        sSQL = sSQL & ",EXEMP_L"
        sSQL = sSQL & " FROM WCP_SPENDABLE, CODES"
        sSQL = sSQL & " WHERE CODES.CODE_ID = WCP_SPENDABLE.TAX_STATUS_CODE"
        sSQL = sSQL & " AND JURIS_ROW_ID = " & lJurisRowID
        sSQL = sSQL & " AND EFFECTIVE_DATE = '" & sEffectiveDateDTG & "'"
        sSQL = sSQL & " ORDER BY BASE_AMOUNT, SHORT_CODE ASC"
        GetFieldSelect = sSQL


    End Function
    Private Function GetSQLFieldList_SaveCol() As String
        Dim sSQL As String
        GetSQLFieldList_SaveCol = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",DELETED_FLAG"
        sSQL = sSQL & ",BASE_AMOUNT"
        sSQL = sSQL & ",TAX_STATUS_CODE"
        sSQL = sSQL & ",EXEMP_A"
        sSQL = sSQL & ",USE_DISCNT_A_CODE"
        sSQL = sSQL & ",EXEMP_B"
        sSQL = sSQL & ",EXEMP_C"
        sSQL = sSQL & ",EXEMP_D"
        sSQL = sSQL & ",EXEMP_E"
        sSQL = sSQL & ",EXEMP_F"
        sSQL = sSQL & ",EXEMP_G"
        sSQL = sSQL & ",EXEMP_H"
        sSQL = sSQL & ",EXEMP_I"
        sSQL = sSQL & ",EXEMP_J"
        sSQL = sSQL & ",EXEMP_K"
        sSQL = sSQL & ",EXEMP_L"
        GetSQLFieldList_SaveCol = sSQL


    End Function
End Class

