Option Strict Off
Option Explicit On
Public Class CJRCOLA
    Private Const sClassName As String = "CJRCOLA"
    Private Const sTableName As String = "WCP_COLA"

    'Class properties--local copy
    Private m_JurisRowID As Short
    Private m_DeletedFlag As Short
    Private m_EffectiveDateDTG As String
    Private m_TableRowID As Integer

    Private m_AnniversaryCode As Integer
    Private m_BeginningDateRangeDTG As String
    Private m_EndDateDTG As String
    Private m_EndingDateRangeDTG As String
    Private m_MaxAmount As Double
    Private m_Multipler As Double
    Private m_Percentage As Double
    Private m_Permitted As Integer
    Private m_Reference As String
    Private m_DataHasChanged As Boolean
    Private m_Warning As String

    Private Function GetFieldNames() As String
        Dim sSQL As String
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID, JURIS_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",DELETED_FLAG, EFFECTIVE_DATE"
        sSQL = sSQL & ",ANNIVERSARY_CODE"
        sSQL = sSQL & ",BEGININGRANGE_DATE"
        sSQL = sSQL & ",END_DATE"
        sSQL = sSQL & ",ENDINGRAGE_DATE,MAX_AMT,MULTIPLER"
        sSQL = sSQL & ",PERCENTAGE,PERMITTED"
        sSQL = sSQL & ",REFERENCE"
        GetFieldNames = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0


            sSQL = ""
            sSQL = sSQL & GetFieldNames()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")

                m_AnniversaryCode = objReader.GetInt32("ANNIVERSARY_CODE")
                m_BeginningDateRangeDTG = objReader.GetString("BEGININGRANGE_DATE")
                m_EndDateDTG = objReader.GetString("END_DATE")
                m_EndingDateRangeDTG = objReader.GetString("ENDINGRAGE_DATE")
                m_MaxAmount = objReader.GetDouble("MAX_AMT")
                m_Multipler = objReader.GetDouble("MULTIPLER")
                m_Percentage = objReader.GetDouble("PERCENTAGE")
                m_Permitted = objReader.GetInt32("PERMITTED")
                m_Reference = objReader.GetString("REFERENCE")
            End If
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = ""
            sSQL = sSQL & GetFieldNames()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("ANNIVERSARY_CODE").Value = m_AnniversaryCode
                objWriter.Fields("BEGININGRANGE_DATE").Value = m_BeginningDateRangeDTG
                objWriter.Fields("END_DATE").Value = m_EndDateDTG
                objWriter.Fields("ENDINGRAGE_DATE").Value = m_EndingDateRangeDTG
                objWriter.Fields("MAX_AMT").Value = m_MaxAmount
                objWriter.Fields("MULTIPLER").Value = m_Multipler
                objWriter.Fields("PERCENTAGE").Value = m_Percentage
                objWriter.Fields("PERMITTED").Value = m_Permitted
                objWriter.Fields("REFERENCE").Value = m_Reference
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lTest = lGetNextUID(sTableName)
                objWriter.Fields.Add("TABLE_ROW_ID", lTest)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("ANNIVERSARY_CODE", m_AnniversaryCode)
                objWriter.Fields.Add("BEGININGRANGE_DATE", m_BeginningDateRangeDTG)
                objWriter.Fields.Add("END_DATE", m_EndDateDTG)
                objWriter.Fields.Add("ENDINGRAGE_DATE", m_EndingDateRangeDTG)
                objWriter.Fields.Add("MAX_AMT", m_MaxAmount)
                objWriter.Fields.Add("MULTIPLER", m_Multipler)
                objWriter.Fields.Add("PERCENTAGE", m_Percentage)
                objWriter.Fields.Add("PERMITTED", m_Permitted)
                objWriter.Fields.Add("REFERENCE", m_Reference)
            End If
            objWriter.Execute()

            SaveData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function ValidateData() As Integer
        ValidateData = 0
        m_Warning = ""


    End Function
    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property AnniversaryCode() As Integer
        Get
            AnniversaryCode = m_AnniversaryCode
        End Get
        Set(ByVal Value As Integer)
            m_AnniversaryCode = Value
        End Set
    End Property

    Public Property Multipler() As Double
        Get
            Multipler = m_Multipler
        End Get
        Set(ByVal Value As Double)
            m_Multipler = Value
        End Set
    End Property

    Public Property Percentage() As Double
        Get
            Percentage = m_Percentage
        End Get
        Set(ByVal Value As Double)
            m_Percentage = Value
        End Set
    End Property

    Public Property MaxAmount() As Double
        Get
            MaxAmount = m_MaxAmount
        End Get
        Set(ByVal Value As Double)
            m_MaxAmount = Value
        End Set
    End Property

    Public Property EndingDateRangeDTG() As String
        Get
            EndingDateRangeDTG = m_EndingDateRangeDTG
        End Get
        Set(ByVal Value As String)
            m_EndingDateRangeDTG = Value
        End Set
    End Property

    Public Property BeginningDateRangeDTG() As String
        Get
            BeginningDateRangeDTG = m_BeginningDateRangeDTG
        End Get
        Set(ByVal Value As String)
            m_BeginningDateRangeDTG = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property
    Public Property JurisRowID() As Short
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Short)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property DeletedFlag() As Short
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Short)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property EndDateDTG() As String
        Get
            EndDateDTG = m_EndDateDTG
        End Get
        Set(ByVal Value As String)
            m_EndDateDTG = Value
        End Set
    End Property

    Public Property Permitted() As Integer
        Get
            Permitted = m_Permitted
        End Get
        Set(ByVal Value As Integer)
            m_Permitted = Value
        End Set
    End Property

    Public Property Reference() As String
        Get
            Reference = m_Reference
        End Get
        Set(ByVal Value As String)
            m_Reference = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
End Class

