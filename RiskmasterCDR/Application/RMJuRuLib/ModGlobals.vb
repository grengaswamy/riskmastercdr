Option Strict Off
Option Explicit On
Module modGlobals
	
    Public g_objLogin As Riskmaster.Security.Login
    Public g_objUser As Riskmaster.Security.UserLogin
    Public g_dbMake As Riskmaster.Db.eDatabaseType
	
    Public g_hEnv As Integer

    Public g_ConnectionString As String


	Public g_bInitialized As Boolean
	Public g_sErrProcedure As String
	Public g_lErrLine As Integer
	Public g_lErrNum As Integer
	Public g_sErrSrc As String
	Public g_sErrDescription As String
	
	'BSB 04.23.2003 Global hacks put in to support RMNet which does not have
	' a fully populated CRMApp object to work with...
	Public g_RmDSN As String
	Public g_SecDSN As String
	Public g_UserId As String
	Public g_UserLoginName As String
	
	Public Enum DisBenCalcMask
		dbcSuccess = 0
		dbcNoWeeklyWage = 1
		dbcNoEventDate = 2
		dbcNoBankAccounts = 4
		dbcNoTransMapping = 8
		dbcNoRecord = 16
		dbcNoPaymentDue = 32
		dbcNoPPDRecord = 64
		dbcSplitPayment = 128
		dbcNoRateChange = 256
		dbcRateChanged = 512
		dbcPreCATwoYear = 1024
		dbcNoEarnings = 2048
		dbcNoEmployee = 4096
		dbcNoJurisdictionalRule = 8192
		dbcNoImpairment = 16384
	End Enum
	
	'This variable will used if errors have to be logged
	'into a file and not display on the screenm
	Public g_bBatchMode As Boolean
	
	Public g_sClientDictPath As String
	
	Public g_objDeleteReturn As CJRDeleteReturn
End Module

