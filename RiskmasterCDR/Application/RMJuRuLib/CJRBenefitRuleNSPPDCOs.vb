Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleNSPPDCOs
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRBenefitRuleNSPPDCOs"
    Const sTableName As String = "WCP_MIB_RULE"

    'Class properties, local variable(s)
    Private mCol As Collection
    Private Function Add(ByRef DataHasChanged As Boolean, ByRef EffectiveDateDTG As String, ByRef JurisRowID As Integer, ByRef MMIDateRequiredCode As Integer, ByRef MonthsFromInjury As Integer, ByRef ScheduledInjuryCode As Integer, ByRef TableRowID As Integer, ByRef UseTTDRateCode As Integer, ByRef WeeksPayable As Integer, ByRef sKey As String) As CJRBenefitRuleNSPPDCO
        Try
            'create a new object
            Dim objNewMember As CJRBenefitRuleNSPPDCO
            objNewMember = New CJRBenefitRuleNSPPDCO
            'set the properties passed into the method
            objNewMember.JurisRowID = JurisRowID
            objNewMember.DataHasChanged = DataHasChanged
            objNewMember.TableRowID = TableRowID
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.MMIDateRequiredCode = MMIDateRequiredCode
            objNewMember.MonthsFromInjury = MonthsFromInjury
            objNewMember.ScheduledInjuryCode = ScheduledInjuryCode
            objNewMember.UseTTDRateCode = UseTTDRateCode
            objNewMember.WeeksPayable = WeeksPayable
            mCol.Add(objNewMember, sKey)
            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Private Function ClearObject() As Integer



    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID,EFFECTIVE_DATE,DELETED_FLAG"
        sSQL = sSQL & ",MMI_DATE_REQD_CODE,USE_TTD_RATE_CODE, WEEKS_TO_PAY"
        sSQL = sSQL & ",MONTHS_FROM_INJURY, SCHEDULED_CODE"
        sSQL = sSQL & " FROM " & sTableName
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim objRecord As CJRBenefitRuleNSPPDCO
        Dim sSQL As String
        Try

            LoadData = 0
            ClearObject()

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " ORDER BY EFFECTIVE_DATE"


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRBenefitRuleNSPPDCO
                With objRecord
                    .DataHasChanged = False
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .MMIDateRequiredCode = objReader.GetInt32("MMI_DATE_REQD_CODE")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .UseTTDRateCode = objReader.GetInt32("USE_TTD_RATE_CODE")
                    .WeeksPayable = objReader.GetInt32("WEEKS_TO_PAY")
                    .MonthsFromInjury = objReader.GetInt32("MONTHS_FROM_INJURY")
                    .ScheduledInjuryCode = objReader.GetInt32("SCHEDULED_CODE")
                    Add(.DataHasChanged, .EffectiveDateDTG, .JurisRowID, .MMIDateRequiredCode, .MonthsFromInjury, .ScheduledInjuryCode, .TableRowID, .UseTTDRateCode, .WeeksPayable, "k" & CStr(.TableRowID))
                End With

            End While


            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function


    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleNSPPDCO
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

