Option Strict Off
Option Explicit On
Public Class CJRPenalty
    Private Const sClassName As String = "CJRPenalty"

    'Class properties--local copy
    Private m_TableRowID As Integer
    Private m_JurisRowID As Short
    Private m_DeletedFlag As Short
    Private m_Permitted As Integer
    Private m_EffectiveDateDTG As String
    Private m_EndDateDTG As String
    Private m_FixedAmount As Double 'local copy
    Private m_TriggerEventCode As Integer 'local copy
    Private m_PercentageOfPayment As Double 'local copy
    Private m_PercentageOfTotalBenefit As Double 'local copy
    Private m_SelfImposedCode As Integer
    Private m_AssessedCode As Integer
    Private m_TranactionCode As Integer
    Private m_PayeeCode As Integer
    Private m_EnforcementNone As Integer
    Private m_ReferenceText As String
    Private m_DataHasChanged As Boolean
    Private m_DaysFromTrigger As Integer
    Private m_Warning As String
    '

    Private Function AssignData(ByVal objReader As Riskmaster.Db.DbReader) As Integer
        Const sFunctionName As String = "AssignData"
        Try

            AssignData = 0
            m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
            m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
            m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
            m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
            m_EndDateDTG = Trim(objReader.GetString("END_DATE"))
            m_FixedAmount = objReader.GetDouble("FIXED_AMT")
            m_TriggerEventCode = objReader.GetInt32("TRIG_EVENT_CODE")
            m_PercentageOfPayment = objReader.GetDouble("PERCT_PAYMENT_AMT")
            m_PercentageOfTotalBenefit = objReader.GetDouble("PERCT_BENEFIT_AMT")
            m_SelfImposedCode = objReader.GetInt32("SELFIMPOSED_CODE")
            m_AssessedCode = objReader.GetInt32("ASSESSED_CODE")
            m_TranactionCode = objReader.GetInt32("TRANS_TYPE_CODE")
            m_PayeeCode = objReader.GetInt32("PAYEE_TYPE_CODE")
            m_ReferenceText = Trim(objReader.GetString("REFERENCE_TXT"))
            m_DaysFromTrigger = objReader.GetInt32("DAYS_FROM_TRIGGER")
            m_DataHasChanged = False
            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function Validate() As Integer
        Validate = 0
        m_Warning = ""
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_TranactionCode < 1 Then
            m_Warning = m_Warning & "A Penaly Name is required." & vbCrLf
        End If



    End Function
    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property EnforcementNone() As Integer
        Get
            EnforcementNone = m_EnforcementNone
        End Get
        Set(ByVal Value As Integer)
            m_EnforcementNone = Value
        End Set
    End Property

    Public Property ReferenceText() As String
        Get
            ReferenceText = m_ReferenceText
        End Get
        Set(ByVal Value As String)
            m_ReferenceText = Value
        End Set
    End Property

    Public Property PayeeCode() As Integer
        Get
            PayeeCode = m_PayeeCode
        End Get
        Set(ByVal Value As Integer)
            m_PayeeCode = Value
        End Set
    End Property

    Public Property TranactionCode() As Integer
        Get
            TranactionCode = m_TranactionCode
        End Get
        Set(ByVal Value As Integer)
            m_TranactionCode = Value
        End Set
    End Property

    Public Property SelfImposedCode() As Integer
        Get
            SelfImposedCode = m_SelfImposedCode
        End Get
        Set(ByVal Value As Integer)
            m_SelfImposedCode = Value
        End Set
    End Property
    Public Property AssessedCode() As Integer
        Get
            AssessedCode = m_AssessedCode
        End Get
        Set(ByVal Value As Integer)
            m_AssessedCode = Value
        End Set
    End Property

    Public Property PercentageOfTotalBenefit() As Double
        Get
            PercentageOfTotalBenefit = m_PercentageOfTotalBenefit
        End Get
        Set(ByVal Value As Double)
            m_PercentageOfTotalBenefit = Value
        End Set
    End Property
    Public Property PercentageOfPayment() As Double
        Get
            PercentageOfPayment = m_PercentageOfPayment
        End Get
        Set(ByVal Value As Double)
            m_PercentageOfPayment = Value
        End Set
    End Property
    Public Property TriggerEventCode() As Integer
        Get
            TriggerEventCode = m_TriggerEventCode
        End Get
        Set(ByVal Value As Integer)
            m_TriggerEventCode = Value
        End Set
    End Property
    Public Property FixedAmount() As Double
        Get
            FixedAmount = m_FixedAmount
        End Get
        Set(ByVal Value As Double)
            m_FixedAmount = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property
    Public Property JurisRowID() As Short
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Short)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property DeletedFlag() As Short
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Short)
            m_DeletedFlag = Value
        End Set
    End Property
    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property EndDateDTG() As String
        Get
            EndDateDTG = m_EndDateDTG
        End Get
        Set(ByVal Value As String)
            m_EndDateDTG = Value
        End Set
    End Property
    Public Property Permitted() As Integer
        Get
            Permitted = m_Permitted
        End Get
        Set(ByVal Value As Integer)
            m_Permitted = Value
        End Set
    End Property

    Public Property DaysFromTrigger() As Integer
        Get
            DaysFromTrigger = m_DaysFromTrigger
        End Get
        Set(ByVal Value As Integer)
            m_DaysFromTrigger = Value
        End Set
    End Property


    Public Property Warning() As String
        Get

            Warning = m_Warning

        End Get
        Set(ByVal Value As String)

            m_Warning = Value

        End Set
    End Property

    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try
            SaveData = 0



            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM WCP_PENALTY_RULES"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("TABLE_ROW_ID").Value = m_TableRowID
                objWriter.Fields("ADDED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("DTTM_RCD_ADDED").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = g_objUser.LoginName
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("END_DATE").Value = m_EndDateDTG
                objWriter.Fields("FIXED_AMT").Value = m_FixedAmount
                objWriter.Fields("TRIG_EVENT_CODE").Value = m_TriggerEventCode
                objWriter.Fields("PERCT_PAYMENT_AMT").Value = m_PercentageOfPayment
                objWriter.Fields("PERCT_BENEFIT_AMT").Value = m_PercentageOfTotalBenefit
                objWriter.Fields("SELFIMPOSED_CODE").Value = m_SelfImposedCode
                objWriter.Fields("ASSESSED_CODE").Value = m_AssessedCode
                objWriter.Fields("TRANS_TYPE_CODE").Value = m_TranactionCode
                objWriter.Fields("PAYEE_TYPE_CODE").Value = m_PayeeCode
                objWriter.Fields("REFERENCE_TXT").Value = m_ReferenceText
                If objReader.GetString("ENF_NONE_CODE") Then
                    objWriter.Fields("ENF_NONE_CODE").Value = m_ReferenceText
                End If
                If objReader.GetString("DAYS_FROM_TRIGGER") Then
                    objWriter.Fields("DAYS_FROM_TRIGGER").Value = m_DaysFromTrigger
                End If
                objWriter.Execute()

            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                m_TableRowID = lGetNextUID("WCP_PENALTY_RULES")
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("END_DATE", m_EndDateDTG)
                objWriter.Fields.Add("FIXED_AMT", m_FixedAmount)
                objWriter.Fields.Add("TRIG_EVENT_CODE", m_TriggerEventCode)
                objWriter.Fields.Add("PERCT_PAYMENT_AMT", m_PercentageOfPayment)
                objWriter.Fields.Add("PERCT_BENEFIT_AMT", m_PercentageOfTotalBenefit)
                objWriter.Fields.Add("SELFIMPOSED_CODE", m_SelfImposedCode)
                objWriter.Fields.Add("ASSESSED_CODE", m_AssessedCode)
                objWriter.Fields.Add("TRANS_TYPE_CODE", m_TranactionCode)
                objWriter.Fields.Add("PAYEE_TYPE_CODE", m_PayeeCode)
                objWriter.Fields.Add("REFERENCE_TXT", m_ReferenceText)
                If objReader.GetString("ENF_NONE_CODE") <> "" Then
                    objWriter.Fields.Add("ENF_NONE_CODE", m_ReferenceText)
                End If
                If objReader.GetString("DAYS_FROM_TRIGGER") Then
                    objWriter.Fields.Add("DAYS_FROM_TRIGGER", m_DaysFromTrigger)
                End If
                objWriter.Execute()
            End If



            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            SaveData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            If InStr(1, UCase(g_sErrDescription), UCase("UNIQUE CONSTRAINT")) > 0 Then
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                SaveData = 89999
                Exit Function
            End If
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try
            LoadData = 0
            ClearObject()

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM WCP_PENALTY_RULES"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                AssignData(objReader)
            End If
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Public Function LoadDataBenefit(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String, ByRef sBenefitAbb As String) As Integer
        Const sFunctionName As String = "LoadDataBenefit"
        Dim objReader As DbReader
        Dim sSQL As String
        Try
            LoadDataBenefit = 0
            ClearObject()

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM WCP_PENALTY_RULES"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                AssignData(objReader)
            End If
            LoadDataBenefit = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataBenefit = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function

    Public Function LoadDataLateMedicalPayment(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataLateMedicalPayment"
        Dim objReader As DbReader
        Dim lCodeId As Integer
        Dim lTableID As Integer
        Dim sShortCode As String
        Dim sSQL As String
        Dim sSQL2 As String
        Dim sSQL3 As String

        Try

            LoadDataLateMedicalPayment = 0
            ClearObject()

            lCodeId = 0
            sShortCode = "DMBR"
            lTableID = lGetTableID("JR_TRIGGER_EVENTS")
            sSQL3 = ""
            sSQL3 = sSQL3 & "SELECT CODE_ID"
            sSQL3 = sSQL3 & " FROM CODES"
            sSQL3 = sSQL3 & " WHERE TABLE_ID = " & lGetTableID("JR_TRIGGER_EVENTS")
            sSQL3 = sSQL3 & " AND SHORT_CODE = 'DMBR'"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL3)
            If (objReader.Read()) Then
                lCodeId = objReader.GetInt32("CODE_ID")
            End If

            SafeCloseRecordset(objReader)
            If lCodeId = 0 Then
                SafeCloseRecordset(objReader)

                Exit Function
            End If
            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM WCP_PENALTY_RULES"
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"
            sSQL2 = sSQL2 & " AND TRIG_EVENT_CODE = " & lCodeId

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM WCP_PENALTY_RULES"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"
            sSQL = sSQL & " AND TRIG_EVENT_CODE = " & lCodeId

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                AssignData(objReader)
            End If
            LoadDataLateMedicalPayment = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataLateMedicalPayment = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", EFFECTIVE_DATE,JURIS_ROW_ID"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", END_DATE"
        sSQL = sSQL & ", FIXED_AMT"
        sSQL = sSQL & ", TRIG_EVENT_CODE"
        sSQL = sSQL & ", PERCT_PAYMENT_AMT"
        sSQL = sSQL & ", PERCT_BENEFIT_AMT"
        sSQL = sSQL & ", SELFIMPOSED_CODE"
        sSQL = sSQL & ", ASSESSED_CODE"
        sSQL = sSQL & ", TRANS_TYPE_CODE"
        sSQL = sSQL & ", PAYEE_TYPE_CODE"
        sSQL = sSQL & ", REFERENCE_TXT"
        sSQL = sSQL & ", DAYS_FROM_TRIGGER"
        'sSQL = sSQL & ", ENF_NONE_CODE"
        GetSQLFieldList = sSQL



    End Function

    Private Function ClearObject() As Integer
        m_TableRowID = 0
        m_JurisRowID = 0
        m_DaysFromTrigger = 0
        m_DeletedFlag = -2
        m_EffectiveDateDTG = ""
        m_EndDateDTG = ""
        m_FixedAmount = 0
        m_TriggerEventCode = 0
        m_PercentageOfPayment = 0
        m_PercentageOfTotalBenefit = 0
        m_SelfImposedCode = -1
        m_AssessedCode = -1
        m_TranactionCode = -1
        m_PayeeCode = -1
        m_ReferenceText = ""
        m_EnforcementNone = 0



    End Function
End Class

