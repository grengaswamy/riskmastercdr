Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleTTDAA
    Private Const sClassName As String = "CJRBenefitRuleTTDAA"
    Private Const sTableName As String = "WCP_RULE_TTD"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34822

    Public objJurisWorkWeek As New CJRXJurisWorkWeek

    Dim m_sDateOfEventDTG As String 'jtodd22 not a Class Property

    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EarningsRequiredCode As Integer
    Private m_EarningsPermittedCode As Integer

    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    Private m_DollarForDollar As Integer
    Private m_EndingDateDTG As String
    Private m_FloorAmount As Double

    Private m_LessThanHourlyRate As Double
    Private m_MaxAWW As Double
    Private m_MinCompRate As Double 'jtodd 07/05/2006 not a jurisdictional rule parameter, used as part of rule in RMWCCalc.dll
    Private m_PayFloorAmount As Integer
    Private m_PayConCurrentPPD As Integer

    Private m_PayCurrentRateAfterTwoYears As Integer
    Private m_MaxRatePercentSAWW As Double
    Private m_PrimeRate As Double
    Private m_PrimeRateMaxAmount As Integer
    Private m_PrimeRateMaxWeeks As Integer

    Private m_SecondRate As Double
    Private m_SecondRateMaxAmount As Double
    Private m_SecondRateMaxWeeks As Integer
    Private m_TotalAmount As Double

    Private m_UseSAWWMaximumCode As Integer
    Private m_UseSAWWMinimumCode As Integer
    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Dim lReturn As Integer

        Try

            AssignData = 0
            ClearObject()



            m_YesCodeID = GetYesCodeID()

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_DollarForDollar = objReader.GetInt32("PAY_DOLLAR_CODE")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_EndingDateDTG = objReader.GetString("END_DATE")
                m_FloorAmount = objReader.GetDouble("MIN_BENEFIT")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_LessThanHourlyRate = objReader.GetDouble("LESS_HOURLY_AMT")
                m_MaxAWW = objReader.GetDouble("MAX_AWW")
                m_MaxCompRateWeekly = objReader.GetDouble("MAX_COMP_RATE")
                m_PayFloorAmount = objReader.GetInt32("PAY_FLOOR_CODE")
                m_PayConCurrentPPD = objReader.GetInt32("PAY_CONC_PPD_CODE")    'Jira RMA-1046
                m_PayCurrentRateAfterTwoYears = objReader.GetInt32("USE_TWOYEAR_CODE")
                m_MaxRatePercentSAWW = objReader.GetDouble("MAX_BENEFIT") 'jtodd22 03/08/2005--this is Max Percent of SAWW
                m_PrimeRate = objReader.GetDouble("PRIME_RATE")
                m_PrimeRateMaxWeeks = objReader.GetDouble("PR_RAT_MAX_WEEKS")   'Jira RMA-1046
                m_PrimeRateMaxAmount = objReader.GetDouble("SC_RAT_MAX_WEEKS")  'Jira RMA-1046
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_SecondRate = objReader.GetDouble("SECOND_RATE")
                m_SecondRateMaxWeeks = objReader.GetInt32("SC_RAT_MAX_WEEKS")
                m_SecondRateMaxAmount = objReader.GetDouble("SC_RAT_MAX_BENFIT")
                m_TotalAmount = objReader.GetDouble("TOTAL_AMT")
                m_RuleTotalWeeks = objReader.GetInt32("TOTAL_WEEKS")
                m_UseSAWWMaximumCode = objReader.GetInt32("USE_SAWW_MAX_CODE")
                m_UseSAWWMinimumCode = objReader.GetInt32("USE_SAWW_MIN_CODE")
            End If

            SafeCloseRecordset(objReader)

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            lReturn = Me.objJurisWorkWeek.LoadData(m_JurisRowID, m_EffectiveDateDTG)
            If lReturn = -1 Then
                m_JurisDefinedWorkWeek = Me.objJurisWorkWeek.JurisDefinedWorkWeek
                AssignData = -1
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Private Function GetJurisDefinedWorkWeek(ByRef lJurisRowID As Integer, ByRef sBeginDateDTG As String) As Integer
        Const sFunctionName As String = "GetJurisDefinedWorkWeek"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            GetJurisDefinedWorkWeek = 0
            m_JurisDefinedWorkWeek = 0

            sSQL = ""
            sSQL = sSQL & " SELECT JURIS_WORK_WEEK, EFFECTIVE_DATE"
            sSQL = sSQL & " FROM WCP_BEN_SWCH"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE <= '" & sBeginDateDTG & "'"
            sSQL = sSQL & " ORDER BY EFFECTIVE_DATE DESC"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_JurisDefinedWorkWeek = objReader.GetInt32("JURIS_WORK_WEEK")
            End If
            If m_JurisDefinedWorkWeek = 0 Then m_JurisDefinedWorkWeek = 7
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetJurisDefinedWorkWeek = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : DeleteRecord
    ' DateTime  : 4/18/2006 07:43
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : lReturnCode is a number defining why the record was not deleted
    '---------------------------------------------------------------------------------------
    '
    Public Function DeleteRecord(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sEffectiveDateDBFormat As String, ByRef lReturnCode As Integer) As Integer
        Const sFunctionName As String = "DeleteRecord"

        Try

            DeleteRecord = modFunctions.DeleteRecordTemporary(objUser, lJurisRowID, sEffectiveDateDBFormat, lReturnCode, sTableName)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DeleteRecord = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public Function ValidateData() As Integer

        ValidateData = 0
        m_Warning = ""
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_UseSAWWMaximumCode < 1 Then
            m_Warning = m_Warning & "A 'Use SAWW Max Comp Rate' selection is required." & vbCrLf
        End If
        If m_UseSAWWMinimumCode < 1 Then
            m_Warning = m_Warning & "A 'Use Minimum From SAWW Table' selection is required." & vbCrLf
        End If


    End Function

    Public Property JurisDefinedWorkWeek() As Integer
        Get
            JurisDefinedWorkWeek = m_JurisDefinedWorkWeek
        End Get
        Set(ByVal Value As Integer)
            m_JurisDefinedWorkWeek = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property PayConCurrentPPD() As Integer
        Get
            PayConCurrentPPD = m_PayConCurrentPPD
        End Get
        Set(ByVal Value As Integer)
            m_PayConCurrentPPD = Value
        End Set
    End Property

    Public Property SecondRate() As Double
        Get
            SecondRate = m_SecondRate
        End Get
        Set(ByVal Value As Double)
            m_SecondRate = Value
        End Set
    End Property

    Public Property PrimeRate() As Double
        Get
            PrimeRate = m_PrimeRate
        End Get
        Set(ByVal Value As Double)
            m_PrimeRate = Value
        End Set
    End Property

    Public Property MaxRatePercentSAWW() As Double
        Get
            MaxRatePercentSAWW = m_MaxRatePercentSAWW
        End Get
        Set(ByVal Value As Double)
            m_MaxRatePercentSAWW = Value
        End Set
    End Property

    Public Property MaxAWW() As Double
        Get
            MaxAWW = m_MaxAWW
        End Get
        Set(ByVal Value As Double)
            m_MaxAWW = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property

    Public Property MinCompRate() As Double
        Get
            MinCompRate = m_MinCompRate
        End Get
        Set(ByVal Value As Double)
            m_MinCompRate = Value
        End Set
    End Property

    Public Property DollarForDollar() As Integer
        Get
            DollarForDollar = m_DollarForDollar
        End Get
        Set(ByVal Value As Integer)
            m_DollarForDollar = Value
        End Set
    End Property
    Public Property PayFloorAmount() As Integer
        Get
            PayFloorAmount = m_PayFloorAmount
        End Get
        Set(ByVal Value As Integer)
            m_PayFloorAmount = Value
        End Set
    End Property
    Public Property FloorAmount() As Double
        Get
            FloorAmount = m_FloorAmount
        End Get
        Set(ByVal Value As Double)
            m_FloorAmount = Value
        End Set
    End Property

    Public Property EarningsRequiredCode() As Integer
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property

    Public Property EndingDateDTG() As String
        Get
            EndingDateDTG = m_EndingDateDTG
        End Get
        Set(ByVal Value As String)
            m_EndingDateDTG = Value
        End Set
    End Property
    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property SecondRateMaxAmount() As Double
        Get
            SecondRateMaxAmount = m_SecondRateMaxAmount
        End Get
        Set(ByVal Value As Double)
            m_SecondRateMaxAmount = Value
        End Set
    End Property
    Public Property SecondRateMaxWeeks() As Integer
        Get
            SecondRateMaxWeeks = m_SecondRateMaxWeeks
        End Get
        Set(ByVal Value As Integer)
            m_SecondRateMaxWeeks = Value
        End Set
    End Property
    Public Property PrimeRateMaxAmount() As Integer
        Get
            PrimeRateMaxAmount = m_PrimeRateMaxAmount
        End Get
        Set(ByVal Value As Integer)
            m_PrimeRateMaxAmount = Value
        End Set
    End Property
    Public Property PrimeRateMaxWeeks() As Integer
        Get
            PrimeRateMaxWeeks = m_PrimeRateMaxWeeks
        End Get
        Set(ByVal Value As Integer)
            m_PrimeRateMaxWeeks = Value
        End Set
    End Property

    Public Property TotalAmount() As Double
        Get
            TotalAmount = m_TotalAmount
        End Get
        Set(ByVal Value As Double)
            m_TotalAmount = Value
        End Set
    End Property
    Public Property PayCurrentRateAfterTwoYears() As Integer
        Get
            PayCurrentRateAfterTwoYears = m_PayCurrentRateAfterTwoYears
        End Get
        Set(ByVal Value As Integer)
            m_PayCurrentRateAfterTwoYears = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property UseSAWWMaximumCode() As Integer
        Get
            UseSAWWMaximumCode = m_UseSAWWMaximumCode
        End Get
        Set(ByVal Value As Integer)
            m_UseSAWWMaximumCode = Value
        End Set
    End Property

    Public Property UseSAWWMinimumCode() As Integer
        Get
            UseSAWWMinimumCode = m_UseSAWWMinimumCode
        End Get
        Set(ByVal Value As Integer)
            m_UseSAWWMinimumCode = Value
        End Set
    End Property

    Public Property LessThanHourlyRate() As Double
        Get
            LessThanHourlyRate = m_LessThanHourlyRate
        End Get
        Set(ByVal Value As Double)
            m_LessThanHourlyRate = Value
        End Set
    End Property

    Public Property EarningsPermittedCode() As Integer
        Get
            EarningsPermittedCode = m_EarningsPermittedCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsPermittedCode = Value
        End Set
    End Property

    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadDataByEventDate
    ' DateTime  : 12/22/2004 10:25
    ' Author    : jtodd22
    ' Purpose   : To load data based on the Event Date, not the Rule Effective Date
    ' Note      : The Field "EFFECTIVE_DATE" must be less than or equal the Event Date.
    ' ..........: Use the record with the "EFFECTIVE_DATE" closest to the Event Date
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim lReturn As Integer
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            m_sDateOfEventDTG = sDateOfEventDTG

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            lReturn = AssignData(objUser, sSQL)
            If lReturn <> -1 Then
                g_sErrProcedure = "|" & sClassName & "." & sFunctionName & "|"
                g_sErrDescription = "Assign Data sSQL:  " & sSQL
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                Exit Function
            End If
            Dim objSAWW As CJRSAWW
            If m_UseSAWWMaximumCode = m_YesCodeID Or m_UseSAWWMinimumCode = m_YesCodeID Then
                objSAWW = New CJRSAWW
                lReturn = objSAWW.LoadDataByEventDate(objUser, m_JurisRowID, m_sDateOfEventDTG)
                Select Case lReturn
                    Case -1 'Normal, expected
                        If objSAWW.TableRowID = 0 Then
                            m_ErrorMaskSAWW = modGlobals.DisBenCalcMask.dbcNoJurisdictionalRule
                            'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                            objSAWW = Nothing
                            g_sErrProcedure = "|" & sClassName & "." & sFunctionName & "|"
                            g_sErrDescription = "objSAWW is Nothing"
                            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                            Exit Function
                        End If
                    Case Else 'error
                        'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                        objSAWW = Nothing
                        'error should/will bubble up
                        g_sErrProcedure = "|" & sClassName & "." & sFunctionName & "|"
                        g_sErrDescription = "objSAWW is Nothing"
                        LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                        Exit Function
                End Select
                If m_UseSAWWMaximumCode = m_YesCodeID Then
                    If objSAWW.PercentMax > 0 Then
                        m_MaxCompRateWeekly = objSAWW.SAWWAmount * objSAWW.PercentMax / 100
                    End If
                    If objSAWW.MaxAmountAsPublished > 0 Then
                        m_MaxCompRateWeekly = objSAWW.MaxAmountAsPublished
                    End If
                End If
                If m_UseSAWWMinimumCode = m_YesCodeID Then
                    If objSAWW.PercentMin > 1 Then
                        m_FloorAmount = objSAWW.SAWWAmount * objSAWW.PercentMin / 100
                    Else
                        If objSAWW.PercentMin < 1 Then
                            m_FloorAmount = objSAWW.SAWWAmount * objSAWW.PercentMin
                        End If
                    End If
                    If objSAWW.MinAmountAsPublished > 0 Then
                        m_FloorAmount = objSAWW.MinAmountAsPublished
                    End If
                End If
                'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objSAWW = Nothing

            End If

            LoadDataByEventDate = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataLoadDataByTableRowID"
        Dim sSQL As String
        Try

            LoadDataByTableRowID = 0

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID
            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Private Function ClearObject() As Object
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_FloorAmount = 0
        m_LessThanHourlyRate = 0
        m_MaxAWW = 0
        m_MinCompRate = 0
        m_PayFloorAmount = 0
        m_MaxRatePercentSAWW = 0
        m_PayConCurrentPPD = 0
        m_PayCurrentRateAfterTwoYears = -1
        m_PrimeRate = 0
        m_PrimeRateMaxWeeks = 0
        m_PrimeRateMaxAmount = 0
        m_SecondRate = 0
        m_SecondRateMaxWeeks = 0
        m_SecondRateMaxAmount = 0
        m_TotalAmount = 0
        m_UseSAWWMaximumCode = 0
        m_UseSAWWMinimumCode = 0


    End Function

    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        SaveData = 0
        lTest = 0
        Try


            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                If objWriter.Fields("EFFECTIVE_DATE").Value = "" Then
                    objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                End If
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                If objWriter.Fields("BEGIN_DATE").Value = "" Then
                    objWriter.Fields("BEGIN_DATE").Value = m_EffectiveDateDTG
                End If
                objWriter.Fields("PAY_DOLLAR_CODE").Value = m_DollarForDollar
                objWriter.Fields("END_DATE").Value = m_EndingDateDTG
                objWriter.Fields("MIN_BENEFIT").Value = m_FloorAmount
                objWriter.Fields("LESS_HOURLY_AMT").Value = m_LessThanHourlyRate
                objWriter.Fields("MAX_AWW").Value = m_MaxAWW
                objWriter.Fields("MAX_COMP_RATE").Value = m_MaxCompRateWeekly
                objWriter.Fields("PAY_FLOOR_CODE").Value = m_PayFloorAmount
                objWriter.Fields("MAX_BENEFIT").Value = m_MaxRatePercentSAWW
                objWriter.Fields("USE_TWOYEAR_CODE").Value = m_PayCurrentRateAfterTwoYears
                objWriter.Fields("PRIME_RATE").Value = m_PrimeRate
                objWriter.Fields("PR_RAT_MAX_WEEKS").Value = m_PrimeRateMaxWeeks
                objWriter.Fields("PR_RAT_MAX_BENFIT").Value = m_PrimeRateMaxAmount
                objWriter.Fields("SECOND_RATE").Value = m_SecondRate
                objWriter.Fields("SC_RAT_MAX_WEEKS").Value = m_SecondRateMaxWeeks
                objWriter.Fields("SC_RAT_MAX_BENFIT").Value = m_SecondRateMaxAmount
                objWriter.Fields("TOTAL_AMT").Value = m_TotalAmount
                objWriter.Fields("TOTAL_WEEKS").Value = m_RuleTotalWeeks
                objWriter.Fields("PAY_CONC_PPD_CODE").Value = m_PayConCurrentPPD
                objWriter.Fields("USE_SAWW_MAX_CODE").Value = m_UseSAWWMaximumCode
                objWriter.Fields("USE_SAWW_MIN_CODE").Value = m_UseSAWWMinimumCode
                objWriter.Execute()

            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("BEGIN_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("PAY_DOLLAR_CODE", m_DollarForDollar)
                objWriter.Fields.Add("END_DATE", m_EndingDateDTG)
                objWriter.Fields.Add("MIN_BENEFIT", m_FloorAmount)
                objWriter.Fields.Add("LESS_HOURLY_AMT", m_LessThanHourlyRate)
                objWriter.Fields.Add("MAX_AWW", m_MaxAWW)
                objWriter.Fields.Add("MAX_COMP_RATE", m_MaxCompRateWeekly)
                objWriter.Fields.Add("PAY_FLOOR_CODE", m_PayFloorAmount)
                objWriter.Fields.Add("MAX_BENEFIT", m_MaxRatePercentSAWW)
                objWriter.Fields.Add("USE_TWOYEAR_CODE", m_PayCurrentRateAfterTwoYears)
                objWriter.Fields.Add("PRIME_RATE", m_PrimeRate)
                objWriter.Fields.Add("PR_RAT_MAX_WEEKS", m_PrimeRateMaxWeeks)
                objWriter.Fields.Add("PR_RAT_MAX_BENFIT", m_PrimeRateMaxAmount)
                objWriter.Fields.Add("SECOND_RATE", m_SecondRate)
                objWriter.Fields.Add("SC_RAT_MAX_WEEKS", m_SecondRateMaxWeeks)
                objWriter.Fields.Add("SC_RAT_MAX_BENFIT", m_SecondRateMaxAmount)
                objWriter.Fields.Add("TOTAL_AMT", m_TotalAmount)
                objWriter.Fields.Add("TOTAL_WEEKS", m_RuleTotalWeeks)
                objWriter.Fields.Add("PAY_CONC_PPD_CODE", m_PayConCurrentPPD)
                objWriter.Fields.Add("USE_SAWW_MAX_CODE", m_UseSAWWMaximumCode)
                objWriter.Fields.Add("USE_SAWW_MIN_CODE", m_UseSAWWMinimumCode)
                objWriter.Execute()

            End If


            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try


    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        'jtodd22 12/22/2004--sSQL = sSQL & ", BEGIN_DATE"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", END_DATE"
        sSQL = sSQL & ", MAX_AWW"
        sSQL = sSQL & ", MAX_BENEFIT" 'jtodd22 03/08/2005--this is Max Percent of SAWW
        sSQL = sSQL & ", MAX_COMP_RATE" 'jtodd22 03/08/2005--this is the Dollars per week
        sSQL = sSQL & ", MIN_BENEFIT"
        sSQL = sSQL & ", PAY_CONC_PPD_CODE"
        sSQL = sSQL & ", PAY_DOLLAR_CODE"
        sSQL = sSQL & ", PAY_FLOOR_CODE"
        sSQL = sSQL & ", PRIME_RATE"
        sSQL = sSQL & ", PR_RAT_MAX_BENFIT"
        sSQL = sSQL & ", PR_RAT_MAX_WEEKS"
        sSQL = sSQL & ", SECOND_RATE"
        sSQL = sSQL & ", SC_RAT_MAX_BENFIT"
        sSQL = sSQL & ", SC_RAT_MAX_WEEKS"
        sSQL = sSQL & ", TOTAL_AMT"
        sSQL = sSQL & ", TOTAL_WEEKS"
        sSQL = sSQL & ", USE_SAWW_MAX_CODE"
        sSQL = sSQL & ", USE_SAWW_MIN_CODE"
        sSQL = sSQL & ", USE_TWOYEAR_CODE"
        sSQL = sSQL & ", LESS_HOURLY_AMT"
        GetSQLFieldList = sSQL


    End Function

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        If Not Me.objJurisWorkWeek Is Nothing Then
            'UPGRADE_NOTE: Object Me.objJurisWorkWeek may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            Me.objJurisWorkWeek = Nothing
        End If



    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

