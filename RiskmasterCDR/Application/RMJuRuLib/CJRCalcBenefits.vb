Option Strict Off
Option Explicit On
Public Class CJRCalcBenefits
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRCalcBenefits"
    Private Const sBaseDatabaseTableName As String = "WCP_BEN_SWCH"

    Private mCol As Collection
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 8/17/2004 11:32
    ' Author    : jtodd22
    ' Purpose   : To fetch a collection of T(emporary) T(otal) D(isability) data
    ' Note      : Beware that this triggers a total of three different data fetches per row
    '...........: 1) that populates cTableRowIDs
    '...........: 2) that uses cTableRowIDs to populate objRecord(CJRCalcBenefit)
    '...........: 3) the fetch within objRecord(CJRCalcBenefit) for cOffsets.DLLFetchParentTableRowID
    ' Note      : jtodd22 12/01/2007
    '...........: Added trap for database with duplicate records
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim cTableRowIDs As Collection
        Dim objRecord As CJRCalcBenefit

        Dim i As Short
        Dim objReader As DbReader
        Dim lCurrentTableRowID As Integer
        Dim lOldTableRowID As Integer
        Dim sSQL As String
        Dim sText As String
        Try

            LoadData = 0



            sSQL = GetBaseSQL()

            cTableRowIDs = New Collection
            i = 0
            lOldTableRowID = 0
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                i = i + 1
                lCurrentTableRowID = objReader.GetInt32("TABLE_ROW_ID")

                If lOldTableRowID = lCurrentTableRowID Then
                    'jtodd22 12/01/2007 this is fatal to the application
                    'as duplicates will cause an error.
                    'UPGRADE_NOTE: Object cTableRowIDs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    cTableRowIDs = Nothing
                    SafeCloseRecordset(objReader)

                    sText = ""
                    sText = sText & "There are duplicate records in the database table " & sBaseDatabaseTableName & "."
                    sText = sText & "The table's Primary Key is missing or failed."
                    g_sErrDescription = sText
                    g_sErrProcedure = g_sErrSrc & "|" & "RMJuRuLib." & sClassName & ".LoadData|"
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                    Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
                End If

                lOldTableRowID = lCurrentTableRowID
                cTableRowIDs.Add(lCurrentTableRowID, "k" & i)

            End While
            'jlt 07/06/2004 be sure to drop the Recordset
            SafeCloseRecordset(objReader)
            For i = 1 To cTableRowIDs.Count()
                objRecord = New CJRCalcBenefit
                'UPGRADE_WARNING: Couldn't resolve default property of object cTableRowIDs.Item(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objRecord.DLLFetchDataByTableRowID(cTableRowIDs.Item(i)) = -1 Then


                    With objRecord
                        Add(.OSAnyWageContinuationPlanCode, .OSSocialSecurityDisabilityCode, .OSSocialSecurityRetirementCode, .OSUnemploymentCompensationCode, .OSAnyPensionCode, .OSEmployerFundedBenefitCode, .OSEmployerSicknessAndAccidentCode, .OSEmployerDisabilityPlanCode, .OSEmployerProfitSharingCode, .OSEmployerFundedPensionCode, .OSSecondInjurySpecialFundCode, .DaysToPaymentDue, .RetroActiveDays, .WaitDaysIfHospitalized, .WaitDays, .DOINotPaidAsWaitDayCode, .PPDPayPeriodCode, .TTDPayPeriodCode, .EffectiveDateDTG, .JurisRowID, .TableRowID, .DeletedFlag, .RetroDaysFromCode, .PPDMultiplier, .TTDMultiplier01, .TTDMultiplier02, .ConWaitingDaysCode, .CountFromCode, .JurisWorkWeek, .TTDMaxAmount, .DaysToPaymentFromDateCode, "k" & .TableRowID)

                    End With
                End If
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing
            Next i
            'UPGRADE_NOTE: Object cTableRowIDs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            cTableRowIDs = Nothing
            LoadData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & "  " & Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            'If g_DBObject.DB_EnumRecordsets > "" Then Stop

        End Try

    End Function
    Public Function Add(ByRef OSAnyWageContinuationPlanCode As Integer, ByRef OSSocialSecurityDisabilityCode As Integer, ByRef OSSocialSecurityRetirementCode As Integer, ByRef OSUnemploymentCompensationCode As Integer, ByRef OSAnyPensionCode As Integer, ByRef OSEmployerFundedBenefitCode As Integer, ByRef OSEmployerSicknessAndAccidentCode As Integer, ByRef OSEmployerDisabilityPlanCode As Integer, ByRef OSEmployerProfitSharingCode As Integer, ByRef OSEmployerFundedPensionCode As Integer, ByRef OSSecondInjurySpecialFundCode As Integer, ByRef DaysToPaymentDue As Integer, ByRef RetroActiveDays As Integer, ByRef WaitDaysIfHospitalized As Integer, ByRef WaitDays As Integer, ByRef DOINotPaidAsWaitDayCode As Integer, ByRef PPDPayPeriodCode As Integer, ByRef TTDPayPeriodCode As Integer, ByRef EffectiveDateDTG As String, ByRef JurisRowID As Integer, ByRef TableRowID As Integer, ByRef DeletedFlag As Integer, ByRef RetroDaysFromCode As Integer, ByRef PPDMultiplier As Double, ByRef TTDMultiplier1 As Double, ByRef TTDMultiplier2 As Double, ByRef ConWaitingDaysCode As Integer, ByRef CountFromCode As Integer, ByRef JurisWorkWeek As Integer, ByRef TTDMaxAmount As Double, ByRef DaysToPaymentFromDateCode As Integer, Optional ByRef sKey As String = "") As CJRCalcBenefit
        'create a new object

        Dim objNewMember As CJRCalcBenefit
        Try
            objNewMember = New CJRCalcBenefit


            'set the properties passed into the method
            objNewMember.DaysToPaymentDue = DaysToPaymentDue
            objNewMember.DaysToPaymentFromDateCode = DaysToPaymentFromDateCode
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.DOINotPaidAsWaitDayCode = DOINotPaidAsWaitDayCode
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.JurisRowID = JurisRowID
            objNewMember.OSAnyPensionCode = OSAnyPensionCode
            objNewMember.OSAnyWageContinuationPlanCode = OSAnyWageContinuationPlanCode
            objNewMember.OSEmployerDisabilityPlanCode = OSEmployerDisabilityPlanCode
            objNewMember.OSEmployerFundedBenefitCode = OSEmployerFundedBenefitCode
            objNewMember.OSEmployerFundedPensionCode = OSEmployerFundedPensionCode
            objNewMember.OSEmployerProfitSharingCode = OSEmployerProfitSharingCode
            objNewMember.OSEmployerSicknessAndAccidentCode = OSEmployerSicknessAndAccidentCode
            objNewMember.OSSecondInjurySpecialFundCode = OSSecondInjurySpecialFundCode
            objNewMember.OSSocialSecurityDisabilityCode = OSSocialSecurityDisabilityCode
            objNewMember.OSSocialSecurityRetirementCode = OSSocialSecurityRetirementCode
            objNewMember.OSUnemploymentCompensationCode = OSUnemploymentCompensationCode
            objNewMember.PPDPayPeriodCode = PPDPayPeriodCode
            objNewMember.TTDPayPeriodCode = TTDPayPeriodCode
            objNewMember.RetroActiveDays = RetroActiveDays
            objNewMember.TableRowID = TableRowID
            objNewMember.WaitDays = WaitDays
            objNewMember.WaitDaysIfHospitalized = WaitDaysIfHospitalized
            objNewMember.RetroDaysFromCode = RetroDaysFromCode
            objNewMember.PPDMultiplier = PPDMultiplier
            objNewMember.TTDMultiplier01 = TTDMultiplier1
            objNewMember.TTDMultiplier02 = TTDMultiplier2
            objNewMember.ConWaitingDaysCode = ConWaitingDaysCode
            objNewMember.CountFromCode = CountFromCode
            objNewMember.JurisWorkWeek = JurisWorkWeek
            objNewMember.TTDMaxAmount = TTDMaxAmount
            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRCalcBenefit
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", ADDED_BY_USER,DTTM_RCD_ADDED"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", BT_WAIT_DAYS"
        sSQL = sSQL & ", BT_WAIT_HOS_DAYS"
        sSQL = sSQL & ", BT_RETRO_DAYS"
        sSQL = sSQL & ", BT_FIRST_PAY_DUE"
        sSQL = sSQL & ", JURIS_WORK_WEEK"
        sSQL = sSQL & " FROM WCP_BEN_SWCH"
        GetBaseSQL = sSQL


    End Function
End Class

