Option Strict Off
Option Explicit On
Public Class CJRCode
    Private Const sClassName As String = "CJRCode"

    Private m_CodeID As Integer
    Private m_CodeDesc As String
    Private m_CodeShortCode As String
    Private m_DataHasChanged As Boolean

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property
    Public Property CodeShortCode() As String
        Get
            CodeShortCode = m_CodeShortCode
        End Get
        Set(ByVal Value As String)
            m_CodeShortCode = Value
        End Set
    End Property
    Public Property CodeDesc() As String
        Get
            CodeDesc = m_CodeDesc
        End Get
        Set(ByVal Value As String)
            m_CodeDesc = Value
        End Set
    End Property
    Public Property CodeID() As Integer
        Get
            CodeID = m_CodeID
        End Get
        Set(ByVal Value As Integer)
            m_CodeID = Value
        End Set
    End Property

    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try
            LoadData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC"
            sSQL = sSQL & " FROM CODES, CODE_TEXT,GLOSSARY_TEXT"
            sSQL = sSQL & " WHERE TABLE_NAME = 'Pay Type Codes'"
            sSQL = sSQL & " AND GLOSSARY_TEXT.TABLE_ID = CODES.TABLE_ID"
            sSQL = sSQL & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID"
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
End Class

