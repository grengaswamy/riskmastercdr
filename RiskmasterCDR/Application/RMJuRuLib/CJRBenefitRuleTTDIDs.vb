Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleTTDIDs
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRBenefitRuleTTDIDs"
    Private Const sTableName As String = "WCP_RULE_TTD_ID"

    'Class properties, local copy
    Private mCol As Collection
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        'jtodd22 12/22/2004--sSQL = sSQL & ", BEGIN_DATE"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", END_DATE"
        sSQL = sSQL & ", MAX_BENEFIT" 'jtodd22 03/08/2005--this is Max Percent of SAWW
        sSQL = sSQL & ", MAX_COMP_RATE" 'jtodd22 03/08/2005--this is the Dollars per week
        sSQL = sSQL & ", MIN_BENEFIT"
        sSQL = sSQL & ", PAY_CONC_PPD_CODE"
        sSQL = sSQL & ", PAY_DOLLAR_CODE"
        sSQL = sSQL & ", PAY_FLOOR_CODE"
        sSQL = sSQL & ", HIGH_RATE"
        sSQL = sSQL & ", LOW_RATE"
        sSQL = sSQL & ", TOTAL_AMT"
        sSQL = sSQL & ", TOTAL_WEEKS"
        sSQL = sSQL & ", USE_SAWW_MAX_CODE"
        sSQL = sSQL & ", USE_SAWW_MIN_CODE"
        sSQL = sSQL & ", CLAIMANT_AWW_AMT"
        sSQL = sSQL & ", PAY_FIXED_AMT"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sBeginDateDTG As String) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objTTDRule As CJRBenefitRuleTTDID
        Try

            LoadData = 0
            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            If sBeginDateDTG > "" Then
                sSQL = sSQL & " AND EFFECTIVE_DATE = '" & sBeginDateDTG & "'"
            End If
            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	While objReader.Read()
            '		objTTDRule = New CJRBenefitRuleTTDID
            '		With objTTDRule
            '			.ClaimantsAWWPoint = objReader.GetDouble( "CLAIMANT_AWW_AMT")
            '			.DeletedFlag = objReader.GetInt32( "DELETED_FLAG")
            '			.DollarForDollar = objReader.GetInt32( "PAY_DOLLAR_CODE")
            '			.EffectiveDateDTG = objReader.GetString( "EFFECTIVE_DATE")
            '			.EndingDateDTG = objReader.GetString( "END_DATE")
            '			.FloorAmount = objReader.GetDouble( "MIN_BENEFIT")
            '			.JurisRowID = objReader.GetInt32( "JURIS_ROW_ID")
            '			.MaxCompRateWeekly = objReader.GetDouble( "MAX_COMP_RATE")
            '			.PayFixedAmount = objReader.GetDouble( "PAY_FIXED_AMT")
            '			.PayFloorAmount = objReader.GetInt32( "PAY_FLOOR_CODE")
            '			.PayConCurrentPPD = objReader.GetDouble( "PAY_CONC_PPD_CODE")
            '			.MaxRatePercentSAWW = objReader.GetDouble( "MAX_BENEFIT")
            '			.HighRate = objReader.GetDouble( "HIGH_RATE")
            '			.TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            '			.LowRate = objReader.GetDouble( "LOW_RATE")
            '			.TotalAmount = objReader.GetDouble( "TOTAL_AMT")
            '			.RuleTotalWeeks = objReader.GetInt32( "TOTAL_WEEKS")
            '			.UseSAWWMaximumCode = objReader.GetInt32( "USE_SAWW_MAX_CODE")
            '			.UseSAWWMinimumCode = objReader.GetInt32( "USE_SAWW_MIN_CODE")
            '			Add(.ClaimantsAWWPoint, .DeletedFlag, .DollarForDollar, .EffectiveDateDTG, .EndingDateDTG, .FloorAmount, .JurisDefinedWorkWeek, .JurisRowID, .MaxCompRateWeekly, .PayConCurrentPPD, .PayFixedAmount, .PayFloorAmount, .MaxRatePercentSAWW, .HighRate, .LowRate, .TableRowID, .TotalAmount, .RuleTotalWeeks, .UseSAWWMaximumCode, .UseSAWWMinimumCode, "k" & .TableRowID)
            '		End With
            '		'UPGRADE_NOTE: Object objTTDRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            '		objTTDRule = Nothing
            '		
            '	End While
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objTTDRule = New CJRBenefitRuleTTDID
                With objTTDRule
                    .ClaimantsAWWPoint = objReader.GetDouble("CLAIMANT_AWW_AMT")
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .DollarForDollar = objReader.GetInt32("PAY_DOLLAR_CODE")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .EndingDateDTG = objReader.GetString("END_DATE")
                    .FloorAmount = objReader.GetDouble("MIN_BENEFIT")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .MaxCompRateWeekly = objReader.GetDouble("MAX_COMP_RATE")
                    .PayFixedAmount = objReader.GetDouble("PAY_FIXED_AMT")
                    .PayFloorAmount = objReader.GetInt32("PAY_FLOOR_CODE")
                    .PayConCurrentPPD = objReader.GetDouble("PAY_CONC_PPD_CODE")
                    .MaxRatePercentSAWW = objReader.GetDouble("MAX_BENEFIT")
                    .HighRate = objReader.GetDouble("HIGH_RATE")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .LowRate = objReader.GetDouble("LOW_RATE")
                    .TotalAmount = objReader.GetDouble("TOTAL_AMT")
                    .RuleTotalWeeks = objReader.GetInt32("TOTAL_WEEKS")
                    .UseSAWWMaximumCode = objReader.GetInt32("USE_SAWW_MAX_CODE")
                    .UseSAWWMinimumCode = objReader.GetInt32("USE_SAWW_MIN_CODE")
                    Add(.ClaimantsAWWPoint, .DeletedFlag, .DollarForDollar, .EffectiveDateDTG, .EndingDateDTG, .FloorAmount, .JurisDefinedWorkWeek, .JurisRowID, .MaxCompRateWeekly, .PayConCurrentPPD, .PayFixedAmount, .PayFloorAmount, .MaxRatePercentSAWW, .HighRate, .LowRate, .TableRowID, .TotalAmount, .RuleTotalWeeks, .UseSAWWMaximumCode, .UseSAWWMinimumCode, "k" & .TableRowID)
                End With
                'UPGRADE_NOTE: Object objTTDRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objTTDRule = Nothing
            End While


            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function Add(ByRef ClaimantsAWWPoint As Double, ByRef DeletedFlag As Integer, ByRef DollarForDollar As Integer, ByRef EffectiveDateDTG As String, ByRef EndingDateDTG As String, ByRef FloorAmount As Double, ByRef JurisDefinedWorkWeek As Integer, ByRef JurisRowID As Integer, ByRef MaxCompRateWeekly As Double, ByRef PayConCurrentPPD As Integer, ByRef PayFixedAmount As Double, ByRef PayFloorAmount As Integer, ByRef MaxPercentSAWW As Double, ByRef HighRate As Double, ByRef LowRate As Double, ByRef TableRowID As Integer, ByRef TotalAmount As Double, ByRef RuleTotalWeeks As Integer, ByRef UseSAWWMaximumCode As Integer, ByRef UseSAWWMinimumCode As Integer, ByRef sKey As String) As CJRBenefitRuleTTDID

        Try
            'create a new object
            Dim objNewMember As CJRBenefitRuleTTDID
            objNewMember = New CJRBenefitRuleTTDID

            'set the properties passed into the method
            objNewMember.ClaimantsAWWPoint = ClaimantsAWWPoint
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.DollarForDollar = DollarForDollar
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.EndingDateDTG = EndingDateDTG
            objNewMember.FloorAmount = FloorAmount
            objNewMember.JurisDefinedWorkWeek = JurisDefinedWorkWeek
            objNewMember.JurisRowID = JurisRowID
            objNewMember.MaxCompRateWeekly = MaxCompRateWeekly
            objNewMember.PayConCurrentPPD = PayConCurrentPPD
            objNewMember.PayFixedAmount = PayFixedAmount
            objNewMember.PayFloorAmount = PayFloorAmount
            objNewMember.MaxRatePercentSAWW = MaxPercentSAWW
            objNewMember.HighRate = HighRate
            objNewMember.LowRate = LowRate
            objNewMember.TableRowID = TableRowID
            objNewMember.TotalAmount = TotalAmount
            objNewMember.RuleTotalWeeks = RuleTotalWeeks
            objNewMember.UseSAWWMaximumCode = UseSAWWMaximumCode
            objNewMember.UseSAWWMinimumCode = UseSAWWMinimumCode
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleTTDID
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

