Option Strict Off
Option Explicit On
Public Class CJRCodes
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRCodes"

    Private mCol As Collection
    '---------------------------------------------------------------------------------------
    ' Procedure : AssignData
    ' DateTime  : 3/24/2006 08:14
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : Do not make/destroy database connections here.
    ' ..........: This is called from two different places.
    '---------------------------------------------------------------------------------------
    '
    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sTableName As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Dim objCode As CJRCode
        Dim sSQL As String
        Try

            AssignData = 0

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC"
            sSQL = sSQL & " FROM CODES, CODES_TEXT,GLOSSARY"
            sSQL = sSQL & " WHERE SYSTEM_TABLE_NAME = '" & sTableName & "'"
            sSQL = sSQL & " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID"
            sSQL = sSQL & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID"
            sSQL = sSQL & " AND CODES.CODE_ID > 0"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objCode = New CJRCode
                With objCode
                    .CodeDesc = Trim(objReader.GetString("CODE_DESC"))
                    .CodeID = objReader.GetInt32("CODE_ID")
                    .CodeShortCode = Trim(objReader.GetString("SHORT_CODE"))
                    Add(.CodeID, .CodeDesc, .CodeShortCode, "k" & CStr(.CodeID))
                End With
                'UPGRADE_NOTE: Object objCode may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objCode = Nothing

            End While

            Add(0, " ", " ", "k0")
            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByVal sTableName As String) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim objCode As CJRCode
        Dim sSQL As String
        Try

            LoadData = 0



            LoadData = AssignData(objUser, sTableName)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataNoConnInFun(ByRef objUser As Riskmaster.Security.UserLogin, ByVal sTableName As String) As Integer
        Const sFunctionName As String = "LoadDataNoConnInFun"
        Dim objReader As DbReader
        Dim objCode As CJRCode
        Dim sSQL As String
        Try

            LoadDataNoConnInFun = AssignData(objUser, sTableName)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataNoConnInFun = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function Add(ByRef CodeID As Integer, ByRef CodeDesc As String, ByRef CodeShortCode As String, ByRef sKey As String) As CJRCode
        'create a new object
        Dim objNewMember As CJRCode
        objNewMember = New CJRCode

        'set the properties passed into the method
        objNewMember.CodeID = CodeID
        objNewMember.CodeDesc = CodeDesc
        objNewMember.CodeShortCode = CodeShortCode
        mCol.Add(objNewMember, sKey)

        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing




    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRCode
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub

    Public Function GetCodeId(ByRef sShortCode As String) As Integer
        Dim i As Short
        GetCodeId = 0
        For i = 1 To mCol.Count()
            If Item(i).CodeShortCode = sShortCode Then
                GetCodeId = Item(i).CodeID
                Exit For
            End If
        Next


    End Function

    Public Function GetCodeDesc(ByRef sShortCode As String) As String
        Dim i As Short
        GetCodeDesc = ""
        For i = 1 To mCol.Count()
            If Item(i).CodeShortCode = sShortCode Then
                GetCodeDesc = Item(i).CodeDesc
                Exit For
            End If
        Next


    End Function

    Public Function GetCodeShortDescFromID(ByRef lCodeId As Integer) As String
        Dim i As Short
        Dim sDescription As String

        GetCodeShortDescFromID = ""

        For i = 1 To mCol.Count()
            If Item(i).CodeID = lCodeId Then
                sDescription = Item(i).CodeShortCode
                sDescription = sDescription & " - "
                sDescription = sDescription & Item(i).CodeDesc
                GetCodeShortDescFromID = sDescription
                Exit For
            End If
        Next



    End Function

    Public Function GetShortCodeFromID(ByRef lCodeId As Integer) As String
        Dim i As Short
        Dim sShortCode As String

        GetShortCodeFromID = ""

        For i = 1 To mCol.Count()
            If Item(i).CodeID = lCodeId Then
                sShortCode = Item(i).CodeShortCode
                GetShortCodeFromID = sShortCode
                Exit For
            End If
        Next



    End Function
End Class

