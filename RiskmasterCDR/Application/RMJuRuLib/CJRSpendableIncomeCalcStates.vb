Option Strict Off
Option Explicit On
Public Class CJRSpendableIncomeCalcStates
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRSpendableIncomeCalcStates"
    Const sTableName As String = "WCP_WITHHOL_JU"
    'Class properties, local copy
    Private mCol As Collection
    Private Function GetSQLFieldSelect() As String
        Dim sSQL As String
        GetSQLFieldSelect = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",DELETED_FLAG"
        sSQL = sSQL & ",JU_TAX_STTUS_CODE"
        sSQL = sSQL & ",JU_EXEMP_VALUE_AMT"
        sSQL = sSQL & ",STE_MIN_AMT"
        sSQL = sSQL & ",STE_MAX_AMT"
        sSQL = sSQL & ",STE_OFFSET_AMT"
        sSQL = sSQL & ",STE_MULTIPLER"
        sSQL = sSQL & ",STE_PERCENT_RATE"
        GetSQLFieldSelect = sSQL


    End Function
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJRSpendableIncomeCalcState
        Try

            LoadData = 0



            sSQL = GetSQLFieldSelect()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " ORDER BY EFFECTIVE_DATE"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRSpendableIncomeCalcState
                With objRecord
                    .DataHasChanged = False
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .JurisExemptionValueAmount = objReader.GetDouble("JU_EXEMP_VALUE_AMT")
                    .StateFlatPercentRate = objReader.GetDouble("STE_PERCENT_RATE")
                    .StateMaxAmount = objReader.GetDouble("STE_MAX_AMT")
                    .StateMinAmount = objReader.GetDouble("STE_MIN_AMT")
                    .StateMultipler = objReader.GetDouble("STE_MULTIPLER")
                    .StateOffsetAmount = objReader.GetDouble("STE_OFFSET_AMT")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .TaxStatusCode = objReader.GetInt32("JU_TAX_STTUS_CODE")
                    Add(.DataHasChanged, .DeletedFlag, .EffectiveDateDTG, .JurisExemptionValueAmount, .StateFlatPercentRate, .StateMaxAmount, .StateMinAmount, .StateMultipler, .StateOffsetAmount, .JurisRowID, .TableRowID, .TaxStatusCode, "k" & CStr(.TableRowID))
                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function


    Public Function Add(ByRef DataHasChanged As Boolean, ByRef DeletedFlag As Short, ByRef EffectiveDateDTG As String, ByRef JurisExemptionValueAmount As Double, ByRef StateFlatPercentRate As Double, ByRef StateMaxAmount As Double, ByRef StateMinAmount As Double, ByRef StateMultipler As Double, ByRef StateOffsetAmount As Double, ByRef JurisRowID As Short, ByRef TableRowID As Integer, ByRef TaxStatusCode As Integer, ByRef sKey As String) As CJRSpendableIncomeCalcState
        'create a new object
        Dim objNewMember As CJRSpendableIncomeCalcState
        objNewMember = New CJRSpendableIncomeCalcState


        'set the properties passed into the method
        objNewMember.DataHasChanged = DataHasChanged
        objNewMember.DeletedFlag = DeletedFlag
        objNewMember.EffectiveDateDTG = EffectiveDateDTG
        objNewMember.JurisExemptionValueAmount = JurisExemptionValueAmount
        objNewMember.StateFlatPercentRate = StateFlatPercentRate
        objNewMember.StateMaxAmount = StateMaxAmount
        objNewMember.StateMinAmount = StateMinAmount
        objNewMember.StateMultipler = StateMultipler
        objNewMember.StateOffsetAmount = StateOffsetAmount
        objNewMember.JurisRowID = JurisRowID
        objNewMember.TableRowID = TableRowID
        objNewMember.TaxStatusCode = TaxStatusCode

        mCol.Add(objNewMember, sKey)

        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing




    End Function
    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRSpendableIncomeCalcState
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

