Option Strict Off
Option Explicit On
Public Class CJRJurisdiction
    Private Const sClassName As String = "CJRJurisdiction"

    'Class properties, local copy
    Private m_JurisRowID As Integer
    Private m_JurisdictionShortName As String
    Private m_JurisdictionLongName As String
    Private m_DataHasChanged As Boolean

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property JurisdictionLongName() As String
        Get
            JurisdictionLongName = m_JurisdictionLongName
        End Get
        Set(ByVal Value As String)
            m_JurisdictionLongName = Value
        End Set
    End Property
    Public Property JurisdictionShortName() As String
        Get
            JurisdictionShortName = m_JurisdictionShortName
        End Get
        Set(ByVal Value As String)
            m_JurisdictionShortName = Value
        End Set
    End Property
    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property
End Class

