Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleDeaths
    Implements System.Collections.IEnumerable

    Const sClassName As String = "CJRBenefitRuleDeaths"
    Const sTableName As String = "WCP_DETHB_RULE"

    'local variable to hold collection
    Private mCol As Collection
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & "TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID,EFFECTIVE_DATE,DELETED_FLAG"
        sSQL = sSQL & ",FIXED_AMOUNT,MAX_AMOUNT,PAY_FIX_AMT_CODE,PAY_VAR_AMT_CODE"
        sSQL = sSQL & ",TIMES_CLMANT_AWW,TIMES_SAWW"
        sSQL = sSQL & ",PAY_TO_ESTATE_CODE"
        sSQL = sSQL & ",PAY_TO_PERSON_CODE"
        sSQL = sSQL & ",FIXED_AMOUNT_DOWN"
        sSQL = sSQL & ",MAX_AMOUNT_DOWN"
        sSQL = sSQL & ",PAY_TRANS_AMT_CODE"
        sSQL = sSQL & ",MAX_AMOUNT_TRANS"
        sSQL = sSQL & " FROM WCP_DETHB_RULE"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim objRecord As CJRBenefitRuleDeath
        Dim sSQL As String
        Try

            LoadData = 0


            sSQL = GetSQLFieldList()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRBenefitRuleDeath
                With objRecord
                    .DataHasChanged = False
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .FixedAmount = objReader.GetDouble("FIXED_AMOUNT")
                    .FixedAmountDownState = objReader.GetDouble("FIXED_AMOUNT_DOWN")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .MaxAmount = objReader.GetDouble("MAX_AMOUNT")
                    .MaxAmountDownState = objReader.GetDouble("MAX_AMOUNT_DOWN")
                    .MaxTransportAmount = objReader.GetDouble("MAX_AMOUNT_TRANS")
                    .PayFixedAmountCode = objReader.GetInt32("PAY_FIX_AMT_CODE")
                    .PayToEstateCode = objReader.GetInt32("PAY_TO_ESTATE_CODE")
                    .PayToPersonCode = objReader.GetInt32("PAY_TO_PERSON_CODE")
                    .PayTransportationCode = objReader.GetInt32("PAY_TRANS_AMT_CODE")
                    .PayVariableAmountCode = objReader.GetInt32("PAY_VAR_AMT_CODE")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .TimesClaimantAWW = objReader.GetInt32("TIMES_CLMANT_AWW")
                    .TimesSAWW = objReader.GetInt32("TIMES_SAWW")
                    Add(.DataHasChanged, .DeletedFlag, .EffectiveDateDTG, .FixedAmount, .FixedAmountDownState, .JurisRowID, .MaxAmount, .MaxAmountDownState, .MaxTransportAmount, .PayFixedAmountCode, .PayToEstateCode, .PayToPersonCode, .PayTransportationCode, .PayVariableAmountCode, .TableRowID, .TimesClaimantAWW, .TimesSAWW, "k" & CStr(.TableRowID))
                End With

            End While


            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function Add(ByRef DataHasChanged As Boolean, ByRef DeletedFlag As Short, ByRef EffectiveDateDTG As String, ByRef FixedAmount As Double, ByRef FixedAmountDownState As Double, ByRef JurisRowID As Short, ByRef MaxAmount As Double, ByRef MaxAmountDownState As Double, ByRef MaxTransportAmount As Double, ByRef PayFixedAmountCode As Integer, ByRef PayToEstateCode As Integer, ByRef PayToPersonCode As Integer, ByRef PayVariableAmountCode As Integer, ByRef PayTransportationCode As Integer, ByRef TableRowID As Integer, ByRef TimesClaimantAWW As Integer, ByRef TimesSAWW As Integer, ByRef sKey As String) As CJRBenefitRuleDeath
        Const sFunctionName As String = "Add"
        'create a new object
        Dim objNewMember As CJRBenefitRuleDeath
        Try
            objNewMember = New CJRBenefitRuleDeath

            'set the properties passed into the method
            objNewMember.DataHasChanged = DataHasChanged
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.FixedAmount = FixedAmount
            objNewMember.FixedAmountDownState = FixedAmountDownState
            objNewMember.JurisRowID = JurisRowID
            objNewMember.MaxAmount = MaxAmount
            objNewMember.MaxAmountDownState = MaxAmountDownState
            objNewMember.MaxTransportAmount = MaxTransportAmount
            objNewMember.PayFixedAmountCode = PayFixedAmountCode
            objNewMember.PayToEstateCode = PayToEstateCode
            objNewMember.PayToPersonCode = PayToPersonCode
            objNewMember.PayTransportationCode = PayTransportationCode
            objNewMember.PayVariableAmountCode = PayVariableAmountCode
            objNewMember.TableRowID = TableRowID
            objNewMember.TimesClaimantAWW = TimesClaimantAWW
            objNewMember.TimesSAWW = TimesSAWW
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleDeath
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function

    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

