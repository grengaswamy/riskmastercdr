Option Strict Off
Option Explicit On
Public Class CJRDeleteReturn
    Private Const sClassName As String = "CJRDeleteReturn"

    Public cAffectedEvents As Integer
    Public cMostCurrentRule As Integer
    Public cOnlyRule As Integer
    Public cOnlyRuleBeforeEvent As Integer
    Public cSuccess As Integer

    Public Function SetValues() As Integer
        cSuccess = 0
        cMostCurrentRule = 1
        cOnlyRule = 2
        cOnlyRuleBeforeEvent = 4
        cAffectedEvents = 8


    End Function
End Class

