Option Strict Off
Option Explicit On
Public Class CJREDIStdRelease
    Const sClassName As String = "CJREDIStdRelease"
    Const sStandardTableName As String = "EDI_STDFILING_LKUP"
    Const sWCPTableName As String = "WCP_STDFILING_LKUP"

    Private m_TableRowID As Integer
    Private m_JurisRowID As Integer
    Private m_EffectiveDate As String
    Private m_ReleaseDesc As String
    Private m_ReleaseRowID As Integer 'reference to EDI_STDFILING_LKUP.TABLE_ROW_ID when used
    Private m_ShortCode As String
    Private m_DeletedFlag As Integer
    Private m_DataHasChanged As Boolean
    Private m_Warning As String

    Private Function ClearObject() As Integer
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDate = ""
        m_JurisRowID = 0
        m_ReleaseDesc = ""
        m_ReleaseRowID = 0
        m_ShortCode = ""
        m_TableRowID = 0


    End Function

    Private Function GetSQLFieldList() As String
        Dim sSQL As String

        GetSQLFieldList = sSQL
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", SHORT_CODE, RELEASE_NAME"
        sSQL = sSQL & ", RELEASE_ROW_ID"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef lTableRowID As Integer, ByRef lReleaseRowID As Integer, Optional ByRef sEffectiveDateDTG As String = "") As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0
            ClearObject()



            sSQL = GetSQLFieldList()
            If lJurisRowID > 0 Then
                sSQL = sSQL & " FROM " & sWCPTableName
                sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID

                If lTableRowID > 0 Then
                    sSQL = sSQL & " AND TABLE_ROW_ID = " & lTableRowID
                Else 'Typically data for release is loaded using the release and effective date combination
                    sSQL = sSQL & " AND RELEASE_ROW_ID = " & lReleaseRowID
                    sSQL = sSQL & " AND EFFECTIVE_DATE = '" & sEffectiveDateDTG & "'"
                End If

            Else
                sSQL = sSQL & " FROM " & sStandardTableName
                sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lReleaseRowID
            End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                If Not IsNothing(objReader.Item("EFFECTIVE_DATE")) Then
                    m_EffectiveDate = objReader.GetString("EFFECTIVE_DATE")
                Else
                    m_EffectiveDate = ""
                End If
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_ReleaseDesc = objReader.GetString("RELEASE_NAME")

                m_ShortCode = objReader.GetString("SHORT_CODE")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                If lJurisRowID > 0 Then
                    m_ReleaseRowID = objReader.GetInt32("RELEASE_ROW_ID")
                End If
            End If
            LoadData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadDataForCloning
    ' DateTime  : 3/5/2005 13:00
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : for cloning we get the standard, then set
    ' ..........: .ReleaseRowID = .TableRowID
    ' ..........: .TableRowID = 0
    ' ..........: .JurisRowID = lTargetJurisRowID
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadDataForCloning(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTargetJurisRowID As Integer, ByRef lSourceTableRowID As Integer, ByRef lReleaseRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataForCloning"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadDataForCloning = 0
            ClearObject()

            sSQL = GetSQLFieldList()
            If lTargetJurisRowID > 0 Then
                sSQL = sSQL & " FROM " & sWCPTableName
                sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lTargetJurisRowID
                sSQL = sSQL & " AND RELEASE_ROW_ID = " & lReleaseRowID
            Else
                sSQL = sSQL & " FROM " & sStandardTableName
                sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lSourceTableRowID
            End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                If Not IsNothing(objReader.Item("EFFECTIVE_DATE")) Then
                    m_EffectiveDate = objReader.GetString("EFFECTIVE_DATE")
                Else
                    m_EffectiveDate = ""
                End If
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_ReleaseDesc = objReader.GetString("RELEASE_NAME")

                m_ShortCode = objReader.GetString("SHORT_CODE")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                If lTargetJurisRowID > 0 Then
                    m_ReleaseRowID = objReader.GetInt32("RELEASE_ROW_ID")
                End If
            End If
            LoadDataForCloning = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataForCloning = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            DBDisconnect()

        End Try

    End Function
    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property EffectiveDate() As String
        Get
            EffectiveDate = m_EffectiveDate
        End Get
        Set(ByVal Value As String)
            m_EffectiveDate = Value
        End Set
    End Property

    Public Property ReleaseDesc() As String
        Get
            ReleaseDesc = m_ReleaseDesc
        End Get
        Set(ByVal Value As String)
            m_ReleaseDesc = Value
        End Set
    End Property
    '
    'Public Property Get ReleaseVersion() As String
    '    ReleaseVersion = m_ReleaseVersion
    'End Property
    'Public Property Let ReleaseVersion(ByVal vData As String)
    '    m_ReleaseVersion = vData
    'End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property ReleaseRowID() As Integer
        Get
            ReleaseRowID = m_ReleaseRowID
        End Get
        Set(ByVal Value As Integer)
            m_ReleaseRowID = Value
        End Set
    End Property

    Public Property ShortCode() As String
        Get
            ShortCode = m_ShortCode
        End Get
        Set(ByVal Value As String)
            m_ShortCode = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = GetSQLFieldList()
            If m_JurisRowID > 0 Then
                sSQL = sSQL & " FROM " & sWCPTableName
                sSQL = sSQL & " WHERE JURIS_ROW_ID = " & m_JurisRowID
                sSQL = sSQL & " AND TABLE_ROW_ID = " & m_TableRowID
            Else
                sSQL = sSQL & " FROM " & sStandardTableName
                sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID
            End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("RELEASE_NAME").Value = m_ReleaseDesc
                If IsDBNull(m_ShortCode) Or Trim(m_ShortCode) = "" Then m_ShortCode = "X"
                objWriter.Fields("SHORT_CODE").Value = m_ShortCode
                If m_JurisRowID > 0 Then
                    objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDate
                    objWriter.Fields("RELEASE_ROW_ID").Value = m_ReleaseRowID
                End If
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                If m_JurisRowID > 0 Then
                    m_TableRowID = lGetNextUID(sWCPTableName)
                Else
                    m_TableRowID = lGetNextUID(sStandardTableName)
                End If
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("RELEASE_NAME", m_ReleaseDesc)
                If IsDBNull(m_ShortCode) Or Trim(m_ShortCode) = "" Then m_ShortCode = "X"
                objWriter.Fields.Add("SHORT_CODE", m_ShortCode)
                If m_JurisRowID > 0 Then
                    objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDate)
                    objWriter.Fields.Add("RELEASE_ROW_ID", m_ReleaseRowID)
                End If
            End If
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)
            DBDisconnect()

        End Try

    End Function
    Public Function SaveDataClone(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveDataClone"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveDataClone = 0

            If m_JurisRowID < 1 Then
                g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
                g_sErrDescription = "Jurisdiction ID not set."
                If Not g_bBatchMode Then Err.Raise(vbObjectError + 70000, g_sErrProcedure, g_sErrDescription)
                LogError(g_sErrProcedure, Erl(), vbObjectError + 70000, Err.Source, g_sErrDescription)
                Exit Function
            End If

            If m_ReleaseRowID < 1 Then
                g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
                g_sErrDescription = "Release Row ID not set."
                If Not g_bBatchMode Then Err.Raise(vbObjectError + 70000, g_sErrProcedure, g_sErrDescription)
                LogError(g_sErrProcedure, Erl(), vbObjectError + 70000, Err.Source, g_sErrDescription)
                Exit Function
            End If

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM WCP_STDFILING_LKUP"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & m_JurisRowID
            sSQL = sSQL & " AND RELEASE_ROW_ID = " & m_ReleaseRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = '" & m_EffectiveDate & "'"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("RELEASE_NAME").Value = m_ReleaseDesc
                If IsDBNull(m_ShortCode) Or Trim(m_ShortCode) = "" Then m_ShortCode = "X"
                objWriter.Fields("SHORT_CODE").Value = m_ShortCode
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDate
                objWriter.Fields("RELEASE_ROW_ID").Value = m_ReleaseRowID
                objWriter.Execute()
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                m_TableRowID = lGetNextUID("WCP_STDFILING_LKUP")
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)

                If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("RELEASE_NAME", m_ReleaseDesc)
                If IsDBNull(m_ShortCode) Or Trim(m_ShortCode) = "" Then m_ShortCode = "X"
                objWriter.Fields.Add("SHORT_CODE", m_ShortCode)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDate)
                objWriter.Fields.Add("RELEASE_ROW_ID", m_ReleaseRowID)
            End If
            objWriter.Execute()
            SaveDataClone = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & "|" & Err.Description
            End With
            SaveDataClone = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function ValidateData() As Integer

        ValidateData = 0
        m_Warning = ""

        If Trim(m_ReleaseDesc & "") = "" Then
            m_Warning = m_Warning & "A Name and Version is  required." & vbCrLf
        End If
        If Trim(m_ShortCode & "") = "" Then
            m_Warning = m_Warning & "A Short Code is  required." & vbCrLf
        End If


    End Function

    Public Function DeleteObject(ByRef objUser As Riskmaster.Security.UserLogin) As Short

        Const sFunctionName As String = "DeleteObject"
        Dim sSQL As String
        Dim sMessage As String
        Dim objTrans As DbTransaction
        Dim objConn As DbConnection
        Try

            DeleteObject = 0
            objConn = DbFactory.GetDbConnection(g_ConnectionString)


            objTrans = objConn.BeginTransaction()

            'Performing Simple Deletes. Not loading the child objects and calling delete functions of the child objects
            '(1) Delete Conditions associated with the DN Records associated with this Release
            If m_JurisRowID > 0 Then
                sSQL = "DELETE FROM WCP_STDCONDITIONS WHERE RECDIF_ROW_ID IN "
                sSQL = sSQL & "(SELECT TABLE_ROW_ID FROM WCP_STDRECDIF_LKUP WHERE PT_PT_TABLE_ROW_ID=" & m_TableRowID & ")"
            Else
                sSQL = "DELETE FROM EDI_STDCONDITIONS WHERE RECDIF_ROW_ID IN "
                sSQL = sSQL & "(SELECT TABLE_ROW_ID FROM EDI_STDRECDIF_LKUP WHERE RELEASE_ROW_ID=" & m_TableRowID & ")"
            End If
            objConn.ExecuteNonQuery(sSQL, objTrans)

            '(2) Delete the DN Records associated with this Release
            If m_JurisRowID > 0 Then
                sSQL = "DELETE FROM WCP_STDRECDIF_LKUP WHERE PT_PT_TABLE_ROW_ID=" & m_TableRowID
            Else
                sSQL = "DELETE FROM EDI_STDRECDIF_LKUP WHERE RELEASE_ROW_ID=" & m_TableRowID
            End If
            objConn.ExecuteNonQuery(sSQL, objTrans)

            '(3) Delete the Record Types associated with this Release
            If m_JurisRowID > 0 Then
                sSQL = "DELETE FROM WCP_STDRECTYP_LKUP WHERE PT_TABLE_ROW_ID=" & m_TableRowID
            Else
                sSQL = "DELETE FROM EDI_STDRECTYP_LKUP WHERE RELEASE_ROW_ID=" & m_TableRowID
            End If
            objConn.ExecuteNonQuery(sSQL, objTrans)

            '(4) Delete the MTCs associated with this Release
            If m_JurisRowID > 0 Then
                sSQL = "DELETE FROM WCP_STDMTC_CODES WHERE JURISRELEASE_ROW_ID=" & m_TableRowID
            Else
                sSQL = "DELETE FROM EDI_STDMTC_CODES WHERE RELEASE_ROW_ID=" & m_TableRowID
            End If
            objConn.ExecuteNonQuery(sSQL, objTrans)

            '(5) Finally delete the Release Record
            If m_JurisRowID > 0 Then
                sSQL = "DELETE FROM " & sWCPTableName & " WHERE TABLE_ROW_ID=" & m_TableRowID
            Else
                sSQL = "DELETE FROM " & sStandardTableName & " WHERE TABLE_ROW_ID=" & m_TableRowID
            End If
            objConn.ExecuteNonQuery(sSQL, objTrans)

            objTrans.Commit()

            'Log the Deleled Record
            sMessage = "Reelease Deleted" & " | "
            sMessage = sMessage & "Release Row Id: " & m_TableRowID & " | " & m_ShortCode & " | " & m_ReleaseDesc & " | "
            If m_JurisRowID > 0 Then
                sMessage = sMessage & "Jurisdiction Row Id: " & m_JurisRowID & " | "
            End If
            sMessage = sMessage & "Deleted By " & objUser.LoginName & " | "
            LogError(Err.Source & "|" & sClassName & "|" & sFunctionName, Erl(), 70001, Err.Source, sMessage)

            DeleteObject = -1

        Catch ex As Exception
            DeleteObject = 0
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DeleteObject = Err.Number
            objTrans.Rollback()

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            objTrans = Nothing
            objTrans.Dispose()
            objConn = Nothing
            objConn.Dispose()
        End Try

    End Function
End Class

