Option Strict Off
Option Explicit On
Public Class CJRFriendlyNameUseds
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRFriendlyNameUseds"

    'Class properties, local copy
    Private mCol As Collection
    Public Function LoadDataBene(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lFormID As Integer) As Integer
        Dim objReader As DbReader
        Dim objRecord As CJRFriendlyName
        Dim sSQL As String
        Try

            LoadDataBene = 0



            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " TABLE_ROW_ID"
            sSQL = sSQL & ", FORM_ID, FRIENDLY_NAME, FIELD_NAME"
            sSQL = sSQL & " FROM WCP_FRIENDLY_NAME"
            If lFormID > 0 Then
                sSQL = sSQL & " WHERE FORM_ID = " & lFormID
            End If
            sSQL = sSQL & " ORDER BY TABLE_ROW_ID"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRFriendlyName
                With objRecord
                    .FieldName = objReader.GetString("FIELD_NAME")
                    .FormID = objReader.GetInt32("FORM_ID")
                    .FriendlyName = objReader.GetString("FRIENDLY_NAME")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.FormID, CInt(.FriendlyName), .FieldName, CStr(.TableRowID), "k" & CStr(.TableRowID))
                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing

            End While

            LoadDataBene = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataBene = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataBene|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function

    Public Function LoadDataAWW(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lFormID As Integer) As Integer
        Dim objReader As DbReader
        Dim objRecord As CJRFriendlyName
        Dim sSQL As String
        Try

            LoadDataAWW = 0



            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " TABLE_ROW_ID"
            sSQL = sSQL & ", FORM_ID, FRIENDLY_NAME, FIELD_NAME"
            sSQL = sSQL & " FROM WCP_FRIENDLY_NAME"
            If lFormID > 0 Then
                sSQL = sSQL & " WHERE FORM_ID = " & lFormID
            End If
            sSQL = sSQL & " ORDER BY TABLE_ROW_ID"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRFriendlyName
                With objRecord
                    .FieldName = objReader.GetString("FIELD_NAME")
                    .FormID = objReader.GetInt32("FORM_ID")
                    .FriendlyName = objReader.GetString("FRIENDLY_NAME")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.FormID, CInt(.FriendlyName), .FieldName, CStr(.TableRowID), "k" & CStr(.TableRowID))
                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing

            End While

            SafeCloseRecordset(objReader)
            LoadDataAWW = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataAWW = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataAWW|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function


    Public Function Add(ByRef TableRowID As Integer, ByRef FormID As Integer, ByRef FriendlyName As String, ByRef FieldName As String, Optional ByRef sKey As String = "") As CJRFriendlyName
        'create a new object
        Dim objNewMember As CJRFriendlyName
        objNewMember = New CJRFriendlyName


        'set the properties passed into the method
        objNewMember.TableRowID = TableRowID
        objNewMember.FormID = FormID
        objNewMember.FriendlyName = FriendlyName
        objNewMember.FieldName = FieldName
        If Len(sKey) = 0 Then
            mCol.Add(objNewMember)
        Else
            mCol.Add(objNewMember, sKey)
        End If


        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing




    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRFriendlyName
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'destroys collection when this class is terminated
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

