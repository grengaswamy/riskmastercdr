Option Strict Off
Option Explicit On
Public Class CJRBodyMember
    '---------------------------------------------------------------------------------------
    ' Module    : CJRBodyMember
    ' DateTime  : 7/19/2004 15:24
    ' Author    : jtodd22
    ' Purpose   : Permanent Partial Benefits based on Body Members
    '---------------------------------------------------------------------------------------
    Const sClassName As String = "CJRBodyMember"
    Const sTableName As String = "WCP_DATA_BODY_MEMB"
    'Class properties, local copy
    Private m_TableRowID As Integer
    Private m_Degrees As Short
    Private m_EffectiveDateDTG As String
    Private m_JurisCode As String
    Private m_JurisRowID As Short
    Private m_DeletedFlag As Short
    Private m_BodyMemberDesc As String
    Private m_MaxAmount As Decimal
    Private m_MaxWeeks As Double
    Private m_MaxMonths As Double
    Private m_TimeUnit As Integer
    Private m_AmputationPercentRate As Double
    Private m_ImpairmentPercentage As Double 'jtodd22 impairment is NOT jurisdictional rule based, it is claim based
    Private m_Amputated As Integer 'jtodd22 amputated is NOT jurisdictional rule
    Private m_LiabilityAmount As Double 'jtodd22 LiabilityAmount is NOT jurdictional rule, is is calculated
    Private m_PayRate As Double 'jtodd22 PayRate is NOT jurisdictional fule, is for calculation only
    Private m_DataHasChanged As Boolean
    Private m_Warning As String

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property PayRate() As Double
        Get
            PayRate = m_PayRate
        End Get
        Set(ByVal Value As Double)
            m_PayRate = Value
        End Set
    End Property

    Public Property LiabilityAmount() As Double
        Get
            LiabilityAmount = m_LiabilityAmount
        End Get
        Set(ByVal Value As Double)
            m_LiabilityAmount = Value
        End Set
    End Property

    Public Property Amputated() As Integer
        Get
            Amputated = m_Amputated
        End Get
        Set(ByVal Value As Integer)
            m_Amputated = Value
        End Set
    End Property

    Public Property AmputationPercentRate() As Double
        Get
            AmputationPercentRate = m_AmputationPercentRate
        End Get
        Set(ByVal Value As Double)
            m_AmputationPercentRate = Value
        End Set
    End Property

    Public Property Degrees() As Short
        Get
            Degrees = m_Degrees
        End Get
        Set(ByVal Value As Short)
            m_Degrees = Value
        End Set
    End Property

    Public Property ImpairmentPercentage() As Double
        Get
            ImpairmentPercentage = m_ImpairmentPercentage
        End Get
        Set(ByVal Value As Double)
            m_ImpairmentPercentage = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property TimeUnit() As Integer
        Get
            TimeUnit = m_TimeUnit
        End Get
        Set(ByVal Value As Integer)
            m_TimeUnit = Value
        End Set
    End Property
    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property JurisCode() As String
        Get
            JurisCode = m_JurisCode
        End Get
        Set(ByVal Value As String)
            m_JurisCode = Value
        End Set
    End Property
    Public Property JurisRowID() As Short
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Short)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property DeletedFlag() As Short
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Short)
            m_DeletedFlag = Value
        End Set
    End Property
    Public Property MaxAmount() As Decimal
        Get
            MaxAmount = m_MaxAmount
        End Get
        Set(ByVal Value As Decimal)
            m_MaxAmount = Value
        End Set
    End Property

    Public Property MaxMonths() As Double
        Get
            MaxMonths = m_MaxMonths
        End Get
        Set(ByVal Value As Double)
            m_MaxMonths = Value
        End Set
    End Property

    Public Property MaxWeeks() As Double
        Get
            MaxWeeks = m_MaxWeeks
        End Get
        Set(ByVal Value As Double)
            m_MaxWeeks = Value
        End Set
    End Property
    Public Property BodyMemberDesc() As String
        Get
            BodyMemberDesc = m_BodyMemberDesc
        End Get
        Set(ByVal Value As String)
            m_BodyMemberDesc = Value
        End Set
    End Property
    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim objReader As DbReader
        Dim sSQL As String
        Try
            LoadDataByTableRowID = 0
            ClearObject()



            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND TABLE_ROW_ID = " & lTableRowID
            sSQL = sSQL & " ORDER BY BODY_MEMB_DESC"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                AssignData(objReader)
            End If

            LoadDataByTableRowID = -1
            m_DataHasChanged = False
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function LoadDataByMemberDesc(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As Object, ByRef sMemberDesc As String) As Integer
        Const sFunctionName As String = "LoadDataByMemberDesc"
        Dim objReader As DbReader
        Dim sSQL As String
        Try
            LoadDataByMemberDesc = 0
            ClearObject()

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            'UPGRADE_WARNING: Couldn't resolve default property of object sDateOfEventDTG. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sSQL = sSQL & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"
            sSQL = sSQL & " AND BODY_MEMB_DESC = '" & sMemberDesc & "'"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                AssignData(objReader)
            End If

            LoadDataByMemberDesc = -1
            m_DataHasChanged = False
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByMemberDesc = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataByMemberDesc|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        m_TableRowID = 0
        m_Degrees = 0
        m_EffectiveDateDTG = ""
        m_JurisRowID = 0
        m_DeletedFlag = 0
        m_BodyMemberDesc = ""
        m_MaxAmount = 0
        m_MaxWeeks = 0
        m_MaxMonths = 0
        m_AmputationPercentRate = 0
        m_ImpairmentPercentage = 0
        m_DataHasChanged = False


    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)

                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("BODY_MEMB_DESC").Value = m_BodyMemberDesc
                objWriter.Fields("MAX_AMOUNT").Value = m_MaxAmount
                objWriter.Fields("MAX_WEEKS").Value = m_MaxWeeks
                objWriter.Fields("MAX_MONTHS").Value = m_MaxMonths
                objWriter.Fields("AMPUTAT_RATE").Value = m_AmputationPercentRate
                objWriter.Fields("DEGREES_NUMB").Value = m_Degrees
                SafeCloseRecordset(objReader)
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("BODY_MEMB_DESC", m_BodyMemberDesc)
                objWriter.Fields.Add("MAX_AMOUNT", m_MaxAmount)
                objWriter.Fields.Add("MAX_WEEKS", m_MaxWeeks)
                objWriter.Fields.Add("MAX_MONTHS", m_MaxMonths)
                objWriter.Fields.Add("AMPUTAT_RATE", m_AmputationPercentRate)
                objWriter.Fields.Add("DEGREES_NUMB", m_Degrees)
            End If
            objWriter.Execute()
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            SaveData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            If InStr(1, UCase(g_sErrDescription), UCase("UNIQUE CONSTRAINT")) > 0 Then
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                SaveData = 89999
                Exit Function
            End If
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Delete(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try
            Delete = 0
            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " *"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                SafeCloseRecordset(objReader)
                objWriter.Fields("DELETED_FLAG").Value = -1
                objWriter.Execute()
            End If
            Delete = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            Delete = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Delete|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try
    End Function

    Private Function AssignData(ByRef objReader As DbReader) As Integer
        Const sFunctionName As String = "AssignData"
        Try

            AssignData = 0

            m_BodyMemberDesc = Trim(objReader.GetString("BODY_MEMB_DESC"))
            m_Degrees = objReader.GetInt32("DEGREES_NUMB")
            m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
            m_EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
            m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
            m_MaxAmount = objReader.GetDouble("MAX_AMOUNT")
            m_MaxWeeks = objReader.GetDouble("MAX_WEEKS")
            m_MaxMonths = objReader.GetDouble("MAX_MONTHS")
            m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
            m_AmputationPercentRate = objReader.GetInt32("AMPUTAT_RATE")

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE,JURIS_ROW_ID,DELETED_FLAG"
        sSQL = sSQL & ",BODY_MEMB_DESC, MAX_AMOUNT"
        sSQL = sSQL & ",MAX_MONTHS, MAX_WEEKS"
        sSQL = sSQL & ",AMPUTAT_RATE"
        sSQL = sSQL & ",DEGREES_NUMB"
        GetSQLFieldList = sSQL


    End Function
    Public Function ValidateData() As Integer
        Const sRoutineName As String = "ValidateData"

        Try

            m_Warning = ""
            If Len(m_EffectiveDateDTG) <> 8 Then
                m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
            End If
            If m_BodyMemberDesc = "" Then
                m_Warning = m_Warning & "A Body Member description is required." & vbCrLf
            End If
            ValidateData = 0
        Catch ex As Exception

        Finally

        End Try

    End Function
End Class

