Option Strict Off
Option Explicit On
Public Class CJRAgeFactor
    Const sClassName As String = "CJRAgeFactor"
    Const sTableName As String = "WCP_AGE_FACTOR"

    'local variable(s) to hold property value(s)
    Private m_TableRowID As Integer 'local copy
    Private m_Age As Integer 'local copy
    Private m_Factor As Double 'local copy
    Private m_JurisRowID As Integer 'local copy
    Private m_EffectiveDateDTG As String 'local copy
    Private m_Weeks As Double
    Private m_Multiplier As Double
    Private m_DataHasChanged As Boolean
    Private m_Warning As String

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()

            'If ModMain.DBConnect(objUser, sClassName) <> -1 Then
            '	g_sErrDescription = "Failed to connect to database."
            '	g_sErrSrc = "RMJuRuLib"
            '	g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            '	g_lErrLine = Erl()
            '	LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            '	Err.Raise(70001, sClassName & "." & sFunctionName, g_sErrDescription)
            '	Exit Function
            'End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If objReader.Read() Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_Age = objReader.GetInt32("AGE")
                m_Factor = objReader.GetDouble("FACTOR")
                m_Multiplier = objReader.GetDouble("MULTIPLIER")
                m_Weeks = objReader.GetDouble("WEEKS")
            End If



            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer



    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",EFFECTIVE_DATE"
        sSQL = sSQL & ",AGE"
        sSQL = sSQL & ",FACTOR"
        sSQL = sSQL & ",MULTIPLIER"
        sSQL = sSQL & ",WEEKS"
        GetSQLFieldList = sSQL


    End Function
    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property
    Public Property Weeks() As Double
        Get
            Weeks = m_Weeks
        End Get
        Set(ByVal Value As Double)
            m_Weeks = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property Factor() As Double
        Get
            Factor = m_Factor
        End Get
        Set(ByVal Value As Double)
            m_Factor = Value
        End Set
    End Property
    Public Property Age() As Integer
        Get
            Age = m_Age
        End Get
        Set(ByVal Value As Integer)
            m_Age = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property Multiplier() As Double
        Get
            Multiplier = m_Multiplier
        End Get
        Set(ByVal Value As Double)
            m_Multiplier = Value
        End Set
    End Property


    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property

    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0

            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadData = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try


    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String, ByRef lAge As Integer) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"
            sSQL = sSQL & " AND AGE = " & lAge

            LoadDataByEventDate = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0

            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("AGE").Value = m_Age
                If IsDBNull(m_Factor) Then m_Factor = 0
                objWriter.Fields("FACTOR").Value = m_Factor
                If IsDBNull(m_Multiplier) Then m_Multiplier = 0
                objWriter.Fields("MULTIPLIER").Value = m_Multiplier
                If IsDBNull(m_Weeks) Then m_Weeks = 0
                objWriter.Fields("WEEKS").Value = m_Weeks
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("AGE", m_Age)
                If IsDBNull(m_Factor) Then m_Factor = 0
                objWriter.Fields.Add("FACTOR", m_Factor)
                If IsDBNull(m_Multiplier) Then m_Multiplier = 0
                objWriter.Fields.Add("MULTIPLIER", m_Multiplier)
                If IsDBNull(m_Weeks) Then m_Weeks = 0
                objWriter.Fields.Add("WEEKS", m_Weeks)
            End If

            objWriter.Execute()

            SaveData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = .Number
                g_sErrSrc = .Source
                g_sErrDescription = .Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try


    End Function
    Public Function ValidateData() As Integer
        ValidateData = 0
        m_Warning = ""
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If Not IsNumeric(m_Age) Then
            m_Warning = m_Warning & "A valid Age greater then zero is required." & vbCrLf
        Else
            If m_Age = 0 Then
                m_Warning = m_Warning & "A valid Age greater then zero is required." & vbCrLf
            End If
        End If
        If Not IsNumeric(m_Multiplier) Then
            m_Warning = m_Warning & "A value is required for Multiplier, even if it is zero." & vbCrLf
        End If



    End Function
End Class

