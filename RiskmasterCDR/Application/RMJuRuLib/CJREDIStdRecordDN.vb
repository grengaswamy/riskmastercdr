Option Strict Off
Option Explicit On
Public Class CJREDIStdRecordDN
    '---------------------------------------------------------------------------------------
    ' Module    : CJREDIReleaseDN
    ' DateTime  : 9/7/2004 09:04
    ' Author    : jtodd22
    ' Purpose   : Class for the D(ata)N(umber) or data element of the named as part of a record
    ' Note      : DN is used by IAIABC.  For program coding "SubRecord" would be more discriptive
    ' Note      : A record is not an extraction.  It is a line of data in a file.
    '---------------------------------------------------------------------------------------
    Const sClassName As String = "CJREDIStdRecordDN"
    Const sTableName2 As String = "EDI_STDRECDIF_LKUP"
    Const sTableName3 As String = "WCP_STDRECDIF_LKUP"

    'local variable(s) to hold property value(s)
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_DNNumberText As String
    Private m_EffectiveDateDTG As String
    Private m_FieldSeq As Integer
    Private m_FieldStart As Integer
    Private m_FieldLength As Integer
    Private m_FieldFormat As Integer
    Private m_FieldStatus As Integer
    Private m_FieldName As String
    Private m_JurisRowID As Integer
    Private m_PtPtTableRowID As Integer
    Private m_PtTableRowID As Integer
    Private m_RecordName As String
    Private m_RecordTypeID As Integer
    Private m_ReleaseRowID As Integer
    Private m_ReleaseName As String
    Private m_RiskMasterDesc As String
    Private m_TableRowID As Integer
    Private m_Warning As String


    Private Function ClearObject() As Integer
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_DNNumberText = vbNullString
        m_EffectiveDateDTG = vbNullString
        m_FieldFormat = 0
        m_FieldLength = 0
        m_FieldName = vbNullString
        m_FieldSeq = 0
        m_FieldStart = 0
        m_FieldStatus = 0
        m_JurisRowID = 0
        m_PtPtTableRowID = 0
        m_PtTableRowID = 0
        m_RecordTypeID = 0
        m_ReleaseRowID = 0
        m_ReleaseName = vbNullString
        m_RiskMasterDesc = vbNullString
        m_TableRowID = 0
        m_Warning = ""


    End Function

    Private Function GetFieldSelect() As String
        Dim sTable As String
        Dim sSQL As String
        Dim sPTSQL As String

        GetFieldSelect = ""
        If m_JurisRowID > 0 Then
            sTable = sTableName3 & "."
            sPTSQL = ""
            sPTSQL = sPTSQL & ", " & sTableName3 & ".PT_TABLE_ROW_ID"
            sPTSQL = sPTSQL & ", " & sTableName3 & ".PT_PT_TABLE_ROW_ID"
        Else
            sTable = sTableName2 & "."
        End If
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " " & sTable & "TABLE_ROW_ID"
        sSQL = sSQL & ", " & sTable & "ADDED_BY_USER"
        sSQL = sSQL & ", " & sTable & "DTTM_RCD_ADDED"
        sSQL = sSQL & ", " & sTable & "UPDATED_BY_USER"
        sSQL = sSQL & ", " & sTable & "DTTM_RCD_LAST_UPD"
        sSQL = sSQL & ", " & sTable & "JURIS_ROW_ID"
        sSQL = sSQL & ", " & sTable & "RECORD_TYPE_ID"
        sSQL = sSQL & ", " & sTable & "RELEASE_ROW_ID"
        sSQL = sSQL & ", " & sTable & "EFFECTIVE_DATE"
        sSQL = sSQL & ", DN_NUMBER_TXT"
        sSQL = sSQL & ", FIELD_FORMAT"
        sSQL = sSQL & ", FIELD_LENGTH"
        sSQL = sSQL & ", FIELD_NAME"
        sSQL = sSQL & ", FIELD_SEQ"
        sSQL = sSQL & ", FIELD_START"
        sSQL = sSQL & ", FIELD_STATUS"
        sSQL = sSQL & ", RISKMASTER_DESC"
        sSQL = sSQL & sPTSQL

        GetFieldSelect = sSQL


    End Function

    Private Function GetSQLFieldListCloning() As String
        Dim sTable As String
        Dim sSQL As String
        GetSQLFieldListCloning = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_ADDED"
        sSQL = sSQL & ", UPDATED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", RECORD_TYPE_ID"
        sSQL = sSQL & ", RELEASE_ROW_ID"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", DN_NUMBER_TXT"
        sSQL = sSQL & ", FIELD_FORMAT"
        sSQL = sSQL & ", FIELD_LENGTH"
        sSQL = sSQL & ", FIELD_NAME"
        sSQL = sSQL & ", FIELD_SEQ"
        sSQL = sSQL & ", FIELD_START"
        sSQL = sSQL & ", FIELD_STATUS"
        sSQL = sSQL & ", RISKMASTER_DESC"
        sSQL = sSQL & ", PT_TABLE_ROW_ID, PT_PT_TABLE_ROW_ID"
        GetSQLFieldListCloning = sSQL


    End Function

    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0
            ClearObject()
            m_JurisRowID = lJurisRowID


            sSQL = GetFieldSelect()
            If m_JurisRowID > 0 Then
                sSQL = sSQL & ", WCP_STDFILING_LKUP.RELEASE_NAME"
                sSQL = sSQL & " FROM " & sTableName3 & ", WCP_STDFILING_LKUP"
                sSQL = sSQL & " WHERE " & sTableName3 & ".PT_PT_TABLE_ROW_ID = WCP_STDFILING_LKUP.TABLE_ROW_ID"
                sSQL = sSQL & " AND " & sTableName3 & ".TABLE_ROW_ID = " & lTableRowID
            Else
                sSQL = sSQL & ", EDI_STDFILING_LKUP.RELEASE_NAME"
                sSQL = sSQL & " FROM " & sTableName2 & ", EDI_STDFILING_LKUP"
                sSQL = sSQL & " WHERE " & sTableName2 & ".RELEASE_ROW_ID = EDI_STDFILING_LKUP.TABLE_ROW_ID"
                sSQL = sSQL & " AND " & sTableName2 & ".TABLE_ROW_ID = " & lTableRowID
            End If
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_DataHasChanged = False
                m_DNNumberText = Trim(objReader.GetString("DN_NUMBER_TXT"))
                m_EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
                m_FieldFormat = CInt(Trim(objReader.GetString("FIELD_FORMAT")))
                m_FieldLength = objReader.GetInt32("FIELD_LENGTH")
                m_FieldName = Trim(objReader.GetString("FIELD_NAME"))
                m_FieldSeq = objReader.GetInt32("FIELD_SEQ")
                m_FieldStart = objReader.GetInt32("FIELD_START")
                m_FieldStatus = objReader.GetInt32("FIELD_STATUS")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")

                If lJurisRowID > 0 Then
                    m_RecordTypeID = objReader.GetInt32("PT_TABLE_ROW_ID")
                Else
                    m_RecordTypeID = objReader.GetInt32("RECORD_TYPE_ID")
                End If

                m_ReleaseRowID = objReader.GetInt32("RELEASE_ROW_ID")
                m_ReleaseName = Trim(objReader.GetString("RELEASE_NAME"))
                m_RiskMasterDesc = Trim(objReader.GetString("RISKMASTER_DESC"))
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")

            End If
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"

        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = GetFieldSelect()
            If m_JurisRowID > 0 Then
                sSQL = sSQL & " FROM " & sTableName3
            Else
                sSQL = sSQL & " FROM " & sTableName2
            End If
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName

                If Not IsNothing(objReader.Item("DELETED_FLAG")) Then
                    If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                    objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                End If

                If Not IsNothing(objReader.Item("JURIS_ROW_ID")) Then
                    objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                End If
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("DN_NUMBER_TXT").Value = m_DNNumberText
                objWriter.Fields("FIELD_FORMAT").Value = m_FieldFormat
                objWriter.Fields("FIELD_LENGTH").Value = m_FieldLength
                objWriter.Fields("FIELD_NAME").Value = m_FieldName
                objWriter.Fields("FIELD_SEQ").Value = m_FieldSeq
                objWriter.Fields("FIELD_START").Value = m_FieldStart
                objWriter.Fields("FIELD_STATUS").Value = m_FieldStatus
                objWriter.Fields("RECORD_TYPE_ID").Value = m_RecordTypeID
                objWriter.Fields("RELEASE_ROW_ID").Value = m_ReleaseRowID
                objWriter.Fields("RISKMASTER_DESC").Value = m_RiskMasterDesc

                If Not IsNothing(objReader.Item("PT_TABLE_ROW_ID")) Then
                    objWriter.Fields("PT_TABLE_ROW_ID").Value = m_PtTableRowID
                End If

                If Not IsNothing(objReader.Item("PT_PT_TABLE_ROW_ID")) Then
                    objWriter.Fields("PT_PT_TABLE_ROW_ID").Value = m_PtPtTableRowID
                End If
                SafeCloseRecordset(objReader)
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                If m_JurisRowID > 0 Then
                    m_TableRowID = lGetNextUID(sTableName3)
                Else
                    m_TableRowID = lGetNextUID(sTableName2)
                End If
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName

                If Not IsNothing(objReader.Item("DELETED_FLAG")) Then
                    'UPGRADE_WARNING: Use of Null/IsNull() detected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"'
                    If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                    objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                End If

                If Not IsNothing(objReader.Item("JURIS_ROW_ID")) Then
                    objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                End If

                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("DN_NUMBER_TXT", m_DNNumberText)
                objWriter.Fields.Add("FIELD_FORMAT", m_FieldFormat)
                objWriter.Fields.Add("FIELD_LENGTH", m_FieldLength)
                objWriter.Fields.Add("FIELD_NAME", m_FieldName)
                objWriter.Fields.Add("FIELD_SEQ", m_FieldSeq)
                objWriter.Fields.Add("FIELD_START", m_FieldStart)
                objWriter.Fields.Add("FIELD_STATUS", m_FieldStatus)
                objWriter.Fields.Add("RECORD_TYPE_ID", m_RecordTypeID)
                objWriter.Fields.Add("RELEASE_ROW_ID", m_ReleaseRowID)
                objWriter.Fields.Add("RISKMASTER_DESC", m_RiskMasterDesc)

                If Not IsNothing(objReader.Item("PT_TABLE_ROW_ID")) Then
                    objWriter.Fields.Add("PT_TABLE_ROW_ID", m_PtTableRowID)
                End If

                If Not IsNothing(objReader.Item("PT_PT_TABLE_ROW_ID")) Then
                    objWriter.Fields.Add("PT_PT_TABLE_ROW_ID", m_PtPtTableRowID)
                End If
            End If
            objWriter.Execute()
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Public Function SaveDataClone(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveDataClone"

        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveDataClone = 0

            sSQL = GetSQLFieldListCloning()
            sSQL = sSQL & " FROM WCP_STDRECDIF_LKUP"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = 0" '& m_TableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName

                If Not IsNothing(objReader.Item("DELETED_FLAG")) Then
                    'UPGRADE_WARNING: Use of Null/IsNull() detected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"'
                    If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                    objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                End If

                If Not IsNothing(objReader.Item("JURIS_ROW_ID")) Then
                    objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                End If

                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("DN_NUMBER_TXT").Value = m_DNNumberText
                objWriter.Fields("FIELD_FORMAT").Value = m_FieldFormat
                objWriter.Fields("FIELD_LENGTH").Value = m_FieldLength
                objWriter.Fields("FIELD_NAME").Value = m_FieldName
                objWriter.Fields("FIELD_SEQ").Value = m_FieldSeq
                objWriter.Fields("FIELD_START").Value = m_FieldStart
                objWriter.Fields("FIELD_STATUS").Value = m_FieldStatus
                objWriter.Fields("RECORD_TYPE_ID").Value = m_RecordTypeID
                objWriter.Fields("RELEASE_ROW_ID").Value = m_ReleaseRowID
                objWriter.Fields("RISKMASTER_DESC").Value = m_RiskMasterDesc
                objWriter.Fields("PT_TABLE_ROW_ID").Value = m_PtTableRowID
                objWriter.Fields("PT_PT_TABLE_ROW_ID").Value = m_PtPtTableRowID
                objWriter.Execute()
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                m_TableRowID = lGetNextUID("WCP_STDRECDIF_LKUP")
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))

                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName

                If Not IsNothing(objReader.Item("DELETED_FLAG")) Then
                    'UPGRADE_WARNING: Use of Null/IsNull() detected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"'
                    If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                    objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                End If

                If Not IsNothing(objReader.Item("JURIS_ROW_ID")) Then
                    objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                End If

                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("DN_NUMBER_TXT", m_DNNumberText)
                objWriter.Fields.Add("FIELD_FORMAT", m_FieldFormat)
                objWriter.Fields.Add("FIELD_LENGTH", m_FieldLength)
                objWriter.Fields.Add("FIELD_NAME", m_FieldName)
                objWriter.Fields.Add("FIELD_SEQ", m_FieldSeq)
                objWriter.Fields.Add("FIELD_START", m_FieldStart)
                objWriter.Fields.Add("FIELD_STATUS", m_FieldStatus)
                objWriter.Fields.Add("RECORD_TYPE_ID", m_RecordTypeID)
                objWriter.Fields.Add("RELEASE_ROW_ID", m_ReleaseRowID)
                objWriter.Fields.Add("RISKMASTER_DESC", m_RiskMasterDesc)
                objWriter.Fields.Add("PT_TABLE_ROW_ID", m_PtTableRowID)
                objWriter.Fields.Add("PT_PT_TABLE_ROW_ID", m_PtPtTableRowID)
                objWriter.Execute()
            End If



            SaveDataClone = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveDataClone = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveDataClone|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Validate() As Integer

        Validate = 0

        m_Warning = ""
        If m_ReleaseRowID < 1 Then
            m_Warning = m_Warning & "A valid 'Industry Standard Release' is required." & vbCrLf
        End If
        If Me.RecordTypeID < 1 Then
            m_Warning = m_Warning & "A valid 'Release Record Type' is required." & vbCrLf
        End If

        If Trim(m_DNNumberText & "") = "" Then
            m_Warning = m_Warning & "A valid 'IAIABC DN Number' is required." & vbCrLf
        Else
            If Len(m_DNNumberText) < 6 Then
                m_Warning = m_Warning & "The 'IAIABC DN Number' is too short to be valid." & vbCrLf
            Else
                If Left(m_DNNumberText, 2) <> UCase("DN") Then
                    m_Warning = m_Warning & "The first two characters of 'IAIABC DN Number' must be 'DN'." & vbCrLf
                End If

                If Not IsNumeric(Mid(m_DNNumberText, 3, 4)) Then
                    m_Warning = m_Warning & "The last four characters of 'IAIABC DN Number' must be numbers." & vbCrLf
                End If
            End If
        End If

        If m_FieldName = "" Then
            m_Warning = m_Warning & "A valid 'IAIABC Field Name' is required." & vbCrLf
        End If

        If m_FieldFormat < 10 Then
            m_Warning = m_Warning & "A valid 'Field Format' is required." & vbCrLf
        End If

        If m_FieldLength < 1 Then
            m_Warning = m_Warning & "A valid 'Field Length' is required." & vbCrLf
        End If

        If m_FieldStart < 1 Then
            m_Warning = m_Warning & "A valid 'Field Start Position' is required." & vbCrLf
        End If

        If m_FieldStatus < 1 Then
            m_Warning = m_Warning & "A valid 'Field Status' is required." & vbCrLf
        End If


    End Function
    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property
    Public Property DNNumberText() As String
        Get
            DNNumberText = m_DNNumberText
        End Get
        Set(ByVal Value As String)
            m_DNNumberText = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property FieldFormat() As String
        Get
            FieldFormat = CStr(m_FieldFormat)
        End Get
        Set(ByVal Value As String)
            m_FieldFormat = CInt(Value)
        End Set
    End Property

    Public Property FieldSeq() As Integer
        Get
            FieldSeq = m_FieldSeq
        End Get
        Set(ByVal Value As Integer)
            m_FieldSeq = Value
        End Set
    End Property

    Public Property FieldStart() As Integer
        Get
            FieldStart = m_FieldStart
        End Get
        Set(ByVal Value As Integer)
            m_FieldStart = Value
        End Set
    End Property

    Public Property FieldStatus() As Integer
        Get
            FieldStatus = m_FieldStatus
        End Get
        Set(ByVal Value As Integer)
            m_FieldStatus = Value
        End Set
    End Property

    Public Property FieldLength() As Integer
        Get
            FieldLength = m_FieldLength
        End Get
        Set(ByVal Value As Integer)
            m_FieldLength = Value
        End Set
    End Property

    Public Property FieldName() As String
        Get
            FieldName = m_FieldName
        End Get
        Set(ByVal Value As String)
            m_FieldName = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property PtPtTableRowID() As Integer
        Get
            PtPtTableRowID = m_PtPtTableRowID
        End Get
        Set(ByVal Value As Integer)
            m_PtPtTableRowID = Value
        End Set
    End Property

    Public Property PtTableRowID() As Integer
        Get
            PtTableRowID = m_PtTableRowID
        End Get
        Set(ByVal Value As Integer)
            m_PtTableRowID = Value
        End Set
    End Property

    Public Property RecordName() As String
        Get
            RecordName = m_RecordName
        End Get
        Set(ByVal Value As String)
            m_RecordName = Value
        End Set
    End Property

    Public Property RecordTypeID() As Integer
        Get
            RecordTypeID = m_RecordTypeID
        End Get
        Set(ByVal Value As Integer)
            m_RecordTypeID = Value
        End Set
    End Property

    Public Property ReleaseName() As String
        Get
            ReleaseName = m_ReleaseName
        End Get
        Set(ByVal Value As String)
            m_ReleaseName = Value
        End Set
    End Property

    Public Property ReleaseRowID() As Integer
        Get
            ReleaseRowID = m_ReleaseRowID
        End Get
        Set(ByVal Value As Integer)
            m_ReleaseRowID = Value
        End Set
    End Property

    Public Property RiskMasterDesc() As String
        Get
            RiskMasterDesc = m_RiskMasterDesc
        End Get
        Set(ByVal Value As String)
            m_RiskMasterDesc = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property



    Public Property Warning() As String
        Get

            Warning = m_Warning

        End Get
        Set(ByVal Value As String)

            m_Warning = Value

        End Set
    End Property

    Public Function DeleteObject(ByRef objUser As Riskmaster.Security.UserLogin) As Short

        Const sFunctionName As String = "DeleteObject"
        Dim sSQL As String
        Dim sMessage As String
        Dim objTrans As DbTransaction
        Dim objConn As DbConnection

        Try
            objConn = DbFactory.GetDbConnection(g_ConnectionString)
            DeleteObject = 0


            'Performing Simple Deletes. Not loading the child objects and calling delete functions of the child objects
            'First Delete Conditions associated with this DN Record
            If m_JurisRowID > 0 Then
                sSQL = "DELETE FROM WCP_STDCONDITIONS WHERE RECDIF_ROW_ID=" & m_TableRowID
            Else
                sSQL = "DELETE FROM EDI_STDCONDITIONS WHERE RECDIF_ROW_ID=" & m_TableRowID
            End If
            objTrans = objConn.BeginTransaction()
            objConn.ExecuteNonQuery(sSQL, objTrans)

            'Delete the DN Record
            If m_JurisRowID > 0 Then
                sSQL = "DELETE FROM " & sTableName3 & " WHERE TABLE_ROW_ID=" & m_TableRowID
            Else
                sSQL = "DELETE FROM " & sTableName2 & " WHERE TABLE_ROW_ID=" & m_TableRowID
            End If
            objConn.ExecuteNonQuery(sSQL, objTrans)

            objTrans.Commit()

            'Log the Deleled Record
            sMessage = "DN Record Deleted" & " | "
            sMessage = sMessage & "Record Row Id: " & m_TableRowID & " | " & m_DNNumberText & " | " & m_FieldName & " | "
            If m_JurisRowID > 0 Then
                sMessage = sMessage & "Jurisdiction Row Id: " & m_JurisRowID & " | "
            End If
            sMessage = sMessage & "Release: " & m_ReleaseName & " | "
            sMessage = sMessage & "Deleted By " & objUser.LoginName & " | "
            LogError(Err.Source & "|" & sClassName & "|" & sFunctionName, Erl(), 70001, Err.Source, sMessage)

            DeleteObject = -1

        Catch ex As Exception
            DeleteObject = 0
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DeleteObject = Err.Number
            objTrans.Rollback()

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            objConn = Nothing
            objConn.Dispose()
            objTrans = Nothing
            objTrans.Dispose()
        End Try

    End Function
End Class

