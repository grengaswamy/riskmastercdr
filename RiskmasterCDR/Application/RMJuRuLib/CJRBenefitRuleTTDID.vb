Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleTTDID
    Const sClassName As String = "CJRBenefitRuleTTDID"
    Const sTableName As String = "WCP_RULE_TTD_ID"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821

    Public objJurisWorkWeek As New CJRXJurisWorkWeek

    Dim m_sDateOfEventDTG As String 'jtodd22 not a Class Property

    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    Private m_DollarForDollar As Integer

    Private m_EndingDateDTG As String
    Private m_FloorAmount As Double
    Private m_HighRate As Double

    Private m_LowRate As Double
    Private m_PayFloorAmount As Integer
    Private m_PayConCurrentPPD As Integer

    Private m_MaxRatePercentSAWW As Double

    Private m_TotalAmount As Double

    Private m_UseSAWWMaximumCode As Integer
    Private m_UseSAWWMinimumCode As Integer
    Private m_ClaimantsAWWPoint As Double
    Private m_PayFixedAmount As Double

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Dim lReturn As Integer

        Try

            AssignData = 0
            ClearObject()



            m_YesCodeID = GetYesCodeID()

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                m_ClaimantsAWWPoint = objReader.GetDouble("CLAIMANT_AWW_AMT")
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_DollarForDollar = objReader.GetInt32("PAY_DOLLAR_CODE")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_EndingDateDTG = objReader.GetString("END_DATE")
                m_FloorAmount = objReader.GetDouble("MIN_BENEFIT")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_MaxCompRateWeekly = objReader.GetDouble("MAX_COMP_RATE")
                m_PayFixedAmount = objReader.GetDouble("PAY_FIXED_AMT")
                m_PayFloorAmount = objReader.GetInt32("PAY_FLOOR_CODE")
                m_PayConCurrentPPD = objReader.GetDouble("PAY_CONC_PPD_CODE")
                m_MaxRatePercentSAWW = objReader.GetDouble("MAX_BENEFIT") 'jtodd22 03/08/2005--this is Max Percent of SAWW
                m_HighRate = objReader.GetDouble("HIGH_RATE")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_LowRate = objReader.GetDouble("LOW_RATE")
                m_TotalAmount = objReader.GetDouble("TOTAL_AMT")
                m_RuleTotalWeeks = objReader.GetInt32("TOTAL_WEEKS")
                m_UseSAWWMaximumCode = objReader.GetInt32("USE_SAWW_MAX_CODE")
                m_UseSAWWMinimumCode = objReader.GetInt32("USE_SAWW_MIN_CODE")
            End While


            SafeCloseRecordset(objReader)

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            lReturn = Me.objJurisWorkWeek.LoadData(m_JurisRowID, m_EffectiveDateDTG)
            If lReturn = -1 Then
                m_JurisDefinedWorkWeek = Me.objJurisWorkWeek.JurisDefinedWorkWeek
                AssignData = -1
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Private Function GetJurisDefinedWorkWeek(ByRef lJurisRowID As Integer, ByRef sBeginDateDTG As String) As Integer
        Const sFunctionName As String = "GetJurisDefinedWorkWeek"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            GetJurisDefinedWorkWeek = 0
            m_JurisDefinedWorkWeek = 0

            sSQL = ""
            sSQL = sSQL & " SELECT JURIS_WORK_WEEK, EFFECTIVE_DATE"
            sSQL = sSQL & " FROM WCP_BEN_SWCH"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE <= '" & sBeginDateDTG & "'"
            sSQL = sSQL & " ORDER BY EFFECTIVE_DATE DESC"

            '      objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	m_JurisDefinedWorkWeek = objReader.GetInt32( "JURIS_WORK_WEEK")
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_JurisDefinedWorkWeek = objReader.GetInt32("JURIS_WORK_WEEK")
            End If


            If m_JurisDefinedWorkWeek = 0 Then m_JurisDefinedWorkWeek = 7
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetJurisDefinedWorkWeek = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : DeleteRecord
    ' DateTime  : 4/18/2006 07:43
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : lReturnCode is a number defining why the record was not deleted
    '---------------------------------------------------------------------------------------
    '
    Public Function DeleteRecord(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sEffectiveDateDBFormat As String, ByRef lReturnCode As Integer) As Integer
        Const sFunctionName As String = "DeleteRecord"

        Try

            DeleteRecord = modFunctions.DeleteRecordTemporary(objUser, lJurisRowID, sEffectiveDateDBFormat, lReturnCode, sTableName)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DeleteRecord = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public Function ValidateData() As Integer

        ValidateData = 0
        m_Warning = ""
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required."
        End If
        If m_UseSAWWMaximumCode < 1 Then
            m_Warning = m_Warning & "A 'Use SAWW Max Comp Rate' selection is required." & vbCrLf
        End If
        If m_UseSAWWMinimumCode < 1 Then
            m_Warning = m_Warning & "A 'Use Minimum From SAWW Table' selection is required." & vbCrLf
        End If


    End Function

    Public Property JurisDefinedWorkWeek() As Integer
        Get
            JurisDefinedWorkWeek = m_JurisDefinedWorkWeek
        End Get
        Set(ByVal Value As Integer)
            m_JurisDefinedWorkWeek = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property PayConCurrentPPD() As Integer
        Get
            PayConCurrentPPD = m_PayConCurrentPPD
        End Get
        Set(ByVal Value As Integer)
            m_PayConCurrentPPD = Value
        End Set
    End Property

    Public Property LowRate() As Double
        Get
            LowRate = m_LowRate
        End Get
        Set(ByVal Value As Double)
            m_LowRate = Value
        End Set
    End Property

    Public Property HighRate() As Double
        Get
            HighRate = m_HighRate
        End Get
        Set(ByVal Value As Double)
            m_HighRate = Value
        End Set
    End Property

    Public Property MaxRatePercentSAWW() As Double
        Get
            MaxRatePercentSAWW = m_MaxRatePercentSAWW
        End Get
        Set(ByVal Value As Double)
            m_MaxRatePercentSAWW = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property
    Public Property DollarForDollar() As Integer
        Get
            DollarForDollar = m_DollarForDollar
        End Get
        Set(ByVal Value As Integer)
            m_DollarForDollar = Value
        End Set
    End Property
    Public Property PayFloorAmount() As Integer
        Get
            PayFloorAmount = m_PayFloorAmount
        End Get
        Set(ByVal Value As Integer)
            m_PayFloorAmount = Value
        End Set
    End Property
    Public Property FloorAmount() As Double
        Get
            FloorAmount = m_FloorAmount
        End Get
        Set(ByVal Value As Double)
            m_FloorAmount = Value
        End Set
    End Property
    Public Property EndingDateDTG() As String
        Get
            EndingDateDTG = m_EndingDateDTG
        End Get
        Set(ByVal Value As String)
            m_EndingDateDTG = Value
        End Set
    End Property
    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property TotalAmount() As Double
        Get
            TotalAmount = m_TotalAmount
        End Get
        Set(ByVal Value As Double)
            m_TotalAmount = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property UseSAWWMaximumCode() As Integer
        Get
            UseSAWWMaximumCode = m_UseSAWWMaximumCode
        End Get
        Set(ByVal Value As Integer)
            m_UseSAWWMaximumCode = Value
        End Set
    End Property

    Public Property UseSAWWMinimumCode() As Integer
        Get
            UseSAWWMinimumCode = m_UseSAWWMinimumCode
        End Get
        Set(ByVal Value As Integer)
            m_UseSAWWMinimumCode = Value
        End Set
    End Property


    Public Property ClaimantsAWWPoint() As Double
        Get
            ClaimantsAWWPoint = m_ClaimantsAWWPoint
        End Get
        Set(ByVal Value As Double)
            m_ClaimantsAWWPoint = Value
        End Set
    End Property


    Public Property PayFixedAmount() As Double
        Get
            PayFixedAmount = m_PayFixedAmount
        End Get
        Set(ByVal Value As Double)
            m_PayFixedAmount = Value
        End Set
    End Property

    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadDataByEventDate
    ' DateTime  : 12/22/2004 10:25
    ' Author    : jtodd22
    ' Purpose   : To load data based on the Event Date, not the Rule Effective Date
    ' Note      : The Field "EFFECTIVE_DATE" must be less than or equal the Event Date.
    ' ..........: Use the record with the "EFFECTIVE_DATE" closest to the Event Date
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim lReturn As Integer
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            m_sDateOfEventDTG = sDateOfEventDTG

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            lReturn = AssignData(objUser, sSQL)
            If lReturn <> -1 Then Exit Function

            Dim objSAWW As CJRSAWW
            If m_UseSAWWMaximumCode = m_YesCodeID Or m_UseSAWWMinimumCode = m_YesCodeID Then
                objSAWW = New CJRSAWW
                lReturn = objSAWW.LoadDataByEventDate(objUser, m_JurisRowID, m_sDateOfEventDTG)
                Select Case lReturn
                    Case -1
                        If objSAWW.TableRowID = 0 Then
                            m_ErrorMaskSAWW = 8192
                            'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                            objSAWW = Nothing
                            Exit Function
                        End If
                    Case Else
                        'error should bubble up
                End Select
                If m_UseSAWWMaximumCode = m_YesCodeID Then
                    If objSAWW.PercentMax > 0 Then
                        m_MaxCompRateWeekly = objSAWW.SAWWAmount * objSAWW.PercentMax / 100
                    End If
                    If objSAWW.MaxAmountAsPublished > 0 Then
                        m_MaxCompRateWeekly = objSAWW.MaxAmountAsPublished
                    End If
                End If
                If m_UseSAWWMinimumCode = m_YesCodeID Then
                    m_FloorAmount = objSAWW.SAWWAmount * objSAWW.PercentMin
                    If objSAWW.MinAmountAsPublished > 0 Then
                        m_FloorAmount = objSAWW.MinAmountAsPublished
                    End If
                End If
                'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objSAWW = Nothing
            End If

            LoadDataByEventDate = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataLoadDataByTableRowID"
        Dim sSQL As String
        Try

            LoadDataByTableRowID = 0

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID
            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Private Function ClearObject() As Object
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_DollarForDollar = 0
        m_EndingDateDTG = ""
        m_FloorAmount = 0
        m_PayFloorAmount = 0
        m_MaxRatePercentSAWW = 0
        m_PayConCurrentPPD = 0
        m_HighRate = 0
        m_LowRate = 0
        m_TotalAmount = 0
        m_UseSAWWMaximumCode = 0
        m_UseSAWWMinimumCode = 0


    End Function

    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        SaveData = 0
        lTest = 0
        Try

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                If Not IsNothing(objReader.Item("EFFECTIVE_DATE")) Then
                    objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                End If
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                If Not IsNothing(objReader.Item("BEGIN_DATE")) Then
                    objWriter.Fields("BEGIN_DATE").Value = m_EffectiveDateDTG
                End If
                objWriter.Fields("PAY_DOLLAR_CODE").Value = m_DollarForDollar
                objWriter.Fields("END_DATE").Value = m_EndingDateDTG
                objWriter.Fields("MIN_BENEFIT").Value = m_FloorAmount
                objWriter.Fields("MAX_COMP_RATE").Value = m_MaxCompRateWeekly
                objWriter.Fields("PAY_FLOOR_CODE").Value = m_PayFloorAmount
                objWriter.Fields("MAX_BENEFIT").Value = m_MaxRatePercentSAWW
                objWriter.Fields("HIGH_RATE").Value = m_HighRate
                objWriter.Fields("LOW_RATE").Value = m_LowRate
                objWriter.Fields("TOTAL_AMT").Value = m_TotalAmount
                objWriter.Fields("TOTAL_WEEKS").Value = m_RuleTotalWeeks
                objWriter.Fields("PAY_CONC_PPD_CODE").Value = m_PayConCurrentPPD
                objWriter.Fields("USE_SAWW_MAX_CODE").Value = m_UseSAWWMaximumCode
                objWriter.Fields("USE_SAWW_MIN_CODE").Value = m_UseSAWWMinimumCode
                objWriter.Fields("CLAIMANT_AWW_AMT").Value = m_ClaimantsAWWPoint
                objWriter.Fields("PAY_FIXED_AMT").Value = m_PayFixedAmount
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                If Not IsNothing(objReader.Item("EFFECTIVE_DATE")) Then
                    objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                End If
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                If Not IsNothing(objReader.Item("BEGIN_DATE")) Then
                    objWriter.Fields.Add("BEGIN_DATE", m_EffectiveDateDTG)
                End If
                objWriter.Fields.Add("PAY_DOLLAR_CODE", m_DollarForDollar)
                objWriter.Fields.Add("END_DATE", m_EndingDateDTG)
                objWriter.Fields.Add("MIN_BENEFIT", m_FloorAmount)
                objWriter.Fields.Add("MAX_COMP_RATE", m_MaxCompRateWeekly)
                objWriter.Fields.Add("PAY_FLOOR_CODE", m_PayFloorAmount)
                objWriter.Fields.Add("MAX_BENEFIT", m_MaxRatePercentSAWW)
                objWriter.Fields.Add("HIGH_RATE", m_HighRate)
                objWriter.Fields.Add("LOW_RATE", m_LowRate)
                objWriter.Fields.Add("TOTAL_AMT", m_TotalAmount)
                objWriter.Fields.Add("TOTAL_WEEKS", m_RuleTotalWeeks)
                objWriter.Fields.Add("PAY_CONC_PPD_CODE", m_PayConCurrentPPD)
                objWriter.Fields.Add("USE_SAWW_MAX_CODE", m_UseSAWWMaximumCode)
                objWriter.Fields.Add("USE_SAWW_MIN_CODE", m_UseSAWWMinimumCode)
                objWriter.Fields.Add("CLAIMANT_AWW_AMT", m_ClaimantsAWWPoint)
                objWriter.Fields.Add("PAY_FIXED_AMT", m_PayFixedAmount)
            End If
            objWriter.Execute()

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & "SaveData" & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try


    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        'jtodd22 12/22/2004--sSQL = sSQL & ", BEGIN_DATE"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", END_DATE"
        sSQL = sSQL & ", MAX_BENEFIT" 'jtodd22 03/08/2005--this is Max Percent of SAWW
        sSQL = sSQL & ", MAX_COMP_RATE" 'jtodd22 03/08/2005--this is the Dollars per week
        sSQL = sSQL & ", MIN_BENEFIT"
        sSQL = sSQL & ", PAY_CONC_PPD_CODE"
        sSQL = sSQL & ", PAY_DOLLAR_CODE"
        sSQL = sSQL & ", PAY_FLOOR_CODE"
        sSQL = sSQL & ", HIGH_RATE"
        sSQL = sSQL & ", LOW_RATE"
        sSQL = sSQL & ", TOTAL_AMT"
        sSQL = sSQL & ", TOTAL_WEEKS"
        sSQL = sSQL & ", USE_SAWW_MAX_CODE"
        sSQL = sSQL & ", USE_SAWW_MIN_CODE"
        sSQL = sSQL & ", CLAIMANT_AWW_AMT"
        sSQL = sSQL & ", PAY_FIXED_AMT"
        GetSQLFieldList = sSQL


    End Function

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        If Not Me.objJurisWorkWeek Is Nothing Then
            'UPGRADE_NOTE: Object Me.objJurisWorkWeek may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            Me.objJurisWorkWeek = Nothing
        End If



    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

