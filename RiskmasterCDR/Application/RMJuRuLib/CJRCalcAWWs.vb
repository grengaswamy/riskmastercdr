Option Strict Off
Option Explicit On
Public Class CJRCalcAWWs
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRCalcAWWs"

    Private mCol As Collection
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 7/15/2004 16:18
    ' Author    : jtodd22
    ' Purpose   :
    ' Notes     : This function has a two step data fetch.  This function loads the collection
    '...........: for display in the ListView control.
    '...........: Create a collection of Jurisdiction IDs based TABLE_ROW_IDs from the WCP_AWW_SWCH table.
    '...........: Use the TABLE_ROW_IDs to fetch data from the table WCP_AWW_SWCH and the table
    '...........:
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim cTableRowIDs As Collection
        Dim i As Short
        Dim objReader As DbReader
        Dim lTest As Integer
        Dim sSQL As String
        Dim objAWWCalculator As CJRCalcAWW
        Try




            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " STATES.STATE_NAME"
            sSQL = sSQL & ", WCP_AWW_SWCH.TABLE_ROW_ID"
            sSQL = sSQL & ", WCP_AWW_SWCH.EFFECTIVE_DATE"
            sSQL = sSQL & " FROM STATES, WCP_AWW_SWCH"
            sSQL = sSQL & " WHERE STATES.STATE_ROW_ID = WCP_AWW_SWCH.JURIS_ROW_ID"
            sSQL = sSQL & " ORDER BY STATE_NAME,EFFECTIVE_DATE"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            cTableRowIDs = New Collection
            i = 0
            While objReader.Read()
                i = i + 1
                cTableRowIDs.Add(objReader.GetInt32("TABLE_ROW_ID"), "k" & i)

            End While

            'jtodd22 07/15/2004 be sure to close RecordSet
            SafeCloseRecordset(objReader)
            If cTableRowIDs.Count() > 0 Then
                For i = 1 To cTableRowIDs.Count()
                    objAWWCalculator = New CJRCalcAWW
                    'UPGRADE_WARNING: Couldn't resolve default property of object cTableRowIDs.Item(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    lTest = objAWWCalculator.DLLFetchData(cTableRowIDs.Item(i))
                    If lTest = -1 Then
                        With objAWWCalculator
                            Add(.TableRowID, .HoursInGrid, .DaysInGrid, .NumberOfWeeksInGrid, .AllOtherAdvanage, .TaxStatusSpendableIncome, .Meals, .GratuitiesTips, .EmployerPaidBenefitsDiscontinued, .EmployerPaidBenefitsContinued, .Domicile, .ConcurrentEmployment, .BonusCommissionsIncentive, .MonthlyCalculation, .JurisRowID, .EffectiveDateDTG, "k" & CStr(.TableRowID))
                        End With
                    End If
                Next i
            End If
            'UPGRADE_NOTE: Object cTableRowIDs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            cTableRowIDs = Nothing
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Add(ByRef TableRowID As Integer, ByRef HoursInGrid As Integer, ByRef DaysInGrid As Integer, ByRef NumberOfWeeksInGrid As Integer, ByRef AllOtherAdvanage As Integer, ByRef TaxStatusSpendableIncome As Integer, ByRef Meals As Integer, ByRef GratuitiesTips As Integer, ByRef EmployerPaidBenefitsDiscontinued As Integer, ByRef EmployerPaidBenefitsContinued As Integer, ByRef Domicile As Integer, ByRef ConcurrentEmployment As Integer, ByRef BonusCommissionsIncentive As Integer, ByRef MonthlyCalculation As Integer, ByRef JurisRowID As Integer, ByRef EffectiveDateDTG As String, Optional ByRef sKey As String = "") As CJRCalcAWW
        Try
            'create a new object
            Dim objNewMember As CJRCalcAWW
            objNewMember = New CJRCalcAWW


            'set the properties passed into the method
            objNewMember.TableRowID = TableRowID
            objNewMember.HoursInGrid = HoursInGrid
            objNewMember.DaysInGrid = DaysInGrid
            objNewMember.NumberOfWeeksInGrid = NumberOfWeeksInGrid
            objNewMember.AllOtherAdvanage = AllOtherAdvanage
            objNewMember.TaxStatusSpendableIncome = TaxStatusSpendableIncome
            objNewMember.Meals = Meals
            objNewMember.GratuitiesTips = GratuitiesTips
            objNewMember.EmployerPaidBenefitsDiscontinued = EmployerPaidBenefitsDiscontinued
            objNewMember.EmployerPaidBenefitsContinued = EmployerPaidBenefitsContinued
            objNewMember.Domicile = Domicile
            objNewMember.ConcurrentEmployment = ConcurrentEmployment
            objNewMember.BonusCommissionsIncentive = BonusCommissionsIncentive
            objNewMember.MonthlyCalculation = MonthlyCalculation
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.JurisRowID = JurisRowID
            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If


            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRCalcAWW
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

