Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleSPPDAAs
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRBenefitRuleSPPDAAs"
    Const sTableName As String = "WCP_SPPD_RULE"

    'Class properties, local copy
    Private mCol As Collection

    Public Function Add(ByRef TableRowID As Object, ByRef JurisRowID As Object, ByRef EffectiveDateDTG As Object, ByRef FixedMinimumAmount As Object, ByRef DeletedFlag As Object, ByRef MinPercentOfSAWW As Object, ByRef UseTTDRateCode As Object, ByRef UseBodyMembersCode As Object, ByRef UseImpairPercentCode As Object, ByRef FixedPercentage As Object, ByRef LesserFixedAmount As Object, ByRef LesserPercentSAWW As Object, ByRef PayConcurrentCode As Object, ByRef PercentOfMaxTTD As Object, ByRef sKey As String) As CJRBenefitRuleSPPDAA
        'create a new object
        Dim objNewMember As CJRBenefitRuleSPPDAA
        objNewMember = New CJRBenefitRuleSPPDAA

        'set the properties passed into the method
        'UPGRADE_WARNING: Couldn't resolve default property of object TableRowID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.TableRowID = TableRowID
        'UPGRADE_WARNING: Couldn't resolve default property of object JurisRowID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.JurisRowID = JurisRowID
        'UPGRADE_WARNING: Couldn't resolve default property of object EffectiveDateDTG. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.EffectiveDateDTG = EffectiveDateDTG
        'UPGRADE_WARNING: Couldn't resolve default property of object DeletedFlag. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.DeletedFlag = DeletedFlag
        'UPGRADE_WARNING: Couldn't resolve default property of object FixedMinimumAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.FixedMinimumAmount = FixedMinimumAmount
        'UPGRADE_WARNING: Couldn't resolve default property of object FixedPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.FixedPercentage = FixedPercentage
        'UPGRADE_WARNING: Couldn't resolve default property of object MinPercentOfSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.MinPercentOfSAWW = MinPercentOfSAWW
        'UPGRADE_WARNING: Couldn't resolve default property of object UseBodyMembersCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.UseBodyMembersCode = UseBodyMembersCode
        'UPGRADE_WARNING: Couldn't resolve default property of object UseImpairPercentCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.UseImpairPercentCode = UseImpairPercentCode
        'UPGRADE_WARNING: Couldn't resolve default property of object UseTTDRateCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.UseTTDRateCode = UseTTDRateCode
        'UPGRADE_WARNING: Couldn't resolve default property of object LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.LesserFixedAmount = LesserFixedAmount
        'UPGRADE_WARNING: Couldn't resolve default property of object LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.LesserPercentSAWW = LesserPercentSAWW
        'UPGRADE_WARNING: Couldn't resolve default property of object PayConcurrentCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.PayConcurrentCode = PayConcurrentCode
        'UPGRADE_WARNING: Couldn't resolve default property of object PercentOfMaxTTD. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objNewMember.PercentOfMaxTTD = PercentOfMaxTTD
        mCol.Add(objNewMember, sKey)


        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing




    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleSPPDAA
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByVal lJurisID As Integer) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRule As CJRBenefitRuleSPPDAA
        Try

            LoadData = 0
            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " WCP_SPPD_RULE.TABLE_ROW_ID"
            sSQL = sSQL & ", JURIS_ROW_ID"
            sSQL = sSQL & ", EFFECTIVE_DATE, DELETED_FLAG"
            sSQL = sSQL & ", MIN_PERCENT_SAWW"
            sSQL = sSQL & ", USE_TTDRATE_CODE,USE_BODYMEMBERCODE"
            sSQL = sSQL & ", USE_IMPARPRCT_CODE,FIXED_PERCT_NUMB"
            sSQL = sSQL & ", LESSERFIXED_AMOUNT, LESSERPERCENT_SAWW,PERCENT_OF_MAX_TTD"
            sSQL = sSQL & ", CONCURRENT_CODE"
            sSQL = sSQL & ", FIX_MINIMUM_AMT"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisID

            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	While objReader.Read()
            '		objRule = New CJRBenefitRuleSPPDAA
            '		With objRule
            '			.DeletedFlag = objReader.GetInt32( "DELETED_FLAG")
            '			.EffectiveDateDTG = Trim(objReader.GetString( "EFFECTIVE_DATE"))
            '			.FixedMinimumAmount = objReader.GetDouble( "FIX_MINIMUM_AMT")
            '			.JurisRowID = objReader.GetInt32( "JURIS_ROW_ID")
            '			.TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            '			.MinPercentOfSAWW = objReader.GetDouble( "MIN_PERCENT_SAWW")
            '			.UseTTDRateCode = objReader.GetInt32( "USE_TTDRATE_CODE")
            '			.UseBodyMembersCode = objReader.GetInt32( "USE_BODYMEMBERCODE")
            '			.UseImpairPercentCode = objReader.GetInt32( "USE_IMPARPRCT_CODE")
            '			.FixedPercentage = objReader.GetDouble( "FIXED_PERCT_NUMB")
            '			.LesserFixedAmount = objReader.GetDouble( "LESSERFIXED_AMOUNT")
            '			.LesserPercentSAWW = objReader.GetDouble( "LESSERPERCENT_SAWW")
            '			.PayConcurrentCode = objReader.GetInt32( "CONCURRENT_CODE")
            '			.PercentOfMaxTTD = objReader.GetDouble( "PERCENT_OF_MAX_TTD")
            '			Add(.TableRowID, .JurisRowID, .EffectiveDateDTG, .FixedMinimumAmount, .DeletedFlag, .MinPercentOfSAWW, .UseTTDRateCode, .UseBodyMembersCode, .UseImpairPercentCode, .FixedPercentage, .LesserFixedAmount, .LesserPercentSAWW, .PayConcurrentCode, .PercentOfMaxTTD, "k" & CStr(.TableRowID))
            '		End With
            '		'UPGRADE_NOTE: Object objRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            '		objRule = Nothing
            '		
            '	End While
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRule = New CJRBenefitRuleSPPDAA
                With objRule
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
                    .FixedMinimumAmount = objReader.GetDouble("FIX_MINIMUM_AMT")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .MinPercentOfSAWW = objReader.GetDouble("MIN_PERCENT_SAWW")
                    .UseTTDRateCode = objReader.GetInt32("USE_TTDRATE_CODE")
                    .UseBodyMembersCode = objReader.GetInt32("USE_BODYMEMBERCODE")
                    .UseImpairPercentCode = objReader.GetInt32("USE_IMPARPRCT_CODE")
                    .FixedPercentage = objReader.GetDouble("FIXED_PERCT_NUMB")
                    .LesserFixedAmount = objReader.GetDouble("LESSERFIXED_AMOUNT")
                    .LesserPercentSAWW = objReader.GetDouble("LESSERPERCENT_SAWW")
                    .PayConcurrentCode = objReader.GetInt32("CONCURRENT_CODE")
                    .PercentOfMaxTTD = objReader.GetDouble("PERCENT_OF_MAX_TTD")
                    Add(.TableRowID, .JurisRowID, .EffectiveDateDTG, .FixedMinimumAmount, .DeletedFlag, .MinPercentOfSAWW, .UseTTDRateCode, .UseBodyMembersCode, .UseImpairPercentCode, .FixedPercentage, .LesserFixedAmount, .LesserPercentSAWW, .PayConcurrentCode, .PercentOfMaxTTD, "k" & CStr(.TableRowID))
                End With
                'UPGRADE_NOTE: Object objRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRule = Nothing
            End While

            SafeCloseRecordset(objReader)
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
End Class

