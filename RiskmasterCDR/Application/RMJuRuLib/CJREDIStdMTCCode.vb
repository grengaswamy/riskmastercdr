Option Strict Off
Option Explicit On
Public Class CJREDIStdMTCCode
    '---------------------------------------------------------------------------------------
    ' Module    : CJREDIStdMTCCode
    ' DateTime  : 8/24/2005 11:52
    ' Author    : Venkata Mandayam
    ' Purpose   : Maintains the EDI Standard MTC Codes.
    '---------------------------------------------------------------------------------------
    Const sClassName As String = "CJREDIStdMTCCode"
    Const sTableName2 As String = "EDI_STDMTC_CODES"
    Const sTableName3 As String = "WCP_STDMTC_CODES"

    'local variable(s) to hold property value(s)
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_MTCCode As String
    Private m_MTCCodeText As String
    Private m_ReleaseRowID As Integer
    Private m_JurisReleaseRowID As Integer
    Private m_JurisRowID As Integer
    Private m_TableRowID As Integer
    Private m_MTCTableRowID As Integer

    Private Function ClearObject() As Integer
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_MTCCode = ""
        m_MTCCodeText = ""
        m_JurisRowID = 0
        m_ReleaseRowID = 0
        m_JurisReleaseRowID = 0
        m_TableRowID = 0


    End Function

    Private Function GetFieldSelect() As String
        Dim sTable As String
        Dim sSQL As String
        Dim sJurisSQL As String

        GetFieldSelect = ""
        If m_JurisReleaseRowID > 0 Then
            sTable = sTableName3 & "."
            sJurisSQL = ""
            sJurisSQL = sJurisSQL & ", " & sTableName3 & ".JURISRELEASE_ROW_ID"
            sJurisSQL = sJurisSQL & ", " & sTableName3 & ".MTC_TABLE_ROW_ID"
        Else
            sTable = sTableName2 & "."
        End If
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " " & sTable & "TABLE_ROW_ID"
        sSQL = sSQL & ", " & sTable & "ADDED_BY_USER"
        sSQL = sSQL & ", " & sTable & "DTTM_RCD_ADDED"
        sSQL = sSQL & ", " & sTable & "UPDATED_BY_USER"
        sSQL = sSQL & ", " & sTable & "DTTM_RCD_LAST_UPD"
        sSQL = sSQL & ", " & sTable & "MTC_CODE"
        sSQL = sSQL & ", " & sTable & "MTC_CODE_TEXT"
        sSQL = sSQL & ", " & sTable & "RELEASE_ROW_ID"
        sSQL = sSQL & ", " & sTable & "DELETED_FLAG"
        sSQL = sSQL & sJurisSQL

        GetFieldSelect = sSQL


    End Function

    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer, ByRef lReleaseRowID As Integer, ByRef lJurisReleaseRowId As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0
            ClearObject()
            m_JurisReleaseRowID = lJurisReleaseRowId


            sSQL = GetFieldSelect()
            If m_JurisReleaseRowID > 0 Then
                sSQL = sSQL & " FROM " & sTableName3
                sSQL = sSQL & " AND " & sTableName3 & ".TABLE_ROW_ID = " & lTableRowID
                sSQL = sSQL & " AND " & sTableName3 & ".DELETED_FLAG = 0"
            Else
                sSQL = sSQL & " FROM " & sTableName2
                sSQL = sSQL & " WHERE " & sTableName2 & ".TABLE_ROW_ID = " & lTableRowID
                sSQL = sSQL & " AND " & sTableName2 & ".DELETED_FLAG = 0"
            End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then

                m_DataHasChanged = False
                m_MTCCode = Trim(objReader.GetString("MTC_CODE"))
                m_MTCCodeText = Trim(objReader.GetString("MTC_CODE_TEXT"))
                m_ReleaseRowID = objReader.GetInt32("RELEASE_ROW_ID")

                If Not IsNothing(objReader.Item("JURISRELEASE_ROW_ID")) Then
                    m_JurisReleaseRowID = objReader.GetInt32("JURISRELEASE_ROW_ID")
                Else
                    m_JurisReleaseRowID = 0
                End If

                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")

            End If

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            DBDisconnect()
        End Try

    End Function
    Public Function SaveData(ByRef objuser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0


            sSQL = GetFieldSelect()
            If m_JurisReleaseRowID > 0 Then
                sSQL = sSQL & " FROM " & sTableName3
                If m_TableRowID > 0 Then
                    sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID
                Else
                    sSQL = sSQL & " WHERE MTC_TABLE_ROW_ID = " & m_MTCTableRowID
                    sSQL = sSQL & " AND JURISRELEASE_ROW_ID = " & m_JurisReleaseRowID
                End If
            Else
                sSQL = sSQL & " FROM " & sTableName2
                sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID
            End If


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objuser.LoginName

                objWriter.Fields("MTC_CODE").Value = m_MTCCode
                objWriter.Fields("MTC_CODE_TEXT").Value = m_MTCCodeText
                objWriter.Fields("RELEASE_ROW_ID").Value = m_ReleaseRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag

                If Not IsNothing(objReader.Item("JURISRELEASE_ROW_ID")) Then
                    objWriter.Fields("JURISRELEASE_ROW_ID").Value = m_JurisReleaseRowID
                End If

                If Not IsNothing(objReader.Item("MTC_TABLE_ROW_ID")) Then
                    objWriter.Fields("MTC_TABLE_ROW_ID").Value = m_MTCTableRowID
                End If
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                If m_JurisReleaseRowID > 0 Then
                    m_TableRowID = lGetNextUID(sTableName3)
                Else
                    m_TableRowID = lGetNextUID(sTableName2)
                End If
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objuser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objuser.LoginName

                objWriter.Fields.Add("MTC_CODE", m_MTCCode)
                objWriter.Fields.Add("MTC_CODE_TEXT", m_MTCCodeText)
                objWriter.Fields.Add("RELEASE_ROW_ID", m_ReleaseRowID)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)

                If Not IsNothing(objReader.Item("JURISRELEASE_ROW_ID")) Then
                    objWriter.Fields.Add("JURISRELEASE_ROW_ID", m_JurisReleaseRowID)
                End If

                If Not IsNothing(objReader.Item("MTC_TABLE_ROW_ID")) Then
                    objWriter.Fields.Add("MTC_TABLE_ROW_ID", m_MTCTableRowID)
                End If
            End If
            SafeCloseRecordset(objReader)
            objWriter.Execute()
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property MTCCodeText() As String
        Get
            MTCCodeText = m_MTCCodeText
        End Get
        Set(ByVal Value As String)
            m_MTCCodeText = Value
        End Set
    End Property

    Public Property MTCCode() As String
        Get
            MTCCode = m_MTCCode
        End Get
        Set(ByVal Value As String)
            m_MTCCode = Value
        End Set
    End Property

    Public Property JurisReleaseRowId() As Integer
        Get
            JurisReleaseRowId = m_JurisReleaseRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisReleaseRowID = Value
        End Set
    End Property

    Public Property ReleaseRowID() As Integer
        Get
            ReleaseRowID = m_ReleaseRowID
        End Get
        Set(ByVal Value As Integer)
            m_ReleaseRowID = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property MTCTableRowID() As Integer
        Get
            MTCTableRowID = m_MTCTableRowID
        End Get
        Set(ByVal Value As Integer)
            m_MTCTableRowID = Value
        End Set
    End Property

    Public Property DeletedFlag() As Boolean
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Boolean)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Function DeleteObject(ByRef objuser As Riskmaster.Security.UserLogin) As Short

        Const sFunctionName As String = "DeleteObject"
        Dim sSQL As String
        Dim sMessage As String
        Dim objDbConnection As DbConnection
        Dim objDbTransaction As DbTransaction

        Try

            objDbConnection = DbFactory.GetDbConnection(g_ConnectionString)
            objDbConnection.Open()

            DeleteObject = 0




            'Delete only if it is a Industry standard MTC code. There is another module to delete Jurisdictional MTC codes
            If m_JurisReleaseRowID <= 0 Then
                sSQL = "DELETE FROM " & sTableName2 & " WHERE TABLE_ROW_ID=" & m_TableRowID
            End If

            objDbConnection.ExecuteNonQuery(sSQL, objDbTransaction)

            'Log the Deleled Record
            sMessage = "MTC Code Deleted" & " | "
            sMessage = sMessage & "Record Row Id: " & m_TableRowID & " | " & m_MTCCode & " | " & m_MTCCodeText & " | "
            '''    If m_JurisRowID > 0 Then
            '''        sMessage = sMessage & "Jurisdiction Row Id: " & m_JurisRowID & " | "
            '''    End If
            sMessage = sMessage & "Release Row Id: " & m_ReleaseRowID & " | "
            sMessage = sMessage & "Deleted By " & objuser.LoginName & " | "
            LogError(Err.Source & "|" & sClassName & "|" & sFunctionName, Erl(), 70001, Err.Source, sMessage)

            DeleteObject = -1

        Catch ex As Exception
            DeleteObject = 0
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DeleteObject = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function
End Class

