Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleSuppIncome
    Const sClassName As String = "CJRBenefitRuleSuppIncome"
    Const sTableName As String = "WCP_RULE_SIB"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821
    Dim msDateOfEventDTG As String
    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    'local variable(s) to hold property value(s)
    Private m_AnnualIncrease As Double
    Private m_AWWMultiplierPercentage As Double
    Private m_EarningsPermittedCode As Integer
    Private m_ImpairmentRate As Double
    Private m_MaxRateAmountMonthly As Double
    Private m_MaxRateAmountWeekly As Double
    Private m_MaxSAWWPercentMonthly As Double
    Private m_MaxSAWWPercentWeekly As Double
    Private m_MinRateAmount As Double
    Private m_MinSawwPercent As Double
    Private m_WorkLossPercentage As Double
    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()


            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then

            '	m_EffectiveDateDTG = objReader.GetString( "EFFECTIVE_DATE")
            '	m_JurisRowID = objReader.GetInt32( "JURIS_ROW_ID")
            '	m_ImpairmentRate = objReader.GetDouble( "IMPAIRMENT_RATE")
            '	m_MaxRateAmountMonthly = objReader.GetDouble( "MAX_MONTHLY_RATE")
            '	m_MaxSAWWPercentMonthly = objReader.GetDouble( "MAX_MONTHLY_SAWW")
            '	m_MaxRateAmountWeekly = objReader.GetDouble( "MAX_AMOUNT")
            '	m_WorkLossPercentage = objReader.GetInt32( "MAX_OF_AWW")
            '	m_MaxSAWWPercentWeekly = objReader.GetDouble( "MAX_SAWW_PERCENT")
            '	m_MinRateAmount = objReader.GetDouble( "MIN_AMOUNT")
            '	m_MinSawwPercent = objReader.GetDouble( "MIN_SAWW_PERCENT")
            '	m_PayPeriodCode = objReader.GetInt32( "PAY_PERIOD_CODE")
            '	m_PayPeriodLockedInCalcCode = objReader.GetInt32( "PAY_PERD_LOCK_CODE")
            '	m_AWWMultiplierPercentage = objReader.GetDouble( "STANDARD_PERCENT")
            '	m_TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            '	m_EarningsPermittedCode = objReader.GetInt32( "ERNNGS_PRMTED_CODE")
            '	m_WeekToMonthConvFactor = objReader.GetDouble( "WEEKMONTH_CONVFACT")
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_ImpairmentRate = objReader.GetDouble("IMPAIRMENT_RATE")
                m_MaxRateAmountMonthly = objReader.GetDouble("MAX_MONTHLY_RATE")
                m_MaxSAWWPercentMonthly = objReader.GetDouble("MAX_MONTHLY_SAWW")
                m_MaxRateAmountWeekly = objReader.GetDouble("MAX_AMOUNT")
                m_WorkLossPercentage = objReader.GetInt32("MAX_OF_AWW")
                m_MaxSAWWPercentWeekly = objReader.GetDouble("MAX_SAWW_PERCENT")
                m_MinRateAmount = objReader.GetDouble("MIN_AMOUNT")
                m_MinSawwPercent = objReader.GetDouble("MIN_SAWW_PERCENT")
                m_PayPeriodCode = objReader.GetInt32("PAY_PERIOD_CODE")
                m_PayPeriodLockedInCalcCode = objReader.GetInt32("PAY_PERD_LOCK_CODE")
                m_AWWMultiplierPercentage = objReader.GetDouble("STANDARD_PERCENT")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_EarningsPermittedCode = objReader.GetInt32("ERNNGS_PRMTED_CODE")
                m_WeekToMonthConvFactor = objReader.GetDouble("WEEKMONTH_CONVFACT")
            End While



            SafeCloseRecordset(objReader)

            If m_EarningsPermittedCode = 0 Then m_EarningsPermittedCode = modFunctions.GetYesCodeID

            If m_MaxRateAmountMonthly > 0 Then m_MaxCompRateMonthly = m_MaxRateAmountMonthly
            If m_MaxRateAmountWeekly > 0 Then m_MaxCompRateWeekly = m_MaxRateAmountWeekly
            If m_MaxSAWWPercentMonthly > 0 Then m_MaxCompRateMonthly = GetMaxCompRateWeekly(m_MaxSAWWPercentMonthly / 100)
            If m_MaxSAWWPercentWeekly > 0 Then m_MaxCompRateWeekly = GetMaxCompRateWeekly(m_MaxSAWWPercentWeekly / 100)

            If m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault Then m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_MaxRateAmountMonthly = 0
        m_MaxRateAmountWeekly = 0
        m_MaxSAWWPercentMonthly = 0
        m_MaxSAWWPercentWeekly = 0
        m_MinRateAmount = 0
        m_MinSawwPercent = 0
        m_AWWMultiplierPercentage = 0
        m_ImpairmentRate = 0
        m_WorkLossPercentage = 0


    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", MAX_MONTHLY_RATE"
        sSQL = sSQL & ", MAX_MONTHLY_SAWW"
        sSQL = sSQL & ", MAX_AMOUNT"
        sSQL = sSQL & ", MAX_SAWW_PERCENT"
        sSQL = sSQL & ", MIN_AMOUNT"
        sSQL = sSQL & ", MIN_SAWW_PERCENT"
        sSQL = sSQL & ", STANDARD_PERCENT"

        sSQL = sSQL & ", IMPAIRMENT_RATE"
        sSQL = sSQL & ", MAX_OF_AWW"
        sSQL = sSQL & ", PAY_PERIOD_CODE"

        sSQL = sSQL & ", ERNNGS_PRMTED_CODE"
        sSQL = sSQL & ", WEEKMONTH_CONVFACT"
        sSQL = sSQL & ", PAY_PERD_LOCK_CODE"
        sSQL = sSQL & ", USE_TWOTHIRDS_CODE"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim sSQL As String
        Try
            LoadDataByTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try
            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            msDateOfEventDTG = sDateOfEventDTG

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try
            SaveData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT *"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)

                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("MAX_MONTHLY_RATE").Value = m_MaxRateAmountMonthly
                objWriter.Fields("MAX_MONTHLY_SAWW").Value = m_MaxSAWWPercentMonthly
                objWriter.Fields("MAX_AMOUNT").Value = m_MaxRateAmountWeekly
                objWriter.Fields("MAX_SAWW_PERCENT").Value = m_MaxSAWWPercentWeekly
                objWriter.Fields("MIN_AMOUNT").Value = m_MinRateAmount
                objWriter.Fields("MIN_SAWW_PERCENT").Value = m_MinSawwPercent
                objWriter.Fields("STANDARD_PERCENT").Value = m_AWWMultiplierPercentage
                objWriter.Fields("IMPAIRMENT_RATE").Value = m_ImpairmentRate
                objWriter.Fields("MAX_OF_AWW").Value = m_WorkLossPercentage
                objWriter.Fields("PAY_PERIOD_CODE").Value = m_PayPeriodCode
                objWriter.Fields("PAY_PERD_LOCK_CODE").Value = m_PayPeriodLockedInCalcCode
                objWriter.Fields("WEEKMONTH_CONVFACT").Value = m_WeekToMonthConvFactor
                objWriter.Fields("ERNNGS_PRMTED_CODE").Value = m_EarningsPermittedCode
                objWriter.Execute()

            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("MAX_MONTHLY_RATE", m_MaxRateAmountMonthly)
                objWriter.Fields.Add("MAX_MONTHLY_SAWW", m_MaxSAWWPercentMonthly)
                objWriter.Fields.Add("MAX_AMOUNT", m_MaxRateAmountWeekly)
                objWriter.Fields.Add("MAX_SAWW_PERCENT", m_MaxSAWWPercentWeekly)
                objWriter.Fields.Add("MIN_AMOUNT", m_MinRateAmount)
                objWriter.Fields.Add("MIN_SAWW_PERCENT", m_MinSawwPercent)
                objWriter.Fields.Add("STANDARD_PERCENT", m_AWWMultiplierPercentage)
                objWriter.Fields.Add("IMPAIRMENT_RATE", m_ImpairmentRate)
                objWriter.Fields.Add("MAX_OF_AWW", m_WorkLossPercentage)
                objWriter.Fields.Add("PAY_PERIOD_CODE", m_PayPeriodCode)
                objWriter.Fields.Add("PAY_PERD_LOCK_CODE", m_PayPeriodLockedInCalcCode)
                objWriter.Fields.Add("WEEKMONTH_CONVFACT", m_WeekToMonthConvFactor)
                objWriter.Fields.Add("ERNNGS_PRMTED_CODE", m_EarningsPermittedCode)
                objWriter.Execute()

            End If

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try


    End Function

    Public Function ValidateData() As Integer
        Dim sMessage As String
        ValidateData = CInt("")
        If Len(m_EffectiveDateDTG) <> 8 Then
            sMessage = sMessage & "A valid Effective Date is required." & vbCrLf
        Else
            If m_EffectiveDateDTG < "19100101" Then
                sMessage = sMessage & "A valid Effective Date is required, Workers' Compensation did not exist before 1910." & vbCrLf
            End If
        End If
        If m_ImpairmentRate = 0 Then
            sMessage = sMessage & "A 'Minimum Required Body Impairment Rating' is required." & vbCrLf
        End If
        If m_WorkLossPercentage = 0 Then
            sMessage = sMessage & "A 'Maximum Replacement of Work Loss' is required." & vbCrLf
        End If
        If m_AWWMultiplierPercentage = 0 Then
            sMessage = sMessage & "A 'Maximum Percentage Of AWW To Consider' is required." & vbCrLf
        End If
        If m_MaxRateAmountMonthly > 0 And m_MaxSAWWPercentMonthly > 0 Then
            sMessage = sMessage & "For Maximum Monthly Rates, only one of the two choices can have a value greater than 0 (zero)." & vbCrLf
        End If
        If m_MaxRateAmountWeekly > 0 And m_MaxSAWWPercentWeekly > 0 Then
            sMessage = sMessage & "For Maximum Weekly Rates, only one of the two choices can have a value greater than 0 (zero)." & vbCrLf
        End If
        If m_MinRateAmount > 0 And m_MinSawwPercent > 0 Then
            sMessage = sMessage & "For Minimum Rates, only one of the two choices can have a value greater than 0 (zero)." & vbCrLf
        End If
        ValidateData = CInt(sMessage)



    End Function

    Public Property AnnualIncrease() As Double
        Get
            AnnualIncrease = m_AnnualIncrease
        End Get
        Set(ByVal Value As Double)
            m_AnnualIncrease = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property ImpairmentRate() As Double
        Get
            ImpairmentRate = m_ImpairmentRate
        End Get
        Set(ByVal Value As Double)
            m_ImpairmentRate = Value
        End Set
    End Property

    Public Property MaxRateAmountMonthly() As Double
        Get
            MaxRateAmountMonthly = m_MaxRateAmountMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxRateAmountMonthly = Value
        End Set
    End Property

    Public Property MaxRateAmountWeekly() As Double
        Get
            MaxRateAmountWeekly = m_MaxRateAmountWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxRateAmountWeekly = Value
        End Set
    End Property

    Public Property MaxSAWWPercentMonthly() As Double
        Get
            MaxSAWWPercentMonthly = m_MaxSAWWPercentMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxSAWWPercentMonthly = Value
        End Set
    End Property

    Public Property MaxSawwPercentWeekly() As Double
        Get
            MaxSawwPercentWeekly = m_MaxSAWWPercentWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxSAWWPercentWeekly = Value
        End Set
    End Property

    Public Property MinRateAmount() As Double
        Get
            MinRateAmount = m_MinRateAmount
        End Get
        Set(ByVal Value As Double)
            m_MinRateAmount = Value
        End Set
    End Property


    Public Property MinSawwPercent() As Double
        Get
            MinSawwPercent = m_MinSawwPercent
        End Get
        Set(ByVal Value As Double)
            m_MinSawwPercent = Value
        End Set
    End Property


    Public Property PayPeriodCode() As Integer
        Get
            PayPeriodCode = m_PayPeriodCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodCode = Value
        End Set
    End Property

    Public Property AWWMultiplierPercentage() As Double
        Get
            AWWMultiplierPercentage = m_AWWMultiplierPercentage
        End Get
        Set(ByVal Value As Double)
            m_AWWMultiplierPercentage = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property

    Public Property WorkLossPercentage() As Double
        Get
            WorkLossPercentage = m_WorkLossPercentage
        End Get
        Set(ByVal Value As Double)
            m_WorkLossPercentage = Value
        End Set
    End Property

    Public Property EarningsPermittedCode() As Integer
        Get
            EarningsPermittedCode = m_EarningsPermittedCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsPermittedCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property

    Public Property WeekToMonthConvFactor() As Double
        Get
            WeekToMonthConvFactor = m_WeekToMonthConvFactor
        End Get
        Set(ByVal Value As Double)
            m_WeekToMonthConvFactor = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property

    Private Function GetMaxCompRateWeekly(ByRef dMultipler As Double) As Double
        Dim dSAWWAmount As Double
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL2 As String

        Try

            dSAWWAmount = 0
            m_ErrorMaskSAWW = modGlobals.DisBenCalcMask.dbcSuccess

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & m_JurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & msDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM WCP_SAWW_LKUP"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & m_JurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	dSAWWAmount = objReader.GetDouble( "SAWW_AMOUNT")
            'Else
            '	m_ErrorMaskSAWW = modGlobals.DisBenCalcMask.dbcNoJurisdictionalRule
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                dSAWWAmount = objReader.GetDouble("SAWW_AMOUNT")
            Else
                m_ErrorMaskSAWW = modGlobals.DisBenCalcMask.dbcNoJurisdictionalRule
            End If

            GetMaxCompRateWeekly = System.Math.Round(dSAWWAmount * (m_MaxSAWWPercentWeekly / 100), 2)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".GetMaxCompRateWeekly|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
End Class

