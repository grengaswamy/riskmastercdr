Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleImpairIncome

    Const sClassName As String = "CJRBenefitRuleImpairIncome"
    Const sTableName As String = "WCP_RULE_IIB"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821
    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    Private m_MaxAmountPerWeek As Double
    Private m_MaxPercentOfSAWW As Double
    Private m_MaxTotalBenefitAmount As Double
    Private m_MaxWeeks As Double
    Private m_MinAmountPerWeek As Double
    Private m_MinPercentOfSAWW As Double
    Private m_MMIDateRequired As Integer
    '       123456789012345678901234567890
    Private m_MMIDateBenefitStartAfterCode As Integer
    Private m_MMIDateBenefitStartOnCode As Integer
    Private m_PayAfterAllTemporary As Integer
    Private m_PayLengthOfDisability As Integer
    Private m_PayNaturalLife As Integer
    Private m_PayMaxWeeks As Integer
    Private m_PercentageOfAWW As Double
    Private m_UseCurrentTTDRules As Integer
    Private m_WeeksPerPercentagePoint As Double

    Private m_EventDateDTG As String 'Input data, not Jurisdictional Rule
    Private m_MinCompRate As Double 'Calculated value, not Jurisdictional Rule


    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_DataHasChanged = False
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_MaxAmountPerWeek = objReader.GetDouble("MAX_AMTPERWEEK")
                m_MaxPercentOfSAWW = objReader.GetDouble("MAX_PERCENTOFSAWW")
                m_MaxTotalBenefitAmount = objReader.GetDouble("MAX_TALBENEFITAMT")
                m_MaxWeeks = objReader.GetInt32("MAX_WEEKS")
                m_MinAmountPerWeek = objReader.GetDouble("MIN_AMOUNTPERWEEK")
                m_MinPercentOfSAWW = objReader.GetDouble("MIN_PERCENTOFSAWW")
                m_MMIDateRequired = objReader.GetInt32("MMI_DATEREQUD_CODE")
                m_PayAfterAllTemporary = objReader.GetInt32("PAY_AFTERTEMP_CODE")
                m_PayLengthOfDisability = objReader.GetInt32("PAY_NATALLIFE_CODE")
                m_PayNaturalLife = objReader.GetInt32("PAY_NATALLIFE_CODE")
                m_PayMaxWeeks = objReader.GetInt32("PAY_MAXWEEKS")
                m_PayPeriodCode = objReader.GetInt32("PAY_PERIOD_CODE")
                m_PercentageOfAWW = objReader.GetDouble("PERCENT_OF_AWW")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_UseCurrentTTDRules = objReader.GetInt32("USE_TTDRULES_CODE")
                m_WeeksPerPercentagePoint = objReader.GetDouble("WEEKS_PER_POINT")
                m_MMIDateBenefitStartAfterCode = objReader.GetInt32("MMI_DATE_AFTR_CODE")
                m_MMIDateBenefitStartOnCode = objReader.GetInt32("MMI_DATE_ON_CODE")
            End If
            SafeCloseRecordset(objReader)

            If m_EventDateDTG > "" And Len(m_EventDateDTG) = 8 Then
                GetMaxMinCompRate(objUser)
            End If

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_MaxAmountPerWeek = 0
        m_MaxPercentOfSAWW = 0
        m_MaxTotalBenefitAmount = 0
        m_MaxWeeks = 0
        m_MinAmountPerWeek = 0
        m_MinPercentOfSAWW = 0
        m_MMIDateRequired = 0
        m_MMIDateBenefitStartAfterCode = 0
        m_MMIDateBenefitStartOnCode = 0
        m_PayAfterAllTemporary = 0
        m_PayLengthOfDisability = 0
        m_PayNaturalLife = 0
        m_PayMaxWeeks = 0
        m_UseCurrentTTDRules = 0
        m_WeeksPerPercentagePoint = 0

        m_MaxCompRateWeekly = 0
        m_MinCompRate = 0


    End Function

    Private Function GetMaxMinCompRate(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "GetMaxMinCompRate"
        Dim objReader As DbReader
        Dim lReturn As Integer
        Dim objSAWW As CJRSAWW
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            GetMaxMinCompRate = 0
            If m_MaxAmountPerWeek > 0 Then m_MaxCompRateWeekly = m_MaxAmountPerWeek
            If m_MinAmountPerWeek > 0 Then m_MinCompRate = m_MinAmountPerWeek
            If m_MaxPercentOfSAWW > 0 Or m_MinPercentOfSAWW > 0 Then
                objSAWW = New CJRSAWW
                lReturn = objSAWW.LoadDataByEventDate(objUser, m_JurisRowID, m_EventDateDTG)
                Select Case lReturn
                    Case -1
                        If objSAWW.TableRowID > 0 Then
                            If m_MaxPercentOfSAWW > 0 Then
                                m_MaxCompRateWeekly = System.Math.Round((m_MaxPercentOfSAWW / 100) * objSAWW.SAWWAmount, 2)
                            End If
                            If m_MinPercentOfSAWW > 0 Then
                                m_MinCompRate = System.Math.Round((m_MinPercentOfSAWW / 100) * objSAWW.SAWWAmount, 2)
                            End If
                        Else
                            'error--missing data
                            g_sErrDescription = "No valid data for SAWW was found."
                            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
                            g_lErrLine = Erl()
                            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
                        End If
                    Case Else
                        'error--data fetch
                End Select
                'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objSAWW = Nothing
            End If
            GetMaxMinCompRate = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & " " & Err.Description
            End With
            GetMaxMinCompRate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",EFFECTIVE_DATE"
        sSQL = sSQL & ",DELETED_FLAG"
        sSQL = sSQL & ",MAX_AMTPERWEEK"
        sSQL = sSQL & ",MAX_PERCENTOFSAWW"
        sSQL = sSQL & ",MAX_TALBENEFITAMT"
        sSQL = sSQL & ",MAX_WEEKS"
        sSQL = sSQL & ",MIN_AMOUNTPERWEEK"
        sSQL = sSQL & ",MIN_PERCENTOFSAWW"
        sSQL = sSQL & ",MMI_DATEREQUD_CODE"
        sSQL = sSQL & ",PAY_AFTERTEMP_CODE"
        sSQL = sSQL & ",PAY_LEN_DISAB_CODE"
        sSQL = sSQL & ",PAY_NATALLIFE_CODE"
        sSQL = sSQL & ",PAY_MAXWEEKS"
        sSQL = sSQL & ",PAY_PERIOD_CODE"
        sSQL = sSQL & ",PERCENT_OF_AWW"
        sSQL = sSQL & ",USE_TTDRULES_CODE"
        sSQL = sSQL & ",WEEKS_PER_POINT"
        sSQL = sSQL & ",MMI_DATE_ON_CODE"
        sSQL = sSQL & ",MMI_DATE_AFTR_CODE"

        GetSQLFieldList = sSQL


    End Function
    Public Function LoadDataTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataTableRowID"
        Dim sSQL As String
        Try
            LoadDataTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function
            m_EventDateDTG = ""

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try
            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function
            m_EventDateDTG = sDateOfEventDTG

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String

        Try
            SaveData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT *"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("MAX_AMTPERWEEK").Value = m_MaxAmountPerWeek
                objWriter.Fields("MAX_PERCENTOFSAWW").Value = m_MaxPercentOfSAWW
                objWriter.Fields("MAX_TALBENEFITAMT").Value = m_MaxTotalBenefitAmount
                objWriter.Fields("MAX_WEEKS").Value = m_MaxWeeks
                objWriter.Fields("MIN_AMOUNTPERWEEK").Value = m_MinAmountPerWeek
                objWriter.Fields("MIN_PERCENTOFSAWW").Value = m_MinPercentOfSAWW
                objWriter.Fields("MMI_DATEREQUD_CODE").Value = m_MMIDateRequired
                objWriter.Fields("MMI_DATE_AFTR_CODE").Value = m_MMIDateBenefitStartAfterCode
                objWriter.Fields("MMI_DATE_ON_CODE").Value = m_MMIDateBenefitStartOnCode
                objWriter.Fields("PAY_AFTERTEMP_CODE").Value = m_PayAfterAllTemporary
                objWriter.Fields("PAY_LEN_DISAB_CODE").Value = m_PayLengthOfDisability
                objWriter.Fields("PAY_NATALLIFE_CODE").Value = m_PayNaturalLife
                objWriter.Fields("PAY_MAXWEEKS").Value = m_PayMaxWeeks
                objWriter.Fields("PAY_PERIOD_CODE").Value = m_PayPeriodCode
                objWriter.Fields("PERCENT_OF_AWW").Value = m_PercentageOfAWW
                objWriter.Fields("USE_TTDRULES_CODE").Value = m_UseCurrentTTDRules
                objWriter.Fields("WEEKS_PER_POINT").Value = m_WeeksPerPercentagePoint
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("MAX_AMTPERWEEK", m_MaxAmountPerWeek)
                objWriter.Fields.Add("MAX_PERCENTOFSAWW", m_MaxPercentOfSAWW)
                objWriter.Fields.Add("MAX_TALBENEFITAMT", m_MaxTotalBenefitAmount)
                objWriter.Fields.Add("MAX_WEEKS", m_MaxWeeks)
                objWriter.Fields.Add("MIN_AMOUNTPERWEEK", m_MinAmountPerWeek)
                objWriter.Fields.Add("MIN_PERCENTOFSAWW", m_MinPercentOfSAWW)
                objWriter.Fields.Add("MMI_DATEREQUD_CODE", m_MMIDateRequired)
                objWriter.Fields.Add("MMI_DATE_AFTR_CODE", m_MMIDateBenefitStartAfterCode)
                objWriter.Fields.Add("MMI_DATE_ON_CODE", m_MMIDateBenefitStartOnCode)
                objWriter.Fields.Add("PAY_AFTERTEMP_CODE", m_PayAfterAllTemporary)
                objWriter.Fields.Add("PAY_LEN_DISAB_CODE", m_PayLengthOfDisability)
                objWriter.Fields.Add("PAY_NATALLIFE_CODE", m_PayNaturalLife)
                objWriter.Fields.Add("PAY_MAXWEEKS", m_PayMaxWeeks)
                objWriter.Fields.Add("PAY_PERIOD_CODE", m_PayPeriodCode)
                objWriter.Fields.Add("PERCENT_OF_AWW", m_PercentageOfAWW)
                objWriter.Fields.Add("USE_TTDRULES_CODE", m_UseCurrentTTDRules)
                objWriter.Fields.Add("WEEKS_PER_POINT", m_WeeksPerPercentagePoint)
            End If
            objWriter.Execute()

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try
    End Function

    Public Function ValidateData() As Integer
        Dim sFormatted As String

        ValidateData = 0

        m_Warning = ""
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_PayAfterAllTemporary < 1 Then
            m_Warning = m_Warning & "An 'Yes/No' is required for 'Pay After Temporary'." & vbCrLf
        End If
        If m_MMIDateRequired < 1 Then
            m_Warning = m_Warning & "An 'Yes/No' is required for the MMI Date." & vbCrLf
        Else
            If m_MMIDateRequired = m_NoCodeID Then
                m_MMIDateBenefitStartAfterCode = m_NoCodeID
                m_MMIDateBenefitStartOnCode = m_NoCodeID
            Else
                If m_MMIDateRequired = m_YesCodeID Then
                    If m_MMIDateBenefitStartAfterCode < 1 Then
                        m_Warning = m_Warning & "An 'Yes/No' is required for the MMI Date." & vbCrLf
                    End If
                    If m_MMIDateBenefitStartOnCode < 1 Then
                        m_Warning = m_Warning & "An 'Yes/No' is required for the MMI Date." & vbCrLf
                    End If
                    If m_MMIDateBenefitStartAfterCode > 1 And m_MMIDateBenefitStartOnCode > 1 Then
                        If m_MMIDateBenefitStartAfterCode = m_MMIDateBenefitStartOnCode Then
                            m_Warning = m_Warning & "The MMI Date Benefit Start After and On value can not be the same." & vbCrLf
                        End If
                    End If
                End If
            End If
        End If
        If m_MaxAmountPerWeek > 0 And m_MaxPercentOfSAWW > 0 Then
            m_Warning = m_Warning & "For Maximum Rates, only one of the two choices can have a value greater than 0 (zero)." & vbCrLf
        End If
        If m_MinAmountPerWeek > 0 And m_MinPercentOfSAWW > 0 Then
            m_Warning = m_Warning & "For Minimum Rates, only one of the two choices can have a value greater than 0 (zero)." & vbCrLf
        End If
        If m_UseCurrentTTDRules < 1 Then
            m_Warning = m_Warning & "Amount Limits does require an 'Yes or No' input for 'Use Current TTD Rules." & vbCrLf
        End If
        If m_UseCurrentTTDRules = m_NoCodeID Then
            If m_MaxAmountPerWeek = 0 And m_MaxPercentOfSAWW = 0 Then
                m_Warning = m_Warning & "For Maximum Rates, one of the two choices must have a value greater than 0 (zero)." & vbCrLf
            End If
            If m_MinAmountPerWeek > 0 And m_MinPercentOfSAWW > 0 Then
                m_Warning = m_Warning & "For Minimum Rates, one of the two choices must have a value greater than 0 (zero)." & vbCrLf
            End If
        End If


    End Function

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property MaxAmountPerWeek() As Double
        Get
            MaxAmountPerWeek = m_MaxAmountPerWeek
        End Get
        Set(ByVal Value As Double)
            m_MaxAmountPerWeek = Value
        End Set
    End Property

    Public Property MaxPercentOfSAWW() As Double
        Get
            MaxPercentOfSAWW = m_MaxPercentOfSAWW
        End Get
        Set(ByVal Value As Double)
            m_MaxPercentOfSAWW = Value
        End Set
    End Property

    Public Property MaxTotalBenefitAmount() As Double
        Get
            MaxTotalBenefitAmount = m_MaxTotalBenefitAmount
        End Get
        Set(ByVal Value As Double)
            m_MaxTotalBenefitAmount = Value
        End Set
    End Property

    Public Property MaxWeeks() As Integer
        Get
            MaxWeeks = m_MaxWeeks
        End Get
        Set(ByVal Value As Integer)
            m_MaxWeeks = Value
        End Set
    End Property

    Public Property MinAmountPerWeek() As Double
        Get
            MinAmountPerWeek = m_MinAmountPerWeek
        End Get
        Set(ByVal Value As Double)
            m_MinAmountPerWeek = Value
        End Set
    End Property

    Public Property MinPercentOfSAWW() As Double
        Get
            MinPercentOfSAWW = m_MinPercentOfSAWW
        End Get
        Set(ByVal Value As Double)
            m_MinPercentOfSAWW = Value
        End Set
    End Property

    Public Property MMIDateRequired() As Integer
        Get
            MMIDateRequired = m_MMIDateRequired
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequired = Value
        End Set
    End Property

    Public Property PayAfterAllTemporary() As Integer
        Get
            PayAfterAllTemporary = m_PayAfterAllTemporary
        End Get
        Set(ByVal Value As Integer)
            m_PayAfterAllTemporary = Value
        End Set
    End Property


    Public Property PayLengthOfDisability() As Integer
        Get
            PayLengthOfDisability = m_PayLengthOfDisability
        End Get
        Set(ByVal Value As Integer)
            m_PayLengthOfDisability = Value
        End Set
    End Property

    Public Property PayNaturalLife() As Integer
        Get
            PayNaturalLife = m_PayNaturalLife
        End Get
        Set(ByVal Value As Integer)
            m_PayNaturalLife = Value
        End Set
    End Property

    Public Property PayMaxWeeks() As Integer
        Get
            PayMaxWeeks = m_PayMaxWeeks
        End Get
        Set(ByVal Value As Integer)
            m_PayMaxWeeks = Value
        End Set
    End Property

    Public Property PayPeriodCode() As Integer
        Get
            PayPeriodCode = m_PayPeriodCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodCode = Value
        End Set
    End Property

    Public Property UseCurrentTTDRules() As Integer
        Get
            UseCurrentTTDRules = m_UseCurrentTTDRules
        End Get
        Set(ByVal Value As Integer)
            m_UseCurrentTTDRules = Value
        End Set
    End Property

    Public Property WeeksPerPercentagePoint() As Double
        Get
            WeeksPerPercentagePoint = m_WeeksPerPercentagePoint
        End Get
        Set(ByVal Value As Double)
            m_WeeksPerPercentagePoint = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property

    Public Property MinCompRate() As Double
        Get
            MinCompRate = m_MinCompRate
        End Get
        Set(ByVal Value As Double)
            m_MinCompRate = Value
        End Set
    End Property

    Public Property PercentageOfAWW() As Double
        Get
            PercentageOfAWW = m_PercentageOfAWW
        End Get
        Set(ByVal Value As Double)
            m_PercentageOfAWW = Value
        End Set
    End Property

    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property


    Public Property MMIDateBenefitStartOnCode() As Integer
        Get
            MMIDateBenefitStartOnCode = m_MMIDateBenefitStartOnCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateBenefitStartOnCode = Value
        End Set
    End Property


    Public Property MMIDateBenefitStartAfterCode() As Integer
        Get
            MMIDateBenefitStartAfterCode = m_MMIDateBenefitStartAfterCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateBenefitStartAfterCode = Value
        End Set
    End Property


    Public Property NoCodeID() As Integer
        Get
            NoCodeID = m_NoCodeID
        End Get
        Set(ByVal Value As Integer)
            m_NoCodeID = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property YesCodeID() As Integer
        Get
            YesCodeID = m_YesCodeID
        End Get
        Set(ByVal Value As Integer)
            m_YesCodeID = Value
        End Set
    End Property
End Class

