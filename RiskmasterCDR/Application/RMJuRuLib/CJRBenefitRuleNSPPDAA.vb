Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleNSPPDAA
    Const sClassName As String = "CJRBenefitRuleNSPPDAA"
    Const sTableName As String = "WCP_NSPPD_LKUP"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821
    Dim m_sDateOfEventDTG As String 'Local only
    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    Private m_MaxCompRateCalcd As Double 'jtodd22 Not Jurisdictional Rule parameter
    Private m_MinCompRateCalcd As Double 'jtodd22 Not Jurisdictional Rule parameter

    Private m_BaseWeeks As Double
    Private m_BeginningDateDTG As String
    Private m_BeginPercent As Double
    Private m_EndingDateDTG As String
    Private m_EndPercent As Double
    Private m_MaxAmount As Double
    Private m_MimAmount As Double
    Private m_MMIDateRequiredCode As Integer
    Private m_PayDollarForDollarCode As Integer
    Private m_PayFloorAmountCode As Integer
    Private m_PayLumpSumInReEmpCode As Integer
    Private m_PayLumpSumNotInReEmpCode As Integer
    Private m_PDMultiplier As Double
    Private m_PDDistrator As Double
    Private m_PercentOfAvMoWg As Double
    Private m_PercentOfAvWeWg As Double
    Private m_RTWageLessThanAWW As Integer
    Private m_RTWageEqOrGrThanAWW As Integer
    Private m_SAWWMaxPercent As Double
    Private m_SAWWMinPercent As Double
    Private m_ValueOfWholePerson As Double
    Private m_ValueOfWholePersonWeeks As Double
    Private m_UseTTDRateCode As Integer
    Private m_UseImpairPercentage As Double
    Private m_UseSAWWMaximumCode As Integer
    Private m_UseSAWWMinimumCode As Integer

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try
            AssignData = 0
            ClearObject()

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                m_BaseWeeks = objReader.GetDouble("BASE_WEEKS")
                m_BeginningDateDTG = objReader.GetString("BEGIN_DATE")
                m_EndingDateDTG = objReader.GetString("END_DATE")
                m_BeginPercent = objReader.GetDouble("BEGIN_PERCENT")
                m_EndPercent = objReader.GetDouble("END_PERCENT")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_MimAmount = objReader.GetDouble("MIN_BENEFIT")
                m_MaxAmount = objReader.GetDouble("MAX_BENEFIT")
                m_MMIDateRequiredCode = objReader.GetDouble("MMIDATEREQED_CODE")
                m_PayFloorAmountCode = objReader.GetInt32("PAY_FLOOR_CODE")
                m_PayDollarForDollarCode = objReader.GetInt32("PAY_DOLLAR_CODE")
                m_PayFloorAmountCode = objReader.GetInt32("PAY_FLOOR_CODE")
                m_PayLumpSumInReEmpCode = objReader.GetInt32("LMPSUMINREEMP_CODE")
                m_PayLumpSumNotInReEmpCode = objReader.GetInt32("LMPSUMNTREEMP_CODE")
                m_PayPeriodCode = objReader.GetInt32("PAYPERIOD_CODE")
                m_PDMultiplier = objReader.GetDouble("PD_MULTIPLIER")
                m_PDDistrator = objReader.GetDouble("PD_DISTRACTOR")
                m_PercentOfAvMoWg = objReader.GetDouble("CLAIMT_MO_PERCENT")
                m_PercentOfAvWeWg = objReader.GetDouble("CLAIMT_WK_PERCENT")
                m_RTWageLessThanAWW = objReader.GetInt32("RTW_LESS_AWW")
                m_RTWageEqOrGrThanAWW = objReader.GetInt32("RTW_EQL_OR_GTR_AWW")
                m_SAWWMaxPercent = objReader.GetDouble("SAWW_MAX_PERCENT")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_ValueOfWholePerson = objReader.GetDouble("VALUE_WHOLE_PERSON")
                m_ValueOfWholePersonWeeks = objReader.GetDouble("VAL_WHLE_PRSN_WEKS")
                m_UseImpairPercentage = objReader.GetInt32("USE_IMPAIR_PERCENT")
                m_UseSAWWMaximumCode = objReader.GetInt32("USE_SAWW_MAX_CODE")
                m_UseSAWWMinimumCode = objReader.GetInt32("USE_SAWW_MIN_CODE")
                m_UseTTDRateCode = objReader.GetInt32("USE_TTD_RATE_CODE")
            End If
            SafeCloseRecordset(objReader)

            'jtodd modFunctions.GetYesCodeID does not have a connection to data base
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID



            If m_MaxAmount > 0 Then m_MaxCompRateCalcd = m_MaxAmount
            If m_UseSAWWMaximumCode = m_YesCodeID Then
                m_MaxCompRateCalcd = modFunctions.GetSAWWMaxCompRateWeekly(m_ErrorMaskSAWW, objUser, m_JurisRowID, m_sDateOfEventDTG)
            End If

            If m_MimAmount > 0 Then m_MinCompRateCalcd = m_MimAmount
            If m_UseSAWWMinimumCode = m_YesCodeID Then
                m_MinCompRateCalcd = modFunctions.GetSAWWMinCompRate(m_ErrorMaskSAWW, objUser, m_JurisRowID, m_sDateOfEventDTG)
            End If

            m_DataHasChanged = False

            AssignData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            AssignData = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_MaxCompRateCalcd = 0 'jtodd22 Not in database
        m_MinCompRateCalcd = 0 'jtodd22 Not in database

        m_BaseWeeks = 0
        m_BeginningDateDTG = ""
        m_BeginPercent = 0
        m_EndingDateDTG = ""
        m_EndPercent = 0
        m_MaxAmount = 0
        m_MimAmount = 0
        m_MMIDateRequiredCode = 0
        m_PayFloorAmountCode = 0
        m_PayLumpSumInReEmpCode = 0
        m_PayLumpSumNotInReEmpCode = 0
        m_PDMultiplier = 0
        m_PDDistrator = 0
        m_PercentOfAvMoWg = 0
        m_PercentOfAvWeWg = 0
        m_RTWageLessThanAWW = 0
        m_RTWageEqOrGrThanAWW = 0
        m_SAWWMaxPercent = 0
        m_SAWWMinPercent = 0
        m_ValueOfWholePerson = 0
        m_ValueOfWholePersonWeeks = 0
        m_UseImpairPercentage = 0
        m_UseSAWWMaximumCode = 0
        m_UseSAWWMinimumCode = 0
        m_UseTTDRateCode = 0



    End Function
    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        '123456789012345678
        sSQL = sSQL & " ADDED_BY_USER"
        sSQL = sSQL & ",BASE_WEEKS"
        sSQL = sSQL & ",BEGIN_DATE"
        sSQL = sSQL & ",BEGIN_PERCENT"
        sSQL = sSQL & ",CLAIMT_MO_PERCENT"
        sSQL = sSQL & ",CLAIMT_WK_PERCENT"
        sSQL = sSQL & ",DTTM_RCD_ADDED"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD"
        sSQL = sSQL & ",END_DATE"
        sSQL = sSQL & ",END_PERCENT"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",LMPSUMINREEMP_CODE"
        sSQL = sSQL & ",LMPSUMNTREEMP_CODE"
        sSQL = sSQL & ",MIN_BENEFIT"
        sSQL = sSQL & ",MAX_BENEFIT"
        sSQL = sSQL & ",MMIDATEREQED_CODE"
        sSQL = sSQL & ",PAY_DOLLAR_CODE"
        sSQL = sSQL & ",PAY_FLOOR_CODE"
        sSQL = sSQL & ",PD_MULTIPLIER"
        sSQL = sSQL & ",PD_DISTRACTOR"
        sSQL = sSQL & ",RTW_LESS_AWW"
        sSQL = sSQL & ",RTW_EQL_OR_GTR_AWW"
        sSQL = sSQL & ",PAYPERIOD_CODE"
        sSQL = sSQL & ",SAWW_MAX_PERCENT"
        sSQL = sSQL & ",TABLE_ROW_ID"
        sSQL = sSQL & ",VALUE_WHOLE_PERSON"
        sSQL = sSQL & ",VAL_WHLE_PRSN_WEKS"
        sSQL = sSQL & ",UPDATED_BY_USER"
        sSQL = sSQL & ",USE_IMPAIR_PERCENT"
        sSQL = sSQL & ",USE_TTD_RATE_CODE"
        sSQL = sSQL & ",USE_SAWW_MAX_CODE"
        sSQL = sSQL & ",USE_SAWW_MIN_CODE"
        '123456789012345678

        GetBaseSQL = sSQL



    End Function

    Public Function ValidateData() As Integer

        ValidateData = 0

        m_Warning = ""

        If Len(m_BeginningDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_MMIDateRequiredCode < 1 Then
            m_Warning = m_Warning & "'MMI Date Required' requires a selection." & vbCrLf
        End If
        If m_UseTTDRateCode < 1 Then
            m_Warning = m_Warning & "'Use TTD Rate' requires a selection." & vbCrLf
        End If
        If m_UseSAWWMaximumCode < 1 Then
            m_Warning = m_Warning & "'Use SAWW Maximum' requires a selection." & vbCrLf
        End If
        If m_UseSAWWMinimumCode < 1 Then
            m_Warning = m_Warning & "'Use SAWW Minimum' requires a selection." & vbCrLf
        End If


    End Function

    Public Property PayLumpSumNotInReEmpCode() As Integer
        Get
            PayLumpSumNotInReEmpCode = m_PayLumpSumNotInReEmpCode
        End Get
        Set(ByVal Value As Integer)
            m_PayLumpSumNotInReEmpCode = Value
        End Set
    End Property

    Public Property PayLumpSumInReEmpCode() As Integer
        Get
            PayLumpSumInReEmpCode = m_PayLumpSumInReEmpCode
        End Get
        Set(ByVal Value As Integer)
            m_PayLumpSumInReEmpCode = Value
        End Set
    End Property

    Public Property PayPeriodCode() As Integer
        Get
            PayPeriodCode = m_PayPeriodCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodCode = Value
        End Set
    End Property

    Public Property RTWageEqOrGrThanAWW() As Integer
        Get
            RTWageEqOrGrThanAWW = m_RTWageEqOrGrThanAWW
        End Get
        Set(ByVal Value As Integer)
            m_RTWageEqOrGrThanAWW = Value
        End Set
    End Property

    Public Property RTWageLessThanAWW() As Integer
        Get
            RTWageLessThanAWW = m_RTWageLessThanAWW
        End Get
        Set(ByVal Value As Integer)
            m_RTWageLessThanAWW = Value
        End Set
    End Property

    Public Property ValueOfWholePerson() As Double
        Get
            ValueOfWholePerson = m_ValueOfWholePerson
        End Get
        Set(ByVal Value As Double)
            m_ValueOfWholePerson = Value
        End Set
    End Property

    Public Property UseImpairPercentage() As Double
        Get
            UseImpairPercentage = m_UseImpairPercentage
        End Get
        Set(ByVal Value As Double)
            m_UseImpairPercentage = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property BeginningDateDTG() As String
        Get
            BeginningDateDTG = m_BeginningDateDTG
        End Get
        Set(ByVal Value As String)
            m_BeginningDateDTG = Value
        End Set
    End Property

    Public Property EndingDateDTG() As String
        Get
            EndingDateDTG = m_EndingDateDTG
        End Get
        Set(ByVal Value As String)
            m_EndingDateDTG = Value
        End Set
    End Property
    Public Property PDDistrator() As Double
        Get
            PDDistrator = m_PDDistrator
        End Get
        Set(ByVal Value As Double)
            m_PDDistrator = Value
        End Set
    End Property
    Public Property PDMultiplier() As Double
        Get
            PDMultiplier = m_PDMultiplier
        End Get
        Set(ByVal Value As Double)
            m_PDMultiplier = Value
        End Set
    End Property
    Public Property BaseWeeks() As Double
        Get
            BaseWeeks = m_BaseWeeks
        End Get
        Set(ByVal Value As Double)
            m_BaseWeeks = Value
        End Set
    End Property
    Public Property MaxAmount() As Double
        Get
            MaxAmount = m_MaxAmount
        End Get
        Set(ByVal Value As Double)
            m_MaxAmount = Value
        End Set
    End Property
    Public Property MimAmount() As Double
        Get
            MimAmount = m_MimAmount
        End Get
        Set(ByVal Value As Double)
            m_MimAmount = Value
        End Set
    End Property
    Public Property EndPercent() As Double
        Get
            EndPercent = m_EndPercent
        End Get
        Set(ByVal Value As Double)
            m_EndPercent = Value
        End Set
    End Property
    Public Property BeginPercent() As Double
        Get
            BeginPercent = m_BeginPercent
        End Get
        Set(ByVal Value As Double)
            m_BeginPercent = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property
    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property


    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property

    Public Property ValueOfWholePersonWeeks() As Double
        Get
            ValueOfWholePersonWeeks = m_ValueOfWholePersonWeeks
        End Get
        Set(ByVal Value As Double)
            m_ValueOfWholePersonWeeks = Value
        End Set
    End Property

    Public Property UseTTDRateCode() As Integer
        Get
            UseTTDRateCode = m_UseTTDRateCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTTDRateCode = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property

    Public Property MaxCompRateCalcd() As Double
        Get
            MaxCompRateCalcd = m_MaxCompRateCalcd
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateCalcd = Value
        End Set
    End Property

    Public Property MinCompRateCalcd() As Double
        Get
            MinCompRateCalcd = m_MinCompRateCalcd
        End Get
        Set(ByVal Value As Double)
            m_MinCompRateCalcd = Value
        End Set
    End Property

    Public Property PercentOfAvMoWg() As Double
        Get
            PercentOfAvMoWg = m_PercentOfAvMoWg
        End Get
        Set(ByVal Value As Double)
            m_PercentOfAvMoWg = Value
        End Set
    End Property

    Public Property PercentOfAvWeWg() As Double
        Get
            PercentOfAvWeWg = m_PercentOfAvWeWg
        End Get
        Set(ByVal Value As Double)
            m_PercentOfAvWeWg = Value
        End Set
    End Property

    Public Property SAWWMaxPercent() As Double
        Get
            SAWWMaxPercent = m_SAWWMaxPercent
        End Get
        Set(ByVal Value As Double)
            m_SAWWMaxPercent = Value
        End Set
    End Property

    Public Property SAWWMinPercent() As Double
        Get
            SAWWMinPercent = m_SAWWMinPercent
        End Get
        Set(ByVal Value As Double)
            m_SAWWMinPercent = Value
        End Set
    End Property

    Public Property PayFloorAmountCode() As Integer
        Get
            PayFloorAmountCode = m_PayFloorAmountCode
        End Get
        Set(ByVal Value As Integer)
            m_PayFloorAmountCode = Value
        End Set
    End Property

    Public Property UseSAWWMaximumCode() As Integer
        Get
            UseSAWWMaximumCode = m_UseSAWWMaximumCode
        End Get
        Set(ByVal Value As Integer)
            m_UseSAWWMaximumCode = Value
        End Set
    End Property

    Public Property UseSAWWMinimumCode() As Integer
        Get
            UseSAWWMinimumCode = m_UseSAWWMinimumCode
        End Get
        Set(ByVal Value As Integer)
            m_UseSAWWMinimumCode = Value
        End Set
    End Property

    Public Property MMIDateRequiredCode() As Integer
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property

    Public Property PayDollarForDollarCode() As Integer
        Get
            PayDollarForDollarCode = m_PayDollarForDollarCode
        End Get
        Set(ByVal Value As Integer)
            m_PayDollarForDollarCode = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property

    '---------------------------------------------------------------------------------------
    ' Procedure : SaveData
    ' DateTime  : 7/13/2004 09:59
    ' Author    : jtodd22
    ' Purpose   :
    ' Notes     : Do not pass TableRowID, it is part of class (m_TableRowID)
    '---------------------------------------------------------------------------------------
    '
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String

        Try
            SaveData = 0

            sSQL = GetBaseSQL()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("BEGIN_DATE").Value = m_BeginningDateDTG
                objWriter.Fields("END_DATE").Value = m_EndingDateDTG
                objWriter.Fields("BEGIN_PERCENT").Value = m_BeginPercent
                objWriter.Fields("END_PERCENT").Value = m_EndPercent
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("MIN_BENEFIT").Value = m_MimAmount
                objWriter.Fields("MAX_BENEFIT").Value = m_MaxAmount
                objWriter.Fields("MMIDATEREQED_CODE").Value = m_MMIDateRequiredCode
                objWriter.Fields("BASE_WEEKS").Value = m_BaseWeeks

                objWriter.Fields("PAY_FLOOR_CODE").Value = m_PayFloorAmountCode

                objWriter.Fields("PAY_DOLLAR_CODE").Value = m_PayDollarForDollarCode
                objWriter.Fields("PD_MULTIPLIER").Value = m_PDMultiplier
                objWriter.Fields("PD_DISTRACTOR").Value = m_PDDistrator
                objWriter.Fields("CLAIMT_MO_PERCENT").Value = m_PercentOfAvMoWg
                objWriter.Fields("CLAIMT_WK_PERCENT").Value = m_PercentOfAvWeWg

                objWriter.Fields("RTW_LESS_AWW").Value = m_RTWageLessThanAWW
                objWriter.Fields("RTW_EQL_OR_GTR_AWW").Value = m_RTWageEqOrGrThanAWW
                objWriter.Fields("LMPSUMNTREEMP_CODE").Value = m_PayLumpSumNotInReEmpCode
                objWriter.Fields("LMPSUMINREEMP_CODE").Value = m_PayLumpSumInReEmpCode
                objWriter.Fields("PAYPERIOD_CODE").Value = m_PayPeriodCode
                objWriter.Fields("VALUE_WHOLE_PERSON").Value = m_ValueOfWholePerson
                objWriter.Fields("VAL_WHLE_PRSN_WEKS").Value = m_ValueOfWholePersonWeeks
                objWriter.Fields("USE_IMPAIR_PERCENT").Value = m_UseImpairPercentage
                objWriter.Fields("USE_TTD_RATE_CODE").Value = m_UseTTDRateCode

                objWriter.Fields("SAWW_MAX_PERCENT").Value = m_SAWWMaxPercent
                objWriter.Fields("USE_SAWW_MAX_CODE").Value = m_UseSAWWMaximumCode
                objWriter.Fields("USE_SAWW_MIN_CODE").Value = m_UseSAWWMinimumCode
                objWriter.Fields("MMIDATEREQED_CODE").Value = m_MMIDateRequiredCode

            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))

                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("BEGIN_DATE", m_BeginningDateDTG)
                objWriter.Fields.Add("END_DATE", m_EndingDateDTG)
                objWriter.Fields.Add("BEGIN_PERCENT", m_BeginPercent)
                objWriter.Fields.Add("END_PERCENT", m_EndPercent)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("MIN_BENEFIT", m_MimAmount)
                objWriter.Fields.Add("MAX_BENEFIT", m_MaxAmount)
                objWriter.Fields.Add("MMIDATEREQED_CODE", m_MMIDateRequiredCode)
                objWriter.Fields.Add("BASE_WEEKS", m_BaseWeeks)
                objWriter.Fields.Add("PAY_FLOOR_CODE", m_PayFloorAmountCode)
                objWriter.Fields.Add("PAY_DOLLAR_CODE", m_PayDollarForDollarCode)
                objWriter.Fields.Add("PD_MULTIPLIER", m_PDMultiplier)
                objWriter.Fields.Add("PD_DISTRACTOR", m_PDDistrator)
                objWriter.Fields.Add("CLAIMT_MO_PERCENT", m_PercentOfAvMoWg)
                objWriter.Fields.Add("CLAIMT_WK_PERCENT", m_PercentOfAvWeWg)
                objWriter.Fields.Add("RTW_LESS_AWW", m_RTWageLessThanAWW)
                objWriter.Fields.Add("RTW_EQL_OR_GTR_AWW", m_RTWageEqOrGrThanAWW)
                objWriter.Fields.Add("LMPSUMNTREEMP_CODE", m_PayLumpSumNotInReEmpCode)
                objWriter.Fields.Add("LMPSUMINREEMP_CODE", m_PayLumpSumInReEmpCode)
                objWriter.Fields.Add("PAYPERIOD_CODE", m_PayPeriodCode)
                objWriter.Fields.Add("VALUE_WHOLE_PERSON", m_ValueOfWholePerson)
                objWriter.Fields.Add("VAL_WHLE_PRSN_WEKS", m_ValueOfWholePersonWeeks)
                objWriter.Fields.Add("USE_IMPAIR_PERCENT", m_UseImpairPercentage)
                objWriter.Fields.Add("USE_TTD_RATE_CODE", m_UseTTDRateCode)
                objWriter.Fields.Add("SAWW_MAX_PERCENT", m_SAWWMaxPercent)
                objWriter.Fields.Add("USE_SAWW_MAX_CODE", m_UseSAWWMaximumCode)
                objWriter.Fields.Add("USE_SAWW_MIN_CODE", m_UseSAWWMinimumCode)
                objWriter.Fields.Add("MMIDATEREQED_CODE", m_MMIDateRequiredCode)
            End If
            objWriter.Execute()
            m_DataHasChanged = False
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try
    End Function

    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0

            If Trim(sDateOfEventDTG & "") = "" Then Exit Function
            m_sDateOfEventDTG = sDateOfEventDTG

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(BEGIN_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND BEGIN_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetBaseSQL()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND BEGIN_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim sSQL As String
        Try

            LoadDataByTableRowID = 0

            sSQL = GetBaseSQL()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
End Class

