Option Strict Off
Option Explicit On
Public Class CJRJurisFiling
    Const sClassName As String = "CJRJurisFiling"
    Const sTableName As String = "WCP_FILE_SCHEDULE"

    'local variable(s) to hold property value(s)
    Private m_TableRowID As Integer
    Private m_JurisRowID As Integer
    Private m_EffectiveDateDTG As String
    Private m_DeletedFlag As Integer
    Private m_ReleaseRowID As Integer
    Private m_ReleaseDesc As String
    Private m_FormID As Integer
    Private m_FormTitle As String
    Private m_FormName As String
    Private m_FromDateOfCode As Integer
    Private m_TriggeringEventCode As Integer
    Private m_DaysToFile As Integer
    Private m_SchedTimeCode As Integer
    Private m_NotifyAdjustCode As Integer
    Private m_DataHasChanged As Boolean 'local copy
    Private Function GetFieldSQL() As String
        Dim sSQL As String
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",DELETED_FLAG"
        sSQL = sSQL & ",RELEASE_ROW_ID"
        sSQL = sSQL & ",FORM_ID"
        sSQL = sSQL & ",DATE_OF_CODE"
        sSQL = sSQL & ",TRIG_EVENT_CODE"
        sSQL = sSQL & ",DAYS_TO_FILE"
        sSQL = sSQL & ",SCHEDULE_TIME_CODE"
        sSQL = sSQL & ",NOTIFY_ADJUST_CODE"
        GetFieldSQL = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0



            sSQL = ""
            sSQL = sSQL & GetFieldSQL()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_ReleaseRowID = objReader.GetInt32("RELEASE_ROW_ID")
                m_FormID = objReader.GetInt32("FORM_ID")
                m_FromDateOfCode = objReader.GetInt32("DATE_OF_CODE")
                m_TriggeringEventCode = objReader.GetInt32("TRIG_EVENT_CODE")
                m_DaysToFile = objReader.GetInt32("DAYS_TO_FILE")
                m_SchedTimeCode = objReader.GetInt32("SCHEDULE_TIME_CODE")
                m_NotifyAdjustCode = objReader.GetInt32("NOTIFY_ADJUST_CODE")
            End If
            LoadData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = ""
            sSQL = sSQL & GetFieldSQL()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("RELEASE_ROW_ID").Value = m_ReleaseRowID
                objWriter.Fields("FORM_ID").Value = m_FormID
                objWriter.Fields("DATE_OF_CODE").Value = m_FromDateOfCode
                objWriter.Fields("TRIG_EVENT_CODE").Value = m_TriggeringEventCode
                objWriter.Fields("DAYS_TO_FILE").Value = m_DaysToFile
                objWriter.Fields("SCHEDULE_TIME_CODE").Value = m_SchedTimeCode
                objWriter.Fields("NOTIFY_ADJUST_CODE").Value = m_NotifyAdjustCode
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lTest = lGetNextUID(sTableName)
                objWriter.Fields.Add("TABLE_ROW_ID", lTest)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("RELEASE_ROW_ID", m_ReleaseRowID)
                objWriter.Fields.Add("FORM_ID", m_FormID)
                objWriter.Fields.Add("DATE_OF_CODE", m_FromDateOfCode)
                objWriter.Fields.Add("TRIG_EVENT_CODE", m_TriggeringEventCode)
                objWriter.Fields.Add("DAYS_TO_FILE", m_DaysToFile)
                objWriter.Fields.Add("SCHEDULE_TIME_CODE", m_SchedTimeCode)
                objWriter.Fields.Add("NOTIFY_ADJUST_CODE", m_NotifyAdjustCode)
            End If
            SafeCloseRecordset(objReader)
            objWriter.Execute()

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property FormID() As Integer
        Get
            FormID = m_FormID
        End Get
        Set(ByVal Value As Integer)
            m_FormID = Value
        End Set
    End Property

    Public Property FromDateOfCode() As Integer
        Get
            FromDateOfCode = m_FromDateOfCode
        End Get
        Set(ByVal Value As Integer)
            m_FromDateOfCode = Value
        End Set
    End Property

    Public Property TriggeringEventCode() As Integer
        Get
            TriggeringEventCode = m_TriggeringEventCode
        End Get
        Set(ByVal Value As Integer)
            m_TriggeringEventCode = Value
        End Set
    End Property

    Public Property DaysToFile() As Integer
        Get
            DaysToFile = m_DaysToFile
        End Get
        Set(ByVal Value As Integer)
            m_DaysToFile = Value
        End Set
    End Property

    Public Property SchedTimeCode() As Integer
        Get
            SchedTimeCode = m_SchedTimeCode
        End Get
        Set(ByVal Value As Integer)
            m_SchedTimeCode = Value
        End Set
    End Property

    Public Property NotifyAdjustCode() As Integer
        Get
            NotifyAdjustCode = m_NotifyAdjustCode
        End Get
        Set(ByVal Value As Integer)
            m_NotifyAdjustCode = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property ReleaseRowID() As Integer
        Get
            ReleaseRowID = m_ReleaseRowID
        End Get
        Set(ByVal Value As Integer)
            m_ReleaseRowID = Value
        End Set
    End Property

    Public Property FormTitle() As String
        Get
            FormTitle = m_FormTitle
        End Get
        Set(ByVal Value As String)
            m_FormTitle = Value
        End Set
    End Property

    Public Property FormName() As String
        Get
            FormName = m_FormName
        End Get
        Set(ByVal Value As String)
            m_FormName = Value
        End Set
    End Property

    Public Property ReleaseDesc() As String
        Get
            ReleaseDesc = m_ReleaseDesc
        End Get
        Set(ByVal Value As String)
            m_ReleaseDesc = Value
        End Set
    End Property
End Class

