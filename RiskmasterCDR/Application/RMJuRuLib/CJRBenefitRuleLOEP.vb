Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleLOEP
    Const sClassName As String = "CJRBenefitRuleLOEP"
    Const sTableName As String = "WCP_RULE_LOEP"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821
    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    'local variable(s) to hold property value(s)
    Private m_MethodBBeginDateDBFormat As String
    Private m_MinimumLostRequiredPercent As Double
    Private m_MaxTimeLossRateCode As Integer
    Private m_MaxTimesSAWRateCode As Integer
    Private m_UseHigherMethodForRateCode As Integer

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                m_DataHasChanged = False
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_MaxTimeLossRateCode = objReader.GetInt32("MAXTILOSSRATE_CODE")
                m_MaxTimesSAWRateCode = objReader.GetInt32("MAXTISAW_RATE_CODE")
                m_MethodBBeginDateDBFormat = objReader.GetString("MDB_BEGDATEDBFRMAT")
                m_MinimumLostRequiredPercent = objReader.GetDouble("MINLOSTREQDPERCENT")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_UseHigherMethodForRateCode = objReader.GetInt32("USEHIMDFRRATE_CODE")
            End If

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_MinimumLostRequiredPercent = 0
        m_MethodBBeginDateDBFormat = ""
        m_UseHigherMethodForRateCode = 0
        m_MaxTimeLossRateCode = 0
        m_MaxTimesSAWRateCode = 0



    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE,JURIS_ROW_ID,DELETED_FLAG"
        '                    123456789012345678
        sSQL = sSQL & ",MINLOSTREQDPERCENT"
        sSQL = sSQL & ",MDB_BEGDATEDBFRMAT"
        sSQL = sSQL & ",USEHIMDFRRATE_CODE"
        sSQL = sSQL & ",MAXTILOSSRATE_CODE"
        sSQL = sSQL & ",MAXTISAW_RATE_CODE"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadDataTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataTableRowID"
        Dim sSQL As String
        Try
            LoadDataTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try
            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try
            SaveData = 0



            sSQL = ""
            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("MINLOSTREQDPERCENT").Value = m_MinimumLostRequiredPercent
                objWriter.Fields("MDB_BEGDATEDBFRMAT").Value = m_MethodBBeginDateDBFormat
                objWriter.Fields("USEHIMDFRRATE_CODE").Value = m_UseHigherMethodForRateCode
                objWriter.Fields("MAXTILOSSRATE_CODE").Value = m_MaxTimeLossRateCode
                objWriter.Fields("MAXTISAW_RATE_CODE").Value = m_MaxTimesSAWRateCode
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("MINLOSTREQDPERCENT", m_MinimumLostRequiredPercent)
                objWriter.Fields.Add("MDB_BEGDATEDBFRMAT", m_MethodBBeginDateDBFormat)
                objWriter.Fields.Add("USEHIMDFRRATE_CODE", m_UseHigherMethodForRateCode)
                objWriter.Fields.Add("MAXTILOSSRATE_CODE", m_MaxTimeLossRateCode)
                objWriter.Fields.Add("MAXTISAW_RATE_CODE", m_MaxTimesSAWRateCode)
            End If



            objWriter.Execute()

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try


    End Function

    Public Function ValidateData() As Integer

        ValidateData = 0
        m_Warning = ""
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_MinimumLostRequiredPercent < 1 Then
            m_Warning = m_Warning & "A value is required for Minimum Loss." & vbCrLf
        End If


    End Function

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property MinimumLostRequiredPercent() As Double
        Get
            MinimumLostRequiredPercent = m_MinimumLostRequiredPercent
        End Get
        Set(ByVal Value As Double)
            m_MinimumLostRequiredPercent = Value
        End Set
    End Property

    Public Property MethodBBeginDateDBFormat() As String
        Get
            MethodBBeginDateDBFormat = m_MethodBBeginDateDBFormat
        End Get
        Set(ByVal Value As String)
            m_MethodBBeginDateDBFormat = Value
        End Set
    End Property

    Public Property UseHigherMethodForRateCode() As Integer
        Get
            UseHigherMethodForRateCode = m_UseHigherMethodForRateCode
        End Get
        Set(ByVal Value As Integer)
            m_UseHigherMethodForRateCode = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property

    Public Property MaxTimeLossRateCode() As Integer
        Get
            MaxTimeLossRateCode = m_MaxTimeLossRateCode
        End Get
        Set(ByVal Value As Integer)
            m_MaxTimeLossRateCode = Value
        End Set
    End Property

    Public Property MaxTimesSAWRateCode() As Integer
        Get
            MaxTimesSAWRateCode = m_MaxTimesSAWRateCode
        End Get
        Set(ByVal Value As Integer)
            m_MaxTimesSAWRateCode = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
End Class

