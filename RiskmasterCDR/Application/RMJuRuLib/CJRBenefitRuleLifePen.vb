Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleLifePen
    Const sClassName As String = "CJRBenefitRuleLifePen"
    Const sTableName As String = "WCP_LIFEPEN_LIMITS"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821
    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    Private m_BeginDateDTG As String
    Private m_EndDateDTG As String
    Private m_MaxAWW As Double
    Private m_MinAWW As Double
    Private m_MinDisability As Double
    Private m_PDMultipler As Double

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_BeginDateDTG = objReader.GetString("BEGIN_DATE")
                m_DataHasChanged = False
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_EndDateDTG = objReader.GetString("END_DATE")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_MaxAWW = objReader.GetDouble("MAX_AWW")
                m_MinAWW = objReader.GetDouble("MIN_AWW")
                m_MinDisability = objReader.GetDouble("MIN_DIS_PERCENT")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_PDMultipler = objReader.GetInt32("PD_MULTIPLIER")
            End If

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_BeginDateDTG = vbNullString
        m_EndDateDTG = vbNullString
        m_MaxAWW = 0
        m_MinAWW = 0


    End Function
    Private Function GetFieldSQL() As String
        Dim sSQL As String
        GetFieldSQL = sSQL
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", BEGIN_DATE"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", END_DATE"
        sSQL = sSQL & ", MAX_AWW"
        sSQL = sSQL & ", MIN_AWW"
        sSQL = sSQL & ", MIN_DIS_PERCENT"
        sSQL = sSQL & ", PD_MULTIPLIER"
        GetFieldSQL = sSQL



    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM WCP_LIFEPEN_LIMITS"
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetFieldSQL()
            sSQL = sSQL & " FROM WCP_LIFEPEN_LIMITS"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim sSQL As String
        Try

            LoadDataByTableRowID = 0


            sSQL = GetFieldSQL()
            sSQL = sSQL & " FROM WCP_LIFEPEN_LIMITS"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try
            SaveData = 0

            sSQL = GetFieldSQL()
            sSQL = sSQL & " FROM WCP_LIFEPEN_LIMITS"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("BEGIN_DATE").Value = m_BeginDateDTG
                'UPGRADE_WARNING: Use of Null/IsNull() detected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"'
                If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("END_DATE").Value = m_EndDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("MAX_AWW").Value = m_MaxAWW
                objWriter.Fields("MIN_AWW").Value = m_MinAWW
                objWriter.Fields("MIN_DIS_PERCENT").Value = m_MinDisability
                objWriter.Fields("PD_MULTIPLIER").Value = m_PDMultipler
                objWriter.Execute()

            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))

                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("BEGIN_DATE", m_BeginDateDTG)
                'UPGRADE_WARNING: Use of Null/IsNull() detected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"'
                If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("END_DATE", m_EndDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("MAX_AWW", m_MaxAWW)
                objWriter.Fields.Add("MIN_AWW", m_MinAWW)
                objWriter.Fields.Add("MIN_DIS_PERCENT", m_MinDisability)
                objWriter.Fields.Add("PD_MULTIPLIER", m_PDMultipler)
                objWriter.Execute()

            End If


            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try


    End Function

    Public Function ValidateData() As Integer

        ValidateData = 0
        m_Warning = ""
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If Not IsNumeric(m_MinDisability) Then
            m_Warning = m_Warning & "A percentage is required for Minimum Disablility." & vbCrLf
        Else
            If m_MinDisability = 0 Then
                m_Warning = m_Warning & "A percentage, greater than zero, is required for Minimum Disablility." & vbCrLf
            End If
        End If
        If Not IsNumeric(m_PDMultipler) Then
            m_Warning = m_Warning & "A PD Multipler is required." & vbCrLf
        Else
            If m_PDMultipler = 0 Then
                m_Warning = m_Warning & "A PD Multipler, greater than zero, is required." & vbCrLf
            End If
        End If


    End Function

    Public Property BeginDateDTG() As String
        Get
            BeginDateDTG = m_BeginDateDTG
        End Get
        Set(ByVal Value As String)
            m_BeginDateDTG = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property DeletedFlag() As Short
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Short)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property EndDateDTG() As String
        Get
            EndDateDTG = m_EndDateDTG
        End Get
        Set(ByVal Value As String)
            m_EndDateDTG = Value
        End Set
    End Property

    Public Property JurisRowID() As Short
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Short)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property MaxAWW() As Double
        Get
            MaxAWW = m_MaxAWW
        End Get
        Set(ByVal Value As Double)
            m_MaxAWW = Value
        End Set
    End Property

    Public Property MinAWW() As Double
        Get
            MinAWW = m_MinAWW
        End Get
        Set(ByVal Value As Double)
            m_MinAWW = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property

    Public Property MinDisability() As Double
        Get
            MinDisability = m_MinDisability
        End Get
        Set(ByVal Value As Double)
            m_MinDisability = Value
        End Set
    End Property

    Public Property PDMultipler() As Double
        Get
            PDMultipler = m_PDMultipler
        End Get
        Set(ByVal Value As Double)
            m_PDMultipler = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property


    Public Property ErrorMaskJR() As Integer
        Get

            ErrorMaskJR = m_ErrorMaskJR

        End Get
        Set(ByVal Value As Integer)

            m_ErrorMaskJR = Value

        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer
        Get

            ErrorMaskSAWW = m_ErrorMaskSAWW

        End Get
        Set(ByVal Value As Integer)

            m_ErrorMaskSAWW = Value

        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
End Class

