Option Strict Off
Option Explicit On
Public Class CJRLifeExpectancys
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRLifeExpectancys"
    'local variable to hold collection
    Private mCol As Collection
    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED"
        sSQL = sSQL & ", ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD"
        sSQL = sSQL & ", UPDATED_BY_USER"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", CDC_DECENNIAL"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", SEX_CODE_ID"
        sSQL = sSQL & ", AGE"
        sSQL = sSQL & ", YRS_REMAINING"
        sSQL = sSQL & " FROM WCP_CDC_LIFE_EXPCT"
        GetBaseSQL = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sCDCDecennial As String) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim lTableRowID As Integer
        Try

            LoadData = 0



            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND CDC_DECENNIAL = '" & sCDCDecennial & "'"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                lTableRowID = objReader.GetInt32("TABLE_ROW_ID")
                Add("", "", 0, lJurisRowID, lTableRowID, CInt(objReader.GetString("SEX_CODE_ID")), objReader.GetString("CDC_DECENNIAL"), objReader.GetDouble("YRS_REMAINING"), objReader.GetInt32("AGE"), "k" & CStr(lTableRowID))

            End While

            LoadData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function LoadDistinctDecennials(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDistinctDecennials"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadDistinctDecennials = 0



            sSQL = ""
            sSQL = sSQL & "SELECT DISTINCT"
            sSQL = sSQL & " CDC_DECENNIAL"
            sSQL = sSQL & " FROM WCP_CDC_LIFE_EXPCT"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                Add("", "", 0, lJurisRowID, 0, 0, objReader.GetString("CDC_DECENNIAL"), 0, 0)

            End While

            LoadDistinctDecennials = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDistinctDecennials = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Add(ByRef EndDateDTG As String, ByRef EffectiveDateDTG As String, ByRef DeletedFlag As Short, ByRef JurisRowID As Integer, ByRef TableRowID As Integer, ByRef SexCodeID As Integer, ByRef CDCDecennial As String, ByRef YearsRemaining As Double, ByRef Age As Short, Optional ByRef sKey As String = "") As CJRLifeExpectancy
        Const sFunctionName As String = "Add"
        'create a new object
        Dim objNewMember As CJRLifeExpectancy
        Try
            objNewMember = New CJRLifeExpectancy

            'set the properties passed into the method
            objNewMember.EndDateDTG = EndDateDTG
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.JurisRowID = JurisRowID
            objNewMember.TableRowID = TableRowID
            objNewMember.SexCodeID = SexCodeID
            objNewMember.CDCDecennial = CDCDecennial
            objNewMember.YearsRemaining = YearsRemaining
            objNewMember.Age = Age
            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRLifeExpectancy
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

