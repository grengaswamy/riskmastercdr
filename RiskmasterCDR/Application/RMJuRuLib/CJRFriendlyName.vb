Option Strict Off
Option Explicit On
Public Class CJRFriendlyName
    Private Const sClassName As String = "CJRFriendlyName"
    Private Const sTableName As String = "WCP_FRIENDLY_NAME"

    'Class properties, local copy
    Private m_TableRowID As Integer
    Private m_FormID As Integer
    Private m_FriendlyName As String
    Private m_FieldName As String
    Private m_UsedInCalc As Integer
    Private m_DataHasChanged As Boolean
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT "
            sSQL = sSQL & " TABLE_ROW_ID"
            sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
            sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
            sSQL = sSQL & ",FORM_ID"
            sSQL = sSQL & ",USED_IN_CALC"
            sSQL = sSQL & ",FRIENDLY_NAME"
            sSQL = sSQL & ",FIELD_NAME"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_FormID = objReader.GetInt32("FORM_ID")
                m_UsedInCalc = objReader.GetInt32("USED_IN_CALC")
                m_FriendlyName = objReader.GetString("FRIENDLY_NAME")
                m_FieldName = objReader.GetString("FIELD_NAME")
            End If
            LoadData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT "
            sSQL = sSQL & " TABLE_ROW_ID"
            sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
            sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
            sSQL = sSQL & ",FORM_ID"
            sSQL = sSQL & ",USED_IN_CALC"
            sSQL = sSQL & ",FRIENDLY_NAME"
            sSQL = sSQL & ",FIELD_NAME"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("FRIENDLY_NAME").Value = m_FriendlyName
                objWriter.Fields("FIELD_NAME").Value = m_FieldName
                objWriter.Fields("FORM_ID").Value = m_FormID
                objWriter.Fields("USED_IN_CALC").Value = m_UsedInCalc
                SafeCloseRecordset(objReader)
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lTest = lGetNextUID(sTableName)
                objWriter.Fields.Add("TABLE_ROW_ID", lTest)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields.Add("FRIENDLY_NAME", m_FriendlyName)
                objWriter.Fields.Add("FIELD_NAME", m_FieldName)
                objWriter.Fields.Add("FORM_ID", m_FormID)
                objWriter.Fields.Add("USED_IN_CALC", m_UsedInCalc)
            End If

            objWriter.Execute()


            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property FieldName() As String
        Get
            FieldName = m_FieldName
        End Get
        Set(ByVal Value As String)
            m_FieldName = Value
        End Set
    End Property

    Public Property FriendlyName() As String
        Get
            FriendlyName = m_FriendlyName
        End Get
        Set(ByVal Value As String)
            m_FriendlyName = Value
        End Set
    End Property

    Public Property FormID() As Integer
        Get
            FormID = m_FormID
        End Get
        Set(ByVal Value As Integer)
            m_FormID = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property UsedInCalc() As Integer
        Get
            UsedInCalc = m_UsedInCalc
        End Get
        Set(ByVal Value As Integer)
            m_UsedInCalc = Value
        End Set
    End Property
End Class

