Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleNSPPDAAs
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRBenefitRuleNSPPDAAs"
    Const sTableName As String = "WCP_NSPPD_LKUP"

    'Class properties, local variable(s)
    Private mCol As Collection
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJRBenefitRuleNSPPDAA
        Try

            LoadData = 0
            'ClearObject



            sSQL = GetBaseSQL()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID

            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	While objReader.Read()
            '		objRecord = New CJRBenefitRuleNSPPDAA
            '		With objRecord
            '			.BaseWeeks = objReader.GetDouble( "BASE_WEEKS")
            '			.BeginningDateDTG = objReader.GetString( "BEGIN_DATE")
            '			.BeginPercent = objReader.GetDouble( "BEGIN_PERCENT")
            '			.EndingDateDTG = objReader.GetString( "END_DATE")
            '			.EndPercent = objReader.GetDouble( "END_PERCENT")
            '			.JurisRowID = objReader.GetInt32( "JURIS_ROW_ID")
            '			.MaxAmount = objReader.GetDouble( "MAX_BENEFIT")
            '			.MimAmount = objReader.GetDouble( "MIN_BENEFIT")
            '			.MMIDateRequiredCode = objReader.GetDouble( "MMIDATEREQED_CODE")
            '			.PayDollarForDollarCode = objReader.GetInt32( "PAY_DOLLAR_CODE")
            '			.PayFloorAmountCode = objReader.GetInt32( "PAY_FLOOR_CODE")
            '			.PayLumpSumInReEmpCode = objReader.GetInt32( "LMPSUMINREEMP_CODE")
            '			.PayLumpSumNotInReEmpCode = objReader.GetInt32( "LMPSUMNTREEMP_CODE")
            '			.PayPeriodCode = objReader.GetInt32( "PAYPERIOD_CODE")
            '			.PDDistrator = objReader.GetDouble( "PD_DISTRACTOR")
            '			.PDMultiplier = objReader.GetDouble( "PD_MULTIPLIER")
            '			.PercentOfAvMoWg = objReader.GetDouble( "CLAIMT_MO_PERCENT")
            '			.PercentOfAvWeWg = objReader.GetDouble( "CLAIMT_WK_PERCENT")
            '			.RTWageLessThanAWW = objReader.GetInt32( "RTW_LESS_AWW")
            '			.RTWageEqOrGrThanAWW = objReader.GetInt32( "RTW_EQL_OR_GTR_AWW")
            '			.SAWWMaxPercent = objReader.GetDouble( "SAWW_MAX_PERCENT")
            '			.TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            '			.ValueOfWholePerson = objReader.GetDouble( "VALUE_WHOLE_PERSON")
            '			.ValueOfWholePersonWeeks = objReader.GetDouble( "VAL_WHLE_PRSN_WEKS")
            '			.UseImpairPercentage = objReader.GetInt32( "USE_IMPAIR_PERCENT")
            '			.UseSAWWMaximumCode = objReader.GetInt32( "USE_SAWW_MAX_CODE")
            '			.UseSAWWMinimumCode = objReader.GetInt32( "USE_SAWW_MIN_CODE")
            '			.UseTTDRateCode = objReader.GetInt32( "USE_TTD_RATE_CODE")
            '			Add2(.BaseWeeks, .BeginningDateDTG, .BeginPercent, .EndingDateDTG, .EndPercent, .JurisRowID, .MaxAmount, .MimAmount, .MMIDateRequiredCode, .PayDollarForDollarCode, .PayFloorAmountCode, .PayLumpSumInReEmpCode, .PayLumpSumNotInReEmpCode, .PayPeriodCode, .PDDistrator, .PDMultiplier, .PercentOfAvMoWg, .PercentOfAvWeWg, .RTWageLessThanAWW, .RTWageEqOrGrThanAWW, .SAWWMaxPercent, .TableRowID, .ValueOfWholePerson, .ValueOfWholePersonWeeks, .UseImpairPercentage, .UseSAWWMaximumCode, .UseSAWWMinimumCode, .UseTTDRateCode, "k" & .TableRowID)

            '		End With

            '		
            '	End While
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRBenefitRuleNSPPDAA
                With objRecord
                    .BaseWeeks = objReader.GetDouble("BASE_WEEKS")
                    .BeginningDateDTG = objReader.GetString("BEGIN_DATE")
                    .BeginPercent = objReader.GetDouble("BEGIN_PERCENT")
                    .EndingDateDTG = objReader.GetString("END_DATE")
                    .EndPercent = objReader.GetDouble("END_PERCENT")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .MaxAmount = objReader.GetDouble("MAX_BENEFIT")
                    .MimAmount = objReader.GetDouble("MIN_BENEFIT")
                    .MMIDateRequiredCode = objReader.GetDouble("MMIDATEREQED_CODE")
                    .PayDollarForDollarCode = objReader.GetInt32("PAY_DOLLAR_CODE")
                    .PayFloorAmountCode = objReader.GetInt32("PAY_FLOOR_CODE")
                    .PayLumpSumInReEmpCode = objReader.GetInt32("LMPSUMINREEMP_CODE")
                    .PayLumpSumNotInReEmpCode = objReader.GetInt32("LMPSUMNTREEMP_CODE")
                    .PayPeriodCode = objReader.GetInt32("PAYPERIOD_CODE")
                    .PDDistrator = objReader.GetDouble("PD_DISTRACTOR")
                    .PDMultiplier = objReader.GetDouble("PD_MULTIPLIER")
                    .PercentOfAvMoWg = objReader.GetDouble("CLAIMT_MO_PERCENT")
                    .PercentOfAvWeWg = objReader.GetDouble("CLAIMT_WK_PERCENT")
                    .RTWageLessThanAWW = objReader.GetInt32("RTW_LESS_AWW")
                    .RTWageEqOrGrThanAWW = objReader.GetInt32("RTW_EQL_OR_GTR_AWW")
                    .SAWWMaxPercent = objReader.GetDouble("SAWW_MAX_PERCENT")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .ValueOfWholePerson = objReader.GetDouble("VALUE_WHOLE_PERSON")
                    .ValueOfWholePersonWeeks = objReader.GetDouble("VAL_WHLE_PRSN_WEKS")
                    .UseImpairPercentage = objReader.GetInt32("USE_IMPAIR_PERCENT")
                    .UseSAWWMaximumCode = objReader.GetInt32("USE_SAWW_MAX_CODE")
                    .UseSAWWMinimumCode = objReader.GetInt32("USE_SAWW_MIN_CODE")
                    .UseTTDRateCode = objReader.GetInt32("USE_TTD_RATE_CODE")
                    Add2(.BaseWeeks, .BeginningDateDTG, .BeginPercent, .EndingDateDTG, .EndPercent, .JurisRowID, .MaxAmount, .MimAmount, .MMIDateRequiredCode, .PayDollarForDollarCode, .PayFloorAmountCode, .PayLumpSumInReEmpCode, .PayLumpSumNotInReEmpCode, .PayPeriodCode, .PDDistrator, .PDMultiplier, .PercentOfAvMoWg, .PercentOfAvWeWg, .RTWageLessThanAWW, .RTWageEqOrGrThanAWW, .SAWWMaxPercent, .TableRowID, .ValueOfWholePerson, .ValueOfWholePersonWeeks, .UseImpairPercentage, .UseSAWWMaximumCode, .UseSAWWMinimumCode, .UseTTDRateCode, "k" & .TableRowID)

                End With
            End While


            LoadData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Add(ByRef BaseWeeks As Double, ByRef BeginningDateDTG As String, ByRef BeginPercent As Double, ByRef EndingDateDTG As String, ByRef EndPercent As Double, ByRef JurisRowID As Integer, ByRef MaxAmount As Double, ByRef MimAmount As Double, ByRef PayLumpSumNotInReEmpCode As Integer, ByRef PayLumpSumInReEmpCode As Integer, ByRef PayPeriodCode As Integer, ByRef PDDistrator As Double, ByRef PDMultiplier As Double, ByRef RTWageLessThanAWW As Integer, ByRef RTWageEqOrGrThanAWW As Integer, ByRef TableRowID As Integer, ByRef UseImpairPercentage As Integer, ByRef ValueOfWholePerson As Double, ByRef sKey As String) As CJRBenefitRuleNSPPDAA
        'create a new object
        Dim objNewMember As CJRBenefitRuleNSPPDAA

        Try
            objNewMember = New CJRBenefitRuleNSPPDAA
            'set the properties passed into the method
            objNewMember.JurisRowID = JurisRowID
            objNewMember.TableRowID = TableRowID
            objNewMember.BeginningDateDTG = BeginningDateDTG
            objNewMember.EndingDateDTG = EndingDateDTG
            objNewMember.BeginPercent = BeginPercent
            objNewMember.EndPercent = EndPercent
            objNewMember.MimAmount = MimAmount
            objNewMember.MaxAmount = MaxAmount
            objNewMember.BaseWeeks = BaseWeeks
            objNewMember.PayLumpSumNotInReEmpCode = PayLumpSumNotInReEmpCode
            objNewMember.PayLumpSumInReEmpCode = PayLumpSumInReEmpCode
            objNewMember.PayPeriodCode = PayPeriodCode
            objNewMember.PDMultiplier = PDMultiplier
            objNewMember.PDDistrator = PDDistrator
            objNewMember.RTWageLessThanAWW = RTWageLessThanAWW
            objNewMember.RTWageEqOrGrThanAWW = RTWageEqOrGrThanAWW
            objNewMember.UseImpairPercentage = UseImpairPercentage
            objNewMember.ValueOfWholePerson = ValueOfWholePerson
            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If
            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleNSPPDAA
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        '123456789012345678
        sSQL = sSQL & " ADDED_BY_USER"
        sSQL = sSQL & ",BASE_WEEKS"
        sSQL = sSQL & ",BEGIN_DATE"
        sSQL = sSQL & ",BEGIN_PERCENT"
        sSQL = sSQL & ",CLAIMT_MO_PERCENT"
        sSQL = sSQL & ",CLAIMT_WK_PERCENT"
        sSQL = sSQL & ",DTTM_RCD_ADDED"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD"
        sSQL = sSQL & ",END_DATE"
        sSQL = sSQL & ",END_PERCENT"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",LMPSUMINREEMP_CODE"
        sSQL = sSQL & ",LMPSUMNTREEMP_CODE"
        sSQL = sSQL & ",MIN_BENEFIT"
        sSQL = sSQL & ",MAX_BENEFIT"
        sSQL = sSQL & ",MMIDATEREQED_CODE"
        sSQL = sSQL & ",PAY_DOLLAR_CODE"
        sSQL = sSQL & ",PAY_FLOOR_CODE"
        sSQL = sSQL & ",PD_MULTIPLIER"
        sSQL = sSQL & ",PD_DISTRACTOR"
        sSQL = sSQL & ",RTW_LESS_AWW"
        sSQL = sSQL & ",RTW_EQL_OR_GTR_AWW"
        sSQL = sSQL & ",PAYPERIOD_CODE"
        sSQL = sSQL & ",SAWW_MAX_PERCENT"
        sSQL = sSQL & ",TABLE_ROW_ID"
        sSQL = sSQL & ",VALUE_WHOLE_PERSON"
        sSQL = sSQL & ",VAL_WHLE_PRSN_WEKS"
        sSQL = sSQL & ",UPDATED_BY_USER"
        sSQL = sSQL & ",USE_IMPAIR_PERCENT"
        sSQL = sSQL & ",USE_TTD_RATE_CODE"
        sSQL = sSQL & ",USE_SAWW_MAX_CODE"
        sSQL = sSQL & ",USE_SAWW_MIN_CODE"
        '123456789012345678

        GetBaseSQL = sSQL



    End Function


    Public Function Add2(ByRef BaseWeeks As Double, ByRef BeginningDateDTG As String, ByRef BeginPercent As Double, ByRef EndingDateDTG As String, ByRef EndPercent As Double, ByRef JurisRowID As Integer, ByRef MaxAmount As Double, ByRef MimAmount As Double, ByRef MMIDateRequiredCode As Integer, ByRef PayDollarForDollarCode As Integer, ByRef PayFloorAmountCode As Integer, ByRef PayLumpSumInReEmpCode As Integer, ByRef PayLumpSumNotInReEmpCode As Integer, ByRef PayPeriodCode As Integer, ByRef PDDistrator As Double, ByRef PDMultiplier As Double, ByRef PercentOfAvMoWg As Double, ByRef PercentOfAvWeWg As Double, ByRef RTWageLessThanAWW As Integer, ByRef RTWageEqOrGrThanAWW As Integer, ByRef SAWWMaxPercent As Double, ByRef TableRowID As Integer, ByRef ValueOfWholePerson As Double, ByRef ValueOfWholePersonWeeks As Double, ByRef UseImpairPercentage As Integer, ByRef UseSAWWMaximumCode As Integer, ByRef UseSAWWMinimumCode As Integer, ByRef UseTTDRateCode As Integer, ByRef sKey As String) As CJRBenefitRuleNSPPDAA
        'create a new object
        Dim objNewMember As CJRBenefitRuleNSPPDAA

        Try
            objNewMember = New CJRBenefitRuleNSPPDAA
            'set the properties passed into the method
            objNewMember.BaseWeeks = BaseWeeks
            objNewMember.BeginningDateDTG = BeginningDateDTG
            objNewMember.BeginPercent = BeginPercent
            objNewMember.EndingDateDTG = EndingDateDTG
            objNewMember.EndPercent = EndPercent
            objNewMember.JurisRowID = JurisRowID
            objNewMember.MaxAmount = MaxAmount
            objNewMember.MimAmount = MimAmount
            objNewMember.MMIDateRequiredCode = MMIDateRequiredCode

            objNewMember.PayDollarForDollarCode = PayDollarForDollarCode
            objNewMember.PayFloorAmountCode = PayFloorAmountCode
            objNewMember.PayLumpSumNotInReEmpCode = PayLumpSumNotInReEmpCode
            objNewMember.PayLumpSumInReEmpCode = PayLumpSumInReEmpCode
            objNewMember.PayPeriodCode = PayPeriodCode
            objNewMember.PDDistrator = PDDistrator
            objNewMember.PDMultiplier = PDMultiplier
            objNewMember.PercentOfAvMoWg = PercentOfAvMoWg
            objNewMember.PercentOfAvWeWg = PercentOfAvWeWg

            objNewMember.RTWageLessThanAWW = RTWageLessThanAWW
            objNewMember.RTWageEqOrGrThanAWW = RTWageEqOrGrThanAWW
            objNewMember.SAWWMaxPercent = SAWWMaxPercent
            objNewMember.TableRowID = TableRowID
            objNewMember.UseImpairPercentage = UseImpairPercentage
            objNewMember.UseSAWWMaximumCode = UseSAWWMaximumCode
            objNewMember.UseSAWWMinimumCode = UseSAWWMinimumCode
            objNewMember.UseTTDRateCode = UseTTDRateCode
            objNewMember.ValueOfWholePerson = ValueOfWholePerson
            objNewMember.ValueOfWholePersonWeeks = ValueOfWholePersonWeeks
            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If
            'return the object created
            Add2 = objNewMember
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add2|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function
End Class

