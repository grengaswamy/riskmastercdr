Option Strict Off
Option Explicit On
Public Class CJRCommutation
    Private Const sClassName As String = "CJRCommutation"
    Private Const sTableName As String = "WCP_COMMUTAT_RE"

    'Class properties--local copy
    Private m_TableRowID As Integer
    Private m_JurisRowID As Short
    Private m_DeletedFlag As Short
    Private m_EffectiveDateDTG As String
    Private m_JurisBenefitID As Integer
    Private m_PermittedCode As Integer
    Private m_EndDateDTG As String
    Private m_MaxDiscount As Double
    Private m_MinDiscount As Double
    Private m_DiscountJurisDefined As Double
    Private m_Reference As String
    Private m_UseJurisProcedure As Integer
    Private m_CommutTypeCode As Integer
    Private m_JurisApprovalReqCode As Integer
    Private m_DataHasChanged As Boolean
    Private m_Warning As String

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_JurisBenefitID = objReader.GetInt32("JURIS_BENEFIT_ID")
                m_MaxDiscount = objReader.GetInt32("MAX_DISCOUNT")
                m_MinDiscount = objReader.GetInt32("MIN_DISCOUNT")
                m_DiscountJurisDefined = objReader.GetInt32("DIS_JURIS_VALUE")
                m_UseJurisProcedure = objReader.GetInt32("JURIS_PROCED_CODE")
                m_CommutTypeCode = objReader.GetInt32("COMM_TYPE_CODE")
                m_JurisApprovalReqCode = objReader.GetInt32("JURIS_APPVL_CODE")
                m_Reference = objReader.GetString("REFERENCE")
                m_DataHasChanged = False
            End If
            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        m_CommutTypeCode = 0
        m_TableRowID = 0
        m_JurisRowID = 0
        m_DeletedFlag = 0
        m_PermittedCode = 0
        m_EffectiveDateDTG = ""
        m_EndDateDTG = ""
        m_MaxDiscount = 0
        m_MinDiscount = 0
        m_DiscountJurisDefined = 0
        m_Reference = ""
        m_UseJurisProcedure = 0
        m_JurisApprovalReqCode = 0
        m_DataHasChanged = False
        m_Warning = ""


    End Function

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property
    Public Property JurisApprovalReqCode() As Integer
        Get
            JurisApprovalReqCode = m_JurisApprovalReqCode
        End Get
        Set(ByVal Value As Integer)
            m_JurisApprovalReqCode = Value
        End Set
    End Property

    Public Property CommutTypeCode() As Integer
        Get
            CommutTypeCode = m_CommutTypeCode
        End Get
        Set(ByVal Value As Integer)
            m_CommutTypeCode = Value
        End Set
    End Property

    Public Property JurisBenefitID() As Integer
        Get
            JurisBenefitID = m_JurisBenefitID
        End Get
        Set(ByVal Value As Integer)
            m_JurisBenefitID = Value
        End Set
    End Property

    Public Property MaxDiscount() As Double
        Get
            MaxDiscount = m_MaxDiscount
        End Get
        Set(ByVal Value As Double)
            m_MaxDiscount = Value
        End Set
    End Property

    Public Property MinDiscount() As Double
        Get
            MinDiscount = m_MinDiscount
        End Get
        Set(ByVal Value As Double)
            m_MinDiscount = Value
        End Set
    End Property

    Public Property DiscountJurisDefined() As Double
        Get
            DiscountJurisDefined = m_DiscountJurisDefined
        End Get
        Set(ByVal Value As Double)
            m_DiscountJurisDefined = Value
        End Set
    End Property

    Public Property UseJurisProcedure() As Integer
        Get
            UseJurisProcedure = m_UseJurisProcedure
        End Get
        Set(ByVal Value As Integer)
            m_UseJurisProcedure = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property
    Public Property JurisRowID() As Short
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Short)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property DeletedFlag() As Short
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Short)
            m_DeletedFlag = Value
        End Set
    End Property
    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property EndDateDTG() As String
        Get
            EndDateDTG = m_EndDateDTG
        End Get
        Set(ByVal Value As String)
            m_EndDateDTG = Value
        End Set
    End Property
    Public Property PermittedCode() As Integer
        Get
            PermittedCode = m_PermittedCode
        End Get
        Set(ByVal Value As Integer)
            m_PermittedCode = Value
        End Set
    End Property

    Public Property Reference() As String
        Get
            Reference = m_Reference
        End Get
        Set(ByVal Value As String)
            m_Reference = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property

    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try
            SaveData = 0

            sSQL = GetBaseSQL()
            sSQL = sSQL & " FROM WCP_COMMUTAT_RE"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = g_objUser.LoginName
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("JURIS_BENEFIT_ID").Value = m_JurisBenefitID
                objWriter.Fields("MAX_DISCOUNT").Value = m_MaxDiscount
                objWriter.Fields("MIN_DISCOUNT").Value = m_MinDiscount
                objWriter.Fields("DIS_JURIS_VALUE").Value = m_DiscountJurisDefined
                objWriter.Fields("JURIS_PROCED_CODE").Value = m_UseJurisProcedure
                objWriter.Fields("COMM_TYPE_CODE").Value = m_CommutTypeCode
                objWriter.Fields("JURIS_APPVL_CODE").Value = m_JurisApprovalReqCode
                objWriter.Fields("REFERENCE").Value = m_Reference
                SafeCloseRecordset(objReader)
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                m_TableRowID = lGetNextUID(sTableName)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields.Add("UPDATED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("JURIS_BENEFIT_ID", m_JurisBenefitID)
                objWriter.Fields.Add("MAX_DISCOUNT", m_MaxDiscount)
                objWriter.Fields.Add("MIN_DISCOUNT", m_MinDiscount)
                objWriter.Fields.Add("DIS_JURIS_VALUE", m_DiscountJurisDefined)
                objWriter.Fields.Add("JURIS_PROCED_CODE", m_UseJurisProcedure)
                objWriter.Fields.Add("COMM_TYPE_CODE", m_CommutTypeCode)
                objWriter.Fields.Add("JURIS_APPVL_CODE", m_JurisApprovalReqCode)
                objWriter.Fields.Add("REFERENCE", m_Reference)
            End If

            objWriter.Execute()

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetBaseSQL()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0

            sSQL = GetBaseSQL()
            sSQL = sSQL & " FROM WCP_COMMUTAT_RE"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID
            LoadData = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Public Function Validate() As Integer

        Validate = 0

        m_Warning = ""
        If Len(m_EffectiveDateDTG) Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_JurisRowID < 1 Then
            m_Warning = m_Warning & "A Jurisdiction is required." & vbCrLf
        End If



    End Function

    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID, DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        'sSQL = sSQL & ", EFF_END_DATE"
        sSQL = sSQL & ", MAX_DISCOUNT, MIN_DISCOUNT, DIS_JURIS_VALUE"
        sSQL = sSQL & ", JURIS_PROCED_CODE"
        sSQL = sSQL & ", JURIS_BENEFIT_ID"
        sSQL = sSQL & ", COMM_TYPE_CODE"
        sSQL = sSQL & ", JURIS_APPVL_CODE"
        sSQL = sSQL & ", REFERENCE"

        GetBaseSQL = sSQL



    End Function
End Class

