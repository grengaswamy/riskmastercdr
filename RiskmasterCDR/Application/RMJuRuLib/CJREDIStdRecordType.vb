Option Strict Off
Option Explicit On
Public Class CJREDIStdRecordType
    Const sClassName As String = "CJREDIStdRecordType"
    Const sTableName2 As String = "EDI_STDRECTYP_LKUP"
    Const sTableName3 As String = "WCP_STDRECTYP_LKUP"

    Private m_TableRowID As Integer
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_JurisRowID As Integer
    Private m_ReleaseName As String
    Private m_ReleaseVersion As Integer 'local copy
    Private m_ShortCode As String 'local copy
    Private m_Description As String 'local copy
    Private m_DataHasChanged As Boolean
    Private m_PtTableRowID As Integer
    Private m_RecordTypeID As Integer
    Private m_ReleaseRowID As Integer
    Private Function ClearObject() As Integer
        m_TableRowID = 0
        m_ReleaseName = vbNullString
        m_ReleaseVersion = 0
        m_ShortCode = vbNullString
        m_DeletedFlag = 0
        m_Description = vbNullString
        m_DataHasChanged = False
        m_JurisRowID = 0
        m_PtTableRowID = 0
        m_RecordTypeID = 0
        m_ReleaseRowID = 0


    End Function
    Private Function GetFieldSQL() As String
        Dim sSQL As String
        GetFieldSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        If m_JurisRowID > 0 Then
            sSQL = sSQL & " " & sTableName3 & ".TABLE_ROW_ID"
            sSQL = sSQL & ", " & sTableName3 & ".ADDED_BY_USER"
            sSQL = sSQL & ", " & sTableName3 & ".DTTM_RCD_ADDED"
            sSQL = sSQL & ", " & sTableName3 & ".UPDATED_BY_USER"
            sSQL = sSQL & ", " & sTableName3 & ".DTTM_RCD_LAST_UPD"
            sSQL = sSQL & ", " & sTableName3 & ".PT_TABLE_ROW_ID"
            sSQL = sSQL & ", " & sTableName3 & ".RECORD_TYPE_ID"
            sSQL = sSQL & ", " & sTableName3 & ".RELEASE_ROW_ID"
        Else
            sSQL = sSQL & " " & sTableName2 & ".TABLE_ROW_ID"
            sSQL = sSQL & ", " & sTableName2 & ".ADDED_BY_USER"
            sSQL = sSQL & ", " & sTableName2 & ".DTTM_RCD_ADDED"
            sSQL = sSQL & ", " & sTableName2 & ".UPDATED_BY_USER"
            sSQL = sSQL & ", " & sTableName2 & ".DTTM_RCD_LAST_UPD"
            sSQL = sSQL & ", " & sTableName2 & ".RECORD_TYPE_ID"
            sSQL = sSQL & ", " & sTableName2 & ".RELEASE_ROW_ID"
        End If
        sSQL = sSQL & ", RECORD_TYPE"
        sSQL = sSQL & ", RECORD_DESC"
        GetFieldSQL = sSQL


    End Function
    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property RecordTypeID() As Integer
        Get
            RecordTypeID = m_RecordTypeID
        End Get
        Set(ByVal Value As Integer)
            m_RecordTypeID = Value
        End Set
    End Property

    Public Property ReleaseName() As String
        Get
            ReleaseName = m_ReleaseName
        End Get
        Set(ByVal Value As String)
            m_ReleaseName = Value
        End Set
    End Property

    Public Property ReleaseRowID() As Integer
        Get
            ReleaseRowID = m_ReleaseRowID
        End Get
        Set(ByVal Value As Integer)
            m_ReleaseRowID = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Description = m_Description
        End Get
        Set(ByVal Value As String)
            m_Description = Value
        End Set
    End Property
    Public Property ShortCode() As String
        Get
            ShortCode = m_ShortCode
        End Get
        Set(ByVal Value As String)
            m_ShortCode = Value
        End Set
    End Property

    Public Property PtTableRowID() As Integer
        Get
            PtTableRowID = m_PtTableRowID
        End Get
        Set(ByVal Value As Integer)
            m_PtTableRowID = Value
        End Set
    End Property

    Public Property ReleaseVersion() As Integer
        Get
            ReleaseVersion = m_ReleaseVersion
        End Get
        Set(ByVal Value As Integer)
            m_ReleaseVersion = Value
        End Set
    End Property

    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0


            sSQL = GetFieldSQL()
            If m_JurisRowID > 0 Then
                sSQL = sSQL & " FROM " & sTableName3
            Else
                sSQL = sSQL & " FROM " & sTableName2
            End If
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName

                If Not IsNothing(objReader.Item("DELETED_FLAG")) Then
                    If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                    objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                End If

                If IsNothing(objReader.Item("JURIS_ROW_ID")) Then
                    objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                End If
                If IsNothing(objReader.Item("PT_TABLE_ROW_ID")) Then
                    If m_PtTableRowID = 0 Then m_PtTableRowID = m_ReleaseRowID
                    objWriter.Fields("PT_TABLE_ROW_ID").Value = m_PtTableRowID
                End If

                If m_JurisRowID = 0 Then
                    objWriter.Fields("RECORD_TYPE_ID").Value = m_TableRowID
                Else
                    objWriter.Fields("RECORD_TYPE_ID").Value = m_RecordTypeID
                End If
                objWriter.Fields("RECORD_DESC").Value = m_Description

                If IsDBNull(m_ShortCode) Or Trim(m_ShortCode) = "" Then m_ShortCode = "X"
                objWriter.Fields("RECORD_TYPE").Value = m_ShortCode
                objWriter.Fields("RELEASE_ROW_ID").Value = m_ReleaseRowID
                SafeCloseRecordset(objReader)
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                If m_JurisRowID > 0 Then
                    m_TableRowID = lGetNextUID(sTableName2)
                Else
                    m_TableRowID = lGetNextUID(sTableName2)
                End If
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))

                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)

                If Not IsNothing(objReader.Item("DELETED_FLAG")) Then
                    If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                    objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                End If

                If Not IsNothing(objReader.Item("JURIS_ROW_ID")) Then
                    objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                End If

                If IsNothing(objReader.Item("PT_TABLE_ROW_ID")) Then
                    If m_PtTableRowID = 0 Then m_PtTableRowID = m_ReleaseRowID
                    objWriter.Fields.Add("PT_TABLE_ROW_ID", m_PtTableRowID)
                End If

                If m_JurisRowID = 0 Then
                    objWriter.Fields.Add("RECORD_TYPE_ID", m_TableRowID)
                Else
                    objWriter.Fields.Add("RECORD_TYPE_ID", m_RecordTypeID)
                End If
                objWriter.Fields.Add("RECORD_DESC", m_Description)

                If IsDBNull(m_ShortCode) Or Trim(m_ShortCode) = "" Then m_ShortCode = "X"
                objWriter.Fields.Add("RECORD_TYPE", m_ShortCode)
                objWriter.Fields.Add("RELEASE_ROW_ID", m_ReleaseRowID)
            End If
            objWriter.Execute()
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function SaveDataClone(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveDataClone"
        Dim sSQL As String
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Try

            SaveDataClone = 0

            If m_JurisRowID < 1 Then
                g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
                If Not g_bBatchMode Then Err.Raise(vbObjectError + 70000, g_sErrProcedure, "Jurisdiction ID not set.")
                LogError(g_sErrProcedure, Erl(), vbObjectError + 70000, Err.Source, "Jurisdiction ID not set.")
                Exit Function
            End If

            sSQL = GetFieldSQL()
            sSQL = sSQL & " FROM WCP_STDRECTYP_LKUP"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = 0"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)

                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName

                If Not IsNothing(objReader.Item("DELETED_FLAG")) Then
                    If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                    objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                End If

                If Not IsNothing(objReader.Item("JURIS_ROW_ID")) Then
                    objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                End If
                If Not IsNothing(objReader.Item("EFFECTIVE_DATE")) Then
                    objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                End If
                If Not IsNothing(objReader.Item("PT_TABLE_ROW_ID")) Then
                    If Not m_PtTableRowID > 0 Then m_PtTableRowID = 0
                    objWriter.Fields("PT_TABLE_ROW_ID").Value = m_PtTableRowID
                End If
                objWriter.Fields("RECORD_DESC").Value = m_Description
                If IsDBNull(m_ShortCode) Or Trim(m_ShortCode) = "" Then m_ShortCode = "X"
                objWriter.Fields("RECORD_TYPE").Value = m_ShortCode
                objWriter.Fields("RELEASE_ROW_ID").Value = m_ReleaseRowID
                objWriter.Fields("RECORD_TYPE_ID").Value = m_RecordTypeID
                objWriter.Execute()

            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                m_TableRowID = lGetNextUID("WCP_STDRECTYP_LKUP")
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)

                If Not IsNothing(objReader.Item("DELETED_FLAG")) Then
                    If IsDBNull(m_DeletedFlag) Then m_DeletedFlag = 0
                    objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                End If

                If Not IsNothing(objReader.Item("JURIS_ROW_ID")) Then
                    objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                End If
                If Not IsNothing(objReader.Item("EFFECTIVE_DATE")) Then
                    objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                End If
                If Not IsNothing(objReader.Item("PT_TABLE_ROW_ID")) Then
                    If Not m_PtTableRowID > 0 Then m_PtTableRowID = 0
                    objWriter.Fields.Add("PT_TABLE_ROW_ID", m_PtTableRowID)
                End If
                objWriter.Fields.Add("RECORD_DESC", m_Description)
                If IsDBNull(m_ShortCode) Or Trim(m_ShortCode) = "" Then m_ShortCode = "X"
                objWriter.Fields.Add("RECORD_TYPE", m_ShortCode)
                objWriter.Fields.Add("RELEASE_ROW_ID", m_ReleaseRowID)
                objWriter.Fields.Add("RECORD_TYPE_ID", m_RecordTypeID)
                objWriter.Execute()
            End If


            SaveDataClone = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & "|" & Err.Description
            End With
            SaveDataClone = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef lTableRowID As Integer, ByRef lReleaseRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0
            ClearObject()
            m_JurisRowID = lJurisRowID



            sSQL = GetFieldSQL()
            If m_JurisRowID > 0 Then
                sSQL = sSQL & ", WCP_STDFILING_LKUP.RELEASE_NAME"
                sSQL = sSQL & " FROM " & sTableName3 & ",WCP_STDFILING_LKUP"

                sSQL = sSQL & " WHERE " & sTableName3 & ".TABLE_ROW_ID  = " & lTableRowID
            Else
                sSQL = sSQL & ", EDI_STDFILING_LKUP.RELEASE_NAME"
                sSQL = sSQL & " FROM EDI_STDFILING_LKUP,EDI_STDRECTYP_LKUP"
                sSQL = sSQL & " WHERE EDI_STDFILING_LKUP.TABLE_ROW_ID = EDI_STDRECTYP_LKUP.RELEASE_ROW_ID"
                sSQL = sSQL & " AND EDI_STDRECTYP_LKUP.TABLE_ROW_ID  = " & lTableRowID
                sSQL = sSQL & " AND EDI_STDRECTYP_LKUP.RELEASE_ROW_ID = " & lReleaseRowID
            End If

            sSQL = sSQL & " ORDER BY RELEASE_NAME, RECORD_DESC"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                If Not IsNothing(objReader.Item("PT_TABLE_ROW_ID")) Then
                    m_PtTableRowID = objReader.GetInt32("PT_TABLE_ROW_ID")
                End If
                m_RecordTypeID = objReader.GetInt32("RECORD_TYPE_ID")
                m_Description = objReader.GetString("RECORD_DESC")
                m_ReleaseVersion = objReader.GetInt32("RELEASE_ROW_ID")
                m_ShortCode = objReader.GetString("RECORD_TYPE")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
            End If
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function DeleteObject(ByRef objUser As Riskmaster.Security.UserLogin) As Short

        Const sFunctionName As String = "DeleteObject"
        Dim sSQL As String
        Dim sMessage As String
        Dim objTrans As DbTransaction
        Dim objConn As DbConnection
        Try

            DeleteObject = 0
            objConn = DbFactory.GetDbConnection(g_ConnectionString)


            objTrans = objConn.BeginTransaction()

            'Performing Simple Deletes. Not loading the child objects and calling delete functions of the child objects
            '(1) Delete Conditions associated with the DN Records associated with this Record Type
            If m_JurisRowID > 0 Then
                sSQL = "DELETE FROM WCP_STDCONDITIONS WHERE RECDIF_ROW_ID IN "
                sSQL = sSQL & "(SELECT TABLE_ROW_ID FROM WCP_STDRECDIF_LKUP WHERE PT_TABLE_ROW_ID=" & m_TableRowID & ")"
            Else
                sSQL = "DELETE FROM EDI_STDCONDITIONS WHERE RECDIF_ROW_ID IN "
                sSQL = sSQL & "(SELECT TABLE_ROW_ID FROM EDI_STDRECDIF_LKUP WHERE RECORD_TYPE_ID=" & m_TableRowID & ")"
            End If
            objConn.ExecuteNonQuery(sSQL, objTrans)

            '(2) Delete the DN Records associated with this Record Type
            If m_JurisRowID > 0 Then
                sSQL = "DELETE FROM WCP_STDRECDIF_LKUP WHERE PT_TABLE_ROW_ID=" & m_TableRowID
            Else
                sSQL = "DELETE FROM EDI_STDRECDIF_LKUP WHERE RECORD_TYPE_ID=" & m_TableRowID
            End If
            objConn.ExecuteNonQuery(sSQL, objTrans)

            '(3) Delete the Record Type Record
            If m_JurisRowID > 0 Then
                sSQL = "DELETE FROM " & sTableName3 & " WHERE TABLE_ROW_ID=" & m_TableRowID
            Else
                sSQL = "DELETE FROM " & sTableName2 & " WHERE TABLE_ROW_ID=" & m_TableRowID
            End If
            objConn.ExecuteNonQuery(sSQL, objTrans)

            objTrans.Commit()

            'Log the Deleled Record
            sMessage = "Record Type Deleted" & " | "
            sMessage = sMessage & "Record Row Id: " & m_TableRowID & " | " & m_ShortCode & " | " & m_Description & " | "
            If m_JurisRowID > 0 Then
                sMessage = sMessage & "Jurisdiction Row Id: " & m_JurisRowID & " | "
                sMessage = sMessage & "Juris Release Row Id: " & m_PtTableRowID & " | "
            Else
                sMessage = sMessage & "Release Row Id: " & m_ReleaseVersion & " | "
            End If
            sMessage = sMessage & "Deleted By " & objUser.LoginName & " | "
            LogError(Err.Source & "|" & sClassName & "|" & sFunctionName, Erl(), 70001, Err.Source, sMessage)

            DeleteObject = -1

        Catch ex As Exception
            DeleteObject = 0
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DeleteObject = Err.Number
            objTrans.Rollback()

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            objTrans = Nothing
            objTrans.Dispose()
            objConn = Nothing
            objConn.Dispose()
        End Try

    End Function
End Class

