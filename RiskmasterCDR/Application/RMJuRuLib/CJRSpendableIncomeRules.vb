Option Strict Off
Option Explicit On
Public Class CJRSpendableIncomeRules
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRSpendableIncomeRules"

    'Class properties--local copy
    Private mCol As Collection

    Public Function Add(ByRef TableRowID As Integer, ByRef JurisRowID As Integer, ByRef JurisdictionName As String, ByRef EffectiveDateDTG As String, ByRef DeletedFlag As Integer, ByRef Exemption00Supported As Integer, ByRef Exemption05Supported As Integer, ByRef Exemption09Supported As Integer, ByRef Exemption10Supported As Integer, ByRef Exemption11Supported As Integer, Optional ByRef sKey As String = "") As CJRSpendableIncomeRule
        'create a new object
        Dim objNewMember As CJRSpendableIncomeRule
        objNewMember = New CJRSpendableIncomeRule

        'set the properties passed into the method
        objNewMember.TableRowID = TableRowID
        objNewMember.JurisdictionName = JurisdictionName
        objNewMember.JurisRowID = JurisRowID
        objNewMember.EffectiveDateDTG = EffectiveDateDTG
        objNewMember.DeletedFlag = DeletedFlag
        objNewMember.Exemption00Supported = Exemption00Supported
        objNewMember.Exemption05Supported = Exemption05Supported
        objNewMember.Exemption09Supported = Exemption09Supported
        objNewMember.Exemption10Supported = Exemption10Supported
        objNewMember.Exemption11Supported = Exemption11Supported
        If Len(sKey) = 0 Then
            mCol.Add(objNewMember)
        Else
            mCol.Add(objNewMember, sKey)
        End If


        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing



    End Function

    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin) As Integer
        Dim objReader As DbReader
        Dim objRecord As CJRSpendableIncomeRule
        Dim sSQL As String
        Try

            LoadData = 0

            sSQL = GetBaseSQL()
            sSQL = sSQL & " ORDER BY STATE_NAME"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRSpendableIncomeRule
                With objRecord
                    .DataHasChanged = False
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .Exemption00Supported = objReader.GetInt32("EXEMP_A_CODE")
                    .Exemption05Supported = objReader.GetInt32("EXEMP_F_CODE")
                    .Exemption09Supported = objReader.GetInt32("EXEMP_J_CODE")
                    .Exemption10Supported = objReader.GetInt32("EXEMP_K_CODE")
                    .Exemption11Supported = objReader.GetInt32("EXEMP_L_CODE")
                    .JurisdictionName = objReader.GetString("STATE_NAME")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .MinimumMarriedExempts = objReader.GetInt32("MIN_MARRIED_EXEMPS")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.TableRowID, .JurisRowID, .JurisdictionName, .EffectiveDateDTG, .DeletedFlag, .Exemption00Supported, .Exemption05Supported, .Exemption09Supported, .Exemption10Supported, .Exemption11Supported, "k" & .TableRowID)
                End With

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRSpendableIncomeRule
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    Protected Overrides Sub Finalize()
        mCol = Nothing
        MyBase.Finalize()
    End Sub

    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",EFFECTIVE_DATE"
        sSQL = sSQL & ",WCP_SPENDABLE_RULE.DELETED_FLAG"
        sSQL = sSQL & ",EXEMP_A_CODE"
        sSQL = sSQL & ",EXEMP_F_CODE"
        sSQL = sSQL & ",EXEMP_J_CODE"
        sSQL = sSQL & ",EXEMP_K_CODE"
        sSQL = sSQL & ",EXEMP_L_CODE"
        sSQL = sSQL & ",MIN_MARRIED_EXEMPS"
        sSQL = sSQL & ",STATE_NAME"
        sSQL = sSQL & " FROM WCP_SPENDABLE_RULE, STATES"
        sSQL = sSQL & " WHERE WCP_SPENDABLE_RULE.JURIS_ROW_ID = STATES.STATE_ROW_ID"

        GetBaseSQL = sSQL


    End Function
End Class

