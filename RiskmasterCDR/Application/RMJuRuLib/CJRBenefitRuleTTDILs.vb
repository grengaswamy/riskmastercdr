Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleTTDILs
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRBenefitRuleTTDILs"
    Const sTableName As String = "WCP_RULE_TTD_IL"


    'Class properties, local copy
    Private mCol As Collection
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID,DELETED_FLAG,EFFECTIVE_DATE"
        sSQL = sSQL & ",CLAIMANT_AWW_CODE"
        sSQL = sSQL & ",CHILD_A"
        sSQL = sSQL & ",CHILD_B"
        sSQL = sSQL & ",CHILD_C"
        sSQL = sSQL & ",CHILD_D"
        sSQL = sSQL & ",MARRIED_VALUE"
        sSQL = sSQL & ",MAX_COMP_RATE"
        sSQL = sSQL & ",MAX_PERCENT_OFSAWW"
        sSQL = sSQL & ",PAY_CONC_PPD_CODE"
        sSQL = sSQL & ",PRIME_RATE"
        sSQL = sSQL & ",SINGLE_VALUE"
        sSQL = sSQL & ",TOTAL_WEEKS"
        sSQL = sSQL & ",TOTAL_AMT"
        sSQL = sSQL & ",USE_SAWW_MIN_CODE"
        sSQL = sSQL & ",USE_SAWW_MAX_CODE"

        '                   123456789012345678
        sSQL = sSQL & ",CHILD_SPOUSE_A_AMT"
        sSQL = sSQL & ",CHILD_SPOUSE_B_AMT"
        sSQL = sSQL & ",CHILD_SPOUSE_C_AMT"
        sSQL = sSQL & ",CHILD_SPOUSE_D_AMT"
        sSQL = sSQL & ",CHILD_SPOUSE_E_AMT"
        sSQL = sSQL & ",CLAIMANT_AWWB_CODE"
        sSQL = sSQL & ",USE_SAWW_MINB_CODE"

        GetSQLFieldList = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sBeginDateDTG As String) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objTTDRule As CJRBenefitRuleTTDIL
        Try

            LoadData = 0

            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            If sBeginDateDTG > "" Then
                sSQL = sSQL & " AND EFFECTIVE_DATE = '" & sBeginDateDTG & "'"
            End If
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objTTDRule = New CJRBenefitRuleTTDIL
                With objTTDRule
                    .Child1 = objReader.GetDouble("CHILD_A")
                    .Child2 = objReader.GetDouble("CHILD_B")
                    .Child3 = objReader.GetDouble("CHILD_C")
                    .Child4 = objReader.GetDouble("CHILD_D")
                    .ClaimantsAWWCode = objReader.GetInt32("CLAIMANT_AWW_CODE")
                    .DataHasChanged = False
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .MarriedValue = objReader.GetDouble("MARRIED_VALUE")
                    .MaxCompRateWeekly = objReader.GetDouble("MAX_COMP_RATE")
                    .MaxPercentOfSAWW = objReader.GetDouble("MAX_PERCENT_OFSAWW")
                    .PayConCurrentPPD = objReader.GetInt32("PAY_CONC_PPD_CODE")
                    .PrimeRate = objReader.GetDouble("PRIME_RATE")
                    .SingleValue = objReader.GetDouble("SINGLE_VALUE")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .TotalAmount = objReader.GetDouble("TOTAL_AMT")
                    .RuleTotalWeeks = objReader.GetDouble("TOTAL_WEEKS")
                    .UseSAWWMaximumCode = objReader.GetInt32("USE_SAWW_MAX_CODE")
                    .UseSAWWMinimumCode = objReader.GetInt32("USE_SAWW_MIN_CODE")
                    Add(.Child1, .Child2, .Child3, .Child4, .ClaimantsAWWCode, .EffectiveDateDTG, .JurisRowID, .MarriedValue, .MaxCompRateWeekly, .MaxPercentOfSAWW, .PayConCurrentPPD, .PrimeRate, .SingleValue, .TableRowID, .TotalAmount, .RuleTotalWeeks, .UseSAWWMaximumCode, .UseSAWWMinimumCode, "k" & .TableRowID)
                End With
                'UPGRADE_NOTE: Object objTTDRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objTTDRule = Nothing

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function Add(ByRef Child1 As Double, ByRef Child2 As Double, ByRef Child3 As Double, ByRef Child4 As Double, ByRef ClaimantsAWWCode As Integer, ByRef EffectiveDateDTG As String, ByRef JurisRowID As Integer, ByRef MarriedValue As Double, ByRef MaxCompRateWeekly As Double, ByRef MaxPercentOfSAWW As Double, ByRef PayConCurrentPPD As Integer, ByRef PrimeRate As Double, ByRef SingleValue As Double, ByRef TableRowID As Integer, ByRef TotalAmount As Double, ByRef RuleTotalWeeks As Integer, ByRef UseSAWWMaximumCode As Integer, ByRef UseSAWWMinimumCode As Integer, ByRef sKey As String) As CJRBenefitRuleTTDIL

        Try
            'create a new object
            Dim objNewMember As CJRBenefitRuleTTDIL
            objNewMember = New CJRBenefitRuleTTDIL

            'set the properties passed into the method
            objNewMember.Child1 = Child1
            objNewMember.Child2 = Child2
            objNewMember.Child3 = Child3
            objNewMember.Child4 = Child4
            objNewMember.ClaimantsAWWCode = ClaimantsAWWCode
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.MarriedValue = MarriedValue
            objNewMember.MaxCompRateWeekly = MaxCompRateWeekly
            objNewMember.JurisRowID = JurisRowID
            objNewMember.MaxCompRateWeekly = MaxCompRateWeekly
            objNewMember.MaxPercentOfSAWW = MaxPercentOfSAWW
            objNewMember.PayConCurrentPPD = PayConCurrentPPD
            objNewMember.PrimeRate = PrimeRate
            objNewMember.SingleValue = SingleValue
            objNewMember.TableRowID = TableRowID
            objNewMember.TotalAmount = TotalAmount
            objNewMember.RuleTotalWeeks = RuleTotalWeeks
            objNewMember.UseSAWWMaximumCode = UseSAWWMaximumCode
            objNewMember.UseSAWWMinimumCode = UseSAWWMinimumCode
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleTTDIL
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

