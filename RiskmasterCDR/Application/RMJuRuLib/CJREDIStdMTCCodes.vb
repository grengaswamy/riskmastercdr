Option Strict Off
Option Explicit On
Public Class CJREDIStdMTCCodes
    Implements System.Collections.IEnumerable
    '---------------------------------------------------------------------------------------
    ' Module    : CJREDIStdMTCCodes
    ' DateTime  : 8/25/2005 10:44
    ' Author    : Venkata Mandayam
    ' Purpose   : Collection of CJREDIStdMTCCode objects
    '---------------------------------------------------------------------------------------
    Const sClassName As String = "CJREDIStdMTCCodes"
    Const sTableName2 As String = "EDI_STDMTC_CODES"
    Const sTableName3 As String = "WCP_STDMTC_CODES"

    Private mCol As Collection
    Private m_DataHasChanged As Boolean

    Private Function GetFieldSelect(ByRef sTableName As String) As String
        Dim sSQL As String
        GetFieldSelect = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & sTableName & ".TABLE_ROW_ID"
        sSQL = sSQL & "," & sTableName & ".DTTM_RCD_ADDED"
        sSQL = sSQL & "," & sTableName & ".ADDED_BY_USER"
        sSQL = sSQL & "," & sTableName & ".DTTM_RCD_LAST_UPD"
        sSQL = sSQL & "," & sTableName & ".UPDATED_BY_USER"
        sSQL = sSQL & "," & sTableName & ".MTC_CODE"
        sSQL = sSQL & "," & sTableName & ".MTC_CODE_TEXT"
        sSQL = sSQL & "," & sTableName & ".RELEASE_ROW_ID"
        If sTableName = sTableName3 Then
            sSQL = sSQL & "," & sTableName & ".JURISRELEASE_ROW_ID"
            sSQL = sSQL & "," & sTableName & ".MTC_TABLE_ROW_ID"
        End If
        GetFieldSelect = sSQL


    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 8/25/2005 11:13
    ' Author    : Venkata Mandayam
    ' Purpose   : Loads all MTCs for a given release.
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByVal lReleaseRowID As Integer, ByVal lJurisReleaseRowId As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim bCreate As Boolean
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJREDIStdMTCCode
        Try

            LoadData = 0

            g_sErrDescription = ""


            bCreate = False

            If lJurisReleaseRowId > 0 Then
                sSQL = ""
                sSQL = sSQL & GetFieldSelect(sTableName3)
                sSQL = sSQL & " FROM " & sTableName3
                sSQL = sSQL & " WHERE " & sTableName3 & ".JURISRELEASE_ROW_ID = " & lJurisReleaseRowId
                sSQL = sSQL & " AND " & sTableName3 & ".DELETED_FLAG = 0"
            Else
                sSQL = ""
                sSQL = sSQL & GetFieldSelect(sTableName2)
                sSQL = sSQL & " FROM " & sTableName2
                sSQL = sSQL & " WHERE " & sTableName2 & ".RELEASE_ROW_ID = " & lReleaseRowID
                sSQL = sSQL & " AND " & sTableName2 & ".DELETED_FLAG = 0"
            End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()

                objRecord = New CJREDIStdMTCCode

                With objRecord
                    .DataHasChanged = False
                    .MTCCode = Trim(objReader.GetString("MTC_CODE"))
                    .MTCCodeText = Trim(objReader.GetString("MTC_CODE_TEXT"))
                    .ReleaseRowID = objReader.GetInt32("RELEASE_ROW_ID")

                    If Not IsNothing(objReader.Item("JURISRELEASE_ROW_ID")) Then
                        .JurisReleaseRowId = objReader.GetInt32("JURISRELEASE_ROW_ID")
                    End If

                    If Not IsNothing(objReader.Item("MTC_TABLE_ROW_ID")) Then
                        .MTCTableRowID = objReader.GetInt32("MTC_TABLE_ROW_ID")
                    End If

                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")

                    Add(.DataHasChanged, .MTCCode, .MTCCodeText, CStr(.ReleaseRowID), CStr(.JurisReleaseRowId), .DeletedFlag, .TableRowID, .MTCTableRowID, "k" & .TableRowID)
                End With

                objRecord = Nothing



            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function

    Public Function SaveData(ByRef objuser As Riskmaster.Security.UserLogin) As Integer

        Const sFunctionName As String = "SaveData"
        Dim objMember As CJREDIStdMTCCode

        Try

            SaveData = 0

            If m_DataHasChanged Then
                For Each objMember In mCol
                    If objMember.SaveData(objuser) <> -1 Then
                        Throw (New Exception)
                    End If
                Next objMember
            End If

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            SaveData = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    Public Function Add(ByRef DataHasChanged As Boolean, ByRef MTCCode As String, ByRef MTCCodeText As String, ByRef ReleaseRowID As String, ByRef JurisReleaseRowId As String, ByRef Deleted_Flag As Boolean, ByRef TableRowID As Integer, ByRef MTCTableRowID As Integer, ByRef sKey As String) As CJREDIStdMTCCode


        Const sFunctionName As String = "Add"
        Dim objNewMember As CJREDIStdMTCCode
        Try
            objNewMember = New CJREDIStdMTCCode

            'set the properties passed into the method
            objNewMember.DataHasChanged = DataHasChanged
            objNewMember.MTCCode = MTCCode
            objNewMember.MTCCodeText = MTCCodeText
            objNewMember.ReleaseRowID = CInt(ReleaseRowID)
            objNewMember.JurisReleaseRowId = CInt(JurisReleaseRowId)
            objNewMember.TableRowID = TableRowID
            objNewMember.DeletedFlag = Deleted_Flag
            objNewMember.MTCTableRowID = MTCTableRowID
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object Add. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'Add = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJREDIStdMTCCode
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function


    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

