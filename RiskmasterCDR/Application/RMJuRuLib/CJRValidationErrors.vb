Option Strict Off
Option Explicit On
Public Class CJRValidationErrors
    Implements System.Collections.IEnumerable
    Private mCol As Collection
    Private Const sClassName As String = "CJRValidationErrors"

    Public Sub AddErrorObject(ByRef objError As CJRValidationError, Optional ByRef bIgnoreIfErrorAlreadyExists As Boolean = False)

        Dim i As Short
        Dim bErrorAlreadyExists As Boolean

        bErrorAlreadyExists = False

        If Not objError Is Nothing Then

            If bIgnoreIfErrorAlreadyExists Then

                mCol.Add(objError)

            Else

                If mCol.Count() > 0 Then

                    For i = 1 To mCol.Count()

                        'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item(i).ErrDescription. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item(i).ErrField. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If ((mCol.Item(i).ErrField = objError.ErrField) And (mCol.Item(i).ErrDescription = objError.ErrDescription)) Then

                            bErrorAlreadyExists = True
                            Exit For

                        End If

                    Next

                End If

                If Not bErrorAlreadyExists Then
                    mCol.Add(objError)
                End If

            End If

        End If



    End Sub

    Public Sub AppendValidationErrors(ByRef objErrors As Object)

        Dim i As Short

        If TypeOf objErrors Is CJRValidationErrors Then
            'UPGRADE_WARNING: Couldn't resolve default property of object objErrors.Count. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For i = 1 To objErrors.Count
                'UPGRADE_WARNING: Couldn't resolve default property of object objErrors.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                AddErrorObject(objErrors.Item(i))
            Next
        End If



    End Sub

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRValidationError
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function

    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        'creates the collection when this class is created
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'destroys collection when this class is terminated
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub

    Public Sub CreateUserDiaryWithErrors(ByRef objuser As Riskmaster.Security.UserLogin, ByRef sUser As String, ByRef sEntryName As String, ByRef sNote As String, ByRef sAttachTable As String, ByRef lAttachTableId As Integer, ByRef sRegarding As String)

        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Dim result As Short
        Dim lDiaryID As Integer
        Const sFunctionName As String = "GenerateUserDiaryWithErrors"
        Dim objValidationError As New CJRValidationError

        Try

            If sUser = "" Then Exit Sub

            'Get all errors bundled into one text to create the diary
            If mCol.Count() > 0 Then
                For Each objValidationError In mCol
                    sNote = sNote & objValidationError.ErrField & " - " & objValidationError.ErrRiskMasterField & " -- "
                    If Not objValidationError.ErrFieldValue = "" Then
                        sNote = sNote & "Value: " & objValidationError.ErrFieldValue & " -- "
                    End If
                    sNote = sNote & objValidationError.ErrDescription & vbCrLf
                Next objValidationError
            End If

            ' open diary and activity tables
            objReader = DbFactory.GetDbReader(g_ConnectionString, "SELECT * FROM WPA_DIARY_ENTRY WHERE ENTRY_ID = -1")
            Call SafeCloseRecordset(objReader)

            ' get unique id for diary entry
            lDiaryID = lGetNextUID("WPA_DIARY_ENTRY")

            ' commit a new record
            objWriter = DbFactory.GetDbWriter(objReader, False)

            ' stuff in values
            result = objWriter.Fields.Add("ENTRY_ID", lDiaryID)
            result = objWriter.Fields.Add("ENTRY_NAME", sEntryName)
            result = objWriter.Fields.Add("ENTRY_NOTES", sNote)
            result = objWriter.Fields.Add("CREATE_DATE", System.DateTime.Now().ToString("yyyyMMdd") & System.DateTime.Now().ToString("HHmmss"))

            result = objWriter.Fields.Add("PRIORITY", 0)

            result = objWriter.Fields.Add("STATUS_OPEN", 1)
            result = objWriter.Fields.Add("AUTO_CONFIRM", False)

            result = objWriter.Fields.Add("COMPLETE_TIME", "")
            result = objWriter.Fields.Add("ASSIGNED_USER", sUser)
            result = objWriter.Fields.Add("ASSIGNING_USER", objuser.UserId)
            result = objWriter.Fields.Add("ESTIMATE_TIME", 0)
            result = objWriter.Fields.Add("ASSIGNED_GROUP", "")
            result = objWriter.Fields.Add("DIARY_VOID", 0)
            result = objWriter.Fields.Add("DIARY_DELETED", 0)

            If sAttachTable <> "" And lAttachTableId > 0 Then
                result = objWriter.Fields.Add("IS_ATTACHED", True)
                result = objWriter.Fields.Add("ATTACH_TABLE", sAttachTable)
                result = objWriter.Fields.Add("ATTACH_RECORDID", Val(CStr(lAttachTableId)))
                result = objWriter.Fields.Add("REGARDING", sRegarding)
                result = objWriter.Fields.Add("ATT_SEC_REC_ID", 0)
            End If

            result = objWriter.Fields.Add("COMPLETE_DATE", System.DateTime.Now().ToString("yyyyMMdd"))

            ' commit values
            objWriter.Execute()

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeDropRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeDropRecordset(objReader)


        End Try

    End Sub
End Class

