Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleVRMAs
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRBenefitRuleVRMAs"

    'local variable to hold collection
    Private mCol As Collection
    Private Function GetFieldSQL() As String
        Dim sSQL As String
        GetFieldSQL = sSQL
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", BEGIN_DATE"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", END_DATE"
        sSQL = sSQL & ", JURIS_MAX_AMT"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", PAY_TTDRATE_CODE"
        GetFieldSQL = sSQL



    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim lTableRowID As Integer
        Try

            LoadData = 0



            sSQL = GetFieldSQL()
            sSQL = sSQL & " FROM WCP_VRMA_LIMITS"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                lTableRowID = objReader.GetInt32("TABLE_ROW_ID")
                Add(False, objReader.GetInt32("DELETED_FLAG"), objReader.GetString("EFFECTIVE_DATE"), objReader.GetString("END_DATE"), objReader.GetDouble("JURIS_MAX_AMT"), objReader.GetInt32("JURIS_ROW_ID"), objReader.GetInt32("PAY_TTDRATE_CODE"), objReader.GetInt32("TABLE_ROW_ID"), "k" & CStr(lTableRowID))

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function Add(ByRef DataHasChanged As Boolean, ByRef DeletedFlag As Short, ByRef EffectiveDateDTG As String, ByRef EndDateDTG As String, ByRef JurisMaxAmount As Double, ByRef JurisRowID As Short, ByRef PayTTDRateCode As Integer, ByRef TableRowID As Integer, ByRef sKey As String) As CJRBenefitRuleVRMA
        Try
            'create a new object
            Dim objNewMember As CJRBenefitRuleVRMA
            objNewMember = New CJRBenefitRuleVRMA


            'set the properties passed into the method
            objNewMember.JurisMaxAmount = JurisMaxAmount
            objNewMember.TableRowID = TableRowID
            objNewMember.JurisRowID = JurisRowID
            objNewMember.EndDateDTG = EndDateDTG
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.DataHasChanged = DataHasChanged
            objNewMember.PayTTDRateCode = PayTTDRateCode
            mCol.Add(objNewMember, sKey)


            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleVRMA
        Get
            'used when referencing an element in the collection
            'vntIndexKey contains either the Index or Key to the collection,
            'this is why it is declared as a Variant
            'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property



    Public ReadOnly Property Count() As Integer
        Get
            'used when retrieving the number of elements in the
            'collection. Syntax: Debug.Print x.Count
            Count = mCol.Count()
        End Get
    End Property


    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function


    Public Sub Remove(ByRef vntIndexKey As Object)
        'used when removing an element from the collection
        'vntIndexKey contains either the Index or Key, which is why
        'it is declared as a Variant
        'Syntax: x.Remove(xyz)


        mCol.Remove(vntIndexKey)


    End Sub


    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        'creates the collection when this class is created
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub


    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'destroys collection when this class is terminated
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

