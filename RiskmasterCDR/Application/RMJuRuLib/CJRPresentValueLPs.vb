Option Strict Off
Option Explicit On
Public Class CJRPresentValueLPs
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRPresentValueLPs"
    Private Const sTableName As String = "WCP_PRSNT_VALUE_LP"
    'local variable to hold collection
    Private mCol As Collection

    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", JURIS_ROW_ID,SEX_CODE_ID,AGE"
        sSQL = sSQL & ", YRS_0"
        sSQL = sSQL & ", YRS_1,YRS_2,YRS_3,YRS_4,YRS_5"
        sSQL = sSQL & ", YRS_6,YRS_7,YRS_8,YRS_9,YRS_10"
        sSQL = sSQL & ", YRS_11,YRS_12,YRS_13,YRS_14"
        sSQL = sSQL & " FROM " & sTableName
        GetBaseSQL = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJRPresentValueLP
        Try

            LoadData = 0



            sSQL = ""
            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " ORDER BY AGE, SEX_CODE_ID"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                objRecord = New CJRPresentValueLP
                With objRecord
                    .DataHasChanged = False
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .PresentAge = objReader.GetInt32("AGE")
                    .SexCode = objReader.GetInt32("SEX_CODE_ID")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .Year00 = objReader.GetDouble("YRS_0")
                    .Year01 = objReader.GetDouble("YRS_1")
                    .Year02 = objReader.GetDouble("YRS_2")
                    .Year03 = objReader.GetDouble("YRS_3")
                    .Year04 = objReader.GetDouble("YRS_4")
                    .Year05 = objReader.GetDouble("YRS_5")
                    .Year06 = objReader.GetDouble("YRS_6")
                    .Year07 = objReader.GetDouble("YRS_7")
                    .Year08 = objReader.GetDouble("YRS_8")
                    .Year09 = objReader.GetDouble("YRS_9")
                    .Year10 = objReader.GetDouble("YRS_10")
                    .Year11 = objReader.GetDouble("YRS_11")
                    .Year12 = objReader.GetDouble("YRS_12")
                    .Year13 = objReader.GetDouble("YRS_13")
                    .Year14 = objReader.GetDouble("YRS_14")
                    Add(0, .EffectiveDateDTG, .JurisRowID, .PresentAge, .SexCode, .TableRowID, .Year00, .Year01, .Year02, .Year03, .Year04, .Year05, .Year06, .Year07, .Year08, .Year09, .Year10, .Year11, .Year12, .Year13, .Year14, "k" & CStr(.TableRowID))
                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing

            End While
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Add(ByRef DeletedFlag As Integer, ByRef EffectiveDateDTG As String, ByRef JurisRowID As Integer, ByRef PresentAge As Integer, ByRef SexCode As Integer, ByRef TableRowID As Integer, ByRef Year00 As Double, ByRef Year01 As Double, ByRef Year02 As Double, ByRef Year03 As Double, ByRef Year04 As Double, ByRef Year05 As Double, ByRef Year06 As Double, ByRef Year07 As Double, ByRef Year08 As Double, ByRef Year09 As Double, ByRef Year10 As Double, ByRef Year11 As Double, ByRef Year12 As Double, ByRef Year13 As Double, ByRef Year14 As Double, Optional ByRef sKey As String = "") As CJRPresentValueLP
        Const sFunctionName As String = "Add"
        Dim objNewMember As CJRPresentValueLP
        Try

            objNewMember = New CJRPresentValueLP

            'jlt objNewMember.DelectedFlag = DeletedFlag
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.JurisRowID = JurisRowID
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.PresentAge = PresentAge
            objNewMember.SexCode = SexCode
            objNewMember.TableRowID = TableRowID
            objNewMember.Year10 = Year10
            objNewMember.Year00 = Year00
            objNewMember.Year01 = Year01
            objNewMember.Year02 = Year02
            objNewMember.Year03 = Year03
            objNewMember.Year04 = Year04
            objNewMember.Year05 = Year05
            objNewMember.Year06 = Year06
            objNewMember.Year07 = Year07
            objNewMember.Year08 = Year08
            objNewMember.Year09 = Year09
            objNewMember.Year11 = Year11
            objNewMember.Year12 = Year12
            objNewMember.Year13 = Year13
            objNewMember.Year14 = Year14

            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If
            'return the object created
            Add = objNewMember
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRPresentValueLP
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

