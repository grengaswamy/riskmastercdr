Option Strict Off
Option Explicit On
Friend Class CJRConditionalMust
	Private Const sClassName As String = "CJRConditionalMust"
	
	Private m_MustBeValidDate As Integer
	Private m_MustBeEntityType As Integer
	Private m_DataHasChanged As Boolean
	
	Public Property DataHasChanged() As Boolean
		Get
			DataHasChanged = m_DataHasChanged
		End Get
		Set(ByVal Value As Boolean)
			m_DataHasChanged = Value
		End Set
	End Property
	
	Public Property MustBeEntityType() As Integer
		Get
			MustBeEntityType = m_MustBeEntityType
		End Get
		Set(ByVal Value As Integer)
			m_MustBeEntityType = Value
		End Set
	End Property
	Public Property MustBeValidDate() As Integer
		Get
			MustBeValidDate = m_MustBeValidDate
		End Get
		Set(ByVal Value As Integer)
			m_MustBeValidDate = Value
		End Set
	End Property
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As dbReader
        Dim sSQL As String
        Try

            LoadData = 0
            ClearObject()



        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As dbReader
        Dim sSQL As String
        Try

            SaveData = 0


        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Private Function ClearObject() As Integer



    End Function
End Class

