Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleSPPDIL
    '---------------------------------------------------------------------------------------
    ' Module    : CJRBenefitRuleSPPDIL
    ' DateTime  : 7/19/2004 15:24
    ' Author    : jtodd22
    ' Purpose   : Permanent Partial Benefits based on Body Members
    '---------------------------------------------------------------------------------------
    Const sClassName As String = "CJRBenefitRuleSPPDIL"
    Const sTableName As String = "WCP_RULE_SPPD_IL"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821
    '------------------------------------------------------
    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MMIDateRequiredCode As Integer
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer
    '------------------------------------------------------
    'Class properties, local copy
    Private m_FixedMinimumAmount As Double
    Private m_FixedPercentage As Double
    Private m_LesserFixedAmount As Double
    Private m_LesserPercentSAWW As Double
    Private m_MaxFixedAmount As Double
    Private m_MaxPercentOfSAWW As Double
    Private m_MaxRateAmount As Double
    Private m_MaxRoundSAWWCode As Integer
    Private m_MaxLesserOfCode As Integer
    Private m_MinFixedAmount As Double
    Private m_MinPercentOfSAWW As Double
    Private m_MinRateAmount As Double
    Private m_MinRoundSAWWCode As Integer
    Private m_PayConcurrentCode As Integer
    Private m_PercentOfMaxTTD As Double
    Private m_UseTTDRateCode As Integer
    Private m_UseBodyMembersCode As Integer
    Private m_UseImpairPercentCode As Integer
    Private m_FixedPercentClaimantAWW As Double
    Private m_MaxAmpSawwRoundCode As Integer
    Private m_MaxAmpSAWWPercent As Double
    Private m_MaxAmpAmount As Double
    Private m_MaxAmpUseFourThirdsCode As Integer
    'jtodd22 08/01/2006--To support Illinois Minimum
    Private m_MarriedValue As Double
    Private m_SingleValue As Double
    Private m_Child1 As Double
    Private m_Child2 As Double
    Private m_Child3 As Double
    Private m_Child4 As Double
    Private m_ClaimantsAWWCode As Integer
    Private m_UseSAWWMinimumCode As Integer
    'New for 01 Febuary 2006 changes
    Private m_ChildSpouse0 As Double
    Private m_ChildSpouse1 As Double
    Private m_ChildSpouse2 As Double
    Private m_ChildSpouse3 As Double
    Private m_ChildSpouse4P As Double
    Private m_ClaimantsAWWBCode As Integer
    Private m_UseSAWWMinimumBCode As Integer

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_DataHasChanged = False
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
                'm_ErrorMaskJR jtodd22 not stored
                'm_ErrorMaskSAWW jtodd22 not stored
                m_FixedMinimumAmount = objReader.GetDouble("FIX_MINIMUM_AMT")
                m_FixedPercentage = objReader.GetDouble("FIXED_PERCT_NUMB")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_MaxFixedAmount = objReader.GetInt32("MAX_FIXED_AMOUNT")
                m_MaxLesserOfCode = objReader.GetInt32("MAX_LESSEROF_CODE")
                m_MaxPercentOfSAWW = objReader.GetInt32("MAX_PERCENTOF_SAWW")
                m_MaxRoundSAWWCode = objReader.GetInt32("MAX_ROUNDSAWW_CODE")
                m_MinFixedAmount = objReader.GetDouble("FIX_MINIMUM_AMT")
                m_MinPercentOfSAWW = objReader.GetDouble("MIN_PERCENT_SAWW")
                m_MinRoundSAWWCode = objReader.GetInt32("MIN_ROUNDSAWW_CODE")
                m_MMIDateRequiredCode = objReader.GetInt32("MMI_DATE_REQD_CODE")
                m_LesserFixedAmount = objReader.GetDouble("LESSERFIXED_AMOUNT")
                m_LesserPercentSAWW = objReader.GetDouble("LESSERPERCENT_SAWW")
                m_PayConcurrentCode = objReader.GetInt32("CONCURRENT_CODE")
                m_PercentOfMaxTTD = objReader.GetDouble("PERCENT_OF_MAX_TTD")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_UseTTDRateCode = objReader.GetInt32("USE_TTDRATE_CODE")
                m_UseBodyMembersCode = objReader.GetInt32("USE_BODYMEMBERCODE")
                m_UseImpairPercentCode = objReader.GetInt32("USE_IMPARPRCT_CODE")

                m_FixedPercentClaimantAWW = objReader.GetDouble("FIXED_PRECENT_AWW")
                m_MaxAmpSawwRoundCode = objReader.GetInt32("MAXAMP_SAWWRD_CODE")
                m_MaxAmpSAWWPercent = objReader.GetDouble("MAX_AMP_SAWW_PRCNT")
                m_MaxAmpAmount = objReader.GetDouble("MAX_AMP_AMOUNT")

                'jtodd22 08/01/2006 --group the minimum fields
                m_Child1 = objReader.GetDouble("CHILD_A")
                m_Child2 = objReader.GetDouble("CHILD_B")
                m_Child3 = objReader.GetDouble("CHILD_C")
                m_Child4 = objReader.GetDouble("CHILD_D")
                m_ClaimantsAWWCode = objReader.GetInt32("CLAIMANT_AWW_CODE")
                m_MarriedValue = objReader.GetDouble("MARRIED_VALUE")
                m_SingleValue = objReader.GetDouble("SINGLE_VALUE")
                m_UseSAWWMinimumCode = objReader.GetInt32("USE_SAWW_MIN_CODE")
                m_ChildSpouse0 = objReader.GetDouble("CHILD_SPOUSE_A_AMT")
                m_ChildSpouse1 = objReader.GetDouble("CHILD_SPOUSE_B_AMT")
                m_ChildSpouse2 = objReader.GetDouble("CHILD_SPOUSE_C_AMT")
                m_ChildSpouse3 = objReader.GetDouble("CHILD_SPOUSE_D_AMT")
                m_ChildSpouse4P = objReader.GetDouble("CHILD_SPOUSE_E_AMT")
                m_ClaimantsAWWBCode = objReader.GetInt32("CLAIMANT_AWWB_CODE")
                m_UseSAWWMinimumBCode = objReader.GetInt32("USE_SAWW_MINB_CODE")
            End If

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            If System.Math.Round(MaxAmpSAWWPercent, 2) = 133.33 Then
                m_MaxAmpUseFourThirdsCode = m_YesCodeID
            End If

            If m_UseBodyMembersCode = -1 Then m_UseBodyMembersCode = m_YesCodeID

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            AssignData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            AssignData = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_MMIDateRequiredCode = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_FixedMinimumAmount = 0
        m_FixedPercentage = 0
        m_LesserFixedAmount = 0
        m_LesserPercentSAWW = 0
        m_MaxFixedAmount = 0
        m_MaxPercentOfSAWW = 0
        m_MaxRoundSAWWCode = 0
        m_MaxLesserOfCode = 0
        m_MinFixedAmount = 0
        m_MinPercentOfSAWW = 0
        m_MinRoundSAWWCode = 0
        m_PayConcurrentCode = 0
        m_PercentOfMaxTTD = 0
        m_UseTTDRateCode = 0
        m_UseBodyMembersCode = 0
        m_UseImpairPercentCode = 0

        m_FixedPercentClaimantAWW = 0
        m_MaxAmpSawwRoundCode = 0
        m_MaxAmpSAWWPercent = 0
        m_MaxAmpAmount = 0
        m_MaxAmpUseFourThirdsCode = 0



    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", ADDED_BY_USER,DTTM_RCD_ADDED"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", EFFECTIVE_DATE, DELETED_FLAG"
        sSQL = sSQL & ", MIN_PERCENT_SAWW"
        sSQL = sSQL & ", USE_TTDRATE_CODE,USE_BODYMEMBERCODE"
        sSQL = sSQL & ", USE_IMPARPRCT_CODE,FIXED_PERCT_NUMB"
        sSQL = sSQL & ", LESSERFIXED_AMOUNT, LESSERPERCENT_SAWW,PERCENT_OF_MAX_TTD"
        sSQL = sSQL & ", CONCURRENT_CODE"
        sSQL = sSQL & ", FIX_MINIMUM_AMT"
        sSQL = sSQL & ", MAX_FIXED_AMOUNT"
        sSQL = sSQL & ", MAX_LESSEROF_CODE"
        sSQL = sSQL & ", MAX_PERCENTOF_SAWW"
        sSQL = sSQL & ", MAX_ROUNDSAWW_CODE"
        sSQL = sSQL & ", MIN_ROUNDSAWW_CODE"

        sSQL = sSQL & ", MAXAMP_SAWWRD_CODE,FIXED_PRECENT_AWW,MAX_AMP_SAWW_PRCNT,MAX_AMP_AMOUNT"
        'jtodd22 08/01/2006 --group the minimum fields
        sSQL = sSQL & ",CHILD_A"
        sSQL = sSQL & ",CHILD_B"
        sSQL = sSQL & ",CHILD_C"
        sSQL = sSQL & ",CHILD_D"
        sSQL = sSQL & ",CLAIMANT_AWW_CODE"
        sSQL = sSQL & ",MARRIED_VALUE"
        sSQL = sSQL & ",SINGLE_VALUE"
        sSQL = sSQL & ",USE_SAWW_MIN_CODE"
        sSQL = sSQL & ",CHILD_SPOUSE_A_AMT"
        sSQL = sSQL & ",CHILD_SPOUSE_B_AMT"
        sSQL = sSQL & ",CHILD_SPOUSE_C_AMT"
        sSQL = sSQL & ",CHILD_SPOUSE_D_AMT"
        sSQL = sSQL & ",CHILD_SPOUSE_E_AMT"
        sSQL = sSQL & ",CLAIMANT_AWWB_CODE"
        sSQL = sSQL & ",USE_SAWW_MINB_CODE"
        GetSQLFieldList = sSQL


    End Function

    Public Function ValidateData() As Integer

        ValidateData = 0
        m_Warning = ""

        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_MaxPercentOfSAWW < 1 Then
            m_Warning = m_Warning & "A 'Yes' or 'No' is required for 'Use Maximum From SAWW Table'." & vbCrLf
        End If
        If m_EffectiveDateDTG < "20060201" Then
            If m_UseSAWWMinimumCode < 1 Then
                m_Warning = m_Warning & "A 'Yes' or 'No' is required for 'Use Minimum From SAWW Table'." & vbCrLf
            End If
            If m_ClaimantsAWWCode < 1 Then
                m_Warning = m_Warning & "A 'Yes' or 'No' is required for 'Use Claimant's AWW'." & vbCrLf
            End If
        Else
            If m_UseSAWWMinimumBCode < 1 Then
                m_Warning = m_Warning & "A 'Yes' or 'No' is required for 'Use Minimum From SAWW Table'." & vbCrLf
            End If
            If m_ClaimantsAWWBCode < 1 Then
                m_Warning = m_Warning & "A 'Yes' or 'No' is required for 'Use Claimant's AWW'." & vbCrLf
            End If
        End If


    End Function
    Public Function BodyMembersExists(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Boolean
        BodyMembersExists = modFunctions.BodyMembersExists(objUser, lJurisRowID, sDateOfEventDTG)


    End Function

    Public Property NoCodeID() As Integer
        Get
            NoCodeID = m_NoCodeID
        End Get
        Set(ByVal Value As Integer)
            m_NoCodeID = Value
        End Set
    End Property

    Public Property PayConcurrentCode() As Integer
        Get
            PayConcurrentCode = m_PayConcurrentCode
        End Get
        Set(ByVal Value As Integer)
            m_PayConcurrentCode = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property JurisRowID() As Short
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Short)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property DeletedFlag() As Short
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Short)
            m_DeletedFlag = Value
        End Set
    End Property
    '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    Public Property MinPercentOfSAWW() As Double
        Get
            MinPercentOfSAWW = m_MinPercentOfSAWW
        End Get
        Set(ByVal Value As Double)
            m_MinPercentOfSAWW = Value
        End Set
    End Property

    Public Property UseTTDRateCode() As Integer
        Get
            UseTTDRateCode = m_UseTTDRateCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTTDRateCode = Value
        End Set
    End Property

    Public Property UseBodyMembersCode() As Integer
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property

    Public Property UseImpairPercentCode() As Integer
        Get
            UseImpairPercentCode = m_UseImpairPercentCode
        End Get
        Set(ByVal Value As Integer)
            m_UseImpairPercentCode = Value
        End Set
    End Property

    Public Property FixedMinimumAmount() As Double
        Get
            FixedMinimumAmount = m_FixedMinimumAmount
        End Get
        Set(ByVal Value As Double)
            m_FixedMinimumAmount = Value
        End Set
    End Property

    Public Property FixedPercentage() As Double
        Get
            FixedPercentage = m_FixedPercentage
        End Get
        Set(ByVal Value As Double)
            m_FixedPercentage = Value
        End Set
    End Property

    Public Property LesserFixedAmount() As Double
        Get
            LesserFixedAmount = m_LesserFixedAmount
        End Get
        Set(ByVal Value As Double)
            m_LesserFixedAmount = Value
        End Set
    End Property

    Public Property LesserPercentSAWW() As Double
        Get
            LesserPercentSAWW = m_LesserPercentSAWW
        End Get
        Set(ByVal Value As Double)
            m_LesserPercentSAWW = Value
        End Set
    End Property

    Public Property PercentOfMaxTTD() As Double
        Get
            PercentOfMaxTTD = m_PercentOfMaxTTD
        End Get
        Set(ByVal Value As Double)
            m_PercentOfMaxTTD = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property

    Public Property FixedPercentClaimantAWW() As Double
        Get
            FixedPercentClaimantAWW = m_FixedPercentClaimantAWW
        End Get
        Set(ByVal Value As Double)
            m_FixedPercentClaimantAWW = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property

    Public Property MaxFixedAmount() As Double
        Get
            MaxFixedAmount = m_MaxFixedAmount
        End Get
        Set(ByVal Value As Double)
            m_MaxFixedAmount = Value
        End Set
    End Property

    Public Property MaxLesserOfCode() As Integer
        Get
            MaxLesserOfCode = m_MaxLesserOfCode
        End Get
        Set(ByVal Value As Integer)
            m_MaxLesserOfCode = Value
        End Set
    End Property

    Public Property MaxPercentOfSAWW() As Double
        Get
            MaxPercentOfSAWW = m_MaxPercentOfSAWW
        End Get
        Set(ByVal Value As Double)
            m_MaxPercentOfSAWW = Value
        End Set
    End Property

    Public Property MaxRateAmount() As Double
        Get
            MaxRateAmount = m_MaxRateAmount
        End Get
        Set(ByVal Value As Double)
            m_MaxRateAmount = Value
        End Set
    End Property

    Public Property MaxRoundSAWWCode() As Integer
        Get
            MaxRoundSAWWCode = m_MaxRoundSAWWCode
        End Get
        Set(ByVal Value As Integer)
            m_MaxRoundSAWWCode = Value
        End Set
    End Property

    Public Property MinFixedAmount() As Double
        Get
            MinFixedAmount = m_MinFixedAmount
        End Get
        Set(ByVal Value As Double)
            m_MinFixedAmount = Value
        End Set
    End Property

    Public Property MinRoundSAWWCode() As Integer
        Get
            MinRoundSAWWCode = m_MinRoundSAWWCode
        End Get
        Set(ByVal Value As Integer)
            m_MinRoundSAWWCode = Value
        End Set
    End Property

    Public Property MinRateAmount() As Double
        Get
            MinRateAmount = m_MinRateAmount
        End Get
        Set(ByVal Value As Double)
            m_MinRateAmount = Value
        End Set
    End Property

    Public Property MaxAmpSawwRoundCode() As Integer
        Get
            MaxAmpSawwRoundCode = m_MaxAmpSawwRoundCode
        End Get
        Set(ByVal Value As Integer)
            m_MaxAmpSawwRoundCode = Value
        End Set
    End Property

    Public Property MaxAmpSAWWPercent() As Double
        Get
            MaxAmpSAWWPercent = m_MaxAmpSAWWPercent
        End Get
        Set(ByVal Value As Double)
            m_MaxAmpSAWWPercent = Value
        End Set
    End Property

    Public Property MaxAmpAmount() As Double
        Get
            MaxAmpAmount = m_MaxAmpAmount
        End Get
        Set(ByVal Value As Double)
            m_MaxAmpAmount = Value
        End Set
    End Property

    Public Property MaxAmpUseFourThirdsCode() As Integer
        Get
            MaxAmpUseFourThirdsCode = m_MaxAmpUseFourThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_MaxAmpUseFourThirdsCode = Value
        End Set
    End Property

    Public Property MarriedValue() As Double
        Get
            MarriedValue = m_MarriedValue
        End Get
        Set(ByVal Value As Double)
            m_MarriedValue = Value
        End Set
    End Property

    Public Property MMIDateRequiredCode() As Integer
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property

    Public Property SingleValue() As Double
        Get
            SingleValue = m_SingleValue
        End Get
        Set(ByVal Value As Double)
            m_SingleValue = Value
        End Set
    End Property

    Public Property Child1() As Double
        Get
            Child1 = m_Child1
        End Get
        Set(ByVal Value As Double)
            m_Child1 = Value
        End Set
    End Property

    Public Property Child2() As Double
        Get
            Child2 = m_Child2
        End Get
        Set(ByVal Value As Double)
            m_Child2 = Value
        End Set
    End Property

    Public Property Child3() As Double
        Get
            Child3 = m_Child3
        End Get
        Set(ByVal Value As Double)
            m_Child3 = Value
        End Set
    End Property

    Public Property Child4() As Double
        Get
            Child4 = m_Child4
        End Get
        Set(ByVal Value As Double)
            m_Child4 = Value
        End Set
    End Property
    Public Property ClaimantsAWWCode() As Integer
        Get
            ClaimantsAWWCode = m_ClaimantsAWWCode
        End Get
        Set(ByVal Value As Integer)
            m_ClaimantsAWWCode = Value
        End Set
    End Property


    Public Property ChildSpouse0() As Double
        Get
            ChildSpouse0 = m_ChildSpouse0
        End Get
        Set(ByVal Value As Double)
            m_ChildSpouse0 = Value
        End Set
    End Property


    Public Property ChildSpouse1() As Double
        Get
            ChildSpouse1 = m_ChildSpouse1
        End Get
        Set(ByVal Value As Double)
            m_ChildSpouse1 = Value
        End Set
    End Property


    Public Property ChildSpouse2() As Double
        Get
            ChildSpouse2 = m_ChildSpouse2
        End Get
        Set(ByVal Value As Double)
            m_ChildSpouse2 = Value
        End Set
    End Property


    Public Property ChildSpouse3() As Double
        Get
            ChildSpouse3 = m_ChildSpouse3
        End Get
        Set(ByVal Value As Double)
            m_ChildSpouse3 = Value
        End Set
    End Property


    Public Property ChildSpouse4P() As Double
        Get
            ChildSpouse4P = m_ChildSpouse4P
        End Get
        Set(ByVal Value As Double)
            m_ChildSpouse4P = Value
        End Set
    End Property


    Public Property ClaimantsAWWBCode() As Integer
        Get
            ClaimantsAWWBCode = m_ClaimantsAWWBCode
        End Get
        Set(ByVal Value As Integer)
            m_ClaimantsAWWBCode = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property

    Public Property UseSAWWMinimumCode() As Integer
        Get
            UseSAWWMinimumCode = m_UseSAWWMinimumCode
        End Get
        Set(ByVal Value As Integer)
            m_UseSAWWMinimumCode = Value
        End Set
    End Property


    Public Property UseSAWWMinimumBCode() As Integer
        Get
            UseSAWWMinimumBCode = m_UseSAWWMinimumBCode
        End Get
        Set(ByVal Value As Integer)
            m_UseSAWWMinimumBCode = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property

    Public Property YesCodeID() As Integer
        Get
            YesCodeID = m_YesCodeID
        End Get
        Set(ByVal Value As Integer)
            m_YesCodeID = Value
        End Set
    End Property

    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim sSQL As String
        Try
            LoadDataByTableRowID = 0
            ClearObject()

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND TABLE_ROW_ID = " & lTableRowID

            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadDataByEventDate
    ' DateTime  : 1/27/2005 09:35
    ' Author    : jtodd22
    ' Purpose   : To fetch Event related data
    ' Note      : Must have 'ORDER BY EFFECTIVE_DATE' clause.  We expect multiple records
    ' ..........: and want the closest record not after the Date Of Event
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataByEventDate|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("MAX_FIXED_AMOUNT").Value = m_MaxFixedAmount
                objWriter.Fields("MAX_LESSEROF_CODE").Value = m_MaxLesserOfCode
                objWriter.Fields("MAX_PERCENTOF_SAWW").Value = m_MaxPercentOfSAWW
                objWriter.Fields("MAX_ROUNDSAWW_CODE").Value = m_MaxRoundSAWWCode
                objWriter.Fields("MIN_PERCENT_SAWW").Value = m_MinPercentOfSAWW
                objWriter.Fields("MIN_ROUNDSAWW_CODE").Value = m_MinRoundSAWWCode

                objWriter.Fields("MMI_DATE_REQD_CODE").Value = m_MMIDateRequiredCode

                objWriter.Fields("USE_TTDRATE_CODE").Value = m_UseTTDRateCode
                objWriter.Fields("USE_BODYMEMBERCODE").Value = m_UseBodyMembersCode
                objWriter.Fields("USE_IMPARPRCT_CODE").Value = m_UseImpairPercentCode
                objWriter.Fields("FIX_MINIMUM_AMT").Value = m_FixedMinimumAmount
                objWriter.Fields("FIXED_PERCT_NUMB").Value = m_FixedPercentage
                objWriter.Fields("LESSERFIXED_AMOUNT").Value = m_LesserFixedAmount
                objWriter.Fields("LESSERPERCENT_SAWW").Value = m_LesserPercentSAWW
                objWriter.Fields("PERCENT_OF_MAX_TTD").Value = m_PercentOfMaxTTD
                objWriter.Fields("CONCURRENT_CODE").Value = m_PayConcurrentCode

                objWriter.Fields("FIXED_PRECENT_AWW").Value = m_FixedPercentClaimantAWW
                objWriter.Fields("MAXAMP_SAWWRD_CODE").Value = m_MaxAmpSawwRoundCode
                objWriter.Fields("MAX_AMP_SAWW_PRCNT").Value = m_MaxAmpSAWWPercent
                objWriter.Fields("MAX_AMP_AMOUNT").Value = m_MaxAmpAmount

                'jtodd22 08/01/2006 -group the Minimum fields
                objWriter.Fields("CHILD_A").Value = m_Child1
                objWriter.Fields("CHILD_B").Value = m_Child2
                objWriter.Fields("CHILD_C").Value = m_Child3
                objWriter.Fields("CHILD_D").Value = m_Child4
                objWriter.Fields("CLAIMANT_AWW_CODE").Value = m_ClaimantsAWWCode
                objWriter.Fields("MARRIED_VALUE").Value = m_MarriedValue
                objWriter.Fields("SINGLE_VALUE").Value = m_SingleValue
                objWriter.Fields("USE_SAWW_MIN_CODE").Value = m_UseSAWWMinimumCode
                objWriter.Fields("CHILD_SPOUSE_A_AMT").Value = m_ChildSpouse0
                objWriter.Fields("CHILD_SPOUSE_B_AMT").Value = m_ChildSpouse1
                objWriter.Fields("CHILD_SPOUSE_C_AMT").Value = m_ChildSpouse2
                objWriter.Fields("CHILD_SPOUSE_D_AMT").Value = m_ChildSpouse3
                objWriter.Fields("CHILD_SPOUSE_E_AMT").Value = m_ChildSpouse4P
                objWriter.Fields("CLAIMANT_AWWB_CODE").Value = m_ClaimantsAWWBCode
                objWriter.Fields("USE_SAWW_MINB_CODE").Value = m_UseSAWWMinimumBCode

            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("MAX_FIXED_AMOUNT", m_MaxFixedAmount)
                objWriter.Fields.Add("MAX_LESSEROF_CODE", m_MaxLesserOfCode)
                objWriter.Fields.Add("MAX_PERCENTOF_SAWW", m_MaxPercentOfSAWW)
                objWriter.Fields.Add("MAX_ROUNDSAWW_CODE", m_MaxRoundSAWWCode)
                objWriter.Fields.Add("MIN_PERCENT_SAWW", m_MinPercentOfSAWW)
                objWriter.Fields.Add("MIN_ROUNDSAWW_CODE", m_MinRoundSAWWCode)

                objWriter.Fields.Add("MMI_DATE_REQD_CODE", m_MMIDateRequiredCode)

                objWriter.Fields.Add("USE_TTDRATE_CODE", m_UseTTDRateCode)
                objWriter.Fields.Add("USE_BODYMEMBERCODE", m_UseBodyMembersCode)
                objWriter.Fields.Add("USE_IMPARPRCT_CODE", m_UseImpairPercentCode)
                objWriter.Fields.Add("FIX_MINIMUM_AMT", m_FixedMinimumAmount)
                objWriter.Fields.Add("FIXED_PERCT_NUMB", m_FixedPercentage)
                objWriter.Fields.Add("LESSERFIXED_AMOUNT", m_LesserFixedAmount)
                objWriter.Fields.Add("LESSERPERCENT_SAWW", m_LesserPercentSAWW)
                objWriter.Fields.Add("PERCENT_OF_MAX_TTD", m_PercentOfMaxTTD)
                objWriter.Fields.Add("CONCURRENT_CODE", m_PayConcurrentCode)

                objWriter.Fields.Add("FIXED_PRECENT_AWW", m_FixedPercentClaimantAWW)
                objWriter.Fields.Add("MAXAMP_SAWWRD_CODE", m_MaxAmpSawwRoundCode)
                objWriter.Fields.Add("MAX_AMP_SAWW_PRCNT", m_MaxAmpSAWWPercent)
                objWriter.Fields.Add("MAX_AMP_AMOUNT", m_MaxAmpAmount)

                'jtodd22 08/01/2006 -group the Minimum fields
                objWriter.Fields.Add("CHILD_A", m_Child1)
                objWriter.Fields.Add("CHILD_B", m_Child2)
                objWriter.Fields.Add("CHILD_C", m_Child3)
                objWriter.Fields.Add("CHILD_D", m_Child4)
                objWriter.Fields.Add("CLAIMANT_AWW_CODE", m_ClaimantsAWWCode)
                objWriter.Fields.Add("MARRIED_VALUE", m_MarriedValue)
                objWriter.Fields.Add("SINGLE_VALUE", m_SingleValue)
                objWriter.Fields.Add("USE_SAWW_MIN_CODE", m_UseSAWWMinimumCode)
                objWriter.Fields.Add("CHILD_SPOUSE_A_AMT", m_ChildSpouse0)
                objWriter.Fields.Add("CHILD_SPOUSE_B_AMT", m_ChildSpouse1)
                objWriter.Fields.Add("CHILD_SPOUSE_C_AMT", m_ChildSpouse2)
                objWriter.Fields.Add("CHILD_SPOUSE_D_AMT", m_ChildSpouse3)
                objWriter.Fields.Add("CHILD_SPOUSE_E_AMT", m_ChildSpouse4P)
                objWriter.Fields.Add("CLAIMANT_AWWB_CODE", m_ClaimantsAWWBCode)
                objWriter.Fields.Add("USE_SAWW_MINB_CODE", m_UseSAWWMinimumBCode)

            End If

            objWriter.Execute()


            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Delete(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try
            Delete = 0

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " *"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DELETED_FLAG").Value = -1
            End If

            Delete = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Delete|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try


    End Function
End Class

