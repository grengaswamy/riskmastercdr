Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleLOEPs
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRBenefitRuleLOEPs"
    '                             123456789012345678
    Const sTableName As String = "WCP_RULE_LOEP"
    'Class properties, local copy
    Private mCol As Collection
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE,JURIS_ROW_ID,DELETED_FLAG"
        '                    123456789012345678
        sSQL = sSQL & ",MINLOSTREQDPERCENT"
        sSQL = sSQL & ",MDB_BEGDATEDBFRMAT"
        sSQL = sSQL & ",USEHIMDFRRATE_CODE"
        sSQL = sSQL & ",MAXTILOSSRATE_CODE"
        sSQL = sSQL & ",MAXTISAW_RATE_CODE"
        GetSQLFieldList = sSQL


    End Function


    Public Function Add(ByRef DataHasChanged As Boolean, ByRef DeletedFlag As Integer, ByRef EffectiveDateDTG As String, ByRef JurisRowID As Integer, ByRef MaxTimeLossRateCode As Integer, ByRef MaxTimesSAWRateCode As Integer, ByRef MethodBBeginDateDBFormat As String, ByRef MinimumLostRequiredPercent As Double, ByRef TableRowID As Integer, ByRef UseHigherMethodForRateCode As Integer, ByRef sKey As String) As CJRBenefitRuleLOEP
        'create a new object
        Dim objNewMember As CJRBenefitRuleLOEP
        objNewMember = New CJRBenefitRuleLOEP

        'set the properties passed into the method
        With objNewMember
            .DataHasChanged = DataHasChanged
            .DeletedFlag = DeletedFlag
            .EffectiveDateDTG = EffectiveDateDTG
            .JurisRowID = JurisRowID
            .MethodBBeginDateDBFormat = MethodBBeginDateDBFormat
            .MinimumLostRequiredPercent = MinimumLostRequiredPercent
            .MaxTimeLossRateCode = MaxTimeLossRateCode
            .MaxTimesSAWRateCode = MaxTimesSAWRateCode
            .UseHigherMethodForRateCode = UseHigherMethodForRateCode
            .TableRowID = TableRowID
        End With
        mCol.Add(objNewMember, sKey)


        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing




    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleLOEP
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByVal lJurisID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRule As CJRBenefitRuleLOEP
        Try

            LoadData = 0
            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRule = New CJRBenefitRuleLOEP
                With objRule
                    .DataHasChanged = False
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .MaxTimeLossRateCode = objReader.GetInt32("MAXTILOSSRATE_CODE")
                    .MaxTimesSAWRateCode = objReader.GetInt32("MAXTISAW_RATE_CODE")
                    .MethodBBeginDateDBFormat = objReader.GetString("MDB_BEGDATEDBFRMAT")
                    .MinimumLostRequiredPercent = objReader.GetDouble("MINLOSTREQDPERCENT")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .UseHigherMethodForRateCode = objReader.GetInt32("USEHIMDFRRATE_CODE")
                    Add(.DataHasChanged, .DeletedFlag, .EffectiveDateDTG, .JurisRowID, .MaxTimeLossRateCode, .MaxTimesSAWRateCode, .MethodBBeginDateDBFormat, .MinimumLostRequiredPercent, .TableRowID, .UseHigherMethodForRateCode, "k" & CStr(.TableRowID))
                End With
                'UPGRADE_NOTE: Object objRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRule = Nothing

            End While

            SafeCloseRecordset(objReader)
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
End Class

