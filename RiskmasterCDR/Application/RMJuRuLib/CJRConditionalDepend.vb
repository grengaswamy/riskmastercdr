Option Strict Off
Option Explicit On
Friend Class CJRConditionalDepend
	Private Const sClassName As String = "CJRConditionalDepend"
	
	Private m_DependsOnFieldValue As Integer
	Private m_DependsOnEntityType As Integer
	Private m_DependsOnDeathDate As Integer
	Private m_DependsOnSelfInsured As Integer
	Private m_DependsOnDateOfEvent As Integer
	Private m_DataHasChanged As Boolean
	
	Public Property DataHasChanged() As Boolean
		Get
			DataHasChanged = m_DataHasChanged
		End Get
		Set(ByVal Value As Boolean)
			m_DataHasChanged = Value
		End Set
	End Property
	
	Public Property DependsOnDateOfEvent() As Integer
		Get
			DependsOnDateOfEvent = m_DependsOnDateOfEvent
		End Get
		Set(ByVal Value As Integer)
			m_DependsOnDateOfEvent = Value
		End Set
	End Property
	Public Property DependsOnSelfInsured() As Integer
		Get
			DependsOnSelfInsured = m_DependsOnSelfInsured
		End Get
		Set(ByVal Value As Integer)
			m_DependsOnSelfInsured = Value
		End Set
	End Property
	Public Property DependsOnDeathDate() As Integer
		Get
			DependsOnDeathDate = m_DependsOnDeathDate
		End Get
		Set(ByVal Value As Integer)
			m_DependsOnDeathDate = Value
		End Set
	End Property
	Public Property DependsOnEntityType() As Integer
		Get
			DependsOnEntityType = m_DependsOnEntityType
		End Get
		Set(ByVal Value As Integer)
			m_DependsOnEntityType = Value
		End Set
	End Property
	Public Property DependsOnFieldValue() As Integer
		Get
			DependsOnFieldValue = m_DependsOnFieldValue
		End Get
		Set(ByVal Value As Integer)
			m_DependsOnFieldValue = Value
		End Set
	End Property
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As dbReader
        Dim sSQL As String
        Try

            LoadData = 0
            ClearObject()



        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As dbReader
        Dim sSQL As String
        Try

            SaveData = 0



        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Private Function ClearObject() As Integer



    End Function
End Class


