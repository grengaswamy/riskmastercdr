Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.


' TODO: Review the values of the assembly attributes


<Assembly: AssemblyTitle("Workers' Compensation Configuration Tool Library")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Computer Sciences Corporation")>
<Assembly: AssemblyProduct("Jurisdictional Rules DLL")>
<Assembly: AssemblyCopyright("Copyright � 2004. Computer Sciences Corporation. All rights reserved")>
<Assembly: AssemblyTrademark("")>
<Assembly: AssemblyCulture("")>

' Version information for an assembly consists of the following four values:

'	Major version
'	Minor Version
'	Build Number
'	Revision

' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:

<Assembly: AssemblyVersion("14.1.0.0")> 
<Assembly: AssemblyFileVersion("14.1.0.0")> 
<Assembly: AssemblyInformationalVersion("14.1.0.0")> 





