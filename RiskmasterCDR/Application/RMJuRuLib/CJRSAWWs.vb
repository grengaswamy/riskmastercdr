Option Strict Off
Option Explicit On
Public Class CJRSAWWs
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRSAWWs"

    'Class properties, local copy
    Private mCol As Collection
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objSAWW As CJRSAWW
        Try

            LoadData = 0


            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " TABLE_ROW_ID, JURIS_ROW_ID, EFFECTIVE_DATE, DELETED_FLAG, SAWW_AMOUNT"
            sSQL = sSQL & ", PERCENT_ANN_CHGE"
            sSQL = sSQL & ", MAX_PERCENTAGE, MAX_AMT_PUBLISHED, MIN_PERCENTAGE, MIN_AMT_PUBLISHED"
            sSQL = sSQL & " FROM WCP_SAWW_LKUP"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " ORDER BY EFFECTIVE_DATE"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objSAWW = New CJRSAWW
                With objSAWW
                    '.DeletedFlag = objReader.GetInt32( "DELETED_FLAG"))
                    .EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
                    .JurisRowID = lJurisRowID
                    .MaxAmountAsPublished = objReader.GetDouble("MAX_AMT_PUBLISHED")
                    .MinAmountAsPublished = objReader.GetDouble("MIN_AMT_PUBLISHED")
                    .PercentAnnualChange = objReader.GetDouble("PERCENT_ANN_CHGE")
                    .PercentMax = objReader.GetDouble("MAX_PERCENTAGE")
                    .PercentMin = objReader.GetDouble("MIN_PERCENTAGE")
                    .SAWWAmount = objReader.GetDouble("SAWW_AMOUNT")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.EffectiveDateDTG, .JurisRowID, .MaxAmountAsPublished, .MinAmountAsPublished, .PercentAnnualChange, .PercentMax, .PercentMin, .SAWWAmount, .TableRowID, "k" & CStr(.TableRowID))
                End With
                'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objSAWW = Nothing

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function


    Public Function Add(ByRef EffectiveDateDTG As String, ByRef JurisRowID As Integer, ByRef MaxAmountAsPublished As Double, ByRef MinAmountAsPublished As Double, ByRef PercentAnnualChange As Double, ByRef PercentMax As Double, ByRef PercentMin As Double, ByRef SAWWAmount As Decimal, ByRef TableRowID As Integer, ByRef sKey As String) As CJRSAWW
        'create a new object
        Dim objNewMember As CJRSAWW
        objNewMember = New CJRSAWW


        'set the properties passed into the method
        objNewMember.JurisRowID = JurisRowID
        objNewMember.EffectiveDateDTG = EffectiveDateDTG
        objNewMember.PercentAnnualChange = PercentAnnualChange
        objNewMember.SAWWAmount = SAWWAmount
        objNewMember.TableRowID = TableRowID
        objNewMember.MaxAmountAsPublished = MaxAmountAsPublished
        objNewMember.MinAmountAsPublished = MinAmountAsPublished
        objNewMember.PercentMax = PercentMax
        objNewMember.PercentMin = PercentMin

        mCol.Add(objNewMember, sKey)

        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing




    End Function
    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRSAWW
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

