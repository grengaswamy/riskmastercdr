Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleTPDNtSAAs
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRBenefitRuleTPDNtSAA"
    Private Const sTableName As String = "WCP_RULE_TPDNTS"

    'Class properties, local copy
    Private mCol As Collection
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        'jtodd22 12/22/2004--sSQL = sSQL & ", BEGIN_DATE"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", END_DATE"
        sSQL = sSQL & ", MIN_BENEFIT,MAX_AWW"
        sSQL = sSQL & ", MAX_COMP_RATE"
        sSQL = sSQL & ", PAY_FLOOR_CODE"
        sSQL = sSQL & ", MAX_BENEFIT"
        sSQL = sSQL & ", PAY_DOLLAR_CODE"
        sSQL = sSQL & ", PRIME_RATE"
        sSQL = sSQL & ", PR_RAT_MAX_BENFIT"
        sSQL = sSQL & ", PR_RAT_MAX_WEEKS"
        sSQL = sSQL & ", SECOND_RATE"
        sSQL = sSQL & ", SC_RAT_MAX_WEEKS"
        sSQL = sSQL & ", SC_RAT_MAX_BENFIT"
        sSQL = sSQL & ", TOTAL_WEEKS"
        sSQL = sSQL & ", TOTAL_AMT,USE_TWOYEAR_CODE"
        sSQL = sSQL & ", PAY_CONC_PPD_CODE"
        sSQL = sSQL & ", USE_SAWW_MAX_CODE"
        sSQL = sSQL & ", USE_SAWW_MIN_CODE"
        sSQL = sSQL & ", LESS_HOURLY_AMT"
        sSQL = sSQL & ", ERNNGS_REQUED_CODE"
        sSQL = sSQL & ", INCL_TT_WEEKS_CODE"
        sSQL = sSQL & ", USE_DISCOUNT_CODE"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sBeginDateDTG As String) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objTTDRule As CJRBenefitRuleTPDNtSAA
        Try

            LoadData = 0


            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            If sBeginDateDTG > "" Then
                sSQL = sSQL & " AND EFFECTIVE_DATE = '" & sBeginDateDTG & "'"
            End If
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objTTDRule = New CJRBenefitRuleTPDNtSAA
                With objTTDRule
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .DollarForDollar = objReader.GetInt32("PAY_DOLLAR_CODE")
                    .EarningsRequiredCode = objReader.GetInt32("ERNNGS_REQUED_CODE")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .EndingDateDTG = objReader.GetString("END_DATE")
                    .FloorAmount = objReader.GetDouble("MIN_BENEFIT")
                    .IncludeTempTotalWeeksCode = objReader.GetInt32("INCL_TT_WEEKS_CODE")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .LessThanHourlyRate = objReader.GetDouble("LESS_HOURLY_AMT")
                    .MaxAWW = objReader.GetDouble("MAX_AWW")
                    .MaxCompRateWeekly = objReader.GetDouble("MAX_COMP_RATE")
                    .PayFloorAmount = objReader.GetInt32("PAY_FLOOR_CODE")
                    .PayConCurrentPPD = objReader.GetDouble("PAY_CONC_PPD_CODE")
                    .PayCurrentRateAfterTwoYears = objReader.GetInt32("USE_TWOYEAR_CODE")
                    .MaxRatePercentSAWW = objReader.GetDouble("MAX_BENEFIT")
                    .PrimeRate = objReader.GetDouble("PRIME_RATE")
                    .PrimeRateMaxWeeks = objReader.GetInt32("PR_RAT_MAX_WEEKS")
                    .PrimeRateMaxAmount = objReader.GetDouble("PR_RAT_MAX_BENFIT")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .SecondRate = objReader.GetDouble("SECOND_RATE")
                    .SecondRateMaxWeeks = objReader.GetInt32("SC_RAT_MAX_WEEKS")
                    .SecondRateMaxAmount = objReader.GetDouble("SC_RAT_MAX_BENFIT")
                    .TotalAmount = objReader.GetDouble("TOTAL_AMT")
                    .RuleTotalWeeks = objReader.GetInt32("TOTAL_WEEKS")
                    .UseDiscountCode = objReader.GetInt32("USE_DISCOUNT_CODE")

                    .UseSAWWMaximumCode = objReader.GetInt32("USE_SAWW_MAX_CODE")
                    .UseSAWWMinimumCode = objReader.GetInt32("USE_SAWW_MIN_CODE")
                    Add(.DeletedFlag, .DollarForDollar, .EarningsRequiredCode, .EffectiveDateDTG, .EndingDateDTG, .FloorAmount, .IncludeTempTotalWeeksCode, .JurisDefinedWorkWeek, .JurisRowID, .LessThanHourlyRate, .MaxAWW, .MaxCompRateWeekly, .PayConCurrentPPD, .PayCurrentRateAfterTwoYears, .PayFloorAmount, .MaxRatePercentSAWW, .PrimeRate, .PrimeRateMaxAmount, .PrimeRateMaxWeeks, .SecondRate, .SecondRateMaxAmount, .SecondRateMaxWeeks, .TableRowID, .TotalAmount, .RuleTotalWeeks, .UseSAWWMaximumCode, .UseSAWWMinimumCode, "k" & .TableRowID)
                End With
                'UPGRADE_NOTE: Object objTTDRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objTTDRule = Nothing

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function Add(ByRef DeletedFlag As Integer, ByRef DollarForDollar As Integer, ByRef EarningsRequiredCode As Integer, ByRef EffectiveDateDTG As String, ByRef EndingDateDTG As String, ByRef FloorAmount As Double, ByRef IncludeTempTotalWeeksCode As Integer, ByRef JurisDefinedWorkWeek As Integer, ByRef JurisRowID As Integer, ByRef LessThanHourlyRate As Double, ByRef MaxAWW As Double, ByRef MaxCompRateWeekly As Double, ByRef PayConCurrentPPD As Integer, ByRef PayCurrentRateAfterTwoYears As Integer, ByRef PayFloorAmount As Integer, ByRef MaxPercentSAWW As Double, ByRef PrimeRate As Double, ByRef PrimeRateMaxAmount As Integer, ByRef PrimeRateMaxWeeks As Integer, ByRef SecondRate As Double, ByRef SecondRateMaxAmount As Double, ByRef SecondRateMaxWeeks As Integer, ByRef TableRowID As Integer, ByRef TotalAmount As Double, ByRef TotalWeeks As Integer, ByRef UseSAWWMaximumCode As Integer, ByRef UseSAWWMinimumCode As Integer, ByRef sKey As String) As CJRBenefitRuleTPDNtSAA

        Try
            'create a new object
            Dim objNewMember As CJRBenefitRuleTPDNtSAA
            objNewMember = New CJRBenefitRuleTPDNtSAA

            'set the properties passed into the method
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.DollarForDollar = DollarForDollar
            objNewMember.EarningsRequiredCode = EarningsRequiredCode
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.EndingDateDTG = EndingDateDTG
            objNewMember.FloorAmount = FloorAmount
            objNewMember.IncludeTempTotalWeeksCode = IncludeTempTotalWeeksCode
            objNewMember.JurisDefinedWorkWeek = JurisDefinedWorkWeek
            objNewMember.JurisRowID = JurisRowID
            objNewMember.LessThanHourlyRate = LessThanHourlyRate
            objNewMember.MaxAWW = MaxAWW
            objNewMember.MaxCompRateWeekly = MaxCompRateWeekly
            objNewMember.PayConCurrentPPD = PayConCurrentPPD
            objNewMember.PayCurrentRateAfterTwoYears = PayCurrentRateAfterTwoYears
            objNewMember.PayFloorAmount = PayFloorAmount
            objNewMember.MaxRatePercentSAWW = MaxPercentSAWW
            objNewMember.PrimeRate = PrimeRate
            objNewMember.PrimeRateMaxAmount = PrimeRateMaxAmount
            objNewMember.PrimeRateMaxWeeks = PrimeRateMaxWeeks
            objNewMember.SecondRate = SecondRate
            objNewMember.SecondRateMaxAmount = SecondRateMaxAmount
            objNewMember.SecondRateMaxWeeks = SecondRateMaxWeeks
            objNewMember.TableRowID = TableRowID
            objNewMember.TotalAmount = TotalAmount
            objNewMember.RuleTotalWeeks = TotalWeeks
            objNewMember.UseSAWWMaximumCode = UseSAWWMaximumCode
            objNewMember.UseSAWWMinimumCode = UseSAWWMinimumCode
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleTPDNtSAA
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

