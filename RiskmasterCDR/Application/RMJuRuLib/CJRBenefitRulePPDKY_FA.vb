Option Strict Off
Option Explicit On
Public Class CJRBenefitRulePPDKY_FA
    Const sClassName As String = "CJRBenefitRulePPDKY_FA"
    Const sTableName As String = "WCP_RULE_PPD_KY_FA"
    'local variable(s) to hold property value(s)
    Private m_DataHasChanged As Boolean 'local copy
    Private m_EffectiveDateDTG As String
    Private m_FactorValue As Double
    Private m_HighValue As Double
    Private m_JurisRowID As Integer
    Private m_LowValue As Double
    Private m_TableRowID As Integer
    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_FactorValue = objReader.GetDouble("FACTOR_VALUE")
                m_HighValue = objReader.GetDouble("HIGH_VALUE")
                m_LowValue = objReader.GetDouble("LOW_VALUE")
            End If

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer



    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        '                  '123456789012345678
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", LOW_VALUE"
        sSQL = sSQL & ", HIGH_VALUE"
        sSQL = sSQL & ", FACTOR_VALUE"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadDataTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataTableRowID"
        Dim sSQL As String
        Try
            LoadDataTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try
            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT "
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("FACTOR_VALUE").Value = m_FactorValue
                objWriter.Fields("HIGH_VALUE").Value = m_HighValue
                objWriter.Fields("LOW_VALUE").Value = m_LowValue

            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("FACTOR_VALUE", m_FactorValue)
                objWriter.Fields.Add("HIGH_VALUE", m_HighValue)
                objWriter.Fields.Add("LOW_VALUE", m_LowValue)
            End If

            objWriter.Execute()

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property FactorValue() As Double
        Get
            FactorValue = m_FactorValue
        End Get
        Set(ByVal Value As Double)
            m_FactorValue = Value
        End Set
    End Property

    Public Property HighValue() As Double
        Get
            HighValue = m_HighValue
        End Get
        Set(ByVal Value As Double)
            m_HighValue = Value
        End Set
    End Property

    Public Property LowValue() As Double
        Get
            LowValue = m_LowValue
        End Get
        Set(ByVal Value As Double)
            m_LowValue = Value
        End Set
    End Property
End Class

