Option Strict Off
Option Explicit On
Public Class CJRCalcAWWAdditions
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRCalcAWWAdditions"

    Private mCol As Collection
    '---------------------------------------------------------------------------------------
    ' Procedure : DLLFetchData
    ' DateTime  : 7/15/2004 14:47
    ' Author    : jtodd22
    ' Purpose   : This is an internal Data Fetch of the DLL.
    '...........: There is no need to connect to the database.
    '---------------------------------------------------------------------------------------
    '
    Public Function DLLFetchData(ByRef lTableRowIDAWWParent As Integer) As Integer
        Dim sSQL As String
        Try

            DLLFetchData = 0
            ClearObject()
            sSQL = GetSQLTableRowIDAWWParent(lTableRowIDAWWParent)
            FetchDataCollection(sSQL)
            DLLFetchData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DLLFetchData = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".DLLFetchData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function SaveData(ByRef objuser As Riskmaster.Security.UserLogin) As Integer
        Dim i As Short

        SaveData = 0

        For i = 1 To mCol.Count()
            'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item().SaveData. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            mCol.Item(i).SaveData()
        Next i


    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 7/15/2004 13:52
    ' Author    : jtodd22
    ' Purpose   :
    ' Notes     : This LoadData function was called from inside the DLL and from the
    '...........: AWW Calculator in the EXE.  This presented an issue with database connections.
    '...........: The issue was solved by splitting the original function.
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lTableRowIDAWWParent As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0
            ClearObject()

            sSQL = GetSQLTableRowIDAWWParent(lTableRowIDAWWParent)
            LoadData = FetchDataCollection(sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function Add(ByRef FriendlyName As String, ByRef TableRowID As Integer, ByRef TableRowIDFriendlyName As Integer, ByRef TableRowIDParent As Integer, ByRef UseCode As Integer, ByRef UsedInCalc As Object, ByRef sKey As String) As CJRCalcAWWAddition
        'create a new object
        Try
            Dim objNewMember As CJRCalcAWWAddition
            objNewMember = New CJRCalcAWWAddition


            'set the properties passed into the method
            objNewMember.TableRowID = TableRowID
            objNewMember.UseCode = UseCode
            'UPGRADE_WARNING: Couldn't resolve default property of object UsedInCalc. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            objNewMember.UsedInCalc = UsedInCalc
            objNewMember.TableRowIDParent = TableRowIDParent
            objNewMember.TableRowIDFriendlyName = TableRowIDFriendlyName
            objNewMember.FriendlyName = FriendlyName
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRCalcAWWAddition
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function ClearObject() As Integer



    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadDataByJurisRowID
    ' DateTime  : 2/7/2005 15:02
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : The SQL is not straight foreward.
    ' ..........: WCP_FRIENDLY_NAME holds a row id and the name desc.  There are no links to
    ' ..........: anything.
    ' ..........: WCP_AWW_SWCH, this is the parent table.  It holds the jurisdictional link.
    ' ..........: WCP_AWW_X_SWCH, this is the child of WCP_AWW_SWCH.  It holds linking ids to
    ' ..........: to the other tables.
    ' Notes.....: The joins, starting with a jurisdictional id:
    ' ..........: WHERE WCP_AWW_X_SWCH.PT_TABLE_ROW_ID = WCP_AWW_SWCH.TABLE_ROW_ID"
    ' ..........: AND WCP_AWW_X_SWCH.FN_TABLE_ROW_ID = WCP_FRIENDLY_NAME.TABLE_ROW_ID"
    ' ..........: AND WCP_AWW_SWCH.JURIS_ROW_ID = " & lJurisRowID

    '---------------------------------------------------------------------------------------
    '
    Public Function LoadDataByEventDate(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim i As Short
        Dim objReader As DbReader
        Dim lTest As Integer
        Dim lUseCode As Integer
        Dim objRecord As CJRCalcAWWAddition
        Dim sSQL As String
        Dim sSQL2 As String

        Try

            LoadDataByEventDate = 0

            ClearObject()

            lUseCode = GetYesCodeID()

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM WCP_AWW_SWCH"
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " WCP_FRIENDLY_NAME.TABLE_ROW_ID"
            sSQL = sSQL & ", WCP_FRIENDLY_NAME.FRIENDLY_NAME"
            sSQL = sSQL & ", WCP_FRIENDLY_NAME.USED_IN_CALC"
            sSQL = sSQL & ", WCP_AWW_X_SWCH.FN_TABLE_ROW_ID"
            sSQL = sSQL & ", WCP_AWW_X_SWCH.PT_TABLE_ROW_ID"
            sSQL = sSQL & ", WCP_AWW_X_SWCH.USE_CODE"
            sSQL = sSQL & ", WCP_AWW_SWCH.EFFECTIVE_DATE"
            sSQL = sSQL & " FROM WCP_AWW_SWCH, WCP_AWW_X_SWCH, WCP_FRIENDLY_NAME"
            sSQL = sSQL & " WHERE WCP_AWW_X_SWCH.PT_TABLE_ROW_ID = WCP_AWW_SWCH.TABLE_ROW_ID"
            sSQL = sSQL & " AND WCP_AWW_X_SWCH.FN_TABLE_ROW_ID = WCP_FRIENDLY_NAME.TABLE_ROW_ID"
            sSQL = sSQL & " AND WCP_AWW_SWCH.JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND USE_CODE = " & lUseCode
            sSQL = sSQL & " AND WCP_AWW_SWCH.EFFECTIVE_DATE = (" & sSQL2 & ")"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRCalcAWWAddition
                With objRecord
                    .FriendlyName = objReader.GetString("FRIENDLY_NAME")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .TableRowIDFriendlyName = objReader.GetInt32("FN_TABLE_ROW_ID")
                    .TableRowIDParent = objReader.GetInt32("PT_TABLE_ROW_ID")
                    .UseCode = objReader.GetInt32("USE_CODE")
                    .UsedInCalc = objReader.GetInt32("USED_IN_CALC")
                    Add(.FriendlyName, .TableRowID, .TableRowIDFriendlyName, .TableRowIDParent, .UseCode, .UsedInCalc, "k" & .TableRowIDFriendlyName)
                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing

            End While

            LoadDataByEventDate = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Private Function GetSQLTableRowIDAWWParent(ByRef lTableRowIDAWWParent As Object) As String
        Dim sSQL As String

        GetSQLTableRowIDAWWParent = ""

        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " WCP_AWW_X_SWCH.TABLE_ROW_ID"
        sSQL = sSQL & ", FN_TABLE_ROW_ID"
        sSQL = sSQL & ", PT_TABLE_ROW_ID"
        sSQL = sSQL & ", USE_CODE"
        sSQL = sSQL & ", FRIENDLY_NAME"
        sSQL = sSQL & ", WCP_FRIENDLY_NAME.USED_IN_CALC"
        sSQL = sSQL & " FROM WCP_AWW_X_SWCH,WCP_FRIENDLY_NAME"
        'UPGRADE_WARNING: Couldn't resolve default property of object lTableRowIDAWWParent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sSQL = sSQL & " WHERE WCP_AWW_X_SWCH.PT_TABLE_ROW_ID = " & lTableRowIDAWWParent
        sSQL = sSQL & " AND WCP_AWW_X_SWCH.FN_TABLE_ROW_ID = WCP_FRIENDLY_NAME.TABLE_ROW_ID"

        GetSQLTableRowIDAWWParent = sSQL


    End Function
    Public Function FetchDataCollection(ByRef sSQL As String) As Integer
        Dim objReader As DbReader
        Dim lTest As Integer

        Try

            FetchDataCollection = 0

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                lTest = objReader.GetInt32("FN_TABLE_ROW_ID")
                Add(objReader.GetString("FRIENDLY_NAME"), objReader.GetInt32("TABLE_ROW_ID"), lTest, objReader.GetInt32("PT_TABLE_ROW_ID"), objReader.GetInt32("USE_CODE"), objReader.GetInt32("USED_IN_CALC"), "k" & lTest)

            End While


            FetchDataCollection = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            FetchDataCollection = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".FetchDataCollection|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
End Class

