Option Strict Off
Option Explicit On
Public Class CJRJurisFilings
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRJurisFiling"

    Private mCol As Collection
    Private Function GetFieldSQL() As String
        Dim sSQL As String
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " WCP_FILE_SCHEDULE.TABLE_ROW_ID" '1
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.DTTM_RCD_ADDED" '2
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.ADDED_BY_USER"
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.DTTM_RCD_LAST_UPD"
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.UPDATED_BY_USER"
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.EFFECTIVE_DATE"
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.JURIS_ROW_ID"
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.DELETED_FLAG"
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.RELEASE_ROW_ID"
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.FORM_ID"
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.DATE_OF_CODE"
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.TRIG_EVENT_CODE"
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.DAYS_TO_FILE"
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.SCHEDULE_TIME_CODE"
        sSQL = sSQL & ",WCP_FILE_SCHEDULE.NOTIFY_ADJUST_CODE" '15
        GetFieldSQL = sSQL


    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 11/1/2004 16:49
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      :  2= EDI, 3 = Paper
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef lFilingType As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim lNotifyAdjusterCode As Integer
        Dim lTableRowID As Integer
        Dim sFormName As String
        Dim sFormTitle As String
        Dim sReleaseDesc As String
        Dim sSQL As String
        Try

            LoadData = 0

            g_sErrDescription = ""


            sSQL = ""
            sSQL = sSQL & GetFieldSQL()
            Select Case lFilingType
                Case 2
                    sSQL = sSQL & ", RELEASE_DESC"
                    sSQL = sSQL & " FROM WCP_FILE_SCHEDULE, WCP_EDIJURIS_RELAS"
                    sSQL = sSQL & " WHERE WCP_FILE_SCHEDULE.JURIS_ROW_ID = " & lJurisRowID
                    sSQL = sSQL & " AND WCP_FILE_SCHEDULE.RELEASE_ROW_ID = WCP_EDIJURIS_RELAS.RELEASE_ROW_ID"
                    sSQL = sSQL & " AND WCP_FILE_SCHEDULE.RELEASE_ROW_ID > 0"
                Case 3
                    sSQL = sSQL & ", FORM_NAME, FORM_TITLE"
                    sSQL = sSQL & " FROM WCP_FILE_SCHEDULE, WCP_FORMS"
                    sSQL = sSQL & " WHERE WCP_FILE_SCHEDULE.JURIS_ROW_ID = " & lJurisRowID
                    sSQL = sSQL & " AND WCP_FILE_SCHEDULE.FORM_ID = WCP_FORMS.FORM_ID"
                    sSQL = sSQL & " AND WCP_FILE_SCHEDULE.FORM_ID > 0"
                Case Else
                    'bad news
            End Select
            sFormName = ""
            sFormTitle = ""
            sReleaseDesc = ""

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                lNotifyAdjusterCode = objReader.GetInt32("NOTIFY_ADJUST_CODE")
                lTableRowID = objReader.GetInt32("TABLE_ROW_ID")
                If Not IsNothing(objReader.Item("FORM_NAME")) Then
                    sFormName = objReader.GetString("FORM_NAME")
                    sFormTitle = objReader.GetString("FORM_TITLE")
                    If Not IsNothing(objReader.Item("RELEASE_DESC")) Then
                        sReleaseDesc = objReader.GetString("RELEASE_DESC")
                    End If

                End If
                Add(objReader.GetInt32("DATE_OF_CODE"), objReader.GetInt32("DAYS_TO_FILE"), objReader.GetInt32("DELETED_FLAG"), objReader.GetString("EFFECTIVE_DATE"), objReader.GetInt32("FORM_ID"), sFormName, sFormTitle, objReader.GetInt32("JURIS_ROW_ID"), objReader.GetInt32("NOTIFY_ADJUST_CODE"), objReader.GetInt32("RELEASE_ROW_ID"), sReleaseDesc, objReader.GetInt32("SCHEDULE_TIME_CODE"), lTableRowID, objReader.GetInt32("TRIG_EVENT_CODE"), "k" & CStr(lTableRowID))

            End While

            LoadData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                If g_sErrDescription = "" Then
                    g_sErrDescription = Err.Description
                End If
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function Add(ByRef FromDateOfCode As Integer, ByRef DaysToFile As Integer, ByRef DeletedFlag As Integer, ByRef EffectiveDateDTG As String, ByRef FormID As Integer, ByRef FormName As Object, ByRef FormTitle As Object, ByRef JurisRowID As Integer, ByRef NotifyAdjustCode As Integer, ByRef ReleaseRowID As Integer, ByRef ReleaseDesc As String, ByRef SchedTimeCode As Integer, ByRef TableRowID As Integer, ByRef TriggeringEventCode As Integer, ByRef sKey As String) As CJRJurisFiling
        Const sFunctionName As String = "Add"
        Dim objNewMember As CJRJurisFiling
        Try

            objNewMember = New CJRJurisFiling


            'set the properties passed into the method
            objNewMember.ReleaseRowID = ReleaseRowID
            objNewMember.ReleaseDesc = ReleaseDesc
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.NotifyAdjustCode = NotifyAdjustCode
            objNewMember.SchedTimeCode = SchedTimeCode
            objNewMember.DaysToFile = DaysToFile
            objNewMember.TriggeringEventCode = TriggeringEventCode
            objNewMember.FromDateOfCode = FromDateOfCode
            objNewMember.FormID = FormID
            'UPGRADE_WARNING: Couldn't resolve default property of object FormName. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            objNewMember.FormName = FormName
            'UPGRADE_WARNING: Couldn't resolve default property of object FormTitle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            objNewMember.FormTitle = FormTitle
            'jtodd22--objNewMember.DataHasChanged = DataHasChanged
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.JurisRowID = JurisRowID
            objNewMember.TableRowID = TableRowID
            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If


            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                If g_sErrDescription = "" Then
                    g_sErrDescription = Err.Description
                End If
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRJurisFiling
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function

    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

