Option Strict Off
Option Explicit On
Public Class CJRAbbreviation
    Const sClassName As String = "CJRAbbreviation"
    Const sTableName As String = "WCP_BENEFIT_ABBR"

    Private m_Abbreviation As String
    Private m_Description As String
    Private m_TableRowID As Integer

    Public Property Abbreviation() As String
        Get
            Abbreviation = m_Abbreviation
        End Get
        Set(ByVal Value As String)
            m_Abbreviation = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Description = m_Description
        End Get
        Set(ByVal Value As String)
            m_Description = Value
        End Set
    End Property
End Class

