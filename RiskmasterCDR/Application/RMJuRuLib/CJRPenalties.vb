Option Strict Off
Option Explicit On
Public Class CJRPenalties
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRPenalties"
    Const sTableName As String = "WCP_PENALTY_RULES"
    'Class properties--local copy
    Private mCol As Collection
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", EFFECTIVE_DATE,JURIS_ROW_ID"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", END_DATE"
        sSQL = sSQL & ", FIXED_AMT"
        sSQL = sSQL & ", TRIG_EVENT_CODE"
        sSQL = sSQL & ", PERCT_PAYMENT_AMT"
        sSQL = sSQL & ", PERCT_BENEFIT_AMT"
        sSQL = sSQL & ", SELFIMPOSED_CODE"
        sSQL = sSQL & ", ASSESSED_CODE"
        sSQL = sSQL & ", TRANS_TYPE_CODE"
        sSQL = sSQL & ", PAYEE_TYPE_CODE"
        sSQL = sSQL & ", REFERENCE_TXT"
        sSQL = sSQL & ", DAYS_FROM_TRIGGER"
        GetSQLFieldList = sSQL


    End Function

    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJRPenalty
        Try

            LoadData = 0

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM WCP_PENALTY_RULES"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                objRecord = New CJRPenalty
                With objRecord
                    .DaysFromTrigger = objReader.GetInt32("DAYS_FROM_TRIGGER")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
                    .EndDateDTG = Trim(objReader.GetString("END_DATE"))
                    .FixedAmount = objReader.GetDouble("FIXED_AMT")
                    .TriggerEventCode = objReader.GetInt32("TRIG_EVENT_CODE")
                    .PercentageOfPayment = objReader.GetDouble("PERCT_PAYMENT_AMT")
                    .PercentageOfTotalBenefit = objReader.GetDouble("PERCT_BENEFIT_AMT")
                    .SelfImposedCode = objReader.GetInt32("SELFIMPOSED_CODE")
                    .AssessedCode = objReader.GetInt32("ASSESSED_CODE")
                    .TranactionCode = objReader.GetInt32("TRANS_TYPE_CODE")
                    .PayeeCode = objReader.GetInt32("PAYEE_TYPE_CODE")
                    .ReferenceText = Trim(objReader.GetString("REFERENCE_TXT"))
                    .DataHasChanged = False
                    Add(.DaysFromTrigger, .Permitted, .EndDateDTG, .EffectiveDateDTG, .DeletedFlag, .JurisRowID, .TableRowID, .FixedAmount, .TriggerEventCode, .PercentageOfPayment, .PercentageOfTotalBenefit, .AssessedCode, .SelfImposedCode, .TranactionCode, .PayeeCode, .ReferenceText, "k" & .TableRowID)
                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing

            End While
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|CJRPenalties.LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Public Function Add(ByRef DaysFromTrigger As Integer, ByRef Permitted As Integer, ByRef EndDateDTG As String, ByRef EffectiveDateDTG As String, ByRef DeletedFlag As Short, ByRef JurisRowID As Short, ByRef TableRowID As Integer, ByRef FixedAmount As Double, ByRef TriggerEventCode As Integer, ByRef PercentageOfPayment As Double, ByRef PercentageOfTotalBenefit As Double, ByRef AssessedCode As Integer, ByRef SelfImposedCode As Integer, ByRef TranactionCode As Integer, ByRef PayeeCode As Integer, ByRef ReferenceText As String, Optional ByRef sKey As String = "") As CJRPenalty
        'create a new object
        Dim objNewMember As CJRPenalty
        objNewMember = New CJRPenalty


        'set the properties passed into the method
        objNewMember.DaysFromTrigger = DaysFromTrigger
        objNewMember.Permitted = Permitted
        objNewMember.EndDateDTG = EndDateDTG
        objNewMember.EffectiveDateDTG = EffectiveDateDTG
        objNewMember.DeletedFlag = DeletedFlag
        objNewMember.JurisRowID = JurisRowID
        objNewMember.TableRowID = TableRowID
        objNewMember.FixedAmount = FixedAmount
        objNewMember.TriggerEventCode = TriggerEventCode
        objNewMember.PercentageOfPayment = PercentageOfPayment
        objNewMember.PercentageOfTotalBenefit = PercentageOfTotalBenefit
        objNewMember.AssessedCode = AssessedCode
        objNewMember.SelfImposedCode = SelfImposedCode
        objNewMember.TranactionCode = TranactionCode
        objNewMember.PayeeCode = PayeeCode
        objNewMember.ReferenceText = ReferenceText
        If Len(sKey) = 0 Then
            mCol.Add(objNewMember)
        Else
            mCol.Add(objNewMember, sKey)
        End If
        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing


    End Function
    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRPenalty
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

