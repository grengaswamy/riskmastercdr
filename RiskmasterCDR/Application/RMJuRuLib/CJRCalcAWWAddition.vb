Option Strict Off
Option Explicit On
Public Class CJRCalcAWWAddition
    '---------------------------------------------------------------------------------------
    ' Module    : CJRCalcAWWAddition
    ' DateTime  : 7/1/2004 17:45
    ' Author    : jtodd22
    ' Purpose   : Defines properties and saves data.
    ' Notes     : Does not use a "LoadData" function.  The data is loaded by CJRCalcAWWAdditions in
    '...........: the "LoadCollection" function.
    '...........:
    ' Note......: This is a supporting class to CJRCalcAWW

    '---------------------------------------------------------------------------------------
    Private Const sClassName As String = "CJRCalcAWWAddition"
    Private Const sTableName As String = "WCP_AWW_X_SWCH"

    Private m_TableRowID As Integer 'local copy
    Private m_UseCode As Integer 'local copy
    Private m_UsedInCalc As Integer 'jtodd22 not a Jurisdictional Rule, used in the collection on AWW Calculator screen

    Private m_TableRowIDParent As Integer 'local copy
    Private m_TableRowIDFriendlyName As Integer 'local copy
    Private m_FriendlyName As String 'local copy
    Private m_DataHasChanged As Boolean

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property FriendlyName() As String
        Get
            FriendlyName = m_FriendlyName
        End Get
        Set(ByVal Value As String)
            m_FriendlyName = Value
        End Set
    End Property
    Public Property TableRowIDFriendlyName() As Integer
        Get
            TableRowIDFriendlyName = m_TableRowIDFriendlyName
        End Get
        Set(ByVal Value As Integer)
            m_TableRowIDFriendlyName = Value
        End Set
    End Property
    Public Property TableRowIDParent() As Integer
        Get
            TableRowIDParent = m_TableRowIDParent
        End Get
        Set(ByVal Value As Integer)
            m_TableRowIDParent = Value
        End Set
    End Property
    Public Property UseCode() As Integer
        Get
            UseCode = m_UseCode
        End Get
        Set(ByVal Value As Integer)
            m_UseCode = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property



    Public Property UsedInCalc() As Integer
        Get

            UsedInCalc = m_UsedInCalc

        End Get
        Set(ByVal Value As Integer)

            m_UsedInCalc = Value

        End Set
    End Property
    Public Function SaveData() As Integer
        'Public Function SaveData(objUser As RMUser) As Long
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try

            SaveData = 0

            '    If ModMain.DBConnect(objUser, sClassName) <> -1 Then
            '        Err.Raise 70001, sClassName & ".SaveData", "Failed to connect to database."
            '        Exit Function
            '    End If

            sSQL = GetBaseSQL()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("FN_TABLE_ROW_ID").Value = m_TableRowIDFriendlyName
                objWriter.Fields("PT_TABLE_ROW_ID").Value = m_TableRowIDParent
                objWriter.Fields("USE_CODE").Value = m_UseCode
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lTest = lGetNextUID(sTableName)
                objWriter.Fields.Add("TABLE_ROW_ID", lTest)
                objWriter.Fields.Add("FN_TABLE_ROW_ID", m_TableRowIDFriendlyName)
                objWriter.Fields.Add("PT_TABLE_ROW_ID", m_TableRowIDParent)
                objWriter.Fields.Add("USE_CODE", m_UseCode)
            End If
            SafeCloseRecordset(objReader)
            objWriter.Execute()
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = g_lErrNum
            SafeCloseRecordset(objReader)
            'ModMain.DBDisconnect
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            'ModMain.DBDisconnect
        End Try

    End Function
    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " FN_TABLE_ROW_ID"
        sSQL = sSQL & ", PT_TABLE_ROW_ID"
        sSQL = sSQL & ", TABLE_ROW_ID"
        sSQL = sSQL & ", USE_CODE"
        GetBaseSQL = sSQL


    End Function
End Class

