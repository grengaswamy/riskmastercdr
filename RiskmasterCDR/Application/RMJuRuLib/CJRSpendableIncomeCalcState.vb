Option Strict Off
Option Explicit On
Public Class CJRSpendableIncomeCalcState
    Const sClassName As String = "CJRSpendableIncomeCalcState"
    Const sTableName As String = "WCP_WITHHOL_JU"
    'Class properties--local copy
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_TableRowID As Integer
    Private m_JurisRowID As Short
    Private m_EffectiveDateDTG As String
    Private m_ExemptionValueAmount As Double
    Private m_TaxStatusCode As Integer
    Private m_JurisExemptionValueAmount As Double
    Private m_StateMaxAmount As Double
    Private m_StateMinAmount As Double
    Private m_StateMultipler As Double
    Private m_StateOffsetAmount As Double
    Private m_StateFlatPercentRate As Double
    Private m_Warning As String


    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()


            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	m_TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            '	m_JurisRowID = objReader.GetInt32( "JURIS_ROW_ID")
            '	m_EffectiveDateDTG = objReader.GetString( "EFFECTIVE_DATE")
            '	m_DeletedFlag = objReader.GetInt32( "DELETED_FLAG")
            '	m_TaxStatusCode = objReader.GetInt32( "JU_TAX_STTUS_CODE")
            '	m_ExemptionValueAmount = objReader.GetDouble( "JU_EXEMP_VALUE_AMT")
            '	m_StateMaxAmount = objReader.GetDouble( "STE_MAX_AMT")
            '	m_StateMinAmount = objReader.GetDouble( "STE_MIN_AMT")
            '	m_StateMultipler = objReader.GetDouble( "STE_MULTIPLER")
            '	m_StateOffsetAmount = objReader.GetDouble( "STE_OFFSET_AMT")
            '	m_StateFlatPercentRate = objReader.GetDouble( "STE_PERCENT_RATE")
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_TaxStatusCode = objReader.GetInt32("JU_TAX_STTUS_CODE")
                m_ExemptionValueAmount = objReader.GetDouble("JU_EXEMP_VALUE_AMT")
                m_StateMaxAmount = objReader.GetDouble("STE_MAX_AMT")
                m_StateMinAmount = objReader.GetDouble("STE_MIN_AMT")
                m_StateMultipler = objReader.GetDouble("STE_MULTIPLER")
                m_StateOffsetAmount = objReader.GetDouble("STE_OFFSET_AMT")
                m_StateFlatPercentRate = objReader.GetDouble("STE_PERCENT_RATE")
            End If


            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        m_TableRowID = 0
        m_JurisRowID = 0
        m_EffectiveDateDTG = vbNullString
        m_DeletedFlag = 0
        m_TaxStatusCode = 0
        m_ExemptionValueAmount = 0
        m_StateMaxAmount = 0
        m_StateMinAmount = 0
        m_StateMultipler = 0
        m_StateOffsetAmount = 0
        m_StateFlatPercentRate = 0



    End Function
    Private Function GetSQLFieldSelect() As String
        Dim sSQL As String
        GetSQLFieldSelect = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",DELETED_FLAG"
        sSQL = sSQL & ",JU_TAX_STTUS_CODE"
        sSQL = sSQL & ",JU_EXEMP_VALUE_AMT"
        sSQL = sSQL & ",STE_MIN_AMT"
        sSQL = sSQL & ",STE_MAX_AMT"
        sSQL = sSQL & ",STE_MULTIPLER"
        sSQL = sSQL & ",STE_OFFSET_AMT"
        sSQL = sSQL & ",STE_PERCENT_RATE"
        GetSQLFieldSelect = sSQL


    End Function
    Public Function LoadDataTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataTableRowID"
        Dim objReader As DbReader
        Dim sSQL As String
        Try
            LoadDataTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL = GetSQLFieldSelect()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByClaimData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String, ByRef lAWW As Integer, ByRef lTaxStatusCode As Integer) As Integer
        Const sFunctionName As String = "LoadDataByClaimData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sTmp As String
        Try
            LoadDataByClaimData = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sTmp = ""
            sTmp = sTmp & "(SELECT MAX(EFFECTIVE_DATE) FROM " & sTableName
            sTmp = sTmp & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sTmp = sTmp & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"
            sTmp = sTmp & " AND JU_TAX_STTUS_CODE = " & lTaxStatusCode
            sTmp = sTmp & " AND " & lAWW & " BETWEEN  STE_MAX_AMT AND STE_MIN_AMT)"

            sSQL = GetSQLFieldSelect()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"
            sSQL = sSQL & " AND JU_TAX_STTUS_CODE = " & lTaxStatusCode
            sSQL = sSQL & " AND " & lAWW & " BETWEEN STE_MAX_AMT AND STE_MIN_AMT"

            LoadDataByClaimData = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByClaimData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT *"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("DELETED_FLAG").Value = 0
                objWriter.Fields("JU_TAX_STTUS_CODE").Value = m_TaxStatusCode
                objWriter.Fields("JU_EXEMP_VALUE_AMT").Value = m_JurisExemptionValueAmount
                objWriter.Fields("STE_MAX_AMT").Value = m_StateMaxAmount
                objWriter.Fields("STE_MIN_AMT").Value = m_StateMinAmount
                objWriter.Fields("STE_MULTIPLER").Value = m_StateMultipler
                objWriter.Fields("STE_OFFSET_AMT").Value = m_StateOffsetAmount
                objWriter.Fields("STE_PERCENT_RATE").Value = m_StateFlatPercentRate
                objWriter.Execute()
            Else
                'new record
                objWriter = DbFactory.GetDbWriter(objReader, False)
                m_TableRowID = lGetNextUID(sTableName)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("DELETED_FLAG", 0)
                objWriter.Fields.Add("JU_TAX_STTUS_CODE", m_TaxStatusCode)
                objWriter.Fields.Add("JU_EXEMP_VALUE_AMT", m_JurisExemptionValueAmount)
                objWriter.Fields.Add("STE_MAX_AMT", m_StateMaxAmount)
                objWriter.Fields.Add("STE_MIN_AMT", m_StateMinAmount)
                objWriter.Fields.Add("STE_MULTIPLER", m_StateMultipler)
                objWriter.Fields.Add("STE_OFFSET_AMT", m_StateOffsetAmount)
                objWriter.Fields.Add("STE_PERCENT_RATE", m_StateFlatPercentRate)
                objWriter.Execute()
            End If


            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function ValidateData() As Integer
        ValidateData = 0
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_TaxStatusCode < 1 Then
            m_Warning = m_Warning & "A Tax Status is required." & vbCrLf
        End If



    End Function

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property JurisExemptionValueAmount() As Double
        Get
            JurisExemptionValueAmount = m_JurisExemptionValueAmount
        End Get
        Set(ByVal Value As Double)
            m_JurisExemptionValueAmount = Value
        End Set
    End Property

    Public Property StateMaxAmount() As Double
        Get
            StateMaxAmount = m_StateMaxAmount
        End Get
        Set(ByVal Value As Double)
            m_StateMaxAmount = Value
        End Set
    End Property

    Public Property StateMinAmount() As Double
        Get
            StateMinAmount = m_StateMinAmount
        End Get
        Set(ByVal Value As Double)
            m_StateMinAmount = Value
        End Set
    End Property

    Public Property StateOffsetAmount() As Double
        Get
            StateOffsetAmount = m_StateOffsetAmount
        End Get
        Set(ByVal Value As Double)
            m_StateOffsetAmount = Value
        End Set
    End Property

    Public Property StateFlatPercentRate() As Double
        Get
            StateFlatPercentRate = m_StateFlatPercentRate
        End Get
        Set(ByVal Value As Double)
            m_StateFlatPercentRate = Value
        End Set
    End Property
    Public Property StateMultipler() As Double
        Get
            StateMultipler = m_StateMultipler
        End Get
        Set(ByVal Value As Double)
            m_StateMultipler = Value
        End Set
    End Property
    Public Property ExemptionValueAmount() As Double
        Get
            ExemptionValueAmount = m_ExemptionValueAmount
        End Get
        Set(ByVal Value As Double)
            m_ExemptionValueAmount = Value
        End Set
    End Property
    Public Property TaxStatusCode() As Integer
        Get
            TaxStatusCode = m_TaxStatusCode
        End Get
        Set(ByVal Value As Integer)
            m_TaxStatusCode = Value
        End Set
    End Property



    Public Property Warning() As String
        Get

            Warning = m_Warning

        End Get
        Set(ByVal Value As String)

            m_Warning = Value

        End Set
    End Property
End Class

