Option Strict Off
Option Explicit On
Public Class CJRSpendableIncomeRule
    '---------------------------------------------------------------------------------------
    ' Module    : CJRSpendableIncomeRule
    ' DateTime  : 6/22/2004 10:41
    ' Author    : jlt
    ' Purpose   : Used for display of values in frmJurisdictionalRules (frmJurisdictionalRules.cmdImport)
    ' Purpose   : Used for validation in AWW Calcuator (as to number of Dependents/Exemptions
    '---------------------------------------------------------------------------------------
    Private Const sClassName As String = "CJRSpendableIncomeRule"
    Private Const sTableName As String = "WCP_SPENDABLE_RULE"
    'Class properties--local copy
    Private m_TableRowID As Integer 'local copy
    Private m_JurisRowID As Integer 'local copy
    Private m_EffectiveDateDTG As String
    Private m_DeletedFlag As Integer
    Private m_Warning As String
    Private m_Exemption00Supported As Integer 'local copy
    Private m_Exemption05Supported As Integer 'local copy
    Private m_Exemption09Supported As Integer 'local copy
    Private m_Exemption10Supported As Integer 'local copy
    Private m_Exemption11Supported As Integer 'local copy
    Private m_JurisdictionName As String
    Private m_MinimumMarriedExempts As Integer

    Private m_DataHasChanged As Boolean
    Private Function ClearObject() As Integer
        m_TableRowID = 0
        m_JurisRowID = 0
        m_EffectiveDateDTG = ""
        m_DeletedFlag = 0
        m_Exemption00Supported = 0
        m_Exemption05Supported = 0
        m_Exemption09Supported = 0
        m_Exemption10Supported = 0
        m_Exemption11Supported = 0
        m_JurisdictionName = ""
        m_MinimumMarriedExempts = 0
        m_DataHasChanged = False



    End Function
    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",EFFECTIVE_DATE"
        sSQL = sSQL & ",DELETED_FLAG"
        sSQL = sSQL & ",EXEMP_A_CODE"
        sSQL = sSQL & ",EXEMP_F_CODE"
        sSQL = sSQL & ",EXEMP_J_CODE"
        sSQL = sSQL & ",EXEMP_K_CODE"
        sSQL = sSQL & ",EXEMP_L_CODE"
        sSQL = sSQL & ",MIN_MARRIED_EXEMPS"
        sSQL = sSQL & " FROM WCP_SPENDABLE_RULE"
        GetBaseSQL = sSQL


    End Function
    Public Function GetTaxStatusTableID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As String
        Const sFunctionName As String = "GetTaxStatusTableID"
        Dim objReader As DbReader
        Dim sSQL As String
        Try
            GetTaxStatusTableID = ""



            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " SYSTEM_TABLE_NAME"
            sSQL = sSQL & " FROM GLOSSARY"
            sSQL = sSQL & " WHERE SYSTEM_TABLE_NAME LIKE '%TAX_STATS_%'"
            sSQL = sSQL & " AND SYSTEM_TABLE_NAME LIKE '%" & GetStatePostalCode_SQL(lJurisRowID) & "_CODE" & "'"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetTaxStatusTableID = objReader.GetString("SYSTEM_TABLE_NAME")
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function GetSpendableIncome(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String, ByRef dGrossAWW As Double, ByRef lTaxStatus As Integer, ByRef iDependents As Short) As Double
        Const sFunctionName As String = "GetSpendableIncome"
        Dim dTest As Double
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sExemptField As String
        Try
            GetSpendableIncome = 0


            Select Case iDependents
                Case 0
                    sExemptField = "EXEMP_A"
                Case 1
                    sExemptField = "EXEMP_B"
                Case 2
                    sExemptField = "EXEMP_C"
                Case 3
                    sExemptField = "EXEMP_D"
                Case 4
                    sExemptField = "EXEMP_E"
                Case 5
                    sExemptField = "EXEMP_F"
                Case 6
                    sExemptField = "EXEMP_G"
                Case 7
                    sExemptField = "EXEMP_H"
                Case 8
                    sExemptField = "EXEMP_I"
                Case 9
                    sExemptField = "EXEMP_J"
                Case 10
                    sExemptField = "EXEMP_K"
                Case 11
                    sExemptField = "EXEMP_L"
            End Select

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " " & sExemptField
            sSQL = sSQL & " FROM WCP_SPENDABLE"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND BASE_AMOUNT = " & System.Math.Round(dGrossAWW, 2)
            sSQL = sSQL & " AND TAX_STATUS_CODE = " & lTaxStatus
            sSQL = sSQL & " AND EFFECTIVE_DATE = "
            sSQL = sSQL & "(SELECT MAX(EFFECTIVE_DATE) FROM WCP_SPENDABLE"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "')"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then
                dTest = objReader.GetDouble(1)
            End If
            SafeCloseRecordset(objReader)

            If dTest = 0 Then 'jtodd22 Gross AWW is out of range
                sSQL = ""
                sSQL = sSQL & "SELECT"
                sSQL = sSQL & " " & sExemptField
                sSQL = sSQL & " FROM WCP_SPENDABLE"
                sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
                sSQL = sSQL & " AND BASE_AMOUNT = " & System.Math.Round(dGrossAWW, 2)
                sSQL = sSQL & " AND TAX_STATUS_CODE = " & lTaxStatus
                sSQL = sSQL & " AND EFFECTIVE_DATE = "
                sSQL = sSQL & "(SELECT MAX(EFFECTIVE_DATE) FROM WCP_SPENDABLE"
                sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
                sSQL = sSQL & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "')"

                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

                If (objReader.Read()) Then
                    dTest = objReader.GetDouble(1)
                End If
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim objReader As DbReader
        Dim sSQL As String
        Try
            LoadDataByTableRowID = 0
            ClearObject()

            sSQL = ""
            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            AssignData(sSQL)
            LoadDataByTableRowID = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function LoadDataByClaimData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByClaimData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sTmp As String
        Try

            LoadDataByClaimData = 0
            ClearObject()

            sTmp = ""
            sTmp = sTmp & "(SELECT MAX(EFFECTIVE_DATE) FROM WCP_SPENDABLE_RULE"
            sTmp = sTmp & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sTmp = sTmp & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "')"



            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = " & sTmp

            AssignData(sSQL)
            LoadDataByClaimData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByClaimData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = CStr(g_sErrSrc & g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|")
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            DBDisconnect()

        End Try

    End Function
    Public Function LoadDataByJurisdiction(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByJurisdiction"
        Dim objReader As DbReader
        Dim sSQL As String
        Try
            LoadDataByJurisdiction = 0
            ClearObject()


            sSQL = ""
            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID

            AssignData(sSQL)
            LoadDataByJurisdiction = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByJurisdiction = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try

            SaveData = 0

            sSQL = ""
            sSQL = sSQL & GetBaseSQL()
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss") 'ToDbDateTime

                objWriter.Fields("UPDATED_BY_USER").Value = g_objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("EXEMP_A_CODE").Value = m_Exemption00Supported
                objWriter.Fields("EXEMP_F_CODE").Value = m_Exemption05Supported
                objWriter.Fields("EXEMP_J_CODE").Value = m_Exemption09Supported
                objWriter.Fields("EXEMP_K_CODE").Value = m_Exemption10Supported
                objWriter.Fields("EXEMP_L_CODE").Value = m_Exemption11Supported
                objWriter.Fields("MIN_MARRIED_EXEMPS").Value = m_MinimumMarriedExempts

            Else
                'new record
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lTest = lGetNextUID("WCP_SPENDABLE_RULE")
                objWriter.Fields.Add("TABLE_ROW_ID", lTest)
                objWriter.Fields.Add("ADDED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("EXEMP_A_CODE", m_Exemption00Supported)
                objWriter.Fields.Add("EXEMP_F_CODE", m_Exemption05Supported)
                objWriter.Fields.Add("EXEMP_J_CODE", m_Exemption09Supported)
                objWriter.Fields.Add("EXEMP_K_CODE", m_Exemption10Supported)
                objWriter.Fields.Add("EXEMP_L_CODE", m_Exemption11Supported)
                objWriter.Fields.Add("MIN_MARRIED_EXEMPS", m_MinimumMarriedExempts)
            End If
            SafeCloseRecordset(objReader)
            objWriter.Execute()
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = CStr(g_sErrSrc & g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|")
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            DBDisconnect()

        End Try

    End Function
    Public Function ValidateData() As Integer
        ValidateData = 0
        m_Warning = ""
        If Not Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_JurisRowID < 1 Then
            m_Warning = m_Warning & "A Jurisdiction is required." & vbCrLf
        End If
        If m_Exemption00Supported < 1 Then
            m_Warning = m_Warning & "A selection is required for 'Does Jurisdiction Support Zero Dependents (Exemptions)?'." & vbCrLf
        End If
        If m_Exemption05Supported < 1 Then
            m_Warning = m_Warning & "A selection is required for 'Does Jurisdiction Support Five (or More) Dependents (Exemptions)?'." & vbCrLf
        End If
        If m_Exemption09Supported < 1 Then
            m_Warning = m_Warning & "A selection is required for 'Does Jurisdiction Support Nine (or More) Dependents (Exemptions)?'." & vbCrLf
        End If
        If m_Exemption10Supported < 1 Then
            m_Warning = m_Warning & "A selection is required for 'Does Jurisdiction Support Ten (or More) Dependents (Exemptions)?'." & vbCrLf
        End If
        If m_Exemption11Supported < 1 Then
            m_Warning = m_Warning & "A selection is required for 'Does Jurisdiction Support Eleven Dependents (Exemptions)?'." & vbCrLf
        End If



    End Function
    Public Property JurisdictionName() As String
        Get
            JurisdictionName = m_JurisdictionName
        End Get
        Set(ByVal Value As String)
            m_JurisdictionName = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property Exemption11Supported() As Integer
        Get
            Exemption11Supported = m_Exemption11Supported
        End Get
        Set(ByVal Value As Integer)
            m_Exemption11Supported = Value
        End Set
    End Property

    Public Property Exemption10Supported() As Integer
        Get
            Exemption10Supported = m_Exemption10Supported
        End Get
        Set(ByVal Value As Integer)
            m_Exemption10Supported = Value
        End Set
    End Property

    Public Property Exemption09Supported() As Integer
        Get
            Exemption09Supported = m_Exemption09Supported
        End Get
        Set(ByVal Value As Integer)
            m_Exemption09Supported = Value
        End Set
    End Property

    Public Property Exemption05Supported() As Integer
        Get
            Exemption05Supported = m_Exemption05Supported
        End Get
        Set(ByVal Value As Integer)
            m_Exemption05Supported = Value
        End Set
    End Property

    Public Property Exemption00Supported() As Integer
        Get
            Exemption00Supported = m_Exemption00Supported
        End Get
        Set(ByVal Value As Integer)
            m_Exemption00Supported = Value
        End Set
    End Property
    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property MinimumMarriedExempts() As Integer
        Get
            MinimumMarriedExempts = m_MinimumMarriedExempts
        End Get
        Set(ByVal Value As Integer)
            m_MinimumMarriedExempts = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property

    Private Function AssignData(ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_Exemption00Supported = objReader.GetInt32("EXEMP_A_CODE")
                m_Exemption05Supported = objReader.GetInt32("EXEMP_F_CODE")
                m_Exemption09Supported = objReader.GetInt32("EXEMP_J_CODE")
                m_Exemption10Supported = objReader.GetInt32("EXEMP_K_CODE")
                m_Exemption11Supported = objReader.GetInt32("EXEMP_L_CODE")
                m_MinimumMarriedExempts = objReader.GetInt32("MIN_MARRIED_EXEMPS")
                m_DataHasChanged = False
            End If
            AssignData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = CStr(g_sErrSrc & g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".AssignData|")
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function IsSpendableIncome(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Dim objReader As DbReader
        Dim lTest As Integer
        Dim sSQL As String
        Try

            IsSpendableIncome = 0

            sSQL = ""
            sSQL = sSQL & "SELECT * FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                IsSpendableIncome = -1
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            IsSpendableIncome = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = CStr(g_sErrSrc & g_sErrProcedure = g_sErrSrc & "|" & sClassName & "IsSpendableIncome|")
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
End Class

