Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleLifeTimeImpair
    Const sClassName As String = "CJRBenefitRuleLifeTimeImpair"
    Const sTableName As String = "WCP_RULE_LTI"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821
    Dim m_sDateOfEventDTG As String 'Input data, not Jurisdictional Rule
    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    Private m_AllwaysRequireMMIDate As Integer
    Private m_AnnualIncrease As Double
    Private m_MaxAmount As Double
    Private m_MaxSAWWPercent As Double
    Private m_MinAmount As Double
    Private m_MinSawwPercent As Double
    Private m_StandardPercent As Double
    Private m_MinCompRate As Double 'Calculated value, not Jurisdictional Rule

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_AllwaysRequireMMIDate = objReader.GetInt32("AL_RQ_MMIDATE_CODE")
                m_AnnualIncrease = objReader.GetDouble("ANNUAL_INCREASE")
                m_DataHasChanged = False
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_MaxAmount = objReader.GetDouble("MAX_AMOUNT")
                m_MaxSAWWPercent = objReader.GetDouble("MAX_SAWW_PERCENT")
                m_MinAmount = objReader.GetDouble("MIN_AMOUNT")
                m_MinSawwPercent = objReader.GetDouble("MIN_SAWW_PERCENT")
                m_StandardPercent = objReader.GetDouble("STANDARD_PERCENT")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
            End If
            SafeCloseRecordset(objReader)

            If m_sDateOfEventDTG > "" And Len(m_sDateOfEventDTG) = 8 Then
                GetMaxMinCompRate(objUser)
            End If

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_AnnualIncrease = 0
        m_MaxAmount = 0
        m_MaxSAWWPercent = 0
        m_MinAmount = 0
        m_MinSawwPercent = 0
        m_StandardPercent = 0
        m_MinCompRate = 0


    End Function
    Private Function GetMaxMinCompRate(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "GetMaxMinCompRate"
        Dim objReader As DbReader
        Dim lReturn As Integer
        Dim objSAWW As CJRSAWW
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            GetMaxMinCompRate = 0
            If m_MaxAmount > 0 Then m_MaxCompRateWeekly = m_MaxAmount
            If m_MinAmount > 0 Then m_MinCompRate = m_MinAmount
            If m_MaxSAWWPercent > 0 Or m_MinSawwPercent > 0 Then
                objSAWW = New CJRSAWW
                lReturn = objSAWW.LoadDataByEventDate(objUser, m_JurisRowID, m_EffectiveDateDTG)
                If m_MaxSAWWPercent > 0 Then
                    m_MaxCompRateWeekly = System.Math.Round((m_MaxSAWWPercent / 100) * objSAWW.SAWWAmount, 2)
                End If
                If m_MinSawwPercent > 0 Then
                    m_MinCompRate = System.Math.Round((m_MinSawwPercent / 100) * objSAWW.SAWWAmount, 2)
                End If
                'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objSAWW = Nothing
            End If
            GetMaxMinCompRate = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetMaxMinCompRate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", ANNUAL_INCREASE"
        sSQL = sSQL & ", MAX_AMOUNT"
        sSQL = sSQL & ", MAX_SAWW_PERCENT"
        sSQL = sSQL & ", MIN_AMOUNT"
        sSQL = sSQL & ", MIN_SAWW_PERCENT"
        sSQL = sSQL & ", STANDARD_PERCENT"
        sSQL = sSQL & ", AL_RQ_MMIDATE_CODE" 'jtodd22 03/08/2006
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadDataTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataTableRowID"
        Dim sSQL As String
        Try
            LoadDataTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function
            m_sDateOfEventDTG = ""

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try
            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function
            m_sDateOfEventDTG = sDateOfEventDTG

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT *"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("AL_RQ_MMIDATE_CODE").Value = m_AllwaysRequireMMIDate
                objWriter.Fields("ANNUAL_INCREASE").Value = m_AnnualIncrease
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("MAX_AMOUNT").Value = m_MaxAmount
                objWriter.Fields("MAX_SAWW_PERCENT").Value = m_MaxSAWWPercent
                objWriter.Fields("MIN_AMOUNT").Value = m_MinAmount
                objWriter.Fields("MIN_SAWW_PERCENT").Value = m_MinSawwPercent
                objWriter.Fields("STANDARD_PERCENT").Value = m_StandardPercent
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("AL_RQ_MMIDATE_CODE", m_AllwaysRequireMMIDate)
                objWriter.Fields.Add("ANNUAL_INCREASE", m_AnnualIncrease)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("MAX_AMOUNT", m_MaxAmount)
                objWriter.Fields.Add("MAX_SAWW_PERCENT", m_MaxSAWWPercent)
                objWriter.Fields.Add("MIN_AMOUNT", m_MinAmount)
                objWriter.Fields.Add("MIN_SAWW_PERCENT", m_MinSawwPercent)
                objWriter.Fields.Add("STANDARD_PERCENT", m_StandardPercent)
            End If


            objWriter.Execute()

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try


    End Function

    Public Function ValidateData() As Integer

        ValidateData = 0
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_StandardPercent = 0 Then
            m_Warning = m_Warning & "For 'Standard Percentage Of AWW', requires a value greater than 0 (zero)." & vbCrLf
        End If
        If m_MaxAmount > 0 And m_MaxSAWWPercent > 0 Then
            m_Warning = m_Warning & "For Maximum Rates, only one of the two choices can have a value greater than 0 (zero)." & vbCrLf
        End If
        If m_MinAmount > 0 And m_MinSawwPercent > 0 Then
            m_Warning = m_Warning & "For Minimum Rates, only one of the two choices can have a value greater than 0 (zero)." & vbCrLf
        End If



    End Function

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property AnnualIncrease() As Double
        Get
            AnnualIncrease = m_AnnualIncrease
        End Get
        Set(ByVal Value As Double)
            m_AnnualIncrease = Value
        End Set
    End Property

    Public Property MaxAmount() As Double
        Get
            MaxAmount = m_MaxAmount
        End Get
        Set(ByVal Value As Double)
            m_MaxAmount = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property


    Public Property MaxSawwPercent() As Double
        Get
            MaxSawwPercent = m_MaxSAWWPercent
        End Get
        Set(ByVal Value As Double)
            m_MaxSAWWPercent = Value
        End Set
    End Property

    Public Property MinAmount() As Double
        Get
            MinAmount = m_MinAmount
        End Get
        Set(ByVal Value As Double)
            m_MinAmount = Value
        End Set
    End Property

    Public Property MinCompRate() As Double
        Get
            MinCompRate = m_MinCompRate
        End Get
        Set(ByVal Value As Double)
            m_MinCompRate = Value
        End Set
    End Property


    Public Property MinSawwPercent() As Double
        Get
            MinSawwPercent = m_MinSawwPercent
        End Get
        Set(ByVal Value As Double)
            m_MinSawwPercent = Value
        End Set
    End Property

    Public Property StandardPercent() As Double
        Get
            StandardPercent = m_StandardPercent
        End Get
        Set(ByVal Value As Double)
            m_StandardPercent = Value
        End Set
    End Property


    Public Property AllwaysRequireMMIDate() As Integer
        Get
            AllwaysRequireMMIDate = m_AllwaysRequireMMIDate
        End Get
        Set(ByVal Value As Integer)
            m_AllwaysRequireMMIDate = Value
        End Set
    End Property


    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
End Class

