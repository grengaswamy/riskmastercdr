Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CJREDIStdConditional

    '---------------------------------------------------------------------------------------
    ' Module    : CJREDIStdConditional
    ' DateTime  : 07/22/2005
    ' Author    : Venkata Mandayam
    ' Purpose   : This class is used to define the Rules associated with Jurisdictional EDI
    '             edits, store them, retrieve them and apply to the EDI data
    '---------------------------------------------------------------------------------------
    Const sClassName As String = "CJREDIStdConditional"
    Const sTableName2 As String = "EDI_STDCONDITIONS"
    Const sTableName3 As String = "WCP_STDCONDITIONS"


    'local variable(s) to hold property value(s)
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_RecdIfRowID As Integer
    Private m_TableRowID As Integer
    Private m_ConditionsXML As String

    Private Function ClearObject() As Integer
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_RecdIfRowID = 0
        m_TableRowID = 0
        m_ConditionsXML = ""


    End Function

    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lRecdIfRowID As Integer, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try


            LoadData = 0
            ClearObject()
            m_RecdIfRowID = lRecdIfRowID



            Dim sTableName As String
            If lJurisRowID > 0 Then
                sTableName = sTableName3
            Else
                sTableName = sTableName2
            End If

            sSQL = "SELECT * FROM " & sTableName

            sSQL = sSQL & " WHERE RECDIF_ROW_ID = " & lRecdIfRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_DataHasChanged = False
                'm_RecdIfRowID = Trim(objReader.GetString( "RECDIF_ROW_ID"))
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_ConditionsXML = Trim(objReader.GetString("CONDITIONS_XML"))

            End If
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0

            Dim sTableName As String
            If lJurisRowID > 0 Then
                sTableName = sTableName3
            Else
                sTableName = sTableName2
            End If

            sSQL = "SELECT * FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("RECDIF_ROW_ID").Value = m_RecdIfRowID
                objWriter.Fields("CONDITIONS_XML").Value = m_ConditionsXML

                SafeCloseRecordset(objReader)
            Else
                'new record
                SafeCloseRecordset(objReader)
                m_TableRowID = lGetNextUID(sTableName)

                objWriter = DbFactory.GetDbWriter(objReader, False)

                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("RECDIF_ROW_ID", m_RecdIfRowID)
                objWriter.Fields.Add("CONDITIONS_XML", m_ConditionsXML)
                objWriter.Execute()

            End If
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function


    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property


    Public Property RecdIfRowID() As Integer
        Get
            RecdIfRowID = m_RecdIfRowID
        End Get
        Set(ByVal Value As Integer)
            m_RecdIfRowID = Value
        End Set
    End Property


    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property


    Public Property ConditionsXML() As String
        Get
            ConditionsXML = m_ConditionsXML
        End Get
        Set(ByVal Value As String)
            m_ConditionsXML = Value
        End Set
    End Property
End Class

