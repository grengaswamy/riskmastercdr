Option Strict Off
Option Explicit On
Public Class CJRBenefitRulePTD
    Private Const sClassName As String = "CJRBenefitRulePTD"
    Private Const sTableName As String = "WCP_PTD_RULE"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821
    Dim m_sDateOfEventDTG As String
    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    Private m_UseTTDRateCode As Integer
    Private m_MaxAmount As Double
    Private m_FixedMax As Double
    Private m_FixedMin As Double
    Private m_NonTTDRate As Double
    Private m_MaxPercentOfSAWW As Double
    Private m_MinPercentOfSAWW As Double
    Private m_MinCompRate As Double 'not juris rule
    Private m_PayForLengthDisability As Integer
    Private m_PayForLife As Integer
    Private m_MaxWeeks As Integer
    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim lReturn As Integer
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_UseTTDRateCode = objReader.GetInt32("USE_TTDRULE_CODE")
                m_MaxAmount = objReader.GetDouble("MAX_AMT")
                m_FixedMax = objReader.GetDouble("FIX_MAX")
                m_FixedMin = objReader.GetDouble("FIX_MIN")
                m_NonTTDRate = objReader.GetDouble("NON_TTD_RATE")
                m_MaxPercentOfSAWW = objReader.GetDouble("MAX_PER_SAWW")
                m_MinPercentOfSAWW = objReader.GetDouble("MIN_PER_SAWW")
                m_PayForLengthDisability = objReader.GetInt32("LEN_DISABILITY")
                m_PayForLife = objReader.GetInt32("LIFE")
                m_MaxWeeks = objReader.GetInt32("MAX_WEEKS")
            End If

            SafeCloseRecordset(objReader)

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID



            Dim objSAWW As CJRSAWW
            If m_UseTTDRateCode <> m_YesCodeID Then
                If m_MaxPercentOfSAWW + m_MinPercentOfSAWW > 0 Then
                    objSAWW = New CJRSAWW
                    lReturn = objSAWW.LoadDataByEventDate(objUser, m_JurisRowID, m_sDateOfEventDTG)
                    If lReturn = -1 Then
                        With objSAWW
                            m_MaxCompRateWeekly = .SAWWAmount * m_MaxPercentOfSAWW
                            m_MinCompRate = .SAWWAmount * m_MinPercentOfSAWW
                        End With
                    End If
                    'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    objSAWW = Nothing
                Else
                    m_MaxCompRateWeekly = m_FixedMax
                    m_MinCompRate = m_FixedMin
                End If
            End If

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_UseTTDRateCode = 0
        m_MaxAmount = 0
        m_FixedMax = 0
        m_FixedMin = 0
        m_NonTTDRate = 0
        m_MaxPercentOfSAWW = 0
        m_MinPercentOfSAWW = 0
        m_PayForLengthDisability = 0
        m_PayForLife = 0
        m_MaxWeeks = 0

        m_MinCompRate = 0



    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE,JURIS_ROW_ID,DELETED_FLAG"
        sSQL = sSQL & ",USE_TTDRULE_CODE"
        sSQL = sSQL & ",NON_TTD_RATE"
        sSQL = sSQL & ",MAX_AMT, FIX_MAX, FIX_MIN"
        sSQL = sSQL & ",MAX_PER_SAWW, MIN_PER_SAWW"
        sSQL = sSQL & ", LEN_DISABILITY, LIFE, MAX_WEEKS"

        GetSQLFieldList = sSQL


    End Function
    Public Function LoadDataTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataTableRowID"
        Dim sSQL As String
        Try
            LoadDataTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM WCP_PTD_RULE"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            m_sDateOfEventDTG = sDateOfEventDTG

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String

        Try
            SaveData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT * "
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("USE_TTDRULE_CODE").Value = m_UseTTDRateCode
                objWriter.Fields("NON_TTD_RATE").Value = m_NonTTDRate
                objWriter.Fields("MAX_AMT").Value = m_MaxAmount
                objWriter.Fields("FIX_MAX").Value = m_FixedMax
                objWriter.Fields("FIX_MIN").Value = m_FixedMin
                objWriter.Fields("MAX_PER_SAWW").Value = m_MaxPercentOfSAWW
                objWriter.Fields("MIN_PER_SAWW").Value = m_MinPercentOfSAWW
                objWriter.Fields("LEN_DISABILITY").Value = m_PayForLengthDisability
                objWriter.Fields("LIFE").Value = m_PayForLife
                objWriter.Fields("MAX_WEEKS").Value = m_MaxWeeks
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("USE_TTDRULE_CODE", m_UseTTDRateCode)
                objWriter.Fields.Add("NON_TTD_RATE", m_NonTTDRate)
                objWriter.Fields.Add("MAX_AMT", m_MaxAmount)
                objWriter.Fields.Add("FIX_MAX", m_FixedMax)
                objWriter.Fields.Add("FIX_MIN", m_FixedMin)
                objWriter.Fields.Add("MAX_PER_SAWW", m_MaxPercentOfSAWW)
                objWriter.Fields.Add("MIN_PER_SAWW", m_MinPercentOfSAWW)
                objWriter.Fields.Add("LEN_DISABILITY", m_PayForLengthDisability)
                objWriter.Fields.Add("LIFE", m_PayForLife)
                objWriter.Fields.Add("MAX_WEEKS", m_MaxWeeks)
            End If
            objWriter.Execute()
            m_DataHasChanged = False

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try


    End Function

    Public Function ValidateData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "ValidateData"
        Dim sTmp As String
        Dim sTmp2 As String
        Try
            ValidateData = 0

            m_Warning = ""
            If Len(m_EffectiveDateDTG) <> 8 Then
                m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
            End If
            If m_UseTTDRateCode < 1 Then
                m_Warning = m_Warning & "'Use TTD Rates' requires a 'Yes' or 'No'." & vbCrLf
            End If

            If m_NoCodeID < 1 Then

                m_NoCodeID = modFunctions.GetNoCodeID

            End If
            If m_UseTTDRateCode = m_NoCodeID Then
                If m_FixedMax + m_MaxPercentOfSAWW = 0 Then
                    m_Warning = m_Warning & "With 'Use TTD Rates' set to 'No'" & vbCrLf
                    m_Warning = m_Warning & "a value is required for Maximum." & vbCrLf
                End If
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            ValidateData = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".ValidateData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property UseTTDRateCode() As Integer
        Get
            UseTTDRateCode = m_UseTTDRateCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTTDRateCode = Value
        End Set
    End Property

    Public Property MaxAmount() As Double
        Get
            MaxAmount = m_MaxAmount
        End Get
        Set(ByVal Value As Double)
            m_MaxAmount = Value
        End Set
    End Property

    Public Property FixedMax() As Double
        Get
            FixedMax = m_FixedMax
        End Get
        Set(ByVal Value As Double)
            m_FixedMax = Value
        End Set
    End Property

    Public Property FixedMin() As Double
        Get
            FixedMin = m_FixedMin
        End Get
        Set(ByVal Value As Double)
            m_FixedMin = Value
        End Set
    End Property

    Public Property NonTTDRate() As Double
        Get
            NonTTDRate = m_NonTTDRate
        End Get
        Set(ByVal Value As Double)
            m_NonTTDRate = Value
        End Set
    End Property

    Public Property MaxPercentOfSAWW() As Double
        Get
            MaxPercentOfSAWW = m_MaxPercentOfSAWW
        End Get
        Set(ByVal Value As Double)
            m_MaxPercentOfSAWW = Value
        End Set
    End Property

    Public Property MinPercentOfSAWW() As Double
        Get
            MinPercentOfSAWW = m_MinPercentOfSAWW
        End Get
        Set(ByVal Value As Double)
            m_MinPercentOfSAWW = Value
        End Set
    End Property

    Public Property PayForLengthDisability() As Integer
        Get
            PayForLengthDisability = m_PayForLengthDisability
        End Get
        Set(ByVal Value As Integer)
            m_PayForLengthDisability = Value
        End Set
    End Property

    Public Property PayForLife() As Integer
        Get
            PayForLife = m_PayForLife
        End Get
        Set(ByVal Value As Integer)
            m_PayForLife = Value
        End Set
    End Property

    Public Property MaxWeeks() As Integer
        Get
            MaxWeeks = m_MaxWeeks
        End Get
        Set(ByVal Value As Integer)
            m_MaxWeeks = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property

    Public Property MinCompRate() As Integer
        Get
            MinCompRate = m_MinCompRate
        End Get
        Set(ByVal Value As Integer)
            m_MinCompRate = Value
        End Set
    End Property

    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
End Class

