Option Strict Off
Option Explicit On
Public Class CJRCalcBenefitOffset
    Private Const sClassName As String = "CJRCalcBenefitOffset"

    Private m_TableRowID As Integer 'local copy
    Private m_UseCode As Integer 'local copy
    Private m_TableRowIDParent As Integer 'local copy
    Private m_TableRowIDFriendlyName As Integer 'local copy
    Private m_FriendlyName As String 'local copy
    Private m_DataHasChanged As Boolean

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property FriendlyName() As String
        Get
            FriendlyName = m_FriendlyName
        End Get
        Set(ByVal Value As String)
            m_FriendlyName = Value
        End Set
    End Property
    Public Property TableRowIDFriendlyName() As Integer
        Get
            TableRowIDFriendlyName = m_TableRowIDFriendlyName
        End Get
        Set(ByVal Value As Integer)
            m_TableRowIDFriendlyName = Value
        End Set
    End Property
    Public Property TableRowIDParent() As Integer
        Get
            TableRowIDParent = m_TableRowIDParent
        End Get
        Set(ByVal Value As Integer)
            m_TableRowIDParent = Value
        End Set
    End Property
    Public Property UseCode() As Integer
        Get
            UseCode = m_UseCode
        End Get
        Set(ByVal Value As Integer)
            m_UseCode = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property
    '---------------------------------------------------------------------------------------
    ' Procedure : SaveData
    ' DateTime  : 8/17/2004 16:50
    ' Author    : jtodd22
    ' Purpose   : To save data
    ' Note......: This is called from a function that has all the Rocket connections in place
    '---------------------------------------------------------------------------------------
    '
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try

            SaveData = 0

            '    If ModMain.DBConnect(objUser, sClassName) <> -1 Then
            '        Err.Raise 70001, sClassName & ".SaveData", "Failed to connect to database."
            '        Exit Function
            '    End If

            sSQL = GetBaseSQL()
            sSQL = sSQL & " FROM WCP_BEN_X_SWCH"
            sSQL = sSQL & " WHERE WCP_BEN_X_SWCH.FN_TABLE_ROW_ID = " & m_TableRowIDFriendlyName
            sSQL = sSQL & " AND WCP_BEN_X_SWCH.PT_TABLE_ROW_ID = " & m_TableRowIDParent
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("FN_TABLE_ROW_ID").Value = m_TableRowIDFriendlyName
                objWriter.Fields("PT_TABLE_ROW_ID").Value = m_TableRowIDParent
                objWriter.Fields("USE_CODE").Value = m_UseCode
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lTest = lGetNextUID("WCP_BEN_X_SWCH")
                objWriter.Fields.Add("TABLE_ROW_ID", lTest)
                objWriter.Fields.Add("FN_TABLE_ROW_ID", m_TableRowIDFriendlyName)
                objWriter.Fields.Add("PT_TABLE_ROW_ID", m_TableRowIDParent)
                objWriter.Fields.Add("USE_CODE", m_UseCode)

            End If
            objWriter.Execute()
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)
            'ModMain.DBDisconnect
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            'ModMain.DBDisconnect
        End Try

    End Function
    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " FN_TABLE_ROW_ID"
        sSQL = sSQL & ", PT_TABLE_ROW_ID"
        sSQL = sSQL & ", TABLE_ROW_ID"
        sSQL = sSQL & ", USE_CODE"
        GetBaseSQL = sSQL


    End Function
End Class

