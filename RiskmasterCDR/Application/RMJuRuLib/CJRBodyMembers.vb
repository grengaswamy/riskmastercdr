Option Strict Off
Option Explicit On
Public Class CJRBodyMembers
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRBodyMembers"
    Const sTableName As String = "WCP_DATA_BODY_MEMB"
    'Class properties, local copy
    Private mCol As Collection
    Private Function AssignData(ByRef objReader As DbReader) As Integer
        Try
            Dim objBodyPart As CJRBodyMember


            AssignData = 0
            While objReader.Read()
                objBodyPart = New CJRBodyMember
                With objBodyPart
                    .BodyMemberDesc = Trim(objReader.GetString("BODY_MEMB_DESC"))
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .Degrees = objReader.GetInt32("DEGREES_NUMB")
                    .EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
                    .LiabilityAmount = 0 'jtodd22 this is a calculated field
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .MaxAmount = objReader.GetDouble("MAX_AMOUNT")
                    .MaxMonths = objReader.GetDouble("MAX_MONTHS")
                    .MaxWeeks = objReader.GetDouble("MAX_WEEKS")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .AmputationPercentRate = objReader.GetInt32("AMPUTAT_RATE")
                    If Not IsNothing(objReader.Item("IMPAIRMENT_PCNT")) Then
                        .ImpairmentPercentage = objReader.GetDouble("IMPAIRMENT_PCNT")
                    Else
                        .ImpairmentPercentage = 0
                    End If
                    .PayRate = 0
                    Add(.BodyMemberDesc, .MaxMonths, .MaxWeeks, .MaxAmount, .DeletedFlag, .Degrees, .JurisRowID, .EffectiveDateDTG, .TableRowID, .AmputationPercentRate, .ImpairmentPercentage, .LiabilityAmount, .PayRate)
                End With
                'UPGRADE_NOTE: Object objBodyPart may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objBodyPart = Nothing

            End While
            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".AssignData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE,JURIS_ROW_ID,DELETED_FLAG"
        sSQL = sSQL & ",BODY_MEMB_DESC, MAX_AMOUNT"
        sSQL = sSQL & ",MAX_MONTHS, MAX_WEEKS"
        sSQL = sSQL & ",AMPUTAT_RATE"
        sSQL = sSQL & ",DEGREES_NUMB"
        sSQL = sSQL & " FROM " & sTableName
        GetSQLFieldList = sSQL


    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : Add
    ' DateTime  : 1/29/2005 11:40
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : The Add function must be Public as it is used in the Indemnity Calculator
    '---------------------------------------------------------------------------------------
    '
    Public Function Add(ByRef BodyMemberDesc As String, ByRef MaxMonths As Double, ByRef MaxWeeks As Double, ByRef MaxAmount As Decimal, ByRef DeletedFlag As Short, ByRef Degrees As Integer, ByRef JurisRowID As Short, ByRef EffectiveDateDTG As String, ByRef TableRowID As Integer, ByRef AmputationPercentRate As Integer, ByRef ImpairmentPercentage As Double, ByRef LiabilityAmount As Double, ByRef PayRate As Double, Optional ByRef sKey As String = "") As CJRBodyMember
        'create a new object
        Dim objNewMember As CJRBodyMember
        Try
            objNewMember = New CJRBodyMember
            'set the properties passed into the method
            objNewMember.BodyMemberDesc = BodyMemberDesc
            objNewMember.MaxMonths = MaxMonths
            objNewMember.MaxWeeks = MaxWeeks
            objNewMember.MaxAmount = MaxAmount
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.Degrees = Degrees
            objNewMember.JurisRowID = JurisRowID
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.TableRowID = TableRowID
            objNewMember.AmputationPercentRate = AmputationPercentRate
            objNewMember.ImpairmentPercentage = ImpairmentPercentage
            objNewMember.LiabilityAmount = LiabilityAmount
            objNewMember.PayRate = PayRate
            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If


            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
        End Try

    End Function
    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBodyMember
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 2/9/2005 15:39
    ' Author    : jtodd22
    ' Purpose   : To get all data for a jurisdictions.
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByVal lJurisID As Integer) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0
            sSQL = GetSQLFieldList()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisID
            sSQL = sSQL & " ORDER BY EFFECTIVE_DATE, BODY_MEMB_DESC"


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                AssignData(objReader)
            End If
            SafeCloseRecordset(objReader)
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function LoadDataByEventDate(ByRef objuser As Riskmaster.Security.UserLogin, ByVal lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0
            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"
            sSQL = sSQL & " ORDER BY BODY_MEMB_DESC"


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                AssignData(objReader)
            End If
            SafeCloseRecordset(objReader)
            LoadDataByEventDate = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadDataByPIEmployee
    ' DateTime  : 1/11/2005 10:01
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : Database tables involved are PERSON_INVOLVED, PI_X_BODY_MEMBER, WCP_DATA_BODY_MEMB
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadDataByPIEmployee(ByRef objuser As Riskmaster.Security.UserLogin, ByVal lJurisID As Integer, ByRef sDateOfEventDTG As String, ByRef lEventID As Integer, ByRef lClaimantID As Integer) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadDataByPIEmployee = 0
            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " PI_X_BODY_MEMBER.IMPAIRMENT_PCNT"
            sSQL = sSQL & ",WCP_DATA_BODY_MEMB.TABLE_ROW_ID"
            sSQL = sSQL & ",JURIS_ROW_ID"
            sSQL = sSQL & ",EFFECTIVE_DATE, DELETED_FLAG, BODY_MEMB_DESC, MAX_AMOUNT, MAX_MONTHS, MAX_WEEKS, AMPUTAT_RATE"

            sSQL = sSQL & " FROM PERSON_INVOLVED, PI_X_BODY_MEMBER, WCP_DATA_BODY_MEMB"
            sSQL = sSQL & " WHERE PERSON_INVOLVED.PI_ROW_ID = PI_X_BODY_MEMBER.PI_ROW_ID"
            sSQL = sSQL & " AND PI_X_BODY_MEMBER.WCP_BODY_MEMBER_ID = WCP_DATA_BODY_MEMB.TABLE_ROW_ID"

            sSQL = sSQL & " AND PERSON_INVOLVED.EVENT_ID = " & lEventID
            sSQL = sSQL & " AND PERSON_INVOLVED.PI_EID = " & lClaimantID

            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisID
            sSQL = sSQL & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"
            sSQL = sSQL & " ORDER BY BODY_MEMB_DESC"


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                AssignData(objReader)
            End If
            SafeCloseRecordset(objReader)
            LoadDataByPIEmployee = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByPIEmployee = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataByPIEmployee|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
End Class

