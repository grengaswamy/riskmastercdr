Option Strict Off
Option Explicit On
Public Class CJRPresentValueLP
    Private Const sClassName As String = "CJRPresentValueLP"
    Private Const sTableName As String = "WCP_PRSNT_VALUE_LP"
    'local variable(s) to hold property value(s)
    Private m_DeletedFlag As Boolean

    Private m_SexCode As Integer 'local copy
    Private m_PresentAge As Integer 'local copy
    Private m_Year10 As Double 'local copy
    Private m_Year00 As Double 'local copy
    Private m_Year01 As Double 'local copy
    Private m_Year02 As Double 'local copy
    Private m_Year03 As Double 'local copy
    Private m_Year04 As Double 'local copy
    Private m_Year05 As Double 'local copy
    Private m_Year06 As Double 'local copy
    Private m_Year07 As Double 'local copy
    Private m_Year08 As Double 'local copy
    Private m_Year09 As Double 'local copy
    Private m_Year11 As Double 'local copy
    Private m_Year12 As Double 'local copy
    Private m_Year13 As Double 'local copy
    Private m_Year14 As Double 'local copy
    Private m_JurisRowID As Integer 'local copy
    Private m_TableRowID As Integer 'local copy
    Private m_EffectiveDateDTG As String
    Private m_EndDateDTG As String
    Private m_DataHasChanged As Boolean
    Private m_Warning As String


    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", END_DATE"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", JURIS_ROW_ID,SEX_CODE_ID,AGE"
        sSQL = sSQL & ", YRS_0"
        sSQL = sSQL & ", YRS_1,YRS_2,YRS_3,YRS_4,YRS_5"
        sSQL = sSQL & ", YRS_6,YRS_7,YRS_8,YRS_9,YRS_10"
        sSQL = sSQL & ", YRS_11,YRS_12,YRS_13,YRS_14"
        sSQL = sSQL & " FROM " & sTableName
        GetBaseSQL = sSQL


    End Function
    Public Function DeleteData() As Integer



    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0



            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
                m_EndDateDTG = Trim(objReader.GetString("END_DATE"))
                m_SexCode = objReader.GetInt32("SEX_CODE_ID")
                m_PresentAge = objReader.GetInt32("AGE")
                m_Year10 = objReader.GetDouble("YRS_10")
                m_Year00 = objReader.GetDouble("YRS_0")
                m_Year01 = objReader.GetDouble("YRS_1")
                m_Year02 = objReader.GetDouble("YRS_2")
                m_Year03 = objReader.GetDouble("YRS_3")
                m_Year04 = objReader.GetDouble("YRS_4")
                m_Year05 = objReader.GetDouble("YRS_5")
                m_Year06 = objReader.GetDouble("YRS_6")
                m_Year07 = objReader.GetDouble("YRS_7")
                m_Year08 = objReader.GetDouble("YRS_8")
                m_Year09 = objReader.GetDouble("YRS_9")
                m_Year11 = objReader.GetDouble("YRS_11")
                m_Year12 = objReader.GetDouble("YRS_12")
                m_Year13 = objReader.GetDouble("YRS_13")
                m_Year14 = objReader.GetDouble("YRS_14")
            End If

            LoadData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try
            SaveData = 0



            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = g_objUser.LoginName
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("END_DATE").Value = m_EndDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("SEX_CODE_ID").Value = m_SexCode
                objWriter.Fields("AGE").Value = m_PresentAge
                objWriter.Fields("YRS_0").Value = m_Year00
                objWriter.Fields("YRS_1").Value = m_Year01
                objWriter.Fields("YRS_2").Value = m_Year02
                objWriter.Fields("YRS_3").Value = m_Year03
                objWriter.Fields("YRS_4").Value = m_Year04
                objWriter.Fields("YRS_5").Value = m_Year05
                objWriter.Fields("YRS_6").Value = m_Year06
                objWriter.Fields("YRS_7").Value = m_Year07
                objWriter.Fields("YRS_8").Value = m_Year08
                objWriter.Fields("YRS_9").Value = m_Year09
                objWriter.Fields("YRS_10").Value = m_Year10
                objWriter.Fields("YRS_11").Value = m_Year11
                objWriter.Fields("YRS_12").Value = m_Year12
                objWriter.Fields("YRS_13").Value = m_Year13
                objWriter.Fields("YRS_14").Value = m_Year14
                objWriter.Execute()

                SafeCloseRecordset(objReader)
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lTest = lGetNextUID(sTableName)
                objWriter.Fields.Add("TABLE_ROW_ID", lTest)
                objWriter.Fields.Add("ADDED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))

                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("END_DATE", m_EndDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("SEX_CODE_ID", m_SexCode)
                objWriter.Fields.Add("AGE", m_PresentAge)
                objWriter.Fields.Add("YRS_0", m_Year00)
                objWriter.Fields.Add("YRS_1", m_Year01)
                objWriter.Fields.Add("YRS_2", m_Year02)
                objWriter.Fields.Add("YRS_3", m_Year03)
                objWriter.Fields.Add("YRS_4", m_Year04)
                objWriter.Fields.Add("YRS_5", m_Year05)
                objWriter.Fields.Add("YRS_6", m_Year06)
                objWriter.Fields.Add("YRS_7", m_Year07)
                objWriter.Fields.Add("YRS_8", m_Year08)
                objWriter.Fields.Add("YRS_9", m_Year09)
                objWriter.Fields.Add("YRS_10", m_Year10)
                objWriter.Fields.Add("YRS_11", m_Year11)
                objWriter.Fields.Add("YRS_12", m_Year12)
                objWriter.Fields.Add("YRS_13", m_Year13)
                objWriter.Fields.Add("YRS_14", m_Year14)
                objWriter.Execute()
            End If
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            SaveData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            If InStr(1, UCase(g_sErrDescription), UCase("UNIQUE CONSTRAINT")) > 0 Then
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                SaveData = 89999
                Exit Function
            End If
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function ValidateData() As Integer

        ValidateData = 0
        m_Warning = ""


    End Function
    Public Property DeletedFlag() As Boolean
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Boolean)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property
    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property EndDateDTG() As String
        Get
            EndDateDTG = m_EndDateDTG
        End Get
        Set(ByVal Value As String)
            m_EndDateDTG = Value
        End Set
    End Property

    Public Property Year14() As Double
        Get
            Year14 = m_Year14
        End Get
        Set(ByVal Value As Double)
            m_Year14 = Value
        End Set
    End Property
    Public Property Year13() As Double
        Get
            Year13 = m_Year13
        End Get
        Set(ByVal Value As Double)
            m_Year13 = Value
        End Set
    End Property
    Public Property Year12() As Double
        Get
            Year12 = m_Year12
        End Get
        Set(ByVal Value As Double)
            m_Year12 = Value
        End Set
    End Property
    Public Property Year11() As Double
        Get
            Year11 = m_Year11
        End Get
        Set(ByVal Value As Double)
            m_Year11 = Value
        End Set
    End Property
    Public Property Year09() As Double
        Get
            Year09 = m_Year09
        End Get
        Set(ByVal Value As Double)
            m_Year09 = Value
        End Set
    End Property
    Public Property Year08() As Double
        Get
            Year08 = m_Year08
        End Get
        Set(ByVal Value As Double)
            m_Year08 = Value
        End Set
    End Property
    Public Property Year07() As Double
        Get
            Year07 = m_Year07
        End Get
        Set(ByVal Value As Double)
            m_Year07 = Value
        End Set
    End Property
    Public Property Year06() As Double
        Get
            Year06 = m_Year06
        End Get
        Set(ByVal Value As Double)
            m_Year06 = Value
        End Set
    End Property
    Public Property Year05() As Double
        Get
            Year05 = m_Year05
        End Get
        Set(ByVal Value As Double)
            m_Year05 = Value
        End Set
    End Property
    Public Property Year04() As Double
        Get
            Year04 = m_Year04
        End Get
        Set(ByVal Value As Double)
            m_Year04 = Value
        End Set
    End Property
    Public Property Year03() As Double
        Get
            Year03 = m_Year03
        End Get
        Set(ByVal Value As Double)
            m_Year03 = Value
        End Set
    End Property
    Public Property Year02() As Double
        Get
            Year02 = m_Year02
        End Get
        Set(ByVal Value As Double)
            m_Year02 = Value
        End Set
    End Property
    Public Property Year01() As Double
        Get
            Year01 = m_Year01
        End Get
        Set(ByVal Value As Double)
            m_Year01 = Value
        End Set
    End Property
    Public Property Year00() As Double
        Get
            Year00 = m_Year00
        End Get
        Set(ByVal Value As Double)
            m_Year00 = Value
        End Set
    End Property
    Public Property Year10() As Double
        Get
            Year10 = m_Year10
        End Get
        Set(ByVal Value As Double)
            m_Year10 = Value
        End Set
    End Property
    Public Property PresentAge() As Integer
        Get
            PresentAge = m_PresentAge
        End Get
        Set(ByVal Value As Integer)
            m_PresentAge = Value
        End Set
    End Property
    Public Property SexCode() As Integer
        Get
            SexCode = m_SexCode
        End Get
        Set(ByVal Value As Integer)
            m_SexCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
End Class

