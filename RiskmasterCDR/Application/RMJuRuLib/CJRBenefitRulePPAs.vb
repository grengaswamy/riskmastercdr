Option Strict Off
Option Explicit On
Public Class CJRBenefitRulePPAs
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRBenefitRulePPAs"
    Private Const sTableName As String = "WCP_RULE_PPA"
    'local variable to hold collection
    Private mCol As Collection
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED"
        sSQL = sSQL & ",ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD"
        sSQL = sSQL & ",UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",DELETED_FLAG"
        sSQL = sSQL & ",EFFECTIVE_DATE"
        sSQL = sSQL & ",ASSUM_IMPAIR_PERCT"
        sSQL = sSQL & ",FOL_PPDLIMITS_CODE"
        sSQL = sSQL & ",REQU_IMPAIRMT_CODE"
        sSQL = sSQL & ",REQU_MMIDATE_CODE"

        GetSQLFieldList = sSQL



    End Function
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJRBenefitRulePPA
        Try

            LoadData = 0

            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName

            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'While objReader.Read()
            '	objRecord = New CJRBenefitRulePPA
            '	With objRecord
            '		.AssumedImpairmentPercentage = objReader.GetDouble( "ASSUM_IMPAIR_PERCT")
            '		.DataHasChanged = False
            '		.DeletedFlag = False
            '		.EffectiveDateDTG = objReader.GetString( "EFFECTIVE_DATE")
            '		.FollowPPDLimitsCode = objReader.GetInt32( "FOL_PPDLIMITS_CODE")
            '		.JurisRowID = objReader.GetInt32( "JURIS_ROW_ID")
            '		.RequireImpairmentCode = objReader.GetInt32( "REQU_IMPAIRMT_CODE")
            '		.RequireMMIDateCode = objReader.GetInt32( "REQU_MMIDATE_CODE")
            '		.TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            '		Add(.AssumedImpairmentPercentage, .DataHasChanged, .DeletedFlag, .EffectiveDateDTG, .FollowPPDLimitsCode, .JurisRowID, .RequireImpairmentCode, .RequireMMIDateCode, .TableRowID, "k" & CStr(.TableRowID))
            '	End With
            '	'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            '	objRecord = Nothing
            '	
            '      End While

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRBenefitRulePPA
                With objRecord
                    .AssumedImpairmentPercentage = objReader.GetDouble("ASSUM_IMPAIR_PERCT")
                    .DataHasChanged = False
                    .DeletedFlag = False
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .FollowPPDLimitsCode = objReader.GetInt32("FOL_PPDLIMITS_CODE")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .RequireImpairmentCode = objReader.GetInt32("REQU_IMPAIRMT_CODE")
                    .RequireMMIDateCode = objReader.GetInt32("REQU_MMIDATE_CODE")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.AssumedImpairmentPercentage, .DataHasChanged, .DeletedFlag, .EffectiveDateDTG, .FollowPPDLimitsCode, .JurisRowID, .RequireImpairmentCode, .RequireMMIDateCode, .TableRowID, "k" & CStr(.TableRowID))
                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing
            End While
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Add(ByRef AssumedImpairmentPercentage As Double, ByRef DataHasChanged As Boolean, ByRef DeletedFlag As Boolean, ByRef EffectiveDateDTG As String, ByRef FollowPPDLimitsCode As Integer, ByRef JurisRowID As Integer, ByRef RequireImpairmentCode As Integer, ByRef RequireMMIDateCode As Integer, ByRef TableRowID As Integer, Optional ByRef sKey As String = "") As CJRBenefitRulePPA
        Const sFunctionName As String = "Add"
        Dim objNewMember As CJRBenefitRulePPA
        Try

            objNewMember = New CJRBenefitRulePPA

            objNewMember.AssumedImpairmentPercentage = AssumedImpairmentPercentage
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.FollowPPDLimitsCode = FollowPPDLimitsCode
            objNewMember.JurisRowID = JurisRowID
            objNewMember.RequireImpairmentCode = RequireImpairmentCode
            objNewMember.RequireMMIDateCode = RequireMMIDateCode
            objNewMember.TableRowID = TableRowID

            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If
            'return the object created
            Add = objNewMember
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        End Try

    End Function
    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRulePPA
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

