﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Application.MediaViewWrapper.MediaViewService;
using System.Xml.Linq;
using Riskmaster.ExceptionTypes;
using System.IO;



namespace Riskmaster.Application.MediaViewWrapper
{
    /// <summary>
    /// This class is used to interact with Media View web services
    /// Author:Raman Bhatia
    /// Date  :July 18 2011
    /// </summary>
    public class MediaView : IDisposable
    {
        #region private variables
        private  ServiceApp serviceApp = null;
        private string appConfigList = String.Empty;
        private string flagList = String.Empty;
        private string importList = String.Empty;
        #endregion 

        #region public properties
        public string AppConfigList
        {
            get
            {
                return appConfigList;
            }
            set
            {
                appConfigList = value;
            }
        }
        public string FlagList
        {
            get
            {
                return flagList;
            }
            set
            {
                flagList = value;
            }
        }

        public string ImportList
        {
            get
            {
                return importList;
            }
            set
            {
                importList = value;
            }
        }

        #endregion

        #region Contructor
        public MediaView()
        {
            serviceApp = new ServiceApp();
            RMConfigurationManager.GetMediaViewSettings();
            serviceApp.Url = MediaViewSection.MediaViewService;
           
        }
        #endregion

        #region public methods

        public bool MMGetAttachedDocumentsList(string MMDocNum, string MMGroupID, out IEnumerable<XElement> DocList)
        {
            MediaViewService.QUERYWRAP QueryObj = null;
            XElement oEle = null;
            bool bResponse = false;
            string sResponse = "";

            try
            {
                //Anu Tennyson : Media View different for Claim and Policy System Starts
                switch (MMGroupID.ToUpper())
                {
                    case "CLAIM":
                        appConfigList = MediaViewSection.AppConfigListCLM;
                    flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceCLM, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    case "POLICY":
                        appConfigList = MediaViewSection.AppConfigListPOL;
                    flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourcePOL, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    case "EVENT":
                        appConfigList = MediaViewSection.AppConfigListEvent;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceEvent, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    case "MISC":
                        appConfigList = MediaViewSection.AppConfigListMM;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceMM, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    default:
                        appConfigList = MediaViewSection.AppConfigList;
                    flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.Source, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                }
                //Anu Tennyson : Media View different for Claim and Policy System Ends
                QueryObj = new MediaViewService.QUERYWRAP();
                QueryObj.Query = new MediaViewService.QUERYINFO();
                QueryObj.Query.SortName = "";
                QueryObj.Query.SortDesc = false;
                QueryObj.Query.SortFormat = "";
                QueryObj.Query.CritRoot = GetCritAnd(1);
                QueryObj.Query.CritRoot.Items = new MediaViewService.CRITINFO[3];
                QueryObj.Query.CritRoot.Items[0] = GetCritSimple("MMDocNum", OPERENUM.OPER_STARTSWITH, MMDocNum, "");
                QueryObj.Query.CritRoot.Items[1] = GetCritSimple("UserType", OPERENUM.OPER_EQ, MediaViewSection.UserType, "");
                QueryObj.Query.CritRoot.Items[2] = GetCritSimple("MMGroupID", OPERENUM.OPER_STARTSWITH, MMGroupID, "");

                sResponse = serviceApp.MMSearch(appConfigList, QueryObj, flagList);
                oEle = XElement.Parse(sResponse);

                bResponse = oEle.Element("Info").Element("Status").Value.ToUpper() == "S" ? true : false;

                DocList = oEle.Descendants("Doc");

                

            }
            catch (Exception e)
            {
                bResponse = false;
                throw new RMAppException(e.Message);
            }
            
            return bResponse;

        }
        
        

        public bool MMGetDocumentByKey(string sKey, out XElement Doc, ref string OutFileLocation , ref string OutFileName, string sMMGroupID)
        {
            bool bResponse = false;
            string sResponse = "";
            
           
            try
            {
                //Anu Tennyson : Media View different for Claim and Policy System Starts
                switch (sMMGroupID.ToUpper())
                {
                    case "CLAIM":
                        appConfigList = MediaViewSection.AppConfigListCLM;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceCLM, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    case "POLICY":
                        appConfigList = MediaViewSection.AppConfigListPOL;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourcePOL, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    case "EVENT":
                        appConfigList = MediaViewSection.AppConfigListEvent;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceEvent, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    case "MISC":
                        appConfigList = MediaViewSection.AppConfigListMM;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceMM, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    default:
                        appConfigList = MediaViewSection.AppConfigList;
                    flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.Source, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                }
                //Anu Tennyson : Media View different for Claim and Policy System Ends
                //appConfigList = MediaViewSection.AppConfigList;
                //flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},UserType:{3}", MediaViewSection.Source, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                sResponse = serviceApp.MMGet(appConfigList, sKey, flagList);
                Doc = XElement.Parse(sResponse);

                bResponse = Doc.Element("Info").Element("Status").Value.ToUpper() == "S" ? true : false;
                if(String.IsNullOrEmpty(OutFileName))
                {
                    OutFileName = Doc.Element("Doc").Element("File").Value;
                }


                if(bResponse)
                {

                  if(serviceApp.ResponseSoapContext.Attachments.Count > 0)
                  {
                      if (String.IsNullOrEmpty(OutFileLocation))
                      {
                          OutFileLocation = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath , "MediaView\\Download");
                      }
                      
                     string OutFilePath = RMConfigurator.CombineFilePath(OutFileLocation  , OutFileName);
                     System.IO.Stream InStream = serviceApp.ResponseSoapContext.Attachments[0].Stream;  
                     SaveStreamAsFile(ref InStream, OutFilePath);
                  }
                }
            }
        

            catch (Exception e)
            {
                bResponse = false;
                throw new RMAppException(e.Message);
            }

            return bResponse;

        }

        public bool MMUpdate(string oldMMDocNum, string MMGroupID, string newMMDocNum)
        {
            MediaViewService.QUERYWRAP QueryObj = null;
            XElement oEle = null;
            bool bResponse = false;
            string sResponse = "";
            string updateList = "";
            try
            {
                //Anu Tennyson : Media View different for Claim and Policy System Starts
                switch (MMGroupID.ToUpper())
                {
                    case "CLAIM":
                        appConfigList = MediaViewSection.AppConfigListCLM;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceCLM, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    case "POLICY":
                        appConfigList = MediaViewSection.AppConfigListPOL;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourcePOL, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    case "EVENT":
                        appConfigList = MediaViewSection.AppConfigListEvent;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceEvent, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    case "MISC":
                        appConfigList = MediaViewSection.AppConfigListMM;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceMM, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    default:
                        appConfigList = MediaViewSection.AppConfigList;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.Source, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                }

                //Anu Tennyson : Media View different for Claim and Policy System Ends
                //appConfigList = MediaViewSection.AppConfigList;
                //flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},UserType:{3}", MediaViewSection.Source, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                QueryObj = new MediaViewService.QUERYWRAP();
                QueryObj.Query = new MediaViewService.QUERYINFO();
                QueryObj.Query.SortName = "";
                QueryObj.Query.SortDesc = false;
                QueryObj.Query.SortFormat = "";
                QueryObj.Query.CritRoot = GetCritAnd(1);
                QueryObj.Query.CritRoot.Items = new MediaViewService.CRITINFO[3];
                QueryObj.Query.CritRoot.Items[0] = GetCritSimple("MMDocNum", OPERENUM.OPER_STARTSWITH, oldMMDocNum, "");
                QueryObj.Query.CritRoot.Items[1] = GetCritSimple("UserType", OPERENUM.OPER_EQ, MediaViewSection.UserType, "");
                QueryObj.Query.CritRoot.Items[2] = GetCritSimple("MMGroupID", OPERENUM.OPER_STARTSWITH, MMGroupID, "");

                updateList = "MMDocNum:" + newMMDocNum;

                sResponse = serviceApp.MMUpdate(appConfigList, QueryObj, updateList,flagList);
                oEle = XElement.Parse(sResponse);

                bResponse = oEle.Element("Info").Element("Status").Value.ToUpper() == "S" ? true : false;

                
            }
            catch (Exception e)
            {
                bResponse = false;
                throw new RMAppException(e.Message);
            }

            return bResponse;

        }
        
        


        public bool MMGetDocumentByName(string MMDocNum, string MMGroupID , ref string SearchFileName, ref string OutFileLocation)
        {
            bool bResponse = false;
            IEnumerable<XElement> DocList;
            IEnumerable<string> sKey = null;
            XElement Doc = null;
            string OutFileName = SearchFileName;

            try
            {
                bResponse = MMGetAttachedDocumentsList(MMDocNum, MMGroupID, out DocList);
                if (bResponse)
                {
                    sKey = from item in DocList
                           where ((string)item.Element("Fields").Element("MMReferenceId")).ToLower() == OutFileName.ToLower()
                           && ((string)item.Element("Fields").Element("MMDisplay")).ToLower() == "normal"
                           select (string)item.Element("Key");

                }

                if (sKey.Count() != 0)
                {
                    bResponse = MMGetDocumentByKey(sKey.ToArray()[0], out Doc, ref OutFileLocation, ref SearchFileName, MMGroupID);
                }
                else
                {
                    bResponse = false;
                }
                
            }

               
            catch (Exception e)
            {
                bResponse = false;
                throw new RMAppException(e.Message);
            }

            return bResponse;

        }

        public bool MMDeleteDocumentByKey(string sKey, string MMGroupID)
        {
            bool bResponse = false;
            string sResponse = "";
            XElement Doc = null;

            try
            {
                //Anu Tennyson : Media View different for Claim and Policy System Starts
                switch (MMGroupID.ToUpper())
                {
                    case "CLAIM":
                        appConfigList = MediaViewSection.AppConfigListCLM;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceCLM, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    case "POLICY":
                        appConfigList = MediaViewSection.AppConfigListPOL;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourcePOL, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    case "EVENT":
                        appConfigList = MediaViewSection.AppConfigListEvent;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceEvent, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    case "MISC":
                        appConfigList = MediaViewSection.AppConfigListMM;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceMM, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                    default:
                        appConfigList = MediaViewSection.AppConfigList;
                        flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.Source, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                        break;
                }

                //Anu Tennyson : Media View different for Claim and Policy System Ends
                //appConfigList = MediaViewSection.AppConfigList;
                //flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},UserType:{3}", MediaViewSection.Source, MediaViewSection.CommonUser, MediaViewSection.Enterprise, MediaViewSection.UserType);
                sResponse = serviceApp.MMDelete(appConfigList, sKey, flagList);
                Doc = XElement.Parse(sResponse);

                bResponse = Doc.Element("Info").Element("Status").Value.ToUpper() == "S" ? true : false;
            }


            catch (Exception e)
            {
                bResponse = false;
                throw new RMAppException(e.Message);
            }

            return bResponse;

        }

        public bool MMDeleteDocumentByName(string MMDocNum, string MMGroupID, string sFileName)
        {
            bool bResponse = false;
            IEnumerable<XElement> DocList;
            IEnumerable<string> sKey = null;
            


            try
            {
                bResponse = MMGetAttachedDocumentsList(MMDocNum, MMGroupID, out DocList);
                if (bResponse)
                {
                    sKey = from item in DocList
                           where ((string)item.Element("Fields").Element("MMReferenceId")).ToLower() == sFileName.ToLower()
                           && ((string)item.Element("Fields").Element("MMDisplay")).ToLower() == "normal"
                           select (string)item.Element("Key");

                }

                if (sKey.Count() != 0)
                {
                    bResponse = MMDeleteDocumentByKey(sKey.ToArray()[0], MMGroupID);
                }
                else
                {
                    bResponse = false;
                }

            }


            catch (Exception e)
            {
                bResponse = false;
                throw new RMAppException(e.Message);
            }

            return bResponse;

        }

        public bool MMUploadDocument(string importFileName, string importFilePath , importList oList)
        {
            bool bResponse = false;
            Microsoft.Web.Services2.Dime.DimeAttachment AttachObj = null;
            string ImportFileExt= "";
            string importFileFullName = importFilePath + "\\" + importFileName;
            string sImportList = "";
            string OutXmlStr;
            XElement oEle = null;


            try
            {
                if (File.Exists(importFileFullName))
                {
                    ImportFileExt = System.IO.Path.GetExtension(importFileFullName);
                    AttachObj = new Microsoft.Web.Services2.Dime.DimeAttachment(ImportFileExt, Microsoft.Web.Services2.Dime.TypeFormat.Unknown, importFileFullName);
                    serviceApp.RequestSoapContext.Attachments.Add(AttachObj);
                    //Anu Tennyson : Media View different for Claim and Policy System Starts
                    switch (oList.MMGroupId.ToUpper())
                    {
                        case "CLAIM":
                            appConfigList = MediaViewSection.AppConfigListCLM;
                            flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceCLM, MediaViewSection.CommonUser, MediaViewSection.Enterprise, importFileName);
                            break;
                        case "POLICY":
                            appConfigList = MediaViewSection.AppConfigListPOL;
                            flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourcePOL, MediaViewSection.CommonUser, MediaViewSection.Enterprise, importFileName);
                            break;
                        case "EVENT":
                            appConfigList = MediaViewSection.AppConfigListEvent;
                            flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceEvent, MediaViewSection.CommonUser, MediaViewSection.Enterprise, importFileName);
                            break;
                        case "MISC":
                            appConfigList = MediaViewSection.AppConfigListMM;
                            flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceMM, MediaViewSection.CommonUser, MediaViewSection.Enterprise, importFileName);
                            break;
                        default:
                            appConfigList = MediaViewSection.AppConfigList;
                            flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.Source, MediaViewSection.CommonUser, MediaViewSection.Enterprise, importFileName);
                            break;
                    }
                    //Anu Tennyson : Media View different for Claim and Policy System Ends
                    sImportList = String.Format("MMReferenceId:{0},MMAgency:{1},MMAgent:{2},MMType:{3},MMDescription:{4},MMSecurity:{5},MMNotes:{6},MMDateAdded:{7},FileSize:{8},MMDocNum:{9},MMGroupID:{10},FileExt:{11},FileName:{12}", oList.MMReferenceId, oList.MMAgency, oList.MMAgent, oList.MMType, oList.MMDescription, oList.MMSecurity, oList.MMNotes, oList.MMDateAdded, oList.FileSize, oList.MMDocNum, oList.MMGroupId, ImportFileExt, importFileName);
                    OutXmlStr = serviceApp.MMAttach(appConfigList, sImportList, importFileName, flagList);
                    oEle = XElement.Parse(OutXmlStr);

                    bResponse = oEle.Element("Info").Element("Status").Value.ToUpper() == "S" ? true : false;
                }

            }
            catch(Exception e)
            {
                bResponse = false;
            }

            return bResponse;
        }

        public bool MMUploadDocument(string importFileName, byte[] content ,  importList oList)
        {
            bool bResponse = false;
            Microsoft.Web.Services2.Dime.DimeAttachment AttachObj = null;
            string ImportFileExt = "";
            string sPath = RMConfigurator.UserDataPath;
            string importFileFullName = sPath + @"\MediaView\" + importFileName;
            
            string sImportList = "";
            string OutXmlStr;
            XElement oEle = null;


            try
            {
                if (content != null)
                {
                    WriteBinaryFile(importFileFullName, content);
                }

                if (File.Exists(importFileFullName))
                {
                    ImportFileExt = System.IO.Path.GetExtension(importFileFullName);
                    AttachObj = new Microsoft.Web.Services2.Dime.DimeAttachment(ImportFileExt, Microsoft.Web.Services2.Dime.TypeFormat.Unknown, importFileFullName);
                    serviceApp.RequestSoapContext.Attachments.Add(AttachObj);
                    //Anu Tennyson : Media View different for Claim and Policy System Starts
                    switch (oList.MMGroupId.ToUpper())
                    {
                        case "CLAIM":
                            appConfigList = MediaViewSection.AppConfigListCLM;
                            flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceCLM, MediaViewSection.CommonUser, MediaViewSection.Enterprise, importFileName);
                            break;
                        case "POLICY":
                            appConfigList = MediaViewSection.AppConfigListPOL;
                            flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourcePOL, MediaViewSection.CommonUser, MediaViewSection.Enterprise, importFileName);
                            break;
                        case "EVENT":
                            appConfigList = MediaViewSection.AppConfigListEvent;
                            flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceEvent, MediaViewSection.CommonUser, MediaViewSection.Enterprise, importFileName);
                            break;
                        case "MISC":
                            appConfigList = MediaViewSection.AppConfigListMM;
                            flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.SourceMM, MediaViewSection.CommonUser, MediaViewSection.Enterprise, importFileName);
                            break;
                        default:
                            appConfigList = MediaViewSection.AppConfigList;
                            flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.Source, MediaViewSection.CommonUser, MediaViewSection.Enterprise, importFileName);
                            break;
                    }
                    //Anu Tennyson : Media View different for Claim and Policy System Ends
                    //appConfigList = MediaViewSection.AppConfigList;
                    //flagList = String.Format("Config:{0},UserId:{1},Enterprise:{2},LocalFile:{3}", MediaViewSection.Source, MediaViewSection.CommonUser, MediaViewSection.Enterprise, importFileName);
                    sImportList = String.Format("MMReferenceId:{0},MMAgency:{1},MMAgent:{2},MMType:{3},MMDescription:{4},MMSecurity:{5},MMNotes:{6},MMDateAdded:{7},FileSize:{8},MMDocNum:{9},MMGroupID:{10},FileExt:{11},FileName:{12}", oList.MMReferenceId, oList.MMAgency, oList.MMAgent, oList.MMType, oList.MMDescription, oList.MMSecurity, oList.MMNotes, oList.MMDateAdded, oList.FileSize, oList.MMDocNum, oList.MMGroupId, ImportFileExt, importFileName);
                    OutXmlStr = serviceApp.MMAttach(appConfigList, sImportList, importFileName, flagList);
                    oEle = XElement.Parse(OutXmlStr);

                    bResponse = oEle.Element("Info").Element("Status").Value.ToUpper() == "S" ? true : false;
                }

            }
            catch (Exception e)
            {
                bResponse = false;
            }

            return bResponse;
        }

#endregion

        #region private methods
        private int GetFileBuffer(string p_sFileName, ref byte[] fileBuffer)
        {
            FileStream fileStream;
            BinaryReader binaryRdr;
            int iRetCd = 42;

            try
                {
                    
                    fileStream = File.OpenRead(p_sFileName);
                    binaryRdr = new BinaryReader(fileStream);
                    fileBuffer = binaryRdr.ReadBytes(Convert.ToInt32(fileStream.Length));

                    binaryRdr.Close();
                    fileStream.Close();
                    iRetCd = 0;
                }
               
                catch (Exception e)
                {
                    
                }
       
           
            return iRetCd;
        }

        private void WriteBinaryFile(string fileName, byte[] fileBuffer)
        {
            FileStream fileStream;
            BinaryWriter binaryWrtr;

            try
            {
                // create new or overwrite existing
                fileStream = new FileStream(fileName, FileMode.Create);
                binaryWrtr = new BinaryWriter(fileStream);
                binaryWrtr.Write(fileBuffer);

                binaryWrtr.Close();
                fileStream.Close();
            }
            catch (Exception ex)
            {

            }
        }

        private void SaveStreamAsFile(ref System.IO.Stream InStream, string FilePath)
            {
	            byte[] MemBytes = null;
	            System.IO.FileStream OutStream = null;
	            if ((InStream.Position > 0 & InStream.CanSeek)) {
		            InStream.Position = 0;
	            }
	            System.IO.MemoryStream MemStream = new System.IO.MemoryStream();
	            while (MemStream.Length != InStream.Length) {
		            MemStream.WriteByte(Convert.ToByte(InStream.ReadByte()));
	            }
	            MemBytes = MemStream.ToArray();
	            OutStream = System.IO.File.OpenWrite(FilePath);
	            OutStream.Write(MemBytes, 0, MemBytes.Length);
	            OutStream.Close();
            }
        
        private MediaViewService.CRITINFO GetCritAnd(int Max)
        {
            MediaViewService.CRITINFO oCrit = default(MediaViewService.CRITINFO);
            oCrit = new MediaViewService.CRITINFO();
            oCrit.Type = CRITENUM.CRIT_AND;
            oCrit.Name = "";
            oCrit.Oper = OPERENUM.OPER_EQ;
            oCrit.Value1 = "";
            oCrit.Value2 = "";
            // ERROR: Not supported in C#: ReDimStatement

            return oCrit;
        }

        private MediaViewService.CRITINFO GetCritSimple(string Name, OPERENUM Oper, string Value1, string Value2)
        {
            MediaViewService.CRITINFO oCrit = default(MediaViewService.CRITINFO);
            oCrit = new MediaViewService.CRITINFO();
            oCrit.Type = CRITENUM.CRIT_SIMPLE;
            oCrit.Name = Name;
            oCrit.Oper = Oper;
            oCrit.Value1 = Value1;
            oCrit.Value2 = Value2;
            

            return oCrit;
        }
        #endregion

        #region Dispose method
        public void Dispose()
        {
            serviceApp.Dispose();
        }
        #endregion
    }

    /// <summary>
    /// This class is used to store information regarding the document to be uploaded to media view system
    /// Author:Raman Bhatia
    /// Date  :July 18 2011
    /// </summary>
    public class importList
    {
       
        public string MMDocNum { get; set; }
        public string MMGroupId { get; set; }
        public string MMReferenceId { get; set; }
        public string MMAgency { get; set; }
        public string MMAgent { get; set; }
        public string MMType { get; set; }
        public string MMDescription { get; set; }
        public string MMSecurity { get; set; }
        public string MMNotes { get; set; }
        public string MMDateAdded { get; set; }
        public string FileSize { get; set; }

        public importList(string p_mmDocNum , string p_mmGroupId , string p_mmDocName)
        {
            MMDocNum = p_mmDocNum;
            MMGroupId = p_mmGroupId;
            MMReferenceId = p_mmDocName;
            MMDateAdded = DateTime.Now.ToShortDateString();
        }

        
    }
}
