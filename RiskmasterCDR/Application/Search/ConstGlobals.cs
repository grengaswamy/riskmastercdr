using System;

namespace Riskmaster.Application.Search
{
	/// <summary>
	///Author:   Sumeet Rathod
	///Dated :   24th August 2004
	///Purpose:   This class contains the constants, which are being used in the supporting search classes.
	/// </summary>
	public class ConstGlobals
	{
		/// <summary>
		/// This is default constructor
		/// </summary>
		public ConstGlobals()
		{
		}

		/// <summary>
		///Represents organizational hierarchy
		/// </summary>
		public const long ORGHEI_SEARCH_PARAM = -3;
		//Search modules
		/// <summary>
		/// Claim search
		/// </summary>
		public const long CLAIM_SEARCH = 1;
		/// <summary>
		/// Event search
		/// </summary>
		public const long EVENT_SEARCH = 2;
		/// <summary>
		/// Employee search
		/// </summary>
		public const long EMPLOYEE_SEARCH = 3;
		/// <summary>
		/// Entity search
		/// </summary>
		public const long ENTITY_SEARCH = 4;
		/// <summary>
		/// Vehicle search
		/// </summary>
		public const long VEHICLE_SEARCH = 5;
		/// <summary>
		/// Policy search
		/// </summary>
		public const long POLICY_SEARCH = 6;
		/// <summary>
		/// Payment search
		/// </summary>
		public const long PAYMENT_SEARCH = 7;
		/// <summary>
		/// Patient search
		/// </summary>
		public const long PATIENT_SEARCH = 8;
		/// <summary>
		/// Physician search
		/// </summary>
		public const long PHYSICIAN_SEARCH = 9;
		/// <summary>
		/// Medical Staff search
		/// </summary>
		public const long MED_STAFF_SEARCH = 10;
		/// <summary>
		/// Disability Plan search
		/// </summary>
		public const long DISP_PLAN_SEARCH = 11;
        //Shruti 
        /// <summary>
        /// Leave Plan search
        /// </summary>
        public const long LV_PLAN_SEARCH = 23;
		/// <summary>
		/// Administrative Tracking search
		/// </summary>
		public const long AT_SEARCH = 20;
        // Start Naresh Enhanced Policy Search
        /// <summary>
        /// Enhanced Policy Search
        /// </summary>
        public const long ENHANCED_POLICY_SEARCH = 21;
        // End Naresh Enhanced Policy Search
        //Anu Tennyson for MITS 18291 : STARTS 10/25/2009
        public const long PROPERTY_SEARCH = 12;
        //Anu Tennyson for MITS 18291 : ENDS
       //skhare7 Diary 
        public const long DIARY_SEARCH = 13;
      // skhare7
        // Start Anshul Catastrophe Search MITS - 28528
        /// <summary>
        /// Catastrophe Search
        /// </summary>
        public const long CATASTROPHE_SEARCH = 24;
        // End Anshul Catastrophe Search  MITS - 28528
        /// <summary>
        /// Driver Search
        /// </summary>
        public const long DRIVER_SEARCH = 25;  //Amandeep Driver Enhacement
        //Ashish ahuja Address RMA-8753
        public const long ADDRESS_SEARCH = 26;  
        
		//ASINGH263 Arbitration search mits 34597
		public const long ARBITRATION_SEARCH = 26;  
	}
}
