﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/11/2014 | 34276  | anavinkumars   | Enhancement to Entity related functionality in RMA 
 **********************************************************************************************/
using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.IO;
using System.Collections;
using System.Text;
using System.Data;
using Riskmaster.Security.RMApp;
using Riskmaster.Settings;
using Riskmaster.DataModel;
using Riskmaster.Application.SupportScreens;
using System.Text.RegularExpressions;
using Riskmaster.Security;
namespace Riskmaster.Application.Search
{
    /// <summary>
    ///Author  :   Sumeet Rathod
    ///Dated   :   24th August 2004
    ///Purpose :   This class queries the database and fetches the search data for the given search module.
    /// </summary>
    public class SearchResults
    {
        #region Private Variables

        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_sConnectionString = "";

        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private static string m_sDBType = "";

        /// <summary>
        /// Represents the User Id.
        /// </summary>
        private long m_lUserId = 0;
        /// <summary>
        /// Represents the User Id.
        /// </summary>
        private long m_lGroupId = 0;
        /// <summary>
        /// Represents the User Name.
        /// </summary>
        private string m_sUserName = "";

        /// <summary>
        /// The total number of result records 
        /// </summary>
        private long m_lTotalRecords = 0;
        /// AttachedRecord for Diary Search//skhare7
        /// </summary>
        private string m_sAttachedRecord = string.Empty;
        /// <summary>

        //MITS Abhishek 10651
        private bool m_bPrintSearch = false;

        //abansa23 MITS 17648 09/10/2009
        private string m_SelOrgLevels = "";
        private string m_TableName = "";
        private bool m_bOrgSec = false; //pmittal5 Confidential Record
        //Added Rakhi for R7:Add Emp Data Elements
        bool bPhoneNumbersIncluded = false;
        bool bAddressesIncluded = false;
        bool bOrgHierarchyPhoneIncluded = false;
        bool bAddressFlag = false;
        bool bOrgFlag = false;
        //Added Rakhi for R7:Add Emp Data Elements
        //skhare7 
        string sColumnOrder = string.Empty;
        int iEntryId = 0;
        public DbConnection p_Connection = null;
        SysSettings objSysSettingsGlobal = null;//Deb MITS 25775
        /// <summary>
        /// Call from Mobile Adjuster
        /// </summary>
        private bool m_bFromMobileAdj = false;

        /// <summary>
        /// /// Call from Mobility Windows Adjuster //rsharma220
        /// </summary>
        private bool m_bFromMobilityAdj = false;

        /// <summary>
        /// Call from Mobile Adjuster for Claim Info part    
        /// </summary>
        //private bool m_bFromMobileAdjClaimInfo = false;   //akaur9 Mobile Adjuster added a new variable for the claim info part

        /// <summary>
        /// Call from Mobile Adjuster for Claim List part    
        /// </summary>
        private bool m_bFromMobileAdjClaimList = false;   //akaur9 Mobile Adjuster added a new variable for the claim list part
        private bool m_bFromMobilityAdjClaimList = false;
        private bool m_bFromMobilityAdjEventList = false;  //mbahl3 mobility Event list changes

        bool bEntityIdType = false;//MITS:34276-- Entity ID Type search 
        private int m_iClientId = 0;

        #endregion

        #region Constructor
                
        //public SearchResults()
        //{
        //}
        
        /// <summary>
        ///  This is default constructor.
        /// </summary>
        /// <param name="p_sConnectionstring">Database connection string</param>
        /// <param name="p_iClientId">cloud client ID</param>
        public SearchResults(string p_sConnectionstring, int p_iClientId)
        {
            DbConnection objConn = null;
            try
            {
                m_sConnectionString = p_sConnectionstring;
                m_iClientId = p_iClientId;
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                m_sDBType = objConn.DatabaseType.ToString().ToUpper();
                objSysSettingsGlobal = new SysSettings(m_sConnectionString, m_iClientId);//Deb MITS 25775
            }
            catch (Exception p_objException)
            {
                throw new StringException(Globalization.GetString("SearchResults.Constructor.Connection",m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                objConn = null;
            }
        }


        #endregion

        #region Properties

        /// <summary>
        /// Gets & sets the user id
        /// </summary>
        public long UserId { get { return m_lUserId; } set { m_lUserId = value; } }
        public long GroupId { get { return m_lGroupId; } set { m_lGroupId = value; } }
        //MITS 10651 Start Abhishek
        public bool PrintSearch { get { return m_bPrintSearch; } set { m_bPrintSearch = value; } }
        //MITS 10651 End
        public long TotalRecords { get { return m_lTotalRecords; } set { m_lTotalRecords = value; } }

        //abansa23 MITS 17648 09/10/2009
        public string SelOrgLevels { get { return m_SelOrgLevels; } set { m_SelOrgLevels = value; } }
        public string TableName { get { return m_TableName; } set { m_TableName = value; } }
        public bool IsOrgSec { get { return m_bOrgSec; } set { m_bOrgSec = value; } }
        public bool IsEntitySearch = false;//mbahl3 Mits 30224

        /// <summary>
        /// Attached Record Property for Diary Search//skhare7
        /// </summary>
        private string AttachedRecordColumn
        {
            get
            {
                return (m_sAttachedRecord);
            }
            set
            {
                m_sAttachedRecord = value;
            }
        }
        /// <summary>
        public SysSettings SysSettingsGlobal { get { return objSysSettingsGlobal; } set { objSysSettingsGlobal = value; } }//Deb MITS 25775
        public bool IsMobileAdjuster { get { return m_bFromMobileAdj; } set { m_bFromMobileAdj = value; } } //gbhatnagar Mobile Adjuster
        public bool IsMobilityAdjuster { get { return m_bFromMobilityAdj; } set { m_bFromMobilityAdj = value; } } //rsharma220 Mobility Adjuster

        // public bool IsMobileAdjusterClaimInfo { get { return m_bFromMobileAdjClaimInfo; } set { m_bFromMobileAdjClaimInfo = value; } } //akaur9 Mobile Adjuster

        public bool IsMobileAdjusterClaimList { get { return m_bFromMobileAdjClaimList; } set { m_bFromMobileAdjClaimList = value; } } //akaur9 Mobile Adjuster
        public bool IsMobilityAdjusterClaimList { get { return m_bFromMobilityAdjClaimList; } set { m_bFromMobilityAdjClaimList = value; } } //rsharma220 Mobility Adjuster
        public bool IsMobilityAdjusterEventList { get { return m_bFromMobilityAdjEventList; } set { m_bFromMobilityAdjEventList = value; } } //mbal3 Mobility Adjuster

        // npadhy Jira 6415 We need Security DB info and DSN id to retrieve the user details for User Lookup
        public UserLogin UserLogin
        {
            get;
            set;
        }
        #endregion

        private int GetCatID(string p_sSearchCat)
        {

            int iCatId = 0;
            switch (p_sSearchCat.ToLower())
            {
                case "claimgc":  //Claim search
                case "claimwc":
                case "claimva":
                case "claimdi":
                case "claim":
                    iCatId = 1;
                    break;
                case "event":  //Event search
                    iCatId = 2;
                    break;
                case "piemployee":
                case "employee":  //employee search
                    iCatId = 3;
                    break;
                case "entity":  //entity search
                case "people":	 //People search
                case "entitymaint":
                    iCatId = 4;
                    break;
                case "vehicle":  //vehicle search
                    iCatId = 5;
                    break;
                case "policy":  //policy search
                    iCatId = 6;
                    break;
                // Start Naresh Enhanced Policy Search

                case "policybillinglookup":
                case "policyenh":
                    iCatId = 21;
                    break;
                // End Naresh Enhanced Policy Search
                case "payment":  //payment search
                    iCatId = 7;
                    break;
                case "pipatient":
                case "patient":  //patient search
                    iCatId = 8;
                    break;
                case "piphysician":
                case "physician":  //physician search
                    iCatId = 9;
                    break;
                case "pimedstaff":
                case "medstaff":  //medstaff search
                case "staff":
                    iCatId = 10;
                    break;
                case "at":
                    iCatId = 20;
                    break;
                case "dp":
                case "displan": // Adding "displan" to execute "Return to Search" in case of Disability Plan
                    iCatId = 11;
                    break;
                case "leaveplan": // Adding "leaveplan" 05/05/2010 Raman Bhatia
                    iCatId = 23;
                    break;
                //Anu Tennyson for MITS 18291 : STARTS 10/25/2009
                case "propertyunit":  //property search
                    iCatId = 12;
                    break;
                //Anu Tennyson for MITS 18291 : ENDS 
                //skhare7
                case "diary":  //diary search
                    iCatId = 13;
                    break;
                //Ashish Ahuja Address Master 8753
                case "address":
                    iCatId = 26;
                    break;
                default:
                    iCatId = 4; // Changing the default search from admin tracking to standard entity search
                    break;
            }//end switch
            return iCatId;
        }
        #region Public Functions
        public void SetUserPrefNode(XmlDocument p_objDoc, int p_iUserId)
        {
            string sSql = "";
            DbReader objReader = null;
            XmlDocument objPrefXml = null;
            XmlElement objElem = null;
            DbCommand objCmd = null;//Umesh
            DbParameter objParam = null;//Umesh
            string sXml = "";
            string sFormName = "";
            string sViewId = "";
            string sCatId = "";
            string sFullEntitySearch = string.Empty;
            string sSetAsDefault = string.Empty;
            try
            {
                sViewId = p_objDoc.SelectSingleNode("//ViewID").InnerText;
                sFormName = p_objDoc.SelectSingleNode("//FormName").InnerText;
                if (sFormName != "")
                {
                    sCatId = GetCatID(sFormName).ToString();
                }
                else
                {
                    sCatId = p_objDoc.SelectSingleNode("//CatId").InnerText;
                }
                sSql = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID=" + p_iUserId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);

                //if user xml is present in the database
                if (objReader != null && objReader.Read())
                {

                    if ((Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML")) != null) ||
                        (Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML")) != ""))
                    {
                        objPrefXml = new XmlDocument();
                        sXml = Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                        objReader.Close();
                        objPrefXml.LoadXml(sXml);
                        if (p_objDoc.SelectSingleNode("//SetasDefault") != null && p_objDoc.SelectSingleNode("//SetasDefault").InnerText != string.Empty)//skhare7 JIRA 340
                        {
                            sViewId = p_objDoc.SelectSingleNode("//ViewID").InnerText;

                            sFormName = p_objDoc.SelectSingleNode("//FormName").InnerText;
                            if (sFormName != "")
                            {
                                sCatId = GetCatID(sFormName).ToString();
                            }
                            else
                            {
                                if (p_objDoc.SelectSingleNode("//CatId") != null)//skhare7 JIRA 340
                                    sCatId = p_objDoc.SelectSingleNode("//CatId").InnerText;
                            }
                        }
                        objElem = (XmlElement)objPrefXml.SelectSingleNode("//SearchDefault");
                        if (objElem != null)
                        {
                            if (objElem.GetAttribute(sCatId) == null)
                                objElem.SetAttribute("Cat_" + sCatId, sViewId);
                            else
                                objElem.SetAttribute("Cat_" + sCatId, sViewId);
                        }
                        else
                        {
                            objElem = objPrefXml.CreateElement("SearchDefault");
                            objElem.SetAttribute("Cat_" + sCatId, sViewId);
                            objPrefXml.FirstChild.AppendChild(objElem);
                        }
                        //skhare7 JIRA 340 start
                        if (p_objDoc.SelectSingleNode("//UseFullEntitySearch") != null && (p_objDoc.SelectSingleNode("//UseFullEntitySearch").InnerText != string.Empty) && (p_objDoc.SelectSingleNode("//UseFullEntitySearch").InnerText != "1"))//skhare7 JIRA 340
                        {
                            sFullEntitySearch = p_objDoc.SelectSingleNode("//UseFullEntitySearch").InnerText;


                            //JIRA 340 End

                            if (string.Compare(sFullEntitySearch, "true") == 0)
                                sFullEntitySearch = "-1";
                            else
                            sFullEntitySearch = "0";
                            objElem = (XmlElement)objPrefXml.SelectSingleNode("//UseFullEntitySearch");
                            if (objElem != null)
                            {
                                if (objElem.GetAttribute("value") == null)
                                    objElem.SetAttribute("value", sFullEntitySearch);
                                else
                                    objElem.SetAttribute("value", sFullEntitySearch);
                            }
                            else
                            {
                                objElem = objPrefXml.CreateElement("UseFullEntitySearch");
                                objElem.SetAttribute("value", sFullEntitySearch);
                                objPrefXml.FirstChild.AppendChild(objElem);
                            }
                        }
                        //skhare7 JIRA 340 End
                        sXml = objPrefXml.InnerXml;
                        //sSql = "UPDATE USER_PREF_XML SET PREF_XML='" + sXml + "' WHERE USER_ID=" + p_iUserId;
                        DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString);
                        objConn.Open();
                        objCmd = objConn.CreateCommand();
                        objParam = objCmd.CreateParameter();
                        objParam.Direction = ParameterDirection.Input;
                        objParam.Value = sXml;
                        objParam.ParameterName = "XML";
                        objParam.SourceColumn = "PREF_XML";
                        objCmd.Parameters.Add(objParam);
                        sSql = "UPDATE USER_PREF_XML SET PREF_XML=~XML~ WHERE USER_ID=" + p_iUserId;
                        //objConn.ExecuteNonQuery (sSql);
                        objCmd.CommandText = sSql;
                        objCmd.ExecuteNonQuery();
                        objConn.Close();
                        objConn.Dispose();

                    }
                }
                else
                {
                    //pmittal5 MITS 13187 10/16/08 - Replacing 2 single quotes with double quotes
                     if (p_objDoc.SelectSingleNode("//SetasDefault") != null && p_objDoc.SelectSingleNode("//SetasDefault").InnerText!=string.Empty)//skhare7 JIRA 340
                    { //pmittal5 MITS 13187 10/16/08 - Replacing 2 single quotes with double quotes
                    //sXml = "<setting><SearchDefault Cat_" + sCatId + "=''" + sViewId + "''" + "></SearchDefault></setting>"; 
                    sXml = "<setting><SearchDefault Cat_" + sCatId + "=\"" + sViewId + "\"" + "></SearchDefault></setting>";
                    //sSql = sSql = "INSERT INTO USER_PREF_XML(USER_ID,PREF_XML) VALUES(" + p_iUserId + ",'" + sXml + "')";
                    DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString);
                    objConn.Open();
                    objCmd = objConn.CreateCommand();
                    objParam = objCmd.CreateParameter();
                    objParam.Direction = ParameterDirection.Input;
                    objParam.Value = sXml;
                    objParam.ParameterName = "XML";
                    objParam.SourceColumn = "PREF_XML";
                    objCmd.Parameters.Add(objParam);

                    sSql = "INSERT INTO USER_PREF_XML(USER_ID,PREF_XML) VALUES(" + p_iUserId + ",~XML~)";
                    //objConn.Open();
                    //objConn.ExecuteNonQuery (sSql);
                    objCmd.CommandText = sSql;
                    objCmd.ExecuteNonQuery();
                    objConn.Close();
                    objConn.Dispose();
                    }
                     if (p_objDoc.SelectSingleNode("//UseFullEntitySearch") != null && (p_objDoc.SelectSingleNode("//UseFullEntitySearch").InnerText != string.Empty))//skhare7 JIRA 340
                        {
                            sFullEntitySearch = p_objDoc.SelectSingleNode("//UseFullEntitySearch").InnerText;
                            //JIRA 340 End

                            sXml = "<setting><UseFullEntitySearch value =\"" + sFullEntitySearch + "\"" + "></UseFullEntitySearch></setting>";//skhare7 JIRA 340
                        //sSql = sSql = "INSERT INTO USER_PREF_XML(USER_ID,PREF_XML) VALUES(" + p_iUserId + ",'" + sXml + "')";
                        DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString);
                        objConn.Open();
                        objCmd = objConn.CreateCommand();
                        objParam = objCmd.CreateParameter();
                        objParam.Direction = ParameterDirection.Input;
                        objParam.Value = sXml;
                        objParam.ParameterName = "XML";
                        objParam.SourceColumn = "PREF_XML";
                        objCmd.Parameters.Add(objParam);

                        sSql = "INSERT INTO USER_PREF_XML(USER_ID,PREF_XML) VALUES(" + p_iUserId + ",~XML~)";
                        //objConn.Open();
                        //objConn.ExecuteNonQuery (sSql);
                        objCmd.CommandText = sSql;
                        objCmd.ExecuteNonQuery();
                        objConn.Close();
                        objConn.Dispose();
                    
                    }
                }
                if (!objReader.IsClosed)
                    objReader.Close();
                objReader.Dispose();
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.SetUserPrefNode.GenericError", m_iClientId), p_objException);
            }
        }
        #region Claim search

        /// <summary>
        /// Splitting by string : Anjaneya : MITS 11695
        /// </summary>
        /// <param name="input">Input String</param>
        /// <param name="split">Split pattern</param>
        /// <returns></returns>
        private string[] SplitUsingString(string input, string split)
        {
            int offset = 0;
            int index = 0;
            int[] offsets = new int[input.Length + 1];

            while (index < input.Length)
            {
                int indexOf = input.IndexOf(split, index);
                if (indexOf != -1)
                {
                    offsets[offset++] = indexOf;
                    index = (indexOf + split.Length);
                }
                else
                {
                    index = input.Length;
                }
            }

            string[] final = new string[offset + 1];
            if (offset == 0)
            {
                final[0] = input;
            }
            else
            {
                offset--;
                final[0] = input.Substring(0, offsets[0]);
                for (int i = 0; i < offset; i++)
                {
                    final[i + 1] = input.Substring(offsets[i] + split.Length, offsets[i + 1] - offsets[i] - split.Length);
                }
                final[offset + 1] = input.Substring(offsets[offset] + split.Length);
            }
            return final;
        }

        /// <summary>
        /// This function generates the search data related to Claims module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sClaimData">Claim data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        //pmittal5 Mits 18356 11/13/09 - Added parameter "p_arrlstTextSuppFields" containing Free Text type Supp fields 
        //public void DoClaimSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, 
        //    string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, 
        //    string p_sCriteria, string p_sJustFieldNames,ref string p_sRetSQL, 
        //    ref string p_sClaimData, ref DbConnection p_objConn, string p_sOrgLevel, int p_iPageSize, int p_iPageNumber)
        public void DoClaimSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables,
            string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields,
            string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL,
            ref string p_sClaimData, ref DbConnection p_objConn, string p_sOrgLevel, int p_iPageSize, int p_iPageNumber, ArrayList p_arrlstTextSuppFields, string p_sUserName)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            string sMultiCodeTableAlias = "";
            string sTemp = "";
            int iIdx = 0;
            int iIdxInsured = 0;

            // npadhy Start MITS 18653 Added the case for Reinsurer and modified the logic for Insurer
            int iIdxInsurer = 0;
            int iIdxReinsurer = 0;
            // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
            bool bInsurerIncluded = false;
            bool bReinsurerIncluded = false;
            // npadhy End 20622 MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
            // npadhy End MITS 18653 Added the case for Reinsurer and modified the logic for Insurer

            long lFieldId = 0;
            //Added by Shivendu for MITS 9965---Partial
            int iIdx2 = 0;
            int iIdxPatient = 0; //Added by zalam 07/02/2008 Mits:-12801
            int iIndex = 0;
            //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
            bool bPersonInvIncluded = false;

            SysSettings objSettings = null;
            //aaggarwal29 : MITS 37120 start
            string[] sTEmpSplit;
            string sNewField;
            StringBuilder sFinalField;
            sFinalField = new StringBuilder();
            //aaggarwal29 : MITS 37120 end


            try
            {
                //Delete temp table, if any
                // JP 06.15.2006   Cleanup no matter what.    if (m_sDBType != Constants.DB_ORACLE) 
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupClaimSearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpClaimSearch");
                }

                CleanupClaimSearch("", ref p_objConn);
                this.m_sUserName = p_sUserName;

                //Build criteria query
                sFrom = "CLAIM";
                //START Change  MITS 10608
                //abisht MITS 10690
                //if (sEvalDistinct(p_sQueryFields) == " DISTINCT ")  //pmittal5 Mits 18356
                if (sEvalDistinct(p_sQueryFields, p_arrlstTextSuppFields) == " DISTINCT ")
                {
                    sSelect = "SELECT DISTINCT CLAIM.CLAIM_ID,CLAIM.LINE_OF_BUS_CODE,CLAIM.CLAIM_TYPE_CODE,CLAIM.CLAIM_STATUS_CODE, CLAIM.CATASTROPHE_ROW_ID "; //Amandeep Catastrophe
                }
                else
                {
                    sSelect = "SELECT CLAIM.CLAIM_ID,CLAIM.LINE_OF_BUS_CODE,CLAIM.CLAIM_TYPE_CODE,CLAIM.CLAIM_STATUS_CODE, CLAIM.CATASTROPHE_ROW_ID ";  //Amandeep Catastrophe
                }
                //END Change  MITS 10608
                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "EVENT") || Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_ENTITY"))
                {
                    sFrom = sFrom + ",EVENT";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.EVENT_ID = EVENT.EVENT_ID";
                }
				//rsharma220 MITS 33923 Start
                objSettings = new SysSettings(p_objConn.ConnectionString , m_iClientId);
                if (objSettings.MultiCovgPerClm == -1)
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + " (CLAIM.LINE_OF_BUS_CODE= 241 OR CLAIM.LINE_OF_BUS_CODE= 243)";
                }
                //rsharma220 MITS 33923 End
                //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380 Start
                if ((Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_SUPP") && !Utilities.ExistsInArray(p_arrlstQueryTables, "PI_SUPP")) ||
                    Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_ENTITY"))
                {
                    bPersonInvIncluded = true;
                }
                //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380 End

                //START MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                if (bPersonInvIncluded && sFrom.IndexOf("PERSON_INVOLVED") < 0)
                {
                    sFrom = sFrom + " LEFT OUTER JOIN PERSON_INVOLVED PIJOIN ON EVENT.EVENT_ID = PIJOIN.EVENT_ID";

                }
                //END MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380

                // atavaragiri mits 26342
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "EVENT_X_OSHA_SUPP"))
                {
                    sFrom = sFrom + ",EVENT_X_OSHA_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.EVENT_ID = EVENT_X_OSHA_SUPP.EVENT_ID";
                }
                // end:mits 26342

                //Start averma62 - MITS 28903
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PI_X_INJURY_SUPP") && Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM")
                    && !Utilities.ExistsInArray(p_arrlstQueryTables, "EVENT"))
                {
                    sFrom = sFrom + ",PI_X_INJURY_SUPP";
                    sFrom = sFrom + ",PERSON_INVOLVED";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PERSON_INVOLVED.PI_ROW_ID = PI_X_INJURY_SUPP.PI_ROW_ID AND CLAIM.EVENT_ID=PERSON_INVOLVED.EVENT_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PI_X_INJURY_SUPP") && Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM")
                     && Utilities.ExistsInArray(p_arrlstQueryTables, "EVENT"))
                {
                    sFrom = sFrom + ",PI_X_INJURY_SUPP";
                    if (p_arrlstDisplayTables.Count != 0) sFrom = sFrom + ",PERSON_INVOLVED";

                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PERSON_INVOLVED.PI_ROW_ID = PI_X_INJURY_SUPP.PI_ROW_ID AND CLAIM.EVENT_ID=PERSON_INVOLVED.EVENT_ID";
                }
                //End averma62 - MITS 28903

                //MGaba2:MITS 13205:09/04/2008:It should not be added if org hierarchy fields are supplemental
                //Commented the following code as following criteria has been added in "DoSearch" itself:start
                //// Abhishek  changes related to MITS 11341 start
                ////if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_HIERARCHY"))
                //{
                //    sFrom = sFrom + ",ORG_HIERARCHY";
                //    if (sWhere != "")
                //        sWhere = sWhere + " AND ";
                //    sWhere = sWhere + "EVENT.DEPT_EID = ORG_HIERARCHY.DEPARTMENT_EID";
                //}
                //// Abhishek  changes related to MITS 11341 end

                //Ankit:Uncommenting the below code and made changes
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_HIE"))
                {
                    //Ankit:Start changes for MITS 28164
                    //sFrom = sFrom + ",ORG_HIERARCHY ORG_HIE";
                    sFrom = sFrom + " INNER JOIN ORG_HIERARCHY ORG_HIE ON EVENT.DEPT_EID = ORG_HIE.DEPARTMENT_EID ";
                    //Ankit:End changes
                }
                //Ankit:End changes
                //MGaba2:MITS 13205:09/04/2008:End
                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_ENTITY"))
                {
                    sFrom = sFrom + ",ENTITY ORG_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    if (p_sOrgLevel != "")
                    {
                        sFrom = sFrom + ", ORG_HIERARCHY";
                        sWhere = sWhere + "ORG_HIERARCHY." + p_sOrgLevel + " = ORG_ENTITY.ENTITY_ID"
                            + " AND ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID";
                    }
                    else
                        sWhere = sWhere + "EVENT.DEPT_EID = ORG_ENTITY.ENTITY_ID";
                }

                //MCO support
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "MCO_ENTITY"))
                {
                    sFrom = sFrom + ",ENTITY MCO_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.MCO_EID = MCO_ENTITY.ENTITY_ID";
                }

                //Comment related     csingh7 R6 Claim Comment enhancement
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "COMMENTS_TEXTCLAIM"))
                {
                    sFrom = sFrom + ",COMMENTS_TEXT COMMENTS_TEXTCLAIM";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";

                    iIndex = p_sCriteria.IndexOf("UPPER(LTRIM(RTRIM(COMMENTS_TEXTCLAIM");
                    if (iIndex == -1)
                        iIndex = p_sCriteria.IndexOf("LTRIM(RTRIM(COMMENTS_TEXTCLAIM");
                    if (iIndex == -1)
                        iIndex = p_sCriteria.IndexOf("COMMENTS_TEXTCLAIM");

                    if (iIndex != -1)
                    {
                        iIndex = iIndex - 1;    // To include the bracket '(' before COMMENTCLAIM
                        sTemp = p_sCriteria.Substring(iIndex);
                        p_sCriteria = p_sCriteria.Replace(sTemp, "");

                        iIndex = sTemp.IndexOf("AND");
                        if (iIndex != -1)
                        {
                            p_sCriteria = p_sCriteria + sTemp.Substring(iIndex + 3);
                            sTemp = sTemp.Substring(0, iIndex);
                        }
                        p_sCriteria = p_sCriteria.TrimEnd();
                        if (p_sCriteria.EndsWith("AND"))
                            p_sCriteria = p_sCriteria.Substring(0, p_sCriteria.Length - 3);
                    }
                    sWhere = sWhere + "(CLAIM.CLAIM_ID = COMMENTS_TEXTCLAIM.ATTACH_RECORDID AND COMMENTS_TEXTCLAIM.ATTACH_TABLE LIKE 'CLAIM' AND " + sTemp + ")";
                }
                //claimant & related
                //Added by Amitosh for R8 enhancement of Claim search using enhanced notes text

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_PRG_NOTE"))
                {
                    sFrom = sFrom + ",CLAIM_PRG_NOTE";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = CLAIM_PRG_NOTE.CLAIM_ID";
                    //Deb MITS 30959
                    if (p_sQueryFields.Contains("CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH"))
                    {
                        if (m_sDBType == Constants.DB_ORACLE)
                        {
                            p_sQueryFields = p_sQueryFields.Replace("CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH", "CAST(SUBSTR(NOTE_MEMO_CARETECH,0,4000) AS VARCHAR2(4000))");
                        }
                        else
                        {
                            p_sQueryFields = p_sQueryFields.Replace("CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH", "CAST(NOTE_MEMO_CARETECH AS VARCHAR(MAX))");
                        }
                    }
                    //Deb MITS 30959
                }

                //End Amitosh
                //Amandeep Catstrophe Enhancement --start
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CATASTROPHE"))
                {
                    sFrom = sFrom + ",CATASTROPHE";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CATASTROPHE_ROW_ID = CATASTROPHE.CATASTROPHE_ROW_ID";

                }
                //Amandeep Catstrophe Enhancement --end                
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIMANT") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIMANT_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIMANT_ATTORNEY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIMANT_SUPP")) // shruti for including claimant 
                // supplemental in claim search
                {
                    sFrom = sFrom + ",CLAIMANT";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIMANT_SUPP"))
                {
                    sFrom = sFrom + ",CLAIMANT_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIMANT_SUPP.CLAIMANT_ROW_ID = CLAIMANT.CLAIMANT_ROW_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIMANT_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY CLAIMANT_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIMANT.CLAIMANT_EID = CLAIMANT_ENTITY.ENTITY_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIMANT_ATTORNEY"))
                {
                    sFrom = sFrom + ", ENTITY CLAIMANT_ATTORNEY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIMANT.ATTORNEY_EID = CLAIMANT_ATTORNEY.ENTITY_ID";
                }
                //unit & related
                //asharma326 MITS 29225 merge to MITS 30518
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "UNIT_X_CLAIM") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "VEHICLE") ||
                     Utilities.ExistsInArray(p_arrlstQueryTables, "UNIT_X_CLAIM_SUPP"))
                     //(Utilities.ExistsInArray(p_arrlstQueryTables, "VEHICLE_SUPP")))
                {
                    sFrom = sFrom + ",UNIT_X_CLAIM";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = UNIT_X_CLAIM.CLAIM_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "UNIT_X_CLAIM_SUPP"))
                {

                    sFrom = sFrom + ",UNIT_X_CLAIM_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "UNIT_X_CLAIM_SUPP.UNIT_ROW_ID = UNIT_X_CLAIM.UNIT_ROW_ID";

                }
                //for Vehicle_Supp asharma326 MITS 30518 STARTS
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "VEHICLE_SUPP"))
                //{
                //    sFrom = sFrom + " ,VEHICLE_SUPP,VEHICLE";
                //    if (sWhere != "")
                //        sWhere = sWhere + " AND ";
                //    sWhere = sWhere + " UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID " +
                //        "AND VEHICLE.UNIT_ID = VEHICLE_SUPP.UNIT_ID ";
                //}
                //for Vehicle_Supp asharma326 MITS 30518 ENDS
                
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "VEHICLE"))
                {
                    sFrom = sFrom + ", VEHICLE";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "UNIT_X_CLAIM.UNIT_ID = VEHICLE.UNIT_ID";
                }
                //Property Unit 
                //Anu Tennyson for MITS 18230 STARTS 01/10/2010
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_X_PROPERTY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "PROPERTY_UNIT"))
                {
                    sFrom = sFrom + ",CLAIM_X_PROPERTY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.PROP_ROW_ID = CLAIM_X_PROPERTY.PROP_ROW_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PROPERTY_UNIT"))
                {
                    sFrom = sFrom + ", PROPERTY_UNIT";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_PROPERTY.PROPERTY_ID = PROPERTY_UNIT.PROPERTY_ID";
                }
                //Anu Tennyson for MITS 18230 ENDS 
                //litigation
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_X_LITIGATION") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "LIT_ATTORNEY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "EXPERT") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "EXPERT_ENTITY"))
                {
                    sFrom = sFrom + ",CLAIM_X_LITIGATION";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = CLAIM_X_LITIGATION.CLAIM_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "LIT_ATTORNEY"))
                {
                    sFrom = sFrom + ", ENTITY LIT_ATTORNEY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_LITIGATION.CO_ATTORNEY_EID = LIT_ATTORNEY.ENTITY_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "EXPERT") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "EXPERT_ENTITY"))
                {
                    sFrom = sFrom + ", EXPERT";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_LITIGATION.LITIGATION_ROW_ID = EXPERT.LITIGATION_ROW_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "EXPERT_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY EXPERT_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "EXPERT.EXPERT_EID = EXPERT_ENTITY.ENTITY_ID";
                }
                //asingh263 MITS 34597--start
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_X_ARB"))          
                {
                    sFrom = sFrom + ",CLAIM_X_ARB";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = CLAIM_X_ARB.CLAIM_ID";
                }
                //asingh263 MITS 34597--end
                //adjuster & related
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ADJUSTER_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "ADJUST_DATED_TEXT") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_ADJ_SUPP"))
                {
                    sFrom = sFrom + ",CLAIM_ADJUSTER";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ADJUSTER_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY ADJUSTER_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_ADJUSTER.ADJUSTER_EID = ADJUSTER_ENTITY.ENTITY_ID";
                    //akaur9 change the code for adjuster
                    if (IsMobileAdjusterClaimList || IsMobilityAdjusterClaimList)
                    {
                        sWhere = sWhere + " AND ";
                        sWhere = sWhere + "ADJUSTER_ENTITY.RM_USER_ID = " + m_lUserId;
                    }
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_ADJ_SUPP"))
                {
                    sFrom = sFrom + ",CLAIM_ADJ_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_ADJUSTER.ADJ_ROW_ID = CLAIM_ADJ_SUPP.ADJ_ROW_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ADJUST_DATED_TEXT"))
                {
                    sFrom = sFrom + ", ADJUST_DATED_TEXT";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_ADJUSTER.ADJ_ROW_ID = ADJUST_DATED_TEXT.ADJ_ROW_ID";
                }

                //Geeta 05/22/07 Starts: Code added to support Leave Mgmt Search 
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_LEAVE") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "LEAVE_RB_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_LV_X_DETAIL") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_LV_SUPP"))
                {
                    sFrom = sFrom + ", CLAIM_LEAVE";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = CLAIM_LEAVE.CLAIM_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "LEAVE_RB_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY LEAVE_RB_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_LEAVE.REVIEWER_EID = LEAVE_RB_ENTITY.ENTITY_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_LV_X_DETAIL"))
                {
                    sFrom = sFrom + ", CLAIM_LV_X_DETAIL";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_LEAVE.LEAVE_ROW_ID = CLAIM_LV_X_DETAIL.LEAVE_ROW_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_LV_SUPP"))
                {
                    sFrom = sFrom + ",CLAIM_LV_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_LEAVE.LEAVE_ROW_ID = CLAIM_LV_SUPP.LEAVE_ROW_ID";
                }
                //Geeta 05/22/07 Ends

                //defendant & related
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "DEFENDANT") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "DEFEND_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "DEFEND_ATTORNEY"))
                {
                    sFrom = sFrom + ",DEFENDANT";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = DEFENDANT.CLAIM_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "DEFEND_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY DEFEND_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "DEFENDANT.DEFENDANT_EID = DEFEND_ENTITY.ENTITY_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "DEFEND_ATTORNEY"))
                {
                    sFrom = sFrom + ", ENTITY DEFEND_ATTORNEY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "DEFENDANT.ATTORNEY_EID = DEFEND_ATTORNEY.ENTITY_ID";
                }

                //funds & related
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "FUNDS") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "PAYEE_ENTITY"))
                {
                    sFrom = sFrom + ",FUNDS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = FUNDS.CLAIM_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PAYEE_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY PAYEE_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "FUNDS.PAYEE_EID = PAYEE_ENTITY.ENTITY_ID";
                }

                //claim supplemental
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_SUPP"))
                {
                    sFrom = sFrom + ",CLAIM_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = CLAIM_SUPP.CLAIM_ID";
                }
                //objSettings = new SysSettings(p_objConn.ConnectionString);
                int iUseAdvClaim = objSettings.MultiCovgPerClm;
                // npadhy Start MITS 18653 Added the case for Reinsurer and modified the logic for Insurer
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "INSURER_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INSURER") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "REINSURER_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INS_REINS") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_CVG_TYPE") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INSURED") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "INSURED_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_MCO") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "MCO_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_SUPP"))
                // npadhy End MITS 18653 Added the case for Reinsurer and modified the logic for Insurer
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    if (iUseAdvClaim != -1)
                    {
                        sFrom = sFrom + ", POLICY";
                        sWhere = sWhere + "CLAIM.PRIMARY_POLICY_ID = POLICY.POLICY_ID";
                    }
                    else
                    {
                        sFrom = sFrom + ", POLICY,CLAIM_X_POLICY";
                        sWhere = sWhere + "CLAIM_X_POLICY.POLICY_ID = POLICY.POLICY_ID AND CLAIM_X_POLICY.CLAIM_ID = CLAIM.CLAIM_ID";
                    }
                }

                // npadhy Start MITS 18653 Added the case for Reinsurer and modified the logic for Insurer
                //insurer
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INSURER") || Utilities.ExistsInArray(p_arrlstQueryTables, "INSURER_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY INSURER_ENTITY, POLICY_X_INSURER";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID AND POLICY_X_INSURER.INSURER_CODE = INSURER_ENTITY.ENTITY_ID";

                    bInsurerIncluded = true;
                }
                else if (!bInsurerIncluded && (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_INSURER") || Utilities.ExistsInArray(p_arrlstDisplayTables, "INSURER_ENTITY")))
                {
                    if (!sFrom.Contains("POLICY"))
                    {
                        sFrom += " , POLICY";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "CLAIM.PRIMARY_POLICY_ID = POLICY.POLICY_ID ";
                    }
                    if (!sFrom.Contains("POLICY_X_INSURER"))
                    {
                        sFrom += " , POLICY_X_INSURER";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID ";
                    }
                    bInsurerIncluded = true;
                }

                // reinsurer
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INS_REINS") || Utilities.ExistsInArray(p_arrlstQueryTables, "REINSURER_ENTITY"))
                {

                    sFrom = sFrom + ", ENTITY REINSURER_ENTITY , POLICY_X_INS_REINS";
                    if (!sFrom.Contains("POLICY_X_INSURER"))
                        sFrom += ",POLICY_X_INSURER";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID AND POLICY_X_INSURER.IN_ROW_ID = POLICY_X_INS_REINS.POL_X_INS_ROW_ID AND POLICY_X_INS_REINS.REINSURER_EID = REINSURER_ENTITY.ENTITY_ID";
                    // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                    bReinsurerIncluded = true;
                }
                if (!bReinsurerIncluded && (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_INS_REINS") || Utilities.ExistsInArray(p_arrlstDisplayTables, "REINSURER_ENTITY")))
                {
                    if (!sFrom.Contains("POLICY"))
                    {
                        sFrom += " , POLICY";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "CLAIM.PRIMARY_POLICY_ID = POLICY.POLICY_ID ";
                    }
                    if (!sFrom.Contains("POLICY_X_INSURER"))
                    {
                        sFrom += " , POLICY_X_INSURER";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID ";
                    }
                    bReinsurerIncluded = true;
                }
                // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                // npadhy End MITS 18653 Added the case for Reinsurer and modified the logic for Insurer

                //coverages
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_CVG_TYPE"))
                {
                    sFrom = sFrom + ",POLICY_X_CVG_TYPE";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID";
                }

                //insured layers
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INSURED") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "INSURED_ENTITY"))
                {
                    sFrom = sFrom + ", POLICY_X_INSURED, ENTITY INSURED_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID AND POLICY_X_INSURED.INSURED_EID = INSURED_ENTITY.ENTITY_ID";
                    //aaggarwal29 : MITS 37120 changes start
                    string[] sCriteriaSplit = new string[3];
                    string[] sTextSplit = new string[2];
                    string sReplaceText = string.Empty;
                    int iSplitIndex, iLengthOfReplaceText, iIndexAND;
                    if (m_sDBType == Constants.DB_ORACLE)
                    {
                        sReplaceText = "UPPER(INSURED_ENTITY.FIRST_NAME||MIDDLE_NAME||LAST_NAME)";
                    }
                    else if (m_sDBType == Constants.DB_SQLSRVR)
                    {
                        sReplaceText = "INSURED_ENTITY.FIRST_NAME||MIDDLE_NAME||LAST_NAME";
                    }
                    string sTempTxt;
                    iSplitIndex = p_sCriteria.IndexOf(sReplaceText);
                    if (iSplitIndex != -1)
                    {
                        iLengthOfReplaceText = sReplaceText.Length;
                        if (iSplitIndex == 0) // only search criteria, or along with other
                        {
                            sCriteriaSplit[0] = "";//p_sCriteria.Substring(0, iSplitIndex); // first part of string
                        }
                        else
                        {
                            sCriteriaSplit[0] = p_sCriteria.Substring(0, iSplitIndex); // first part of string
                        }

                        //sCriteriaSplit[0] = p_sCriteria.Substring(0, iSplitIndex ); // first part of string
                        sTempTxt = p_sCriteria.Substring(iSplitIndex + iLengthOfReplaceText + 1);
                        iIndexAND = sTempTxt.IndexOf("AND");
                        if (iIndexAND != -1)
                        {
                            sCriteriaSplit[2] = sTempTxt.Substring(iIndexAND); // last part of string
                            sCriteriaSplit[1] = p_sCriteria.Substring(iSplitIndex, iLengthOfReplaceText + iIndexAND); //middle part of string
                        }
                        else
                        {
                            sCriteriaSplit[2] = "";
                            sCriteriaSplit[1] = p_sCriteria.Substring(iSplitIndex); //middle part of string
                            iIndexAND = 0;
                        }


                        sTextSplit[0] = sCriteriaSplit[1].Substring(0, iLengthOfReplaceText);


                        sTextSplit[1] = sCriteriaSplit[1].Substring(iLengthOfReplaceText);
                        if (m_sDBType == Constants.DB_SQLSRVR)
                            sCriteriaSplit[1] = " (INSURED_ENTITY.FIRST_NAME" + sTextSplit[1] + " OR " + "INSURED_ENTITY.MIDDLE_NAME" + sTextSplit[1] + " OR " + "INSURED_ENTITY.LAST_NAME" + sTextSplit[1] + ") ";
                        else if (m_sDBType == Constants.DB_ORACLE)
                            sCriteriaSplit[1] = " ( UPPER(INSURED_ENTITY.FIRST_NAME)" + sTextSplit[1] + "  OR " + " UPPER (INSURED_ENTITY.MIDDLE_NAME)" + sTextSplit[1] + " OR " + "UPPER(INSURED_ENTITY.LAST_NAME)" + sTextSplit[1] + ") ";
                        p_sCriteria = sCriteriaSplit[0] + sCriteriaSplit[1] + sCriteriaSplit[2];
                    }  
                        // p_sQueryFields
                        sTEmpSplit = p_sQueryFields.Split(',');
                        sNewField = string.Empty;
                        foreach (string str in sTEmpSplit)
                        {
                            string temp = null;
                            if (str.Contains("||"))
                            {

                                string[] splitarray = str.Split(new string[] { "||" }, StringSplitOptions.None);
                                for (int i = 0; i < splitarray.Length; i++)
                                {
                                    if (splitarray[i].Contains("|"))
                                    {
                                        temp = splitarray[i].Split('|')[1].ToString();
                                        break;
                                        //splitarray[i] = splitarray[i].Split('|')[0].ToString();

                                    }
                                    // splitarray[i] = splitarray[i].Replace(splitarray[i], " COALESCE(" + splitarray[i] + ",'') ");

                                }
                                if (m_sDBType == Constants.DB_SQLSRVR)
                                {
                                    sNewField = "COALESCE(INSURED_ENTITY.FIRST_NAME,'') +' '+ COALESCE(INSURED_ENTITY.MIDDLE_NAME,'') +' '+ COALESCE(INSURED_ENTITY.LAST_NAME ,'') " + " | " + temp;
                                }
                                else if (m_sDBType == Constants.DB_ORACLE)
                                {
                                    sNewField = "INSURED_ENTITY.FIRST_NAME || ' '|| INSURED_ENTITY.MIDDLE_NAME || ' '|| INSURED_ENTITY.LAST_NAME |" + temp;
                                }

                                if (string.IsNullOrEmpty(sFinalField.ToString()))
                                    sFinalField.Append(sNewField);
                                else
                                    sFinalField.Append("," + sNewField);

                            }
                            else
                            {
                                if (string.IsNullOrEmpty(sFinalField.ToString()))
                                    sFinalField.Append(str);
                                else
                                    sFinalField.Append("," + str);
                            }
                        }
                        p_sQueryFields = sFinalField.ToString();
                   // }


                    //aaggarwal29 : MITS 37120 end

                }

                //MCO support
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_MCO") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "MCO_ENTITY"))
                {
                    sFrom = sFrom + ", POLICY_X_MCO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_MCO.POLICY_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "MCO_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY MCO_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_X_MCO.MCO_EID = MCO_ENTITY.ENTITY_ID";
                }

                //supp. support
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_SUPP"))
                {
                    sFrom = sFrom + ",POLICY_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY.POLICY_ID = POLICY_SUPP.POLICY_ID";
                }

                // npadhy MITS 16508 Implemented the Search for Enhanced Policy from Claim
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "INSURER_ENTITY_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INSRD_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "INSURER_ENTITY_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "BROKER_ENTITY_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_CVG_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_EXP_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_RTNG_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_MCO_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "MCO_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_DCNT_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_DTIER_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_TRANS_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_TERM_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_ENH_SUPP") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "CVG_SUPP_ENH") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "EXP_SUPP_ENH"))
                {
                    sFrom = sFrom + ", POLICY_ENH, POLICY_X_TERM_ENH";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID AND POLICY_X_TERM_ENH.TERM_ID = (SELECT MAX(TERM_ID) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = POLICY_ENH.POLICY_ID)";
                }

                //insurer
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "INSURER_ENTITY_ENH"))
                {
                    sFrom = sFrom + ", ENTITY INSURER_ENTITY_ENH";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_ENH.INSURER_EID = INSURER_ENTITY_ENH.ENTITY_ID";
                }

                //insured layers
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INSRD_ENH") || Utilities.ExistsInArray(p_arrlstQueryTables, "INSURED_ENTITY_ENH"))
                {
                    sFrom = sFrom + ", POLICY_X_INSRD_ENH, ENTITY INSURED_ENTITY_ENH";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_ENH.POLICY_ID = POLICY_X_INSRD_ENH.POLICY_ID AND POLICY_X_INSRD_ENH.INSURED_EID = INSURED_ENTITY_ENH.ENTITY_ID";
                }

                //Broker
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "BROKER_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY BROKER_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_ENH.BROKER_EID = BROKER_ENTITY.ENTITY_ID";
                }


                //coverages
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_CVG_ENH"))
                {
                    sFrom = sFrom + ",POLICY_X_CVG_ENH";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_X_CVG_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_CVG_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID) AND"
                        + " POLICY_ENH.POLICY_ID = POLICY_X_CVG_ENH.POLICY_ID";
                }

                //Rating
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_RTNG_ENH"))
                {
                    sFrom = sFrom + ",POLICY_X_RTNG_ENH";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_X_RTNG_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_RTNG_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID) AND"
                        + " POLICY_ENH.POLICY_ID = POLICY_X_RTNG_ENH.POLICY_ID";
                }

                //Discount
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_DCNT_ENH"))
                {
                    sFrom = sFrom + ",POLICY_X_DCNT_ENH";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_X_DCNT_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_DCNT_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID) AND"
                        + " POLICY_ENH.POLICY_ID = POLICY_X_DCNT_ENH.POLICY_ID";
                }

                //Discount Tier
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_DTIER_ENH"))
                {
                    sFrom = sFrom + ",POLICY_X_DTIER_ENH";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_X_DTIER_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_DTIER_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID) AND";
                    sWhere = sWhere + " POLICY_ENH.POLICY_ID = POLICY_X_DTIER_ENH.POLICY_ID";
                }

                //Transaction Data
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_TRANS_ENH"))
                {
                    sFrom = sFrom + ",POLICY_X_TRANS_ENH";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_X_TRANS_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_TRANS_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID) AND"
                        + " POLICY_ENH.POLICY_ID = POLICY_X_TRANS_ENH.POLICY_ID";
                }

                //Term Data
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_TERM_ENH"))
                {
                    sFrom = sFrom + ",POLICY_X_TERM_ENH";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_X_TERM_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_TERM_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID) AND"
                        + " POLICY_ENH.POLICY_ID = POLICY_X_TERM_ENH.POLICY_ID";
                }

                //MCO support
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_MCO_ENH") || Utilities.ExistsInArray(p_arrlstQueryTables, "MCO_ENTITY_ENH"))
                {
                    sFrom = sFrom + ", POLICY_X_MCO_ENH, ENTITY INSURED_ENTITY_ENH";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_ENH.POLICY_ID = POLICY_X_MCO_ENH.POLICY_ID AND POLICY_X_MCO_ENH.MCO_EID = MCO_ENTITY_ENH.ENTITY_ID";
                }

                //supp. support
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_ENH_SUPP"))
                {
                    sFrom = sFrom + ",POLICY_SUPP_ENH";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_ENH.POLICY_ID = POLICY_ENH_SUPP.POLICY_ID";
                }
                //Add by kuladeep for mits:23622 07/14/2011 Start
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "LITIGATION_SUPP"))
                {
                    sFrom = sFrom + ",LITIGATION_SUPP,CLAIM_X_LITIGATION";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "LITIGATION_SUPP.LITIGATION_ROW_ID = CLAIM_X_LITIGATION.LITIGATION_ROW_ID  AND CLAIM_X_LITIGATION.CLAIM_ID = CLAIM.CLAIM_ID";
                }
                //Add by kuladeep for mits:23622 07/14/2011 End
                //asingh263 MITS 34597--start
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ARBITRATION_SUPP"))
                {
                    sFrom = sFrom + ",ARBITRATION_SUPP,CLAIM_X_ARB";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "ARBITRATION_SUPP.ARBITRATION_ROW_ID = CLAIM_X_ARB.ARBITRATION_ROW_ID  AND CLAIM_X_ARB.CLAIM_ID = CLAIM.CLAIM_ID";
                }
                //asingh263 MITS 34597--end
                //BOB Enhancement Unit Stat : Start
                //UNIT STAT
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "UNIT_STAT"))
                {
                    sFrom = sFrom + ",UNIT_STAT";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = UNIT_STAT.CLAIM_ID";
                }
                //BOB Enhancement Unit Stat : End




                //supp. support for COVERAGES
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CVG_ENH_SUPP"))
                {
                    sFrom = sFrom + ",CVG_ENH_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_ENH.POLICY_ID = CVG_ENH_SUPP.POLICY_ID";
                }

                //supp. support for Exposure
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "EXP_ENH_SUPP"))
                {
                    sFrom = sFrom + ",EXP_ENH_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "POLICY_ENH.POLICY_ID = EXP_ENH_SUPP.POLICY_ID";
                }
                //skhare7 Subrogation Details For R8 Enhancements


                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_X_SUBRO") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "SUBRO_SPECIALIST_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "SUBRO_ADVPARTY_ENTITY") ||
                       Utilities.ExistsInArray(p_arrlstQueryTables, "SUBRO_COMPANY_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "SUBRO_ADJUSTER_ENTITY"))
                {
                    sFrom = sFrom + ",CLAIM_X_SUBRO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = CLAIM_X_SUBRO.CLAIM_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "SUBRO_SPECIALIST_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY SUBRO_SPECIALIST_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_SUBRO.SUB_SPECIALIST_EID = SUBRO_SPECIALIST_ENTITY.ENTITY_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "SUBRO_ADVPARTY_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY SUBRO_ADVPARTY_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_SUBRO.SUB_ADVERSE_PARTY_EID = SUBRO_ADVPARTY_ENTITY.ENTITY_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "SUBRO_COMPANY_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY SUBRO_COMPANY_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_SUBRO.SUB_ADVERSE_INS_CO_EID = SUBRO_COMPANY_ENTITY.ENTITY_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "SUBRO_ADJUSTER_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY SUBRO_ADJUSTER_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_SUBRO.SUB_ADV_ADJUSTER_EID = SUBRO_ADJUSTER_ENTITY.ENTITY_ID";
                }
                ////supp 

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "SUBROGATION_SUPP"))
                {
                    sFrom = sFrom + ",SUBROGATION_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_SUBRO.SUBROGATION_ROW_ID = SUBROGATION_SUPP.SUBROGATION_ROW_ID";
                }
                //Liability Info



                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_X_LIABILITYLOSS") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "LIABILTY_PARTY_NAME") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "LIABILTY_OWNER_NAME")
                      )
                {
                    sFrom = sFrom + ",CLAIM_X_LIABILITYLOSS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = CLAIM_X_LIABILITYLOSS.CLAIM_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "LIABILTY_PARTY_NAME"))
                {
                    sFrom = sFrom + ", ENTITY LIABILTY_PARTY_NAME";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_LIABILITYLOSS.PARTY_AFFECTED = LIABILTY_PARTY_NAME.ENTITY_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "LIABILTY_OWNER_NAME"))
                {
                    sFrom = sFrom + ", ENTITY LIABILTY_OWNER_NAME";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_LIABILITYLOSS.POLICYHOLDER = LIABILTY_OWNER_NAME.ENTITY_ID";
                }

                ////supp 

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_X_LIABILITYLOSS_SUPP"))
                {
                    sFrom = sFrom + ",CLAIM_X_LIABILITYLOSS_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_LIABILITYLOSS_SUPP.ROW_ID = CLAIM_X_LIABILITYLOSS_SUPP.ROW_ID";
                }


                if (
                    Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_X_PROPERTYLOSS") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "PROPERTY_OWNER_NAME") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_X_PROPERTYINVOLVED") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_X_PROPERTYLOSS_SUPP") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "PROPERTY_PHOLDER_NAME"))
                {
                    sFrom = sFrom + ",CLAIM_X_PROPERTYLOSS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID";

                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PROPERTY_OWNER_NAME"))
                {
                    sFrom = sFrom + ", ENTITY PROPERTY_OWNER_NAME";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_PROPERTYLOSS.OWNER = PROPERTY_OWNER_NAME.ENTITY_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PROPERTY_PHOLDER_NAME"))
                {
                    sFrom = sFrom + ", ENTITY PROPERTY_PHOLDER_NAME";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_PROPERTYLOSS.POLICYHOLDER = PROPERTY_PHOLDER_NAME.ENTITY_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_X_PROPERTYLOSS_SUPP"))
                {
                    sFrom = sFrom + ", CLAIM_X_PROPERTYLOSS_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_X_PROPERTYLOSS.ROW_ID = CLAIM_X_PROPERTYLOSS_SUPP.ROW_ID";
                }
                // Added by Amitosh for R8 enhancement of Property Loss
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_X_PROPERTYINVOLVED"))
                {
                    sFrom = sFrom + ", CLAIM_X_PROPERTYINVOLVED ";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "  CLAIM_X_PROPERTYINVOLVED.ROW_ID = CLAIM_X_PROPERTYLOSS.ROW_ID";
                }
                //   end Amitosh
                //skhare7 Property loss 

                //salvage Info


                if (Utilities.ExistsInArray(p_arrlstQueryTables, "SALVAGE_UNIT") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "UNIT_X_CLAIM") ||
                    Utilities.ExistsInArray(p_arrlstQueryTables, "UNITSALVAGE_INPOSS_NAME") ||//mits 29452 asingh263
                    Utilities.ExistsInArray(p_arrlstQueryTables, "UNITSALVAGE_YARD_ADD") ||//mits 29452 asingh263
                    Utilities.ExistsInArray(p_arrlstQueryTables, "UNTSALVAGE_OWNER_NAME"))
                {
                    sFrom = sFrom + ",SALVAGE SALVAGE_UNIT";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + " CLAIM.CLAIM_ID = UNIT_X_CLAIM.CLAIM_ID AND";
                    sWhere = sWhere + " SALVAGE_UNIT.PARENT_ID = UNIT_X_CLAIM.UNIT_ROW_ID";
                    sWhere = sWhere + " AND SALVAGE_UNIT.PARENT_NAME = 'UnitXClaim'";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "UNTSALVAGE_OWNER_NAME"))
                {
                    sFrom = sFrom + ", ENTITY UNTSALVAGE_OWNER_NAME";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "SALVAGE.BUYER_EID = UNTSALVAGE_OWNER_NAME.ENTITY_ID";
                }
                //mits 29452 asingh263 starts
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "UNITSALVAGE_INPOSS_NAME"))
                {
                    sFrom = sFrom + ", ENTITY UNITSALVAGE_INPOSS_NAME";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "SALVAGE.POSSESSION_OF = UNITSALVAGE_INPOSS_NAME.ENTITY_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "UNITSALVAGE_YARD_ADD"))
                {
                    sFrom = sFrom + ", ENTITY_X_ADDRESSES UNITSALVAGE_YARD_ADD";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "SALVAGE.SALVAGE_YARD_ADDR_ID = UNITSALVAGE_YARD_ADD.ADDRESS_ID";
                }
                //mits 29452 asingh263 ends
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "SALVAGE") ||
                     Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_X_PROPERTYLOSS") ||
                  Utilities.ExistsInArray(p_arrlstQueryTables, "PROPSALVAGE_OWNER_NAME"))
                {
                    sFrom = sFrom + ",SALVAGE ";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + " CLAIM.CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID AND";
                    sWhere = sWhere + " SALVAGE.PARENT_ID = CLAIM_X_PROPERTYLOSS.ROW_ID";
                    sWhere = sWhere + " AND SALVAGE.PARENT_NAME = 'ClaimXPropertyLoss'";
                }
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PROPSALVAGE_OWNER_NAME"))
                {
                    sFrom = sFrom + ", ENTITY PROPSALVAGE_OWNER_NAME";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "SALVAGE.BUYER_EID = PROPSALVAGE_OWNER_NAME.ENTITY_ID";
                }
                //skhare7 End
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "CLAIM.CLAIM_ID");

                //plug in other tables which have event-related criteria (all event fields are applicable to 
                //claim as well)
                EventCriteriaTables(p_arrlstQueryTables, ref sFrom, ref sWhere, ref p_sCriteria);   // csingh7 R6 Claim Comment Enhancement

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    //Mjain8 Added MITS 7643:Field Length is 14 so, only 8 chars taken for query
                    if (p_sCriteria.IndexOf("CLAIM.DTTM_CLOSED") > -1)
                    {
                        if (m_sDBType == Constants.DB_ORACLE)
                        {
                            p_sCriteria = p_sCriteria.Replace("CLAIM.DTTM_CLOSED", "SUBSTR(CLAIM.DTTM_CLOSED,1,8)");
                        }
                        else
                        {
                            p_sCriteria = p_sCriteria.Replace("CLAIM.DTTM_CLOSED", "SUBSTRING(CLAIM.DTTM_CLOSED,1,8)");
                        }

                    }
                    //Mjain8 MITS 7643 end
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }

                //add on fields
                p_sQueryFields = p_sQueryFields.Replace("ADJUSTER_ENTITY.CURRENT_ADJ_FLAG", "CLAIM_ADJUSTER.CURRENT_ADJ_FLAG");
                p_sQueryFields = p_sQueryFields.Replace("ADJUSTER_ENTITY.ADJUSTER_TYPE", "CLAIM_ADJUSTER.ADJUSTER_TYPE");//JIRA RMA-10751 ajohari2

                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;

                // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                if (bReinsurerIncluded || bInsurerIncluded)
                {
                    sSelect += ",POLICY_X_INSURER.IN_ROW_ID";
                }
                // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                //START MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                if (bPersonInvIncluded)
                {
                    sSelect += ",PIJOIN.PI_ROW_ID";
                }
                //END MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380

                //need event id if we have event children
                sSelect = sSelect + ", CLAIM.EVENT_ID" //JUST THROW IN EVENT_ID ALWAYS
                    + ", CLAIM.PRIMARY_POLICY_ID"; //Throw in POLICY_ID as well
                //				sSelect = sSelect + ", CLAIMANT.CLAIMANT_EID";

                //run criteria query into temp table
                sSQL = sSelect + " [INTO CLAIM_TEMP] FROM " + sFrom + " ";
                sWhere = sWhere.Replace("ADJUSTER_ENTITY.CURRENT_ADJ_FLAG", "CLAIM_ADJUSTER.CURRENT_ADJ_FLAG");
                sWhere = sWhere.Replace("ADJUSTER_ENTITY.ADJUSTER_TYPE", "CLAIM_ADJUSTER.ADJUSTER_TYPE");//JIRA RMA-10751 ajohari2

                // Mihika 06-Feb-2006: Only those claims where visible for which a claimant was present.
                // Also a claim was listed as many times as the number of its claimants.
                // Removed the check to mimic the RM World behavior, which lists the claim only once,
                // irrespective of the fact whether a claimant is present or not.
                //				if(sWhere != "")
                //					sWhere = sWhere + " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID ";
                //				else
                //					sWhere = sWhere + " CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID ";

                //akaur9 07/11/2011 Mobile Adjuster
                string sSelectDiary = string.Empty;
                string sSQLDiary = string.Empty;
                string sWhereDiary = string.Empty;
                string sUser = string.Empty;
                string sSQLAttachRecord = string.Empty;

                if (IsMobileAdjusterClaimList || IsMobilityAdjusterClaimList)
                {
                    string sFrom1 = sFrom + ", [DIARY_TEMP]";
                    string sSQL1 = sSelect + " [INTO CLAIM_TEMP] FROM " + sFrom1 + " ";
                    //sWhere = "(" + sWhere + " AND Claim.Claim_id in (Select DISTINCT ATTACH_RECORDID from WPA_DIARY_ENTRY Where ATTACH_TABLE = 'CLAIM' and ASSIGNED_USER = '" + p_sUserName + "')) or (" + sWhere + " AND CLAIM.CLAIM_ID not in (Select DISTINCT ATTACH_RECORDID from WPA_DIARY_ENTRY Where ATTACH_TABLE = 'CLAIM' and ASSIGNED_USER = '" + p_sUserName + "') )";
                    sWhereDiary = sWhere;
                    sSelectDiary = sSQL1;
                    if (sWhereDiary.Contains("AND CLAIM.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID"))
                    {
                        sWhereDiary = sWhereDiary.Replace("AND CLAIM.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID", "");
                    }
                    if (sWhereDiary.Contains("AND CLAIM_ADJUSTER.ADJUSTER_EID = ADJUSTER_ENTITY.ENTITY_ID"))
                    {
                        sWhereDiary = sWhereDiary.Replace("AND CLAIM_ADJUSTER.ADJUSTER_EID = ADJUSTER_ENTITY.ENTITY_ID", "");
                    }
                    if (sWhereDiary.Contains("AND ADJUSTER_ENTITY.RM_USER_ID = " + m_lUserId + ""))
                    {
                        sUser = "AND ADJUSTER_ENTITY.RM_USER_ID = " + m_lUserId + "";
                        sWhereDiary = sWhereDiary.Replace(sUser, "");
                    }
                    sSQLAttachRecord = "SELECT DISTINCT ATTACH_RECORDID [INTO DIARY_TEMP] FROM WPA_DIARY_ENTRY Where ATTACH_TABLE = 'CLAIM' and ASSIGNED_USER = '" + p_sUserName + "'";
                    ExecSearchSQL(sSQLAttachRecord, ref p_objConn);

                    sWhereDiary = sWhereDiary + " AND CLAIM.CLAIM_ID = ATTACH_RECORDID";

                    sSQL = sSQL.Replace("[INTO CLAIM_TEMP]", "");
                    sSQL1 = sSQL1.Replace("[INTO CLAIM_TEMP]", "");
                    if (IsMobilityAdjusterClaimList)
                    {
                        sSQL = sSQL.Replace("EVENT.EVENT_DESCRIPTION", "CAST(EVENT.EVENT_DESCRIPTION AS varchar(max))");
                        sSQL1 = sSQL1.Replace("EVENT.EVENT_DESCRIPTION", "CAST(EVENT.EVENT_DESCRIPTION AS varchar(max))");
                    }   
                    sSQLDiary = sSQL1 + " WHERE " + sWhereDiary;

                    sSQL = sSQL + " WHERE " + sWhere;

                    sSQL = sSQL + " UNION " + sSQLDiary;

                    //sSQL = sSelectDiary + sSQL + " )a";
                    sSQL = "Select * [INTO CLAIM_TEMP] from ( " + sSQL + " )a";

                }
                else
                {
                    if (sWhere != "")
                        sSQL = sSQL + " WHERE " + sWhere;
                }
                ExecSearchSQL(sSQL, ref p_objConn);

                //index CLAIM_ID for quick processing in display only fields
                string sIndex = PrepareTempSQL("CREATE INDEX CLTEMP_" + m_lUserId + " ON [CLAIM_TEMP](CLAIM_ID)");
                p_objConn.ExecuteNonQuery(sIndex);

                //Resolve display only fields (need temp tables & outer joins)
                sFrom = "[CLAIM_TEMP]";
                sWhere = "";

                //MCO support
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "MCO_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "MCO_ENTITY");
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[MCO_TEMP]", "CLAIM_ID");

                    //Add temp table to final outer joined query
                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString())
                        + " [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString()
                        + " [INTO MCO_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIM,ENTITY MCO_ENTITY"
                        + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM.CLAIM_ID AND "
                        + " CLAIM.MCO_EID = MCO_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX MCOTMP_" + m_lUserId +
                                                " ON [MCO_TEMP](CLAIM_ID)"));

                }

                //claimant & related
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIMANT"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIMANT");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIMANT_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    //MGaba2:MITS 12410-05/30/2008-Modified the following statement 
                    //sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + "CLAIMANT.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + 
                    //		" [INTO CLAIMANT_TEMP]";
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + "CLAIMANT.CLAIM_ID,CLAIMANT.CLAIMANT_EID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO CLAIMANT_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIMANT"
                        + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIMANT.CLAIM_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLMNTTMP_" + m_lUserId +
                                                " ON [CLAIMANT_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIMANT_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIMANT_ENTITY");
                    //Add temp table to final joined query
                    //					sFrom = sFrom + ",[CLMNT_ENT_TEMP] ";
                    //					if (sWhere != "") 
                    //						sWhere = sWhere + " AND ";
                    //					sWhere = sWhere + " [CLAIM_TEMP].CLAIM_ID=[CLMNT_ENT_TEMP].CLAIM_ID ";
                    //					sWhere = sWhere + " AND [CLAIM_TEMP].CLAIMANT_EID=[CLMNT_ENT_TEMP].CLAIMANT_EID ";

                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLMNT_ENT_TEMP]", "CLAIM_ID");
                    //MGaba2:MITS 12410-05/30/2008-Start
                    //fields from table claimant like primary claimant flag was coming yes as well as no
                    //for all Claimants associated with a claim.
                    if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIMANT"))
                    {
                        //MGaba2:MITS 12410:07/29/2008: Added Code for Oracle
                        if (m_sDBType == Constants.DB_ORACLE)
                        {
                            sWhere = sWhere + " AND [CLMNT_ENT_TEMP].CLAIMANT_EID = [CLAIMANT_TEMP].CLAIMANT_EID";
                        }
                        else
                        {
                            sFrom = sFrom.Substring(0, sFrom.Length - 1);
                            sFrom = sFrom + " AND [CLAIMANT_TEMP].CLAIMANT_EID = [CLMNT_ENT_TEMP].CLAIMANT_EID"
                                + ")";
                        }
                    }
                    //MGaba2:MITS 12410-05/30/2008-End
                    //Dump results into temp table
                    //MGaba2:MITS 12410-05/30/2008-Modified the following statement 
                    //sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIMANT.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + 
                    //		" [INTO CLMNT_ENT_TEMP]";
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIMANT.CLAIM_ID,CLAIMANT.CLAIMANT_EID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO CLMNT_ENT_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIMANT,ENTITY CLAIMANT_ENTITY WHERE [CLAIM_TEMP].CLAIM_ID = CLAIMANT.CLAIM_ID"
                        + " AND CLAIMANT.CLAIMANT_EID = CLAIMANT_ENTITY.ENTITY_ID";

                    // Anjaneya : MITS 11695
                    if (!string.IsNullOrEmpty(p_sCriteria))
                    {
                        string[] sTempWhere = SplitUsingString(p_sCriteria, "AND");
                        for (int i = 0; i < sTempWhere.Length; i++)
                        {
                            if (!sTempWhere[i].Contains("CLAIM.") && sTempWhere[i].Contains("CLAIMANT."))
                            {
                                sSQL = sSQL + " AND " + sTempWhere[i].ToString();
                            }
                        }

                    }

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLETEMP_" + m_lUserId +
                                                                " ON [CLMNT_ENT_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "COMMENTS_TEXTCLAIM")) //csingh7 R6 Claim Comment Enhancement
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "COMMENTS_TEXTCLAIM");
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[COMMENTS_TEXTCLAIM_TEMP]", "CLAIM_ID", "ATTACH_RECORDID");
                    //start - averma62 MITS 31643
                    string strTemp = p_arrlstDisplayFields[iIdx].ToString();
                    if (strTemp.ToUpper().Contains("COMMENTS_TEXTCLAIM"))
                    {
                        if (m_sDBType == Constants.DB_ORACLE)
                        {
                            //srajindersin MITS 36483 5/28/2014
                            strTemp = strTemp.Replace("COMMENTS_TEXTCLAIM.COMMENTS", "dbms_lob.substr( COMMENTS_TEXTCLAIM.COMMENTS, 4000, 1 )");
                        }
                        else
                        {
                            strTemp = strTemp.Replace("COMMENTS_TEXTCLAIM.COMMENTS", "CAST(COMMENTS_TEXTCLAIM.COMMENTS AS VARCHAR(MAX))");
                        }
                    }
                    // sSQL = "SELECT COMMENTS_TEXTCLAIM.ATTACH_RECORDID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO COMMENTS_TEXTCLAIM_TEMP] FROM [CLAIM_TEMP],COMMENTS_TEXT COMMENTS_TEXTCLAIM  WHERE [CLAIM_TEMP].CLAIM_ID = COMMENTS_TEXTCLAIM.ATTACH_RECORDID AND COMMENTS_TEXTCLAIM.ATTACH_TABLE = 'CLAIM'";
                    sSQL = "SELECT COMMENTS_TEXTCLAIM.ATTACH_RECORDID," + strTemp + " [INTO COMMENTS_TEXTCLAIM_TEMP] FROM [CLAIM_TEMP],COMMENTS_TEXT COMMENTS_TEXTCLAIM  WHERE [CLAIM_TEMP].CLAIM_ID = COMMENTS_TEXTCLAIM.ATTACH_RECORDID AND COMMENTS_TEXTCLAIM.ATTACH_TABLE = 'CLAIM'";
                    //end - averma62 MITS 31643

                    ExecSearchSQL(sSQL, ref p_objConn);
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "COMMENTS_TEXTEVENT")) //csingh7 R6 Claim Comment Enhancement
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "COMMENTS_TEXTEVENT");
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[COMMENTS_TEXTEVENT_TEMP]", "EVENT_ID", "ATTACH_RECORDID");
                    sSQL = "SELECT COMMENTS_TEXTEVENT.ATTACH_RECORDID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO COMMENTS_TEXTEVENT_TEMP] FROM [CLAIM_TEMP],COMMENTS_TEXT COMMENTS_TEXTEVENT  WHERE [CLAIM_TEMP].EVENT_ID = COMMENTS_TEXTEVENT.ATTACH_RECORDID AND COMMENTS_TEXTEVENT.ATTACH_TABLE = 'EVENT'";

                    ExecSearchSQL(sSQL, ref p_objConn);
                }

                //UNIT STAT
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "UNIT_STAT"))
                {
                    //sFrom = sFrom + ",UNIT_STAT";
                    //if (sWhere != "")
                    //    sWhere = sWhere + " AND ";
                    //sWhere = sWhere + "CLAIM.CLAIM_ID = UNIT_STAT.CLAIM_ID";

                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "UNIT_STAT");

                    ////Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[UNIT_STAT_TEMP]", "CLAIM_ID");

                    sSQL = "SELECT " + p_arrlstDisplayFields[iIdx] + " [INTO UNIT_STAT_TEMP]"
                        + " FROM [CLAIM_TEMP],UNIT_STAT WHERE [CLAIM_TEMP].CLAIM_ID=UNIT_STAT.CLAIM_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLAIMUNITSTATTEMP_" + m_lUserId + " ON [UNIT_STAT_TEMP](CLAIM_ID)"));
                }

                //akaur9 12/16/2011 MITS 26842--START
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_PRG_NOTE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_PRG_NOTE");
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIM_PRG_NOTE_TEMP]", "CLAIM_ID");
                    //Deb : MITS 30982
                    string strTemp = p_arrlstDisplayFields[iIdx].ToString();
                    if (strTemp.ToUpper().Contains("CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH"))
                    {
                        if (m_sDBType == Constants.DB_ORACLE)
                        {
                            strTemp = strTemp.Replace("CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH", "CAST(SUBSTR(NOTE_MEMO_CARETECH,0,4000) AS VARCHAR2(4000))");
                        }
                        else
                        {
                            strTemp = strTemp.Replace("CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH", "CAST(NOTE_MEMO_CARETECH AS VARCHAR(MAX))");
                        }
                    }
                    //Deb : MITS 30982
                    sSQL = "SELECT " + strTemp + ", CLAIM_PRG_NOTE.CLAIM_ID" + " [INTO CLAIM_PRG_NOTE_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIM_PRG_NOTE WHERE [CLAIM_TEMP].CLAIM_ID=CLAIM_PRG_NOTE.CLAIM_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLAIMPRGNOTETEMP_" + m_lUserId + " ON [CLAIM_PRG_NOTE_TEMP](CLAIM_ID)"));
                }
                //akaur9 12/16/2011 MITS 26842--END

                //Amandeep Catastrophe Enhancement MITS 28528--START
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CATASTROPHE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CATASTROPHE");
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CATASTROPHE_TEMP]", "CATASTROPHE_ROW_ID");

                    sSQL = "SELECT " + p_arrlstDisplayFields[iIdx] + ", CATASTROPHE.CATASTROPHE_ROW_ID" + " [INTO CATASTROPHE_TEMP]"
                        + " FROM [CLAIM_TEMP],CATASTROPHE WHERE [CLAIM_TEMP].CATASTROPHE_ROW_ID = CATASTROPHE.CATASTROPHE_ROW_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLAIMACATTEMP_" + m_lUserId + " ON [CATASTROPHE_TEMP](CATASTROPHE_ROW_ID)"));
                }
                //Amandeep Catastrophe Enhancement MITS 28528--END

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIMANT_ATTORNEY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIMANT_ATTORNEY");
                    //Add temp table to final joined query
                    //					sFrom = sFrom + ",[CLMNT_ATT_TEMP] ";
                    //					if (sWhere != "") 
                    //						sWhere = sWhere + " AND ";
                    //					sWhere = sWhere + " [CLAIM_TEMP].CLAIM_ID=[CLMNT_ATT_TEMP].CLAIM_ID ";
                    //					sWhere = sWhere + " AND [CLAIM_TEMP].CLAIMANT_EID=[CLMNT_ATT_TEMP].CLAIMANT_EID ";

                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLMNT_ATT_TEMP]", "CLAIM_ID");
                    //Dump results into temp table
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIMANT.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO CLMNT_ATT_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIMANT,ENTITY CLAIMANT_ATTORNEY WHERE [CLAIM_TEMP].CLAIM_ID = CLAIMANT.CLAIM_ID"
                        + " AND CLAIMANT.ATTORNEY_EID = CLAIMANT_ATTORNEY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLATEMP_" + m_lUserId +
                                                                " ON [CLMNT_ATT_TEMP](CLAIM_ID)"));
                }
                //Shruti for adding claimant supplemental fields in claim search
                //supplemental support
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIMANT_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIMANT_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIMANTSUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIMANT_SUPP.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CLAIMANTSUPP_TEMP]";
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString(), p_arrlstTextSuppFields) + " CLAIMANT_SUPP.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CLAIMANTSUPP_TEMP]" //Mits 18356
                        + " FROM [CLAIM_TEMP],CLAIMANT,CLAIMANT_SUPP"
                        + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIMANT.CLAIM_ID "
                        + " AND CLAIMANT.CLAIMANT_ROW_ID = CLAIMANT_SUPP.CLAIMANT_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);
                    //index for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLMNTSUPTEMP_" + m_lUserId + " ON [CLAIMANTSUPP_TEMP](CLAIM_ID)"));
                }
                //unit & related
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "UNIT_X_CLAIM"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "UNIT_X_CLAIM");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[UNIT_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " UNIT_X_CLAIM.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO UNIT_TEMP]"
                        + " FROM [CLAIM_TEMP],UNIT_X_CLAIM"
                        + " WHERE [CLAIM_TEMP].CLAIM_ID = UNIT_X_CLAIM.CLAIM_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX UNITTMP_" + m_lUserId +
                                                                " ON [UNIT_TEMP](CLAIM_ID)"));
                }
                //asharma326 for MITS 30518 Starts
                //if (Utilities.ExistsInArray(p_arrlstDisplayTables, "VEHICLE_SUPP"))
                //{
                //    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "VEHICLE_SUPP");

                //    //Add temp table to final outer joined query
                //    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[VEH_SUPP_TEMP]", "CLAIM_ID");

                //    //Dump results into temp table
                //    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                //    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " UNIT_X_CLAIM.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                //            " [INTO VEH_SUPP_TEMP]"
                //        + " FROM [CLAIM_TEMP],UNIT_X_CLAIM,VEHICLE,VEHICLE_SUPP WHERE [CLAIM_TEMP].CLAIM_ID = UNIT_X_CLAIM.CLAIM_ID"
                //        + " AND UNIT_X_CLAIM.UNIT_ID = VEHICLE.UNIT_ID"
                //        + " AND VEHICLE.UNIT_ID = VEHICLE_SUPP.UNIT_ID ";
                //    ExecSearchSQL(sSQL, ref p_objConn);

                //    //index CLAIM_ID for quick processing
                //    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX VEHSUPPTEMP_" + m_lUserId +
                //                                                " ON [VEH_SUPP_TEMP](CLAIM_ID)"));

                //}
                //if (Utilities.ExistsInArray(p_arrlstDisplayTables, "UNIT_X_CLAIM_SUPP"))
                //{
                //    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "UNIT_X_CLAIM_SUPP");

                //    //Add temp table to final outer joined query
                //    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[UNIT_SUPP_TEMP]", "CLAIM_ID");

                //    //Dump results into temp table
                //    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                //    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " UNIT_X_CLAIM.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                //            " [INTO UNIT_SUPP_TEMP]"
                //        + " FROM [CLAIM_TEMP],UNIT_X_CLAIM,UNIT_X_CLAIM_SUPP WHERE [CLAIM_TEMP].CLAIM_ID = UNIT_X_CLAIM.CLAIM_ID"
                //        + " AND UNIT_X_CLAIM.UNIT_ROW_ID = UNIT_X_CLAIM_SUPP.UNIT_ROW_ID";
                //    ExecSearchSQL(sSQL, ref p_objConn);

                //    //index CLAIM_ID for quick processing
                //    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX UNITSUPPTEMP_" + m_lUserId +
                //                                                " ON [UNIT_SUPP_TEMP](CLAIM_ID)"));

                //}
                //asharma326 for MITS 30518 Ends
                //unit Suppl
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "UNIT_X_CLAIM_SUPP"))
                {

                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "UNIT_X_CLAIM_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[UNIT_X_CLAIM_SUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString(), p_arrlstTextSuppFields) + " UNIT_X_CLAIM.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO UNIT_X_CLAIM_SUPP_TEMP]" 
                        + " FROM [CLAIM_TEMP],UNIT_X_CLAIM,UNIT_X_CLAIM_SUPP"
                        + " WHERE [CLAIM_TEMP].CLAIM_ID = UNIT_X_CLAIM.CLAIM_ID "
                        + " AND UNIT_X_CLAIM.UNIT_ROW_ID = UNIT_X_CLAIM_SUPP.UNIT_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX UNITXCLAIMSUPPTEMP_" + m_lUserId + " ON [UNIT_X_CLAIM_SUPP_TEMP](CLAIM_ID)"));
                    
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "VEHICLE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "VEHICLE");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[VEH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " UNIT_X_CLAIM.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO VEH_TEMP]"
                        + " FROM [CLAIM_TEMP],UNIT_X_CLAIM,VEHICLE WHERE [CLAIM_TEMP].CLAIM_ID = UNIT_X_CLAIM.CLAIM_ID"
                        + " AND UNIT_X_CLAIM.UNIT_ID = VEHICLE.UNIT_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX VEHTEMP_" + m_lUserId +
                                                                " ON [VEH_TEMP](CLAIM_ID)"));

                }
                //Property Claim Customize Search
                //Anu Tennyson for MITS 18230 STARTS 01/10/2010
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_X_PROPERTY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_X_PROPERTY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROPUNIT_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_PROPERTY.PROP_ROW_ID, [CLAIM_TEMP].CLAIM_ID, " + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO PROPUNIT_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIM_X_PROPERTY"
                        + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_PROPERTY.PROP_ROW_ID ";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PROPUNITTMP_" + m_lUserId +
                                                                " ON [PROPUNIT_TEMP](CLAIM_ID)"));
                }
                //Anu Tennyson for MITS 18230 ENDS 
                //Litigation & related
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_X_LITIGATION"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_X_LITIGATION");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LIT_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_LITIGATION.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO LIT_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIM_X_LITIGATION"
                        + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_LITIGATION.CLAIM_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LITTMP_" + m_lUserId +
                                                                " ON [LIT_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "LIT_ATTORNEY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "LIT_ATTORNEY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LIT_ATT_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_LITIGATION.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO LIT_ATT_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIM_X_LITIGATION,ENTITY LIT_ATTORNEY WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_LITIGATION.CLAIM_ID"
                        + " AND CLAIM_X_LITIGATION.CO_ATTORNEY_EID = LIT_ATTORNEY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LITATEMP_" + m_lUserId +
                                                                " ON [LIT_ATT_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EXPERT"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EXPERT");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[EXPERT_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_LITIGATION.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO EXPERT_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIM_X_LITIGATION,EXPERT WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_LITIGATION.CLAIM_ID"
                        + " AND CLAIM_X_LITIGATION.LITIGATION_ROW_ID = EXPERT.LITIGATION_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX EXPWTEMP_" + m_lUserId +
                                                                " ON [EXPERT_TEMP](CLAIM_ID)"));
                }

                // Mihika 16-Feb-2006 Corrected the first argument from Displayfields to DisplayTables
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EXPERT_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EXPERT_ENTITY");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[EXP_ENT_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_LITIGATION.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO EXP_ENT_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIM_X_LITIGATION,EXPERT,ENTITY EXPERT_ENTITY WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_LITIGATION.CLAIM_ID"
                        + " AND CLAIM_X_LITIGATION.LITIGATION_ROW_ID = EXPERT.LITIGATION_ROW_ID"
                        + " AND EXPERT.EXPERT_EID = EXPERT_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX EXPWETEMP_" + m_lUserId +
                                                                " ON [EXP_ENT_TEMP](CLAIM_ID)"));
                }

                //adjuster & related
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ADJUSTER_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ADJUSTER_ENTITY");

                    //Add temp table to final outer joined query
                    //akaur9 added a inner join for the mobile adjuster
                    if (IsMobileAdjusterClaimList || IsMobilityAdjusterClaimList)
                    {
                        JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[ADJ_TEMP]", "CLAIM_ID", 1);
                    }
                    else
                    {
                        JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[ADJ_TEMP]", "CLAIM_ID");
                    }

                    p_arrlstDisplayFields[iIdx] = p_arrlstDisplayFields[iIdx].ToString().Replace("ADJUSTER_ENTITY.CURRENT_ADJ_FLAG", "CLAIM_ADJUSTER.CURRENT_ADJ_FLAG");
                    p_arrlstDisplayFields[iIdx] = p_arrlstDisplayFields[iIdx].ToString().Replace("ADJUSTER_ENTITY.ADJUSTER_TYPE", "CLAIM_ADJUSTER.ADJUSTER_TYPE");//JIRA RMA-10751 ajohari2

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_ADJUSTER.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString()
                        + " [INTO ADJ_TEMP] FROM [CLAIM_TEMP],CLAIM_ADJUSTER,ENTITY ADJUSTER_ENTITY WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID"
                        + " AND CLAIM_ADJUSTER.ADJUSTER_EID = ADJUSTER_ENTITY.ENTITY_ID";
                    //akaur9 changed the code for mobile adjuster
                    if (IsMobileAdjusterClaimList || IsMobilityAdjusterClaimList)
                    {
                        sSQL = sSQL + " AND ADJUSTER_ENTITY.RM_USER_ID = " + m_lUserId;
                    }

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ADJTEMP_" + m_lUserId +
                                                                " ON [ADJ_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ADJUST_DATED_TEXT"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ADJUST_DATED_TEXT");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[ADJ_TEXT_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_ADJUSTER.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO ADJ_TEXT_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIM_ADJUSTER,ADJUST_DATED_TEXT WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID"
                        + " AND CLAIM_ADJUSTER.ADJ_ROW_ID = ADJUST_DATED_TEXT.ADJ_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ADJTTEMP_" + m_lUserId +
                                                                " ON [ADJ_TEXT_TEMP](CLAIM_ID)"));
                }

                //supplemental support
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_ADJ_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_ADJ_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLMADJSUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //sSQL = "SELECT " +  sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) +" CLAIM_ADJUSTER.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CLMADJSUPP_TEMP]";
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString(), p_arrlstTextSuppFields) + " CLAIM_ADJUSTER.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CLMADJSUPP_TEMP]" //Mits 18356
                        + " FROM [CLAIM_TEMP],CLAIM_ADJUSTER,CLAIM_ADJ_SUPP"
                        + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID "
                        + " AND CLAIM_ADJUSTER.ADJ_ROW_ID = CLAIM_ADJ_SUPP.ADJ_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);
                    //index for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLMADJSUPTEMP_" + m_lUserId + " ON [CLMADJSUPP_TEMP](CLAIM_ID)"));
                }
                //Geeta 05/22/07 Starts: Code added to support Leave Mgmt Search 
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_LEAVE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_LEAVE");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIM_LEAVE_TEMP]", "CLAIM_ID");

                    //Dump results into temp table                   
                    sSQL = "SELECT " + p_arrlstDisplayFields[iIdx].ToString() + ", CLAIM_LEAVE.CLAIM_ID" +
                            " [INTO CLAIM_LEAVE_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIM_LEAVE"
                        + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_LEAVE.CLAIM_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLMLVTMP_" + m_lUserId +
                                                " ON [CLAIM_LEAVE_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "LEAVE_RB_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "LEAVE_RB_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LEAVE_RB_TEMP]", "CLAIM_ID");

                    p_arrlstDisplayFields[iIdx] = p_arrlstDisplayFields[iIdx].ToString().Replace("LEAVE_RB_ENTITY.REVIEWER_EID", "CLAIM_LEAVE.REVIEWER_EID");

                    //Dump results into temp table                
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_LEAVE.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString()
                        + " [INTO LEAVE_RB_TEMP] FROM [CLAIM_TEMP],CLAIM_LEAVE,ENTITY LEAVE_RB_ENTITY WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_LEAVE.CLAIM_ID"
                        + " AND CLAIM_LEAVE.REVIEWER_EID = LEAVE_RB_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LVRBTEMP_" + m_lUserId +
                                                                " ON [LEAVE_RB_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_LV_X_DETAIL"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_LV_X_DETAIL");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIM_LV_X_DETAIL_TEMP]", "CLAIM_ID");

                    //Dump results into temp table               
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_LEAVE.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO CLAIM_LV_X_DETAIL_TEMP]"
                        + " FROM [CLAIM_TEMP],CLAIM_LEAVE,CLAIM_LV_X_DETAIL WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_LEAVE.CLAIM_ID"
                        + " AND CLAIM_LEAVE.LEAVE_ROW_ID = CLAIM_LV_X_DETAIL.LEAVE_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLMLVDETAILTEMP_" + m_lUserId +
                                                                " ON [CLAIM_LV_X_DETAIL_TEMP](CLAIM_ID)"));
                }
                //supplemental support
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_LV_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_LV_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIM_LV_SUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_LEAVE.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CLAIM_LV_SUPP_TEMP]";
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString(), p_arrlstTextSuppFields) + " CLAIM_LEAVE.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CLAIM_LV_SUPP_TEMP]" //Mits 18356
                        + " FROM [CLAIM_TEMP],CLAIM_LEAVE,CLAIM_LV_SUPP"
                        + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_LEAVE.CLAIM_ID "
                        + " AND CLAIM_LEAVE.LEAVE_ROW_ID = CLAIM_LV_SUPP.LEAVE_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);
                    //index for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLMLVSUPP_" + m_lUserId + " ON [CLAIM_LV_SUPP_TEMP](CLAIM_ID)"));
                }
                //defendant & related
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "DEFENDANT"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "DEFENDANT");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[DEF_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " DEFENDANT.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO DEF_TEMP]"
                        + " FROM [CLAIM_TEMP],DEFENDANT"
                        + " WHERE [CLAIM_TEMP].CLAIM_ID = DEFENDANT.CLAIM_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DEFTMP_" + m_lUserId +
                                                                " ON [DEF_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "DEFEND_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "DEFEND_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[DEF_ENT_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " DEFENDANT.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO DEF_ENT_TEMP]"
                        + " FROM [CLAIM_TEMP],DEFENDANT,ENTITY DEFEND_ENTITY WHERE [CLAIM_TEMP].CLAIM_ID = DEFENDANT.CLAIM_ID"
                        + " AND DEFENDANT.DEFENDANT_EID = DEFEND_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DEFETEMP_" + m_lUserId +
                                                                " ON [DEF_ENT_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "DEFEND_ATTORNEY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "DEFEND_ATTORNEY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[DEF_ATT_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " DEFENDANT.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO DEF_ATT_TEMP]"
                        + " FROM [CLAIM_TEMP],DEFENDANT,ENTITY DEFEND_ATTORNEY WHERE [CLAIM_TEMP].CLAIM_ID = DEFENDANT.CLAIM_ID"
                        + " AND DEFENDANT.ATTORNEY_EID = DEFEND_ATTORNEY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DEFATEMP_" + m_lUserId +
                                                                " ON [DEF_ATT_TEMP](CLAIM_ID)"));
                }

                //funds & related
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "FUNDS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "FUNDS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[FUNDS_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    // Deb: Multi Currency Search 
                    if (objSettings.UseMultiCurrency != 0 && (p_arrlstDisplayFields[iIdx].ToString().Contains("PMT_CURRENCY_AMOUNT") || p_arrlstDisplayFields[iIdx].ToString().Contains("CLAIM_CURRENCY_AMOUNT")))
                    {
                        string[] arPC = null;
                        string[] arCA = null;
                        string[] arFA = null;
                        ArrayList ar = new ArrayList();
                        sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " FUNDS.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " ,FUNDS.PMT_CURRENCY_CODE,FUNDS.CLAIM_CURRENCY_CODE [INTO FUNDS_TEMP]";
                        sSQL = sSQL + " FROM [CLAIM_TEMP],FUNDS";
                        sSQL = sSQL + " WHERE [CLAIM_TEMP].CLAIM_ID = FUNDS.CLAIM_ID";
                        ExecSearchSQL(sSQL, ref p_objConn);

                        if (p_arrlstDisplayFields[iIdx].ToString().Contains("PMT_CURRENCY_AMOUNT"))
                        {
                            int iDex = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT")).IndexOf(",");
                            if (iDex > -1)
                                arPC = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT")).Substring(0, p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT")).IndexOf(",")).Split('|');
                            else
                                arPC = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT")).Split('|');
                        }
                        if (p_arrlstDisplayFields[iIdx].ToString().Contains("CLAIM_CURRENCY_AMOUNT"))
                        {
                            int inDex = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT")).IndexOf(",");
                            if (inDex > -1)
                                arCA = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT")).Substring(0, p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT")).IndexOf(",")).Split('|');
                            else
                                arCA = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT")).Split('|');
                        }

                        if (p_arrlstDisplayFields[iIdx].ToString().Contains("FUNDS.AMOUNT"))
                        {
                            int inNDex = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("FUNDS.AMOUNT")).IndexOf(",");
                            if (inNDex > -1)
                                arFA = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("FUNDS.AMOUNT")).Substring(0, p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("AMOUNT")).IndexOf(",")).Split('|');
                            else
                                arFA = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("FUNDS.AMOUNT")).Split('|');
                        }
                        string sSQLTempNew = PrepareTempSQL("SELECT " + p_arrlstDisplayFields[iIdx].ToString().Replace("FUNDS.AMOUNT", "").Replace("FUNDS.CLAIM_CURRENCY_AMOUNT", "").Replace("FUNDS.PMT_CURRENCY_AMOUNT", "")
                                     + " ,[FUNDS_TEMP].PMT_CURRENCY_CODE,[FUNDS_TEMP].CLAIM_CURRENCY_CODE FROM [FUNDS_TEMP]");
                        if (arCA != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arCA[1].Trim(), arCA[1].Trim() + " " + arCA[0].Trim());
                            if (m_sDBType == Constants.DB_SQLSRVR)
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TEMP] ALTER COLUMN "
                                    + arCA[1].Trim() + " VARCHAR(30) "));
                            }
                            else
                            {
                                //Deb Some modification for Oracle
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("SELECT  * [INTO FUNDS_TEMP_NEW] FROM [FUNDS_TEMP]"));
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("TRUNCATE TABLE [FUNDS_TEMP]"));
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TEMP] MODIFY " + arCA[1].Trim() + " VARCHAR2(30) "));
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("INSERT INTO [FUNDS_TEMP] SELECT * FROM [FUNDS_TEMP_NEW]"));
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("DROP TABLE [FUNDS_TEMP_NEW]"));
                                //Deb Some modification for Oracle
                            }
                        }
                        if (arPC != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arPC[1].Trim(), arPC[1].Trim() + " " + arPC[0].Trim());
                            if (m_sDBType == Constants.DB_SQLSRVR)
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TEMP] ALTER COLUMN "
                                    + arPC[1].Trim() + " VARCHAR(30) "));
                            }
                            else
                            {
                                //Deb Some modification for Oracle
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("SELECT  * [INTO FUNDS_TEMP_NEW] FROM [FUNDS_TEMP]"));
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("TRUNCATE TABLE [FUNDS_TEMP]"));
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TEMP] MODIFY " + arPC[1].Trim() + " VARCHAR2(30) "));
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("INSERT INTO [FUNDS_TEMP] SELECT * FROM [FUNDS_TEMP_NEW]"));
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("DROP TABLE [FUNDS_TEMP_NEW]"));
                                //Deb Some modification for Oracle
                            }
                        }
                        if (arFA != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arFA[1].Trim(), arFA[1].Trim() + " " + "AMOUNT");
                        }
                        //Deb Need to be optimized in future
                        //if (arCA != null)
                        //{
                        //    p_objConn.ExecuteNonQuery(PrepareTempSQL("UPDATE [FUNDS_TEMP] SET "+ arCA[1].Trim() + "=" + arCA[1].Trim() + "+'_'+" + "[FUNDS_TEMP].CLAIM_CURRENCY_CODE"));
                        //}
                        //if (arPC != null)
                        //{
                        //    p_objConn.ExecuteNonQuery(PrepareTempSQL("UPDATE [FUNDS_TEMP] SET " + arPC[1].Trim() + "=" + arPC[1].Trim() + "+'_'+" + "[FUNDS_TEMP].PMT_CURRENCY_CODE"));
                        //}
                        //Deb Need to be optimized in future
                        using (DbReader rdr = p_objConn.ExecuteReader(PrepareTempSQL(sSQLTempNew)))
                        {
                            while (rdr.Read())
                            {

                                if (arCA != null)
                                {
                                    string sClaimAmt = Conversion.ConvertObjToStr(rdr.GetValue("CLAIM_CURRENCY_AMOUNT"));
                                    string sCurrCode = Conversion.ConvertObjToStr(rdr.GetValue("CLAIM_CURRENCY_CODE"));
                                    ar.Add(PrepareTempSQL("UPDATE [FUNDS_TEMP] SET " + arCA[1].Trim() + "='" + sClaimAmt + "_" + sCurrCode +
                                        "' WHERE " + arCA[1].Trim() + "= '" + sClaimAmt + "' AND [FUNDS_TEMP].CLAIM_CURRENCY_CODE=" + sCurrCode));
                                }
                                if (arPC != null)
                                {
                                    string sPmtAmt = Conversion.ConvertObjToStr(rdr.GetValue("PMT_CURRENCY_AMOUNT"));
                                    string sCurrCode = Conversion.ConvertObjToStr(rdr.GetValue("PMT_CURRENCY_CODE"));
                                    ar.Add(PrepareTempSQL("UPDATE [FUNDS_TEMP] SET " + arPC[1].Trim() + "='" + sPmtAmt + "_" + sCurrCode +
                                        "' WHERE " + arPC[1].Trim() + "= '" + sPmtAmt + "' AND [FUNDS_TEMP].PMT_CURRENCY_CODE=" + sCurrCode));
                                }
                            }
                        }

                        foreach (string sStr in ar)
                        {
                            p_objConn.ExecuteNonQuery(sStr);
                        }
                    }
                    // Deb : end
                    else
                    {
                        sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " FUNDS.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO FUNDS_TEMP]";
                        sSQL = sSQL + " FROM [CLAIM_TEMP],FUNDS";
                        sSQL = sSQL + " WHERE [CLAIM_TEMP].CLAIM_ID = FUNDS.CLAIM_ID";
                        ExecSearchSQL(sSQL, ref p_objConn);
                    }
                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX FUNDSTMP_" + m_lUserId +
                                                                " ON [FUNDS_TEMP](CLAIM_ID)"));

                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PAYEE_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PAYEE_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PAYEE_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " FUNDS.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO PAYEE_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],FUNDS,ENTITY PAYEE_ENTITY WHERE [CLAIM_TEMP].CLAIM_ID = FUNDS.CLAIM_ID";
                    sSQL = sSQL + " AND FUNDS.PAYEE_EID = PAYEE_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX FUNDSPTEMP_" + m_lUserId +
                                                " ON [PAYEE_TEMP](CLAIM_ID)"));
                }

                //claim supplemental
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIM_SUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    //sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_SUPP.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +   //Mits 18356
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString(), p_arrlstTextSuppFields) + " CLAIM_SUPP.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO CLAIM_SUPP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_SUPP";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_SUPP.CLAIM_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLSTMP_" + m_lUserId + " ON [CLAIM_SUPP_TEMP](CLAIM_ID)"));
                }

                //event
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PERSON_INVOLVED"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PERSON_INVOLVED");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PI_TEMP]", "EVENT_ID");


                    //averma62 MITS 28903
                    if (!Utilities.ExistsInArray(p_arrlstQueryTables, "PI_X_INJURY_SUPP") && !Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM"))
                        //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                        JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PI_TEMP]", "PI_ROW_ID", true);

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    //START MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " PERSON_INVOLVED.EVENT_ID,PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PI_TEMP]";

                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    if (bPersonInvIncluded)//Add by kuladeep for mits:26380
                    {
                        sSQL = sSQL + " AND [CLAIM_TEMP].PI_ROW_ID=PERSON_INVOLVED.PI_ROW_ID";
                    }
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //END MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PITEMP_" + m_lUserId + " ON [PI_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "RPTD_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "RPTD_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[RPTD_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO RPTD_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],EVENT,ENTITY RPTD_ENTITY WHERE [CLAIM_TEMP].EVENT_ID = EVENT.EVENT_ID";
                    sSQL = sSQL + " AND EVENT.RPTD_BY_EID = RPTD_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX RPETEMP_" + m_lUserId + " ON [RPTD_TEMP](EVENT_ID)"));
                }


                //START MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                //MITS 25626: smishra54, 08/08/2011 - added new 'Person involved supplementals' search fields for claim search 
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PI_SUPP");

                    //Add temp table to final outer joined query
                    //Change by kuladeep for mits:26380

                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PI_SUPP_TEMP]", "PI_ROW_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " PERSON_INVOLVED.EVENT_ID,PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PI_SUPP_TEMP]"
                        + " FROM [CLAIM_TEMP],PERSON_INVOLVED, PI_SUPP WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID"
                        + " AND PERSON_INVOLVED.PI_ROW_ID = PI_SUPP.PI_ROW_ID";
                    if (bPersonInvIncluded)//Add by kuladeep for mits:26380
                    {
                        sSQL = sSQL + " AND [CLAIM_TEMP].PI_ROW_ID=PERSON_INVOLVED.PI_ROW_ID";
                    }
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PISTEMP_" + m_lUserId + " ON [PI_SUPP_TEMP](EVENT_ID)"));
                }
                //End: smishra54
                //END MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380

                //support for event category code
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EVENT_CAT"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EVENT_CAT");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CAT_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CAT_TEMP]"
                        + " FROM [CLAIM_TEMP],EVENT,CODES EVENT_CAT WHERE [CLAIM_TEMP].EVENT_ID = EVENT.EVENT_ID"
                        + " AND EVENT.EVENT_IND_CODE = EVENT_CAT.CODE_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CATTEMP_" + m_lUserId + " ON [CAT_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PI_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PI_ENTITY_TEMP]", "EVENT_ID");

                    //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PI_ENTITY_TEMP]", "PI_ROW_ID", true);

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380

                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " PERSON_INVOLVED.EVENT_ID,PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PI_ENTITY_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,ENTITY PI_ENTITY WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PI_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PIETEMP_" + m_lUserId + " ON [PI_ENTITY_TEMP](EVENT_ID)"));
                }


                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EVENT_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EVENT_SUPP");

                    //Add temp table to final outer joined query
					//vsharma203 code merged for RMA-10032
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[EVENT_SUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:28518
                    //vsharma203 code merged for RMA-10032
					sSQL = "SELECT DISTINCT EVENT_SUPP.EVENT_ID,[CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO EVENT_SUPP_TEMP]";
                    
                    sSQL = sSQL + " FROM [CLAIM_TEMP],EVENT_SUPP WHERE [CLAIM_TEMP].EVENT_ID = EVENT_SUPP.EVENT_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX EVSTEMP_" + m_lUserId + " ON [EVENT_SUPP_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_X_BODY_PART"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PI_X_BODY_PART");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[BODY_PART_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO BODY_PART_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PI_X_BODY_PART WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_ROW_ID = PI_X_BODY_PART.PI_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX BPTEMP_" + m_lUserId + " ON [BODY_PART_TEMP](EVENT_ID)"));
                }
                //skhare7 R8
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_X_INJURY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PI_X_INJURY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[INJURY_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO INJURY_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PI_X_INJURY WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_ROW_ID = PI_X_INJURY.PI_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INJTEMP_" + m_lUserId + " ON [INJURY_TEMP](EVENT_ID)"));
                }
                //........................Start by Shivendu for MITS 9764.....................
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "STAFF_PRIVS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "STAFF_PRIVS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[STPRIV_TEMP]", "CLAIM_ID");

                    //Dump results into temp table				

                    sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO STPRIV_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,STAFF_PRIVS WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = STAFF_PRIVS.STAFF_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SPRIVTEMP_" + m_lUserId + " ON [STPRIV_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "STAFF_CERTS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "STAFF_CERTS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[STCERT_TEMP]", "CLAIM_ID");

                    //Dump results into temp table			
                    sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO STCERT_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,STAFF_CERTS WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = STAFF_CERTS.STAFF_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SCERTTEMP_" + m_lUserId + " ON [STCERT_TEMP](CLAIM_ID)"));
                }

                //.............................End by Shivendu for MITS 9764.............................


                //Start by Shivendu for MITS 9965---Partial
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT") || Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_ENTITY"))
                {



                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT");
                    iIdx2 = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_ENTITY");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PATIENT_TEMP]", "CLAIM_ID");


                    //Dump results into temp table
                    if (iIdx >= 0 && iIdx2 >= 0)
                        sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + "," + p_arrlstDisplayFields[iIdx2].ToString() + " [INTO PATIENT_TEMP]";
                    else if (iIdx >= 0 && iIdx2 < 0)
                        sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATIENT_TEMP]";
                    else if (iIdx < 0 && iIdx2 >= 0)
                        sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx2].ToString() + " [INTO PATIENT_TEMP]";
                    else if (iIdx < 0 && iIdx2 < 0)
                        sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID" + " [INTO PATIENT_TEMP]";

                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PATIENT,ENTITY | PATIENT_ENTITY WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT.PATIENT_ID AND PATIENT.PATIENT_EID = PATIENT_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PATTEMP_" + m_lUserId + " ON [PATIENT_TEMP](CLAIM_ID)"));



                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_DRG_CODES"))
                {

                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_DRG_CODES");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[DRG_TEMP]", "CLAIM_ID");

                    //Dump results into temp table			
                    sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO DRG_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PATIENT_DRG_CODES WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT_DRG_CODES.PATIENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DRGTEMP_" + m_lUserId + " ON [DRG_TEMP](CLAIM_ID)"));





                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_DIAGNOSIS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_DIAGNOSIS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PATDIAG_TEMP]", "CLAIM_ID");

                    //Dump results into temp table			
                    sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATDIAG_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PATIENT_DIAGNOSIS WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT_DIAGNOSIS.PATIENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PATDIATEMP_" + m_lUserId + " ON [PATDIAG_TEMP](CLAIM_ID)"));





                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_ACT_TAKEN"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_ACT_TAKEN");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PATACT_TEMP]", "CLAIM_ID");

                    //Dump results into temp table			
                    sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATACT_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PATIENT_ACT_TAKEN WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT_ACT_TAKEN.PATIENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PATACTTEMP_" + m_lUserId + " ON [PATACT_TEMP](CLAIM_ID)"));




                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_PROCEDURE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_PROCEDURE");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PATPROC_TEMP]", "CLAIM_ID");

                    //Dump results into temp table			
                    sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATPROC_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PATIENT_PROCEDURE WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT_PROCEDURE.PATIENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PPROCTEMP_" + m_lUserId + " ON [PATPROC_TEMP](CLAIM_ID)"));






                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_SUPP"))
                {

                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PATSUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table			
                    sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATSUPP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PATIENT_SUPP WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT_SUPP.PATIENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PSUPPTEMP_" + m_lUserId + " ON [PATSUPP_TEMP](CLAIM_ID)"));


                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PAT_PRI_PHYS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PAT_PRI_PHYS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PRI_PHYS_TEMP]", "CLAIM_ID");

                    //Dump results into temp table			
                    sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PRI_PHYS_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PATIENT,ENTITY|PAT_PRI_PHYS WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PATIENT.PRI_PHYSICIAN_EID = PAT_PRI_PHYS.ENTITY_ID AND PERSON_INVOLVED.PATIENT_ID = PATIENT.PATIENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PPRIPHYTEMP_" + m_lUserId + " ON [PRI_PHYS_TEMP](CLAIM_ID)"));


                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "MED_STAFF") || Utilities.ExistsInArray(p_arrlstDisplayTables, "MED_ENTITY"))
                {



                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "MED_STAFF");
                    iIdx2 = Utilities.IndexInArray(p_arrlstDisplayTables, "MED_ENTITY");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[MED_TEMP]", "CLAIM_ID");


                    //Dump results into temp table
                    if (iIdx >= 0 && iIdx2 >= 0)
                        sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + "," + p_arrlstDisplayFields[iIdx2].ToString() + " [INTO MED_TEMP]";
                    else if (iIdx >= 0 && iIdx2 < 0)
                        sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO MED_TEMP]";
                    else if (iIdx < 0 && iIdx2 >= 0)
                        sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx2].ToString() + " [INTO MED_TEMP]";
                    else if (iIdx < 0 && iIdx2 < 0)
                        sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID" + " [INTO MED_TEMP]";

                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,MED_STAFF,ENTITY | MED_ENTITY WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = MED_STAFF.STAFF_EID AND MED_STAFF.STAFF_EID = MED_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PATTEMP_" + m_lUserId + " ON [MED_TEMP](CLAIM_ID)"));



                }
                //End by Shivendu for MITS 9965---Partial
                //Scan for multi-values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "CLAIM_ID");
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]";
                        sSQL = sSQL + " FROM [CLAIM_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                        sSQL = sSQL + " WHERE [CLAIM_TEMP].CLAIM_ID = " + sMultiCodeTableAlias + ".RECORD_ID";
                        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](CLAIM_ID)"));
                    }
                }

                //zalam 07/02/2008 Mits:-12801 Start
                //Physician part was not handled

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYSICIAN") || Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYSICIAN");
                    iIdxPatient = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PHYS_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID,";

                    if (iIdx > -1)
                    {
                        sSQL = sSQL + p_arrlstDisplayFields[iIdx].ToString();
                        if (iIdxPatient > -1)
                            sSQL = sSQL + ",";
                    }
                    if (iIdxPatient > -1)
                        sSQL = sSQL + p_arrlstDisplayFields[iIdxPatient];

                    sSQL = sSQL + " [INTO PHYS_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PHYSICIAN,ENTITY | PHYS_ENTITY WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYSICIAN.PHYS_EID AND PHYSICIAN.PHYS_EID = PHYS_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PPHYSTEMP_" + m_lUserId + " ON [PHYS_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PHSUPP_TEMP]", "EVENT_ID");

                    //Dump results into temp table        
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PHSUPP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PHYS_SUPP WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_SUPP.PHYS_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PSPTEMP_" + m_lUserId + " ON [PHSUPP_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_SUB_SPECIALTY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_SUB_SPECIALTY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PSSPEC_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PSSPEC_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PHYS_SUB_SPECIALTY WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_SUB_SPECIALTY.PHYS_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PSPECTEMP_" + m_lUserId + " ON [PSSPEC_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_PRIVS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_PRIVS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PRIV_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PRIV_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PHYS_PRIVS WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_PRIVS.PHYS_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PRVTEMP_" + m_lUserId + " ON [PRIV_TEMP](EVENT_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_CERTS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_CERTS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CERT_TEMP]", "EVENT_ID");

                    //Dump results into temp table				
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CERT_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],PERSON_INVOLVED,PHYS_CERTS WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_CERTS.PHYS_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CRTTEMP_" + m_lUserId + " ON [CERT_TEMP](EVENT_ID)"));
                }
                //zalam 07/02/2008 Mits:-12801 End

                //POLICY related stuff
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POLICY_TEMP]", "CLAIM_ID");
                    if (iUseAdvClaim != -1)
                        JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POLICY_TEMP]", "PRIMARY_POLICY_ID", "POLICY_ID", true);

                    if (bInsurerIncluded || bReinsurerIncluded)
                    {
                        // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                        JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POLICY_TEMP]", "IN_ROW_ID", true);
                    }
                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [CLAIM_TEMP].CLAIM_ID, POLICY.POLICY_ID, " + p_arrlstDisplayFields[iIdx].ToString();

                    if (bInsurerIncluded || bReinsurerIncluded)
                    {
                        sSQL += ",POLICY_X_INSURER.IN_ROW_ID";
                    }

                    sSQL = sSQL + " [INTO POLICY_TEMP]";

                    if (iUseAdvClaim != -1)
                        sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY";
                    else
                        sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY,CLAIM_X_POLICY";

                    if (bInsurerIncluded || bReinsurerIncluded)
                    {
                        sSQL += ",POLICY_X_INSURER";
                    }

                    if (iUseAdvClaim != -1)
                        sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY.POLICY_ID";
                    else
                        sSQL = sSQL + " WHERE CLAIM_X_POLICY.POLICY_ID = POLICY.POLICY_ID AND CLAIM_X_POLICY.CLAIM_ID = [CLAIM_TEMP].CLAIM_ID";

                    if (bInsurerIncluded || bReinsurerIncluded)
                    {
                        sSQL = sSQL + " AND POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID ";
                    }

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX POLTEMP_" + m_lUserId + " ON [POLICY_TEMP](CLAIM_ID)"));
                }
                // npadhy If the Criteria is specified for Policy Table, then it will be part of p_arrlstQueryTables and not p_arrlstDisplayTables
                // So without this else the POLICY_TEMP table will not be created. But we need this Temp table, as we are making join
                // of this Table with Insurer and reinsurer
                //Added by Ashutosh MITS 23308
                //Problem arouses when in claim search user is selecting any field which is causing insurer_Entity as a display table i.e Insurer Abbreviation
                //Policy temp table earlier was not being created so adding  one more or  condition below 
                //else if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY") )
                else if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY") || Utilities.ExistsInArray(p_arrlstDisplayTables, "INSURER_ENTITY") || (Utilities.ExistsInArray(p_arrlstQueryTables, "INSURER_ENTITY")))
                {
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POLICY_TEMP]", "CLAIM_ID");

                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POLICY_TEMP]", "PRIMARY_POLICY_ID", "POLICY_ID", true);

                    if (bInsurerIncluded || bReinsurerIncluded)
                    {
                        // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                        JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POLICY_TEMP]", "IN_ROW_ID", true);
                    }
                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT [CLAIM_TEMP].CLAIM_ID, POLICY.POLICY_ID ";

                    if (bInsurerIncluded || bReinsurerIncluded)
                    {
                        sSQL += ",POLICY_X_INSURER.IN_ROW_ID";
                    }

                    sSQL = sSQL + " [INTO POLICY_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY";
                    if (bInsurerIncluded || bReinsurerIncluded)
                    {
                        sSQL += ",POLICY_X_INSURER";
                    }
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY.POLICY_ID";
                    if (bInsurerIncluded || bReinsurerIncluded)
                    {
                        sSQL = sSQL + " AND POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID ";
                    }
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX POLTEMP_" + m_lUserId + " ON [POLICY_TEMP](CLAIM_ID)"));
                }
                // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                // npadhy Start MITS 18653  Added the case for Reinsurer and modified the logic for Insurer
                //insurer
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "INSURER_ENTITY") || Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_INSURER"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "INSURER_ENTITY");
                    iIdxInsurer = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_INSURER");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[INS_ENT_TEMP]", "POLICY_ID");

                    // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                    if (bReinsurerIncluded)
                        JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[INS_ENT_TEMP]", "IN_ROW_ID", true);
                    // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                    //Dump results into temp table
                    sSQL = "SELECT POLICY.POLICY_ID, POLICY_X_INSURER.IN_ROW_ID";
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    if (iIdxInsurer > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxInsurer].ToString());
                    sSQL = sSQL + " [INTO INS_ENT_TEMP]"
                        + " FROM [POLICY_TEMP],POLICY,ENTITY INSURER_ENTITY, POLICY_X_INSURER"
                        + " WHERE [POLICY_TEMP].POLICY_ID = POLICY.POLICY_ID"
                        + " AND POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID"
                        + " AND POLICY_X_INSURER.INSURER_CODE = INSURER_ENTITY.ENTITY_ID";

                    // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                    if (bReinsurerIncluded)
                        sSQL = sSQL + " AND [POLICY_TEMP].IN_ROW_ID = POLICY_X_INSURER.IN_ROW_ID";
                    // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INSUTEMP_" + m_lUserId + " ON [INS_ENT_TEMP](POLICY_ID)"));
                }
                // npadhy If the Criteria is specified for Policy Table, then it will be part of p_arrlstQueryTables and not p_arrlstDisplayTables
                // So without this else the POLICY_TEMP table will not be created. But we need this Temp table, as we are making join
                // of this Table with Insurer and reinsurer
                else if (Utilities.ExistsInArray(p_arrlstQueryTables, "INSURER_ENTITY") || Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INSURER"))
                {
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[INS_ENT_TEMP]", "POLICY_ID");



                    if (bReinsurerIncluded)
                        JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[INS_ENT_TEMP]", "IN_ROW_ID", true);
                    // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                    //Dump results into temp table
                    sSQL = "SELECT POLICY.POLICY_ID, POLICY_X_INSURER.IN_ROW_ID"
                        + " [INTO INS_ENT_TEMP]"
                        + " FROM [POLICY_TEMP],POLICY,ENTITY INSURER_ENTITY, POLICY_X_INSURER"
                        + " WHERE [POLICY_TEMP].POLICY_ID = POLICY.POLICY_ID"
                        + " AND POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID"
                        + " AND POLICY_X_INSURER.INSURER_CODE = INSURER_ENTITY.ENTITY_ID";

                    // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                    if (bReinsurerIncluded)
                        sSQL = sSQL + " AND [POLICY_TEMP].IN_ROW_ID = POLICY_X_INSURER.IN_ROW_ID";
                    // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INSUTEMP_" + m_lUserId + " ON [INS_ENT_TEMP](POLICY_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "REINSURER_ENTITY") || Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_INS_REINS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "REINSURER_ENTITY");
                    iIdxReinsurer = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_INS_REINS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[REINS_ENT_TEMP]", "POLICY_ID");

                    // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                    // We need to add the join for Policy_X_Insurer.Inrowid and POLICY_X_INS_REINS.POL_X_INS_ROW_ID
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[REINS_ENT_TEMP]", "IN_ROW_ID", "POL_X_INS_ROW_ID", true);

                    //Dump results into temp table
                    sSQL = "SELECT POLICY.POLICY_ID,POLICY_X_INS_REINS.POL_X_INS_ROW_ID";
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    if (iIdxReinsurer > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxReinsurer].ToString());
                    sSQL = sSQL + " [INTO REINS_ENT_TEMP]"
                        + " FROM [POLICY_TEMP],POLICY,ENTITY REINSURER_ENTITY, POLICY_X_INS_REINS, POLICY_X_INSURER"
                        + " WHERE [POLICY_TEMP].POLICY_ID = POLICY.POLICY_ID"
                        + " AND POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID"
                        + " AND POLICY_X_INSURER.IN_ROW_ID = POLICY_X_INS_REINS.POL_X_INS_ROW_ID"
                        + " AND POLICY_X_INS_REINS.REINSURER_EID = REINSURER_ENTITY.ENTITY_ID";
                    if (bReinsurerIncluded)
                        sSQL = sSQL + " AND [POLICY_TEMP].IN_ROW_ID = POLICY_X_INSURER.IN_ROW_ID";
                    // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX REINSUTEMP_" + m_lUserId + " ON [REINS_ENT_TEMP](POLICY_ID)"));
                }
                else if (Utilities.ExistsInArray(p_arrlstQueryTables, "REINSURER_ENTITY") || Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INS_REINS"))
                {

                    // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                    // We need to add the join for Policy_X_Insurer.Inrowid and POLICY_X_INS_REINS.POL_X_INS_ROW_ID
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[REINS_ENT_TEMP]", "IN_ROW_ID", "POL_X_INS_ROW_ID", true);

                    //Dump results into temp table
                    sSQL = "SELECT POLICY.POLICY_ID,POLICY_X_INS_REINS.POL_X_INS_ROW_ID"
                        + " [INTO REINS_ENT_TEMP]"
                        + " FROM [POLICY_TEMP],POLICY,ENTITY REINSURER_ENTITY, POLICY_X_INS_REINS, POLICY_X_INSURER"
                        + " WHERE [POLICY_TEMP].POLICY_ID = POLICY.POLICY_ID"
                        + " AND POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID"
                        + " AND POLICY_X_INSURER.IN_ROW_ID = POLICY_X_INS_REINS.POL_X_INS_ROW_ID"
                        + " AND POLICY_X_INS_REINS.REINSURER_EID = REINSURER_ENTITY.ENTITY_ID";
                    if (bReinsurerIncluded)
                        sSQL = sSQL + " AND [POLICY_TEMP].IN_ROW_ID = POLICY_X_INSURER.IN_ROW_ID";
                    // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX REINSUTEMP_" + m_lUserId + " ON [REINS_ENT_TEMP](POLICY_ID)"));
                }
                // npadhy End MITS 18653  Added the case for Reinsurer and modified the logic for Insurer
                //coverages
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_CVG_TYPE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_CVG_TYPE");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CVG_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CVG_TEMP]"
                        + " FROM [CLAIM_TEMP],POLICY_X_CVG_TYPE"
                        + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CVGTEMP_" + m_lUserId + " ON [CVG_TEMP](CLAIM_ID)"));
                }

                //insured layers
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_INSURED") || Utilities.ExistsInArray(p_arrlstDisplayTables, "INSURED_ENTITY"))
                {
                    sFinalField = new StringBuilder();// changes for JIRA: RMA-6390
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_INSURED");
                    iIdxInsured = Utilities.IndexInArray(p_arrlstDisplayTables, "INSURED_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[INSURED_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    //MITS 17025 fix an issue related to index out of range issue
                    string sFieldList = string.Empty;
                    if (iIdx > -1)
                    {
                        sFieldList = p_arrlstDisplayFields[iIdx].ToString();
                    }
                    if (iIdxInsured > -1)
                    {
                        if (!string.IsNullOrEmpty(sFieldList))
                            sFieldList += ",";
                        sFieldList += p_arrlstDisplayFields[iIdxInsured].ToString();
                    }

                    //aaggarwal29 : MITS 37120 start
                    sTEmpSplit = sFieldList.Split(',');
                    sSQL = "SELECT " + sEvalDistinct(sFieldList) + " [CLAIM_TEMP].CLAIM_ID ";
                    sNewField = string.Empty;
                    foreach (string str in sTEmpSplit)
                    {
                        string temp = null;
                        if (str.Contains("||"))
                        {
                            string[] splitarray = str.Split(new string[] { "||" }, StringSplitOptions.None);
                            for (int i = 0; i < splitarray.Length; i++)
                            {
                                if (splitarray[i].Contains("|"))
                                {
                                    temp = splitarray[i].Split('|')[1].ToString();
                                    //        splitarray[i] = splitarray[i].Split('|')[0].ToString();
                                    break;

                                }
                                //    splitarray[i] = splitarray[i].Replace(splitarray[i], " COALESCE(" + splitarray[i] + ",'') ");
                            }
                            //temp = str.Split('|')[1].ToString();
                            if (m_sDBType == Constants.DB_SQLSRVR)
                                sNewField = "COALESCE(INSURED_ENTITY.FIRST_NAME,'') +' '+ COALESCE(INSURED_ENTITY.MIDDLE_NAME,'') +' '+ COALESCE(INSURED_ENTITY.LAST_NAME ,'') " + " | " + temp;
                            else if (m_sDBType == Constants.DB_ORACLE)
                                sNewField = "INSURED_ENTITY.FIRST_NAME || ' '|| INSURED_ENTITY.MIDDLE_NAME || ' ' || INSURED_ENTITY.LAST_NAME | " + temp;

                            if (string.IsNullOrEmpty(sFinalField.ToString()))
                                sFinalField.Append(sNewField);
                            else
                                sFinalField.Append("," + sNewField);

                        }
                        else
                        {
                            if (string.IsNullOrEmpty(sFinalField.ToString()))
                                sFinalField.Append(str);
                            else
                                sFinalField.Append("," + str);
                        }
                    }

                    
                    //igupta3 Mits# 36041 adding first middle last name in insurer name
                    //if (p_arrlstDisplayFields[iIdxInsured].ToString().Contains("||") && m_sDBType != Constants.DB_ORACLE)
                    //{
                    //    string temp=null;
                    //    string[] splitarray= p_arrlstDisplayFields[iIdxInsured].ToString().Split(new string[] {"||"},StringSplitOptions.None);
                    //    for (int i = 0; i < splitarray.Length; i++)
                    //    {
                    //        if (splitarray[i].Contains("|"))
                    //        {
                    //            temp = splitarray[i].Split('|')[1].ToString();
                    //            splitarray[i] = splitarray[i].Split('|')[0].ToString();
                                
                    //        }
                    //        splitarray[i]=splitarray[i].Replace(splitarray[i]," COALESCE("+splitarray[i]+",'') ");                            
                    //    }
                    //    p_arrlstDisplayFields[iIdxInsured] = splitarray[0] + "+' '+" + splitarray[1] + "+' '+" + splitarray[2] + " | " + temp;
                    //        //p_arrlstDisplayFields[iIdxInsured] = p_arrlstDisplayFields[iIdxInsured].ToString().Replace("||", " +' ' + ");
                    //}
                    //igupta3 Mits# 36041 end

                    //Fixed RMA-6390 START
                    //if (iIdx > -1)
                    //    sSQL += Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());

                    //if (iIdxInsured > -1)
                    //    sSQL += Utilities.AppendComma(sFinalField.ToString()); // aaggarwal29: Just need to update for insured_entity
                                   
                        sSQL += Utilities.AppendComma(sFinalField.ToString()); 

                        //Fixed RMA-6390 END
                    //aaggarwal29 : MITS 37120 changes end
                    
                    sSQL = sSQL + " [INTO INSURED_TEMP]"
                        + " FROM [CLAIM_TEMP],POLICY_X_INSURED,ENTITY INSURED_ENTITY"
                        + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_X_INSURED.POLICY_ID AND POLICY_X_INSURED.INSURED_EID = INSURED_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INSDTEMP_" + m_lUserId + " ON [INSURED_TEMP](CLAIM_ID)"));
                }

                //MCO support
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_MCO"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_MCO");
                    iIdxInsured = Utilities.IndexInArray(p_arrlstDisplayTables, "MCO_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POLMCO_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    //MITS 17025 fix an issue related to index out of range issue
                    string sFieldList = string.Empty;
                    if (iIdx > -1)
                    {
                        sFieldList = p_arrlstDisplayFields[iIdx].ToString();
                    }
                    if (iIdxInsured > -1)
                    {
                        if (!string.IsNullOrEmpty(sFieldList))
                            sFieldList += ",";
                        sFieldList += p_arrlstDisplayFields[iIdxInsured].ToString();
                    }
                    sSQL = "SELECT " + sEvalDistinct(sFieldList) + " [CLAIM_TEMP].CLAIM_ID ";
                    if (iIdx > -1)
                        sSQL += Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    if (iIdxInsured > -1)
                        sSQL += Utilities.AppendComma(p_arrlstDisplayFields[iIdxInsured].ToString());
                    sSQL = sSQL + " [INTO POLMCO_TEMP]"
                        + " FROM [CLAIM_TEMP],POLICY_X_MCO,ENTITY MCO_ENTITY"
                        + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_X_MCO.POLICY_ID"
                        + " AND POLICY_X_MCO.MCO_EID = MCO_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX POLMCOTEMP_" + m_lUserId + " ON [POLMCO_TEMP](CLAIM_ID)"));
                }

                //supplemental support
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POLSUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    sSQL = "SELECT  [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO POLSUPP_TEMP]"
                        + " FROM [CLAIM_TEMP],POLICY_SUPP"
                        + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_SUPP.POLICY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX POLSPTEMP_" + m_lUserId + " ON [POLSUPP_TEMP](CLAIM_ID)"));
                }


                //npadhy 05/14/2009 Mits:-16508 End
                //Enhanced Policy related stuff
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POLICY_ENH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO POLICY_ENH_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_ENH, POLICY_X_TERM_ENH";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID AND POLICY_X_TERM_ENH.TERM_ID = (SELECT MAX(TERM_ID) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = POLICY_ENH.POLICY_ID)";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX POLICY_ENH_TEMP_" + m_lUserId + " ON [POLICY_ENH_TEMP](CLAIM_ID)"));
                }

                //insurer
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "INSURER_ENTITY_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "INSURER_ENTITY_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[INS_ENT_ENH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO INS_ENT_ENH_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_ENH,ENTITY INSURER_ENTITY_ENH";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID";
                    sSQL = sSQL + " AND POLICY_ENH.INSURER_EID = INSURER_ENTITY_ENH.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INS_ENT_ENHTEMP_" + m_lUserId + " ON [INS_ENT_ENH_TEMP](CLAIM_ID)"));

                }

                //insured layers
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_INSRD_ENH") || Utilities.ExistsInArray(p_arrlstDisplayTables, "INSURED_ENTITY_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_INSRD_ENH");
                    iIdxInsured = Utilities.IndexInArray(p_arrlstDisplayTables, "INSURED_ENTITY_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[INSURED_ENH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    string sFieldList = string.Empty;
                    if (iIdx > -1)
                    {
                        sFieldList = p_arrlstDisplayFields[iIdx].ToString();
                    }
                    if (iIdxInsured > -1)
                    {
                        if (!string.IsNullOrEmpty(sFieldList))
                            sFieldList += ",";
                        sFieldList += p_arrlstDisplayFields[iIdxInsured].ToString();
                    }
                    sSQL = "SELECT " + sEvalDistinct(sFieldList) + " [CLAIM_TEMP].CLAIM_ID ";
                    if (iIdx > -1)
                        sSQL += Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());

                    if (iIdxInsured > -1)
                    {
                        sSQL += Utilities.AppendComma(p_arrlstDisplayFields[iIdxInsured].ToString());
                    }
                    sSQL = sSQL + " [INTO INSURED_ENH_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_X_INSRD_ENH,ENTITY INSURED_ENTITY_ENH";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_X_INSRD_ENH.POLICY_ID AND POLICY_X_INSRD_ENH.INSURED_EID = INSURED_ENTITY_ENH.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INSD_ENHTEMP_" + m_lUserId + " ON [INSURED_ENH_TEMP](CLAIM_ID)"));
                }

                //Broker
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "BROKER_ENTITY_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "BROKER_ENTITY_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[BRO_ENT_ENH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO BRO_ENT_ENH_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_ENH,ENTITY BROKER_ENTITY_ENH";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID";
                    sSQL = sSQL + " AND POLICY_ENH.BROKER_EID = BROKER_ENTITY_ENH.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX BRO_ENT_ENHTEMP_" + m_lUserId + " ON [BRO_ENT_ENH_TEMP](CLAIM_ID)"));

                }

                //coverages
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_CVG_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_CVG_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CVG_ENH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CVG_ENH_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_ENH, POLICY_X_CVG_ENH ";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID AND ";
                    sSQL = sSQL + " POLICY_ENH.POLICY_ID = POLICY_X_CVG_ENH.POLICY_ID AND ";
                    sSQL = sSQL + " POLICY_X_CVG_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_CVG_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID)";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CVG_ENHTEMP_" + m_lUserId + " ON [CVG_ENH_TEMP](CLAIM_ID)"));
                }

                //Rating
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_RTNG_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_RTNG_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[RTNG_ENH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO RTNG_ENH_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_ENH, POLICY_X_RTNG_ENH ";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID AND ";
                    sSQL = sSQL + " POLICY_ENH.POLICY_ID = POLICY_X_RTNG_ENH.POLICY_ID AND ";
                    sSQL = sSQL + " POLICY_X_RTNG_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_RTNG_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID)";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX RTNG_ENHTEMP_" + m_lUserId + " ON [RTNG_ENH_TEMP](CLAIM_ID)"));
                }


                // Discount
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_DCNT_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_DCNT_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[DCNT_ENH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO DCNT_ENH_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_ENH, POLICY_X_DCNT_ENH ";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID AND ";
                    sSQL = sSQL + " POLICY_ENH.POLICY_ID = POLICY_X_DCNT_ENH.POLICY_ID AND ";
                    sSQL = sSQL + " POLICY_X_DCNT_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_DCNT_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID)";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DCNT_ENHTEMP_" + m_lUserId + " ON [DCNT_ENH_TEMP](CLAIM_ID)"));
                }

                // Discount Tier
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_DTIER_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_DTIER_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[DTIER_ENH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO DTIER_ENH_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_ENH, POLICY_X_DTIER_ENH ";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID AND ";
                    sSQL = sSQL + " POLICY_ENH.POLICY_ID = POLICY_X_DTIER_ENH.POLICY_ID AND ";
                    sSQL = sSQL + " POLICY_X_DTIER_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_DTIER_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID)";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DTIER_ENHTEMP_" + m_lUserId + " ON [DTIER_ENH_TEMP](CLAIM_ID)"));
                }

                // Transaction
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_TRANS_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_TRANS_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POL_TRANS_ENH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO POL_TRANS_ENH_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_ENH, POLICY_X_TRANS_ENH ";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID AND ";
                    sSQL = sSQL + " POLICY_ENH.POLICY_ID = POLICY_X_TRANS_ENH.POLICY_ID AND ";
                    sSQL = sSQL + " POLICY_X_TRANS_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_TRANS_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID)";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX POL_TRANS_ENHTEMP_" + m_lUserId + " ON [POL_TRANS_ENH_TEMP](CLAIM_ID)"));
                }

                // Term
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_TERM_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_TERM_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POL_TERM_ENH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO POL_TERM_ENH_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_ENH, POLICY_X_TERM_ENH ";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID AND ";
                    sSQL = sSQL + " POLICY_ENH.POLICY_ID = POLICY_X_TERM_ENH.POLICY_ID AND ";
                    sSQL = sSQL + " POLICY_X_TERM_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_TERM_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID)";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX POL_TERM_ENHTEMP_" + m_lUserId + " ON [POL_TERM_ENH_TEMP](CLAIM_ID)"));
                }


                //MCO support
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_MCO_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_MCO_ENH");
                    iIdxInsured = Utilities.IndexInArray(p_arrlstDisplayTables, "MCO_ENTITY_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[MCO_ENT_ENH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    string sFieldList = string.Empty;
                    if (iIdx > -1)
                    {
                        sFieldList = p_arrlstDisplayFields[iIdx].ToString();
                    }
                    if (iIdxInsured > -1)
                    {
                        if (!string.IsNullOrEmpty(sFieldList))
                            sFieldList += ",";
                        sFieldList += p_arrlstDisplayFields[iIdxInsured].ToString();
                    }
                    sSQL = "SELECT " + sEvalDistinct(sFieldList) + " [CLAIM_TEMP].CLAIM_ID ";
                    if (iIdx > -1)
                        sSQL += Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());

                    if (iIdxInsured > -1)
                    {
                        sSQL += Utilities.AppendComma(p_arrlstDisplayFields[iIdxInsured].ToString());
                    }
                    sSQL = sSQL + " [INTO MCO_ENT_ENH_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_X_MCO_ENH,ENTITY MCO_ENTITY_ENH";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_X_MCO_ENH.POLICY_ID";
                    sSQL = sSQL + " AND POLICY_X_MCO_ENH.MCO_EID = MCO_ENTITY_ENH.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX MCO_ENT_ENH_TEMP_" + m_lUserId + " ON [MCO_ENT_ENH_TEMP](CLAIM_ID)"));
                }

                //supplemental support for Policy
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_ENH_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_ENH_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[POLSUPPENH_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    sSQL = "SELECT  [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO POLSUPPENH_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_ENH_SUPP";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = POLICY_ENH_SUPP.POLICY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX POLSUPPENH_TEMP_" + m_lUserId + " ON [POLSUPPENH_TEMP](CLAIM_ID)"));
                }

                //Add by kuladeep for mits:23622 07/14/2011 Start
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "LITIGATION_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "LITIGATION_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LITIGATIONSUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    sSQL = "SELECT  [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO LITIGATIONSUPP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],LITIGATION_SUPP,CLAIM_X_LITIGATION";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_LITIGATION.CLAIM_ID AND CLAIM_X_LITIGATION.LITIGATION_ROW_ID=LITIGATION_SUPP.LITIGATION_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LITIGATIONSUPP_TEMP" + m_lUserId + " ON [LITIGATIONSUPP_TEMP](CLAIM_ID)"));
                }
                //Add by kuladeep for mits:23622 07/14/2011 End
                //supplemental support for Coverage
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CVG_ENH_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CVG_ENH_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CVGENHSUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    sSQL = "SELECT  [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CVGENHSUPP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CVG_ENH_SUPP";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = CVG_ENH_SUPP.POLICY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CVGENHSPTEMP_" + m_lUserId + " ON [CVGENHSUPP_TEMP](CLAIM_ID)"));
                }


                //supplemental support for Exposure
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EXP_ENH_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EXP_ENH_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[EXPENHSUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table
                    sSQL = "SELECT  [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO EXPENHSUPP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],POLICY_ENH_SUPP";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].PRIMARY_POLICY_ID = EXP_ENH_SUPP.POLICY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX EXPENHSPTEMP_" + m_lUserId + " ON [EXPENHSUPP_TEMP](CLAIM_ID)"));
                }
                //Table EVENT_QM for QUEST
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EVENT_QM"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EVENT_QM");
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[CLAIMQM_TEMP]", "EVENT_ID");
                    // Mihika 16-Feb-2006 DISTINCT was causing exception to occur. Removed it.
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " EVENT_QM.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CLAIMQM_TEMP]"
                        + " FROM [CLAIM_TEMP],EVENT_QM WHERE [CLAIM_TEMP].EVENT_ID=EVENT_QM.EVENT_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX EVENTQMTEMP_" + m_lUserId + " ON [CLAIMQM_TEMP](EVENT_ID)"));
                }

                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ORG_ENTITY");

                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[ORG_TEMP]", "EVENT_ID");
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO ORG_TEMP]"
                        + " FROM [CLAIM_TEMP],EVENT,ENTITY ORG_ENTITY WHERE [CLAIM_TEMP].EVENT_ID = EVENT.EVENT_ID"
                        + " AND EVENT.DEPT_EID = ORG_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ORGTEMP_" + m_lUserId + " ON [ORG_TEMP](EVENT_ID)"));
                }

                // Mihika 16-Feb-2006 Changes related to MITS 5034
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EVENT_X_OUTCOME"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EVENT_X_OUTCOME");

                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[OUTCOME_TEMP]", "CLAIM_ID");
                    //Nitesh: MITS-7618 Added Selectively " DISTINCT " for search sqls
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [CLAIM_TEMP].CLAIM_ID," + p_arrlstDisplayFields[iIdx] + " [INTO OUTCOME_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],EVENT_X_OUTCOME WHERE [CLAIM_TEMP].EVENT_ID=EVENT_X_OUTCOME.EVENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX EVENTOUTCOMETEMP_" + m_lUserId + " ON [OUTCOME_TEMP](CLAIM_ID)"));
                }

                //skhare7 R8 enhancement Subrogation  & related


                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_X_SUBRO"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_X_SUBRO");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SUB_TEMP]", "CLAIM_ID");

                    //Dump results into temp table

                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_SUBRO.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO SUB_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_SUBRO";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_SUBRO.CLAIM_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SUBTMP_" + m_lUserId +
                                                                " ON [SUB_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "SUBRO_SPECIALIST_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "SUBRO_SPECIALIST_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SUB_SPE_TEMP]", "CLAIM_ID");


                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_SUBRO.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO SUB_SPE_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_SUBRO,ENTITY SUBRO_SPECIALIST_ENTITY WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_SUBRO.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_SUBRO.SUB_SPECIALIST_EID = SUBRO_SPECIALIST_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SUBSPETEMP_" + m_lUserId +
                                                                " ON [SUB_SPE_TEMP](CLAIM_ID)"));
                }


                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "SUBRO_ADVPARTY_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "SUBRO_ADVPARTY_ENTITY");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SUB_ADP_TEMP]", "CLAIM_ID");


                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_SUBRO.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO SUB_ADP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_SUBRO,ENTITY SUBRO_ADVPARTY_ENTITY WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_SUBRO.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_SUBRO.SUB_ADVERSE_PARTY_EID = SUBRO_ADVPARTY_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SUBADPTEMP_" + m_lUserId +
                                                                " ON [SUB_ADP_TEMP](CLAIM_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "SUBRO_COMPANY_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "SUBRO_COMPANY_ENTITY");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SUB_COMP_TEMP]", "CLAIM_ID");


                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_SUBRO.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO SUB_COMP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_SUBRO,ENTITY SUBRO_COMPANY_ENTITY WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_SUBRO.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_SUBRO.SUB_ADVERSE_INS_CO_EID = SUBRO_COMPANY_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SUBCOMPTEMP_" + m_lUserId +
                                                                " ON [SUB_COMP_TEMP](CLAIM_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "SUBRO_ADJUSTER_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "SUBRO_ADJUSTER_ENTITY");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SUB_ADJ_TEMP]", "CLAIM_ID");


                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_SUBRO.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO SUB_ADJ_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_SUBRO,ENTITY SUBRO_ADJUSTER_ENTITY WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_SUBRO.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_SUBRO.SUB_ADV_ADJUSTER_EID = SUBRO_ADJUSTER_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SUBADJPTEMP_" + m_lUserId +
                                                                " ON [SUB_ADJ_TEMP](CLAIM_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "SUBROGATION_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "SUBROGATION_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SUB_SUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table

                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString(), p_arrlstTextSuppFields) + " CLAIM_TEMP.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO SUB_SUPP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_SUBRO,SUBROGATION_SUPP";
                    sSQL = sSQL + " WHERE  [CLAIM_TEMP].CLAIM_ID = CLAIM_X_SUBRO.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_SUBRO.SUBROGATION_ROW_ID = SUBROGATION_SUPP.SUBROGATION_ROW_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SUBSTMP_" + m_lUserId + " ON [SUB_SUPP_TEMP](CLAIM_ID)"));
                }
                //liability Info


                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_X_LIABILITYLOSS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_X_LIABILITYLOSS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LIAB_TEMP]", "CLAIM_ID");

                    //Dump results into temp table

                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_LIABILITYLOSS.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO LIAB_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_LIABILITYLOSS";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_LIABILITYLOSS.CLAIM_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LAIBTMP_" + m_lUserId +
                                                                " ON [LIAB_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "LIABILTY_PARTY_NAME"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "LIABILTY_PARTY_NAME");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LIAB_PARTY_TEMP]", "CLAIM_ID");


                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_LIABILITYLOSS.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO LIAB_PARTY_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_LIABILITYLOSS,ENTITY LIABILTY_PARTY_NAME WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_LIABILITYLOSS.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_LIABILITYLOSS.PARTY_AFFECTED = LIABILTY_PARTY_NAME.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LIABPARTEMP_" + m_lUserId +
                                                                " ON [LIAB_PARTY_TEMP](CLAIM_ID)"));
                }


                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "LIABILTY_OWNER_NAME"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "LIABILTY_OWNER_NAME");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LAIB_OWN_TEMP]", "CLAIM_ID");


                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_LIABILITYLOSS.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO LAIB_OWN_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_LIABILITYLOSS,ENTITY LIABILTY_OWNER_NAME WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_LIABILITYLOSS.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_LIABILITYLOSS.POLICYHOLDER = LIABILTY_OWNER_NAME.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LIABOWNTEMP_" + m_lUserId +
                                                                " ON [LAIB_OWN_TEMP](CLAIM_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_X_LIABILITYLOSS_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_X_LIABILITYLOSS_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[LIAB_SUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table

                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString(), p_arrlstTextSuppFields) + " CLAIM_TEMP.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO LIAB_SUPP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_LIABILITYLOSS,CLAIM_X_LIABILITYLOSS_SUPP";
                    sSQL = sSQL + " WHERE  [CLAIM_TEMP].CLAIM_ID = CLAIM_X_LIABILITYLOSS.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_LIABILITYLOSS.ROW_ID = CLAIM_X_LIABILITYLOSS_SUPP.ROW_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LIABSTMP_" + m_lUserId + " ON [LIAB_SUPP_TEMP](CLAIM_ID)"));
                }

                //Property Loss R8 enhancements
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_X_PROPERTYLOSS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_X_PROPERTYLOSS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROPLOSS_TEMP]", "CLAIM_ID");

                    //Dump results into temp table

                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_PROPERTYLOSS.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO PROPLOSS_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_PROPERTYLOSS";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PROPLTMP_" + m_lUserId +
                                                                " ON [PROPLOSS_TEMP](CLAIM_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_X_PROPERTYINVOLVED"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_X_PROPERTYINVOLVED");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROPINV_TEMP]", "CLAIM_ID");

                    //Dump results into temp table

                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_PROPERTYLOSS.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO PROPINV_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_PROPERTYLOSS,CLAIM_X_PROPERTYINVOLVED";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_PROPERTYINVOLVED.ROW_ID = CLAIM_X_PROPERTYLOSS.ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PROPPITMP_" + m_lUserId +
                                                                " ON [PROPINV_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PROPERTY_OWNER_NAME"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PROPERTY_OWNER_NAME");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROP_OWNR_TEMP]", "CLAIM_ID");


                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_PROPERTYLOSS.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO PROP_OWNR_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_PROPERTYLOSS,ENTITY PROPERTY_OWNER_NAME WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_PROPERTYLOSS.OWNER = PROPERTY_OWNER_NAME.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PROPOWNTEMP_" + m_lUserId +
                                                                " ON [PROP_OWNR_TEMP](CLAIM_ID)"));
                }


                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PROPERTY_PHOLDER_NAME"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PROPERTY_PHOLDER_NAME");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROP_PHOLD_TEMP]", "CLAIM_ID");


                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_PROPERTYLOSS.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO PROP_PHOLD_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_PROPERTYLOSS,ENTITY PROPERTY_PHOLDER_NAME WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_PROPERTYLOSS.POLICYHOLDER = PROPERTY_PHOLDER_NAME.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PROPHOLDTEMP_" + m_lUserId +
                                                                " ON [PROP_PHOLD_TEMP](CLAIM_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_X_PROPERTYLOSS_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_X_PROPERTYLOSS_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROPLOSS_SUPP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table

                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString(), p_arrlstTextSuppFields) + " CLAIM_TEMP.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO PROPLOSS_SUPP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_PROPERTYLOSS,CLAIM_X_PROPERTYLOSS_SUPP";
                    sSQL = sSQL + " WHERE  [CLAIM_TEMP].CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_PROPERTYLOSS.ROW_ID = CLAIM_X_PROPERTYLOSS_SUPP.ROW_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PROPLSTMP_" + m_lUserId + " ON [PROPLOSS_SUPP_TEMP](CLAIM_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "SALVAGE_UNIT"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "SALVAGE_UNIT");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SALU_TEMP]", "CLAIM_ID");

                    //Dump results into temp table

                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " UNIT_X_CLAIM.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO SALU_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],SALVAGE SALVAGE_UNIT,UNIT_X_CLAIM";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].CLAIM_ID =UNIT_X_CLAIM.CLAIM_ID";
                    sSQL = sSQL + " AND UNIT_X_CLAIM.UNIT_ROW_ID =  SALVAGE_UNIT.PARENT_ID";
                    sSQL = sSQL + " AND SALVAGE_UNIT.PARENT_NAME = 'UnitXClaim'";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SALUTMP_" + m_lUserId +
                                                                " ON [SALU_TEMP](CLAIM_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "SALVAGE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "SALVAGE");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SALP_TEMP]", "CLAIM_ID");

                    //Dump results into temp table

                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_PROPERTYLOSS.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO SALP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],SALVAGE ,CLAIM_X_PROPERTYLOSS";
                    sSQL = sSQL + " WHERE [CLAIM_TEMP].CLAIM_ID =CLAIM_X_PROPERTYLOSS.CLAIM_ID";
                    sSQL = sSQL + " AND CLAIM_X_PROPERTYLOSS.ROW_ID =  SALVAGE.PARENT_ID";
                    sSQL = sSQL + " AND SALVAGE.PARENT_NAME = 'ClaimXPropertyLoss'";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SALPTMP_" + m_lUserId +
                                                                " ON [SALP_TEMP](CLAIM_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PROPSALVAGE_OWNER_NAME"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PROPSALVAGE_OWNER_NAME");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PROP_SALB_TEMP]", "CLAIM_ID");


                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM_X_PROPERTYLOSS.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO PROP_SALB_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_PROPERTYLOSS,SALVAGE,ENTITY PROPSALVAGE_OWNER_NAME WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID";
                    sSQL = sSQL + " AND SALVAGE.BUYER_EID = PROPSALVAGE_OWNER_NAME.ENTITY_ID";
                    sSQL = sSQL + " AND CLAIM_X_PROPERTYLOSS.ROW_ID = SALVAGE.PARENT_ID";
                    sSQL = sSQL + " AND SALVAGE.PARENT_NAME = 'ClaimXPropertyLoss'";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PROPSALBTEMP_" + m_lUserId +
                                                                " ON [PROP_SALB_TEMP](CLAIM_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "UNTSALVAGE_OWNER_NAME"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "UNTSALVAGE_OWNER_NAME");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[UNIT_SALB_TEMP]", "CLAIM_ID");


                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " UNIT_X_CLAIM.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO UNIT_SALB_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],UNIT_X_CLAIM,SALVAGE,ENTITY UNTSALVAGE_OWNER_NAME WHERE [CLAIM_TEMP].CLAIM_ID = UNIT_X_CLAIM.CLAIM_ID";
                    sSQL = sSQL + " AND SALVAGE.BUYER_EID = UNTSALVAGE_OWNER_NAME.ENTITY_ID";
                    sSQL = sSQL + " AND UNIT_X_CLAIM.UNIT_ROW_ID = SALVAGE.PARENT_ID";
                    sSQL = sSQL + " AND SALVAGE.PARENT_NAME = 'UnitXClaim'";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX UNITSALBTEMP_" + m_lUserId +
                                                                " ON [UNIT_SALB_TEMP](CLAIM_ID)"));
                }
                //mits 29452 asingh263 starts
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "UNITSALVAGE_INPOSS_NAME"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "UNITSALVAGE_INPOSS_NAME");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SALVAGEIP_TEMP]", "CLAIM_ID");


                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO SALVAGEIP_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_PROPERTYLOSS,SALVAGE,ENTITY UNITSALVAGE_INPOSS_NAME WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM.CLAIM_ID";
                    sSQL = sSQL + " AND SALVAGE.POSSESSION_OF = UNITSALVAGE_INPOSS_NAME.ENTITY_ID";
                    sSQL = sSQL + " AND UNIT_X_CLAIM.UNIT_ROW_ID = SALVAGE.PARENT_ID";
                    sSQL = sSQL + " AND SALVAGE.PARENT_ID=CLAIM_X_PROPERTYLOSS.ROW_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SALVAGEIPTEMP_" + m_lUserId +
                                                                " ON [SALVAGEIP_TEMP](CLAIM_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "UNITSALVAGE_YARD_ADD"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "UNITSALVAGE_YARD_ADD");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[SALVAGEY_TEMP]", "CLAIM_ID");


                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " CLAIM.CLAIM_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " [INTO SALVAGEY_TEMP]";
                    sSQL = sSQL + " FROM [CLAIM_TEMP],CLAIM_X_PROPERTYLOSS,SALVAGE,ENTITY_X_ADDRESSES UNITSALVAGE_YARD_ADD WHERE [CLAIM_TEMP].CLAIM_ID = CLAIM.CLAIM_ID";
                    sSQL = sSQL + " AND SALVAGE.SALVAGE_YARD_ADDR_ID = UNITSALVAGE_YARD_ADD.ADDRESS_ID";
                    sSQL = sSQL + " AND UNIT_X_CLAIM.UNIT_ROW_ID = SALVAGE.PARENT_ID";
                    sSQL = sSQL + " AND SALVAGE.PARENT_ID=CLAIM_X_PROPERTYLOSS.ROW_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index CLAIM_ID for quick processing in display only fields
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SALVAGEYTEMP_" + m_lUserId +
                                                                " ON [SALVAGEY_TEMP](CLAIM_ID)"));
                }
                //mits 29452 asingh263 ends



                //skhare7 End R8 enhancements

                //START Change MITS 10608
                string sql = "";
                int iTableId = 0;
                using (LocalCache objCache = new LocalCache(m_sConnectionString,m_iClientId))
                {
                    iTableId = objCache.GetTableId("LINE_OF_BUSINESS");
                    objCache.Dispose();
                }
                int iAddBracket = 0;
                //Getting all LOB's
                DataSet objDs = GetCodeList(iTableId.ToString(), m_sConnectionString);
                if (objDs.Tables[0] != null)
                {
                    SysParms internalSettings = new SysParms(m_sConnectionString, m_iClientId);
                    foreach (DataRow dr in objDs.Tables[0].Rows)
                    {
                        string m_LOB = Conversion.ConvertObjToStr(dr["CODE_ID"]);
                        LobSettings objLobSettings = null;
                        // SysParms internalSettings = new SysParms(m_sConnectionString);
                        objLobSettings = internalSettings.ColLobSettings[Conversion.ConvertStrToInteger(m_LOB)];
                        UserLoginLimits objLimits = null;
                        if (objLobSettings.ClmAccLmtFlag)//If enabled
                        {
                            using (DbConnection objDbConnection = DbFactory.GetDbConnection(m_sConnectionString))
                            {
                                objDbConnection.Open();
                                objLimits = new UserLoginLimits((int)UserId, (int)GroupId, objDbConnection, m_iClientId);
                                objDbConnection.Close();
                            }
                            if (objLimits != null)
                            {
                                //Creating A dynamic Sql Condition based on Claim Limits Set in User Privileges setup
                                string[,] arr = objLimits.ClaimStatusTypeAccessList(Conversion.ConvertStrToInteger(m_LOB));

                                if (arr.Length != 0)
                                {
                                    string m_ClaimLimitFilter = "";
                                    string sTypeFilter = "";
                                    string sTypeFilter1 = "";
                                    string sTypeFilter2 = "";

                                    for (int i = 0; i < Convert.ToInt32(arr.Length / 2); i++)
                                    {
                                        if (arr[i, 0] != "0" && arr[i, 1] != "0")
                                        {
                                            if (i > 0 && sTypeFilter != "")
                                                sTypeFilter += " AND ";
                                            //pmittal5 Mits 16820 10/21/09 - Added alias name CLAIM_TEMP with claim fields
                                            sTypeFilter += "NOT([CLAIM_TEMP].CLAIM_TYPE_CODE =" + arr[i, 0]
                                                 + " AND "
                                                 + "[CLAIM_TEMP].CLAIM_STATUS_CODE =" + arr[i, 1]
                                                 + ")";
                                        }
                                        else if (arr[i, 0] != "0" || arr[i, 1] != "0")
                                        {
                                            if (arr[i, 0] != "0")
                                            {
                                                if (i > 0 && sTypeFilter1 != "")
                                                    sTypeFilter1 += ",";
                                                sTypeFilter1 += arr[i, 0];
                                            }
                                            else
                                            {
                                                if (i > 0 && sTypeFilter2 != "")
                                                    sTypeFilter2 += ",";
                                                sTypeFilter2 += arr[i, 1];
                                            }
                                        }
                                    }
                                    if (sTypeFilter != "")
                                        m_ClaimLimitFilter += sTypeFilter;

                                    if (sTypeFilter1 != "")
                                    {
                                        if (m_ClaimLimitFilter != "")
                                            m_ClaimLimitFilter += " AND " + "[CLAIM_TEMP].CLAIM_TYPE_CODE NOT IN (" + sTypeFilter1 + ")";
                                        else
                                            m_ClaimLimitFilter += "[CLAIM_TEMP].CLAIM_TYPE_CODE NOT IN (" + sTypeFilter1 + ")";
                                    }
                                    if (sTypeFilter2 != "")
                                    {
                                        if (m_ClaimLimitFilter != "")
                                            m_ClaimLimitFilter += " AND " + "[CLAIM_TEMP].CLAIM_STATUS_CODE NOT IN (" + sTypeFilter2 + ")";
                                        else
                                            m_ClaimLimitFilter += "[CLAIM_TEMP].CLAIM_STATUS_CODE NOT IN (" + sTypeFilter2 + ")";
                                    }
                                    if (sql == "")
                                    {
                                        iAddBracket = 1;
                                        sql = " (([CLAIM_TEMP].LINE_OF_BUS_CODE= " + m_LOB + " AND " + m_ClaimLimitFilter + ")";
                                    }
                                    else
                                        sql = sql + " OR ([CLAIM_TEMP].LINE_OF_BUS_CODE= " + m_LOB + " AND " + m_ClaimLimitFilter + ")";
                                }
                                else
                                {
                                    if (sql == "")
                                    {
                                        iAddBracket = 1;
                                        sql = sql + "( [CLAIM_TEMP].LINE_OF_BUS_CODE= " + m_LOB;
                                    }
                                    else
                                        sql = sql + " OR [CLAIM_TEMP].LINE_OF_BUS_CODE= " + m_LOB;
                                }
                            }
                        }
                        else
                        {
                            if (sql == "")
                            {
                                iAddBracket = 1;
                                sql = sql + "( [CLAIM_TEMP].LINE_OF_BUS_CODE= " + m_LOB;
                            }
                            else
                                sql = sql + " OR [CLAIM_TEMP].LINE_OF_BUS_CODE= " + m_LOB;
                        }
                        //End - pmittal5
                        objLobSettings = null;
                        //internalSettings = null;
                    }
                }
                if (objDs != null)
                    objDs.Dispose();
                if (sWhere != "")
                    sWhere = sWhere + " AND ";
                if (iAddBracket == 1)
                    sql = sql + ")";
                sWhere = sWhere + sql;
                //END Change  MITS 10608
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sClaimData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sClaimData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }

            }
            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (CollectionException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoClaimSearch.GenericError", m_iClientId), p_objException);
            }
        }
        #endregion

        #endregion

        #region Private Functions
        //Start MITS 10608
        private DataSet GetCodeList(string p_sTableId, string p_sConnectString)
        {
            DataSet objDs = null;
            //StringBuilder objSql = null;
            string sSql;
            try
            {
                //objSql = new StringBuilder();
                //objSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC,CODES.LINE_OF_BUS_CODE FROM CODES,CODES_TEXT ");
                //objSql.Append(" WHERE CODES.TABLE_ID =" + p_sTableId);
                //objSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                //objSql.Append(" AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0 ");
                //objSql.Append(" ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC ");

                //objDs = DbFactory.GetDataSet(p_sConnectString, objSql.ToString());


                sSql = " SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC,CODES.LINE_OF_BUS_CODE FROM CODES,CODES_TEXT "
                    + " WHERE CODES.TABLE_ID =" + p_sTableId
                    + " AND CODES.CODE_ID = CODES_TEXT.CODE_ID "
                    + " AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0 "
                    + " ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC ";

                objDs = DbFactory.GetDataSet(p_sConnectString, sSql, m_iClientId);
                return objDs;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.GetCodeList.Error", m_iClientId), p_objEx);
            }
            finally
            {
                //objSql = null;
                objDs = null;
            }

        }
        //End MITS 10608
        /// <summary>
        /// Called by non-Hipaa related DoSearch function
        /// </summary>
        /// <param name="p_iPageSize"></param>
        /// <param name="p_iPageNumber"></param>
        /// <param name="p_objConn"></param>
        /// <param name="p_sFrom"></param>
        /// <param name="p_sFieldNames"></param>
        /// <param name="p_sWhere"></param>
        /// <param name="p_sOrderBy"></param>
        /// <returns></returns>
        private string GetResultString(int p_iPageSize, int p_iPageNumber, DbConnection p_objConn,
            string p_sFrom, string p_sFieldNames, string p_sWhere, string p_sOrderBy, ref string p_sRetSql)
        {
            string sHipaaIDs = string.Empty;
            return GetResultString(p_iPageSize, p_iPageNumber, p_objConn,
                    p_sFrom, p_sFieldNames, p_sWhere, p_sOrderBy, string.Empty, ref sHipaaIDs, ref p_sRetSql);

        }


        private string GetResultString(int p_iPageSize, int p_iPageNumber, DbConnection p_objConn,
            string p_sFrom, string p_sFieldNames, string p_sWhere, string p_sOrderBy, string p_sHipaaColumn, ref string p_sHipaaIDs, ref string p_sRetSql)
        {
            string sSQLCount = string.Empty;
            string sSQL = string.Empty;
            //nadim for 14233
            int iCountExtra = 0;
            string[] arrOrderBy = null;
            string sOrderBy = string.Empty;
            //nadim for 14233
            StringBuilder objSB = null;
            DbReader objReader = null;
            long lRowStart = 0;
            long lRowEnd = 0;
            LocalCache objCache = null; //mbahl3 Mits 30224
            lRowStart = (p_iPageNumber - 1) * p_iPageSize;
            lRowEnd = p_iPageNumber * p_iPageSize;
            int lRecords = 0;
            //skhare7
            string sRecordData = string.Empty;
            bool blnSuccess = false;
            string sPresentDate = string.Empty; //mbahl3 Mits 30224
            //Add by kuladeep for Publix mits:29986 
            string sHipaaIDs = string.Empty;

            //nadim for 14233-start
            //..To Check if there is order by clause - add extra columns so that DISTINCT ORDER BY FIELD has to be in SELECT statement
            // srajindersin - 08/16/2012 - MITS 29229
            // if (p_sFieldNames.Contains("DISTINCT"))
            // {
            if (p_sOrderBy.Length > 0)
            {
                sOrderBy = p_sOrderBy.Replace("ORDER BY ", "");
                if (sOrderBy.Contains("DESC"))
                {

                    sOrderBy = sOrderBy.Replace(" DESC", "");
                }

                sOrderBy = sOrderBy.Trim();
                if (!p_sFieldNames.Trim().Contains(sOrderBy))
                {
                    p_sFieldNames = p_sFieldNames + "," + sOrderBy;
                    arrOrderBy = sOrderBy.Split(',');
                    iCountExtra = arrOrderBy.Length;
                }
            }
            //}
            //nadim for 14233

            //pmittal5 Mits 16820 10/08/09 - Sort on the basis of Short_code in case Order by clause is given on Code type field
            //Sort on the basis of State_Id in case Order by clause is given on State type field
            //Sort on the basis of Abbreviation in case Order by clause is given on Department
            //p_sOrderBy contains different values, so Changes need to be done in p_sWhere and p_sFrom accordingly
            //Abansal23 MITS 18835 :  Payee records not sorted
            if ((p_sOrderBy.IndexOf("SHORT_CODE") != -1 && p_sOrderBy.IndexOf("CD") != -1) || (p_sOrderBy.IndexOf("STATE_ID") != -1 && p_sOrderBy.IndexOf("ST") != -1) || (p_sOrderBy.IndexOf("ABBREVIATION") != -1 && p_sOrderBy.IndexOf("ORG") != -1) || (p_sOrderBy.IndexOf("TABLE_NAME") != -1 && p_sOrderBy.IndexOf("TBL") != -1))
            {
                string[] sOrderByArray = p_sOrderBy.Split(',');
                int iShortCodeIndex = 0;
                int iCodeAliasIndex = 0;
                string sFieldNum = string.Empty;
                string sFieldName = string.Empty;
                int iDotIndex = 0;
                for (int iOrderByCount = 0; iOrderByCount < sOrderByArray.Length; iOrderByCount++)
                {
                    iDotIndex = sOrderByArray[iOrderByCount].IndexOf(".") + 1;
                    // akaushik5 MITS 30289 starts
                    //sFieldName = sOrderByArray[iOrderByCount].Substring(iDotIndex, sOrderByArray[iOrderByCount].IndexOf(")") - iDotIndex).ToUpper();
                    sFieldName = sOrderByArray[iOrderByCount].Substring(iDotIndex, sOrderByArray[iOrderByCount].LastIndexOf(")") - iDotIndex).ToUpper();
                    // akaushik5 MITS 30289 ends
                    switch (sFieldName)
                    {
                        case "SHORT_CODE":
                            iShortCodeIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("SHORT_CODE");
                            iCodeAliasIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("CD");
                            if (iShortCodeIndex != -1 && iCodeAliasIndex != -1)
                            {
                                sFieldNum = sOrderByArray[iOrderByCount].Substring(iCodeAliasIndex + 2, iShortCodeIndex - iCodeAliasIndex - 3);
                                //pmittal5 MITS 18353 When the Codes were sorted in Claim Search for Policy Status, the Code Fields with blank values were not coming in the search result.
                                //p_sFrom = p_sFrom + " LEFT OUTER JOIN CODES CD" + sFieldNum;
                                //p_sFrom = p_sFrom + " ON CD" + sFieldNum + ".CODE_ID = FLD" + sFieldNum;
                                JoinIt(ref p_sFrom, ref p_sWhere, "", "CODES", "FLD" + sFieldNum, "CODE_ID", "CD" + sFieldNum);
                            }
                            break;
                        case "STATE_ID":
                            iShortCodeIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("STATE_ID");
                            iCodeAliasIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("ST");
                            if (iShortCodeIndex != -1 && iCodeAliasIndex != -1)
                            {
                                sFieldNum = sOrderByArray[iOrderByCount].Substring(iCodeAliasIndex + 2, iShortCodeIndex - iCodeAliasIndex - 3);
                                JoinIt(ref p_sFrom, ref p_sWhere, "", "STATES", "FLD" + sFieldNum, "STATE_ROW_ID", "ST" + sFieldNum);
                            }
                            break;
                        case "ABBREVIATION":
                            iShortCodeIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("ABBREVIATION");
                            iCodeAliasIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("ORG");
                            if (iShortCodeIndex != -1 && iCodeAliasIndex != -1)
                            {
                                sFieldNum = sOrderByArray[iOrderByCount].Substring(iCodeAliasIndex + 3, iShortCodeIndex - iCodeAliasIndex - 4);
                                JoinIt(ref p_sFrom, ref p_sWhere, "", "ENTITY", "FLD" + sFieldNum, "ENTITY_ID", "ORG" + sFieldNum);
                            }
                            break;
                        //Abansal23 MITS 18835 :  Payee records not sorted Starts
                        case "TABLE_NAME":
                            iShortCodeIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("TABLE_NAME");
                            iCodeAliasIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("TBL");
                            if (iShortCodeIndex != -1 && iCodeAliasIndex != -1)
                            {
                                sFieldNum = sOrderByArray[iOrderByCount].Substring(iCodeAliasIndex + 3, iShortCodeIndex - iCodeAliasIndex - 4);
                                JoinIt(ref p_sFrom, ref p_sWhere, "", "GLOSSARY_TEXT", "FLD" + sFieldNum, "TABLE_ID", "TBL" + sFieldNum);
                            }
                            break;
                        //Abansal23 MITS 18835 :  Payee records not sorted Ends
                    }
                }
            }
            //End - pmittal5
            //Deb : Removed the distinct used the single query as before 
            //Aman Mobile Adjuster Change
            //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:28518
            //if (!IsMobileAdjuster)
            //{
                //if (p_sFieldNames.Contains("DISTINCT"))
                //    sSQL = "SELECT " + p_sFieldNames + " FROM " + p_sFrom;
                //else
                //    sSQL = "SELECT DISTINCT " + p_sFieldNames + " FROM " + p_sFrom;
            //}
            //else
            //{
            //  sSQL = "SELECT " + p_sFieldNames + " FROM " + p_sFrom;  
            //}
            //Aman Mobile Adjuster Change
            sSQL = "SELECT " + p_sFieldNames + " FROM " + p_sFrom;
            //Deb : Removed the distinct used the single query as before 
            if (p_sWhere.Length > 0)
                sSQL += " WHERE " + p_sWhere;
            //vsharma203 code merged for RMA-10032
			string sTempSql = sSQL;
            // akaushik5 MITS 30289 starts
            // if (p_sOrderBy.Length > 0)
            //   sSQL += " " + p_sOrderBy;
            if (!string.IsNullOrEmpty(p_sOrderBy))
            {
                Regex regex = new Regex(@" AS FLD\d*");
                p_sOrderBy = regex.Replace(p_sOrderBy, string.Empty);
                sSQL += " " + p_sOrderBy;
            }
            // akaushik5 MITS 30289 ends
            sSQL = PrepareTempSQL(sSQL);
            objSB = new StringBuilder();

            try
            {
                objReader = p_objConn.ExecuteReader(sSQL);
                p_sRetSql = sSQL;
                objCache = new LocalCache(m_sConnectionString,m_iClientId); //mbahl3 Mits 30224
                while (objReader.Read())
                {
                    if (lRecords >= lRowEnd)
                    {
                        //gbhatnagar Mobile Adjuster
                        if (!IsMobileAdjuster && !IsMobilityAdjuster)
                        {
                            break;
                        }
                    }

                    if ((lRecords >= lRowStart) && ((lRecords < lRowEnd) || IsMobileAdjuster || IsMobilityAdjuster))
                    {
                        if (AttachedRecordColumn != string.Empty)
                        {
                            AttachedRecordColumn = AttachedRecordColumn.Replace("WPA_DIARY_ENTRY.ATTACH_TABLE | ", "");
                            sColumnOrder = AttachedRecordColumn;
                            iEntryId = Conversion.CastToType<int>(objReader.GetValue("ENTRY_ID").ToString(), out blnSuccess);
                            //skhare7 R8 DEfects Removed
                            sRecordData = sRetrieveAttachedRecord(objReader.GetValue(sColumnOrder).ToString(), iEntryId);


                        }
                        //nadim for 14233-To Remove extra column that I have added above(objReader.FieldCount - iCountExtra)
					    if (IsEntitySearch)
                        {
                            //sPresentDate = DateTime.Now.ToShortDateString();
                            sPresentDate = Conversion.ToDbDate(DateTime.Now);

                            //for (int j = 0; j < objReader.FieldCount - iCountExtra - (objSysSettingsGlobal.UseEntityRole ? 3 : 2); j++)          //avipinsrivas start : Worked for Jira-340
                            for (int j = 0; j < objReader.FieldCount - iCountExtra - 2; j++)
                            {
                                if (sColumnOrder != string.Empty)
                                {
                                    if (sColumnOrder == objReader.GetName(j).ToString())
                                    {
                                        objSB.Append(sRecordData);
                                        objSB.Append("~*~");

                                    }
                                    else
                                    {
                                        objSB.Append(Conversion.ConvertObjToStr(objReader.GetValue(j)));
                                        objSB.Append("~*~");
                                    }
                                }
                                else
                                {
                                    objSB.Append(Conversion.ConvertObjToStr(objReader.GetValue(j)));
                                    objSB.Append("~*~");
                                }

                                //if (j == (objReader.FieldCount - iCountExtra - (objSysSettingsGlobal.UseEntityRole ? 4 : 3)))         //avipinsrivas start : Worked for Jira-340
                                if (j == (objReader.FieldCount - iCountExtra - 3))
                                {
                                    if (((Conversion.ConvertObjToStr(objReader.GetValue("EFFDATE")).CompareTo(sPresentDate) <= 0) &&
                                         ((sPresentDate).CompareTo(Conversion.ConvertObjToStr(objReader.GetValue("EXPDATE"))) <= 0)) ||
                                    ((Conversion.ConvertObjToStr(objReader.GetValue("EFFDATE")).CompareTo(sPresentDate) <= 0) &&
                                         string.IsNullOrEmpty(Conversion.ConvertObjToStr(objReader.GetValue("EXPDATE")))) ||
                                         (string.IsNullOrEmpty(Conversion.ConvertObjToStr(objReader.GetValue("EFFDATE"))) &&
                                         ((sPresentDate).CompareTo(Conversion.ConvertObjToStr(objReader.GetValue("EXPDATE"))) <= 0)))
                                    {


                                        objSB.Append(objCache.GetCodeDesc(objCache.GetCodeId("A", "ENTITY_STATUS")));
                                        objSB.Append("~*~");
                                    }
                                    else
                                    {
                                        objSB.Append(objCache.GetCodeDesc(objCache.GetCodeId("I", "ENTITY_STATUS")));
                                        objSB.Append("~*~");
                                    }
                                    ////avipinsrivas Start : Worked for Jira-340
                                    //if (objSysSettingsGlobal.UseEntityRole)
                                    //{
                                    //    objSB.Append(Conversion.CastToType<int>(objReader.GetValue(objReader.FieldCount - iCountExtra - 1).ToString(), out blnSuccess));
                                    //    objSB.Append("~*~");
                                    //}
                                    ////avipinsrivas End
                                    break;
                                }
                            }
                            }
                        else
                        {
						//mbahl3 Mits 30224
                        for (int j = 0; j < objReader.FieldCount - iCountExtra; j++)
                        {//skhare7
                            if (sColumnOrder != string.Empty)
                            {
                                if (sColumnOrder == objReader.GetName(j).ToString())
                                {
                                    objSB.Append(sRecordData);
                                    objSB.Append("~*~");

                                }
                                else
                                {
                                    objSB.Append(Conversion.ConvertObjToStr(objReader.GetValue(j)));
                                    objSB.Append("~*~");
                                }
                            }
                            else
                            {
                                objSB.Append(Conversion.ConvertObjToStr(objReader.GetValue(j)));
                                objSB.Append("~*~");
                                }
                        }

                        //mbahl3 mobility changes for event search 
                        if (IsMobilityAdjusterEventList)
                        {
                            int iClaimCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT COUNT(1) FROM EVENT,CLAIM WHERE CLAIM.EVENT_ID=EVENT.EVENT_ID AND EVENT.EVENT_ID=" + Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), m_iClientId)), m_iClientId);
                            objSB.Append(iClaimCount);
                            objSB.Append("~*~");
                            int iTaskCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT COUNT(1) FROM EVENT,WPA_DIARY_ENTRY WHERE WPA_DIARY_ENTRY.ATTACH_RECORDID=EVENT.EVENT_ID AND WPA_DIARY_ENTRY.ATTACH_TABLE='EVENT' AND STATUS_OPEN='-1' AND DIARY_VOID='0' AND EVENT.EVENT_ID=" + Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), m_iClientId) + "   AND ASSIGNED_USER = '" + m_sUserName + "'"), m_iClientId);//mobility change made to show diarys count in xml for only those diarys assigned to teh logged in user 
                            objSB.Append(iTaskCount);
                            objSB.Append("~*~");
                            }
                        }
                        //nadim for 14233
                        objSB.Append("|^|");

                        //Get Hippa related IDs
                        if (p_sHipaaColumn.Length > 0)
                        {
                            if (p_sHipaaIDs.Length > 0)
                            {
                                p_sHipaaIDs += ",";
                                sHipaaIDs += ",";//Add by kuladeep for Publix mits:29986 
                            }
                            p_sHipaaIDs += Conversion.ConvertObjToStr(objReader.GetValue(p_sHipaaColumn));
                            sHipaaIDs += "'" + Conversion.ConvertObjToStr(objReader.GetValue(p_sHipaaColumn)) + "'";//Add by kuladeep for Publix mits:29986 
                        }
                      
                    }
                    lRecords++;
                }
                objReader.Close();

                //Add by kuladeep for Publix mits:29986  Start
                if (p_sHipaaColumn.Length > 0 && p_sHipaaIDs.Length > 0 && ((string.Compare(p_sHipaaColumn, "EVENT_ID") == 0) || (string.Compare(p_sHipaaColumn, "ENTITY_ID") == 0)))
                {
                    p_sHipaaIDs = string.Empty;//For getting correct only Patient value not for every entry

                    if (string.Compare(p_sHipaaColumn, "EVENT_ID") == 0)
                    {
                        sSQL = "SELECT DISTINCT EVENT.EVENT_ID FROM EVENT,PERSON_INVOLVED,PATIENT WHERE EVENT.EVENT_ID=PERSON_INVOLVED.EVENT_ID" +
                               " AND PERSON_INVOLVED.PI_EID=PATIENT.PATIENT_EID AND EVENT.EVENT_ID IN (" + sHipaaIDs + ")";
                    }
                    else if (string.Compare(p_sHipaaColumn, "ENTITY_ID") == 0)
                    {
                        sSQL = "SELECT DISTINCT ENTITY.ENTITY_ID FROM ENTITY,PATIENT WHERE ENTITY.ENTITY_ID=PATIENT.PATIENT_EID AND ENTITY.ENTITY_ID IN (" + sHipaaIDs + ")";
                    }

                    objReader = p_objConn.ExecuteReader(sSQL);
                    p_sRetSql = sSQL;
                    while (objReader.Read())
                    {
                        if (p_sHipaaIDs.Length > 0)
                        {
                            p_sHipaaIDs += ",";
                        }
                        p_sHipaaIDs += Conversion.ConvertObjToStr(objReader.GetValue(p_sHipaaColumn));
                    }
                    objReader.Close();
                }

                //Add by kuladeep for Publix mits:29986  End

                //Retrieve the record count if page number is 1
                if ((p_iPageNumber == 1) && (lRecords >= lRowEnd) && (m_lTotalRecords == 0) && (!IsMobileAdjuster))  //Aman Mobile Adjuster
                {
                    //Deb : Removed the distinct used the single query as before 
                    //START srajindersin - 08/20/2012 - MITS 29229 - to get the correct count of records to filter duplicate records
                    //if (p_sFieldNames.Contains("DISTINCT"))
                    //    sSQLCount = "SELECT COUNT(*) FROM ( SELECT " + p_sFieldNames + " FROM " + p_sFrom;
                    //else
                    //    sSQLCount = "SELECT COUNT(*) FROM ( SELECT " + p_sFieldNames + " FROM " + p_sFrom;
                    //mini performance tseting     
                    //sSQLCount = "SELECT COUNT(*) FROM ( SELECT " + p_sFieldNames + " FROM " + p_sFrom;
                    //vsharma203 code merged for RMA-10032
					   /* sSQLCount = "SELECT COUNT(1) FROM ( SELECT " + p_sFieldNames + " FROM " + p_sFrom;
                    //Deb : Removed the distinct used the single query as before 
                    //sSQLCount = "SELECT COUNT(*) FROM " + p_sFrom;
                    if (p_sWhere.Length != 0)
                        sSQLCount += " WHERE " + p_sWhere;

                    sSQLCount += ") T1";*/
                    //END srajindersin - 08/20/2012 - MITS 29229
                    
                    sSQLCount = "SELECT COUNT(*) FROM (" + sTempSql + ")";
                    // vsharma203. Added for SQL
                    if (m_sDBType.Equals(Constants.DB_SQLSRVR))
                    {
                        sSQLCount += " as CountSql";
                    } 
                    //vsharma203 code merged for RMA-10032 ends
					sSQLCount = PrepareTempSQL(sSQLCount);
                    m_lTotalRecords = long.Parse(p_objConn.ExecuteScalar(sSQLCount).ToString());
                }
                else
                {
                    if (m_lTotalRecords == 0)
                        m_lTotalRecords = lRecords;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    //Added by Shivendu
                    objReader.Dispose();
                }
				//mbahl3 Mits 30224
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
				//mbahl3 Mits 30224
            }

            return objSB.ToString();
        }


        //Added by Abhishek MITS 10651 Start
        /// <summary>
        /// Returns Complete Search results for printing pdf 
        /// </summary>
        /// <param name="p_objConn"></param>
        /// <param name="p_sFrom"></param>
        /// <param name="p_sFieldNames"></param>
        /// <param name="p_sWhere"></param>
        /// <param name="p_sOrderBy"></param>
        /// <returns>returns complete searched records seperated by |</returns>
        private string GetCompleteResultString(DbConnection p_objConn,
            string p_sFrom, string p_sFieldNames, string p_sWhere, string p_sOrderBy)
        {
            string sSQLCount = string.Empty;
            string sSQL = string.Empty;
            StringBuilder objSB = null;
            DbReader objReader = null;
            //nadim for 14233
            int iCountExtra = 0;
            string[] arrOrderBy = null;
            string sOrderBy = string.Empty;
            //nadim for 14233
            //skhare7 r8 Defects Removed for Diary Search
            bool blnSuccess = false;
            //nadim for 14233-start
            //..To Check if there is order by clause - add extra columns so that DISTINCT ORDER BY FIELD has to be in SELECT statement
            if (p_sFieldNames.Contains("DISTINCT"))
            {
                if (p_sOrderBy.Length > 0)
                {
                    sOrderBy = p_sOrderBy.Replace("ORDER BY ", "");
                    if (sOrderBy.Contains("DESC"))
                    {
                        sOrderBy = sOrderBy.Replace(" DESC", "");
                    }
                    p_sFieldNames = p_sFieldNames + "," + sOrderBy;
                    arrOrderBy = sOrderBy.Split(',');
                    iCountExtra = arrOrderBy.Length;
                }
            }
            //nadim for 14233

            //pmittal5 Mits 16820 10/08/09 - Sort on the basis of Short_code in case Order by clause is given on Code type field
            //Sort on the basis of State_Id in case Order by clause is given on State type field
            //Sort on the basis of Abbreviation in case Order by clause is given on Department
            //p_sOrderBy contains different values, so Changes need to be done in p_sWhere and p_sFrom accordingly
            //Abansal23 MITS 18835 :  Payee records not sorted
            if ((p_sOrderBy.IndexOf("SHORT_CODE") != -1 && p_sOrderBy.IndexOf("CD") != -1) || (p_sOrderBy.IndexOf("STATE_ID") != -1 && p_sOrderBy.IndexOf("ST") != -1) || (p_sOrderBy.IndexOf("ABBREVIATION") != -1 && p_sOrderBy.IndexOf("ORG") != -1) || (p_sOrderBy.IndexOf("TABLE_NAME") != -1 && p_sOrderBy.IndexOf("TBL") != -1))
            {
                string[] sOrderByArray = p_sOrderBy.Split(',');
                int iShortCodeIndex = 0;
                int iCodeAliasIndex = 0;
                string sFieldNum = string.Empty;
                string sFieldName = string.Empty;
                int iDotIndex = 0;
                for (int iOrderByCount = 0; iOrderByCount < sOrderByArray.Length; iOrderByCount++)
                {
                    iDotIndex = sOrderByArray[iOrderByCount].IndexOf(".") + 1;
                    sFieldName = sOrderByArray[iOrderByCount].Substring(iDotIndex, sOrderByArray[iOrderByCount].IndexOf(")") - iDotIndex).ToUpper();

                    switch (sFieldName)
                    {
                        case "SHORT_CODE":
                            iShortCodeIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("SHORT_CODE");
                            iCodeAliasIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("CD");
                            if (iShortCodeIndex != -1 && iCodeAliasIndex != -1)
                            {
                                sFieldNum = sOrderByArray[iOrderByCount].Substring(iCodeAliasIndex + 2, iShortCodeIndex - iCodeAliasIndex - 3);
                                //pmittal5 MITS 18353 When the Codes were sorted in Claim Search for Policy Status, the Code Fields with blank values were not coming in the search result.
                                //p_sFrom = p_sFrom + " LEFT OUTER JOIN CODES CD" + sFieldNum;
                                //p_sFrom = p_sFrom + " ON CD" + sFieldNum + ".CODE_ID = FLD" + sFieldNum;
                                JoinIt(ref p_sFrom, ref p_sWhere, "", "CODES", "FLD" + sFieldNum, "CODE_ID", "CD" + sFieldNum);
                            }
                            break;
                        case "STATE_ID":
                            iShortCodeIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("STATE_ID");
                            iCodeAliasIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("ST");
                            if (iShortCodeIndex != -1 && iCodeAliasIndex != -1)
                            {
                                sFieldNum = sOrderByArray[iOrderByCount].Substring(iCodeAliasIndex + 2, iShortCodeIndex - iCodeAliasIndex - 3);
                                JoinIt(ref p_sFrom, ref p_sWhere, "", "STATES", "FLD" + sFieldNum, "STATE_ROW_ID", "ST" + sFieldNum);
                            }
                            break;
                        case "ABBREVIATION":
                            iShortCodeIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("ABBREVIATION");
                            iCodeAliasIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("ORG");
                            if (iShortCodeIndex != -1 && iCodeAliasIndex != -1)
                            {
                                sFieldNum = sOrderByArray[iOrderByCount].Substring(iCodeAliasIndex + 3, iShortCodeIndex - iCodeAliasIndex - 4);
                                JoinIt(ref p_sFrom, ref p_sWhere, "", "ENTITY", "FLD" + sFieldNum, "ENTITY_ID", "ORG" + sFieldNum);
                            }
                            break;
                        //Abansal23 MITS 18835 :  Payee records not sorted Starts
                        case "TABLE_NAME":
                            iShortCodeIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("TABLE_NAME");
                            iCodeAliasIndex = sOrderByArray[iOrderByCount].ToUpper().IndexOf("TBL");
                            if (iShortCodeIndex != -1 && iCodeAliasIndex != -1)
                            {
                                sFieldNum = sOrderByArray[iOrderByCount].Substring(iCodeAliasIndex + 3, iShortCodeIndex - iCodeAliasIndex - 4);
                                JoinIt(ref p_sFrom, ref p_sWhere, "", "GLOSSARY_TEXT", "FLD" + sFieldNum, "TABLE_ID", "TBL" + sFieldNum);
                            }
                            break;
                        //Abansal23 MITS 18835 :  Payee records not sorted Ends
                    }
                }
            }
            //End - pmittal5

            sSQL = "SELECT " + p_sFieldNames + " FROM " + p_sFrom;
            if (p_sWhere.Length > 0)
                sSQL += " WHERE " + p_sWhere;

            
            //if (p_sOrderBy.Length > 0)
            //Added by sharishkumar for Mits 35326
            if (!string.IsNullOrEmpty(p_sOrderBy))
            {
                Regex regex = new Regex(@" AS FLD\d*");
                p_sOrderBy = regex.Replace(p_sOrderBy, string.Empty);
                sSQL += " " + p_sOrderBy;
            }
            //End Mits 35326
            sSQL = PrepareTempSQL(sSQL);
            objSB = new StringBuilder();

            //skhare7
            try
            {
                objReader = p_objConn.ExecuteReader(sSQL);

                // Mits 36350 commented to move the below code in while loop
                //if (AttachedRecordColumn != string.Empty)
                //{
                //    AttachedRecordColumn = AttachedRecordColumn.Replace("WPA_DIARY_ENTRY.ATTACH_TABLE | ", "");
                //    sColumnOrder = AttachedRecordColumn;
                //    //skhare7 R8 Diary Search defects removed
                //    iEntryId = Conversion.CastToType<int>(objReader.GetValue("ENTRY_ID").ToString(), out blnSuccess);


                //    string sRecordData = sRetrieveAttachedRecord((objReader.GetValue(sColumnOrder).ToString()), iEntryId);


                //}

                while (objReader.Read())
                {
                    //Mits 36350 starts
                    if (AttachedRecordColumn != string.Empty)
                    {
                        AttachedRecordColumn = AttachedRecordColumn.Replace("WPA_DIARY_ENTRY.ATTACH_TABLE | ", "");
                        sColumnOrder = AttachedRecordColumn;
                        //skhare7 R8 Diary Search defects removed
                        iEntryId = Conversion.CastToType<int>(objReader.GetValue("ENTRY_ID").ToString(), out blnSuccess);
                       
                     //Mits 36350 ends
                    }
                    //nadim for 14233-To Remove extra column that I have added above(objReader.FieldCount - iCountExtra)             
                    for (int j = 0; j < objReader.FieldCount - iCountExtra; j++)
                    {
                        if (sColumnOrder != string.Empty)
                        {
                            if (sColumnOrder == objReader.GetName(j).ToString())
                            {
                                string sRecordData = sRetrieveAttachedRecord((objReader.GetValue(sColumnOrder).ToString()), iEntryId);
                                objSB.Append(sRecordData);
                                objSB.Append("~*~");
                            }
                            else
                            {
                                objSB.Append(Conversion.ConvertObjToStr(objReader.GetValue(j)));
                                objSB.Append("~*~");
                            }
                        }
                        else
                        {
                            objSB.Append(Conversion.ConvertObjToStr(objReader.GetValue(j)));
                            objSB.Append("~*~");
                        }

                    }
                    objSB.Append("|^|");
                    //nadim for 14233
                }
                objReader.Close();

                sSQLCount = "SELECT COUNT(1) FROM " + p_sFrom;
                
                //sSQLCount = "SELECT COUNT(*) FROM " + p_sFrom;
                if (p_sWhere.Length != 0)
                    sSQLCount += " WHERE " + p_sWhere;
                sSQLCount = PrepareTempSQL(sSQLCount);
                m_lTotalRecords = long.Parse(p_objConn.ExecuteScalar(sSQLCount).ToString());

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }

            return objSB.ToString();
        }
        /// <summary>
        /// To Find the attached record data for diary search
        /// </summary>
        /// <returns>" Attached Record " as string </returns>

        public string sRetrieveAttachedRecord(string p_sAttachedTable, int p_iEntryId)
        {
            string sAttached = string.Empty;
            DbReader objReaderAttached = null;
            string sSql = string.Empty;
            int iAttachedRecordId = 0;
            LocalCache objCache = new LocalCache(m_sConnectionString,m_iClientId);
            //skhare7 R8 Defects Removed
            string sPIXmlFormName = string.Empty;
            string sFname = string.Empty;
            string sLname = string.Empty;
            string sName = string.Empty;
            long lTmp = 0;
            bool blnSuccess = false;

            try
            {
                sSql = "SELECT ATTACH_RECORDID FROM WPA_DIARY_ENTRY WHERE ENTRY_ID=" + p_iEntryId;

                objReaderAttached = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objReaderAttached != null)
                {
                    if (objReaderAttached.Read())
                    {
                        iAttachedRecordId = Conversion.CastToType<int>(objReaderAttached.GetValue("ATTACH_RECORDID").ToString(), out blnSuccess);
                    }
                }
                switch (p_sAttachedTable.ToLower())
                {
                    case "claim":
                        sAttached = p_sAttachedTable + ":" + GetStringAttachedData("CLAIM_NUMBER", "CLAIM", "CLAIM_ID=" + iAttachedRecordId.ToString(), m_sConnectionString);
                        break;

                    case "event":

                        sAttached = p_sAttachedTable + ":" + GetStringAttachedData("EVENT_NUMBER", "EVENT", "EVENT_ID=" + iAttachedRecordId.ToString(), m_sConnectionString);
                        break;

                    case "entity":
                        objCache.GetEntityInfo(p_iEntryId, ref sFname, ref sLname);
                        sName = sFname.Trim().Equals("") ? sLname : sFname + ", " + sLname;
                        sAttached = p_sAttachedTable + ":" + sName;
                        break;

                    case "claimant":
                        objCache.GetClaimantInfo(p_iEntryId, ref sFname, ref sLname);
                        sName = sFname.Trim().Equals("") ? sLname : sFname + ", " + sLname;
                        sAttached = p_sAttachedTable + ":" + sName;
                        break;

                    case "person_involved":
                        sPIXmlFormName = GetPiXMLFormName(p_iEntryId, ref sAttached);
                        //sAttached = p_sAttachedTable + ":" + sPIXmlFormName;
                        break;
                    case "funds":
                        sAttached = p_sAttachedTable + ":" + GetStringAttachedData("CTL_NUMBER", "FUNDS", "TRANS_ID=" + iAttachedRecordId.ToString(), m_sConnectionString);
                        break;

                    case "policy":
                        sAttached = p_sAttachedTable + ":" + GetStringAttachedData("POLICY_NUMBER", "POLICY", string.Format("POLICY_ID={0}", iAttachedRecordId.ToString()), m_sConnectionString);
                        break;
                    case "policy_enh":

                        sAttached = p_sAttachedTable + ":" + GetStringAttachedData("POLICY_NAME", "POLICY_ENH", string.Format("POLICY_ID={0}", iAttachedRecordId.ToString()), m_sConnectionString);
                        break;
                    default:
                        lTmp = objCache.GetTableId(p_sAttachedTable);
                        if (Conversion.CastToType<bool>((lTmp.ToString()), out blnSuccess))
                        {
                            sAttached = objCache.GetTableName(Conversion.CastToType<int>(lTmp.ToString(), out blnSuccess)) + " (" + iAttachedRecordId.ToString() + ")";
                        }
                        else
                        {
                            sAttached = p_sAttachedTable + iAttachedRecordId.ToString();
                        }
                        if (sAttached == "0")
                        {

                            sAttached = "<No Data>";
                        }
                        break;
                }



            }

            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objReaderAttached != null)
                {
                    objReaderAttached.Close();
                    objReaderAttached.Dispose();
                }
                //skhare7 Defects Removed For Diary sEarch
                if (objCache != null) objCache.Dispose();
            }
            return sAttached;

        }
        private string GetPiXMLFormName(long p_lPiRowId, ref string p_sPrompt)
        {//skhare7 R8 Defects Removed
            string sSql = string.Empty;
            string sPiType = string.Empty;
            string sFN = string.Empty;
            string sLN = string.Empty;
            string sSC = string.Empty;
            string sReturn = string.Empty;
            DbReader objReader = null;
            try
            {
                sSql = "SELECT CODES.SHORT_CODE, CODES_TEXT.CODE_DESC, ENTITY.LAST_NAME, ENTITY.FIRST_NAME" +
                    " FROM PERSON_INVOLVED, CODES, CODES_TEXT, ENTITY" +
                    " WHERE CODES.CODE_ID = PERSON_INVOLVED.PI_TYPE_CODE" +
                    " AND CODES_TEXT.CODE_ID = CODES.CODE_ID " +
                    " AND PERSON_INVOLVED.PI_EID = ENTITY.ENTITY_ID" +
                    " AND PERSON_INVOLVED.PI_ROW_ID = " + p_lPiRowId.ToString() +
                    " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sSC = objReader.GetValue(0) == System.DBNull.Value ? "" : Conversion.ConvertObjToStr(objReader.GetValue(0));
                        sPiType = objReader.GetValue(1) == System.DBNull.Value ? "" : Conversion.ConvertObjToStr(objReader.GetValue(1));
                        sLN = objReader.GetValue(2) == System.DBNull.Value ? "" : Conversion.ConvertObjToStr(objReader.GetValue(2));
                        sFN = objReader.GetValue(3) == System.DBNull.Value ? "" : Conversion.ConvertObjToStr(objReader.GetValue(3));

                    }
                }
                if (sFN.Trim().Length > 0)
                {
                    sLN = sLN + ", " + sFN;
                    p_sPrompt = sPiType + ": " + sLN;
                }
                switch (sSC.ToUpper())
                {
                    case "E":
                        sReturn = "piemployee";
                        break;
                    case "MED":
                        sReturn = "pimedstaff";
                        break;
                    case "O":
                        sReturn = "piother";
                        break;
                    case "P":
                        sReturn = "piqpatient";
                        break;
                    case "PHYS":
                        sReturn = "piphysician";
                        break;
                    case "W":
                        sReturn = "piwitness";
                        break;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.GetPiXMLFormName.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (sReturn);
        }
        //skhare7
        private string GetStringAttachedData(string p_sFieldName, string p_sTableName, string p_sCriteria, string p_sConnectString)
        {
            string sGetSingleLong = "";
            string sSql = "";
            DbReader objReader = null;

            try
            {
                if (p_sCriteria.Trim().Length == 0)
                {
                    p_sCriteria = "";
                }
                else
                {
                    p_sCriteria = " WHERE " + p_sCriteria;
                }
                sSql = "SELECT " + p_sFieldName + " FROM " + p_sTableName + p_sCriteria;
                objReader = DbFactory.GetDbReader(p_sConnectString, sSql);

                if (objReader != null)
                    if (objReader.Read())
                    {
                        if (p_sTableName == "CLAIMANT, ENTITY")
                        {
                            if (Conversion.ConvertObjToStr(objReader.GetValue(1)).Trim() != "")
                                sGetSingleLong = Conversion.ConvertObjToStr(objReader.GetValue(0)) + ", " + Conversion.ConvertObjToStr(objReader.GetValue(1));
                            else
                                sGetSingleLong = Conversion.ConvertObjToStr(objReader.GetValue(0));
                        }
                        else
                        {
                            if (objReader.GetValue(0) is System.DBNull)
                            {
                                sGetSingleLong = "";
                            }
                            else
                            {
                                sGetSingleLong = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            }
                        }
                    }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.GetSingleString.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (sGetSingleLong);
        }
        /// <summary>
        /// To Evaluate Distinct String 
        /// </summary>
        /// <returns>" DISTINCT " as string </returns>
        //Asif MITS 13218 Start
        //made function private to public because its being used in another class of same class library
        //in order to fix MITS 13218
        //private string sEvalDistinct(string sFieldsList)
        //Asif MITS 13218 End
        public string sEvalDistinct(string sFieldsList)
        {
            //Nitesh: MITS-7618 
            Hashtable textFieldColloction = new Hashtable();

            //Entity 
            textFieldColloction.Add(1, "COMMENTS");
            textFieldColloction.Add(2, "HTMLCOMMENTS");
            //Claimant
            textFieldColloction.Add(3, "INJURY_DESCRIPTION");
            textFieldColloction.Add(4, "DAMAGE_DESCRIPTION");
            textFieldColloction.Add(5, "DAMAGE_DESCRIPTION");
            //Unit_X_Claim
            textFieldColloction.Add(8, "DAMAGE");
            //Vehicle
            textFieldColloction.Add(9, "TYPE_OF_SERVICE");
            textFieldColloction.Add(10, "INSURANCE_COVERAGE");
            //CLAIM_X_LITIGATION
            textFieldColloction.Add(11, "DEMAND_ALLEGATIONS");
            //EXPERT
            textFieldColloction.Add(12, "SPECIALTY");
            textFieldColloction.Add(13, "CASE_DESCRIPTION");
            //ADJUST_DATED_TEXT
            textFieldColloction.Add(14, "DATED_TEXT");
            textFieldColloction.Add(15, "DATED_TEXT_HTMLCOMMENTS");
            //FUNDS
            textFieldColloction.Add(16, "NOTES");
            //PERSON_INVOLVED
            textFieldColloction.Add(17, "REHAB_TEXT");
            textFieldColloction.Add(18, "DESC_BY_WITNESS");
            textFieldColloction.Add(19, "ADMISSION_REASON");
            textFieldColloction.Add(20, "WHY_AT_FACILITY");
            textFieldColloction.Add(21, "OTHER_TREATMENTS");
            textFieldColloction.Add(22, "OTHER_MEDCOND");
            //EVENT
            textFieldColloction.Add(23, "EVENT_DESCRIPTION");
            textFieldColloction.Add(24, "LOCATION_AREA_DESC");
            textFieldColloction.Add(25, "PHYS_NOTES");
            textFieldColloction.Add(26, "EVENT_DESCRIPTION_HTMLCOMMENTS");
            //POLICY_X_CVG_TYPE
            textFieldColloction.Add(27, "REMARKS");
            textFieldColloction.Add(28, "EXCEPTIONS");

            textFieldColloction.Add(29, "PA_COMMENTS");
            textFieldColloction.Add(30, "PA_REC_ACTIONS");
            textFieldColloction.Add(31, "IR_COMMENTS");
            textFieldColloction.Add(32, "IR_REC_ACTIONS");
            textFieldColloction.Add(33, "CD_COMMENTS");
            textFieldColloction.Add(34, "CD_REC_ACTIONS");
            textFieldColloction.Add(35, "QM_COMMENTS");
            textFieldColloction.Add(36, "QM_REC_ACTIONS");


            foreach (Object oField in textFieldColloction)
            {
                if (sFieldsList.IndexOf(((DictionaryEntry)oField).Value.ToString()) >= 0)
                    return string.Empty;
            }

            return " DISTINCT ";

        }

        //Overloaded function created by pmittal5 Mits 18356 11/13/09 - If Free Text Supplemental fields are there, DISTINCT is not added 
        /// <summary>
        /// To Evaluate Distinct String 
        /// </summary>
        /// <returns>" DISTINCT " as string </returns>
        public string sEvalDistinct(string sFieldsList, ArrayList arrlstTextSuppFields)
        {
            //Nitesh: MITS-7618 
            Hashtable textFieldColloction = new Hashtable();

            //Entity 
            textFieldColloction.Add(1, "COMMENTS");
            textFieldColloction.Add(2, "HTMLCOMMENTS");
            //Claimant
            textFieldColloction.Add(3, "INJURY_DESCRIPTION");
            textFieldColloction.Add(4, "DAMAGE_DESCRIPTION");
            textFieldColloction.Add(5, "DAMAGE_DESCRIPTION");
            //Unit_X_Claim
            textFieldColloction.Add(8, "DAMAGE");
            //Vehicle
            textFieldColloction.Add(9, "TYPE_OF_SERVICE");
            textFieldColloction.Add(10, "INSURANCE_COVERAGE");
            //CLAIM_X_LITIGATION
            textFieldColloction.Add(11, "DEMAND_ALLEGATIONS");
            //EXPERT
            textFieldColloction.Add(12, "SPECIALTY");
            textFieldColloction.Add(13, "CASE_DESCRIPTION");
            //ADJUST_DATED_TEXT
            textFieldColloction.Add(14, "DATED_TEXT");
            textFieldColloction.Add(15, "DATED_TEXT_HTMLCOMMENTS");
            //FUNDS
            textFieldColloction.Add(16, "NOTES");
            //PERSON_INVOLVED
            textFieldColloction.Add(17, "REHAB_TEXT");
            textFieldColloction.Add(18, "DESC_BY_WITNESS");
            textFieldColloction.Add(19, "ADMISSION_REASON");
            textFieldColloction.Add(20, "WHY_AT_FACILITY");
            textFieldColloction.Add(21, "OTHER_TREATMENTS");
            textFieldColloction.Add(22, "OTHER_MEDCOND");
            //EVENT
            textFieldColloction.Add(23, "EVENT_DESCRIPTION");
            textFieldColloction.Add(24, "LOCATION_AREA_DESC");
            textFieldColloction.Add(25, "PHYS_NOTES");
            textFieldColloction.Add(26, "EVENT_DESCRIPTION_HTMLCOMMENTS");
            //POLICY_X_CVG_TYPE
            textFieldColloction.Add(27, "REMARKS");
            textFieldColloction.Add(28, "EXCEPTIONS");

            textFieldColloction.Add(29, "PA_COMMENTS");
            textFieldColloction.Add(30, "PA_REC_ACTIONS");
            textFieldColloction.Add(31, "IR_COMMENTS");
            textFieldColloction.Add(32, "IR_REC_ACTIONS");
            textFieldColloction.Add(33, "CD_COMMENTS");
            textFieldColloction.Add(34, "CD_REC_ACTIONS");
            textFieldColloction.Add(35, "QM_COMMENTS");
            textFieldColloction.Add(36, "QM_REC_ACTIONS");
            
            //Amitosh
            textFieldColloction.Add(37, "NOTE_MEMO_CARETECH");
            //srajindersin 5/20/2014
            textFieldColloction.Add(38, "LOSS_DESCRIPTION");

            foreach (Object oField in textFieldColloction)
            {
                if (sFieldsList.IndexOf(((DictionaryEntry)oField).Value.ToString()) >= 0)
                    return string.Empty;
            }

            foreach (Object oTextSuppField in arrlstTextSuppFields)
            {
                if (sFieldsList.IndexOf(oTextSuppField.ToString()) >= 0)
                    return string.Empty;
            }

            return " DISTINCT ";

        }

        #region Create Alias
        /// <summary>
        /// This function creates the WHERE clause for the multiple supplemental values.
        /// </summary>
        /// <param name="p_arrlstArray">Arraylist containing name of the query tables</param>
        /// <param name="p_sFrom">FROM part of the query</param>
        /// <param name="p_sWhere">WHERE part of the query</param>
        /// <param name="p_sField">Database field</param>
        private void CreateSMVAlias(ArrayList p_arrlstArray, ref string p_sFrom, ref string p_sWhere, string p_sField)
        {
            //New Parameter Added Due to Multi-Code enhancement
            int iBound = 0;
            try
            {
                //iBound = p_arrlstArray.Count;
                for (iBound = 0; iBound < p_arrlstArray.Count; iBound++)
                {
                    if (p_arrlstArray[iBound].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Changes made as per RM World code
                        p_sFrom = p_sFrom + "," + p_arrlstArray[iBound];
                        if (p_sWhere != "")
                            p_sWhere = p_sWhere + " AND ";
                        p_sWhere = p_sWhere + p_sField + " = " + p_arrlstArray[iBound].ToString().Substring(17) + ".RECORD_ID";
                    }
                }
            }
            catch
            {
                throw new CollectionException(Globalization.GetString("SearchResults.SQLError", m_iClientId));
            }
        }
        #endregion

        #region Event table query generation
        /// <summary>
        /// This function forms the FROM & WHERE part of the SQL query for the EVENT module.
        /// </summary>
        /// <param name="p_arrlstArray">Arraylist containing the query tables</param>
        /// <param name="p_sFrom">FROM part of the query</param>
        /// <param name="p_sWhere">WHERE part of the query</param>
        private void EventCriteriaTables(ArrayList p_arrlstArray, ref string p_sFrom, ref string p_sWhere, ref string p_sCriteria)  // csingh7 R6 Claim Comment Enhancement : Adding p_scriteria parameter
        {
            int iIndex = 0;
            string sTemp = "";
            try
            {

                //Comment related     csingh7 R6 Claim Comment enhancement
                if (Utilities.ExistsInArray(p_arrlstArray, "COMMENTS_TEXTEVENT"))
                {
                    p_sFrom = p_sFrom + ",COMMENTS_TEXT COMMENTS_TEXTEVENT";

                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    //iIndex = p_sCriteria.IndexOf("COMMENTS_TEXTEVENT");

                    iIndex = p_sCriteria.IndexOf("UPPER(LTRIM(RTRIM(COMMENTS_TEXTEVENT");
                    if (iIndex == -1)
                        iIndex = p_sCriteria.IndexOf("LTRIM(RTRIM(COMMENTS_TEXTEVENT");
                    if (iIndex == -1)
                        iIndex = p_sCriteria.IndexOf("COMMENTS_TEXTEVENT");

                    if (iIndex != -1)
                    {
                        iIndex = iIndex - 1;    // To include the bracket '(' before COMMENTCLAIM
                        sTemp = p_sCriteria.Substring(iIndex);
                        p_sCriteria = p_sCriteria.Replace(sTemp, "");

                        iIndex = sTemp.IndexOf("AND");
                        if (iIndex != -1)
                        {
                            p_sCriteria = p_sCriteria + sTemp.Substring(iIndex + 3);
                            sTemp = sTemp.Substring(0, iIndex);
                        }
                        p_sCriteria = p_sCriteria.TrimEnd();
                        if (p_sCriteria.EndsWith("AND"))
                            p_sCriteria = p_sCriteria.Substring(0, p_sCriteria.Length - 3);
                    }
                    if (Utilities.ExistsInArray(p_arrlstArray, "CLAIM"))
                        p_sWhere = p_sWhere + "(CLAIM.EVENT_ID = COMMENTS_TEXTEVENT.ATTACH_RECORDID AND COMMENTS_TEXTEVENT.ATTACH_TABLE LIKE 'EVENT' AND " + sTemp + ")";
                    else if (Utilities.ExistsInArray(p_arrlstArray, "EVENT"))
                        p_sWhere = p_sWhere + "(EVENT.EVENT_ID = COMMENTS_TEXTEVENT.ATTACH_RECORDID AND COMMENTS_TEXTEVENT.ATTACH_TABLE LIKE 'EVENT' AND " + sTemp + ")";

                }
                if (Utilities.ExistsInArray(p_arrlstArray, "EVENT_X_OSHA"))
                {
                    p_sFrom = p_sFrom + ",EVENT_X_OSHA";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "EVENT.EVENT_ID = EVENT_X_OSHA.EVENT_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "RPTD_ENTITY"))
                {
                    //link in RPTD_ENTITY
                    p_sFrom = p_sFrom + ", ENTITY RPTD_ENTITY";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "EVENT.RPTD_BY_EID = RPTD_ENTITY.ENTITY_ID";
                }

                //Support for event category (parent of indicator)
                if (Utilities.ExistsInArray(p_arrlstArray, "EVENT_CAT"))
                {
                    p_sFrom = p_sFrom + ", CODES EVENT_CAT";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "EVENT.EVENT_IND_CODE = EVENT_CAT.CODE_ID";
                }

                //PI
                if (Utilities.ExistsInArray(p_arrlstArray, "PERSON_INVOLVED") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PI_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PI_X_BODY_PART") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PI_X_INJURY") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PATIENT") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PATIENT_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PATIENT_DRG_CODES") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PATIENT_DIAGNOSIS") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PATIENT_ACT_TAKEN") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PATIENT_PROCEDURE") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PATIENT_SUPP") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PHYSICIAN") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PHYS_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PHYS_SUPP") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PHYS_SUB_SPECIALTY") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PHYS_PRIVS") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PHYS_CERTS") ||
                    Utilities.ExistsInArray(p_arrlstArray, "MED_STAFF") ||
                    Utilities.ExistsInArray(p_arrlstArray, "MED_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstArray, "MED_STAFF_SUPP") ||
                    Utilities.ExistsInArray(p_arrlstArray, "STAFF_PRIVS") ||
                    Utilities.ExistsInArray(p_arrlstArray, "STAFF_CERTS") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PAT_PRI_PHYS") ||
                    //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                    Utilities.ExistsInArray(p_arrlstArray, "PI_SUPP"))
                {
                    p_sFrom = p_sFrom + ",PERSON_INVOLVED";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "EVENT.EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                }

                //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                if (Utilities.ExistsInArray(p_arrlstArray, "PI_ENTITY") || Utilities.ExistsInArray(p_arrlstArray, "PI_SUPP"))
                {
                    p_sFrom = p_sFrom + ", ENTITY PI_ENTITY";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PI_EID = PI_ENTITY.ENTITY_ID";
                }

                //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                if (Utilities.ExistsInArray(p_arrlstArray, "PI_SUPP"))
                {
                    p_sFrom = p_sFrom + ", PI_SUPP";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PI_SUPP.PI_ROW_ID = PERSON_INVOLVED.PI_ROW_ID";
                }


                if (Utilities.ExistsInArray(p_arrlstArray, "EVENT_SUPP"))
                {
                    p_sFrom = p_sFrom + ",EVENT_SUPP";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "EVENT.EVENT_ID = EVENT_SUPP.EVENT_ID";
                }

                //Added event_qm records to search_dictionary
                if (Utilities.ExistsInArray(p_arrlstArray, "EVENT_QM"))
                {
                    p_sFrom = p_sFrom + ",EVENT_QM";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "EVENT.EVENT_ID = EVENT_QM.EVENT_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "PI_X_BODY_PART"))
                {
                    p_sFrom = p_sFrom + ", PI_X_BODY_PART";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PI_ROW_ID = PI_X_BODY_PART.PI_ROW_ID";
                }
                //skhare7 R8 enhancement

                if (Utilities.ExistsInArray(p_arrlstArray, "PI_X_INJURY"))
                {
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    p_sFrom = p_sFrom + ", PI_X_INJURY";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PI_ROW_ID = PI_X_INJURY.PI_ROW_ID";
                }
                //skhare7
                //QUEST/Win support **
                if (Utilities.ExistsInArray(p_arrlstArray, "PATIENT") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PATIENT_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PAT_PRI_PHYS"))
                {
                    //Start by Shivendu for MITS 9965---Partial
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //End by Shivendu for MITS 9965---Partial
                    p_sFrom = p_sFrom + ", PATIENT, ENTITY | PATIENT_ENTITY";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PATIENT_ID = PATIENT.PATIENT_ID AND PATIENT.PATIENT_EID = PATIENT_ENTITY.ENTITY_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "PAT_PRI_PHYS"))
                {
                    //Start by Shivendu for MITS 9965---Partial
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //End by Shivendu for MITS 9965---Partial
                    p_sFrom = p_sFrom + ", ENTITY | PAT_PRI_PHYS";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PATIENT.PRI_PHYSICIAN_EID = PAT_PRI_PHYS.ENTITY_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "PATIENT_DRG_CODES"))
                {
                    //Start by Shivendu for MITS 9965---Partial
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //End by Shivendu for MITS 9965---Partial
                    p_sFrom = p_sFrom + ", PATIENT_DRG_CODES";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PATIENT_ID = PATIENT_DRG_CODES.PATIENT_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "PATIENT_DIAGNOSIS"))
                {
                    //Start by Shivendu for MITS 9965---Partial
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //End by Shivendu for MITS 9965---Partial
                    p_sFrom = p_sFrom + ", PATIENT_DIAGNOSIS";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PATIENT_ID = PATIENT_DIAGNOSIS.PATIENT_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "PATIENT_ACT_TAKEN"))
                {
                    //Start by Shivendu for MITS 9965---Partial
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //End by Shivendu for MITS 9965---Partial
                    p_sFrom = p_sFrom + ", PATIENT_ACT_TAKEN";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PATIENT_ID = PATIENT_ACT_TAKEN.PATIENT_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "PATIENT_PROCEDURE"))
                {
                    //Start by Shivendu for MITS 9965---Partial
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //End by Shivendu for MITS 9965---Partial
                    p_sFrom = p_sFrom + ", PATIENT_PROCEDURE";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PATIENT_ID = PATIENT_PROCEDURE.PATIENT_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "PATIENT_SUPP"))
                {
                    //Start by Shivendu for MITS 9965---Partial
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //End by Shivendu for MITS 9965---Partial
                    p_sFrom = p_sFrom + ", PATIENT_SUPP";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PATIENT_ID = PATIENT_SUPP.PATIENT_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "PHYSICIAN") ||
                    Utilities.ExistsInArray(p_arrlstArray, "PHYS_ENTITY"))
                {
                    //zalam 07/14/208 Mits:-12801 Start
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //zalam 07/14/208 Mits:-12801 End
                    p_sFrom = p_sFrom + ", PHYSICIAN, ENTITY | PHYS_ENTITY";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PI_EID = PHYSICIAN.PHYS_EID AND PHYSICIAN.PHYS_EID = PHYS_ENTITY.ENTITY_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "PHYS_SUPP"))
                {
                    //zalam 07/14/208 Mits:-12801 Start
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //zalam 07/14/208 Mits:-12801 End
                    p_sFrom = p_sFrom + ", PHYS_SUPP";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PI_EID = PHYS_SUPP.PHYS_EID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "PHYS_SUB_SPECIALTY"))
                {
                    //zalam 07/14/208 Mits:-12801 Start
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //zalam 07/14/208 Mits:-12801 End
                    p_sFrom = p_sFrom + ", PHYS_SUB_SPECIALTY";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PI_EID = PHYS_SUB_SPECIALTY.PHYS_EID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "PHYS_PRIVS"))
                {
                    //zalam 07/14/208 Mits:-12801 Start
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //zalam 07/14/208 Mits:-12801 End
                    p_sFrom = p_sFrom + ", PHYS_PRIVS";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PI_EID = PHYS_PRIVS.PHYS_EID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "PHYS_CERTS"))
                {
                    //zalam 07/14/208 Mits:-12801 Start
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //zalam 07/14/208 Mits:-12801 End
                    p_sFrom = p_sFrom + ", PHYS_CERTS";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PI_EID = PHYS_CERTS.PHYS_EID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "MED_STAFF") || Utilities.ExistsInArray(p_arrlstArray, "MED_ENTITY"))
                {
                    //Start by Shivendu for MITS 9965---Partial
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //End by Shivendu for MITS 9965---Partial
                    p_sFrom = p_sFrom + ", MED_STAFF,ENTITY | MED_ENTITY";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PI_EID = MED_STAFF.STAFF_EID AND MED_STAFF.STAFF_EID = MED_ENTITY.ENTITY_ID";
                }

                //plug in other tables which have criteria
                if (Utilities.ExistsInArray(p_arrlstArray, "MED_STAFF_SUPP"))
                {
                    p_sFrom = p_sFrom + ", MED_STAFF_SUPP";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PI_EID = MED_STAFF_SUPP.STAFF_EID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "STAFF_PRIVS"))
                {
                    //Start by Shivendu for MITS 9764
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //End by Shivendu for MITS 9764
                    p_sFrom = p_sFrom + ", STAFF_PRIVS";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PI_EID = STAFF_PRIVS.STAFF_EID";
                }

                if (Utilities.ExistsInArray(p_arrlstArray, "STAFF_CERTS"))
                {
                    //Start by Shivendu for MITS 9764
                    if ((string)p_arrlstArray[0] == "CLAIM")
                        p_sWhere = p_sWhere.Replace("EVENT.", "CLAIM.");
                    //End by Shivendu for MITS 9764
                    p_sFrom = p_sFrom + ", STAFF_CERTS";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "PERSON_INVOLVED.PI_EID = STAFF_CERTS.STAFF_EID";
                }

                // Mihika 16-Feb-2006 Changes related to MITS 5034
                if (Utilities.ExistsInArray(p_arrlstArray, "EVENT_X_OUTCOME"))
                {
                    p_sFrom = p_sFrom + ", EVENT_X_OUTCOME";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    //asharma326 7/3/2013 MITS-19829
                    //p_sWhere = p_sWhere + "EVENT.EVENT_ID = EVENT_X_OUTCOME.ENTITY_ID";
                    p_sWhere = p_sWhere + "EVENT.EVENT_ID = EVENT_X_OUTCOME.EVENT_ID";
                }
                //Event Intervention
                if (Utilities.ExistsInArray(p_arrlstArray, "EVENT_X_INTERVENT") ||
                    Utilities.ExistsInArray(p_arrlstArray, "EVENT_X_INT_CODES") ||
                    Utilities.ExistsInArray(p_arrlstArray, "INT_EB_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstArray, "INT_EMP_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstArray, "INT_SUP_ENTITY") ||
                    Utilities.ExistsInArray(p_arrlstArray, "EVENT_X_INT_SUPP"))
                {
                    p_sFrom = p_sFrom + ", EVENT_X_INTERVENT";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "EVENT.EVENT_ID = EVENT_X_INTERVENT.EVENT_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstArray, "EVENT_X_INT_SUPP"))
                {
                    p_sFrom = p_sFrom + ", EVENT_X_INT_SUPP";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "EVENT_X_INTERVENT.INTERVENT_ROW_ID = EVENT_X_INT_SUPP.INTERVENT_ROW_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstArray, "EVENT_X_INT_CODES"))
                {
                    p_sFrom = p_sFrom + ", EVENT_X_INT_CODES";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "EVENT_X_INTERVENT.INTERVENT_ROW_ID = EVENT_X_INT_CODES.INTERVENT_ROW_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstArray, "INT_EB_ENTITY"))
                {
                    p_sFrom = p_sFrom + ", ENTITY INT_EB_ENTITY";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "EVENT_X_INTERVENT.EVALUATOR_EID = INT_EB_ENTITY.ENTITY_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstArray, "INT_EMP_ENTITY"))
                {
                    p_sFrom = p_sFrom + ", ENTITY INT_EMP_ENTITY";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "EVENT_X_INTERVENT.EMPLOYEE_EID = INT_EMP_ENTITY.ENTITY_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstArray, "INT_SUP_ENTITY"))
                {
                    p_sFrom = p_sFrom + ", ENTITY INT_SUP_ENTITY";
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + "EVENT_X_INTERVENT.SUPERVISOR_EID = INT_SUP_ENTITY.ENTITY_ID";
                }

            }
            catch (Exception p_objException)
            {
                throw new StringException(Globalization.GetString("SearchResults.EventCriteriaTables.EventError", m_iClientId), p_objException);
            } //Empty catch block as simple string manipulation is taking place.
        }
        #endregion

        #region Execute SQL query
        /// <summary>
        /// This function executes the SQL query and creates the temporary tables for the specific search module.
        /// </summary>
        /// <param name="p_sSQL">SQL query containing statement for the creation of temp table</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void ExecSearchSQL(string p_sSQL, ref DbConnection p_objConn)
        {
            string sTmp = "";
            string sSQLMulti = "";
            string sSQLMultiTemp = "";
            int iPos = 0;
            DbTransaction objTrans = null;
            DbCommand objCommand = null;

            try
            {
                if ((m_sDBType == Constants.DB_DB2) && (p_sSQL.IndexOf("[INTO ") != -1))   //Special case for DB2 temp table creation
                {
                    sTmp = PrepareTempSQL(p_sSQL);

                    //check for multi-statement case - db2 requires a multi-step process to do a "select into" temp table creation
                    iPos = sTmp.IndexOf(";");
                    if (iPos > -1)
                    {
                        sSQLMulti = sTmp.Substring(0, iPos - 1).Trim();
                        sSQLMultiTemp = sTmp.Substring(iPos + 1).Trim();
                    }
                    else
                        sSQLMulti = sTmp.Trim();

                    //Execute SQL

                    objCommand = p_objConn.CreateCommand();
                    objTrans = p_objConn.BeginTransaction();
                    objCommand.Transaction = objTrans; //transaction needed in case 'not logged initially' is used
                    objCommand.CommandText = sSQLMulti;
                    objCommand.ExecuteNonQuery();

                    if (sSQLMultiTemp.Length > 0)
                    {
                        objCommand.CommandText = sSQLMultiTemp;
                        objCommand.ExecuteNonQuery();
                    }
                    objTrans.Commit();
                }
                else
                {
                    p_objConn.ExecuteNonQuery(PrepareTempSQL(p_sSQL));
                }
            }
            catch (Exception p_objException)
            {
                if (objTrans != null)
                    objTrans.Rollback();

                throw new RMAppException(Globalization.GetString("SearchResults.ExecSearchSQL.CreateTemp", m_iClientId), p_objException);
            }
            finally
            {
                if (objTrans != null) objTrans.Dispose();
                objCommand = null;
            }
        }
        #endregion

        #region Prepare SQL
        /// Name		: DeleteOracleTables
        /// Author		: Parag Sarin
        /// Date Created: 06/21/2006		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function deletes ORACLE tables
        /// </summary>
        /// <param name="p_sSql">Sql query</param>
        /// <param name="p_sTableName">Table name</param>
        internal void DeleteOracleTables(string p_sSql)
        {
            //bConnection objConn=null;
            try
            {
                //objConn = DbFactory.GetDbConnection(m_sConnectionString);
                //objConn.Open();
                // p_sSql=p_sSql+" "+"NOWAIT";
                //p_objConn.ExecuteNonQuery(p_sSql);
                this.p_Connection.ExecuteNonQuery(p_sSql);
                //objConn.Close();
            }
            catch (RMAppException p_objException)
            {
                //throw p_objException;
            }
            catch (Exception p_objException)
            {
                //throw p_objException;
            }
            finally
            {
                //if (objConn != null)
                //{
                //    objConn.Close();
                //    objConn.Dispose();
                //}
            }
        }
        /// <summary>
        /// This function generates the database specific SQL queries.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <returns>String containing the database specific query.</returns>
        private string PrepareTempSQL(string p_sSQL)
        {
            int iPos = 0;
            int iPosTemp = 0;
          //mini performance tseting 
		  //int iPosNext = 0;
            string sTemp = "";
            string sTempName = "";
		  //mini performance tseting 
          // string sSQLTemp = "";
            string sShortTempName = "";
            string sShortUserID = "";

            string sTempOra = "";
            try
            {
                sTemp = p_sSQL;
                iPos = sTemp.IndexOf("[INTO ");

                if (iPos != -1)
                {
                    iPosTemp = sTemp.IndexOf("]", iPos + 6);
                    sTempName = sTemp.Substring(iPos + 6, iPosTemp - iPos - 6);
                    switch (m_sDBType)
                    {
                        case Constants.DB_SQLSRVR:
                            sTemp = sTemp.Substring(0, iPos - 1) + " INTO #" + sTempName + " " + sTemp.Substring(iPosTemp + 1);
                            break;
                        case Constants.DB_ORACLE:
                            sTempName = "S_" + sTempName;
                            //Raman: 07/04/2007 : Modifying logic for oracle as this fails in many scenarios
                            /*
							if ((sTempName.Length > 21) || m_lUserId.ToString().Length > 7)
							{
								sTempOra="DROP TABLE " + sTempName.Substring(0, 21) + "_" + m_lUserId.ToString().Substring(0, 8);
								DeleteOracleTables(sTempOra);
								sTemp = "CREATE TABLE " + sTempName.Substring(0, 21) + "_" + m_lUserId.ToString().Substring(0, 8) + " NOLOGGING AS " + sTemp.Substring(0,iPos - 1) + " " + sTemp.Substring (iPosTemp + 1);
							}
							else
							{
								sTempOra="DROP TABLE " + sTempName + "_" + m_lUserId.ToString();
								DeleteOracleTables(sTempOra);
								sTemp = "CREATE TABLE " + sTempName + "_" + m_lUserId.ToString() + " NOLOGGING AS " + sTemp.Substring(0,iPos - 1) + " " + sTemp.Substring (iPosTemp + 1);
							}
                            */
                            if (sTempName.Length > 21)
                                sShortTempName = sTempName.Substring(0, 21);
                            else
                                sShortTempName = sTempName;
                            if (m_lUserId.ToString().Length > 7)
                                sShortUserID = m_lUserId.ToString().Substring(0, 8);
                            else
                                sShortUserID = m_lUserId.ToString();

                            //sTempOra = "DROP TABLE " + sShortTempName + "_" + sShortUserID;
                            //DeleteOracleTables(sTempOra);
                            CleanupSearchStoredProcOracle(sShortTempName + "_" + sShortUserID);
                            sTemp = "CREATE TABLE " + sShortTempName + "_" + sShortUserID + " NOLOGGING AS " + sTemp.Substring(0, iPos - 1) + " " + sTemp.Substring(iPosTemp + 1);

                            break;
                    }
                }
                //replace temp table create (depending on DB)
                iPos = sTemp.IndexOf("[");

                while (iPos != -1)
                {
                    iPosTemp = sTemp.IndexOf("]");
                    sTempName = sTemp.Substring(iPos + 1, iPosTemp - iPos - 1).Trim();
                    switch (m_sDBType)
                    {
                        case Constants.DB_SQLSRVR:
                            sTempName = "#" + sTempName;
                            break;
                        case Constants.DB_ORACLE:
                            sTempName = "S_" + sTempName;
                            //Raman: 07/04/2007 : Modifying logic for oracle as this fails in many scenarios
                            /*
                            if ((sTempName.Length > 21) || m_lUserId.ToString().Length > 7)
								sTempName = sTempName.Substring(0, 21) + "_" + m_lUserId.ToString().Substring(0, 8);
							else
								sTempName = sTempName + "_" + m_lUserId.ToString();
                             */
                            if (sTempName.Length > 21)
                                sShortTempName = sTempName.Substring(0, 21);
                            else
                                sShortTempName = sTempName;
                            if (m_lUserId.ToString().Length > 7)
                                sShortUserID = m_lUserId.ToString().Substring(0, 8);
                            else
                                sShortUserID = m_lUserId.ToString();

                            sTempName = sShortTempName + "_" + sShortUserID;

                            break;
                    }
                    sTemp = sTemp.Substring(0, iPos) + sTempName + sTemp.Substring(iPosTemp + 1);
                    iPos = sTemp.IndexOf("[");
                }

                //Replace | with AS or blank (depending on db)
                if (m_sDBType == Constants.DB_ORACLE)  //aaggarwal29 : MITS 37120: need to prevent || from being replaced in Oracle as it is used for concatenation
                {
                    sTemp = sTemp.Replace("||", "^%^~rmA^%^~");
                    sTemp = sTemp.Replace("|", " ");
                    sTemp = sTemp.Replace("^%^~rmA^%^~", " || ");
                }
                else
                {
                    sTemp = sTemp.Replace("|", " ");
                }


                //Alter Index names on Oracle
                if (m_sDBType == Constants.DB_ORACLE)
                {
                    if ((sTemp.Substring(0, 13) == "CREATE INDEX " && (sTemp.Substring(13, 1)) != " "))
                    {
                        //p_objConn.ExecuteNonQuery (PrepareTempSQL("CREATE INDEX ATTEMP_" + m_lUserId + " ON [" + sTempTable + "](" + p_sSysKeyField + ")"));
                        int index = sTemp.IndexOf("ON");
                        string sname = "";
                        if (index != -1)
                        {
                            sname = sTemp.Substring(14);
                            sname = sname.Split(" ".ToCharArray())[0];

                        }
                        sTemp = "CREATE INDEX S_" + sTemp.Substring(14);
                        if (sname != "")
                        {
                            sTempOra = "DROP INDEX " + "S_" + sname;
                            DeleteOracleTables(sTempOra);
                        }
                    }
                }
                //Deb MITS 25775
                //int indexTaxID = sTemp.IndexOf("ENTITY.TAX_ID");
                //if (objSysSettingsGlobal.MaskSSN && indexTaxID > -1)
                //{
                //    if (sTemp.Substring(indexTaxID - 1, 1) == "," || sTemp.Substring(indexTaxID - 1, 1) == " ")
                //    {
                //        sTemp = sTemp.Replace("ENTITY.TAX_ID", " CASE WHEN ENTITY.TAX_ID IS NOT NULL THEN  '###-##-####' END ");
                //    }
                //    else
                //    {
                //        indexTaxID = indexTaxID - 1;
                //        string sFld = sTemp.Substring(indexTaxID, 1);
                //        do
                //        {
                //            string sTmp = sTemp.Substring(indexTaxID - 1, 1);
                //            if (sTmp == "," || sTmp == " ")
                //                break;
                //            sFld = sTmp + sFld;
                //            indexTaxID = indexTaxID - 1;
                //        } while (indexTaxID > 0);
                //        sTemp = sTemp.Replace(sFld + "ENTITY.TAX_ID", " CASE WHEN ENTITY.TAX_ID IS NOT NULL THEN  '###-##-####' END ");
                //    }
                //}
                //Deb MITS 25775
            }

            catch (Exception p_objException)
            {
                throw new StringException(Globalization.GetString("SearchResults.PrepareTempSQL.StringError", m_iClientId), p_objException);
            }
            return (sTemp);
        }

        /// Name		: doDB2Process
        /// Author		: Parag Sarin
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function does the DB2 related process
        /// </summary>
        /// <param name="p_sSql">SQL query</param>
        /// <param name="p_sTempName">Temporary table name</param>
        /// <param name="p_sTemp">Temporary table name</param>
        /// <param name="p_iPos">Start position</param>
        /// <param name="p_iPosNext">Next position</param>
        /// <returns>string</returns>
        internal string DoDB2Process(string p_sSql, string p_sTempName, ref string p_sTemp, int p_iPos, int p_iPosNext)
        {
            DbReader objReader = null;
            DataTable objTable = null;
            DbConnection objConn = null;
            string sType = "";
            string sCols = "";
            string sTable = "";
            string sSQL = "";
            try
            {
                p_sSql = p_sSql + " AND 0=1";
                objReader = DbFactory.GetDbReader(m_sConnectionString, p_sSql);
                objTable = objReader.GetSchemaTable();
                objTable = objReader.GetSchemaTable();
                for (int i = 0; i < objReader.FieldCount; i++)
                {
                    sType = objReader.GetDataTypeName(i);
                    if (sCols.Length > 0)
                    {
                        sCols = sCols + ",";
                    }
                    switch (sType.ToUpper())
                    {
                        case ("SQL_C_CHAR"):
                            sCols = sCols + " VARCHAR(" + objTable.Rows[0]["ColumnSize"].ToString() + ")";
                            break;
                        case ("SQL_C_BIT"):
                            sCols = sCols + " INTEGER ";
                            break;
                        case ("SQL_C_SHORT"):
                            sCols = sCols + " INTEGER ";
                            break;
                        case ("SQL_C_LONG"):
                            sCols = sCols + " INTEGER ";
                            break;
                        case ("SQL_C_SLONG"):
                            sCols = sCols + " INTEGER ";
                            break;
                        case ("SQL_C_FLOAT"):
                            sCols = sCols + " FLOAT ";
                            break;
                        case ("SQL_C_DOUBLE"):
                            sCols = sCols + " FLOAT ";
                            break;
                    }
                }
                p_sTempName = p_sTempName + "_" + m_lUserId.ToString();
                sTable = p_sTempName.Substring(0, 18);
                sSQL = "CREATE TABLE " + sTable + "(" + sCols + ")";
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
                p_sTemp = "INSERT INTO " + sTable + " " + p_sTemp.Substring(0, p_iPos - 1)
                    + " " + p_sTemp.Substring(0, p_iPosNext + 1);
                objConn.ExecuteNonQuery(p_sTemp);
                objConn.Close();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Generic.DoDB2Process.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                sType = null;
                sTable = null;
                sSQL = null;
            }
            return sCols;
        }
        #endregion

        #region Join queries
        /// <summary>
        /// This function forms the FROM & WHERE part of the SQL query.
        /// </summary>
        /// <param name="p_sFrom">FROM part of the query</param>
        /// <param name="p_sWhere">WHERE part of the query</param>
        /// <param name="p_sTempJoin">Join statement before column name</param>
        /// <param name="p_sTempJoinAnother">Join statement after column name</param>
        /// <param name="p_sField">Field of the database</param>
        public void JoinIt(ref string p_sFrom, ref string p_sWhere, string p_sTempJoin, string p_sTempJoinAnother, string p_sField)
        {
            try
            {
                if (m_sDBType == Constants.DB_SQLSRVR)
                {
                    //p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
                    //if (p_sWhere != "") 
                    //    p_sWhere = p_sWhere + " AND ";
                    //p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField + " *= " + p_sTempJoinAnother + "." + p_sField;
                    //psharma compatibility level 90 Issue
                    p_sFrom = "(" + p_sFrom + " LEFT OUTER JOIN " + p_sTempJoinAnother;
                    p_sFrom = p_sFrom + " ON " + p_sTempJoin + "." + p_sField + " = " + p_sTempJoinAnother + "." + p_sField + ")";
                }
                else if (m_sDBType == Constants.DB_ORACLE)
                {
                    p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField + " = " + p_sTempJoinAnother + "." + p_sField + "(+)";
                }
            }
            catch (Exception p_objException)
            {
                throw new StringException(Globalization.GetString("SearchResults.JoinIt.JoinQueries", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Join queries
        /// <summary>  Added by akaur9 for the Mobile Adjuster
        /// This function forms the FROM & WHERE part of the SQL query.
        /// </summary>
        /// <param name="p_sFrom">FROM part of the query</param>
        /// <param name="p_sWhere">WHERE part of the query</param>
        /// <param name="p_sTempJoin">Join statement before column name</param>
        /// <param name="p_sTempJoinAnother">Join statement after column name</param>
        /// <param name="p_sField">Field of the database</param> 
        public void JoinIt(ref string p_sFrom, ref string p_sWhere, string p_sTempJoin, string p_sTempJoinAnother, string p_sField, int p_mobileAdjuster)
        {
            try
            {
                if (m_sDBType == Constants.DB_SQLSRVR)
                {
                    //p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
                    //if (p_sWhere != "") 
                    //    p_sWhere = p_sWhere + " AND ";
                    //p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField + " *= " + p_sTempJoinAnother + "." + p_sField;
                    //psharma compatibility level 90 Issue
                    p_sFrom = "(" + p_sFrom + " INNER JOIN " + p_sTempJoinAnother;
                    p_sFrom = p_sFrom + " ON " + p_sTempJoin + "." + p_sField + " = " + p_sTempJoinAnother + "." + p_sField + ")";
                }
                else if (m_sDBType == Constants.DB_ORACLE)
                {
                    p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField + " = " + p_sTempJoinAnother + "." + p_sField;
                }
            }
            catch (Exception p_objException)
            {
                throw new StringException(Globalization.GetString("SearchResults.JoinIt.JoinQueries", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Join queries
        /// <summary>
        /// This function forms the FROM & WHERE part of the SQL query.     Added by csingh7 R6 claim comment enhancement.
        /// </summary>
        /// <param name="p_sFrom">FROM part of the query</param>
        /// <param name="p_sWhere">WHERE part of the query</param>
        /// <param name="p_sTempJoin">Join statement before column name</param>
        /// <param name="p_sTempJoinAnother">Join statement after column name</param>
        /// <param name="p_sField">Field of the database</param>
        private void JoinIt(ref string p_sFrom, ref string p_sWhere, string p_sTempJoin, string p_sTempJoinAnother, string p_sField1, string p_sField2)
        {
            try
            {
                if (m_sDBType == Constants.DB_SQLSRVR)
                {
                    //p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
                    //if (p_sWhere != "") 
                    //    p_sWhere = p_sWhere + " AND ";
                    //p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField + " *= " + p_sTempJoinAnother + "." + p_sField;
                    //psharma compatibility level 90 Issue
                    p_sFrom = "(" + p_sFrom + " LEFT OUTER JOIN " + p_sTempJoinAnother
                        + " ON " + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnother + "." + p_sField2 + ")";
                }
                else if (m_sDBType == Constants.DB_ORACLE)
                {
                    p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnother + "." + p_sField2 + "(+)";
                }
            }
            catch (Exception p_objException)
            {
                throw new StringException(Globalization.GetString("SearchResults.JoinIt.JoinQueries", m_iClientId), p_objException);
            }
        }
        #endregion

        // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
        #region Join queries
        /// <summary>
        /// This function forms the FROM & WHERE part of the SQL query.     Added by csingh7 R6 claim comment enhancement.
        /// </summary>
        /// <param name="p_sFrom">FROM part of the query</param>
        /// <param name="p_sWhere">WHERE part of the query</param>
        /// <param name="p_sTempJoin">Join statement before column name</param>
        /// <param name="p_sTempJoinAnother">Join statement after column name</param>
        /// <param name="p_sField">Field of the database on which p_sTempJoin Table and p_sTempJoinAnother will create join </param>
        /// <param name="bCreateAndCondition">If it is true and p_sTempJoinAnother Table already exists in 
        ///                                     p_sFrom then it will add the condition with AND Condition</param>
        private void JoinIt(ref string p_sFrom, ref string p_sWhere, string p_sTempJoin, string p_sTempJoinAnother, string p_sField, bool bCreateAndCondition)
        {
            JoinIt(ref p_sFrom, ref p_sWhere, p_sTempJoin, p_sTempJoinAnother, p_sField, p_sField, true);
        }
        #endregion
        #region Join queries
        /// <summary>
        /// This function forms the FROM & WHERE part of the SQL query.     Added by csingh7 R6 claim comment enhancement.
        /// </summary>
        /// <param name="p_sFrom">FROM part of the query</param>
        /// <param name="p_sWhere">WHERE part of the query</param>
        /// <param name="p_sTempJoin">Join statement before column name</param>
        /// <param name="p_sTempJoinAnother">Join statement after column name</param>
        /// <param name="p_sField1">Field of the database on which p_sTempJoin Table will create join </param>
        /// <param name="p_sField2">Field of the database  on which p_sTempJoinAnother Table will create join </param>
        /// <param name="bCreateAndCondition">If it is true and p_sTempJoinAnother Table already exists in 
        ///                                     p_sFrom then it will add the condition with AND Condition</param>
        private void JoinIt(ref string p_sFrom, ref string p_sWhere, string p_sTempJoin, string p_sTempJoinAnother, string p_sField1, string p_sField2, bool bCreateAndCondition)
        {
            try
            {
                int iTableLocn;
                int iJoinLocn;
                if (m_sDBType == Constants.DB_SQLSRVR)
                {
                    //p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
                    //if (p_sWhere != "") 
                    //    p_sWhere = p_sWhere + " AND ";
                    //p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField + " *= " + p_sTempJoinAnother + "." + p_sField;
                    //psharma compatibility level 90 Issue
                    if (!bCreateAndCondition || (bCreateAndCondition && !p_sFrom.Contains(p_sTempJoinAnother)))
                    {
                        p_sFrom = "(" + p_sFrom + " LEFT OUTER JOIN " + p_sTempJoinAnother;
                        p_sFrom = p_sFrom + " ON " + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnother + "." + p_sField2 + ")";
                    }
                    else
                    {
                        iTableLocn = p_sFrom.IndexOf(p_sTempJoinAnother);
                        iJoinLocn = p_sFrom.IndexOf(" ON ", iTableLocn);
                        p_sFrom = p_sFrom.Insert(iJoinLocn + 4, p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnother + "." + p_sField2 + " AND ");
                    }
                }
                else if (m_sDBType == Constants.DB_ORACLE)
                {
                    if (!bCreateAndCondition || (bCreateAndCondition && !p_sFrom.Contains(p_sTempJoinAnother)))
                    {
                        p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
                        if (p_sWhere != "")
                            p_sWhere = p_sWhere + " AND ";
                        p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnother + "." + p_sField2 + "(+)";
                    }
                    else
                    {
                        if (p_sWhere != "")
                            p_sWhere = p_sWhere + " AND ";
                        p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnother + "." + p_sField2 + "(+)";
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw new StringException(Globalization.GetString("SearchResults.JoinIt.JoinQueries", m_iClientId), p_objException);
            }
        }
        #endregion
        // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
        #region Join queries
        /// <summary>
        /// This function forms the FROM & WHERE part of the SQL query.     Added by pmittal5 Mits 16820 11/04/09
        /// </summary>
        /// <param name="p_sFrom">FROM part of the query</param>
        /// <param name="p_sWhere">WHERE part of the query</param>
        /// <param name="p_sTempJoin">Join statement before column name</param>
        /// <param name="p_sTempJoinAnother">Join statement after column name</param>
        /// <param name="p_sField1">Field of the database</param>
        /// <param name="p_sField2">Field of the database</param>
        /// <param name="p_sTableAlias">Alias of the table name</param>
        private void JoinIt(ref string p_sFrom, ref string p_sWhere, string p_sTempJoin, string p_sTempJoinAnother, string p_sField1, string p_sField2, string p_sTableAlias)
        {
            try
            {
                if (m_sDBType == Constants.DB_SQLSRVR)
                {
                    p_sFrom = "(" + p_sFrom + " LEFT OUTER JOIN " + p_sTempJoinAnother + " " + p_sTableAlias;
                    if (p_sTempJoin != "")
                        p_sFrom = p_sFrom + " ON " + p_sTempJoin + "." + p_sField1 + " = " + p_sTableAlias + "." + p_sField2 + ")";
                    else
                        p_sFrom = p_sFrom + " ON " + p_sField1 + " = " + p_sTableAlias + "." + p_sField2 + ")";
                }
                else if (m_sDBType == Constants.DB_ORACLE)
                {
                    p_sFrom = p_sFrom + "," + p_sTempJoinAnother + " " + p_sTableAlias;
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    if (p_sTempJoin != "")
                        p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField1 + " = " + p_sTableAlias + "." + p_sField2 + "(+)";
                    else
                        p_sWhere = p_sWhere + p_sField1 + " = " + p_sTableAlias + "." + p_sField2 + "(+)";
                }
            }
            catch (Exception p_objException)
            {
                throw new StringException(Globalization.GetString("SearchResults.JoinIt.JoinQueries", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Drop temp tables
        /// <summary>
        /// This function drops the temporary tables.
        /// </summary>
        /// <param name="p_sTableName">Table name to be dropped</param>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void DropTable(string p_sTableName, string p_sSQL, ref DbConnection p_objConn)
        {
            int iRet = -1;
            //srajindersin MITS 32618 dt:05/14/2013 - checking temp tables before deletion
            string check = "IF EXISTS (SELECT * FROM sysobjects WHERE type = 'U' AND name = '" + p_sTableName + "')";
            try
            {
                //Changes done by kuladeep for mits:30546
                //if((p_sSQL == "") || (p_sSQL.IndexOf(p_sTableName) != -1))
                if ((p_sSQL == "") || (p_sSQL.IndexOf((p_sTableName.TrimEnd(']').TrimStart('['))) != -1))
                {
                    if (m_sDBType != Constants.DB_ORACLE)
                    {
                    iRet = p_objConn.ExecuteNonQuery(check + PrepareTempSQL(" DROP TABLE " + p_sTableName));
                    }
                    else
                    {
                        string[] sQuery = PrepareTempSQL("DROP TABLE " + p_sTableName).Split(' ');
                        CleanupSearchStoredProcOracle(sQuery[2].Trim());
                    }
                }
            }

//			catch (StringException p_objException)
            //			{
            //				throw p_objException; 
            //			}

            catch (Exception p_objException)
            {
                //If temporary table does not exist for the given connection session then do nothing 
                //else in case of any other error throw the exception.
                if (p_objException is System.Data.SqlClient.SqlException || iRet == -1) { }
                else
                    throw new RMAppException(Globalization.GetString("SearchResults.DropTable.TableError", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Generate XML
        /// <summary>
        /// This function generates the final XML out of the data obtained from the database.
        /// </summary>
        /// <param name="p_sOutputPath">Path where final search XML needs to be stored</param>
        /// <param name="p_lPageSize">Page size</param>
        /// <param name="p_lMaxRecords">Maximum records that needs to be displayed</param>
        /// <param name="p_lCatID">Category id</param>
        /// <param name="p_sADMTable">Admin table</param>
        /// <param name="p_sSysEx">Soundex feature parameter</param>
        /// <param name="p_lEntityTableID">Entity table id</param>
        /// <param name="p_sData">Search data entered by the user</param>
        /// <param name="p_arrlstFieldTypes">Arraylist containing field types</param>
        /// <param name="p_arrlstColumnHeaders">Arraylist containing column headers</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_sSearchXMLData">Final search XML</param>
        /// <returns>Number of search result pages needs to be shown.</returns>
        public long GenXMLResults(string p_sOutputPath, int p_iPageSize, int p_iPageNumber, long p_lCatID, string p_sADMTable, string p_sSysEx, long p_lEntityTableID, string p_sData, ArrayList p_arrlstFieldTypes, ArrayList p_arrlstColumnHeaders, ref DbConnection p_objConn, string p_sSQL, ref XmlDocument p_objSearchDom, string p_sSortColumn, string p_sOrder, string p_sLangCode="1033")
        {
            string sTmp = "";
            string sCatID = "";
            int iCurrField = 0;
            long lClaimCount = 0;
            int iFieldCount = 0;
            long lCount = 0;
            long lMaxLoop = 0;
            bool bFirst = false;
            XmlDocument objXml = null;
            XmlElement objXMLRoot = null;
            XmlElement objXMLColumns = null;
            XmlElement objXMLColumn = null;
            XmlElement objXMLData = null;
            XmlElement objXMLRow = null;
            XmlElement objXMLField = null;
            FileInfo objFile = null;
            string[] arrClaimWholeData = null;
            string[] arrClaimSpecificData = null;
            string[] arrClaimWholeDataSeparator = { "|^|" };
            string[] arrClaimSpecificDataSeparator = { "~*~" };
            LocalCache objCache = null;
            string sEntityFormName = string.Empty;
            bool blnSuccess = false;

            try
            {
                //Start search def
                objXml = new XmlDataDocument();
                p_objSearchDom = null;
                objXMLRoot = objXml.CreateElement("results");
                objXml.AppendChild(objXMLRoot);

                //Setup column definitions and data sections
                objXMLColumns = objXml.CreateElement("columns");
                objXMLRoot.AppendChild(objXMLColumns);
                objXMLData = objXml.CreateElement("data");
                objXMLRoot.AppendChild(objXMLData);

                //Nuke any existing search result cache files
                if (p_sOutputPath.Trim().Length > 0)
                {
                    objFile = new FileInfo(p_sOutputPath + "\\searchresults-uid" + m_lUserId + ".xml");
                    if (objFile == null)
                        throw new FileInputOutputException(Globalization.GetString("SearchResults.GenXMLResults.FileError", m_iClientId));
                    if (objFile.Exists)
                        objFile.Delete();
                }

                //Read data and create row list
                arrClaimWholeData = p_sData.Split(arrClaimWholeDataSeparator, System.StringSplitOptions.None);
                if (arrClaimWholeData.Length > 0)
                {
                    arrClaimSpecificData = arrClaimWholeData[0].Split(arrClaimSpecificDataSeparator, System.StringSplitOptions.None);
                    iFieldCount = arrClaimSpecificData.Length - 1;
                    bFirst = true;
                    lCount = 0;
                }

                lMaxLoop = arrClaimWholeData.Length;

                //set page bounds attributes so XSL can display correct rows
                long lTotalPages = m_lTotalRecords / p_iPageSize;

                long lRowStart = (p_iPageNumber - 1) * p_iPageSize;
                long lRowEnd = lRowStart + p_iPageSize;
                if (lRowEnd > (m_lTotalRecords - 1))
                    lRowEnd = m_lTotalRecords;

                objCache = new LocalCache(m_sConnectionString,m_iClientId);
                for (lClaimCount = 0; lClaimCount < lMaxLoop; lClaimCount++)
                {
                    //arrClaimSpecificData = arrClaimWholeData[lClaimCount].Split('~');
                    arrClaimSpecificData = arrClaimWholeData[lClaimCount].Split(arrClaimSpecificDataSeparator, System.StringSplitOptions.None);
                    //Create new row in result list
                    objXMLRow = objXml.CreateElement("row");
                    if (arrClaimSpecificData[0].Length == 0)
                        continue;
						//mbahl3 mits 30224
                    if (IsEntitySearch)
                    {
                        //Aman Mobo MITS 34651
                        if (!IsMobileAdjuster)
                        {
                            //avipinsrivas Start : Worked for Jira-340
                            if (objCache.GetCodeDesc(objCache.GetCodeId("A", "ENTITY_STATUS")).Equals(arrClaimSpecificData[arrClaimSpecificData.Length - (objSysSettingsGlobal.UseEntityRole ? 3 : 2)]))       //avipinsrivas start : Worked for Jira-340
                            //if (objCache.GetCodeDesc(objCache.GetCodeId("A", "ENTITY_STATUS")).Equals(arrClaimSpecificData[arrClaimSpecificData.Length - 2]))
                            {
                                if (objSysSettingsGlobal.UseEntityRole && !string.IsNullOrEmpty(arrClaimSpecificData[arrClaimSpecificData.Length - 2]))
                                {
                                    string sEntityRoleIDs = string.Concat(arrClaimSpecificData[0], "~", arrClaimSpecificData[arrClaimSpecificData.Length - 2]);
                                    objXMLRow.SetAttribute("pid", arrClaimSpecificData[0] + "|true|" + sEntityRoleIDs);
                                }
                                else
                                objXMLRow.SetAttribute("pid", arrClaimSpecificData[0] + "|true");
                            }
                            //avipinsrivas End
                            else
                                objXMLRow.SetAttribute("pid", arrClaimSpecificData[0] + "|false");
                        }
                        else
                        {
                            objXMLRow.SetAttribute("pid", arrClaimSpecificData[0]);
                        }
                        //Aman Mobo MITS 34651
                    }
                    else
					//mbahl3 mits 30224
                    objXMLRow.SetAttribute("pid", arrClaimSpecificData[0]);
                    if (p_lCatID == 4)
                    {
                      	//mini performance tseting 
                        //avipinsrivas start : Worked for Jira-340
                        sEntityFormName = GetEntityFormName(Conversion.ConvertStrToInteger(arrClaimSpecificData[0]));
                        //sEntityFormName = GetEntityFormName(Conversion.ConvertStrToInteger(arrClaimSpecificData[0]));
                        objXMLRow.SetAttribute("entityformname", sEntityFormName);

                    }
                    //Mridul. 11/26/09. MITS:18229
                    if (p_lCatID == ConstGlobals.ENHANCED_POLICY_SEARCH)
                    {
                        sEntityFormName = GetPolicyTypeFormName(Conversion.CastToType<int>(arrClaimSpecificData[0].ToString(), out blnSuccess));
                        objXMLRow.SetAttribute("entityformname", sEntityFormName);
                    }

                    objXMLData.AppendChild(objXMLRow);

                    for (iCurrField = 1; iCurrField < iFieldCount; ++iCurrField)
                    {
                        if (iCurrField > p_arrlstFieldTypes.Count) //JIRA RMA-358//JIRA RMA-869 - Ashish Ahuja
                            break; //JIRA RMA-358//JIRA RMA-869 - Ashish Ahuja
                    //mbahl3 code change for cloud
					 //  if (IsMobileAdjuster || IsMobilityAdjuster)
                       if (IsMobileAdjuster )
                    //mbahl3 code change for cloud
                        {
                            sTmp = FormatDisplayFieldForMobileAdjuster(arrClaimSpecificData[iCurrField], iCurrField, p_arrlstFieldTypes, objCache, p_lCatID);
                        }
                        else
                        {
                            sTmp = FormatDisplayField(arrClaimSpecificData[iCurrField], iCurrField, p_arrlstFieldTypes, objCache, p_lCatID, p_sLangCode);
                        }

                        //if (p_lCatID == 4)
                        //{
                        //    objXMLRow.SetAttribute("entityformname", sEntityFormName);
                        //}

                        //Save column info - if on first pass
                        if (bFirst)
                        {
                            objXMLColumn = objXml.CreateElement("column");
                            objXMLColumns.AppendChild(objXMLColumn);

                            objXMLColumn.InnerText = p_arrlstColumnHeaders[iCurrField - 1].ToString();
                            objXMLColumn.SetAttribute("type", p_arrlstFieldTypes[iCurrField - 1].ToString());
                            //MGaba2:MITS 16476 Added search for Multi Text/Codes field type in supplementals
                            //gagnihotri MITS 14233 Disabling sorting for Comments column
                            //if (p_arrlstFieldTypes[iCurrField - 1].ToString() == Constants.TEXTML)
                            if (p_arrlstFieldTypes[iCurrField - 1].ToString() == Constants.TEXTML || p_arrlstFieldTypes[iCurrField - 1].ToString() == Constants.FREECODE)
                                objXMLColumn.SetAttribute("nosort", "on");
                        }

                        //Save data
                        objXMLField = objXml.CreateElement("field");
                        objXMLRow.AppendChild(objXMLField);

                        //Put formated date into an attribute, and leave the actual 8 digits string
                        //sorting MITS 7676
                        if (p_arrlstFieldTypes[iCurrField - 1].ToString() == Constants.DATE)
                        {
                            objXMLField.InnerText = arrClaimSpecificData[iCurrField];
                            objXMLField.SetAttribute("formatedtext", sTmp);
                        }
                        else
                            objXMLField.InnerText = sTmp;
                    }
                    bFirst = false;
                }

                // write xml file
                switch (p_lCatID)
                {
                    case ConstGlobals.CLAIM_SEARCH: //Claim search
                        sCatID = "claim";
                        break;
                    case ConstGlobals.EVENT_SEARCH: //Event search
                        sCatID = "event";
                        break;
                    case ConstGlobals.EMPLOYEE_SEARCH: //Employee search
                        sCatID = "employee";
                        break;
                    case ConstGlobals.ENTITY_SEARCH: //Entity search
                        sCatID = "entity";
                        objXMLRoot.SetAttribute("entitytableid", p_lEntityTableID.ToString());
                        break;
                    case ConstGlobals.VEHICLE_SEARCH: //Vehicle search
                        sCatID = "vehicle";
                        break;
                    case ConstGlobals.POLICY_SEARCH: //Policy search
                        sCatID = "policy";
                        break;
                    case ConstGlobals.PAYMENT_SEARCH: //Payment search
                        sCatID = "payment";
                        break;
                    case ConstGlobals.PATIENT_SEARCH: //Patient search
                        sCatID = "patient";
                        break;
                    case ConstGlobals.PHYSICIAN_SEARCH: //Physician search
                        sCatID = "physician";
                        break;
                    case ConstGlobals.MED_STAFF_SEARCH: // Med. Staff search
                        sCatID = "medstaff";
                        break;
                    case ConstGlobals.DISP_PLAN_SEARCH: // Disability Plan
                        sCatID = "displan";
                        break;
                    case ConstGlobals.AT_SEARCH: // Admin tracking search
                        sCatID = "adm";
                        objXMLRoot.SetAttribute("admtable", p_sADMTable);
                        break;
                    // Start Shruti Leave Plan Search
                    case ConstGlobals.LV_PLAN_SEARCH: //Enhanced Policy search
                        sCatID = "leaveplan";
                        break;
                    // End Shruti Leave Plan Search
                    // Start Naresh Enhanced Policy Search
                    case ConstGlobals.ENHANCED_POLICY_SEARCH: //Policy search
                        sCatID = "policyenh";
                        break;
                    // End Naresh Enhanced Policy Search
                    //Anu Tennyson for MITS 18291 : STARTS 10/25/2009
                    case ConstGlobals.PROPERTY_SEARCH: //Property search
                        sCatID = "propertyunit";
                        break;
                    //Anu Tennyson for MITS 18291 : ENDS 
                    //skhare7 Diary
                    case ConstGlobals.DIARY_SEARCH: //Diary search
                        sCatID = "diary";
                        break;
                    //skhare7 Diary
                    //Start averma62 Catastrophe - MITS - 28528
                    case ConstGlobals.CATASTROPHE_SEARCH:
                        sCatID = "catastrophe";
                        break;
                    //End averma62 Catastrophe - MITS - 28528
                    //Start Amandeep Driver Search
                    case ConstGlobals.DRIVER_SEARCH:
                        sCatID = "driver";
                        break;
                    //End Amandeep Driver Search
                    //Ashish Ahuja Address Master //RMA-8753
                    case ConstGlobals.ADDRESS_SEARCH:
                        sCatID = "address";
                        break;
                }
                objXMLRoot.SetAttribute("searchcat", sCatID);
                objXMLRoot.SetAttribute("totalrows", m_lTotalRecords.ToString());

                objXMLRoot.SetAttribute("rowstart", "1");				// 1 based since position() function in XSLT is 1 based TEMP
                objXMLRoot.SetAttribute("rowend", (lRowEnd - lRowStart).ToString());  // TEMP

                // Defect no. 1817: Added attributes for implementing sorting of the columns.	
                objXMLRoot.SetAttribute("SortColumn", p_sSortColumn);
                objXMLRoot.SetAttribute("Order", p_sOrder);

                objXMLRoot.SetAttribute("pagesize", p_iPageSize.ToString());
                objXMLRoot.SetAttribute("totalpages", Conversion.ConvertObjToStr(((m_lTotalRecords - 1) / p_iPageSize) + 1));
                //search from the people maint entity screen returns data as a
                //vanilla entity rather than a person
                objXMLRoot.SetAttribute("sys_ex", p_sSysEx);

                if (p_sOutputPath.Length > 0)
                    objXml.Save(p_sOutputPath + "\\searchresults-uid" + m_lUserId + ".xml");
                else
                    p_objSearchDom = objXml;

                //avipinsrivas start : Worked for JIRA - 7767
                objXMLRoot.SetAttribute("useentityrole", this.SysSettingsGlobal.UseEntityRole.ToString());
                //avipinsrivas end
                switch (p_lCatID)
                {
                    case ConstGlobals.CLAIM_SEARCH: //Claim search
                        CleanupClaimSearch(p_sSQL, ref p_objConn); //Only cleanup temp tables actually used
                        break;
                    case ConstGlobals.EVENT_SEARCH: //event search,
                        CleanupEventSearch(p_sSQL, ref p_objConn); //Only cleanup temp tables actually used
                        break;
                    case ConstGlobals.EMPLOYEE_SEARCH: // Employee Search
                        CleanupEmployeeSearch(p_sSQL, ref p_objConn);
                        break;
                    case ConstGlobals.ENTITY_SEARCH:
                        CleanupEntitySearch(p_sSQL, ref p_objConn);
                        break;
                    case ConstGlobals.VEHICLE_SEARCH:
                        CleanupVehicleSearch(p_sSQL, ref p_objConn);
                        break;
                    case ConstGlobals.POLICY_SEARCH: //policy search
                        CleanupPolicySearch(p_sSQL, ref p_objConn); //Only cleanup temp tables actually used
                        break;
                    case ConstGlobals.PATIENT_SEARCH: //patient search
                        CleanupPatientSearch(p_sSQL, ref p_objConn);
                        break;
                    case ConstGlobals.PHYSICIAN_SEARCH: //physician search
                        CleanupPhysicianSearch(p_sSQL, ref p_objConn);
                        break;
                    case ConstGlobals.MED_STAFF_SEARCH: //med. staff search
                        CleanupMedStaffSearch(p_sSQL, ref p_objConn);
                        break;
                    case ConstGlobals.DISP_PLAN_SEARCH: //Dis Plan search
                        CleanupDisPlanSearch(p_sSQL, ref p_objConn);
                        break;
                    // Start Shruti Leave Plan Search
                    case ConstGlobals.LV_PLAN_SEARCH: //Enhanced Policy search
                        CleanupLeavePlanSearch(p_sSQL, ref p_objConn);
                        break;
                    // End Shruti Leave Plan Search
                    // Start Naresh Enhanced Policy Search
                    case ConstGlobals.ENHANCED_POLICY_SEARCH: //Enhanced Policy search
                        CleanupEnhPolicySearch(p_sSQL, ref p_objConn);
                        break;
                    case ConstGlobals.PROPERTY_SEARCH: //Property search
                        CleanupPropertySearch(p_sSQL, ref p_objConn);
                        break;
                    //skhare7 Diary
                    case ConstGlobals.DIARY_SEARCH:
                        CleanupDiarySearch(p_sSQL, ref p_objConn);//Diary Serach
                        break;
                    // End Naresh Enhanced Policy Search
                    //TODO as default AT search is not present
                    //case ConstGlobals.AT_SEARCH : //Admin Tracking search
                    //SubCleanupATSearch (p_sSQL, ref p_objConn);
                    //break;
                    //RMA-8753 nshah28(Added by ashish)
                    case ConstGlobals.ADDRESS_SEARCH:
                        CleanUpAddressSearch(p_sSQL, ref p_objConn); //Address Search
                        break;
                }
            }

            catch (CollectionException p_objException)
            {
                throw p_objException;
            }
            catch (FileInputOutputException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.GenXMLResults.XMLCreation", m_iClientId), p_objException);
            }

            finally
            {
                objXml = null;
                objXMLRoot = null;
                objXMLColumns = null;
                objXMLColumn = null;
                objXMLData = null;
                objXMLRow = null;
                objXMLField = null;
                objFile = null;
                arrClaimWholeData = null;
                arrClaimSpecificData = null;
                arrClaimSpecificDataSeparator = null;
                arrClaimWholeDataSeparator = null;
                if (objCache != null) objCache.Dispose();
            }
            return (((m_lTotalRecords - 1) / p_iPageSize) + 1);
        }
        #endregion

        #region Format fields
        /// <summary>
        /// This function creates the string out of the data fetched from the database for the given search criteria.
        /// </summary>
        /// Author          :   Amandeep Kaur
        /// Date Created    :   09/05/2011
        /// <param name="p_sModuleSpecificData">Module specific like Claim, Entity data</param>
        /// <param name="p_iCol">Column of the Arraylist</param>
        /// <param name="p_arrlstFieldTypes">Field types present in the Arraylist</param>
        /// <returns>String with the proper format.</returns>
        private string FormatDisplayFieldForMobileAdjuster(string p_sModuleSpecificData, int p_iCol, ArrayList p_arrlstFieldTypes, LocalCache objCache, long p_lCatId)
        {
            string sResult = "";
            string sTmp = "";
            string sTmpResult = "";
            // npadhy MITS 16508 If the Code Lookup data comes blank, then it should not display -
            string sFiller = "";

            try
            {
                switch (p_arrlstFieldTypes[p_iCol - 1].ToString())
                {
                    case Constants.CHECK_BOX: //field type "checkbox"
                        if ((p_sModuleSpecificData != "") && (p_sModuleSpecificData != "0"))
                            sResult = "Yes";
                        else
                            sResult = "No";
                        break;
                    case Constants.CODE: //field type "code"
                    case Constants.CODE_LIST://field type "codelist"
                        objCache.GetCodeInfo(Conversion.ConvertStrToInteger(p_sModuleSpecificData), ref sTmp, ref sTmpResult);
                        if (sTmp != "" && sTmpResult != "")
                            sFiller = " - ";
                        sResult = sTmp + sFiller + sTmpResult;
                        break;
                    case Constants.ORGH://field type "orgh"
                        objCache.GetOrgInfo(Conversion.ConvertStrToInteger(p_sModuleSpecificData), ref sTmp, ref sTmpResult);
                        if (sTmp != "" && sTmpResult != "")
                            sFiller = " - ";
                        sResult = sTmp + sFiller + sTmpResult;
                        break;
                    case Constants.STATE://field type "state"
                    case Constants.MULTI_STATE://field type "multistate"
                        objCache.GetStateInfo(Conversion.ConvertStrToInteger(p_sModuleSpecificData), ref sTmp, ref sTmpResult);
                        if (sTmp != "" && sTmpResult != "")
                            sFiller = " - ";
                        sResult = sTmp + sFiller + sTmpResult;
                        break;
                    case Constants.ENTITY_LIST: //field type "entitylist"
                    case Constants.ENTITY://field type "entity"
                        objCache.GetEntityInfo(Conversion.ConvertStrToLong(p_sModuleSpecificData), ref sTmp, ref sTmpResult);
                        sResult = sTmp + " " + sTmpResult;
                        sResult.Trim();
                        break;
                    case Constants.CURRENCY://field type "currency"
                        double iCurr = Conversion.ConvertStrToDouble(p_sModuleSpecificData);
                        sResult = string.Format("{0:C}", iCurr);
                        break;
                    case Constants.NUMERIC://field type "numeric"
                        sResult = p_sModuleSpecificData;
                        break;
                    case Constants.DOUBLE://field type "double"
                        sResult = p_sModuleSpecificData;
                        break;
                    case Constants.TEXT://field type "text"    //Added by csingh7 MITS 20653
                        sResult = p_sModuleSpecificData;
                        break;
                    //case Constants.TEXT://field type "text"   Commented by csingh7 for MITS 20653
                    case Constants.SSN: //field type "ssn"
                        sResult = p_sModuleSpecificData.Replace("'", "");
                        break;
                    case Constants.DATE://field type "date"
                        sResult = Conversion.GetDBDateFormat(p_sModuleSpecificData, "d");
                        break;
                    case Constants.TIME: //field type "time"
                        sResult = Conversion.GetDBTimeFormat(p_sModuleSpecificData, "t");
                        break;
                    case Constants.TABLE_LIST://field type "tablelist"
                        //if (p_lCatId == 4)
                        //{
                        //    p_sEntityFormName = GetEntityFormName(Conversion.ConvertStrToInteger(p_sModuleSpecificData));
                        //}

                        sResult = objCache.GetUserTableName(Conversion.ConvertStrToLong(p_sModuleSpecificData));
                        break;
                    case Constants.TEXTML: //field type "textml"
                    case Constants.FREECODE://MGaba2:MITS 16476 Added search for Multi Text/Codes field type in supplementals
                        //if (p_sModuleSpecificData.Length > 80)                  //akaur9 changed it for the Mobile Adjuster to display the whole string
                        //    sResult = p_sModuleSpecificData.Substring(0, 80);   //display 80 characters max from free text
                        //else
                        sResult = p_sModuleSpecificData;
                        sResult = sResult.Replace((char)13, ' ');
                        sResult = sResult.Replace((char)10, ' ');
                        break;
                    default:
                        throw new RMAppException(Globalization.GetString("ResultList.FormatDisplayField.FieldError", m_iClientId));
                }
                return (sResult);
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new CollectionException(Globalization.GetString("SearchResults.FormatDisplayField.FormatError", m_iClientId), p_objException);
            }
        }
        /// <summary>
        /// This function creates the string out of the data fetched from the database for the given search criteria.
        /// </summary>
        /// <param name="p_sModuleSpecificData">Module specific like Claim, Entity data</param>
        /// <param name="p_iCol">Column of the Arraylist</param>
        /// <param name="p_arrlstFieldTypes">Field types present in the Arraylist</param>
        /// <returns>String with the proper format.</returns>
        private string FormatDisplayField(string p_sModuleSpecificData, int p_iCol, ArrayList p_arrlstFieldTypes, LocalCache objCache, long p_lCatId,string sLangCode="1033")
        {
            string sResult = "";
            string sTmp = "";
            string sTmpResult = "";
            // npadhy MITS 16508 If the Code Lookup data comes blank, then it should not display -
            string sFiller = "";
            bool blnSuccess = false;
            try
            {
                switch (p_arrlstFieldTypes[p_iCol - 1].ToString())
                {
                    case Constants.CHECK_BOX: //field type "checkbox"
                        if ((p_sModuleSpecificData != "") && (p_sModuleSpecificData != "0"))
                            sResult = "Yes";
                        else
                            sResult = "No";
                        break;
                    case Constants.CODE: //field type "code"
                    case Constants.CODE_LIST://field type "codelist"
                        if (p_sModuleSpecificData != "Yes" && p_sModuleSpecificData != "No") //igupta3 jira 439
                        {
                            objCache.GetCodeInfo(Conversion.ConvertStrToInteger(p_sModuleSpecificData), ref sTmp, ref sTmpResult);
                            if (sTmp != "" && sTmpResult != "")
                                sFiller = " - ";
                            sResult = sTmp + sFiller + sTmpResult;
                        }
                        else
                            sResult = p_sModuleSpecificData;
                        break;
                    case Constants.ORGH://field type "orgh"
                        objCache.GetOrgInfo(Conversion.ConvertStrToInteger(p_sModuleSpecificData), ref sTmp, ref sTmpResult);
                        if (sTmp != "" && sTmpResult != "")
                            sFiller = " - ";
                        sResult = sTmp + sFiller + sTmpResult;
                        break;
                    case Constants.STATE://field type "state"
                    case Constants.MULTI_STATE://field type "multistate"
                        objCache.GetStateInfo(Conversion.ConvertStrToInteger(p_sModuleSpecificData), ref sTmp, ref sTmpResult);
                        if (sTmp != "" && sTmpResult != "")
                            sFiller = " - ";
                        sResult = sTmp + sFiller + sTmpResult;
                        break;
                    case Constants.ENTITY_LIST: //field type "entitylist"
                    case Constants.ENTITY://field type "entity"
                        objCache.GetEntityInfo(Conversion.ConvertStrToLong(p_sModuleSpecificData), ref sTmp, ref sTmpResult);
                        //MITS ID 33230: nkaranam2
                        if (!string.IsNullOrEmpty(sTmp) && !string.IsNullOrEmpty(sTmpResult))
                            sResult = sTmpResult + ", " + sTmp;
                        else
                        sResult = sTmp + " " + sTmpResult;
                        sResult.Trim();
                        break;
                    case Constants.CURRENCY://field type "currency"
                        if (this.SysSettingsGlobal.UseMultiCurrency != 0)
                        {
                            string[] sArr = p_sModuleSpecificData.Split('_');
                            if (sArr != null)
                            {
                                if (sArr.Length > 1)
                                {
                                    int iD = int.Parse(sArr[1].Trim());
                                    if (iD > 0)
                                        sResult = CommonFunctions.ConvertByCurrencyCode(int.Parse(sArr[1].Trim()), Conversion.ConvertStrToDouble(sArr[0].Trim()), m_sConnectionString, m_iClientId);
                                    else
                                        sResult = CommonFunctions.ConvertByCurrencyCode(this.SysSettingsGlobal.BaseCurrencyType, Conversion.ConvertStrToDouble(sArr[0].Trim()), m_sConnectionString, m_iClientId);
                                }
                                else
                                {
                                    sResult = CommonFunctions.ConvertByCurrencyCode(this.SysSettingsGlobal.BaseCurrencyType, Conversion.ConvertStrToDouble(sArr[0].Trim()), m_sConnectionString, m_iClientId);
                                }
                            }
                            else
                            {
                                sResult = CommonFunctions.ConvertByCurrencyCode(this.SysSettingsGlobal.BaseCurrencyType, Conversion.ConvertStrToDouble(sArr[0].Trim()), m_sConnectionString, m_iClientId);
                            }
                        }
                        else
                        {
                            sResult = CommonFunctions.ConvertByCurrencyCode(this.SysSettingsGlobal.BaseCurrencyType, Conversion.ConvertStrToDouble(p_sModuleSpecificData), m_sConnectionString, m_iClientId);
                            //Base Currency at that time 
                        }
                        break;
                    case Constants.NUMERIC://field type "numeric"
                        sResult = p_sModuleSpecificData;
                        break;
                    case Constants.DOUBLE://field type "double"
                        sResult = p_sModuleSpecificData;
                        break;
                    case Constants.TEXT://field type "text"    //Added by csingh7 MITS 20653
                        sResult = p_sModuleSpecificData;
                        break;
                    //case Constants.TEXT://field type "text"   Commented by csingh7 for MITS 20653
                    case Constants.SSN: //field type "ssn"
                        if (!string.IsNullOrEmpty(p_sModuleSpecificData) && this.SysSettingsGlobal.MaskSSN) //Added by amitosh for Mits 27378
                            sResult = "###-##-####";
                        else
                            sResult = p_sModuleSpecificData.Replace("'", "");
                        break;
                    case Constants.DATE://field type "date"
                        sResult = Conversion.GetUIDate(p_sModuleSpecificData, sLangCode, m_iClientId);
                        break;
                    case Constants.TIME: //field type "time"
                        sResult = Conversion.GetDBTimeFormat(p_sModuleSpecificData, "t");
                        break;
                    case Constants.TABLE_LIST://field type "tablelist"
                        //if (p_lCatId == 4)
                        //{
                        //    p_sEntityFormName = GetEntityFormName(Conversion.ConvertStrToInteger(p_sModuleSpecificData));
                        //}

                        sResult = objCache.GetUserTableName(Conversion.ConvertStrToLong(p_sModuleSpecificData));
                        break;
                    case Constants.TEXTML: //field type "textml"
                    case Constants.FREECODE://MGaba2:MITS 16476 Added search for Multi Text/Codes field type in supplementals
                        //Code commented by mdhamija--Start MITS 25829
                        //if (p_sModuleSpecificData.Length>80)
                        //	sResult = p_sModuleSpecificData.Substring(0,80);   //display 80 characters max from free text
                        //else Code commented by mdhamija--End MITS 25829
                        sResult = p_sModuleSpecificData;
                        sResult = sResult.Replace((char)13, ' ');
                        sResult = sResult.Replace((char)10, ' ');
                        break;
                    case Constants.ATTACHEDRECORD://field type "attachedrecord"    //Added by skhare7 for Diary search enhancement
                        sResult = p_sModuleSpecificData;
                        break;
                    //field type "userlookup"    //Added by npadhy for Jira - 6415 User Lookup in Supplementals
                    case Constants.USERLOOKUP + "|0":
                    case Constants.USERLOOKUP + "|1":
                        string sFieldType = p_arrlstFieldTypes[p_iCol - 1].ToString();
                        bool isUser = Conversion.CastToType<int>(sFieldType.Substring(sFieldType.IndexOf("|") + 1, 1), out blnSuccess) == 0 ? true : false;
                        // If the entry is for User retrieve from user Table
                            if (isUser)
                            {
                               sResult = objCache.GetSystemLoginName(Conversion.CastToType<int>(p_sModuleSpecificData, out blnSuccess), UserLogin.DatabaseId, RMConfigurationManager.GetConnectionString("RMXSecurity", m_iClientId), m_iClientId);
                            }
                            else
                            {
                                sResult = objCache.GetGroupName(Conversion.CastToType<int>(p_sModuleSpecificData, out blnSuccess), UserLogin.DatabaseId);
                            }

                        break;
                    default:
                        throw new RMAppException(Globalization.GetString("ResultList.FormatDisplayField.FieldError", m_iClientId));
                }
                return (sResult);
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new CollectionException(Globalization.GetString("SearchResults.FormatDisplayField.FormatError", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Drops Event temp tables

        private void CleanupSearchStoredProc(ref DbConnection p_objConn, string sStoredProcName)
        {

            DbCommand objCommand = null;


            try
            {

                objCommand = p_objConn.CreateCommand();
                objCommand.CommandType = System.Data.CommandType.StoredProcedure;
                objCommand.CommandText = sStoredProcName;
                objCommand.ExecuteNonQuery();

            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {


                if (objCommand != null)
                    objCommand = null;
            }
        }
        private void CleanupSearchStoredProcOracle(string p_sTableName)
        {
            DbCommand objCommand = null;
            DbParameter objParam = null;
            string strRegExpr = "^(?<CONN_STR_VALUE>[^;].+)UID=(?<UID_VALUE>[^;]+);PWD=(?<PWD_VALUE>[^;]+)[;]*$";
            const string USER_ID = "UID_VALUE";
            string strMatchValue = string.Empty;
            Match regExMatch = Regex.Match(this.m_sConnectionString, strRegExpr, RegexOptions.IgnoreCase);
            //Get the matching value for the specified user name
            strMatchValue = regExMatch.Groups[USER_ID].Value;
            try
            {
                objCommand = this.p_Connection.CreateCommand();
                objCommand.CommandType = System.Data.CommandType.StoredProcedure;
                objCommand.CommandText = "CLEANUPSEARCH";
                objParam = objCommand.CreateParameter();
                objParam.Direction = System.Data.ParameterDirection.Input;
                objParam.Value = p_sTableName;
                objParam.ParameterName = "p_Table_Name";
                objCommand.Parameters.Add(objParam);
                //deb:MITS 24404
                objParam = objCommand.CreateParameter();
                objParam.Direction = System.Data.ParameterDirection.Input;
                objParam.Value = strMatchValue;
                objParam.ParameterName = "p_sUserName";
                objCommand.Parameters.Add(objParam);
                //deb
                objCommand.ExecuteNonQuery();
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {

				//mini performance testing 
                if (objParam != null)
                    objParam = null;
                //mini performance testing
                if (objCommand != null)
                    objCommand = null;
            }
        }
        /// <summary>
        /// This function drops the temporary tables related to Event module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupEventSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[EVENT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PI_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PI_ENTITY_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[BODY_PART_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[EVENT_SUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[RPTD_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CAT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PATIENT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PATPRIPHYS_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[DRG_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PATDIAG_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PATACT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PATPROC_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PATSUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PHYS_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PHSUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PSSPEC_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PRIV_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CERT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[MED_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[MEDSUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[STPRIV_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[STCERT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[EVENTQM_TEMP]", p_sSQL, ref p_objConn); //table EVENT_QM is added for QUEST
                //Event Intervention temp table dropped
                DropTable("[INT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[INT_CODE_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[INT_EB_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[INT_EMP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[INT_SUP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[INT_SUPP_TEMP]", p_sSQL, ref p_objConn);
                //mbahl3 mobility changes for event search 
                if (IsMobilityAdjusterEventList)
                    DropTable("[DIARY_TEMP]", p_sSQL, ref p_objConn);
                //apeykov JIRA RMA-8381
                DropTable("[CLAIM_TEMP]", p_sSQL, ref p_objConn);
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupEventSearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Clean Claim temp tables
        /// <summary>
        /// This function drops the temporary tables related to Claims module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupClaimSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                if (IsMobileAdjusterClaimList || IsMobilityAdjusterClaimList)
                {
                    DropTable("[DIARY_TEMP]", p_sSQL, ref p_objConn);
                }
                DropTable("[CLAIM_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CLAIMANT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CLMNT_ENT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CLMNT_ATT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[UNIT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[VEH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[LIT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[LIT_ATT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[EXPERT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[EXP_ENT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ADJ_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ADJ_TEXT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[DEF_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[DEF_ENT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[DEF_ATT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[FUNDS_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PAYEE_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CLAIM_SUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[MCO_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[POL_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[POLMCO_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CLAIMQM_TEMP]", p_sSQL, ref p_objConn); //table EVENT_QM for QUEST Search
                DropTable("[SMV_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CLAIM_LEAVE_TEMP]", p_sSQL, ref p_objConn);//Added support for Leave Mgmt Search
                DropTable("[CLAIM_LV_X_DETAIL_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CLAIM_LV_SUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[LEAVE_RB_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[UNIT_STAT_TEMP]", p_sSQL, ref p_objConn);
                //Anu Tennyson for MITS 18230 01/10/2010 STARTS
                DropTable("[PROPUNIT_TEMP]", p_sSQL, ref p_objConn);
                //Anu Tennyson for MITS 18230 01/10/2010 ENDS
                //Add by kuladeep for mits:23622 07/14/2011 Start
                DropTable("[LITIGATIONSUPP_TEMP]", p_sSQL, ref p_objConn);
                //Add by kuladeep for mits:23622 07/14/2011 End
                //skhare7 subrogation R8 enhancement
                DropTable("[SUB_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[SUB_SPE_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[SUB_ADP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[SUB_COMP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[SUB_ADJ_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[SUB_SUPP_TEMP]", p_sSQL, ref p_objConn);
                //Liability Info
                DropTable("[LIAB_SUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[LIAB_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[LAIB_OWN_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[LIAB_PARTY_TEMP]", p_sSQL, ref p_objConn);
                //Property Loss Info
                DropTable("[PROPLOSS_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PROPLOSS_SUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PROP_OWNR_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PROP_PHOLD_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PROPINV_TEMP]", p_sSQL, ref p_objConn);
                //salvageInfo
                DropTable("[SALU_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[SALP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PROP_SALB_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[UNIT_SALB_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[INJURY_TEMP]", p_sSQL, ref p_objConn);
                //akaur9 12/17/2011 MITS 26842--Start
                DropTable("[CLAIM_PRG_NOTE_TEMP]", p_sSQL, ref p_objConn);
                //akaur9 12/17/2011 MITS 26842--End



                //skhare7 end
                if (string.IsNullOrEmpty(p_sSQL) && m_sDBType == Constants.DB_SQLSRVR)
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpEventSearch");
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpPolicySearch");
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpEnhPolicySearch");


                }
                else
                {
                    CleanupEventSearch(p_sSQL, ref p_objConn);
                    CleanupPolicySearch(p_sSQL, ref p_objConn);
                    CleanupEnhPolicySearch(p_sSQL, ref p_objConn);
                }
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupClaimSearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Drops Policy temp tables
        /// <summary>
        /// This function drops the temporary tables related to Policy module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupPolicySearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[POLICY_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[INS_ENT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[REINS_ENT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CVG_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[INSURED_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[MCO_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[POLSUPP_TEMP]", p_sSQL, ref p_objConn);
                //Gagan Safeway Retrofit Policy Jursidiction : START
                DropTable("[STATE_TEMP]", p_sSQL, ref p_objConn);
                //Gagan Safeway Retrofit Policy Jursidiction : END
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupPolicySearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Drops employee temp tables
        /// <summary>
        /// This function drops the temporary tables related to Employee module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupEmployeeSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[EMPLOYEE_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[EMPSUPP_TEMP]", p_sSQL, ref p_objConn);
                //Added Rakhi for R7:Add Emp Data Elements
                DropTable("[PHONES_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ADDRESSES_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ORG_ADD_TEMP]", p_sSQL, ref p_objConn);
                //Added Rakhi for R7:Add Emp Data Elements
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupEmployeeSearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Drops Catastrophe temp tables -- Anshul Verma
        /// <summary>
        /// This function drops the temporary tables related to catastrophe module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupCatastropheSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[CATASTROPHE_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CATASTROPHESUPP_TEMP]", p_sSQL, ref p_objConn);
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupCatastropheSearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion
        #region Drops Driver temp tables -- Amandeep
        /// <summary>
        /// This function drops the temporary tables related to Driver module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanUpDriverSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[DRIVER_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[DRIVER_TEMP]", p_sSQL, ref p_objConn);
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupDriverSearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion
        //Ashish Ahuja Address //RMA-8753 starts
        #region Drops Address temp tables -- Ashish
        /// <summary>
        /// This function drops the temporary tables related to Driver module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanUpAddressSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[ADDRESS_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ADDRESS_TEMP]", p_sSQL, ref p_objConn);
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupDriverSearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion
        //RMA-8753 End
        #region Drops Entity temp tables
        /// <summary>
        /// This function drops the temporary tables related to Entity module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupEntitySearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[ENTITY_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ENTSUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PHONES_TEMP]", p_sSQL, ref p_objConn); //Added Rakhi for R7:Add Emp Data Elements
                DropTable("[ADDRESSES_TEMP]", p_sSQL, ref p_objConn); //Added Rakhi for R7:Add Emp Data Elements
                DropTable("[ENTITYIDTYPE_TEMP]", p_sSQL, ref p_objConn); //MITS:34276-- Entity ID Type search 
                DropTable("[CONTACT_TEMP]", p_sSQL, ref p_objConn); // rkapoor29 added JIRA 7526
                DropTable("[CODELICEN_TEMP]", p_sSQL, ref p_objConn); //  rkapoor29 added JIRA 7526

            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupEntitySearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Drops Vehicle temp tables
        /// <summary>
        /// This function drops the temporary tables related to Vehicle module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupVehicleSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[VEHICLE_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[VEHSUPP_TEMP]", p_sSQL, ref p_objConn);
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupVehicleSearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Drops Dis Plan temp tables
        /// <summary>
        /// This function drops the temporary tables related to Disability Plan module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupDisPlanSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[PLAN_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CLASS_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[TD_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[INSURED_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PLANSUPP_TEMP]", p_sSQL, ref p_objConn);
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupDisPlanSearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion
        //Shruti Leave Plan Search starts
        #region Drops Leave Plan temp tables
        /// <summary>
        /// This function drops the temporary tables related to Leave Plan module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupLeavePlanSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[LP_PLAN_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[LP_INSURED_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[LP_PLANSUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ORG_TEMP]", p_sSQL, ref p_objConn);
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupLeavePlanSearch.DropTable", m_iClientId), p_objException);
            }
        }
        //Shruti Leave Plan Search ends
        #endregion

        #region Drops Patient temp tables
        /// <summary>
        /// This function drops the temporary tables related to Patient module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupPatientSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[PATIENT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[DRG_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PRI_PHYS_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PATDIAG_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PATACT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PATPROC_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PATSUPP_TEMP]", p_sSQL, ref p_objConn);
                //Added Rakhi for R7:Add Emp Data Elements
                DropTable("[PHONES_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ADDRESSES_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ORG_ADD_TEMP]", p_sSQL, ref p_objConn);
                //Added Rakhi for R7:Add Emp Data Elements
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupPatientSearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Drops Physician temp tables
        /// <summary>
        /// This function drops the temporary tables related to Physician module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupPhysicianSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[PHYS_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PHSUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PSSPEC_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PRIV_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CERT_TEMP]", p_sSQL, ref p_objConn);
                //Added Rakhi for R7:Add Emp Data Elements
                DropTable("[PHONES_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ADDRESSES_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ORG_ADD_TEMP]", p_sSQL, ref p_objConn);
                //Added Rakhi for R7:Add Emp Data Elements
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupPhysicianSearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Drops Med Staff temp tables
        /// <summary>
        /// This function drops the temporary tables related to Medical Staff module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupMedStaffSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[MED_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[MSUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PRIV_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CERT_TEMP]", p_sSQL, ref p_objConn);
                //Added Rakhi for R7:Add Emp Data Elements
                DropTable("[PHONES_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ADDRESSES_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[ORG_ADD_TEMP]", p_sSQL, ref p_objConn);
                //Added Rakhi for R7:Add Emp Data Elements
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupMedStaffSearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Drops Payment temp tables
        /// <summary>
        /// This function drops the temporary tables related to Payment module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupPaymentSearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[FUNDS_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CLAIM_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[FUNDSSUPP_TEMP]", p_sSQL, ref p_objConn);
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupPaymentSearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion
        // Start Naresh Enhanced Policy Search
        #region Drops Enhanced Policy temp tables
        /// Name		: CleanupEnhPolicySearch
        /// Author		: Naresh Chandra Padhy
        /// Date Created: 01/23/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function drops the temporary tables related to Enhanced Policy module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        private void CleanupEnhPolicySearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[POLICY_ENH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[POLICY_ENH_TEMP2]", p_sSQL, ref p_objConn);
                DropTable("[INS_ENT_ENH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[TRANS_ENH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CVG_ENH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[EXP_ENH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[INSURED_ENH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[MCO_ENH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[POLSUPPENH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[RTNG_ENH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[DCNT_ENH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[DTIER_ENH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[BRO_ENT_ENH_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[CVGENHSUPP_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[EXPENHSUPP_TEMP]", p_sSQL, ref p_objConn);
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupEnhPolicySearch.DropTable", m_iClientId), p_objException);
            }
        }
        #endregion
        // End Naresh Enhanced Policy Search

        //Anu Tennyson for MITS 18291 : Starts 10/25/2009
        #region Drops Property temp tables
        /// <summary>
        /// This function drops the temporary tables related to Property module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param> 
        private void CleanupPropertySearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[PROPERTY_UNIT_TEMP]", p_sSQL, ref p_objConn);
                DropTable("[PROPSUPP_TEMP]", p_sSQL, ref p_objConn);
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupPropertySearch.DropTable", m_iClientId), p_objException);
            }
        }

        #endregion
        //Anu Tennyson for MITS 18291 : ENDS

        //skhare7 Diary
        #region Drops Diary temp tables
        /// <summary>
        /// This function drops the temporary tables related to Property module.
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param> 
        private void CleanupDiarySearch(string p_sSQL, ref DbConnection p_objConn)
        {
            try
            {
                DropTable("[DIARY_TEMP]", p_sSQL, ref p_objConn);
                //DropTable("[PROPSUPP_TEMP]", p_sSQL, ref p_objConn);
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.CleanupPropertySearch.DropTable", m_iClientId), p_objException);
            }
        }

        #endregion
        #region Event search
        /// <summary>
        /// This function generates the search data related to Event module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sEventData">Event Data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        public void DoEventSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sEventData, ref DbConnection p_objConn, string p_sOrgLevel, int p_iPageSize, int p_iPageNumber
            , string p_sUserName)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            int iIdx = 0;
            int iIdxPatient = 0;
            string sTable = "";
            long lFieldId = 0;
            string sMultiCodeTableAlias = "";
            DbReader objReaderEvent = null;
            bool bIsPIInvolved = false;
            HipaaLog objHipaa = null;


            //MGaba2:MITS 22353:Start
            SysSettings objSysSettings = null;
            UserLoginLimits objLimits = null;
            string m_EventLimitFilter = string.Empty;
            //MGaba2:MITS 22353:End

            //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
            bool bPersonInvIncluded = false;
            int iPITypeCode = 0; //Add by kuladeep for Publix mits:29986

            try
            {
                m_sUserName = p_sUserName;
                //Delete temp table, if any
                // JP 06.15.2006   Cleanup no matter what.     if (m_sDBType != Constants.DB_ORACLE)
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupEventSearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpEventSearch");
                }

                //Build criteria query
                sFrom = "EVENT";
                //MGaba2:MITS 22353
                //sSelect = "SELECT EVENT.EVENT_ID ";
                sSelect = "SELECT EVENT.EVENT_ID,EVENT.EVENT_TYPE_CODE ";

                //START MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                if ((Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_SUPP") && !Utilities.ExistsInArray(p_arrlstQueryTables, "PI_SUPP")) ||
                    Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_ENTITY") || (Utilities.ExistsInArray(p_arrlstDisplayTables, "PERSON_INVOLVED"))) //mbahl3 mits 28903
                {
                    bPersonInvIncluded = true;
                }
                //END MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380

                //START MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                if (bPersonInvIncluded && sFrom.IndexOf("PERSON_INVOLVED") < 0)
                {
                    sFrom = sFrom + " LEFT OUTER JOIN PERSON_INVOLVED PIJOIN ON EVENT.EVENT_ID = PIJOIN.EVENT_ID";

                }
                //END MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380


                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_ENTITY"))
                {
                    sFrom = sFrom + ",ENTITY ORG_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    if (p_sOrgLevel != "")
                    {
                        sFrom = sFrom + ", ORG_HIERARCHY";
                        sWhere = sWhere + "ORG_HIERARCHY." + p_sOrgLevel + " = ORG_ENTITY.ENTITY_ID"
                        + " AND ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID";
                    }
                    else
                        sWhere = sWhere + "EVENT.DEPT_EID = ORG_ENTITY.ENTITY_ID";
                }




                //MGaba2:MITS 13205:09/04/2008:It should not be added if org hierarchy fields are supplemental
                //Commented the following code as following criteria has been added in "DoSearch" itself:Start
                // Abhishek  changes related to MITS 11341 start
                ////if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_HIERARCHY"))
                //{
                //    sFrom = sFrom + ",ORG_HIERARCHY";
                //    if (sWhere != "")
                //        sWhere = sWhere + " AND ";
                //    sWhere = sWhere + "EVENT.DEPT_EID = ORG_HIERARCHY.DEPARTMENT_EID";
                //}
                //// Abhishek  changes related to MITS 11341 end
                //MGaba2:MITS 13205:09/04/2008:End

                //ankit:Uncommenting the following code and
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_HIE"))
                {
                    //Ankit:Start changes for MITS 28164
                    //sFrom = sFrom + ",ORG_HIERARCHY ORG_HIE";
                    sFrom = sFrom + " INNER JOIN ORG_HIERARCHY ORG_HIE ON EVENT.DEPT_EID = ORG_HIE.DEPARTMENT_EID ";
                    //Ankit:End changes for MITS 28164
                }
                //ankit:Changes end
                //MGaba2:MITS 13205:09/04/2008:End
                //mbahl3 mobility changes for event search 
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM"))
                {
                    sFrom = sFrom + ",CLAIM ";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";

                    sWhere = sWhere + "EVENT.EVENT_ID = CLAIM.EVENT_ID";
                }


                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ADJUSTER_ENTITY"))
                {
                    sFrom = sFrom + ",CLAIM CLAIM_ADJ,CLAIM_ADJUSTER";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_ADJ.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID AND CLAIM_ADJ.EVENT_ID=EVENT.EVENT_ID";

                    sFrom = sFrom + ", ENTITY ADJUSTER_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM_ADJUSTER.ADJUSTER_EID = ADJUSTER_ENTITY.ENTITY_ID";

                    if (IsMobilityAdjusterEventList)
                    {
                        sWhere = sWhere + " AND ";
                        sWhere = sWhere + "ADJUSTER_ENTITY.RM_USER_ID = " + m_lUserId;

                    }
                }
                //mbahl3 mobility changes for event search End
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "EVENT.EVENT_ID");

                //plug in other tables which have criteria
                EventCriteriaTables(p_arrlstQueryTables, ref sFrom, ref sWhere, ref p_sCriteria);   // csingh7 R6 Claim Comment Enhancement


                //Inner Join to this table

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EVENT_X_OSHA"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EVENT_X_OSHA");
                    sTable = p_arrlstDisplayTables[iIdx].ToString();
                    if (sTable.Trim() != "")
                        sSelect = sSelect + "," + sTable;
                    sFrom = sFrom + ",EVENT_X_OSHA";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "EVENT.EVENT_ID = EVENT_X_OSHA.EVENT_ID";
                }

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }

                //START MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                if (bPersonInvIncluded)
                {
                    sSelect += ",PIJOIN.PI_ROW_ID";
                }
                //END MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380


                //added:Yukti, DT:03/04/2014, MITS 35324

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM_PRG_NOTE"))
                {
                    sFrom = sFrom + ",CLAIM_PRG_NOTE";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "EVENT.EVENT_ID = CLAIM_PRG_NOTE.EVENT_ID";
                    sWhere = sWhere + " AND CLAIM_PRG_NOTE.CLAIM_ID = 0";
                    if (p_sQueryFields.Contains("CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH"))
                    {
                        if (m_sDBType == Constants.DB_ORACLE)
                        {
                            p_sQueryFields = p_sQueryFields.Replace("CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH", "CAST(SUBSTR(NOTE_MEMO_CARETECH,0,4000) AS VARCHAR2(4000))");
                        }
                        else
                        {
                            p_sQueryFields = p_sQueryFields.Replace("CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH", "CAST(NOTE_MEMO_CARETECH AS VARCHAR(MAX))");
                        }
                    }
                }

                //Ended:Yukti, DT:03/04/2014, MITS 35324


                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;
                sSQL = sSelect + " [INTO EVENT_TEMP] FROM " + sFrom + " ";
                sWhere = sWhere.Replace("ADJUSTER_ENTITY.CURRENT_ADJ_FLAG", "CLAIM_ADJUSTER.CURRENT_ADJ_FLAG");

                //run criteria query into temp table
                //mbahl3 mobility changes for event search 
                if (IsMobilityAdjusterEventList)
                {
                    string sSelectDiary = string.Empty;
                    string sSQLDiary = string.Empty;
                    string sWhereDiary = string.Empty;
                    string sUser = string.Empty;
                    string sSQLAttachRecord = string.Empty;
                    //string sSQLAttachRecord = string.Empty;

                    // string sSQL1 = sSelect + " FROM " + sFrom + " ";

                    //sSQLAttachRecord = "SELECT DISTINCT ATTACH_RECORDID  FROM WPA_DIARY_ENTRY WHERE ATTACH_TABLE = 'EVENT' AND ASSIGNED_USER = '" + p_sUserName + "'";

                    //sSQL = sSQL1 + " WHERE " + sWhere  +" AND EVENT.EVENT_ID IN ( " +sSQLAttachRecord + ")";

                    //sSQL = "Select * [INTO EVENT_TEMP] from ( " + sSQL + " )a";

                    string sFrom1 = sFrom + ", [DIARY_TEMP]";
                    sFrom1 = sFrom1.Replace(",CLAIM CLAIM_ADJ,CLAIM_ADJUSTER, ENTITY ADJUSTER_ENTITY", "");

                    string sSQL1 = sSelect + " [INTO EVENT_TEMP] FROM " + sFrom1 + " ";
                    //sWhere = "(" + sWhere + " AND Claim.Claim_id in (Select DISTINCT ATTACH_RECORDID from WPA_DIARY_ENTRY Where ATTACH_TABLE = 'CLAIM' and ASSIGNED_USER = '" + p_sUserName + "')) or (" + sWhere + " AND CLAIM.CLAIM_ID not in (Select DISTINCT ATTACH_RECORDID from WPA_DIARY_ENTRY Where ATTACH_TABLE = 'CLAIM' and ASSIGNED_USER = '" + p_sUserName + "') )";
                    sWhereDiary = sWhere;
                    sSelectDiary = sSQL1;
                    if (sWhereDiary.Contains("CLAIM_ADJ.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID"))
                    {
                        sWhereDiary = sWhereDiary.Replace("CLAIM_ADJ.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID", "");
                    }
                    if (sWhereDiary.Contains("AND CLAIM_ADJ.EVENT_ID=EVENT.EVENT_ID"))
                    {
                        sWhereDiary = sWhereDiary.Replace("AND CLAIM_ADJ.EVENT_ID=EVENT.EVENT_ID", "");
                    }
                    if (sWhereDiary.Contains("AND CLAIM_ADJUSTER.ADJUSTER_EID = ADJUSTER_ENTITY.ENTITY_ID"))
                    {
                        sWhereDiary = sWhereDiary.Replace("AND CLAIM_ADJUSTER.ADJUSTER_EID = ADJUSTER_ENTITY.ENTITY_ID", "");
                    }
                    if (sWhereDiary.Contains("AND ADJUSTER_ENTITY.RM_USER_ID = " + m_lUserId + ""))
                    {
                        sUser = "AND ADJUSTER_ENTITY.RM_USER_ID = " + m_lUserId + "";
                        sWhereDiary = sWhereDiary.Replace(sUser, "");
                    }
                    sSQLAttachRecord = "SELECT DISTINCT ATTACH_RECORDID [INTO DIARY_TEMP] FROM WPA_DIARY_ENTRY Where ATTACH_TABLE = 'EVENT' AND STATUS_OPEN='-1' AND DIARY_VOID='0' and ASSIGNED_USER = '" + p_sUserName + "'";
                    ExecSearchSQL(sSQLAttachRecord, ref p_objConn);

                    sWhereDiary = sWhereDiary + " EVENT.EVENT_ID = ATTACH_RECORDID";

                    sSQL = sSQL.Replace("[INTO EVENT_TEMP]", "");
                    sSQL1 = sSQL1.Replace("[INTO EVENT_TEMP]", "");
                    //if (IsMobilityAdjusterEventList)
                    //{
                    sSQL = sSQL.Replace("EVENT.EVENT_DESCRIPTION", "CAST(EVENT.EVENT_DESCRIPTION AS varchar(max))");
                    sSQL1 = sSQL1.Replace("EVENT.EVENT_DESCRIPTION", "CAST(EVENT.EVENT_DESCRIPTION AS varchar(max))");
                    //}
                    sSQL = sSQL.Replace("EVENT.LOCATION_AREA_DESC", "CAST(EVENT.LOCATION_AREA_DESC AS varchar(max))");
                    sSQL1 = sSQL1.Replace("EVENT.LOCATION_AREA_DESC", "CAST(EVENT.LOCATION_AREA_DESC AS varchar(max))");

                    sSQLDiary = sSQL1 + " WHERE " + sWhereDiary;

                    sSQL = sSQL + " WHERE " + sWhere;

                    sSQL = sSQL + " UNION " + sSQLDiary;

                    //sSQL = sSelectDiary + sSQL + " )a";

                    sSQL = "Select * [INTO EVENT_TEMP] from ( " + sSQL + " )a";

                }
                //mbahl3 mobility changes for event search  End 
                else
                {


                    sSQL = sSelect + " [INTO EVENT_TEMP] FROM " + sFrom + " ";
                    if (sWhere != "")
                        sSQL = sSQL + " WHERE " + sWhere;
                }

                ExecSearchSQL(sSQL, ref p_objConn);
                //index EVENT_ID for quick processing in display only fields
                string sIndex = PrepareTempSQL("CREATE INDEX EVTEMP_" + m_lUserId + " ON [EVENT_TEMP](EVENT_ID)");
                p_objConn.ExecuteNonQuery(sIndex);

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[EVENT_TEMP]";
                sWhere = "";

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "COMMENTS_TEXTEVENT")) //csingh7 R6 Claim Comment Enhancement
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "COMMENTS_TEXTEVENT");
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[COMMENTS_TEXT_TEMP]", "EVENT_ID", "ATTACH_RECORDID");
                    sSQL = "SELECT COMMENTS_TEXTEVENT.ATTACH_RECORDID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO COMMENTS_TEXT_TEMP] FROM [EVENT_TEMP],COMMENTS_TEXT COMMENTS_TEXTEVENT  WHERE [EVENT_TEMP].EVENT_ID = COMMENTS_TEXTEVENT.ATTACH_RECORDID AND COMMENTS_TEXTEVENT.ATTACH_TABLE = 'EVENT'";

                    ExecSearchSQL(sSQL, ref p_objConn);
                }

                //apeykov JIRA RMA-8381 - start
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[CLAIM_TEMP]", "EVENT_ID");
                    //Dump results into temp table				
                    sSQL = "SELECT CLAIM.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CLAIM_TEMP]"
                        + " FROM [EVENT_TEMP],CLAIM WHERE"
                        + " [EVENT_TEMP].EVENT_ID = CLAIM.EVENT_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLMEMP_" + m_lUserId + " ON [CLAIM_TEMP](EVENT_ID)"));
                }
                //apeykov JIRA RMA-8381 - end

                //Intervention & related 
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EVENT_X_INTERVENT"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EVENT_X_INTERVENT");
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[INT_TEMP]", "EVENT_ID");
                    sSQL = "SELECT EVENT_X_INTERVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString()
                        + " [INTO INT_TEMP]"
                        + "  FROM [EVENT_TEMP],EVENT_X_INTERVENT  WHERE [EVENT_TEMP].EVENT_ID = EVENT_X_INTERVENT.EVENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EVENT_X_INT_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EVENT_X_INT_SUPP");
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[INT_SUPP_TEMP]", "EVENT_ID");
                    sSQL = "SELECT EVENT_X_INTERVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString()
                        + " [INTO INT_SUPP_TEMP]"
                        + "  FROM [EVENT_TEMP],EVENT_X_INTERVENT,EVENT_X_INT_SUPP  WHERE [EVENT_TEMP].EVENT_ID = EVENT_X_INTERVENT.EVENT_ID"
                        + " AND EVENT_X_INTERVENT.INTERVENT_ROW_ID = EVENT_X_INT_SUPP.INTERVENT_ROW_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EVENT_X_INT_CODES"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EVENT_X_INT_CODES");
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[INT_CODE_TEMP]", "EVENT_ID");
                    sSQL = "SELECT EVENT_X_INTERVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString()
                        + " [INTO INT_CODE_TEMP]"
                        + "  FROM [EVENT_TEMP],EVENT_X_INTERVENT,EVENT_X_INT_CODES WHERE [EVENT_TEMP].EVENT_ID = EVENT_X_INTERVENT.EVENT_ID"
                        + " AND EVENT_X_INTERVENT.INTERVENT_ROW_ID = EVENT_X_INT_CODES.INTERVENT_ROW_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "INT_EB_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "INT_EB_ENTITY");
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[INT_EB_TEMP]", "EVENT_ID");
                    sSQL = "SELECT EVENT_X_INTERVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString()
                        + " [INTO INT_EB_TEMP]"
                        + "  FROM [EVENT_TEMP],EVENT_X_INTERVENT,ENTITY INT_EB_ENTITY WHERE [EVENT_TEMP].EVENT_ID = EVENT_X_INTERVENT.EVENT_ID"
                        + " AND EVENT_X_INTERVENT.EVALUATOR_EID = INT_EB_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "INT_EMP_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "INT_EMP_ENTITY");
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[INT_EMP_TEMP]", "EVENT_ID");
                    sSQL = "SELECT EVENT_X_INTERVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString()
                        + " [INTO INT_EMP_TEMP]"
                        + "  FROM [EVENT_TEMP],EVENT_X_INTERVENT,ENTITY INT_EMP_ENTITY WHERE [EVENT_TEMP].EVENT_ID = EVENT_X_INTERVENT.EVENT_ID"
                        + " AND EVENT_X_INTERVENT.EMPLOYEE_EID = INT_EMP_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "INT_SUP_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "INT_SUP_ENTITY");
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[INT_SUP_TEMP]", "EVENT_ID");
                    sSQL = "SELECT EVENT_X_INTERVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString()
                        + " [INTO INT_SUP_TEMP]"
                        + "  FROM [EVENT_TEMP],EVENT_X_INTERVENT,ENTITY INT_SUP_ENTITY WHERE [EVENT_TEMP].EVENT_ID = EVENT_X_INTERVENT.EVENT_ID"
                        + "  AND EVENT_X_INTERVENT.SUPERVISOR_EID = INT_SUP_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PERSON_INVOLVED"))
                {
                    bIsPIInvolved = true;
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PERSON_INVOLVED");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PI_TEMP]", "EVENT_ID");
                    //START MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PI_TEMP]", "PI_ROW_ID", true);
                    //Dump results into temp table
                    //sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PI_TEMP]"
                    // + " FROM [EVENT_TEMP],PERSON_INVOLVED";
                    // + " WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";

                    //Add by kuladeep "PERSON_INVOLVED.PI_ROW_ID" for mits:26380

                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " PERSON_INVOLVED.EVENT_ID,PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PI_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED";
                    sSQL = sSQL + " WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID ";

                    if (bPersonInvIncluded)//Add by kuladeep for mits:26380
                    {
                        sSQL = sSQL + " AND [EVENT_TEMP].PI_ROW_ID=PERSON_INVOLVED.PI_ROW_ID";
                    }
                    //END MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PITEMP_" + m_lUserId + " ON [PI_TEMP](EVENT_ID)"));
                }

                //START MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PI_SUPP");
                    //Change by kuladeep for mits:26380
                    //JoinIt(ref sFrom, ref sWhere, "[CLAIM_TEMP]", "[PI_SUPP_TEMP]", "EVENT_ID");

                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PI_SUPP_TEMP]", "PI_ROW_ID");

                    //Dump results into temp table

                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " PERSON_INVOLVED.EVENT_ID,PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PI_SUPP_TEMP]"
                      + " FROM [EVENT_TEMP],PERSON_INVOLVED, PI_SUPP WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID"
                    + " AND PERSON_INVOLVED.PI_ROW_ID = PI_SUPP.PI_ROW_ID";
                    if (bPersonInvIncluded)//Add by kuladeep for mits:26380
                    {
                        sSQL = sSQL + " AND [EVENT_TEMP].PI_ROW_ID=PERSON_INVOLVED.PI_ROW_ID";
                    }
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PISTEMP_" + m_lUserId + " ON [PI_SUPP_TEMP](EVENT_ID)"));
                }

                //START MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "RPTD_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "RPTD_ENTITY");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[RPTD_TEMP]", "EVENT_ID");
                    //Dump results into temp table				
                    sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO RPTD_TEMP]"
                        + " FROM [EVENT_TEMP],EVENT,ENTITY RPTD_ENTITY WHERE [EVENT_TEMP].EVENT_ID = EVENT.EVENT_ID"
                        + " AND EVENT.RPTD_BY_EID = RPTD_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX RPETEMP_" + m_lUserId + " ON [RPTD_TEMP](EVENT_ID)"));
                }

                //support for event category code
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EVENT_CAT"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EVENT_CAT");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[CAT_TEMP]", "EVENT_ID");

                    //srajindersin 11/19/2013 - MITS 34198 - DISTINCT Keyword added and tested
                    //Dump results into temp table				
                    sSQL = "SELECT DISTINCT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CAT_TEMP]"
                        + " FROM [EVENT_TEMP],EVENT,CODES EVENT_CAT WHERE [EVENT_TEMP].EVENT_ID = EVENT.EVENT_ID"
                        + " AND EVENT.EVENT_IND_CODE = EVENT_CAT.CODE_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CATTEMP_" + m_lUserId + " ON [CAT_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_ENTITY"))
                {
                    bIsPIInvolved = true;
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PI_ENTITY");
                    //Add temp table to final outer joined query

                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PI_ENTITY_TEMP]", "EVENT_ID");
                    //START MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PI_ENTITY_TEMP]", "PI_ROW_ID", true);
                    //Dump results into temp table


                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID,PERSON_INVOLVED.PI_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PI_ENTITY_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,ENTITY PI_ENTITY WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PI_ENTITY.ENTITY_ID";


                    if (bPersonInvIncluded)
                    {
                        sSQL = sSQL + " AND [EVENT_TEMP].PI_ROW_ID=PERSON_INVOLVED.PI_ROW_ID";
                    }
                    //END MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:26380
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PIETEMP_" + m_lUserId + " ON [PI_ENTITY_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EVENT_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EVENT_SUPP");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[EVENT_SUPP_TEMP]", "EVENT_ID");
                    //Dump results into temp table
                    //MITS 29229 08/06/2012 - Add "PI_SUPP" by srajindersin merge from for mits:28518
                    sSQL = "SELECT DISTINCT EVENT_SUPP.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO EVENT_SUPP_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],EVENT_SUPP WHERE [EVENT_TEMP].EVENT_ID = EVENT_SUPP.EVENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);
                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX EVSTEMP_" + m_lUserId + " ON [EVENT_SUPP_TEMP](EVENT_ID)"));

                }

                //Scan for multi-values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "EVENT_ID");

                        //Temporary - add to CleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //srajindersin 10/29/2013 - MITS 34198 - DISTINCT Keyword added and tested
                        //Dump results into temp table				
                        sSQL = "SELECT DISTINCT [EVENT_TEMP].EVENT_ID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]"
                            + " FROM [EVENT_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias
                            + " WHERE [EVENT_TEMP].EVENT_ID = " + sMultiCodeTableAlias + ".RECORD_ID"
                            + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;

                        ExecSearchSQL(sSQL, ref p_objConn);
                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](EVENT_ID)"));
                    }
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_X_BODY_PART"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PI_X_BODY_PART");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[BODY_PART_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO BODY_PART_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PI_X_BODY_PART WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_ROW_ID = PI_X_BODY_PART.PI_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX BPTEMP_" + m_lUserId + " ON [BODY_PART_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT") || Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT");
                    iIdxPatient = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PATIENT_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    // akaushik5 Changed for MITS 37520 Starts
                    //sSQL = "SELECT PERSON_INVOLVED.EVENT_ID,";
                    sSQL = "SELECT DISTINCT PERSON_INVOLVED.EVENT_ID,";
                    // akaushik5 Changed for MITS 37520 Ends

                    if (iIdx > -1)
                    {
                        sSQL = sSQL + p_arrlstDisplayFields[iIdx].ToString();
                        // Mihika 16-Feb-2006 Corrected the index checking
                        if (iIdxPatient > -1)
                            sSQL = sSQL + ",";
                    }

                    if (iIdxPatient > -1)
                        sSQL = sSQL + p_arrlstDisplayFields[iIdxPatient];

                    sSQL = sSQL + " [INTO PATIENT_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PATIENT,ENTITY | PATIENT_ENTITY WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT.PATIENT_ID AND PATIENT.PATIENT_EID = PATIENT_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PATTEMP_" + m_lUserId + " ON [PATIENT_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PAT_PRI_PHYS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PAT_PRI_PHYS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PATPRIPHYS_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID,";

                    if (iIdx > -1)
                        sSQL = sSQL + p_arrlstDisplayFields[iIdx].ToString();
                    sSQL = sSQL + " [INTO PATPRIPHYS_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PATIENT,ENTITY | PAT_PRI_PHYS WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT.PATIENT_ID AND PATIENT.PRI_PHYSICIAN_EID = PAT_PRI_PHYS.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PATPRIPHTEMP_" + m_lUserId + " ON [PATPRIPHYS_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_DRG_CODES"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_DRG_CODES");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[DRG_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO DRG_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PATIENT_DRG_CODES WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT_DRG_CODES.PATIENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DRGTEMP_" + m_lUserId + " ON [DRG_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_DIAGNOSIS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_DIAGNOSIS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PATDIAG_TEMP]", "EVENT_ID");

                    //Dump results into temp table				
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATDIAG_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PATIENT_DIAGNOSIS WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT_DIAGNOSIS.PATIENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PDIATEMP_" + m_lUserId + " ON [PATDIAG_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_ACT_TAKEN"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_ACT_TAKEN");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PATACT_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATACT_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PATIENT_ACT_TAKEN WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT_ACT_TAKEN.PATIENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PACTTEMP_" + m_lUserId + " ON [PATACT_TEMP](EVENT_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_PROCEDURE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_PROCEDURE");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PATPROC_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATPROC_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PATIENT_PROCEDURE WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT_PROCEDURE.PATIENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PROCTEMP_" + m_lUserId + " ON [PATPROC_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PATSUPP_TEMP]", "EVENT_ID");

                    //Dump results into temp table				
                    sSQL = "SELECT DISTINCT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATSUPP_TEMP]"; //mkaran2 - JIRA RMA-1686 MITS 36897
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PATIENT_SUPP WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PATIENT_ID = PATIENT_SUPP.PATIENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PASTEMP_" + m_lUserId + " ON [PATSUPP_TEMP](EVENT_ID)"));

                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYSICIAN") || Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYSICIAN");
                    iIdxPatient = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PHYS_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID,";

                    if (iIdx > -1)
                    {
                        sSQL = sSQL + p_arrlstDisplayFields[iIdx].ToString();
                        if (iIdxPatient > -1)
                            sSQL = sSQL + ",";
                    }
                    if (iIdxPatient > -1)
                        sSQL = sSQL + p_arrlstDisplayFields[iIdxPatient];

                    sSQL = sSQL + " [INTO PHYS_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PHYSICIAN,ENTITY | PHYS_ENTITY WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYSICIAN.PHYS_EID AND PHYSICIAN.PHYS_EID = PHYS_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PPHYSTEMP_" + m_lUserId + " ON [PHYS_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PHSUPP_TEMP]", "EVENT_ID");

                    //Dump results into temp table        
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PHSUPP_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PHYS_SUPP WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_SUPP.PHYS_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PSPTEMP_" + m_lUserId + " ON [PHSUPP_TEMP](EVENT_ID)"));
                }
                //mbahl3 mits 28903
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_X_INJURY_SUPP"))
                {
                    string sSQLAttachRecord = string.Empty;
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PI_X_INJURY_SUPP");

                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PI_X_INJURY_SUPP_TEMP]", "EVENT_ID");

                    sSQLAttachRecord = "SELECT  [EVENT_TEMP].EVENT_ID,PERSON_INVOLVED.PI_ROW_ID";
                    sSQLAttachRecord = sSQLAttachRecord + " FROM  [EVENT_TEMP],PERSON_INVOLVED,PI_X_INJURY,PI_X_INJURY_SUPP where [EVENT_TEMP].EVENT_ID=PERSON_INVOLVED.EVENT_ID";
                    sSQLAttachRecord = sSQLAttachRecord + " AND PERSON_INVOLVED.PI_ROW_ID=PI_X_INJURY_SUPP.PI_ROW_ID";
                
                    sSQL = "SELECT " + p_arrlstDisplayFields[iIdx].ToString() + "  ,T.EVENT_ID,T.PI_ROW_ID  [INTO PI_X_INJURY_SUPP_TEMP] FROM PI_X_INJURY_SUPP,(" + sSQLAttachRecord + " ) T ";// PI_X_INJURY_SUPP_TEMP;
                    sSQL = sSQL + "  WHERE PI_X_INJURY_SUPP.PI_ROW_ID=T.PI_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INJSUPPTEMP_" + m_lUserId + " ON [PI_X_INJURY_SUPP_TEMP](EVENT_ID)"));
                }
                //if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PI_X_INJURY"))
                //{
                //    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PI_X_INJURY");

                    
                //    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[INJURY_TEMP]", "EVENT_ID");

                   
                //    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO INJURY_TEMP]";
                //    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PI_X_INJURY WHERE [CLAIM_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                //    sSQL = sSQL + " AND PERSON_INVOLVED.PI_ROW_ID = PI_X_INJURY.PI_ROW_ID";
                //    ExecSearchSQL(sSQL, ref p_objConn);

                    
                //    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INJTEMP_" + m_lUserId + " ON [INJURY_TEMP](EVENT_ID)"));
                //}

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_SUB_SPECIALTY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_SUB_SPECIALTY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PSSPEC_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PSSPEC_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PHYS_SUB_SPECIALTY WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_SUB_SPECIALTY.PHYS_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PSPECTEMP_" + m_lUserId + " ON [PSSPEC_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_PRIVS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_PRIVS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[PRIV_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PRIV_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PHYS_PRIVS WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_PRIVS.PHYS_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PRVTEMP_" + m_lUserId + " ON [PRIV_TEMP](EVENT_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_CERTS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_CERTS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[CERT_TEMP]", "EVENT_ID");

                    //Dump results into temp table				
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CERT_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,PHYS_CERTS WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = PHYS_CERTS.PHYS_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CRTTEMP_" + m_lUserId + " ON [CERT_TEMP](EVENT_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "MED_STAFF") || Utilities.ExistsInArray(p_arrlstDisplayTables, "MED_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "MED_STAFF");
                    iIdxPatient = Utilities.IndexInArray(p_arrlstDisplayTables, "MED_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[MED_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID,";
                    if (iIdx > -1)
                    {
                        sSQL = sSQL + p_arrlstDisplayFields[iIdx].ToString();
                        if (iIdxPatient > -1)
                            sSQL = sSQL + ",";
                    }

                    if (iIdxPatient > -1)
                        sSQL = sSQL + p_arrlstDisplayFields[iIdxPatient];

                    sSQL = sSQL + " [INTO MED_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,MED_STAFF,ENTITY | MED_ENTITY WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = MED_STAFF.STAFF_EID AND MED_STAFF.STAFF_EID = MED_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX MEDTEMP_" + m_lUserId + " ON [MED_TEMP](EVENT_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "MED_STAFF_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "MED_STAFF_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[MEDSUPP_TEMP]", "EVENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO MEDSUPP_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,MED_STAFF_SUPP WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = MED_STAFF_SUPP.STAFF_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX MEDSPTEMP_" + m_lUserId + " ON [MEDSUPP_TEMP](EVENT_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "STAFF_PRIVS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "STAFF_PRIVS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[STPRIV_TEMP]", "EVENT_ID");

                    //Dump results into temp table				

                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO STPRIV_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,STAFF_PRIVS WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = STAFF_PRIVS.STAFF_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SPRIVTEMP_" + m_lUserId + " ON [STPRIV_TEMP](EVENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "STAFF_CERTS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "STAFF_CERTS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[STCERT_TEMP]", "EVENT_ID");

                    //Dump results into temp table			
                    sSQL = "SELECT PERSON_INVOLVED.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO STCERT_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],PERSON_INVOLVED,STAFF_CERTS WHERE [EVENT_TEMP].EVENT_ID = PERSON_INVOLVED.EVENT_ID";
                    sSQL = sSQL + " AND PERSON_INVOLVED.PI_EID = STAFF_CERTS.STAFF_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index EVENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SCERTTEMP_" + m_lUserId + " ON [STCERT_TEMP](EVENT_ID)"));
                }

                //Add event_qm table for QUEST
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EVENT_QM"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EVENT_QM");
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[EVENTQM_TEMP]", "EVENT_ID");

                    sSQL = "SELECT EVENT_QM.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO EVENTQM_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],EVENT_QM WHERE [EVENT_TEMP].EVENT_ID=EVENT_QM.EVENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX EVENTQMTEMP_" + m_lUserId + " ON [EVENTQM_TEMP](EVENT_ID)"));
                }

                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ORG_ENTITY");
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[ORG_TEMP]", "EVENT_ID");

                    sSQL = "SELECT EVENT.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO ORG_TEMP]";
                    sSQL = sSQL + " FROM [EVENT_TEMP],EVENT,ENTITY ORG_ENTITY WHERE [EVENT_TEMP].EVENT_ID = EVENT.EVENT_ID";
                    sSQL = sSQL + " AND EVENT.DEPT_EID = ORG_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ORGTEMP_" + m_lUserId + " ON [ORG_TEMP](EVENT_ID)"));
                }

                // Mihika 16-Feb-2006 Changes related to MITS 5034
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EVENT_X_OUTCOME"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EVENT_X_OUTCOME");
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[OUTCOME_TEMP]", "EVENT_ID");

                    sSQL = "SELECT DISTINCT EVENT_X_OUTCOME.EVENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO OUTCOME_TEMP]"; //aaggarwal29: MITS 37557,JIRA 6366 added distinct for event id as it was returning duplicate rows. 
                    sSQL = sSQL + " FROM [EVENT_TEMP],EVENT_X_OUTCOME WHERE [EVENT_TEMP].EVENT_ID=EVENT_X_OUTCOME.EVENT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX EVENTOUTCOMETEMP_" + m_lUserId + " ON [OUTCOME_TEMP](EVENT_ID)"));
                }

                //Added:Yukti, DT:03/04/2014, MITS 35324
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM_PRG_NOTE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM_PRG_NOTE");
                    JoinIt(ref sFrom, ref sWhere, "[EVENT_TEMP]", "[CLAIM_PRG_NOTE_TEMP]", "EVENT_ID");
                    string strTemp = p_arrlstDisplayFields[iIdx].ToString();
                    if (strTemp.ToUpper().Contains("CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH"))
                    {
                        if (m_sDBType == Constants.DB_ORACLE)
                        {
                            strTemp = strTemp.Replace("CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH", "CAST(SUBSTR(NOTE_MEMO_CARETECH,0,4000) AS VARCHAR2(4000))");
                        }
                        else
                        {
                            strTemp = strTemp.Replace("CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH", "CAST(NOTE_MEMO_CARETECH AS VARCHAR(MAX))");
                        }
                    }
                    //Deb : MITS 30982
                    sSQL = "SELECT " + strTemp + ", CLAIM_PRG_NOTE.EVENT_ID" + " [INTO CLAIM_PRG_NOTE_TEMP]"
                        + " FROM [EVENT_TEMP],CLAIM_PRG_NOTE WHERE [EVENT_TEMP].EVENT_ID=CLAIM_PRG_NOTE.EVENT_ID"
                        + " AND CLAIM_PRG_NOTE.CLAIM_ID = 0";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLAIMPRGNOTETEMP_" + m_lUserId + " ON [CLAIM_PRG_NOTE_TEMP](EVENT_ID)"));
                }

                //Ended:Yukti,MITS 35324


                //MGaba2:MITS 22353:Start

                objSysSettings = new SysSettings(p_objConn.ConnectionString,m_iClientId);

                if (objSysSettings.EVTypeAccFlag)//If Event Type Limit is enabled
                {
                    using (DbConnection objDbConnection = DbFactory.GetDbConnection(m_sConnectionString))
                    {
                        objDbConnection.Open();
                        objLimits = new UserLoginLimits((int)UserId, (int)GroupId, objDbConnection, m_iClientId);
                        objDbConnection.Close();
                    }

                    if (objLimits != null)
                    {
                        string[] arr = objLimits.EventAccessTypeList();
                        string sTypeFilter = "([EVENT_TEMP].EVENT_TYPE_CODE NOT IN (";
                        for (int i = 0; i < arr.Length; i++)
                        {
                            if (i > 0)
                                sTypeFilter += ",";
                            sTypeFilter += arr[i];
                        }
                        if (arr.Length == 0)
                            sTypeFilter = "";
                        else
                            sTypeFilter += "))";

                        m_EventLimitFilter = sTypeFilter;

                        if (m_EventLimitFilter != "")
                        {
                            if (sWhere != "")
                                sWhere = sWhere + " AND ";
                            sWhere += m_EventLimitFilter;
                        }
                    }
                }

                //MGaba2:MITS 22353:End

                //Chain everything together
                string sHipaaIDs = string.Empty;
                //p_sEventData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, "EVENT_ID", ref sHipaaIDs);
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sEventData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, "EVENT_ID", ref sHipaaIDs, ref p_sRetSQL);
                }
                else
                {
                    p_sEventData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }
                //Add by kuladeep for Publix mits:29986 Start
                using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
                {
                    iPITypeCode = objCache.GetCodeId("P", "PERSON_INV_TYPE");
                    objCache.Dispose();
                }
                //Add by kuladeep for Publix mits:29986 End
                sSQL = "SELECT EVENT_ID,PI_EID,PI_TYPE_CODE FROM PERSON_INVOLVED WHERE PI_TYPE_CODE= " + iPITypeCode + " AND EVENT_ID IN ("; // Add PI_TYPE_CODE by kuladeep for Publix mits:29986
                if (bIsPIInvolved == true)
                {
                    if (sHipaaIDs.Length > 0)
                    {
                        sSQL += sHipaaIDs + ")";
                        objReaderEvent = p_objConn.ExecuteReader(sSQL);
                        objHipaa = new HipaaLog(m_sConnectionString, m_iClientId);
                        while (objReaderEvent.Read())
                        {
                            objHipaa.LogHippaInfo(Conversion.ConvertObjToInt(objReaderEvent.GetValue("PI_EID"), m_iClientId), Conversion.ConvertObjToStr(objReaderEvent.GetValue("EVENT_ID")), "0", "Search/Event", "VW", m_sUserName);
                        }
                        objReaderEvent.Close();
                        objHipaa = null;
                    }
                }

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoEventSearch.GenericError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReaderEvent != null)
                {
                    objReaderEvent.Close();
                    objReaderEvent.Dispose();
                }
                objSysSettings = null;
                objLimits = null;
            }
        }
        #endregion
        //Start averma62 Catastrophe Search MITS - 28528

        #region Catastrophe Search
        /// <summary>
        /// This function generates the search data related to Entity module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_lEntityTableRestrict">Retricts number of enity tables to be displayed</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sEntityData">Catastrophe Data</param>
        /// <param name="p_sSysEx">Soundex feature parameter</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        /// 
        public void DoCatastropheSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables,
            string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields,
            string p_sCriteria, string p_sJustFieldNames, long p_lCatTableRestrict, ref string p_sRetSQL,
            ref string p_sCatData, string p_sSysEx, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber, string p_sUserName)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            int iIdx = 0;
            long lFieldId = 0;
            string sMultiCodeTableAlias = "";
            SysSettings objSettings = null;
            string sSqlAdmin = string.Empty;
            try
            {
                m_sUserName = p_sUserName;
                //Delete temp table, if any

                //if (m_sDBType != Constants.DB_ORACLE) 
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupCatastropheSearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanupCatastropheSearch");
                }
                objSettings = new SysSettings(p_objConn.ConnectionString,m_iClientId);
                sFrom = "CATASTROPHE";
                sSelect = "SELECT CATASTROPHE.CATASTROPHE_ROW_ID ";
                sWhere = " CATASTROPHE.CATASTROPHE_ROW_ID > 0 ";


                if (p_lCatTableRestrict > 0)
                    sWhere = "CATASTROPHE.CATASTROPHE_ROW_ID <> 0 AND CATASTROPHE.CATASTROPHE_ROW_ID <> " + p_lCatTableRestrict;

                //plug in other tables which have criteria
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CATASTROPHE_SUPP"))
                {
                    sFrom = sFrom + ", CATASTROPHE_SUPP ";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CATASTROPHE.CATASTROPHE_ROW_ID = CATASTROPHE_SUPP.CATASTROPHE_ROW_ID";
                }

                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "CATASTROPHE.CATASTROPHE_ROW_ID");

                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }
                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;

                //run criteria query into temp table
                sSQL = sSelect + " [INTO CATASTROPHE_TEMP] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;

                ExecSearchSQL(sSQL, ref p_objConn);
                //index for quick processing in display only fields
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CATTEMP_" + m_lUserId + " ON [CATASTROPHE_TEMP](CATASTROPHE_ROW_ID)"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[CATASTROPHE_TEMP]";
                sWhere = "";

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CATASTROPHE_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CATASTROPHE_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[CATASTROPHE_TEMP]", "[CATASTROPHESUPP_TEMP]", "CATASTROPHE_ROW_ID");

                    //Dump results into temp table
                    sSQL = "SELECT CATASTROPHESUPP_TEMP.CATASTROPHE_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CATASTROPHESUPP_TEMP]"
                        + " FROM [CATASTROPHE_TEMP], CATASTROPHESUPP_TEMP"
                        + " WHERE [CATASTROPHE_TEMP].CATASTROPHE_ROW_ID = CATASTROPHESUPP_TEMP.CATASTROPHE_ROW_ID ";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CATASTROPHESUPP_TEMP_" + m_lUserId + " ON [CATASTROPHESUPP_TEMP](ENTITY_ID )"));
                }

                //MULTI-CODE Type Supplementals
                //Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[CATASTROPHE_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "CATASTROPHE_ROW_ID");

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [CATASTROPHE_TEMP].CATASTROPHE_ROW_ID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]";
                        sSQL = sSQL + " FROM [CATASTROPHE_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                        sSQL = sSQL + " WHERE [CATASTROPHE_TEMP].CATASTROPHE_ROW_ID = " + sMultiCodeTableAlias + ".RECORD_ID";
                        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](CATASTROPHE_ROW_ID)"));
                    }
                }
                if (!m_bPrintSearch)
                {
                    p_sCatData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sCatData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }
            }
            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoCatastropheSearch.GenericError", m_iClientId), p_objException);
            }
        }
        #endregion

        //End averma62 Catastrophe Search MITS - 28528
        //Start Amandeep Driver Search 

        #region Driver Search
        /// <summary>
        /// This function generates the search data related to Entity module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_lEntityTableRestrict">Retricts number of enity tables to be displayed</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sEntityData">Catastrophe Data</param>
        /// <param name="p_sSysEx">Soundex feature parameter</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        /// 
        public void DoDriverSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables,
            string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields,
            string p_sCriteria, string p_sJustFieldNames, long p_lCatTableRestrict, ref string p_sRetSQL,
            ref string p_sCatData, string p_sSysEx, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber, string p_sUserName)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            int iIdx = 0;
            long lFieldId = 0;
            string sMultiCodeTableAlias = "";
            SysSettings objSettings = null;
            string sSqlAdmin = string.Empty;
            try
            {
                m_sUserName = p_sUserName;
                //Delete temp table, if any

                //if (m_sDBType != Constants.DB_ORACLE) 
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanUpDriverSearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpDriverSearch");
                }
                objSettings = new SysSettings(p_objConn.ConnectionString, m_iClientId);

                //Build criteria query
                sFrom = "DRIVER,ENTITY | DRIVER_ENTITY";
                sWhere = "DRIVER.DRIVER_EID = DRIVER_ENTITY.ENTITY_ID";
                sSelect = "SELECT DRIVER.DRIVER_ROW_ID ";
                //avipinsrivas start : Worked for JIRA - 7767
                if (objSysSettingsGlobal.UseEntityRole)
                    sSelect = string.Concat(sSelect, ",DRIVER.DRIVER_EID ");
                //avipinsrivas end
                //sFrom = "DRIVER";
                //sSelect = "SELECT DRIVER.DRIVER_ROW_ID ";
                //sWhere = " DRIVER.DRIVER_ROW_ID > 0 ";


                if (p_lCatTableRestrict > 0)
                    sWhere = "DRIVER.DRIVER_ROW_ID <> 0 AND DRIVER.DRIVER_ROW_ID <> " + p_lCatTableRestrict;

                //plug in other tables which have criteria

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS_X_PHONEINFO"))
                {
                    sFrom = sFrom + ", ADDRESS_X_PHONEINFO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "DRIVER.DRIVER_EID  = ADDRESS_X_PHONEINFO.ENTITY_ID";
                    bPhoneNumbersIncluded = true;
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "DRIVER_SUPP"))
                {
                    sFrom = sFrom + ", DRIVER_SUPP ";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "DRIVER.DRIVER_ROW_ID = DRIVER_SUPP.DRIVER_ROW_ID";
                }

                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "DRIVER.DRIVER_ROW_ID");

                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }
                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;
                CheckMultiplePhoneAddresses(bPhoneNumbersIncluded, bAddressesIncluded, bOrgHierarchyPhoneIncluded, p_arrlstDisplayTables, ref sSelect, ref bAddressFlag, ref bOrgFlag);
                //run criteria query into temp table
                sSQL = sSelect + " [INTO DRIVER_TEMP] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;

                ExecSearchSQL(sSQL, ref p_objConn);
                //index for quick processing in display only fields
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DRIVERTEMP_" + m_lUserId + " ON [DRIVER_TEMP](DRIVER_ROW_ID)"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[DRIVER_TEMP]";
                sWhere = "";

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[DRIVER_TEMP]", "[PHONES_TEMP]", "DRIVER_ROW_ID");

                    //Dump results into temp table
                    if (bAddressesIncluded || bOrgHierarchyPhoneIncluded)
                    {
                        //if (bAddressesIncluded)
                        //{
                        //    JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[PHONES_TEMP]", "ADDRESS_ID", true);
                        //    sSQL = "SELECT PATIENT.PATIENT_ID,[PATIENT_TEMP].ADDRESS_ID";
                        //}
                        //if (bOrgHierarchyPhoneIncluded)
                        //{
                        //    JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[PHONES_TEMP]", "PHONE_ID", true);
                        //    sSQL = "SELECT PATIENT.PATIENT_ID,[PATIENT_TEMP].PHONE_ID";
                        //}
                    }
                    else
                    {
                        sSQL = "SELECT DRIVER.DRIVER_ROW_ID";
                        //avipinsrivas end
                    }


                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    sSQL = sSQL + " [INTO PHONES_TEMP]"
                        + " FROM DRIVER,ADDRESS_X_PHONEINFO"
                        + " WHERE DRIVER.DRIVER_EID = ADDRESS_X_PHONEINFO.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PHONETEMP_" + m_lUserId + " ON [PHONES_TEMP](DRIVER_ROW_ID)"));
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "DRIVER_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "DRIVER_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[DRIVER_TEMP]", "[DRIVERSUPP_TEMP]", "DRIVER_ROW_ID");

                    //Dump results into temp table
                    sSQL = "SELECT DRIVER_SUPP.DRIVER_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO DRIVERSUPP_TEMP]"
                        + " FROM [DRIVER_TEMP], DRIVER_SUPP"
                        + " WHERE [DRIVER_TEMP].DRIVER_ROW_ID = DRIVER_SUPP.DRIVER_ROW_ID ";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DRIVERSUPPTEMP_" + m_lUserId + " ON [DRIVERSUPP_TEMP](DRIVER_ROW_ID )"));
                }

                //MULTI-CODE Type Supplementals
                //Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[DRIVER_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "DRIVER_ROW_ID");

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [DRIVER_TEMP].DRIVER_ROW_ID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]";
                        sSQL = sSQL + " FROM [DRIVER_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                        sSQL = sSQL + " WHERE [DRIVER_TEMP].DRIVER_ROW_ID = " + sMultiCodeTableAlias + ".RECORD_ID";
                        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](DRIVER_ROW_ID)"));
                    }
                }
                if (!m_bPrintSearch)
                {
                    //avipinsrivas start : Worked for JIRA - 7767
                    if (objSysSettingsGlobal.UseEntityRole)
                        p_sJustFieldNames = p_sJustFieldNames.Replace("[DRIVER_TEMP].DRIVER_ROW_ID", "[DRIVER_TEMP].DRIVER_EID");
                    //avipinsrivas end
                    p_sCatData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sCatData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }
            }
            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoDriverSearch.GenericError", m_iClientId), p_objException);
            }
        }
        #endregion

        //End Amandeep Driver Search 
        #region Address Search 
        /// <summary>
        /// This function generates the search data related to Entity module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_lEntityTableRestrict">Retricts number of enity tables to be displayed</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sEntityData">Catastrophe Data</param>
        /// <param name="p_sSysEx">Soundex feature parameter</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        /// RMA-8753 Ashish Ahuja starts
        public void DoAddressSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables,
            string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields,
            string p_sCriteria, string p_sJustFieldNames, long p_lCatTableRestrict, ref string p_sRetSQL,
            ref string p_sAddressData, string p_sSysEx, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber, string p_sUserName)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            int iIdx = 0;
            int iIdxInsured = 0;
            long lFieldId = 0;
            string sMultiCodeTableAlias = "";

            try
            {
                //Delete temp table, if any
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanUpAddressSearch("", ref p_objConn);
                }
                else
                {
                  //  CleanupSearchStoredProc(ref p_objConn, "CleanUpAddressSearch");
                }

                //Build criteria query
                sFrom = "ADDRESS";
                sSelect = "SELECT ADDRESS.ADDRESS_ID ";

                ////Insured
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS"))
                //{
                //    sFrom = sFrom + ", LP_X_ENTITY, ENTITY LP_INSURED_ENTITY";
                //    if (sWhere != "")
                //        sWhere = sWhere + " AND ";
                //    sWhere = sWhere + "LEAVE_PLAN.LP_ROW_ID = LP_X_ENTITY.LP_ROW_ID AND LP_X_ENTITY.ENTITY_EID = LP_INSURED_ENTITY.ENTITY_ID";
                //}

                //Supplementals
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "LEAVE_PLAN_SUPP"))
                //{
                //    sFrom = sFrom + ", LEAVE_PLAN_SUPP";
                //    if (sWhere != "")
                //        sWhere = sWhere + " AND ";
                //    sWhere = sWhere + "LEAVE_PLAN.LP_ROW_ID = LEAVE_PLAN_SUPP.LP_ROW_ID";
                //}

                //Supplimental Multi-Code Field
               // CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "LEAVE_PLAN.LP_ROW_ID");

                
                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }

                //RMA-14248(Part of RMA-8753 addr master) nshah28 start
                if (sWhere != "")
                    sWhere = sWhere + " AND DELETED_FLAG!=-1";  
                else
                    sWhere = sWhere + " DELETED_FLAG!=-1";
                //RMA-14248(Part of RMA-8753 addr master) nshah28 end

                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;

                //run criteria query into temp table
                sSQL = sSelect + " [INTO ADDRESS_TEMP] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;

                ExecSearchSQL(sSQL, ref p_objConn);

                //index for quick processing in display only fields
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ADDRESSTEMP_" + m_lUserId + " ON [ADDRESS_TEMP](ADDRESS_ID)"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[ADDRESS_TEMP]";
                sWhere = "";

                //Insured
                //if (Utilities.ExistsInArray(p_arrlstDisplayTables, "LP_X_ENTITY") || Utilities.ExistsInArray(p_arrlstDisplayTables, "LP_INSURED_ENTITY"))
                //{
                //    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "LP_X_ENTITY");
                //    iIdxInsured = Utilities.IndexInArray(p_arrlstDisplayTables, "LP_INSURED_ENTITY");

                //    //Add temp table to final outer joined query
                //    JoinIt(ref sFrom, ref sWhere, "[LP_PLAN_TEMP]", "[LP_INSURED_TEMP]", "LP_ROW_ID");

                //    //Dump results into temp table
                //    sSQL = "SELECT LP_X_ENTITY.LP_ROW_ID";
                //    if (iIdx > -1)
                //        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                //    // Mihika 16-Feb-2006 Corrected the index
                //    if (iIdxInsured > -1)
                //        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxInsured].ToString());

                //    sSQL = sSQL + " [INTO LP_INSURED_TEMP]";
                //    sSQL = sSQL + " FROM [LP_PLAN_TEMP], LP_X_ENTITY, ENTITY LP_INSURED_ENTITY";
                //    sSQL = sSQL + " WHERE [LP_PLAN_TEMP].LP_ROW_ID = LP_X_ENTITY.LP_ROW_ID AND LP_X_ENTITY.ENTITY_EID = LP_INSURED_ENTITY.ENTITY_ID";
                //    ExecSearchSQL(sSQL, ref p_objConn);

                //    //index for quick processing
                //    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LPINSDTEMP_" + m_lUserId + " ON [LP_INSURED_TEMP](LP_ROW_ID)"));
                //}

                ////Supplemental
                //if (Utilities.ExistsInArray(p_arrlstDisplayTables, "LEAVE_PLAN_SUPP"))
                //{
                //    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "LEAVE_PLAN_SUPP");

                //    //Add temp table to final outer joined query
                //    JoinIt(ref sFrom, ref sWhere, "[LP_PLAN_TEMP]", "[LP_PLANSUPP_TEMP]", "LP_ROW_ID");

                //    //Dump results into temp table
                //    sSQL = "SELECT LEAVE_PLAN_SUPP.LP_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO LP_PLANSUPP_TEMP]";
                //    sSQL = sSQL + " FROM [LP_PLAN_TEMP], LEAVE_PLAN_SUPP";
                //    sSQL = sSQL + " WHERE [LP_PLAN_TEMP].LP_ROW_ID = LEAVE_PLAN_SUPP.LP_ROW_ID";
                //    ExecSearchSQL(sSQL, ref p_objConn);

                //    //index for quick processing
                //    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LPPLANSPTEMP_" + m_lUserId + " ON [LP_PLANSUPP_TEMP](LP_ROW_ID)"));
                //}

                ////MULTI-CODE Type Supplementals
                ////Scan for Multi-Code Values
                //for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                //{
                //    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                //    {
                //        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                //        sMultiCodeTableAlias = "SMV" + lFieldId;

                //        //Add temp table to final outer joined query
                //        JoinIt(ref sFrom, ref sWhere, "[LP_PLAN_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "LP_ROW_ID");

                //        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                //        //Dump results into temp table
                //        sSQL = "SELECT [LP_PLAN_TEMP].LP_ROW_ID, " + p_arrlstDisplayTables[iIdx] + " [INTO SMV" + lFieldId + "_TEMP]";
                //        sSQL = sSQL + " FROM [LP_PLAN_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                //        sSQL = sSQL + " WHERE [LP_PLAN_TEMP].LP_ROW_ID = " + sMultiCodeTableAlias + ".RECORD_ID";
                //        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                //        ExecSearchSQL(sSQL, ref p_objConn);

                //        //index EVENT_ID for quick processing
                //        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](LP_ROW_ID)"));
                //    }
                //}

                //Chain everything together
                sSQL = "SELECT " + p_sJustFieldNames;
                //p_sDisPlanData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sAddressData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sAddressData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }

            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoAddressSearch.GenericError", m_iClientId), p_objException);
            }
        }
        #endregion

        //End RMA-8753 Ashish Address Search 
       
        #region Entity search
        /// <summary>
        /// This function generates the search data related to Entity module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_lEntityTableRestrict">Retricts number of enity tables to be displayed</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sEntityData">Entity Data</param>
        /// <param name="p_sSysEx">Soundex feature parameter</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        public void DoEntitySearch(string p_sOrderBy, ArrayList p_arrlstQueryTables,
            string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields,
            string p_sCriteria, string p_sJustFieldNames, long p_lEntityTableRestrict, ref string p_sRetSQL,
            ref string p_sEntityData, string p_sSysEx, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber, string p_sUserName)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            int iIdx = 0;
            int iIdxAddr = 0;//RMA-8753 nshah28(Added by ashish)
            long lFieldId = 0;
            string sMultiCodeTableAlias = "";
            bool bFilterOtherPeople = false;
            SysSettings objSettings = null;
            HipaaLog objHippa = null;
            string sSqlAdmin = string.Empty;
            bool bAdmin = false; 
            string sPresentdate = string.Empty; //mbahl3 mits 30224
            try
            {
                m_sUserName = p_sUserName;
                //Delete temp table, if any


                //if (m_sDBType != Constants.DB_ORACLE) 
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupEntitySearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpEntitySearch");
                }
                objSettings = new SysSettings(p_objConn.ConnectionString, m_iClientId);
                sFrom = "ENTITY";                
                sWhere = " ENTITY.ENTITY_ID > 0 AND ENTITY.DELETED_FLAG = 0"; // achouhan3 added TableRef for Deleted flag to avoid ambigous coulmn exception

                //mbahl3 mits 30224

                if (objSettings.StatusActive)
                {
                    //sPresentdate= DateTime.Now.ToShortDateString();
                    sPresentdate = Conversion.ToDbDate(DateTime.Now);
                    
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + " ( ENTITY.EFFECTIVE_DATE <='" + sPresentdate + "'  OR ENTITY.EFFECTIVE_DATE IS NULL OR ENTITY.EFFECTIVE_DATE ='')";
                    sWhere = sWhere + " AND ( ENTITY.EXPIRATION_DATE >='" + sPresentdate + "'  OR ENTITY.EXPIRATION_DATE IS NULL OR ENTITY.EXPIRATION_DATE ='')";

              //mbahl3 mits 30224
			    }
                else
                {
                    p_sJustFieldNames = p_sJustFieldNames + ",[ENTITY_TEMP].EFFECTIVE_DATE EFFDATE,[ENTITY_TEMP].EXPIRATION_DATE EXPDATE";
                    IsEntitySearch = true;
					//mbahl3 mits 30224
                }
                //mbahl3 mits 30224


                //Restrict to a specific entity type (if specified)
                if (p_lEntityTableRestrict > 0)
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    //avipinsrivas start : Worked for Jira-340
                    if (objSysSettingsGlobal.UseEntityRole)
                        sWhere = string.Concat(sWhere, " ENTITY_X_ROLES.ENTITY_TABLE_ID = ", p_lEntityTableRestrict);
                    else
                        sWhere = sWhere + " ENTITY.ENTITY_TABLE_ID = " + p_lEntityTableRestrict;
                    //avipinsrivas End
                }
                //else if(p_lEntityTableRestrict == -1) // JP 05.12.2006 || p_lEntityTableRestrict == 0) Commented by Charanpreet for MITS 12545
                else if (p_lEntityTableRestrict == -1 || p_lEntityTableRestrict == 0)  //Added by Charanpreet for MITS 12545
                {
                    if (p_sSysEx != "" && p_sSysEx.IndexOf("bFunds") != -1) // Saurav, for MITS 11279 : modified 
                    {
                        DbReader objReader = p_objConn.ExecuteReader("SELECT * FROM SYS_PARMS");
                        if (objReader.Read())
                        {
                            bFilterOtherPeople = Convert.ToBoolean(objReader.GetValue("FILTER_OTH_PEOPLE"));
                            objReader.Close();
                        }
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        if (bFilterOtherPeople)
                        {
                            //avipinsrivas start : Worked for Jira-340
                            if (objSysSettingsGlobal.UseEntityRole)
                                sWhere = string.Concat(sWhere, " ((ENTITY_X_ROLES.ENTITY_TABLE_ID < 1005 OR ENTITY_X_ROLES.ENTITY_TABLE_ID > 1012) AND ENTITY_X_ROLES.ENTITY_TABLE_ID <> 1046)");
                            else
                                sWhere = sWhere + " ((ENTITY.ENTITY_TABLE_ID < 1005 OR ENTITY.ENTITY_TABLE_ID > 1012) AND ENTITY.ENTITY_TABLE_ID <> 1046)"; //mwc 05/13/2003
                            //avipinsrivas End
                        }
                        else
                        {
                            //avipinsrivas start : Worked for Jira-340
                            if (objSysSettingsGlobal.UseEntityRole)
                                sWhere = string.Concat(sWhere, " (ENTITY_X_ROLES.ENTITY_TABLE_ID < 1005 OR ENTITY_X_ROLES.ENTITY_TABLE_ID > 1012)");
                        else
                                sWhere = sWhere + " (ENTITY.ENTITY_TABLE_ID < 1005 OR ENTITY.ENTITY_TABLE_ID > 1012)";
                            //avipinsrivas End
                        }
                    }
                    else if (p_lEntityTableRestrict == -1)
                    {
                        if (sWhere != "")
                        {
                            //avipinsrivas start : Worked for Jira-340
                            if (objSysSettingsGlobal.UseEntityRole)
                                sWhere = string.Concat(sWhere, " AND (ENTITY_X_ROLES.ENTITY_TABLE_ID < 1005 OR ENTITY_X_ROLES.ENTITY_TABLE_ID > 1012) ");
                            else
                                sWhere = sWhere + " AND (ENTITY.ENTITY_TABLE_ID < 1005 OR ENTITY.ENTITY_TABLE_ID > 1012) ";
                            //avipinsrivas End
                        }
                    }
                }//Added by Charanpreet for MITS 12545  :end

                    //Address search on Org Hei. Exactly opposite of the above condition
                else if (p_lEntityTableRestrict == ConstGlobals.ORGHEI_SEARCH_PARAM)
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    //avipinsrivas start : Worked for Jira-340
                    if(objSysSettingsGlobal.UseEntityRole)
                        sWhere = string.Concat(sWhere, " (ENTITY_X_ROLES.ENTITY_TABLE_ID >= 1005 AND ENTITY_X_ROLES.ENTITY_TABLE_ID <= 1012)");
                    else
                        sWhere = sWhere + " (ENTITY.ENTITY_TABLE_ID >= 1005 AND ENTITY.ENTITY_TABLE_ID <= 1012)";
                    //avipinsrivas End
                }

                //pmittal5 Confidential Record

                //Raman Bhatia April 25 MITS 24776 

                //deb: Commented as it was affecting the Entity search for Admin user when Conf is Enabled 
                //if (m_bOrgSec)

                if (m_bOrgSec)
                {
                    sSqlAdmin = "SELECT GROUP_ENTITIES FROM ORG_SECURITY INNER JOIN GROUP_MAP ON ORG_SECURITY.GROUP_ID = GROUP_MAP.SUPER_GROUP_ID INNER JOIN ORG_SECURITY_USER ON GROUP_MAP.GROUP_ID = ORG_SECURITY_USER.GROUP_ID WHERE USER_ID = " + UserId;
                    using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, sSqlAdmin))
                    {
                        while (oDbReader.Read())
                        {
                            bAdmin = oDbReader["GROUP_ENTITIES"].ToString() == "<ALL>";
                        }
                    }
                }

                if (m_bOrgSec && !bAdmin)
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + " SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID=" + UserId + ")";
                }
                //End
                //abansal23 MITS 17648 09/10/2009 Starts
                if (m_SelOrgLevels != string.Empty || objSettings.UseEntityRole)        //avipinsrivas start : Worked for JIRA - 7767
                    sSelect = "SELECT DISTINCT ENTITY.ENTITY_ID ";
                else
                    sSelect = "SELECT ENTITY.ENTITY_ID ";
                //abansal23 MITS 17648 09/10/2009 Ends

                //plug in other tables which have criteria
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_SUPP"))
                {
                    sFrom = sFrom + ", ENTITY_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "ENTITY.ENTITY_ID = ENTITY_SUPP.ENTITY_ID";
                }

                // Mihika 16-Feb-2006 Department - Contact Info Tab
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENT_X_CONTACTINFO"))
                {
                    sFrom = sFrom + ", ENT_X_CONTACTINFO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "ENTITY.ENTITY_ID = ENT_X_CONTACTINFO.ENTITY_ID";
                }

                // Mihika 16-Feb-2006 Entity - Operating As Tab
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENT_X_OPERATINGAS"))
                {
                    sFrom = sFrom + ", ENT_X_OPERATINGAS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "ENTITY.ENTITY_ID = ENT_X_OPERATINGAS.ENTITY_ID";
                }
                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS_X_PHONEINFO"))
                {
                    sFrom = sFrom + ", ADDRESS_X_PHONEINFO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "ENTITY.ENTITY_ID = ADDRESS_X_PHONEINFO.ENTITY_ID";
                    bPhoneNumbersIncluded = true;
                }
                //AA//RMA-8753 nshah28(Added by ashish)
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_ADDRESSES"))
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS"))
                {
                    sFrom = sFrom + ", ENTITY_X_ADDRESSES,ADDRESS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    //sWhere = sWhere + "ENTITY.ENTITY_ID = ENTITY_X_ADDRESSES.ENTITY_ID";
                    sWhere = sWhere + "ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID AND ENTITY.ENTITY_ID  = ENTITY_X_ADDRESSES.ENTITY_ID";
                    bAddressesIncluded = true;
                }
                //RMA-8753 nshah28(Added by ashish) END
                //Added Rakhi for R7:Add Emp Data Elements

                //MITS:34276-- Entity ID Type Search
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENT_ID_TYPE"))
                {
                    sFrom = sFrom + ", ENT_ID_TYPE";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "ENTITY.ENTITY_ID = ENT_ID_TYPE.ENTITY_ID";
                    sWhere = sWhere + " AND ENT_ID_TYPE.DELETED_FLAG = 0";
                    bEntityIdType = true;
                }
//Start : Added By Nitika for AIC Gap 9
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_CODELICEN"))
                {
                    sFrom = sFrom + ", ENTITY_X_CODELICEN";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "ENTITY.ENTITY_ID = ENTITY_X_CODELICEN.ENTITY_ID";
                }
                //End : Added By Nitika for AIC Gap 9

                //avipinsrivas start : Worked for Jira-340
                if (objSettings.UseEntityRole)
                {
                    //avipinsrivas start : Worked for JIRA - 13491 (Issue from Epic 7767 and Story 13196)
                    //sFrom = sFrom + ", ENTITY_X_ROLES";
                    //if (sWhere != "")
                    //    sWhere = sWhere + " AND ";
                    //sWhere = sWhere + "ENTITY.ENTITY_ID = ENTITY_X_ROLES.ENTITY_ID";
                    sFrom = string.Concat(sFrom, " LEFT OUTER JOIN ENTITY_X_ROLES ON ENTITY.ENTITY_ID = ENTITY_X_ROLES.ENTITY_ID");
                    //avipinsrivas end
                }
                //avipinsrivas End
                //Now Supplimental Multi-Code Field allowed
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "ENTITY.ENTITY_ID");

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }
                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;

                //Added Rakhi for R7:Add Emp Data Elements
                if (bAddressesIncluded && !bPhoneNumbersIncluded)
                {
                    sSelect += ",ENTITY_X_ADDRESSES.ADDRESS_ID";
                }
                else if (bPhoneNumbersIncluded && !bAddressesIncluded)
                {
                    sSelect += ",ADDRESS_X_PHONEINFO.PHONE_ID";
                }
                //Added Rakhi for R7:Add Emp Data Elements
                //abansal23 MITS 17648 09/12/2009 Starts
                if (m_SelOrgLevels != string.Empty && sFrom != "")
                {
                    if (objSettings.UseEnhPolFlag == 0)
                    {
                        sFrom = sFrom + ",POLICY p,POLICY_X_INSURED pxi";
                    }
                    else
                    {
                        sFrom = sFrom + ",POLICY_ENH p, POLICY_X_INSRD_ENH pxi";
                    }
                    if (m_TableName == "INSURERS")
                        sWhere = sWhere + " AND entity.entity_id = p.insurer_eid AND p.policy_id = pxi.policy_id AND pxi.insured_eid IN (" + m_SelOrgLevels + ")";
                    else if (m_TableName == "BROKER")
                        sWhere = sWhere + " AND entity.entity_id = p.BROKER_EID AND p.policy_id = pxi.policy_id AND pxi.insured_eid IN (" + m_SelOrgLevels + ")";
                }
                //abansal23 MITS 17648 09/12/2009 Ends

 				//mbahl3 Mits 30224
                if(!objSettings.StatusActive)
                {
                    sSelect = sSelect + ", ENTITY.EFFECTIVE_DATE, ENTITY.EXPIRATION_DATE "; //srajindersin 1/30/2014 MITS 35117
                }
				//mbahl3 Mits 30224

                ////avipinsrivas start : Worked for Jira-340
                //if (objSettings.UseEntityRole)
                //{
                //    sSelect = string.Concat(sSelect, ", ", "ENTITY_X_ROLES.ER_ROW_ID");
                //    p_sJustFieldNames = string.Concat(p_sJustFieldNames, ", [ENTITY_TEMP].ER_ROW_ID ENTITYROLEROWID");
                //}
                ////avipinsrivas End
                
                //run criteria query into temp table
                sSQL = sSelect + " [INTO ENTITY_TEMP] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;


                ExecSearchSQL(sSQL, ref p_objConn);
            

                string sIndex = PrepareTempSQL("CREATE INDEX ENTTEMP_" + m_lUserId + " ON [ENTITY_TEMP](ENTITY_ID)");
                p_objConn.ExecuteNonQuery(sIndex);

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[ENTITY_TEMP]";
                sWhere = "";

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ENTITY_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[ENTITY_TEMP]", "[ENTSUPP_TEMP]", "ENTITY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT ENTITY_SUPP.ENTITY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO ENTSUPP_TEMP]"
                        + " FROM [ENTITY_TEMP], ENTITY_SUPP"
                        + " WHERE [ENTITY_TEMP].ENTITY_ID = ENTITY_SUPP.ENTITY_ID ";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ENTSUPPTEMP_" + m_lUserId + " ON [ENTSUPP_TEMP](ENTITY_ID )"));
                }

                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[ENTITY_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "ENTITY_ID");

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [ENTITY_TEMP].ENTITY_ID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]";
                        sSQL = sSQL + " FROM [ENTITY_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                        sSQL = sSQL + " WHERE [ENTITY_TEMP].ENTITY_ID = " + sMultiCodeTableAlias + ".RECORD_ID";
                        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;

                        ExecSearchSQL(sSQL, ref p_objConn);

                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](ENTITY_ID)"));
                    }
                }

                // Mihika 16-Feb-2006 (Contact Info Tab)
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENT_X_CONTACTINFO"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ENT_X_CONTACTINFO");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[ENTITY_TEMP]", "[CONTACT_TEMP]", "ENTITY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT ENT_X_CONTACTINFO.ENTITY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CONTACT_TEMP]";
                    sSQL = sSQL + " FROM [ENTITY_TEMP], ENT_X_CONTACTINFO";
                    sSQL = sSQL + " WHERE [ENTITY_TEMP].ENTITY_ID = ENT_X_CONTACTINFO.ENTITY_ID ";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CONTTEMP_" + m_lUserId + " ON [CONTACT_TEMP](ENTITY_ID )"));
                }

                // Mihika 16-Feb-2006 (Operating As Tab)
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENT_X_OPERATINGAS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ENT_X_OPERATINGAS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[ENTITY_TEMP]", "[OA_TEMP]", "ENTITY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT ENT_X_OPERATINGAS.ENTITY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO OA_TEMP]";
                    sSQL = sSQL + " FROM [ENTITY_TEMP], ENT_X_OPERATINGAS";
                    sSQL = sSQL + " WHERE [ENTITY_TEMP].ENTITY_ID = ENT_X_OPERATINGAS.ENTITY_ID ";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX OATEMP_" + m_lUserId + " ON [OA_TEMP](ENTITY_ID )"));
                }
                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[ENTITY_TEMP]", "[PHONES_TEMP]", "ENTITY_ID");

                    //Dump results into temp table
                    if (bAddressesIncluded)
                    {
                        JoinIt(ref sFrom, ref sWhere, "[ENTITY_TEMP]", "[PHONES_TEMP]", "ADDRESS_ID", true);
                        //avipinsrivas Start : Worked for Jira-9251 (Issue - 4634 / Epic - 340)
                        //sSQL = "SELECT ADDRESS_X_PHONEINFO.ENTITY_ID,[ENTITY_TEMP].ADDRESS_ID";
                        sSQL = string.Concat("SELECT", objSettings.UseEntityRole ? " DISTINCT" : "", " ADDRESS_X_PHONEINFO.ENTITY_ID,[ENTITY_TEMP].ADDRESS_ID");
                        //avipinsrivas End
                    }
                    else
                    {
                        //avipinsrivas Start : Worked for Jira-9251 (Issue - 4634 / Epic - 340)
                        //sSQL = "SELECT ADDRESS_X_PHONEINFO.ENTITY_ID";
                        sSQL = string.Concat("SELECT", objSettings.UseEntityRole ? " DISTINCT" : "", " ADDRESS_X_PHONEINFO.ENTITY_ID");
                        //avipinsrivas End
                    }
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    sSQL = sSQL + " [INTO PHONES_TEMP]";
                    sSQL = sSQL + " FROM [ENTITY_TEMP], ADDRESS_X_PHONEINFO";
                    sSQL = sSQL + " WHERE [ENTITY_TEMP].ENTITY_ID = ADDRESS_X_PHONEINFO.ENTITY_ID ";


                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PHONETEMP_" + m_lUserId + " ON [PHONES_TEMP](ENTITY_ID )"));
                }
                //AA//RMA-8753 nshah28(Added by ashish)
                //if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES"))
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstDisplayTables, "ADDRESS"))
                {


                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES");
                    iIdxAddr = Utilities.IndexInArray(p_arrlstDisplayTables, "ADDRESS");//RMA-8753 nshah28(Added by ashish)
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[ENTITY_TEMP]", "[ADDRESSES_TEMP]", "ENTITY_ID");
                    //Dump results into temp table

                    if (bPhoneNumbersIncluded)
                    {
                        JoinIt(ref sFrom, ref sWhere, "[ENTITY_TEMP]", "[ADDRESSES_TEMP]", "PHONE_ID", true);
                        //avipinsrivas Start : Worked for Jira-9251 (Issue - 4634 / Epic - 340)
                        //sSQL = "SELECT ENTITY_X_ADDRESSES.ENTITY_ID,[ENTITY_TEMP].PHONE_ID";
                        sSQL = string.Concat("SELECT", objSettings.UseEntityRole ? " DISTINCT" : "", " ENTITY_X_ADDRESSES.ENTITY_ID,[ENTITY_TEMP].PHONE_ID");
                        //avipinsrivas End
                    }
                    else
                    {
                        //avipinsrivas Start : Worked for Jira-9251 (Issue - 4634 / Epic - 340)
                        //sSQL = "SELECT ENTITY_X_ADDRESSES.ENTITY_ID";
                        sSQL = string.Concat("SELECT",objSettings.UseEntityRole ? " DISTINCT" : "", " ENTITY_X_ADDRESSES.ENTITY_ID");
                        //avipinsrivas End
                    }

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    //RMA-8753 nshah28(Added by ashish)
                    if (iIdxAddr > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxAddr].ToString());
                    sSQL = sSQL + " [INTO ADDRESSES_TEMP]";
                    sSQL = sSQL + " FROM [ENTITY_TEMP],ENTITY_X_ADDRESSES,ADDRESS";
                    sSQL = sSQL + " WHERE ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID AND [ENTITY_TEMP].ENTITY_ID = ENTITY_X_ADDRESSES.ENTITY_ID";
                    //RMA-8753 nshah28(Added by ashish) END
                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ADDTEMP_" + m_lUserId + " ON [ADDRESSES_TEMP](ENTITY_ID)"));

                }
                //Added Rakhi for R7:Add Emp Data Elements

                //duplicate code --commented by sranjan9 -RMA-7526
                //// Added by Nitika for AIC Gap 9
                //                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_CODELICEN"))
                //                {
                //                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ENTITY_X_CODELICEN");

                //                    //Add temp table to final outer joined query
                //                    JoinIt(ref sFrom, ref sWhere, "[ENTITY_TEMP]", "[CODELICEN_TEMP]", "ENTITY_ID");

                //                    //Dump results into temp table
                //                    sSQL = "SELECT ENTITY_X_CODELICEN.ENTITY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CODELICEN_TEMP]";
                //                    sSQL = sSQL + " FROM [ENTITY_TEMP], ENTITY_X_CODELICEN";
                //                    sSQL = sSQL + " WHERE [ENTITY_TEMP].ENTITY_ID = ENTITY_X_CODELICEN.ENTITY_ID ";
                //                    ExecSearchSQL(sSQL, ref p_objConn);

                //                    //index PATIENT_ID for quick processing
                //                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CODELICTEMP_" + m_lUserId + " ON [CODELICEN_TEMP](ENTITY_ID )"));
                //                }
                //                // Added by Nitika for AIC Gap 9

                //MITS:34276-- Entity ID Type search criteria start
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENT_ID_TYPE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ENT_ID_TYPE");

                    //add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[ENTITY_TEMP]", "[ENTITYIDTYPE_TEMP]", "ENTITY_ID");

                    //dump results into temp table
                    //avipinsrivas Start : Worked for Jira-9251 (Issue - 4634 / Epic - 340)
                    //sSQL = "SELECT ENT_ID_TYPE.ENTITY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO ENTITYIDTYPE_TEMP]";
                    sSQL = string.Concat("SELECT", objSettings.UseEntityRole ? " DISTINCT" : "", " ENT_ID_TYPE.ENTITY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO ENTITYIDTYPE_TEMP]");
                    //avipinsrivas End
                    sSQL = sSQL + " from [ENTITY_TEMP], ENT_ID_TYPE";
                    sSQL = sSQL + " where [ENTITY_TEMP].ENTITY_ID = ENT_ID_TYPE.ENTITY_ID ";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CONTTEMP_" + m_lUserId + " ON [ENTITYIDTYPE_TEMP](ENTITY_ID )"));

                }
                //MITS:34276-- Entity ID Type search criteria end
				// Added by Nitika for AIC Gap 9
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_CODELICEN"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ENTITY_X_CODELICEN");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[ENTITY_TEMP]", "[CODELICEN_TEMP]", "ENTITY_ID");

                    //Dump results into temp table
                    //avipinsrivas Start : Worked for Jira-9251 (Issue - 4634 / Epic - 340)
                    //sSQL = "SELECT ENTITY_X_CODELICEN.ENTITY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CODELICEN_TEMP]";
                    sSQL = string.Concat("SELECT", objSettings.UseEntityRole ? " DISTINCT" : "", " ENTITY_X_CODELICEN.ENTITY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CODELICEN_TEMP]");
                    //avipinsrivas End
                    sSQL = sSQL + " FROM [ENTITY_TEMP], ENTITY_X_CODELICEN";
                    sSQL = sSQL + " WHERE [ENTITY_TEMP].ENTITY_ID = ENTITY_X_CODELICEN.ENTITY_ID ";
                    ExecSearchSQL(sSQL, ref p_objConn);
                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CODELICTEMP_" + m_lUserId + " ON [CODELICEN_TEMP](ENTITY_ID )"));
                }
                // Added by Nitika for AIC Gap 9

                //Chain everything together
                string sHipaaIDs = string.Empty;
                string[] aHipaaIDs;
                //p_sEntityData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, "ENTITY_ID", ref sHipaaIDs);
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sEntityData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, "ENTITY_ID", ref sHipaaIDs, ref p_sRetSQL);
                }
                else
                {
                    p_sEntityData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }
                if (p_lEntityTableRestrict == 1061)
                {
                    objHippa = new HipaaLog(m_sConnectionString, m_iClientId);
                    aHipaaIDs = sHipaaIDs.Split(',');
                    for (int i = 0; i < aHipaaIDs.Length; i++)
                    {
                        objHippa.LogHippaInfo(Conversion.ConvertObjToInt(aHipaaIDs[i], m_iClientId), "0", "0", "Search", "VW", m_sUserName);
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoEntitySearch.GenericError", m_iClientId), p_objException);
            }
         
        }
        //avipinsrivas start : Worked for Jira-340 (Added parameter erRowID which will come only if EntityRoleSetting will be on in General System Parameter Setups)
        private string GetEntityFormName(int entityId)
        //private string GetEntityFormName(int entityId)
        {
		//mini performance tseting 
            LocalCache objCache = null;
            int p_iTableId = 0;
            try
            {
                if (entityId == 0)
                    return "entitymaint";

               //mini performance tseting 
                using (objCache = new LocalCache(m_sConnectionString, m_iClientId))
               {
                   p_iTableId = objCache.GetOrgTableId(entityId);
                   //avipinsrivas start : Worked on JIRA - 14241 (For Epic 7767 and Story 13197)
                   if (objSysSettingsGlobal.UseEntityRole)
                   {
                       if (p_iTableId >= 1005 && p_iTableId <= 1012)
                           return "orghierarchymaint|" + p_iTableId;
                       else
                           return "entitymaint";
                   }
                   //avipinsrivas end

                    if (p_iTableId == objCache.GetTableId("EMPLOYEES"))
                        return "employee";
                    else if (p_iTableId == objCache.GetTableId("PHYSICIANS"))
                        return "physician";
                    else if (p_iTableId == objCache.GetTableId("MEDICAL_STAFF"))
                        return "staff";
                    else
                    {
                        using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
                        {
                            objConn.Open();
                            int iGlossType = objCache.GetGlossaryType(p_iTableId);

                            if (iGlossType == 7)
                                return "people";
                            else
                            {
                                if (p_iTableId >= 1005 && p_iTableId <= 1012)
                                {
                                    // We will return "orghierarchymaint" once this is implemented
                                    // in the nav tree.For now we will open an org hierarchy record
                                    // in the entity maintenance screen when a result is clicked on the
                                    // search result screen (R4 Implementation).
                                    return "orghierarchymaint|" + p_iTableId;
                                    //return "entitymaint";
                                }
                                else
                                {
                                    return "entitymaint";
                                }
                            }
                        }
                    }
                }
            }
            finally
            {
                if (objCache != null)
                    objCache.Dispose();
            }
        }

        #endregion

		#region Patient search
        // akaushik5 Chnaged for MITS 30805 Starts
        /// <summary>
        /// This function generates the search data related to Patient module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sPatientData">Patient data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        /// <param name="p_sOrgLevel">The P_S org level.</param>
        /// <param name="p_iPageSize">Size of the p_i page.</param>
        /// <param name="p_iPageNumber">The p_i page number.</param>
        /// <param name="p_sUserName">Name of the P_S user.</param>
        /// <param name="dedupePatients">if set to <c>true</c> [dedupe patients].</param>
        // public void DoPatientSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sPatientData, ref DbConnection p_objConn, string p_sOrgLevel, int p_iPageSize, int p_iPageNumber, string p_sUserName)
        public void DoPatientSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sPatientData, ref DbConnection p_objConn, string p_sOrgLevel, int p_iPageSize, int p_iPageNumber, string p_sUserName, bool dedupePatients)
        // akaushik5 Chnaged for MITS 30805 Ends
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            string sMultiCodeTableAlias = "";
            int iIdx = 0;
            int iIdxAddr = 0;//AA//RMA-8753 nshah28(Added by ashish)
            long lFieldId = 0;
            DbReader objReaderPatient = null;
            HipaaLog objHippa = null;
            try
            {
                m_sUserName = p_sUserName;
                //Delete temp table, if any
                //if (m_sDBType != Constants.DB_ORACLE)
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupPatientSearch("", ref p_objConn);
                }
                else
                {

                    CleanupSearchStoredProc(ref p_objConn, "CleanUpPatientSearch");
                }

                //Build criteria query
                sFrom = "PATIENT,ENTITY | PATIENT_ENTITY";
                sWhere = "PATIENT.PATIENT_EID = PATIENT_ENTITY.ENTITY_ID";
                sSelect = "SELECT PATIENT.PATIENT_ID ";
                //avipinsrivas start : Worked for JIRA - 7767
                if (objSysSettingsGlobal.UseEntityRole)
                    sSelect = string.Concat(sSelect, ",PATIENT.PATIENT_EID ");
                //avipinsrivas end
                //plug in other tables which have criteria
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PAT_PRI_PHYS"))
                {
                    sFrom = sFrom + ", ENTITY | PAT_PRI_PHYS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PATIENT.PRI_PHYSICIAN_EID = PAT_PRI_PHYS.ENTITY_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PATIENT_DRG_CODES"))
                {
                    sFrom = sFrom + ", PATIENT_DRG_CODES";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PATIENT.PATIENT_ID = PATIENT_DRG_CODES.PATIENT_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PATIENT_DIAGNOSIS"))
                {
                    sFrom = sFrom + ", PATIENT_DIAGNOSIS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PATIENT.PATIENT_ID = PATIENT_DIAGNOSIS.PATIENT_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PATIENT_ACT_TAKEN"))
                {
                    sFrom = sFrom + ", PATIENT_ACT_TAKEN";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PATIENT.PATIENT_ID = PATIENT_ACT_TAKEN.PATIENT_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PATIENT_PROCEDURE"))
                {
                    sFrom = sFrom + ", PATIENT_PROCEDURE";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PATIENT.PATIENT_ID = PATIENT_PROCEDURE.PATIENT_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PATIENT_SUPP"))
                {
                    sFrom = sFrom + ", PATIENT_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PATIENT.PATIENT_ID = PATIENT_SUPP.PATIENT_ID";
                }

                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_ENTITY"))
                {
                    sFrom = sFrom + ",ENTITY ORG_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    if (p_sOrgLevel != "")
                    {
                        sFrom = sFrom + ", ORG_HIERARCHY";
                        sWhere = sWhere + "ORG_HIERARCHY." + p_sOrgLevel + " = ORG_ENTITY.ENTITY_ID";
                        sWhere = sWhere + " AND ORG_HIERARCHY.DEPARTMENT_EID = PATIENT.FACILITY_DEPT_EID";
                    }
                    else
                        sWhere = sWhere + "PATIENT.FACILITY_DEPT_EID = ORG_ENTITY.ENTITY_ID";
                }

                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS_X_PHONEINFO"))
                {
                    sFrom = sFrom + ", ADDRESS_X_PHONEINFO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PATIENT.PATIENT_EID  = ADDRESS_X_PHONEINFO.ENTITY_ID";
                    bPhoneNumbersIncluded = true;
                }

                //AA//RMA-8753 nshah28(Added by ashish)
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_ADDRESSES"))
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS"))
                {

                    sFrom = sFrom + ", ENTITY_X_ADDRESSES, ADDRESS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID AND PATIENT.PATIENT_EID  = ENTITY_X_ADDRESSES.ENTITY_ID";
                    bAddressesIncluded = true;

                }
                //RMA-8753 nshah28(Added by ashish) END
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_ADDRESS_X_PHONEINFO"))
                {
                    sFrom = sFrom + ",ADDRESS_X_PHONEINFO ORG_ADDRESS_X_PHONEINFO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PATIENT.PATIENT_EID  = ORG_ADDRESS_X_PHONEINFO.ENTITY_ID";
                    bOrgHierarchyPhoneIncluded = true;
                }
                //Added Rakhi for R7:Add Emp Data Elements
                //Supplimental Multi-Code Field
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "PATIENT.PATIENT_ID");

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }

                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;
                //Added Rakhi for R7:Add Emp Data Elements
                CheckMultiplePhoneAddresses(bPhoneNumbersIncluded, bAddressesIncluded, bOrgHierarchyPhoneIncluded, p_arrlstDisplayTables, ref sSelect, ref bAddressFlag, ref bOrgFlag);
                //Added Rakhi for R7:Add Emp Data Elements
				//run criteria query into temp table
				sSQL = sSelect + " [INTO PATIENT_TEMP] FROM " + sFrom + " ";

                // akaushik5 Added For MITS 30805 Starts
                if (dedupePatients)
                {
                    string deDupeQuery = string.Format("(SELECT MAX(PATIENT_ID) FROM PATIENT, ENTITY PATIENT_ENTITY WHERE {0} Group by PATIENT.PATIENT_EID)", sWhere);
                    sWhere += string.Format(" AND PATIENT.PATIENT_ID IN {0}", deDupeQuery);
                }
                // akaushik5 Added For MITS 30805 Ends
                
                if (sWhere != "") 
					sSQL = sSQL + " WHERE " + sWhere;

                ExecSearchSQL(sSQL, ref p_objConn);

                //index EVENT_ID for quick processing
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PATTEMP_" + m_lUserId + " ON [PATIENT_TEMP](PATIENT_ID)"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[PATIENT_TEMP]";
                sWhere = "";

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_DRG_CODES"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_DRG_CODES");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[DRG_TEMP]", "PATIENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PATIENT_DRG_CODES.PATIENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO DRG_TEMP]";
                    sSQL = sSQL + " FROM [PATIENT_TEMP],PATIENT_DRG_CODES";
                    sSQL = sSQL + " WHERE [PATIENT_TEMP].PATIENT_ID = PATIENT_DRG_CODES.PATIENT_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DRGTEMP_" + m_lUserId + " ON [DRG_TEMP](PATIENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PAT_PRI_PHYS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PAT_PRI_PHYS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[PRI_PHYS_TEMP]", "PATIENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PATIENT.PATIENT_ID,";
                    if (iIdx > -1)
                        sSQL = sSQL + p_arrlstDisplayFields[iIdx].ToString();

                    sSQL = sSQL + " [INTO PRI_PHYS_TEMP]";
                    //srajindersin 5/20/2014
                    sSQL = sSQL + " FROM PATIENT,ENTITY PAT_PRI_PHYS WHERE ";
                    sSQL = sSQL + " PATIENT.PRI_PHYSICIAN_EID = PAT_PRI_PHYS.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PATPRIPHTEMP_" + m_lUserId + " ON [PRI_PHYS_TEMP](PATIENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_DIAGNOSIS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_DIAGNOSIS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[PATDIAG_TEMP]", "PATIENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PATIENT_DIAGNOSIS.PATIENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATDIAG_TEMP]";
                    sSQL = sSQL + " FROM [PATIENT_TEMP],PATIENT_DIAGNOSIS";
                    sSQL = sSQL + " WHERE [PATIENT_TEMP].PATIENT_ID = PATIENT_DIAGNOSIS.PATIENT_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PDIATEMP_" + m_lUserId + " ON [PATDIAG_TEMP](PATIENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_ACT_TAKEN"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_ACT_TAKEN");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[PATACT_TEMP]", "PATIENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PATIENT_ACT_TAKEN.PATIENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATACT_TEMP]";
                    sSQL = sSQL + " FROM [PATIENT_TEMP],PATIENT_ACT_TAKEN";
                    sSQL = sSQL + " WHERE [PATIENT_TEMP].PATIENT_ID = PATIENT_ACT_TAKEN.PATIENT_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ACTTEMP_" + m_lUserId + " ON [PATACT_TEMP](PATIENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_PROCEDURE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_PROCEDURE");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[PATPROC_TEMP]", "PATIENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PATIENT_PROCEDURE.PATIENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATPROC_TEMP]";
                    sSQL = sSQL + " FROM [PATIENT_TEMP],PATIENT_PROCEDURE";
                    sSQL = sSQL + " WHERE [PATIENT_TEMP].PATIENT_ID = PATIENT_PROCEDURE.PATIENT_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PROCTEMP_" + m_lUserId + " ON [PATPROC_TEMP](PATIENT_ID)"));
                }

                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ORG_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[ORG_TEMP]", "PATIENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PATIENT.PATIENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO ORG_TEMP]";
                    sSQL = sSQL + " FROM [PATIENT_TEMP],PATIENT,ENTITY ORG_ENTITY WHERE [PATIENT_TEMP].PATIENT_ID = PATIENT.PATIENT_ID";
                    sSQL = sSQL + " AND PATIENT.FACILITY_DEPT_EID = ORG_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ORGTEMP_" + m_lUserId + " ON [ORG_TEMP](PATIENT_ID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PATIENT_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PATIENT_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[PATSUPP_TEMP]", "PATIENT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT PATIENT_SUPP.PATIENT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PATSUPP_TEMP]"
                        + " FROM [PATIENT_TEMP],PATIENT_SUPP"
                        + " WHERE [PATIENT_TEMP].PATIENT_ID = PATIENT_SUPP.PATIENT_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PASTEMP_" + m_lUserId + " ON [PATSUPP_TEMP](PATIENT_ID)"));
                }

                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[PHONES_TEMP]", "PATIENT_ID");

                    //Dump results into temp table
                    if (bAddressesIncluded || bOrgHierarchyPhoneIncluded)
                    {
                        if (bAddressesIncluded)
                        {
                            JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[PHONES_TEMP]", "ADDRESS_ID", true);
                            sSQL = "SELECT PATIENT.PATIENT_ID,[PATIENT_TEMP].ADDRESS_ID";
                        }
                        if (bOrgHierarchyPhoneIncluded)
                        {
                            JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[PHONES_TEMP]", "PHONE_ID", true);
                            sSQL = "SELECT PATIENT.PATIENT_ID,[PATIENT_TEMP].PHONE_ID";
                        }
                    }
                    else
                    {
                        sSQL = "SELECT PATIENT.PATIENT_ID";
                    }


                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    sSQL = sSQL + " [INTO PHONES_TEMP]"
                        + " FROM PATIENT,ADDRESS_X_PHONEINFO"
                        + " WHERE PATIENT.PATIENT_EID = ADDRESS_X_PHONEINFO.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PHONETEMP_" + m_lUserId + " ON [PHONES_TEMP](PATIENT_ID)"));
                }

                //AA//RMA-8753 nshah28(Added by ashish)
                //if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES"))
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstDisplayTables, "ADDRESS"))
                {


                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES");
                    iIdxAddr = Utilities.IndexInArray(p_arrlstDisplayTables, "ADDRESS");//RMA-8753 nshah28(Added by ashish)
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[ADDRESSES_TEMP]", "PATIENT_ID");

                    //Dump results into temp table
                    if (bPhoneNumbersIncluded && bAddressFlag)
                    {
                        JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[ADDRESSES_TEMP]", "PHONE_ID", true);
                        sSQL = "SELECT PATIENT.PATIENT_ID,[PATIENT_TEMP].PHONE_ID";
                    }
                    else
                    {
                        sSQL = "SELECT PATIENT.PATIENT_ID";
                    }

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    //RMA-8753 nshah28(Added by ashish)
                    if (iIdxAddr > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxAddr].ToString());
                    sSQL = sSQL + " [INTO ADDRESSES_TEMP]"
                        + " FROM PATIENT,ENTITY_X_ADDRESSES,ADDRESS"
                        + " WHERE ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID AND PATIENT.PATIENT_EID = ENTITY_X_ADDRESSES.ENTITY_ID";
                    //RMA-8753 nshah28(Added by ashish) END
                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ADDTEMP_" + m_lUserId + " ON [ADDRESSES_TEMP](PATIENT_ID)"));

                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ADDRESS_X_PHONEINFO"))
                {


                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ORG_ADDRESS_X_PHONEINFO");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[ORG_ADD_TEMP]", "PATIENT_ID");

                    //Dump results into temp table
                    if (bPhoneNumbersIncluded && bOrgFlag)
                    {
                        JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[ORG_ADD_TEMP]", "PHONE_ID", true);
                        sSQL = "SELECT PATIENT.PATIENT_ID,[PATIENT_TEMP].PHONE_ID";
                    }
                    else
                    {
                        sSQL = "SELECT PATIENT.PATIENT_ID";
                    }

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    sSQL = sSQL + " [INTO ORG_ADD_TEMP]"
                        + " FROM [PATIENT_TEMP],PATIENT,ADDRESS_X_PHONEINFO ORG_ADDRESS_X_PHONEINFO WHERE [PATIENT_TEMP].PATIENT_ID = PATIENT.PATIENT_ID"
                        + " AND PATIENT.FACILITY_DEPT_EID = ORG_ADDRESS_X_PHONEINFO.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ORGADDTEMP_" + m_lUserId + " ON [ORG_ADD_TEMP](PATIENT_ID)"));

                }
                //Added Rakhi for R7:Add Emp Data Elements
                //MULTI-CODE Type Supplementals
                //Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[PATIENT_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "PATIENT_ID");

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [PATIENT_TEMP].PATIENT_ID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]"
                            + " FROM [PATIENT_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias
                            + " WHERE [PATIENT_TEMP].PATIENT_ID = " + sMultiCodeTableAlias + ".RECORD_ID"
                            + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](PATIENT_ID)"));
                    }
                }

                //Chain everything together
                string sHipaaIDs = string.Empty;
                //p_sPatientData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, "PATIENT_ID", ref sHipaaIDs);
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //avipinsrivas start : Worked for JIRA - 7767
                    string sHipaaColumn = "PATIENT_ID";
                    if (objSysSettingsGlobal.UseEntityRole)
                    {
                        p_sJustFieldNames = p_sJustFieldNames.Replace("[PATIENT_TEMP].PATIENT_ID", "[PATIENT_TEMP].PATIENT_EID");
                        sHipaaColumn = "PATIENT_EID";
                    }
                    //avipinsrivas end
                    //Chain everything together
                    p_sPatientData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, sHipaaColumn, ref sHipaaIDs, ref p_sRetSQL);
                }
                else
                {
                    p_sPatientData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }
                string sSql = "select * from patient where patient_id in (";
                if (sHipaaIDs.Length > 0)
                {
                    sSql += sHipaaIDs + ")";
                    objReaderPatient = p_objConn.ExecuteReader(sSql);
                    objHippa = new HipaaLog(m_sConnectionString, m_iClientId);
                    while (objReaderPatient.Read())
                    {
                        objHippa.LogHippaInfo(Conversion.ConvertObjToInt(objReaderPatient.GetValue("PATIENT_EID"), m_iClientId), "0", "0", "Search", "VW", m_sUserName);
                    }
                    objReaderPatient.Close();
                    objHippa = null;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoPatientSearch.GenericError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReaderPatient != null)
                {
                    objReaderPatient.Close();
                    objReaderPatient.Dispose();
                }
            }
        }
        #endregion

        #region Policy search
        /// <summary>
        /// This function generates the search data related to Policy module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_lPolicyRestrict">Restricts the number of policy to be displayed</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sPolicyData">Policy data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        public void DoPolicySearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, long p_lPolicyRestrict, ref string p_sRetSQL, ref string p_sPolicyData, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            string sMultiCodeTableAlias = "";
            int iIdx = 0;
            int iIdxInsured = 0;
            // npadhy Start MITS 18653 Added the case for Reinsurer and modified the logic for Insurer
            int iIdxInsurer = 0;
            int iIdxReinsurer = 0;
            // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
            bool bReinsurerIncluded = false;
            // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
            // npadhy End MITS 18653 Added the case for Reinsurer and modified the logic for Insurer
            long lFieldId = 0;


            try
            {
                //Delete temp table, if any
                //if (m_sDBType != Constants.DB_ORACLE)
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupPolicySearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpPolicySearch");
                }

                //Build criteria query
                sFrom = "POLICY";
                //zmohammad MITs 34993 Start - Adding code for Query Designer having field tables other than just Policy table. Chaniging logic to use left outer join so that result is obtained even if any of the tables is empty, also remove duplicates.
                sSelect = "SELECT DISTINCT POLICY.POLICY_ID ";

                if (p_lPolicyRestrict > 0)
                    sWhere = " POLICY.PRIMARY_POLICY_FLG = 0 AND POLICY.POLICY_ID <> 0 AND POLICY.POLICY_ID <> " + p_lPolicyRestrict;

                // npadhy Start MITS 18653 Added the case for Reinsurer and modified the logic for Insurer
                //insurer
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INSURER") || Utilities.ExistsInArray(p_arrlstQueryTables, "INSURER_ENTITY"))
                {
                    sFrom = sFrom + " LEFT OUTER JOIN POLICY_X_INSURER ON POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID LEFT OUTER JOIN ENTITY INSURER_ENTITY ON POLICY_X_INSURER.INSURER_CODE = INSURER_ENTITY.ENTITY_ID ";
                    //   if (sWhere != "")
                    //       sWhere = sWhere + " AND ";
                    //   sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID AND POLICY_X_INSURER.INSURER_CODE = INSURER_ENTITY.ENTITY_ID";
                }

                // reinsurer
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INS_REINS") || Utilities.ExistsInArray(p_arrlstQueryTables, "REINSURER_ENTITY"))
                {

                    if (!sFrom.Contains("POLICY_X_INSURER"))
                        sFrom += " LEFT OUTER JOIN POLICY_X_INSURER ON POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID "; //Raman 12/30/2009 : reinsurer search not working
                    sFrom = sFrom + " LEFT OUTER JOIN POLICY_X_INS_REINS ON POLICY_X_INSURER.IN_ROW_ID = POLICY_X_INS_REINS.POL_X_INS_ROW_ID LEFT OUTER JOIN ENTITY REINSURER_ENTITY ON POLICY_X_INS_REINS.REINSURER_EID = REINSURER_ENTITY.ENTITY_ID ";
                    //  if (sWhere != "")
                    //      sWhere = sWhere + " AND ";
                    //  sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID AND POLICY_X_INSURER.IN_ROW_ID = POLICY_X_INS_REINS.POL_X_INS_ROW_ID AND POLICY_X_INS_REINS.REINSURER_EID = REINSURER_ENTITY.ENTITY_ID";

                    // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                    bReinsurerIncluded = true;

                }

                if (!bReinsurerIncluded && (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_INS_REINS") || Utilities.ExistsInArray(p_arrlstDisplayTables, "REINSURER_ENTITY")))
                {
                    if (!sFrom.Contains("POLICY_X_INSURER"))
                    {
                        sFrom += " LEFT OUTER JOIN POLICY_X_INSURER ON POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID ";
                        // if (sWhere != "")
                        //     sWhere = sWhere + " AND ";
                        // sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID ";
                    }
                    bReinsurerIncluded = true;
                }
                // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                // npadhy End MITS 18653 Added the case for Reinsurer and modified the logic for Insurer
                //Gagan Safeway Retrofit Policy Jursidiction : START
                //states
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_STATE"))
                {
                    sFrom = sFrom + " LEFT OUTER JOIN POLICY_X_STATE ON POLICY.POLICY_ID = POLICY_X_STATE.POLICY_ID ";
                    //  if (sWhere != "")
                    //     sWhere = sWhere + " AND ";
                    // sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_STATE.POLICY_ID";
                }
                //Gagan Safeway Retrofit Policy Jursidiction : END		

                //coverages
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_CVG_TYPE"))
                {
                    sFrom = sFrom + " LEFT OUTER JOIN POLICY_X_CVG_TYPE ON POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID ";
                    //   if (sWhere != "")
                    //      sWhere = sWhere + " AND ";
                    //  sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID";
                }

                //insured layers
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INSURED") || Utilities.ExistsInArray(p_arrlstQueryTables, "INSURED_ENTITY")) //Added support for full insured entity breakout
                {
                    sFrom = sFrom + " LEFT OUTER JOIN POLICY_X_INSURED ON POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID LEFT OUTER JOIN ENTITY INSURED_ENTITY ON POLICY_X_INSURED.INSURED_EID = INSURED_ENTITY.ENTITY_ID "; //aaggarwal29 : MITS 37573, JIRA 6595
                    // if (sWhere != "")
                    //     sWhere = sWhere + " AND ";
                    // sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID AND POLICY_X_INSURED.INSURED_EID = INSURED_ENTITY.ENTITY_ID";
                }

                //MCO support
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_MCO") || Utilities.ExistsInArray(p_arrlstQueryTables, "MCO_ENTITY"))
                {
                    sFrom = sFrom + " LEFT OUTER JOIN POLICY_X_MCO ON POLICY.POLICY_ID = POLICY_X_MCO.POLICY_ID ";
                    //  if (sWhere != "")
                    //      sWhere = sWhere + " AND ";
                    //  sWhere = sWhere + "POLICY.POLICY_ID = POLICY_X_MCO.POLICY_ID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "MCO_ENTITY"))
                {
                    sFrom = sFrom + " LEFT OUTER JOIN ENTITY MCO_ENTITY ON POLICY_X_MCO.MCO_EID = MCO_ENTITY.ENTITY_ID ";//aaggarwal29 : MITS 37573, JIRA 6595
                    //   if (sWhere != "")
                    //       sWhere = sWhere + " AND ";
                    //   sWhere = sWhere + "POLICY_X_MCO.MCO_EID = MCO_ENTITY.ENTITY_ID";
                }

                //supp. support
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_SUPP"))
                {
                    sFrom = sFrom + " LEFT OUTER JOIN POLICY_SUPP ON POLICY.POLICY_ID = POLICY_SUPP.POLICY_ID ";
                    //  if (sWhere != "")
                    //      sWhere = sWhere + " AND ";
                    //  sWhere = sWhere + "POLICY.POLICY_ID = POLICY_SUPP.POLICY_ID";
                }
                //zmohammad MITs 34993 End - Adding code for Query Designer having field tables other than just Policy table. Chaniging logic to use left outer join so that result is obtained even if any of the tables is empty, also remove duplicates.
                //Supplimental Multi-Code Field
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "POLICY.POLICY_ID");

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }

                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;

                // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                if (bReinsurerIncluded)
                {
                    sSelect += ",POLICY_X_INSURER.IN_ROW_ID";
                }
                // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                //run criteria query into temp table
                sSQL = sSelect + " [INTO POLICY_TEMP] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;

                ExecSearchSQL(sSQL, ref p_objConn);

                //index for quick processing in display only fields
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX POLTEMP_" + m_lUserId + " ON [POLICY_TEMP](POLICY_ID)"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[POLICY_TEMP]";
                sWhere = "";

                // npadhy Start MITS 18653 Added the case for Reinsurer and modified the logic for Insurer
                //insurer
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "INSURER_ENTITY") || Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_INSURER"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "INSURER_ENTITY");
                    iIdxInsurer = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_INSURER");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[INS_ENT_TEMP]", "POLICY_ID");

                    // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                    if (bReinsurerIncluded)
                        JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[INS_ENT_TEMP]", "IN_ROW_ID", true);
                    // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                    //Dump results into temp table
                    sSQL = "SELECT POLICY.POLICY_ID, POLICY_X_INSURER.IN_ROW_ID";
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    if (iIdxInsurer > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxInsurer].ToString());
                    sSQL = sSQL + " [INTO INS_ENT_TEMP]"
                        + " FROM [POLICY_TEMP],POLICY,ENTITY INSURER_ENTITY, POLICY_X_INSURER"
                        + " WHERE [POLICY_TEMP].POLICY_ID = POLICY.POLICY_ID"
                        + " AND POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID"
                        + " AND POLICY_X_INSURER.INSURER_CODE = INSURER_ENTITY.ENTITY_ID";

                    // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                    if (bReinsurerIncluded)
                        sSQL = sSQL + " AND [POLICY_TEMP].IN_ROW_ID = POLICY_X_INSURER.IN_ROW_ID";
                    // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INSUTEMP_" + m_lUserId + " ON [INS_ENT_TEMP](POLICY_ID)"));
                }

                // npadhy MITS Handling the case for Reinsurer
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "REINSURER_ENTITY") || Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_INS_REINS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "REINSURER_ENTITY");
                    iIdxReinsurer = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_INS_REINS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[REINS_ENT_TEMP]", "POLICY_ID");

                    // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                    // We need to add the join for Policy_X_Insurer.Inrowid and POLICY_X_INS_REINS.POL_X_INS_ROW_ID
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[REINS_ENT_TEMP]", "IN_ROW_ID", "POL_X_INS_ROW_ID", true);
                    // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                    //Dump results into temp table
                    sSQL = "SELECT POLICY.POLICY_ID,POLICY_X_INS_REINS.POL_X_INS_ROW_ID";
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    if (iIdxReinsurer > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxReinsurer].ToString());
                    sSQL = sSQL + " [INTO REINS_ENT_TEMP]"
                        + " FROM [POLICY_TEMP],POLICY,ENTITY REINSURER_ENTITY, POLICY_X_INS_REINS, POLICY_X_INSURER"
                        + " WHERE [POLICY_TEMP].POLICY_ID = POLICY.POLICY_ID"
                        + " AND POLICY.POLICY_ID = POLICY_X_INSURER.POLICY_ID"
                        + " AND POLICY_X_INSURER.IN_ROW_ID = POLICY_X_INS_REINS.POL_X_INS_ROW_ID"
                        + " AND POLICY_X_INS_REINS.REINSURER_EID = REINSURER_ENTITY.ENTITY_ID";

                    // npadhy Start MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search
                    if (bReinsurerIncluded)
                        sSQL = sSQL + " AND [POLICY_TEMP].IN_ROW_ID = POLICY_X_INSURER.IN_ROW_ID";
                    // npadhy End MITS 20622 Claim search not working properly if policy info,insurer,reinsurer related fields are part of search

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX REINSUTEMP_" + m_lUserId + " ON [REINS_ENT_TEMP](POLICY_ID)"));
                }
                // npadhy End MITS 18653 Added the case for Reinsurer and modified the logic for Insurer

                //coverages
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_CVG_TYPE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_CVG_TYPE");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[CVG_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_X_CVG_TYPE.POLICY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CVG_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_TEMP],POLICY_X_CVG_TYPE";
                    sSQL = sSQL + " WHERE [POLICY_TEMP].POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CVGTEMP_" + m_lUserId + " ON [CVG_TEMP](POLICY_ID)"));
                }

                //insured layers
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_INSURED") || Utilities.ExistsInArray(p_arrlstDisplayTables, "INSURED_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_INSURED");
                    iIdxInsured = Utilities.IndexInArray(p_arrlstDisplayTables, "INSURED_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[INSURED_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_X_INSURED.POLICY_ID";
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    if (iIdxInsured > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxInsured].ToString());
                    sSQL = sSQL + " [INTO INSURED_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_TEMP],POLICY_X_INSURED,ENTITY INSURED_ENTITY";
                    sSQL = sSQL + " WHERE [POLICY_TEMP].POLICY_ID = POLICY_X_INSURED.POLICY_ID AND POLICY_X_INSURED.INSURED_EID = INSURED_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INSDTEMP_" + m_lUserId + " ON [INSURED_TEMP](POLICY_ID)"));
                }

                //MCO support
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_MCO") || Utilities.ExistsInArray(p_arrlstDisplayTables, "MCO_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_MCO");
                    iIdxInsured = Utilities.IndexInArray(p_arrlstDisplayTables, "MCO_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[MCO_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_X_MCO.POLICY_ID";
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    if (iIdxInsured > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxInsured].ToString());

                    sSQL = sSQL + " [INTO MCO_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_TEMP],POLICY_X_MCO,ENTITY MCO_ENTITY";
                    sSQL = sSQL + " WHERE [POLICY_TEMP].POLICY_ID = POLICY_X_MCO.POLICY_ID";
                    sSQL = sSQL + " AND POLICY_X_MCO.MCO_EID = MCO_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX MCOTEMP_" + m_lUserId + " ON [MCO_TEMP](POLICY_ID)"));
                }

                //Gagan Safeway Retrofit Policy Jursidiction : START
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_STATE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_STATE");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[STATE_TEMP]", "POLICY_ID");

                    //New Query
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " [POLICY_TEMP].POLICY_ID," + p_arrlstDisplayFields[iIdx] + " [INTO STATE_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_TEMP],POLICY_X_STATE WHERE [POLICY_TEMP].POLICY_ID=POLICY_X_STATE.POLICY_ID";
                    //New Q end
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX STATETEMP_" + m_lUserId + " ON [STATE_TEMP](POLICY_ID)"));

                }
                //Gagan Safeway Retrofit Policy Jursidiction : END

                //supplemental support
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[POLSUPP_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_SUPP.POLICY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO POLSUPP_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_TEMP],POLICY_SUPP";
                    sSQL = sSQL + " WHERE [POLICY_TEMP].POLICY_ID = POLICY_SUPP.POLICY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX POLSPTEMP_" + m_lUserId + " ON [POLSUPP_TEMP](POLICY_ID)"));
                }

                //MULTI-CODE Type Supplementals
                //Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[POLICY_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "POLICY_ID");

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [POLICY_TEMP].POLICY_ID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]";
                        sSQL = sSQL + " FROM [POLICY_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                        sSQL = sSQL + " WHERE [POLICY_TEMP].POLICY_ID = " + sMultiCodeTableAlias + ".RECORD_ID";
                        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](POLICY_ID)"));
                    }
                }

                //Chain everything together
                //p_sPolicyData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sPolicyData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sPolicyData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }

            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoPolicySearch.GenericError", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Physician search
        /// <summary>
        /// This function generates the search data related to Physician module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sPhysicianData">Physician data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        public void DoPhysicianSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sPhysicianData, ref DbConnection p_objConn, string p_sOrgLevel, int p_iPageSize, int p_iPageNumber)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            string sMultiCodeTableAlias = "";
            int iIdx = 0;
            int iIdxAddr = 0;//AA//RMA-8753 nshah28(Added by ashish)
            long lFieldId = 0;
            SysSettings objSettings = null; //mbahl3 mits 30224
            string sPresentdate = string.Empty; //mbahl3 mits 30224
            try
            {
                //Delete temp table, if any
                //if (m_sDBType != Constants.DB_ORACLE)
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupPhysicianSearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpPhysicianSearch");
                }

                //Build criteria query
                sFrom = "PHYSICIAN,ENTITY | PHYS_ENTITY";
                sWhere = "PHYSICIAN.PHYS_EID = PHYS_ENTITY.ENTITY_ID";
                sSelect = "SELECT PHYSICIAN.PHYS_EID ";

                //plug in other tables which have criteria
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PHYS_SUPP"))
                {
                    sFrom = sFrom + ", PHYS_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PHYSICIAN.PHYS_EID = PHYS_SUPP.PHYS_EID";
                }

                //mbahl3 mits 30224

                objSettings = new SysSettings(m_sConnectionString, m_iClientId);
                if (objSettings.StatusActive)
                {
                    //sPresentdate = DateTime.Now.ToShortDateString();
                    sPresentdate = Conversion.ToDbDate(DateTime.Now);

                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + " ( PHYS_ENTITY.EFFECTIVE_DATE <='" + sPresentdate + "'  OR PHYS_ENTITY.EFFECTIVE_DATE IS NULL OR PHYS_ENTITY.EFFECTIVE_DATE ='')";
                    sWhere = sWhere + " AND ( PHYS_ENTITY.EXPIRATION_DATE >='" + sPresentdate + "'  OR PHYS_ENTITY.EXPIRATION_DATE IS NULL OR PHYS_ENTITY.EXPIRATION_DATE ='')";

                //mbahl3 mits 30224
				}
                else
                {
                    p_sJustFieldNames = p_sJustFieldNames + ",[PHYS_TEMP].EFFECTIVE_DATE EFFDATE,[PHYS_TEMP].EXPIRATION_DATE EXPDATE";
                    IsEntitySearch = true;
					//mbahl3 mits 30224
                }
                //mbahl3 mits 30224

                //MGaba2:MITS 13205:09/04/2008:It should not be added if org hierarchy fields are supplemental
                //Commented the following code as following criteria has been added in "DoSearch" itself:Start
                //// Abhishek  changes related to MITS 11341 start
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_HIERARCHY"))
                //{
                //    sFrom = sFrom + ",ORG_HIERARCHY";
                //    if (sWhere != "")
                //        sWhere = sWhere + " AND ";
                //    sWhere = sWhere + "PHYSICIAN.DEPT_ASSIGNED_EID = ORG_HIERARCHY.DEPARTMENT_EID";
                //}
                ////Abhishek  changes related to MITS 11341 End
                //MGaba2:MITS 20175:Commenting this as this join is no more needed
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_HIE"))
                //{
                //    sFrom = sFrom + ",ORG_HIERARCHY ORG_HIE";
                //}
                //MGaba2:MITS 13205:09/04/2008:End

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PHYS_SUB_SPECIALTY"))
                {
                    sFrom = sFrom + ", PHYS_SUB_SPECIALTY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PHYSICIAN.PHYS_EID = PHYS_SUB_SPECIALTY.PHYS_EID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PHYS_PRIVS"))
                {
                    sFrom = sFrom + ", PHYS_PRIVS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PHYSICIAN.PHYS_EID = PHYS_PRIVS.PHYS_EID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PHYS_CERTS"))
                {
                    sFrom = sFrom + ", PHYS_CERTS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PHYSICIAN.PHYS_EID = PHYS_CERTS.PHYS_EID";
                }

                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_ENTITY"))
                {
                    sFrom = sFrom + ",ENTITY ORG_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    if (p_sOrgLevel != "")
                    {
                        sFrom = sFrom + ", ORG_HIERARCHY";
                        sWhere = sWhere + "ORG_HIERARCHY." + p_sOrgLevel + " = ORG_ENTITY.ENTITY_ID"
                            + " AND ORG_HIERARCHY.DEPARTMENT_EID = PHYSICIAN.DEPT_ASSIGNED_EID";
                    }
                    else
                        sWhere = sWhere + "PHYSICIAN.DEPT_ASSIGNED_EID = ORG_ENTITY.ENTITY_ID";
                }

                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS_X_PHONEINFO"))
                {
                    sFrom = sFrom + ", ADDRESS_X_PHONEINFO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PHYSICIAN.PHYS_EID  = ADDRESS_X_PHONEINFO.ENTITY_ID";
                    bPhoneNumbersIncluded = true;
                }

                //AA//RMA-8753 nshah28(Added by ashish)
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_ADDRESSES"))
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS"))
                {

                    sFrom = sFrom + ", ENTITY_X_ADDRESSES, ADDRESS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID AND PHYSICIAN.PHYS_EID  = ENTITY_X_ADDRESSES.ENTITY_ID";
                    bAddressesIncluded = true;


                }
                //RMA-8753 nshah28(Added by ashish)END
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_ADDRESS_X_PHONEINFO"))
                {
                    sFrom = sFrom + ",ADDRESS_X_PHONEINFO ORG_ADDRESS_X_PHONEINFO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "PHYSICIAN.PHYS_EID  = ORG_ADDRESS_X_PHONEINFO.ENTITY_ID";
                    bOrgHierarchyPhoneIncluded = true;
                }
                //Added Rakhi for R7:Add Emp Data Elements
                //Supplimental Multi-Code Field
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "PHYSICIAN.PHYS_EID");
                //mbahl3 Mits 30224
                if (!objSettings.StatusActive)
                {
                    //RMA-8753 nshah28(Added by ashish)
                    if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS"))
                    {
                        sSelect = sSelect + ",ENTITY_X_ADDRESSES.EFFECTIVE_DATE ,ENTITY_X_ADDRESSES.EXPIRATION_DATE ";
                    }
                    else
                    {
                        sSelect = sSelect + ",EFFECTIVE_DATE ,EXPIRATION_DATE ";
                    }
                    //RMA-8753 nshah28(Added by ashish) END
                }
                //mbahl3 Mits 30224
                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }

                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;

                //Added Rakhi for R7:Add Emp Data Elements
                CheckMultiplePhoneAddresses(bPhoneNumbersIncluded, bAddressesIncluded, bOrgHierarchyPhoneIncluded, p_arrlstDisplayTables, ref sSelect, ref bAddressFlag, ref bOrgFlag);
                //Added Rakhi for R7:Add Emp Data Elements
                //run criteria query into temp table
                sSQL = sSelect + " [INTO PHYS_TEMP] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;

                ExecSearchSQL(sSQL, ref p_objConn);

                //index EVENT_ID for quick processing in display only fields
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PHYSTEMP_" + m_lUserId + " ON [PHYS_TEMP](PHYS_EID)"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[PHYS_TEMP]";
                sWhere = "";

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[PHSUPP_TEMP]", "PHYS_EID");

                    //Dump results into temp table
                    sSQL = "SELECT PHYS_SUPP.PHYS_EID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PHSUPP_TEMP]";
                    sSQL = sSQL + " FROM [PHYS_TEMP],PHYS_SUPP";
                    sSQL = sSQL + " WHERE [PHYS_TEMP].PHYS_EID = PHYS_SUPP.PHYS_EID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PHSTEMP_" + m_lUserId + " ON [PHSUPP_TEMP](PHYS_EID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_SUB_SPECIALTY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_SUB_SPECIALTY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[PSSPEC_TEMP]", "PHYS_EID");

                    //Dump results into temp table
                    sSQL = "SELECT PHYS_SUB_SPECIALTY.PHYS_EID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PSSPEC_TEMP]"
                        + " FROM [PHYS_TEMP],PHYS_SUB_SPECIALTY"
                        + " WHERE [PHYS_TEMP].PHYS_EID = PHYS_SUB_SPECIALTY.PHYS_EID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SPCTEMP_" + m_lUserId + " ON [PSSPEC_TEMP](PHYS_EID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_PRIVS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_PRIVS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[PRIV_TEMP]", "PHYS_EID");

                    //Dump results into temp table
                    sSQL = "SELECT PHYS_PRIVS.PHYS_EID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PRIV_TEMP]"
                        + " FROM [PHYS_TEMP],PHYS_PRIVS"
                        + " WHERE [PHYS_TEMP].PHYS_EID = PHYS_PRIVS.PHYS_EID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PRVTEMP_" + m_lUserId + " ON [PRIV_TEMP](PHYS_EID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PHYS_CERTS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PHYS_CERTS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[CERT_TEMP]", "PHYS_EID");

                    //Dump results into temp table
                    sSQL = "SELECT PHYS_CERTS.PHYS_EID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CERT_TEMP]";
                    sSQL = sSQL + " FROM [PHYS_TEMP],PHYS_CERTS";
                    sSQL = sSQL + " WHERE [PHYS_TEMP].PHYS_EID = PHYS_CERTS.PHYS_EID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CRTTEMP_" + m_lUserId + " ON [CERT_TEMP](PHYS_EID)"));
                }

                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ORG_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[ORG_TEMP]", "PHYS_EID");

                    //Dump results into temp table
                    sSQL = "SELECT PHYSICIAN.PHYS_EID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO ORG_TEMP]";
                    sSQL = sSQL + " FROM [PHYS_TEMP],PHYSICIAN,ENTITY ORG_ENTITY WHERE [PHYS_TEMP].PHYS_EID = PHYSICIAN.PHYS_EID";
                    sSQL = sSQL + " AND PHYSICIAN.DEPT_ASSIGNED_EID = ORG_ENTITY.ENTITY_ID ";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ORGTEMP_" + m_lUserId + " ON [ORG_TEMP](PHYS_EID)"));
                }
                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[PHONES_TEMP]", "PHYS_EID");

                    //Dump results into temp table
                    if (bAddressesIncluded || bOrgHierarchyPhoneIncluded)
                    {
                        if (bAddressesIncluded)
                        {
                            JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[PHONES_TEMP]", "ADDRESS_ID", true);
                            sSQL = "SELECT [PHYS_TEMP].PHYS_EID,[PHYS_TEMP].ADDRESS_ID";
                        }
                        if (bOrgHierarchyPhoneIncluded)
                        {
                            JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[PHONES_TEMP]", "PHONE_ID", true);
                            sSQL = "SELECT [PHYS_TEMP].PHYS_EID,[PHYS_TEMP].PHONE_ID";
                        }
                    }
                    else
                    {
                        sSQL = "SELECT [PHYS_TEMP].PHYS_EID";
                    }


                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    sSQL = sSQL + " [INTO PHONES_TEMP]";
                    sSQL = sSQL + " FROM [PHYS_TEMP],ADDRESS_X_PHONEINFO";
                    sSQL = sSQL + " WHERE [PHYS_TEMP].PHYS_EID = ADDRESS_X_PHONEINFO.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PHONETEMP_" + m_lUserId + " ON [PHONES_TEMP](PHYS_EID)"));
                }

                //AA//RMA-8753 nshah28(Added by ashish)
                //if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES"))
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstDisplayTables, "ADDRESS"))
                {

                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES");
                    iIdxAddr = Utilities.IndexInArray(p_arrlstDisplayTables, "ADDRESS");//RMA-8753 nshah28(Added by ashish)
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[ADDRESSES_TEMP]", "PHYS_EID");

                    //Dump results into temp table
                    if (bPhoneNumbersIncluded && bAddressFlag)
                    {
                        JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[ADDRESSES_TEMP]", "PHONE_ID", true);
                        sSQL = "SELECT [PHYS_TEMP].PHYS_EID,[PHYS_TEMP].PHONE_ID";
                    }
                    else
                    {
                        sSQL = "SELECT [PHYS_TEMP].PHYS_EID";
                    }

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    //RMA-8753 nshah28(Added by ashish)
                    if (iIdxAddr > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxAddr].ToString());
                    sSQL = sSQL + " [INTO ADDRESSES_TEMP]";
                    sSQL = sSQL + " FROM [PHYS_TEMP],ENTITY_X_ADDRESSES,ADDRESS";
                    sSQL = sSQL + " WHERE ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID AND [PHYS_TEMP].PHYS_EID = ENTITY_X_ADDRESSES.ENTITY_ID";
                    //RMA-8753 nshah28(Added by ashish)END

                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ADDTEMP_" + m_lUserId + " ON [ADDRESSES_TEMP](PHYS_EID)"));

                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ADDRESS_X_PHONEINFO"))
                {

                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ORG_ADDRESS_X_PHONEINFO");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[ORG_ADD_TEMP]", "PHYS_EID");

                    //Dump results into temp table
                    if (bPhoneNumbersIncluded && bOrgFlag)
                    {
                        JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[ORG_ADD_TEMP]", "PHONE_ID", true);
                        sSQL = "SELECT [PHYS_TEMP].PHYS_EID,[PHYS_TEMP].PHONE_ID";
                    }
                    else
                    {
                        sSQL = "SELECT [PHYS_TEMP].PHYS_EID";
                    }

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    sSQL = sSQL + " [INTO ORG_ADD_TEMP]";
                    sSQL = sSQL + " FROM [PHYS_TEMP],PHYSICIAN,ADDRESS_X_PHONEINFO ORG_ADDRESS_X_PHONEINFO WHERE [PHYS_TEMP].PHYS_EID = PHYSICIAN.PHYS_EID";
                    sSQL = sSQL + " AND PHYSICIAN.DEPT_ASSIGNED_EID = ORG_ADDRESS_X_PHONEINFO.ENTITY_ID ";

                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ORGADDTEMP_" + m_lUserId + " ON [ORG_ADD_TEMP](PHYS_EID)"));

                }
                //Added Rakhi for R7:Add Emp Data Elements

                //MULTI-CODE Type Supplementals
                //Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[PHYS_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "PHYS_EID");

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [PHYS_TEMP].PHYS_EID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]";
                        sSQL = sSQL + " FROM [PHYS_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                        sSQL = sSQL + " WHERE [PHYS_TEMP].PHYS_EID = " + sMultiCodeTableAlias + ".RECORD_ID";
                        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](PHYS_EID)"));
                    }
                }

                //Chain everything together
                //p_sPhysicianData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sPhysicianData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sPhysicianData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }

            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoPhysicianSearch.GenericError", m_iClientId), p_objException);
            }
			//mbahl3 mits 30224
            finally
            {
                objSettings = null;
                    
            }
			//mbahl3 mits 30224
        }
        #endregion

        #region Medical Staff search
        /// <summary>
        /// This function generates the search data related to Medical Staff module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sMedStaffData">Medical staff data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        public void DoMedStaffSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sMedStaffData, ref DbConnection p_objConn, string p_sOrgLevel, int p_iPageSize, int p_iPageNumber)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            string sMultiCodeTableAlias = "";
            int iIdx = 0;
            int iIdxAddr = 0;//AA//RMA-8753 nshah28(Added by ashish)
            long lFieldId = 0;
            SysSettings objSettings = null; //mbahl3 mits 30224 
            string sPresentdate = string.Empty; //mbahl3 mits 30224
            try
            {
                //Delete temp table, if any
                //if (m_sDBType != Constants.DB_ORACLE)
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupMedStaffSearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpMedStaffSearch");
                }

                //Build criteria query
                sFrom = "MED_STAFF,ENTITY | MED_ENTITY";
                sWhere = "MED_STAFF.STAFF_EID = MED_ENTITY.ENTITY_ID";
                sSelect = "SELECT MED_STAFF.STAFF_EID ";

                //plug in other tables which have criteria
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "MED_STAFF_SUPP"))
                {
                    sFrom = sFrom + ", MED_STAFF_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "MED_STAFF.STAFF_EID = MED_STAFF_SUPP.STAFF_EID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "STAFF_PRIVS"))
                {
                    sFrom = sFrom + ", STAFF_PRIVS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "MED_STAFF.STAFF_EID = STAFF_PRIVS.STAFF_EID";
                }

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "STAFF_CERTS"))
                {
                    sFrom = sFrom + ", STAFF_CERTS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "MED_STAFF.STAFF_EID = STAFF_CERTS.STAFF_EID";
                }
               //mbahl3 mits 30224

                objSettings = new SysSettings(m_sConnectionString,m_iClientId);
                if (objSettings.StatusActive)
                {
                   // sPresentdate = DateTime.Now.ToShortDateString();
                    sPresentdate = Conversion.ToDbDate(DateTime.Now);

                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + " ( MED_ENTITY.EFFECTIVE_DATE <='" + sPresentdate + "'  OR MED_ENTITY.EFFECTIVE_DATE IS NULL OR MED_ENTITY.EFFECTIVE_DATE ='')";
                    sWhere = sWhere + " AND ( MED_ENTITY.EXPIRATION_DATE >='" + sPresentdate + "'  OR MED_ENTITY.EXPIRATION_DATE IS NULL OR MED_ENTITY.EXPIRATION_DATE ='')";

                }
                else
                {
                    p_sJustFieldNames = p_sJustFieldNames + ",[MED_TEMP].EFFECTIVE_DATE EFFDATE,[MED_TEMP].EXPIRATION_DATE EXPDATE";
                    IsEntitySearch = true;
                }
               //mbahl3 mits 30224

                //MGaba2:MITS 13205:09/04/2008:It should not be added if org hierarchy fields are supplemental
                //Commented the following code as following criteria has been added in "DoSearch" itself:Start
                //// Abhishek  changes related to MITS 11341 start
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_HIERARCHY"))
                //{
                //    sFrom = sFrom + ",ORG_HIERARCHY, EVENT";
                //    if (sWhere != "")
                //        sWhere = sWhere + " AND ";
                //    sWhere = sWhere + "MED_STAFF.DEPT_ASSIGNED_EID = ORG_HIERARCHY.DEPARTMENT_EID";
                //}
                ////Abhishek  changes related to MITS 11341 End
                //MGaba2:MITS 20175:Commenting this as this join is no more needed
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_HIE"))
                //{
                //    sFrom = sFrom + ",ORG_HIERARCHY ORG_HIE";
                //}
                //MGaba2:MITS 13205:09/04/2008:End
                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY ORG_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    if (p_sOrgLevel != "")
                    {
                        sFrom = sFrom + ", ORG_HIERARCHY";
                        sWhere = sWhere + "ORG_HIERARCHY." + p_sOrgLevel + " = ORG_ENTITY.ENTITY_ID";
                        sWhere = sWhere + " AND ORG_HIERARCHY.DEPARTMENT_EID = MED_STAFF.DEPT_ASSIGNED_EID";
                    }
                    else
                        sWhere = sWhere + "MED_STAFF.DEPT_ASSIGNED_EID = ORG_ENTITY.ENTITY_ID";
                }

                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS_X_PHONEINFO"))
                {
                    sFrom = sFrom + ", ADDRESS_X_PHONEINFO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "MED_STAFF.STAFF_EID  = ADDRESS_X_PHONEINFO.ENTITY_ID";
                    bPhoneNumbersIncluded = true;
                }

                //AA//RMA-8753 nshah28(Added by ashish)
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_ADDRESSES"))
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS"))
                {

                    sFrom = sFrom + ", ENTITY_X_ADDRESSES, ADDRESS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID AND MED_STAFF.STAFF_EID  = ENTITY_X_ADDRESSES.ENTITY_ID";
                    bAddressesIncluded = true;

                }
                //RMA-8753 nshah28(Added by ashish)END
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_ADDRESS_X_PHONEINFO"))
                {
                    sFrom = sFrom + ",ADDRESS_X_PHONEINFO ORG_ADDRESS_X_PHONEINFO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "MED_STAFF.STAFF_EID  = ORG_ADDRESS_X_PHONEINFO.ENTITY_ID";
                    bOrgHierarchyPhoneIncluded = true;
                }
                //Added Rakhi for R7:Add Emp Data Elements
                //Now Supplimental Multi-Code Field
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "MED_STAFF.STAFF_EID");

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }
                //mbahl3 Mits 30224
                if (!objSettings.StatusActive)
                {
                    //RMA-8753 nshah28(Added by ashish)
                    if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS"))
                    {
                        sSelect = sSelect + ",ENTITY_X_ADDRESSES.EFFECTIVE_DATE ,ENTITY_X_ADDRESSES.EXPIRATION_DATE ";
                    }
                    else
                    {
                        sSelect = sSelect + ",EFFECTIVE_DATE ,EXPIRATION_DATE ";
                    }
                    //RMA-8753 nshah28(Added by ashish) END
                }
                //mbahl3 Mits 30224

                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;
                //Added Rakhi for R7:Add Emp Data Elements
                CheckMultiplePhoneAddresses(bPhoneNumbersIncluded, bAddressesIncluded, bOrgHierarchyPhoneIncluded, p_arrlstDisplayTables, ref sSelect, ref bAddressFlag, ref bOrgFlag);
                //Added Rakhi for R7:Add Emp Data Elements

                //run criteria query into temp table
                sSQL = sSelect + " [INTO MED_TEMP] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;

                ExecSearchSQL(sSQL, ref p_objConn);
                //index EVENT_ID for quick processing in display only fields
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX MEDTEMP_" + m_lUserId + " ON [MED_TEMP](STAFF_EID)"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[MED_TEMP]";
                sWhere = "";

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "MED_STAFF_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "MED_STAFF_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[MED_TEMP]", "[MSUPP_TEMP]", "STAFF_EID");

                    //Dump results into temp table
                    sSQL = "SELECT MED_STAFF_SUPP.STAFF_EID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO MSUPP_TEMP]";
                    sSQL = sSQL + " FROM [MED_TEMP],MED_STAFF_SUPP";
                    sSQL = sSQL + " WHERE [MED_TEMP].STAFF_EID = MED_STAFF_SUPP.STAFF_EID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX MSPTEMP_" + m_lUserId + " ON [MSUPP_TEMP](STAFF_EID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "STAFF_PRIVS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "STAFF_PRIVS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[MED_TEMP]", "[PRIV_TEMP]", "STAFF_EID");

                    //Dump results into temp table
                    sSQL = "SELECT STAFF_PRIVS.STAFF_EID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PRIV_TEMP]";
                    sSQL = sSQL + " FROM [MED_TEMP],STAFF_PRIVS";
                    sSQL = sSQL + " WHERE [MED_TEMP].STAFF_EID = STAFF_PRIVS.STAFF_EID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PRIVTMP_" + m_lUserId + " ON [PRIV_TEMP](STAFF_EID)"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "STAFF_CERTS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "STAFF_CERTS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[MED_TEMP]", "[CERT_TEMP]", "STAFF_EID");

                    //Dump results into temp table
                    sSQL = "SELECT STAFF_CERTS.STAFF_EID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CERT_TEMP]";
                    sSQL = sSQL + " FROM [MED_TEMP],STAFF_CERTS";
                    sSQL = sSQL + " WHERE [MED_TEMP].STAFF_EID = STAFF_CERTS.STAFF_EID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CRTTEMP_" + m_lUserId + " ON [CERT_TEMP](STAFF_EID)"));
                }

                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ORG_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[MED_TEMP]", "[ORG_TEMP]", "STAFF_EID");

                    //Dump results into temp table
                    sSQL = "SELECT MED_STAFF.STAFF_EID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO ORG_TEMP]";
                    sSQL = sSQL + " FROM [MED_TEMP],MED_STAFF,ENTITY ORG_ENTITY WHERE [MED_TEMP].STAFF_EID = MED_STAFF.STAFF_EID";
                    sSQL = sSQL + " AND MED_STAFF.DEPT_ASSIGNED_EID = ORG_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ORGTEMP_" + m_lUserId + " ON [ORG_TEMP](STAFF_EID)"));
                }
                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[MED_TEMP]", "[PHONES_TEMP]", "STAFF_EID");

                    //Dump results into temp table
                    if (bAddressesIncluded || bOrgHierarchyPhoneIncluded)
                    {
                        if (bAddressesIncluded)
                        {
                            JoinIt(ref sFrom, ref sWhere, "[MED_TEMP]", "[PHONES_TEMP]", "ADDRESS_ID", true);
                            sSQL = "SELECT [MED_TEMP].STAFF_EID,[MED_TEMP].ADDRESS_ID";
                        }
                        if (bOrgHierarchyPhoneIncluded)
                        {
                            JoinIt(ref sFrom, ref sWhere, "[MED_TEMP]", "[PHONES_TEMP]", "PHONE_ID", true);
                            sSQL = "SELECT [MED_TEMP].STAFF_EID,[MED_TEMP].PHONE_ID";
                        }
                    }
                    else
                    {
                        sSQL = "SELECT [MED_TEMP].STAFF_EID";
                    }

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    sSQL = sSQL + " [INTO PHONES_TEMP]";
                    sSQL = sSQL + " FROM [MED_TEMP],ADDRESS_X_PHONEINFO";
                    sSQL = sSQL + " WHERE [MED_TEMP].STAFF_EID = ADDRESS_X_PHONEINFO.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PHONETEMP_" + m_lUserId + " ON [PHONES_TEMP](STAFF_EID)"));
                }

                //AA//RMA-8753 nshah28(Added by ashish)
                //if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES"))
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstDisplayTables, "ADDRESS"))
                {


                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES");
                    iIdxAddr = Utilities.IndexInArray(p_arrlstDisplayTables, "ADDRESS");//RMA-8753 nshah28(Added by ashish)
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[MED_TEMP]", "[ADDRESSES_TEMP]", "STAFF_EID");

                    //Dump results into temp table
                    if (bPhoneNumbersIncluded && bAddressFlag)
                    {
                        JoinIt(ref sFrom, ref sWhere, "[MED_TEMP]", "[ADDRESSES_TEMP]", "PHONE_ID", true);
                        sSQL = "SELECT [MED_TEMP].STAFF_EID,[MED_TEMP].PHONE_ID";
                    }
                    else
                    {
                        sSQL = "SELECT [MED_TEMP].STAFF_EID";
                    }

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    //RMA-8753 nshah28(Added by ashish)
                    if (iIdxAddr > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxAddr].ToString());
                    sSQL = sSQL + " [INTO ADDRESSES_TEMP]";
                    sSQL = sSQL + " FROM [MED_TEMP],ENTITY_X_ADDRESSES,ADDRESS";
                    sSQL = sSQL + " WHERE ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID AND [MED_TEMP].STAFF_EID = ENTITY_X_ADDRESSES.ENTITY_ID";
                    //RMA-8753 nshah28(Added by ashish)END

                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ADDTEMP_" + m_lUserId + " ON [ADDRESSES_TEMP](STAFF_EID)"));

                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ADDRESS_X_PHONEINFO"))
                {


                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ORG_ADDRESS_X_PHONEINFO");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[MED_TEMP]", "[ORG_ADD_TEMP]", "STAFF_EID");

                    //Dump results into temp table
                    if (bPhoneNumbersIncluded && bOrgFlag)
                    {
                        JoinIt(ref sFrom, ref sWhere, "[MED_TEMP]", "[ORG_ADD_TEMP]", "PHONE_ID", true);
                        sSQL = "SELECT [MED_TEMP].STAFF_EID,[MED_TEMP].PHONE_ID";
                    }
                    else
                    {
                        sSQL = "SELECT [MED_TEMP].STAFF_EID";
                    }

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    sSQL = sSQL + " [INTO ORG_ADD_TEMP]";
                    sSQL = sSQL + " FROM [MED_TEMP],MED_STAFF,ADDRESS_X_PHONEINFO ORG_ADDRESS_X_PHONEINFO WHERE [MED_TEMP].STAFF_EID = MED_STAFF.STAFF_EID";
                    sSQL = sSQL + " AND MED_STAFF.DEPT_ASSIGNED_EID = ORG_ADDRESS_X_PHONEINFO.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ORGADDTEMP_" + m_lUserId + " ON [ORG_ADD_TEMP](STAFF_EID)"));

                }
                //Added Rakhi for R7:Add Emp Data Elements

                //MULTI-CODE Type Supplementals
                //Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[MED_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "STAFF_EID");

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [MED_TEMP].STAFF_EID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]";
                        sSQL = sSQL + " FROM [MED_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                        sSQL = sSQL + " WHERE [MED_TEMP].STAFF_EID = " + sMultiCodeTableAlias + ".RECORD_ID";
                        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](STAFF_EID)"));
                    }
                }

                //Chain everything together
                //p_sMedStaffData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sMedStaffData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sMedStaffData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoMedStaffSearch.GenericError", m_iClientId), p_objException);
            }
            //mbahl3 mits 30224
            finally
            {
                objSettings = null;

            }
           //mbahl3 mits 30224
        }
        #endregion

        #region Employee search
        /// <summary>
        /// This function generates the search data related to Employee module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sEmployeeData">Employee Data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        public void DoEmployeeSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sEmployeeData, ref DbConnection p_objConn, string p_sOrgLevel, int p_iPageSize, int p_iPageNumber)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            string sMultiCodeTableAlias = "";
            int iIdx = 0;
            int iIdxAddr = 0;//RMA-8753 nshah28(Added by ashish)
            long lFieldId = 0;
            SysSettings objSettings = null; //mbahl3  mits 30224
            string sPresentdate = string.Empty; //mbahl3  mits 30224
            try
            {
                //Delete temp table, if any
                //if (m_sDBType != Constants.DB_ORACLE)
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupEmployeeSearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpEmployeeSearch");
                }

                sFrom = "EMPLOYEE, ENTITY | ENTITY";
                sWhere = "EMPLOYEE.EMPLOYEE_EID = ENTITY.ENTITY_ID AND ENTITY.DELETED_FLAG = 0 ";
                sSelect = "SELECT EMPLOYEE.EMPLOYEE_EID ";

                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_ENTITY"))
                {
                    sFrom = sFrom + ",ENTITY ORG_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    if (p_sOrgLevel != "")
                    {
                        sFrom = sFrom + ", ORG_HIERARCHY";
                        sWhere = sWhere + "ORG_HIERARCHY." + p_sOrgLevel + " = ORG_ENTITY.ENTITY_ID"
                            + " AND ORG_HIERARCHY.DEPARTMENT_EID = EMPLOYEE.DEPT_ASSIGNED_EID";
                    }
                    else
                        sWhere = sWhere + "EMPLOYEE.DEPT_ASSIGNED_EID = ORG_ENTITY.ENTITY_ID";
                }

                //mbahl3  mits 30224
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);
                if (objSettings.StatusActive)
                {
                   // sPresentdate = DateTime.Now.ToShortDateString();
                    sPresentdate = Conversion.ToDbDate(DateTime.Now);

                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + " ( ENTITY.EFFECTIVE_DATE <='" + sPresentdate + "'  OR ENTITY.EFFECTIVE_DATE IS NULL OR ENTITY.EFFECTIVE_DATE ='')";
                    sWhere = sWhere + " AND ( ENTITY.EXPIRATION_DATE >='" + sPresentdate + "'  OR ENTITY.EXPIRATION_DATE IS NULL OR ENTITY.EXPIRATION_DATE ='')";

                }
                else
                {
                    p_sJustFieldNames = p_sJustFieldNames + ",[EMPLOYEE_TEMP].EFFECTIVE_DATE EFFDATE,[EMPLOYEE_TEMP].EXPIRATION_DATE EXPDATE";
                    IsEntitySearch = true;
                }
                //mbahl3  mits 30224


                //MGaba2:MITS 13205:09/04/2008:It should not be added if org hierarchy fields are supplemental
                //Commented the following code as following criteria has been added in "DoSearch" itself
                //// Abhishek  changes related to MITS 11341 start
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_HIERARCHY"))
                //{
                //    sFrom = sFrom + ",ORG_HIERARCHY";
                //    if (sWhere != "")
                //        sWhere = sWhere + " AND ";
                //    sWhere = sWhere + "EMPLOYEE.DEPT_ASSIGNED_EID = ORG_HIERARCHY.DEPARTMENT_EID";
                //}
                ////Abhishek  changes related to MITS 11341 End
                //MGaba2:MITS 20175:Commenting this as this join is no more needed
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_HIE"))
                //{
                //    sFrom = sFrom + ",ORG_HIERARCHY ORG_HIE";
                //}
                //MGaba2:MITS 13205:09/04/2008:End
                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS_X_PHONEINFO"))
                {
                    sFrom = sFrom + ", ADDRESS_X_PHONEINFO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "EMPLOYEE.EMPLOYEE_EID  = ADDRESS_X_PHONEINFO.ENTITY_ID";
                    bPhoneNumbersIncluded = true;
                }

                //RMA-8753 nshah28(Added by ashish)
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS"))
                {

                    sFrom = sFrom + ", ENTITY_X_ADDRESSES, ADDRESS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID AND EMPLOYEE.EMPLOYEE_EID  = ENTITY_X_ADDRESSES.ENTITY_ID";
                    bAddressesIncluded = true;

                }
                //RMA-8753 nshah28(Added by ashish) END
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_ADDRESS_X_PHONEINFO"))
                {
                    sFrom = sFrom + ",ADDRESS_X_PHONEINFO ORG_ADDRESS_X_PHONEINFO";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "EMPLOYEE.EMPLOYEE_EID  = ORG_ADDRESS_X_PHONEINFO.ENTITY_ID";
                    bOrgHierarchyPhoneIncluded = true;
                }
                //Added Rakhi for R7:Add Emp Data Elements

                //plug in other tables which have criteria
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "EMP_SUPP"))
                {
                    sFrom = sFrom + ", EMP_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "EMPLOYEE.EMPLOYEE_EID = EMP_SUPP.EMPLOYEE_EID";
                }

                //Supplimental Multi-Code Field allowed
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "EMPLOYEE.EMPLOYEE_EID");

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }

                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;

                //Added Rakhi for R7:Add Emp Data Elements
                CheckMultiplePhoneAddresses(bPhoneNumbersIncluded, bAddressesIncluded, bOrgHierarchyPhoneIncluded, p_arrlstDisplayTables, ref sSelect, ref bAddressFlag, ref bOrgFlag);
                //Added Rakhi for R7:Add Emp Data Elements
                //mbahl3 Mits 30224
                if (!objSettings.StatusActive)
                {
                    //RMA-8753 nshah28(Added by ashish)
                    if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstQueryTables, "ADDRESS"))
                    {
                        sSelect = sSelect + ",ENTITY_X_ADDRESSES.EFFECTIVE_DATE ,ENTITY_X_ADDRESSES.EXPIRATION_DATE ";
                    }
                    else
                    {
                        sSelect = sSelect + ",EFFECTIVE_DATE ,EXPIRATION_DATE ";
                    }
                    //RMA-8753 nshah28(Added by ashish) END
                }
                //mbahl3 Mits 30224


                //run criteria query into temp table
                sSQL = sSelect + " [INTO EMPLOYEE_TEMP] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;

                //run criteria query into temp table
                ExecSearchSQL(sSQL, ref p_objConn);
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX EMPTEMP_" + m_lUserId + " ON [EMPLOYEE_TEMP](EMPLOYEE_EID)"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[EMPLOYEE_TEMP]";
                sWhere = "";

                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ORG_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EMPLOYEE_TEMP]", "[ORG_TEMP]", "EMPLOYEE_EID");

                    //Dump results into temp table
                    sSQL = "SELECT EMPLOYEE.EMPLOYEE_EID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO ORG_TEMP]"
                        + " FROM [EMPLOYEE_TEMP],EMPLOYEE,ENTITY ORG_ENTITY WHERE [EMPLOYEE_TEMP].EMPLOYEE_EID = EMPLOYEE.EMPLOYEE_EID"
                        + " AND EMPLOYEE.DEPT_ASSIGNED_EID = ORG_ENTITY.ENTITY_ID ";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ORGTEMP_" + m_lUserId + " ON [ORG_TEMP](EMPLOYEE_EID )"));
                }
                //Added Rakhi for R7:Add Emp Data Elements
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EMPLOYEE_TEMP]", "[PHONES_TEMP]", "EMPLOYEE_EID");

                    //Dump results into temp table
                    if (bAddressesIncluded || bOrgHierarchyPhoneIncluded)
                    {
                        if (bAddressesIncluded)
                        {
                            JoinIt(ref sFrom, ref sWhere, "[EMPLOYEE_TEMP]", "[PHONES_TEMP]", "ADDRESS_ID", true);
                            sSQL = "SELECT [EMPLOYEE_TEMP].EMPLOYEE_EID,[EMPLOYEE_TEMP].ADDRESS_ID";
                        }
                        if (bOrgHierarchyPhoneIncluded)
                        {
                            JoinIt(ref sFrom, ref sWhere, "[EMPLOYEE_TEMP]", "[PHONES_TEMP]", "PHONE_ID", true);
                            sSQL = "SELECT [EMPLOYEE_TEMP].EMPLOYEE_EID,[EMPLOYEE_TEMP].PHONE_ID";
                        }
                    }
                    else
                    {
                        sSQL = "SELECT [EMPLOYEE_TEMP].EMPLOYEE_EID";
                    }
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    sSQL = sSQL + " [INTO PHONES_TEMP]"
                        + " FROM [EMPLOYEE_TEMP],ADDRESS_X_PHONEINFO"
                        + " WHERE [EMPLOYEE_TEMP].EMPLOYEE_EID = ADDRESS_X_PHONEINFO.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PHONETEMP_" + m_lUserId + " ON [PHONES_TEMP](EMPLOYEE_EID )"));
                }
                //AA//RMA-8753 nshah28(Added by ashish)
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstDisplayTables, "ADDRESS"))
                {

                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES");
                    iIdxAddr = Utilities.IndexInArray(p_arrlstDisplayTables, "ADDRESS");//RMA-8753 nshah28(Added by ashish)
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EMPLOYEE_TEMP]", "[ADDRESSES_TEMP]", "EMPLOYEE_EID");

                    //Dump results into temp table
                    if (bPhoneNumbersIncluded && bAddressFlag)
                    {
                        JoinIt(ref sFrom, ref sWhere, "[EMPLOYEE_TEMP]", "[ADDRESSES_TEMP]", "PHONE_ID", true);
                        sSQL = "SELECT [EMPLOYEE_TEMP].EMPLOYEE_EID,[EMPLOYEE_TEMP].PHONE_ID";
                    }
                    else
                    {
                        sSQL = "SELECT [EMPLOYEE_TEMP].EMPLOYEE_EID";
                    }

                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    //RMA-8753 nshah28(Added by ashish)
                    if (iIdxAddr > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxAddr].ToString());
                    sSQL = sSQL + " [INTO ADDRESSES_TEMP]"
                        + " FROM [EMPLOYEE_TEMP],ENTITY_X_ADDRESSES,ADDRESS"
                        + " WHERE ENTITY_X_ADDRESSES.ADDRESS_ID = ADDRESS.ADDRESS_ID AND [EMPLOYEE_TEMP].EMPLOYEE_EID = ENTITY_X_ADDRESSES.ENTITY_ID";
                    //RMA-8753 nshah28(Added by ashish) END

                    ExecSearchSQL(sSQL, ref p_objConn);


                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ADDTEMP_" + m_lUserId + " ON [ADDRESSES_TEMP](EMPLOYEE_EID)"));

                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ADDRESS_X_PHONEINFO"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ORG_ADDRESS_X_PHONEINFO");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EMPLOYEE_TEMP]", "[ORG_ADD_TEMP]", "EMPLOYEE_EID");

                    //Dump results into temp table
                    if (bPhoneNumbersIncluded && bOrgFlag)
                    {
                        JoinIt(ref sFrom, ref sWhere, "[EMPLOYEE_TEMP]", "[ORG_ADD_TEMP]", "PHONE_ID", true);
                        sSQL = "SELECT [EMPLOYEE_TEMP].EMPLOYEE_EID,[EMPLOYEE_TEMP].PHONE_ID";
                    }
                    else
                    {
                        sSQL = "SELECT [EMPLOYEE_TEMP].EMPLOYEE_EID";
                    }

                    //Dump results into temp table
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    sSQL = sSQL + " [INTO ORG_ADD_TEMP]"
                        + " FROM [EMPLOYEE_TEMP],EMPLOYEE,ADDRESS_X_PHONEINFO ORG_ADDRESS_X_PHONEINFO WHERE [EMPLOYEE_TEMP].EMPLOYEE_EID = EMPLOYEE.EMPLOYEE_EID"
                        + " AND EMPLOYEE.DEPT_ASSIGNED_EID = ORG_ADDRESS_X_PHONEINFO.ENTITY_ID ";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ORGADDTEMP_" + m_lUserId + " ON [ORG_ADD_TEMP](EMPLOYEE_EID )"));
                }
                //Added Rakhi for R7:Add Emp Data Elements

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "EMP_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "EMP_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[EMPLOYEE_TEMP]", "[EMPSUPP_TEMP]", "EMPLOYEE_EID");

                    //Dump results into temp table
                    sSQL = "SELECT EMP_SUPP.EMPLOYEE_EID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO EMPSUPP_TEMP]"
                        + " FROM [EMPLOYEE_TEMP], EMP_SUPP"
                        + " WHERE [EMPLOYEE_TEMP].EMPLOYEE_EID = EMP_SUPP.EMPLOYEE_EID ";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX EMPSUPPTEMP_" + m_lUserId + " ON [EMPSUPP_TEMP](EMPLOYEE_EID )"));
                }

                //Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[EMPLOYEE_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "EMPLOYEE_EID");

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [EMPLOYEE_TEMP].EMPLOYEE_EID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]"
                            + " FROM [EMPLOYEE_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias
                            + " WHERE [EMPLOYEE_TEMP].EMPLOYEE_EID = " + sMultiCodeTableAlias + ".RECORD_ID"
                            + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](EMPLOYEE_EID)"));
                    }
                }

                //Chain everything together
                //p_sEmployeeData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sEmployeeData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sEmployeeData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoEmployeeSearch.GenericError", m_iClientId), p_objException);
            }
            //mbahl3  mits 30224
            finally
            {
                objSettings = null;

            }
            //mbahl3  mits 30224
        }
        #endregion

        #region Payment search
        /// <summary>
        /// This function generates the search data related to Payment module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sPaymentData">Payment data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        // Mihika 16-Feb-2006 Changed Payment Search to inlude supplemental field search.
        //pmittal5 Mits 18356 11/13/09 - Added parameter "p_arrlstTextSuppFields" containing Free Text type Supp fields 
        //public void DoPaymentSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sPaymentData, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber)
        public void DoPaymentSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sPaymentData, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber, ArrayList p_arrlstTextSuppFields)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            string sMultiCodeTableAlias = "";
            int iIdx = 0;
            long lFieldId = 0;
            //Add by kuladeep for mits:25621 Start
            string sAmtColumn = string.Empty;
            string sSqlUpdate = string.Empty;
            //Add by kuladeep for mits:25621 end
            SysSettings objSettings = null;
            try
            {
                //Delete temp table, if any
                //if (m_sDBType != Constants.DB_ORACLE)
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupPaymentSearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpPaymentSearch");
                }

                //Build criteria query
                sFrom = "FUNDS";
                sWhere = "";
                sSelect = "SELECT FUNDS.TRANS_ID,FUNDS.COLLECTION_FLAG ";//Add COLLECTION_FLAG by kuladeep for mits:25621

                //payee
                //pmittal5 Mits 14021 01/28/09
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "PAYEE_ENTITY") || Utilities.ExistsInArray(p_arrlstDisplayTables, "PAYEE_ENTITY"))
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PAYEE_ENTITY"))
                {
                    sFrom = sFrom + ", ENTITY PAYEE_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "FUNDS.PAYEE_EID = PAYEE_ENTITY.ENTITY_ID";
                }

                //splits
                //pmittal5 Mits 14021 01/28/09
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "FUNDS_TRANS_SPLIT") || Utilities.ExistsInArray(p_arrlstDisplayTables, "FUNDS_TRANS_SPLIT"))
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "FUNDS_TRANS_SPLIT"))
                {
                    sFrom = sFrom + ",FUNDS_TRANS_SPLIT";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID";
                }

                // Account Table
                //pmittal5 Mits 14021 01/28/09
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "FUNDS_ACCOUNT") || Utilities.ExistsInArray(p_arrlstDisplayTables, "FUNDS_ACCOUNT"))
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "FUNDS_ACCOUNT"))
                {
                    sFrom = sFrom + ",ACCOUNT FUNDS_ACCOUNT";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "FUNDS.ACCOUNT_ID = FUNDS_ACCOUNT.ACCOUNT_ID";
                }

                // Mihika Defect no. 2040 Search was not working when Claim_Number was selected as a search criteria
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIM"))
                {
                    sFrom = sFrom + ",CLAIM";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIM.CLAIM_ID = FUNDS.CLAIM_ID";
                }
                //Shruti for adding Primary Claimant in Funds Search
                //pmittal5 Mits 14021 01/28/09
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIMANT_ENTITY") || Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIMANT_ENTITY"))
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIMANT_ENTITY"))
                {
                    sFrom = sFrom + ",ENTITY CLAIMANT_ENTITY, CLAIMANT";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "CLAIMANT.CLAIM_ID = FUNDS.CLAIM_ID AND CLAIMANT.PRIMARY_CLMNT_FLAG = -1"
                    + " AND CLAIMANT_ENTITY.ENTITY_ID = CLAIMANT.CLAIMANT_EID";
                }

                // Funds Supplemental
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "FUNDS_SUPP"))
                {
                    sFrom = sFrom + ",FUNDS_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "FUNDS.TRANS_ID = FUNDS_SUPP.TRANS_ID";
                }


                // Funds Trans Split Supplemental
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "FUNDS_TRANS_SPLIT_SUPP"))
                {
                    //Raman 01/04/2009 : We cannot add Funds_Trans_Split if it already exists
                    if (sFrom.Contains("FUNDS_TRANS_SPLIT"))
                        sFrom = sFrom + ",FUNDS_TRANS_SPLIT_SUPP";
                    else
                        sFrom = sFrom + ",FUNDS_TRANS_SPLIT_SUPP, FUNDS_TRANS_SPLIT";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID";
                    sWhere = sWhere + " AND FUNDS_TRANS_SPLIT.SPLIT_ROW_ID = FUNDS_TRANS_SPLIT_SUPP.SPLIT_ROW_ID";
                }

                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "FUNDS.TRANS_ID");

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }

                // For Supplemental field Support add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;

                sSQL = sSelect + " [INTO FUNDS_TEMP] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;

                ExecSearchSQL(sSQL, ref p_objConn);
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX FUNDSTEMP_" + m_lUserId + " ON [FUNDS_TEMP](TRANS_ID)"));
                //Add by kuladeep for mits:25621 Start
                if (sSQL.IndexOf("FUNDS.AMOUNT") > 0)
                {
                    //MITS 35391 2/18/2014
                    //if (sSQL.Substring(sSQL.IndexOf("FUNDS.AMOUNT") + 15).IndexOf(",") > 0)//skhare7 MITS 27263 start
                    if (sSQL.IndexOf("FUNDS.AMOUNT") > 0 && (sSQL.Substring(sSQL.IndexOf("FUNDS.AMOUNT") + 15).IndexOf(",") != -1 && sSQL.Substring(sSQL.IndexOf("FUNDS.AMOUNT") + 15).IndexOf(",") < sSQL.Substring(sSQL.IndexOf("FUNDS.AMOUNT") + 15).IndexOf("[")))//skhare7 MITS 27263 start
                        sAmtColumn = sSQL.Substring(sSQL.IndexOf("FUNDS.AMOUNT") + 15, sSQL.Substring(sSQL.IndexOf("FUNDS.AMOUNT") + 15).IndexOf(","));
                    else
                        sAmtColumn = sSQL.Substring(sSQL.IndexOf("FUNDS.AMOUNT") + 15, sSQL.Substring(sSQL.IndexOf("FUNDS.AMOUNT") + 15).IndexOf("["));//skhare7 MITS 27263 End
                    if (!string.IsNullOrEmpty(sAmtColumn))
                    {
                        sSqlUpdate = "UPDATE [FUNDS_TEMP] SET " + sAmtColumn + "=-" + sAmtColumn + " WHERE [FUNDS_TEMP].COLLECTION_FLAG=-1 AND " + sAmtColumn + ">0";
                        p_objConn.ExecuteNonQuery(PrepareTempSQL(sSqlUpdate));
                    }
                }
                //Add by kuladeep for mits:25621 End
                sFrom = "[FUNDS_TEMP]";
                sWhere = "";

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIM"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIM");

                    //Add temp table to final outer joined query VEHICLE.UNIT_ID
                    JoinIt(ref sFrom, ref sWhere, "[FUNDS_TEMP]", "[CLAIM_TEMP]", "TRANS_ID");

                    //Dump results into temp table
                    sSQL = "SELECT DISTINCT [FUNDS_TEMP].TRANS_ID," + p_arrlstDisplayFields[iIdx] + " [INTO CLAIM_TEMP]"
                        + " FROM [FUNDS_TEMP],CLAIM,FUNDS"
                        + " WHERE [FUNDS_TEMP].TRANS_ID = FUNDS.TRANS_ID"
                        + " AND FUNDS.CLAIM_ID = CLAIM.CLAIM_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLAIMTEMP_" + m_lUserId + " ON [CLAIM_TEMP](TRANS_ID )"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "FUNDS_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "FUNDS_SUPP");

                    //Add temp table to final outer joined query VEHICLE.UNIT_ID
                    JoinIt(ref sFrom, ref sWhere, "[FUNDS_TEMP]", "[FUNDSSUPP_TEMP]", "TRANS_ID");

                    //Dump results into temp table
                    //pmittal5 Mits 18356 11/13/09 - If Free Text Supplemental field is there in Select list, DISTINCT is not added
                    //sSQL = "SELECT DISTINCT FUNDS_SUPP.TRANS_ID," + p_arrlstDisplayFields[iIdx] + " [INTO FUNDSSUPP_TEMP]";
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString(), p_arrlstTextSuppFields) + "  FUNDS_SUPP.TRANS_ID," + p_arrlstDisplayFields[iIdx] + " [INTO FUNDSSUPP_TEMP]"
                        + " FROM [FUNDS_TEMP],FUNDS_SUPP"
                        + " WHERE [FUNDS_TEMP].TRANS_ID = FUNDS_SUPP.TRANS_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX FUNDSSUPPTEMP_" + m_lUserId + " ON [FUNDSSUPP_TEMP](TRANS_ID )"));
                }

                //pmittal5 Mits 14021 01/28/09
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PAYEE_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PAYEE_ENTITY");

                    JoinIt(ref sFrom, ref sWhere, "[FUNDS_TEMP]", "[PAYEE_TEMP]", "TRANS_ID");

                    //Dump results into temp table
                    sSQL = "SELECT DISTINCT [FUNDS_TEMP].TRANS_ID," + p_arrlstDisplayFields[iIdx] + " [INTO PAYEE_TEMP]"
                        + " FROM [FUNDS_TEMP],FUNDS,ENTITY PAYEE_ENTITY"
                        + " WHERE [FUNDS_TEMP].TRANS_ID = FUNDS.TRANS_ID"
                        + " AND FUNDS.PAYEE_EID = PAYEE_ENTITY.ENTITY_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PAYEETEMP_" + m_lUserId + " ON [PAYEE_TEMP](TRANS_ID )"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "FUNDS_TRANS_SPLIT"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "FUNDS_TRANS_SPLIT");

                    JoinIt(ref sFrom, ref sWhere, "[FUNDS_TEMP]", "[FUNDS_TRANSPLT_TEMP]", "TRANS_ID");
                    // Ishan : Multi Currency Search. 
                    objSettings = new SysSettings(p_objConn.ConnectionString,m_iClientId);
                    if (objSettings.UseMultiCurrency != 0 && (p_arrlstDisplayFields[iIdx].ToString().Contains("PMT_CURRENCY_AMOUNT") || p_arrlstDisplayFields[iIdx].ToString().Contains("PMT_CURRENCY_AUTO_DISCOUNT") || p_arrlstDisplayFields[iIdx].ToString().Contains("PMT_CURRENCY_INVOICE_AMOUNT")
                        || p_arrlstDisplayFields[iIdx].ToString().Contains("CLAIM_CURRENCY_AMOUNT") || p_arrlstDisplayFields[iIdx].ToString().Contains("CLAIM_CURRENCY_INVOICE_AMOUNT") || p_arrlstDisplayFields[iIdx].ToString().Contains("CLAIM_CURRENCY_AUTO_DISCOUNT")))
                    {
                        string[] arIPC = null;
                        string[] arICA = null;
                        string[] arFA = null;
                        string[] arPC = null;
                        string[] arCA = null;
                        string[] arDPC = null;
                        string[] arDCA = null;
                        ArrayList ar = new ArrayList();
                        sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString()) + " FUNDS_TRANS_SPLIT.TRANS_ID," + p_arrlstDisplayFields[iIdx].ToString() +
                            " ,FUNDS.PMT_CURRENCY_CODE,FUNDS.CLAIM_CURRENCY_CODE [INTO FUNDS_TRANSPLT_TEMP]";
                        sSQL = sSQL + " FROM [FUNDS_TEMP],FUNDS_TRANS_SPLIT,FUNDS";
                        sSQL = sSQL + " WHERE [FUNDS_TEMP].TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID";
                        ExecSearchSQL(sSQL, ref p_objConn);

                        if (p_arrlstDisplayFields[iIdx].ToString().Contains("PMT_CURRENCY_AMOUNT"))
                        {
                            int iDex = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT")).IndexOf(",");
                            if (iDex > -1)
                                arPC = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT")).Substring(0, p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT")).IndexOf(",")).Split('|');
                            else
                                arPC = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AMOUNT")).Split('|');
                        }
                        if (p_arrlstDisplayFields[iIdx].ToString().Contains("CLAIM_CURRENCY_AMOUNT"))
                        {
                            int inDex = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT")).IndexOf(",");
                            if (inDex > -1)
                                arCA = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT")).Substring(0, p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT")).IndexOf(",")).Split('|');
                            else
                                arCA = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AMOUNT")).Split('|');
                        }
                        if (p_arrlstDisplayFields[iIdx].ToString().Contains("PMT_CURRENCY_INVOICE_AMOUNT"))
                        {
                            int iDex = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_INVOICE_AMOUNT")).IndexOf(",");
                            if (iDex > -1)
                                arIPC = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_INVOICE_AMOUNT")).Substring(0, p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_INVOICE_AMOUNT")).IndexOf(",")).Split('|');
                            else
                                arIPC = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_INVOICE_AMOUNT")).Split('|');
                        }
                        if (p_arrlstDisplayFields[iIdx].ToString().Contains("CLAIM_CURRENCY_INVOICE_AMOUNT"))
                        {
                            int inDex = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_INVOICE_AMOUNT")).IndexOf(",");
                            if (inDex > -1)
                                arICA = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_INVOICE_AMOUNT")).Substring(0, p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_INVOICE_AMOUNT")).IndexOf(",")).Split('|');
                            else
                                arICA = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_INVOICE_AMOUNT")).Split('|');
                        }
                        if (p_arrlstDisplayFields[iIdx].ToString().Contains("PMT_CURRENCY_AUTO_DISCOUNT"))
                        {
                            int iDex = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AUTO_DISCOUNT")).IndexOf(",");
                            if (iDex > -1)
                                arDPC = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AUTO_DISCOUNT")).Substring(0, p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AUTO_DISCOUNT")).IndexOf(",")).Split('|');
                            else
                                arDPC = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("PMT_CURRENCY_AUTO_DISCOUNT")).Split('|');
                        }
                        if (p_arrlstDisplayFields[iIdx].ToString().Contains("CLAIM_CURRENCY_AUTO_DISCOUNT"))
                        {
                            int inDex = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AUTO_DISCOUNT")).IndexOf(",");
                            if (inDex > -1)
                                arDCA = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AUTO_DISCOUNT")).Substring(0, p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AUTO_DISCOUNT")).IndexOf(",")).Split('|');
                            else
                                arDCA = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("CLAIM_CURRENCY_AUTO_DISCOUNT")).Split('|');
                        }
                        if (p_arrlstDisplayFields[iIdx].ToString().Contains("FUNDS.AMOUNT"))
                        {
                            int inNDex = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("FUNDS.AMOUNT")).IndexOf(",");
                            if (inNDex > -1)
                                arFA = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("FUNDS.AMOUNT")).Substring(0, p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("AMOUNT")).IndexOf(",")).Split('|');
                            else
                                arFA = p_arrlstDisplayFields[iIdx].ToString().Substring(p_arrlstDisplayFields[iIdx].ToString().IndexOf("FUNDS.AMOUNT")).Split('|');
                        }


                        string sSQLTempNew = PrepareTempSQL("SELECT " + p_arrlstDisplayFields[iIdx].ToString().Replace("FUNDS_TRANS_SPLIT.CLAIM_CURRENCY_INVOICE_AMOUNT", "").Replace("FUNDS_TRANS_SPLIT.PMT_CURRENCY_INVOICE_AMOUNT", "").Replace("FUNDS_TRANS_SPLIT.CLAIM_CURRENCY_AUTO_DISCOUNT", "").Replace("FUNDS_TRANS_SPLIT.PMT_CURRENCY_AUTO_DISCOUNT", "").Replace("FUNDS_TRANS_SPLIT.CLAIM_CURRENCY_AMOUNT", "").Replace("FUNDS_TRANS_SPLIT.PMT_CURRENCY_AMOUNT", "")
                                     + " ,[FUNDS_TRANSPLT_TEMP].PMT_CURRENCY_CODE,[FUNDS_TRANSPLT_TEMP].CLAIM_CURRENCY_CODE FROM [FUNDS_TRANSPLT_TEMP]");
                        if (arICA != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arICA[1].Trim(), arICA[1].Trim() + " " + arICA[0].Trim());
                            if (m_sDBType == Constants.DB_SQLSRVR)
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TRANSPLT_TEMP] ALTER COLUMN "
                                    + arICA[1].Trim() + " VARCHAR(30) "));
                            }
                            else
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TRANSPLT_TEMP] MODIFY "
                                    + arICA[1].Trim() + " VARCHAR2(30) "));
                            }
                        }
                        if (arIPC != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arIPC[1].Trim(), arIPC[1].Trim() + " " + arIPC[0].Trim());
                            if (m_sDBType == Constants.DB_SQLSRVR)
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TRANSPLT_TEMP] ALTER COLUMN "
                                    + arIPC[1].Trim() + " VARCHAR(30) "));
                            }
                            else
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TRANSPLT_TEMP] MODIFY "
                                    + arIPC[1].Trim() + " VARCHAR2(30) "));
                            }
                        }
                        if (arPC != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arPC[1].Trim(), arPC[1].Trim() + " " + arPC[0].Trim());
                            if (m_sDBType == Constants.DB_SQLSRVR)
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TRANSPLT_TEMP] ALTER COLUMN "
                                    + arPC[1].Trim() + " VARCHAR(30) "));
                            }
                            else
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TRANSPLT_TEMP] MODIFY "
                                    + arPC[1].Trim() + " VARCHAR2(30) "));
                            }
                        }
                        if (arCA != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arCA[1].Trim(), arCA[1].Trim() + " " + arCA[0].Trim());
                            if (m_sDBType == Constants.DB_SQLSRVR)
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TRANSPLT_TEMP] ALTER COLUMN "
                                    + arCA[1].Trim() + " VARCHAR(30) "));
                            }
                            else
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TRANSPLT_TEMP] MODIFY "
                                    + arCA[1].Trim() + " VARCHAR2(30) "));
                            }
                        }
                        if (arDPC != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arDPC[1].Trim(), arDPC[1].Trim() + " " + arDPC[0].Trim());
                            if (m_sDBType == Constants.DB_SQLSRVR)
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TRANSPLT_TEMP] ALTER COLUMN "
                                    + arDPC[1].Trim() + " VARCHAR(30) "));
                            }
                            else
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TRANSPLT_TEMP] MODIFY "
                                    + arDPC[1].Trim() + " VARCHAR2(30) "));
                            }
                        }
                        if (arDCA != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arDCA[1].Trim(), arDCA[1].Trim() + " " + arDCA[0].Trim());
                            if (m_sDBType == Constants.DB_SQLSRVR)
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TRANSPLT_TEMP] ALTER COLUMN "
                                    + arDCA[1].Trim() + " VARCHAR(30) "));
                            }
                            else
                            {
                                p_objConn.ExecuteNonQuery(PrepareTempSQL("ALTER TABLE [FUNDS_TRANSPLT_TEMP] MODIFY "
                                    + arDCA[1].Trim() + " VARCHAR2(30) "));
                            }
                        }
                        if (arFA != null)
                        {
                            sSQLTempNew = sSQLTempNew.Replace(arFA[1].Trim(), arFA[1].Trim() + " " + "AMOUNT");
                        }
                        using (DbReader rdr = p_objConn.ExecuteReader(PrepareTempSQL(sSQLTempNew)))
                        {
                            while (rdr.Read())
                            {
                                if (arICA != null)
                                {
                                    string sClaimInvAmt = Conversion.ConvertObjToStr(rdr.GetValue("CLAIM_CURRENCY_INVOICE_AMOUNT"));
                                    string sCurrCode = Conversion.ConvertObjToStr(rdr.GetValue("CLAIM_CURRENCY_CODE"));
                                    ar.Add(PrepareTempSQL("UPDATE [FUNDS_TRANSPLT_TEMP] SET " + arICA[1].Trim() + "='" + sClaimInvAmt + "_" + sCurrCode +
                                        "' WHERE " + arICA[1].Trim() + "= '" + sClaimInvAmt + "' AND [FUNDS_TRANSPLT_TEMP].CLAIM_CURRENCY_CODE=" + sCurrCode));
                                }
                                if (arIPC != null)
                                {
                                    string sPmtInvAmt = Conversion.ConvertObjToStr(rdr.GetValue("PMT_CURRENCY_INVOICE_AMOUNT"));
                                    string sCurrCode = Conversion.ConvertObjToStr(rdr.GetValue("PMT_CURRENCY_CODE"));
                                    ar.Add(PrepareTempSQL("UPDATE [FUNDS_TRANSPLT_TEMP] SET " + arIPC[1].Trim() + "='" + sPmtInvAmt + "_" + sCurrCode +
                                        "' WHERE " + arIPC[1].Trim() + "= '" + sPmtInvAmt + "' AND [FUNDS_TRANSPLT_TEMP].PMT_CURRENCY_CODE=" + sCurrCode));
                                }
                                if (arCA != null)
                                {
                                    string sClaimAmt = Conversion.ConvertObjToStr(rdr.GetValue("CLAIM_CURRENCY_AMOUNT"));
                                    string sCurrCode = Conversion.ConvertObjToStr(rdr.GetValue("CLAIM_CURRENCY_CODE"));
                                    ar.Add(PrepareTempSQL("UPDATE [FUNDS_TRANSPLT_TEMP] SET " + arCA[1].Trim() + "='" + sClaimAmt + "_" + sCurrCode +
                                        "' WHERE " + arCA[1].Trim() + "= '" + sClaimAmt + "' AND [FUNDS_TRANSPLT_TEMP].CLAIM_CURRENCY_CODE=" + sCurrCode));
                                }
                                if (arPC != null)
                                {
                                    string sPmtAmt = Conversion.ConvertObjToStr(rdr.GetValue("PMT_CURRENCY_AMOUNT"));
                                    string sCurrCode = Conversion.ConvertObjToStr(rdr.GetValue("PMT_CURRENCY_CODE"));
                                    ar.Add(PrepareTempSQL("UPDATE [FUNDS_TRANSPLT_TEMP] SET " + arPC[1].Trim() + "='" + sPmtAmt + "_" + sCurrCode +
                                        "' WHERE " + arPC[1].Trim() + "= '" + sPmtAmt + "' AND [FUNDS_TRANSPLT_TEMP].PMT_CURRENCY_CODE=" + sCurrCode));
                                }
                                if (arDCA != null)
                                {
                                    string sClaimDisAmt = Conversion.ConvertObjToStr(rdr.GetValue("CLAIM_CURRENCY_AUTO_DISCOUNT"));
                                    string sCurrCode = Conversion.ConvertObjToStr(rdr.GetValue("CLAIM_CURRENCY_CODE"));
                                    ar.Add(PrepareTempSQL("UPDATE [FUNDS_TRANSPLT_TEMP] SET " + arDCA[1].Trim() + "='" + sClaimDisAmt + "_" + sCurrCode +
                                        "' WHERE " + arDCA[1].Trim() + "= '" + sClaimDisAmt + "' AND [FUNDS_TRANSPLT_TEMP].CLAIM_CURRENCY_CODE=" + sCurrCode));
                                }
                                if (arDPC != null)
                                {
                                    string sPmtDisAmt = Conversion.ConvertObjToStr(rdr.GetValue("PMT_CURRENCY_AUTO_DISCOUNT"));
                                    string sCurrCode = Conversion.ConvertObjToStr(rdr.GetValue("PMT_CURRENCY_CODE"));
                                    ar.Add(PrepareTempSQL("UPDATE [FUNDS_TRANSPLT_TEMP] SET " + arDPC[1].Trim() + "='" + sPmtDisAmt + "_" + sCurrCode +
                                        "' WHERE " + arDPC[1].Trim() + "= '" + sPmtDisAmt + "' AND [FUNDS_TRANSPLT_TEMP].PMT_CURRENCY_CODE=" + sCurrCode));
                                }
                            }
                        }
                        foreach (string sStr in ar)
                        {
                            p_objConn.ExecuteNonQuery(sStr);
                        }
                    }
                    else
                    {
                        //Dump results into temp table
                        sSQL = "SELECT DISTINCT [FUNDS_TEMP].TRANS_ID, FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, " + p_arrlstDisplayFields[iIdx] + " [INTO FUNDS_TRANSPLT_TEMP]"
                            + " FROM [FUNDS_TEMP],FUNDS,FUNDS_TRANS_SPLIT"
                            + " WHERE [FUNDS_TEMP].TRANS_ID = FUNDS.TRANS_ID"
                            + " AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID";

                        ExecSearchSQL(sSQL, ref p_objConn);
                    }
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX FUNDSTRANSPLTTEMP_" + m_lUserId + " ON [FUNDS_TRANSPLT_TEMP](TRANS_ID )"));

                }


                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "FUNDS_TRANS_SPLIT_SUPP"))
                {
                    //MITS 18026: 12/22/2009 Raman Bhatia
                    //Funds Trans Split Supp need creation of Funds_Trans_Split temperory table due to absence 
                    //of any direct relationship with parent Funds. 
                    //hence adding Funds_Trans_split temperory table to create appropriate joins.
                    if (!(Utilities.ExistsInArray(p_arrlstDisplayTables, "FUNDS_TRANS_SPLIT")))
                    {
                        JoinIt(ref sFrom, ref sWhere, "[FUNDS_TEMP]", "[FUNDS_TRANSPLT_TEMP]", "TRANS_ID");

                        //Dump results into temp table
                        sSQL = "SELECT DISTINCT [FUNDS_TEMP].TRANS_ID, FUNDS_TRANS_SPLIT.SPLIT_ROW_ID [INTO FUNDS_TRANSPLT_TEMP]"
                            + " FROM [FUNDS_TEMP],FUNDS,FUNDS_TRANS_SPLIT"
                            + " WHERE [FUNDS_TEMP].TRANS_ID = FUNDS.TRANS_ID"
                            + " AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID";

                        ExecSearchSQL(sSQL, ref p_objConn);

                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX FUNDSTRANSPLTTEMP_" + m_lUserId + " ON [FUNDS_TRANSPLT_TEMP](TRANS_ID )"));
                    }
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "FUNDS_TRANS_SPLIT_SUPP");

                    JoinIt(ref sFrom, ref sWhere, "[FUNDS_TRANSPLT_TEMP]", "[FUNDS_TRANSPLTSUPP_TEMP]", "SPLIT_ROW_ID");

                    //Dump results into temp table
                    //sSQL = "SELECT DISTINCT FUNDS_TRANS_SPLIT_SUPP.SPLIT_ROW_ID," + p_arrlstDisplayFields[iIdx] + " [INTO FUNDS_TRANSPLTSUPP_TEMP]";
                    sSQL = "SELECT " + sEvalDistinct(p_arrlstDisplayFields[iIdx].ToString(), p_arrlstTextSuppFields) + " FUNDS_TRANS_SPLIT_SUPP.SPLIT_ROW_ID," + p_arrlstDisplayFields[iIdx] + " [INTO FUNDS_TRANSPLTSUPP_TEMP]"  //Mits 18356
                    + " FROM [FUNDS_TEMP], FUNDS_TRANS_SPLIT, FUNDS_TRANS_SPLIT_SUPP"
                    + " WHERE [FUNDS_TEMP].TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
                    + " AND FUNDS_TRANS_SPLIT.SPLIT_ROW_ID = FUNDS_TRANS_SPLIT_SUPP.SPLIT_ROW_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX FUNDSTRANSPLTSUPPTEMP_" + m_lUserId + " ON [FUNDS_TRANSPLTSUPP_TEMP](SPLIT_ROW_ID )"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "FUNDS_ACCOUNT"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "FUNDS_ACCOUNT");

                    JoinIt(ref sFrom, ref sWhere, "[FUNDS_TEMP]", "[FUNDS_ACCNT_TEMP]", "TRANS_ID");

                    //Dump results into temp table
                    sSQL = "SELECT DISTINCT [FUNDS_TEMP].TRANS_ID," + p_arrlstDisplayFields[iIdx] + " [INTO FUNDS_ACCNT_TEMP]"
                        + " FROM [FUNDS_TEMP],FUNDS,ACCOUNT FUNDS_ACCOUNT"
                        + " WHERE [FUNDS_TEMP].TRANS_ID = FUNDS.TRANS_ID"
                        + " AND FUNDS.ACCOUNT_ID = FUNDS_ACCOUNT.ACCOUNT_ID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX FUNDSACCNTTEMP_" + m_lUserId + " ON [FUNDS_ACCNT_TEMP](TRANS_ID )"));
                }

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "CLAIMANT_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "CLAIMANT_ENTITY");

                    JoinIt(ref sFrom, ref sWhere, "[FUNDS_TEMP]", "[CLMT_ENT_TEMP]", "TRANS_ID");

                    //Dump results into temp table
                    sSQL = "SELECT DISTINCT [FUNDS_TEMP].TRANS_ID," + p_arrlstDisplayFields[iIdx] + " [INTO CLMT_ENT_TEMP]"
                        + " FROM [FUNDS_TEMP],FUNDS,CLAIMANT,ENTITY CLAIMANT_ENTITY"
                        + " WHERE [FUNDS_TEMP].TRANS_ID = FUNDS.TRANS_ID"
                        + " AND FUNDS.CLAIM_ID = CLAIMANT.CLAIM_ID AND CLAIMANT.PRIMARY_CLMNT_FLAG = -1 AND CLAIMANT_ENTITY.ENTITY_ID = CLAIMANT.CLAIMANT_EID";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLMTENTTEMP_" + m_lUserId + " ON [CLMT_ENT_TEMP](TRANS_ID )"));
                }
                //End - pmittal5

                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[FUNDS_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "TRANS_ID");

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [FUNDS_TEMP].TRANS_ID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]"
                        + " FROM [FUNDS_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias
                        + " WHERE [FUNDS_TEMP].TRANS_ID = " + sMultiCodeTableAlias + ".RECORD_ID"
                        + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](TRANS_ID)"));
                    }
                }

                //p_sPaymentData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sPaymentData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sPaymentData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }

            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoPaymentSearch.GenericError", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Vehicle search
        /// <summary>
        /// This function generates the search data related to Vehicles module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sVehicleData">Vehicle data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        public void DoVehicleSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sVehicleData, ref DbConnection p_objConn, string p_sOrgLevel, int p_iPageSize, int p_iPageNumber)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            string sMultiCodeTableAlias = "";
            int iIdx = 0;
            long lFieldId = 0;

            try
            {
                //Delete temp table, if any
                //if (m_sDBType != Constants.DB_ORACLE)
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupVehicleSearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpVehicleSearch");
                }

                sFrom = "VEHICLE";
                sSelect = "SELECT VEHICLE.UNIT_ID ";

                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_ENTITY"))
                {
                    sFrom = sFrom + ",ENTITY ORG_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    if (p_sOrgLevel != "")
                    {
                        sFrom = sFrom + ", ORG_HIERARCHY";
                        sWhere = sWhere + "ORG_HIERARCHY." + p_sOrgLevel + " = ORG_ENTITY.ENTITY_ID"
                            + " AND ORG_HIERARCHY.DEPARTMENT_EID = VEHICLE.HOME_DEPT_EID";
                    }
                    else
                        sWhere = sWhere + "VEHICLE.HOME_DEPT_EID = ORG_ENTITY.ENTITY_ID";
                }
                //MGaba2:MITS 13205:09/04/2008:It should not be added if org hierarchy fields are supplemental
                //Commented the following code as following criteria has been added in "DoSearch" itself:Start
                //// Abhishek  changes related to MITS 11341 start
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_HIERARCHY"))
                //{
                //    sFrom = sFrom + ",ORG_HIERARCHY";
                //    if (sWhere != "")
                //        sWhere = sWhere + " AND ";
                //    sWhere = sWhere + "VEHICLE.HOME_DEPT_EID = ORG_HIERARCHY.DEPARTMENT_EID";
                //}
                ////Abhishek  changes related to MITS 11341 End
                //MGaba2:MITS 20175:Commenting this as this join is no more needed
                //if (Utilities.ExistsInArray(p_arrlstQueryTables, "ORG_HIE"))
                //{
                //    sFrom = sFrom + ",ORG_HIERARCHY ORG_HIE";
                //}
                //MGaba2:MITS 13205:09/04/2008:End

                //plug in other tables which have criteria
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "VEHICLE_SUPP"))
                {
                    sFrom = sFrom + ", VEHICLE_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "VEHICLE.UNIT_ID = VEHICLE_SUPP.UNIT_ID";
                }
                //Supplimental Multi-Code Field
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "VEHICLE.UNIT_ID");

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }

                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;

                // Mihika 9-Feb-2006 Defect no. 754 Added the where clause for DELETED_FLAG before the execution of query

                // Add  VEHICLE.DELETED_FLAG = 0
                if (sWhere != "")
                    sWhere = sWhere + " AND ";
                sWhere = sWhere + "VEHICLE.DELETED_FLAG = 0";

                //run criteria query into temp table
                sSQL = sSelect + " [INTO VEHICLE_TEMP] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;

                ExecSearchSQL(sSQL, ref p_objConn);
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX VEHTEMP_" + m_lUserId + " ON [VEHICLE_TEMP](UNIT_ID)"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[VEHICLE_TEMP]";
                sWhere = "";

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "VEHICLE_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "VEHICLE_SUPP");

                    //Add temp table to final outer joined query VEHICLE.UNIT_ID
                    JoinIt(ref sFrom, ref sWhere, "[VEHICLE_TEMP]", "[VEHSUPP_TEMP]", "UNIT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT VEHICLE_SUPP.UNIT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO VEHSUPP_TEMP]";
                    sSQL = sSQL + " FROM [VEHICLE_TEMP], VEHICLE_SUPP";
                    sSQL = sSQL + " WHERE [VEHICLE_TEMP].UNIT_ID = VEHICLE_SUPP.UNIT_ID ";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX VEHSUPPTEMP_" + m_lUserId + " ON [VEHSUPP_TEMP](UNIT_ID )"));
                }

                // Mihika 16-Feb-2006 Changes related to MITS 4860
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ORG_ENTITY");

                    //Add temp table to final outer joined query VEHICLE.UNIT_ID
                    JoinIt(ref sFrom, ref sWhere, "[VEHICLE_TEMP]", "[ORG_TEMP]", "UNIT_ID");

                    //Dump results into temp table
                    sSQL = "SELECT VEHICLE.UNIT_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO ORG_TEMP]";
                    sSQL = sSQL + " FROM [VEHICLE_TEMP], VEHICLE,ENTITY ORG_ENTITY WHERE [VEHICLE_TEMP].UNIT_ID = VEHICLE.UNIT_ID";
                    sSQL = sSQL + " AND VEHICLE.HOME_DEPT_EID = ORG_ENTITY.ENTITY_ID ";

                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index PATIENT_ID for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ORGTEMP_" + m_lUserId + " ON [ORG_TEMP](UNIT_ID )"));
                }

                //Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[VEHICLE_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "UNIT_ID");

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [VEHICLE_TEMP].UNIT_ID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]";
                        sSQL = sSQL + " FROM [VEHICLE_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                        sSQL = sSQL + " WHERE [VEHICLE_TEMP].UNIT_ID = " + sMultiCodeTableAlias + ".RECORD_ID";
                        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](UNIT_ID)"));
                    }
                }

                //Chain everything together
                //p_sVehicleData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sVehicleData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sVehicleData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoVehicleSearch.GenericError", m_iClientId), p_objException);
            }
        }
        #endregion
        //Anu Tennyson for MITS 18291 : Starts  10/25/2009
        #region Property search
        /// <summary>
        /// This function generates the search data related to Property module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sPropertyData">Property data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        /// <param name="p_iPageSize">The Page Size</param>
        /// <param name="p_iPageNumber">Page Number</param>
        public void DoPropertySearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sPropertyData, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber)
        {
            int iIdx = 0;
            string sFieldId = string.Empty;
            string sFrom = string.Empty;
            string sWhere = string.Empty;
            string sMultiCodeTableAlias = string.Empty;
            bool blnSuccess = false;
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbTmp = new StringBuilder();
            StringBuilder sbFrom = new StringBuilder();
            StringBuilder sbSelect = new StringBuilder();
            StringBuilder sbWhere = new StringBuilder();

            try
            {
                //Delete temp table, if any
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupPropertySearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpPropertySearch");
                }

                sbFrom.Append("PROPERTY_UNIT");
                sbSelect.Append("SELECT PROPERTY_UNIT.PROPERTY_ID ");

                if (Utilities.ExistsInArray(p_arrlstQueryTables, "PROPERTY_UNIT_SUPP"))
                {
                    sbFrom.Append(", PROPERTY_UNIT_SUPP");
                    if (!string.IsNullOrEmpty(sbWhere.ToString()))
                        sbWhere.Append(" AND ");
                    sbWhere.Append("PROPERTY_UNIT.PROPERTY_ID = PROPERTY_UNIT_SUPP.PROPERTY_ID");
                    sWhere = sbWhere.ToString();
                }
                //Supplimental Multi-Code Field
                sFrom = sbFrom.ToString();
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "PROPERTY_UNIT.PROPERTY_ID");

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (!string.IsNullOrEmpty(sbWhere.ToString()))
                        sbWhere.Append(" AND ");
                    sbWhere.Append(p_sCriteria);
                }

                //add on fields
                if (p_sQueryFields.Trim() != "")
                {
                    sbSelect.Append(",");
                    sbSelect.Append(p_sQueryFields);
                }

                // Add  PROPERTY.DELETED_FLAG = 0
                if (!string.IsNullOrEmpty(sbWhere.ToString()))
                    sbWhere.Append(" AND ");
                sbWhere.Append("PROPERTY_UNIT.DELETED_FLAG <> -1");

                //run criteria query into temp table
                sbSql.Append(sbSelect.ToString());
                sbSql.Append(" [INTO PROPERTY_UNIT_TEMP] FROM ");
                sbSql.Append(sFrom);
                sWhere = sbWhere.ToString();
                if (!string.IsNullOrEmpty(sbWhere.ToString()))
                {
                    sbSql.Append(" WHERE ");
                    sbSql.Append(sWhere);
                }

                ExecSearchSQL(sbSql.ToString(), ref p_objConn);
                sbTmp.Append("CREATE INDEX PROPTEMP_");
                sbTmp.Append(m_lUserId);
                sbTmp.Append(" ON [PROPERTY_UNIT_TEMP](PROPERTY_ID)");
                p_objConn.ExecuteNonQuery(PrepareTempSQL(sbTmp.ToString()));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[PROPERTY_UNIT_TEMP]";
                sWhere = "";

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "PROPERTY_UNIT_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "PROPERTY_UNIT_SUPP");

                    //Add temp table to final outer joined query PROPERTY.UNIT_ID
                    JoinIt(ref sFrom, ref sWhere, "[PROPERTY_UNIT_TEMP]", "[PROPSUPP_TEMP]", "PROPERTY_ID");

                    //Dump results into temp table
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT PROPERTY_UNIT_SUPP.PROPERTY_ID,");
                    sbSql.Append(p_arrlstDisplayFields[iIdx].ToString());
                    sbSql.Append(" [INTO PROPSUPP_TEMP] FROM [PROPERTY_UNIT_TEMP], PROPERTY_UNIT_SUPP WHERE [PROPERTY_UNIT_TEMP].PROPERTY_ID = PROPERTY_UNIT_SUPP.PROPERTY_ID");
                    ExecSearchSQL(sbSql.ToString(), ref p_objConn);

                    //index PATIENT_ID for quick processing
                    sbTmp = new StringBuilder();
                    sbTmp.Append("CREATE INDEX PROPSUPPTEMP_");
                    sbTmp.Append(m_lUserId);
                    sbTmp.Append(" ON [PROPSUPP_TEMP](PROPERTY_ID )");
                    p_objConn.ExecuteNonQuery(PrepareTempSQL(sbTmp.ToString()));
                }

                //Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        //lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        //Conversion.CastToType<bool>(p_objXMLDoc.SelectSingleNode("//Param[@name='Authenticated']").InnerText, out blnSuccess); 
                        //sFieldId = Conversion.CastToType<long>(p_arrlstDisplayTables[iIdx].ToString().Substring(20), blnSuccess);
                        sFieldId = p_arrlstDisplayTables[iIdx].ToString().Substring(20);
                        sMultiCodeTableAlias = "SMV" + sFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[PROPERTY_UNIT_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "PROPERTY_ID");

                        sbTmp = new StringBuilder();
                        sbTmp.Append("[");
                        sbTmp.Append(sMultiCodeTableAlias);
                        sbTmp.Append("_TEMP]");
                        DropTable(sbTmp.ToString(), "", ref p_objConn);

                        //Dump results into temp table
                        sbSql = new StringBuilder();
                        sbSql.Append("SELECT [PROPERTY_UNIT_TEMP].PROPERTY_ID, ");
                        sbSql.Append(p_arrlstDisplayFields[iIdx].ToString());
                        sbSql.Append(" [INTO SMV");
                        sbSql.Append(sFieldId);
                        sbSql.Append("_TEMP]");
                        sbSql.Append(" FROM [PROPERTY_UNIT_TEMP], SUPP_MULTI_VALUE ");
                        sbSql.Append(sMultiCodeTableAlias);
                        sbSql.Append(" WHERE [PROPERTY_UNIT_TEMP].UNIT_ID = ");
                        sbSql.Append(sMultiCodeTableAlias);
                        sbSql.Append(".RECORD_ID AND ");
                        sbSql.Append(sMultiCodeTableAlias);
                        sbSql.Append(".FIELD_ID = ");
                        sbSql.Append(sFieldId);
                        ExecSearchSQL(sbSql.ToString(), ref p_objConn);

                        //Create Index on the temp table
                        sbTmp = new StringBuilder();
                        sbTmp.Append("CREATE INDEX SMV");
                        sbTmp.Append(sFieldId);
                        sbTmp.Append("TMP_");
                        sbTmp.Append(m_lUserId);
                        sbTmp.Append(" ON [SMV");
                        sbTmp.Append(sFieldId);
                        sbTmp.Append("_TEMP](PROPERTY_ID)");
                        p_objConn.ExecuteNonQuery(PrepareTempSQL(sbTmp.ToString()));
                    }
                }

                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sPropertyData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sPropertyData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoPropertySearch.GenericError", m_iClientId), p_objException);
            }
            finally
            {
                sbSql = null;
                sbTmp = null;
                sbFrom = null;
                sbSelect = null;
                sbWhere = null;
            }

        }

        #endregion

        //Anu Tennyson for MITS 18291 : ENDS

        //skhare7
        #region Diary Search
        /// <summary>
        /// This function generates the search data related to Diary module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sPropertyData">Property data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        /// <param name="p_iPageSize">The Page Size</param>
        /// <param name="p_iPageNumber">Page Number</param>
        ///  /// <param name="p_sAttachedRecord">Atatched Record Table</param>
        public void DoWPADiarySearch(string p_sOrderBy, string p_sAttachedRecord, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sPropertyData, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber)
        {
            int iIdx = 0;
            string sFieldId = string.Empty;
            string sFrom = string.Empty;
            string sWhere = string.Empty;
            string sMultiCodeTableAlias = string.Empty;
            bool blnSuccess = false;
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbTmp = new StringBuilder();
            StringBuilder sbFrom = new StringBuilder();
            StringBuilder sbSelect = new StringBuilder();
            StringBuilder sbWhere = new StringBuilder();
            LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId); //jira 2595

            try
            {
                //Delete temp table, if any
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupDiarySearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanupDiarySearch");
                }

                sbFrom.Append("WPA_DIARY_ENTRY  ");

                sbSelect.Append("SELECT WPA_DIARY_ENTRY.ENTRY_ID ,ATTACH_RECORDID ");
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "WPA_DIARY_ACT"))
                {

                    sbFrom.Append(" ,WPA_DIARY_ACT  ");
                    if (!string.IsNullOrEmpty(sbWhere.ToString()))
                    { sbWhere.Append(" AND "); }
                    sbWhere.Append(" WPA_DIARY_ENTRY.ENTRY_ID=WPA_DIARY_ACT.PARENT_ENTRY_ID");

                }


                if ((p_sAttachedRecord.ToUpper().Contains("CLAIM_NUMBER")))
                {
                    sbFrom.Append(", CLAIM ATTACH_RECORDTABLE");
                    if (!string.IsNullOrEmpty(sbWhere.ToString()))
                    { sbWhere.Append(" AND "); }
                    sbWhere.Append(" WPA_DIARY_ENTRY.ATTACH_RECORDID=ATTACH_RECORDTABLE.CLAIM_ID ");
                    sbWhere.Append(" AND ");

                    sbWhere.Append(" ATTACH_TABLE = 'CLAIM' AND ATTACH_RECORDID<>0");
                }

                //pkandhari Jira 4691 Insert Diary Supplement in Query Designer starts
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "WPA_DIARY_ENTRY_SUPP"))
                {
                    sbFrom.Append(", WPA_DIARY_ENTRY_SUPP ");
                    if (!string.IsNullOrEmpty(sbWhere.ToString()))
                    { sbWhere.Append(" AND "); }
                    sbWhere.Append(" WPA_DIARY_ENTRY.ENTRY_ID=WPA_DIARY_ENTRY_SUPP.ENTRY_ID");
                }

                sFrom = sbFrom.ToString();
                sWhere = sbWhere.ToString();
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "WPA_DIARY_ENTRY.ENTRY_ID");
                sbFrom = new StringBuilder();
                sbFrom.Append(sFrom);
                sbWhere = new StringBuilder();
                sbWhere.Append(sWhere);
                //pkandhari Jira 4691 Insert Diary Supplement in Query Designer ends

                if ((p_sAttachedRecord.ToUpper().Contains("EVENT_NUMBER")))
                {
                    sbFrom.Append(", EVENT ATTACH_RECORDTABLE");
                    if (!string.IsNullOrEmpty(sbWhere.ToString()))
                    { sbWhere.Append(" AND "); }
                    sbWhere.Append(" WPA_DIARY_ENTRY.ATTACH_RECORDID=ATTACH_RECORDTABLE.EVENT_ID ");
                    sbWhere.Append(" AND ");

                    sbWhere.Append(" ATTACH_TABLE = 'EVENT' AND ATTACH_RECORDID<>0");
                }
                if ((p_sAttachedRecord.ToUpper().Contains("CTL_NUMBER")))
                {
                    sbFrom.Append(", FUNDS ATTACH_RECORDTABLE");
                    if (!string.IsNullOrEmpty(sbWhere.ToString()))
                    {
                        sbWhere.Append(" AND ");
                    }
                    sbWhere.Append(" WPA_DIARY_ENTRY.ATTACH_RECORDID=ATTACH_RECORDTABLE.TRANS_ID ");
                    sbWhere.Append(" AND ");

                    sbWhere.Append(" ATTACH_TABLE = 'FUNDS' AND ATTACH_RECORDID<>0");
                }
                if ((p_sAttachedRecord.ToUpper().Contains("POLICY_NUMBER")))
                {

                    sbFrom.Append(", POLICY ATTACH_RECORDTABLE");
                    if (!string.IsNullOrEmpty(sbWhere.ToString()))
                    {
                        sbWhere.Append(" AND ");
                    }
                    sbWhere.Append(" WPA_DIARY_ENTRY.ATTACH_RECORDID=ATTACH_RECORDTABLE.POLICY_ID ");
                    sbWhere.Append(" AND ");

                    sbWhere.Append(" ATTACH_TABLE = 'POLICY' AND ATTACH_RECORDID<>0");


                }
                if ((p_sAttachedRecord.ToUpper().Contains("POLICY_NAME")))
                {
                    sbFrom.Append(", POLICY_ENH ATTACH_RECORDTABLE");
                    if (!string.IsNullOrEmpty(sbWhere.ToString()))
                    {
                        sbWhere.Append(" AND ");
                    }
                    sbWhere.Append(" WPA_DIARY_ENTRY.ATTACH_RECORDID=ATTACH_RECORDTABLE.POLICY_ID ");
                    sbWhere.Append(" AND ");

                    sbWhere.Append(" ATTACH_TABLE = 'POLICY_ENH' AND ATTACH_RECORDID<>0");
                }


                if (p_sAttachedRecord.ToUpper().Contains("LAST_NAME"))
                {
                    if (Utilities.ExistsInArray(p_arrlstQueryTables, "CLAIMANT"))
                    {
                        sbFrom.Append(", CLAIMANT ");

                        sbFrom.Append(",  ENTITY ATTACH_RECORDTABLE");
                        if (!string.IsNullOrEmpty(sbWhere.ToString()))
                        {
                            sbWhere.Append(" AND ");
                        }
                        sbWhere.Append(" WPA_DIARY_ENTRY.ATTACH_RECORDID=CLAIMANT.CLAIMANT_ROW_ID ");
                        sbWhere.Append(" AND ");
                        sbWhere.Append("CLAIMANT.CLAIMANT_EID = ATTACH_RECORDTABLE.ENTITY_ID");
                        sbWhere.Append(" AND ");
                        sbWhere.Append(" ATTACH_TABLE = 'CLAIMANT' AND ATTACH_RECORDID<>0 ");

                    }
                    else if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY"))
                    {
                        sbFrom.Append(", ENTITY ATTACH_RECORDTABLE");


                        if (!string.IsNullOrEmpty(sbWhere.ToString()))
                        {
                            sbWhere.Append(" AND ");
                        }
                        sbWhere.Append(" WPA_DIARY_ENTRY.ATTACH_RECORDID=ATTACH_RECORDTABLE.ENTITY_ID ");

                        sbWhere.Append(" AND ");
                        sbWhere.Append(" ATTACH_TABLE = 'ENTITY' AND ATTACH_RECORDID<>0");



                    }
                    else if (Utilities.ExistsInArray(p_arrlstQueryTables, "PERSON_INVOLVED"))
                    {
                        sbFrom.Append(", PERSON_INVOLVED ");
                        sbFrom.Append(",  ENTITY ATTACH_RECORDTABLE");


                        if (!string.IsNullOrEmpty(sbWhere.ToString()))
                        {
                            sbWhere.Append(" AND ");
                        }
                        sbWhere.Append(" WPA_DIARY_ENTRY.ATTACH_RECORDID=PERSON_INVOLVED.PI_ROW_ID ");

                        sbWhere.Append(" AND ");
                        sbWhere.Append("PERSON_INVOLVED.PI_EID = ATTACH_RECORDTABLE.ENTITY_ID");
                        sbWhere.Append(" AND ");

                        sbWhere.Append(" ATTACH_RECORDID<>0");



                    }
                }


                sWhere = sbWhere.ToString();
                //  }

                sFrom = sbFrom.ToString();

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (!string.IsNullOrEmpty(sbWhere.ToString()))
                        sbWhere.Append(" AND ");
                    sbWhere.Append(p_sCriteria);
                }
                //igupta3 jira 439
                if (p_sQueryFields.Trim() != "")
                {
                    int iLangCode = Convert.ToInt32(DbFactory.ExecuteScalar(Security.SecurityDatabase.Dsn, "Select NLS_CODE from USER_TABLE where USER_ID= " + m_lUserId));
                    if (p_sQueryFields.Contains("WPA_DIARY_ENTRY.NON_ROLL_FLAG"))
                    {
                        p_sQueryFields = p_sQueryFields.Replace("WPA_DIARY_ENTRY.NON_ROLL_FLAG", "CASE WPA_DIARY_ENTRY.NON_ROLL_FLAG WHEN -1 THEN '" + objCache.GetCodeDesc(objCache.GetCodeId("Y", "YES_NO"), iLangCode) + "' ELSE '" + objCache.GetCodeDesc(objCache.GetCodeId("N", "YES_NO"), iLangCode) + "' END ");
                    }
                    if (p_sQueryFields.Contains("WPA_DIARY_ENTRY.NON_ROUTE_FLAG"))
                    {
                        p_sQueryFields = p_sQueryFields.Replace("WPA_DIARY_ENTRY.NON_ROUTE_FLAG", "CASE WPA_DIARY_ENTRY.NON_ROUTE_FLAG  WHEN -1 THEN '" + objCache.GetCodeDesc(objCache.GetCodeId("Y", "YES_NO"), iLangCode) + "' ELSE '" + objCache.GetCodeDesc(objCache.GetCodeId("N", "YES_NO"), iLangCode) + "' END ");
                    }
                }

                //add on fields
                if (p_sQueryFields.Trim() != "")
                {
                    sbSelect.Append(",");
                    sbSelect.Append(p_sQueryFields);
                }

                // Add  PROPERTY.DELETED_FLAG = 0
                if (!string.IsNullOrEmpty(sbWhere.ToString()))
                    sbWhere.Append(" AND ");
                sbWhere.Append("WPA_DIARY_ENTRY.DIARY_DELETED <> 1");

                //run criteria query into temp table
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ATTACH_RECORDTABLE"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "ATTACH_RECORDTABLE");
                    p_arrlstDisplayFields[iIdx] = p_arrlstDisplayFields[iIdx].ToString().Replace("ATTACH_RECORDTABLE.ATTACH_RECORD", "WPA_DIARY_ENTRY.ATTACH_TABLE");
                    sbSelect = sbSelect.Append(" , " + p_arrlstDisplayFields[iIdx].ToString());
                    AttachedRecordColumn = p_arrlstDisplayFields[iIdx].ToString();
                }
                sbSql.Append(sbSelect.ToString());
                sbSql.Append(" [INTO DIARY_TEMP] FROM ");
                sbSql.Append(sFrom);
                sWhere = sbWhere.ToString();
                if (!string.IsNullOrEmpty(sbWhere.ToString()))
                {
                    sbSql.Append(" WHERE ");
                    sbSql.Append(sWhere);
                }

                ExecSearchSQL(sbSql.ToString(), ref p_objConn);
                sbTmp.Append("CREATE INDEX DIARYTEMP_");
                sbTmp.Append(m_lUserId);
                sbTmp.Append(" ON [DIARY_TEMP](ENTRY_ID)");
                p_objConn.ExecuteNonQuery(PrepareTempSQL(sbTmp.ToString()));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[DIARY_TEMP]";
                sWhere = "";

                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "WPA_DIARY_ACT"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "WPA_DIARY_ACT");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[DIARY_TEMP]", "[DIARYACT_TEMP]", "ENTRY_ID", "PARENT_ENTRY_ID");
                    sbSql = new StringBuilder();
                    //Dump results into temp table
                    sbSql = sbSql.Append("SELECT WPA_DIARY_ACT.PARENT_ENTRY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO DIARYACT_TEMP]");
                    sbSql = sbSql.Append(" FROM [DIARY_TEMP], WPA_DIARY_ACT");
                    sbSql = sbSql.Append(" WHERE [DIARY_TEMP].ENTRY_ID = WPA_DIARY_ACT.PARENT_ENTRY_ID");
                    ExecSearchSQL(sbSql.ToString(), ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DIARYACTPTEMP_" + m_lUserId + " ON [DIARYACT_TEMP](PARENT_ENTRY_ID)"));
                }

                //pkandhari Jira 4691 Insert Diary Supplement in Query Designer starts
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "WPA_DIARY_ENTRY_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "WPA_DIARY_ENTRY_SUPP");
                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[DIARY_TEMP]", "[DIARYSUPP_TEMP]", "ENTRY_ID");
                    sbSql = new StringBuilder();
                    //Dump results into temp table
                    sbSql.Append("SELECT WPA_DIARY_ENTRY_SUPP.ENTRY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO DIARYSUPP_TEMP]");
                    sbSql.Append(" FROM [DIARY_TEMP], WPA_DIARY_ENTRY_SUPP");
                    sbSql.Append(" WHERE [DIARY_TEMP].ENTRY_ID = WPA_DIARY_ENTRY_SUPP.ENTRY_ID ");
                    ExecSearchSQL(sbSql.ToString(), ref p_objConn);
                    
                    //index ENTRY_ID  for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DIARYSUPPTEMP_" + m_lUserId + " ON [DIARYSUPP_TEMP](ENTRY_ID)"));
                }
                //pkandhari Jira 4691 Insert Diary Supplement in Query Designer ends
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sPropertyData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sPropertyData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.WPADiarySearch.GenericError", m_iClientId), p_objException);
            }
            finally
            {
                sbSql = null;
                sbTmp = null;
                sbFrom = null;
                sbSelect = null;
                sbWhere = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }

        }

        #endregion

        //skhare7 : ENDS
        #region Admin Tracking search
        /// <summary>
        /// This function generates the search data related to Admin tracking module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sSysKeyField">Contains System key field</param>
        /// <param name="p_sATData">Admin tracking data</param>
        /// <param name="p_objConn">Connection object as reference as logical temp tables are made for a single connection session</param>
        public void DoATSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, string p_sSysKeyField, ref string p_sATData, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber, ref string p_sRetSQL)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            string sTempTable = "";
            string sMultiCodeTableAlias = "";
            int iIdx = 0;
            long lFieldId = 0;
            int iEntAliasCount = 1;

            try
            {
                sTempTable = p_arrlstQueryTables[0].ToString() + "_TEMP";
                //06/20/2006 Raman Bhatia: Table not being dropped correctly

                //DropTable("[" + sTempTable + "_TEMP]", "", ref p_objConn);
                DropTable("[" + sTempTable + "]", "", ref p_objConn);

                //Build criteria query
                sFrom = p_arrlstQueryTables[0].ToString();
                sSelect = "SELECT " + p_sSysKeyField;

                //Supplimental Multi-Code Field
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, p_sSysKeyField);

                //Geeta 06/17/08 : Mits 12643
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY"))
                {
                    while (iEntAliasCount <= p_arrlstQueryTables.Count)
                    {
                        if (Utilities.ExistsInArray(p_arrlstQueryTables, "ENTITY" + iEntAliasCount))
                        {
                            sFrom = sFrom + ",ENTITY " + ("ENTITY" + iEntAliasCount);
                        }

                        iEntAliasCount = iEntAliasCount + 1;
                    }
                }

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }

                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;

                //run criteria query into temp table
                sSQL = sSelect + " [INTO " + sTempTable + "] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;

                //run criteria query into temp table
                ExecSearchSQL(sSQL, ref p_objConn);
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX ATTEMP_" + m_lUserId + " ON [" + sTempTable + "](" + p_sSysKeyField + ")"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[" + sTempTable + "]";
                sWhere = "";

                // Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[" + sTempTable + "]", "[" + sMultiCodeTableAlias + "_TEMP]", p_sSysKeyField);

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [" + sTempTable + "]." + p_sSysKeyField + ", " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]";
                        sSQL = sSQL + " FROM [" + sTempTable + "], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                        sSQL = sSQL + " WHERE [" + sTempTable + "]." + p_sSysKeyField + " = " + sMultiCodeTableAlias + ".RECORD_ID";
                        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](" + p_sSysKeyField + ")"));
                    }
                }
                //Chain everything together
                //bkumar33 Mits 15604
                //p_sATData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);
                if (!m_bPrintSearch)
                {
                    p_sATData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sATData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);
                }
                //bkumar33 End Mits 15604
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoATSearch.GenericError", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Disability Plan search
        /// <summary>
        /// This function generates the search data related to Disability Plan module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sDisPlanData">Disability Plan data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        public void DoDisPlanSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sDisPlanData, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            int iIdx = 0;
            int iIdxInsured = 0;
            long lFieldId = 0;
            string sMultiCodeTableAlias = "";

            try
            {
                //Delete temp table, if any
                //if (m_sDBType != Constants.DB_ORACLE)
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupDisPlanSearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpDisPlanSearch");
                }

                //Build criteria query
                sFrom = "DISABILITY_PLAN";
                sSelect = "SELECT DISABILITY_PLAN.PLAN_ID ";

                //Classes
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "DISABILITY_CLASS") || Utilities.ExistsInArray(p_arrlstQueryTables, "DIS_CLASS_TD"))
                {
                    sFrom = sFrom + ", DISABILITY_CLASS";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "DISABILITY_PLAN.PLAN_ID = DISABILITY_CLASS.PLAN_ID";
                }

                //Insured
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "DIS_PLAN_X_INSURED") || Utilities.ExistsInArray(p_arrlstQueryTables, "INSURED_ENTITY"))
                {
                    sFrom = sFrom + ", DIS_PLAN_X_INSURED, ENTITY INSURED_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "DISABILITY_PLAN.PLAN_ID = DIS_PLAN_X_INSURED.PLAN_ID AND DIS_PLAN_X_INSURED.INSURED_EID = INSURED_ENTITY.ENTITY_ID";
                }

                //Class Table Driven
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "DIS_CLASS_TD"))
                {
                    sFrom = sFrom + ", DIS_CLASS_TD";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "DISABILITY_CLASS.CLASS_ROW_ID = DIS_CLASS_TD.CLASS_ID";
                }

                //Supplementals
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "DIS_PLAN_SUPP"))
                {
                    sFrom = sFrom + ", DIS_PLAN_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "DISABILITY_PLAN.PLAN_ID = DIS_PLAN_SUPP.PLAN_ID";
                }

                //Supplimental Multi-Code Field
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "DISABILITY_PLAN.PLAN_ID");

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }

                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;

                //run criteria query into temp table
                sSQL = sSelect + " [INTO PLAN_TEMP] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;

                ExecSearchSQL(sSQL, ref p_objConn);

                //index for quick processing in display only fields
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PLANTEMP_" + m_lUserId + " ON [PLAN_TEMP](PLAN_ID)"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[PLAN_TEMP]";
                sWhere = "";

                //Insured
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "DIS_PLAN_X_INSURED") || Utilities.ExistsInArray(p_arrlstDisplayTables, "INSURED_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "DIS_PLAN_X_INSURED");
                    iIdxInsured = Utilities.IndexInArray(p_arrlstDisplayTables, "INSURED_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PLAN_TEMP]", "[INSURED_TEMP]", "PLAN_ID");

                    //Dump results into temp table
                    sSQL = "SELECT DIS_PLAN_X_INSURED.PLAN_ID";
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    // Mihika 16-Feb-2006 Corrected the index
                    if (iIdxInsured > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxInsured].ToString());

                    sSQL = sSQL + " [INTO INSURED_TEMP]";
                    sSQL = sSQL + " FROM [PLAN_TEMP], DIS_PLAN_X_INSURED, ENTITY INSURED_ENTITY";
                    sSQL = sSQL + " WHERE [PLAN_TEMP].PLAN_ID = DIS_PLAN_X_INSURED.PLAN_ID AND DIS_PLAN_X_INSURED.INSURED_EID = INSURED_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INSDTEMP_" + m_lUserId + " ON [INSURED_TEMP](PLAN_ID)"));
                }

                //Classes
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "DISABILITY_CLASS"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "DISABILITY_CLASS");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PLAN_TEMP]", "[CLASS_TEMP]", "PLAN_ID");

                    //Dump results into temp table
                    sSQL = "SELECT DISABILITY_CLASS.PLAN_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CLASS_TEMP]";
                    sSQL = sSQL + " FROM [PLAN_TEMP], DISABILITY_CLASS";
                    sSQL = sSQL + " WHERE [PLAN_TEMP].PLAN_ID = DISABILITY_CLASS.PLAN_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CLASSTEMP_" + m_lUserId + " ON [CLASS_TEMP](PLAN_ID)"));
                }

                //Class Table Driven
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "DIS_CLASS_TD"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "DIS_CLASS_TD");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PLAN_TEMP]", "[TD_TEMP]", "PLAN_ID");

                    //Dump results into temp table
                    sSQL = "SELECT DISABILITY_CLASS.PLAN_ID, " + p_arrlstDisplayFields[iIdx].ToString();
                    sSQL = sSQL + " [INTO TD_TEMP]";
                    sSQL = sSQL + " FROM [PLAN_TEMP], DISABILITY_CLASS, DIS_CLASS_TD ";
                    sSQL = sSQL + " WHERE [PLAN_TEMP].PLAN_ID = DISABILITY_CLASS.PLAN_ID";
                    sSQL = sSQL + " AND DISABILITY_CLASS.CLASS_ROW_ID = DIS_CLASS_TD.CLASS_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX TDTEMP_" + m_lUserId + " ON [TD_TEMP](PLAN_ID)"));
                }

                //Supplemental
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "DIS_PLAN_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "DIS_PLAN_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[PLAN_TEMP]", "[PLANSUPP_TEMP]", "PLAN_ID");

                    //Dump results into temp table
                    sSQL = "SELECT DIS_PLAN_SUPP.PLAN_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO PLANSUPP_TEMP]";
                    sSQL = sSQL + " FROM [PLAN_TEMP], DIS_PLAN_SUPP";
                    sSQL = sSQL + " WHERE [PLAN_TEMP].PLAN_ID = DIS_PLAN_SUPP.PLAN_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX PLANSPTEMP_" + m_lUserId + " ON [PLANSUPP_TEMP](PLAN_ID)"));
                }

                //MULTI-CODE Type Supplementals
                //Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        // Mihika 16-Feb-2006 Corrected the substring index
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[PLAN_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "PLAN_ID");

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [PLAN_TEMP].PLAN_ID, " + p_arrlstDisplayTables[iIdx] + " [INTO SMV" + lFieldId + "_TEMP]";
                        sSQL = sSQL + " FROM [PLAN_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                        sSQL = sSQL + " WHERE [PLAN_TEMP].PLAN_ID = " + sMultiCodeTableAlias + ".RECORD_ID";
                        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](PLAN_ID)"));
                    }
                }

                //Chain everything together
                sSQL = "SELECT " + p_sJustFieldNames;
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sDisPlanData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sDisPlanData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }
                //p_sDisPlanData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoDisPlanSearch.GenericError", m_iClientId), p_objException);
            }
        }
        #endregion

        //Shruti Leave Plan Search starts
        /// <summary>
        /// This function generates the search data related to Leave Plan module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sDisPlanData">Leave Plan data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        public void DoLeavePlanSearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, ref string p_sRetSQL, ref string p_sDisPlanData, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            int iIdx = 0;
            int iIdxInsured = 0;
            long lFieldId = 0;
            string sMultiCodeTableAlias = "";

            try
            {
                //Delete temp table, if any
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupLeavePlanSearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpLeavePlanSearch");
                }

                //Build criteria query
                sFrom = "LEAVE_PLAN";
                sSelect = "SELECT LEAVE_PLAN.LP_ROW_ID ";

                //Insured
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "LP_X_ENTITY") || Utilities.ExistsInArray(p_arrlstQueryTables, "LP_INSURED_ENTITY"))
                {
                    sFrom = sFrom + ", LP_X_ENTITY, ENTITY LP_INSURED_ENTITY";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "LEAVE_PLAN.LP_ROW_ID = LP_X_ENTITY.LP_ROW_ID AND LP_X_ENTITY.ENTITY_EID = LP_INSURED_ENTITY.ENTITY_ID";
                }

                //Supplementals
                if (Utilities.ExistsInArray(p_arrlstQueryTables, "LEAVE_PLAN_SUPP"))
                {
                    sFrom = sFrom + ", LEAVE_PLAN_SUPP";
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + "LEAVE_PLAN.LP_ROW_ID = LEAVE_PLAN_SUPP.LP_ROW_ID";
                }

                //Supplimental Multi-Code Field
                CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "LEAVE_PLAN.LP_ROW_ID");

                //add on actual criteria
                if (p_sCriteria.Trim() != "")
                {
                    if (sWhere != "")
                        sWhere = sWhere + " AND ";
                    sWhere = sWhere + p_sCriteria;
                }

                //add on fields
                if (p_sQueryFields.Trim() != "")
                    sSelect = sSelect + "," + p_sQueryFields;

                //run criteria query into temp table
                sSQL = sSelect + " [INTO LP_PLAN_TEMP] FROM " + sFrom + " ";
                if (sWhere != "")
                    sSQL = sSQL + " WHERE " + sWhere;

                ExecSearchSQL(sSQL, ref p_objConn);

                //index for quick processing in display only fields
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LPPLANTEMP_" + m_lUserId + " ON [LP_PLAN_TEMP](LP_ROW_ID)"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[LP_PLAN_TEMP]";
                sWhere = "";

                //Insured
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "LP_X_ENTITY") || Utilities.ExistsInArray(p_arrlstDisplayTables, "LP_INSURED_ENTITY"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "LP_X_ENTITY");
                    iIdxInsured = Utilities.IndexInArray(p_arrlstDisplayTables, "LP_INSURED_ENTITY");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[LP_PLAN_TEMP]", "[LP_INSURED_TEMP]", "LP_ROW_ID");

                    //Dump results into temp table
                    sSQL = "SELECT LP_X_ENTITY.LP_ROW_ID";
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    // Mihika 16-Feb-2006 Corrected the index
                    if (iIdxInsured > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxInsured].ToString());

                    sSQL = sSQL + " [INTO LP_INSURED_TEMP]";
                    sSQL = sSQL + " FROM [LP_PLAN_TEMP], LP_X_ENTITY, ENTITY LP_INSURED_ENTITY";
                    sSQL = sSQL + " WHERE [LP_PLAN_TEMP].LP_ROW_ID = LP_X_ENTITY.LP_ROW_ID AND LP_X_ENTITY.ENTITY_EID = LP_INSURED_ENTITY.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LPINSDTEMP_" + m_lUserId + " ON [LP_INSURED_TEMP](LP_ROW_ID)"));
                }

                //Supplemental
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "LEAVE_PLAN_SUPP"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "LEAVE_PLAN_SUPP");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[LP_PLAN_TEMP]", "[LP_PLANSUPP_TEMP]", "LP_ROW_ID");

                    //Dump results into temp table
                    sSQL = "SELECT LEAVE_PLAN_SUPP.LP_ROW_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO LP_PLANSUPP_TEMP]";
                    sSQL = sSQL + " FROM [LP_PLAN_TEMP], LEAVE_PLAN_SUPP";
                    sSQL = sSQL + " WHERE [LP_PLAN_TEMP].LP_ROW_ID = LEAVE_PLAN_SUPP.LP_ROW_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX LPPLANSPTEMP_" + m_lUserId + " ON [LP_PLANSUPP_TEMP](LP_ROW_ID)"));
                }

                //MULTI-CODE Type Supplementals
                //Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[LP_PLAN_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "LP_ROW_ID");

                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [LP_PLAN_TEMP].LP_ROW_ID, " + p_arrlstDisplayTables[iIdx] + " [INTO SMV" + lFieldId + "_TEMP]";
                        sSQL = sSQL + " FROM [LP_PLAN_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                        sSQL = sSQL + " WHERE [LP_PLAN_TEMP].LP_ROW_ID = " + sMultiCodeTableAlias + ".RECORD_ID";
                        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](LP_ROW_ID)"));
                    }
                }

                //Chain everything together
                sSQL = "SELECT " + p_sJustFieldNames;
                //p_sDisPlanData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sDisPlanData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sDisPlanData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }

            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoLeavePlanSearch.GenericError", m_iClientId), p_objException);
            }
        }
        //Shruti Leave Plan Search starts
        // Start Naresh Enhanced Policy Search
        #region Enhanced Policy search
        /// Name		: DoENHPolicySearch
        /// Author		: Naresh Chandra Padhy
        /// Date Created: 01/23/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function generates the search data related to Enhanced Policy module for the given search criteria.
        /// </summary>
        /// <param name="p_sOrderBy">Appends order by clause with the column name in the SQL query</param>
        /// <param name="p_arrlstQueryTables">Contains query tables</param>
        /// <param name="p_sQueryFields">Contains query fields</param>
        /// <param name="p_arrlstDisplayTables">Contains display tables</param>
        /// <param name="p_arrlstDisplayFields">Contains display fields</param>
        /// <param name="p_sCriteria">Contains query criteria</param>
        /// <param name="p_sJustFieldNames">Contains field names</param>
        /// <param name="p_lPolicyRestrict">Restricts the number of policy to be displayed</param>
        /// <param name="p_sRetSQL">Contains the SQL statement to used to to drop temp tables</param>
        /// <param name="p_sPolicyData">Policy data</param>
        /// <param name="p_objConn">Connection object as reference, as logical temp tables are made for a single connection session</param>
        /// <param name="p_iPageSize">The Page Size</param>
        /// <param name="p_iPageNumber">Page Number</param>
        public void DoENHPolicySearch(string p_sOrderBy, ArrayList p_arrlstQueryTables, string p_sQueryFields, ArrayList p_arrlstDisplayTables, ArrayList p_arrlstDisplayFields, string p_sCriteria, string p_sJustFieldNames, long p_lPolicyRestrict, ref string p_sRetSQL, ref string p_sEnhPolicyData, ref DbConnection p_objConn, int p_iPageSize, int p_iPageNumber, string p_sPolicyRestrict)
        {
            string sSQL = "";
            string sFrom = "";
            string sWhere = "";
            string sSelect = "";
            string sMultiCodeTableAlias = "";
            int iIdx = 0;
            int iIdxInsured = 0;
            long lFieldId = 0;
            //Anu Tennyson for VACo - To ahow the result according to the LOB for which the enhance policy is enabled Starts
            string sLob = string.Empty;
            //Anu Tennyson for VACo - To ahow the result according to the LOB for which the enhance policy is enabled Ends

            try
            {
                //Delete temp table, if any
                //if (m_sDBType != Constants.DB_ORACLE)
                if (m_sDBType != Constants.DB_SQLSRVR)
                {
                    CleanupEnhPolicySearch("", ref p_objConn);
                }
                else
                {
                    CleanupSearchStoredProc(ref p_objConn, "CleanUpEnhPolicySearch");
                }
                // Missing P and Q part
                LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId);
                int iPolicyId = objCache.GetCodeId("P", "POLICY_INDICATOR");
                int iQuoteId = objCache.GetCodeId("Q", "POLICY_INDICATOR");

                if (p_sPolicyRestrict == "P")
                {
                    if (!p_sCriteria.Contains("POLICY_ENH.POLICY_INDICATOR"))
                    {
                        if (p_sCriteria.Trim() == string.Empty)
                        {
                            p_sCriteria = p_sCriteria + " POLICY_ENH.POLICY_INDICATOR = " + iPolicyId;
                        }
                        else
                        {
                            p_sCriteria = p_sCriteria + " AND POLICY_ENH.POLICY_INDICATOR = " + iPolicyId;
                        }
                    }
                }
                else if (p_sPolicyRestrict == "Q")
                {
                    if (!p_sCriteria.Contains("POLICY_ENH.POLICY_INDICATOR"))
                    {
                        if (p_sCriteria.Trim() == string.Empty)
                        {
                            p_sCriteria = p_sCriteria + " POLICY_ENH.POLICY_INDICATOR = " + iQuoteId;
                        }
                        else
                        {
                            p_sCriteria = p_sCriteria + " AND POLICY_ENH.POLICY_INDICATOR = " + iQuoteId;
                        }
                    }
                }

                //================================================================================

                //Anu Tennyson for VACo - To ahow the result according to the LOB for which the enhance policy is enabled Starts
                sLob = this.CheckPolicyTypeEnabled();
                //Anu Tennyson for VACo - To ahow the result according to the LOB for which the enhance policy is enabled Ends
                for (int iCount = 0; iCount < 2; iCount++)
                {
                    //Build criteria query
                    sFrom = "POLICY_ENH, POLICY_X_TERM_ENH";
                    sWhere = "POLICY_ENH.POLICY_ID = POLICY_X_TERM_ENH.POLICY_ID AND ";

                    if (iCount == 0)
                        sWhere = sWhere + " POLICY_INDICATOR = " + iQuoteId + " AND ";
                    else
                        sWhere = sWhere + " POLICY_INDICATOR = " + iPolicyId + " AND ";
                    //Anu Tennyson for VACo - To ahow the result according to the LOB for which the enhance policy is enabled Starts
                    if (sLob != "")
                    {
                        sWhere = sWhere + "POLICY_X_TERM_ENH.TERM_ID = (SELECT MAX(TERM_ID) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = POLICY_ENH.POLICY_ID) AND POLICY_ENH.POLICY_TYPE IN (" + sLob + ")";
                    }
                    else
                    {
                        sWhere = sWhere + "POLICY_X_TERM_ENH.TERM_ID = (SELECT MAX(TERM_ID) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = POLICY_ENH.POLICY_ID)";
                    }
                    //Anu Tennyson for VACo - To ahow the result according to the LOB for which the enhance policy is enabled Ends
                    sSelect = "SELECT POLICY_ENH.POLICY_ID ";

                    if (p_lPolicyRestrict > 0)
                        sWhere = sWhere + " POLICY_ENH.PRIMARY_POLICY_FLG = 0 AND POLICY_ENH.POLICY_ID <> 0 AND POLICY_ENH.POLICY_ID <> " + p_lPolicyRestrict;

                    // To Get POLICY_STATUS from POLICY_X_TRANS_ENH table but not from POlICY_ENH table because it contains the accurate policy status
                    if (iCount == 1)
                    {
                        sFrom = sFrom + ",POLICY_X_TRANS_ENH";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "POLICY_X_TRANS_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_TRANS_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID) AND";
                        sWhere = sWhere + " POLICY_ENH.POLICY_ID = POLICY_X_TRANS_ENH.POLICY_ID";
                    }
                    //insurer
                    if (Utilities.ExistsInArray(p_arrlstQueryTables, "INSURER_ENTITY_ENH"))
                    {
                        sFrom = sFrom + ", ENTITY INSURER_ENTITY_ENH";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "POLICY_ENH.INSURER_EID = INSURER_ENTITY_ENH.ENTITY_ID";
                    }

                    //coverages
                    if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_CVG_ENH"))
                    {
                        sFrom = sFrom + ",POLICY_X_CVG_ENH";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "POLICY_X_CVG_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_CVG_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID) AND";
                        sWhere = sWhere + " POLICY_ENH.POLICY_ID = POLICY_X_CVG_ENH.POLICY_ID";
                    }

                    //Rating
                    if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_RTNG_ENH"))
                    {
                        sFrom = sFrom + ",POLICY_X_RTNG_ENH";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "POLICY_X_RTNG_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_RTNG_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID) AND";
                        sWhere = sWhere + " POLICY_ENH.POLICY_ID = POLICY_X_RTNG_ENH.POLICY_ID";
                    }

                    //Discount
                    if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_DCNT_ENH"))
                    {
                        sFrom = sFrom + ",POLICY_X_DCNT_ENH";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "POLICY_X_DCNT_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_DCNT_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID) AND";
                        sWhere = sWhere + " POLICY_ENH.POLICY_ID = POLICY_X_DCNT_ENH.POLICY_ID";
                    }

                    //Discount Tier
                    if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_DTIER_ENH"))
                    {
                        sFrom = sFrom + ",POLICY_X_DTIER_ENH";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "POLICY_X_DTIER_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_DTIER_ENH A WHERE A.POLICY_ID = POLICY_ENH.POLICY_ID) AND";
                        sWhere = sWhere + " POLICY_ENH.POLICY_ID = POLICY_X_DTIER_ENH.POLICY_ID";
                    }

                    //insured layers
                    if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_INSRD_ENH") || Utilities.ExistsInArray(p_arrlstQueryTables, "INSURED_ENTITY_ENH"))
                    {
                        sFrom = sFrom + ", POLICY_X_INSRD_ENH, ENTITY INSURED_ENTITY_ENH";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "POLICY_ENH.POLICY_ID = POLICY_X_INSRD_ENH.POLICY_ID AND POLICY_X_INSRD_ENH.INSURED_EID = INSURED_ENTITY_ENH.ENTITY_ID";
                    }

                    //MCO support
                    if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_X_MCO_ENH") || Utilities.ExistsInArray(p_arrlstQueryTables, "MCO_ENTITY_ENH"))
                    {
                        sFrom = sFrom + ", POLICY_X_MCO_ENH, ENTITY INSURED_ENTITY_ENH";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "POLICY_ENH.POLICY_ID = POLICY_X_MCO_ENH.POLICY_ID AND POLICY_X_MCO_ENH.MCO_EID = MCO_ENTITY_ENH.ENTITY_ID";
                    }

                    //supp. support
                    if (Utilities.ExistsInArray(p_arrlstQueryTables, "POLICY_SUPP_ENH"))
                    {
                        sFrom = sFrom + ",POLICY_SUPP_ENH";
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + "POLICY.POLICY_ID = POLICY_SUPP_ENH.POLICY_ID";
                    }
                    //Supplimental Multi-Code Field
                    CreateSMVAlias(p_arrlstQueryTables, ref sFrom, ref sWhere, "POLICY_ENH.POLICY_ID");
                    //add on actual criteria
                    if (p_sCriteria.Trim() != "")
                    {
                        if (sWhere != "")
                            sWhere = sWhere + " AND ";
                        sWhere = sWhere + p_sCriteria;
                    }

                    //add on fields
                    if (p_sQueryFields.Trim() != "")
                        sSelect = sSelect + "," + p_sQueryFields;

                    //run criteria query into temp table
                    if (iCount == 0) //Quotes
                    {
                        sSQL = sSelect + " [INTO POLICY_ENH_TEMP] FROM " + sFrom + " ";
                        if (sWhere != "")
                            sSQL = sSQL + " WHERE " + sWhere;
                    }

                    if (iCount == 1) //Policy
                    {
                        sSQL = sSelect + " [INTO POLICY_ENH_TEMP2] FROM " + sFrom + " ";
                        if (sWhere != "")
                            sSQL = sSQL + " WHERE " + sWhere;
                        sSQL = sSQL.Replace("POLICY_ENH.POLICY_STATUS_CODE", "POLICY_X_TRANS_ENH.POLICY_STATUS");
                    }

                    ExecSearchSQL(sSQL, ref p_objConn);

                }
                // Combine POLICY_ENH_TEMP and POLICY_ENH_TEMP2 into 1 table - Only for EPAS Search
                sSQL = "INSERT INTO [POLICY_ENH_TEMP] SELECT * FROM [POLICY_ENH_TEMP2]";
                ExecSearchSQL(sSQL, ref p_objConn);

                //index for quick processing in display only fields
                p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX POLENHTEMP_" + m_lUserId + " ON [POLICY_ENH_TEMP](POLICY_ID)"));

                //Resolve display fields (need temp tables & outer joins)
                sFrom = "[POLICY_ENH_TEMP]";
                sWhere = "";

                //insurer
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "INSURER_ENTITY_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "INSURER_ENTITY_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[INS_ENT_ENH_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_ENH.POLICY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO INS_ENT_ENH_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_ENH_TEMP],POLICY_ENH,ENTITY INSURER_ENTITY_ENH";
                    sSQL = sSQL + " WHERE [POLICY_ENH_TEMP].POLICY_ID = POLICY_ENH.POLICY_ID";
                    sSQL = sSQL + " AND POLICY_ENH.INSURER_EID = INSURER_ENTITY_ENH.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INSUENHTEMP_" + m_lUserId + " ON [INS_ENT_ENH_TEMP](POLICY_ID)"));
                }

                //POLICY_STATUS FROM POLICY_X_TRANS_ENH Table
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_TRANS_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_TRANS_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[TRANS_ENH_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_X_TRANS_ENH.POLICY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO TRANS_ENH_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_ENH_TEMP],POLICY_X_TRANS_ENH";
                    sSQL = sSQL + " WHERE [POLICY_ENH_TEMP].POLICY_ID = POLICY_X_TRANS_ENH.POLICY_ID";
                    sSQL = sSQL + " AND POLICY_X_TRANS_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_TRANS_ENH A WHERE A.POLICY_ID = [POLICY_ENH_TEMP].POLICY_ID)";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX TRANSENHTEMP_" + m_lUserId + " ON [TRANS_ENH_TEMP](POLICY_ID)"));
                }

                //coverages
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_CVG_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_CVG_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[CVG_ENH_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_X_CVG_ENH.POLICY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO CVG_ENH_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_ENH_TEMP],POLICY_X_CVG_ENH";
                    sSQL = sSQL + " WHERE [POLICY_ENH_TEMP].POLICY_ID = POLICY_X_CVG_ENH.POLICY_ID";
                    sSQL = sSQL + "  AND POLICY_X_CVG_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_CVG_ENH A WHERE A.POLICY_ID = [POLICY_ENH_TEMP].POLICY_ID)";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX CVGENHTEMP_" + m_lUserId + " ON [CVG_ENH_TEMP](POLICY_ID)"));
                }
                //Ratings
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_RTNG_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_RTNG_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[RTNG_ENH_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_X_RTNG_ENH.POLICY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO RTNG_ENH_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_ENH_TEMP],POLICY_X_RTNG_ENH";
                    sSQL = sSQL + " WHERE [POLICY_ENH_TEMP].POLICY_ID = POLICY_X_RTNG_ENH.POLICY_ID";
                    sSQL = sSQL + "  AND POLICY_X_RTNG_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_RTNG_ENH A WHERE A.POLICY_ID = [POLICY_ENH_TEMP].POLICY_ID)";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX RTNGENHTEMP_" + m_lUserId + " ON [RTNG_ENH_TEMP](POLICY_ID)"));
                }
                //DISCOUNT
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_DCNT_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_DCNT_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[DCNT_ENH_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_X_DCNT_ENH.POLICY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO DCNT_ENH_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_ENH_TEMP],POLICY_X_DCNT_ENH";
                    sSQL = sSQL + " WHERE [POLICY_ENH_TEMP].POLICY_ID = POLICY_X_DCNT_ENH.POLICY_ID";
                    sSQL = sSQL + "  AND POLICY_X_DCNT_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_DCNT_ENH A WHERE A.POLICY_ID = [POLICY_ENH_TEMP].POLICY_ID)";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DCNTENHTEMP_" + m_lUserId + " ON [DCNT_ENH_TEMP](POLICY_ID)"));
                }
                //DISCOUNT TIER
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_DTIER_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_DTIER_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[DTIER_ENH_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_X_DTIER_ENH.POLICY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO DTIER_ENH_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_ENH_TEMP],POLICY_X_DTIER_ENH";
                    sSQL = sSQL + " WHERE [POLICY_ENH_TEMP].POLICY_ID = POLICY_X_DTIER_ENH.POLICY_ID";
                    sSQL = sSQL + "  AND POLICY_X_DTIER_ENH.TRANSACTION_ID = (SELECT MAX(A.TRANSACTION_ID) FROM POLICY_X_DTIER_ENH A WHERE A.POLICY_ID = [POLICY_ENH_TEMP].POLICY_ID)";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX DTIERENHTEMP_" + m_lUserId + " ON [DTIER_ENH_TEMP](POLICY_ID)"));
                }

                //insured layers
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_INSRD_ENH") || Utilities.ExistsInArray(p_arrlstDisplayTables, "INSURED_ENTITY_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_INSRD_ENH");
                    iIdxInsured = Utilities.IndexInArray(p_arrlstDisplayTables, "INSURED_ENTITY_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[INSURED_ENH_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_X_INSRD_ENH.POLICY_ID";
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    if (iIdxInsured > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxInsured].ToString());
                    sSQL = sSQL + " [INTO INSURED_ENH_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_ENH_TEMP],POLICY_X_INSRD_ENH,ENTITY INSURED_ENTITY_ENH";
                    sSQL = sSQL + " WHERE [POLICY_ENH_TEMP].POLICY_ID = POLICY_X_INSRD_ENH.POLICY_ID AND POLICY_X_INSRD_ENH.INSURED_EID = INSURED_ENTITY_ENH.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX INSDENHTEMP_" + m_lUserId + " ON [INSURED_ENH_TEMP](POLICY_ID)"));
                }

                //MCO support
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_X_MCO_ENH") || Utilities.ExistsInArray(p_arrlstDisplayTables, "MCO_ENTITY_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_X_MCO_ENH");
                    iIdxInsured = Utilities.IndexInArray(p_arrlstDisplayTables, "MCO_ENTITY_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[MCO_ENH_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_X_MCO_ENH.POLICY_ID";
                    if (iIdx > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdx].ToString());
                    if (iIdxInsured > -1)
                        sSQL = sSQL + Utilities.AppendComma(p_arrlstDisplayFields[iIdxInsured].ToString());

                    sSQL = sSQL + " [INTO MCO_ENH_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_ENH_TEMP],POLICY_X_MCO_ENH,ENTITY MCO_ENTITY_ENH";
                    sSQL = sSQL + " WHERE [POLICY_ENH_TEMP].POLICY_ID = POLICY_X_MCO_ENH.POLICY_ID";
                    sSQL = sSQL + " AND POLICY_X_MCO_ENH.MCO_EID = MCO_ENTITY_ENH.ENTITY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX MCOENHTEMP_" + m_lUserId + " ON [MCO_ENH_TEMP](POLICY_ID)"));
                }

                //supplemental support
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "POLICY_SUPP_ENH"))
                {
                    iIdx = Utilities.IndexInArray(p_arrlstDisplayTables, "POLICY_SUPP_ENH");

                    //Add temp table to final outer joined query
                    JoinIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[POLSUPPENH_TEMP]", "POLICY_ID");

                    //Dump results into temp table
                    sSQL = "SELECT POLICY_SUPP_ENH.POLICY_ID," + p_arrlstDisplayFields[iIdx].ToString() + " [INTO POLSUPPENH_TEMP]";
                    sSQL = sSQL + " FROM [POLICY_ENH_TEMP],POLICY_SUPP_ENH";
                    sSQL = sSQL + " WHERE [POLICY_TEMP_ENH].POLICY_ID = POLICY_SUPP_ENH.POLICY_ID";
                    ExecSearchSQL(sSQL, ref p_objConn);

                    //index for quick processing
                    p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX POLSPENHTEMP_" + m_lUserId + " ON [POLSUPPENH_TEMP](POLICY_ID)"));
                }

                //MULTI-CODE Type Supplementals
                //Scan for Multi-Code Values
                for (iIdx = 0; iIdx < p_arrlstDisplayTables.Count; iIdx++)
                {
                    if (p_arrlstDisplayTables[iIdx].ToString().IndexOf("SUPP_MULTI_VALUE") != -1)
                    {
                        lFieldId = Conversion.ConvertStrToLong((p_arrlstDisplayTables[iIdx].ToString().Substring(20)));
                        sMultiCodeTableAlias = "SMV" + lFieldId;

                        //Add temp table to final outer joined query
                        JoinIt(ref sFrom, ref sWhere, "[POLICY_ENH_TEMP]", "[" + sMultiCodeTableAlias + "_TEMP]", "POLICY_ID");

                        //Temporary - add to subCleanupEventSearch later
                        DropTable("[" + sMultiCodeTableAlias + "_TEMP]", "", ref p_objConn);

                        //Dump results into temp table
                        sSQL = "SELECT [POLICY_ENH_TEMP].POLICY_ID, " + p_arrlstDisplayFields[iIdx].ToString() + " [INTO SMV" + lFieldId + "_TEMP]";
                        sSQL = sSQL + " FROM [POLICY_ENH_TEMP], SUPP_MULTI_VALUE " + sMultiCodeTableAlias;
                        sSQL = sSQL + " WHERE [POLICY_ENH_TEMP].POLICY_ID = " + sMultiCodeTableAlias + ".RECORD_ID";
                        sSQL = sSQL + " AND " + sMultiCodeTableAlias + ".FIELD_ID = " + lFieldId;
                        ExecSearchSQL(sSQL, ref p_objConn);

                        //index EVENT_ID for quick processing
                        p_objConn.ExecuteNonQuery(PrepareTempSQL("CREATE INDEX SMV" + lFieldId + "TMP_" + m_lUserId + " ON [SMV" + lFieldId + "_TEMP](POLICY_ID)"));
                    }
                }

                //Chain everything together
                //p_sEnhPolicyData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);
                //Abhishek MITS 10651
                if (!m_bPrintSearch)
                {
                    //Chain everything together
                    p_sEnhPolicyData = GetResultString(p_iPageSize, p_iPageNumber, p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy, ref p_sRetSQL);
                }
                else
                {
                    p_sEnhPolicyData = GetCompleteResultString(p_objConn, sFrom, p_sJustFieldNames, sWhere, p_sOrderBy);

                }

            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("SearchResults.DoENHPolicySearch.GenericError", m_iClientId), p_objException);
            }
        }

        /// <summary>
        /// Returns the name of form (aspx) depending on the type of the Policy.
        /// Author : Mridul.
        /// MITS#:18229
        /// </summary>
        /// <param name="p_iPolicyId">Policy ID</param>
        /// <returns>Name of aspx page</returns>
        private string GetPolicyTypeFormName(int p_iPolicyId)
        {
            LocalCache objCache = null;
            StringBuilder sbSql = null;
            int iPolType = 0;
            string sPolicyType = string.Empty;
            string sPolFormType = string.Empty;

            try
            {
                if (p_iPolicyId == 0)
                    return "policyenh";
                using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT POLICY_TYPE FROM POLICY_ENH WHERE POLICY_ID = ");
                    sbSql.Append(p_iPolicyId.ToString());
                    objConn.Open();
                    iPolType = objConn.ExecuteInt(sbSql.ToString());
                }

                using (objCache = new LocalCache(m_sConnectionString, m_iClientId))
                {
                    sPolicyType = objCache.GetShortCode(iPolType);
                    switch (sPolicyType)
                    {
                        case "AL":
                            sPolFormType = "policyenhal";
                            break;
                        case "GL":
                            sPolFormType = "policyenhgl";
                            break;
                        case "PC":
                            sPolFormType = "policyenhpc";
                            break;
                        case "WC":
                            sPolFormType = "policyenhwc";
                            break;
                    }
                }
                return sPolFormType;
            }
            finally
            {
                if (sbSql != null)
                    sbSql = null;
            }
        }
        //Anu Tennyson for VACo - To ahow the result according to the LOB for which the enhance policy is enabled Starts
        private string CheckPolicyTypeEnabled()
        {
            int iLob = 0;
            string sLob = string.Empty;
            LocalCache objLocalCache = null;
            objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);
            ColLobSettings objLobSettings = new ColLobSettings(m_sConnectionString,m_iClientId);
            if (objLobSettings[242].UseEnhPolFlag == -1)
            {
                iLob = objLocalCache.GetCodeId("AL", "POLICY_LOB");
                sLob = iLob.ToString();
            }
            if (objLobSettings[243].UseEnhPolFlag == -1)
            {
                iLob = objLocalCache.GetCodeId("WC", "POLICY_LOB");
                if (sLob != "")
                {
                    sLob = sLob + "," + iLob.ToString();
                }
                else
                {
                    sLob = iLob.ToString();
                }
            }
            if (objLobSettings[241].UseEnhPolFlag == -1)
            {
                iLob = objLocalCache.GetCodeId("GL", "POLICY_LOB");
                if (sLob != "")
                {
                    sLob = sLob + "," + iLob.ToString();
                }
                else
                {
                    sLob = iLob.ToString();
                }
            }
            if (objLobSettings[845].UseEnhPolFlag == -1)
            {
                iLob = objLocalCache.GetCodeId("PC", "POLICY_LOB");
                if (sLob != "")
                {
                    sLob = sLob + "," + iLob.ToString();
                }
                else
                {
                    sLob = iLob.ToString();
                }
            }
            return sLob;
        }
        //Anu Tennyson for VACo - To ahow the result according to the LOB for which the enhance policy is enabled Ends
        #endregion
        // End Naresh Enhanced Policy Search
        #endregion
        #region Multiple Phone Numbers and Addresses
        private void CheckMultiplePhoneAddresses(bool bPhoneNumbersIncluded, bool bAddressesIncluded, bool bOrgHierarchyPhoneIncluded, ArrayList p_arrlstDisplayTables, ref string sSelect, ref bool bAddressFlag, ref bool bOrgFlag)
        {
            if (!bPhoneNumbersIncluded && (bAddressesIncluded || bOrgHierarchyPhoneIncluded))
            {
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ADDRESS_X_PHONEINFO"))
                {
                    if (bAddressesIncluded)
                    {
                        sSelect += ",ENTITY_X_ADDRESSES.ADDRESS_ID";
                    }
                    if (bOrgHierarchyPhoneIncluded)
                    {
                        sSelect += ",ORG_ADDRESS_X_PHONEINFO.PHONE_ID";
                    }
                }
            }
            else if (bPhoneNumbersIncluded && (!bAddressesIncluded || !bOrgHierarchyPhoneIncluded))
            {
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES") || Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ADDRESS_X_PHONEINFO"))
                {
                    sSelect += ",ADDRESS_X_PHONEINFO.PHONE_ID";
                }
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ENTITY_X_ADDRESSES"))
                    bAddressFlag = true;
                if (Utilities.ExistsInArray(p_arrlstDisplayTables, "ORG_ADDRESS_X_PHONEINFO"))
                    bOrgFlag = true;
            }
        }
        #endregion
    }
}
