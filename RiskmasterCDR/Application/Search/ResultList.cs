﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 11/20/2014 | RMA-345         | achouhan3  | Angular grid implementation 
 **********************************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;//nadim  for 14393 and 7247
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using C1.C1PrintDocument;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;

namespace Riskmaster.Application.Search
{ 
	/// <summary>
	///Author  :   Sumeet Rathod
	///Dated   :   24th August 2004
	///Purpose :   This class performs the search operation and calls the module specific search functions.
	///			   This class also calls the functions related to XML generation corresponding to the search data.
	/// </summary>
	public class ResultList
	{
		#region Private Variables

		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = "";

		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private static string m_sDBType="";

		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		public string m_sConnectionStringSession = "";

		/// <summary>
		/// Page Count
		/// </summary>
		private long m_lPageCount = 0;

        /// <summary>
        /// Client ID for multi-tenant environment, cloud
        /// </summary>
        private int m_iClientId = 0;

		/// <summary>
		/// Contains the final search data received from the Database.
		/// </summary>
 		string[] m_arrStrFinalData = null;

        //MITS 10651 start Abhishek
        private bool m_bPrintSearch = false;
        /// <summary>
        /// C1PrintDocumnet object instance.
        /// </summary>
        private C1PrintDocument m_objPrintDoc = null;

        /// <summary>
        /// CurrentX
        /// </summary>
        private double m_dblCurrentX = 0;
        /// <summary>
        /// CurrentY
        /// </summary>
        private double m_dblCurrentY = 0;
        /// <summary>
        /// Font object.
        /// </summary>
        private Font m_objFont = null;
        /// <summary>
        /// Text object.
        /// </summary>
        private RenderText m_objText = null;
        /// <summary>
        /// PageWidth
        /// </summary>
        private double m_dblPageWidth = 0.0;
        /// <summary>
        /// PageHeight
        /// </summary>
        private double m_dblPageHeight = 0.0;
        /// <summary>
        /// ClientWidth
        /// </summary>
        private double m_dblClientWidth = 0.0;

        private int m_intColumns = 8;

        // Ash - cloud, config settings moved to Db
        //private Hashtable m_MessageSettings = RMConfigurationSettings.GetRMMessages();
        private Hashtable m_MessageSettings = null;

        private UserLogin m_objUserLogin = null;
        //Tushar:MITS#18229
        private Boolean bIsSuccess;
        //End
		//skhare7

        private string sAttachedRecord = "";
        //MITS 10651 End

		#endregion
        //MITS 10651 start Abhishek
        #region Constants Declaration
        /// <summary>
        /// Left alignment
        /// </summary>
        private const string LEFT_ALIGN = "1";
        /// <summary>
        /// Right alignment
        /// </summary>
        private const string RIGHT_ALIGN = "2";
        /// <summary>
        /// Center alignment
        /// </summary>
        private const string CENTER_ALIGN = "3";
        /// <summary>
        /// Number of rows in a report page
        /// </summary>
        private const int NO_OF_ROWS = 54;

        private bool m_bOrgSec = false; //pmittal5 Confidential Record
        /// <summary>
        /// Call from Mobile Adjuster
        /// </summary>
        private bool m_bFromMobileAdj = false;

        /// <summary>
        /// Call from Mobility Adjuster
        /// </summary>
        private bool m_bFromMobilityAdj = false;

        /// <summary>
        /// Call from Mobile Adjuster for Claim Info part    
        /// </summary>
        //private bool m_bFromMobileAdjClaimInfo = false;   //akaur9 Mobile Adjuster added a new variable for the claim info part

        /// <summary>
        /// Call from Mobile Adjuster for Claim List part    
        /// </summary>
        private bool m_bFromMobileAdjClaimList = false;   //akaur9 Mobile Adjuster added a new variable for the claim list part
        private bool m_bFromMobilityAdjEventList = false; 	//mbahl3 mobility changes for event search 
        private bool m_bFromMobilityAdjClaimList = false; 

        #endregion
        //MITS 10651 End

        #region Property Declarations
        /// <summary>
        /// Read property for PageHeight.
        /// </summary>
        private double PageHeight
        {
            get
            {
                return (m_dblPageHeight);
            }
        }
        /// <summary>
        /// Read property for PageWidth.
        /// </summary>
        private double PageWidth
        {
            get
            {
                return (m_dblPageWidth);
            }
        }
        /// <summary>
        /// Read property for PageWidth.
        /// </summary>
        private double ClientWidth
        {
            get
            {
                return (m_dblClientWidth);
            }
            set
            {
                m_dblClientWidth = value;
            }
        }

        /// <summary>
        /// Read/Write property for CurrentX.
        /// </summary>
        private double CurrentX
        {
            get
            {
                return (m_dblCurrentX);
            }
            set
            {
                m_dblCurrentX = value;
            }
        }
        /// <summary>
        /// Read/Write property for CurrentY.
        /// </summary>
        private double CurrentY
        {
            get
            {
                return (m_dblCurrentY);
            }
            set
            {
                m_dblCurrentY = value;
            }
        }
        /// <summary>
        /// Gets unique filename
        /// </summary>
        private string TempFile
        {
            get
            {
                string sFileName = string.Empty;
                string strDirectoryPath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "Payments");
                
                DirectoryInfo objReqFolder = new DirectoryInfo(strDirectoryPath);

                if (!objReqFolder.Exists)
                    objReqFolder.Create();

                sFileName = String.Format("{0}\\{1}", strDirectoryPath, Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));
                return sFileName;
            }
        }
        public bool IsOrgSec { get { return m_bOrgSec; } set { m_bOrgSec = value; } } //pmittal5 Confidential Record
        public bool IsMobileAdjuster { get { return m_bFromMobileAdj; } set { m_bFromMobileAdj = value; } } //gbhatnagar Mobile Adjuster
        public bool IsMobilityAdjuster { get { return m_bFromMobilityAdj; } set { m_bFromMobilityAdj = value; } } //rsharma220 Mobility Adjuster

        //public bool IsMobileAdjusterClaimInfo { get { return m_bFromMobileAdjClaimInfo; } set { m_bFromMobileAdjClaimInfo = value; } } //akaur9 Mobile Adjuster

        public bool IsMobileAdjusterClaimList { get { return m_bFromMobileAdjClaimList; } set { m_bFromMobileAdjClaimList = value; } } //akaur9 Mobile Adjuster
        public bool IsMobilityAdjusterClaimList { get { return m_bFromMobilityAdjClaimList; } set { m_bFromMobilityAdjClaimList = value; } } //akaur9 Mobile Adjuster

        //mbahl3 mobility changes for event search 
        public bool IsMobilityAdjusterEventList { get { return m_bFromMobilityAdjEventList; } set { m_bFromMobilityAdjEventList = value; } } //mbahl3 Mobile Adjuster

        #endregion

		#region Constructor
		/// <summary>
		/// This is default constructor
		/// </summary>
		public ResultList()
		{}


		/// <summary>
		/// Overloaded constructor, which sets the connection string.
		/// </summary>
		/// <param name="p_sConnectionstring">Database connection string</param>
		public ResultList(string p_sConnectionstring, UserLogin p_objUserLogin, int p_iClientId)
		{
			//mini performance issue fix
			//DbConnection objConn = null;
			try
			{
				m_sConnectionString = p_sConnectionstring;
                m_objUserLogin = p_objUserLogin;

				//mini performance issue fix
                //objConn = DbFactory.GetDbConnection(m_sConnectionString);
                //objConn.Open();
				//m_sDBType = objConn.DatabaseType.ToString().ToUpper();
                m_sDBType = p_objUserLogin.objRiskmasterDatabase.DbType.ToString().ToUpper();
                
                // Ash - cloud, config settings moved to Db
                m_iClientId = p_iClientId;
                m_MessageSettings = RMConfigurationSettings.GetRMMessages(m_sConnectionString, m_iClientId);
			}
			catch(Exception p_objException)
			{
                throw new StringException(Globalization.GetString("SearchResults.Constructor.Connection", m_iClientId), p_objException);
			}
				//mini performance issue fix
		//	finally
		//	{
                //if(objConn != null)
                //{
                //    objConn.Close();
                //    objConn.Dispose();
                //}
                //objConn = null;
		//	}
		}


		#endregion

		#region Start search

		/// <summary>
		/// This function sends the generated XML for the given search criteria to the Adapter layer.
		/// </summary>
		/// <param name="p_iUserID">User Id</param>
		/// <param name="p_iGroupID">Group id</param>
		/// <param name="p_lLookupOnlyViewID">View id corresponding to the search module</param>
		/// <param name="p_iPageNo"> Page Number to be displayed</param>
		/// <param name="p_lPageSize">Total page size to be displayed on to the web page</param>
		/// <param name="p_lMaxResults">Maximum results to be displayed</param>
		/// <param name="p_sSysEx">Parameter corresponding to the soundex feature</param>
		/// <param name="p_sFieldValue">Search input data</param>
		/// <param name="p_sDBName">Data base name</param>
		/// <param name="p_sUserName">User name</param>
		/// <param name="p_sPwd">User password</param>
		/// <returns>String containing the search XML.</returns>
		//public string SearchRun(string p_sDsn, int p_iUserID, int p_iGroupID, long p_lLookupOnlyViewID, long p_lPageSize, long p_lMaxResults, string p_sSysEx, string p_sFieldValue,string p_sDBName, string p_sUserName, string p_sPwd)
        public XmlDocument SearchRun(int p_iUserID, int p_iGroupID, long p_lLookupOnlyViewID, int p_iPageNo, int p_iPageSize, long p_lMaxResults, long p_lTotalRecords, XmlDocument xmlUserData, string p_sSortColumn, string p_sOrder, string p_sUserName)
		{
			bool bRet = false;
			string sSQL = "";
			//AL Changes as per Adaptor - Sumeet
			XmlDocument objSearchDom = null;
			//string sRetSearchData = "";
            XElement oXmlUser = null;
            XElement xNode = null; 
            string sLangId = string.Empty;
			try
			{
				//The returned boolean value will be used depending upon the type of session management going to be used in the future.
				//Currently "SearchRun" is just a acting as place holder.
				//AL Changes as per Adaptor - Sumeet
				//bRet = DoSearch( m_sConnectionString, p_iUserID, p_iGroupID, "", p_lLookupOnlyViewID, p_lPageSize, p_lMaxResults, p_sFieldValue, ref sRetSearchData, p_sDBName,p_sUserName,p_sPwd);
                //RMA-345 Satrts    achouhan3       commented to get specified pagination record
               // p_iPageSize = GetRecordsPerPage();
                if (p_iPageSize < 1)
                    p_iPageSize = GetRecordsPerPage();
                //RMA-345 Ends    achouhan3       commented to get specified pagination record

                oXmlUser = XElement.Parse(xmlUserData.OuterXml);

                if (!object.ReferenceEquals(oXmlUser, default(XElement)))
                {
                    xNode = oXmlUser.XPathSelectElement("//SearchMain/LangId");
                    if (xNode != null)
                    {
                        sLangId = xNode.Value.Trim();
                    }
                }

				bRet = DoSearch(p_iUserID, p_iGroupID, "", p_lLookupOnlyViewID, p_iPageSize, p_iPageNo, p_lTotalRecords, xmlUserData, ref objSearchDom, p_sSortColumn, p_sOrder, p_sUserName);

			
				if ((bRet) && (m_lPageCount > 0))
					GetResultPage (p_iPageNo, p_iUserID, "", ref objSearchDom);

				return (objSearchDom);
			}

			catch (XmlOperationException p_objException)
			{
				throw p_objException;
			}

			catch (FileInputOutputException p_objException)
			{
				throw p_objException;
			}

			catch (StringException p_objException)
			{
				throw p_objException;
			}

			catch (CollectionException p_objException)
			{
				throw p_objException;
			}

			catch (RMAppException p_objException)
			{
				throw p_objException;
			}

			catch (Exception p_objException) 
			{
                throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ResultList.SearchRun.Search", m_iClientId), sLangId), p_objException);
			}
			finally
			{
				objSearchDom = null;
                oXmlUser = null;
                xNode = null; 
			}

			//AL Changes as per Adaptor - commented following line - Sumeet
			//return (sRetSearchData);
		}

        //Added By Abhishek
        /// <summary>
        /// This function sends the generated XML for the given search criteria to the Adapter layer.
        /// </summary>
        /// <param name="p_iUserID">User Id</param>
        /// <param name="p_iGroupID">Group id</param>
        /// <param name="p_lLookupOnlyViewID">View id corresponding to the search module</param>
        /// <param name="p_iPageNo"> Page Number to be displayed</param>
        /// <param name="p_lPageSize">Total page size to be displayed on to the web page</param>
        /// <param name="p_lMaxResults">Maximum results to be displayed</param>
        /// <param name="p_sSysEx">Parameter corresponding to the soundex feature</param>
        /// <param name="p_sFieldValue">Search input data</param>
        /// <param name="p_sDBName">Data base name</param>
        /// <param name="p_sUserName">User name</param>
        /// <param name="p_sPwd">User password</param>
        /// <returns>String containing the search XML.</returns>
        public MemoryStream PrintSearch(int p_iUserID, int p_iGroupID, long p_lLookupOnlyViewID, int p_iPageNo, int p_iPageSize, long p_lMaxResults, long p_lTotalRecords, XmlDocument xmlUserData, string p_sSortColumn, string p_sOrder, string p_sUserName, ref XmlDocument p_objXmlOut)
        {
            bool bRet = false;
            //AL Changes as per Adaptor - Sumeet
            XmlDocument objSearchDom = null;
            //string sRetSearchData = "";
            DbConnection objConn = null;
            MemoryStream objMemory = null;
            XmlNode objNode = null;
            string strFormName = "";
            string strTmp = "";
            try
            {
                objNode = (XmlNode)xmlUserData.SelectSingleNode("//Search/SearchMain/FormName");
                if (objNode != null)
                {
                    strTmp = objNode.InnerText;
                    strTmp = strTmp.Substring(0, 1).ToUpper() + strTmp.Substring(1);
                    strFormName = strTmp + " Search";
                    //strFormName = strFormName.r(strTmp, strTmp.ToUpper());
                }
                //RMA-345 Starts    achouhan3      Added PageSize in case of print when Page size comes as 0 or blank in XML
                if (p_iPageSize < 1)
                    p_iPageSize = GetRecordsPerPage();
                //RMA-345 Ends    achouhan3      Added PageSize in case of print when Page size comes as 0 or blank in XML
                m_bPrintSearch = true;
                bRet = DoSearch(p_iUserID, p_iGroupID, "", p_lLookupOnlyViewID, p_iPageSize, p_iPageNo, p_lTotalRecords, xmlUserData, ref objSearchDom, p_sSortColumn, p_sOrder, p_sUserName);
                CreateSearchReport(objSearchDom, out objMemory, strFormName);
                p_objXmlOut = objSearchDom;
                return objMemory;
            }

            catch (XmlOperationException p_objException)
            {
                throw p_objException;
            }

            catch (FileInputOutputException p_objException)
            {
                throw p_objException;
            }

            catch (StringException p_objException)
            {
                throw p_objException;
            }

            catch (CollectionException p_objException)
            {
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.SearchRun.Search", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                objSearchDom = null;
            }
        } 
		
		#endregion

        #region Print Search Result, MITS 10651 Abhishek

        /// Name		: CreateSearchReport
         ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************

        /// <summary>
        /// Creates report based on passed XML document
        /// </summary>
        /// <param name="p_objXmlDoc">The input XML document</param>
        /// <param name="p_objMemory">Memory object containing report</param>
        /// <returns>Success: 1 or Failure: 0</returns>
        public int CreateSearchReport(XmlDocument p_objXmlDoc, out MemoryStream p_objMemory, string p_strSearchFor)
        {
            #region local variables
            double dblCharWidth = 0.0;
            double dblLineHeight = 0.0;
            double dblLeftMargin = 8.0;
            double dblRightMargin = 8.0;
            double dblTopMargin = 8.0;
            double dblPageWidth = 0.0;
            double dblPageHeight = 0.0;
            double dblBottomHeader = 0.0;
            double dblCurrentY = 0.0;
            double dblCurrentX = 0.0;
            double iCounter = 0.0;
            double[] arrColWidth = null;
            int iReturnValue = 0;

            string sHeader = string.Empty;
            string sFile = string.Empty;
            string sSQL = string.Empty;
            //string sNumChecks = string.Empty;
            string[] arrColAlign = null;
            string[] arrCol = null;
            #endregion

            p_objMemory = null;

            try
            {
                arrColAlign = new string[8];
                arrColWidth = new double[8];
                arrCol = new string[8];  

                //sNumChecks = p_objXmlDoc.SelectSingleNode("//TotalNumChecks").InnerText;

                StartDoc(true, 10, 10, 10, 10);
                SetFontBold(true);
                ClientWidth = 15400;

                dblCharWidth = GetTextWidth("0123456789/$,.ABCDEFGHIJKLMNOPQRSTUVWXYZ") / 40;
                dblLineHeight = GetTextHeight("W");
                dblLeftMargin = dblCharWidth * 2;
                dblRightMargin = dblCharWidth * 2;

                dblTopMargin = dblLineHeight * 5;
                dblPageWidth = PageWidth - dblLeftMargin - dblRightMargin;
                dblPageHeight = PageHeight;
                dblBottomHeader = dblPageHeight - (5 * dblLineHeight);

                arrColWidth[0] = dblCharWidth * 28;// First Column
                arrColWidth[1] = dblCharWidth * 23;// 2nd  Column
                arrColWidth[2] = dblCharWidth * 18;// 3rd Column
                arrColWidth[3] = dblCharWidth * 18;// 4th Column
                arrColWidth[4] = dblCharWidth * 20;// 5th Column
                arrColWidth[5] = dblCharWidth * 24;// 6th Column
                arrColWidth[6] = dblCharWidth * 28;// 7th Column
                arrColWidth[7] = dblCharWidth * 23;// 8th Column

               

                

                dblCurrentX = dblLeftMargin;
                dblCurrentY = 900;

                CreateNewPage(m_sConnectionString, true, p_strSearchFor);

                arrColAlign[0] = LEFT_ALIGN; 
                arrColAlign[1] = LEFT_ALIGN; 
                arrColAlign[2] = LEFT_ALIGN; 
                arrColAlign[3] = LEFT_ALIGN; 
                arrColAlign[4] = LEFT_ALIGN; 
                arrColAlign[5] = LEFT_ALIGN;
                arrColAlign[6] = LEFT_ALIGN;
                arrColAlign[7] = LEFT_ALIGN;
             
                CurrentX = dblLeftMargin;
                CurrentY = dblCurrentY;

                #region Intialize array for heading
                XmlNodeList objHeadingRows = (XmlNodeList)p_objXmlDoc.SelectNodes("//results/columns/column");

                if (objHeadingRows.Count < m_intColumns)
                {
                    m_intColumns = objHeadingRows.Count;
                }
                for (int i = 0; i < objHeadingRows.Count; i++)
                {
                    
                    if (i < m_intColumns)
                   arrCol[i] = objHeadingRows.Item(i).InnerText;
                }
               
                #endregion
                //Print header
                PrintHeader(ref dblCurrentY, m_intColumns, dblPageWidth, dblLeftMargin, dblLineHeight, arrCol, arrColWidth, arrColAlign);

                SetFontBold(false);
                dblCurrentY = 1200;
                iCounter = 0;

                XmlNodeList objRows = (XmlNodeList)p_objXmlDoc.SelectNodes("//results/data/row");

                for (int i = 0; i < objRows.Count; i++)
                {
                    CreateRow(objRows.Item(i), dblLeftMargin, ref dblCurrentY, arrColAlign,
                        dblLeftMargin, dblLineHeight, arrColWidth, dblCharWidth);
                    iCounter++;
                    if (iCounter == NO_OF_ROWS)
                    {
                        CreateNewPage(m_sConnectionString, false, p_strSearchFor);
                        iCounter = 0;
                        dblCurrentY = 900;
                        //MGaba2 : 06/05/2008 : MITS 12241-start
                        //Header need to be printed on each page of the search's result.
                        if (i != objRows.Count - 1)
                        {
                            SetFontBold(true);
                            PrintHeader(ref dblCurrentY, m_intColumns, dblPageWidth, dblLeftMargin, dblLineHeight, arrCol, arrColWidth, arrColAlign);
                            SetFontBold(false);
                        }
				//MGaba2 : 06/05/2008 : MITS 12241-end
                    }
                }

                // Printing the Summary
                if ((iCounter + 5) > NO_OF_ROWS)
                {
                    CreateNewPage(m_sConnectionString, false, p_strSearchFor);
                    dblCurrentY = 900;
                    iCounter = 0;
                }
                else
                {
                    dblCurrentY = dblCurrentY + dblLineHeight;
                }

                SetFontBold(true);
                CurrentX = dblLeftMargin;
                CurrentY = dblCurrentY;
            
                // Printing the Footer
                //if ((iCounter + 7) > NO_OF_ROWS)
                //{
                //    if (pcount > 1)//ASINGH263 MITS 34916
                //        dblCurrentY = 900 * pcount;//ASINGH263 MITS 34916
                //}
                //else
                //{
                    dblCurrentY = dblCurrentY + dblLineHeight * 2;
                //}

                CreateFooter(dblLeftMargin, dblCurrentY, dblLineHeight, 250);
                EndDoc();
                sFile = TempFile;
                Save(sFile);
                p_objMemory = Utilities.ConvertFilestreamToMemorystream(sFile, m_iClientId);

                File.Delete(sFile);
                iReturnValue = 1;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException("An error has occurred generating Report.", p_objException);
            }
            finally
            {
                arrColAlign = null;
                arrColWidth = null;
                m_objPrintDoc = null;
                m_objFont = null;
                m_objText = null;
            }
            return iReturnValue;
        }

        /// <summary>
        /// Creates a new page in a report
        /// </summary>
        /// <param name="p_sDSN">Connection string</param>
        /// <param name="p_bFirstPage">First page or not</param>
        /// <param name="p_sNumChecks">Number of Checks that need to be printed</param>
        private void CreateNewPage(string p_sDSN, bool p_bFirstPage, string p_strSearchFor)
        {
            SysParms objSysSetting = null;

            RenderText m_objText = null;
            try
            {
                if (!p_bFirstPage)
                {
                    m_objPrintDoc.NewPage();
                }
                m_objPrintDoc.RenderDirectRectangle(0, 0, 15600, 11700);

                m_objPrintDoc.RenderDirectRectangle(0, 0, 15600, 500, Color.Black, 1, Color.LightGray);
                m_objPrintDoc.RenderDirectRectangle(0, 11300, 15600, 11900, Color.Black, 1, Color.LightGray);
                m_objText = new RenderText(m_objPrintDoc);
                m_objText.Text = " ";
                m_objText.AutoWidth = true;
                CurrentX = m_objText.BoundWidth;

                m_objText.Text = "Results For - " + p_strSearchFor;
                m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.Width, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Left);
                m_objText = new RenderText(m_objPrintDoc);

                objSysSetting = new SysParms(p_sDSN, m_iClientId);
                m_objText.Text = objSysSetting.SysSettings.ClientName + "  " + Conversion.GetDBDateFormat(Conversion.ToDbDate(DateTime.Now), "d") + "  ";
                objSysSetting = null;
                CurrentX = (ClientWidth - m_objText.BoundWidth);

                m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Right);
                m_objText = new RenderText(m_objPrintDoc);

                m_objText.Text = "Page" + " [@@PageNo@@]";
                CurrentX = (PageWidth - m_objText.BoundWidth) / 2;
                m_objPrintDoc.RenderDirectText(CurrentX, 11600, m_objText.Text, m_objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Center);

                m_objText.Text = String.Format("Confidential Data - {0}  ", m_MessageSettings["DEF_RMCAPTION"]);
                CurrentX = (ClientWidth - m_objText.BoundWidth);
                m_objPrintDoc.RenderDirectText(CurrentX, 11600, m_objText.Text, m_objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold | FontStyle.Italic), Color.Black, AlignHorzEnum.Right);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error has occurred while creating initializing new page in report.", p_objEx);
            }
            finally
            {
                objSysSetting = null;
                m_objText = null;
            }
        }

        /// Name		: StartDoc
        /// Date Created: 08/01/2005	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************

        /// <summary>
        /// Initialize the document
        /// </summary>
        /// <param name="p_bIsLandscape">Landscape or Portrait</param>
        /// <param name="p_Left">Left</param>
        /// <param name="p_Right">Right</param>
        /// <param name="p_Top">Top</param>
        /// <param name="p_Bottom">Bottom</param>
        private void StartDoc(bool p_bIsLandscape, int p_Left, int p_Right, int p_Top, int p_Bottom)
        {
            try
            {
                m_objPrintDoc = new C1PrintDocument();
                m_objPrintDoc.DefaultUnit = UnitTypeEnum.Twip;
             
                m_objPrintDoc.PageSettings.Landscape = p_bIsLandscape;
                m_objPrintDoc.PageSettings.Margins.Left = p_Left;
                m_objPrintDoc.PageSettings.Margins.Right = p_Right;
                m_objPrintDoc.PageSettings.Margins.Top = p_Top;
                m_objPrintDoc.PageSettings.Margins.Bottom = p_Bottom;

                m_objPrintDoc.StartDoc();

                // Causes access to default printer     
                m_dblPageHeight = 11 * 1440;
                m_dblPageWidth = 8.5 * 1440;
                if (p_bIsLandscape)
                    m_dblPageHeight += 700;
                else
                    m_dblPageWidth += 700;

                m_objFont = new Font("Arial", 7);
                m_objText = new RenderText(m_objPrintDoc);
                m_objText.Style.Font = m_objFont;
                m_objText.Style.WordWrap = false;
                m_objText.AutoWidth = true;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error has occurred while initializing the print document.", p_objEx);
            }
        }

        /// Name		: SetFont
        ///************************************************************
        /// Amendment History
        ///***************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Set Font 
        /// </summary>
        /// <param name="p_sFontName">Font Name</param>
        /// <param name="p_dblFontSize">Font Size</param>
        private void SetFont(string p_sFontName, double p_dblFontSize)
        {
            try
            {
                m_objFont = new Font(p_sFontName, (float)p_dblFontSize);
                m_objText.Style.Font = m_objFont;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error has occurred while setting the fonts.", p_objEx);
            }
        }

        /// Name		: SetFontBold
        /// Date Created: 08/01/2005	
        ///************************************************************
        /// Amendment History
        ///***************************************************
        /// Date Amended   *   Amendment   *    Author
        /// <summary>
        /// Set font bold
        /// </summary>
        /// <param name="p_bBold">Bool flag</param>
        private void SetFontBold(bool p_bBold)
        {
            try
            {
                if (p_bBold)
                    m_objFont = new Font(m_objFont.Name, m_objFont.Size, FontStyle.Bold);
                else
                    m_objFont = new Font(m_objFont.Name, m_objFont.Size);

                m_objText.Style.Font = m_objFont;

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error has occurred while setting the fonts bold property.", p_objEx);
            }

        }

        /// Name		: GetTextWidth
        /// Date Created: 08/01/2005	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get text width.
        /// </summary>
        /// <param name="p_sText">String Text</param>
        /// <returns>Width</returns>
        private double GetTextWidth(string p_sText)
        {
            try
            {
                m_objText.Text = p_sText + "";
                return (m_objText.BoundWidth);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error has occurred while getting the text width.", p_objEx);
            }
        }

        /// Name		: GetTextHeight
        ///************************************************************
        /// Amendment History
        ///***************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get Text Height
        /// </summary>
        /// <param name="p_sText">String Text</param>
        /// <returns>Height</returns>
        private double GetTextHeight(string p_sText)
        {
            try
            {
                m_objText.Text = p_sText + "";
                return (m_objText.BoundHeight);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error has occurred while getting the text height.", p_objEx);
            }
        }

        /// Name		: PrintText
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Render Text on the document.
        /// </summary>
        /// <param name="p_sPrintText">String text to print</param>
        private void PrintText(string p_sPrintText)
        {
            try
            {
                m_objText.Text = p_sPrintText + "\r";
                m_objText.AutoWidth = true;
                m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error has occurred while rendering the text.", p_objEx);
            }
        }

        /// Name		: PrintTextNoCr
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// <summary>
        /// Render Text on the document with no "\r" on the end.
        /// </summary>
        /// <param name="p_sPrintText">String text to print</param>
        private void PrintTextNoCr(string p_sPrintText)
        {
            try
            {
                m_objText.Text = p_sPrintText + "";
                m_objText.AutoWidth = true;
                m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error has occurred while rendering the text.", p_objEx);
            }
        }

        /// Name		: Save
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Save the C1PrintDocument in PDF format.
        /// </summary>
        /// <param name="p_sPath">Path</param>
        private void Save(string p_sPath)
        {
            try
            {
                m_objPrintDoc.ExportToPDF(p_sPath, false);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error has occurred while saving the document.", p_objEx);
            }
        }

        /// Name		: EndDoc
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Call the end event.
        /// </summary>
        private void EndDoc()
        {
            try
            {
                m_objPrintDoc.EndDoc();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error has occurred while calling the end event of the document.", p_objEx);
            }
        }

        /// Name		: PrintHeader
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Prints report header.
        /// </summary>
        /// <param name="p_dblCurrentY">Current Y</param>
        /// <param name="p_iColumnCount">Columns Count</param>
        /// <param name="p_dblPageWidth">Page Width</param>
        /// <param name="p_dblLeftMargin">Left Margin</param>
        /// <param name="p_dblLineHeight">Line Height</param>
        /// <param name="p_arrCol">Columns Text</param>
        /// <param name="p_arrColWidth">Columns Width</param>
        /// <param name="p_arrColAlign">Columns Alignment.</param>
        private void PrintHeader(ref double p_dblCurrentY, int p_iColumnCount, double p_dblPageWidth,
            double p_dblLeftMargin, double p_dblLineHeight, string[] p_arrCol,
            double[] p_arrColWidth, string[] p_arrColAlign)
        {
            double dblCurrentX = 0.0;
            int iIndex = 0;

            try
            {
                dblCurrentX = p_dblLeftMargin;
                for (iIndex = 0; iIndex < p_iColumnCount; iIndex++)
                {
                    CurrentY = p_dblCurrentY;
                    if (p_arrColAlign[iIndex] == LEFT_ALIGN)
                    {
                        CurrentX = dblCurrentX;
                        PrintText(p_arrCol[iIndex]);
                    }
                    else
                    {
                        CurrentX = dblCurrentX + p_arrColWidth[iIndex]
                            - GetTextWidth(p_arrCol[iIndex]);
                        PrintText(p_arrCol[iIndex]);
                    }

                    dblCurrentX += p_arrColWidth[iIndex];
                }
                p_dblCurrentY += p_dblLineHeight * 2;

            }
            catch (Exception p_objException)
            {
                throw new RMAppException("An error has occurred while creating page header.", p_objException);
            }
        }

        /// Name		: CreateRow
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Creates a record in a report
        /// </summary>
        /// <param name="p_objData">DataRow object containing a row data</param>
        /// <param name="p_dblCurrentX">Current X position</param>
        /// <param name="p_dblCurrentY">Current Y position</param>
        /// <param name="p_arrColAlign">Columns alignment</param>
        /// <param name="p_dblLeftMargin">Left margin</param>
        /// <param name="p_dblLineHeight">Line height</param>
        /// <param name="p_arrColWidth">Column width</param>
        /// <param name="p_dblCharWidth">Character width</param>
        /// <param name="p_bLeadingLine">Leading Line Flag</param>
        /// <param name="p_bCalTotal">Calculate Total Flag</param>
        /// <param name="p_dblTotalAll">Over all Total</param>
        /// <param name="p_dblTotalPay">Total Payments</param>
        /// <param name="p_dblTotalCollect">Total Collections</param>
        /// <param name="p_dblTotalVoid">Total Voids</param>
        private void CreateRow(XmlNode p_objData, double p_dblCurrentX,
            ref double p_dblCurrentY, string[] p_arrColAlign, double p_dblLeftMargin, double p_dblLineHeight,
            double[] p_arrColWidth, double p_dblCharWidth)
        {
            string sData = "";
            //			LocalCache objLocalCache = null;
            string sCheckStatusCode = string.Empty;
            string sCheckStatusDesc = string.Empty;
            XmlNodeList objNodes = null;

            try
            {
                CurrentX = p_dblCurrentX;
                CurrentY = p_dblCurrentY;
                //				objLocalCache = new LocalCache(m_sConnectionString);

                objNodes = p_objData.ChildNodes;
                for (int iIndexJ = 0; iIndexJ < m_intColumns; iIndexJ++)
                {
                    if ((objNodes[iIndexJ].Attributes).GetNamedItem("formatedtext") != null)        //csingh7 MITS 18434 : Start
                    {
                        sData = (objNodes[iIndexJ].Attributes).GetNamedItem("formatedtext").Value;
                    }
                    else
                    {
                        sData = objNodes[iIndexJ].InnerText;
                    }                                               //csingh7 MITS 18434 : End

                    if (sData == "")
                        sData = " ";
                    while (GetTextWidth(sData) > p_arrColWidth[iIndexJ])
                        sData = sData.Substring(0, sData.Length - 1);

                    // Print using alignment
                    if (p_arrColAlign[iIndexJ] == LEFT_ALIGN)
                    {
                        CurrentX = p_dblCurrentX;
                        PrintTextNoCr(" " + sData);
                    }
                    else
                    {
                        CurrentX = p_dblCurrentX + p_arrColWidth[iIndexJ]
                            - GetTextWidth(sData) - p_dblCharWidth;
                        PrintTextNoCr(sData);
                    }
                    p_dblCurrentX += p_arrColWidth[iIndexJ];
                }

                p_dblCurrentX = p_dblLeftMargin;
                p_dblCurrentY += p_dblLineHeight;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException("An error has occurred while creating a row in the Report.", p_objException);
            }
        }

        /// Name		: CreateFooter
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Creates Report Footer
        /// </summary>
        /// <param name="p_dCurrentX">X Position</param>
        /// <param name="p_dCurrentY">Y Position</param>
        /// <param name="p_dHeight">Height</param>
        /// <param name="p_dblSpace">Space between 2 words</param>
        private void CreateFooter(double p_dCurrentX, double p_dCurrentY, double p_dHeight, double p_dblSpace)
        {
            try
            {
                m_objText.Style.Font = new Font("Arial", 7f, FontStyle.Underline | FontStyle.Bold);
                m_objText.Text = m_MessageSettings["DEF_NOTE"].ToString();
                p_dCurrentY = p_dCurrentY + p_dHeight;
                m_objPrintDoc.RenderDirect(p_dCurrentX, p_dCurrentY, m_objText);

                m_objText.Style.Font = new Font("Arial", 7f, FontStyle.Bold);
                p_dCurrentY = p_dCurrentY + p_dHeight;
                m_objText.Text = m_MessageSettings["DEF_RMPROPRIETARY"].ToString();
                m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

                p_dCurrentY = p_dCurrentY + p_dHeight;
                m_objText.Text = m_MessageSettings["DEF_NAME"].ToString();
                m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

                p_dCurrentY = p_dCurrentY + p_dHeight;
                m_objText.Text = m_MessageSettings["DEF_RMCOPYRIGHT"].ToString();
                m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

                p_dCurrentY = p_dCurrentY + p_dHeight;
                m_objText.Text = m_MessageSettings["DEF_RMRIGHTSRESERVED"].ToString();
                m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error has occurred while creating page footer.", p_objEx);
            }
        }

        #endregion

        #region Private Functions
        /// <summary>
		/// Gets the number records displayed per page 
		/// </summary>
		/// <returns></returns>
		public int GetRecordsPerPage()//smishra25:Changing the access modifier for MITS 18098
		{
			StringBuilder sbSQL=null;
			int iReturn=0;
			DbReader objReader=null;

			try
			{
				sbSQL=new StringBuilder();
				sbSQL.Append("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE");
				sbSQL.Append(" WHERE PARM_NAME = 'SRCH_USER_LIMIT' ");
				objReader = DbFactory.GetDbReader(m_sConnectionString,sbSQL.ToString());
				if  (objReader.Read())
				{ 
					iReturn= Conversion.ConvertObjToInt(objReader.GetValue("STR_PARM_VALUE"), m_iClientId);
					if (iReturn<10 )
						iReturn=10;//Record per Page should greater than equal to 10
				}
				else
				{
					//iReturn=0;
					iReturn=10;//Record per Page should greater than equal to 10
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
			}
			catch (Exception p_objEx)
			{
				throw p_objEx;
			}
			finally
			{
				sbSQL=null;
				if (!objReader.IsClosed)
				{
					objReader.Close();
                    //Shivendu to dispose the object
                    objReader.Dispose();
				}
				objReader=null;
			}
			return iReturn;
		}

		#region Get Result Page

		/// <summary>
		/// Gets the Result Page
		/// </summary>
		/// <param name="p_lPage">Page number</param>
		/// <param name="p_iUserId">User id</param>
		/// <param name="p_sResultPath">Result path</param>
		/// <param name="p_objSearchDom">Search XML</param>
		private void GetResultPage (long p_lPage, int p_iUserId, string p_sResultPath, ref XmlDocument p_objSearchDom)
		{
			string sFileName = "";
			long lTotalRecords = 0;
			long lTotalPages = 0;
			long lPageSize = 0;
			long lRowStart = 0;
			long lRowEnd = 0;
			XmlElement objSearchElem = null;

			try
			{
				if (p_sResultPath.Trim().Length > 0)
				{
					p_objSearchDom = new XmlDocument ();
					sFileName = p_sResultPath + "\\searchresults-uid" + p_iUserId + ".xml";
					p_objSearchDom.Load (sFileName);
				}

				objSearchElem = p_objSearchDom.DocumentElement;

				//set page bounds attributes so XSL can display correct rows
				lTotalRecords = Conversion.ConvertStrToLong (objSearchElem.GetAttribute("totalrows"));
				lTotalPages = Conversion.ConvertStrToLong (objSearchElem.GetAttribute("totalpages"));
				lPageSize = Conversion.ConvertStrToLong (objSearchElem.GetAttribute("pagesize"));

				if ((lTotalRecords <= 0) || (lTotalPages <= 0))
					return;

				lRowStart = (p_lPage - 1) * lPageSize;
				//lRowEnd = lRowStart + (lPageSize - 1);
				lRowEnd = lRowStart + lPageSize;
				if (lRowEnd > (lTotalRecords - 1)) 
					lRowEnd = lTotalRecords;

				objSearchElem.SetAttribute ("rowstart", lRowStart.ToString ());
				objSearchElem.SetAttribute ("rowend", lRowEnd.ToString ());
				objSearchElem.SetAttribute ("pageno", p_lPage.ToString ());
			}
			catch (Exception p_objException) 
			{
                throw new RMAppException(Globalization.GetString("ResultList.GetResultPage.Error", m_iClientId), p_objException);
			}
			finally 
			{
				objSearchElem = null;
			}
		}
		#endregion

        #region Get Restrincted Search Field
        ///<Summary>
        ///Tushar:MITS 18229
        ///</Summary>
        private void GetCodeFieldRestricted(ref XmlDocument p_XmlIn)
        {
            StringBuilder  sSql = null;
            XmlElement objXmlElement = null;
            XmlElement objFieldXmlElement = null;
            XmlElement objFieldValueElement = null;
            string sCodeFieldRestrict = String.Empty;
            string sCodeFieldRestrictID = string.Empty ;
            DbReader objreader = null;
            DbConnection objConn = null;

            try
            {

                if (p_XmlIn.SelectSingleNode("//SearchMain/codefieldrestrict") != null)
                {
                    sCodeFieldRestrict = p_XmlIn.SelectSingleNode("//SearchMain/codefieldrestrict").InnerText.Trim();
                }

                if (p_XmlIn.SelectSingleNode("//SearchMain/codefieldrestrictid") != null)
                {
                    sCodeFieldRestrictID = p_XmlIn.SelectSingleNode("//SearchMain/codefieldrestrictid").InnerText;
                }
                sSql = new StringBuilder(); 
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();



                 sSql.Append(   "SELECT  FIELD_NAME,FIELD_DESC,FIELD_TABLE,FIELD_TYPE,CODE_TABLE"); 
                 sSql.Append(" FROM SEARCH_DICTIONARY where FIELD_ID ='" + sCodeFieldRestrict  + "'");
                 

                objreader = objConn.ExecuteReader( sSql.ToString() );

                
                if (objreader.Read())
                {
                    objXmlElement = (XmlElement)p_XmlIn.SelectSingleNode("//SearchFields");
                    objFieldXmlElement = p_XmlIn.CreateElement("field");
                    objFieldXmlElement.SetAttribute("fieldtype", objreader.GetInt16("FIELD_TYPE").ToString());
                    objFieldXmlElement.SetAttribute("id", "FLD"+ sCodeFieldRestrict);
                    objFieldXmlElement.SetAttribute("issupp", "0");
                    objFieldXmlElement.SetAttribute("table",  objreader.GetString("FIELD_TABLE"));
                    objFieldXmlElement.SetAttribute("name", objreader.GetString("FIELD_NAME"));
                    objFieldXmlElement.SetAttribute("codetable",   objreader.GetString("CODE_TABLE"));
                    objFieldXmlElement.SetAttribute("type", objreader.GetInt16("FIELD_TYPE").ToString());
                    objFieldXmlElement.InnerText =   objreader.GetString("FIELD_DESC");


                    objFieldValueElement = p_XmlIn.CreateElement("_cid");
                    objFieldValueElement.InnerText = sCodeFieldRestrictID;
                    objFieldXmlElement.AppendChild(objFieldValueElement);

                    objFieldValueElement = p_XmlIn.CreateElement("_tableid");
                    objFieldValueElement.InnerText = objreader.GetString("FIELD_TABLE");
                    objFieldXmlElement.AppendChild(objFieldValueElement);

                    objFieldValueElement = p_XmlIn.CreateElement("Operation");
                    objFieldValueElement.InnerText = "=";
                    objFieldXmlElement.AppendChild(objFieldValueElement);

                    

                    objXmlElement.AppendChild(objFieldXmlElement);
                }
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                objConn.Dispose();
                objreader.Dispose(); 
            }
        }

        #endregion

        /// <summary>
        /// MGaba2:MITS 20175
        /// </summary>
        /// <param name="p_sEntityId">Entity Id of the entity which is selected in the search criteria</param>
        /// <param name="p_iEntityLevel">Its Org level like 1012 for department</param>
        /// <returns>returns a comma separated list of ids of entity itself and its children when selected in the search criteria</returns>        
        private string GetOrgHierarchyChilden(string p_sEntityId, int p_iEntityLevel)
        {
            string sEntityLevel= null;
            ArrayList sArrChildEntityLevels = null ;
            StringBuilder sbChildEntities = null;            
            string sSql=null ;        
            
            try
            {              
                sEntityLevel = SetOrgLevel(p_iEntityLevel.ToString());
                sArrChildEntityLevels = new ArrayList();
                while(p_iEntityLevel < 1012)
                {
                   p_iEntityLevel++;
                   sArrChildEntityLevels.Add(SetOrgLevel(p_iEntityLevel.ToString()));                    
                }
                
                sbChildEntities = new StringBuilder();
                sbChildEntities.Append(p_sEntityId);

                foreach (string sLevel in sArrChildEntityLevels)
                {
                    sSql = string.Format("SELECT DISTINCT {0} FROM ORG_HIERARCHY WHERE {1} = {2}", sLevel, sEntityLevel, p_sEntityId);
                    using (DbReader objDb = DbFactory.GetDbReader(m_sConnectionString, sSql))
                    {
                        while (objDb.Read())
                        {   
                            sbChildEntities.AppendFormat(",{0}",objDb.GetInt32(sLevel));
                        }
                    }
                }

                return sbChildEntities.ToString();
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ResultList.GetOrgHierarchyChilden.Error", m_iClientId), p_objException);
            }
        }

		#region Perform search
		/// <summary>
		/// This function calls the specific search module functions and in turn generates the XML out of the data obtained from the database.
		/// </summary>
		/// <param name="p_sDsn">Database connection string</param>
		/// <param name="p_iUserID">User id</param>
		/// <param name="p_iGroupID">Group id</param>
		/// <param name="p_sResultsPath">Path where XML file is located</param>
		/// <param name="p_lLookupOnlyViewID">View id corresponding to the search module</param>
		/// <param name="p_lPageSize">Total page size to be displayed on to the web page</param>
		/// <param name="p_lMaxResults">Maximum results to be displayed</param>
		/// <param name="p_sFinalData">Search criteria enteterd by the user</param>
		/// <param name="p_sSearchXMLData">Search data corresponding to the given criteria</param>
		/// <param name="p_sDBName">Data base name</param>
		/// <param name="p_sUserName">User name</param>
		/// <param name="p_sPwd">User password</param>
		/// <returns>True if search result is greater than 0 else returns false</returns>
		private bool DoSearch (int p_iUserID, int p_iGroupID, string p_sResultsPath, long p_lLookupOnlyViewID, int p_iPageSize, int p_iPageNumber, long p_lTotalRecords, XmlDocument p_xmlUserData, ref XmlDocument p_objSearchDom, string p_sSortColumn, string p_sOrder,string p_sUserName)
        {
            #region local variables
            int iNumDisplay = 0;
			int iCnt = 0;			
			int iIndex = 0;
			int iViewID = 0;
			int iFieldType = 0;
            int iATCount = 0; //Geeta 06/17/08 : Mits 12643
			string sCriteria = "";			
			string sFieldName = "";
			string sFieldTable = "";
			string sFieldID = "";
			string sFieldDesc = "";
			string sFieldType = "";			
			string sQueryFields = "";       //display fields piggy backed onto criteria query
			string sJustFieldNames = "";
			string sSQL = "";			
			string sJustFieldNamesByNumber = "";
			string sTmp = "";
			string sOperator = "";
			string sValue = "";
			string sValueEnd = "";			
			string sOrderBy = null;
			string sFieldCodeTable = "";			
			string sCriteriaAddendum = "";
			string sAlias = "";			
			string sTableRestrict = "";
			string sSysEx = "";
			string sFormName = "";
			string sSettings = "";
			string sATTable = "";
			string sATKeyField = "";
			string sCaseSen = "";
			string sEntityType = "";
			string sOrgLevel = ""; // Mihika 16-Feb-2006 MITS 4860 changes
            string sSubTable = ""; // csingh7 R6 Claim Comment Enhancement
            string sPolicyRestrict = "";
            // npadhy Start MITS 22029 To restrict the Policy records of another LOB when searched from a Policy Screen
            string sFilter = string.Empty;
            int iPolicyLob = 0;
            // npadhy End MITS 22029 To restrict the Policy records of another LOB when searched from a Policy Screen
            //nadim for 14393
            string sTempValue = "";
            //nadim for 14393
			ArrayList arrlstQueryTables = null;
			ArrayList arrlstDisplayTables = null; //tables for display only (i.e. no criteria fields)
			ArrayList arrlstDisplayFields= null; //fields for display only (not piggy backed, etc.)
			ArrayList arrlstColumnHeaders = null;
			ArrayList arrlstFieldTypes = null;
			long lCatID = 0;
			long lEntityTableRestrict = 0;   //used only for people/entity search where we need to restrict search to a particular entity type
			long lPolicyRestrict = 0;			
			long[] orderBy = null;
			long lResults = 0;
			int iCaseSen = 0;
			//long lPageCount = 0;
			bool bSuppField = false;
			bool bWildCard = false;
			bool bDoSoundex = false;
			bool bLookupOnly = false;    //specifies this is a lookup - no criteria - just display all records
			bool bMultiValues= false;
			XmlDocument objXml = null;
			XmlElement objXmlSearch = null;
			XmlNodeList objXmlTempFields = null;
			XmlNodeList objXmlFields = null;

			//AL Changes as per Adaptor - Sumeet
			XmlElement oXmlUser = null;
			XmlElement oXmlTmp = null;
			XmlElement oXmlTableList = null;
			XmlNamespaceManager nsSearchDef = null;
			XmlNamespaceManager nsUserData = null;

			DbReader objReader = null;
			SearchResults objSearchResults = null;
			SearchXML objSearch = null;
			DbConnection objConn = null;
			LocalCache objCache = null;
            SysSettings objSysSettings = null;//abansal23 09/16/2009 MITS 17937: Claim search is still case sensitive when Oracle Case Insensitive option is checked
            ArrayList arrlstTextSuppFields = null; //pmittal5 Mits 18356 11/13/09 - arraylist containing names of Free Text type Supplemental fields
            //Tushar:MITS#18229
                string sCodeFieldRestrict = string.Empty;
            //End
            bool bSettingOn = true; //Added Rakhi for R7:Add Emp Data Elements

            int iOrgTableId = 0;            //MGaba2:MITS 20175
            string sOrgChilren = null;      //MGaba2:MITS 20175

            string sLangId = string.Empty;
            string sPageId = string.Empty;
            XElement oXmlID = null;
            XElement xNode = null; 
            #endregion

            try
			{			
				orderBy = new long[3];
				arrlstDisplayTables = new ArrayList();
				arrlstQueryTables = new ArrayList();
				arrlstDisplayFields = new ArrayList();
                arrlstTextSuppFields = new ArrayList(); //Mits 18356 11/13/09
				objXml = new XmlDocument();
				objCache = new LocalCache(m_sConnectionString,m_iClientId);
				//Get the DB Type
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                m_sDBType = objConn.DatabaseType.ToString();

                //objConn.Close();

				if (p_lLookupOnlyViewID != 0)
					bLookupOnly = true;
				else 
					bLookupOnly = false;

				//Obtain user input data so we can pick the search info directly from it
				if (!bLookupOnly)
					oXmlUser = p_xmlUserData.DocumentElement;

                oXmlID = XElement.Parse(oXmlUser.OuterXml);

                if (!object.ReferenceEquals(oXmlID, default(XElement)))
                {
                    xNode = oXmlID.XPathSelectElement("//SearchMain/LangId");
                    if (xNode != null)
                    {
                        sLangId = xNode.Value.Trim();
                    }

                    xNode = oXmlID.XPathSelectElement("//SearchMain/PageId");

                    if (xNode != null)
                    {
                        sPageId = xNode.Value.Trim();
                    }
                }

				//Pull some initial data from Request form data
				if (!bLookupOnly)
				{
					//AL Changes as per Adaptor - Sumeet
					if (oXmlUser.SelectSingleNode("//SearchMain/SoundEx").InnerText == "-1") // Mihika 27-Feb-2006: Defect#2335 In case 'Soundex' is selected, '-1' is passed.
						bDoSoundex = true;
					else
						bDoSoundex = false;

					//AL Changes as per Adaptor - Sumeet
					//iViewID = Conversion.ConvertStrToInteger(GetFieldValue("viewid"));
					iViewID = Conversion.ConvertStrToInteger(oXmlUser.SelectSingleNode("//SearchMain/ViewId").InnerText.Trim());

					if (iViewID == 0)
						throw new RMAppException( CommonFunctions.FilterBusinessMessage(Globalization.GetString("ResultList.DoSearch.SearchError", m_iClientId),sLangId));
				}
				else
				{
					bDoSoundex = false;
					iViewID = (int) p_lLookupOnlyViewID;
				}

                //abansal23 09/16/2009 MITS 17937: Claim search is still case sensitive when Oracle Case Insensitive option is checked Started
                //objCache.GetSysInfo("ORACLE_CASE_INS", ref sCaseSen);
                objSysSettings = new SysSettings(m_sConnectionString,m_iClientId); //Ash - cloud
                iCaseSen = objSysSettings.OracleCaseIns;
				
                //if (sCaseSen != "")
                //    iCaseSen = Conversion.ConvertStrToInteger (sCaseSen);

                //if (iCaseSen != 0)
                //    iCaseSen = (int) (iCaseSen);

                //abansal23 09/16/2009 MITS 17937: Claim search is still case sensitive when Oracle Case Insensitive option is checked End

				//check for oracle case insensitivity
				if ((m_sDBType == Constants.DB_ORACLE) && (iCaseSen == 1 || iCaseSen == -1))
					iCaseSen = 1;
				else
					iCaseSen = 0;

				sFormName = oXmlUser.SelectSingleNode("//SearchMain/FormName").InnerText.Trim();
				sTableRestrict = oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText.Trim();


                //Tushar:MITS#18229
                if (oXmlUser.SelectSingleNode("//SearchMain/codefieldrestrict") != null)
                {
                    sCodeFieldRestrict = oXmlUser.SelectSingleNode("//SearchMain/codefieldrestrict").InnerText.Trim();
                }

                if (!string.IsNullOrEmpty(sCodeFieldRestrict))
                {
                    GetCodeFieldRestricted(ref p_xmlUserData);
                }

                //End:MITS#18229
				objSearch = new SearchXML(m_sConnectionString, m_iClientId);
                objSearch.GetSearchXML(ref iViewID, p_iUserID, p_iGroupID, sTableRestrict, sFormName, sSettings, ref objXml, "", sLangId, sPageId,string.Empty,string.Empty);//skhare7 JIRA 340 Entity Role

				if (objXml == null)
					throw new XmlOperationException (CommonFunctions.FilterBusinessMessage(Globalization.GetString("CommonErrors.XMLError", m_iClientId),sLangId));
				
				objXmlSearch = objXml.DocumentElement;   //get root

				//AL Changes as per Adaptor - Sumeet
				// Setup namespace managers for any XPath queries we do
				nsSearchDef = new XmlNamespaceManager(objXml.NameTable);  
				nsSearchDef.AddNamespace("app", "Riskmaster.Application.Search");
				nsUserData = new XmlNamespaceManager(p_xmlUserData.NameTable);
				nsUserData.AddNamespace("app", "Riskmaster.Application.Search");

				//Pick out a few common pieces of information
				lCatID =  Conversion.ConvertStrToLong(objXmlSearch.GetAttribute("catid"));
				sCriteria = "";

				//Setup initial fields/tables based on type of search				
				switch(lCatID)
				{
					case ConstGlobals.CLAIM_SEARCH :   //Claim search
						AddUnique(ref arrlstQueryTables, "CLAIM");
						sJustFieldNames = "CLAIM.CLAIM_ID";   //should be init on every search type
						sJustFieldNamesByNumber = "[CLAIM_TEMP].CLAIM_ID";
						//if event is displayed only, it can be always satisfied with an inner join rather than an expensive outer join
						
						//AL Changes as per Adaptor - Sumeet (Following commented line is kept just in case it is required in future)
						//objXmlFields = objXml.SelectNodes("app:search/app:displayfields/app:field", nsSearchDef);   // JP added namespace manager
						objXmlFields = objXml.SelectNodes("search/displayfields/field");

						foreach(XmlElement XMLField in objXmlFields)
						{
							sFieldTable = XMLField.GetAttribute("table");
                            //MITS 25626: smishra54, 08/08/2011 - added new 'Person involved supplementals' search fields for claim search 
                            if ((sFieldTable == "EVENT") || (sFieldTable == "PERSON_INVOLVED") || (sFieldTable == "PI_ENTITY") || (sFieldTable == "EVENT_SUPP") || (sFieldTable == "PI_X_BODY_PART") || (sFieldTable == "RPTD_ENTITY") || (sFieldTable == "PI_SUPP"))
							{
								AddUnique(ref arrlstQueryTables, "EVENT");
							}
                            //skhare7 R8 enhancement Carrier claim search
                            if (XMLField.InnerText.Contains("PropSalvage"))
                            {
                                AddUnique(ref arrlstQueryTables, "CLAIM_X_PROPERTYLOSS");
                            
                            }
                            if (XMLField.InnerText.Contains("UnitSalvage"))
                            {
                                AddUnique(ref arrlstQueryTables, "UNIT_X_CLAIM");

                            }
                            // atavaragiri mits 26342
                            if (sFieldTable == "EVENT_X_OSHA_SUPP")
                            {
                                AddUnique(ref arrlstQueryTables, "EVENT_X_OSHA_SUPP");
                            
                            }  // END:MITS 26342

                            //Start averma62 - MITS 28903
                            if (sFieldTable == "PI_X_INJURY_SUPP")
                            {
                                AddUnique(ref arrlstQueryTables, "PI_X_INJURY_SUPP");

                            } 
                            //End averma62 - MITS 28903
                            // skhare7 End
                            //asingh263 MITS 34597
                            if (sFieldTable == "CLAIM_X_ARB")
                            {
                                AddUnique(ref arrlstQueryTables, "CLAIM_X_ARB");

                            }
                            //asingh263 MITS 34597
						}
						break;
					case ConstGlobals.EVENT_SEARCH :   //Event search
						AddUnique(ref arrlstQueryTables, "EVENT");
						sJustFieldNames = "EVENT.EVENT_ID";   //should be init on every search type
						sJustFieldNamesByNumber = "[EVENT_TEMP].EVENT_ID";

						// Mihika 16-Feb-2006 "EVENT_X_OSHA" added in QueryTable if present.
						objXmlFields = objXml.SelectNodes("search/displayfields/field");

						foreach(XmlElement XMLField in objXmlFields)
						{
							sFieldTable = XMLField.GetAttribute("table");
                            //MITS 26380: tmalhotra2, 11/11/2011 - added new 'Person involved supplementals' search fields for event search
                            if ((sFieldTable == "EVENT_X_OSHA") || (sFieldTable == "PI_SUPP"))
							{
                                //following line added by tushar for testing
                                //AddUnique(ref arrlstQueryTables, "EVENT_X_OSHA");

                                AddUnique(ref arrlstQueryTables, "EVENT");
							}
						}
						break;
                        //Mits 26380: END
					case ConstGlobals.EMPLOYEE_SEARCH :   //Employee search
						AddUnique(ref arrlstQueryTables, "EMPLOYEE");
						AddUnique(ref arrlstQueryTables, "ENTITY");
						sJustFieldNames = "EMPLOYEE.EMPLOYEE_EID";
						sJustFieldNamesByNumber = "[EMPLOYEE_TEMP].EMPLOYEE_EID";

                        //MITS 20237 Add support for permission "View SSN" for employee
                        if (!m_objUserLogin.IsAllowedEx(RMPermissions.RMO_EMPLOYEE, RMPermissions.RMO_EMPLOYEE_VIEW_SSN))
                        {
                            XmlNode objEntityTaxID = objXml.SelectSingleNode("//displayfields/field[@table='ENTITY' and @name='TAX_ID']");
                            if (objEntityTaxID != null)
                                objEntityTaxID.ParentNode.RemoveChild(objEntityTaxID);
                        }
						break;
					case ConstGlobals.ENTITY_SEARCH :   //People/entity search
						AddUnique(ref arrlstQueryTables, "ENTITY");
						sJustFieldNames = "ENTITY.ENTITY_ID";
						sJustFieldNamesByNumber = "[ENTITY_TEMP].ENTITY_ID";
                        //avipinsrivas start : Worked for JIRA - 7767
                        bool bSuccess;
                        ////*********************Mohit Yadav for Bug No. 000080************************
                        //if (Conversion.ConvertStrToInteger(oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText) > 0 ||
                        //    Conversion.ConvertStrToInteger(oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText) == ConstGlobals.ORGHEI_SEARCH_PARAM)
                        //    lEntityTableRestrict = Conversion.ConvertStrToInteger(oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText);
                        //    //Shruti, for MITS 7904
                        //else if (oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText == "-1")
                        //{
                        //    lEntityTableRestrict = Conversion.ConvertStrToInteger(oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText);
                        //    //Saurav, for MITS 11279 :start
                        //    sSysEx = oXmlUser.SelectSingleNode("//SearchMain/SysEx").InnerText.Trim();
                        //    //Saurav, for MITS 11279 :end
                        //}
                        //else
                        //{
                        //    lEntityTableRestrict = Convert.ToInt64(objCache.GetTableId(oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText));
                        //    sSysEx = oXmlUser.SelectSingleNode("//SearchMain/SysEx").InnerText.Trim(); // Added by Charanpreet for MITS 12545
                        //}
                        ////*********************Mohit Yadav for Bug No. 000080************************
                        if (Conversion.CastToType<int>(sTableRestrict, out bSuccess) > 0 ||
                            Conversion.CastToType<int>(sTableRestrict, out bSuccess) == ConstGlobals.ORGHEI_SEARCH_PARAM)
                            lEntityTableRestrict = Conversion.CastToType<int>(sTableRestrict, out bSuccess);
                        else if (sTableRestrict == "-1")
                        {
                            lEntityTableRestrict = Conversion.CastToType<int>(sTableRestrict, out bSuccess);
                            sSysEx = oXmlUser.SelectSingleNode("//SearchMain/SysEx").InnerText.Trim();
                        }
                        else
                        {
                            lEntityTableRestrict = objCache.GetTableId(sTableRestrict);
                            sSysEx = oXmlUser.SelectSingleNode("//SearchMain/SysEx").InnerText.Trim(); // Added by Charanpreet for MITS 12545
                        }
                        //avipinsrivas end
						break;
					case ConstGlobals.VEHICLE_SEARCH :   //Vehicle search
						AddUnique(ref arrlstQueryTables, "VEHICLE");
						sJustFieldNames = "VEHICLE.UNIT_ID";
						sJustFieldNamesByNumber = "[VEHICLE_TEMP].UNIT_ID";
						break;
					case ConstGlobals.POLICY_SEARCH : //Policy search
						AddUnique(ref arrlstQueryTables, "POLICY");
						sJustFieldNames = "POLICY.POLICY_ID";
						sJustFieldNamesByNumber = "[POLICY_TEMP].POLICY_ID";
						
						//AL Changes as per Adaptor - Sumeet
						lPolicyRestrict = Conversion.ConvertStrToLong(oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText);
						//lPolicyRestrict = Conversion.ConvertStrToLong(GetFieldValue("tablerestrict").ToString());
						break;
					case ConstGlobals.PAYMENT_SEARCH :  //Payment search
						AddUnique(ref arrlstQueryTables, "FUNDS");
						// Mihika 16-Feb-2006 Adding tables in QueryTables
                        //Commented by pmittal5 : Mits 14021 
                        //AddUnique(ref arrlstQueryTables, "FUNDS_TRANS_SPLIT");
                        //AddUnique(ref arrlstQueryTables, "FUNDS_ACCOUNT");
                        //AddUnique(ref arrlstQueryTables, "PAYEE_ENTITY");
                        //AddUnique(ref arrlstQueryTables, "CLAIMANT_ENTITY");  
                        //End - pmittal5
						sJustFieldNames = "FUNDS.TRANS_ID";
						// Mihika 16-Feb-2006 Changes related to MITS 4860
						sJustFieldNamesByNumber = "[FUNDS_TEMP].TRANS_ID";
						break;
					case ConstGlobals.PATIENT_SEARCH :   //Patient search
						AddUnique(ref arrlstQueryTables, "PATIENT");
						AddUnique(ref arrlstQueryTables, "PATIENT_ENTITY");
						sJustFieldNames = "PATIENT.PATIENT_ID";   //should be init on every search type
						sJustFieldNamesByNumber = "[PATIENT_TEMP].PATIENT_ID";
						break;
					case ConstGlobals.PHYSICIAN_SEARCH :   //Physician search
						AddUnique(ref arrlstQueryTables, "PHYSICIAN");
						AddUnique(ref arrlstQueryTables, "PHYS_ENTITY");
						sJustFieldNames = "PHYSICIAN.PHYS_EID"; // should be init on every search type
						sJustFieldNamesByNumber = "[PHYS_TEMP].PHYS_EID";
						break;
					case ConstGlobals.MED_STAFF_SEARCH :   //Med. Staff search
						AddUnique(ref arrlstQueryTables, "MED_STAFF");
						AddUnique(ref arrlstQueryTables, "MED_ENTITY");
						sJustFieldNames = "MED_STAFF.STAFF_EID";  //should be init on every search type
						sJustFieldNamesByNumber = "[MED_TEMP].STAFF_EID";
						break;
					case ConstGlobals.DISP_PLAN_SEARCH  :   //Disability Plan Search
						AddUnique(ref arrlstQueryTables, "DISABILITY_PLAN");
						sJustFieldNames = "DISABILITY_PLAN.PLAN_ID";
						sJustFieldNamesByNumber = "[PLAN_TEMP].PLAN_ID";
						break;
                    //Shruti Leave Plan Search starts
                    case ConstGlobals.LV_PLAN_SEARCH:   //Leave Plan Search
                        AddUnique(ref arrlstQueryTables, "LEAVE_PLAN");
                        sJustFieldNames = "LEAVE_PLAN.LP_ROW_ID";
                        sJustFieldNamesByNumber = "[LP_PLAN_TEMP].LP_ROW_ID";
                        break;
                    //Shruti Leave Plan Search ends
					case ConstGlobals.AT_SEARCH : //Admin tracking search
						//get AT table name
						sSQL = "SELECT VIEW_AT_TABLE FROM SEARCH_VIEW WHERE VIEW_ID = "+ iViewID;
                        //objConn.Open();
						objReader = objConn.ExecuteReader(sSQL);
						if (objReader.Read())
							sATTable = Conversion.ConvertObjToStr(objReader.GetValue("VIEW_AT_TABLE"));
						else
							sATTable = ""; //abort

						objReader.Close();
                        //objConn.Close();
						if (sATTable == "")
							return false;
						
						//get key field from SUPP_DICTIONARY
						sSQL = "SELECT SYS_FIELD_NAME FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + sATTable + "' AND FIELD_TYPE = 7";
                        //objConn.Open();
						objReader = objConn.ExecuteReader(sSQL);
						if (objReader.Read())
							sATKeyField = Conversion.ConvertObjToStr(objReader.GetValue("SYS_FIELD_NAME"));
						else
							sATKeyField = "";
						objReader.Close();
                        //objConn.Close();
						if (sATKeyField == "") 
							return false;

						//add main table and key field to query
						AddUnique(ref arrlstQueryTables, sATTable);
						sJustFieldNames = sATTable + "." + sATKeyField;   //should be init on every search type
	     
						//Suplimental Multi-Code Field
						sJustFieldNamesByNumber = "[" + sATTable + "_TEMP]." + sATKeyField;
						break;
                    // Start Naresh Enhanced Policy Search
                    case ConstGlobals.ENHANCED_POLICY_SEARCH: //Enhanced Policy search
                        AddUnique(ref arrlstQueryTables, "POLICY_ENH");
                        AddUnique(ref arrlstQueryTables, "POLICY_X_TERM_ENH");
                        sJustFieldNames = "POLICY_ENH.POLICY_ID";
                        sJustFieldNamesByNumber = "[POLICY_ENH_TEMP].POLICY_ID";
                        lPolicyRestrict = Conversion.ConvertStrToLong(oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText);
                        break;
                    // End Naresh Enhanced Policy Search
                    // Anu Tennyson for MITS 18291 : STARTS 10/25/2009
                    case ConstGlobals.PROPERTY_SEARCH:   //Property search
                        AddUnique(ref arrlstQueryTables, "PROPERTY_UNIT");
                        sJustFieldNames = "PROPERTY_UNIT.PROPERTY_ID";
                        sJustFieldNamesByNumber = "[PROPERTY_UNIT_TEMP].PROPERTY_ID";
                        break; 
                    //Anu Tennyson for MITS 18291 : ENDS 
                    // skhare7
                    case ConstGlobals.DIARY_SEARCH:   //Diary Search
                        AddUnique(ref arrlstQueryTables, "WPA_DIARY_ENTRY");
                        sJustFieldNames = "WPA_DIARY_ENTRY.ENTRY_ID";
                        sJustFieldNamesByNumber = "[DIARY_TEMP].ENTRY_ID";
                        break;
                    //skhare7
                    //Start Date : 13-June-2012 Catastrophe Search averma62 -  MITS - 28528
                    case ConstGlobals.CATASTROPHE_SEARCH:
                        AddUnique(ref arrlstQueryTables, "CATASTROPHE");
                        sJustFieldNames = "CATASTROPHE.CATASTROPHE_ROW_ID";
                        sJustFieldNamesByNumber = "[CATASTROPHE_TEMP].CATASTROPHE_ROW_ID";
                        break;
                    //End  Date : 13-June-2012 Catastrophe Search averma62 - MITS - 28528
                    //Amandeep Driver Search  --start
                    case ConstGlobals.DRIVER_SEARCH:
                        AddUnique(ref arrlstQueryTables, "DRIVER");
                        AddUnique(ref arrlstQueryTables, "DRIVER_ENTITY");
                        sJustFieldNames = "DRIVER.DRIVER_ROW_ID";
                        sJustFieldNamesByNumber = "[DRIVER_TEMP].DRIVER_ROW_ID";
                        break;
                    //Amandeep Driver Search  --ends                   
                    //Ashish Ahuja Address //RMA-8753
                    case ConstGlobals.ADDRESS_SEARCH:
                        AddUnique(ref arrlstQueryTables, "ADDRESS");
                        sJustFieldNames = "ADDRESS.ADDRESS_ID";
                        sJustFieldNamesByNumber = "[ADDRESS_TEMP].ADDRESS_ID";
                        break;
				}

				sCriteriaAddendum = sJustFieldNames;   //used for multi searches below

				//Scan query fields & extract value if entered
				
				if (!bLookupOnly)
				{
					//AL Changes as per Adaptor - Sumeet (Following commented line is kept just in case it is required in future)
					objXmlFields = objXml.SelectNodes("//search/searchfields/field");
					//objXmlFields = objXml.SelectNodes("app:search/app:searchfields/app:field", nsSearchDef);    // JP  added namespace manager and prefixes

					if (objXmlFields != null)
					{
						foreach(XmlElement XMLField in objXmlFields)
						{
							sFieldName = XMLField.GetAttribute("name") + "";
							sFieldTable = XMLField.GetAttribute("table") + "";
							sFieldID = XMLField.GetAttribute("id") + "";
							sFieldType = XMLField.GetAttribute("type") + "";
							iFieldType = Conversion.ConvertStrToInteger((XMLField.GetAttribute("fieldtype") + ""));

							//AL Changes as per Adaptor - Sumeet
							if (Conversion.ConvertStrToLong(XMLField.GetAttribute("issupp")) != 0)
								//if (Conversion.ConvertStrToLong(XMLField.GetAttribute("issupp")) > 0)
								bSuppField = true;
							else
								bSuppField = false;
                            //Added Rakhi for R7:Add Emp Data Elements:To prevent Entity_X_Addresses field coming in criteria if the setting is off otherwise throws error and no result is found
                            bSettingOn = CheckUtilSettings(sFieldTable,sFieldName,objSysSettings);
                            if (!bSettingOn)
                            {
                                continue;
                            }
                            //Added Rakhi for R7:Add Emp Data Elements:To prevent Entity_X_Addresses field coming in criteria if the setting is off otherwise throws error and no result is found


							//AL Changes as per Adaptor - Sumeet (Following commented line is kept just in case it is required in future)
							// Locate field element in user submitted criteria
							oXmlTmp = (XmlElement) oXmlUser.SelectSingleNode("//Search/SearchMain/SearchFields/field[@id = '" + sFieldID + "']", nsUserData);
							
							if(oXmlTmp != null)
							{
								switch (sFieldType)
								{
									case Constants.TEXT : //field type "Text"
									
										//AL Changes as per Adaptor - Sumeet (Following commented lines are kept just in case it is required in future)
										//sValue = GetFieldValue(sFieldID);
										//sOperator = GetFieldValue(sFieldID + "op") ;
										sValue = oXmlTmp.LastChild.InnerText.Trim();
                                        //nadim for 14393                                       
                                        if (sValue != "")
                                        {                                           
                                            sTempValue = sValue;
                                            sTempValue = sTempValue.Replace("'", "");
                                            sTempValue = sTempValue.Replace("/[^0-9]/g", "");
                                            sTempValue = Regex.Replace(sTempValue, "[a-zA-Z(^+)|:(+$%)-. \f\n\r\t]", "");
                                        }
                                        //nadim for 14393
										sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();
										if ((sValue != "") && (sOperator != "") && (sValue != sOperator))
										{
											if ((!bSuppField) && (bDoSoundex) && (sFieldName == "LAST_NAME" || sFieldName == "LAST_NAME_CI") && sFieldTable != "FUNDS") //Added support for optional case insensitive version of name
											{
												//Process last name soundex lookup
												bWildCard = false;
												if (sValue.Substring(sValue.Length-1,1) == "*")
													bWildCard = true;

												sValue = sValue.Replace("*", "");
												sValue = Utilities.FindSoundexValue(sValue);
		    
												if (bWildCard)
												{
													sValue = sValue.Replace("0", "") ;   //strip out any 0 paddings (for soundex codes of less than 4 digits)
													sValue = sValue + "%";                //put wildcard back in and change operator to like
													sOperator = "LIKE";
												}
		    
												sValue = SQLTextArg(sValue);
												sFieldName = "LAST_NAME_SOUNDEX";
											}
											else
											{
												//Process normal fields
												sValue = SQLTextArg(sValue);
		    
												//Upcase the search arg. if client has case-insensitive name support on their db (only for Oracle, Sybase, informix db's that don't have case-insensitive query abilities)
												if (sFieldName.Substring(sFieldName.Length-3,3) == "_CI") //Added support for optional case insensitive version of name
													sValue = (sValue.ToUpper());
		  
												//force operator if wildcard
												if (sValue.IndexOf("*",0) > -1)
												{                                
													sOperator = "LIKE";
													sValue = sValue.Replace("*", "%");
												}
												//Nitesh: MITS 7737 Starts: formatted for SSN/TaxID 
												else if( sFieldName.IndexOf("TAX_ID",0) > -1 )
												{
													if(sOperator=="=")
														sOperator = "IN";
													else if (sOperator=="<>")
														sOperator= "NOT IN";

													if(sValue.IndexOf("-",0)>-1)
													{   sValue = sValue.Replace("'","");
														sValue = "(" + SQLTextArg(sValue) + "," + SQLTextArg( sValue.Replace("-", "")) + ")";
													}
													else
													{  sValue=sValue.Replace("'","");
                                                       //mits 9755, calling string.Format on a number fails when taxid begins with 0
                                                    if (sValue.StartsWith("0") && sValue.Length >=5)//&& condition added by Shivendu for MITS 17211
                                                       {
                                                           sValue = "(" + SQLTextArg(sValue.Substring(0, 2) + "-" + sValue.Substring(2)) + "," + SQLTextArg(sValue.Substring(0, 3) + "-" + sValue.Substring(3, 2) + "-" + sValue.Substring(5)) + "," + SQLTextArg(sValue) + ")";
                                                       }
                                                       else
													    sValue = "(" + SQLTextArg(string.Format("{0:###-##-####}", Conversion.ConvertStrToDouble (sValue) )) + "," + SQLTextArg(string.Format("{0:##-#######}",Conversion.ConvertStrToDouble(sValue))) + "," + SQLTextArg(sValue) + ")";
													}
												}
												//Nitesh: MITS 7737 Ends
                                                //Added by nadim  for 14393 and MITS 7247-To convert phone number in specific format for searching,I have added some Or clause because old records are not in proper format.
                                                else if (((sFieldName.IndexOf("PHONE1", 0) > -1) || (sFieldName.IndexOf("PHONE2", 0) > -1) || (sFieldName.IndexOf("FAX_NUMBER", 0) > -1) || (string.Compare(sFieldName, "phone_no", true) == 0)) && (sTempValue.Length >= 10)) //Mits 21590:Added Phone_No
                                                {
                                                        sValue = sValue.Replace("'", "");
                                                    //some of the records are saved like '(123) 456-7898 Ext:',that is why I check the index of Ext also,
                                                        bool bEx = false;
                                                        if ((sValue.IndexOf("ext", 0) > 1) || (sValue.IndexOf("Ext", 0) > 1) || (sValue.IndexOf("EXT", 0) > 1))
                                                        {
                                                            bEx = true;
                                                        }
                                                        else
                                                        {
                                                            bEx = false;
                                                        }
                                                        sValue = sTempValue;                                                   
                                                        string sExt = sValue.Substring(10);                                                      
                                                        if ((sExt != "") || (bEx == true))
                                                        {
                                                            sExt = " Ext:" + sExt;
                                                            bEx = false;
                                                        }
                                                        else
                                                            sExt = "";
                                                        if (sOperator == "=" || sOperator == "<>")
                                                        {
                                                            switch (sOperator)
                                                            {
                                                                case "=":
                                                                    if (sValue.Length > 10)
                                                                    {
                                                                        //some of the records are saved like '(123) 456-7898  Ext:123',means double space beween number and Ext,that is why using IN
                                                                        sOperator = "IN";
                                                                        
                                                                        sValue = "(" + SQLTextArg("(" + sValue.Substring(0, 3) + ")" + " " + sValue.Substring(3, 3) + "-" + sValue.Substring(6, 4) + sExt) + "," + SQLTextArg("(" + sValue.Substring(0, 3) + ")" + " " + sValue.Substring(3, 3) + "-" + sValue.Substring(6, 4) + " " + sExt) + ")";

                                                                    }
                                                                    else
                                                                    {
                                                                        //If a number is having many extension,so it will fetch all the extension of that number.
                                                                        sOperator = "LIKE";
                                                                        sValue = SQLTextArg("%(" + sValue.Substring(0, 3) + ")" + " " + sValue.Substring(3, 3) + "-" + sValue.Substring(6, 4) + "%");
                                                                    }
                                                                    break;
                                                                case "<>":
                                                                    if (sValue.Length > 10)
                                                                    {
                                                                        sOperator = "NOT IN";
                                                                        sValue = "(" + SQLTextArg("(" + sValue.Substring(0, 3) + ")" + " " + sValue.Substring(3, 3) + "-" + sValue.Substring(6, 4) + sExt) + "," + SQLTextArg("(" + sValue.Substring(0, 3) + ")" + " " + sValue.Substring(3, 3) + "-" + sValue.Substring(6, 4) + " " + sExt) + ")";
                                                                    }
                                                                    else
                                                                    {
                                                                        //Dont fetch that number which is having many extensions,if i enter only ten digit number
                                                                        sOperator = "NOT LIKE";
                                                                        sValue = SQLTextArg("%(" + sValue.Substring(0, 3) + ")" + " " + sValue.Substring(3, 3) + "-" + sValue.Substring(6, 4) + "%");
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            sValue = SQLTextArg("(" + sValue.Substring(0, 3) + ")" + " " + sValue.Substring(3, 3) + "-" + sValue.Substring(6, 4) + sExt);
                                                        }
                                                    }
                                            }
                                            //Nadim for 14393 and 7247-End				
											if (sCriteria != "")
												sCriteria = sCriteria + " AND ";

											//for oracle case sens.
											if (iCaseSen == 1)
                                                //MGaba2: MITS 17231:search does not trim the spaces before comparing
												//sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " " + sOperator + " " + sValue.ToUpper();
                                                //SMISHRA54: MITS# 24313: No need to apply the RTRIM and LTRIM in case of like operator
                                                if (string.Equals(sOperator,"LIKE"))
                                                {
                                                    sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " " + sOperator + " " + sValue.ToUpper();
                                                }
                                                else
                                                {
                                                    //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                    //sCriteria = sCriteria + "UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " " + sOperator + " " + sValue.ToUpper();
                                                    sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " " + sOperator + " " + sValue.ToUpper();
                                                }
											else
                                                //MGaba2: MITS 17231:search does not trim the spaces before comparing
												//sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue;
                                                //SMISHRA54: MITS# 24313: No need to apply the RTRIM and LTRIM in case of like operator
                                                if (string.Equals(sOperator, "LIKE"))
                                                {
                                                    sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue;
                                                }
                                                else
                                                {
                                                    //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                    //sCriteria = sCriteria + "LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + "))" + " " + sOperator + " " + sValue;
                                                    sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue;
                                                }

											//Add table to table list (if not already there)
											AddUnique(ref arrlstQueryTables, sFieldTable);
										}
										break;
									case Constants.SSN : //field type "SSN"
										//search for ssn/tax id with or without hyphens

										//AL Changes as per Adaptor - Sumeet (Following commented line is kept just in case it is required in future)
										//sValue = GetFieldValue(sFieldID);
										//sOperator = GetFieldValue(sFieldID + "op");
										sValue = oXmlTmp.LastChild.InnerText.Trim();
										sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();

										if (sValue != "" && sValue != sOperator)
										{
                                            //rsushilaggar MITS 37638 01/13/2015 
											//sValue = SQLTextArg(sValue);
 		               
											//force operator if wildcard
											if (sValue.IndexOf("*",0) > -1)
											{
												sOperator = "LIKE";
												sValue = sValue.Replace("*", "%");
											}

											if (sCriteria != "") 
												sCriteria = sCriteria + " AND ";
                                            //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                            //sCriteria = sCriteria + "(" + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue + " OR " + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue.Replace("-", "") + ") ";
                                            //SMISHRA54: MITS# 24313: No need to apply the RTRIM and LTRIM in case of like operator
                                            //rsushilaggar MITS 37638 01/13/2015 
                                            if (string.Equals(sOperator, "LIKE"))
                                            {
                                                sValue = SQLTextArg(sValue);
                                                sCriteria = sCriteria + "(" + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue + " OR " + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue.Replace("-", "") + ") ";
                                            }
                                            else
                                            {
                                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                //sCriteria = sCriteria + "(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + "))" + " " + sOperator + " " + sValue + " OR " + "LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + "))" + " " + sOperator + " " + sValue.Replace("-", "") + ") ";
                                                //MITS 28481 hlv 6/11/12 begin
                                                string lsTmpValue = sValue.Replace("-", "").Replace("'", "");
                                                if (lsTmpValue.Length == 9)
                                                {
                                                    string lsValue1 = SQLTextArg(lsTmpValue.Substring(0, 3) + "-" + lsTmpValue.Substring(3, 2) + "-" + lsTmpValue.Substring(5));
                                                    string lsValue2 = SQLTextArg(lsTmpValue.Substring(0, 2) + "-" + lsTmpValue.Substring(2));
                                                    sCriteria = sCriteria + "(" + sFieldTable + "." + sFieldName + " IN (" + lsValue1 + ", " + lsValue2 + ", " + SQLTextArg(lsTmpValue) + ") ) ";
                                                }
                                                else
                                                {
                                                    sValue = SQLTextArg(sValue);
                                                    sCriteria = sCriteria + "(" + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue + " OR " + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue.Replace("-", "") + ") ";
                                                }
                                                //MITS 28481 hlv 6/11/12 end
                                            }
                                            //End rsushilaggar MITS 37638 01/13/2015 

											//Rem Add table to table list (if not already there)
											AddUnique(ref arrlstQueryTables, sFieldTable);						
										}
										break;

									case Constants.NUMERIC : //field type "Currency" and "Numeric"

                                        sValue = oXmlTmp.LastChild.InnerText.Replace(",", "").Trim().Replace("$", "");
                                        sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();

                                        if (sValue != "" && sValue != sOperator)
                                        {
                                            if (sCriteria != "")
                                                sCriteria = sCriteria + " AND ";

                                            sValue = string.Format("{0:G}", sValue);
                                            //for oracle case sense.
                                            if (iCaseSen == 1)
                                                //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                //smishra25: Reverting Mona's change for MITS 17231 as LTRIM,RTRIM throws error for float type column if value in table  > 99.99
                                                sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " " + sOperator + " " + sValue.ToUpper();
                                                //sCriteria = sCriteria + "UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " " + sOperator + " " + sValue.ToUpper();
                                            else
                                                //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                //smishra25: Reverting Mona's change for MITS 17231 as LTRIM,RTRIM throws error for float type column if value in table  > 99.99
                                                sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue;
                                                //sCriteria = sCriteria + "LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + "))" + " " + sOperator + " " + sValue;

                                            //Add table to table list (if not already there)
                                            AddUnique(ref arrlstQueryTables, sFieldTable);
                                        }
                                        break;
									case Constants.CURRENCY :

										//AL Changes as per Adaptor - Sumeet (Following commented line is kept just in case it is required in future)
										//sValue = GetFieldValue(sFieldID);
										//sOperator = GetFieldValue(sFieldID + "op");
										//Raman Bhatia.. Removing '$' and ',' signs from the field value as they are not present in database
                                     /*   sValue = oXmlTmp.LastChild.InnerText.Replace("," , "").Trim().Replace("$" , "");
                                        sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();

										if (sValue != "" && sValue != sOperator)
										{
											if (sCriteria != "") 
												sCriteria = sCriteria + " AND ";

											sValue = string.Format("{0:G}",sValue);
											//for oracle case sense.
											if (iCaseSen == 1)
												sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " " + sOperator + " " + sValue.ToUpper();
											else
												sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue;
		    
											//Add table to table list (if not already there)
											AddUnique(ref arrlstQueryTables, sFieldTable);
										}
										break;*/

                                        /*CODE ADDED BY YATHARTH FOR CASE OF CURRENCY
                                         START*/
                                        sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.ToUpper();
                                        //								sValue = oXmlTmp.SelectSingleNode("app:startdate", nsUserData).InnerText;
                                        //								sValueEnd = oXmlTmp.SelectSingleNode("app:enddate", nsUserData).InnerText;
                                        sValue = oXmlTmp.SelectSingleNode("./StartCurrency", nsUserData).InnerText.Trim().Replace("$", "");
                                        sValueEnd = oXmlTmp.SelectSingleNode("./EndCurrency", nsUserData).InnerText.Trim().Replace("$", "");
                                        sValue = sValue.Replace(",", "");
                                        sValueEnd = sValueEnd.Replace(",", "");
                                        if (sValue != "")
                                        {
                                            if (sCriteria != "")
                                                sCriteria = sCriteria + " AND ";

                                            sValue = string.Format("{0:G}", sValue);
                                            sValueEnd = string.Format("{0:G}", sValueEnd);

                                            if (sOperator != "BETWEEN")
                                            {
                                                if (iCaseSen == 1)
                                                    //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                    //smishra25: Reverting Mona's change for MITS 17231 as LTRIM,RTRIM throws error for float type column if value in table  > 99.99
                                                    sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " " + sOperator + " " + sValue.ToUpper() + "";
                                                    //sCriteria = sCriteria + "UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " " + sOperator + " " + sValue.ToUpper() + "";
                                                else
                                                     //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                    //smishra25: Reverting Mona's change for MITS 17231 as LTRIM,RTRIM throws error for float type column if value in table  > 99.99
                                                    sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue.ToUpper() + "";
                                                    //sCriteria = sCriteria + "LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + " " + sOperator + "))" + " " + sValue.ToUpper() + "";
                                            }
                                            else
                                            {
                                                if (sValueEnd != "")
                                                {
                                                    if (iCaseSen == 1)
                                                        //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                        //smishra25: Reverting Mona's change for MITS 17231 as LTRIM,RTRIM throws error for float type column if value in table  > 99.99
                                                        sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " BETWEEN " + sValue.ToUpper() + " AND " + sValueEnd.ToUpper() + "";
                                                        //sCriteria = sCriteria + "UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " BETWEEN " + sValue.ToUpper() + " AND " + sValueEnd.ToUpper() + "";
                                                    else
                                                         //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                        //smishra25: Reverting Mona's change for MITS 17231 as LTRIM,RTRIM throws error for float type column if value in table  > 99.99
                                                        sCriteria = sCriteria + sFieldTable + "." + sFieldName + " BETWEEN " + sValue.ToUpper() + " AND " + sValueEnd.ToUpper() + "";
                                                        //sCriteria = sCriteria + "LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + "))" + " BETWEEN " + sValue.ToUpper() + " AND " + sValueEnd.ToUpper() + "";
                                                }
                                                else
                                                {
                                                    if (iCaseSen == 1)
                                                        //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                        //smishra25: Reverting Mona's change for MITS 17231 as LTRIM,RTRIM throws error for float type column if value in table  > 99.99
                                                        sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " >= " + sValue.ToUpper() + "";
                                                        //sCriteria = sCriteria + "UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " >= " + sValue.ToUpper() + "";
                                                    else
                                                        //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                        //smishra25: Reverting Mona's change for MITS 17231 as LTRIM,RTRIM throws error for float type column if value in table  > 99.99
                                                        sCriteria = sCriteria + sFieldTable + "." + sFieldName + " >= " + sValue.ToUpper() + "";
                                                        //sCriteria = sCriteria + "LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + "))" + " >= " + sValue.ToUpper() + "";
                                                }
                                            }
                                            // Add table to table list (if not already there)
                                            AddUnique(ref arrlstQueryTables, sFieldTable);

                                        }
                                        /*END*/
                                        break;
									case Constants.CODE : //field type "code"

										//AL Changes as per Adaptor - Sumeet (Following commented line is kept just in case it is required in future)
										//For entity search
										//here we need to distinguish the code and org entities
										//								if (sFieldType == "code")
										//								{                    
										//									sValue = GetFieldValue(sFieldID + "code");
										//									sOperator = GetFieldValue(sFieldID + "op");
										//								}
										//								else
										//								{                    
										//									sValue = GetFieldValue(sFieldName + "code");
										//									sOperator = GetFieldValue(sFieldName + "op");
										//								}

										sValue = oXmlTmp.SelectSingleNode("./_cid").InnerText;
										sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();
                                        //jira 2595
                                        string temp=string.Empty;                                        
                                        if (sFieldTable == "WPA_DIARY_ENTRY" && oXmlTmp.InnerText.Contains("Unknown") && (sFieldName == "NON_ROUTE_FLAG" || sFieldName == "NON_ROLL_FLAG"))
                                        {
                                            if (sCriteria == "")
                                            sCriteria = "";
                                        }
                                        else if (Conversion.ConvertStrToDouble(sValue) != 0 && sValue != sOperator) // || (sValue != "" && Convert.ToDouble(sValue) == 0 && (sFieldName == "NON_ROUTE_FLAG" || sFieldName == "NON_ROLL_FLAG") && sValue != sOperator)) //igupta3 Jira 439
										{
                                            
                                            if (sFieldTable == "WPA_DIARY_ENTRY" && (sFieldName == "NON_ROUTE_FLAG" || sFieldName == "NON_ROLL_FLAG"))
                                            {
                                                if (oXmlTmp.InnerText.Contains("Yes"))
                                                {
                                                    temp = "-1";
                                                }
                                                else if (oXmlTmp.InnerText.Contains("No"))
                                                {
                                                    temp = "0";
                                                }
                                            }
                                            else
                                            {
                                                temp = sValue;
                                            }
											if (sCriteria != "") 
												sCriteria = sCriteria + " AND ";
											sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " " + Conversion.ConvertStrToLong(temp);
											//Add table to table list (if not already there)
											AddUnique(ref arrlstQueryTables, sFieldTable);
										}
										break;
									case Constants.ORGH : //field type "orgh"
										//here we need to distinguish the code and org entities

										//AL Changes as per Adaptor - Sumeet (Following commented line is kept just in case it is required in future)
										//								if (sFieldType == "code")
										//								{                    
										//									sValue = GetFieldValue(sFieldID) + "code" + "";
										//									sOperator = GetFieldValue(sFieldID) + "op" + "";
										//								}
										//								else
										//								{                    
										//									sValue = GetFieldValue(sFieldName + "code");
										//									sOperator = GetFieldValue(sFieldName + "op");
										//								}

                                        //Geeta 06/17/08 Starts: Mits 12643                                        
                                        sValue = oXmlTmp.SelectSingleNode("./_cid").InnerText;
                                        sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();                                        
                                        
                                        if (lCatID == ConstGlobals.AT_SEARCH)
                                        {
                                            if (Conversion.ConvertStrToDouble(sValue) != 0 && sValue != sOperator)
                                            {

                                                iATCount = iATCount + 1;
                                                if (sCriteria != "")
                                                    sCriteria = sCriteria + " AND ";                       
                         
                                                sCriteria = sCriteria + sATTable + "." + sFieldName + sOperator + sValue ;
                                                sCriteria = sCriteria + " AND" + " ENTITY" + iATCount + ".ENTITY_ID " + sOperator + sATTable + "." + sFieldName;
                                                
                                                AddUnique(ref arrlstQueryTables, "ENTITY");    
                                                AddUnique(ref arrlstQueryTables, "ENTITY" + iATCount);    
                                                AddUnique(ref arrlstQueryTables, sFieldTable);
                                            }
                                        }
                                        //Geeta 06/17/08 Ends: Mits 12643 
                                        else
                                        {
                                        //if (Conversion.ConvertStrToDouble(sValue) != 0 && sValue != sOperator)
                                        //{
                                        //    if (sCriteria != "") 
                                        //        sCriteria = sCriteria + " AND ";
                                        //    sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " " + Conversion.ConvertStrToLong(sValue);
                                        //    //Add table to table list (if not already there)
                                        //    AddUnique(ref arrlstQueryTables, sFieldTable);
                                        //}
                                        //Added By Abhishek MITS 11341 Start

                                        //MITS 11341 Abhishek
                                        if (!string.IsNullOrEmpty(sValue))
                                        { 
                                            //MGaba2:MITS 20175:Start
                                            //This table Id is taken as an input to function GetOrgHierarchyChilden
                                            //objCache.GetOrgTableId(Conversion.ConvertStrToInteger(sValue));
                                            //sOrgLevel = SetOrgLevel(objCache.GetOrgTableId(Conversion.ConvertStrToInteger(sValue)).ToString());
                                            iOrgTableId =  objCache.GetOrgTableId(Convert.ToInt32(sValue));
                                            sOrgLevel = SetOrgLevel(iOrgTableId.ToString());
                                        }
                                        if (Conversion.ConvertStrToDouble(sValue) != 0 && sValue != sOperator)
                                        {
                                            if (sCriteria != "")
                                                sCriteria = sCriteria + " AND ";
                                            //MGaba2:MITS 13205:09/04/2008:entity supplemental field in your event search
                                            //Commented the following code:it should be handled differently in case of supplemental fields:Start
                                            
                                            //sCriteria = sCriteria + " ORG_HIERARCHY." + sOrgLevel + sOperator + sValue;
                                            //AddUnique(ref arrlstQueryTables, "ORG_HIERARCHY");
                                            //AddUnique(ref arrlstQueryTables, "EVENT");

                                            if (bSuppField==false) //NORMAL FIELD
                                            {//to do for insured_id
                                                if (sFieldName == "INSURED_EID")
                                                {
                                                    sCriteria = sCriteria + sFieldTable + "." + sFieldName + sOperator + sValue; 
                                                }
                                                else
                                                {
                                                    //MGaba2:MITS 20175:Start
                                                    //Client enters data in the Physician department at the Facility level but is unable to run a Physician search with the Facility in the Physician Department field
                                                    //Getting comma separated list of entity_ids of entity and its children when it is selected in the serach criteria 
                                                    sOrgChilren = GetOrgHierarchyChilden(sValue, iOrgTableId);
                                                    sCriteria = sCriteria + sFieldTable + "." + sFieldName + " IN (" + sOrgChilren + ")";

                                                    //Commenting the following code as this join works only when field has only department ids
                                                    //sCriteria = sCriteria + " ORG_HIE." + sOrgLevel + sOperator + sValue;
                                                    //sCriteria = sCriteria + "  AND " + sFieldTable + "." + sFieldName + " = ORG_HIE.DEPARTMENT_EID ";
                                                    //AddUnique(ref arrlstQueryTables, "ORG_HIE");
                                                    //MGaba2:MITS 20175:End
                                                }
                                            }
                                            else
                                            {
                                                sCriteria = sCriteria + sFieldTable + "." + sFieldName + sOperator + sValue; 
                                               
                                            }
                                            //MGaba2:MITS 13205:09/04/2008:End
                                            //Add table to table list (if not already there)
                                            AddUnique(ref arrlstQueryTables, sFieldTable);
                                        }
                                        }
                                        //Abhishek MITS 11341 End
										break;

									case Constants.CHECK_BOX ://field type "checkbox"

										//AL Changes as per Adaptor - Sumeet (Following commented line is kept just in case it is required in future)
										//								sValue = GetFieldValue(sFieldID);
										//								sOperator = GetFieldValue(sFieldID + "op");
										sValue = oXmlTmp.LastChild.InnerText.Trim();
										sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();

										if ((lCatID != ConstGlobals.POLICY_SEARCH) || (lPolicyRestrict == 0) || (sFieldName != "PRIMARY_POLICY_FLG")) //if we have a specific entity search, disable any entity type selections
										{
											if (sValue != "" && sValue != sOperator)
											{
												if (sCriteria != "") 
													sCriteria = sCriteria + " AND ";
												sCriteria = sCriteria + sFieldTable + "." + sFieldName;									
											
												//AL Changes as per Adaptor - Sumeet
												//if (Conversion.ConvertStrToDouble(sValue) > 0)
												if (Conversion.ConvertStrToDouble(sValue) != 0)
												{										
													if (sOperator == "=")
														sCriteria = sCriteria + " <> 0";
													else
														sCriteria = sCriteria + " = 0";
												}
												AddUnique(ref arrlstQueryTables, sFieldTable);
											}
                                            //else if (sValue == sOperator) // Anjaneya : MITS 11695
                                            //{
                                            //    if (sCriteria != "")
                                            //        sCriteria = sCriteria + " AND ";
                                            //    sCriteria = sCriteria + sFieldTable + "." + sFieldName;

                                            //    if (sOperator == "=")
                                            //    {
                                            //        sCriteria = sCriteria + " = 0";
                                            //    }
                                            //    else
                                            //    {
                                            //        sCriteria = sCriteria + " <> 0";
                                            //    }
                                            //    AddUnique(ref arrlstQueryTables, sFieldTable);
                                            //}
										}
										break;
			
									case Constants.DATE : //field type "date"

										//AL Changes as per Adaptor - Sumeet (Following commented lines are kept just in case it is required in future)
										//								sValue = GetFieldValue(sFieldID + "start");
										//								sValueEnd = GetFieldValue(sFieldID + "end");
										//								sOperator = GetFieldValue(sFieldID + "op").ToUpper();
										sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.ToUpper();
										//								sValue = oXmlTmp.SelectSingleNode("app:startdate", nsUserData).InnerText;
										//								sValueEnd = oXmlTmp.SelectSingleNode("app:enddate", nsUserData).InnerText;
										sValue = oXmlTmp.SelectSingleNode("./StartDate", nsUserData).InnerText.Trim();
										sValueEnd = oXmlTmp.SelectSingleNode("./EndDate", nsUserData).InnerText.Trim();

										if (sValue != "")
										{
											if (Utilities.IsDate(sValue))
											{
												if (sCriteria != "")
													sCriteria = sCriteria + " AND ";

												if (sOperator != "BETWEEN")
												{                               
													if (iCaseSen == 1)
                                                        //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                        //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
														//sCriteria = sCriteria + "UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " " + sOperator + " '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "'";
                                                        sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " " + sOperator + " '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "'";
													else
                                                        //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                        //sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "'";
                                                        //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                        //sCriteria = sCriteria + "LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + "))" + " " + sOperator + " '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "'";
                                                        sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "'";
												}
												else
												{
													if (sValueEnd != "")
													{                                    
														if (iCaseSen == 1)
                                                            //MGaba2: MITS 17231:search does not trim the spaces before comparing
															//sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " BETWEEN '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "' AND '" + Conversion.ToDbDate(DateTime.Parse(sValueEnd)) + "'";
                                                            //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                            //sCriteria = sCriteria + "UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " BETWEEN '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "' AND '" + Conversion.ToDbDate(DateTime.Parse(sValueEnd)) + "'";
                                                            sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " BETWEEN '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "' AND '" + Conversion.ToDbDate(DateTime.Parse(sValueEnd)) + "'";
														else
                                                            //MGaba2: MITS 17231:search does not trim the spaces before comparing
															//sCriteria = sCriteria + sFieldTable + "." + sFieldName + " BETWEEN '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "' AND '" + Conversion.ToDbDate(DateTime.Parse(sValueEnd)) + "'";
                                                            //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                            //sCriteria = sCriteria + "LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + "))" + " BETWEEN '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "' AND '" + Conversion.ToDbDate(DateTime.Parse(sValueEnd)) + "'";
                                                            sCriteria = sCriteria + sFieldTable + "." + sFieldName + " BETWEEN '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "' AND '" + Conversion.ToDbDate(DateTime.Parse(sValueEnd)) + "'";
													}
													else
													{
														if (iCaseSen == 1)
                                                            //MGaba2: MITS 17231:search does not trim the spaces before comparing
															//sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " >= '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "'";
                                                            //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                            //sCriteria = sCriteria + "UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " >= '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "'";
                                                            sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " >= '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "'";
														else
                                                            //MGaba2: MITS 17231:search does not trim the spaces before comparing
															//sCriteria = sCriteria + sFieldTable + "." + sFieldName + " >= '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "'";
                                                            //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                            //sCriteria = sCriteria + "LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + "))" + " >= '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "'";
                                                            sCriteria = sCriteria + sFieldTable + "." + sFieldName + " >= '" + Conversion.ToDbDate(DateTime.Parse(sValue)) + "'";
													}
												}                            
												// Add table to table list (if not already there)
												AddUnique(ref arrlstQueryTables, sFieldTable);
											}
										}
										break;
									case Constants.TIME : //field type "time"
										//AL Changes as per Adaptor - Sumeet (Following commented line is kept just in case it is required in future)
										//								sValue = GetFieldValue(sFieldID + "start");
										//								sValueEnd = GetFieldValue(sFieldID + "end");
										//								sOperator = GetFieldValue(sFieldID + "op").ToUpper();
										sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim().ToUpper();
										//								sValue = oXmlTmp.SelectSingleNode("app:starttime", nsUserData).InnerText; 
										//								sValueEnd = oXmlTmp.SelectSingleNode("app:endtime", nsUserData).InnerText; 

										sValue = oXmlTmp.SelectSingleNode("./StartTime", nsUserData).InnerText.Trim();  
										sValueEnd = oXmlTmp.SelectSingleNode("./EndTime", nsUserData).InnerText.Trim();  

										if (sValue != "")
										{
											if (Utilities.IsDate(sValue))
											{
												if (sCriteria != "") 
													sCriteria = sCriteria + " AND ";                            
												if (sOperator != "BETWEEN")
												{                              
													if (iCaseSen == 1)
                                                        //MGaba2: MITS 17231:search does not trim the spaces before comparing
														//sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " " + sOperator + " '" + Conversion.GetTime(sValue) + "'";
                                                        //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                        //sCriteria = sCriteria + "UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " " + sOperator + " '" + Conversion.GetTime(sValue) + "'";
                                                        sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " " + sOperator + " '" + Conversion.GetTime(sValue) + "'";
													else
                                                        //MGaba2: MITS 17231:search does not trim the spaces before comparing
														//sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " '" + Conversion.GetTime(sValue) + "'";
                                                        //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                        //sCriteria = sCriteria + "LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + "))" + " " + sOperator + " '" + Conversion.GetTime(sValue) + "'";
                                                        sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " '" + Conversion.GetTime(sValue) + "'";
												}
												else
												{                                
													if (sValueEnd != "")
													{                                 
														if (iCaseSen == 1)
                                                            //MGaba2: MITS 17231:search does not trim the spaces before comparing
															//sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " BETWEEN '" + Conversion.GetTime(sValue) + "' AND '" + Conversion.GetTime(sValueEnd) + "'";
                                                            //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                            //sCriteria = sCriteria + "UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " BETWEEN '" + Conversion.GetTime(sValue) + "' AND '" + Conversion.GetTime(sValueEnd) + "'";
                                                            sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " BETWEEN '" + Conversion.GetTime(sValue) + "' AND '" + Conversion.GetTime(sValueEnd) + "'";
														else
                                                            //MGaba2: MITS 17231:search does not trim the spaces before comparing
															//sCriteria = sCriteria + sFieldTable + "." + sFieldName + " BETWEEN '" + Conversion.GetTime(sValue) + "' AND '" + Conversion.GetTime(sValueEnd) + "'";
                                                            //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                            sCriteria = sCriteria + sFieldTable + "." + sFieldName + " BETWEEN '" + Conversion.GetTime(sValue) + "' AND '" + Conversion.GetTime(sValueEnd) + "'";
													}
													else
													{										
														if (iCaseSen == 1)
															//MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                            //sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " >= '" + Conversion.GetTime(sValue) + "'";
                                                            //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                            //sCriteria = sCriteria + "UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " >= '" + Conversion.GetTime(sValue) + "'";
                                                            sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " >= '" + Conversion.GetTime(sValue) + "'";
														else
                                                            //MGaba2: MITS 17231:search does not trim the spaces before comparing
															//sCriteria = sCriteria + sFieldTable + "." + sFieldName + " >= '" + Conversion.GetTime(sValue) + "'";
                                                            //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                            //sCriteria = sCriteria + "LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + "))" + " >= '" + Conversion.GetTime(sValue) + "'";
                                                            sCriteria = sCriteria + sFieldTable + "." + sFieldName + " >= '" + Conversion.GetTime(sValue) + "'";
													}
												}
											}
											//Add table to table list (if not already there)
											AddUnique(ref arrlstQueryTables, sFieldTable);
										}
										break;
									case Constants.TABLE_LIST : //field type "tablelist"
										//AL Changes as per Adaptor - Sumeet (Following commented line is kept just in case it is required in future)
										//								sValue = GetFieldValue(sFieldID);
										//								sOperator = GetFieldValue(sFieldID + "op");

										sValue = oXmlTmp.LastChild.InnerText.Trim();
										sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();

										if(sValue == "" || sValue == "=")
										{
											sEntityType = oXmlUser.SelectSingleNode("//Search/SearchMain/TableID").InnerText.Trim();
											if(sEntityType != "")
											{
												sSQL = "select TABLE_ID from Glossary where system_table_name = '" + sEntityType + "'";
                                                //objConn.Open();
												objReader = objConn.ExecuteReader(sSQL);
												if (objReader.Read())
												{
													sValue = Conversion.ConvertObjToStr(objReader.GetValue("TABLE_ID"));
													sOperator = "=";
												}
												else
												{
													oXmlTableList = (XmlElement) XMLField.SelectSingleNode("//search/searchfields/field[@id = '" + sFieldID + "']", nsUserData);
													objXmlTempFields = oXmlTableList.SelectNodes("//tablevalue");
													foreach(XmlElement objXMLTempField in objXmlTempFields)
													{
														if(objXMLTempField.InnerText.ToUpper() == sEntityType.ToUpper())
														{
															sValue = objXMLTempField.GetAttribute("tableid");
															sOperator = "=";
														}
													}
												}
												objReader.Close();
                                                //objConn.Close();
											}
                                            //MITS: 22214/25272 rsushilaggar Date 06/22/2011
                                            else
                                            {
                                                sEntityType = oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText;
                                                if (sEntityType == "PHYSICIANS,MEDICAL_STAFF,EMPLOYEES")
                                                {
                                                    string sValues = string.Empty;
                                                    string sOpt = string.Empty;
                                                    objXmlTempFields = oXmlUser.SelectNodes("//tablevalue");
                                                    foreach (XmlElement objXMLTempField in objXmlTempFields)
                                                    {
                                                        if (sValues != "")
                                                            sValues = sValues + ",";
                                                        sValues = sValues + objXMLTempField.GetAttribute("tableid");
                                                        sOpt = "IN";
                                                    }
                                                    if (sCriteria != "")
                                                        sCriteria = sCriteria + " AND ";
                                                    sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOpt + " " + "(" + sValues + ")";
                                                }
                                            }
                                            //MITS: 22214/25272
											oXmlTableList = null;
											objXmlTempFields = null;
										}//end if(sValue == "" || sValue == "=")

										if ((lCatID != ConstGlobals.ENTITY_SEARCH) || (lEntityTableRestrict <= 0) || (sFieldName != "ENTITY_TABLE_ID")) //if we have a specific entity search, disable any entity type selections
										{
											if (Conversion.ConvertStrToDouble(sValue) != 0 && sValue != sOperator)
											{
												if (sCriteria != "") 
													sCriteria = sCriteria + " AND ";
												sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " " + Conversion.ConvertStrToDouble(sValue);

												// Mihika 16-Feb-2006 MITS 4860 changes
												if ((sFieldTable == "ORG_ENTITY") && sFieldName == "ENTITY_TABLE_ID")
													sOrgLevel = SetOrgLevel(sValue);
											}
											//Add table to table list (if not already there)
											AddUnique(ref arrlstQueryTables, sFieldTable);
										}
										break;
									case Constants.TEXTML ://"textml" : //TextXML
                                    case Constants.FREECODE ://MGaba2:MITS 16476 Added search for Multi Text/Codes field type in supplementals
										//AL Changes as per Adaptor - Sumeet (Following commented line is kept just in case it is required in future)
										//								sValue = GetFieldValue(sFieldID);
										//								sOperator = GetFieldValue(sFieldID + "op");

										sValue = oXmlTmp.LastChild.InnerText.Trim();
										sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();
										if (sValue != "" && sValue != sOperator)
										{
											sValue = SQLTextArg(sValue);
											//operator is always LIKE for free text 
											sOperator = "LIKE";
										
											if (sValue.IndexOf("*",0) > -1) 
												sValue = sValue.Replace("*", "%");
											if (sCriteria != "") 
												sCriteria = sCriteria + " AND ";

                                            if (XMLField.HasAttribute("subtable"))  // csingh7 R6 Claim Comment enhancement
                                            {
                                                sSubTable = XMLField.GetAttribute("subtable");
                                                sFieldTable = sFieldTable + sSubTable;
                                            }                                                                                  
											//for oracle case sense.
                                            if (iCaseSen == 1)
                                            {
                                                //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                //sCriteria = sCriteria + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " " + sOperator + " " + sValue.ToUpper();                                                
                                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                //sCriteria = sCriteria + "(UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " " + sOperator + " " + sValue.ToUpper() + " OR " + "UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + " " + sOperator + " " + sValue.Insert(sValue.Length - 1, "\r\n").ToUpper() + ")";
                                                sCriteria = sCriteria + "(UPPER(" + sFieldTable + "." + sFieldName + ")" + " " + sOperator + " " + sValue.ToUpper() + " OR " + "UPPER(" + sFieldTable + "." + sFieldName + ")" + " " + sOperator + " " + sValue.Insert(sValue.Length - 1, "\r\n").ToUpper() + ")";                                            
                                            }
                                            else
                                            {   //MGaba2: MITS 17231:search does not trim the spaces before comparing
                                                //sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue;
                                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                                if (m_sDBType == Constants.DB_ORACLE)
                                                    sCriteria = sCriteria + "(" + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue + " OR " + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue.Insert(sValue.Length - 1, "\r\n") + ")";
                                                else
                                                    sCriteria = sCriteria + "(" + sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue + " OR "+ sFieldTable + "." + sFieldName + " " + sOperator + " " + sValue.Insert(sValue.Length-1,"\r\n") + ")";

                                            }
											//Add table to table list (if not already there)
											AddUnique(ref arrlstQueryTables, sFieldTable);
										}
										break;
									case Constants.CODE_LIST : //field type "codelist" & "multicode/multistate/multientity"                                       
                                        //MITS 10702 by Gagan : Start
                                        switch (iFieldType)
                                        {
                                            case 14: break;
                                            case 15: break;
                                            case 16:
                                                if (!bSuppField)
                                                    throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ResultList.DoSearch.SupplementalError", m_iClientId),sLangId) + sFieldType);
                                                break;
                                            default:
                                                throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ResultList.DoSearch.FieldError", m_iClientId), sLangId) + sFieldType);
                                        }
                                        sValue = oXmlTmp.SelectSingleNode("./_lst").InnerText.Trim();
										sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();

                                        if (bSuppField)
                                        {
                                            if (Conversion.ConvertStrToDouble(sValue) != 0)
                                            {
                                                //"NOT IN" does not work as user would expect; it adds inner join to supp_multi_value, thus removing all from result set that lacks records here.  removed <> from the xml per Jim P. instruction
                                                if (sOperator == "=")
                                                    sOperator = "IN";
                                                else if (sOperator == "<>")
                                                    sOperator = "NOT IN";

                                                sAlias = "SMV" + Conversion.ConvertStrToDouble(sFieldID.Substring(5)); // Mihika 16-Feb-2006 Changed the index in substring
                                                if (sCriteria != "")
                                                    sCriteria = sCriteria + " AND ";
                                                sTmp = "(" + sValue + ")";
                                                sCriteria = sCriteria + "(" + sAlias + ".CODE_ID " + sOperator + " " + sTmp + " AND " + sAlias + ".FIELD_ID = " + Conversion.ConvertStrToDouble(sFieldID.Substring(5));
                                                sCriteria = sCriteria + " AND " + sAlias + ".RECORD_ID = " + sCriteriaAddendum + ")";
                                                //Add table to table list (if not already there)
                                                AddUnique(ref arrlstQueryTables, "SUPP_MULTI_VALUE " + sAlias);
                                            }
                                        }
                                        else
                                        {
                                            if (Conversion.ConvertStrToDouble(sValue) != 0)
                                            {
                                                if (sOperator == "=")
                                                    sOperator = "IN";
                                                else if (sOperator == "<>")
                                                    sOperator = "NOT IN";

                                                if (sCriteria != "")
                                                    sCriteria = sCriteria + " AND ";
                                                sTmp = "(" + sValue + ")";

                                                sCriteria = sCriteria + sFieldTable + "." + sFieldName + " " + sOperator + " " + sTmp;
                                                //Add table to table list (if not already there)
                                                AddUnique(ref arrlstQueryTables, sFieldTable);
                                            }
                                        }
                                        break;
                                    //MITS 10702 by Gagan : End
									case Constants.ENTITY_LIST ://"entitylist" :
									switch(iFieldType)
									{
										case 14 :
										case 15 :
										case 16 :
											if (!bSuppField)
												throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ResultList.DoSearch.SupplementalError", m_iClientId),sLangId)  + sFieldType);
											break;                          
										default :
                                            throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ResultList.DoSearch.FieldError", m_iClientId), sLangId) + sFieldType);
									}

										//AL Changes as per Adaptor - Sumeet
										//								sValue = GetFieldValue(sFieldName + "_lst"); //ids for items added to a select and stored in [select name]_lst as comma-del string
										//								sOperator = GetFieldValue(sFieldID + "op");
										sValue = oXmlTmp.SelectSingleNode("./_lst").InnerText.Trim();
										sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();
										if (Conversion.ConvertStrToDouble(sValue) != 0)
										{	
											//"NOT IN" does not work as user would expect; it adds inner join to supp_multi_value, thus removing all from result set that lacks records here.  removed <> from the xml per Jim P. instruction
											if (sOperator == "=")
												sOperator = "IN";
											else if (sOperator == "<>")
												sOperator = "NOT IN";                        
										
											sAlias = "SMV" + Conversion.ConvertStrToDouble(sFieldID.Substring(5)); // Mihika 16-Feb-2006 Changed the index in substring
											if (sCriteria != "")
												sCriteria = sCriteria + " AND ";
											sTmp = "(" + sValue + ")";
											sCriteria = sCriteria + "(" + sAlias + ".CODE_ID " + sOperator + " " + sTmp + " AND " + sAlias + ".FIELD_ID = " + Conversion.ConvertStrToDouble(sFieldID.Substring(5));
											sCriteria = sCriteria + " AND " + sAlias + ".RECORD_ID = " + sCriteriaAddendum + ")";
											//Add table to table list (if not already there)
											AddUnique(ref arrlstQueryTables, "SUPP_MULTI_VALUE " + sAlias);
										}
										break;
                                    case Constants.ATTACHEDRECORD: //field type "attachedrecord"

                                     //skhare7
                                        sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.ToLower();
                                      
                                        sValue = oXmlTmp.LastChild.InnerText.Trim();
                                        if(sValue == string.Empty)
                                            sAttachedRecord = sFieldName;
                                        if (sValue != string.Empty)
                                        {
                                            if (sOperator.CompareTo("claim") == 0)
                                            {
                                              
                                                sFieldName = "CLAIM_NUMBER";
                                              
                                            }
                                            else if (sOperator.CompareTo("entity") == 0)
                                            {
                                                
                                                sFieldName = "LAST_NAME";
                                                AddUnique(ref arrlstQueryTables, "ENTITY");

                                            }
                                            else if (sOperator.CompareTo("claimant") == 0)
                                            {
                                               
                                                sFieldName = "LAST_NAME";
                                                AddUnique(ref arrlstQueryTables, "CLAIMANT");

                                            }
                                           
                                            else if (sOperator.CompareTo("event") == 0)
                                            {
                                               
                                                sFieldName = "EVENT_NUMBER";
                                            

                                            }
                                            else if (sOperator.CompareTo("funds") == 0)
                                            {
                                               
                                                sFieldName = "CTL_NUMBER";
                                             

                                            }
                                            else if (sOperator.CompareTo("pi") == 0)
                                            {
                                                sFieldName = "LAST_NAME";
                                                AddUnique(ref arrlstQueryTables, "PERSON_INVOLVED");

                                            }
                                            else if (sOperator.CompareTo("policy") == 0)
                                            {
                                               
                                                sFieldName = "POLICY_NUMBER";
                                             

                                            }
                                            else if (sOperator.CompareTo("policyenh") == 0)
                                            {

                                               
                                                sFieldName = "POLICY_NAME";
                                             

                                            }
                                            sAttachedRecord = sFieldName;
                                            AddUnique(ref arrlstQueryTables, sFieldTable);
                                            if (sValue != "")
                                            {
                                                if (sValue != "" && sValue != sOperator)
                                                {
                                                    /*rkulavil: RMA-6344 start*/
                                                    //sValue = SQLTextArg(sValue);
                                                    /*rkulavil: RMA-6344 end*/
                                                    if (sValue.Contains("'"))
                                                    {
                                                        sValue = sValue.Replace("'", "");
                                                    }
                                                   


                                                    if (sCriteria != "")
                                                    {
                                                        sCriteria = sCriteria + " AND ";

                                                    }



                                                    sOperator = "LIKE";

                                                    if (sValue.IndexOf("*", 0) > -1)
                                                    {
                                                        sValue = sValue.Replace("*", "%");
                                                    }
                                                    else
                                                        sValue = "%" + sValue + "%";

                                                    // else
                                                    if (iCaseSen == 1)
                                                        sCriteria = sCriteria + "(UPPER(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + "))))" + sOperator + " N'" + (sValue) + "'"; //rkulavil: RMA-6344
                                                    else
                                                        sCriteria = sCriteria + "(LTRIM(RTRIM(" + sFieldTable + "." + sFieldName + ")))" + sOperator + " N'" + (sValue) + "'"; //rkulavil: RMA-6344


                                                }
                                            }
                                            
                                        }
                                        break;
                                        // npadhy Jira 6415 Handle the User lookup foe Query Tables
                                    case Constants.USERLOOKUP:
                                        sValue = oXmlTmp.SelectSingleNode("./_lst").InnerText.Trim();
										sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();
										if (Conversion.ConvertStrToDouble(sValue) != 0)
										{	
											//"NOT IN" does not work as user would expect; it adds inner join to supp_multi_value, thus removing all from result set that lacks records here.  removed <> from the xml per Jim P. instruction
											if (sOperator == "=")
												sOperator = "IN";
											else if (sOperator == "<>")
												sOperator = "NOT IN";                        
										
											sAlias = "SMV" + Conversion.ConvertStrToDouble(sFieldID.Substring(5)); // Mihika 16-Feb-2006 Changed the index in substring
											if (sCriteria != "")
												sCriteria = sCriteria + " AND ";
											sTmp = "(" + sValue + ")";
											sCriteria = sCriteria + "(" + sAlias + ".CODE_ID " + sOperator + " " + sTmp + " AND " + sAlias + ".FIELD_ID = " + Conversion.ConvertStrToDouble(sFieldID.Substring(5));
											sCriteria = sCriteria + " AND " + sAlias + ".RECORD_ID = " + sCriteriaAddendum + ")";
											//Add table to table list (if not already there)
											AddUnique(ref arrlstQueryTables, "SUPP_MULTI_VALUE " + sAlias);
										}
                                        break;
                                    // akaushik5 Added for MITS 38161 Starts
                                    case Constants.ENTITY:
                                        sValue = oXmlTmp.SelectSingleNode("./_cid").InnerText;
                                        sOperator = oXmlTmp.SelectSingleNode("./Operation").InnerText.Trim();

                                        if (Conversion.ConvertStrToDouble(sValue) != 0 && sValue != sOperator)
                                        {
                                            if (lCatID == ConstGlobals.AT_SEARCH)
                                            {
                                                iATCount++;
                                                if (!string.IsNullOrWhiteSpace(sCriteria))
                                                {
                                                    sCriteria = string.Format("{0} AND ", sCriteria);
                                                }

                                                sCriteria = string.Format("{0}{1}.{2} {3} {4}", sCriteria, sATTable, sFieldName, sOperator, sValue);
                                                sCriteria = string.Format("{0} AND ENTITY{1}.ENTITY_ID = {2}.{3}", sCriteria, iATCount, sATTable, sFieldName);

                                                AddUnique(ref arrlstQueryTables, "ENTITY");
                                                AddUnique(ref arrlstQueryTables, "ENTITY" + iATCount);
                                                AddUnique(ref arrlstQueryTables, sFieldTable);
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrWhiteSpace(sCriteria))
                                                {
                                                    sCriteria = string.Format("{0} AND ", sCriteria);
                                                }

                                                sCriteria = string.Format("{0}{1}.{2} {3} {4}", sCriteria, sFieldTable, sFieldName, sOperator, sValue);

                                                AddUnique(ref arrlstQueryTables, sFieldTable);
                                            }
                                        }
										break;
                                    // akaushik5 Added for MITS 38161 Ends
								}//end switch (sFieldType)
							}//end if(oXmlTmp != null)
						}//end foreach(XmlElement XMLField in objXmlFields)
					}//end if (objXmlFields != null)
				}//end if (!bLookupOnly)

				//Now run through display fields

				//AL Changes as per Adaptor - Sumeet
				objXmlFields = objXml.SelectNodes("search/displayfields/field");
				//objXmlFields = objXml.SelectNodes("app:search/app:displayfields/app:field", nsSearchDef);

				iNumDisplay = objXmlFields.Count;
				arrlstColumnHeaders = new ArrayList(iNumDisplay);
				arrlstFieldTypes = new ArrayList(iNumDisplay);

				iCnt = 0; orderBy[0] = 0; orderBy[1] = 0; orderBy[2] = 0;

				foreach(XmlElement XMLField in objXmlFields)
				{
					iCnt = iCnt + 1;
					sFieldName = XMLField.GetAttribute("name") + "";

					if (XMLField.FirstChild.NodeType == XmlNodeType.Text) 
						sFieldDesc = XMLField.FirstChild.Value; //Element nodes have text in child node

					sFieldTable = XMLField.GetAttribute("table");
                    //Added Rakhi for R7:Add Emp Data Elements:To Prevent Entity_X_Addresses fields coming in display if the setting is off otherwise throws error and no result is found
                    bSettingOn = CheckUtilSettings(sFieldTable,sFieldName,objSysSettings);
                    if (!bSettingOn)
                    {
                        continue;
                    }
                    //Added Rakhi for R7:Add Emp Data Elements:To Prevent Entity_X_Addresses fields coming in display if the setting is off otherwise throws error and no result is found
					sFieldID = XMLField.GetAttribute("id");
					sFieldType = XMLField.GetAttribute("type");
                    if (XMLField.HasAttribute("subtable"))      // csingh7 R6 Claim Comment enhancement
                    {
                        sSubTable = XMLField.GetAttribute("subtable");
                        sFieldTable = sFieldTable + sSubTable;
                    }
					iFieldType = Conversion.ConvertStrToInteger(XMLField.GetAttribute("fieldtype"));

					if (Conversion.ConvertStrToDouble(XMLField.GetAttribute("issupp"))!= 0)
						bSuppField = true;
					else
						bSuppField = false;

					sFieldCodeTable = XMLField.GetAttribute("codetable");

                    //pmittal5 Mits 18356 11/13/09 - Add Free text type supplemental fields into the arrarylist
                    // akaushik5 Changed for MITS 36043 Starts
                    //if ((sFieldType == "textml" && bSuppField == true) || (sFieldType == "text" && bSuppField == true))
                        //AddUnique(ref arrlstTextSuppFields, sFieldTable + "." + sFieldName);
                    if (bSuppField)
                    {
                        if (sFieldType.Equals("textml") || sFieldType.Equals("text"))
                        {
                            sSQL = string.Format("SELECT FIELD_TYPE FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '{0}' AND SYS_FIELD_NAME = '{1}'", sFieldTable, sFieldName);
                            int fieldType = default(int);
                            using (DbReader objDBReader = DbFactory.GetDbReader(this.m_sConnectionString, sSQL))
                            {
                                if (objDBReader.Read())
                                {
                                    fieldType = objDBReader.GetInt("FIELD_TYPE");
                                    if (fieldType.Equals((int)SupplementalFieldTypes.SuppTypeMemo) || fieldType.Equals((int)SupplementalFieldTypes.SuppTypeFreeText))
                                    {
                                        AddUnique(ref arrlstTextSuppFields, sFieldTable + "." + sFieldName);
                                    }
                                }
                            }
                        }
                    }
                    // akaushik5 Changed for MITS 36043 Ends
                    //End - pmittal5

					if (!bLookupOnly)
					{

						//AL Changes as per Adaptor - Sumeet (Following commented line is kept just in case it is required in future)
						//Determine order by fields					
						//						if ((sFieldID) == (GetFieldValue("orderby1")))
						//							orderBy[1] = iCnt + 1;
						//						else if (sFieldID == (GetFieldValue("orderby2")))
						//							orderBy[2] = iCnt + 1;
						//						else if (sFieldID == (GetFieldValue("orderby3")))
						//							orderBy[3] = iCnt + 1;

						if ((sFieldID) == (oXmlUser.SelectSingleNode("//OrderBy1").InnerText.Trim()))
							orderBy[0] = iCnt + 1;
						else if (sFieldID == (oXmlUser.SelectSingleNode("//OrderBy2").InnerText.Trim()))
							orderBy[1] = iCnt + 1;
						else if (sFieldID == (oXmlUser.SelectSingleNode("//OrderBy3").InnerText.Trim()))
							orderBy[2] = iCnt + 1;
					}

					//For Multi-Code, Multi-State, Multi-Entity
					bMultiValues = false;
					if (bSuppField)
					{					
						switch (iFieldType)
						{
							case 14 :
							case 15 :
							case 16:
                                // npadhy JIRA 6415 Handled the case for User Lookup
                            case 22:
								sFieldTable = "SMV" + (Conversion.ConvertStrToDouble(sFieldID.Substring(5))); // Mihika 16-Feb-2006 Changed the index in substring
								sFieldName = "CODE_ID";
								bMultiValues = true;
								break;
						}
					}

					//Gather names and types for final display list
					if (sJustFieldNames != "") 
						sJustFieldNames = sJustFieldNames + ",";

					sJustFieldNames = sJustFieldNames + sFieldTable + "." + sFieldName;
					//this sJustxxxx var is for complex searches like CLAIM & EVENT where field name duplication may occur - thus we alias columns to their ordinal position
					if (sJustFieldNamesByNumber != "") 
					{
						sJustFieldNamesByNumber = sJustFieldNamesByNumber + ",";
					}
					sJustFieldNamesByNumber = sJustFieldNamesByNumber + "FLD" + iCnt;
					
					arrlstColumnHeaders.Add (sFieldDesc);
                    if (sFieldCodeTable.ToUpper() == "STATES")
                        arrlstFieldTypes.Add("state");
                    else if (sFieldType.ToLower() == Constants.USERLOOKUP)
                        arrlstFieldTypes.Add(sFieldType + '|' + XMLField.GetAttribute("usergroups"));
                    else
                        arrlstFieldTypes.Add(sFieldType);

					//For Multi-Code, Multi-State, Multi-Entity
					string sFieldHolder = "";
					
					if(!bMultiValues)
						sFieldHolder = sFieldTable;
					else
						sFieldHolder = "SUPP_MULTI_VALUE " + sFieldTable;

					if (Utilities.ExistsInArray(arrlstQueryTables,sFieldHolder))
					{//skhare7 
                        if (sFieldHolder.CompareTo("ATTACH_RECORDTABLE") == 0)
                        {
                            sFieldName = sAttachedRecord;

                        }
						if (sQueryFields != "") 
							sQueryFields = sQueryFields + ",";
						//Item can be piggy backed onto criteria query
                        //avipinsrivas Start : Worked for Jira-340
                        if(objSysSettings.UseEntityRole && string.Equals(sFieldTable.ToUpper(), "ENTITY") && string.Equals(sFieldName.ToUpper(), "ENTITY_TABLE_ID"))
                            sQueryFields = sQueryFields + "ENTITY_X_ROLES." + sFieldName + " | FLD" + iCnt.ToString().Trim();
                        else
                            sQueryFields = sQueryFields + sFieldTable + "." + sFieldName + " | FLD" + iCnt.ToString().Trim();
                        //avipinsrivas End
					}
					else
					{//BSB 02.02.2006 Changed to correctly handle parrallell arrays.
					// Was a problem if supp multi-value search criteria field was present
					// previously this book-keeping code was duplicated (incorrectly) for supp multi-value case.
							 
						//Item not in criteria - must be obtained separately
						string sTmpFieldTable = ((bSuppField && bMultiValues) ? "SUPP_MULTI_VALUE " : "")+ sFieldTable;
						AddUnique(ref arrlstDisplayTables, sTmpFieldTable);
						if ((arrlstDisplayTables.Count) != (arrlstDisplayFields.Count)) 
						{
							//Grow Capacity as Needed
							if(arrlstDisplayFields.Capacity<arrlstDisplayTables.Count)
								arrlstDisplayFields.Capacity = arrlstDisplayTables.Count;
							//Populate As Needed
							for (int iCount=arrlstDisplayFields.Count;iCount<arrlstDisplayTables.Count;iCount++)
								arrlstDisplayFields.Add("");
						}
						iIndex = Utilities.IndexInArray(arrlstDisplayTables, sTmpFieldTable);
						
						if (Conversion.ConvertObjToStr(arrlstDisplayFields[iIndex]) != "")
							arrlstDisplayFields[iIndex] = arrlstDisplayFields[iIndex] + ",";
						arrlstDisplayFields[iIndex] = arrlstDisplayFields[iIndex] + sFieldTable + "." + sFieldName + " | FLD" + iCnt.ToString().Trim();
					}
				}
				//See if order by fields were selected and if so, create the SQL Order By clause
				sOrderBy = "";
				if (orderBy[0] != 0)
				{
                    //PJS MITS 15829 - for numeric & currency no UPPER should done otherwise it will compare as string
                    if (arrlstFieldTypes[Convert.ToInt32(orderBy[0]) - 2].ToString() == "numeric" || arrlstFieldTypes[Convert.ToInt32(orderBy[0]) - 2].ToString() == "currency")
                        //upper added by Shivendu to remove case sensitivity effect Databases
                        sOrderBy = sOrderBy + " ORDER BY FLD" + (orderBy[0] - 1);
                    //pmittal5 Mits 16820 10/08/09 - Sort on the basis of Short_code in case Order by clause is given on Code type field
                    else if (arrlstFieldTypes[Convert.ToInt32(orderBy[0]) - 2].ToString() == "code")
                        sOrderBy = sOrderBy + " ORDER BY UPPER(CD" + (orderBy[0] - 1) + ".SHORT_CODE)";
                    //Sort on the basis of State Id in case Order by clause is given on State type field
                    else if (arrlstFieldTypes[Convert.ToInt32(orderBy[0]) - 2].ToString() == "state")
                        sOrderBy = sOrderBy + " ORDER BY UPPER(ST" + (orderBy[0] - 1) + ".STATE_ID)";
                    //Sort on the basis of Abbreviation in case Order by clause is given on Department field
                    else if (arrlstFieldTypes[Convert.ToInt32(orderBy[0]) - 2].ToString() == "orgh")
                        sOrderBy = sOrderBy + " ORDER BY UPPER(ORG" + (orderBy[0] - 1) + ".ABBREVIATION)";
                    //End - pmittal5
                    //Abansal23 MITS 18835 :  Payee records not sorted Starts
                    //Sort on the basis of Table name in case Order by Entity Table ID is given on Type field
                    else if (arrlstFieldTypes[Convert.ToInt32(orderBy[0]) - 2].ToString() == "tablelist")
                        sOrderBy = sOrderBy + " ORDER BY UPPER(TBL" + (orderBy[0] - 1) + ".TABLE_NAME)";
                    //Abansal23 MITS 18835 :  Payee records not sorted Ends 
                    else
                        sOrderBy = sOrderBy + " ORDER BY UPPER(FLD" + (orderBy[0] - 1) + ")";
                    // akaushik5 MITS 30289 starts
                    sOrderBy += string.Format(" AS FLD{0}", iCnt + 1);
                    // akaushik5 MITS 30289 ends
					if (orderBy[1] != 0)
					{
                        //upper added by Shivendu to remove case sensitivity effect Databases
                        if (arrlstFieldTypes[Convert.ToInt32(orderBy[1]) - 2].ToString() == "numeric" || arrlstFieldTypes[Convert.ToInt32(orderBy[1]) - 2].ToString() == "currency")
                            sOrderBy = sOrderBy + ",FLD" + (orderBy[1] - 1);
                        //pmittal5 Mits 16820 10/08/09 - Sort on the basis of Short_code in case Order by clause is given on Code type field
                        else if (arrlstFieldTypes[Convert.ToInt32(orderBy[1]) - 2].ToString() == "code")
                            sOrderBy = sOrderBy + ",UPPER(CD" + (orderBy[1] - 1) + ".SHORT_CODE)";
                        //Sort on the basis of State Id in case Order by clause is given on State type field
                        else if (arrlstFieldTypes[Convert.ToInt32(orderBy[1]) - 2].ToString() == "state")
                            sOrderBy = sOrderBy + ",UPPER(ST" + (orderBy[1] - 1) + ".STATE_ID)";
                        //Sort on the basis of Abbreviation in case Order by clause is given on Department field
                        else if (arrlstFieldTypes[Convert.ToInt32(orderBy[1]) - 2].ToString() == "orgh")
                            sOrderBy = sOrderBy + ",UPPER(ORG" + (orderBy[1] - 1) + ".ABBREVIATION)";
                        //End - pmittal5
                        //Abansal23 MITS 18835 :  Payee records not sorted Starts
                        //Sort on the basis of Table name in case Order by Entity Table ID is given on Type field
                        else if (arrlstFieldTypes[Convert.ToInt32(orderBy[1]) - 2].ToString() == "tablelist")
                        // akaushik5 MITS 30289 Starts    
                            // sOrderBy = sOrderBy + " ORDER BY UPPER(TBL" + (orderBy[1] - 1) + ".TABLE_NAME)";
                            sOrderBy += string.Format(",UPPER(TBL{0}.TABLE_NAME)",orderBy[1] - 1);
                        // akaushik5 MITS 30289 Ends
                        //Abansal23 MITS 18835 :  Payee records not sorted Ends 
				        else
                            sOrderBy = sOrderBy + ",UPPER(FLD" + (orderBy[1] - 1) + ")";
                        // akaushik5 MITS 30289 starts
                        sOrderBy += string.Format(" AS FLD{0}", iCnt + 2);
                        // akaushik5 MITS 30289 Ends
                        if (orderBy[2] != 0)
                        {
                            if (arrlstFieldTypes[Convert.ToInt32(orderBy[2]) - 2].ToString() == "numeric" || arrlstFieldTypes[Convert.ToInt32(orderBy[2]) - 2].ToString() == "currency")
                                sOrderBy = sOrderBy + ",FLD" + (orderBy[2] - 1);
                            //pmittal5 Mits 16820 10/08/09 - Sort on the basis of Short_code in case Order by clause is given on Code type field
                            else if (arrlstFieldTypes[Convert.ToInt32(orderBy[2]) - 2].ToString() == "code")
                                sOrderBy = sOrderBy + ",UPPER(CD" + (orderBy[2] - 1) + ".SHORT_CODE)";
                            //Sort on the basis of State Id in case Order by clause is given on State type field
                            else if (arrlstFieldTypes[Convert.ToInt32(orderBy[2]) - 2].ToString() == "state")
                                sOrderBy = sOrderBy + ",UPPER(ST" + (orderBy[2] - 1) + ".STATE_ID)";
                            //Sort on the basis of Abbreviation in case Order by clause is given on Department field
                            else if (arrlstFieldTypes[Convert.ToInt32(orderBy[2]) - 2].ToString() == "orgh")
                                sOrderBy = sOrderBy + ",UPPER(ORG" + (orderBy[2] - 1) + ".ABBREVIATION)";
                            //End - pmittal5
                            //Abansal23 MITS 18835 :  Payee records not sorted Starts
                            //Sort on the basis of Table name in case Order by Entity Table ID is given on Type field
                            else if (arrlstFieldTypes[Convert.ToInt32(orderBy[2]) - 2].ToString() == "tablelist")
                            // akaushik5 MITS 30289 Starts 
                                // sOrderBy = sOrderBy + " ORDER BY UPPER(TBL" + (orderBy[2] - 1) + ".TABLE_NAME)";
                                sOrderBy += string.Format(",UPPER(TBL{0}.TABLE_NAME)",orderBy[2] - 1);
                            // akaushik5 MITS 30289 Starts 
                            //Abansal23 MITS 18835 :  Payee records not sorted Ends 
                            else
                                sOrderBy = sOrderBy + ",UPPER(FLD" + (orderBy[2] - 1) + ")";
                            // akaushik5 MITS 30289 starts
                            sOrderBy += string.Format(" AS FLD{0}", iCnt + 3);
                            // akaushik5 MITS 30289 ends
                        }
					}
				}
                //Geeta 01/09/08 : MITS 10703
                // RMA-345  starts      achouhan3       Remove DESC keyword and added alias for order by 
                if(p_sSortColumn != "")
                {
                    if (p_sOrder == "descending")
                    {
                        //PJS MITS 15829 - for numeric & currency no UPPER should done otherwise it will compare as string
                        if (arrlstFieldTypes[Conversion.ConvertStrToInteger(p_sSortColumn) - 1].ToString() == "numeric" || arrlstFieldTypes[Conversion.ConvertStrToInteger(p_sSortColumn) - 1].ToString() == "currency")
                        //upper added by Shivendu to remove case sensitivity effect Databases
                            sOrderBy = " ORDER BY " + "FLD" + (Conversion.ConvertStrToInteger(p_sSortColumn));
                        //pmittal5 Mits 16820 10/08/09 - Sort on the basis of Short_code in case Order by clause is given on Code type field
                        else if (arrlstFieldTypes[Conversion.ConvertStrToInteger(p_sSortColumn) - 1].ToString() == "code")
                            sOrderBy = " ORDER BY UPPER(" + "CD" + (Conversion.ConvertStrToInteger(p_sSortColumn)) + ".SHORT_CODE)";
                        //Sort on the basis of State Id in case Order by clause is given on State type field
                        else if (arrlstFieldTypes[Conversion.ConvertStrToInteger(p_sSortColumn) - 1].ToString() == "state")
                            sOrderBy = " ORDER BY UPPER(" + "ST" + (Conversion.ConvertStrToInteger(p_sSortColumn)) + ".STATE_ID)";
                        //Sort on the basis of Abbreviation in case Order by clause is given on Department field
                        else if (arrlstFieldTypes[Conversion.ConvertStrToInteger(p_sSortColumn) - 1].ToString() == "orgh")
                            sOrderBy = " ORDER BY UPPER(" + "ORG" + (Conversion.ConvertStrToInteger(p_sSortColumn)) + ".ABBREVIATION)";
                        //End - pmittal5
                        //Abansal23 MITS 18835 :  Payee records not sorted Starts
                        //Sort on the basis of Table name in case Order by Entity Table ID is given on Type field
                        else if (arrlstFieldTypes[Conversion.ConvertStrToInteger(p_sSortColumn) - 1].ToString() == "tablelist")
                            sOrderBy = " ORDER BY UPPER(" + "TBL" + (Conversion.ConvertStrToInteger(p_sSortColumn)) + ".TABLE_NAME)";
                        //Abansal23 MITS 18835 :  Payee records not sorted Ends 
                        else
                            sOrderBy = " ORDER BY UPPER(" + "FLD" + (Conversion.ConvertStrToInteger(p_sSortColumn) ) + ")";

                         // RMA-345 Starts      achouhan3 
                            sOrderBy += string.Format(" AS FLD{0}", iCnt + 4);
                            sOrderBy += " DESC";
                          // RMA-345 ENDS      achouhan3 
                       

                    }
                    else
                    {
                        //upper added by Shivendu to remove case sensitivity effect in Databases
                        if (arrlstFieldTypes[Conversion.ConvertStrToInteger(p_sSortColumn) - 1].ToString() == "numeric" || arrlstFieldTypes[Conversion.ConvertStrToInteger(p_sSortColumn) - 1].ToString() == "currency") 
                            sOrderBy = " ORDER BY " + "FLD" + (Conversion.ConvertStrToInteger(p_sSortColumn) );
                        //pmittal5 Mits 16820 10/08/09 - Sort on the basis of Short_code in case Order by clause is given on Code type field
                        else if (arrlstFieldTypes[Conversion.ConvertStrToInteger(p_sSortColumn) - 1].ToString() == "code")
                            sOrderBy = " ORDER BY UPPER(" + "CD" + (Conversion.ConvertStrToInteger(p_sSortColumn)) + ".SHORT_CODE)";
                        //Sort on the basis of State Id in case Order by clause is given on State type field
                        else if (arrlstFieldTypes[Conversion.ConvertStrToInteger(p_sSortColumn) - 1].ToString() == "state")
                            sOrderBy = " ORDER BY UPPER(" + "ST" + (Conversion.ConvertStrToInteger(p_sSortColumn)) + ".STATE_ID)";
                        //Sort on the basis of Abbreviation in case Order by clause is given on Department field
                        else if (arrlstFieldTypes[Conversion.ConvertStrToInteger(p_sSortColumn) - 1].ToString() == "orgh")
                            sOrderBy = " ORDER BY UPPER(" + "ORG" + (Conversion.ConvertStrToInteger(p_sSortColumn)) + ".ABBREVIATION)";
                        //End - pmittal5
                        //Abansal23 MITS 18835 :  Payee records not sorted Starts
                        //Sort on the basis of Table name in case Order by Entity Table ID is given on Type field
                        else if (arrlstFieldTypes[Conversion.ConvertStrToInteger(p_sSortColumn) - 1].ToString() == "tablelist")
                            sOrderBy = " ORDER BY UPPER(" + "TBL" + (Conversion.ConvertStrToInteger(p_sSortColumn)) + ".TABLE_NAME) ";
                        //Abansal23 MITS 18835 :  Payee records not sorted Ends 
                        else
                            sOrderBy = " ORDER BY UPPER(" + "FLD" + (Conversion.ConvertStrToInteger(p_sSortColumn)) + ")";

                        // RMA-345 Starts      achouhan3 
                        sOrderBy += string.Format(" AS FLD{0}", iCnt + 4);
                        // RMA-345 Ends      achouhan3 
                    }
                }
                // RMA-345  ends      achouhan3       Remove DESC keyword and added alias for order by 
				string sCompleteData = "";
				//Now process each search individually
				objSearchResults = new SearchResults(m_sConnectionString,m_iClientId);//sonali
                // npadhy Jira 6415 Also pass userlogin object to Search results
                objSearchResults.UserLogin = m_objUserLogin;
                objSearchResults.p_Connection = objConn;
				objSearchResults.UserId = p_iUserID;
                //START MITS 10608
                objSearchResults.GroupId = p_iGroupID;
                //END MITS 10608
				objSearchResults.TotalRecords = p_lTotalRecords;
                //Start MITS 10651 Abhishek
                objSearchResults .PrintSearch = m_bPrintSearch;
                //END MITS 10651
                objSearchResults.IsOrgSec = m_bOrgSec; //pmittal5 Confidential Record
				//Open the database connection before making specific search request as temporary tables are being generated for faster search.
                //objConn.Open();

				switch (lCatID)
				{
					// Mihika 16-Feb-2006 MITS 4860 changes. Added 'sOrgLevel' field while calling the Search Functions
					case ConstGlobals.CLAIM_SEARCH : // Claim search
                        //Asif mits 13218 Start
                        //SearchResults oSearchResults = new SearchResults();//sonali
                        // npadhy Jira 6415 Pass Userlogin object to search as well.
                        objSearchResults.UserLogin = m_objUserLogin;//Jira 6415
                        //pmittal5 Mits 18356 11/13/09 - If Free Text Supplemental field is there in Select list, DISTINCT is not added
                        //if (oSearchResults.sEvalDistinct(sJustFieldNames) == " DISTINCT ")
                        //if (oSearchResults.sEvalDistinct(sJustFieldNames,arrlstTextSuppFields) == " DISTINCT ")
                        if (objSearchResults.sEvalDistinct(sJustFieldNames, arrlstTextSuppFields) == " DISTINCT ")//sonali
                        {
                            sJustFieldNamesByNumber = " DISTINCT " + sJustFieldNamesByNumber;
                        }
                        //gbhatnagar Mobile Adjuster                         
                        objSearchResults.IsMobileAdjuster = this.IsMobileAdjuster;
                        objSearchResults.IsMobilityAdjuster = this.IsMobilityAdjuster; //rsharma220 Mobility Windows changes

                        //objSearchResults.IsMobileAdjusterClaimInfo = this.IsMobileAdjusterClaimInfo;
                        //akaur9 Mobile Adjuster
                        //if (objSearchResults.IsMobileAdjuster)
                        //{
                        //    AddUnique(ref arrlstQueryTables, "ADJUSTER_ENTITY");
                        //}
                        objSearchResults.IsMobileAdjusterClaimList = this.IsMobileAdjusterClaimList;
                        objSearchResults.IsMobilityAdjusterClaimList = this.IsMobilityAdjusterClaimList; //rsharma220 Mobility Windows changes
                        if (objSearchResults.IsMobileAdjusterClaimList || objSearchResults.IsMobilityAdjusterClaimList) 
                        {
                            AddUnique(ref arrlstQueryTables, "ADJUSTER_ENTITY");
                        }
                        //Asif mits 13218 End
                        objSearchResults.DoClaimSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, sOrgLevel, p_iPageSize, p_iPageNumber, arrlstTextSuppFields,p_sUserName);  //Mits 18356
						break;
					case ConstGlobals.EVENT_SEARCH : //Event search
                        //mbahl3 mobility changes for event search 
                        objSearchResults.IsMobilityAdjusterEventList = this.IsMobilityAdjusterEventList; //mbahl3 Mobility Windows changes
                        if (objSearchResults.IsMobilityAdjusterEventList)
                        {
                            AddUnique(ref arrlstQueryTables, "ADJUSTER_ENTITY");

                            arrlstColumnHeaders.Add("Number of Claims");
                            arrlstFieldTypes.Add("textml"); //mbahl3 mobility
                            arrlstColumnHeaders.Add("Number of Tasks");
                            arrlstFieldTypes.Add("textml"); //mbahl3 mobility
                        }
                        //mbahl3 mobility changes for event search 
						objSearchResults.DoEventSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, sOrgLevel, p_iPageSize, p_iPageNumber,p_sUserName);
						break;
					case ConstGlobals.EMPLOYEE_SEARCH : //Employee search
                        //akaur9 Mobile Adjuster
                        objSearchResults.IsMobileAdjuster = this.IsMobileAdjuster;
                        objSearchResults.IsMobilityAdjuster = this.IsMobilityAdjuster; //rsharma220 Mobility Windows changes

						//mbahl3 mits 30224
                        if (!objSysSettings.StatusActive)
                        {
                            //arrlstColumnHeaders.Add("Status");
                            arrlstColumnHeaders.Add(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ResultList.DoSearch.lblStatus", m_iClientId), sLangId));
                            arrlstFieldTypes.Add("textml");
                            //mbahl3 mits 30972
                        }
							//mbahl3 mits 30224
						objSearchResults.DoEmployeeSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, sOrgLevel, p_iPageSize, p_iPageNumber);
						break;
					case ConstGlobals.ENTITY_SEARCH : //Entity search
                        //MITS 17157 Pankaj 7/15/09
                        sCriteria = FilterCriteriaForAdvOrgSearch(sCriteria, p_xmlUserData);
                        //abansal23 MITS 17648 Starts
                        objSearchResults.TableName = oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText;
                        if(objSearchResults.TableName == "INSURERS" || objSearchResults.TableName == "BROKER")
                        objSearchResults.SelOrgLevels = GetOHUmbrella(p_xmlUserData, oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText);//abansal23 MITS 17648 09/10/2009
                        //abansal23 MITS 17648 Ends
						objSearchResults.IsMobileAdjuster = this.IsMobileAdjuster;
                        objSearchResults.IsMobilityAdjuster = this.IsMobilityAdjuster; //rsharma220 Mobility Windows changes
                        	//mbahl3 mits 30224
                        if (!objSysSettings.StatusActive)
                        {
                            //arrlstColumnHeaders.Add("Status");
                            arrlstColumnHeaders.Add(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ResultList.DoSearch.lblStatus", m_iClientId), sLangId));
                            arrlstFieldTypes.Add("textml");  //mbahl3 mits 30972
                        }
							//mbahl3 mits 30224
						objSearchResults.DoEntitySearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber,lEntityTableRestrict, ref sSQL, ref sCompleteData, sSysEx, ref objConn, p_iPageSize, p_iPageNumber,p_sUserName);
						break;
					case ConstGlobals.VEHICLE_SEARCH : //Vehicle search
						objSearchResults.DoVehicleSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, sOrgLevel, p_iPageSize, p_iPageNumber);
						break;
					case ConstGlobals.POLICY_SEARCH : //Policy search
                        //Start - averma62 MITS 25163- Policy Interface Implementation
                        //zmohammad MITs 34993 Start - Adding code for Query Designer having field tables other than just Policy table.
                        foreach (XmlElement XMLField in objXmlFields) 
                        {
                            sFieldTable = XMLField.GetAttribute("table");
                            AddUnique(ref arrlstQueryTables, sFieldTable);
                        }
                            if (oXmlUser.SelectSingleNode("//AllowPolicySearch").InnerText.Trim() == "true")
                            {
                                if (sCriteria != "")
                                {
                                    sCriteria = sCriteria + " AND POLICY.POLICY_SYSTEM_ID=0";
                                }
                                else
                                {
                                    sCriteria = " POLICY.POLICY_SYSTEM_ID=0";
                                }
                            }
                            //zmohammad MITs 34993 End - Adding code for Query Designer having field tables other than just Policy table.
                        //End  - averma62 MITS 25163- Policy Interface Implementation
                      	objSearchResults.DoPolicySearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber,lPolicyRestrict, ref sSQL, ref sCompleteData, ref objConn, p_iPageSize, p_iPageNumber);
						break;
					case ConstGlobals.PAYMENT_SEARCH : //Payment search
						// Mihika 16-Feb-2006 Changed the Payment Search 
                        objSearchResults.IsMobileAdjuster = this.IsMobileAdjuster;
                        objSearchResults.IsMobilityAdjuster = this.IsMobilityAdjuster;  //rsharma220 Mobility Windows changes

                        objSearchResults.DoPaymentSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, p_iPageSize, p_iPageNumber, arrlstTextSuppFields);  //Mits 18356
						break;
					case ConstGlobals.PATIENT_SEARCH : //Patient search
                        // akaushik5 Changed for MITS 30805 Starts
                        // objSearchResults.DoPatientSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, sOrgLevel, p_iPageSize, p_iPageNumber,p_sUserName);
                        bool dedupePatients = (oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText.Equals("MDI") || oXmlUser.SelectSingleNode("//SearchMain/TableRestrict").InnerText.ToUpper().Equals("PATIENT"));
                        objSearchResults.DoPatientSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, sOrgLevel, p_iPageSize, p_iPageNumber, p_sUserName, dedupePatients);
                        // akaushik5 Changed for MITS 30805 Ends
                        break;
					case ConstGlobals.PHYSICIAN_SEARCH : //Physician search
						//mbahl3 mits 30224
                        if (!objSysSettings.StatusActive)
                        {
                            //arrlstColumnHeaders.Add("Status");
                            arrlstColumnHeaders.Add(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ResultList.DoSearch.lblStatus", m_iClientId), sLangId));
                            arrlstFieldTypes.Add("textml"); //mbahl3 mits 30972
                        }
							//mbahl3 mits 30224
						objSearchResults.DoPhysicianSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, sOrgLevel, p_iPageSize, p_iPageNumber);
						break;
					case ConstGlobals.MED_STAFF_SEARCH : //Medical staff search
					//mbahl3 mits 30224
                        if (!objSysSettings.StatusActive)
                        {
                            arrlstColumnHeaders.Add("Status");
                            arrlstFieldTypes.Add("textml"); //mbahl3 mits 30972
                        }
							//mbahl3 mits 30224
						objSearchResults.DoMedStaffSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, sOrgLevel, p_iPageSize, p_iPageNumber);
						break;
					case ConstGlobals.DISP_PLAN_SEARCH : //Disability Plan search
						objSearchResults.DoDisPlanSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, p_iPageSize, p_iPageNumber);
						break;
                    //Shruti Leave Plan Search starts
                    case ConstGlobals.LV_PLAN_SEARCH: //Leave Plan search
                        objSearchResults.DoLeavePlanSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, p_iPageSize, p_iPageNumber);
                        break;
                    //Shruti Leave Plan Search ends
					case ConstGlobals.AT_SEARCH : //Admin Tracking search
                        objSearchResults.DoATSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, sATKeyField, ref sCompleteData, ref objConn, p_iPageSize, p_iPageNumber, ref sSQL);
						break;
                    // Start Naresh Enhanced Policy Search
                    case ConstGlobals.ENHANCED_POLICY_SEARCH: //Enhanced Policy search
                        if (p_xmlUserData.SelectSingleNode("//TableRestrict").InnerText.Trim() == "policybilling")
                            sPolicyRestrict = "P";

                        // npadhy Start MITS 22029 To restrict the Policy records of another LOB when searched from a Policy Screen
                        if (!string.IsNullOrEmpty(p_xmlUserData.SelectSingleNode("//filter").InnerText.Trim()))
                        {
                            sFilter = p_xmlUserData.SelectSingleNode("//filter").InnerText.Trim();

                            iPolicyLob = objCache.GetCodeId(sFilter, "POLICY_LOB");

                            if (iPolicyLob > 0)
                            {
                                if (!string.IsNullOrEmpty(sCriteria))
                                {
                                    sCriteria = String.Format("{0} AND ", sCriteria);
                                }
                                sCriteria = String.Format("{0} POLICY_TYPE = {1}", sCriteria, iPolicyLob);

                            }
                        }
                        // npadhy End MITS 22029 To restrict the Policy records of another LOB when searched from a Policy Screen
                        objSearchResults.DoENHPolicySearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, lPolicyRestrict, ref sSQL, ref sCompleteData, ref objConn, p_iPageSize, p_iPageNumber, sPolicyRestrict);
                        break;
                    // End Naresh Enhanced Policy Search
                   //Anu Tennyson for MITS 18291 : STARTS 10/25/2009
                    case ConstGlobals.PROPERTY_SEARCH: //Property search
                        objSearchResults.DoPropertySearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, p_iPageSize, p_iPageNumber);
                        break;
                  //Anu Tennyson for MITS 18291 : ENDS
                        //skhare7

                    case ConstGlobals.DIARY_SEARCH: //WPA diary search
                        objSearchResults.DoWPADiarySearch(sOrderBy,sAttachedRecord, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, ref sSQL, ref sCompleteData, ref objConn, p_iPageSize, p_iPageNumber);
                        break;
                    //Start averma62 -  MITS - 28528
                    case ConstGlobals.CATASTROPHE_SEARCH: //Catastrphe search
                        objSearchResults.DoCatastropheSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, lEntityTableRestrict, ref sSQL, ref sCompleteData, sSysEx, ref objConn, p_iPageSize, p_iPageNumber, p_sUserName);
                        break;
                    //End averma62 -  MITS - 28528
                    //Amandeep Driver Chnages
                    case ConstGlobals.DRIVER_SEARCH: //Driver Search
                        objSearchResults.DoDriverSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, lEntityTableRestrict, ref sSQL, ref sCompleteData, sSysEx, ref objConn, p_iPageSize, p_iPageNumber, p_sUserName);
                        break;
                    //Amandeep Driver Chnages                  
                    //Ashish Ahuja Address Master //RMA-8753
                    case ConstGlobals.ADDRESS_SEARCH: //Address Search
                        objSearchResults.DoAddressSearch(sOrderBy, arrlstQueryTables, sQueryFields, arrlstDisplayTables, arrlstDisplayFields, sCriteria, sJustFieldNamesByNumber, lEntityTableRestrict, ref sSQL, ref sCompleteData, sSysEx, ref objConn, p_iPageSize, p_iPageNumber, p_sUserName);
                        break;
				}

				//Display results
				//preserve entity table id when user searches from people maint
				//lResults = objSearchResults.GenXMLResults(p_sResultsPath, p_lPageSize, p_lMaxResults, lCatID, arrlstQueryTables[0].ToString(), sSysEx, lEntityTableRestrict, sCompleteData, arrlstFieldTypes, arrlstColumnHeaders, ref objConn, sSQL, ref p_sSearchXMLData);
				lResults = objSearchResults.GenXMLResults(p_sResultsPath, p_iPageSize, p_iPageNumber, lCatID, arrlstQueryTables[0].ToString(), sSysEx, lEntityTableRestrict, sCompleteData, arrlstFieldTypes, arrlstColumnHeaders, ref objConn, sSQL, ref p_objSearchDom, p_sSortColumn, p_sOrder,sLangId);

				m_lPageCount = lResults;

				objConn.Close();

				//AL Changes as per Adaptor - Sumeet
				return (lResults != 0);
				//return (lResults > 0);
			}
			catch (XmlOperationException p_objException)
			{
				throw p_objException;
			}
			catch (FileInputOutputException p_objException)
			{
				throw p_objException;
			}
			catch (StringException p_objException)
			{
				throw p_objException;
			}
			catch (CollectionException p_objException)
			{
				throw p_objException;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException (CommonFunctions.FilterBusinessMessage(Globalization.GetString("ResultList.DoSearch.GenericError", m_iClientId),sLangId),p_objException);
			}
			finally
			{
				if (objConn != null)
				{
					if (objConn.State == ConnectionState.Open)			
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}	
                if (objSearchResults.p_Connection != null)
				{
					if (objSearchResults.p_Connection.State == ConnectionState.Open)			
					{	
						objSearchResults.p_Connection.Close();
						objSearchResults.p_Connection.Dispose();
					}
				}	
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}				
				if (objCache != null) objCache.Dispose();
                //Shivendu to dispose the object
                if (objSearch != null)
                    objSearch.Dispose();
				objSearchResults = null;
				objXml = null;
				objXmlSearch = null;
				objXmlFields = null;
				objSearch = null;
				arrlstQueryTables = null;
				arrlstDisplayTables = null;
				arrlstDisplayFields = null;
				arrlstColumnHeaders = null;
				arrlstFieldTypes = null;
                objSysSettings = null;                //abansal23 09/16/2009 MITS 17937: Claim search is still case sensitive when Oracle Case Insensitive option is checked 
                //Performance issue
                oXmlUser = null;
                oXmlTmp = null;
                oXmlTableList = null;
                nsSearchDef = null;
                arrlstTextSuppFields = null;
                nsUserData = null;
                //Performance issue
                oXmlID = null;
                xNode = null; 
            }
		}
		#endregion

		#region Get field value
		/// <summary>
		/// This function generates the string out of the data fetched from the Presentation Layer.
		/// </summary>
		/// <param name="p_sFieldId">Field Id</param>
		/// <returns>String containing the data corresponding to the given field.</returns>
		private string GetFieldValue (string p_sFieldId)
		{
			string [] arrGetIDValue = null;
			string sRet = "";
			try
			{
				for (int iCnt = 0; iCnt<m_arrStrFinalData.Length ; iCnt++)
				{	
			
					arrGetIDValue = m_arrStrFinalData[iCnt].Split('~');
					if (arrGetIDValue[0] == p_sFieldId)
					{
						sRet = arrGetIDValue[1];
						break;
					}
				}
			}
			catch(Exception p_objException)
			{
                throw new CollectionException(Globalization.GetString("ResultList.GetFieldValue.SearchData", m_iClientId), p_objException);
			}
			finally
			{
				arrGetIDValue = null;
			}
			return (sRet);
		}
		#endregion

		#region Add unique tables
		/// <summary>
		/// This function adds unique tables into the given Arraylist.
		/// </summary>
		/// <param name="p_arrlstTable">Arraylist containing table names</param>
		/// <param name="p_sItem">Table to be added in the Arraylist</param>
		private void AddUnique(ref ArrayList p_arrlstTable,string p_sItem)
		{
			try
			{
				for (int iCnt=0; iCnt<p_arrlstTable.Count;iCnt++)
				{
					if (Conversion.ConvertObjToStr(p_arrlstTable[iCnt]) == p_sItem)
					{
						return;
					}
				}
				p_arrlstTable.Add (p_sItem);
			}
			catch (Exception p_objException)
			{
                throw new CollectionException(Globalization.GetString("ResultList.AddUnique.TableNames", m_iClientId), p_objException);
			}
		}
		#endregion

		#region Quote the string
		/// <summary>
		/// Quotes the string if not empty. Double quotes any single quotes so they will work.
		/// </summary>
		/// <param name="p_sArg">SQL query to be formatted</param>
		/// <returns>Formatted SQL query</returns>
		private string SQLTextArg(string p_sArg)
		{
			string sTmp = "";
			try
			{
				if (p_sArg == "")
				{
					sTmp = "null";
				}
				else
				{
					sTmp = "N'" + p_sArg.Replace("'", "''") + "'";
				}
			}
			catch {} //Empty catch as simple string manipulation is taking place
			return (sTmp);
		}
		#endregion

		#region SetOrgLevel
		private string SetOrgLevel(string p_OrgEntityTblId)
		{
			string sSetOrgLevel = "";
			switch (p_OrgEntityTblId)
			{
				case "1005":
					sSetOrgLevel = "CLIENT_EID";
					break;
				case "1006":
					sSetOrgLevel = "COMPANY_EID";
					break;
				case "1007":
					sSetOrgLevel = "OPERATION_EID";
					break;
				case "1008":
					sSetOrgLevel = "REGION_EID";
					break;
				case "1009":
					sSetOrgLevel = "DIVISION_EID";
					break;
				case "1010":
					sSetOrgLevel = "LOCATION_EID";
					break;
				case "1011":
					sSetOrgLevel = "FACILITY_EID";
					break;
				case "1012":
					sSetOrgLevel = "DEPARTMENT_EID";
					break;
				default:
					sSetOrgLevel = "DEPARTMENT_EID";
					break;
			}
			return sSetOrgLevel;
		}
		#endregion
        #endregion
        //MITS 17157 Pankaj 7/15/09
        private string FilterCriteriaForAdvOrgSearch(string p_sCriteria, XmlDocument p_xmlUserData)
        {
            string stablename = "";
            long lrowid = 0;
            //string seventdate = "";
            //string sclaimdate = "";
            //string spolicydate = "";
            string stablerestrict = "";
            string slevel = "";
            string scriteria = "";
            string sfilter = "";

            Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy = null;
            //objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(m_sConnectionString);
            objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(m_sConnectionString,m_iClientId);

            stablename = p_xmlUserData.SelectSingleNode("//SearchMain/tablename").InnerText.Trim();
            lrowid = Conversion.ConvertStrToLong(p_xmlUserData.SelectSingleNode("//SearchMain/rowid").InnerText.Trim());
            //seventdate = p_xmlUserData.SelectSingleNode("//SearchMain/eventdate").InnerText.Trim();
            //sclaimdate = p_xmlUserData.SelectSingleNode("//SearchMain/claimdate").InnerText.Trim();
            //spolicydate = p_xmlUserData.SelectSingleNode("//SearchMain/policydate").InnerText.Trim();
            stablerestrict = p_xmlUserData.SelectSingleNode("//SearchMain/TableRestrict").InnerText.Trim();
            sfilter = p_xmlUserData.SelectSingleNode("//SearchMain/filter").InnerText.Trim();

            if (stablename == "" || lrowid == 0  || sfilter !="1")
                return p_sCriteria;

            switch (stablerestrict)
            {
                case "1005":
                    slevel = "C";
                    break;
                case "1006":
                    slevel = "CO";
                    break;
                case "1007":
                    slevel = "O";
                    break;
                case "1008":
                    slevel = "R";
                    break;
                case "1009":
                    slevel = "D";
                    break;
                case "1010":
                    slevel = "L";
                    break;
                case "1011":
                    slevel = "F";
                    break;
                case "1012":
                default: 
                    slevel = "DT";
                    break;
            }
            objOrgHierarchy.TableName = stablename;
            objOrgHierarchy.SetEffectiveDate(stablename, lrowid);            
            scriteria = objOrgHierarchy.AppendDateTriggerSql(scriteria, slevel);
            scriteria = scriteria.Replace("B1.", "");
            scriteria = scriteria.Replace("B.", "");
            scriteria = scriteria.Replace("C.", "");
            scriteria = scriteria.Replace("D.", "");
            scriteria = scriteria.Replace("E.", "");
            scriteria = scriteria.Replace("F.", "");
            scriteria = scriteria.Replace("G.", "");
            scriteria = scriteria.Replace("H.", "");
            if (p_sCriteria != "")
                scriteria = p_sCriteria + scriteria;
            return scriteria;
        }



        //abansal23 : MITS 17468 on 10/09/09 Starts
        /// <summary>
        /// </summary>
        /// <param name="p_objXMLDocument"></param>
        /// <param name="Table_Name"></param>
        /// <returns></returns>
        private string GetOHUmbrella(XmlDocument p_objXMLDocument,string Table_Name)
        {
            string sListText = string.Empty;
            string sInClause = string.Empty;
            string sEntry = string.Empty;
            string sPrevEntry = string.Empty;
            string sOptAll = string.Empty;
            string strContent = string.Empty;
            string[] arrFields;
            int iNum = 0;
            long lCount = 0;
            LocalCache objLocalCache = null;
            ArrayList arDeptList = null;
            string sAll = string.Empty;

            XmlDocument objXmlAutoCheck = new XmlDocument();
            strContent = RMSessionManager.GetCustomContent(m_iClientId);
            if (!string.IsNullOrEmpty(strContent))
            {
                objXmlAutoCheck.LoadXml(strContent);
                if (Table_Name == "INSURERS")
                    sAll = objXmlAutoCheck.SelectSingleNode("//RMAdminSettings/SpecialSettings/AllInsurers").InnerText;
                else if (Table_Name == "BROKER")
                sAll = objXmlAutoCheck.SelectSingleNode("//RMAdminSettings/SpecialSettings/AllBrokers").InnerText;
            }

            if (sAll == "-1")
            {
                sInClause = "";
            }
            else
            {
                arDeptList = new ArrayList();
                sListText = p_objXMLDocument.SelectSingleNode("//SelOrgLevels").InnerText;
                objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);
                if (sListText.Trim() != "")
                {
                    arrFields = sListText.Split(',');
                    if (arrFields.Length > 0)
                    {
                        for (int i = 0; i < arrFields.Length; i++)
                        {
                            //if ((i % 2) > 0)
                            //{
                            iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                            EnumDepts(iNum,arDeptList,objLocalCache);

                            //}
                        }
                    }
                }
                
                for (int i = 0; i < arDeptList.Count; i++)
                {
                    if (Conversion.ConvertObjToInt(arDeptList[i], m_iClientId) != 0)
                    {
                        if (objLocalCache.GetOrgTableId(Conversion.ConvertObjToInt(arDeptList[i], m_iClientId)) <= 1008)  //skip lower levels since policies are almost never put that low, m_iClientId
                        {
                            if (lCount > 500)
                            {
                                lCount = 0;
                                sInClause = sInClause + ",";  //need to separate out every 500
                            }
                            else
                            {
                                if (sInClause.Length != 0)
                                    sInClause = sInClause + ",";
                            }
                            sInClause = sInClause + arDeptList[i];
                            lCount = lCount + 1;

                        }
                    }
                }

            }
            if (objLocalCache != null)
                objLocalCache.Dispose();
            return sInClause;
        }

        private void EnumDepts(int p_iEntityId, ArrayList arDeptList, LocalCache objLocalCache)
        {
            int iTableId = 0;
            string sOrgTableName = "";
            int iDeptId = 0;
            int iEntityId = 0;
            int iParentId = 0;
            DbReader objDBReader = null;
            string sAbbr = "";
            string sName = "";
            //string sConnString = "";
            DbConnection objDbConnection = null;
            try
            {
                iTableId = objLocalCache.GetOrgTableId(p_iEntityId);
                objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                objDbConnection.Open();
                if (arDeptList.IndexOf(p_iEntityId) < 0)
                    arDeptList.Add(p_iEntityId);
                sOrgTableName = Conversion.EntityTableIdToOrgTableName(iTableId);
                int iClientID = Conversion.ConvertObjToInt(objDbConnection.ExecuteScalar(String.Format("SELECT CLIENT_EID FROM ORG_HIERARCHY WHERE {0}_EID={1}", sOrgTableName, p_iEntityId)), m_iClientId);
                objDBReader = objDbConnection.ExecuteReader(String.Format("SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE {0}_EID={1}", sOrgTableName, p_iEntityId));
                if (objDBReader != null)
                {
                    while (objDBReader.Read())
                    {
                        iDeptId = Conversion.ConvertObjToInt(objDBReader["DEPARTMENT_EID"].ToString(), m_iClientId);
                        if (arDeptList.IndexOf(iDeptId) < 0)
                            arDeptList.Add(iDeptId);
                        iEntityId = iDeptId;
                        while (iEntityId != iClientID && iEntityId != 0)
                        {
                            iTableId = objLocalCache.GetOrgTableId(iEntityId);
                            objLocalCache.GetParentOrgInfo(iDeptId, iTableId - 1, ref sAbbr, ref sName, ref iParentId);
                            if (arDeptList.IndexOf(iParentId) < 0)
                                arDeptList.Add(iParentId);
                            iEntityId = iParentId;
                        }
                    }
                }
                // }
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
                if (objDBReader != null)
                {
                    objDBReader.Dispose();
                    objDBReader = null;
                }
                if (objDbConnection != null)
                    objDbConnection.Dispose();

            }
        }   


        //abansal23 : MITS 17468 on 10/09/09 Ends
        //Added Rakhi for R7:Add Emp Data Elements
        #region Check Utility Settings
        private bool CheckUtilSettings(string sFieldTable, string sFieldName, SysSettings objSettings)
        {
            //SysSettings objSettings = null;
            bool bReturnValue = true;
            try
            {
                //objSettings = new SysSettings(m_sConnectionString);
                switch (sFieldTable)
                {
                    case "ENTITY_X_ADDRESSES":
                        if (objSettings.UseMultipleAddresses)
                        {
                            bReturnValue = true;
                        }
                        else
                        {
                            bReturnValue = false;
                        }
                        break;
                    case "PATIENT_ENTITY":
                    case "PHYS_ENTITY":
                    case "ENTITY":
                    case "MED_ENTITY":
                    case "ORG_ENTITY":
                        if (sFieldName == "ENTITY_APPROVAL_STATUS")
                        {
                            if (objSettings.UseEntityApproval)
                            {
                                bReturnValue = true;
                            }
                            else
                            {
                                bReturnValue = false;
                            }
                        }
                        break;
                }
            }
            finally
            {
                objSettings = null;
            }
            return bReturnValue;
        }
        //Added Rakhi for R7:Add Emp Data Elements
        #endregion Check Utility Settings


                #region Save User Preferences

        //RMA-345 Start     achouhan3       Methods to get/Set User Preferecens
        /// <summary>
        /// Author		: ngupta36
        /// Date Created: 05/09/2014 
        /// Sets the user preference for searches in user_pref_xml
        /// </summary>
        /// <param name="p_objInputDoc">XmlDocument</param>
        public void GetUserPrefSearchXML(XmlDocument p_objInputDoc, ref XmlDocument p_objInputDocOut)
        {
            XmlDocument objUserPrefXML = null;
            DbReader objReader = null;
            string sUserPrefsXML = "";
            XmlElement objTempNode = null;
            XmlElement objParentNode = null;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();

            try
            {

                strSQL.AppendFormat("SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = {0}", "~USERID~");
                dictParams.Add("USERID", m_objUserLogin.UserId.ToString());

                objReader = DbFactory.ExecuteReader(m_sConnectionString, strSQL.ToString(), dictParams);

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();

                p_objInputDocOut = new XmlDocument();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    p_objInputDocOut.LoadXml(sUserPrefsXML);

                    objParentNode = (XmlElement)p_objInputDocOut.SelectSingleNode("/setting");
                    if (!(objParentNode == null))
                    {
                        objParentNode.RemoveAll();

                        objUserPrefXML.LoadXml(sUserPrefsXML);

                        if (!(objUserPrefXML.SelectSingleNode("//UserPrefSearch") == null))
                        {
                            objTempNode = (XmlElement)p_objInputDocOut.ImportNode(objUserPrefXML.SelectSingleNode("//UserPrefSearch"), true);
                            objParentNode.AppendChild(objTempNode);
                        }
                    }//end of if. ("/setting" == null)
                }
            }// end of try block

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.GetUserPreference.Err", m_iClientId), p_objException);
            }
            finally
            {
                objUserPrefXML = null;
                objTempNode = null;
                objParentNode = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                strSQL = null; //34278
                dictParams = null; //34278
            }
        }

        public void SetUserPrefSearch(XmlDocument p_objInputDoc)
        {
            XmlDocument objUserPrefXML = null;
            DbReader objReader = null;
            string sUserPrefsXML = "";
            string strSearchtype;
            XmlNode objNode = null;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();

            try
            {

                strSQL.AppendFormat("SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = {0}", "~USERID~");
                dictParams.Add("USERID", m_objUserLogin.UserId.ToString());

                objReader = DbFactory.ExecuteReader(m_sConnectionString, strSQL.ToString(), dictParams);

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }

                CreateUserPreferXML(objUserPrefXML);

                strSearchtype = p_objInputDoc.SelectSingleNode("//UserPrefSearch").FirstChild.Name;

                objNode = objUserPrefXML.SelectSingleNode("//UserPrefSearch/" + strSearchtype);

                if (!(objNode == null))
                    objUserPrefXML.SelectSingleNode("//UserPrefSearch").RemoveChild(objNode);

                objNode = objUserPrefXML.ImportNode(p_objInputDoc.SelectSingleNode("//UserPrefSearch").FirstChild, true);

                objUserPrefXML.SelectSingleNode("//UserPrefSearch").AppendChild(objNode);

                this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(m_objUserLogin.UserId));
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.SaveUserPreference.Err", m_iClientId), p_objException);
            }
            finally
            {
                objUserPrefXML = null;
                objNode = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                strSQL = null; //34278
                dictParams = null; //34278
            }
        }
        /// <summary>
        /// Create Default User Prefernce Xml.
        /// </summary>
        /// <param name="p_objUserPrefXML">User Preference Xml Doc</param>
        /// 
        private void CreateUserPreferXML(XmlDocument p_objUserPrefXML)
        {
            XmlElement objTempNode = null;
            XmlElement objParentNode = null;
            string strSearchtype = string.Empty;

            try
            {
                // Create setting node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                if (objTempNode == null)
                {
                    objTempNode = p_objUserPrefXML.CreateElement("setting");
                    p_objUserPrefXML.AppendChild(objTempNode);
                }
                // Create search node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//UserPrefSearch");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                    objTempNode = p_objUserPrefXML.CreateElement("UserPrefSearch");
                    objParentNode.AppendChild(objTempNode);
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.CreateUserPreferXML.Err", m_iClientId), p_objException);
            }
            finally
            {
                objTempNode = null;
                objParentNode = null;
            }
        }
        private void SaveUserPrefersXmlToDb(string p_sUserPrefXml, int p_iUserId)
        {
            DbConnection objConn = null;
            DbWriter objWriter = null;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();

            try
            {

                strSQL.AppendFormat("DELETE FROM USER_PREF_XML WHERE USER_ID = {0}", "~USERID~");
                dictParams.Add("USERID", p_iUserId);

                DbFactory.ExecuteNonQuery(m_sConnectionString, strSQL.ToString(), dictParams);

                objWriter = DbFactory.GetDbWriter(m_sConnectionString);
                objWriter.Tables.Add("USER_PREF_XML");
                objWriter.Fields.Add("USER_ID", p_iUserId);
                objWriter.Fields.Add("PREF_XML", p_sUserPrefXml);
                objWriter.Execute();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.SaveUserPrefersXmlToDb.Err", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objWriter != null)
                {
                    objWriter = null;
                }
                strSQL = null; //34278
                dictParams = null; //34278
            }
        }
        //34278 End
        #endregion

}
}
