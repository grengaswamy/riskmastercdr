﻿

using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Xml;
using C1.C1PrintDocument;

using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Settings;

namespace Riskmaster.Application.BankAccountManagement
{
	///************************************************************** 
	///* $File		: Reports.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 13-Sep-2005
	///* $Author	: Neelima Dabral
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	/// Manages all operations related to fetching of data
	/// and generation of reports.
	/// </summary>
	public class Reports
	{
		private string m_sConnectionString ="";
        private int m_iClientId = 0;


        public Reports(string p_sConnectionString, int p_iClientId)
		{
			m_sConnectionString = p_sConnectionString;
            m_iClientId = p_iClientId;
		}


		#region "MasterAccountBalReport(int p_iAccountId)"
		/// Name			: MasterAccountBalReport
		/// Author			: Neelima Dabral
		/// Date Created	: Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************	
		/// <summary>
		/// This method will check for existing account & gets the data for the report to be generated
		/// </summary>
		/// <param name="p_iAccountId"></param>
		/// <returns>Returns the PDF as string for the report to be generated</returns>
		public string MasterAccountBalReport(int p_iAccountId)
		{
			ArrayList	   arrlstSubAccDtl = null;	
			int			   iNoOfAccount=0;
			string		   sFileName="";
			string		   sBeginDate="";
			string		   sEndDate="";
			try
			{
				if (p_iAccountId == 0)
					throw new InvalidValueException(Globalization.GetString("Reports.MasterAccountBalReport.InvalidAccId",m_iClientId));//dvatsa-cloud

				arrlstSubAccDtl = FetchMasterAccountBalReportData(p_iAccountId,ref iNoOfAccount,ref sBeginDate,ref sEndDate);

				sFileName = PrintMasterAccountBalReport(arrlstSubAccDtl,iNoOfAccount,p_iAccountId,sBeginDate,sEndDate);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Reports.MasterAccountBalReport.ErrorGenerate", m_iClientId), p_objException);//dvatsa-cloud
			}			
			finally
			{
				arrlstSubAccDtl = null;
			}
			return sFileName;
		}

		#endregion

		#region "MasterAccountBalReportSpecial(int p_iAccountId)"
		/// Name			: MasterAccountBalReportSpecial
		/// Author			: Mohit Yadav
		/// Date Created	: 28-Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************	
		/// <summary>
		/// This method will check for existing account & gets the data for the report to be generated
		/// </summary>
		/// <param name="p_iAccountId"></param>
		/// <returns>Returns the PDF as string for the report to be generated</returns>
		public string MasterAccountBalReportSpecial(int p_iAccountId)
		{
			ArrayList	   arrlstSubAccDtl = null;	
			int			   iNoOfAccount=0;
			string		   sFileName="";
			string		   sBeginDate="";
			string		   sEndDate="";
			try
			{
				if (p_iAccountId == 0)
                    throw new InvalidValueException(Globalization.GetString("Reports.MasterAccountBalReportSpecial.InvalidAccId", m_iClientId));//dvatsa-cloud

				arrlstSubAccDtl = FetchMasterAccountBalReportSpecialData(p_iAccountId,ref iNoOfAccount,ref sBeginDate,ref sEndDate);

				sFileName = PrintMasterAccountBalReportSpecial(arrlstSubAccDtl,iNoOfAccount,p_iAccountId,sBeginDate,sEndDate);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Reports.MasterAccountBalReportSpecial.ErrorGenerate", m_iClientId), p_objException);//dvatsa-cloud
			}			
			finally
			{
				arrlstSubAccDtl = null;
			}
			return sFileName;
		}
		#endregion

		#region "MonthyBankReconReport(int p_iAccountId)"
		/// Name			: MonthyBankReconReport
		/// Author			: Neelima Dabral
		/// Date Created	: Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method will check for existing account & gets the data for the report to be generated
		/// </summary>
		/// <param name="p_iAccountId"></param>
		/// <returns>Returns the PDF as string for the report to be generated</returns>
		public string MonthyBankReconReport(int p_iAccountId)
		{
			string		   sFileName="";			
			try
			{
				if (p_iAccountId == 0)
                    throw new InvalidValueException(Globalization.GetString("Reports.MonthyBankReconReport.InvalidAccId", m_iClientId));//dvatsa-cloud

				sFileName = PrintMonthyBankReconReport(p_iAccountId);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Reports.MonthyBankReconReport.ErrorGenerate", m_iClientId), p_objException);//dvatsa-cloud
			}						
			return sFileName;
		}	
	
		#endregion

		#region "MonthyBankReconReportSpecial(int p_iAccountId)"
		/// Name			: MonthyBankReconReportSpecial
		/// Author			: Mohit Yadav
		/// Date Created	: 3-Oct-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method will check for existing account & gets the data for the report to be generated
		/// </summary>
		/// <param name="p_iAccountId"></param>
		/// <returns>Returns the PDF as string for the report to be generated</returns>
		public string MonthyBankReconReportSpecial(int p_iAccountId)
		{
			string		   sFileName="";			
			try
			{
				if (p_iAccountId == 0)
                    throw new InvalidValueException(Globalization.GetString("Reports.MonthyBankReconReportSpecial.InvalidAccId", m_iClientId));//dvatsa-cloud

				sFileName = PrintMonthyBankReconReportSpecial(p_iAccountId);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Reports.MonthyBankReconReportSpecial.ErrorGenerate", m_iClientId), p_objException);//dvatsa-cloud
			}						
			return sFileName;
		}

		#endregion
		
		#region "FetchMasterAccountBalReportData(int p_iAccountId, ref int p_iNoOfAccount,ref string p_sBeginDate,ref string p_sEndDate)"
		/// Name			: FetchMasterAccountBalReportData
		/// Author			: Neelima Dabral
		/// Date Created	: Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method will fetch the data for the report to be generated
		/// </summary>
		/// <param name="p_iAccountId"></param>
		/// <param name="p_iNoOfAccount"></param>
		/// <param name="p_sBeginDate"></param>
		/// <param name="p_sEndDate"></param>
		/// <returns>Returns the data as arraylist for the report to be generated</returns>
		private ArrayList FetchMasterAccountBalReportData(int p_iAccountId, ref int p_iNoOfAccount,ref string p_sBeginDate,ref string p_sEndDate)
		{
			StringBuilder		sbSql = null;
			DbReader			objReader = null;	
			string				sEndDate ="";
			string				sBeginDate ="";
			ArrayList			arrlstSubAccDetails = null;
			SubBankAccPrintDef  structSubAccDetail;
			int					iNumAcct =0;
			int					iCounter =0;
			int					iSubAccId=0;
			bool				bFhOnlyPrinted = false;
			double				dblPayments=0;
			double				dblCollections=0;
			double				dblPrior=0;
			double				dblTmp=0;
			double				dblTransfer=0;
			try
			{
				sbSql = new StringBuilder();
				//get the dates for reconciliation
				sbSql.Remove(0,sbSql.Length);
				sbSql.Append("SELECT END_DATE,BEGIN_DATE FROM BANK_ACC_RECON WHERE ACCOUNT_ID=" + p_iAccountId);
				sbSql.Append(" ORDER BY END_DATE DESC");				

				objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
				
				if (!objReader.Read())
                    throw new RMAppException(Globalization.GetString("Reports.FetchMasterAccountBalReportData.AccNotBal", m_iClientId));		//dvatsa-cloud								

				sEndDate = Conversion.ConvertObjToStr(objReader.GetValue("END_DATE"));
				p_sEndDate = sEndDate;
				sBeginDate = Conversion.ConvertObjToStr(objReader.GetValue("BEGIN_DATE"));
				p_sBeginDate = sBeginDate;
				objReader.Close();

				sbSql.Remove(0,sbSql.Length);
				sbSql.Append("SELECT SUB_ROW_ID,SUB_ACC_NAME FROM BANK_ACC_SUB WHERE ACCOUNT_ID = " + p_iAccountId );				

				objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
				
				if (!objReader.Read())
                    throw new RMAppException(Globalization.GetString("Reports.FetchMasterAccountBalReportData.NoSubAcc", m_iClientId));		//dvatsa-cloud								

				arrlstSubAccDetails = new ArrayList();

				//Determine which sub account are defined
				do
				{
					structSubAccDetail = new SubBankAccPrintDef();
					structSubAccDetail.m_iAccounId = Conversion.ConvertObjToInt(objReader.GetValue("SUB_ROW_ID"), m_iClientId);
					structSubAccDetail.m_sAccountName = Conversion.ConvertObjToStr(objReader.GetValue("SUB_ACC_NAME"));					
					arrlstSubAccDetails.Add(structSubAccDetail);					
					iNumAcct++;
				}while (objReader.Read());
				objReader.Close();
				
				p_iNoOfAccount  = iNumAcct;
				
				
				for (iCounter = 0;iCounter <= iNumAcct -1 ;++iCounter)
				{

					structSubAccDetail = (SubBankAccPrintDef) arrlstSubAccDetails[iCounter];
					iSubAccId = structSubAccDetail.m_iAccounId;

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + iSubAccId);				

					if (ChkFhPrintedOnly())
					{
						sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0 AND STATUS_CODE = 1054 AND VOID_DATE < '" + sBeginDate + "'");
						bFhOnlyPrinted = true;
					}
					else
					{    
						sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0 AND VOID_DATE < '" + sBeginDate + "'");
						bFhOnlyPrinted = false;
					}
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() +  " AND PAYMENT_FLAG <> 0");
					if(objReader.Read())
                        dblPayments = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					//Collections
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() +  " AND PAYMENT_FLAG = 0");
					if(objReader.Read())
                        dblCollections = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					//Deposits   0 DEPOSIT 1 ADJUSTMENT 2 MM DEPOSIT 3 MM TRANSFER 4 MM ADJUSTMENT
					//for cleared deposits go to collections also
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append(" SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append("  AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0 AND VOIDCLEAR_DATE < '" + sBeginDate + "'");
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DEPOSIT_TYPE IN (0,1)");
					if(objReader.Read())
						dblPrior = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DEPOSIT_TYPE IN (2,4)");
					if(objReader.Read())
						dblTmp = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DEPOSIT_TYPE = 3");
					if(objReader.Read())
						dblTransfer = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					
					structSubAccDetail.m_dblBegin = dblPrior + dblCollections - dblPayments + dblTransfer;
					//***********************Mohit Yadav*********************
					//structSubAccDetail.m_dblMMBegin = dblTmp = dblTransfer;
					structSubAccDetail.m_dblMMBegin = dblTmp - dblTransfer;
					//*******************************************************

					//Get the money market account information
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0 AND VOIDCLEAR_DATE >= '" + sBeginDate + "'");
					
					//deposits					
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DEPOSIT_TYPE = 2");
					if(objReader.Read())
					structSubAccDetail.m_dblMMDeposit  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();
					
					//transfers
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DEPOSIT_TYPE = 3");
					if(objReader.Read())
						structSubAccDetail.m_dblMMTransfer  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					//adjustments
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DEPOSIT_TYPE = 4");
					if(objReader.Read())
						structSubAccDetail.m_dblMMAdjustment  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					structSubAccDetail.m_dblMMSubBal = structSubAccDetail.m_dblMMBegin + structSubAccDetail.m_dblMMDeposit 
										+ structSubAccDetail.m_dblAdjustments - structSubAccDetail.m_dblMMTransfer;

					//deposits in transit
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append(" AND VOID_FLAG = 0 AND CLEARED_FLAG = 0");
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DEPOSIT_TYPE = 2");
					if(objReader.Read())
						structSubAccDetail.m_dblMMDepTrans  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					// transfers in transit
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DEPOSIT_TYPE = 3");
					if(objReader.Read())
						structSubAccDetail.m_dblMMTranTrans  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					structSubAccDetail.m_dblMMBalance = structSubAccDetail.m_dblMMSubBal + structSubAccDetail.m_dblMMDepTrans 
													-	structSubAccDetail.m_dblMMTranTrans;

					//REGULAR ACCOUNT
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + iSubAccId);
				    if (bFhOnlyPrinted)
						sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0 AND STATUS_CODE = 1054");
					else
						sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0");

					sbSql.Append(" AND VOID_DATE >= '" + sBeginDate + "'");
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DATE_OF_CHECK <= '" + sEndDate + "' AND PAYMENT_FLAG <> 0");
					if(objReader.Read())
						structSubAccDetail.m_dblClearedChecks  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					//COLLECTIONS
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND TRANS_DATE <= '" + sEndDate + "' AND PAYMENT_FLAG = 0");
					if(objReader.Read())
						dblCollections  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();
					
					//Deposits    0 DEPOSIT 1 ADJUSTMENT 2 MM DEPOSIT 3 MM TRANSFER 4 MM ADJUSTMENT
					//for cleared deposits go to collections also
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0");
					sbSql.Append(" AND VOIDCLEAR_DATE >= '" + sBeginDate + "' AND TRANS_DATE <= '" + sEndDate + "'");
					sbSql.Append(" AND DEPOSIT_TYPE = 0");
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
					if(objReader.Read())
						structSubAccDetail.m_dblDeposits  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId) 
																		+ dblCollections;
					objReader.Close();

					//ADJUSTMENTS
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append(" AND VOID_FLAG = 0");
					sbSql.Append(" AND VOIDCLEAR_DATE >= '" + sBeginDate + "' AND TRANS_DATE <= '" + sEndDate + "'");
					sbSql.Append(" AND DEPOSIT_TYPE = 1");
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
					if(objReader.Read())
						structSubAccDetail.m_dblAdjustments  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					structSubAccDetail.m_dblSubBal = structSubAccDetail.m_dblBegin + structSubAccDetail.m_dblDeposits + 
						structSubAccDetail.m_dblAdjustments - structSubAccDetail.m_dblClearedChecks + structSubAccDetail.m_dblMMTransfer;

					//In Transit
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + iSubAccId);
					sbSql.Append(" AND CLEARED_FLAG = 0 AND VOID_FLAG = 0");
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DATE_OF_CHECK <= '" + sEndDate + "' AND PAYMENT_FLAG <> 0 AND STATUS_CODE = 1054");
					if(objReader.Read())
						structSubAccDetail.m_dblOutChecks  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					//Collections
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() +  " AND TRANS_DATE <= '" + sEndDate + "' AND PAYMENT_FLAG = 0");
					if(objReader.Read())
						dblCollections = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append(" AND CLEARED_FLAG = 0 AND VOID_FLAG = 0");
					sbSql.Append(" AND DEPOSIT_TYPE = 0 AND TRANS_DATE <= '" + sEndDate + "'");
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() );
					if(objReader.Read())
						structSubAccDetail.m_dblOutDeposits  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId) + dblCollections;
					objReader.Close();

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + iSubAccId);
					if (bFhOnlyPrinted)
						sbSql.Append(" AND PAYMENT_FLAG <> 0 AND VOID_FLAG = 0 AND STATUS_CODE = 1054");
					else
						sbSql.Append(" AND PAYMENT_FLAG <> 0 AND VOID_FLAG = 0");
					
					sbSql.Append(" AND DATE_OF_CHECK BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'");
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() );
					if(objReader.Read())
						structSubAccDetail.m_dblChecksWritten  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
						//***********************Mohit*******
                        //structSubAccDetail.m_dblChecksWritten  = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId) + dblCollections;
						//***********************************
					objReader.Close();

					structSubAccDetail.m_dblBalance = structSubAccDetail.m_dblSubBal + structSubAccDetail.m_dblOutDeposits 
											- structSubAccDetail.m_dblOutChecks	+ structSubAccDetail.m_dblMMTranTrans;         

					
					arrlstSubAccDetails[iCounter] = structSubAccDetail;
				}				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Reports.FetchMasterAccountBalReportData.Error", m_iClientId), p_objException);//dvatsa-cloud
			}				
			finally
			{
				sbSql = null;
				if(objReader != null)
				{
					objReader.Dispose();
					objReader = null;
				}				
			}
			return arrlstSubAccDetails;
		}
		#endregion

		#region "FetchMasterAccountBalReportSpecialData(int p_iAccountId, ref int p_iNoOfAccount,ref string p_sBeginDate,ref string p_sEndDate)"
		/// Name			: FetchMasterAccountBalReportSpecialData
		/// Author			: Mohit Yadav
		/// Date Created	: 30-Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method gets the data for the report to be generated based on the accountid passed
		/// </summary>
		/// <param name="p_iAccountId"></param>
		/// <param name="p_iNoOfAccount"></param>
		/// <param name="p_sBeginDate"></param>
		/// <param name="p_sEndDate"></param>
		/// <returns>Returns an arraylist of data</returns>
		private ArrayList FetchMasterAccountBalReportSpecialData(int p_iAccountId, ref int p_iNoOfAccount,ref string p_sBeginDate,ref string p_sEndDate)
		{								
			int					iType;
			string				sEndDate ="";
			string				sBeginDate ="";
			StringBuilder		sbSql = null;
			DbReader			objReader = null;
			SubBankAccPrintDef  structSubAccDetail;
			ArrayList			arrlstSubAccDetails = null;
			int					iNumAcct =0;
			int					iCounter =0;
			int					iSubAccId=0;
			double				dblPayments=0;
			double				dblCollections=0;
			double				dblPrior=0;

			try
			{
				iType = GetFundsAcountBalanceDateCriteria();
				
				sEndDate = DateTime.Now.ToString("yyyyMMdd");
				sBeginDate = DateTime.Now.ToString("yyyy") + DateTime.Now.ToString("MM") + "01";

				p_sBeginDate = sBeginDate;
				p_sEndDate = sEndDate;

				sbSql = new StringBuilder();
				sbSql.Remove(0,sbSql.Length);
				sbSql.Append("SELECT SUB_ROW_ID,SUB_ACC_NAME FROM BANK_ACC_SUB WHERE ACCOUNT_ID =" + p_iAccountId);

				objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
				
				if (!objReader.Read())
                    throw new RMAppException(Globalization.GetString("Reports.FetchMasterAccountBalReportSpecialData.NoSubAcc", m_iClientId));//dvatsa-cloud

				arrlstSubAccDetails = new ArrayList();

				//Determine which sub account are defined
				do
				{
					structSubAccDetail = new SubBankAccPrintDef();
					structSubAccDetail.m_iAccounId = Conversion.ConvertObjToInt(objReader.GetValue("SUB_ROW_ID"), m_iClientId);
					structSubAccDetail.m_sAccountName = Conversion.ConvertObjToStr(objReader.GetValue("SUB_ACC_NAME"));					
					arrlstSubAccDetails.Add(structSubAccDetail);					
					iNumAcct++;
				}while (objReader.Read());
				objReader.Close();

				p_iNoOfAccount  = iNumAcct;

				for (iCounter = 0;iCounter <= iNumAcct -1 ;++iCounter)
				{
					structSubAccDetail = (SubBankAccPrintDef) arrlstSubAccDetails[iCounter];
					iSubAccId = structSubAccDetail.m_iAccounId;

					//Not to worry about Cleared or not
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + iSubAccId);
					sbSql.Append(" AND VOID_FLAG = 0 AND DATE_OF_CHECK < '" + sBeginDate + "'");

					//Now Collections and Payments
					if (iType != 2)
					{
						objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() +  " AND PAYMENT_FLAG = 0");
						if(objReader.Read())
							dblCollections = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
						objReader.Close();
					}

					if (iType == 2)
					{
						sbSql.Append(" AND STATUS_CODE=1054 ");
					}
					
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() +  " AND PAYMENT_FLAG <> 0");
					if(objReader.Read())
						dblPayments = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					//Now Deposits   0-DEPOSIT 1-ADJUSTMENT 2-MM DEPOSIT 3-MM TRANSFER 4-MM ADJUSTMENT
					//For cleared deposits go to collections also

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append(" SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append("  AND VOID_FLAG = 0 AND TRANS_DATE < '" + sBeginDate + "'");
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DEPOSIT_TYPE IN (0,1)");
					if(objReader.Read())
						dblPrior = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					structSubAccDetail.m_dblBegin = dblPrior + dblCollections - dblPayments;

					//Getting rest

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append(" SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + iSubAccId);
					sbSql.Append("  AND VOID_FLAG = 0");
					
					//Collections and Payments

					if (iType != 2)
					{
						objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND PAYMENT_FLAG = 0 AND TRANS_DATE BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'");
						if(objReader.Read())
							dblCollections = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
						objReader.Close();
					}

					if (iType == 2)
					{
						sbSql.Append("  AND STATUS_CODE=1054 ");
					}
					
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND PAYMENT_FLAG <> 0 AND DATE_OF_CHECK BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'");
					if(objReader.Read())
						structSubAccDetail.m_dblChecksWritten = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append(" SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append("  AND VOID_FLAG = 0");
					sbSql.Append("  AND TRANS_DATE BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'");

					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DEPOSIT_TYPE = 0 ");
					if(objReader.Read())
						structSubAccDetail.m_dblOutDeposits = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId) + dblCollections;
					objReader.Close();

					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DEPOSIT_TYPE = 1 ");
					if(objReader.Read())
						structSubAccDetail.m_dblAdjustments = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					objReader.Close();

					structSubAccDetail.m_dblBalance = structSubAccDetail.m_dblOutDeposits - structSubAccDetail.m_dblChecksWritten + structSubAccDetail.m_dblAdjustments;

					arrlstSubAccDetails[iCounter] = structSubAccDetail;
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Reports.FetchMasterAccountBalReportSpecialData.Error", m_iClientId), p_objException);//dvatsa-cloud
			}				
			finally
			{
				sbSql = null;
				if(objReader != null)
				{
					objReader.Dispose();
					objReader = null;
				}				
			}
			return arrlstSubAccDetails;
		}
		#endregion

		#region "ChkFhPrintedOnly()"
		/// Name			: ChkFhPrintedOnly
		/// Author			: Neelima Dabral
		/// Date Created	: Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Checks for the specific field to set certain condition
		/// </summary>
		/// <returns>true/false</returns>
		private bool ChkFhPrintedOnly()
		{
			DataTable   objDataTable=null;			
			DbReader	objReader = null;
			try
			{
				objReader = DbFactory.GetDbReader(m_sConnectionString,"SELECT * FROM SYS_PARMS");
				if(objReader.Read())
				{
					objDataTable = objReader.GetSchemaTable();
					if(objDataTable != null)
					{
						foreach (DataColumn objDataColumn in objDataTable.Columns)
						{
							if(objDataColumn.ColumnName == "FH_ONLY_PRINTED")
								return true;
						}
					}
				}
				objReader.Close();
				return false;
			}
			catch( Exception p_objException )
			{
                throw new RMAppException(Globalization.GetString("Reports.ChkFhPrintedOnly.CompareError", m_iClientId), p_objException);	//dvatsa-cloud			
			}
			finally
			{
				if(objReader != null)
				{
					objReader.Dispose();
					objReader = null;
				}
				if(objDataTable != null)
				{
					objDataTable.Dispose();
					objDataTable = null;
				}
			}
			
		}

		#endregion

		#region "GetFundsAcountBalanceDateCriteria()"
		/// Name			: GetFundsAcountBalanceDateCriteria
		/// Author			: Mohit Yadav
		/// Date Created	: 03-Oct-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Checks for the specific field to set certain condition
		/// </summary>
		/// <returns>Integer value based on the value in that field</returns>
		private int GetFundsAcountBalanceDateCriteria()
		{
			DataTable   objDataTable=null;			
			DbReader	objReader = null;
			try
			{
				objReader = DbFactory.GetDbReader(m_sConnectionString,"SELECT * FROM SYS_PARMS");
				if(objReader.Read())
				{
					objDataTable = objReader.GetSchemaTable();
                    if (objDataTable != null)
					{
                        // akaushik5 Changed for MITS 36902 Starts
                        //foreach (DataColumn objDataColumn in objDataTable.Columns)
                        //{
                        //    if(objDataColumn.ColumnName == "FBBAL_DT_OPT")
                        //        return Convert.ToInt32(objReader["FBBAL_DT_OPT"].ToString());
                        //}
                        if (objDataTable.Columns.Contains("ColumnName"))
                        {
                            foreach (DataRow objDataRow in objDataTable.Rows)
                            {
                                if (objDataRow != null && objDataRow["ColumnName"] != null && objDataRow["ColumnName"].ToString().Trim().Equals("FBBAL_DT_OPT"))
                                {
                                    return Convert.ToInt32(objReader["FBBAL_DT_OPT"].ToString());
                                }
                            }
                        }
                        // akaushik5 Changed for MITS 36902 Ends
					}
				}
				objReader.Close();
				return 0;
			}
			catch( Exception p_objException )
			{
                throw new RMAppException(Globalization.GetString("Reports.GetFundsAcountBalanceDateCriteria.CompareError", m_iClientId), p_objException);//dvatsa-cloud				
			}
			finally
			{
				if(objReader != null)
				{
					objReader.Dispose();
					objReader = null;
				}
				if(objDataTable != null)
				{
					objDataTable.Dispose();
					objDataTable = null;
				}
			}
			
		}

		#endregion

		#region "PrintMasterAccountBalReport(ArrayList p_arrlstSubAccDetails,int p_iNoOfAccount,int p_iAccountId,string p_sBeginDate,string p_sEndDate)"
		/// Name			: PrintMasterAccountBalReport
		/// Author			: Neelima Dabral
		/// Date Created	: Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method is used to format & generate the PDF report along with the data.
		/// </summary>
		/// <param name="p_arrlstSubAccDetails"></param>
		/// <param name="p_iNoOfAccount"></param>
		/// <param name="p_iAccountId"></param>
		/// <param name="p_sBeginDate"></param>
		/// <param name="p_sEndDate"></param>
		/// <returns>returns the string containing the report details with data</returns>
		private string PrintMasterAccountBalReport(ArrayList p_arrlstSubAccDetails,int p_iNoOfAccount,int p_iAccountId,string p_sBeginDate,string p_sEndDate)
		{
			int					iCounter =0;
			StringBuilder		sbSql=null;
			DbReader			objReader =null;
			string				sParentAccountName ="";
			C1PrintDocument		objPrintDoc = null ;
			int					iCurrent=0;
			int					iTotalNumAccounts=0;
			bool				bDisplayTotals = false;
			RenderTable			objTable = null ;
			TableColumn			objTableColumn = null ;
			string				sFileName="";
			int					iNumAccount =0;
			RenderText			objText = null ;
			int				    iColumnCounter=0;
			int				    iRowCounter=0;
			SubBankAccPrintDef  structSubAccDetail;
			double				dblSumAmount=0;
			int					iTableId=0;
			int					iCodeId=0;
			CCacheFunctions		objCache = null;
			string			    sShortCode="";
			string				sDesc="";
			DbReader			objFundReader=null;
			double				dblPrior=0;			
			ArrayList			arrlstTotalRows=null;
			int					iNumPages=0;
			double				dblTempAmt =0;			
			double []			arrTotalRec;

			
			try
			{
				arrTotalRec = new double[p_iNoOfAccount+1];
				for(iCounter=0;iCounter<= p_iNoOfAccount;++iCounter)
					arrTotalRec[iCounter] =0;

				sbSql = new StringBuilder();
				sbSql.Remove(0,sbSql.Length);
				sbSql.Append("SELECT ACCOUNT_NAME FROM ACCOUNT WHERE ACCOUNT_ID = " + p_iAccountId);				
				objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() );
				if(objReader.Read())
					sParentAccountName  = Conversion.ConvertObjToStr(objReader.GetValue(0));
				objReader.Close();
				
				objPrintDoc = new C1PrintDocument();
				objPrintDoc.DefaultUnit = UnitTypeEnum.Twip ;
				objPrintDoc.PageSettings.Margins.Top = 50 ;
				objPrintDoc.PageSettings.Margins.Left = 50 ;
				objPrintDoc.PageSettings.Margins.Bottom = 50 ;
				objPrintDoc.PageSettings.Margins.Right = 50 ;	
				objPrintDoc.PageSettings.Landscape = true ;
				objPrintDoc.PageHeader.Height = 0 ;
				objPrintDoc.PageFooter.Height = 0 ;
				
				objPrintDoc.StartDoc();

				iCurrent = 0;
				iNumAccount = p_iNoOfAccount;
				iTotalNumAccounts = iNumAccount;
				if (iNumAccount > 8)
					iNumAccount = 8 ;
				bDisplayTotals = false;

				arrlstTotalRows=new ArrayList();		

				while (iCurrent < iTotalNumAccounts + 1)
				{

					#region Page Header
					
					objText = new RenderText( objPrintDoc );
                    objText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.BankReconciliationFor", m_iClientId)) + sParentAccountName + "      " + Conversion.GetDBDateFormat(p_sEndDate, "MMMM dd, yyyy");//dvatsa-cloud
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold );
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	

					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock( objText );

					//Blank Line
					objText.Text="  ";
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold );					
					objPrintDoc.RenderBlock( objText );

					#endregion


					if (iNumAccount - iCurrent < 8) 
						bDisplayTotals = true;				

					#region Table Initialization
					objTable = new RenderTable( objPrintDoc );

				
					objTable.StyleTableCell.Padding.Right = 15 ;
					objTable.StyleTableCell.Padding.Top = 0 ;
					objTable.StyleTableCell.Padding.Bottom = 0 ;	
					objTable.StyleTableCell.Padding.Left = 15 ;	

					objPrintDoc.Style.Borders.AllEmpty = true;										
				
					objTable.Columns.AddSome(1);
					objTableColumn = new TableColumn( objPrintDoc );
					objTableColumn.Width = 2400 ;
					objTable.Columns[0] = objTableColumn ;
					iColumnCounter=1;	
					for(iCounter =iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						objTable.Columns.AddSome(1);
						objTableColumn.Width = 1500 ;	
						objTable.Columns[iColumnCounter] = objTableColumn ;
						iColumnCounter++;
					}
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;
					objTable.Body.Cell( 0 , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                    objTable.Body.Cell(0, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.MoneyMktAccount", m_iClientId));//dvatsa-cloud
					#endregion

					#region Row 0
					iRowCounter=0;
					for(iCounter =iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];												
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = structSubAccDetail.m_sAccountName.Trim();							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						objTable.Columns.AddSome(1);
						objTableColumn = new TableColumn( objPrintDoc );
						objTableColumn.Width = 1700 ;
						objTable.Columns[iColumnCounter] = objTableColumn ;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( 0 , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                        objTable.Body.Cell(0, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.Totals", m_iClientId));//dvatsa-cloud						
					}              
					#endregion

					#region Row 1
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.BeginBal", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblMMBegin);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblMMBegin;
						}						
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);		
					}   
					#endregion

					#region Row 2					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.Deposits", m_iClientId));//dvatsa-cloud
					

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblMMDeposit);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblMMDeposit;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}  				

					#endregion

					#region Row 3					
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.TransToDistAcct", m_iClientId));//dvatsa-cloud
					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",-structSubAccDetail.m_dblMMTransfer);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount -=   structSubAccDetail.m_dblMMTransfer;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}  			

					#endregion

					#region Row 4
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.Adjustments", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblMMAdjustment);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblMMAdjustment;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}     

					#endregion

					#region Row 5
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;															
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.UnAdjBalance", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];						
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblMMSubBal);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblMMSubBal;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion

					#region Row 6
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.DepositsInTransit", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblMMDepTrans);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblMMDepTrans;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}  			    

					#endregion

					#region Row 7
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.TransfersInTransit", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",-structSubAccDetail.m_dblMMTranTrans);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount -=   structSubAccDetail.m_dblMMTranTrans;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}  			   

					#endregion

					#region Row 8
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;			
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.AdjBalance", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblMMBalance);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblMMBalance;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}  			   

					#endregion

					//For Distribution Account

					#region Row 9 - Empty row
					
					++iRowCounter;
					iColumnCounter=0;
					objTable.Body.Rows.AddSome( 1 );														
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)				
					{
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = "           ";
						++iColumnCounter;
					}
					#endregion

					#region Row 10
					
					++iRowCounter;
					iColumnCounter=0;
					objTable.Body.Rows.AddSome( 1 );					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.DistribAccount", m_iClientId));	//dvatsa-cloud				
					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)						
					{	
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = "           ";
						++iColumnCounter;
					}										

					#endregion

					#region Row 11
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;	
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Top= new LineDef(Color.Black,1.2);
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.BegBalance", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Borders.Top= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblBegin);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblBegin;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Top= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion

					#region Row 12
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.TransFromMMAcct", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblMMTransfer);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblMMTransfer;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion

					#region Row 13
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.Deposits", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblDeposits);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblDeposits;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion

					#region Row 14
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.Adjustments", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblAdjustments);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblAdjustments;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion

					#region Row 15
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.ClearedChecks", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblClearedChecks);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblClearedChecks;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion

					#region Row 16
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.UnAdjBalance", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblSubBal);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblSubBal;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion

					#region Row 17
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.DepositsInTransit", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblOutDeposits);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblOutDeposits;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion

					#region Row 18
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.OutstandingChecks", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",-structSubAccDetail.m_dblOutChecks);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount -=   structSubAccDetail.m_dblOutChecks;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion

					#region Row 19
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.OutstandingTransfers", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblMMTranTrans);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblMMTranTrans;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion

					#region Row 20
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.AdjBalance", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblBalance);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblBalance;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion

					#region Row 21 - Empty row

					++iRowCounter;
					iColumnCounter=0;
					objTable.Body.Rows.AddSome( 1 );														
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)				
					{
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = "           ";
						++iColumnCounter;
					}				
					
					#endregion

					#region Row 22
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.ChecksWritten", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",-structSubAccDetail.m_dblChecksWritten);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount -=   structSubAccDetail.m_dblChecksWritten;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion

					#region Row 23 - Empty row
					
					++iRowCounter;
					iColumnCounter=0;
					objTable.Body.Rows.AddSome( 1 );														
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)				
					{
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = "           ";
						++iColumnCounter;
					}
					#endregion

					#region Row 24
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Top= new LineDef(Color.Black,1.2);
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text =  CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.TotalBankAdj",m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Borders.Top= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblBalance+structSubAccDetail.m_dblMMBalance);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblBalance+structSubAccDetail.m_dblMMBalance;
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Borders.Top= new LineDef(Color.Black,1.2);
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}
					#endregion				

					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock(objTable);	
			
					objPrintDoc.NewPage();

					#region Page Header

					objText.Text= CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.BankReconDetails",m_iClientId)) + sParentAccountName + "      " + Conversion.GetDBDateFormat(p_sEndDate,"MMMM dd, yyyy") ;
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold );
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	

					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock( objText );

					//Blank Line
					objText.Text="  ";
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold );					
					objPrintDoc.RenderBlock( objText );

					#endregion

					#region Table Initialization
					objTable.Dispose();
					objTable = new RenderTable( objPrintDoc );
					objTable.Columns.AddSome(1);
					objTableColumn = new TableColumn( objPrintDoc );
					objTableColumn.Width = 2400 ;
					objTable.Columns[0] = objTableColumn ;
					iColumnCounter=1;	
					for(iCounter =iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						objTable.Columns.AddSome(1);
						objTableColumn.Width = 1500 ;	
						objTable.Columns[iColumnCounter] = objTableColumn ;
						iColumnCounter++;
					}
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;
					objTable.Body.Cell( 0 , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                    objTable.Body.Cell(0, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.ReconAdjustDetail", m_iClientId));//dvatsa-cloud
					#endregion

					#region New Page - Row 0
					iRowCounter=0;
					for(iCounter =iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = structSubAccDetail.m_sAccountName.Trim();							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						objTable.Columns.AddSome(1);
						objTableColumn = new TableColumn( objPrintDoc );
						objTableColumn.Width = 1700 ;
						objTable.Columns[iColumnCounter] = objTableColumn ;
						objTable.Body.Cell( 0 , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( 0 , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                        objTable.Body.Cell(0, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.Totals", m_iClientId));	//dvatsa-cloud					
					}              
					#endregion

					#region Row 1 - Empty row
					
					++iRowCounter;
					iColumnCounter=0;
					objTable.Body.Rows.AddSome( 1 );														
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)				
					{
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = "           ";
						++iColumnCounter;
					}
					#endregion
					
					#region Row 2 - 13

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'ADJUSTMENT_TYPE'");
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
					if(objReader.Read())
						iTableId = Conversion.ConvertObjToInt(objReader.GetValue("TABLE_ID"), m_iClientId);
					objReader.Close();

					objCache = new CCacheFunctions(m_sConnectionString,m_iClientId);//dvatsa-cloud

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT CODE_ID FROM CODES WHERE TABLE_ID = " +  iTableId + " AND DELETED_FLAG = 0");
					objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());										
								
					arrlstTotalRows.Clear();
					iNumPages =0;
					while(objReader.Read())				
					{
						arrlstTotalRows.Add(0);
						iCodeId = Conversion.ConvertObjToInt(objReader.GetValue("CODE_ID"), m_iClientId);
						sShortCode="";
						sDesc="";
						objCache.GetCodeInfo(iCodeId,ref sShortCode,ref sDesc);
						dblSumAmount =0;

						++iRowCounter;
						objTable.Body.Rows.AddSome( 1 );					
						iColumnCounter=0;					
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text =sDesc;

						for(iCounter = iCurrent;iCounter <= iNumAccount -1;++iCounter)
						{
							sbSql.Remove(0,sbSql.Length);
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + structSubAccDetail.m_iAccounId + " AND DEPOSIT_TYPE IN (1,4)");
							sbSql.Append(" AND ADJUST_CODE = " + iCodeId + " AND VOID_FLAG = 0");
							sbSql.Append(" AND VOIDCLEAR_DATE >= '" + p_sBeginDate + "' AND TRANS_DATE <= '" + p_sEndDate + "'");
							objFundReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
							if (objFundReader.Read())
								dblPrior = Conversion.ConvertObjToDouble(objFundReader.GetValue(0), m_iClientId);
							objFundReader.Close();

							objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
							objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
							objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",dblPrior);		 							
							iColumnCounter++;

							dblSumAmount += dblPrior;
							arrTotalRec[iCounter] += dblPrior;
						}
						arrTotalRec[iTotalNumAccounts] += dblSumAmount;
						dblTempAmt =0;
						dblTempAmt = Convert.ToDouble(arrlstTotalRows[iNumPages]);
						arrlstTotalRows[iNumPages]  = dblTempAmt + dblSumAmount;
						if (bDisplayTotals)
						{
							iColumnCounter++;
							objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
							objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
							objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",arrlstTotalRows[iNumPages]);	
						} 					      
						iNumPages = iNumPages + 1;
					}
					objReader.Close();
					#endregion

					#region Row 14 - Empty row
					
					++iRowCounter;
					iColumnCounter=0;
					objTable.Body.Rows.AddSome( 1 );														
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)				
					{
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = "           ";
						++iColumnCounter;
					}
					#endregion

					#region Row 15
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );					
					iColumnCounter=0;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReport.TotalAdjustments", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = string.Format("{0:C}",arrTotalRec[iCounter] );		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",arrTotalRec[iTotalNumAccounts]);								
					}
					#endregion

					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock(objTable);	

					iCurrent += 8;
				    iNumAccount +=  8;
					if (iNumAccount > iTotalNumAccounts)
						iNumAccount = iTotalNumAccounts;
     
					if (! bDisplayTotals)
						objPrintDoc.NewPage();
				}
				objPrintDoc.EndDoc();				
				SaveToFile(objPrintDoc,ref sFileName);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Reports.PrintMasterAccountBalReport.Error", m_iClientId), p_objException);//dvatsa-cloud
			}	
			finally
			{
				sbSql =null;
				objPrintDoc = null ;
				if(objReader !=null)
				{
					objReader.Dispose();
					objReader =null;
				}
				if(objFundReader !=null)
				{
					objFundReader.Dispose();
					objFundReader =null;
				}
				objTable = null ;
				objTableColumn = null ;
				objText = null ;
				objCache = null;				
				arrlstTotalRows=null;
				
			}
			return sFileName;
		}
		#endregion

		#region "PrintMasterAccountBalReportSpecial(ArrayList p_arrlstSubAccDetails,int p_iNoOfAccount,int p_iAccountId,string p_sBeginDate,string p_sEndDate)"
		/// Name			: PrintMasterAccountBalReportSpecial
		/// Author			: Mohit Yadav
		/// Date Created	: 29-Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method is used to format & generate the PDF report along with the data.
		/// </summary>
		/// <param name="p_arrlstSubAccDetails"></param>
		/// <param name="p_iNoOfAccount"></param>
		/// <param name="p_iAccountId"></param>
		/// <param name="p_sBeginDate"></param>
		/// <param name="p_sEndDate"></param>
		/// <returns>returns the string containing the report details with data</returns>
		private string PrintMasterAccountBalReportSpecial(ArrayList p_arrlstSubAccDetails,int p_iNoOfAccount,int p_iAccountId,string p_sBeginDate,string p_sEndDate)
		{
			double []			arrTotalRec;
			int					iCounter =0;
			StringBuilder		sbSql=null;
			DbReader			objReader =null;
			string				sParentAccountName ="";
			ArrayList			arrlstTotalRows=null;
			C1PrintDocument		objPrintDoc = null;
			int					iCurrent=0;
			int					iNumAccount =0;
			int					iTotalNumAccounts=0;
			bool				bDisplayTotals = false;
			RenderText			objText = null;
			RenderTable			objTable = null;
			TableColumn			objTableColumn = null;
			int				    iColumnCounter=0;
			int				    iRowCounter=0;
			SubBankAccPrintDef  structSubAccDetail;
			double				dblSumAmount=0;
			string				sFileName="";
			
			try
			{
				arrTotalRec = new double[p_iNoOfAccount+1];
				for(iCounter=0;iCounter<= p_iNoOfAccount;++iCounter)
					arrTotalRec[iCounter] = 0;

				arrlstTotalRows=new ArrayList();

				sbSql = new StringBuilder();
				sbSql.Remove(0,sbSql.Length);				
				sbSql.Append("SELECT ACCOUNT_NAME FROM ACCOUNT WHERE ACCOUNT_ID = " + p_iAccountId);				
				objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() );
				if(objReader.Read())
					sParentAccountName  = Conversion.ConvertObjToStr(objReader.GetValue(0));
				objReader.Close();
				
				objPrintDoc = new C1PrintDocument();
				objPrintDoc.DefaultUnit = UnitTypeEnum.Twip;
				objPrintDoc.PageSettings.Margins.Top = 50 ;
				objPrintDoc.PageSettings.Margins.Left = 50 ;
				objPrintDoc.PageSettings.Margins.Bottom = 50 ;
				objPrintDoc.PageSettings.Margins.Right = 50 ;
				objPrintDoc.PageSettings.Landscape = true ;
				objPrintDoc.PageHeader.Height = 0 ;
				objPrintDoc.PageFooter.Height = 0 ;

				objPrintDoc.StartDoc();
				iCurrent = 0;
				iNumAccount = p_iNoOfAccount;
				iTotalNumAccounts = iNumAccount;
				//If client has more than seven sub bank accounts
				if (iNumAccount > 8)
					iNumAccount = 8 ;
				bDisplayTotals = false;

				while (iCurrent < iTotalNumAccounts + 1)
				{
					#region Page Header
					
					objText = new RenderText(objPrintDoc);
					objText.Text= CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReportSpecial.BankReconciliationFor",m_iClientId)) + sParentAccountName + "      " + Conversion.GetDBDateFormat(p_sEndDate,"MMMM dd, yyyy");
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold );
					objText.Style.WordWrap = false;
					objText.AutoWidth = true;	

					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center;
					objPrintDoc.RenderBlock(objText);

					//Blank Line
					objText.Text="  ";
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold );					
					objPrintDoc.RenderBlock( objText );

					#endregion

					if (iNumAccount - iCurrent < 8) 
						bDisplayTotals = true;	

					#region Table Initialization

					objTable = new RenderTable(objPrintDoc);
				
					objTable.StyleTableCell.Padding.Right = 15 ;
					objTable.StyleTableCell.Padding.Top = 0 ;
					objTable.StyleTableCell.Padding.Bottom = 0 ;	
					objTable.StyleTableCell.Padding.Left = 15 ;	

					objPrintDoc.Style.Borders.AllEmpty = true;				
					objTable.Columns.AddSome(1);

					objTableColumn = new TableColumn(objPrintDoc);
					objTableColumn.Width = 2400 ;
					objTable.Columns[0] = objTableColumn ;
					iColumnCounter=1;	
					for(iCounter =iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						objTable.Columns.AddSome(1);
						objTableColumn.Width = 1500 ;	
						objTable.Columns[iColumnCounter] = objTableColumn ;
						iColumnCounter++;
					}
					objTable.Body.Rows.AddSome(1);					
					iColumnCounter=0;
					objTable.Body.Cell(0,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Bold);
                    objTable.Body.Cell(0, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReportSpecial.SubBankAccount", m_iClientId));//dvatsa-cloud

					#endregion
					#region Row 0

					iRowCounter=0;
					for(iCounter =iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];												
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );			
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).RenderText.Text = structSubAccDetail.m_sAccountName.Trim();							
						iColumnCounter++;
					}

					//Determine if total should be shown in this one or not
					if (bDisplayTotals)
					{
						iColumnCounter++;
						objTable.Columns.AddSome(1);
						objTableColumn = new TableColumn(objPrintDoc);
						objTableColumn.Width = 1700 ;
						objTable.Columns[iColumnCounter] = objTableColumn ;
						objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(0,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Bold);
                        objTable.Body.Cell(0, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReportSpecial.Totals", m_iClientId));//dvatsa-cloud
					}

					#endregion
					#region Row 1 "Beginning Balance"

					++iRowCounter;
					objTable.Body.Rows.AddSome(1);					
					iColumnCounter=0;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Bold);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReportSpecial.BeginBal", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.Font= new Font("Arial",9,FontStyle.Bold);			
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblBegin);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblBegin; //sum for row totals
						}						
						objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Bold);			
						objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);		
					}

					#endregion
					#region Row 2 - Empty row

						++iRowCounter;
						iColumnCounter=0;
						objTable.Body.Rows.AddSome( 1 );														
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";

						for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)				
						{
							objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
							objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = "           ";
							++iColumnCounter;
						}

					#endregion
					#region Row 3 "Deposits"

					++iRowCounter;
					objTable.Body.Rows.AddSome(1);					
					iColumnCounter=0;					
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Regular);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReportSpecial.Deposits", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.Font= new Font("Arial",9,FontStyle.Regular);			
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblDeposits);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblDeposits; //sum for row totals
						}
						objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Regular);			
						objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}  				

					#endregion
					#region Row 4 "Adjustments"
					
					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					iColumnCounter=0;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Regular);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReportSpecial.Adjustments", m_iClientId));//dvatsa-cloud
					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.Font= new Font("Arial",9,FontStyle.Regular);			
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblAdjustments);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblAdjustments; //sum for row totals
						}
						objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Regular);			
						objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}

					#endregion
					#region Row 5 "Checks Written"

					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					iColumnCounter=0;					
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Regular);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReportSpecial.ChecksWritten", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.Font= new Font("Arial",9,FontStyle.Regular);			
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).RenderText.Text = string.Format("{0:C}",-structSubAccDetail.m_dblChecksWritten);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblChecksWritten; //sum for row totals
						}
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;						
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell (iRowCounter , iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}

					#endregion
					#region Row 6 "Change This Month"

					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					iColumnCounter=0;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReportSpecial.ChangeThisMonth", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];						
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.Font= new Font("Arial",9,FontStyle.Bold);
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblBalance);
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblBalance; //sum for row totals
						}
						objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Borders.Bottom= new LineDef(Color.Black,1.2);
						objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Bold);			
						objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}

					#endregion
					#region Row 7 - Empty row

						++iRowCounter;
						iColumnCounter=0;
						objTable.Body.Rows.AddSome( 1 );														
						objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";

						for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)				
						{
							objTable.Body.Cell( iRowCounter , iColumnCounter+1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );			
							objTable.Body.Cell( iRowCounter , iColumnCounter+1 ).RenderText.Text = "           ";
							++iColumnCounter;
						}				
									
					#endregion
					#region Row 8 "Current Balance"

					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					iColumnCounter=0;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Bold);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMasterAccountBalReportSpecial.CurrentBalance", m_iClientId));//dvatsa-cloud

					for(iCounter = iCurrent;iCounter <= iNumAccount - 1;++iCounter)
					{
						structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).StyleTableCell.Font= new Font("Arial",9,FontStyle.Bold);
						objTable.Body.Cell(iRowCounter,iColumnCounter+1).RenderText.Text = string.Format("{0:C}",structSubAccDetail.m_dblBalance + structSubAccDetail.m_dblBegin);		 							
						iColumnCounter++;
					}
					if (bDisplayTotals)
					{
						iColumnCounter++;
						dblSumAmount=0;
						for(iCounter=0;iCounter <= iNumAccount -1;++iCounter)
						{
							structSubAccDetail = (SubBankAccPrintDef) p_arrlstSubAccDetails[iCounter];
							dblSumAmount +=   structSubAccDetail.m_dblBalance + structSubAccDetail.m_dblBegin; //sum for row totals
						}
						objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
						objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",9,FontStyle.Bold);			
						objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = string.Format("{0:C}",dblSumAmount);								
					}  			    

					#endregion

					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock(objTable);	

					iCurrent += 8;
					iNumAccount +=  8;
					if (iNumAccount > iTotalNumAccounts)
						iNumAccount = iTotalNumAccounts;
     
					if (! bDisplayTotals)
						objPrintDoc.NewPage();
				}
				objPrintDoc.EndDoc();				
				SaveToFile(objPrintDoc,ref sFileName);
			}		
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Reports.PrintMasterAccountBalReportSpecial.Error", m_iClientId), p_objException);//dvatsa-cloud
			}	
			finally
			{
				sbSql =null;
				objPrintDoc = null ;
				if(objReader !=null)
				{
					objReader.Dispose();
					objReader =null;
				}
				objTable = null ;
				objTableColumn = null ;
				objText = null ;
				arrlstTotalRows=null;
			}
			return sFileName;
		}
		#endregion

		#region "SaveToFile(C1PrintDocument p_objPrintDoc,ref string p_sFileName)"
		/// Name			: SaveToFile
		/// Author			: Neelima Dabral
		/// Date Created	: Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method creates a PDF document from the ComponentOne "C1PrintDocument" document
		/// </summary>
		/// <param name="p_objPrintDoc"></param>
		/// <param name="p_sFileName"></param>
		private void SaveToFile(C1PrintDocument p_objPrintDoc,ref string p_sFileName)
		{
            string sStoragePath = string.Empty;

            sStoragePath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "BankAccountRpt");
            p_sFileName = String.Format(@"{0}\{1}", sStoragePath, GetUniqueFileName());
			p_objPrintDoc.ExportToPDF(p_sFileName,false);

		}
		#endregion

		#region "GetUniqueFileName()"
		/// Name			: GetUniqueFileName
		/// Author			: Neelima Dabral
		/// Date Created	: Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Gets the unique file name to be saved.
		/// </summary>
		/// <returns></returns>
		private string GetUniqueFileName()
		{
			string sTempPath = "" ;			
			try
			{
				sTempPath = "BankAccRpt" + System.DateTime.Now.ToString("yyyyMMddHHmmss") 
					+ System.AppDomain.GetCurrentThreadId() + ".PDF" ; 			
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Reports.GetUniqueFileName.Error", m_iClientId), p_objEx);		//dvatsa-cloud		
			}
			return sTempPath ;
		}
		#endregion

		#region "PrintMonthyBankReconReport(int p_iAccountId)"
		/// Name			: PrintMonthyBankReconReport
		/// Author			: Neelima Dabral
		/// Date Created	: Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method is used to format & generate the PDF report along with the data.
		/// </summary>
		/// <param name="p_iAccountId"></param>
		/// <returns>returns the PDF</returns>
		private string PrintMonthyBankReconReport(int p_iAccountId)
		{
			bool			bFhOnlyPrinted = false;
			StringBuilder	sbSql = null;
			DbReader		objReader = null;
			string			sBeginDate="";
			string			sEndDate="";
			int				iSubAccId =0;
			string			sSubAccName="";
			DbReader		objSubAccDtlReader = null;
			double			dblPayments=0;
			double			dblPrior=0;
			double			dblTempAmt=0;
			double			dblCollections=0;
			double			dblClearedChecks=0;
			double			dblDepTransit =0;     
			double          dblTotalAdditions = 0;
			double			dblDeposits = 0 ;      
			double          dblTotalSubtractions = 0;
			double          dblOutChecks =0;
			double          dblCheckWritten=0;
			bool			bFlag=false;
			C1PrintDocument objPrintDoc=null;
			string			sFileName="";
			RenderText		objText=null;
			RenderTable		objTable=null;
			TableColumn		objTableColumn=null;
			double			dblAdjustments=0;
			int				iCounter=0;
			int				iColumnCounter=0;
			int				iRowCounter=0;
			try
			{
				if (ChkFhPrintedOnly())				
					bFhOnlyPrinted = true;

				sbSql = new StringBuilder();
				
				sbSql.Remove(0,sbSql.Length);
				sbSql.Append("SELECT END_DATE,BEGIN_DATE FROM BANK_ACC_RECON WHERE ACCOUNT_ID=" + p_iAccountId);
				sbSql.Append(" ORDER BY END_DATE DESC");				

				objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());				
				
				if (!objReader.Read())
                    throw new RMAppException(Globalization.GetString("Reports.PrintMonthyBankReconReport.AccNotBal", m_iClientId));	//dvatsa-cloud									

				sEndDate = Conversion.ConvertObjToStr(objReader.GetValue("END_DATE"));				
				sBeginDate = Conversion.ConvertObjToStr(objReader.GetValue("BEGIN_DATE"));				
				objReader.Close();

				sbSql.Remove(0,sbSql.Length);
				sbSql.Append("SELECT SUB_ROW_ID,SUB_ACC_NAME FROM BANK_ACC_SUB WHERE ACCOUNT_ID = " + p_iAccountId);
				objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
				if(! objReader.Read())
                    throw new RMAppException(Globalization.GetString("Reports.PrintMonthyBankReconReport.NoSubAccountsDefined", m_iClientId));		//dvatsa-cloud								

				do
				{
					iSubAccId = Conversion.ConvertObjToInt(objReader.GetValue("SUB_ROW_ID"), m_iClientId);				
					sSubAccName = Conversion.ConvertObjToStr(objReader.GetValue("SUB_ACC_NAME"));		

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + iSubAccId);

					if (bFhOnlyPrinted)
						sbSql.Append("AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0 AND STATUS_CODE = 1054");
					else
						sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0");       
        
					sbSql.Append(" AND VOID_DATE < '" + sBeginDate + "' ");

					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND PAYMENT_FLAG <> 0");					
					if(objSubAccDtlReader.Read())
						dblPayments = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close();

					//Collections					
					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND PAYMENT_FLAG = 0");					
					if(objSubAccDtlReader.Read())
						dblCollections = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close();

					//Deposits
					//0 DEPOSIT 1 ADJUSTMENT 2 MM DEPOSIT 3 MM TRANSFER 4 MM ADJUSTMENT
					//For cleared deposits go to collections also

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0");
					sbSql.Append(" AND VOIDCLEAR_DATE < '" + sBeginDate + "'");

					//include everything except transfers
					sbSql.Append(" AND DEPOSIT_TYPE <> 3 ");
					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());					
					if (objSubAccDtlReader.Read())
						dblTempAmt = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close();        
	        
				    dblPrior = dblTempAmt + dblCollections - dblPayments;

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + iSubAccId);										
					if (bFhOnlyPrinted)
						sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0 AND STATUS_CODE = 1054");
					else
						sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0");

					sbSql.Append(" AND VOID_DATE >= '" + sBeginDate + "'");
					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DATE_OF_CHECK <= '" + sEndDate + "' AND PAYMENT_FLAG <> 0");					
					if (objSubAccDtlReader.Read())
						dblClearedChecks = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close(); 

					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString()+ " AND TRANS_DATE <= '" + sEndDate + "' AND PAYMENT_FLAG = 0" );					
					if (objSubAccDtlReader.Read())
						dblCollections = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close(); 

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0");
					sbSql.Append(" AND VOIDCLEAR_DATE >= '" + sBeginDate + "' AND TRANS_DATE <= '" + sEndDate + "'");
					sbSql.Append(" AND DEPOSIT_TYPE IN (0,2)");
					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
					if (objSubAccDtlReader.Read())
						dblTempAmt = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close(); 

					dblDeposits = dblTempAmt + dblCollections;

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);        					
					sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0");
					sbSql.Append(" AND VOIDCLEAR_DATE >= '" + sBeginDate + "' AND TRANS_DATE <= '" + sEndDate + "'");
					sbSql.Append(" AND DEPOSIT_TYPE IN (1,4)");
					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());					
					if (objSubAccDtlReader.Read())
						dblAdjustments = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close(); 

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + iSubAccId);				 
					sbSql.Append(" AND CLEARED_FLAG = 0 AND VOID_FLAG = 0 ");
					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString()+" AND DATE_OF_CHECK <= '" + sEndDate + "' AND PAYMENT_FLAG <> 0 AND STATUS_CODE = 1054");
					if (objSubAccDtlReader.Read())
						dblOutChecks = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close(); 

					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString()+" AND TRANS_DATE <= '" + sEndDate + "' AND PAYMENT_FLAG = 0");
					if (objSubAccDtlReader.Read())
						dblCollections = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close(); 

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append(" AND CLEARED_FLAG = 0 AND VOID_FLAG = 0");
					sbSql.Append(" AND DEPOSIT_TYPE IN (0,2) AND TRANS_DATE <= '" + sEndDate + "'");
					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
					if (objSubAccDtlReader.Read())
						dblTempAmt = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close(); 

					dblDepTransit = dblTempAmt + dblCollections   ;     
					dblTotalAdditions = dblDepTransit + dblDeposits  ;      
					dblTotalSubtractions = dblOutChecks + dblClearedChecks;

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + iSubAccId);					 
					if (bFhOnlyPrinted)
						sbSql.Append(" AND PAYMENT_FLAG <> 0 AND VOID_FLAG = 0 AND STATUS_CODE=1054");
					else
						sbSql.Append(" AND PAYMENT_FLAG <> 0 AND VOID_FLAG = 0");					
					sbSql.Append(" AND DATE_OF_CHECK BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'");

					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());					
					if (objSubAccDtlReader.Read())
						dblCheckWritten = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close(); 
  
					if(!bFlag)
					{
						objPrintDoc = new C1PrintDocument();
						objPrintDoc.DefaultUnit = UnitTypeEnum.Twip ;
						objPrintDoc.PageSettings.Margins.Top = 50 ;
						objPrintDoc.PageSettings.Margins.Left = 50 ;
						objPrintDoc.PageSettings.Margins.Bottom = 50 ;
						objPrintDoc.PageSettings.Margins.Right = 50 ;	
						objPrintDoc.PageSettings.Landscape = true ;
						objPrintDoc.PageHeader.Height = 0 ;
						objPrintDoc.PageFooter.Height = 0 ;
				
						objPrintDoc.StartDoc();
						bFlag = true;
					}
					else
						objPrintDoc.NewPage();

					#region Page Heading

					objText = new RenderText( objPrintDoc );
                    objText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.BankReconFor", m_iClientId));	//dvatsa-cloud				
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold|FontStyle.Italic );
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock( objText );					

					objText.Text=sSubAccName;					
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock( objText );

					objText.Text=Conversion.GetDBDateFormat(sEndDate,"MMMM dd, yyyy").ToUpper();					
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock( objText );

					objText.Text="        ";
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock( objText );

					objText.Text="        ";
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock( objText );

					objText.Text="            ";
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock( objText );

					objText.Text="            ";
					objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock( objText );

					#endregion					

					#region Row 0

					objTable = new RenderTable( objPrintDoc );					
					iColumnCounter = 0;
					iRowCounter = 0;

					objTable.Style.Borders.AllEmpty=true;
					objTable.Body.StyleTableCell.BorderTableHorz.Empty= true ;
					objTable.Body.StyleTableCell.BorderTableVert.Empty= true ;	

					objTable.Columns.AddSome(1);
					objTable.Body.Rows.AddSome(1);
					objTableColumn = new TableColumn( objPrintDoc );
					objTableColumn.Width = 4800 ;
					objTable.Columns[iColumnCounter] = objTableColumn ;					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.BalancePerBank", m_iClientId));	//dvatsa-cloud				
					++iColumnCounter;	
					objTable.Columns.AddSome(1);
					objTableColumn = new TableColumn( objPrintDoc );
					objTableColumn.Width = 2600 ;
					objTable.Columns[iColumnCounter] = objTableColumn ;															
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = " ";	
					++iColumnCounter;	
					objTable.Columns.AddSome(1);
					objTableColumn = new TableColumn( objPrintDoc );
					objTableColumn.Width = 2880 ;
					objTable.Columns[iColumnCounter] = objTableColumn ;									
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;	
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = string.Format("{0:C}",dblPrior);		 	
					#endregion
					#region Row 1 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}					
					
					#endregion
					#region Row 2								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.BankBalAdditions", m_iClientId));		//dvatsa-cloud								
					for(iCounter=1;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}					
					#endregion
					#region Row 3 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}
					#endregion
					#region Row 4								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.Deposits", m_iClientId));	//dvatsa-cloud									
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;	
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text =  string.Format("{0:C}",dblDeposits);											
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";

					#endregion
					#region Row 5								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.DepInTransit", m_iClientId));	//dvatsa-cloud									
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;	
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text =  string.Format("{0:C}",dblDepTransit);											
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";

					#endregion
					#region Row 6 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}
					#endregion
					#region Row 7								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.TotalAdditions", m_iClientId));	//dvatsa-cloud									
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";				
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;	
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text =  string.Format("{0:C}",dblTotalAdditions);																
					#endregion
					#region Row 8 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}
					#endregion
					#region Row 9 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}
					#endregion
					#region Row 10								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.SubFromBankBalance", m_iClientId));	//dvatsa-cloud									
					for(iCounter=1;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}					
					#endregion
					#region Row 11 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}
					#endregion
					#region Row 12								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.OutstandingChecks", m_iClientId));	//dvatsa-cloud									
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;	
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text =  string.Format("{0:C}",dblOutChecks);											
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";
					#endregion
					#region Row 13								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.ClearedChecks", m_iClientId));//dvatsa-cloud										
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;	
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text =  string.Format("{0:C}",dblClearedChecks);											
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";
					#endregion
					#region Row 14								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.TotalSubs", m_iClientId));	//dvatsa-cloud									
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";				
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;	
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text =  string.Format("{0:C}",dblTotalSubtractions);																
					#endregion
					#region Row 15 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}
					#endregion
					#region Row 16 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}
					#endregion
					#region Row 17								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.TotalAdjToBankBal", m_iClientId));		//dvatsa-cloud								
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";				
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;	
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text =  string.Format("{0:C}",dblAdjustments);																
					#endregion
					#region Row 18 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}
					#endregion
					#region Row 19 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}
					#endregion

					dblTempAmt = dblPrior + dblDeposits + dblDepTransit + dblAdjustments - dblOutChecks - dblClearedChecks;

					#region Row 20								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.EndBalance", m_iClientId));	//dvatsa-cloud									
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";				
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;	
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Bold );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text =  string.Format("{0:C}",dblTempAmt);																
					#endregion
					#region Row 21 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}
					#endregion
					#region Row 22 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}
					#endregion
					#region Row 23								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = 
						 CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.ChecksWrittenFrom",m_iClientId)) + " " +
						Conversion.GetDBDateFormat(sBeginDate,"MMMM dd, yyyy").ToUpper()+ " " + 
						 CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReport.ChecksWrittenTo",m_iClientId)) + " " +
						Conversion.GetDBDateFormat(sEndDate,"MMMM dd, yyyy").ToUpper();	
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text = "          ";				
					++iColumnCounter;
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;	
					objTable.Body.Cell( iRowCounter , iColumnCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
					objTable.Body.Cell( iRowCounter , iColumnCounter ).RenderText.Text =  string.Format("{0:C}",dblCheckWritten);																
					#endregion

					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock( objTable );

				}while (objReader.Read());		
	
				objPrintDoc.EndDoc();				
				SaveToFile(objPrintDoc,ref sFileName);
				return sFileName;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Reports.PrintMonthyBankReconReport.Error", m_iClientId), p_objException);//dvatsa-cloud
			}	
			finally
			{
				sbSql =null;
				if(objReader != null)
				{
					objReader.Dispose();
					objReader = null;
				}
				if(objSubAccDtlReader != null)
				{
					objSubAccDtlReader.Dispose();
					objSubAccDtlReader = null;
				}	
				objPrintDoc = null;
				objText=null;				
				objTable=null;
				objTableColumn=null;
			}

		}	

		#endregion

		#region "PrintMonthyBankReconReportSpecial(int p_iAccountId)"
		/// Name			: PrintMonthyBankReconReportSpecial
		/// Author			: Mohit Yadav
		/// Date Created	: 03-Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method is used to format & generate the PDF report along with the data.
		/// </summary>
		/// <param name="p_iAccountId"></param>
		/// <returns>returns the PDF</returns>
		private string PrintMonthyBankReconReportSpecial(int p_iAccountId)
		{						
			bool			bFlag=false;
			int				iType;
			string			sBeginDate="";
			string			sEndDate="";
			StringBuilder	sbSql = null;
			DbReader		objReader = null;
			int				iSubAccId =0;
			string			sSubAccName="";
			DbReader		objSubAccDtlReader = null;
			double			dblCollections=0;
			double			dblPayments=0;
			double			dblTempAmt=0;
			double			dblPrior=0;
			double          dblDepAdditions = 0;
			double			dblAdjustments=0;
			double			dblDeposits = 0;
			double          dblTotalAdditions = 0;
			double          dblTotalSubtractions = 0;
			double			dblOutChecks = 0;
			C1PrintDocument objPrintDoc=null;			
			RenderText		objText=null;
			RenderTable		objTable=null;
			TableColumn		objTableColumn=null;
			int				iCounter=0;
			int				iColumnCounter=0;
			int				iRowCounter=0;
			string			sFileName="";

			try
			{
				iType = GetFundsAcountBalanceDateCriteria();
				
				//Get the dates for reconciliation
				sEndDate = DateTime.Now.ToString("yyyyMMdd");
				sBeginDate = DateTime.Now.ToString("yyyy") + DateTime.Now.ToString("MM") + "01";

				#region "SQL Part"
				
				sbSql = new StringBuilder();
				sbSql.Remove(0,sbSql.Length);
				sbSql.Append("SELECT SUB_ROW_ID,SUB_ACC_NAME FROM BANK_ACC_SUB WHERE ACCOUNT_ID =" + p_iAccountId);

				objReader = DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
				
				if (!objReader.Read())
                    throw new RMAppException(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.NoSubAccountsDefined", m_iClientId));//dvatsa-cloud
				
				do
				{
					iSubAccId = Conversion.ConvertObjToInt(objReader.GetValue("SUB_ROW_ID"), m_iClientId);
					sSubAccName = Conversion.ConvertObjToStr(objReader.GetValue("SUB_ACC_NAME"));

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + iSubAccId);
					sbSql.Append(" AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0 ");

					//Collections
					if (iType != 2)
					{
						objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND PAYMENT_FLAG = 0 AND TRANS_DATE < '" + sBeginDate + "'");					
						if(objSubAccDtlReader.Read())
							dblCollections = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
						objSubAccDtlReader.Close();
					}
					if (iType == 2)
					{
						sbSql.Append(" AND STATUS_CODE = 1054 ");
					}

					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND PAYMENT_FLAG <> 0 AND DATE_OF_CHECK < '" + sBeginDate + "'");					
					if(objSubAccDtlReader.Read())
						dblPayments = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close();

					//Deposits
					//0-DEPOSIT 1-ADJUSTMENT 2-MM DEPOSIT 3-MM TRANSFER 4-MM ADJUSTMENT

					//Cleared deposits go to collections also
					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append(" AND VOID_FLAG = 0");
					sbSql.Append(" AND TRANS_DATE < '" + sBeginDate + "'");

					//include everything except transfers
					sbSql.Append(" AND DEPOSIT_TYPE IN (0,1)");
					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
					if (objSubAccDtlReader.Read())
						dblTempAmt = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);
					objSubAccDtlReader.Close();

					dblPrior = dblTempAmt + dblCollections - dblPayments;

					//Addition & Subtraction to Balance that are cleared

					//Use the values here to get the bank account balance first line

					//Deposits 0-DEPOSIT 1-ADJUSTMENT 2-MM DEPOSIT 3-MM TRANSFER 4-MM ADJUSTMENT
					//Cleared deposits go to collections also

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);
					sbSql.Append(" AND VOID_FLAG = 0");
					sbSql.Append(" AND TRANS_DATE BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'");
					sbSql.Append(" AND DEPOSIT_TYPE =0"); //only deposits
					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());
					if (objSubAccDtlReader.Read())
						dblTempAmt = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close(); 

					dblDepAdditions = dblTempAmt; //need both collections and deposits

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + iSubAccId);        					
					sbSql.Append(" AND VOID_FLAG = 0");
					sbSql.Append(" AND TRANS_DATE BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'");
					sbSql.Append(" AND DEPOSIT_TYPE = 1"); //only adjustments
					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString());					
					if (objSubAccDtlReader.Read())
						dblAdjustments = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close(); 

					//Collections and Payments

					sbSql.Remove(0,sbSql.Length);
					sbSql.Append("SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + iSubAccId);				 
					sbSql.Append(" AND CLEARED_FLAG = 0 AND VOID_FLAG = 0 ");

					if (iType != 2)
					{
						objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND TRANS_DATE BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "' AND PAYMENT_FLAG = 0");					
						if(objSubAccDtlReader.Read())
							dblCollections = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
						objSubAccDtlReader.Close();
					}

					dblDeposits = dblDepAdditions + dblCollections;
					dblTotalAdditions = dblDeposits;

					if (iType == 2)
					{
						sbSql.Append(" AND STATUS_CODE = 1054 ");
					}

					objSubAccDtlReader= DbFactory.GetDbReader(m_sConnectionString,sbSql.ToString() + " AND DATE_OF_CHECK BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "' AND PAYMENT_FLAG <> 0");
					if(objSubAccDtlReader.Read())
						dblOutChecks = Conversion.ConvertObjToDouble(objSubAccDtlReader.GetValue(0), m_iClientId);	
					objSubAccDtlReader.Close();

					dblTotalSubtractions = dblOutChecks;
					
					#endregion

					if(!bFlag)
					{
						objPrintDoc = new C1PrintDocument();
						objPrintDoc.DefaultUnit = UnitTypeEnum.Twip ;
						objPrintDoc.PageSettings.Margins.Top = 50 ;
						objPrintDoc.PageSettings.Margins.Left = 50 ;
						objPrintDoc.PageSettings.Margins.Bottom = 50 ;
						objPrintDoc.PageSettings.Margins.Right = 50 ;	
						objPrintDoc.PageSettings.Landscape = true ;
						objPrintDoc.PageHeader.Height = 0 ;
						objPrintDoc.PageFooter.Height = 0 ;
				
						objPrintDoc.StartDoc();
						bFlag = true;
					}
					else
						objPrintDoc.NewPage();

					//Start printing

					#region Page Heading

					objText = new RenderText(objPrintDoc);
                    objText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.BankReconFor", m_iClientId));//dvatsa-cloud
					objText.Style.Font = new Font("Arial",12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock(objText);

					objText.Text=sSubAccName;					
					objText.Style.Font = new Font("Arial",12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock(objText);

					objText.Text=Conversion.GetDBDateFormat(sEndDate,"MMMM dd, yyyy").ToUpper();					
					objText.Style.Font = new Font("Arial",12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock(objText);

					objText.Text="        ";
					objText.Style.Font = new Font("Arial",12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock(objText);

					objText.Text="        ";
					objText.Style.Font = new Font("Arial",12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock(objText);

					objText.Text="            ";
					objText.Style.Font = new Font("Arial",12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock(objText);

					objText.Text="            ";
					objText.Style.Font = new Font("Arial",12,FontStyle.Bold|FontStyle.Italic);
					objText.Style.WordWrap = false ;
					objText.AutoWidth = true ;	
					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
					objPrintDoc.RenderBlock(objText);

					#endregion
					#region Row 0 "Balance - Prior Month"

					objTable = new RenderTable(objPrintDoc);					
					iColumnCounter = 0;
					iRowCounter = 0;

					objTable.Style.Borders.AllEmpty=true;
					objTable.Body.StyleTableCell.BorderTableHorz.Empty= true ;
					objTable.Body.StyleTableCell.BorderTableVert.Empty= true ;	

					objTable.Columns.AddSome(1);
					objTable.Body.Rows.AddSome(1);
					objTableColumn = new TableColumn(objPrintDoc);
					objTableColumn.Width = 4800;
					objTable.Columns[iColumnCounter] = objTableColumn;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.BalancePriorMonth", m_iClientId));//dvatsa-cloud
					++iColumnCounter;
	
					objTable.Columns.AddSome(1);
					objTableColumn = new TableColumn(objPrintDoc);
					objTableColumn.Width = 2600 ;
					objTable.Columns[iColumnCounter] = objTableColumn ;															
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);			
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = " ";	
					++iColumnCounter;

					objTable.Columns.AddSome(1);
					objTableColumn = new TableColumn(objPrintDoc);
					objTableColumn.Width = 2880 ;
					objTable.Columns[iColumnCounter] = objTableColumn ;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = string.Format("{0:C}",dblPrior);
					
					#endregion
					#region Row 1 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell(iRowCounter,iCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
						objTable.Body.Cell(iRowCounter,iCounter).RenderText.Text = "          ";
					}					
					
					#endregion
					#region Row 2 "Additions To Bank Balance"

					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.BankBalAdditions", m_iClientId));//dvatsa-cloud
					for(iCounter=1;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell(iRowCounter,iCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
						objTable.Body.Cell(iRowCounter,iCounter).RenderText.Text = "          ";
					}
			
					#endregion
					#region Row 3 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					for(iCounter=0;iCounter<=1;++iCounter)
					{
						objTable.Body.Cell(iRowCounter,iCounter).StyleTableCell.Font= new Font( "Arial",10,FontStyle.Regular);
						objTable.Body.Cell(iRowCounter,iCounter).RenderText.Text = "          ";
					}

					#endregion
					#region Row 4 "Deposits"
								
					iColumnCounter = 0;
					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.Deposits", m_iClientId));//dvatsa-cloud
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text =  string.Format("{0:C}",dblDepAdditions);
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = "          ";

					#endregion
					#region Row 5 "Collections"
								
					iColumnCounter = 0;
					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.Collections", m_iClientId));//dvatsa-cloud
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text =  string.Format("{0:C}",dblCollections);
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = "          ";

					#endregion
					#region Row 6 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					for(iCounter=0;iCounter<=1;++iCounter)
					{
						objTable.Body.Cell(iRowCounter,iCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
						objTable.Body.Cell(iRowCounter,iCounter).RenderText.Text = "          ";
					}

					#endregion
					#region Row 7 "Total Additions"
								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.TotalAdditions", m_iClientId));//dvatsa-cloud
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = "          ";
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text =  string.Format("{0:C}",dblTotalAdditions);

					#endregion
					#region Row 8 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell(iRowCounter,iCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
						objTable.Body.Cell(iRowCounter,iCounter).RenderText.Text = "          ";
					}

					#endregion
					#region Row 9 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell(iRowCounter,iCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
						objTable.Body.Cell(iRowCounter,iCounter).RenderText.Text = "          ";
					}

					#endregion
					#region Row 10 "Total Checks"

					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.TotalChecks", m_iClientId));//dvatsa-cloud
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = "          ";
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text =  string.Format("{0:C}",dblTotalSubtractions);
					

					#endregion
					#region Row 11 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome( 1 );
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell( iRowCounter , iCounter).StyleTableCell.Font= new Font( "Arial" , 10,FontStyle.Regular );			
						objTable.Body.Cell( iRowCounter , iCounter ).RenderText.Text = "          ";
					}
					#endregion
					#region Row 12 "Total Adjustments To Bank Balance"

					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.TotalAdjToBankBal", m_iClientId));//dvatsa-cloud
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = "          ";
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text =  string.Format("{0:C}",dblAdjustments);

					#endregion
					#region Row 13 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell(iRowCounter,iCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
						objTable.Body.Cell(iRowCounter,iCounter).RenderText.Text = "          ";
					}

					#endregion

					dblTempAmt = dblPrior + dblTotalAdditions + dblAdjustments - dblOutChecks;

					#region Row 14 "Ending Balance"

					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
                    objTable.Body.Cell(iRowCounter, iColumnCounter).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.EndBalance", m_iClientId));//dvatsa-cloud
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = "          ";
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Bold);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text =  string.Format("{0:C}",dblTempAmt);

					#endregion
					#region Row 15 - Empty row
					
					++iRowCounter;
					objTable.Body.Rows.AddSome(1);
					for(iCounter=0;iCounter<=2;++iCounter)
					{
						objTable.Body.Cell(iRowCounter,iCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
						objTable.Body.Cell(iRowCounter,iCounter).RenderText.Text = "          ";
					}

					#endregion
					#region Row 16 "Current Period"
								
					iColumnCounter = 0;
					++iRowCounter;				
					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);			
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = 
						 CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.CurrentPeriod",m_iClientId)) + " " +
						Conversion.GetDBDateFormat(sBeginDate,"MM/dd/yyyy").ToUpper()+ " " + //check for format
						 CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.To",m_iClientId)) + " " +
						Conversion.GetDBDateFormat(sEndDate,"MM/dd/yyyy").ToUpper();	//check for format
					++iColumnCounter;
					objTable.Body.Cell(iRowCounter,iColumnCounter).StyleTableCell.Font= new Font("Arial",10,FontStyle.Regular);
					objTable.Body.Cell(iRowCounter,iColumnCounter).RenderText.Text = "          ";
					++iColumnCounter;

					#endregion

					objPrintDoc.Style.AlignChildrenHorz = AlignHorzEnum.Center;
					objPrintDoc.RenderBlock(objTable);

				}while (objReader.Read());

				objReader.Close();

				objPrintDoc.EndDoc();				
				SaveToFile(objPrintDoc,ref sFileName);
				return sFileName;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Reports.PrintMonthyBankReconReportSpecial.Error", m_iClientId), p_objException);//dvatsa-cloud
			}	
			finally
			{
				sbSql =null;
				if(objReader != null)
				{
					objReader.Dispose();
					objReader = null;
				}
				if(objSubAccDtlReader != null)
				{
					objSubAccDtlReader.Dispose();
					objSubAccDtlReader = null;
				}	
				objPrintDoc = null;
				objText=null;				
				objTable=null;
				objTableColumn=null;
			}
		}			
		#endregion

	}


	internal struct SubBankAccPrintDef
	{
		internal int	 m_iAccounId;
		internal string	 m_sAccountName;
		internal double	 m_dblBegin;
		internal double  m_dblDeposits;
		internal double	 m_dblChecksWritten;
		internal double  m_dblAdjustments;
		internal double  m_dblOutChecks;
		internal double  m_dblClearedChecks;
		internal double  m_dblSubBal;
		internal double  m_dblOutDeposits;
		internal double  m_dblBalance;
		internal double  m_dblMMDeposit;
		internal double  m_dblMMAdjustment;
		internal double  m_dblMMTransfer;
		internal double  m_dblMMBegin;
		internal double  m_dblMMSubBal;
		internal double  m_dblMMDepTrans;
		internal double  m_dblMMTranTrans;
		internal double  m_dblMMBalance;
	}
}
